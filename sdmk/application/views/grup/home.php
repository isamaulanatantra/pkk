
<div class="box">
  <div class="box-header">
    <div class="col-md-6" style="padding: 0;">
        <button class="form-control btn btn-primary" data-toggle="modal" data-target="#tambah-grup-menu"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
    </div>
    <div class="col-md-3">
        <a href="<?php echo base_url('Grup/export'); ?>" class="form-control btn btn-default"><i class="glyphicon glyphicon glyphicon-floppy-save"></i> Export Data Excel</a>
    </div>
    <div class="col-md-3">
        <button class="form-control btn btn-default" data-toggle="modal" data-target="#import-pegawai"><i class="glyphicon glyphicon glyphicon-floppy-open"></i> Import Data Excel</button>
    </div>
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="list-data" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>NO</th>
          <th>Nama Grup Menu</th>          
          <th style="text-align: center;">Aksi</th>
        </tr>
      </thead>
      <tbody id="tblUtamaGrup">
        
      </tbody>

      <div class="msg" >
        <?php echo @$this->session->flashdata('msg'); ?>
      </div>
    </table>
  </div>
</div>

<?php echo $modal_tambah_grup_menu; ?>

<div id="tempat-modal"></div>

<?php show_my_confirm('konfirmasiHapus', 'hapus-dataGrup', 'Hapus Data Ini?', 'Ya, Hapus Data Ini'); ?>
<?php
  $data['judul'] = 'Grup';
  $data['url'] = 'Grup/import';
  echo show_my_modal('modals/modal_import', 'import-pegawai', $data);
?>

<script>
  $.get('<?php echo base_url('Grup/tampil'); ?>', function(data) {
      MyTable.fnDestroy();
      $('#tblUtamaGrup').html(data);
      refresh();
    });
</script>