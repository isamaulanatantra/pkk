<?php
  $no = 1;
  foreach ($dataGrup as $grup) {
    ?>
    <tr>
      <td width="5%"><?php echo $no; ?></td>
      <td width="40%"><?php echo $grup->msGrupMenuNama; ?></td>      
      <td class="text-center" style="min-width:20%px;">
          <button class="btn btn-warning update-dataGrup" data-id="<?php echo $grup->msGrupMenuId; ?>"><i class="glyphicon glyphicon-repeat"></i> Edit</button>
          <button class="btn btn-danger konfirmasiHapus-grup" data-id="<?php echo $grup->msGrupMenuId; ?>" data-toggle="modal" data-target="#konfirmasiHapus"><i class="glyphicon glyphicon-remove-sign"></i> Delete</button>          
      </td>
    </tr>
    <?php
    $no++;
  }
?>