<div class="row">
  <div class="row">
  <div class="col-lg-4 col-xs-6">
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3><?php echo $jml_pegawai; ?></h3>

        <p>Jumlah Pegawai</p>
      </div>
      <div class="icon">
        <i class="ion ion-ios-contact"></i>
      </div>
      <a href="<?php echo base_url('Pegawai') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  </div>

  <div class="col-lg-12 col-xs-12">
    <div class="box box-info">
      <div class="box-header with-border">
        <i class="fa fa-briefcase"></i>
        <h3 class="box-title">Statistik Data Pegawai</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <div class="col-xs-4">          
          <table class="table table-bordered" >
            <tbody>
              <tr>
              <th class="text-center">No</th>
              <th class="text-center">Jenis Tenaga</th>
              <th class="text-center">Jumlah</th>
            </tr>
            <?php
            echo $tabel;
            ?>
            </tbody>
          </table>
        </div>
        <div class="col-xs-8">
          <div id="canvas-holder">
            <center>  
              <h3 class="box-title">GRAFIK DATA PEGAWAI PUSKESMAS</h3> <br />
            </center>
            <div id="graph"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- <div class="col-lg-4 col-xs-6">
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?php echo $jml_posisi; ?></h3>

        <p>Jumlah Posisi</p>
      </div>
      <div class="icon">
        <i class="ion ion-ios-briefcase-outline"></i>
      </div>
      <a href="<?php echo base_url('Posisi') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-lg-4 col-xs-6">
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?php echo $jml_kota; ?></h3>

        <p>Jumlah Kota</p>
      </div>
      <div class="icon">
        <i class="ion ion-location"></i>
      </div>
      <a href="<?php echo base_url('Kota') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div> -->

  <!-- <div class="col-lg-6 col-xs-12">
    <div class="box box-info">
      <div class="box-header with-border">
        <i class="fa fa-briefcase"></i>
        <h3 class="box-title">Statistik <small>Data Posisi</small></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <canvas id="data-instansi" style="height:250px"></canvas>
      </div>
    </div>
  </div> -->

  <!-- <div class="col-lg-6 col-xs-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <i class="fa fa-briefcase"></i>
        <h3 class="box-title">Statistik <small>Data Kota</small></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
      </div>
      <div class="box-body">
        <canvas id="data-kota" style="height:250px"></canvas>
      </div>
    </div>
  </div> -->
</div>

<script src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/raphael-min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/morris.min.js'?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'assets/css/morris.css'?>">

<script>
    Morris.Bar({
      element: 'graph',
      data: <?php echo $dataBar;?>,
      xkey: 'msPegawaiJabatan',
      ykeys: ['total'],
      labels: ['Jumlah '],
      barColors: function (row, series, type) {
        if (type === 'bar') {
          var red = Math.ceil(255 * row.y / this.ymax);
          return 'rgb(' + red + ',0,0)';
        }
        else {
          return '#000';
        }
      }
    });
</script>