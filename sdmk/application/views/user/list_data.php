<?php
  $no = 1;
  foreach ($dataUser as $data) {
    
    ?>
    <tr>
      <td width="5%"><?php echo $no; ?></td>
      <td ><?php echo $data->nama; ?></td>
      <td ><?php echo $data->msGrupMenuNama; ?></td>
      <td ><?php echo $data->username; ?></td>
      <td class="text-center" style="min-width:20%px;">
          <button class="btn btn-warning update-dataUser" data-id="<?php echo $data->id; ?>"><i class="glyphicon glyphicon-repeat"></i> Edit</button>
          <button class="btn btn-danger konfirmasiHapus-user" data-id="<?php echo $data->id; ?> " data-toggle="modal" data-target="#konfirmasiHapus"><i class="glyphicon glyphicon-remove-sign"></i> Delete</button>          
      </td>
    </tr>
    <?php
    $no++;
  }
?>