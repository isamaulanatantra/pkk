<footer class="main-footer">
	<!-- To the right -->
	<div class="pull-right hidden-xs">
		Dashboard Admin
	</div>
	<!-- Default to the left -->
	<strong>Copyright &copy; <?php echo date("Y"); ?> <a href="https://dinkes.wonosobokab.go.id/">Dinas Kesehatan Wonosobo</a>.</strong> All rights reserved.
</footer>