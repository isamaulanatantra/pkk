<!-- REQUIRED JS SCRIPTS -->
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>

<!-- <link rel="stylesheet" type="text/css" href="./assets/js/switch.css"> -->

<!-- My Ajax -->

<?php include './assets/js/ajax.php'; ?>

<!-- datetimepicker -->
<link href="<?php echo base_url(); ?>assets/plugins/datepicker/css/bootstrap-datepicker3.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/plugins/datepicker/js/bootstrap-datepicker.js"></script>





