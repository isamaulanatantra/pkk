<?php
  $no = 1;
  foreach ($dataPegawai as $data) {
    // $jenisPegawai = $this->Data_pegawai_model->getJenisPegawaiById($data->msPegawaiJenisTenaga);
    // $unitKerja = $this->Data_pegawai_model->getUnitKerjaById($data->msPegawaiUnitKerja);
    // $status = $data->msPegawaiStatus;
    // if ($status == 1) {
    //   $statusKerja = "PNS";
    // }else if ($status == 2) {
    //   $statusKerja = "BLUD";
    // }
    ?>
    <tr>
      <td width="5%"><?php echo $no; ?></td>
      <td width=""><?php echo $data->msPegawaiNip; ?></td>  
      <td width=""><?php echo $data->msPegawaiNama; ?></td>
      <td width=""><?php echo $data->msPegawaiUnitKerja; ?></td>  
      <td width=""><?php echo $data->msPegawaiJabatan; ?></td>      
      <td width=""><?php echo $data->msPegawaiStatus; ?></td> 
      <td width=""><?php echo $data->msPegawaiGol; ?></td>     
      <td class="text-center" style="min-width:20%px;">
          <button class="btn btn-warning update-dataPegawai" data-id="<?php echo $data->msPegawaiId; ?>"><i class="glyphicon glyphicon-repeat"></i> Edit</button>
          <button class="btn btn-danger konfirmasiHapus-Pegawai" data-id="<?php echo $data->msPegawaiId; ?>" data-toggle="modal" data-target="#konfirmasiHapus"><i class="glyphicon glyphicon-remove-sign"></i> Delete</button>          
      </td>
    </tr>
    <?php
    $no++;
  }
?>