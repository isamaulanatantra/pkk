<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Update Data</h3>
      <form method="POST" id="form-update-user">
        <input type="hidden" name="id" value="<?php echo $dataUser->id; ?>">
        <input type="hidden" name="id_user" value="<?php echo $userdata->id; ?>">

        <div class="form-group">
      <label for="nomor_surat">Nama</label>                   
          <input type="text" class="form-control" placeholder="" name="nama_user" aria-describedby="sizing-addon2" value="<?php echo $dataUser->nama; ?>">
        </div>      

       <div class="form-group">
          <label for="nomor_surat">Grup</label>
          <select name="grup_user" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%">   
            <option value="0">--</option>                  
            <?php
            foreach ($dataGrup as $data) {
              ?>
              <option value="<?php echo $data->msGrupMenuId; ?>" <?php if($data->msGrupMenuId == $dataUser->grupmenu){echo "selected='selected'";} ?> >
                <?php echo $data->msGrupMenuNama; ?>
              </option>
              <?php
            }
            ?>
          </select>
        </div>

    <div class="form-group">
      <label for="nomor_surat">Nama Instansi</label>
      <select name="instansi" id="instansi" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value="0"> --</option>
              <?php
              foreach ($dataUnitKerja as $data) {
                ?>
                <option value="<?php echo $data->msInstansiId; ?>" <?php if($data->msInstansiId == $dataUser->id_instansi){echo "selected='selected'";} ?> >
                  <?php echo $data->msInstansiNama; ?>
                </option>
                <?php
              }
              ?>
          </select>
    </div> 

        <div class="form-group">
          <label for="nomor_surat">Username </label>                   
          <input type="text" class="form-control" placeholder="" name="username_user" aria-describedby="sizing-addon2" value="<?php echo $dataUser->username; ?>">
        </div>        
        
        <div class="form-group">
          <label for="nomor_surat">Password </label>
          <label for="nomor_surat"> (jika tidak ganti kosongi saja) </label>                  
          <input type="password" class="form-control" placeholder="" name="password_user" aria-describedby="sizing-addon2" value="">
        </div> 

        <div class="form-group">      
          <div class="col-md-12">
              <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i> Update Data</button>
          </div>
        </div>
      </form>
</div>

<script type="text/javascript">
$(function () {
    $(".select2").select2();

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
});
</script>