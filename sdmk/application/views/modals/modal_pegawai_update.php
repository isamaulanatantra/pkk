<div class="col-md-12 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Update Data</h3>
      <form method="POST" id="form-update-pegawai">
        <div class="col-md-6">
        <input type="hidden" name="id" value="<?php echo $dataPegawai->msPegawaiId; ?>">        

        <div class="form-group">
          <label for="nomor_surat">NIP / NI BULD</label>
          <input type="text" class="form-control" placeholder="" name="nip_pegawai" aria-describedby="sizing-addon2" value="<?php echo $dataPegawai->msPegawaiNip; ?>">
        </div>

        <div class="form-group">
      <label for="nomor_surat">Nama Pegawai</label>
      <input class="form-control" id="nama_pegawai" name="nama_pegawai" value="<?php echo $dataPegawai->msPegawaiNama; ?>" placeholder="" type="text">
    </div>

    <div class="form-group">
      <label for="nomor_surat">Tempat Lahir</label>
      <input class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?php echo $dataPegawai->msPegawaiTempatLahir; ?>" placeholder="" type="text">
    </div>

    <div class="form-group">
      <label for="nomor_surat">Tanggal Lahir</label>
      <!-- <input class="form-control" id="tgl_lahir" name="tgl_lahir" value="" placeholder="" type="text"> -->
      <input type="text" class="form-control tgl_lahir" name="tgl_lahir" id="tgl_lahir" placeholder="" name="nama_menu" aria-describedby="sizing-addon2" value="<?php echo $dataPegawai->msPegawaiTglLahir; ?>">
    </div>

    <div class="form-group">
      <label for="nomor_surat">Pendidikan</label>
      <select name="pendidikan" id="pendidikan" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%" onselect=""> 
            <option value=""> --</option>
            <option value="SD" <?php if($dataPegawai->msPegawaiPendidikan == 'SD'){echo "selected='selected'";} ?>> SD</option>
            <option value="SMP" <?php if($dataPegawai->msPegawaiPendidikan == 'SMP'){echo "selected='selected'";} ?>> SMP</option>
            <option value="SMA" <?php if($dataPegawai->msPegawaiPendidikan == 'SMA'){echo "selected='selected'";} ?>> SMA</option>
            <option value="D-I" <?php if($dataPegawai->msPegawaiPendidikan == 'D-I'){echo "selected='selected'";} ?>> D-I</option>
            <option value="D-II" <?php if($dataPegawai->msPegawaiPendidikan == 'D-II'){echo "selected='selected'";} ?>> D-II</option>
            <option value="D-II" <?php if($dataPegawai->msPegawaiPendidikan == 'D-II'){echo "selected='selected'";} ?>> D-II</option>
            <option value="D-IV" <?php if($dataPegawai->msPegawaiPendidikan == 'D-IV'){echo "selected='selected'";} ?>> D-IV</option>
            <option value="S-I" <?php if($dataPegawai->msPegawaiPendidikan == 'S-I'){echo "selected='selected'";} ?>> S-I</option>
            <option value="S-II" <?php if($dataPegawai->msPegawaiPendidikan == 'S-II'){echo "selected='selected'";} ?>> S-II</option>
           <option value="S-III" <?php if($dataPegawai->msPegawaiPendidikan == 'S-III'){echo "selected='selected'";} ?>> S-III</option>
            
      </select>
    </div>

    <div class="form-group">
      <label for="nomor_surat">Jurusan</label>
      <input class="form-control" id="jurusan" name="jurusan" value="<?php echo $dataPegawai->msPegawaiPendidikanJurusan; ?>" placeholder="" type="text">
    </div>

    <div class="form-group">
      <label for="nomor_surat">Agama</label>
      <!-- <input class="form-control" id="agama" name="agama" value="" placeholder="" type="text"> -->
      <select name="agama" id="agama" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%" onselect=""> 
            <option value=""> --</option>
            <option value="ISLAM" <?php if($dataPegawai->msPegawaiAgama == 'ISLAM'){echo "selected='selected'";} ?>> ISLAM</option>
            <option value="KRISTEN" <?php if($dataPegawai->msPegawaiAgama == 'KRISTEN'){echo "selected='selected'";} ?>> KRISTEN</option>
            <option value="KHATOLIK" <?php if($dataPegawai->msPegawaiAgama == 'KHATOLIK'){echo "selected='selected'";} ?>> KHATOLIK</option>
            <option value="HINDU" <?php if($dataPegawai->msPegawaiAgama == 'HINDU'){echo "selected='selected'";} ?>> HINDU</option>
            <option value="BUDHA" <?php if($dataPegawai->msPegawaiAgama == 'BUDHA'){echo "selected='selected'";} ?>> BUDHA</option>
            <option value="KONGHUCU" <?php if($dataPegawai->msPegawaiAgama == 'KONGHUCU'){echo "selected='selected'";} ?>> KONGHUCU</option>
            <option value="KEPERCAYAAN" <?php if($dataPegawai->msPegawaiAgama == 'KEPERCAYAAN'){echo "selected='selected'";} ?>> KEPERCAYAAN</option>
            
      </select>
    </div>
    </div>

    <div class="col-md-6">
    <div class="form-group">
      <label for="nomor_surat">Alamat</label>      
      <textarea class="form-control" rows="5" id="alamat" name="alamat" value="" placeholder=""><?php echo $dataPegawai->msPegawaiAlamat; ?></textarea>
    </div>

    <div class="form-group">
      <label for="nomor_surat">Nama Instansi</label>
      <select name="instansi" id="instansi" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value=""> --</option>
              <?php
              foreach ($dataUnitKerja as $data) {
                ?>
                <option value="<?php echo $data->msInstansiId; ?>" <?php if($data->msInstansiId == $dataPegawai->msPegawaiIdInstansi){echo "selected='selected'";} ?> >
                  <?php echo $data->msInstansiNama; ?>
                </option>
                <?php
              }
              ?>
          </select>
    </div> 

    <div class="form-group" id="form_unit_kerja" style="display: visible;">
      <label for="nomor_surat">Unit Kerja *jika pegawai DKK</label>
      <select name="unit_kerja" id="unit_kerja" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value=""> --</option>
            <option value="Dinas Kesehatan" <?php if($dataPegawai->msPegawaiUnitKerja == 'Dinas Kesehatan'){echo "selected='selected'";} ?>> Dinas Kesehatan</option>
            <option value="Bidang KESMAS" <?php if($dataPegawai->msPegawaiUnitKerja == 'Bidang KESMAS'){echo "selected='selected'";} ?>> Bidang KESMAS</option>
            <option value="Bidang Pelayanan dan SDK" <?php if($dataPegawai->msPegawaiUnitKerja == 'Bidang Pelayanan dan SDK'){echo "selected='selected'";} ?>> Bidang Pelayanan dan SDK</option>
            <option value="Bidang P2P" <?php if($dataPegawai->msPegawaiUnitKerja == 'Bidang P2P'){echo "selected='selected'";} ?>> Bidang P2P</option>
            <option value="Bidang Yan SDK" <?php if($dataPegawai->msPegawaiUnitKerja == 'Bidang Yan SDK'){echo "selected='selected'";} ?>> Bidang Yan SDK</option>
            <option value="UPT Labkes" <?php if($dataPegawai->msPegawaiUnitKerja == 'UPT Labkes'){echo "selected='selected'";} ?>> UPT Labkes</option>
            <option value="Sekretariat" <?php if($dataPegawai->msPegawaiUnitKerja == 'Sekretariat'){echo "selected='selected'";} ?>> Sekretariat</option>            
          </select>
    </div>

    <div class="form-group">
      <label for="nomor_surat">Jenis Tenaga</label>
      <select name="jenis_pegawai" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value=""> --</option>
              <?php
              foreach ($dataJenisPegawai as $data) {
                ?>
                <option value="<?php echo $data->msJenisPegawaiNama; ?>" <?php if($data->msJenisPegawaiNama == $dataPegawai->msPegawaiJabatan){echo "selected='selected'";} ?> >
                  <?php echo $data->msJenisPegawaiNama; ?>
                </option>
                <?php
              }
              ?>
          </select>
    </div>            

    <div class="form-group">
      <label for="nomor_surat">Status Pegawai</label>
      <select name="status_pegawai" id="status_pegawai" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%" onselect="myFunction()"> 
           <option value="" > --</option>
                <option value="PNS" <?php if($dataPegawai->msPegawaiStatus == 'PNS'){echo "selected='selected'";} ?>> PNS</option>
                <option value="BLUD" <?php if($dataPegawai->msPegawaiStatus == 'BLUD'){echo "selected='selected'";} ?>> BLUD</option>              
          </select>
    </div>     

    <div class="form-group" id="gol" style="display: visible;">
      <label for="nomor_surat">Golongan *jika PNS</label>
      <select name="golongan" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value=""> --</option>
            <option value="I/a" <?php if($dataPegawai->msPegawaiGol == 'I/a'){echo "selected='selected'";} ?>> I/a</option>
            <option value="I/b" <?php if($dataPegawai->msPegawaiGol == 'I/b'){echo "selected='selected'";} ?>> I/b</option>
            <option value="I/c" <?php if($dataPegawai->msPegawaiGol == 'I/c'){echo "selected='selected'";} ?>> I/c</option>
            <option value="I/d" <?php if($dataPegawai->msPegawaiGol == 'I/d'){echo "selected='selected'";} ?>> I/d</option>
            <option value="II/a" <?php if($dataPegawai->msPegawaiGol == 'II/a'){echo "selected='selected'";} ?>> I/a</option>
            <option value="II/b" <?php if($dataPegawai->msPegawaiGol == 'II/b'){echo "selected='selected'";} ?>> I/b</option>
            <option value="II/c" <?php if($dataPegawai->msPegawaiGol == 'II/c'){echo "selected='selected'";} ?>> I/c</option>
            <option value="II/d" <?php if($dataPegawai->msPegawaiGol == 'II/d'){echo "selected='selected'";} ?>> I/d</option>
            <option value="III/a" <?php if($dataPegawai->msPegawaiGol == 'III/a'){echo "selected='selected'";} ?>> I/a</option>
            <option value="III/b" <?php if($dataPegawai->msPegawaiGol == 'III/b'){echo "selected='selected'";} ?>> I/b</option>
            <option value="III/c" <?php if($dataPegawai->msPegawaiGol == 'III/c'){echo "selected='selected'";} ?>> I/c</option>
            <option value="III/d" <?php if($dataPegawai->msPegawaiGol == 'III/d'){echo "selected='selected'";} ?>> I/d</option>
            <option value="IV/a" <?php if($dataPegawai->msPegawaiGol == 'IV/a'){echo "selected='selected'";} ?>> I/a</option>
            <option value="IV/b" <?php if($dataPegawai->msPegawaiGol == 'IV/b'){echo "selected='selected'";} ?>> I/b</option>
            <option value="IV/c" <?php if($dataPegawai->msPegawaiGol == 'IV/c'){echo "selected='selected'";} ?>> I/c</option>
            <option value="IV/d" <?php if($dataPegawai->msPegawaiGol == 'IV/d'){echo "selected='selected'";} ?>> I/d</option>
          </select>
    </div>  
    </div>      
        


        <div class="form-group">
          <div class="col-md-12">
              <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i> Update Data</button>
          </div>
        </div>
      </form>
</div>

<script type="text/javascript">
$(function () {
    $(".select2").select2({dropdownCssClass : 'bigdrop'});

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
});
</script>

<script type="text/javascript">     
  $(document).ready(function () {
    $('.tgl_lahir').datepicker({
        format: "yyyy-mm-dd",
        autoclose:true,
    });
});
</script>

<script>
    $('#status_pegawai').change(function() {
        var s = document.getElementById('status_pegawai');
        var item1 = s.options[s.selectedIndex].value;

        if(item1 == 'PNS') {
            // $('#pngkt').show();
            $('#gol').show();
        }else {
            // $('#pngkt').hide();
            $('#gol').hide();
        }
    });
</script>

<script>
    $('#instansi').change(function() {
        var s = document.getElementById('instansi');
        var item1 = s.options[s.selectedIndex].value;

        if(item1 == '298') {
            $('#form_unit_kerja').show();
            // $('#gol').show();
        }else {
            $('#form_unit_kerja').hide();
            // $('#gol').hide();
        }

        // alert("hahah");
    });
</script>