<div class="col-md-12 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Tambah Data Pegawai</h3>

  <form id="form-tambah-pegawai" method="POST">
    <div class="col-md-6">
    <div class="form-group">
      <label for="nomor_surat">NIP / NI BULD</label>
      <input class="form-control" id="nip_pegawai" name="nip_pegawai" value="" placeholder="" type="text">
    </div>

    <div class="form-group">
      <label for="nomor_surat">Nama Pegawai</label>
      <input class="form-control" id="nama_pegawai" name="nama_pegawai" value="" placeholder="" type="text">
    </div>

    <div class="form-group">
      <label for="nomor_surat">Tempat Lahir</label>
      <input class="form-control" id="tempat_lahir" name="tempat_lahir" value="" placeholder="" type="text">
    </div>

    <div class="form-group">
      <label for="nomor_surat">Tanggal Lahir</label>
      <!-- <input class="form-control" id="tgl_lahir" name="tgl_lahir" value="" placeholder="" type="text"> -->
      <input type="text" class="form-control tgl_lahir" name="tgl_lahir" id="tgl_lahir" placeholder="" name="nama_menu" aria-describedby="sizing-addon2" value="<?php echo date("Y-m-d"); ?>">
    </div>

    <div class="form-group">
      <label for="nomor_surat">Pendidikan</label>
      <!-- <input class="form-control" id="pendidikan" name="pendidikan" value="" placeholder="" type="text"> 
      -->
      <select name="pendidikan" id="pendidikan" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%" onselect=""> 
            <option value=""> --</option>
            <option value="SD"> SD</option>
            <option value="SMP"> SMP</option>
            <option value="SMA"> SMA</option>
            <option value="D-I"> D-I</option>
            <option value="D-II"> D-II</option>
            <option value="D-III"> D-III</option>
            <option value="D-IV"> D-IV</option>
            <option value="S-I"> S-I</option>
            <option value="S-II"> S-II</option>
            <option value="S-III"> S-III</option>
      </select>
    </div>

    <div class="form-group">
      <label for="nomor_surat">Jurusan</label>
      <input class="form-control" id="jurusan" name="jurusan" value="" placeholder="" type="text">
    </div>

    <div class="form-group">
      <label for="nomor_surat">Agama</label>
      <!-- <input class="form-control" id="agama" name="agama" value="" placeholder="" type="text"> -->
      <select name="agama" id="agama" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%" onselect=""> 
            <option value=""> --</option>
            <option value="ISLAM"> ISLAM</option>
            <option value="KRISTEN"> KRISTEN</option>
            <option value="KHATOLIK"> KHATOLIK</option>
            <option value="HINDU"> HINDU</option>
            <option value="BUDHA"> BUDHA</option>
            <option value="KONGHUCU"> KONGHUCU</option>
            <option value="KEPERCAYAAN"> KEPERCAYAAN</option>
      </select>
    </div>
    </div>

    <div class="col-md-6">
    <div class="form-group">
      <label for="nomor_surat">Alamat</label>
      <!-- <input class="form-control" id="alamat" name="alamat" value="" placeholder="alamat" type="text"> -->
      <textarea class="form-control" rows="5" id="alamat" name="alamat" value="" placeholder=""></textarea>
    </div>

    <div class="form-group">
      <label for="nomor_surat">Nama Instansi</label>
      <select name="instansi" id="instansi" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value=""> --</option>
              <?php
              foreach ($dataUnitKerja as $data) {
                ?>
                <option value="<?php echo $data->msInstansiId; ?>">
                  <?php echo $data->msInstansiNama; ?>
                </option>
                <?php
              }
              ?>
          </select>
    </div> 

    <div class="form-group" id="form_unit_kerja" style="display: none;">
      <label for="nomor_surat">Unit Kerja</label>
      <select name="unit_kerja" id="unit_kerja" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value=""> --</option>
            <option value="Dinas Kesehatan"> Dinas Kesehatan</option>
            <option value="Bidang KESMAS"> Bidang KESMAS</option>
            <option value="Bidang Pelayanan dan SDK"> Bidang Pelayanan dan SDK</option>
            <option value="Bidang P2P"> Bidang P2P</option>
            <option value="Bidang Yan SDK"> Bidang Yan SDK</option>
            <option value="UPT Labkes"> UPT Labkes</option>
            <option value="Sekretariat"> Sekretariat</option>
          </select>
    </div>

    <div class="form-group">
      <label for="nomor_surat">Jenis Tenaga</label>
      <select name="jenis_pegawai" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value=""> --</option>
              <?php
              foreach ($dataJenisPegawai as $data) {
                ?>
                <option value="<?php echo $data->msJenisPegawaiNama; ?>">
                  <?php echo $data->msJenisPegawaiNama; ?>
                </option>
                <?php
              }
              ?>
          </select>
    </div>            

    <div class="form-group">
      <label for="nomor_surat">Status Pegawai</label>
      <select name="status_pegawai" id="status_pegawai" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%" onselect="myFunction()"> 
            <option value=""> --</option>
            <option value="PNS"> PNS</option>
            <option value="BLUD"> BLUD</option>
              
          </select>
    </div>     

    <div class="form-group" id="gol" style="display: none;">
      <label for="nomor_surat">Golongan</label>
      <select name="golongan" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value=""> --</option>
            <option value="I/a"> I/a</option>
            <option value="I/b"> I/b</option>
            <option value="I/c"> I/c</option>
            <option value="I/d"> I/d</option>
            <option value="II/a"> I/a</option>
            <option value="II/b"> I/b</option>
            <option value="II/c"> I/c</option>
            <option value="II/d"> I/d</option>
            <option value="III/a"> I/a</option>
            <option value="III/b"> I/b</option>
            <option value="III/c"> I/c</option>
            <option value="III/d"> I/d</option>
            <option value="IV/a"> I/a</option>
            <option value="IV/b"> I/b</option>
            <option value="IV/c"> I/c</option>
            <option value="IV/d"> I/d</option>
          </select>
    </div>  
  </div>

    <div class="form-group">
      <div class="col-md-12">
          <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i> Tambah Data</button>
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
$(function () {
    $(".select2").select2({dropdownCssClass : 'bigdrop'});

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
});
</script>

<script type="text/javascript">     
  $(document).ready(function () {
    $('.tgl_lahir').datepicker({
        format: "yyyy-mm-dd",
        autoclose:true,
    });
});
</script>

<script>
    $('#status_pegawai').change(function() {
        var s = document.getElementById('status_pegawai');
        var item1 = s.options[s.selectedIndex].value;

        if(item1 == 'PNS') {
            // $('#pngkt').show();
            $('#gol').show();
        }else {
            // $('#pngkt').hide();
            $('#gol').hide();
        }
    });
</script>

<script>
    $('#instansi').change(function() {
        var s = document.getElementById('instansi');
        var item1 = s.options[s.selectedIndex].value;

        if(item1 == '298') {
            $('#form_unit_kerja').show();
            // $('#gol').show();
        }else {
            $('#form_unit_kerja').hide();
            // $('#gol').hide();
        }

        // alert("hahah");
    });
</script>