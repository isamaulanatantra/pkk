<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Tambah Data Pegawai</h3>

  <form id="form-tambah-pegawai" method="POST">
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">Nama Pegawai</span>
      <input type="text" class="form-control" placeholder="" name="nama_pegawai" aria-describedby="sizing-addon2">
    </div>

    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">NIP / NI BULD</span>
      <input type="text" class="form-control" placeholder="" name="nip_pegawai" aria-describedby="sizing-addon2">
    </div>

    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">Jenis Tenaga</span>
      <select name="jenis_pegawai" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value=""> --</option>
              <?php
              foreach ($dataJenisPegawai as $data) {
                ?>
                <option value="<?php echo $data->msJenisPegawaiNama; ?>">
                  <?php echo $data->msJenisPegawaiNama; ?>
                </option>
                <?php
              }
              ?>
          </select>
    </div>
    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">Unit Kerja</span>
      <select name="unit_kerja" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value=""> --</option>
              <?php
              foreach ($dataUnitKerja as $data) {
                ?>
                <option value="<?php echo $data->msUnitKerjaNama; ?>">
                  <?php echo $data->msUnitKerjaNama; ?>
                </option>
                <?php
              }
              ?>
          </select>
    </div>

    <div class="input-group form-group">
      <span class="input-group-addon" id="sizing-addon2">Status Pegawai</span>
      <select name="status_pegawai" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%"> 
            <option value=""> --</option>
            <option value="PNS"> PNS</option>
            <option value="PNS"> BLUD</option>
              
          </select>
    </div>

    <div class="form-group">
      <div class="col-md-12">
          <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i> Tambah Data</button>
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
$(function () {
    $(".select2").select2({dropdownCssClass : 'bigdrop'});

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
});
</script>
