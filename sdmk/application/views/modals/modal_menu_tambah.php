<div class="col-md-offset-1 col-md-10 col-md-offset-1 well">
  <div class="form-msg"></div>
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h3 style="display:block; text-align:center;">Tambah Data</h3>

  <form id="form-tambah-menu" method="POST">

    <div class="input-group form-group" style="display: none;">
      <span class="input-group-addon " id="sizing-addon2">User Id </span>
      <input type="text" class="form-control" placeholder="" name="id_user" value="<?php echo $userdata->id; ?>" aria-describedby="sizing-addon2">
    </div>

    <div class="input-group form-group">
      <span class="input-group-addon " id="sizing-addon2">Nama Menu </span>
      <input type="text" class="form-control" placeholder="" name="nama_menu" aria-describedby="sizing-addon2">
    </div>

    <div class="input-group form-group">
      <span class="input-group-addon " id="sizing-addon2">Order Menu</span>
      <input type="number" class="form-control" placeholder="" name="order_menu" aria-describedby="sizing-addon2">
    </div>

    <div class="input-group form-group">
      <span class="input-group-addon " id="sizing-addon2">Parent Menu</span>
      <select name="parent_menu" class="form-control  select2" aria-describedby="sizing-addon2">
        <option value="0"> --</option>
        <?php
        foreach ($dataParent as $data) {
          ?>
          <option value="<?php echo $data->msMenuId; ?>">
            <?php echo $data->msMenuNama; ?>
          </option>
          <?php
        }
        ?>
      </select>
    </div>

    <div class="input-group form-group">
      <span class="input-group-addon " id="sizing-addon2">Link Menu </span>
      <input type="text" class="form-control" placeholder="" name="link_menu" aria-describedby="sizing-addon2">
    </div>

    <div class="input-group form-group">
      <span class="input-group-addon " id="sizing-addon2">Icon Menu </span>
      <input type="text" class="form-control" placeholder="" name="icon_menu" aria-describedby="sizing-addon2">
    </div>

    <div class="form-group">
      <div class="col-md-12">
          <button type="submit" class="form-control btn btn-primary"> <i class="glyphicon glyphicon-ok"></i> Tambah Data</button>
      </div>
    </div>

  </form>
</div>

<script type="text/javascript">
$(function () {
    $(".select2").select2({dropdownCssClass : 'bigdrop'});

    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
});
</script>

<style>
  .bigdrop {
    width: 300px !important;
}
</style>