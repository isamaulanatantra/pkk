<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
  <div class="box-header">
    <div class="col-md-6" style="padding: 0;">
        <button class="form-control btn btn-primary" data-toggle="modal" data-target="#tambah-menu"><i class="glyphicon glyphicon-plus-sign"></i> Tambah Data</button>
    </div>    
  </div>
  <!-- /.box-header -->
  <div class="box-body">
    <table id="list-data" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>NO</th>
          <th>Nama Menu</th>          
          <th>Parent</th>          
          <th style="text-align: center;">Aksi</th>
        </tr>
      </thead>
      <tbody id="tblUtamaMenu">
        
      </tbody>
    </table>
  </div>
</div>

<?php echo $modal_menu_tambah; ?>

<div id="tempat-modal"></div>

<?php show_my_confirm('konfirmasiHapus', 'hapus-dataMenu', 'Hapus Data Ini?', 'Ya, Hapus Data Ini'); ?>
<?php
  $data['judul'] = 'Grup';
  $data['url'] = 'Grup/import';
  echo show_my_modal('modals/modal_import', 'import-pegawai', $data);
?>
