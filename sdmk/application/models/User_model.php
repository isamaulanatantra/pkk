<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	public function select_all($where_user) {
		$this->db->select('*');
		$this->db->from('table_dinkes_admin');
		$this->db->join('table_dinkes_msgrupmenu','msGrupMenuId = grupmenu');
		$this->db->where($where_user);
		// $this->db->where('id ','!=',1);
		//->where('tbl_somthing.Something != ',0,FALSE);

		$data = $this->db->get();
		return $data->result();
	}
		

	public function insert($data_input, $table_name) {
		$this->db->insert( $table_name, $data_input );	
		return $this->db->affected_rows();
	}

	public function select_by_id($id) {
		$sql = "SELECT * FROM table_dinkes_admin WHERE id = '{$id}'";

		$data = $this->db->query($sql);

		return $data->row();
	}

	function update($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}

	public function delete($id) {
		$sql = "DELETE FROM table_dinkes_admin WHERE id='" .$id ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}
	
}
