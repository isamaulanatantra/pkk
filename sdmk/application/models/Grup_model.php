<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grup_model extends CI_Model {
	public function select_all($where_grup) {
		$this->db->select('*');
		$this->db->from('table_dinkes_msgrupmenu');
		$this->db->where($where_grup);

		$data = $this->db->get();

		return $data->result();
	}	

	public function insert($data) {
		$sql = "INSERT INTO table_dinkes_msgrupmenu VALUES('','" .$data['grup'] ."')";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function select_by_id($id) {
		$sql = "SELECT * FROM table_dinkes_msgrupmenu WHERE msGrupMenuId = '{$id}'";

		$data = $this->db->query($sql);

		return $data->row();
	}

	public function update($data) {
		$sql = "UPDATE table_dinkes_msgrupmenu SET msGrupMenuNama='" .$data['grup'] ."' WHERE msGrupMenuId='" .$data['id'] ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function delete($id) {
		$sql = "DELETE FROM table_dinkes_msgrupmenu WHERE msGrupMenuId='" .$id ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}

	public function insert_batch($data) {
		$this->db->insert_batch('table_dinkes_msgrupmenu', $data);
		
		return $this->db->affected_rows();
	}

	public function check_nama($nama) {
		$this->db->where('msGrupMenuNama', $nama);
		$data = $this->db->get('table_dinkes_msgrupmenu');

		return $data->num_rows();
	}
}
