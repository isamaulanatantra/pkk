<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {
	public function total_pegawai($where) {
		$this->db->select('*');
		$this->db->from('tabel_dinkes_pegawai_instansi');
		$this->db->where($where);

		$data = $this->db->get();
		return $data->num_rows();
	}

	public function select_instansi() {
		$sql = "SELECT * FROM tabel_dinkes_instansi where status !=99";

		$data = $this->db->query($sql);

		return $data->result();
	}

	public function select_by_instansi($id) {
		$sql = "SELECT COUNT(*) AS jml FROM tabel_dinkes_pegawai_instansi WHERE msPegawaiIdInstansi = {$id}";

		$data = $this->db->query($sql);

		return $data->row();
	}

	function get_data_pegawai_puskesmas($where){
      $this->db->select('msPegawaiJabatan,COUNT(msPegawaiJabatan) as total');
      $this->db->where($where);
      $this->db->group_by('msPegawaiJabatan'); 
      $this->db->order_by('total DESC');
      $this->db->limit(10); 
      $result = $this->db->get('tabel_dinkes_pegawai_instansi');
      return $result;
	}

	
}
