<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_model extends CI_Model {
	public function select_all() {
		$this->db->select('*');
		$this->db->from('table_dinkes_msmenu');
		$this->db->where('status','!=','99');

		$data = $this->db->get();
		return $data->result();
	}

	public function select_parent() {
		$this->db->select('*');
		$this->db->from('table_dinkes_msmenu');
		$this->db->where('msMenuParent','=',0);
		$this->db->where('status','!=',99);

		$data = $this->db->get();
		return $data->result();
	}

	function getParentById($id){
		$where = array(
		      'msMenuId' => $id
		      );
	 	$this->db->select('msMenuNama');
	    $this->db->where($where);		    
	    $query = $this->db->get('table_dinkes_msmenu');

	    foreach ($query->result() as $hsl) {
	    	$value = $hsl->msMenuNama;
	    }

	    if ($query->num_rows() > 0) {
	    	$cetak = $value;
	    }else{
	    	$cetak = '';
	    }

		return $cetak;
	}
		

	public function insert($data_input, $table_name) {
		$this->db->insert( $table_name, $data_input );	
		return $this->db->affected_rows();
	}

	public function select_by_id($id) {
		$sql = "SELECT * FROM table_dinkes_msmenu WHERE msMenuId = '{$id}'";

		$data = $this->db->query($sql);

		return $data->row();
	}

	function update($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}

	public function delete($id) {
		$sql = "DELETE FROM table_dinkes_msmenu WHERE msMenuId='" .$id ."'";

		$this->db->query($sql);

		return $this->db->affected_rows();
	}
	
}
