<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_pegawai_model extends CI_Model {
	public function select_all() {
		$this->db->select('*');
		$this->db->from('tabel_dinkes_pegawai_instansi');
		$this->db->where('status !=',99);

		$data = $this->db->get();

		return $data->result();
	}

	public function select_data_pegawai($where_data,$order_by) {
		$this->db->select('*');
		$this->db->from('tabel_dinkes_pegawai_instansi');
		$this->db->where($where_data);
		$this->db->order_by('msPegawaiId DESC');

		$data = $this->db->get();

		return $data->result();
	}	

	public function select_all_pegawai() {
		$sql = "SELECT * FROM tabel_dinkes_pegawai_instansi where status!=99";

		$data = $this->db->query($sql);

		return $data->result();
	}

	public function select_jenis_pegawai() {
		$this->db->select('*');
		$this->db->from('tabel_dinkes_jenis_pegawai');
		$this->db->where('status !=',99);

		$data = $this->db->get();
		return $data->result();
	}

	function getJenisPegawaiById($id){
		$where = array(
		      'msJenisPegawaiId' => $id
		      );
	 	$this->db->select('*');
	    $this->db->where($where);		    
	    $query = $this->db->get('tabel_dinkes_jenis_pegawai');

	    foreach ($query->result() as $hsl) {
	    	$value = $hsl->msJenisPegawaiNama;
	    }

	    if ($query->num_rows() > 0) {
	    	$cetak = $value;
	    }else{
	    	$cetak = '';
	    }

		return $cetak;
	}	

	public function select_unit_kerja() {
		$this->db->select('*');
		$this->db->from('tabel_dinkes_instansi');
		$this->db->where('status !=',99);

		$data = $this->db->get();
		return $data->result();
	}	

	function getUnitKerjaById($id){
		$where = array(
		      'msUnitKerjaId' => $id
		      );
	 	$this->db->select('*');
	    $this->db->where($where);		    
	    $query = $this->db->get('tabel_dinkes_instansi');

	    foreach ($query->result() as $hsl) {
	    	$value = $hsl->msUnitKerjaNama;
	    }

	    if ($query->num_rows() > 0) {
	    	$cetak = $value;
	    }else{
	    	$cetak = '';
	    }

		return $cetak;
	}	

	function insert($data_input, $table_name)
	{
		$this->db->insert( $table_name, $data_input );
		return $this->db->insert_id();
	}

	public function select_by_id($id) {
		$sql = "SELECT * FROM tabel_dinkes_pegawai_instansi WHERE msPegawaiId = '{$id}'";

		$data = $this->db->query($sql);

		return $data->row();
	}

	function update($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}

	// public function delete($id) {
	// 	$sql = "DELETE FROM msgrupmenu WHERE msGrupMenuId='" .$id ."'";

	// 	$this->db->query($sql);

	// 	return $this->db->affected_rows();
	// }

	public function insert_batch($data) {
		$this->db->insert_batch('tabel_dinkes_pegawai_instansi', $data);
		
		return $this->db->affected_rows();
	}

	public function check_nip($nip) {
		$where       = array(
        	'msPegawaiNip' => $nip,
        	'status !='=> 99
		);	

		$this->db->where($where);
		$data = $this->db->get('tabel_dinkes_pegawai_instansi');

		return $data->num_rows();
	}
}
