<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rolemenu_model extends CI_Model {
	public function select_role() {
		$this->db->select('*');
		$this->db->from('table_dinkes_msmenu');		

		$data = $this->db->get();
		return $data->result();
	}

	function getParentById($id){
		$where = array(
		      'msMenuId' => $id
		      );
	 	$this->db->select('msMenuNama');
	    $this->db->where($where);		    
	    $query = $this->db->get('table_dinkes_msmenu');

	    foreach ($query->result() as $hsl) {
	    	$value = $hsl->msMenuNama;
	    }

	    if ($query->num_rows() > 0) {
	    	$cetak = $value;
	    }else{
	    	$cetak = '';
	    }

		return $cetak;
	}

	function getAktif($id, $grup){
		$where = array(
		      'rolMenuMsMenu' => $id,
		      'rolMenuMsGrupMenu' => $grup
		      );
	 	$this->db->select('rolMenuStatus');
	    $this->db->where($where);		    
	    $query = $this->db->get('table_dinkes_rolmenu');

	    if ($query->num_rows() > 0) {
	    	foreach($query->result() as $data){
		    	$hasil = $data->rolMenuStatus;
		    	if ($hasil == 1) {
		    		$result = 1;
		    	}else if($hasil == 2){
		    		$result = 2;
		    	}else{
		    		$result = 0;
		    	}
	    	}	
	    }else{
	    	$result = 0;
	    }
	    
	    

		return $result;
	}

	// function getAktif($id, $grup){
	// 	$where = array(
	// 	      'rolMenuMsMenu' => $id,
	// 	      'rolMenuMsGrupMenu' => $grup
	// 	      );
	//  	$this->db->select('rolMenuMsMenu');
	//     $this->db->where($where);		    
	//     $query = $this->db->get('rolmenu');
	    

	//     if ($query->num_rows() > 0) {
	//     	$result = 1;
	//     }else{
	//     	$result = 0;
	//     }

	// 	return $result;
	// }

	public function save_data($data){
	    return $this->db->insert_batch('table_dinkes_rolmenu', $data);
	}


}
?>