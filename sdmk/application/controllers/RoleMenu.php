<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RoleMenu extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Grup_model');		
		$this->load->model('Rolemenu_model');
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$where_grup = 'msGrupMenuId != 1';	
		$data['dataGrup'] 	= $this->Grup_model->select_all($where_grup);
		$data['dataMenu'] 	= $this->Rolemenu_model->select_role();

		$data['page'] 		= "role menu";
		$data['judul'] 		= "Data Role Menu";
		$data['deskripsi'] 	= "Manage Data Role Menu";		

		$this->template->views('rolemenu/home', $data);
	}

	public function tampil($id) {
		$where_grup = 'msGrupMenuId != 1';	
		$data['dataGrup'] = $this->Grup_model->select_all($where_grup);
		$this->load->view('rolemenu/list_data', $data);
	}

	public function tampilaktif() {
		$data['dataMenu'] = $this->Rolemenu_model->select_role();
		$this->load->view('rolemenu/list_data_aktif', $data);
	}	

	function load_form_role(){    
    $grup = $this->input->post('id');

	$w = $this->db->query("SELECT * from table_dinkes_msmenu WHERE status !=99
									
						");
	if(($w->num_rows())>0){
		}

		$no_urut = 0;
		foreach($w->result() as $data)
		{
			$no_urut++;

			$aktif = $this->Rolemenu_model->getAktif($data->msMenuId, $grup);

            if ($aktif == 1) {
               $cetak =   '                	
					<input type="checkbox" name="aktif[]" id="aktif[]" value="1"  checked >
						&nbsp;<span class="label label-primary">Yes</span>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="aktif[]" id="aktif[]" value="2" >
						&nbsp;<span class="label label-danger">No</span>
                ';
            }elseif ($aktif == 2) {
             $cetak =   '
                	<input type="checkbox" name="aktif[]" id="aktif[]" value="1"  >
                	&nbsp;<span class="label label-primary">Yes</span>
                	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="aktif[]" id="aktif[]" value="2" checked>
						&nbsp;<span class="label label-danger">No</span>
                ';
            }else{
            	 $cetak =   '
                	<input type="checkbox" name="aktif[]" id="aktif[]" value="1"  >
                		&nbsp;<span class="label label-primary">Yes</span>
                		&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="aktif[]" id="aktif[]" value="2" checked>
						&nbsp;<span class="label label-danger">No</span>
                ';
            }

			echo '				
				<tr id_komdat="'.$data->msMenuId.'" id="'.$data->msMenuId.'">					
					
					<td style="padding: 2px;">
						<p>'.$no_urut.'</p>
					</td>

					<td align=center style="padding: 2px; display: none;">
						<input class="form-control" id="grup_menu[]" name="grup_menu[]" value="'.$grup.'" placeholder="0" type="text">
					</td>

					<td align=center style="padding: 2px; display: none;">
						<input class="form-control" id="id_menu[]" name="id_menu[]" value="'.$data->msMenuId.'" placeholder="0" type="text">
					</td>

					<td style="padding: 2px;">
						<p>'.$data->msMenuNama.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$this->Rolemenu_model->getParentById($data->msMenuParent).'</p>
					</td>
					<td>
						<p>'.$cetak.'</p>
					</td>					
				</tr>
			';
		}
	} 

	public function save(){
		
		
		$id_menu = $_POST['id_menu']; 
		$grup = $_POST['grup_menu']; 
		$aktif = $_POST['aktif'];		

		$data = array();

		$index = 0; 	

		foreach($grup as $grup){ 

			array_push($data, array(
			'rolMenuMsGrupMenu'=>$grup,
			'rolMenuMsMenu'=>$id_menu[$index],
			'rolMenuStatus'=>$aktif[$index],
			));
		$index++;
		}

		$this->db->where('rolMenuMsGrupMenu',$grup);
    	$this->db->delete('table_dinkes_rolmenu');

		$sql = $this->Rolemenu_model->save_data($data);

		if($sql){ // Jika sukses
	      	$data['hasil'] = 'sukses';
	 		echo json_encode($data);		 		
	    }else{ 
	    	$data['hasil'] = 'gagal';
	 		echo json_encode($data);
		}
				
	}
}
?>