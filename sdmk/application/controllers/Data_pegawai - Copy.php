<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_pegawai extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Data_pegawai_model');
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$data['dataPegawai'] = $this->Data_pegawai_model->select_all();
		$data['dataJenisPegawai'] 	= $this->Data_pegawai_model->select_jenis_pegawai();
		$data['dataUnitKerja'] 	= $this->Data_pegawai_model->select_unit_kerja();		

		$data['page'] 		= "pegawai";
		$data['judul'] 		= "Data Pegawai";
		$data['deskripsi'] 	= "Manage Data Pegawai";

		$data['modal_pegawai_tambah'] = show_my_modal('modals/modal_pegawai_tambah', 'tambah-pegawai', $data);

		$this->template->views('pegawai/home', $data);
	}

	public function tampil() {
		$data['dataPegawai'] = $this->Data_pegawai_model->select_all();
		$this->load->view('pegawai/list_data', $data);
	}	

	public function prosesTambah() {
		$this->form_validation->set_rules('nama_pegawai', 'Nama pegawai', 'trim|required');
		$this->form_validation->set_rules('nip_pegawai', 'Nip pegawai', 'trim|required');
		$this->form_validation->set_rules('jenis_pegawai', 'Jenis pegawai', 'trim|required');
		$this->form_validation->set_rules('unit_kerja', 'Unit kerja', 'trim|required');
		$this->form_validation->set_rules('status_pegawai', 'Status pegawai', 'trim|required');


		$nama_pegawai 	= $this->input->post('nama_pegawai');
		$nip_pegawai 	= $this->input->post('nip_pegawai');
		$jenis_pegawai 	= $this->input->post('jenis_pegawai');
		$unit_kerja 	= $this->input->post('unit_kerja');
		$status_pegawai 	= $this->input->post('status_pegawai');

		$user = $this->userdata->id;

		if ($this->form_validation->run() == TRUE) {
			$data_input = array(
        		
				'msPegawaiNama' => $nama_pegawai,
				'msPegawaiNip' => $nip_pegawai,
				'msPegawaiJenisTenaga' => $jenis_pegawai,
				'msPegawaiUnitKerja' => $unit_kerja,
				'msPegawaiStatus' => $status_pegawai,				
				'created_by' => $user,
				'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1
				);
     		$table_name = 'tabel_dinkes_pegawai';
      		// $id = $this->Perijinan_penelitian_model->simpan_perijinan_penelitian($data_input, $table_name);

			$result = $this->Data_pegawai_model->insert($data_input, $table_name);

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Berhasil ditambahkan', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Gagal ditambahkan', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function update() {
		$data['userdata'] 	= $this->userdata;

		$id 				= trim($_POST['id']);
		//$id = 1;
		$data['dataPegawai'] 	= $this->Data_pegawai_model->select_by_id($id);
		$data['dataJenisPegawai'] 	= $this->Data_pegawai_model->select_jenis_pegawai();
		$data['dataUnitKerja'] 	= $this->Data_pegawai_model->select_unit_kerja();

		echo show_my_modal('modals/modal_pegawai_update', 'update-pegawai', $data);
	}

	public function prosesUpdate() {
		$this->form_validation->set_rules('nama_pegawai', 'Nama pegawai', 'trim|required');
		$this->form_validation->set_rules('nip_pegawai', 'Nip pegawai', 'trim|required');
		$this->form_validation->set_rules('jenis_pegawai', 'Jenis pegawai', 'trim|required');
		$this->form_validation->set_rules('unit_kerja', 'Unit kerja', 'trim|required');
		$this->form_validation->set_rules('status_pegawai', 'Status pegawai', 'trim|required');


		$nama_pegawai 	= $this->input->post('nama_pegawai');
		$nip_pegawai 	= $this->input->post('nip_pegawai');
		$jenis_pegawai 	= $this->input->post('jenis_pegawai');
		$unit_kerja 	= $this->input->post('unit_kerja');
		$status_pegawai 	= $this->input->post('status_pegawai');
		$user = $this->userdata->id;

		if ($this->form_validation->run() == TRUE) {
			$data_update = array(
        		
				'msPegawaiNama' => $nama_pegawai,
				'msPegawaiNip' => $nip_pegawai,
				'msPegawaiJenisTenaga' => $jenis_pegawai,
				'msPegawaiUnitKerja' => $unit_kerja,
				'msPegawaiStatus' => $status_pegawai,				
				// 'created_by' => 0,
				// 'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				// 'deleted_by' => 0,
				// 'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1
				);
		}
 		$table_name = 'tabel_dinkes_pegawai';
  		
  		$where       = array(
        	'msPegawaiId' => trim($this->input->post('id'))        
		);		

		// $this->Data_pegawai_model->update($data_update, $where, $table_name);

		
		if ($this->form_validation->run() == TRUE) {
			$result = $this->Data_pegawai_model->update($data_update, $where, $table_name);

			if ($result < 1) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data  Berhasil diupdate', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data  Gagal diupdate', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function delete() {

		$id = $_POST['id'];
		// $id=7;
		$user = $this->userdata->id;

		$data_update = array(
			'msPegawaiId' => $id,
			'status' => 99,
			'deleted_by' => $user,
			'deleted_time' => date('Y-m-d H:i:s'),

		);	

		$where       = array(
        	'msPegawaiId' => $id        
		);		


        $table_name  = 'tabel_dinkes_pegawai';        

		$id = $_POST['id'];
		// $result = $this->Data_pegawai_model->delete($id);
		$result = $this->Data_pegawai_model->update($data_update, $where, $table_name);
		
		if ($result < 1) {
			echo show_succ_msg('Data Berhasil dihapus', '20px');
		} else {
			echo show_err_msg('Data Gagal dihapus', '20px');
		}
	}

	public function export() {
		error_reporting(E_ALL);
    
		include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->Data_pegawai_model->select_all_pegawai();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->getStyle('A1:F1')->applyFromArray(array('fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'f4c82b',
					'setBorderStyle' => PHPExcel_Style_Border::BORDER_THICK
        ))));

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "ID");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "NIP/NI BLUD");
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "Nama Pegawai");
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "Jenis Tenaga");
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "Unit Kerja");
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "Status");
		$rowCount++;

		foreach($data as $value){

		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->msPegawaiId); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->msPegawaiNip, PHPExcel_Cell_DataType::TYPE_STRING); 
		    $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$rowCount, $value->msPegawaiNama);
		    $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $value->msPegawaiJenisTenaga); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $value->msPegawaiUnitKerja); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->msPegawaiStatus); 
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data_pegawai.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data_pegawai.xlsx', NULL);
	}

	public function import() {
		$this->form_validation->set_rules('excel', 'File', 'trim|required');

		if ($_FILES['excel']['name'] == '') {
			$this->session->set_flashdata('msg', 'File harus diisi');
		} else {
			$config['upload_path'] = './assets/excel/';
			$config['allowed_types'] = 'xls|xlsx';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('excel')){
				$error = array('error' => $this->upload->display_errors());
			}
			else{
				$data = $this->upload->data();
				
				error_reporting(E_ALL);
				date_default_timezone_set('Asia/Jakarta');

				include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

				$inputFileName = './assets/excel/' .$data['file_name'];
				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

				$index = 0;
				foreach ($sheetData as $key => $value) {
					if ($key != 1) {
						$id = md5(DATE('ymdhms').rand());
						$check = $this->Data_pegawai_model->check_nip($value['B']);
						$user = $this->userdata->id;

						if ($check != 1) {
							//$resultData[$index]['id'] = $id;

							$resultData[$index]['msPegawaiIdInstansi'] = ucwords($value['B']);
							$resultData[$index]['msPegawaiNamaInstansi'] = ucwords($value['C']);
							$resultData[$index]['msPegawaiNip'] = ucwords($value['D']);
							$resultData[$index]['msPegawaiNama'] = ucwords($value['E']);
							$resultData[$index]['msPegawaiPendidikan'] = ucwords($value['F']);
							$resultData[$index]['msPegawaiPangkat'] = ucwords($value['G']);
							$resultData[$index]['msPegawaiJabatan'] = ucwords($value['H']);
							$resultData[$index]['msPegawaiUnitKerja'] = ucwords($value['I']);
							$resultData[$index]['msPegawaiStatus'] = ucwords($value['J']);
							$resultData[$index]['created_by'] = $user;
							$resultData[$index]['created_time'] = date('Y-m-d H:i:s');
							$resultData[$index]['updated_by'] = 0;
							$resultData[$index]['updated_time'] = date('Y-m-d H:i:s');
							$resultData[$index]['deleted_by'] = 0;
							$resultData[$index]['deleted_time'] = date('Y-m-d H:i:s');
							$resultData[$index]['status'] = 1;

						}
					}
					$index++;
				}

				unlink('./assets/excel/' .$data['file_name']);

				if (count($resultData) != 0) {
					$result = $this->Data_pegawai_model->insert_batch($resultData);
					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Data Berhasil diimport ke database'));
						redirect('Data_pegawai');
					}
				} else {
					$this->session->set_flashdata('msg', show_msg('Data Gagal diimport ke database (Data Sudah ada)', 'warning', 'fa-warning'));
					redirect('Data_pegawai');
				}

			}
		}
	}

}
