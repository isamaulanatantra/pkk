<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Grup_model');
		$this->load->model('Data_pegawai_model');
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;

		$where_user = array(
			'status != ' => 99,
			'id != ' => 1
		);
		$data['dataUser'] 	= $this->User_model->select_all($where_user);

		$where_grup = 'msGrupMenuId != 1';	
		$data['dataGrup'] 	= $this->Grup_model->select_all($where_grup);	
		$data['dataUnitKerja'] 	= $this->Data_pegawai_model->select_unit_kerja();	

		$data['page'] 		= "user";
		$data['judul'] 		= "Data User";
		$data['deskripsi'] 	= "Manage Data User";

		$data['modal_user_tambah'] = show_my_modal('modals/modal_user_tambah', 'tambah-user', $data);

		$this->template->views('user/home', $data);
	}

	public function tampilUser() {
		$where_user = array(
			'status != ' => 99,
			'id != ' => 1
		);
		$data['dataUser'] = $this->User_model->select_all($where_user);
		$this->load->view('user/list_data', $data);
	}	

	public function prosesTambah() {
		$this->form_validation->set_rules('nama_user', 'Nama ', 'trim|required');
		$this->form_validation->set_rules('username_user', 'Username', 'trim|required');
		$this->form_validation->set_rules('password_user', 'Password', 'trim|required');
		$this->form_validation->set_rules('instansi', 'Instansi', 'trim|required');

		$user = $this->input->post('id_user');

		$nama = $this->input->post('nama_user');
		$grupmenu = $this->input->post('grup_user');
		$username = $this->input->post('username_user');
		$password = $this->input->post('password_user');
		$instansi = $this->input->post('instansi');
		

		$data_input = array(
    		'nama' => $nama,				
			'grupmenu' => $grupmenu,
			'username' => $username,
			'password' => MD5($password),
			'id_instansi' => $instansi,			
			'created_by' => $user,
			'created_time' => date('Y-m-d H:i:s'),
			// 'updated_by' => 0,
			// 'updated_time' => date('Y-m-d H:i:s'),
			// 'deleted_by' => 0,
			// 'deleted_time' => date('Y-m-d H:i:s'),
			'status' => 0
		);

		$table_name  = 'table_dinkes_admin';


		if ($this->form_validation->run() == TRUE) {
			$result = $this->User_model->insert($data_input, $table_name);			

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Berhasil ditambahkan', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Gagal ditambahkan', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function edit() {
		$data['userdata'] 	= $this->userdata;

		$id 				= trim($_POST['id']);
		// $id = 1;
		$data['dataUser'] 	= $this->User_model->select_by_id($id);
		$where_grup = 'msGrupMenuId != 1';
		$data['dataGrup'] 	= $this->Grup_model->select_all($where_grup);
		$data['dataUnitKerja'] 	= $this->Data_pegawai_model->select_unit_kerja();	

		echo show_my_modal('modals/modal_user_update', 'update-user', $data);
	}

	public function prosesUpdate() {
		$this->form_validation->set_rules('nama_user', 'Nama ', 'trim|required');
		$this->form_validation->set_rules('username_user', 'Username', 'trim|required');
		//$this->form_validation->set_rules('password_user', 'Password', 'trim|required');

		$user = $this->input->post('id_user');
		$id = $this->input->post('id');

		$nama = $this->input->post('nama_user');
		$grupmenu = $this->input->post('grup_user');
		$username = $this->input->post('username_user');
		$password = $this->input->post('password_user');

		if (empty($password)) {
			$data_update = array(
	    		'nama' => $nama,				
				'grupmenu' => $grupmenu,
				'username' => $username,
				
				// 'created_by' => $user,
				// 'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				// 'deleted_by' => 0,
				// 'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 0
			);
		}else{
			$data_update = array(
    		'nama' => $nama,				
			'grupmenu' => $grupmenu,
			'username' => $username,
			'password' =>  MD5($password),
			
			// 'created_by' => $user,
			// 'created_time' => date('Y-m-d H:i:s'),
			'updated_by' => $user,
			'updated_time' => date('Y-m-d H:i:s'),
			// 'deleted_by' => 0,
			// 'deleted_time' => date('Y-m-d H:i:s'),
			'status' => 0
		);

		}

		
		$table_name  = 'table_dinkes_admin';

		$where       = array(
        'id' => $id
			);

		if ($this->form_validation->run() == TRUE) {
			$result = $this->User_model->update($data_update, $where, $table_name);

			if ($result >= 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Berhasil diupdate', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Gagal diupdate', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function delete() {
		$id = $_POST['id'];
		$user = $this->input->post('id_user');

		$data_update = array(
    		
			'deleted_by' => $user,
			'deleted_time' => date('Y-m-d H:i:s'),			
			'status' => 99
		);

		$table_name  = 'table_dinkes_admin';

		$where       = array(
        'id' => $id
			);

		//$result = $this->Menu_model->delete($id);
		$result = $this->User_model->update($data_update, $where, $table_name);
		
		if ($result > 0) {
			echo show_succ_msg('Data Berhasil dihapus', '20px');
		} else {
			echo show_succ_msg('Data dihapus', '20px');
		}
	}
	
}
