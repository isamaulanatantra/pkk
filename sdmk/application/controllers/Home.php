<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Home_model');
		
	}

	public function index() {
		$id_instansi = $this->userdata->id_instansi;

		
		$data['userdata'] 		= $this->userdata;

		$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');

		
		if ($id_instansi==0) {
			$where_total = array(
				'status != ' => 99 
				// 'msPegawaiIdInstansi !=' => 298
			);

			$where_bar = array(
				'status != ' => 99 
				// 'msPegawaiIdInstansi !=' => 298
			);

			$order_bar = array(
				'msPegawaiIdInstansi ASC'
			);

			$y = $this->db->query("SELECT msPegawaiJabatan, COUNT(msPegawaiJabatan) as total 
									from tabel_dinkes_pegawai_instansi
									where status != 99
									-- and msPegawaiIdInstansi != 298
									GROUP BY msPegawaiJabatan
									order by msPegawaiJabatan ASC
	                          ");

			$sql_pegawai = $this->db->query("SELECT * 
									from tabel_dinkes_pegawai_instansi
									where status != 99
									-- and msPegawaiIdInstansi != 298								
									order by msPegawaiNama ASC
	                          ");

		}else{
			$where_total = array(
				'status != ' => 99 ,
				'msPegawaiIdInstansi =' => $id_instansi
				// 'msPegawaiIdInstansi !=' => 298
			);

			$where_bar = array(
				'status != ' => 99 ,
				'msPegawaiIdInstansi =' => $id_instansi
				// 'msPegawaiIdInstansi !=' => 298
			);

			$order_bar = array(
				'msPegawaiIdInstansi ASC'
			);

			$y = $this->db->query("SELECT msPegawaiJabatan, COUNT(msPegawaiJabatan) as total 
									from tabel_dinkes_pegawai_instansi
									where status != 99
									-- and msPegawaiIdInstansi != 298
									and msPegawaiIdInstansi = $id_instansi
									GROUP BY msPegawaiJabatan
									order by msPegawaiJabatan ASC
	                          ");

			$sql_pegawai = $this->db->query("SELECT * 
									from tabel_dinkes_pegawai_instansi
									where status != 99
									-- and msPegawaiIdInstansi != 298
									and msPegawaiIdInstansi = $id_instansi
									order by msPegawaiNama ASC
	                          ");

		}

		$tabel = '';
		$no = 1;		
		foreach($y->result() as $r2) 
		{
			$tabel .= '<tr>';
			$tabel .= '<td class="text-center">';
			$tabel .= ''.$no.'';
			$tabel .= '</td>';
			$tabel .= '<td>';
			$tabel .= ''.$r2->msPegawaiJabatan.'';
			$tabel .= '</td>';
			$tabel .= '<td class="text-center">';
			$tabel .= ''.$r2->total.'';
			$tabel .= '</td>';
			$tabel .= '</tr>';
			 $no++;
		}


		$data['tabel'] = $tabel;

		$data['jml_pegawai'] 	= $this->Home_model->total_pegawai($where_total);

		$data_bar = $this->Home_model->get_data_pegawai_puskesmas($where_bar)->result();
		$data['dataBar'] = json_encode($data_bar);



		$data['page'] 			= "home";
		$data['judul'] 			= "Beranda";
		$data['deskripsi'] 		= "Manage Data Pegawai";
		$this->template->views('home', $data);
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */