<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Menu_model');
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$data['dataMenu'] 	= $this->Menu_model->select_all();
		$data['dataParent'] 	= $this->Menu_model->select_parent();		

		$data['page'] 		= "menu";
		$data['judul'] 		= "Data Menu";
		$data['deskripsi'] 	= "Manage Data Menu";

		$data['modal_menu_tambah'] = show_my_modal('modals/modal_menu_tambah', 'tambah-menu', $data);

		$this->template->views('menu/home', $data);
	}

	public function tampil() {
		$data['dataMenu'] = $this->Menu_model->select_all();
		$this->load->view('menu/list_data', $data);
	}	

	public function prosesTambah() {
		$this->form_validation->set_rules('nama_menu', 'Nama menu', 'trim|required');
		$this->form_validation->set_rules('order_menu', 'Order', 'trim|required');
		// $this->form_validation->set_rules('link_menu', 'Link menu', 'trim|required');

		$msMenuOrder = $this->input->post('order_menu');
		$msMenuParent = $this->input->post('parent_menu');
		$msMenuNama = $this->input->post('nama_menu');
		$msMenuLink = $this->input->post('link_menu');
		$msMenuController = $this->input->post('link_menu');
		$msMenuIcon = $this->input->post('icon_menu');
		$user = $this->input->post('id_user');

		$data_input = array(
    		'msMenuOrder' => $msMenuOrder,				
			'msMenuParent' => $msMenuParent,
			'msMenuNama' => $msMenuNama,
			'msMenuLink' => $msMenuLink,
			'msMenuController' => $msMenuController,
			'msMenuIcon' => $msMenuIcon,
			'created_by' => $user,
			'created_time' => date('Y-m-d H:i:s'),
			// 'updated_by' => 0,
			// 'updated_time' => date('Y-m-d H:i:s'),
			// 'deleted_by' => 0,
			// 'deleted_time' => date('Y-m-d H:i:s'),
			'status' => 0
		);

		$table_name  = 'table_dinkes_msmenu';


		if ($this->form_validation->run() == TRUE) {
			$result = $this->Menu_model->insert($data_input, $table_name);			

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Berhasil ditambahkan', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Gagal ditambahkan', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function edit() {
		$data['userdata'] 	= $this->userdata;

		 $id 				= trim($_POST['id']);
		//$id = 1;
		$data['dataMenu'] 	= $this->Menu_model->select_by_id($id);
		$data['dataParent'] 	= $this->Menu_model->select_parent();	

		echo show_my_modal('modals/modal_menu_update', 'update-menu', $data);
	}

	public function prosesUpdate() {
		$this->form_validation->set_rules('nama_menu', 'Nama menu', 'trim|required');
		$this->form_validation->set_rules('order_menu', 'Order', 'trim|required');
		// $this->form_validation->set_rules('link_menu', 'Link menu', 'trim|required');

		$msMenuId = $this->input->post('id');
		$msMenuOrder = $this->input->post('order_menu');		
		$msMenuParent = $this->input->post('parent_menu');
		$msMenuNama = $this->input->post('nama_menu');
		$msMenuLink = $this->input->post('link_menu');
		$msMenuController = $this->input->post('link_menu');
		$msMenuIcon = $this->input->post('icon_menu');
		$user = $this->input->post('id_user');

		$data_update = array(
    		'msMenuOrder' => $msMenuOrder,				
			'msMenuParent' => $msMenuParent,
			'msMenuNama' => $msMenuNama,
			'msMenuLink' => $msMenuLink,
			'msMenuController' => $msMenuController,
			'msMenuIcon' => $msMenuIcon,
			// 'created_by' => $user,
			// 'created_time' => date('Y-m-d H:i:s'),
			'updated_by' => $user,
			'updated_time' => date('Y-m-d H:i:s'),
			// 'deleted_by' => 0,
			// 'deleted_time' => date('Y-m-d H:i:s'),
			'status' => 0
		);

		$table_name  = 'table_dinkes_msmenu';

		$where       = array(
        'msMenuId' => $msMenuId
			);

		if ($this->form_validation->run() == TRUE) {
			$result = $this->Menu_model->update($data_update, $where, $table_name);

			if ($result >= 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Berhasil diupdate', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Gagal diupdate', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function delete() {
		$msMenuId = $_POST['id'];
		$user = $this->input->post('id_user');

		$data_update = array(
    		
			'deleted_by' => $user,
			'deleted_time' => date('Y-m-d H:i:s'),			
			'status' => 99
		);

		$table_name  = 'table_dinkes_msmenu';

		$where       = array(
        'msMenuId' => $msMenuId
			);

		//$result = $this->Menu_model->delete($id);
		$result = $this->Menu_model->update($data_update, $where, $table_name);
		
		if ($result > 0) {
			echo show_succ_msg('Data Berhasil dihapus', '20px');
		} else {
			echo show_succ_msg('Data dihapus', '20px');
		}
	}
}
