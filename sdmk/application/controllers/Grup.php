<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grup extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Grup_model');
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$where_grup = 'msGrupMenuId != 1';	
		$data['dataGrup'] 	= $this->Grup_model->select_all($where_grup);

		$data['page'] 		= "grup menu";
		$data['judul'] 		= "Data Grup Menu";
		$data['deskripsi'] 	= "Manage Data Grup Menu";

		$data['modal_tambah_grup_menu'] = show_my_modal('modals/modal_grup_menu_tambah', 'tambah-grup-menu', $data);

		$this->template->views('grup/home', $data);
	}

	public function tampil() {
		$where_grup = 'msGrupMenuId != 1';	
		$data['dataGrup'] = $this->Grup_model->select_all($where_grup);
		$this->load->view('grup/list_data', $data);
	}	

	public function prosesTambah() {
		$this->form_validation->set_rules('grup', 'Nama grup', 'trim|required');

		$data 	= $this->input->post();
		if ($this->form_validation->run() == TRUE) {
			$result = $this->Grup_model->insert($data);

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Berhasil ditambahkan', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Gagal ditambahkan', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function update() {
		$data['userdata'] 	= $this->userdata;

		 $id 				= trim($_POST['id']);
		//$id = 1;
		$data['dataGrup'] 	= $this->Grup_model->select_by_id($id);

		echo show_my_modal('modals/modal_grup_menu_update', 'update-grup', $data);
	}

	public function prosesUpdate() {
		$this->form_validation->set_rules('grup', 'Nama grup', 'trim|required');

		$data 	= $this->input->post();
		if ($this->form_validation->run() == TRUE) {
			$result = $this->Grup_model->update($data);

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Menu Berhasil diupdate', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Menu Gagal diupdate', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function delete() {
		$id = $_POST['id'];
		$result = $this->Grup_model->delete($id);
		
		if ($result > 0) {
			echo show_succ_msg('Data Berhasil dihapus', '20px');
		} else {
			echo show_err_msg('Data Gagal dihapus', '20px');
		}
	}

	public function export() {
		error_reporting(E_ALL);
    
		include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->Grup_model->select_all();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "ID");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "Nama Grup Menu");		
		$rowCount++;

		foreach($data as $value){
		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->msGrupMenuId); 
		    $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->msGrupMenuNama); 		    
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data Grup Menu.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data Grup Menu.xlsx', NULL);
	}

	public function import() {
		$this->form_validation->set_rules('excel', 'File', 'trim|required');

		if ($_FILES['excel']['name'] == '') {
			$this->session->set_flashdata('msg', 'File harus diisi');
		} else {
			$config['upload_path'] = './assets/excel/';
			$config['allowed_types'] = 'xls|xlsx';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('excel')){
				$error = array('error' => $this->upload->display_errors());
			}
			else{
				$data = $this->upload->data();
				
				error_reporting(E_ALL);
				date_default_timezone_set('Asia/Jakarta');

				include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

				$inputFileName = './assets/excel/' .$data['file_name'];
				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

				$index = 0;
				foreach ($sheetData as $key => $value) {
					if ($key != 1) {
						$id = md5(DATE('ymdhms').rand());
						$check = $this->Grup_model->check_nama($value['B']);

						if ($check != 1) {
							//$resultData[$index]['id'] = $id;
							$resultData[$index]['msGrupMenuNama'] = ucwords($value['B']);							
						}
					}
					$index++;
				}

				unlink('./assets/excel/' .$data['file_name']);

				if (count($resultData) != 0) {
					$result = $this->Grup_model->insert_batch($resultData);
					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Data Berhasil diimport ke database'));
						redirect('Grup');
					}
				} else {
					$this->session->set_flashdata('msg', show_msg('Data Gagal diimport ke database (Data Sudah ada)', 'warning', 'fa-warning'));
					redirect('Grup');
				}

			}
		}
	}

}
