<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_pegawai extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Data_pegawai_model');
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		// $data['dataPegawai'] = $this->Data_pegawai_model->select_all();
		$data['dataJenisPegawai'] 	= $this->Data_pegawai_model->select_jenis_pegawai();
		$data['dataUnitKerja'] 	= $this->Data_pegawai_model->select_unit_kerja();		

		$data['page'] 		= "pegawai";
		$data['judul'] 		= "Data Pegawai";
		$data['deskripsi'] 	= "Manage Data Pegawai";

		$data['modal_pegawai_tambah'] = show_my_modal('modals/modal_pegawai_tambah', 'tambah-pegawai', $data);

		$this->template->views('pegawai/home', $data);
	}

	public function tampil() {
		$id_instansi = $this->userdata->id_instansi;

		if ($id_instansi == 0) {
			$where_data = array(
				'status != ' => 99,
				// 'msPegawaiIdInstansi ' => $id_instansi
			);
		}else{
			$where_data = array(
				'status != ' => 99,
				'msPegawaiIdInstansi ' => $id_instansi
			);
		}
		
		$order_by = array(
			'msPegawaiId ASC '
			
		);

		
		$data['dataPegawai'] = $this->Data_pegawai_model->select_data_pegawai($where_data,$order_by);
		$this->load->view('pegawai/list_data', $data);
	}	

	public function prosesTambah() {
		$this->form_validation->set_rules('nip_pegawai', 'Nip pegawai', 'trim|required');		
		$this->form_validation->set_rules('nama_pegawai', 'Nama pegawai', 'trim|required');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat lahir', 'trim|required');
		$this->form_validation->set_rules('pendidikan', 'Pendidikan', 'trim|required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|required');
		$this->form_validation->set_rules('agama', 'Agama', 'trim|required');
		$this->form_validation->set_rules('alamat', 'ALamat', 'trim|required');
		$this->form_validation->set_rules('instansi', 'Unit kerja', 'trim|required');
		$this->form_validation->set_rules('jenis_pegawai', 'Jenis tenaga', 'trim|required');		
		$this->form_validation->set_rules('status_pegawai', 'Status pegawai', 'trim|required');


		$nip_pegawai 	= $this->input->post('nip_pegawai');
		$nama_pegawai 	= $this->input->post('nama_pegawai');
		$tempat_lahir 	= $this->input->post('tempat_lahir');
		$tgl_lahir 	= $this->input->post('tgl_lahir');
		$pendidikan 	= $this->input->post('pendidikan');
		$jurusan = $this->input->post('jurusan');
		$agama 	= $this->input->post('agama');
		$alamat 	= $this->input->post('alamat');
		$instansi 	= $this->input->post('instansi');
		$unit_kerja 	= $this->input->post('unit_kerja');
		$jenis_pegawai 	= $this->input->post('jenis_pegawai');
		$status_pegawai = $this->input->post('status_pegawai');
		$golongan = $this->input->post('golongan');

		$sql_instansi = $this->db->query("SELECT msInstansiNama
								from tabel_dinkes_instansi
								where msInstansiId ='$instansi'
                          ");
		foreach($sql_instansi->result() as $hasil){
			$nama_instansi = $hasil->msInstansiNama;	
		}
		

		$user = $this->userdata->id;

		if ($this->form_validation->run() == TRUE) {
			$data_input = array(
        		
				'msPegawaiIdInstansi' => $instansi,
				'msPegawaiNamaInstansi' => $nama_instansi,
				'msPegawaiNip' => $nip_pegawai,
				'msPegawaiNama' => $nama_pegawai,
				'msPegawaiTempatLahir' => $tempat_lahir,
				'msPegawaiTglLahir' => $tgl_lahir,
				'msPegawaiPendidikan' => $pendidikan,
				'msPegawaiPendidikanJurusan' => $jurusan,
				'msPegawaiAgama' => $agama,
				'msPegawaiAlamat' => $alamat,
				'msPegawaiUnitKerja' => $unit_kerja,
				'msPegawaiJabatan' => $jenis_pegawai,
				'msPegawaiStatus' => $status_pegawai,
				'msPegawaiGol' => $golongan,
				'created_by' => $user,
				'created_time' => date('Y-m-d H:i:s'),
				// 'updated_by' => 0,
				// 'updated_time' => date('Y-m-d H:i:s'),
				// 'deleted_by' => 0,
				// 'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1
				);
     		$table_name = 'tabel_dinkes_pegawai_instansi';
      		// $id = $this->Perijinan_penelitian_model->simpan_perijinan_penelitian($data_input, $table_name);

			$result = $this->Data_pegawai_model->insert($data_input, $table_name);

			if ($result > 0) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data Berhasil ditambahkan', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data Gagal ditambahkan', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function update() {
		$data['userdata'] 	= $this->userdata;

		$id 				= trim($_POST['id']);
		//$id = 1;
		$data['dataPegawai'] 	= $this->Data_pegawai_model->select_by_id($id);
		$data['dataJenisPegawai'] 	= $this->Data_pegawai_model->select_jenis_pegawai();
		$data['dataUnitKerja'] 	= $this->Data_pegawai_model->select_unit_kerja();

		echo show_my_modal('modals/modal_pegawai_update', 'update-pegawai', $data);
	}

	public function prosesUpdate() {
		$this->form_validation->set_rules('nip_pegawai', 'Nip pegawai', 'trim|required');		
		$this->form_validation->set_rules('nama_pegawai', 'Nama pegawai', 'trim|required');
		$this->form_validation->set_rules('tempat_lahir', 'Tempat lahir', 'trim|required');
		$this->form_validation->set_rules('pendidikan', 'Pendidikan', 'trim|required');
		$this->form_validation->set_rules('jurusan', 'Jurusan', 'trim|required');
		$this->form_validation->set_rules('agama', 'Agama', 'trim|required');
		$this->form_validation->set_rules('alamat', 'ALamat', 'trim|required');
		$this->form_validation->set_rules('instansi', 'Unit kerja', 'trim|required');
		$this->form_validation->set_rules('jenis_pegawai', 'Jenis tenaga', 'trim|required');		
		$this->form_validation->set_rules('status_pegawai', 'Status pegawai', 'trim|required');


		$nip_pegawai 	= $this->input->post('nip_pegawai');
		$nama_pegawai 	= $this->input->post('nama_pegawai');
		$tempat_lahir 	= $this->input->post('tempat_lahir');
		$tgl_lahir 	= $this->input->post('tgl_lahir');
		$pendidikan 	= $this->input->post('pendidikan');
		$jurusan = $this->input->post('jurusan');
		$agama 	= $this->input->post('agama');
		$alamat 	= $this->input->post('alamat');
		$instansi 	= $this->input->post('instansi');
		$jenis_pegawai 	= $this->input->post('jenis_pegawai');
		$status_pegawai = $this->input->post('status_pegawai');
		$golongan = $this->input->post('golongan');

		$sql_instansi = $this->db->query("SELECT msInstansiNama
								from tabel_dinkes_instansi
								where msInstansiId ='$instansi'
                          ");
		foreach($sql_instansi->result() as $hasil){
			$nama_instansi = $hasil->msInstansiNama;	
		}
		

		$user = $this->userdata->id;

		if ($this->form_validation->run() == TRUE) {
			$data_update = array(
        		'msPegawaiIdInstansi' => $instansi,
				'msPegawaiNamaInstansi' => $nama_instansi,
				'msPegawaiNip' => $nip_pegawai,
				'msPegawaiNama' => $nama_pegawai,
				'msPegawaiTempatLahir' => $tempat_lahir,
				'msPegawaiTglLahir' => $tgl_lahir,
				'msPegawaiPendidikan' => $pendidikan,
				'msPegawaiPendidikanJurusan' => $jurusan,
				'msPegawaiAgama' => $agama,
				'msPegawaiAlamat' => $alamat,
				'msPegawaiUnitKerja' => $nama_instansi,
				'msPegawaiJabatan' => $jenis_pegawai,
				'msPegawaiStatus' => $status_pegawai,
				'msPegawaiGol' => $golongan,
				// 'created_by' => $user,
				// 'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				// 'deleted_by' => 0,
				// 'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1
				);
		}
 		$table_name = 'tabel_dinkes_pegawai_instansi';
  		
  		$where       = array(
        	'msPegawaiId' => trim($this->input->post('id'))        
		);		

		// $this->Data_pegawai_model->update($data_update, $where, $table_name);

		
		if ($this->form_validation->run() == TRUE) {
			$result = $this->Data_pegawai_model->update($data_update, $where, $table_name);

			if ($result < 1) {
				$out['status'] = '';
				$out['msg'] = show_succ_msg('Data  Berhasil diupdate', '20px');
			} else {
				$out['status'] = '';
				$out['msg'] = show_err_msg('Data  Gagal diupdate', '20px');
			}
		} else {
			$out['status'] = 'form';
			$out['msg'] = show_err_msg(validation_errors());
		}

		echo json_encode($out);
	}

	public function delete() {

		$id = $_POST['id'];
		// $id=7;
		$user = $this->userdata->id;

		$data_update = array(
			'msPegawaiId' => $id,
			'status' => 99,
			'deleted_by' => $user,
			'deleted_time' => date('Y-m-d H:i:s'),

		);	

		$where       = array(
        	'msPegawaiId' => $id        
		);		


        $table_name  = 'tabel_dinkes_pegawai_instansi';        

		$id = $_POST['id'];
		// $result = $this->Data_pegawai_model->delete($id);
		$result = $this->Data_pegawai_model->update($data_update, $where, $table_name);
		
		if ($result < 1) {
			echo show_succ_msg('Data Berhasil dihapus', '20px');
		} else {
			echo show_err_msg('Data Gagal dihapus', '20px');
		}
	}

	public function export() {
		error_reporting(E_ALL);
    
		include_once './assets/phpexcel/Classes/PHPExcel.php';
		$objPHPExcel = new PHPExcel();

		$data = $this->Data_pegawai_model->select_all_pegawai();

		$objPHPExcel = new PHPExcel(); 
		$objPHPExcel->setActiveSheetIndex(0); 
		$rowCount = 1; 

		$objPHPExcel->getActiveSheet()->getStyle('A1:O1')->applyFromArray(array('fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'f4c82b',
					'setBorderStyle' => PHPExcel_Style_Border::BORDER_THICK
        ))));

		$objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, "ID");
		$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, "ID INSTANSI");
		$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, "NAMA INSTANSI");
		$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, "NIP");
		$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, "NAMA");
		$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, "TEMPAT LAHIR");
		$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, "TANGGAL LAHIR");
		$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, "PENDIDIKAN");
		$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, "JURUSAN");
		$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, "AGAMA");
		$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, "ALAMAT");
		$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, "SUB BAGIAN");
		$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, "JENIS TENAGA");
		$objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, "STATUS");
		$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, "GOLONGAN");
		$rowCount++;

		foreach($data as $value){

		    $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount, $value->msPegawaiId);
			$objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value->msPegawaiIdInstansi);
			$objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $value->msPegawaiNamaInstansi);
			$objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $value->msPegawaiNip);
			$objPHPExcel->getActiveSheet()->SetCellValue('E'.$rowCount, $value->msPegawaiNama);
			$objPHPExcel->getActiveSheet()->SetCellValue('F'.$rowCount, $value->msPegawaiTempatLahir);
			$objPHPExcel->getActiveSheet()->SetCellValue('G'.$rowCount, $value->msPegawaiTglLahir);
			$objPHPExcel->getActiveSheet()->SetCellValue('H'.$rowCount, $value->msPegawaiPendidikan);
			$objPHPExcel->getActiveSheet()->SetCellValue('I'.$rowCount, $value->msPegawaiPendidikanJurusan);
			$objPHPExcel->getActiveSheet()->SetCellValue('J'.$rowCount, $value->msPegawaiAgama);
			$objPHPExcel->getActiveSheet()->SetCellValue('K'.$rowCount, $value->msPegawaiAlamat);
			$objPHPExcel->getActiveSheet()->SetCellValue('L'.$rowCount, $value->msPegawaiUnitKerja);
			$objPHPExcel->getActiveSheet()->SetCellValue('M'.$rowCount, $value->msPegawaiJabatan);
			$objPHPExcel->getActiveSheet()->SetCellValue('N'.$rowCount, $value->msPegawaiStatus);
			$objPHPExcel->getActiveSheet()->SetCellValue('O'.$rowCount, $value->msPegawaiGol);
		    $rowCount++; 
		} 

		$objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
		$objWriter->save('./assets/excel/Data_pegawai.xlsx'); 

		$this->load->helper('download');
		force_download('./assets/excel/Data_pegawai.xlsx', NULL);
	}

	public function import() {
		$this->form_validation->set_rules('excel', 'File', 'trim|required');

		if ($_FILES['excel']['name'] == '') {
			$this->session->set_flashdata('msg', 'File harus diisi');
		} else {
			$config['upload_path'] = './assets/excel/';
			$config['allowed_types'] = 'xls|xlsx';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('excel')){
				$error = array('error' => $this->upload->display_errors());
			}
			else{
				$data = $this->upload->data();
				
				error_reporting(E_ALL);
				date_default_timezone_set('Asia/Jakarta');

				include './assets/phpexcel/Classes/PHPExcel/IOFactory.php';

				$inputFileName = './assets/excel/' .$data['file_name'];
				$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

				$index = 0;
				foreach ($sheetData as $key => $value) {
					if ($key != 1) {
						$id = md5(DATE('ymdhms').rand());
						$check = $this->Data_pegawai_model->check_nip($value['B']);
						$user = $this->userdata->id;

						if ($check != 1) {
							//$resultData[$index]['id'] = $id;
							$resultData[$index]['msPegawaiIdInstansi'] = ucwords($value['B']);
							$resultData[$index]['msPegawaiIdInstansi'] = ucwords($value['B']);
							$resultData[$index]['msPegawaiNamaInstansi'] = ucwords($value['C']);
							$resultData[$index]['msPegawaiNip'] = ucwords($value['D']);
							$resultData[$index]['msPegawaiNama'] = ucwords($value['E']);
							$resultData[$index]['msPegawaiTempatLahir'] = ucwords($value['F']);
							$resultData[$index]['msPegawaiTglLahir'] = ucwords($value['G']);
							$resultData[$index]['msPegawaiPendidikan'] = ucwords($value['H']);
							$resultData[$index]['msPegawaiPendidikanJurusan'] = ucwords($value['I']);
							$resultData[$index]['msPegawaiAgama'] = ucwords($value['J']);
							$resultData[$index]['msPegawaiAlamat'] = ucwords($value['K']);
							$resultData[$index]['msPegawaiUnitKerja'] = ucwords($value['L']);
							$resultData[$index]['msPegawaiJabatan'] = ucwords($value['M']);
							$resultData[$index]['msPegawaiStatus'] = ucwords($value['N']);
							$resultData[$index]['msPegawaiGol'] = ucwords($value['O']);
							$resultData[$index]['created_by'] = $user;
							$resultData[$index]['created_time'] = date('Y-m-d H:i:s');
							$resultData[$index]['updated_by'] = 0;
							$resultData[$index]['updated_time'] = date('Y-m-d H:i:s');
							$resultData[$index]['deleted_by'] = 0;
							$resultData[$index]['deleted_time'] = date('Y-m-d H:i:s');
							$resultData[$index]['status'] = 1;

						}
					}
					$index++;
				}

				unlink('./assets/excel/' .$data['file_name']);

				if (count($resultData) != 0) {
					$result = $this->Data_pegawai_model->insert_batch($resultData);
					if ($result > 0) {
						$this->session->set_flashdata('msg', show_succ_msg('Data Berhasil diimport ke database'));
						redirect('Data_pegawai');
					}
				} else {
					$this->session->set_flashdata('msg', show_msg('Data Gagal diimport ke database (Data Sudah ada)', 'warning', 'fa-warning'));
					redirect('Data_pegawai');
				}

			}
		}
	}

}
