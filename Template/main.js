var base_url = window.location.origin;
var restore_view = false;

var funcError = function (xhr, ajaxOptions, thrownError) {
	alert("No Response.");
};

function getAjax(url, dataType = "html", callbackSuccess, callbackError = funcError) {
	$.ajax({
		type: "get",
		async: true,
		dataType: dataType,
		url: base_url + url,
		success: function (response) {
			callbackSuccess(response);
		},
		error: function (xhr, ajaxOptions, thrownError) {
			callbackError(xhr, ajaxOptions, thrownError);
		}
	});
}
