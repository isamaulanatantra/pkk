<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Index extends CI_Controller
{
  function __construct()
  {
  parent::__construct();
  $this->load->model('Index_model');
  }
  
  public function selected_kbli()
  {
  $table = 'kbli';
  $where   = array(
    'kbli.status !=' => 99,
    );
  $fields  = 
    "
    kbli.id_kbli,
    kbli.kode_kbli,
    kbli.nama_kbli,
    ";
  $order_by  = 'kbli.kode_kbli';
  echo json_encode($this->Index_model->json_all_index($table, $where, $fields, $order_by));
  }
  
  public function selected_jenis_usaha()
  {
  $table = 'jenis_usaha';
  $where   = array(
    'jenis_usaha.status !=' => 99,
    );
  $fields  = 
    "
    jenis_usaha.id_jenis_usaha,
    jenis_usaha.kode_jenis_usaha,
    jenis_usaha.nama_jenis_usaha,
    jenis_usaha.nilai_index
    ";
  $order_by  = 'jenis_usaha.nama_jenis_usaha';
  echo json_encode($this->Index_model->json_all_index($table, $where, $fields, $order_by));
  }
  
  public function selected_fungsi_jalan()
  {
  $table = 'fungsi_jalan';
  $where   = array(
    'fungsi_jalan.status !=' => 99,
    );
  $fields  = 
    "
    fungsi_jalan.id_fungsi_jalan,
    fungsi_jalan.kode_fungsi_jalan,
    fungsi_jalan.nama_fungsi_jalan,
    fungsi_jalan.nilai_index
    ";
  $order_by  = 'fungsi_jalan.nama_fungsi_jalan';
  echo json_encode($this->Index_model->json_all_index($table, $where, $fields, $order_by));
  }
  
  public function selected_sekala_usaha()
  {
  $table = 'sekala_usaha';
  $where   = array(
    'sekala_usaha.status !=' => 99,
    );
  $fields  = 
    "
    sekala_usaha.id_sekala_usaha,
    sekala_usaha.kode_sekala_usaha,
    sekala_usaha.nama_sekala_usaha,
    sekala_usaha.nilai_index
    ";
  $order_by  = 'sekala_usaha.nama_sekala_usaha';
  echo json_encode($this->Index_model->json_all_index($table, $where, $fields, $order_by));
  }
  public function selected_tingkat_pencemaran()
  {
  $table = 'tingkat_pencemaran';
  $where   = array(
    'tingkat_pencemaran.status !=' => 99,
    );
  $fields  = 
    "
    tingkat_pencemaran.id_tingkat_pencemaran,
    tingkat_pencemaran.kode_tingkat_pencemaran,
    tingkat_pencemaran.nama_tingkat_pencemaran,
    tingkat_pencemaran.nilai_index
    ";
  $order_by  = 'tingkat_pencemaran.nama_tingkat_pencemaran';
  echo json_encode($this->Index_model->json_all_index($table, $where, $fields, $order_by));
  }
  public function selected_waktu_kegiatan()
  {
  $table = 'waktu_kegiatan';
  $where   = array(
    'waktu_kegiatan.status !=' => 99,
    );
  $fields  = 
    "
    waktu_kegiatan.id_waktu_kegiatan,
    waktu_kegiatan.kode_waktu_kegiatan,
    waktu_kegiatan.nama_waktu_kegiatan,
    waktu_kegiatan.nilai_index
    ";
  $order_by  = 'waktu_kegiatan.nama_waktu_kegiatan';
  echo json_encode($this->Index_model->json_all_index($table, $where, $fields, $order_by));
  }
  public function selected_tinggi_bangunan()
  {
  $table = 'tinggi_bangunan';
  $where   = array(
    'tinggi_bangunan.status !=' => 99,
    );
  $fields  = 
    "
    tinggi_bangunan.id_tinggi_bangunan,
    tinggi_bangunan.kode_tinggi_bangunan,
    tinggi_bangunan.nama_tinggi_bangunan,
    tinggi_bangunan.nilai_index
    ";
  $order_by  = 'tinggi_bangunan.nama_tinggi_bangunan';
  echo json_encode($this->Index_model->json_all_index($table, $where, $fields, $order_by));
  }
  public function selected_luas_bangunan()
  {
  $table = 'luas_bangunan';
  $where   = array(
    'luas_bangunan.status !=' => 99,
    );
  $fields  = 
    "
    luas_bangunan.id_luas_bangunan,
    luas_bangunan.kode_luas_bangunan,
    luas_bangunan.nama_luas_bangunan,
    luas_bangunan.nilai_index
    ";
  $order_by  = 'luas_bangunan.nama_luas_bangunan';
  echo json_encode($this->Index_model->json_all_index($table, $where, $fields, $order_by));
  }
	
  public function json_all_jenis_usaha()
  {
  $id_izin_gangguan  = $this->input->post('id_jenis_usaha');
  $table = 'jenis_usaha';
  $where   = array(
    'jenis_usaha.status !=' => 99,
    'jenis_usaha.id_jenis_usaha' => $id_izin_gangguan
    );
  $fields  = 
    "
		jenis_usaha.id_jenis_usaha,
		jenis_usaha.nilai_index
    ";
  $order_by  = 'jenis_usaha.id_jenis_usaha';
  echo json_encode($this->Index_model->json_all_index($table, $where, $fields, $order_by));
  }
}