<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pokja_empat extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Pokja_empat_model');
   }
   
  public function index(){
    $data['main_view'] = 'pokja_empat/home';
    $this->load->view('tes', $data);
   }
  public function rekap(){
    $data['main_view'] = 'pokja_empat/rekap';
    $this->load->view('tes', $data);
   }
  public function cetak_pokja_empat(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_empat/cetak_pokja_empat';
    $this->load->view('print', $data);
   }
  public function cetak_semua_pokja_empat(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_empat/cetak_semua_pokja_empat';
    $this->load->view('print', $data);
   }
  public function cetak_pokja_empat_pekarangan(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_empat/cetak_pokja_empat_pekarangan';
    $this->load->view('print', $data);
   }
  public function cetak(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_empat/cetak';
    $this->load->view('print', $data);
   }
	public function json_all_pokja_empat(){
    $web=$this->uut->namadomain(base_url());
		$table = 'pokja_empat';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
    ( select (dusun.nama_dusun) from dusun where dusun.id_dusun=pokja_empat.id_dusun limit 1) as nama_dusun,
    ( select (desa.nama_desa) from desa where desa.id_desa=pokja_empat.id_desa limit 1) as nama_desa,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=pokja_empat.id_kecamatan limit 1) as nama_kecamatan
    ";
    if($web=='demoopd.wonosobokab.go.id'){
      $where = array(
        'pokja_empat.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where = array(
        'pokja_empat.status !=' => 99
        );
    }else{
      $where = array(
        'pokja_empat.status !=' => 99,
        'pokja_empat.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        $query_desa = $this->db->query("SELECT desa.nama_desa FROM desa WHERE desa.id_desa = ".$row->id_desa."");
        echo '
        <tr id_pokja_empat="'.$row->id_pokja_empat.'" id="'.$row->id_pokja_empat.'" name="id_pokja_empat">
          <td>'.$urut.'</td>
          <td>'; 
            $id_desa = $row->id_desa; 
            if($id_desa=='9999'){
              echo 'TP PKK Kecamatan';
            } 
            else{
              foreach ($query_desa->result() as $row_desa){
                echo ''.$row_desa->nama_desa.'';
              }
            }
          echo'</td>
          <td>'.$row->jumlah_kader_posyandu.'</td>
          <td>'.$row->jumlah_kader_gizi.'</td>
          <td>'.$row->jumlah_kader_kesling.'</td>
          <td>'.$row->jumlah_kader_penyuluhannarkoba.'</td>
          <td>'.$row->jumlah_kader_phbs.'</td>
          <td>'.$row->jumlah_kader_kb.'</td>
          <td>'.$row->jumlah_kesehatan_posyandu_jumlah.'</td>
          <td>'.$row->jumlah_kesehatan_posyandu_terintegrasi.'</td>
          <td>'.$row->jumlah_kesehatan_lansia_kelompok.'</td>
          <td>'.$row->jumlah_kesehatan_lansia_anggota.'</td>
          <td>'.$row->jumlah_kesehatan_lansia_memilikikartu.'</td>
          <td>'.$row->jumlah_kelestarian_rumahberjamban.'</td>
          <td>'.$row->jumlah_kelestarian_rumahberspal.'</td>
          <td>'.$row->jumlah_kelestarian_rumahtmpsampah.'</td>
          <td>'.$row->jumlah_mck.'</td>
          <td>'.$row->jumlah_krtmemiliki_pdam.'</td>
          <td>'.$row->jumlah_krtmemiliki_sumur.'</td>
          <td>'.$row->jumlah_krtmemiliki_lain.'</td>
          <td>'.$row->jumlah_perencanaansehat_pus.'</td>
          <td>'.$row->jumlah_perencanaansehat_wus.'</td>
          <td>'.$row->jumlah_perencanaansehat_asebtorkbl.'</td>
          <td>'.$row->jumlah_perencanaansehat_asebtorkbp.'</td>
          <td>'.$row->jumlah_perencanaansehat_kkmemilikitabungan.'</td>
          <td>'.$row->keterangan.'</td>
          <td>
            <a href="#tab_form_pokja_empat" data-toggle="tab" class="update_id_pokja_empat badge bg-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>
            <a href="#" id="del_ajax_pokja_empat" class="badge bg-danger btn-sm"><i class="fa fa-cut"></i></a>
          </td>
        </tr>
        ';
      }
			/*echo '
      <tr>
        <td valign="top" colspan="10" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'pokja_empat/cetak_pokja_empat/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Keluarga</a>
        </td>
      </tr>
      ';*/
          
	}
	public function json_all_rekap_pokja_empat(){
    $web=$this->uut->namadomain(base_url());
    $query = $this->db->query("SELECT SUM(jumlah_kader_posyandu)as jumlah_kader_posyandu, 
     SUM(jumlah_kader_gizi)as jumlah_kader_gizi,
     SUM(jumlah_kader_kesling)as jumlah_kader_kesling,
     SUM(jumlah_kader_penyuluhannarkoba)as jumlah_kader_penyuluhannarkoba,
     SUM(jumlah_kader_phbs)as jumlah_kader_phbs,
     SUM(jumlah_kader_kb)as jumlah_kader_kb,
     SUM(jumlah_kesehatan_posyandu_jumlah)as jumlah_kesehatan_posyandu_jumlah,
     SUM(jumlah_kesehatan_posyandu_terintegrasi)as jumlah_kesehatan_posyandu_terintegrasi,
     SUM(jumlah_kesehatan_lansia_kelompok)as jumlah_kesehatan_lansia_kelompok,
     SUM(jumlah_kesehatan_lansia_anggota)as jumlah_kesehatan_lansia_anggota,
     SUM(jumlah_kesehatan_lansia_memilikikartu)as jumlah_kesehatan_lansia_memilikikartu,
     SUM(jumlah_kelestarian_rumahberjamban)as jumlah_kelestarian_rumahberjamban,
     SUM(jumlah_kelestarian_rumahberspal)as jumlah_kelestarian_rumahberspal,
     SUM(jumlah_kelestarian_rumahtmpsampah)as jumlah_kelestarian_rumahtmpsampah,
     SUM(jumlah_mck)as jumlah_mck,
     SUM(jumlah_krtmemiliki_pdam)as jumlah_krtmemiliki_pdam,
     SUM(jumlah_krtmemiliki_sumur)as jumlah_krtmemiliki_sumur,
     SUM(jumlah_krtmemiliki_lain)as jumlah_krtmemiliki_lain,
     SUM(jumlah_perencanaansehat_pus)as jumlah_perencanaansehat_pus,
     SUM(jumlah_perencanaansehat_wus)as jumlah_perencanaansehat_wus,
     SUM(jumlah_perencanaansehat_asebtorkbl)as jumlah_perencanaansehat_asebtorkbl,
     SUM(jumlah_perencanaansehat_asebtorkbp)as jumlah_perencanaansehat_asebtorkbp,
     SUM(jumlah_perencanaansehat_kkmemilikitabungan)as jumlah_perencanaansehat_kkmemilikitabungan,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=pokja_empat.id_kecamatan limit 1) as nama_kecamatan
    FROM pokja_empat WHERE pokja_empat.id_kabupaten=1 AND pokja_empat.id_propinsi=1 GROUP BY pokja_empat.id_kecamatan");
    $urut=0;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        echo '
        <tr>
          <td>'.$urut.'</td>
          <td>'.$row->nama_kecamatan.'</td>
          <td>'.$row->jumlah_kader_posyandu.'</td>
          <td>'.$row->jumlah_kader_gizi.'</td>
          <td>'.$row->jumlah_kader_kesling.'</td>
          <td>'.$row->jumlah_kader_penyuluhannarkoba.'</td>
          <td>'.$row->jumlah_kader_phbs.'</td>
          <td>'.$row->jumlah_kader_kb.'</td>
          <td>'.$row->jumlah_kesehatan_posyandu_jumlah.'</td>
          <td>'.$row->jumlah_kesehatan_posyandu_terintegrasi.'</td>
          <td>'.$row->jumlah_kesehatan_lansia_kelompok.'</td>
          <td>'.$row->jumlah_kesehatan_lansia_anggota.'</td>
          <td>'.$row->jumlah_kesehatan_lansia_memilikikartu.'</td>
          <td>'.$row->jumlah_kelestarian_rumahberjamban.'</td>
          <td>'.$row->jumlah_kelestarian_rumahberspal.'</td>
          <td>'.$row->jumlah_kelestarian_rumahtmpsampah.'</td>
          <td>'.$row->jumlah_mck.'</td>
          <td>'.$row->jumlah_krtmemiliki_pdam.'</td>
          <td>'.$row->jumlah_krtmemiliki_sumur.'</td>
          <td>'.$row->jumlah_krtmemiliki_lain.'</td>
          <td>'.$row->jumlah_perencanaansehat_pus.'</td>
          <td>'.$row->jumlah_perencanaansehat_wus.'</td>
          <td>'.$row->jumlah_perencanaansehat_asebtorkbl.'</td>
          <td>'.$row->jumlah_perencanaansehat_asebtorkbp.'</td>
          <td>'.$row->jumlah_perencanaansehat_kkmemilikitabungan.'</td>
          <td></td>
        </tr>
        ';
      }
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='demoopd.wonosobokab.go.id'){
      $where0 = array(
        'pokja_empat.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where0 = array(
        'pokja_empat.status !=' => 99
        );
    }else{
      $where0 = array(
        'pokja_empat.status !=' => 99,
        'pokja_empat.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('pokja_empat');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_pokja_empat()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('jumlah_kader_posyandu', 'jumlah_kader_posyandu', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => 1,
        'id_kabupaten' => 1,
        'id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'tahun' => trim($this->input->post('tahun')),
        'jumlah_kader_posyandu' => trim($this->input->post('jumlah_kader_posyandu')),
        'jumlah_kader_gizi' => trim($this->input->post('jumlah_kader_gizi')),
        'jumlah_kader_kesling' => trim($this->input->post('jumlah_kader_kesling')),
        'jumlah_kader_penyuluhannarkoba' => trim($this->input->post('jumlah_kader_penyuluhannarkoba')),
        'jumlah_kader_phbs' => trim($this->input->post('jumlah_kader_phbs')),
        'jumlah_kader_kb' => trim($this->input->post('jumlah_kader_kb')),
        'jumlah_kesehatan_posyandu_jumlah' => trim($this->input->post('jumlah_kesehatan_posyandu_jumlah')),
        'jumlah_kesehatan_posyandu_terintegrasi' => trim($this->input->post('jumlah_kesehatan_posyandu_terintegrasi')),
        'jumlah_kesehatan_lansia_kelompok' => trim($this->input->post('jumlah_kesehatan_lansia_kelompok')),
        'jumlah_kesehatan_lansia_anggota' => trim($this->input->post('jumlah_kesehatan_lansia_anggota')),
        'jumlah_kesehatan_lansia_memilikikartu' => trim($this->input->post('jumlah_kesehatan_lansia_memilikikartu')),
        'jumlah_kelestarian_rumahberjamban' => trim($this->input->post('jumlah_kelestarian_rumahberjamban')),
        'jumlah_kelestarian_rumahberspal' => trim($this->input->post('jumlah_kelestarian_rumahberspal')),
        'jumlah_kelestarian_rumahtmpsampah' => trim($this->input->post('jumlah_kelestarian_rumahtmpsampah')),
        'jumlah_mck' => trim($this->input->post('jumlah_mck')),
        'jumlah_krtmemiliki_pdam' => trim($this->input->post('jumlah_krtmemiliki_pdam')),
        'jumlah_krtmemiliki_sumur' => trim($this->input->post('jumlah_krtmemiliki_sumur')),
        'jumlah_krtmemiliki_lain' => trim($this->input->post('jumlah_krtmemiliki_lain')),
        'jumlah_perencanaansehat_pus' => trim($this->input->post('jumlah_perencanaansehat_pus')),
        'jumlah_perencanaansehat_wus' => trim($this->input->post('jumlah_perencanaansehat_wus')),
        'jumlah_perencanaansehat_asebtorkbl' => trim($this->input->post('jumlah_perencanaansehat_asebtorkbl')),
        'jumlah_perencanaansehat_asebtorkbp' => trim($this->input->post('jumlah_perencanaansehat_asebtorkbp')),
        'jumlah_perencanaansehat_kkmemilikitabungan' => trim($this->input->post('jumlah_perencanaansehat_kkmemilikitabungan')),
        'keterangan' => trim($this->input->post('keterangan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'pokja_empat';
      $this->Pokja_empat_model->simpan_pokja_empat($data_input, $table_name);
      /*echo $id;
			$table_name  = 'anggota_keluarga';
      $where       = array(
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_pokja_empat' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);*/
     }
   }
  public function update_pokja_empat()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('jumlah_kader_posyandu', 'jumlah_kader_posyandu', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => 1,
        'id_kabupaten' => 1,
        'id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'tahun' => trim($this->input->post('tahun')),
        'jumlah_kader_posyandu' => trim($this->input->post('jumlah_kader_posyandu')),
        'jumlah_kader_gizi' => trim($this->input->post('jumlah_kader_gizi')),
        'jumlah_kader_kesling' => trim($this->input->post('jumlah_kader_kesling')),
        'jumlah_kader_penyuluhannarkoba' => trim($this->input->post('jumlah_kader_penyuluhannarkoba')),
        'jumlah_kader_phbs' => trim($this->input->post('jumlah_kader_phbs')),
        'jumlah_kader_kb' => trim($this->input->post('jumlah_kader_kb')),
        'jumlah_kesehatan_posyandu_jumlah' => trim($this->input->post('jumlah_kesehatan_posyandu_jumlah')),
        'jumlah_kesehatan_posyandu_terintegrasi' => trim($this->input->post('jumlah_kesehatan_posyandu_terintegrasi')),
        'jumlah_kesehatan_lansia_kelompok' => trim($this->input->post('jumlah_kesehatan_lansia_kelompok')),
        'jumlah_kesehatan_lansia_anggota' => trim($this->input->post('jumlah_kesehatan_lansia_anggota')),
        'jumlah_kesehatan_lansia_memilikikartu' => trim($this->input->post('jumlah_kesehatan_lansia_memilikikartu')),
        'jumlah_kelestarian_rumahberjamban' => trim($this->input->post('jumlah_kelestarian_rumahberjamban')),
        'jumlah_kelestarian_rumahberspal' => trim($this->input->post('jumlah_kelestarian_rumahberspal')),
        'jumlah_kelestarian_rumahtmpsampah' => trim($this->input->post('jumlah_kelestarian_rumahtmpsampah')),
        'jumlah_mck' => trim($this->input->post('jumlah_mck')),
        'jumlah_krtmemiliki_pdam' => trim($this->input->post('jumlah_krtmemiliki_pdam')),
        'jumlah_krtmemiliki_sumur' => trim($this->input->post('jumlah_krtmemiliki_sumur')),
        'jumlah_krtmemiliki_lain' => trim($this->input->post('jumlah_krtmemiliki_lain')),
        'jumlah_perencanaansehat_pus' => trim($this->input->post('jumlah_perencanaansehat_pus')),
        'jumlah_perencanaansehat_wus' => trim($this->input->post('jumlah_perencanaansehat_wus')),
        'jumlah_perencanaansehat_asebtorkbl' => trim($this->input->post('jumlah_perencanaansehat_asebtorkbl')),
        'jumlah_perencanaansehat_asebtorkbp' => trim($this->input->post('jumlah_perencanaansehat_asebtorkbp')),
        'jumlah_perencanaansehat_kkmemilikitabungan' => trim($this->input->post('jumlah_perencanaansehat_kkmemilikitabungan')),
        'keterangan' => trim($this->input->post('keterangan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'pokja_empat';
      $where       = array(
        'pokja_empat.id_pokja_empat' => trim($this->input->post('id_pokja_empat')),
        'pokja_empat.domain' => $web
			);
      $this->Pokja_empat_model->update_data_pokja_empat($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      if($web=='demoopd.wonosobokab.go.id'){
        $where = array(
        'pokja_empat.id_pokja_empat' => $this->input->post('id_pokja_empat'),
          );
      }elseif($web=='pkk.wonosobokab.go.id'){
        $where = array(
        'pokja_empat.id_pokja_empat' => $this->input->post('id_pokja_empat'),
          );
      }else{
        $where = array(
        'pokja_empat.id_pokja_empat' => $this->input->post('id_pokja_empat'),
          'pokja_empat.id_kecamatan' => $this->session->userdata('id_kecamatan')
          );
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_pokja_empat');
      $result = $this->db->get('pokja_empat');
      echo json_encode($result->result_array());
		}
   
   public function total_pokja_empat()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('pokja_empat');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_pokja_empat = $this->input->post('id_pokja_empat');
      $where = array(
        'id_pokja_empat' => $id_pokja_empat,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('pokja_empat');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'pokja_empat';
        $this->Pokja_empat_model->update_data_pokja_empat($data_update, $where, $table_name);
        echo 1;
        }
		}
  function option_desa_by_id_kecamatan(){
		$id_kecamatan = $this->session->userdata('id_kecamatan');
		$w = $this->db->query("
		SELECT id_desa, nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function option_desa_by_id_desa(){
		$id_kecamatan = $this->input->post('id_kecamatan');
		$w = $this->db->query("
		SELECT id_desa, nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function option_propinsi(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_propinsi="";
		}
		else{
			//$where_id_propinsi="and propinsi.id_propinsi=".$this->session->userdata('id_propinsi')."";
			$where_id_propinsi="";
		}
		$w = $this->db->query("
		SELECT *
		from propinsi
		where propinsi.status = 1
    ".$where_id_propinsi."
		order by propinsi.id_propinsi
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_propinsi.'">'.$h->nama_propinsi.'</option>';
		}
	}
  function id_kabupaten_by_id_propinsi(){
		$id_propinsi = $this->input->post('id_propinsi');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kabupaten
		where kabupaten.status = 1
    and kabupaten.id_propinsi=".$id_propinsi."
		order by kabupaten.id_kabupaten
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kabupaten.'">'.$h->nama_kabupaten.'</option>';
		}
	}
  function id_kecamatan_by_id_kabupaten(){
		$id_kabupaten = $this->input->post('id_kabupaten');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kecamatan
		where kecamatan.status = 1
		and kecamatan.id_kabupaten=".$id_kabupaten."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function id_desa_by_id_kecamatan(){
		$id_kecamatan = $this->session->userdata('id_kecamatan');
		$w = $this->db->query("
		SELECT nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function id_dusun_by_id_desa(){
		$id_desa = $this->input->post('id_desa');
		$w = $this->db->query("
		SELECT *
		from dusun
		where dusun.status = 1
		and dusun.id_desa = ".$id_desa."
		order by id_dusun");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_dusun.'">'.$h->nama_dusun.'</option>';
		}
	}
  function option_kecamatan(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_kecamatan="";
		}
		else{
			$where_id_kecamatan="and data_kecamatan.id_kecamatan=".$this->session->userdata('id_kecamatan')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_kecamatan, kecamatan
		where data_kecamatan.status = 1
		and kecamatan.id_kecamatan=data_kecamatan.id_kecamatan
    ".$where_id_kecamatan."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function status_dalam_keluarga(){
		$w = $this->db->query("
		SELECT *
		from status_dalam_keluarga
		where status_dalam_keluarga.status = 1
		order by status_dalam_keluarga.id_status_dalam_keluarga
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_dalam_keluarga.'">'.$h->nama_status_dalam_keluarga.'</option>';
		}
	}
  function pekerjaan(){
		$w = $this->db->query("
		SELECT *
		from pekerjaan
		where pekerjaan.status = 1
		order by pekerjaan.id_pekerjaan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pekerjaan.'">'.$h->nama_pekerjaan.'</option>';
		}
	}
  function agama(){
		$w = $this->db->query("
		SELECT *
		from agama
		where agama.status = 1
		order by agama.id_agama
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_agama.'">'.$h->nama_agama.'</option>';
		}
	}
  function status_pernikahan(){
		$w = $this->db->query("
		SELECT *
		from status_pernikahan
		where status_pernikahan.status = 1
		order by status_pernikahan.id_status_pernikahan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_pernikahan.'">'.$h->nama_status_pernikahan.'</option>';
		}
	}
  function pendidikan(){
		$w = $this->db->query("
		SELECT *
		from pendidikan
		where pendidikan.status = 1
		order by pendidikan.id_pendidikan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pendidikan.'">'.$h->nama_pendidikan.'</option>';
		}
	}
 }