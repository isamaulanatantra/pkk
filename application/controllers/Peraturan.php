<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Peraturan extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    //$this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Cetak_model');
    $this->load->model('Posting_model');
    $this->load->model('Crud_model');
    $this->load->model('Peraturan_model');
    $this->load->model('Komponen_model');
  }
  public function check_login()
  {
    $web = $this->uut->namadomain(base_url());
    $table_name = 'users';
    $where      = array(
      'users.id_users' => $this->session->userdata('id_users')
    );
    $a          = $this->Crud_model->semua_data($where, $table_name);
    if ($a == 0) {
      return redirect('' . base_url() . 'login');
    }
  }
  public function index()
  {
    $web = $this->uut->namadomain(base_url());
    $this->check_login();
    $where             = array(
      'peraturan.status !=' => 99
    );
    $a                 = $this->Peraturan_model->json_semua_peraturan($where);
    $data['per_page']  = 20;
    $data['total']     = ceil($a / 20);
    $start = 0;
    $data['peraturan'] = $this->peraturan(0, $h = "", $start);
    $data['jenis_peraturan'] = $this->jenis_peraturan(0, $h = "", $start);
    $data['opsi_parent_jenis_peraturan'] = $this->opsi_parent_jenis_peraturan(0, $h = "", $start);
    $data['e_opsi_parent_jenis_peraturan'] = $this->e_opsi_parent_jenis_peraturan(0, $h = "", $start);
    $data['main_view'] = 'peraturan/home';
    $this->load->view('tes', $data);
  }
  public function json_all_peraturan()
  {
    $web = $this->uut->namadomain(base_url());
    $where      = array(
      'peraturan.domain' => $web,
      'peraturan.status !=' => 99
    );
    $a          = $this->Peraturan_model->json_semua_peraturan($where);
    $halaman    = $this->input->get('halaman');
    $limit      = 20;
    $start      = ($halaman - 1) * $limit;
    $fields     = "
				
				peraturan.id_peraturan,
				peraturan.id_parent,
				peraturan.nomor_peraturan,
				peraturan.tahun_peraturan,
				peraturan.status_peraturan,
				peraturan.kode_peraturan,
        peraturan.nama_peraturan,
				peraturan.inserted_by,
				peraturan.inserted_time,
				peraturan.updated_by,
				peraturan.updated_time,
				peraturan.deleted_by,
				peraturan.deleted_time,
				peraturan.temp,
				peraturan.keterangan,
				peraturan.status,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Isi Peraturan') as attachment_isi_peraturan,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Katalog') as attachment_katalog,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Abstrak') as attachment_abstrak,
        
				";
    $where      = array(
      'peraturan.domain' => $web,
      'peraturan.status !=' => 99
    );
    $order_by   = 'peraturan.inserted_time';
    echo json_encode($this->Peraturan_model->json_all_peraturan($where, $limit, $start, $fields, $order_by));
  }
  public function json_all_welcome_message()
  {
    $web = $this->uut->namadomain(base_url());
    $where      = array(
      'peraturan.domain' => $web,
      'peraturan.status' => 1,
      'peraturan.id_parent' => 7
    );
    $a          = $this->Peraturan_model->json_semua_peraturan($where);
    $halaman    = $this->input->get('halaman');
    $limit      = 20000;
    $start      = ($halaman - 1) * $limit;
    $fields     = "
				
				peraturan.id_peraturan,
				peraturan.id_parent,
				peraturan.nomor_peraturan,
				peraturan.tahun_peraturan,
				peraturan.status_peraturan,
				peraturan.kode_peraturan,
				peraturan.nama_peraturan,
				peraturan.inserted_by,
				peraturan.inserted_time,
				peraturan.updated_by,
				peraturan.updated_time,
				peraturan.deleted_by,
				peraturan.deleted_time,
				peraturan.temp,
				peraturan.keterangan,
				peraturan.status,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Isi Peraturan') as attachment_isi_peraturan,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Katalog') as attachment_katalog,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Abstrak') as attachment_abstrak,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='NA') as attachment_na,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='MOU') as attachment_mou,
        
				";
    $where      = array(
      'peraturan.status !=' => 99,
      'peraturan.id_parent' => 7
    );
    $order_by   = 'peraturan.tahun_peraturan';
    echo json_encode($this->Peraturan_model->json_all_peraturan($where, $limit, $start, $fields, $order_by));
  }
  public function opsi_parent()
  {
    $web = $this->uut->namadomain(base_url());
    $table_name = 'peraturan';
    $fields =
      '*';
    $where = array(
      'peraturan.domain' => $web,
      'peraturan.status' => 1,
      'peraturan.nomor_peraturan' => '',
      'peraturan.id_parent' => 0,
    );
    $order_by = 'peraturan.id_parent';
    echo json_encode($this->Peraturan_model->opsi_parent($table_name, $fields, $where, $order_by));
  }
  public function simpan_peraturan()
  {
    $web = $this->uut->namadomain(base_url());
    //$this->form_validation->set_rules('id_peraturan', 'id_peraturan', 'required');
    $id_peraturan         = '';
    $this->form_validation->set_rules('nomor_peraturan', 'nomor_peraturan', 'required');
    $nomor_peraturan         = trim($this->input->post('nomor_peraturan'));
    //$this->form_validation->set_rules('jenis_peraturan', 'jenis_peraturan', 'required');
    $jenis_peraturan         = trim($this->input->post('jenis_peraturan'));
    $this->form_validation->set_rules('tahun_peraturan', 'tahun_peraturan', 'required');
    $tahun_peraturan         = trim($this->input->post('tahun_peraturan'));
    $this->form_validation->set_rules('status_peraturan', 'status_peraturan', 'required');
    $status_peraturan         = trim($this->input->post('status_peraturan'));
    $this->form_validation->set_rules('kode_peraturan', 'kode_peraturan', 'required');
    $kode_peraturan         = trim($this->input->post('kode_peraturan'));
    $id_parent         = trim($this->input->post('id_parent'));
    $this->form_validation->set_rules('nama_peraturan', 'nama_peraturan', 'required');
    $nama_peraturan         = trim($this->input->post('nama_peraturan'));
    //$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
    $inserted_by         = $this->session->userdata('id_users');
    //$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
    $inserted_time         = date('Y-m-d H:i:s');
    //$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
    $updated_by         = 0;
    //$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
    $updated_time         = date('Y-m-d H:i:s');
    //$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
    $deleted_by         = 0;
    //$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
    $deleted_time         = date('Y-m-d H:i:s');
    $this->form_validation->set_rules('temp', 'temp', 'required');
    $temp         = trim($this->input->post('temp'));
    // $this->form_validation->set_rules('keterangan', 'keterangan', 'required');
    $keterangan         = trim($this->input->post('keterangan'));
    //$this->form_validation->set_rules('status', 'status', 'required');
    $status         = '1';
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_input = array(

        'domain' => $web,
        'id_parent' => $id_parent,
        'nomor_peraturan' => $nomor_peraturan,
        'jenis_peraturan' => $jenis_peraturan,
        'tahun_peraturan' => $tahun_peraturan,
        'status_peraturan' => $status_peraturan,
        'kode_peraturan' => $kode_peraturan,
        'nama_peraturan' => $nama_peraturan,
        'tipe_peraturan' => 'opd',
        'id_pengarang' => 0,
        'singkatan_jenis_peraturan' => '',
        'cetakan_edisi' => '',
        'tempat_terbit' => '',
        'penerbit' => '',
        'tanggal_pengundangan' => '2020-01-01',
        'kolasi_deskripsi_fisik' => '',
        'sumber' => '',
        'subjek' => '',
        'isbn' => '',
        'bahasa' => '',
        'bidang_hukum' => '',
        'nomor_induk_buku' => '',
        'nama_file_download' => '',
        'url_file_lampiran' => '',
        'url_halaman_peraturan' => '',
        'inserted_by' => $inserted_by,
        'inserted_time' => $inserted_time,
        'updated_by' => $updated_by,
        'updated_time' => $updated_time,
        'deleted_by' => $deleted_by,
        'deleted_time' => $deleted_time,
        'temp' => $temp,
        'keterangan' => $keterangan,
        'status' => $status,

      );
      $table_name = 'peraturan';
      $id         = $this->Peraturan_model->simpan_peraturan($data_input, $table_name);
      echo $id;
      $table_name  = 'attachment';
      $where       = array(
        'table_name' => 'peraturan',
        'temp' => $temp
      );
      $data_update = array(
        'id_tabel' => $id
      );
      $this->Crud_model->update_data($data_update, $where, $table_name);
    }
  }
  public function update_data_peraturan()
  {
    $web = $this->uut->namadomain(base_url());
    $this->form_validation->set_rules('id_peraturan', 'id_peraturan', 'required');
    $id_peraturan         = trim($this->input->post('id_peraturan'));
    $this->form_validation->set_rules('nomor_peraturan', 'nomor_peraturan', 'required');
    $nomor_peraturan         = trim($this->input->post('nomor_peraturan'));
    $this->form_validation->set_rules('tahun_peraturan', 'tahun_peraturan', 'required');
    $tahun_peraturan         = trim($this->input->post('tahun_peraturan'));
    $this->form_validation->set_rules('status_peraturan', 'status_peraturan', 'required');
    $status_peraturan         = trim($this->input->post('status_peraturan'));
    $this->form_validation->set_rules('kode_peraturan', 'kode_peraturan', 'required');
    $kode_peraturan         = trim($this->input->post('kode_peraturan'));
    $id_parent         = trim($this->input->post('id_parent'));
    $this->form_validation->set_rules('nama_peraturan', 'nama_peraturan', 'required');
    $nama_peraturan         = trim($this->input->post('nama_peraturan'));
    //$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
    $inserted_by         = $this->session->userdata('id_users');
    //$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
    $inserted_time         = date('Y-m-d H:i:s');
    //$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
    $updated_by         = $this->session->userdata('id_users');
    //$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
    $updated_time         = date('Y-m-d H:i:s');
    //$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
    $deleted_by         = $this->session->userdata('id_users');
    //$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
    $deleted_time         = date('Y-m-d H:i:s');
    //$this->form_validation->set_rules('temp', 'temp', 'required');
    $temp         = '';
    // $this->form_validation->set_rules('keterangan', 'keterangan', 'required');
    $keterangan         = trim($this->input->post('keterangan'));
    //$this->form_validation->set_rules('status', 'status', 'required');
    $status         = '';
    if ($this->form_validation->run() == FALSE) {
      echo '[]';
    } else {
      $data_update = array(

        'peraturan.domain' => $web,
        'id_parent' => $id_parent,
        'nomor_peraturan' => $nomor_peraturan,
        'tahun_peraturan' => $tahun_peraturan,
        'status_peraturan' => $status_peraturan,
        'kode_peraturan' => $kode_peraturan,
        'nama_peraturan' => $nama_peraturan,
        'updated_by' => $updated_by,
        'updated_time' => $updated_time,
        'keterangan' => $keterangan,

      );
      $table_name  = 'peraturan';
      $where       = array(
        'peraturan.id_peraturan' => $id_peraturan
      );
      $this->Peraturan_model->update_data_peraturan($data_update, $where, $table_name);
      echo '[{"save":"ok"}]';
    }
  }
  public function ajukan()
  {
    $web = $this->uut->namadomain(base_url());
    $id_peraturan         = trim($this->input->post('id_peraturan'));
    $keterangan         = trim($this->input->post('keterangan'));
    $status         = trim($this->input->post('status'));        
    $w = $this->db->query("
    SELECT peraturan.keterangan from peraturan
    where peraturan.id_peraturan=".$id_peraturan."
    ");
    $keterangan_tambahan = '';
    $date = date("Y-m-d H:i:s");
    foreach($w->result() as $row){
        $keterangan_tambahan = $row->keterangan;
    }
    $data_update = array(
      'peraturan.status' => $status,
      'keterangan' => $keterangan_tambahan.'<br>Desa :<br>-> '.$date.'<br>  '.$keterangan
    );
    $table_name  = 'peraturan';
    $where       = array(
      'peraturan.id_peraturan' => $id_peraturan
    );
    $this->Peraturan_model->update_data_peraturan($data_update, $where, $table_name);
    echo '[{"save":"ok"}]';
  }
  public function il_simpan_peraturan()
  {
    $web = $this->uut->namadomain(base_url());
    $this->form_validation->set_rules('id_peraturan', 'id_peraturan', 'required');
    $id_peraturan         = trim($this->input->post('id_peraturan'));
    $this->form_validation->set_rules('kode_peraturan', 'kode_peraturan', 'required');
    $kode_peraturan         = trim($this->input->post('kode_peraturan'));
    $this->form_validation->set_rules('nama_peraturan', 'nama_peraturan', 'required');
    $nama_peraturan         = trim($this->input->post('nama_peraturan'));
    //$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
    $inserted_by         = $this->session->userdata('id_users');
    //$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
    $inserted_time         = date('Y-m-d H:i:s');
    //$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
    $updated_by         = $this->session->userdata('id_users');
    //$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
    $updated_time         = date('Y-m-d H:i:s');
    //$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
    $deleted_by         = $this->session->userdata('id_users');
    //$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
    $deleted_time         = date('Y-m-d H:i:s');
    //$this->form_validation->set_rules('temp', 'temp', 'required');
    $temp         = '';
    $this->form_validation->set_rules('keterangan', 'keterangan', 'required');
    $keterangan         = trim($this->input->post('keterangan'));
    //$this->form_validation->set_rules('status', 'status', 'required');
    $status         = '';

    if ($this->form_validation->run() == FALSE) {
      echo 'Error';
    } else {
      $data_update = array(

        'peraturan.domain' => $web,
        'kode_peraturan' => $kode_peraturan,
        'nama_peraturan' => $nama_peraturan,
        'updated_by' => $updated_by,
        'updated_time' => $updated_time,
        'keterangan' => $keterangan,

      );
      $table_name  = 'peraturan';
      $where       = array(
        'peraturan.id_peraturan' => $id_peraturan
      );
      $this->Peraturan_model->update_data_peraturan($data_update, $where, $table_name);
      echo 'Success';
    }
  }
  public function ag_simpan_peraturan()
  {
    $web = $this->uut->namadomain(base_url());
    $this->form_validation->set_rules('id_peraturan', 'id_peraturan', 'required');
    $id_peraturan         = trim($this->input->post('id_peraturan'));
    $this->form_validation->set_rules('kode_peraturan', 'kode_peraturan', 'required');
    $kode_peraturan         = trim($this->input->post('kode_peraturan'));
    $this->form_validation->set_rules('nama_peraturan', 'nama_peraturan', 'required');
    $nama_peraturan         = trim($this->input->post('nama_peraturan'));
    //$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
    $inserted_by         = $this->session->userdata('id_users');
    //$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
    $inserted_time         = date('Y-m-d H:i:s');
    //$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
    $updated_by         = $this->session->userdata('id_users');
    //$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
    $updated_time         = date('Y-m-d H:i:s');
    //$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
    $deleted_by         = $this->session->userdata('id_users');
    //$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
    $deleted_time         = date('Y-m-d H:i:s');
    //$this->form_validation->set_rules('temp', 'temp', 'required');
    $temp         = '';
    $this->form_validation->set_rules('keterangan', 'keterangan', 'required');
    $keterangan         = trim($this->input->post('keterangan'));
    //$this->form_validation->set_rules('status', 'status', 'required');
    $status         = '';
    if ($this->form_validation->run() == FALSE) {
      echo '[]';
    } else {
      $data_update = array(

        'peraturan.domain' => $web,
        'kode_peraturan' => $kode_peraturan,
        'nama_peraturan' => $nama_peraturan,
        'updated_by' => $updated_by,
        'updated_time' => $updated_time,
        'keterangan' => $keterangan,

      );
      $table_name  = 'peraturan';
      $where       = array(
        'peraturan.domain' => $web,
        'peraturan.id_peraturan' => $id_peraturan
      );
      $this->Peraturan_model->update_data_peraturan($data_update, $where, $table_name);
      echo '[{"peraturan":"ok", "jumlah":"ok"}]';
    }
  }
  public function cetak()
  {
    $web = $this->uut->namadomain(base_url());
    $where             = array(
      'peraturan.domain' => $web,
      'peraturan.status !=' => 99
    );
    $data['data_peraturan'] = $this->Peraturan_model->data_peraturan($where);
    $this->load->view('peraturan/cetak', $data);
  }
  public function peraturan_get_by_id()
  {
    $web = $this->uut->namadomain(base_url());
    $table_name = 'peraturan';
    $id         = $_GET['id'];
    $fields     = "
				peraturan.id_peraturan,
				peraturan.id_parent,
				peraturan.nomor_peraturan,
				peraturan.tahun_peraturan,
				peraturan.status_peraturan,
				peraturan.kode_peraturan,
				peraturan.nama_peraturan,
				peraturan.inserted_by,
				peraturan.inserted_time,
				peraturan.updated_by,
				peraturan.updated_time,
				peraturan.deleted_by,
				peraturan.deleted_time,
				peraturan.temp,
				peraturan.keterangan,
				peraturan.status,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Isi Peraturan') as attachment_isi_peraturan,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Katalog') as attachment_katalog,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Abstrak') as attachment_abstrak,
        
								";
    $where      = array(
      'peraturan.domain' => $web,
      'peraturan.id_peraturan' => $id
    );
    $order_by   = 'peraturan.inserted_time';
    echo json_encode($this->Peraturan_model->get_by_id($table_name, $where, $fields, $order_by));
  }
  public function hapus_peraturan()
  {
    $web = $this->uut->namadomain(base_url());
    $table_name = 'peraturan';
    $id         = $_GET['id'];
    $where      = array(
      'peraturan.id_peraturan' => $id
    );
    $t          = $this->Peraturan_model->json_semua_peraturan($where, $table_name);
    if ($t == 0) {
      echo ' {"errors":"Yes"} ';
    } else {
      $data_update = array(
        'peraturan.status' => 99
      );
      $where       = array(
        'peraturan.domain' => $web,
        'peraturan.id_peraturan' => $id
      );
      $this->Peraturan_model->update_data_peraturan($data_update, $where, $table_name);
      echo ' {"errors":"No"} ';
    }
  }
  public function cari_peraturan()
  {
    $web = $this->uut->namadomain(base_url());
    $table_name = 'peraturan';
    $key_word   = $_GET['key_word'];
    $where      = array(
      'peraturan.domain' => $web,
      'peraturan.status !=' => 99
    );
    $field      = 'peraturan.nama_peraturan';
    $a          = $this->Peraturan_model->count_all_search_peraturan($where, $key_word, $table_name, $field);
    if (empty($key_word)) {
      echo '[]';
      exit;
    } else if ($a == 0) {
      echo '[{"jumlah":"0"}]';
      exit;
    }
    $fields     = "
				peraturan.id_peraturan,
				peraturan.id_parent,
				peraturan.nomor_peraturan,
				peraturan.tahun_peraturan,
				peraturan.status_peraturan,
				peraturan.kode_peraturan,
				peraturan.nama_peraturan,
				peraturan.inserted_by,
				peraturan.inserted_time,
				peraturan.updated_by,
				peraturan.updated_time,
				peraturan.deleted_by,
				peraturan.deleted_time,
				peraturan.temp,
				peraturan.keterangan,
				peraturan.status,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Isi Peraturan') as attachment_isi_peraturan,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Katalog') as attachment_katalog,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Abstrak') as attachment_abstrak,
        
								";
    $where      = array(
      'peraturan.domain' => $web,
      'peraturan.status !=' => 99
    );
    $field_like = 'peraturan.nama_peraturan';
    $limit      = $_GET['limit'];
    $start      = (($_GET['start']) * $limit);
    $order_by   = '' . $field_like . '';
    echo json_encode($this->Peraturan_model->search($table_name, $fields, $where, $limit, $start, $field_like, $key_word, $order_by));
  }
  public function count_all_cari_peraturan()
  {
    $web = $this->uut->namadomain(base_url());
    $table_name = 'peraturan';
    $key_word   = $_GET['key_word'];
    if (empty($key_word)) {
      echo '[]';
      exit;
    }
    $where = array(
      'peraturan.domain' => $web,
      'peraturan.status !=' => 99
    );
    $field = 'peraturan.nama_peraturan';
    $a     = $this->Peraturan_model->count_all_search_peraturan($where, $key_word, $table_name, $field);
    $limit = $_GET['limit'];
    echo '[{"peraturan":"' . ceil($a / $limit) . '", "jumlah":"' . $a . '"}]';
  }
  public function download_xls()
  {
    $web = $this->uut->namadomain(base_url());
    $this->load->library('excel');
    $objPHPExcel = new PHPExcel();
    $objPHPExcel
      ->getProperties()
      ->setCreator("Budi Utomo")
      ->setLastModifiedBy("Budi Utomo")
      ->setTitle("Laporan I")
      ->setSubject("Office 2007 XLSX Document")
      ->setDescription("Laporan untuk menampilkan kunjungan pasien")
      ->setKeywords("Laporan Halaman")
      ->setCategory("Bentuk XLS");
    $objPHPExcel
      ->setActiveSheetIndex(0)
      ->setCellValue('A1', 'Cetak')
      ->mergeCells('A1:G1')
      ->setCellValue('A2', 'Laporan Data Halaman')
      ->mergeCells('A2:G2')
      ->setCellValue('A3', 'Tanggal Download : ' . date('d/m/Y') . ' ')
      ->mergeCells('A3:G3')
      ->setCellValue('A6', 'No')
      ->setCellValue('B6', 'Nama Peraturan')
      ->setCellValue('C6', 'Kode Peraturan')
      ->setCellValue('D6', 'Keterangan');
    $table    = 'peraturan';
    $fields     = "
				peraturan.id_peraturan,
				peraturan.id_parent,
				peraturan.nomor_peraturan,
				peraturan.tahun_peraturan,
				peraturan.status_peraturan,
				peraturan.kode_peraturan,
				peraturan.nama_peraturan,
				peraturan.inserted_by,
				peraturan.inserted_time,
				peraturan.updated_by,
				peraturan.updated_time,
				peraturan.deleted_by,
				peraturan.deleted_time,
				peraturan.temp,
				peraturan.keterangan,
				peraturan.status,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Isi Peraturan') as attachment_isi_peraturan,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Katalog') as attachment_katalog,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Abstrak') as attachment_abstrak,
        
								";
    $where    = array(
      'peraturan.domain' => $web,
      'peraturan.status !=' => 99
    );
    $order_by = 'peraturan.nama_peraturan';
    $b        = $this->Cetak_model->cetak_single_table($table, $fields, $where, $order_by);
    $i        = 7;
    $no       = 0;
    foreach ($b->result() as $b1) {
      $no = $no + 1;
      $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A' . $i, '' . $no . '')
        ->setCellValue('B' . $i, '' . $b1->nama_peraturan . '')
        ->setCellValue('C' . $i, '\'' . $b1->kode_peraturan . '')
        ->setCellValue('D' . $i, '' . $b1->keterangan . '');

      $objPHPExcel
        ->getActiveSheet()
        ->getStyle('A' . $i . ':D' . $i . '')
        ->getBorders()
        ->getAllBorders()
        ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
    }
    $objPHPExcel->getActiveSheet()->setTitle('Probable Clients');
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

    $objPHPExcel->getActiveSheet()->getStyle('A6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('C6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('D6')->getFont()->setBold(true);

    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->getStartColor()->setARGB('cccccc');
    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);

    $objPHPExcel->setActiveSheetIndex(0);
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Halaman_' . date('ymdhis') . '.xls"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: cache, must-revalidate');
    header('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
  }

  public function cetak_pdf()
  {
    $web = $this->uut->namadomain(base_url());
    $table             = 'peraturan';
    $fields     = "
				peraturan.id_peraturan,
				peraturan.id_parent,
				peraturan.nomor_peraturan,
				peraturan.tahun_peraturan,
				peraturan.status_peraturan,
				peraturan.kode_peraturan,
				peraturan.nama_peraturan,
				peraturan.inserted_by,
				peraturan.inserted_time,
				peraturan.updated_by,
				peraturan.updated_time,
				peraturan.deleted_by,
				peraturan.deleted_time,
				peraturan.temp,
				peraturan.keterangan,
				peraturan.status,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Isi Peraturan') as attachment_isi_peraturan,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Katalog') as attachment_katalog,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Abstrak') as attachment_abstrak,
        
								";
    $where             = array(
      'peraturan.domain' => $web,
      'peraturan.status !=' => 99
    );
    $order_by          = 'peraturan.nama_peraturan';
    $data['data_peraturan'] = $this->Cetak_model->cetak_single_table($table, $fields, $where, $order_by);
    $this->load->view('peraturan/pdf', $data);
    $html = $this->output->get_output();
    $this->load->library('dompdf_gen');
    $this->dompdf->load_html($html);
    $this->dompdf->set_paper('a4', 'portrait');
    $this->dompdf->render();
    $this->dompdf->stream("cetak_peraturan_" . date('d_m_y') . ".pdf");
  }
  public function search_peraturan()
  {
    $web = $this->uut->namadomain(base_url());
    $key_word = trim($this->input->post('key_word'));
    $nomor_peraturan = $this->input->post('nomor_peraturan');
    $tahun_peraturan = $this->input->post('tahun_peraturan');
    $id_parent = $this->input->post('id_parent');
    $halaman    = $this->input->post('halaman');
    $limit      = 2000;
    $start      = ($halaman - 1) * $limit;
    $fields     = "
				peraturan.id_peraturan,
				peraturan.id_parent,
				peraturan.nomor_peraturan,
				peraturan.tahun_peraturan,
				peraturan.status_peraturan,
				peraturan.kode_peraturan,
				peraturan.nama_peraturan,
				peraturan.inserted_by,
				peraturan.inserted_time,
				peraturan.updated_by,
				peraturan.updated_time,
				peraturan.deleted_by,
				peraturan.deleted_time,
				peraturan.temp,
				peraturan.keterangan,
				peraturan.status,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Isi Peraturan') as attachment_isi_peraturan,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Katalog') as attachment_katalog,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Abstrak') as attachment_abstrak,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='NA') as attachment_na,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='MOU') as attachment_mou,
								";
    $where      = array(
      'peraturan.domain' => $web,
      'peraturan.status !=' => 99,
      'peraturan.id_parent !=' => 0
    );
    $order_by   = 'peraturan.tahun_peraturan';
    echo json_encode($this->Peraturan_model->search_peraturan($fields, $where, $limit, $start, $key_word, $order_by, $id_parent, $nomor_peraturan, $tahun_peraturan));
  }
  public function count_all_search_peraturan()
  {
    $web = $this->uut->namadomain(base_url());
    $table_name = 'peraturan';
    $key_word = $_GET['key_word'];
    $id_parent = $_GET['id_parent'];
    // $nomor_peraturan = $_GET['nomor_peraturan'];
    // $tahun_peraturan = $_GET['tahun_peraturan'];
    // $halaman = $_GET['halaman'];
    $halaman = 1;
    $fields = "
				peraturan.id_parent,
				peraturan.nomor_peraturan,
				peraturan.tahun_peraturan,
				peraturan.status_peraturan,
				peraturan.nama_peraturan
      ";
    $limit      = 20000;
    $start      = ($halaman - 1) * $limit;
    $where      = array(
      'peraturan.domain' => $web,
      'peraturan.status !=' => 99,
      'peraturan.id_parent !=' => 0
    );
    // $a = $this->Peraturan_model->count_all_search_peraturan($where, $key_word, $table_name, $field);
    // $a = $this->Peraturan_model->count_all_search_peraturan($fields, $where, $key_word, $id_parent, $nomor_peraturan, $tahun_peraturan);
    $a = $this->Peraturan_model->count_all_search_peraturan($fields, $where, $limit, $start, $id_parent);
    // $a = $this->Peraturan_model->count_all_search_peraturan($fields, $where, $limit, $start, $key_word, $id_parent, $nomor_peraturan, $tahun_peraturan);
    // $limit = 20;
    echo ceil($a / $limit);
  }
  public function auto_suggest()
  {
    $web = $this->uut->namadomain(base_url());
    $q = $_GET['q'];
    if (empty($_GET['q'])) {
      exit;
    }
    if (strlen($q) > 2) {
      $where      = array(
        'peraturan.domain' => $web,
        'peraturan.status !=' => 99
      );
      $fields     = "
				peraturan.id_peraturan,
				peraturan.id_parent,
				peraturan.nomor_peraturan,
				peraturan.tahun_peraturan,
				peraturan.status_peraturan,
				peraturan.kode_peraturan,
				peraturan.nama_peraturan,
				peraturan.inserted_by,
				peraturan.inserted_time,
				peraturan.updated_by,
				peraturan.updated_time,
				peraturan.deleted_by,
				peraturan.deleted_time,
				peraturan.temp,
				peraturan.keterangan,
				peraturan.status,
								peraturan.nama_peraturan as value
				";
      echo json_encode($this->Peraturan_model->auto_suggest($q, $where, $fields));
    }
  }
  public function daftar_peraturan()
  {
    $web = $this->uut->namadomain(base_url());
    $table_name = 'peraturan';
    $where_id = array(
      'peraturan.domain' => $web,
      'peraturan.status' => 3,
      'peraturan.id_parent' => $this->input->get('id_peraturan')
    );
    $fields     = "
				peraturan.id_peraturan,
				peraturan.id_parent,
				peraturan.nomor_peraturan,
				peraturan.tahun_peraturan,
				peraturan.status_peraturan,
				peraturan.kode_peraturan,
				peraturan.nama_peraturan,
				peraturan.inserted_by,
				peraturan.inserted_time,
				peraturan.updated_by,
				peraturan.updated_time,
				peraturan.deleted_by,
				peraturan.deleted_time,
				peraturan.temp,
				peraturan.keterangan,
				peraturan.status
								";
    $order_by   = 'peraturan.tahun_peraturan';
    $data['daftar_peraturan'] = $this->Peraturan_model->daftar_peraturan($where_id, $table_name, $fields, $order_by);
    $start = 0;
    $data['peraturan'] = $this->peraturan(0, $h = "", $start);
    $data['jenis_peraturan'] = $this->jenis_peraturan(0, $h = "", $start);
    $a = 0;
    $data['total'] = 10;
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
    $data['menu_atas_posting'] = $this->menu_atas_posting(0,$h="", $a);
    $data['menu_kiri'] = $this->posting(0,$h="", $a);
    $data['welcome1'] = $this->get_komponen('welcome1');
    $data['welcome2'] = $this->get_komponen('welcome2');
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $data['slide_show'] = $this->get_komponen('slide_show');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }

      $where = array(
              'id_posting' => $this->uri->segment(3),
              'status' => 1
              );
      $d = $this->Posting_model->get_data($where);
      if(!$d)
            {
              $data['tampil_menu'] = '';
              $data['judul_posting'] = '';
              $data['isi_posting'] = '';
              $data['kata_kunci'] = '';
              $data['gambar'] = 'blankgambar.jpg';
            }
          else
            {
              $data['tampil_menu'] = $d['tampil_menu'];
              $data['judul_posting'] = $d['judul_posting'];
              //$data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
              $data['isi_posting'] = ''.$d['isi_posting'].' ';
              $data['kata_kunci'] = $d['kata_kunci'];
              $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
            }
            $where1 = array(
              'domain' => $web
              );
            $this->db->where($where1);
            $query1 = $this->db->get('komponen');
            foreach ($query1->result() as $row1)
              {
                if( $row1->judul_komponen == 'Header' ){ //Header
                  $data['Header'] = $row1->isi_komponen;
                }
                else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
                  $data['KolomKiriAtas'] = $row1->isi_komponen;
                }
                else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
                  $data['KolomKananAtas'] = $row1->isi_komponen;
                }
                else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
                  $data['KolomKiriBawah'] = $row1->isi_komponen;
                }
                else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
                  $data['KolomPalingBawah'] = $row1->isi_komponen;
                }
                else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
                  $data['KolomKananBawah'] = $row1->isi_komponen;
                }
                else{ 
                }
              }
    // $data['menu_atas'] = $this->menu_atas(0, $h = "", $a);
    // $data['menu_kiri'] = $this->menu_kiri(0, $h = "", $a);
    $data['main_view'] = 'peraturan/daftar_peraturan';
    $this->load->view('postings', $data);
  }
   
  private function menu_atas_posting($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
			$hasil .= '
        <ul id="responsivemenu"> <li class="active"><a href="'.base_url().'"><i class="icon-home homeicon"></i><span class="showmobile">Home</span></a></li>'; 
      }
      else{
        $hasil .= '
        <ul id="responsivemenu"> ';
      }
		}
      $nomor = 0;
      foreach($w->result() as $h)
      {
      
      $r = $this->db->query("
      SELECT * from posting
                    
      where parent='".$h->id_posting."' 
      and status = 1 
			and tampil_menu = 1 
      and domain='".$web."'
      order by urut
      ");
      $xx = $r->num_rows();
      $nomor = $nomor + 1;
        if( $a > 1 ){
          $hasil .= '
          <li> <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a>';
        }
        else{
          if ($xx == 0){
          $hasil .= '
            <li> <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.'</a>';
          }
          else{
          $hasil .= '
            <li> <a>'.$h->judul_posting.'</a>';
          } 
        }
        $hasil = $this->menu_atas1($h->id_posting,$hasil, $a);
        $hasil .= '</li>';
      }
		if(($w->num_rows)>0)
		{
			$hasil .= "
      </ul>";
		}
    else{      
    }
		
		return $hasil;
	}
  

  private function menu_atas1($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
			$hasil .= '
<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'" class="fa-home ">HOME</a></li>'; 
      }
      else{
			$hasil .= '
<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
      $nomor = $nomor + 1;
			if( $a > 1 ){
				if($h->judul_posting == 'Pengaduan Masyarakat'){
				$hasil .= '
				<li><a href="'.base_url().'pengaduan_masyarakat">'.$h->judul_posting.'</a>
				';
				}
				elseif($h->judul_posting == 'Permohonan Informasi'){
				$hasil .= '
				<li><a href="'.base_url().'permohonan_informasi_publik">'.$h->judul_posting.'</a>
				';
				}
				elseif($h->judul_posting == 'Pengajuan Keberatan'){
				$hasil .= '
				<li><a href="'.base_url().'pengajuan_keberatan">'.$h->judul_posting.'</a>
				';
				}
				else{
				$hasil .= '
					<li> <a href="'.base_url().'postings/details/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a>
				';
				}
			}
			else{ 
      if ($xx == 0){
      $hasil .= '
<li> <span class="'.$h->icon.' "> <a style="color: #e3e3e3;" href="'.base_url().'postings/categoris/'.$h->id_posting.'/'.str_replace(' ', '_', url_title($h->judul_posting) ).'.HTML">'.$h->judul_posting.'</a> </span>';
      }
      else{
      $hasil .= '
<li class="parent" > <span class="'.$h->icon.' ">'.$h->judul_posting.' </span>';
      } 
        }
      
			$hasil = $this->menu_atas1($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
      
      $z = $this->db->query("
      SELECT * from posting
      where id_posting='".$parent."'
      ");    
      foreach($z->result() as $z1)
      { 
        if($z1->judul_posting == 'PPID' && $nomor == 4){            
        $hasil .= '
          <li>  <a href="#"> Produk Hukum </a>
            <ul>
              <li><a href="'.base_url().'peraturan/daftar_peraturan/?&id_peraturan=7&tentang=Peraturan_Daerah.html">Peraturan Daerah</a></li>
            </ul>
          </li>
        ';
        }
      }
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  
  private function posting($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
    and posisi='menu_kiri'
		and status = 1 
		and domain = '".$web."'
    order by urut asc, created_time desc limit 10
		");
		$x = $w->num_rows();
		if(($w->num_rows())>0)
		{
      if($parent == 0){
				$hasil .= '<ul class="list-group sidebar-nav" id="sidebar-nav'.$parent.'">';
        }
      else{
        }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
    
    $r = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
    and posisi='menu_kiri'
		and domain = '".$web."'
		and status = 1 
    order by urut
    
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
          if ($xx == 0){
            $hasil .= '<li><a style="color:#97299b;" href="'.base_url().'postings/details/'.$h->id_posting.'/'.url_title($h->judul_posting).'">'.$h->judul_posting.' </a>';
            }
          else{
            $hasil .= '<li class="list-group-item list-toggle"><a data-toggle="collapse" data-parent="#sidebar-nav'.$parent.''.$h->id_posting.'" href="#collapse-typography'.$parent.''.$h->id_posting.'" class=""><i class="fa '.$h->icon.'"></i>  '.$h->judul_posting.' </a>';
						$hasil .= '<ul class="list-group sidebar-nav collapse" id="collapse-typography'.$parent.''.$h->id_posting.'">';
            }
        }
			else{ 
          if ($xx == 0){
            $hasil .= '<li><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'">  '.$h->judul_posting.' </a>';
            }
          else{
            $hasil .= '<li class="list-group-item list-toggle"><a data-toggle="collapse" data-parent="#sidebar-nav'.$parent.''.$h->id_posting.'" href="#collapse-typography'.$parent.''.$h->id_posting.'" class=""><i class="fa '.$h->icon.'"></i>  '.$h->judul_posting.' </a>';
						$hasil .= '<ul class="list-group sidebar-nav collapse" id="collapse-typography'.$parent.''.$h->id_posting.'">';
            }
        }
			$hasil = $this->posting($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= '</ul>';
		}
    else{
      
    }
		
		return $hasil;
  }
  
  public function get_komponen($judul_komponen)
   {
    $web=$this->uut->namadomain(base_url());
     $where = array(
						'judul_komponen' => $judul_komponen,
						'status' => 1
						);
    $d = $this->Komponen_model->get_data($where);
    if(!$d)
          {
						return '';
          }
        else
          {
						return $d['isi_komponen'];
          }
   }

  public function details()
  {
    $web = $this->uut->namadomain(base_url());
    $table_name = 'peraturan';
    $where_id = array(
      'peraturan.domain' => $web,
      'peraturan.id_peraturan' => $this->uri->segment(3)
    );
    $data['details_peraturan'] = $this->Peraturan_model->details_peraturan($where_id, $table_name);
    $a = 0;
    $data['total'] = 10;
    $data['menu_atas'] = $this->menu_atas1(0,$h="", $a);
    $data['menu_atas_posting'] = $this->menu_atas_posting(0,$h="", $a);
    $data['menu_kiri'] = $this->posting(0,$h="", $a);
    $data['welcome1'] = $this->get_komponen('welcome1');
    $data['welcome2'] = $this->get_komponen('welcome2');
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $data['slide_show'] = $this->get_komponen('slide_show');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
      
      $where = array(
        'id_posting' => $this->uri->segment(4),
        'status' => 1
        );
    $d = $this->Posting_model->get_data($where);
    if(!$d)
      {
        $data['tampil_menu'] = '';
        $data['judul_posting'] = '';
        $data['isi_posting'] = '';
        $data['kata_kunci'] = '';
        $data['gambar'] = 'blankgambar.jpg';
      }
    else
      {
        $data['tampil_menu'] = $d['tampil_menu'];
        $data['judul_posting'] = $d['judul_posting'];
        //$data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
        $data['isi_posting'] = ''.$d['isi_posting'].' ';
        $data['kata_kunci'] = $d['kata_kunci'];
        $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
      }

      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->judul_komponen == 'Header' ){ //Header
            $data['Header'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
            $data['KolomKiriAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
            $data['KolomKananAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
            $data['KolomKiriBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
            $data['KolomPalingBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
            $data['KolomKananBawah'] = $row1->isi_komponen;
          }
          else{ 
          }
        }
    // $data['menu_atas'] = $this->menu_atas(0, $h = "", $a);
    // $data['menu_kiri'] = $this->menu_kiri(0, $h = "", $a);

    $data['judul'] = 'Selamat datang';
    $data['main_view'] = 'peraturan/details';
    $this->load->view('postings', $data);
  }

private function menu_atas($parent=NULL,$hasil, $a){
  $web=$this->uut->namadomain(base_url());
  $a = $a + 1;
  $w = $this->db->query("
  SELECT * from posting
  where posisi='menu_atas'
  and parent='".$parent."' 
  and status = 1 
  and tampil_menu_atas = 1 
  and domain='".$web."'
  order by urut
  ");
  $x = $w->num_rows();
  if(($w->num_rows())>0)
  {
  if($parent == 0){
  if($web == 'zb.wonosobokab.go.id'){
  $hasil .= '
  <ul id="hornavmenu" class="nav navbar-nav" >
  <li><a href="'.base_url().'website" class="">HOME</a></li>
  '; 
  }
  elseif ($web == 'ppiddemo.wonosobokab.go.id'){
  $hasil .= '
  <ul id="hornavmenu" class="nav navbar-nav" >
  <li><a href="'.base_url().'">HOME</a></li>
  <li class="parent">
    <span class="">PPID </span>
    <ul>
    <li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid">PPID </a></span></li>
    <li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid_pembantu">PPID Pembantu</a></span></li>
    </ul>
  </li>
  ';
  }
  else{
  $hasil .= '
  <ul id="hornavmenu" class="nav navbar-nav" >
  <li><a href="'.base_url().'" class="">BERANDA</a></li>
  '; 
  }
  }
  else{
  $hasil .= '
  <ul>
    ';
    }
    }
    $nomor = 0;
    foreach($w->result() as $h)
    {
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_","","","","","","","","","","","","","");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
    $r = $this->db->query("
    SELECT * from posting
    where parent='".$h->id_posting."' 
    and status = 1 
    and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
    ");
    $xx = $r->num_rows();
    $nomor = $nomor + 1;
    if( $a > 1 ){
    if ($xx == 0){
      if($h->judul_posting == 'Data Rekap Kunjungan Pasien'){
      $hasil .= '
      <li><a href="'.base_url().'grafik/data_rekap_kunjungan_pasien"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'Data Kunjungan Hari Ini'){
      $hasil .= '
      <li><a href="'.base_url().'grafik"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'Artikel'){
      $hasil .= '
      <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'Sitemap'){
      $hasil .= '
      <li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'Informasi Publik'){
      $hasil .= '
      <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'Data Penyakit'){
      $hasil .= '
      <li><a href="'.base_url().'grafik/data_penyakit"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }

      elseif($h->judul_posting == 'Data Pegawai'){
      $hasil .= '
      <li><a href="'.base_url().'grafik/data_pegawai"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }

      elseif($h->judul_posting == 'Pengaduan Masyarakat'){
      $hasil .= '
      <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'Permohonan Informasi Publik'){
      $hasil .= '
      <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'Permohonan Informasi'){
      $hasil .= '
      <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      else{
          $hasil .= '
          <li><a href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
      }
    }
    else{
    $hasil .= '
    <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
    } 
    }
    else{
    if($h->judul_posting == 'Artikel'){
    $hasil .= '
    <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
    ';
    }
    elseif($h->judul_posting == 'Sitemap'){
    $hasil .= '
    <li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
    ';
    }
    elseif($h->judul_posting == 'Informasi Publik'){
    $hasil .= '
    <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
    ';
    }
    elseif($h->judul_posting == 'FAQ'){
    $hasil .= '
    <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
    ';
    }
    elseif($h->judul_posting == 'Info'){
    $hasil .= '
    <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
    ';
    }
    elseif($h->judul_posting == 'Pengaduan Masyarakat'){
      $hasil .= '
      <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
    elseif($h->judul_posting == 'Permohonan Informasi Publik'){
      $hasil .= '
      <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
    elseif($h->judul_posting == 'Permohonan Informasi'){
      $hasil .= '
      <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
    else{
    if ($xx == 0){
      if($h->judul_posting == 'Data Rekap Kunjungan Pasien'){
      $hasil .= '
      <li><a href="http://180.250.150.76/simpus/charts/grafik_1b"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'Data Penyakit'){
      $hasil .= '
      <li><a href="http://180.250.150.76/simpus/charts/grafik_1d"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'Artikel'){
      $hasil .= '
      <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'Sitemap'){
      $hasil .= '
      <li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'Informasi Publik'){
      $hasil .= '
      <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      else{
        $hasil .= '
        <li><a href="'.base_url().'postings/categoris/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
      }
    }
    else{
          $hasil .= '
          <li><a href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
    } 
    }
    }
    $hasil = $this->menu_atas($h->id_posting,$hasil, $a);
    $hasil .= '
    </li>
    ';
      
      $z = $this->db->query("
      SELECT * from posting
      where id_posting='".$parent."'
      ");    
      foreach($z->result() as $z1)
      { 
        if($z1->judul_posting == 'PPID' && $nomor == 4){            
        $hasil .= '
          <li>  <a href="#"> Produk Hukum </a>
            <ul>
              <li><a href="'.base_url().'peraturan/daftar_peraturan/?&id_peraturan=7&tentang=Peraturan_Daerah.html">Peraturan Daerah</a></li>
            </ul>
          </li>
        ';
        }
      }
    }
    if(($w->num_rows)>0)
    {
    $hasil .= "
  </ul>
  ";
  }
  else{
  }
  return $hasil;
}
  private function menu_kiri($parent = NULL, $hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $web = 'jdih.wonosobokab.go.id';
    $a = $a + 1;
    $w = $this->db->query("
		SELECT * from pages
		where id_parent='" . $parent . "' 
    and domain = '" . $web . "'
		and status = 1 
    order by kode_pages asc limit 10
		");
    $x = $w->num_rows();
    if (($w->num_rows()) > 0) {
      if ($parent == 0) {
        $hasil .= '<ul class="list-group sidebar-nav" id="sidebar-nav' . $parent . '">';
      } else {
      }
    }
    $nomor = 0;
    foreach ($w->result() as $h) {

      $r = $this->db->query("
		SELECT * from pages
		where id_parent='" . $h->id_pages . "' 
    and domain = '" . $web . "'
		and status = 1 
    order by kode_pages
    
		");
      $xx = $r->num_rows();

      $nomor = $nomor + 1;
      $nama_pages = $h->nama_pages;
      if ($nama_pages == 'PRODUK HUKUM') {
        $hasil .= '<li class="list-group-item list-toggle"><a data-toggle="collapse" data-parent="#sidebar-nav' . $parent . '' . $h->id_pages . '" href="#collapse-typography' . $parent . '' . $h->id_pages . '" class=""><i class="fa fa-home"></i>  ' . $h->nama_pages . ' </a>';
        $hasil .= '<ul class="list-group" id="collapse-typography' . $parent . '' . $h->id_pages . '" style="height: 225px; display: inline-block; width: 100%; overflow: auto;">';
        $hasil .= '<li class="list-group-item"><a style="color:#97299b;" href="' . base_url() . 'peraturan/daftar_peraturan/?&id_peraturan=7&tentang=Peraturan_Daerah.html"> PERATURAN DAERAH </a></li>';
        $hasil .= '<li class="list-group-item"><a style="color:#97299b;" href="' . base_url() . 'peraturan/daftar_peraturan/?&id_peraturan=8&tentang=Peraturan_Bupati.html"> PERATURAN BUPATI </a></li>';
        $hasil .= '<li class="list-group-item"><a style="color:#97299b;" href="' . base_url() . 'peraturan/daftar_peraturan/?&id_peraturan=592&tentang=Peraturan_Desa.html"> PERATURAN DESA </a></li>';
        $hasil .= '<li class="list-group-item"><a style="color:#97299b;" href="' . base_url() . 'peraturan/daftar_peraturan/?&id_peraturan=6&tentang=Naskah_Akademik.html"> NASKAH AKADEMIK </a></li>';
        $hasil .= '<li class="list-group-item"><a style="color:#97299b;" href="' . base_url() . 'peraturan/daftar_peraturan/?&id_peraturan=321&tentang=Keputusan_DPRD.html"> KEPUTUSAN DPRD </a></li>';
      }
      // $hasil = $this->menu_kiri($h->id_pages,$hasil, $a);
      $hasil .= '</li>';
    }
    if (($w->num_rows) > 0) {
      $hasil .= '</ul>';
    } else {
    }

    return $hasil;
  }
  private function peraturan($parent = 0, $hasil, $start)
  {
    $web = $this->uut->namadomain(base_url());
    $w = $this->db->query("SELECT * from peraturan where id_parent='" . $parent . "' and domain = '" . $web . "' and nomor_peraturan='0' and id_peraturan in (7,8,9,10,11,19,321) order by inserted_time asc");
    if (($w->num_rows()) > 0) {
      $hasil .= '<ul class="sidebar-menu">';
    }
    foreach ($w->result() as $h) {
      $start = $start + 1;
      $nama_peraturan = $h->nama_peraturan;
      $id_peraturan = $h->id_peraturan;
      if ($nama_peraturan == 'Peraturan Daerah') {
        $hasil .= '
        <li class="treeview">
          <a href="' . base_url() . 'peraturan/daftar_peraturan/?tentang=' . $nama_peraturan . '&id_peraturan=' . $id_peraturan . '">
            <span>
            ' . $nama_peraturan . '
            </span>
          <span class="label label-primary pull-right">New</span>
          </a>
        ';
      } else if ($nama_peraturan == 'Peraturan Bupati') {
        $hasil .= '
        <li class="treeview">
          <a href="' . base_url() . 'peraturan/daftar_peraturan/?tentang=' . $nama_peraturan . '&id_peraturan=' . $id_peraturan . '">
            <span>
            ' . $nama_peraturan . '
            </span>
          <span class="label label-primary pull-right">New</span>
          </a>
        ';
      } else if ($nama_peraturan == 'Keputusan Bupati') {
        $hasil .= '
        <li class="treeview">
          <a href="' . base_url() . 'peraturan/daftar_peraturan/?tentang=' . $nama_peraturan . '&id_peraturan=' . $id_peraturan . '">
            <span>
            ' . $nama_peraturan . '
            </span>
          </a>
        ';
      } else if ($nama_peraturan == 'Intruksi Bupati') {
        $hasil .= '
        <li class="treeview">
          <a href="' . base_url() . 'peraturan/daftar_peraturan/?tentang=' . $nama_peraturan . '&id_peraturan=' . $id_peraturan . '">
            <span>
            ' . $nama_peraturan . '
            </span>
          </a>
        ';
      } else if ($nama_peraturan == 'Raperda') {
        $hasil .= '
        <li class="treeview">
          <a href="' . base_url() . 'peraturan/daftar_peraturan/?tentang=' . $nama_peraturan . '&id_peraturan=' . $id_peraturan . '">
            <span>
            ' . $nama_peraturan . '
            </span>
          </a>
        ';
      } else if ($nama_peraturan == 'E-book') {
        $hasil .= '
        <li class="treeview">
          <a href="' . base_url() . 'peraturan/daftar_peraturan/?tentang=' . $nama_peraturan . '&id_peraturan=' . $id_peraturan . '">
            <span>
            ' . $nama_peraturan . '
            </span>
          </a>
        ';
      } else if ($nama_peraturan == 'Keputusan DPRD') {
        $hasil .= '
        <li class="treeview">
          <a href="' . base_url() . 'peraturan/daftar_peraturan/?tentang=' . $nama_peraturan . '&id_peraturan=' . $id_peraturan . '">
            <span>
            ' . $nama_peraturan . '
            </span>
          <span class="label label-primary pull-right">New</span>
          </a>
        ';
      } else {
        $hasil .= '
        <li class="treeview">
          <a href="http://www.jdihn.bphn.go.id/?page=peraturan&section=produk_hukum&act=jdih" target="_blank">
            <span>
            ' . $nama_peraturan . '
            </span>
          </a>
        ';
      }
      $hasil = $this->peraturan($id_peraturan, $hasil, $start);
      $hasil .= ' </li>';
    }
    if (($w->num_rows()) > 0) {
      $hasil .= "</ul>";
    }
    return $hasil;
  }
  private function jenis_peraturan($parent = 0, $hasil, $start)
  {
    $web = $this->uut->namadomain(base_url());
    $w = $this->db->query("SELECT * from peraturan where id_parent='" . $parent . "' and domain = '" . $web . "' and nomor_peraturan='0' and id_peraturan in (7,8,9,10,11,19) order by inserted_time asc ");
    if (($w->num_rows()) > 0) {
      $hasil .= '<select class="form-control" id="jenis_peraturan">';
    }
    foreach ($w->result() as $h) {
      $start = $start + 1;
      $id_peraturan = $h->id_peraturan;
      $hasil .= '
      <option value="' . $id_peraturan . '">
          ' . $h->nama_peraturan . '
      ';
      $hasil = $this->jenis_peraturan($id_peraturan, $hasil, $start);
      $hasil .= ' </option>';
    }
    if (($w->num_rows()) > 0) {
      $hasil .= "</select>";
    }
    return $hasil;
  }
  private function opsi_parent_jenis_peraturan($parent = 0, $hasil, $start)
  {
    $w = $this->db->query("SELECT * from peraturan where id_parent='" . $parent . "' and nomor_peraturan='0' and tipe_peraturan='opd' order by inserted_time asc ");
    if (($w->num_rows()) > 0) {
      $hasil .= '<select class="form-control" id="id_parent">';
    }
    foreach ($w->result() as $h) {
      $start = $start + 1;
      $id_peraturan = $h->id_peraturan;
      $hasil .= '
      <option value="' . $id_peraturan . '" selected="selected">
          ' . $h->nama_peraturan . '
      ';
      $hasil = $this->opsi_parent_jenis_peraturan($id_peraturan, $hasil, $start);
      $hasil .= ' </option>';
    }
    if (($w->num_rows()) > 0) {
      $hasil .= "</select>";
    }
    return $hasil;
  }
  private function e_opsi_parent_jenis_peraturan($parent = 0, $hasil, $start)
  {
    $w = $this->db->query("SELECT * from peraturan where id_parent='" . $parent . "' and nomor_peraturan='0' and tipe_peraturan='opd' order by inserted_time asc ");
    if (($w->num_rows()) > 0) {
      $hasil .= '<select class="form-control" id="e_id_parent">';
    }
    foreach ($w->result() as $h) {
      $start = $start + 1;
      $id_peraturan = $h->id_peraturan;
      $hasil .= '
      <option value="' . $id_peraturan . '" selected="selected">
          ' . $h->nama_peraturan . '
      ';
      $hasil = $this->e_opsi_parent_jenis_peraturan($id_peraturan, $hasil, $start);
      $hasil .= ' </option>';
    }
    if (($w->num_rows()) > 0) {
      $hasil .= "</select>";
    }
    return $hasil;
  }

  public function views()
  {
    $web = $this->uut->namadomain(base_url());
    $table_name = 'peraturan';
    $where_id = array(
      'peraturan.domain' => $web,
      'peraturan.id_peraturan' => $this->uri->segment(3)
    );
    $fields     = "
				
        *,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Isi Peraturan') as attachment_isi_peraturan,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Katalog') as attachment_katalog,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Abstrak') as attachment_abstrak,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='NA') as attachment_na,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='MOU') as attachment_mou,
								
				";
    $data['detail_peraturan'] = $this->Peraturan_model->detail_peraturan($where_id, $fields, $table_name);
    $start = 0;
    $data['peraturan'] = $this->peraturan(0, $h = "", $start);
    $data['jenis_peraturan'] = $this->jenis_peraturan(0, $h = "", $start);
    $data['main_view'] = 'peraturan/view';
    $this->load->view('back_bone', $data);
  }
}
