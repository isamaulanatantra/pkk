<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Template_role extends CI_Controller
{
  
  function __construct()
  {
    parent::__construct();
  }
  
  public function index()
  {
    $data['test'] = '';
    $this->load->view('back_end/template_role/content', $data);
  }
  
  public function json()
  {
    $table       = $this->input->post('table');
    $page        = $this->input->post('page');
    $limit       = $this->input->post('limit');
    $keyword     = $this->input->post('keyword');
    $status      = $this->input->post('status');
    $order_by    = $this->input->post('order_by');
    $order_field = $this->input->post('order_field');
    $shorting    = $this->input->post('shorting');
    $start       = ($page - 1) * $limit;
    $fields      = "
            template_role.template_role_id,
            template_role.template_id,
            template_role.id_dasar_website,
            template_role.information,
            template_role.created_by,
            template_role.created_time,
            template_role.updated_by,
            template_role.updated_time,
            template_role.deleted_by,
            template_role.deleted_time,
            template_role.status,
            
            template.template_code,
            template.template_name,
            
            dasar_website.keterangan,
            dasar_website.domain,
            
            (
            select attachment.file_name from attachment
            where attachment.id_tabel=template_role.template_role_id
            and attachment.table_name='" . $table . "'
            order by attachment.id_attachment DESC
            limit 1
            ) as file
            ";
    $where       = array(
      '' . $table . '.status' => $status
    );
    $this->db->select("$fields");
    if ($keyword == '') {
      $this->db->where("
            " . $table . ".status = " . $status . "
            ");
    } else {
      $this->db->where("
            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'
            AND
            " . $table . ".status = " . $status . "
            ");
    }
    $this->db->join('template', 'template.template_id=template_role.template_id');
    $this->db->join('dasar_website', 'dasar_website.id_dasar_website=template_role.id_dasar_website');
    $this->db->order_by($order_by, $shorting);
    $this->db->limit($limit, $start);
    $result = $this->db->get($table);
    echo json_encode($result->result_array());
  }
  
  public function total()
  {
    $table       = $this->input->post('table');
    $page        = $this->input->post('page');
    $limit       = $this->input->post('limit');
    $keyword     = $this->input->post('keyword');
    $status      = $this->input->post('status');
    $order_by    = $this->input->post('order_by');
    $shorting    = $this->input->post('shorting');
    $order_field = $this->input->post('order_field');
    $start       = ($page - 1) * $limit;
    $fields      = "
            template_role.template_role_id,
            template_role.template_id,
            template_role.id_dasar_website,
            template_role.information,
            template_role.created_by,
            template_role.created_time,
            template_role.updated_by,
            template_role.updated_time,
            template_role.deleted_by,
            template_role.deleted_time,
            template_role.status
            ";
    $where       = array(
      '' . $table . '.status' => $status
    );
    $this->db->select("$fields");
    if ($keyword == '') {
      $this->db->where("
            " . $table . ".status = " . $status . "
            ");
    } else {
      $this->db->where("
            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'
            AND
            " . $table . ".status = " . $status . "
            ");
    }
    $this->db->join('template', 'template.template_id=template_role.template_id');
    $this->db->join('dasar_website', 'dasar_website.id_dasar_website=template_role.id_dasar_website');
    $query = $this->db->get($table);
    $a     = $query->num_rows();
    echo trim(ceil($a / $limit));
  }
  
  public function simpan()
  {
    $this->form_validation->set_rules('template_id', 'template_id', 'required');
    $this->form_validation->set_rules('temp', 'temp', 'required');
    $this->form_validation->set_rules('id_dasar_website', 'id_dasar_website', 'required');
    $this->form_validation->set_rules('information', 'information', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $fields      = "*";
      $this->db->where("
              template_id='".trim($this->input->post('template_id'))."'
              AND
              id_dasar_website='".trim($this->input->post('id_dasar_website'))."'
              ");
      $query = $this->db->get('template_role');
      $a     = $query->num_rows();
      if( $a > 0  ){
      echo 0;
      exit;
      }
      $data_input = array(
        'template_id' => trim($this->input->post('template_id')),
        'id_dasar_website' => trim($this->input->post('id_dasar_website')),
        'information' => trim($this->input->post('information')),
        'temp' => trim($this->input->post('temp')),
        'created_by' => $this->session->userdata('user_id'),
        'created_time' => date('Y-m-d H:i:s'),
        'status' => 1
      );
      $this->db->insert( 'template_role', $data_input );
      $id= $this->db->insert_id();
      echo '' . $id . '';
      $where       = array(
        'table_name' => 'template_role',
        'temp' => trim($this->input->post('temp'))
      );
      $data_update = array(
        'id_tabel' => $id
      );
      $this->db->where($where);
      $this->db->update('attachment', $data_update);
    }
  }
  
  public function update()
  {
    $this->form_validation->set_rules('template_role_id', 'template_role_id', 'required');
    $this->form_validation->set_rules('template_id', 'template_id', 'required');
    $this->form_validation->set_rules('id_dasar_website', 'id_dasar_website', 'required');
    $this->form_validation->set_rules('information', 'information', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'template_id' => trim($this->input->post('template_id')),
        'id_dasar_website' => trim($this->input->post('id_dasar_website')),
        'information' => trim($this->input->post('information')),
        'updated_by' => $this->session->userdata('user_id'),
        'updated_time' => date('Y-m-d H:i:s')
      );
      $where       = array(
        'template_role_id' => trim($this->input->post('template_role_id'))
      );
      $this->db->where($where);
      $this->db->update('template_role', $data_update);
      echo 1;
    }
  }
  
  public function get_by_id()
  {
    $template_role_id = $this->input->post('template_role_id');
    $where   = array(
      'template_role.template_role_id' => $template_role_id
    );
    $this->db->select("
      template_role.template_role_id,
      template_role.template_id,
      template_role.id_dasar_website,
      template_role.information,
      template_role.created_by,
      template_role.created_time,
      template_role.updated_by,
      template_role.updated_time,
      template_role.deleted_by,
      template_role.deleted_time,
      template_role.status,
      
      template.template_code,
      template.template_name,
      
      dasar_website.keterangan,
      dasar_website.domain
      ");    	
    $this->db->where($where);
    $this->db->join('template', 'template.template_id=template_role.template_id');
    $this->db->join('dasar_website', 'dasar_website.id_dasar_website=template_role.id_dasar_website');
		$result = $this->db->get('template_role');
    echo json_encode($result->result_array());
  }
  
  public function delete_by_id()
  {
    $this->form_validation->set_rules('template_role_id', 'template_role_id', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 99
      );
      $where       = array(
        'template_role_id' => trim($this->input->post('template_role_id'))
      );
      $table_name  = 'template_role';
      $this->db->where($where);
      $this->db->update('template_role', $data_update);
      echo 1;
    }
  }
  
  public function active_by_id()
  {
    $this->form_validation->set_rules('template_role_id', 'template_role_id', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 1
      );
      $where       = array(
        'template_role_id' => trim($this->input->post('template_role_id'))
      );
      $table_name  = 'template_role';
      $this->db->where($where);
      $this->db->update('template_role', $data_update);
      echo 1;
    }
  }
  
  public function inactive_by_id()
  {
    $this->form_validation->set_rules('template_role_id', 'template_role_id', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 0
      );
      $where       = array(
        'template_role_id' => trim($this->input->post('template_role_id'))
      );
      $table_name  = 'template_role';
      $this->db->where($where);
      $this->db->update('template_role', $data_update);
      echo 1;
    }
  }
  
  public function restore_by_id()
  {
    $this->form_validation->set_rules('template_role_id', 'template_role_id', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 1
      );
      $where       = array(
        'template_role_id' => trim($this->input->post('template_role_id'))
      );
      $table_name  = 'template_role';
      $this->db->where($where);
      $this->db->update($table_name, $data_update);
      echo 1;
    }
  }
  
  
  public function dasar_website_by_template_id()
  {
    $fields      = "
            template_role.template_role_id,
            template_role.template_id,
            template_role.id_dasar_website,
            template_role.information,
            template_role.created_by,
            template_role.created_time,
            template_role.updated_by,
            template_role.updated_time,
            template_role.deleted_by,
            template_role.deleted_time,
            template_role.status,
            
            template.template_code,
            template.template_name,
            
            dasar_website.keterangan,
            dasar_website.domain
            
            ";
    $where       = array(
      'template_role.template_id' => $this->input->post('template_id')
    );
    $this->db->select("$fields");
    $this->db->where($where);
    $this->db->join('template', 'template.template_id=template_role.template_id');
    $this->db->join('dasar_website', 'dasar_website.id_dasar_website=template_role.id_dasar_website');
    $this->db->order_by('template_role.template_id');
    $this->db->order_by('template_role.id_dasar_website');
    $this->db->order_by('dasar_website.domain');
    $result = $this->db->get('template_role');
    echo json_encode($result->result_array());
  }
  
}