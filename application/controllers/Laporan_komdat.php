<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Laporan_komdat extends CI_Controller
 {
    
  function __construct(){
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->library('Crud_model');
    $this->load->model('Laporan_komdat_model');
   }
  
  public function index(){
    $data['total'] = 10;
    $data['main_view'] = 'laporan_komdat/home';
    $this->load->view('tes', $data);
   }  
  
   
  function load_table(){
    $web=$this->uut->namadomain(base_url());
    $tahun = $this->input->post('tahun');
    $bulan = $this->input->post('bulan');

	$w = $this->db->query("SELECT * from tr_komdat 
									left join ms_jenis_komdat ON msJenisKomdatId = trKomdatMsJenisId
									left join ms_komdat ON msKomdatId = trKomdatMsKomdatId
									where  trKomdatThn = ".$tahun."
											AND trKomdatBln = ".$bulan."
									ORDER BY trKomdatMsJenisId
						");
	if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		$no_urut = 0;
		foreach($w->result() as $h)
		{
			$no_urut++;
			echo '				
				<tr id_komdat="'.$h->trKomdatId.'" id="'.$h->trKomdatId.'">					
					
					<td style="padding: 2px;">
						<p>'.$no_urut.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->msJenisKomdatNama.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->msKomdatNama.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->trKomdatThn.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->trKomdatBln.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->trKomdatJumlah.'</p>
					</td>					
				</tr>
				</tr>
			';
		}
	} 

	function load_form_komdat(){
    $web=$this->uut->namadomain(base_url()); 
    $jenis_laporan = $this->input->post('jenis_laporan');  

	$w = $this->db->query("SELECT * from ms_komdat
									where  msKomdatJenis = ".$jenis_laporan."
							");
	if(($w->num_rows())>0){
	}
  	$a=0;
	echo $jumlah_komentar = $w->num_rows();
	$no_urut = 0;
	foreach($w->result() as $h)
		{
			$no_urut++;
			echo '
				<tr>	
					<td align=center style="padding: 2px;">
						<p>'.$no_urut.'</p>
					</td>	

					<td align=center style="padding: 2px; display:none;">
						<input class="form-control" id="id[]" name="id[]" value="'.$h->msKomdatId.'" placeholder="0" type="text">
					</td>

					<td align=center style="padding: 2px; display:none;">
						<input class="form-control" id="jenis[]" name="jenis[]" value="'.$h->msKomdatJenis.'" placeholder="0" type="text">
					</td>
					<td style="padding: 2px;">
						<p name="nama[]">'.$h->msKomdatNama.'</p>
						
					</td>
					
					<td style="padding: 2px;">
						<input class="form-control" id="jumlah[]" name="jumlah[]" value="" placeholder="" type="number">
					</td>										
				</tr>
			';
		}
	}   

	
	function load_form_komdat_tahunan(){
    $web=$this->uut->namadomain(base_url()); 
    $jenis_laporan_tahunan = $this->input->post('jenis_laporan_tahunan');  

	$w = $this->db->query("SELECT * from ms_komdat
									where  msKomdatJenis = 6 and msKomdatType = ".$jenis_laporan_tahunan."
							");
	if(($w->num_rows())>0){
	}
  	$a=0;
	echo $jumlah_komentar = $w->num_rows();
	$no_urut = 0;
	foreach($w->result() as $h)
		{
			$no_urut++;
			echo '
				<tr>	
					<td align=center style="padding: 2px;">
						<p>'.$no_urut.'</p>
					</td>	

					<td align=center style="padding: 2px; display:none;">
						<input class="form-control" id_tahunan="id[]" name="id_tahunan[]" value="'.$h->msKomdatId.'" placeholder="0" type="text">
					</td>

					<td align=center style="padding: 2px; display:none;">
						<input class="form-control" id="jenis_tahunan[]" name="jenis_tahunan[]" value="'.$h->msKomdatJenis.'" placeholder="0" type="text">
					</td>
					<td style="padding: 2px;">
						<p name="nama_tahunan[]">'.$h->msKomdatNama.'</p>
						
					</td>
					
					<td style="padding: 2px;">
						<input class="form-control" id="jumlah_tahunan[]" name="jumlah_tahunan[]" value="" placeholder="" type="number">
					</td>										
				</tr>
			';
		}
	}   

	public function save(){
		
		$jenis = $_POST['jenis'];
		$id = $_POST['id']; 
		$tahun = $_POST['thn_komdat_form']; 
		$bulan = $_POST['bln_komdat_form']; 
		$jumlah = $_POST['jumlah']; 		

		if ($jumlah=='') {
			$jumlah = 0;
		}

		$data = array();

		$index = 0; 

		$w = $this->db->query("SELECT * from tr_komdat 
                          where trKomdatMsJenisId = '".$jenis[$index]."'
                          AND trKomdatThn = '".$tahun."'
                          AND trKomdatBln = '".$bulan."'
                          ");

		 if($w->num_rows()>0){ // Jika sukses
	      //echo "<script>alert('Data sudah ada, silahkan cek tahun dan bulan');history.back(self);</script>";
		 	$data['hasil'] = 'dobel';
		 	echo json_encode($data);
		 	
	    }else{ // Jika gagal
		      foreach($id as $id){ 

			  array_push($data, array(
			    'trKomdatMsJenisId'=>$jenis[$index],
			    'trKomdatMsKomdatId'=>$id,  
			    'trKomdatThn'=>$tahun, 
			    'trKomdatBln'=>$bulan, 
			    'trKomdatJumlah'=>$jumlah[$index],
			  ));
			  
			  $index++;
			}

			$sql = $this->Laporan_komdat_model->save_data($data); // Panggil fungsi save_batch yang ada di model siswa (SiswaModel.php)

			if($sql){ // Jika sukses
		      	$data['hasil'] = 'sukses';
		 		echo json_encode($data);		 		
		    }else{ // Jika gagal
		      // echo "<script>alert('Data gagal disimpan');window.location = '".base_url('index.php/laporan_komdat')."';</script>";
		    	$data['hasil'] = 'gagal';
		 		echo json_encode($data);
			}
		}		
	}

	public function save_tahunan(){
		
		$jenis = $_POST['jenis_tahunan'];
		$id = $_POST['id_tahunan']; 
		$tahun = $_POST['thn_komdat_form_tahunan']; 
		$bulan = 12; 
		$jumlah = $_POST['jumlah_tahunan']; 

		if ($jumlah=='') {
			$jumlah = 0;
		}

		$data = array();

		$index = 0; 

		$w = $this->db->query("SELECT * from tr_komdat 
                          where trKomdatMsJenisId = '".$jenis[$index]."'
                          AND trKomdatThn = '".$tahun."'
                          ");

		 if($w->num_rows()>0){ // Jika sukses
	      //echo "<script>alert('Data sudah ada, silahkan cek tahun dan bulan');history.back(self);</script>";
		 	$data['hasil'] = 'dobel';
		 	echo json_encode($data);
		 	
	    }else{ // Jika gagal
		      foreach($id as $id){ 

			  array_push($data, array(
			    'trKomdatMsJenisId'=>$jenis[$index],
			    'trKomdatMsKomdatId'=>$id,  
			    'trKomdatThn'=>$tahun, 
			    'trKomdatBln'=>$bulan, 
			    'trKomdatJumlah'=>$jumlah[$index],
			  ));
			  
			  $index++;
			}

			$sql = $this->Laporan_komdat_model->save_data($data); // Panggil fungsi save_batch yang ada di model siswa (SiswaModel.php)

			if($sql){ // Jika sukses
		      	$data['hasil'] = 'sukses';
		 		echo json_encode($data);		 		
		    }else{ // Jika gagal
		      // echo "<script>alert('Data gagal disimpan');window.location = '".base_url('index.php/laporan_komdat')."';</script>";
		    	$data['hasil'] = 'gagal';
		 		echo json_encode($data);
			}
		}	
	}

	function getJumlah($id, $tahun, $bulan){
		 $where = array(
		      'trKomdatMsKomdatId' => $id,
		      'trKomdatThn' => $tahun,
		      'trKomdatBln ' => $bulan
		      );
		 	$this->db->select('trKomdatJumlah');
		    $this->db->where($where);		    
		    $query = $this->db->get('tr_komdat');

		    foreach ($query->result() as $hsl) {
		    	$value = $hsl->trKomdatJumlah;
		    }

		    if ($query->num_rows() > 0) {
		    	$cetak = $value;
		    }else{
		    	$cetak = '~';
		    }

			return $cetak;

	}

	function load_kia(){
    $web=$this->uut->namadomain(base_url());
    $tahun = $this->input->post('tahun');
    $jan = 1;
    //$bulan = $this->input->post('bulan');   


	$w = $this->db->query("SELECT DISTINCT(trKomdatMsKomdatId), msJenisKomdatNama, msKomdatNama, trKomdatMsJenisId, trKomdatMsKomdatId
									from tr_komdat 
									left join ms_jenis_komdat ON msJenisKomdatId = trKomdatMsJenisId
									left join ms_komdat ON msKomdatId = trKomdatMsKomdatId
									where  trKomdatThn = ".$tahun."
									and trKomdatMsJenisId = '1'
									ORDER BY trKomdatMsJenisId, trKomdatMsKomdatId
						");
	if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		$no_urut = 0;
		echo "<tr bgcolor='#ff9999'><td align='center'>A.</td>
					<td colspan='13'>KESEHATAN IBU DAN ANAK</td>
			  </tr>";
		foreach($w->result() as $h)
		{
			$no_urut++;		

			echo '				
				<tr id_komdat="'.$h->trKomdatId.'" id="'.$h->trKomdatId.'">					
					
					<td style="padding: 2px;" align="center">
						<p>'.$no_urut.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->msKomdatNama.'</p>
					</td>					
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId,  $tahun, 1)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 2)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 3)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 4)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 5)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 6)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 7)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 8)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 9)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 10)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 11)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 12)).'</p>
					</td>					
				</tr>
			';
		}
	} 

	function load_gizi(){
    $web=$this->uut->namadomain(base_url());
    $tahun = $this->input->post('tahun');
    $jan = 1;
    //$bulan = $this->input->post('bulan');   


	$w = $this->db->query("SELECT DISTINCT(trKomdatMsKomdatId), msJenisKomdatNama, msKomdatNama, trKomdatMsJenisId, trKomdatMsKomdatId
									from tr_komdat 
									left join ms_jenis_komdat ON msJenisKomdatId = trKomdatMsJenisId
									left join ms_komdat ON msKomdatId = trKomdatMsKomdatId
									where  trKomdatThn = ".$tahun."
									and trKomdatMsJenisId = '2'
									ORDER BY trKomdatMsJenisId, trKomdatMsKomdatId
						");
	if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		$no_urut = 0;
		echo "<tr bgcolor='#ff9999'>
					<td align='center'>B.</td>
					<td colspan='13'>GIZI</td>
			  </tr>";
		foreach($w->result() as $h)
		{
			$no_urut++;		

			echo '				
				<tr id_komdat="'.$h->trKomdatId.'" id="'.$h->trKomdatId.'">					
					
					<td style="padding: 2px;" align="center">
						<p>'.$no_urut.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->msKomdatNama.'</p>
					</td>					
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 1)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 2)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 3)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 4)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 5)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 6)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 7)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 8)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 9)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 10)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 11)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 12)).'</p>
					</td>					
				</tr>
			';
		}
	} 

	function load_imunisasi(){
    $web=$this->uut->namadomain(base_url());
    $tahun = $this->input->post('tahun');
    $jan = 1;
    //$bulan = $this->input->post('bulan');   


	$w = $this->db->query("SELECT DISTINCT(trKomdatMsKomdatId), msJenisKomdatNama, msKomdatNama, trKomdatMsJenisId, trKomdatMsKomdatId
									from tr_komdat 
									left join ms_jenis_komdat ON msJenisKomdatId = trKomdatMsJenisId
									left join ms_komdat ON msKomdatId = trKomdatMsKomdatId
									where  trKomdatThn = ".$tahun."
									and trKomdatMsJenisId = '3'
									ORDER BY trKomdatMsJenisId, trKomdatMsKomdatId
						");
	if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		$no_urut = 0;
		echo "<tr bgcolor='#ff9999'>
					<td align='center'>C.</td>
					<td colspan='13'>IMUNISASI</td>
			  </tr>";
		foreach($w->result() as $h)
		{
			$no_urut++;		

			echo '				
				<tr id_komdat="'.$h->trKomdatId.'" id="'.$h->trKomdatId.'">					
					
					<td style="padding: 2px;" align="center">
						<p>'.$no_urut.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->msKomdatNama.'</p>
					</td>					
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 1)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 2)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 3)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 4)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 5)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 6)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 7)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 8)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 9)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 10)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 11)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 12)).'</p>
					</td>					
				</tr>
			';
		}
	} 

	function load_penyakit(){
    $web=$this->uut->namadomain(base_url());
    $tahun = $this->input->post('tahun');
    $jan = 1;
    //$bulan = $this->input->post('bulan');   


	$w = $this->db->query("SELECT DISTINCT(trKomdatMsKomdatId), msJenisKomdatNama, msKomdatNama, trKomdatMsJenisId, trKomdatMsKomdatId
									from tr_komdat 
									left join ms_jenis_komdat ON msJenisKomdatId = trKomdatMsJenisId
									left join ms_komdat ON msKomdatId = trKomdatMsKomdatId
									where  trKomdatThn = ".$tahun."
									and trKomdatMsJenisId = '4'
									ORDER BY trKomdatMsJenisId, trKomdatMsKomdatId
						");
	if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		$no_urut = 0;
		echo "<tr bgcolor='#ff9999'>
					<td align='center'>D.</td>
					<td colspan='13'>PENYAKIT</td>
			  </tr>";
		foreach($w->result() as $h)
		{
			$no_urut++;		

			echo '				
				<tr id_komdat="'.$h->trKomdatId.'" id="'.$h->trKomdatId.'">					
					
					<td style="padding: 2px;" align="center">
						<p>'.$no_urut.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->msKomdatNama.'</p>
					</td>					
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 1)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 2)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 3)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 4)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 5)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 6)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 7)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 8)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 9)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 10)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 11)).'</p>
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 12)).'</p>
					</td>					
				</tr>
			';
		}
	} 

	function load_triwulan(){
    $web=$this->uut->namadomain(base_url());
    $tahun = $this->input->post('tahun');
    $jan = 1;
    //$bulan = $this->input->post('bulan');   


	$w = $this->db->query("SELECT DISTINCT(trKomdatMsKomdatId), msJenisKomdatNama, msKomdatNama, trKomdatMsJenisId, trKomdatMsKomdatId
									from tr_komdat 
									left join ms_jenis_komdat ON msJenisKomdatId = trKomdatMsJenisId
									left join ms_komdat ON msKomdatId = trKomdatMsKomdatId
									where  trKomdatThn = ".$tahun."
									and trKomdatMsJenisId = '5'
									ORDER BY trKomdatMsJenisId, trKomdatMsKomdatId
						");
	if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		$no_urut = 0;
		echo "<tr bgcolor='#ff9999'>
					<td align='center'></td>
					<td colspan='13'>TRIWULAN</td>
			  </tr>";
		foreach($w->result() as $h)
		{
			$no_urut++;		

			echo '				
				<tr id_komdat="'.$h->trKomdatId.'" id="'.$h->trKomdatId.'">					
					
					<td style="padding: 2px;" align="center">
						<p>'.$no_urut.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->msKomdatNama.'</p>
					</td>					
					<td style="padding: 2px;" align="center" bgcolor="bdbdbd">
						
					</td>
					<td style="padding: 2px;" align="center" bgcolor="bdbdbd">
						
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 3)).'</p>
					</td>
					<td style="padding: 2px;" align="center" bgcolor="bdbdbd">
						
					</td>
					<td style="padding: 2px;" align="center" bgcolor="bdbdbd">
						
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 6)).'</p>
					</td>
					<td style="padding: 2px;" align="center" bgcolor="bdbdbd">
						
					</td>
					<td style="padding: 2px;" align="center" bgcolor="bdbdbd">
						
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 9)).'</p>
					</td>
					<td style="padding: 2px;" align="center" bgcolor="bdbdbd">
						
					</td>
					<td style="padding: 2px;" align="center" bgcolor="bdbdbd">
						
					</td>
					<td style="padding: 2px;" align="center">
						<p>'.number_format($this->getJumlah($h->trKomdatMsKomdatId, $tahun, 12)).'</p>
					</td>					
				</tr>
			';
		}
	} 


	function load_edit_komdat(){
    $web=$this->uut->namadomain(base_url());
    $jenis = $this->input->post('jenis');
    $tahun = $this->input->post('tahun');
    $bulan = $this->input->post('bulan');

	$w = $this->db->query("SELECT * from tr_komdat 
									left join ms_jenis_komdat ON msJenisKomdatId = trKomdatMsJenisId
									left join ms_komdat ON msKomdatId = trKomdatMsKomdatId
									where  trKomdatThn = ".$tahun."
											AND trKomdatBln = ".$bulan."
											AND trKomdatMsJenisId = ".$jenis."
									ORDER BY trKomdatMsJenisId
						");
	if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		$no_urut = 0;
		foreach($w->result() as $h)
		{
			$no_urut++;
			echo '				
				<tr>	
					<td align=center style="padding: 2px;">
						<p>'.$no_urut.'</p>
					</td>	

					<td align=center style="padding: 2px; display:none;">
						<input class="form-control" id="id_tahunan[]" name="id_tahunan[]" value="'.$h->msKomdatId.'" placeholder="0" type="text">
					</td>

					<td align=center style="padding: 2px; display:none;">
						<input class="form-control" id="id_edit[]" name="id_edit[]" value="'.$h->trKomdatId.'" placeholder="0" type="text">
					</td>

					<td align=center style="padding: 2px; display:none;">
						<input class="form-control" id="jenis_edit[]" name="jenis_edit[]" value="'.$h->msKomdatJenis.'" placeholder="0" type="text">
					</td>
					<td style="padding: 2px;">
						<p name="nama_tahunan[]">'.$h->msKomdatNama.'</p>
						
					</td>
					
					<td style="padding: 2px;">
						<input class="form-control" id="jumlah_edit[]" name="jumlah_edit[]" value="'.$h->trKomdatJumlah.'" placeholder="" type="number">
					</td>										
				</tr>
			';
		}
	} 

	public function update_edit_komdat(){
		
		$id_edit = $_POST['id_edit'];
		$jenis = $_POST['jenis_edit'];
		$id = $_POST['id_edit']; 
		$tahun = $_POST['thn_edit']; 
		$bulan = $_POST['bln_edit']; 
		$jumlah = $_POST['jumlah_edit']; 		

		if ($jumlah=='') {
			$jumlah = 0;
		}

		$data = array();

		$index = 0; 

		
			// foreach($id as $id){ 				 

			//   array_push($data, array(
			//   	'trKomdatId'=>$id_edit[$index],
			//     'trKomdatJumlah'=>$jumlah[$index],
			//   ));
			  
			//   $index++;
			// }

			foreach($id as $id){
			  $data[] = array(
			   'trKomdatId'=>$id_edit[$index],
			    'trKomdatJumlah'=>$jumlah[$index],
			  );
			   $index++;
			}

			//$sql = $this->Laporan_komdat_model->update_data($data); // Panggil fungsi save_batch yang ada di model siswa (SiswaModel.php)

			$sql = $this->db->update_batch('trKomdat',$data,'trKomdatId');

			if($sql){ // Jika sukses
		      	$data['hasil'] = 'sukses';
		 		echo json_encode($data);		 		
		    }else{ // Jika gagal
		      // echo "<script>alert('Data gagal disimpan');window.location = '".base_url('index.php/laporan_komdat')."';</script>";
		    	$data['hasil'] = 'gagal';
		 		echo json_encode($data);
			}
				
	}

 }