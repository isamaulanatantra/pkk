<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rest extends CI_Controller {

  function __construct()
  {
    parent::__construct();
    $this->load->library('Uut');
  }


  function index()
  {
        //Get Url Request
        //$keterangan = $this->uri->segment(2);
        $keterangan = $_GET['data'];
        $id = $this->uri->segment(3);
        //$Xconsid  = $this->input->request_header();
                //print_r $Xconsid;
        //$method = $_SERVER['REQUEST_METHOD']; 
        $method = 'GET'; 
        //Cek
        if($keterangan == ''){
            echo json_encode(array('status' => 400,'message' => 'Bad request.'));
        } else {
            if($method == 'GET'){
                $response = $this->check_auth_client();
                $id_client = $response['id_client'];
                if($response['status'] == 200){
                    if($keterangan=="ApiHargaPokok"){
                        //INSERT api_log
                        $time = date("Y-m-d H:i:s");
                        $tanggal = date("2018-11-26");
                        $data_input= array(
                            'api_log.client' => $id_client,
                            'api_log.api_name' => 'Api Harga Kebutuhan Pokok',
                            'api_log.datetime_request' => $time,
                            'api_log.status' => 1
                        ); 
                        $this->db->insert('api_log', $data_input );
                        //GET request data
                        $this->db->select('
                            judul_komoditi,
                            (select harga from harga_kebutuhan_pokok where id_komoditi = komoditi.id_komoditi and tanggal="'.$tanggal.'" and id_pasar=1 ) as harga
                                                        ');
                        $result = $this->db->get('komoditi');
                        echo json_encode($result->result_array());
                        //echo json_encode(array('status' => 200, 'detail' =>'Data Ditemukan','data' => $result->result_array()));
                    }else{
                        echo json_encode(array('status' => 404, 'detail' =>'Segment yang anda ketik salah'));
                    }                  
                } 
            }else if($method == 'POST'){
                $response = $this->check_auth_client();
                if($response['status'] == 200){
                    if($keterangan=="golPegawai"){
                        //INSERT api_log
                        $time = date("Y-m-d H:i:s");
                        $data_input= array(
                            'api_log.id' => '',
                            'api_log.client' => $id_client,
                            'api_log.api_name' => 'Jumlah Golongan Pegawai',
                            'api_log.datetime_request' => $time,
                            'api_log.status' => 1
                        ); 
                        $this->db->insert('api_log', $data_input );
                        //POST request data
                        $this->db->insert('golPegawai', $parameter);
                        $insert_id = $this->db->insert_id();
                        echo json_encode(array('status' => 200, 'detail' =>'Data Berhasil Ditambahkan','id' => $insert_id));
                    }else{
                        echo json_encode(array('status' => 404, 'detail' =>'Segment yang anda ketik salah'));
                    }                  
                }                 
            }else if($method == 'PUT'){
                $response = $this->check_auth_client();
                if($response['status'] == 200){
                    if($keterangan=="golPegawai"){
                        //INSERT api_log
                        $time = date("Y-m-d H:i:s");
                        $data_input= array(
                            'api_log.client' => $id_client,
                            'api_log.api_name' => 'Jumlah Golongan Pegawai',
                            'api_log.datetime_request' => $time,
                            'api_log.status' => 1
                        ); 
                        $this->db->insert('api_log', $data_input );
                        //PUT request data
                        $where = array(
                          'golPegawai.id' => $id
                          );
                        $this->db->where($where);
                        $this->db->update('golPegawai', $parameter);
                        echo json_encode(array('status' => 200, 'detail' =>'Data Berhasil Di Update','id' => $id));
                    }else{
                        echo json_encode(array('status' => 404, 'detail' =>'Segment yang anda ketik salah'));
                    }                  
                }                      
            }else if($method == 'DELETE'){
                $response = $this->check_auth_client();
                if($response['status'] == 200){
                    if($keterangan=="golPegawai"){
                        //INSERT api_log
                        $time = date("Y-m-d H:i:s");
                        $data_input= array(
                            'api_log.id' => 'NULL',
                            'api_log.client' => $id_client,
                            'api_log.api_name' => 'Jumlah Golongan Pegawai',
                            'api_log.datetime_request' => $time,
                            'api_log.status' => 1
                        ); 
                        $this->db->insert('api_log', $data_input );
                        //DELETE request data
                        $where = array(
                          'golPegawai.id' => $id
                          );
                        $delete = array(
                          'golPegawai.status' => 99
                          );
                        $this->db->where($where);
                        $this->db->update('golPegawai', $delete);
                        return json_encode(array('status' => 200, 'detail' =>'Data Berhasil dihapus'));
                    }else{
                        echo json_encode(array('status' => 404, 'detail' =>'Segment yang anda ketik salah'));
                    }                  
                }                 
            } else {             
                json_encode(array('status' => 400,'message' => 'Bad request.'));
            }          
        }
  }

  public function api_berita()
  {
    $web=$this->uut->namadomain(base_url());
    $query = $this->db->query("SELECT `posting`.`id_posting` FROM `posting` WHERE `posting`.`status` != 99 AND `posting`.`judul_posting` = 'berita' AND `posting`.`domain` = '$web'");
    if(empty($query->result())){
        $data = array(
          'error' => 'Data Tidak Ditemukan(Kosong)!'
          );
        print_r($data);
    } else {
    foreach ($query->result() as $h)
        {
            $query1 = $this->db->query("SELECT `posting`.`id_posting` as id_posting, `posting`.`judul_posting`, `posting`.`isi_posting` FROM `posting` WHERE `posting`.`status` != 99 AND `posting`.`parent` = '$h->id_posting' AND `posting`.`domain` = '$web' ORDER BY `posting`.`created_time` DESC");
            foreach ($query1->result() as $h1)
            {
                $query2 = $this->db->query("SELECT `attachment`.`file_name` FROM `attachment` WHERE `attachment`.`id_tabel` = '$h1->id_posting'");
                $attachment = '';
                foreach ($query2->result() as $h2)
                {   
                    if($attachment == "")
                    {
                        $attachment = base_url().'media/upload/s_'.$h2->file_name;
                    }else{
                        $attachment = $attachment.', '.base_url().'media/upload/s_'.$h2->file_name;
                    }
                }
                $data = array(
                  'judul_berita' => $h1->judul_posting,
                  'isi_berita' => $h1->isi_posting,
                  'attachment' => $attachment
                  );
                print_r($data);
                //echo json_encode($data);
                //echo json_encode($query1->result_array());
            }
        }
    }
  }

    public function check_auth_client(){      
        //Set Time Server
        $maxExpiredReq = 30000; //5 menit      
        date_default_timezone_set('UTC');
        $tStampServer = strval(time()-strtotime('1970-01-01 00:00:00'));       
        //Request Header
        //$Xconsid = $this->input->get_request_header('X-cons-id', TRUE);
        //$XTimestamp  = $this->input->get_request_header('X-Timestamp', TRUE);
        //$XSignature  = $this->input->get_request_header('X-Signature', TRUE);
                $Xconsid = $_GET['Xconsid'];
                $XTimestamp = $_GET['XTimestamp'];
                $XSignature = $_GET['XSignature'];
                //ECHO $XSignature.'<br/>';
        //Cek Id Server
        $where = array(
            'api_webservice_client.consumerID'  => $Xconsid,
            'api_webservice_client.active'      => 1
            );
        $this->db->select('
            api_webservice_client.id,
            api_webservice_client.consumerID,
            api_webservice_client.consumerSecret
            ');
        $this->db->where($where);
        $result = $this->db->get('api_webservice_client');
        foreach ($result->result_array() as $row):                                                     
            $id_client = $row['id'];
            $consumerID = $row['consumerID'];
            $consumerSecret = $row['consumerSecret'];
        endforeach; 
        $intervalRequest = $tStampServer-$XTimestamp;
        if($Xconsid == $consumerID){
            if($intervalRequest < $maxExpiredReq){
                //Check Auth
                $signature = hash_hmac('sha256', $consumerID."&".$XTimestamp, $consumerSecret, true);
                $encodedSignature = base64_encode($signature);  
                $encodedSignature = str_replace('+', '', $encodedSignature);
                                //ECHO $encodedSignature;                               
                if($encodedSignature == $XSignature){
                    return array('status' => 200,'message' => 'Authorized.','id_client' => $id_client);
                }else{
                    echo json_encode(array('status' => 401,'message' => 'Unauthorized<br>please refresh...'));
                    //echo json_encode(array('status' => 401,'message' => 'Unauthorized<br>please refresh...<br>'.$encodedSignature.'<br>'.$XSignature.''));
                }            
            }else{
                echo json_encode(array('status' => 401,'message' => 'Unauthorized<br>please refresh...'));
            }
        }else{
            echo json_encode(array('status' => 401,'message' => 'Unauthorized<br>please refresh...'));
        }
    }

    function request()
  {
        $keterangan = $this->uri->segment(3);
        echo json_encode(array('keterangan' => 'sddww'.$keterangan));
    }

}
