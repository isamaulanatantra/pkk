<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Warga extends CI_Controller
{

  function __construct(){
		parent::__construct();
		$this->load->model('Warga_model');
  }
  public function index(){
    $data['main_view'] = 'warga/home';
    $this->load->view('back_bone', $data);
  }
  public function total_data()
  {
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'warga.status !=' => 99,
      'warga.id_desa=' => $this->session->userdata('id_desa')
      );
    $this->db->where($where0);
    $query0 = $this->db->get('warga');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_warga(){
		$table = 'warga';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where      = array(
      'warga.status !=' => 99,
      'warga.id_desa=' => $this->session->userdata('id_desa')
    );
    $orderby   = ''.$order_by.'';
    $query = $this->Warga_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
          $urut=$urut+1;
          echo '<tr id_warga="'.$row->id_warga.'" id="'.$row->id_warga.'" >';
          echo '<td valign="top">'.($urut).'</td>';
					echo '<td valign="top">'.$row->nama_warga.'</td>';
					echo '<td valign="top"><a href="#tab_form_warga" data-toggle="tab" class="update_id badge bg-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>';
					echo '<a href="#" id="del_ajax" class="badge bg-danger btn-sm"><i class="fa fa-cut"></i></a>';
          echo '<a class="badge bg-warning btn-sm" href="'.base_url().'warga/pdf/?id_warga='.$row->id_warga.'" ><i class="fa fa-file-pdf-o"></i></a></td>';
          echo '</tr>';
      }
          echo '<tr>';
          echo '<td valign="top" colspan="4" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="',base_url(),'warga/xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
          echo '</tr>';
          
   }
  public function simpan_warga(){
    $this->form_validation->set_rules('nama_warga', 'nama_warga', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
			{
				$data_input = array(
				'nama_warga' => trim($this->input->post('nama_warga')),
				'id_desa' => $this->session->userdata('id_desa'),
				'id_kecamatan' => $this->session->userdata('id_kecamatan'),
				'id_kabupaten' => $this->session->userdata('id_kabupaten'),
				'id_propinsi' => $this->session->userdata('id_propinsi'),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'warga';
      $this->Warga_model->save_data($data_input, $table_name);
     }
   }

   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
      'warga.id_warga' => $this->input->post('id_warga')
        );
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_warga');
      $result = $this->db->get('warga');
      echo json_encode($result->result_array());
		}
   
}