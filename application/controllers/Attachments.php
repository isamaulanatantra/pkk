<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attachments extends CI_Controller {

	function __construct()
		{
		parent::__construct();
    $this->load->library('upload', 'image_lib');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
    $this->load->library('Bcrypt');
    $this->load->model('Crud_model');
		$this->load->helper('file');
		}
  
  public function check_login()
   {
    $table_name = 'users';
    $where      = array(
      'users.id_users' => $this->session->userdata('id_users')
    );
    $a          = $this->Crud_model->semua_data($where, $table_name);
    if ($a == 0)
     {
        return redirect(''.base_url().'login');
     }
   }
  public function index()
   {
    $this->check_login();
    }
  
	public function other_attachments_table()
		{
       echo 'ok';
		}
		
  public function upload_foto_profil()
		{
		$where = array(
              'id_tabel' => $this->session->userdata('id_users'),
              'keterangan' => 'foto_profil',
							'table_name' => 'foto_profil'
              );
							
			$this->db->where($where);
			$this->db->delete('attachments');
			
      $config['upload_path'] = './media/peraturan/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '500000';
			$this->upload->initialize($config);
			$uploadFiles = array('img_1' => 'myfile', 'img_2' => 'e_myfile', );		
			$this->load->library('image_lib');
			$newName = '-';
			$table_name = $_GET['table_name'];
			foreach($uploadFiles as $key => $files)
				{
				if ($this->upload->do_upload($files)) 
					{
						$upload = $this->upload->data();
						$file = explode(".", $upload['file_name']);
						$prefix = date('Ymdhis');
						$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
						$filePath =  $upload['file_path'].$newName;
						rename($upload['full_path'],$filePath);
            $data_input = array( 
              'table_name' => $table_name,
              'file_name' => $newName,
              'keterangan' => 'foto_profil',
              'id_tabel' => $this->session->userdata('id_users'),
              'temp' => 'foto_profil',
              'uploaded_time' => date('Y-m-d H:i:s'),
              'uploaded_by' => $this->session->userdata('id_users')
              );
						$table_name = 'attachments';
						$id         = $this->Crud_model->save_data($data_input, $table_name);
						$config['image_library'] = 'gd2';
						$config['source_image']  = './media/peraturan/'.$newName.'';
						$config['new_image']  = './media/peraturan/s_'.$newName.'';						
						$config['width']	 = 128;
						$config['height']	= 128;
						$this->image_lib->initialize($config); 
						$this->image_lib->resize();
						$file = './media/peraturan/s_'.$newName.'';
						$j = get_mime_by_extension($file);
						$ex = explode('/', $j);
						$e = $ex[0];
            echo
            '
						<div class="alert alert-success alert-dismissable" id="img_upload">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						'; 
						if( $e == 'image'){
							echo '<a target="_blank" href="'.base_url().'media/peraturan/'.$newName.'"><img src="'.base_url().'media/peraturan/s_'.$newName.'" /></a>';
							}
						else{
							echo '<a target="_blank" href="'.base_url().'media/peraturan/'.$newName.'"> '.$newName.' </a>';
							}
            echo '</div>';
						echo 'OK';
					}
					
				}
				
		}
		
  public function upload()
		{
      $config['upload_path'] = './media/peraturan/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '500000';
			$this->upload->initialize($config);
			$uploadFiles = array('img_1' => 'myfile', 'img_2' => 'e_myfile', );		
			$this->load->library('image_lib');
			$newName = '-';
			$table_name = $_GET['table_name'];
			$temp = $this->input->post('temp');
			$keterangan = $this->input->post('remake');
			$this->form_validation->set_rules('remake', 'remake', 'required');
			if ($this->form_validation->run() == FALSE)
				{
					echo
					'
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
						Mohon Keterangan Lampiran Diisi
					</div>
					';
				}
			else
				{
					foreach($uploadFiles as $key => $files)
					{
					if ($this->upload->do_upload($files)) 
						{
							$upload = $this->upload->data();
							$file = explode(".", $upload['file_name']);
							$prefix = date('Ymdhis');
							$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
							$filePath =  $upload['file_path'].$newName;
							rename($upload['full_path'],$filePath);
              if($file[1] == 'php'){ echo 'Asem Lu'; exit;}
							$data_input = array(
								'table_name' => $table_name,
								'file_name' => $newName,
								'keterangan' => $keterangan,
								'id_tabel' => '',
								'temp' => $temp,
								'uploaded_time' => date('Y-m-d H:i:s'),
								'uploaded_by' => $this->session->userdata('id_users')
								);
							$table_name = 'attachments';
							$id         = $this->Crud_model->save_data($data_input, $table_name);
							$config['image_library'] = 'gd2';
							$config['source_image']  = './media/peraturan/'.$newName.'';
							$config['new_image']  = './media/peraturan/s_'.$newName.'';						
							$config['width']	 = 128;
							$config['height']	= 128;
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
							$file = './media/peraturan/s_'.$newName.'';
							$j = get_mime_by_extension($file);
							$ex = explode('/', $j);
							$e = $ex[0];
							echo
							'
							<div class="alert alert-success alert-dismissable" id="img_upload">
							<i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							'; 
							if( $e == 'image'){
								echo '<a target="_blank" href="'.base_url().'media/peraturan/'.$newName.'"><img src="'.base_url().'media/peraturan/s_'.$newName.'" /></a>';
								}
							else{
								echo '<a target="_blank" href="'.base_url().'media/peraturan/'.$newName.'"> '.$newName.' </a>';
								}
							echo '</div>';
						}
					}
				}
		}
		
	public function e_upload()
		{
      $config['upload_path'] = './media/peraturan/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '500000';
			$this->upload->initialize($config);
			$uploadFiles = array('img_1' => 'myfile', 'img_2' => 'e_myfile', );		
			$this->load->library('image_lib');
			$newName = '-';
			$table_name = $_GET['table_name'];
			$id = $this->input->post('id');
			$keterangan = $this->input->post('e_remake');
			foreach($uploadFiles as $key => $files)
				{
				if ($this->upload->do_upload($files)) 
					{
						$upload = $this->upload->data();
						$file = explode(".", $upload['file_name']);
						$prefix = date('Ymdhis');
						$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
						$filePath =  $upload['file_path'].$newName;
						rename($upload['full_path'],$filePath);
              if($file[1] == 'php'){ echo 'Asem Lu'; exit;}
            $data_input = array(
              'table_name' => $table_name,
              'file_name' => $newName,
              'keterangan' => $keterangan,
              'id_tabel' => $id,
              'temp' => '',
              'uploaded_time' => date('Y-m-d H:i:s'),
              'uploaded_by' => $this->session->userdata('id_users')
              );
						$table_name = 'attachments';
						$id         = $this->Crud_model->save_data($data_input, $table_name);
						$config['image_library'] = 'gd2';
						$config['source_image']  = './media/peraturan/'.$newName.'';
						$config['new_image']  = './media/peraturan/s_'.$newName.'';						
						$config['width']	 = 128;
						$config['height']	= 128;
						$this->image_lib->initialize($config); 
						$this->image_lib->resize();
						$file = './media/peraturan/s_'.$newName.'';
						$j = get_mime_by_extension($file);
						$ex = explode('/', $j);
						$e = $ex[0];
            echo
            '
						<div class="alert alert-success alert-dismissable" id="img_upload">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						'; 
						if( $e == 'image'){
							echo '<a target="_blank" href="'.base_url().'media/peraturan/'.$newName.'"><img src="'.base_url().'media/peraturan/s_'.$newName.'" /></a>';
							}
						else{
							echo '<a target="_blank" href="'.base_url().'media/peraturan/'.$newName.'"> '.$newName.' </a>';
							}
            echo '</div>';
					}
				}
		}	
		
	public function foto_profil()
		{
      $table_name = $_GET['table_name'];
			$limit    = 200;
      $start    = 0;
      $fields = 
				'
				id_attachments,
				table_name,
				file_name,
				keterangan,
				id_tabel,
				temp,
				uploaded_time,
				uploaded_by
				';
      $where    = array(
        'attachments.id_tabel' => $this->session->userdata('id_users'),
				'table_name' => $table_name
				);
      $order_by = 'attachments.id_attachments';
			$table_name = 'attachments';
      echo json_encode($this->Crud_model->json($where, $limit, $start, $table_name, $fields, $order_by));
		}
	public function load_lampiran()
		{
      $the_table_name = $this->input->get('table');
      $temp = $this->input->post('temp');
			$limit    = 200;
      $start    = 0;
      $fields = 
				'
				id_attachments,
				table_name,
				file_name,
				keterangan,
				id_tabel,
				temp,
				uploaded_time,
				uploaded_by
				';
      $where    = array(
        'attachments.table_name' => $the_table_name,
				'temp' => $temp
				);
      $order_by = 'attachments.id_attachments';
			$table_name = 'attachments';
      echo json_encode($this->Crud_model->json($where, $limit, $start, $table_name, $fields, $order_by));
		}
	public function e_load_lampiran()
		{
      $the_table_name = $this->input->get('table');
      $id_tabel = $this->input->post('id');
			$limit    = 200;
      $start    = 0;
      $fields = 
				'
				id_attachments,
				table_name,
				file_name,
				keterangan,
				id_tabel,
				temp,
				uploaded_time,
				uploaded_by
				';
      $where    = array(
        'attachments.table_name' => $the_table_name,
				'id_tabel' => $id_tabel
				);
      $order_by = 'attachments.id_attachments';
			$table_name = 'attachments';
      echo json_encode($this->Crud_model->json($where, $limit, $start, $table_name, $fields, $order_by));
		}
		
		public function get_attachments_by_id()
		{
      $table_name = $_GET['table_name'];
			$id    = $_GET['id'];
			$limit    = 200;
      $start    = 0;
      $fields = 
				'
				id_attachments,
				table_name,
				file_name,
				keterangan,
				id_tabel,
				temp,
				uploaded_time,
				uploaded_by
				';
      $where    = array(
        'attachments.id_tabel' => $id,
				'table_name' => $table_name
				);
      $order_by = 'attachments.id_attachments';
			$table_name = 'attachments';
      echo json_encode($this->Crud_model->json($where, $limit, $start, $table_name, $fields, $order_by));
		}
		
		public function hapus()
		{
			$id = $_GET['id'];
			$this->db->where('id_attachments', $id);
			$this->db->delete('attachments');
			echo ' {"errors":"No"} ';
		}
    
	public function json_all_attachments(){
		$table = 'attachments';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "*";
    $where = array(
      'attachments.keterangan' => 'Naskah Akademik',
      'attachments.table_name' => 'peraturan'
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_attachments="'.$row->id_attachments.'" id="'.$row->id_attachments.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$row->keterangan.'</td>';
				echo '<td valign="top">';
        $w = $this->db->query("
        SELECT * from peraturan
        where peraturan.id_peraturan=".$row->id_attachments."
        ");
        foreach($w->result() as $row_peraturan){
          $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
          $yummy   = array("_","","","","","","","","","","","","","");
          $newphrase = str_replace($healthy, $yummy, $row_peraturan->nama_peraturan);
          echo'<a class="" href="'.base_url().'peraturan/details/'.$row_peraturan->id_peraturan.'/'.$newphrase.'.HTML" target="_blank">'.$row_peraturan->nama_peraturan.'</a><br />';
        }
        echo'</td>';
      }
	}
  public function total_data()
  {
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'attachments.keterangan' => 'Naskah Akademik',
      'attachments.table_name' => 'peraturan'
      );
    $this->db->where($where0);
    $query0 = $this->db->get('attachments');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
}