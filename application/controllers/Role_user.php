<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Role_user extends CI_Controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('Excel');
    
    $this->load->library('Uut');
  }
  
  public function index()
  {
    $data['test'] = '';
    $this->load->view('back_end/role_user/content', $data);
  }
  
  public function json()
  {
    $table       = $this->input->post('table');
    $page        = $this->input->post('page');
    $limit       = $this->input->post('limit');
    $keyword     = $this->input->post('keyword');
    $status      = $this->input->post('status');
    $order_by    = $this->input->post('order_by');
    $order_field = $this->input->post('order_field');
    $shorting    = $this->input->post('shorting');
    $start       = ($page - 1) * $limit;
    $fields      = "
            role_user.role_user_id,
            role_user.role_id,
            role_user.user_id,
            role_user.information,
            role_user.created_by,
            role_user.created_time,
            role_user.updated_by,
            role_user.updated_time,
            role_user.deleted_by,
            role_user.deleted_time,
            role_user.status,
            
            role.role_code,
            role.role_name,
            
            users.user_name,
            
            (
            select attachment.file_name from attachment
            where attachment.id_tabel=role_user.role_user_id
            and attachment.table_name='" . $table . "'
            order by attachment.id_attachment DESC
            limit 1
            ) as file
            ";
    $where       = array(
      '' . $table . '.status' => $status
    );
    $this->db->select("$fields");
    if ($keyword == '') {
      $this->db->where("
            " . $table . ".status = " . $status . "
            ");
    } else {
      $this->db->where("
            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'
            AND
            " . $table . ".status = " . $status . "
            ");
    }
    $this->db->join('role', 'role.role_id=role_user.role_id');
    $this->db->join('users', 'users.id_users=role_user.user_id');
    $this->db->order_by($order_by, $shorting);
    $this->db->limit($limit, $start);
    $result = $this->db->get($table);
    echo json_encode($result->result_array());
  }
  
  public function total()
  {
    $table       = $this->input->post('table');
    $page        = $this->input->post('page');
    $limit       = $this->input->post('limit');
    $keyword     = $this->input->post('keyword');
    $status      = $this->input->post('status');
    $order_by    = $this->input->post('order_by');
    $shorting    = $this->input->post('shorting');
    $order_field = $this->input->post('order_field');
    $start       = ($page - 1) * $limit;
    $fields      = "
            role_user.role_user_id,
            role_user.role_id,
            role_user.user_id,
            role_user.information,
            role_user.created_by,
            role_user.created_time,
            role_user.updated_by,
            role_user.updated_time,
            role_user.deleted_by,
            role_user.deleted_time,
            role_user.status
            ";
    $where       = array(
      '' . $table . '.status' => $status
    );
    $this->db->select("$fields");
    if ($keyword == '') {
      $this->db->where("
            " . $table . ".status = " . $status . "
            ");
    } else {
      $this->db->where("
            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'
            AND
            " . $table . ".status = " . $status . "
            ");
    }
    $this->db->join('role', 'role.role_id=role_user.role_id');
    $this->db->join('users', 'users.id_users=role_user.user_id');
    $query = $this->db->get($table);
    $a     = $query->num_rows();
    echo trim(ceil($a / $limit));
  }
  
  public function simpan()
  {
    $this->form_validation->set_rules('role_id', 'role_id', 'required');
    $this->form_validation->set_rules('temp', 'temp', 'required');
    $this->form_validation->set_rules('user_id', 'user_id', 'required');
    $this->form_validation->set_rules('information', 'information', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_input = array(
        'role_id' => trim($this->input->post('role_id')),
        'user_id' => trim($this->input->post('user_id')),
        'information' => trim($this->input->post('information')),
        'temp' => trim($this->input->post('temp')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'status' => 1
      );
      $this->db->insert( 'role_user', $data_input );
      $id= $this->db->insert_id();
      echo '' . $id . '';
      $where       = array(
        'table_name' => 'role_user',
        'temp' => trim($this->input->post('temp'))
      );
      $data_update = array(
        'id_tabel' => $id
      );
      $this->db->where($where);
      $this->db->update('attachment', $data_update);
    }
  }
  
  public function update()
  {
    $this->form_validation->set_rules('role_user_id', 'role_user_id', 'required');
    $this->form_validation->set_rules('role_id', 'role_id', 'required');
    $this->form_validation->set_rules('user_id', 'user_id', 'required');
    $this->form_validation->set_rules('information', 'information', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'role_id' => trim($this->input->post('role_id')),
        'user_id' => trim($this->input->post('user_id')),
        'information' => trim($this->input->post('information')),
        'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
      );
      $where       = array(
        'role_user_id' => trim($this->input->post('role_user_id'))
      );
      $this->db->where($where);
      $this->db->update('role_user', $data_update);
      echo 1;
    }
  }
  
  public function get_by_id()
  {
    $role_user_id = $this->input->post('role_user_id');
    $where   = array(
      'role_user.role_user_id' => $role_user_id
    );
    $this->db->select("
      role_user.role_user_id,
      role_user.role_id,
      role_user.user_id,
      role_user.information,
      role_user.created_by,
      role_user.created_time,
      role_user.updated_by,
      role_user.updated_time,
      role_user.deleted_by,
      role_user.deleted_time,
      role_user.status,
      
      role.role_code,
      role.role_name,
      
      users.user_name
      ");    	
    $this->db->where($where);
    $this->db->join('role', 'role.role_id=role_user.role_id');
    $this->db->join('users', 'users.id_users=role_user.user_id');
		$result = $this->db->get('role_user');
    echo json_encode($result->result_array());
  }
  
  public function delete_by_id()
  {
    $this->form_validation->set_rules('role_user_id', 'role_user_id', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 99
      );
      $where       = array(
        'role_user_id' => trim($this->input->post('role_user_id'))
      );
      $table_name  = 'role_user';
      $this->db->where($where);
      $this->db->update('role_user', $data_update);
      echo 1;
    }
  }
  
  public function restore_by_id()
  {
    $this->form_validation->set_rules('role_user_id', 'role_user_id', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 1
      );
      $where       = array(
        'role_user_id' => trim($this->input->post('role_user_id'))
      );
      $table_name  = 'role_user';
      $this->db->where($where);
      $this->db->update($table_name, $data_update);
      echo 1;
    }
  }
  
}