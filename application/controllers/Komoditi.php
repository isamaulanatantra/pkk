<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Komoditi extends CI_Controller
 {
    
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Komoditi_model');
   }
  
  function insert_parrent($urut, $judul_komoditi, $satuan){
    $web=$this->uut->namadomain(base_url());
    $data_input = array(
      'domain' => $web,
      'judul_komoditi' => $judul_komoditi,
      'highlight' => 0,
      'id_pasar' => 1,
      'harga' => 1,
      'parent' => 0,
      'kata_kunci' => $judul_komoditi,
      'temp' => date('YmdHis'),
      'urut' => $urut,
      'tanggal' => 'menu_atas',
      'satuan' => $satuan,
      'isi_komoditi' => 'Isikan deskripsi disini',
      'keterangan' => $judul_komoditi,
      'created_by' => '',
      'created_time' => date('Y-m-d H:i:s'),
      'status' => 1

      );
    $table_name = 'komoditi';
    return $this->Komoditi_model->simpan_komoditi($data_input, $table_name);
  }
  
  function insert_child($parent, $urut, $judul_komoditi, $satuan){
    $web=$this->uut->namadomain(base_url());
    $data_input = array(
      'domain' => $web,
      'judul_komoditi' => $judul_komoditi,
      'highlight' => 0,
      'id_pasar' => 1,
      'harga' => 1,
      'parent' => $parent,
      'kata_kunci' => $judul_komoditi,
      'temp' => date('YmdHis'),
      'urut' => $urut,
      'tanggal' => 'menu_atas',
      'satuan' => $satuan,
      'isi_komoditi' => 'Isikan deskripsi disini',
      'keterangan' => $judul_komoditi,
      'created_by' => '',
      'created_time' => date('Y-m-d H:i:s'),
      'status' => 1

      );
    $table_name = 'komoditi';
    $id         = $this->Komoditi_model->simpan_komoditi($data_input, $table_name);
  }
  
  function cek_default_halaman()
	{
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("SELECT * from komoditi where domain='".$web."' ");
    $j=$w->num_rows();
    
		if( ($w->num_rows()) == 0 ){
      echo 'Komoditi default berhasil digenerate';
      $judul_komoditi='Tentang Kami';
      $satuan = 'fa-tasks';
      $urut='1';
      $parent=$this->insert_parrent($urut, $judul_komoditi, $satuan);
        $judul_komoditi='Profil';
        $satuan = 'fa-tasks';
        $urut='1';
        $this->insert_child($parent, $urut, $judul_komoditi, $satuan);
        ////////////////////////
        $judul_komoditi='Visi Misi';
        $satuan = 'fa-tasks';
        $urut='2';
        $this->insert_child($parent, $urut, $judul_komoditi, $satuan);
        ////////////////////////
        $judul_komoditi='Tupoksi';
        $satuan = 'fa-tasks';
        $urut='3';
        $this->insert_child($parent, $urut, $judul_komoditi, $satuan);
        ////////////////////////
        $judul_komoditi='Personil';
        $satuan = 'fa-tasks';
        $urut='4';
        $this->insert_child($parent, $urut, $judul_komoditi, $satuan);
      ////////////////////////
      $judul_komoditi='Berita';
      $satuan = 'fa-tasks';
      $urut='2';
      $parent=$this->insert_parrent($urut, $judul_komoditi, $satuan);
        ////////////////////////
      $judul_komoditi='Transparansi Anggaran';
      $satuan = 'fa-tasks';
      $urut='3';
      $parent=$this->insert_parrent($urut, $judul_komoditi, $satuan);
        ////////////////////////
      $judul_komoditi='Pengumuman';
      $satuan = 'fa-tasks';
      $urut='4';
      $parent=$this->insert_parrent($urut, $judul_komoditi, $satuan);
        ////////////////////////
      $judul_komoditi='PPID';
      $satuan = 'fa-tasks';
      $urut='5';
      $parent=$this->insert_parrent($urut, $judul_komoditi, $satuan);
        ////////////////////////
      $judul_komoditi='Layanan';
      $satuan = 'fa-tasks';
      $urut='6';
      $parent=$this->insert_parrent($urut, $judul_komoditi, $satuan);
        $judul_komoditi='Pengaduan Masyarakat';
        $satuan = 'fa-tasks';
        $urut='1';
        $this->insert_child($parent, $urut, $judul_komoditi, $satuan);
        ////////////////////////
        $judul_komoditi='Permohonan Informasi';
        $satuan = 'fa-tasks';
        $urut='2';
        $this->insert_child($parent, $urut, $judul_komoditi, $satuan);
        ////////////////////////
        $judul_komoditi='SPPL';
        $satuan = 'fa-tasks';
        $urut='3';
        $this->insert_child($parent, $urut, $judul_komoditi, $satuan);
        ////////////////////////
        $judul_komoditi='UKL UPL';
        $satuan = 'fa-tasks';
        $urut='4';
        $this->insert_child($parent, $urut, $judul_komoditi, $satuan);
        ////////////////////////
        $judul_komoditi='AMDAL';
        $satuan = 'fa-tasks';
        $urut='5';
        $this->insert_child($parent, $urut, $judul_komoditi, $satuan);
        ////////////////////////
		}
    else{
    echo 'Komoditi default Sudah ada';
    }
	}
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'komoditi/home';
    $this->load->view('back_bone', $data);
   }
  
  function load_the_option()
	{
		$a = 0;
		echo $this->option_komoditi(0,$h="", $a);
	}
  
  private function option_komoditi($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from komoditi where parent='".$parent."' and domain='".$web."' and status = 1 order by urut");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_komoditi.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_komoditi.'';
			}
			else{
			$hasil .= ''.$h->judul_komoditi.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_komoditi($h->id_komoditi,$hasil, $a);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
  function load_the_option_by_tanggal()
	{
    $web=$this->uut->namadomain(base_url());
    $tanggal = trim($this->input->post('tanggal'));
		$a = 0;
		echo $this->option_komoditi_by_tanggal(0,$h="", $a, $tanggal);
	}
  
  private function option_komoditi_by_tanggal($parent=0,$hasil, $a, $tanggal){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from komoditi 
                          where parent='".$parent."' 
                          and status = 1 
                          and tanggal='".$tanggal."'
                          and domain='".$web."'
                          order by urut");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_komoditi.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_komoditi.'';
			}
			else{
			$hasil .= ''.$h->judul_komoditi.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_komoditi_by_tanggal($h->id_komoditi,$hasil, $a, $tanggal);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
  function load_table()
	{
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		echo $this->komoditi(0,$h="", $a);
	}
  
  function load_table_arsip()
	{
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		echo $this->komoditi_arsip(0,$h="", $a);
	}
  
  private function komoditi($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from komoditi
		where parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    order by urut
		");
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<tr id_komoditi="'.$h->id_komoditi.'" id="'.$h->id_komoditi.'" >';
      $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->urut.' </td>';
      if( $a > 1 ){
        $hasil .= '<td style="padding: 2px 2px 2px '.($a * 20).'px ;"> <a target="_blank" href="'.base_url().'komoditis/detail/'.$h->id_komoditi.'/'.str_replace(' ', '_', $h->judul_komoditi ).'.HTML">'.$h->judul_komoditi.' </a> </td>';
        }
			else{
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> <a target="_blank" href="'.base_url().'komoditis/detail/'.$h->id_komoditi.'/'.str_replace(' ', '_', $h->judul_komoditi ).'.HTML">'.$h->judul_komoditi.' </a> </td>';
        }
      $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->id_pasar.' </td>';
      $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->harga.' </td>';
      $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->satuan.' </td>';
			$hasil .= 
			'
			<td style="padding: 2px 2px 2px 2px ;"> 
			<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> 
			<a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> 
			</td>
			';
			$hasil .= '</tr>';
			$hasil = $this->komoditi($h->id_komoditi,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  
  private function komoditi_arsip($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from komoditi
									
		where parent='".$parent."' 
    and domain='".$web."'
		order by tanggal, urut
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<tr id_komoditi="'.$h->id_komoditi.'" id="'.$h->id_komoditi.'" >';
      if( $a > 1 ){
        $hasil .= '<td style="padding: 5px '.($a * 20).'px ;"> <a target="_blank" href="'.base_url().'komoditis/detail/'.$h->id_komoditi.'/'.str_replace(' ', '_', $h->judul_komoditi ).'.HTML">'.$h->judul_komoditi.' </a> </td>';
        }
			else{
        $hasil .= '<td style="padding: 5px ;"> '.$h->judul_komoditi.' </td>';
        }
      //$hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->urut.' </td>';
      if( $h->status == 99 ){
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> <a href="#" id="restore_ajax"><i class="fa fa-cut"></i> Restore</a> </td>';
      }
      else{
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> &nbsp; </td>';
      }
      
			$hasil .= '</tr>';
			$hasil = $this->komoditi_arsip($h->id_komoditi,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  
  function load_table1()
	{
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		$menu_atas = $this->komoditi1(0,$h="", $a);
    echo $menu_atas;
	}
  
  private function komoditi1($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from komoditi
									
		where parent='".$parent."'
    and tanggal='menu_atas'
		and status = 1 
    and domain='".$web."'
    order by urut
		");
		
		if(($w->num_rows())>0)
		{
      if($parent == 0){
			$hasil .= '
<ul> '; 
      }
      else{
			$hasil .= '
<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from komoditi
									
		where parent='".$h->id_komoditi."' 
		and status = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
      $hasil .= '
<li> '.$h->judul_komoditi.' ';
        }
			else{ 
      if ($xx == 0){
      $hasil .= '
<li> '.$h->judul_komoditi.'';
      }
      else{
      $hasil .= '
<li> '.$h->judul_komoditi.'';
      } 
        }
			$hasil = $this->komoditi1($h->id_komoditi,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  
  function load_table2()
	{
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		$menu_atas = $this->komoditi2(0,$h="", $a);
    echo $menu_atas;
	}
  
  private function komoditi2($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from komoditi
									
		where parent='".$parent."'
    and tanggal='menu_kanan'
		and status = 1 
    and domain='".$web."'
    order by urut
		");
		
		if(($w->num_rows())>0)
		{
      if($parent == 0){
			$hasil .= '
<ul> '; 
      }
      else{
			$hasil .= '
<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from komoditi
									
		where parent='".$h->id_komoditi."' 
		and status = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
      $hasil .= '
<li> '.$h->judul_komoditi.' ';
        }
			else{ 
      if ($xx == 0){
      $hasil .= '
<li> '.$h->judul_komoditi.'';
      }
      else{
      $hasil .= '
<li> '.$h->judul_komoditi.'';
      } 
        }
			$hasil = $this->komoditi2($h->id_komoditi,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  
function load_table3()
	{
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		$menu_kiri = $this->komoditi3(0,$h="", $a);
    echo $menu_kiri;
	}
  
  private function komoditi3($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from komoditi
									
		where parent='".$parent."'
    and tanggal='menu_kiri'
		and status = 1 
    and domain='".$web."'
    order by urut
		");
		
		if(($w->num_rows())>0)
		{
      if($parent == 0){
			$hasil .= '
<ul> '; 
      }
      else{
			$hasil .= '
<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from komoditi
									
		where parent='".$h->id_komoditi."' 
		and status = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
      $hasil .= '
<li> '.$h->judul_komoditi.' ';
        }
			else{ 
      if ($xx == 0){
      $hasil .= '
<li> '.$h->judul_komoditi.'';
      }
      else{
      $hasil .= '
<li> '.$h->judul_komoditi.'';
      } 
        }
			$hasil = $this->komoditi3($h->id_komoditi,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  
  public function json_all_komoditi()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $start      = ($halaman - 1) * $limit;
    $fields     = "*";
    $where      = array(
      'komoditi.status !=' => 99,
      'domain' => $web
    );
    $order_by   = 'komoditi.parent';
    echo json_encode($this->Komoditi_model->json_all_komoditi($where, $limit, $start, $fields, $order_by));
   }
   
  public function simpan_komoditi()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_komoditi', 'judul_komoditi', 'required');
		$this->form_validation->set_rules('highlight', 'highlight', 'required');
		$this->form_validation->set_rules('id_pasar', 'id_pasar', 'required');
		$this->form_validation->set_rules('harga', 'harga', 'required');
		$this->form_validation->set_rules('parent', 'parent', 'required');
		$this->form_validation->set_rules('urut', 'urut', 'required');
		// $this->form_validation->set_rules('tanggal', 'tanggal', 'required');
		$this->form_validation->set_rules('satuan', 'satuan', 'required');
		$this->form_validation->set_rules('kata_kunci', 'kata_kunci', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('isi_komoditi', 'isi_komoditi', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'judul_komoditi' => trim($this->input->post('judul_komoditi')),
				'highlight' => trim($this->input->post('highlight')),
				'id_pasar' => trim($this->input->post('id_pasar')),
				'harga' => trim($this->input->post('harga')),
				'parent' => trim($this->input->post('parent')),
				'kata_kunci' => trim(str_replace('.', '', str_replace(',', '', $this->input->post('judul_komoditi')))),
        'temp' => trim($this->input->post('temp')),
        'urut' => trim($this->input->post('urut')),
        'tanggal' => date('Y-m-d H:i:s'),
        'satuan' => trim($this->input->post('satuan')),
        'isi_komoditi' => trim($this->input->post('isi_komoditi')),
        'keterangan' => trim($this->input->post('keterangan')),
        'inserted_by' => $this->session->userdata('id_users'),
        'inserted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'komoditi';
      $id         = $this->Komoditi_model->simpan_komoditi($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'komoditi',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
   
  public function update_komoditi()
   {
    $web=$this->uut->namadomain(base_url());
    $this->form_validation->set_rules('judul_komoditi', 'judul_komoditi', 'required');
    // $this->form_validation->set_rules('highlight', 'highlight', 'required');
    $this->form_validation->set_rules('id_pasar', 'id_pasar', 'required');
    $this->form_validation->set_rules('harga', 'harga', 'required');
		$this->form_validation->set_rules('urut', 'urut', 'required');
		// $this->form_validation->set_rules('tanggal', 'tanggal', 'required');
		$this->form_validation->set_rules('satuan', 'satuan', 'required');
		// $this->form_validation->set_rules('kata_kunci', 'kata_kunci', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
				'judul_komoditi' => trim($this->input->post('judul_komoditi')),
				'highlight' => trim($this->input->post('highlight')),
				'id_pasar' => trim($this->input->post('id_pasar')),
				'harga' => trim($this->input->post('harga')),
				'parent' => trim($this->input->post('parent')),
				'kata_kunci' => trim(str_replace('.', '', str_replace(',', '', $this->input->post('judul_komoditi')))),
        'temp' => trim($this->input->post('temp')),
        'urut' => trim($this->input->post('urut')),
        'tanggal' => date('Y-m-d H:i:s'),
        'satuan' => trim($this->input->post('satuan')),
        'isi_komoditi' => trim($this->input->post('isi_komoditi')),
        'keterangan' => trim($this->input->post('keterangan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'komoditi';
      $where       = array(
        'komoditi.id_komoditi' => trim($this->input->post('id_komoditi')),
        'komoditi.domain' => $web
			);
      $this->Komoditi_model->update_data_komoditi($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'id_komoditi' => $this->input->post('id_komoditi'),
        'komoditi.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_komoditi');
      $result = $this->db->get('komoditi');
      echo json_encode($result->result_array());
		}
   
   public function total_komoditi()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $this->db->from('komoditi');
      $where    = array(
        'status' => 1,
        'domain' => $web
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_komoditi = $this->input->post('id_komoditi');
      $where = array(
        'id_komoditi' => $id_komoditi,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('komoditi');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'komoditi';
        $this->Komoditi_model->update_data_komoditi($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function restore()
		{
      $web=$this->uut->namadomain(base_url());
			$id_komoditi = $this->input->post('id_komoditi');
      $where = array(
        'id_komoditi' => $id_komoditi,
        'domain' => $web
        );
      $this->db->from('komoditi');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'komoditi';
        $this->Komoditi_model->update_data_komoditi($data_update, $where, $table_name);
        echo 1;
        }
		}
    
 }