<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dip extends CI_Controller
 {
    
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Dip_model');
    $this->load->model('Halaman_model');
   }
  
  public function index(){
    $data['total'] = 10;
    $data['main_view'] = 'dip/home';
    $this->load->view('tes', $data);
   }
  public function load_the_option(){
		$a = 0;
		echo $this->option_dip(0,$h="", $a);
	}
  
  private function option_dip($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from dip where parent='".$parent."' and domain='".$web."' and status = 1 order by urut");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_dip.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_dip.'';
			}
			else{
			$hasil .= ''.$h->judul_dip.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_dip($h->id_dip,$hasil, $a);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
  public function load_the_option_by_posisi(){
    $web=$this->uut->namadomain(base_url());
    $posisi = trim($this->input->post('posisi'));
		$a = 0;
		echo $this->option_dip_by_posisi(0,$h="", $a, $posisi);
	}
  
  private function option_dip_by_posisi($parent=0,$hasil, $a, $posisi){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from dip 
                          where parent='".$parent."' 
                          and status = 1 
                          and posisi='".$posisi."'
                          and domain='".$web."'
                          order by urut
                          limit 3
                          ");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_dip.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_dip.'';
			}
			else{
			$hasil .= ''.$h->judul_dip.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_dip_by_posisi($h->id_dip,$hasil, $a, $posisi);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  public function load_table(){
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		echo $this->dip(0,$h="", $a);
	}
  private function dip($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *, (select user_name from users where users.id_users=dip.created_by) as nama_pengguna from dip
									
		where parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    order by posisi, urut
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
        $yummy   = array("_","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_dip);
			$nomor = $nomor + 1;
			$hasil .= '<tr id_dip="'.$h->id_dip.'" id="'.$h->id_dip.'" >';
      if( $a > 1 ){
				$hasil .= '<td style="padding: 2px;">'.$h->urut.'.</td>';
        $hasil .= '<td style="padding: 2px '.($a * 20).'px ;">'.$h->judul_dip.'</td>';
        }
			else{
				$hasil .= '<td style="padding: 2px;"><b>'.$h->urut.'.</b></td>';
        $hasil .= '<td style="padding: 2px;"><b>'.$h->judul_dip.'</b></td>';
        }
      $hasil .= '<td style="padding: 2px;">'.$h->isi_dip.'</td>';
      $hasil .= '<td style="padding: 2px;">'.$h->kata_kunci.'</td>';
      if( $h->highlight == 0 ){
        $hasil .= '<td style="padding: 2px;"><a href="#" id="aktifkan_highlight" class="text-danger"><i class="fa fa-times-circle"></i></a></td>';
      }
      else{
        $hasil .= '<td style="padding: 2px;"><a href="#" id="inaktifkan_highlight" class="text-success"><i class="fa fa-check-circle"></i></a></td>';
      }
      if( $h->tampil_menu_atas == 0 ){
        $hasil .= '<td style="padding: 2px;"><a href="#" id="aktifkan_menu_atas" class="text-danger"><i class="fa fa-times-circle"></i></a></td>';
      }
      else{
        $hasil .= '<td style="padding: 2px;"><a href="#" id="inaktifkan_menu_atas" class="text-success"><i class="fa fa-check-circle"></i></a></td>';
      }
      if( $h->tampil_menu == 0 ){
        $hasil .= '<td style="padding: 2px;"><a href="#" id="aktifkan_tampil_menu" class="text-danger"><i class="fa fa-times-circle"></i></a></td>';
      }
      else{
        $hasil .= '<td style="padding: 2px;"><a href="#" id="inaktifkan_tampil_menu" class="text-success"><i class="fa fa-check-circle"></i></a></td>';
      }
      if( $h->informasi_berkala == 0 ){
        $hasil .= '<td style="padding: 2px;"><a href="#" id="aktifkan_informasi_berkala" class="text-danger"><i class="fa fa-times-circle"></i></a></td>';
      }
      else{
        $hasil .= '<td style="padding: 2px;"><a href="#" id="inaktifkan_informasi_berkala" class="text-success"><i class="fa fa-check-circle"></i></a></td>';
      }
      if( $h->informasi_setiap_saat == 0 ){
        $hasil .= '<td style="padding: 2px;"><a href="#" id="aktifkan_informasi_setiap_saat" class="text-danger"><i class="fa fa-times-circle"></i></a></td>';
      }
      else{
        $hasil .= '<td style="padding: 2px;"><a href="#" id="inaktifkan_informasi_setiap_saat" class="text-success"><i class="fa fa-check-circle"></i></a></td>';
      }
      if( $h->informasi_serta_merta == 0 ){
        $hasil .= '<td style="padding: 2px;"><a href="#" id="aktifkan_informasi_serta_merta" class="text-danger"><i class="fa fa-times-circle"></i></a></td>';
      }
      else{
        $hasil .= '<td style="padding: 2px;"><a href="#" id="inaktifkan_informasi_serta_merta" class="text-success"><i class="fa fa-check-circle"></i></a></td>';
      }
      if( $h->informasi_dikecualikan == 0 ){
        $hasil .= '<td style="padding: 2px;"><a href="#" id="aktifkan_informasi_dikecualikan" class="text-danger"><i class="fa fa-times-circle"></i></a></td>';
      }
      else{
        $hasil .= '<td style="padding: 2px;"><a href="#" id="inaktifkan_informasi_dikecualikan" class="text-success"><i class="fa fa-check-circle"></i></a></td>';
      }
      $hasil .= '<td style="padding: 2px;">'.$h->keterangan.'</td>';
			$hasil .= 
			'
			<td style="padding: 2px;">
			<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i> Edit</a><br />
			<a href="#" id="del_ajax"><i class="fa fa-cut"></i> Hapus</a><br />
			<small class="text-muted"><i class="fa fa-user-plus"></i> '.$h->nama_pengguna.'</small>
			</td>
			';
			$hasil .= '</tr>';
			$hasil = $this->dip($h->id_dip,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  public function load_publik(){
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		echo $this->dip_publik(0,$h="", $a);
	}
  private function dip_publik($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from dip
									
		where parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    order by posisi, urut
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
        $yummy   = array("_","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_dip);
			$nomor = $nomor + 1;
			$hasil .= '<tr id_dip="'.$h->id_dip.'" id="'.$h->id_dip.'">';
      if( $a > 1 ){
				$hasil .= '<td style="padding: 2px '.($a * 2).'px;">'.$h->urut.'.</td>';
        $hasil .= '<td style="padding: 2px;">'.$h->judul_dip.'</td>';
				$hasil .= '<td style="padding: 2px;">'.$h->isi_dip.'</td>';
				$hasil .= '<td style="padding: 2px;">'.$h->kata_kunci.'</td>';
				$hasil .= '<td style="padding: 2px;">';
				if( $h->highlight == 0 ){
					$hasil .= '<i class="fa fa-times text-danger"></i>';
				}
				else{
					$hasil .= '<i class="fa fa-check text-success"></i>';
				}
				$hasil .= '</td>';
				if( $h->tampil_menu_atas == 0 ){
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-times text-danger"></i></td>';
				}
				else{
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-check text-success"></i></td>';
				}
				if( $h->tampil_menu == 0 ){
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-times text-danger"></i></td>';
				}
				else{
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-check text-success"></i></td>';
				}
				if( $h->informasi_berkala == 0 ){
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-times text-danger"></i></td>';
				}
				else{
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-check text-success"></i></td>';
				}
				if( $h->informasi_setiap_saat == 0 ){
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-times text-danger"></i></td>';
				}
				else{
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-check text-success"></i></td>';
				}
				if( $h->informasi_serta_merta == 0 ){
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-times text-danger"></i></td>';
				}
				else{
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-check text-success"></i></td>';
				}
				if( $h->informasi_dikecualikan == 0 ){
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-times text-danger"></i></td>';
				}
				else{
					$hasil .= '<td style="padding: 2px;"><i class="fa fa-check text-success"></i></td>';
				}
				$hasil .= '<td style="padding: 2px;">'.$h->keterangan.'</td>';
        }
			else{
				$hasil .= '<td style="padding: 2px;"><b>'.$h->urut.'.</b></td>';
        $hasil .= '<td style="padding: 2px;"><b>'.$h->judul_dip.'</b></td>';
				$hasil .= '<td style="padding: 2px;"></td>';
				$hasil .= '<td style="padding: 2px;"></td>';
				$hasil .= '<td style="padding: 2px;"></td>';
				$hasil .= '<td style="padding: 2px;"></td>';
				$hasil .= '<td style="padding: 2px;"></td>';
				$hasil .= '<td style="padding: 2px;"></td>';
				$hasil .= '<td style="padding: 2px;"></td>';
				$hasil .= '<td style="padding: 2px;"></td>';
				$hasil .= '<td style="padding: 2px;"></td>';
				$hasil .= '<td style="padding: 2px;"></td>';
        }
			$hasil .= '</tr>';
			$hasil = $this->dip_publik($h->id_dip,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  public function load_daftar_alamat_opd(){
		$w = $this->db->query("
		SELECT 
		 skpd.nama_skpd, skpd.keterangan, 
		 data_skpd.skpd_website,
		 dasar_website.domain, dasar_website.alamat, dasar_website.telpon, dasar_website.email, dasar_website.twitter, dasar_website.facebook, dasar_website.google, dasar_website.instagram
		FROM skpd, data_skpd, dasar_website
		WHERE skpd.id_skpd=data_skpd.id_skpd
		AND dasar_website.domain=data_skpd.skpd_website
		AND dasar_website.status=1
		");
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			echo'
				
				<tr>
					<th>'.$nomor.'</th>
					<th><a href="https://'.$h->domain.'" target="_blank"> '.$h->nama_skpd.' </a></th>
					<th>'.$h->alamat.'</th>
					<th>'.$h->telpon.'</th>
					<th>'.$h->email.'</th>
				</tr>
				
			';
		}
	}
  public function load_data_kecamatan(){
		$w = $this->db->query("
		SELECT 
		 kecamatan.nama_kecamatan, skpd.keterangan, 
		 data_skpd.skpd_website,
		 dasar_website.domain, dasar_website.alamat, dasar_website.telpon, dasar_website.email, dasar_website.twitter, dasar_website.facebook, dasar_website.google, dasar_website.instagram
		FROM skpd, data_skpd, dasar_website, kecamatan
		WHERE skpd.id_skpd=data_skpd.id_skpd
		AND dasar_website.domain=data_skpd.skpd_website
		AND dasar_website.status=1
		AND data_skpd.id_kecamatan=kecamatan.id_kecamatan
		AND data_skpd.id_kecamatan!=0
		");
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			echo'
				
				<tr>
					<th>'.$nomor.'</th>
					<th><a class="more" href="https://'.$h->domain.'" target="_blank">KECAMATAN '.$h->nama_kecamatan.' </a></th>
					<th>'.$h->alamat.'</th>
					<th>'.$h->telpon.'</th>
					<th>'.$h->email.'</th>
				</tr>
				
			';
		}
	}
  public function load_table_arsip(){
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		echo $this->dip_arsip(0,$h="", $a);
	}
  private function dip_arsip($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from dip
									
		where parent='".$parent."' 
    and domain='".$web."'
		order by posisi, urut
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<tr id_dip="'.$h->id_dip.'" id="'.$h->id_dip.'" >';
      if( $h->posisi == 'menu_kiri' ){
      $hasil .= '<td style="padding: 2px;"> Menu-Kiri </td>';
      }
      elseif( $h->posisi == 'menu_atas' ){
      $hasil .= '<td style="padding: 2px ;"> Menu-Atas </td>';
      }
      elseif( $h->posisi == 'menu_kanan' ){
      $hasil .= '<td style="padding: 2px ;"> Menu-Kanan </td>';
      }
      elseif( $h->posisi == ' independen' ){
      $hasil .= '<td style="padding: 2px ;"> Independen </td>';
      }
      else{
      $hasil .= '<td style="padding: 2px ;"> Unknown </td>';
      }
      if( $a > 1 ){
        $hasil .= '<td style="padding: 2px '.($a * 20).'px ;"> <a target="_blank" href="'.base_url().'dips/detail/'.$h->id_dip.'/'.str_replace(' ', '_', $h->judul_dip ).'.HTML">'.$h->judul_dip.' </a> </td>';
        }
			else{
        $hasil .= '<td style="padding: 2px ;"> '.$h->judul_dip.' </td>';
        }
      //$hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->urut.' </td>';
      if( $h->status == 99 ){
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> <a href="#" id="restore_ajax"><i class="fa fa-cut"></i> Restore</a> </td>';
      }
      else{
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> &nbsp; </td>';
      }
      
			$hasil .= '</tr>';
			$hasil = $this->dip_arsip($h->id_dip,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  
  public function simpan_dip()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_dip', 'judul_dip', 'required');
		$this->form_validation->set_rules('highlight', 'highlight', 'required');
		$this->form_validation->set_rules('tampil_menu', 'tampil_menu', 'required');
		$this->form_validation->set_rules('pilih_tampil_menu_atas', 'pilih_tampil_menu_atas', 'required');
    $this->form_validation->set_rules('informasi_berkala', 'informasi_berkala', 'required');
    $this->form_validation->set_rules('informasi_serta_merta', 'informasi_serta_merta', 'required');
    $this->form_validation->set_rules('informasi_setiap_saat', 'informasi_setiap_saat', 'required');
    $this->form_validation->set_rules('informasi_dikecualikan', 'informasi_dikecualikan', 'required');
		$this->form_validation->set_rules('parent', 'parent', 'required');
		$this->form_validation->set_rules('urut', 'urut', 'required');
		$this->form_validation->set_rules('posisi', 'posisi', 'required');
		$this->form_validation->set_rules('icon', 'icon', 'required');
		$this->form_validation->set_rules('kata_kunci', 'kata_kunci', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('isi_dip', 'isi_dip', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$this->form_validation->set_rules('tahun', 'tahun', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
			 $kata_kunci = ''.$this->input->post('kata_kunci').'';
      $data_input = array(
        'domain' => $web,
				'judul_dip' => trim($this->input->post('judul_dip')),
				'highlight' => trim($this->input->post('highlight')),
				'tampil_menu' => trim($this->input->post('tampil_menu')),
				'tampil_menu_atas' => trim($this->input->post('pilih_tampil_menu_atas')),
				'parent' => trim($this->input->post('parent')),
				'kata_kunci' => trim($this->input->post('kata_kunci')),
        'temp' => trim($this->input->post('temp')),
        'urut' => trim($this->input->post('urut')),
        'posisi' => trim($this->input->post('posisi')),
        'icon' => trim($this->input->post('icon')),
        'isi_dip' => trim($this->input->post('isi_dip')),
        'keterangan' => trim($this->input->post('keterangan')),
        'tahun' => trim($this->input->post('tahun')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
				'informasi_berkala' => trim($this->input->post('informasi_berkala')),
				'informasi_serta_merta' => trim($this->input->post('informasi_serta_merta')),
				'informasi_setiap_saat' => trim($this->input->post('informasi_setiap_saat')),
				'informasi_dikecualikan' => trim($this->input->post('informasi_dikecualikan')),
        'status' => 1

				);
      $table_name = 'dip';
      $id         = $this->Dip_model->simpan_dip($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'dip',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
   
  public function update_dip()
   {
    $web=$this->uut->namadomain(base_url());
    $this->form_validation->set_rules('judul_dip', 'judul_dip', 'required');
    $this->form_validation->set_rules('highlight', 'highlight', 'required');
    $this->form_validation->set_rules('tampil_menu', 'tampil_menu', 'required');
    $this->form_validation->set_rules('pilih_tampil_menu_atas', 'pilih_tampil_menu_atas', 'required');
    $this->form_validation->set_rules('informasi_berkala', 'informasi_berkala', 'required');
    $this->form_validation->set_rules('informasi_serta_merta', 'informasi_serta_merta', 'required');
    $this->form_validation->set_rules('informasi_setiap_saat', 'informasi_setiap_saat', 'required');
    $this->form_validation->set_rules('informasi_dikecualikan', 'informasi_dikecualikan', 'required');
		$this->form_validation->set_rules('urut', 'urut', 'required');
		$this->form_validation->set_rules('posisi', 'posisi', 'required');
		$this->form_validation->set_rules('icon', 'icon', 'required');
		$this->form_validation->set_rules('kata_kunci', 'kata_kunci', 'required');
		$this->form_validation->set_rules('tahun', 'tahun', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
				'judul_dip' => trim($this->input->post('judul_dip')),
				'highlight' => trim($this->input->post('highlight')),
				'tampil_menu' => trim($this->input->post('tampil_menu')),
				'tampil_menu_atas' => trim($this->input->post('pilih_tampil_menu_atas')),
				'informasi_berkala' => trim($this->input->post('informasi_berkala')),
				'informasi_serta_merta' => trim($this->input->post('informasi_serta_merta')),
				'informasi_setiap_saat' => trim($this->input->post('informasi_setiap_saat')),
				'informasi_dikecualikan' => trim($this->input->post('informasi_dikecualikan')),
				'tahun' => trim($this->input->post('tahun')),
				'parent' => trim($this->input->post('parent')),
				'kata_kunci' => trim($this->input->post('kata_kunci')),
        'temp' => trim($this->input->post('temp')),
        'urut' => trim($this->input->post('urut')),
        'posisi' => trim($this->input->post('posisi')),
        'icon' => trim($this->input->post('icon')),
        'isi_dip' => trim($this->input->post('isi_dip')),
        'keterangan' => trim($this->input->post('keterangan')),
	  	'created_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'dip';
      $where       = array(
        'dip.id_dip' => trim($this->input->post('id_dip')),
        'dip.domain' => $web
			);
      $this->Dip_model->update_data_dip($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'id_dip' => $this->input->post('id_dip'),
        'dip.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_dip');
      $result = $this->db->get('dip');
      echo json_encode($result->result_array());
		}
   
   public function total_dip()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $this->db->from('dip');
      $where    = array(
        'status' => 1,
        'domain' => $web
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dip = $this->input->post('id_dip');
      $where = array(
        'id_dip' => $id_dip,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('dip');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'dip';
        $this->Dip_model->update_data_dip($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function restore()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dip = $this->input->post('id_dip');
      $where = array(
        'id_dip' => $id_dip,
        'domain' => $web
        );
      $this->db->from('dip');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'dip';
        $this->Dip_model->update_data_dip($data_update, $where, $table_name);
        echo 1;
        }
		}
  
  
  public function inaktifkan_menu_atas()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dip = $this->input->post('id_dip');
      $where = array(
        'id_dip' => $id_dip,
		'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('dip');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'tampil_menu_atas' => 0
                  
          );
        $table_name  = 'dip';
        $this->Dip_model->update_data_dip($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function aktifkan_menu_atas()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dip = $this->input->post('id_dip');
      $where = array(
        'id_dip' => $id_dip,
		'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('dip');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'tampil_menu_atas' => 1
                  
          );
        $table_name  = 'dip';
        $this->Dip_model->update_data_dip($data_update, $where, $table_name);
        echo 1;
        }
		}
	
  public function inaktifkan_highlight()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dip = $this->input->post('id_dip');
      $where = array(
        'id_dip' => $id_dip,
        'domain' => $web
        );
      $this->db->from('dip');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'highlight' => 0
                  
          );
        $table_name  = 'dip';
        $this->Dip_model->update_data_dip($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function aktifkan_highlight()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dip = $this->input->post('id_dip');
      $where = array(
        'id_dip' => $id_dip,
        'domain' => $web
        );
      $this->db->from('dip');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'highlight' => 1
                  
          );
        $table_name  = 'dip';
        $this->Dip_model->update_data_dip($data_update, $where, $table_name);
        echo 1;
        }
		}
 }