<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Kebutuhan_pokok extends CI_Controller {

	function __construct()
		{
		parent::__construct();
    $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->model('Cetak_model');
    $this->load->model('Crud_model');
    $this->load->model('Kebutuhan_pokok_model');
		$this->load->model('login_model');
		}
  public function check_login()
   {
    $table_name = 'users';
    $where      = array(
      'users.id_users' => $this->session->userdata('id_users')
    );
    $a          = $this->Crud_model->semua_data($where, $table_name);
    if ($a == 0)
     {
        return redirect(''.base_url().'user/login');
     }
   }
	public function index()
		{
	    $data['main_view'] = 'kebutuhan_pokok/home';
      $this->load->view('back_bone', $data);
		}
  
  public function show_generate_data()
    {
      $this->check_login();
      $tanggal = $this->input->post('tanggal');
      $id_pasar = $this->input->post('id_pasar');
      $where = array(
          'komoditi.status' => '1',
      );
      $table_name='komoditi';
      $data = $this->Kebutuhan_pokok_model->show_data($where,$table_name);
      for($i=0;$i<sizeof($data);$i++) {
        $parent = $data[$i]['parent'];
        $id_komoditi = $data[$i]['id_komoditi'];
        $kemarin = date('Y-m-d', strtotime("-1 day", strtotime(date("Y-m-d"))));
        // $tanggal_kemarin = 
        $w = $this->db->query("SELECT * from harga_kebutuhan_pokok where id_komoditi='".$id_komoditi."' and tanggal='".$kemarin."' and status = 1 
        and id_pasar='".$id_pasar."' order by id_komoditi");
        foreach($w->result() as $h)
        {
          $harga = $h->harga;
          $id_satuan = $h->id_satuan;
          $keterangan = $h->keterangan;
        }
        $where_cek = array(
          'harga_kebutuhan_pokok.status' => '1',
          'harga_kebutuhan_pokok.id_komoditi' => $id_komoditi,
          'harga_kebutuhan_pokok.tanggal' => $tanggal,
          'harga_kebutuhan_pokok.id_pasar' => $id_pasar,
        );
        $cek = $this->Kebutuhan_pokok_model->show_data_generate_baru($where_cek);
        if(!$cek)
        {
			  // $kemarin = date('Y-m-d', strtotime("-1 day", strtotime(date("Y-m-d"))));
			  $data_input = array(
				  'harga_kebutuhan_pokok.id_komoditi' => $id_komoditi,
				  'harga_kebutuhan_pokok.id_pasar' => $id_pasar,
				  'harga_kebutuhan_pokok.tanggal' => $tanggal,
				  'harga_kebutuhan_pokok.harga' => $harga,
				  'harga_kebutuhan_pokok.id_satuan' => $id_satuan,
				  'harga_kebutuhan_pokok.keterangan' => $keterangan,
				  'harga_kebutuhan_pokok.status' => '1',
				  'harga_kebutuhan_pokok.inserted_by' => $this->session->userdata('id_users'),
				  'harga_kebutuhan_pokok.inserted_time' => date('Y-m-d H:i:s'),
			  );
			  $this->Kebutuhan_pokok_model->simpan_data_baru($data_input);
        }
      }
      $where = array(
          'harga_kebutuhan_pokok.status' => '1',
		  'harga_kebutuhan_pokok.tanggal' => $tanggal,
		  'harga_kebutuhan_pokok.id_pasar' => $id_pasar,
      );
      echo json_encode( $this->Kebutuhan_pokok_model->show_data_generate_baru($where) );
    }
  public function update_generate_data_baru()
		{
      $this->check_login();
      $jumlah_data = $this->input->post('jumlah_data');
      $tanggal = $this->input->post('tanggal');
      $id_pasar = $this->input->post('id_pasar');
      for($i=1;$i<=$jumlah_data;$i++)
      {
        ${'id_harga_kebutuhan_pokok_'.$i} = $this->input->post('id_harga_kebutuhan_pokok_'.$i.'');
        ${'harga_'.$i} = $this->input->post('harga_'.$i.'');
        ${'id_satuan_'.$i} = $this->input->post('id_satuan_'.$i.'');
        ${'tanggal_'.$i} = $this->input->post('tanggal_'.$i.'');
        ${'keterangan_'.$i} = $this->input->post('keterangan_'.$i.'');
        $where = array(
          'harga_kebutuhan_pokok.id_harga_kebutuhan_pokok' => ${'id_harga_kebutuhan_pokok_'.$i}
        );
        $data_update = array(
          'harga_kebutuhan_pokok.id_pasar' => $id_pasar,
          'harga_kebutuhan_pokok.tanggal' => $tanggal,
          'harga_kebutuhan_pokok.harga' => ${'harga_'.$i},
          'harga_kebutuhan_pokok.id_satuan' => ${'id_satuan_'.$i},
          'harga_kebutuhan_pokok.keterangan' => ${'keterangan_'.$i},
		  'harga_kebutuhan_pokok.updated_by' => $this->session->userdata('id_users'),
          'harga_kebutuhan_pokok.updated_time' => date('Y-m-d H:i:s')
        ); 
        $id=$this->Kebutuhan_pokok_model->update_data_baru($data_update, $where);
        echo $id;
      }
      
		}
  public function update_generate_data()
		{
      $jumlah_data = $this->input->post('jumlah_data');
      for($i=1;$i<=$jumlah_data;$i++)
      {
        ${'id_harga_kebutuhan_pokok_'.$i} = $this->input->post('id_harga_kebutuhan_pokok_'.$i.'');
        ${'harga_'.$i} = $this->input->post('harga_'.$i.'');
        ${'id_satuan_'.$i} = $this->input->post('id_satuan_'.$i.'');
        $where = array(
          'harga_kebutuhan_pokok.id_harga_kebutuhan_pokok' => ${'id_harga_kebutuhan_pokok_'.$i}
        );
        $data_update = array(
          'harga_kebutuhan_pokok.harga' => ${'harga_'.$i},
          'harga_kebutuhan_pokok.id_satuan' => ${'id_satuan_'.$i}
        ); 
        $this->Kebutuhan_pokok_model->update_data($data_update, $where);
      }
		}
  public function show_json_all()
    {
      $tanggal = $this->uri->segment(4);
      
          $where = array(
              'harga_kebutuhan_pokok.tanggal' => $tanggal,
              'harga_kebutuhan_pokok.status' => '1',
          );
      echo json_encode( $this->Kebutuhan_pokok_model->show_json_all($where) );
      
    }
	
  public function pilih_pasar(){
    $where1    = array(
      'pasar.status!=' => 99
    );
    $this->db->select("*");
	$this->db->where($where1);
	$b1 = $this->db->get('pasar');
    echo '<option value="-">Pilih Pasar</option>';
    foreach ($b1->result() as $r1){
      echo '<option value="'.$r1->id_pasar.'">'.$r1->nama_pasar.'</option>';
    }
  }
}