<?php defined('BASEPATH') OR exit('No direct script access allowed');



class Shortcut extends CI_Controller

{

  

  function __construct()

  {

    parent::__construct();    

    $this->load->library('Excel');

    $this->load->library('Uut');

  }

  

  public function index()

  {

    $data['test'] = '';

    $this->load->view('back_end/shortcut/content', $data);

  }

  

  public function json()

  {

    $table       = $this->input->post('table');

    $page        = $this->input->post('page');

    $limit       = $this->input->post('limit');

    $keyword     = $this->input->post('keyword');

    $status      = $this->input->post('status');

    $order_by    = $this->input->post('order_by');

    $order_field = $this->input->post('order_field');

    $shorting    = $this->input->post('shorting');

    $start       = ($page - 1) * $limit;

    $fields      = "

            shortcut.shortcut_id,

            shortcut.shortcut_code,

            shortcut.shortcut_name,

            shortcut.information,

            shortcut.urutan_menu,

            shortcut.created_by,

            shortcut.created_time,

            shortcut.updated_by,

            shortcut.updated_time,

            shortcut.deleted_by,

            shortcut.deleted_time,

            shortcut.status,

            

            (

            select attachment.file_name from attachment

            where attachment.id_tabel=shortcut.shortcut_id

            and attachment.table_name='" . $table . "'

            order by attachment.id_attachment DESC

            limit 1

            ) as file

            ";

    $where       = array(

      '' . $table . '.status' => $status

    );

    $this->db->select("$fields");

    if ($keyword == '') {

      $this->db->where("

            " . $table . ".status = " . $status . "

            ");

    } else {

      $this->db->where("

            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'

            AND

            " . $table . ".status = " . $status . "

            ");

    }

    $this->db->order_by($order_by, $shorting);

    $this->db->limit($limit, $start);

    $result = $this->db->get($table);

    echo json_encode($result->result_array());

  }

  

  public function total()

  {

    $table       = $this->input->post('table');

    $page        = $this->input->post('page');

    $limit       = $this->input->post('limit');

    $keyword     = $this->input->post('keyword');

    $status      = $this->input->post('status');

    $order_by    = $this->input->post('order_by');

    $shorting    = $this->input->post('shorting');

    $order_field = $this->input->post('order_field');

    $start       = ($page - 1) * $limit;

    $fields      = "

            shortcut.shortcut_id,

            shortcut.shortcut_code,

            shortcut.shortcut_name,

            shortcut.information,

            shortcut.urutan_menu,

            shortcut.created_by,

            shortcut.created_time,

            shortcut.updated_by,

            shortcut.updated_time,

            shortcut.deleted_by,

            shortcut.deleted_time,

            shortcut.status

            ";

    $where       = array(

      '' . $table . '.status' => $status

    );

    $this->db->select("$fields");

    if ($keyword == '') {

      $this->db->where("

            " . $table . ".status = " . $status . "

            ");

    } else {

      $this->db->where("

            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'

            AND

            " . $table . ".status = " . $status . "

            ");

    }

    $query = $this->db->get($table);

    $a     = $query->num_rows();

    echo trim(ceil($a / $limit));

  }

  

  public function simpan()

  {

    $this->form_validation->set_rules('shortcut_code', 'shortcut_code', 'required');

    $this->form_validation->set_rules('temp', 'temp', 'required');

    $this->form_validation->set_rules('shortcut_name', 'shortcut_name', 'required');

    $this->form_validation->set_rules('information', 'information', 'required');

    $this->form_validation->set_rules('urutan_menu', 'urutan_menu', 'required');

    if ($this->form_validation->run() == FALSE) {

      echo 0;

    } else {

      $data_input = array(

        'shortcut_code' => trim($this->input->post('shortcut_code')),

        'shortcut_name' => trim($this->input->post('shortcut_name')),

        'information' => trim($this->input->post('information')),

        'urutan_menu' => trim($this->input->post('urutan_menu')),

        'temp' => trim($this->input->post('temp')),

        'created_by' => $this->session->userdata('id_users'),
        
        'created_time' => date('Y-m-d H:i:s'),
        
        'updated_by' => 0,

        'updated_time' => date('Y-m-d H:i:s'),

        'deleted_by' => 0,

        'deleted_time' => date('Y-m-d H:i:s'),

        'status' => 1

      );

      $this->db->insert( 'shortcut', $data_input );

      $id= $this->db->insert_id();

      echo '' . $id . '';

      $where       = array(

        'table_name' => 'shortcut',

        'temp' => trim($this->input->post('temp'))

      );

      $data_update = array(

        'id_tabel' => $id

      );

      $this->db->where($where);

      $this->db->update('attachment', $data_update);

    }

  }

  

  public function update()

  {

    $this->form_validation->set_rules('shortcut_id', 'shortcut_id', 'required');

    $this->form_validation->set_rules('shortcut_code', 'shortcut_code', 'required');

    $this->form_validation->set_rules('shortcut_name', 'shortcut_name', 'required');

    $this->form_validation->set_rules('information', 'information', 'required');

    $this->form_validation->set_rules('urutan_menu', 'urutan_menu', 'required');

    if ($this->form_validation->run() == FALSE) {

      echo 0;

    } else {

      $data_update = array(

        'shortcut_code' => trim($this->input->post('shortcut_code')),

        'shortcut_name' => trim($this->input->post('shortcut_name')),

        'information' => trim($this->input->post('information')),

        'urutan_menu' => trim($this->input->post('urutan_menu')),

        'updated_by' => $this->session->userdata('id_users'),

        'updated_time' => date('Y-m-d H:i:s')

      );

      $where       = array(

        'shortcut_id' => trim($this->input->post('shortcut_id'))

      );

      $this->db->where($where);

      $this->db->update('shortcut', $data_update);

      echo 1;

    }

  }

  

  public function get_by_id()

  {

    $shortcut_id = $this->input->post('shortcut_id');

    $where   = array(

      'shortcut.shortcut_id' => $shortcut_id

    );

    $this->db->select("*");    	

    $this->db->where($where);

		$result = $this->db->get('shortcut');

    echo json_encode($result->result_array());

  }

  

  public function delete_by_id()

  {

    $this->form_validation->set_rules('shortcut_id', 'shortcut_id', 'required');

    if ($this->form_validation->run() == FALSE) {

      echo 0;

    } else {

      $data_update = array(

        'status' => 99

      );

      $where       = array(

        'shortcut_id' => trim($this->input->post('shortcut_id'))

      );

      $table_name  = 'shortcut';

      $this->db->where($where);

      $this->db->update('shortcut', $data_update);

      echo 1;

    }

  }

  

  public function restore_by_id()

  {

    $this->form_validation->set_rules('shortcut_id', 'shortcut_id', 'required');

    if ($this->form_validation->run() == FALSE) {

      echo 0;

    } else {

      $data_update = array(

        'status' => 1

      );

      $where       = array(

        'shortcut_id' => trim($this->input->post('shortcut_id'))

      );

      $table_name  = 'shortcut';

      $this->db->where($where);

      $this->db->update($table_name, $data_update);

      echo 1;

    }

  }

  

  public function xls()

	{

		$this->load->library('excel');

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()

			->setCreator("Budi Utomo")

			->setLastModifiedBy("Budi Utomo")

			->setTitle("Laporan I")

			->setSubject("Office 2007 XLSX Document")

			->setDescription("Laporan Shortcut")

			->setKeywords("Laporan Halaman")

			->setCategory("Bentuk XLS");

		$objPHPExcel->setActiveSheetIndex(0)

			->setCellValue('A1', 'Shortcut')

			->mergeCells('A1:G1')

			->setCellValue('A2', 'Data Shortcut')

			->mergeCells('A2:E2')

			->setCellValue('A3', 'Tanggal Download : '.date('d/m/Y').' ')

			->mergeCells('A3:E3')			

			->setCellValue('A6', 'No')

			->setCellValue('B6', 'Shortcut Code')

			->setCellValue('C6', 'Shortcut Name')

			->setCellValue('D6', 'Information')

			->setCellValue('E6', 'Status')      

      ;

		

    $page        = $this->input->get('page');

    $limit       = $this->input->get('limit');

    $keyword     = $this->input->get('keyword');

    $status      = $this->input->get('status');

    $order_by    = $this->input->get('order_by');

    $order_field = $this->input->get('order_field');

    $shorting    = $this->input->get('shorting');

    $start       = ($page - 1) * $limit;

    $fields     = "

      shortcut.shortcut_id,

      shortcut.shortcut_code,

      shortcut.shortcut_name,

      shortcut.information,

      shortcut.created_by,

      shortcut.created_time,

      shortcut.updated_by,

      shortcut.updated_time,

      shortcut.deleted_by,

      shortcut.deleted_time,

      shortcut.status

    

    ";

    $this->db->select("$fields");

    if ($keyword == '') {

      $this->db->where("

            shortcut.status = " . $status . "

            ");

    } else {

      $this->db->where("

            shortcut.shortcut_name LIKE '%" . $keyword . "%'

            AND

            shortcut.status = " . $status . "

            ");

    }

    $this->db->order_by($order_by, $shorting);

    $this->db->limit($limit, $start);

		$b= $this->db->get('shortcut');    

    $i = 7;

    $start = (($page - 1) * $limit);

		foreach ($b->result() as $b1) {

      $start = $start + 1;

      $status=$b1->status;

      if( $status == 1 ){

        $statusnya='Belum Terverifikasi';

      }

      else if( $status == 2 ){

        $statusnya='Sudah Terverifikasi';

      }

      else if( $status == 99 ){

        $statusnya='Batal';

      }

      else{

        $statusnya='-';

      }

			$objPHPExcel->setActiveSheetIndex(0)

			->setCellValue('A'.$i, ''.$start.'')

			->setCellValueExplicit('B'.$i, ''.$b1->shortcut_code.'', PHPExcel_Cell_DataType::TYPE_STRING)

			->setCellValueExplicit('C'.$i, ''.$b1->shortcut_name.'', PHPExcel_Cell_DataType::TYPE_STRING)

			->setCellValueExplicit('D'.$i, ''.$b1->information.'', PHPExcel_Cell_DataType::TYPE_STRING)

			->setCellValueExplicit('E'.$i, ''.$statusnya.'', PHPExcel_Cell_DataType::TYPE_STRING)

      

      ;

			$objPHPExcel->getActiveSheet()

			->getStyle('A'.$i.':E'.$i.'')

			->getBorders()->getAllBorders()

			->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

			$i++;

		} 

		

		$objPHPExcel->getActiveSheet()->setTitle('Data Shortcut');

		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);

		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);

		

		$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFont()->setBold(true);

		

		$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);

		$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getFill()->getStartColor()->setARGB('cccccc');

								

		$objPHPExcel->getActiveSheet()->getStyle('A6:E6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

								

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);

		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);

		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);

		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(25);

		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(25);

		

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet

		$objPHPExcel->setActiveSheetIndex(0);

		// Redirect output to a client’s web browser (Excel5)

		header('Content-Type: application/vnd.ms-excel');

		header('Content-Disposition: attachment;filename="Shortcut_'.date('ymdhis').'.xls"');

		header('Cache-Control: max-age=0');

		// If you're serving to IE 9, then the following may be needed

		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed

		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past

		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified

		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1

		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

		$objWriter->save('php://output');

				

	}

  

}