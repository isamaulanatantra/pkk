<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
   
  
  function __construct()
    {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('email');
    $this->load->library('Uut');
    }
    
  public function index()
  {

    $web = $this->uut->namadomain(base_url());
    $wheredasar_website = array(
      'domain' => $web
    );
    $this->db->where($wheredasar_website);
    $this->db->limit(1);
    $querydasar_website = $this->db->get('dasar_website');
    foreach ($querydasar_website->result() as $rowdasar_website) {
      $data['domain'] = $rowdasar_website->domain;
      $data['alamat'] = $rowdasar_website->alamat;
      $data['telpon'] = $rowdasar_website->telpon;
      $data['email'] = $rowdasar_website->email;
      $data['twitter'] = $rowdasar_website->twitter;
      $data['facebook'] = $rowdasar_website->facebook;
      $data['google'] = $rowdasar_website->google;
      $data['instagram'] = $rowdasar_website->instagram;
      $data['peta'] = $rowdasar_website->peta;
      $data['title'] = $rowdasar_website->keterangan;
    }
    $fields0     = "*";
    $where0      = array(
      'users.id_users' => $this->session->userdata('id_users'),
      'module_role.status' => 1,
      'role_user.status' => 1
    );
    $this->db->join('role_user', 'role_user.user_id=users.id_users');
    $this->db->join('module_role', 'module_role.role_id=role_user.role_id');
    $this->db->join('module', 'module.module_id=module_role.module_id');
    $this->db->where($where0);
    $this->db->select("$fields0");
    $this->db->order_by('module.urutan_menu', 'asc');
    $data['menu'] = $this->db->get('users');
    /* Role */
    $fieldsrole     = "*";
    $whererole      = array(
      'role.status' => 1
    );
    $this->db->where($whererole);
    $this->db->select("$fieldsrole");
    $this->db->order_by('role.role_id', 'asc');
    foreach ($this->db->get('role')->result() as $r_role) {
      $fields     = "*";
      $where      = array(
        'users.id_users' => $this->session->userdata('id_users'),
        'module_role.role_id' => $r_role->role_id,
        'module_role.status' => 1,
        'role_user.status' => 1
      );
      $this->db->join('role_user', 'role_user.user_id=users.id_users');
      $this->db->join('module_role', 'module_role.role_id=role_user.role_id');
      $this->db->join('module', 'module.module_id=module_role.module_id');
      $this->db->where($where);
      $this->db->select("$fields");
      $this->db->order_by('module.urutan_menu', 'asc');
      $data['menu_' . $r_role->role_code . ''] = $this->db->get('users');
      $data['role_name'] = $r_role->role_name;
    }
    /* Role */
    $fieldsroles = "*";
    $whereroles = array(
      'role.status' => 1
    );
    $this->db->where($whereroles);
    $this->db->select("$fieldsroles");
    $this->db->order_by('role.role_id', 'asc');
    $data['roles'] = $this->db->get('role');

    $fields1     =
      "
            user_profile.first_name,
            user_profile.last_name,
            
            users.created_time,
            
            (
            select attachment.file_name from attachment
            where attachment.id_tabel=users.id_users
            and attachment.table_name='foto_profil'
            order by attachment.id_attachment DESC
            limit 1
            ) as foto
            
            ";

    $where1      = array(
      'users.id_users' => $this->session->userdata('id_users')
    );
    $this->db->join('user_profile', 'user_profile.user_id=users.id_users');
    $this->db->where($where1);
    $this->db->select("$fields1");
    foreach ($this->db->get('users')->result() as $b2) {
      $data['first_name'] = $b2->first_name;
      $data['last_name'] = $b2->last_name;
      $ex = explode(' ', $b2->created_time);
      $data['membersince'] = $ex[0];
      if ($b2->foto == null) {
        $data['foto'] = '' . base_url() . 'Template/AdminLTE-2.4.3/dist/img/user4-128x128.jpg';
      } else {
        $data['foto'] = '' . base_url() . 'media/upload/s_' . $b2->foto . '';
      }
    }
    $a = 0;
    $data['menu_atas'] = $this->menu_atas_front(0, $h = "", $a);
    $data['page'] = 'front_end/job/signup.php';
    $data['id'] = '';
    $data['web'] = ''.$web.'';
    $this->load->view('sosegov', $data);
    // $this->load->view('nav_top', $data);
    // $this->load->view('back_end', $data);
  }

  private function menu_atas_front($parent = NULL, $hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    if ($web == 'disparbud.wonosobokab.go.id' or $web == 'dikpora.wonosobokab.go.id') {
      $url_baca='front/details';
      $url_baca_list='front/details';
    }else{
      $url_baca='front/details';
      $url_baca_list='front/details';
    }
    $a = $a + 1;
    $w = $this->db->query("
    SELECT * from posting
    where posisi='menu_atas'
    and parent='" . $parent . "' 
    and status = 1 
    and tampil_menu_atas = 1 
    and domain='" . $web . "'
    order by urut
    ");
    $x = $w->num_rows();
    if (($w->num_rows()) > 0) {
      if ($parent == 0) {
        if($web=='demoopd.wonosobokab.go.id'){
          $hasil .= '
          <ul class="navbar-nav u-header__navbar-nav">
          <!-- Home -->
          <li class="nav-item hs-has-mega-menu u-header__nav-item"
              data-event="hover"
              data-animation-in="slideInUp"
              data-animation-out="fadeOut"
              data-position="left">
            <a id="homeMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false">Home</a>
  
            <!-- Home - Mega Menu -->
            <div class="hs-mega-menu w-100 u-header__sub-menu" aria-labelledby="homeMegaMenu">
              <div class="row no-gutters">
                <div class="col-lg-6">
                  <!-- Banner Image -->
                  <div class="u-header__banner" style="background-image: url('.base_url().'media/upload/branding750x750.jpg);">
                    <div class="u-header__banner-content">
                      <div class="mb-4">
                        <span class="u-header__banner-title">Branding Works</span>
                        <p class="u-header__banner-text">Experience a level of our quality in both design &amp; customization works.</p>
                      </div>
                      <a class="btn btn-primary btn-sm transition-3d-hover" href="#">Learn More <span class="fas fa-angle-right ml-2"></span></a>
                    </div>
                  </div>
                  <!-- End Banner Image -->
                </div>
  
                <div class="col-lg-6">
                  <div class="row u-header__mega-menu-wrapper">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                      <span class="u-header__sub-menu-title">Classic</span>
                      <ul class="u-header__sub-menu-nav-group mb-3">
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="'.base_url().'">Classic Agency</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-business.html">Classic Business</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-marketing.html">Classic Marketing</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-consulting.html">Classic Consulting</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-start-up.html">Classic Start-up</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-studio.html">Classic Studio <span class="badge badge-success badge-pill ml-1">New</span></a></li>
                      </ul>
  
                      <span class="u-header__sub-menu-title">Corporate</span>
                      <ul class="u-header__sub-menu-nav-group mb-3">
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="corporate-agency.html">Corporate Agency</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="corporate-start-up.html">Corporate Start-Up</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="corporate-business.html">Corporate Business</a></li>
                      </ul>
  
                      <span class="u-header__sub-menu-title">Portfolio</span>
                      <ul class="u-header__sub-menu-nav-group">
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="portfolio-agency.html">Portfolio Agency</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="portfolio-profile.html">Portfolio Profile</a></li>
                      </ul>
                    </div>
  
                    <div class="col-sm-6">
                      <span class="u-header__sub-menu-title">App</span>
                      <ul class="u-header__sub-menu-nav-group mb-3">
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="app-ui-kit.html">App UI kit</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="app-saas.html">App SaaS</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="app-workflow.html">App Workflow</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="app-payment.html">App Payment</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="app-software.html">App Software</a></li>
                      </ul>
  
                      <span class="u-header__sub-menu-title">Onepages</span>
                      <ul class="u-header__sub-menu-nav-group mb-3">
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="onepage-corporate.html">Corporate <span class="badge badge-success badge-pill ml-1">New</span></a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="onepage-creative.html">Creative</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="onepage-saas.html">SaaS</a></li>
                      </ul>
  
                      <span class="u-header__sub-menu-title">Blog</span>
                      <ul class="u-header__sub-menu-nav-group">
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="blog-agency.html">Blog Agency</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="blog-start-up.html">Blog Start-Up</a></li>
                        <li><a class="nav-link u-header__sub-menu-nav-link" href="blog-business.html">Blog Business</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Home - Mega Menu -->
          </li>
          <!-- End Home -->
          '; 
        }
        else{
          $hasil .= '
          <ul id="pagesMegaMenu" class="hs-sub-menu u-header__sub-menu animated" aria-labelledby="pagesMegaMenu" style="min-width: 230px; display: none;">
         ';
         }
      } else {
        $hasil .= '
         <ul id="pagesMegaMenu" class="hs-sub-menu u-header__sub-menu animated" aria-labelledby="pagesMegaMenu" style="min-width: 230px; display: none;">
        ';
      }
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $r = $this->db->query("
      SELECT * from posting
      where parent='" . $h->id_posting . "' 
      and status = 1 
      and tampil_menu_atas = 1 
      and domain='" . $web . "'
      order by urut
      ");
      $xx = $r->num_rows();
      $nomor = $nomor + 1;
      if ($a > 1) {
        if ($xx == 0) {
          if ($h->judul_posting == 'Pengaduan Masyarakat') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'sosegov"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Permohonan Informasi Publik') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'sosegov"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Permohonan Informasi') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'sosegov"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Profil') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/details/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Sitemap') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Berita') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Pengumuman') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Peraturan Daerah') {
						$where6      = array(
							'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'perda'
						);
						$this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");    	
						$this->db->where($where6);
						$this->db->limit(1);
						$query6 = $this->db->get('jenis_perundang_undangan');
						$a6 = $query6->num_rows();
						if( $a6 == 0 ){
								$jenis_perundang_undangan='';
						}
						else{
							foreach ($query6->result() as $b6) {
								$jenis_perundang_undangan=$b6->jenis_perundang_undangan_id;
							}
						}
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id='.$jenis_perundang_undangan.'"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Peraturan Bupati') {
						$where6      = array(
							'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'perbup'
						);
						$this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");    	
						$this->db->where($where6);
						$this->db->limit(1);
						$query6 = $this->db->get('jenis_perundang_undangan');
						$a6 = $query6->num_rows();
						if( $a6 == 0 ){
								$jenis_perundang_undangan='';
						}
						else{
							foreach ($query6->result() as $b6) {
								$jenis_perundang_undangan=$b6->jenis_perundang_undangan_id;
							}
						}
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id='.$jenis_perundang_undangan.'"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Keputusan DPRD') {
						$where6      = array(
							'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'kepdprd'
						);
						$this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");    	
						$this->db->where($where6);
						$this->db->limit(1);
						$query6 = $this->db->get('jenis_perundang_undangan');
						$a6 = $query6->num_rows();
						if( $a6 == 0 ){
								$jenis_perundang_undangan='';
						}
						else{
							foreach ($query6->result() as $b6) {
								$jenis_perundang_undangan=$b6->jenis_perundang_undangan_id;
							}
						}
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id='.$jenis_perundang_undangan.'"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Peraturan Desa') {
						$where6      = array(
							'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'perdes'
						);
						$this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");    	
						$this->db->where($where6);
						$this->db->limit(1);
						$query6 = $this->db->get('jenis_perundang_undangan');
						$a6 = $query6->num_rows();
						if( $a6 == 0 ){
								$jenis_perundang_undangan='';
						}
						else{
							foreach ($query6->result() as $b6) {
								$jenis_perundang_undangan=$b6->jenis_perundang_undangan_id;
							}
						}
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id='.$jenis_perundang_undangan.'"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } else {
            $hasil .= '
              <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . '' . $url_baca . '/' . $h->id_posting . '">' . $h->judul_posting . '</a>
            ';
          }
        } else {
          $hasil .= '
        <li class="nav-item hs-has-sub-menu u-header__nav-item"
            data-event="hover"
            data-animation-in="slideInUp"
            data-animation-out="fadeOut">
          <a id="pagesMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="pagesSubMenu">' . $h->judul_posting . '</a>

        ';
        }
      } else {

        if ($xx == 0) {
          if ($h->judul_posting == 'Prestasi Entitas Pendidikan') {
            $hasil .= '
              <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/details/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
              ';
          } elseif ($h->judul_posting == 'Sitemap') {
            $hasil .= '
            <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
            ';
          } elseif ($h->judul_posting == 'Berita') {
            $hasil .= '
            <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
            ';
          } elseif ($h->judul_posting == 'Pengumuman') {
            $hasil .= '
            <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
            ';
          } else {
            $hasil .= '
              <li class="nav-item hs-has-sub-menu u-header__nav-item"><a class="nav-link u-header__nav-link" href="' . base_url() . '' . $url_baca . '/' . $h->id_posting . '">' . $h->judul_posting . '</a>
              ';
          }
        } else {
          $hasil .= '
            <li class="nav-item hs-has-sub-menu u-header__nav-item"
                data-event="hover"
                data-animation-in="slideInUp"
                data-animation-out="fadeOut">
              <a id="pagesMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="pagesSubMenu">' . $h->judul_posting . '</a>
    
            ';
        }
      }
      $hasil = $this->menu_atas_front($h->id_posting, $hasil, $a);
      $hasil .= '
      </li>
      ';
    }
    if (($w->num_rows) > 0) {
      $hasil .= "
    </ul>
    ";
    } else { }
    return $hasil;
  }
  private function highlight_layanan_front($hasil, $a){
    $web=$this->uut->namadomain(base_url());
    if($web=='demoopd.wonosobokab.go.id'){
      $web='diskominfo.wonosobokab.go.id';
    }else{
      $web;
    }
		$a = $a + 1;
		$w = $this->db->query("
		SELECT permohonan_informasi_publik.id_permohonan_informasi_publik, permohonan_informasi_publik.created_time, permohonan_informasi_publik.nama as nama_pembuat, permohonan_informasi_publik.instansi, permohonan_informasi_publik.rincian_informasi_yang_diinginkan, permohonan_informasi_publik.domain, permohonan_informasi_publik.tujuan_penggunaan_informasi
		from permohonan_informasi_publik
		where permohonan_informasi_publik.status = 1 
		and permohonan_informasi_publik.parent = 0
		and permohonan_informasi_publik.domain = '".$web."'
		order by permohonan_informasi_publik.created_time desc
		limit 1
		");
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
			$yummy   = array("_","","","","","","","","","","","","","","","");
			$created_time = ''.$this->Crud_model->dateBahasaIndotok($h->created_time).'';
				$hasil .= '
					<div class="js-slide">
						<!-- Testimonials -->
						<div class="w-md-80 w-lg-60 text-center mx-auto">
							<blockquote class="h5 text-white font-weight-normal mb-4">'.substr(strip_tags($h->rincian_informasi_yang_diinginkan), 0, 150).'</blockquote>
							<h1 class="h6 text-white-70">'.$h->nama_pembuat.'</h1>
						</div>
						<!-- End Testimonials -->
					</div>
				';
		}
		return $hasil;
  }
  
  public function proses_register()
  {
    $this->form_validation->set_rules('email', 'email', 'required');
    $this->form_validation->set_rules('password', 'password', 'required');
    $this->form_validation->set_rules('nomot_telp', 'nomot_telp', 'required');
    $this->form_validation->set_rules('jenis_kelamin', 'jenis_kelamin', 'required');
    $this->form_validation->set_rules('tanggal_lahir', 'tanggal_lahir', 'required');
    $this->form_validation->set_rules('nama', 'nama', 'required');
    $this->form_validation->set_rules('nik', 'nik', 'required');
    
    $web = $this->uut->namadomain(base_url());
    
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $where = array(
            'user_name' => $email
            );
    $this->db->select("*");
    $this->db->where($where);
		$query = $this->db->get('users');
		$a = $query->num_rows();    
    
    $where1 = array(
            'nik' => trim($this->input->post('nik'))
            );
    $this->db->select("*");
    $this->db->where($where1);
		$query = $this->db->get('register');
		$a1 = $query->num_rows();    
    if( $a1 <> 0 ){
      echo 'NIK Sudah digunakan';
      exit;
    }
    
    $t=explode('/', $this->input->post('tanggal_lahir'));
    if( count($t) == 3 ){
      $tl=''.$t[2].'-'.$t[1].'-'.$t[0].'';
    }
    else{
        echo 'Tanggal lahir tidak valid';
        exit;
    }
    if ($this->form_validation->run() == FALSE)
      {
        echo 'Semua isian harus diisi';
      }
    else
      {
        if( $a == 0 ){
					$id_skpd=0;
					$id_kecamatan=0;
					$r = $this->db->query("
					SELECT * from data_skpd
					where skpd_website='".trim($this->input->post('domain'))."'
					");
					foreach($r->result() as $h){$id_skpd = $h->id_skpd; $id_kecamatan = $h->id_kecamatan;}
          $password = $this->bcrypt->hash_password($this->input->post('password'));
          $temp=date('YmdHis');
          $data_input = array(      
          'user_name' => trim(strtolower($this->input->post('email'))),
          'password' => trim($password),
          'keterangan' => trim('Web Registration'),
					'hak_akses' => trim('register'),
          'temp' => trim($temp),
          'nip' => '',
          'jabatan' => '',
          'golongan' => '',
          'pangkat' => '',
					'id_skpd' => $id_skpd,
					'id_propinsi' => 1,
					'id_kabupaten' => 1,
					'id_kecamatan' => 0,
					'id_desa' => 0,
          'nama' => trim($this->input->post('nama')),
          'created_by' => '0',
          'created_time' => date('Y-m-d H:i:s'),
					'updated_by' => 0,
					'updated_time' => date('Y-m-d H:i:s'),
					'deleted_by' => 0,
					'deleted_time' => date('Y-m-d H:i:s'),
					'last_login' => date('Y-m-d H:i:s'),
					'activated_time' => date('Y-m-d H:i:s'),
          'status' => 0
          );
          $this->db->insert( 'users', $data_input );
          $id= $this->db->insert_id();
          
          $data_inputx = array(
            'id_users' => trim($id),
            'keterangan' => trim('Anggota melakukan registrasi dengan email: '.trim($this->input->post('email')).', Melalui website: '.$web.' '),
            'domain' => ''.$web.'',
            'hak_akses' => trim('register'),
            'temp' => trim($temp),
            'created_by' => trim($id),
            'created_time' => date('Y-m-d H:i:s'),
						'updated_by' => 0,
						'updated_time' => date('Y-m-d H:i:s'),
            'status' => 1								
          );
          $this->db->insert( 'tracker_users', $data_inputx );
          
          $data_input = array(      
          'role_id' => trim(5),
          'user_id' => trim($id),
          'temp' => trim($temp),
          'information' => 'register online',
          'created_by' => '0',
          'created_time' => date('Y-m-d H:i:s'),
          'status' => 1								
          );
          $this->db->insert( 'role_user', $data_input );
          
          $data_input = array(      
          'nama' => trim($this->input->post('nama')),
          'jenis_kelamin' => trim($this->input->post('jenis_kelamin')),
          'nik' => trim($this->input->post('nik')),
          'email' => trim($this->input->post('email')),
          'nomot_telp' => trim($this->input->post('nomot_telp')),
          'tanggal_lahir' => trim($tl),
          'keterangan' => trim('Anggota melakukan registrasi dengan email: '.trim($this->input->post('email')).', Melalui website: '.$web.' '),
          'id_users' => trim($id),
          'temp' => trim($temp),
          'domain' => ''.$web.'',
          'alamat' => '', 
          'id_kecamatan' => 0, 
          'id_desa' => 0, 
          'temp' => $temp, 
          'created_by' => '0', 
          'created_time' => date('Y-m-d H:i:s'),
					'updated_by' => 0,
					'updated_time' => date('Y-m-d H:i:s'),
					'deleted_by' => 0,
					'deleted_time' => date('Y-m-d H:i:s'),
          'status' => 1								
          );
          $this->db->insert( 'register', $data_input );
          
          echo 'OK';
          $aa=explode(".",$web);
          $this->email->from('huda.tolhas@gmail.com', ''.$aa[0].'');
          $this->email->to(''.$this->input->post('email').'');
          $this->email->subject('Registrasi');
          $this->email->set_header('MIME-Version', '1.0; charset=utf-8');
          $this->email->set_header('Content-type', 'text/html');
          $this->email->message('
          Anda telah melakukan registrasi '.$web.', Silahkan <a href="'.base_url().'activated/?c='.trim($temp).'&e='.trim($this->input->post('email')).'">Klik disini Untuk Aktifasi</a>
          <br>
          Atau copikan link ini '.base_url().'activated/?c='.trim($temp).'&e='.trim($this->input->post('email')).' pada browser anda
          ');
          $this->email->send();
        }
        else{
          echo 'Email sudah digunakan';
        }
      }
  }
  
}
