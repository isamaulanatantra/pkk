<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ppid extends CI_Controller
 {
    
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Posting_model');
    $this->load->model('Komponen_model');
    $this->load->model('Halaman_model');
   }
  
  public function get_attachment_by_id_posting($id_posting)
   {
    $where = array(
      'id_tabel' => $id_posting
      );
    $d = $this->Posting_model->get_attachment_by_id_posting($where);
    if(!$d)
      {
        return 'blankgambar.jpg';
      }
    else
      {
        return $d['file_name'];
      }
   }
   
  private function menu_atas($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
        if($web == 'zb.wonosobokab.go.id'){
        $hasil .= '<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'website" class="fa-home ">HOME</a></li>'; 
        }
        elseif ($web == 'ppid.wonosobokab.go.id'){
					$hasil .= '
						<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'">HOME</a></li>
						<li class="parent"> <span class="">PPID </span>
							<ul>
								<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid">PPID </a></span></li>
								<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid_pembantu">PPID Pembantu</a></span></li>
							</ul>
						</li>
					';
        }
        else{
        $hasil .= '<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'">HOME</a></li>'; 
        }
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
        $hasil .= '
          <li> <span class=""> <a href="'.base_url().'postings/faqs/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </span></a>';
        }
			else{
        if($h->judul_posting == 'Berita'){
            $hasil .= '
              <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
        }
        else{
          if ($xx == 0){
            $hasil .= '
              <li><a href="'.base_url().'postings/faqs/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
          }
          else{
            $hasil .= '
              <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
          } 
        }
      }
      
			$hasil = $this->menu_atas($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  
  private function menu_kiri($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    and tampil_menu = 1
    and posisi='menu_kiri'
    order by urut asc, created_time desc limit 10
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
        $hasil .= '<ul class="list-group sidebar-nav" id="sidebar-nav">';
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
		and status = 1 
    and domain='".$web."'
    and tampil_menu = 1
    and posisi='menu_kiri'
    order by urut
    
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
          if ($xx == 0){
            $hasil .= '<li class="list-group-item"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'">'.$h->judul_posting.' </a>';
            }
          else{
            $hasil .= '<li class="list-group-item list-toggle"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'"><i class="fa '.$h->icon.'"></i>  '.$h->judul_posting.' </a>';
            }
        }
			else{
          if ($xx == 0){
            $hasil .= '<li class="list-group-item"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'">  '.$h->judul_posting.' </a>';
            }
          else{
            $hasil .= '<li class="list-group-item list-toggle"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'"><i class="fa '.$h->icon.'"></i>  '.$h->judul_posting.' </a>';
            }
        }
			$hasil = $this->menu_kiri($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= '</ul>';
		}
    else{
      
    }
		
		return $hasil;
	}
  
  public function get_komponen($judul_komponen)
   {
    $web=$this->uut->namadomain(base_url());
     $where = array(
						'judul_komponen' => $judul_komponen,
						'status' => 1
						);
    $d = $this->Komponen_model->get_data($where);
    if(!$d)
          {
						return '';
          }
        else
          {
						return $d['isi_komponen'];
          }
   }
  
  public function index()
   {
    $web=$this->uut->namadomain(base_url());
    $a = 0;
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
    $data['menu_kiri'] = $this->menu_kiri(0,$h="", $a);
    $data['welcome1'] = $this->get_komponen('welcome1');
    $data['welcome2'] = $this->get_komponen('welcome2');
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $data['slide_show'] = $this->get_komponen('slide_show');
		$data['judul'] = 'Selamat datang';
		$data['main_view'] = 'ppid/home';
		$this->load->view('halaman', $data);
   }
   
}