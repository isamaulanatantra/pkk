<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dashboard extends CI_Controller {
    
  function __construct(){
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Posting_model');
    $this->load->model('Komoditi_model');
    $this->load->model('Dashboard_model');
	}
	
  function index(){
      $a = 0;
      $web=$this->uut->namadomain(base_url());
      $aa=explode(".",$web);
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['galery_berita'] = $this->option_posting_terbarukan($h="", $a);
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'dashboard/'.$aa[0].'';
      $this->load->view('dashboard', $data); 
  }
  function disparbud_kebudayaan(){
      $a = 0;
      $web=$this->uut->namadomain(base_url());
      $aa=explode(".",$web);
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['galery_berita'] = $this->option_posting_terbarukan($h="", $a);
      $data['judul'] = 'Selamat datang';
      // $data['main_view'] = 'dashboard/disparbud/'.$aa[0].'';
      $data['main_view'] = 'dashboard/disparbud/kebudayaan';
      $this->load->view('dashboard', $data); 
  }
  function kebudayaan(){
      $a = 0;
      $web=$this->uut->namadomain(base_url());
      $aa=explode(".",$web);
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['galery_berita'] = $this->option_posting_terbarukan($h="", $a);
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'dashboard/kebudayaan';
      $this->load->view('dashboard', $data); 
  }
  function details(){
      $a = 0;
      $web=$this->uut->namadomain(base_url());
      $aa=explode(".",$web);
      $where = array(
        'id_komoditi' => $this->uri->segment(3)
        );
      $d = $this->Komoditi_model->get_data($where);
      if(!$d){
          $data['id_komoditi'] = '';
          $data['judul_komoditi'] = '';
          $data['parent'] = '';
          $data['gambar'] = '';
        }
      else{
          $data['id_komoditi'] = $d['id_komoditi'];
          $data['judul_komoditi'] = $d['judul_komoditi'];
          $data['parent'] = $d['parent'];
          $data['gambar'] = $this->get_attachment_by_id_komoditi($d['id_komoditi']);
        }
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['galery_berita'] = $this->option_posting_terbarukan($h="", $a);
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'dashboard/detail_'.$aa[0].'';
      $this->load->view('dashboard', $data); 
  }
	public function json_all_disparbud(){
        $web=$this->uut->namadomain(base_url());
        $w = $this->db->query("
        SELECT id_permohonan_organisasi
        from permohonan_organisasi
        where status = 1 and domain = '".$web."'
        ");
        $total_permohonan_organisasi = $w->num_rows();
        $w1 = $this->db->query("
        SELECT id_cagar_budaya
        from cagar_budaya
        where status = 1 and domain = '".$web."'
        ");
        $total_cagar_budaya = $w1->num_rows();
        $w2 = $this->db->query("
        SELECT id_ekonomi_kreatif
        from ekonomi_kreatif
        where status = 1 and domain = '".$web."'
        ");
        $total_ekonomi_kreatif = $w2->num_rows();
				echo '
        <div class="col-md-12" style="padding: 2px;">
          <H3>KEBUDAYAAN DAN EKONOMI KREATIF</H3>
        </div>
        <div class="col-md-4" style="padding: 2px;">
          <div class="panel" style="background-color: #6cbcfc !important; color: white;">
            <div class="panel-body" style="background-color: #6cbcfc !important; color: white;">
              <div class="row">
              <div class="col-xs-8">
                <div style="height: 110px;">
                  <b style="font-size:20px; color:#fff">DATA KELOMPOK KESENIAN</b><br />
                  <b style="font-size:26px;"><u>'.$total_permohonan_organisasi.'</u></b>
                </div>
              </div>
              <div class="col-xs-4"><img src="'.base_url().'media/logo wonosobo.png" style="height:80px; width:60;"></div>
              </div>
            </div>
            <div class="panel-footer no-padding">
              <ul class="nav nav-stacked">
              <li><a class="txt-green">KECAMATAN<span class="pull-right badge bg-blue">TOTAL</span></a></li>
              </ul>
              <ul class="nav nav-stacked">
                ';$k = $this->db->query("SELECT id_kecamatan, nama_kecamatan from kecamatan where id_kabupaten = 1 and status = 1 ");foreach($k->result() as $rk){
                  echo '<li><a target="_blank" href="https://kecamatan'.strtolower($rk->nama_kecamatan).'.wonosobokab.go.id/dashboard/disparbud_kebudayaan">'.$rk->nama_kecamatan.' ';
                  $k1 = $this->db->query("SELECT count(id_kecamatan) as perkecamatan from permohonan_organisasi where id_kecamatan=".$rk->id_kecamatan." and status = 1 and domain = '".$web."' ");
                  foreach($k1->result() as $rk1){echo '<span class="pull-right badge bg-blue">'.$rk1->perkecamatan.'</span></a>'; }echo'</li>';
                }echo'
              </ul>
            </div>
          </div>
        </div>
        ';
				echo ' 
        <div class="col-md-4" style="padding: 2px;">
          <div class="panel" style="background-color: #6cbcfc !important; color: white;">
            <div class="panel-body" style="background-color: #6cbcfc !important; color: white;">
              <div class="row">
              <div class="col-xs-8">
                <div style="height: 110px;">
                  <b style="font-size:20px;">DATA CAGAR BUDAYA</b><br />
                  <b style="font-size:26px;"><u>'.$total_cagar_budaya.'</u></b>
                </div>
              </div>
              <div class="col-xs-4"><img src="'.base_url().'media/logo wonosobo.png" style="height:80px; width:60;"></div>
              </div>
            </div>
            <div class="panel-footer no-padding">
              <ul class="nav nav-stacked">
              <li><a class="txt-green">KECAMATAN<span class="pull-right badge bg-blue">TOTAL</span></a></li>
              </ul>
              <ul class="nav nav-stacked">
                ';$k = $this->db->query("SELECT id_kecamatan, nama_kecamatan from kecamatan where id_kabupaten = 1 and status = 1 ");foreach($k->result() as $rk){
                  echo '<li><a target="_blank" href="https://kecamatan'.strtolower($rk->nama_kecamatan).'.wonosobokab.go.id/dashboard/disparbud_cagar_budaya">'.$rk->nama_kecamatan.' ';
                  $k1 = $this->db->query("SELECT count(id_kecamatan) as perkecamatan from cagar_budaya where id_kecamatan=".$rk->id_kecamatan." and status = 1 and domain = '".$web."' ");
                  foreach($k1->result() as $rk1){echo '<span class="pull-right badge bg-blue">'.$rk1->perkecamatan.'</span></a>'; }echo'</li>';
                }echo'
              </ul>
            </div>
          </div>
        </div>
        ';
				echo ' 
        <div class="col-md-4" style="padding: 2px;">
          <div class="panel" style="background-color: #6cbcfc !important; color: white;">
            <div class="panel-body" style="background-color: #6cbcfc !important; color: white;">
              <div class="row">
              <div class="col-xs-8">
                <div style="height: 110px;">
                  <b style="font-size:20px;">DATA EKONOMI KREATIF</b><br />
                  <b style="font-size:26px;"><u>'.$total_ekonomi_kreatif.'</u></b>
                </div>
              </div>
              <div class="col-xs-4"><img src="'.base_url().'media/logo wonosobo.png" style="height:80px; width:60;"></div>
              </div>
            </div>
            <div class="panel-footer no-padding">
              <ul class="nav nav-stacked">
              <li><a class="txt-green">KECAMATAN<span class="pull-right badge bg-blue">TOTAL</span></a></li>
              </ul>
              <ul class="nav nav-stacked">
                ';$k = $this->db->query("SELECT id_kecamatan, nama_kecamatan from kecamatan where id_kabupaten = 1 and status = 1 ");foreach($k->result() as $rk){
                  echo '<li><a target="_blank" href="https://kecamatan'.strtolower($rk->nama_kecamatan).'.wonosobokab.go.id/dashboard/disparbud_ekonomi_kreatif">'.$rk->nama_kecamatan.' ';
                  $k1 = $this->db->query("SELECT count(id_kecamatan) as perkecamatan from ekonomi_kreatif where id_kecamatan=".$rk->id_kecamatan." and status = 1 and domain = '".$web."' ");
                  foreach($k1->result() as $rk1){echo '<span class="pull-right badge bg-blue">'.$rk1->perkecamatan.'</span></a>'; }echo'</li>';
                }echo'
              </ul>
            </div>
          </div>
        </div>
        ';
  }
	public function json_all_disparbud_kebudayaan_perkecamatan(){
        $web=$this->uut->namadomain(base_url());
        $k = $this->db->query("SELECT id_kecamatan, kecamatan_nama from data_kecamatan where status = 1 and kecamatan_website='".$web."'");
        foreach($k->result() as $rk){
        $w = $this->db->query("
        SELECT id_permohonan_organisasi
        from permohonan_organisasi
        where status = 1 
        and id_kecamatan = ".$rk->id_kecamatan."
        ");
        $total_permohonan_organisasi = $w->num_rows();
				echo '
        <div class="col-md-12" style="padding: 2px;">
          <h3>'.$total_permohonan_organisasi.' KELOMPOK KESENIAN '.$rk->kecamatan_nama.'</h3>
          <div class="row">
                    ';
                    $no=0;
                      $k1 = $this->db->query("SELECT * from permohonan_organisasi where id_kecamatan=".$rk->id_kecamatan." and status = 1");
                      foreach($k1->result() as $rk1){
                        $no=$no+1;
                    echo'
            <div class="col-md-4" style="padding: 2px;">
              <div class="panel" style="background-color: #6cbcfc !important; color: white;">
                <div class="panel-body" style="background-color: #6cbcfc !important; color: white;">
                  <div class="row">
                  <div class="col-xs-8">
                    <div style="height: 110px;">
                      <b style="font-size:20px;">DATA CAGAR BUDAYA</b><br />
                      <b style="font-size:26px;"><u>m</u></b>
                    </div>
                  </div>
                  <div class="col-xs-4"><img src="'.base_url().'media/logo wonosobo.png" style="height:80px; width:60;"></div>
                  </div>
                      <b style="font-size:20px; color:#fff"> KELOMPOK KESENIAN KECAMATAN </b><br /><br />
                          <table class="table">
                            <tr>
                              <td>No.</td>
                              <td>Nama</td>
                              <td>Alamat</td>
                              <td>Jenis</td>
                              <td>Jumlah Anggota</td>
                            </tr>
                    ';
                        echo '
                            <tr>
                              <td>'.$no.'</td>
                              <td>'.$rk1->nama_organisasi.'</td>
                              <td>'.$rk1->alamat_organisasi.'</td>
                              <td>'.$rk1->kesenian.'</td>
                              <td>'.$rk1->jumlah_anggota.'</td>
                            </tr>
                          '; 
                    echo'
                          </table>
                </div>
              </div>
            </div>
          ';
                      }
                    }
          echo'
          </div>
        </div>
        ';
  }
	public function json_all_harga_kebutuhan_pokok(){
    $web=$this->uut->namadomain(base_url());
		$table = 'harga_kebutuhan_pokok';
    $id_pasar = $this->input->post('id_pasar');
    $page = $this->input->post('page');
    $limit = $this->input->post('limit');
    $keyword = $this->input->post('keyword');
    $order_by = $this->input->post('orderby');
    $filter = $this->input->post('filter');
    $start = ($page - 1) * $limit;
    $fields = "
    harga_kebutuhan_pokok.id_komoditi,
    harga_kebutuhan_pokok.id_pasar,
    harga_kebutuhan_pokok.tanggal,
    harga_kebutuhan_pokok.harga,
    harga_kebutuhan_pokok.id_satuan,
    komoditi.judul_komoditi
    ";
    if($id_pasar==''){
    $where = array(
      'harga_kebutuhan_pokok.status !=' => 99,
      'harga_kebutuhan_pokok.id_pasar' => 1,
      'komoditi.parent !=' => 0
      );
    }
    else{
    $where = array(
      'harga_kebutuhan_pokok.status !=' => 99,
      'harga_kebutuhan_pokok.id_pasar' => $id_pasar,
      'komoditi.parent !=' => 0
      );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Dashboard_model->html_harga_kebutuhan_pokok($table, $where, $limit, $start, $fields, $orderby, $keyword, $filter);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        $harga = $row->harga;
        $tanggal_terakhir_input = $row->tanggal;
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "`", "'");
        $yummy   = array("_","","","","","","","","","","","","","","","");
        $judul_komoditi = str_replace($healthy, $yummy, $row->judul_komoditi);
        $w1 = $this->db->query("SELECT attachment.file_name from attachment where attachment.id_tabel = ".$row->id_komoditi." and attachment.table_name='komoditi' limit 1");
        foreach($w1->result() as $h1){$file_name=$h1->file_name;}
        
        if(($w1->num_rows())>0)
        {
          $file_name='<img src="'.base_url().'media/upload/s_'.$file_name.'" style="height:80px; width:60;">';
        }
        else{
          $file_name='<img src="'.base_url().'media/logo wonosobo.png" style="height:80px; width:60;">';
        }
        if(empty($harga)){$harga=0;}
				echo ' 
        <a href="'.base_url().'dashboard/details/'.$row->id_komoditi.'/'.$judul_komoditi.'.HTML">
        <div class="col-md-3" style="padding: 2px;">
          <div class="panel" id_komoditi="'.$row->id_komoditi.'" id="'.$row->id_komoditi.'" style="background-color: #6cbcfc !important; color: white;">
            <div class="panel-body" style="background-color: #6cbcfc !important; color: white;">
              <div class="row">
              <div class="col-xs-8">
                <div style="height: 110px;">
                  '.$row->judul_komoditi.' <br />
                  <b style="font-size:20px;">Rp ';echo ''.number_format($harga).''; echo'</b>
                  <br />/'.$this->Crud_model->TanggalBahasaIndo($tanggal_terakhir_input).'
                </div>
              </div>
              <div class="col-xs-4">'.$file_name.'</div>
            </div>
            </div>';
              $date = "".$tanggal_terakhir_input."";
              $date = strtotime($date);
              $date = strtotime("-1 day", $date);
              $tanggal_terakhir_input_tampil = date('Y-m-d', $date);
              $w = $this->db->query("SELECT harga from harga_kebutuhan_pokok where status = 1 and id_komoditi=".$row->id_komoditi." and tanggal='".$tanggal_terakhir_input_tampil."' order by tanggal DESC limit 1");
              if(($w->num_rows())>0){
                foreach($w->result() as $h)
                {
                  $harga_lalu = $h->harga;
                  if($harga==$harga_lalu){echo '
                  <div class="panel-footer" style="background-color: #2196F3; border-top: 1px solid rgba(221, 221, 221, 0);">
                    Harga Stabil <small class="label pull-right bg-aqua"><i class="fa fa-long-arrow-right"></i></small> 
                  </div>
                  ';}
                  elseif($harga<$harga_lalu){echo '
                  <div class="panel-footer" style="background-color: #1ff426; border-top: 1px solid rgba(221, 221, 221, 0);">
                    Harga Turun '; $turun = $harga_lalu-$harga; echo ''.number_format($turun).''; echo' <small class="label pull-right bg-green"><i class="fa fa-long-arrow-down"></i></small> 
                  </div>
                  ';}
                  elseif($harga>$harga_lalu){echo '
                  <div class="panel-footer" style="background-color: #f4222c; border-top: 1px solid rgba(221, 221, 221, 0);">
                    Harga Naik '; $naik = $harga-$harga_lalu; echo ''.number_format($naik).''; echo' <small class="label pull-right bg-red"><i class="fa fa-long-arrow-up"></i></small> 
                  </div>
                  ';}
                  elseif($harga_lalu==''){echo '
                  <div class="panel-footer" style="background-color: #1ff426; border-top: 1px solid rgba(221, 221, 221, 0);">
                    Harga Turun | '.$tanggal.' <small class="label pull-right"><i class="fa fa-long-arrow-right"></i></small> 
                  </div>
                  ';}
                  else{}
                  
                }
              }else{
                echo '
                  <div class="panel-footer" style="background-color: #efbf21; border-top: 1px solid rgba(221, 221, 221, 0);">
                    Harga Sebelumnya Belum Input <small class="label pull-right bg-yellow"><i class="fa fa-info"></i></small> 
                  </div>
                  ';
              }
              echo'
          </div>
        </div>
        </a>
        ';
      }
          
	}
	public function data_perbulan_harga_kebutuhan_pokok(){
    $web=$this->uut->namadomain(base_url());
    $id_pasar = $this->input->post('id_pasar');
    $tahun = $this->input->post('tahun');
    $bulan = $this->input->post('bulan');
    $id_komoditi = $this->input->post('id_komoditi');
    if($id_pasar==''){$id_pasar=1;}else{$id_pasar;}
    $query = $this->db->query("SELECT 
    harga_kebutuhan_pokok.id_komoditi,
    harga_kebutuhan_pokok.id_pasar,
    harga_kebutuhan_pokok.tanggal,
    harga_kebutuhan_pokok.harga,
    harga_kebutuhan_pokok.id_satuan,
    (select komoditi.judul_komoditi from komoditi where komoditi.id_komoditi=harga_kebutuhan_pokok.id_komoditi) as judul_komoditi
    from harga_kebutuhan_pokok
    where harga_kebutuhan_pokok.status = 1
    and MONTH(harga_kebutuhan_pokok.tanggal) = '".$bulan."'
    and YEAR(harga_kebutuhan_pokok.tanggal) = '".$tahun."'
    and harga_kebutuhan_pokok.id_komoditi= ".$id_komoditi."
    ");
    $urut=0;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        $harga = $row->harga;
        if(empty($harga)){$harga=0;}
        $tanggal = $row->tanggal;
        // echo'<td>'.$this->Crud_model->TanggalBahasaIndo($tanggal).'</td>';
        echo'
        <tr>
          <td>'.$urut.'</td>
          <td>'.$this->Crud_model->TanggalBahasaIndo($tanggal).'</td>
          <td>'.number_format($harga).'</td>
        </tr>
        ';
      }
          
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $id_pasar = trim($this->input->post('id_pasar'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "harga_kebutuhan_pokok.id_harga_kebutuhan_pokok";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'harga_kebutuhan_pokok.status !=' => 99,
      'harga_kebutuhan_pokok.id_pasar' => $id_pasar
      );
    $this->db->where($where0);
    $query0 = $this->db->get('harga_kebutuhan_pokok');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  function kelurahan(){
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'dashboard/kelurahan';
      $this->load->view('tes', $data);
	}
	
  function desa(){
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'dashboard/desa';
      $this->load->view('tes', $data);
	}
	
  function kecamatan(){
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'dashboard/kecamatan';
      $this->load->view('tes', $data);
	}
	
  function opd(){
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'dashboard/opd';
      $this->load->view('tes', $data);
	}
	
  function posting(){
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'dashboard/posting';
      $this->load->view('tes', $data);
	}
  function load_table_posting(){
    $domain    = $this->input->post('domain');
		$a = 0;
		echo $this->load_posting(0,$h="", $a, $domain);
	}
  private function load_posting($parent=0,$hasil, $a, $domain){
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *, (select user_name from users where users.id_users=posting.created_by) as nama_pengguna from posting
									
		where parent='".$parent."' 
		and status = 1 
    and domain='".$domain."'
    order by posisi, urut
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
        $yummy   = array("_","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$nomor = $nomor + 1;
			$hasil .= '<tr id_posting="'.$h->id_posting.'" id="'.$h->id_posting.'" >';
      if( $a > 1 ){
        $hasil .= '<td style="padding: 2px '.($a * 20).'px ;"> <a target="_blank" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.' </a> </td>';
        }
			else{
        $hasil .= '<td style="padding: 2px;"> '.$h->judul_posting.' </td>';
        }
      $hasil .= '<td style="padding: 2px;"> '.$h->urut.' </td>';
      if( $h->highlight == 0 ){
        $hasil .= '<td style="padding: 2px;"><a href="#" id="aktifkan_highlight" class="text-danger"><i class="fa fa-times-circle"></i></a></td>';
      }
      else{
        $hasil .= '<td style="padding: 2px;"><a href="#" id="inaktifkan_highlight" class="text-success"><i class="fa fa-check-circle"></i></a></td>';
      }
			$hasil = $this->load_posting($h->id_posting,$hasil, $a, $domain);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
private function menu_atas($parent=NULL,$hasil, $a){
	$web=$this->uut->namadomain(base_url());
	$a = $a + 1;
	$w = $this->db->query("
	SELECT * from posting
	where posisi='menu_atas'
	and parent='".$parent."' 
	and status = 1 
	and tampil_menu_atas = 1 
	and domain='".$web."'
	order by urut
	");
	$x = $w->num_rows();
	if(($w->num_rows())>0)
	{
	if($parent == 0){
	if($web == 'zb.wonosobokab.go.id'){
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'website" class="fa-home ">HOME</a></li>
	'; 
	}
	elseif ($web == 'ppiddemo.wonosobokab.go.id'){
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'">HOME</a></li>
	<li class="parent">
	  <span class="">PPID </span>
	  <ul>
		<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid">PPID </a></span></li>
		<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid_pembantu">PPID Pembantu</a></span></li>
	  </ul>
	</li>
	';
	}
	else{
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'">HOME</a></li>
	'; 
	}
	}
	else{
	$hasil .= '
	<ul>
	  ';
	  }
	  }
	  $nomor = 0;
	  foreach($w->result() as $h)
	  {
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
	  $r = $this->db->query("
	  SELECT * from posting
	  where parent='".$h->id_posting."' 
	  and status = 1 
	  and tampil_menu_atas = 1 
	  and domain='".$web."'
	  order by urut
	  ");
	  $xx = $r->num_rows();
	  $nomor = $nomor + 1;
	  if( $a > 1 ){
			if ($xx == 0){
				if($h->judul_posting == 'Data Rekap Kunjungan Pasien'){
				$hasil .= '
				<li><a href="'.base_url().'grafik/data_rekap_kunjungan_pasien"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Data Kunjungan Hari Ini'){
				$hasil .= '
				<li><a href="'.base_url().'grafik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Pengumuman'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Artikel'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'FAQ'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Sitemap'){
				$hasil .= '
				<li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Informasi Publik'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Data Penyakit'){
				$hasil .= '
				<li><a href="'.base_url().'grafik/data_penyakit"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Pengaduan Masyarakat'){
				$hasil .= '
				<li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Permohonan Informasi Publik'){
				$hasil .= '
				<li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Permohonan Informasi'){
				$hasil .= '
				<li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Berita'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'BERITA'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				else{
				$hasil .= '
				<li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
				}
			}
			else{
			$hasil .= '
			<li class="parent" > <span class="">'.$h->judul_posting.' </span>';
			} 
		}
		else{
			if($h->judul_posting == 'Berita'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'BERITA'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Artikel'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Pengumuman'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Sitemap'){
			$hasil .= '
			<li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Informasi Publik'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'FAQ'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Info'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Pengaduan Masyarakat'){
				$hasil .= '
				<li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
			elseif($h->judul_posting == 'Permohonan Informasi Publik'){
				$hasil .= '
				<li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
			elseif($h->judul_posting == 'Permohonan Informasi'){
				$hasil .= '
				<li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
			else{
				if ($xx == 0){
					if($h->judul_posting == 'Berita'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'BERITA'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Data Rekap Kunjungan Pasien'){
					$hasil .= '
					<li><a href="http://180.250.150.76/simpus/charts/grafik_1b"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Data Penyakit'){
					$hasil .= '
					<li><a href="http://180.250.150.76/simpus/charts/grafik_1d"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Pengumuman'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Artikel'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Sitemap'){
					$hasil .= '
					<li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'FAQ'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Informasi Publik'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					else{
					$hasil .= '
					<li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
					}
				}
				else{
					if($h->judul_posting == 'Pengumuman'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					else{
					$hasil .= '
					<li class="parent" > <span class="">'.$h->judul_posting.' </span>';
					}
				}
			}
		}
		$hasil = $this->menu_atas($h->id_posting,$hasil, $a);
		$hasil .= '
	  </li>
	  ';
	  }
	  if(($w->num_rows)>0)
	  {
	  $hasil .= "
	</ul>
	";
	}
	else{
	}
	return $hasil;
}
  private function option_posting_terbarukan($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 10
		");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
			$hasil .= 
			'
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<!-- BEGIN CAROUSEL -->            
								<div class="front-carousel">
									<div class="carousel slide" id="myCarousel">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<div class="item active">
												<img alt="" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'">
											</div>
										</div>
										<!-- Carousel nav -->
										<a data-slide="prev" href="#myCarousel" class="carousel-control left">
											<i class="fa fa-angle-left"></i>
										</a>
										<a data-slide="next" href="#myCarousel" class="carousel-control right">
											<i class="fa fa-angle-right"></i>
										</a>
									</div>                
								</div>
								<!-- END CAROUSEL -->             
							</div>
							<div class="col-md-8 col-sm-8">
								<h2><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></h2>
								<ul class="blog-info">
									<li><i class="fa fa-calendar"></i> '.$created_time.'</li>
								</ul>
								<p>'.substr(strip_tags($h->isi_posting), 0, 200).'</p>
								<a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="more">Baca Selengkapnya <i class="icon-angle-right"></i></a>
							</div>
						</div>
						<hr class="blog-post-sep">
			';
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  public function get_attachment_by_id_posting($id_posting)
   {
     $where = array(
						'id_tabel' => $id_posting,
						'table_name' => 'posting'
						);
    $d = $this->Posting_model->get_attachment_by_id_posting($where);
    if(!$d)
          {
						return 'blankgambar.jpg';
          }
        else
          {
						return $d['file_name'];
          }
   }
  public function get_attachment_by_id_komoditi($id_komoditi)
   {
     $where = array(
						'id_tabel' => $id_komoditi,
						'table_name' => 'komoditi'
						);
    $d = $this->Komoditi_model->get_attachment_by_id_komoditi($where);
    if(!$d)
          {
						return ''.base_url().'media/logo wonosobo.png';
          }
        else
          {
						return ''.base_url().'media/upload/'.$d['file_name'].'';
          }
   }
   // linechart harga_kebutuhan_pokok
   
	public function lineChart()
	{
    $web=$this->uut->namadomain(base_url());
    $id_pasar = $this->input->post('id_pasar');
    $tahun = $this->input->post('tahun');
    $bulan = $this->input->post('bulan');
    $id_komoditi = $this->input->post('id_komoditi');
    if($id_pasar==''){$id_pasar=1;}else{$id_pasar;}
    $query = $this->db->query("SELECT 
    harga_kebutuhan_pokok.id_komoditi,
    harga_kebutuhan_pokok.id_pasar,
    harga_kebutuhan_pokok.tanggal,
    harga_kebutuhan_pokok.harga,
    harga_kebutuhan_pokok.id_satuan,
    (select komoditi.judul_komoditi from komoditi where komoditi.id_komoditi=harga_kebutuhan_pokok.id_komoditi) as judul_komoditi
    from harga_kebutuhan_pokok
    where harga_kebutuhan_pokok.status = 1
    and MONTH(harga_kebutuhan_pokok.tanggal) = '".$bulan."'
    and YEAR(harga_kebutuhan_pokok.tanggal) = '".$tahun."'
    and harga_kebutuhan_pokok.id_komoditi= ".$id_komoditi."
    ");
    $urut=0;
    echo'
    <div class="chart" id="line-chart" style="height: 300px;"></div>
    <script>
      $(function () {
        "use strict";

        // LINE CHART
        var line = new Morris.Line({
          element: \'line-chart\',
          resize: true,
          data: [';
          foreach ($query->result() as $row)
            {
              $urut=$urut+1;
              $harga = $row->harga;
              $tanggal = $row->tanggal;
              echo'{y: \''.$tanggal.'\', item1: '.$harga.'},';
            }
          echo'
          ],
          xkey: \'y\',
          ykeys: [\'item1\'],
          labels: [\'Harga\'],
          lineColors: [\'#3c8dbc\'],
          hideHover: \'auto\'
        });
      });
    </script>
    ';
  }
  public function list_opd(){
    $w = $this->db->query("SELECT data_skpd.id_data_skpd, data_skpd.skpd_nama , data_skpd.skpd_website 
    from data_skpd
    where data_skpd.status = 1
    and data_skpd.id_kecamatan = 0
    ");
    $no=1;
    $jumlah_website_opd = $w->num_rows();
    echo'
      <div class="row">
        <div class="col-md-12" style="padding: 2px;">
          <H3>JUMLAH POSTING <br />OPD</H3>
        </div>
        <div class="col-md-12" style="padding: 2px;">
          <div class="panel" style="background-color: #6cbcfc !important; color: white;">
            <div class="panel-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
    ';
    foreach ($w->result() as $row)
      {
				echo '
                <li>
                  <a target="_blank" href="https://'.$row->skpd_website.'">'.$row->skpd_website.'
                  <span class="pull-right text-red"><i class="fa fa-angle-up"></i>
                  <input type="hidden" id="'.$no.'" value="'.$row->id_data_skpd.'">
                  <input name="skpd_website" id="skpd_website" value="'.$row->skpd_website.'" type="hidden">
                  ';
                  $query = $this->db->query("SELECT 
                  COUNT(posting.id_posting)as jumlah_posting
                  FROM posting 
                  WHERE  posting.domain='".$row->skpd_website."'
                  AND  posting.status=1
                  ");
                  foreach ($query->result() as $row1)
                    {
                      echo ''.$row1->jumlah_posting.'';
                    }
                  echo'<div id="'.$row->id_data_skpd.'_1"></span>
                  </a>
                </li>
        ';
      }
      echo'
              </ul>
            </div>
          </div>
        </div>
      ';
  }
  public function list_kecamatan(){
    $w = $this->db->query("SELECT data_skpd.id_data_skpd, data_skpd.skpd_nama , data_skpd.skpd_website 
    from data_skpd
    where data_skpd.status = 1
    and data_skpd.id_kecamatan!= 0
    ");
    $no=1;
    $jumlah_website_opd = $w->num_rows();
    echo'
      <div class="row">
        <div class="col-md-12" style="padding: 2px;">
          <H3>JUMLAH POSTING <br />KECAMATAN</H3>
        </div>
        <div class="col-md-12" style="padding: 2px;">
          <div class="panel" style="background-color: #6cbcfc !important; color: white;">
            <div class="panel-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
    ';
    foreach ($w->result() as $row)
      {
				echo '
                <li>
                  <a target="_blank" href="https://'.$row->skpd_website.'">'.$row->skpd_nama.'
                  <span class="pull-right text-red"><i class="fa fa-angle-up"></i>
                  <input type="hidden" id="'.$no.'" value="'.$row->id_data_skpd.'">
                  <input name="skpd_website" id="skpd_website" value="'.$row->skpd_website.'" type="hidden">
                  ';
                  $query = $this->db->query("SELECT 
                  COUNT(posting.id_posting)as jumlah_posting
                  FROM posting 
                  WHERE  posting.domain='".$row->skpd_website."'
                  AND  posting.status=1
                  ");
                  foreach ($query->result() as $row1)
                    {
                      echo ''.$row1->jumlah_posting.'';
                    }
                  echo'<div id="'.$row->id_data_skpd.'_1"></span>
                  </a>
                </li>
        ';
      }
      echo'
              </ul>
            </div>
          </div>
        </div>
      ';
  }
  public function list_desakelurahan(){
    $w = $this->db->query("SELECT data_desa.id_data_desa, data_desa.desa_nama , data_desa.desa_website 
    from data_desa
    where data_desa.status = 1
    order by data_desa.id_desa
    ");
    $no=1;
    $jumlah_website_opd = $w->num_rows();
    echo'
      <div class="row">
        <div class="col-md-12" style="padding: 2px;">
          <H3>JUMLAH POSTING <br />DESA DAN KELURAHAN</H3>
        </div>
        <div class="col-md-12" style="padding: 2px;">
          <div class="panel" style="background-color: #6cbcfc !important; color: white;">
            <div class="panel-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
    ';
    foreach ($w->result() as $row)
      {
				echo '
                <li>
                  <a target="_blank" href="https://'.$row->desa_website.'">'.$row->desa_website.'
                  <span class="pull-right text-red"><i class="fa fa-angle-up"></i>
                  <input type="hidden" id="'.$no.'" value="'.$row->id_data_desa.'">
                  <input name="skpd_website" id="skpd_website" value="'.$row->desa_website.'" type="hidden">
                  ';
                  $query = $this->db->query("SELECT 
                  COUNT(posting.id_posting)as jumlah_posting
                  FROM posting 
                  WHERE  posting.domain='".$row->desa_website."'
                  AND  posting.status=1
                  ");
                  foreach ($query->result() as $row1)
                    {
                      echo ''.$row1->jumlah_posting.'';
                    }
                  echo'<div id="'.$row->id_data_desa.'_1"></span>
                  </a>
                </li>
        ';
      }
      echo'
              </ul>
            </div>
          </div>
        </div>
      ';
  }
  public function total_posting_opd(){    
    $id_data_skpd = $this->input->get('id_data_skpd');
    $skpd_website = $this->input->get('skpd_website');
    $query = $this->db->query("SELECT 
    COUNT(posting.id_posting)as jumlah_posting
    FROM posting 
    WHERE  posting.domain='".$skpd_website."'
    AND  posting.status=1
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_posting.'';
      }
  }
}