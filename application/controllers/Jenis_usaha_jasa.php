<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jenis_usaha_jasa extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Jenis_usaha_jasa_model');
   }
   
  public function json_option(){
		$table = 'jenis_usaha_jasa';
		$hak_akses = $this->session->userdata('hak_akses');
		if($hak_akses=='root'){
			$where   = array(
				'jenis_usaha_jasa.status !=' => 99
				);
		}
		else{
			$where   = array(
				'jenis_usaha_jasa.status !=' => 99
				);
		}
		$fields  = 
			"
			*
			";
		$order_by  = 'jenis_usaha_jasa.judul_jenis_usaha_jasa';
		echo json_encode($this->Crud_model->opsi($table, $where, $fields, $order_by));
  }
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'jenis_usaha_jasa/home';
    $this->load->view('tes', $data);
   }
   
  public function json_all_jenis_usaha_jasa()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $start      = ($halaman - 1) * $limit;
    $fields     = "*";
    $where      = array(
      'jenis_usaha_jasa.status!=' => 99,
      'jenis_usaha_jasa.domain' => $web
    );
    $order_by   = 'jenis_usaha_jasa.isi_jenis_usaha_jasa';
    echo json_encode($this->Jenis_usaha_jasa_model->json_all_jenis_usaha_jasa($where, $limit, $start, $fields, $order_by));
   }
   
  public function simpan_jenis_usaha_jasa()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_jenis_usaha_jasa', 'judul_jenis_usaha_jasa', 'required');
		$this->form_validation->set_rules('isi_jenis_usaha_jasa', 'isi_jenis_usaha_jasa', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'judul_jenis_usaha_jasa' => trim($this->input->post('judul_jenis_usaha_jasa')),
        'temp' => trim($this->input->post('temp')),
        'icon' => trim($this->input->post('icon')),
        'isi_jenis_usaha_jasa' => trim($this->input->post('isi_jenis_usaha_jasa')),
        'keterangan' => trim($this->input->post('keterangan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'jenis_usaha_jasa';
      $id         = $this->Jenis_usaha_jasa_model->simpan_jenis_usaha_jasa($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'jenis_usaha_jasa',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
  public function update_jenis_usaha_jasa()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_jenis_usaha_jasa', 'judul_jenis_usaha_jasa', 'required');
		$this->form_validation->set_rules('isi_jenis_usaha_jasa', 'isi_jenis_usaha_jasa', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'judul_jenis_usaha_jasa' => trim($this->input->post('judul_jenis_usaha_jasa')),
        'isi_jenis_usaha_jasa' => trim($this->input->post('isi_jenis_usaha_jasa')),
        'icon' => trim($this->input->post('icon')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'jenis_usaha_jasa';
      $where       = array(
        'jenis_usaha_jasa.id_jenis_usaha_jasa' => trim($this->input->post('id_jenis_usaha_jasa')),
        'jenis_usaha_jasa.domain' => $web
			);
      $this->Jenis_usaha_jasa_model->update_data_jenis_usaha_jasa($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'jenis_usaha_jasa.id_jenis_usaha_jasa' => $this->input->post('id_jenis_usaha_jasa'),
        'jenis_usaha_jasa.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_jenis_usaha_jasa');
      $result = $this->db->get('jenis_usaha_jasa');
      echo json_encode($result->result_array());
		}
   
   public function total_jenis_usaha_jasa()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('jenis_usaha_jasa');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function cek_jenis_usaha_jasa()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('master_jenis_usaha_jasa');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'judul_jenis_usaha_jasa' => $row->nama_master_jenis_usaha_jasa,
          'domain' => $web
          );
        $this->db->from('jenis_usaha_jasa');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'domain' => $web,
            'judul_jenis_usaha_jasa' => $row->nama_master_jenis_usaha_jasa,
            'isi_jenis_usaha_jasa' => $row->isi_master_jenis_usaha_jasa_default,
            'temp' => '-',
            'created_by' => 1,
            'created_time' => date('Y-m-d H:i:s'),
            'updated_by' => 1,
            'updated_time' => date('Y-m-d H:i:s'),
            'deleted_by' => 1,
            'deleted_time' => date('Y-m-d H:i:s'),
            'keterangan' => '-',
            'icon' => 'fa-home',
            'status' => 1
                    
            );
          $table_name = 'jenis_usaha_jasa';
          $this->Jenis_usaha_jasa_model->simpan_jenis_usaha_jasa($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
  public function inaktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_jenis_usaha_jasa = $this->input->post('id_jenis_usaha_jasa');
      $where = array(
        'id_jenis_usaha_jasa' => $id_jenis_usaha_jasa
        );
      $this->db->from('jenis_usaha_jasa');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'jenis_usaha_jasa';
        if( $cek <> '' ){
          $this->Jenis_usaha_jasa_model->update_data_jenis_usaha_jasa($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_jenis_usaha_jasa = $this->input->post('id_jenis_usaha_jasa');
      $where = array(
        'id_jenis_usaha_jasa' => $id_jenis_usaha_jasa
        );
      $this->db->from('jenis_usaha_jasa');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'jenis_usaha_jasa';
        if( $cek <> '' ){
          $this->Jenis_usaha_jasa_model->update_data_jenis_usaha_jasa($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
    
 }