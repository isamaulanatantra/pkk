<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Halaman extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Halaman_model');
   }
  public function check_login()
   {
    $table_name = 'users';
    $where      = array(
      'users.id_users' => $this->session->userdata('id_users')
    );
    $a          = $this->Crud_model->semua_data($where, $table_name);
    if ($a == 0)
     {
        return redirect(''.base_url().'login');
     }
   }
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'halaman/home';
    $this->load->view('back_bone', $data);
   }
   
  public function json_all_posting()
   {
    $this->check_login();
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_posting    = $this->input->post('urut_data_posting');
    $posisi    = $this->input->post('posisi');
    $parent    = $this->input->post('parent');
    if($halaman==''){$halaman=1;}else{$halaman;}
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  *,
                  (select user_name from users where users.id_users=posting.created_by) as nama_pengguna
                  ";
    // if($posisi==''){$posisi=0;}else{$posisi;}
    if($parent==''){$parent=0;}else{$parent;}
    $where      = array(
      'domain' => $web,
      'posisi' => $posisi,
      'parent' => $parent,
      'status!=' => 99
    );
    $order_by   = 'posting.'.$urut_data_posting.'';
    echo json_encode($this->Halaman_model->json_all_posting($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
  public function json_all_posting_arsip()
   {
    $this->check_login();
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_posting    = $this->input->post('urut_data_posting');
    $posisi    = $this->input->post('posisi');
    $parent    = $this->input->post('parent');
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  *,
                  (select user_name from users where users.id_users=posting.created_by) as nama_pengguna
                  ";
    if($parent==''){$parent=0;}
    else{$parent;}
    $where      = array(
      'domain' => $web,
      'posisi' => $posisi,
      'parent' => $parent,
      'status' => 99
    );
    $order_by   = 'posting.'.$urut_data_posting.'';
    echo json_encode($this->Halaman_model->json_all_posting($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
   
  public function inaktifkan_menu_atas()
		{
      $this->check_login();
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'tampil_menu_atas' => 0
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Halaman_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan_menu_atas()
		{
      $this->check_login();
      $web=$this->uut->namadomain(base_url());
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'tampil_menu_atas' => 1
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Halaman_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function inaktifkan_highlight()
		{
      $this->check_login();
      $web=$this->uut->namadomain(base_url());
      $cek = $this->session->userdata('id_users');
			$id_posting = $this->input->post('id_posting');
      $where = array(
        'id_posting' => $id_posting,
        'domain' => $web
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'highlight' => 0
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Halaman_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan_highlight()
		{
      $this->check_login();
      $web=$this->uut->namadomain(base_url());
      $cek = $this->session->userdata('id_users');
			$id_posting = $this->input->post('id_posting');
      $where = array(
        'id_posting' => $id_posting,
        'domain' => $web
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'highlight' => 1
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Halaman_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
  public function inaktifkan()
		{
    $this->check_login();
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Halaman_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
    $this->check_login();
      $web=$this->uut->namadomain(base_url());
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Halaman_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function cek_tema()
		{
    $this->check_login();
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('tema');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'judul_posting' => $row->nama_tema,
          'domain' => $web
          );
        $this->db->from('posting');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'domain' => $web,
            'judul_posting' => $row->nama_tema,
            'keterangan' => $row->judul,
            'created_by' => 1,
            'created_time' => date('Y-m-d H:i:s'),
            'status' => 1
                    
            );
          $table_name = 'posting';
          $this->Halaman_model->simpan_posting($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
   public function total_posting_tos()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $halaman    = $this->input->post('halaman');
      $limit    = $this->input->post('limit');
      $kata_kunci    = $this->input->post('kata_kunci');
      $urut_data_posting    = $this->input->post('urut_data_posting');
      $parent    = $this->input->post('parent');
      $this->db->from('posting');
      if( $kata_kunci <> '' ){
        $this->db->like('judul_posting', $kata_kunci);
        $this->db->or_like('domain', $kata_kunci);
      }
      if($parent==''){$parent=0;}
      else{$parent;}
      $where    = array(
        'status' => 1,
        'parent' => $parent,
        'domain' => $web
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
    
   public function total_posting()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $this->db->from('posting');
      $where    = array(
        'status' => 1,
        'domain' => $web
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  function load_the_option_by_posisi_filter()
	{
    $web=$this->uut->namadomain(base_url());
    $posisi = trim($this->input->post('posisi'));
		$a = 0;
		echo $this->option_posting_by_posisi_filter(0,$h="", $a, $posisi);
	}
  
  private function option_posting_by_posisi_filter($parent=0,$hasil, $a, $posisi){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from posting 
                          where parent='".$parent."' 
                          and status = 1 
                          and posisi='".$posisi."'
                          and domain='".$web."'
                          order by urut");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_posting.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_posting.'';
			}
			else{
			$hasil .= ''.$h->judul_posting.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_posting_by_posisi_filter($h->id_posting,$hasil, $a, $posisi);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
  function insert_parrent($urut, $judul_posting, $icon){
    $web=$this->uut->namadomain(base_url());
    $data_input = array(
      'domain' => $web,
      'judul_posting' => $judul_posting,
      'highlight' => 0,
      'tampil_menu' => 1,
      'parent' => 0,
      'kata_kunci' => '-',
      'temp' => date('YmdHis'),
      'urut' => $urut,
      'posisi' => 'menu_atas',
      'icon' => $icon,
      'isi_posting' => 'Isikan deskripsi disini',
      'keterangan' => '-',
      'created_by' => '',
      'created_time' => date('Y-m-d H:i:s'),
      'status' => 1

      );
    $table_name = 'posting';
    return $this->Halaman_model->simpan_posting($data_input, $table_name);
  }
  
  function insert_child($parent, $urut, $judul_posting, $icon){
    $web=$this->uut->namadomain(base_url());
    $data_input = array(
      'domain' => $web,
      'judul_posting' => $judul_posting,
      'highlight' => 0,
      'tampil_menu' => 1,
      'parent' => $parent,
      'kata_kunci' => '-',
      'temp' => date('YmdHis'),
      'urut' => $urut,
      'posisi' => 'menu_atas',
      'icon' => $icon,
      'isi_posting' => 'Isikan deskripsi disini',
      'keterangan' => '-',
      'created_by' => '',
      'created_time' => date('Y-m-d H:i:s'),
      'status' => 1

      );
    $table_name = 'posting';
    $id         = $this->Halaman_model->simpan_posting($data_input, $table_name);
  }
  
  function cek_default_halaman()
	{
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("SELECT * from posting where domain='".$web."' ");
    $j=$w->num_rows();
    
		if( ($w->num_rows()) == 0 ){
      echo 'Halaman default berhasil digenerate';
      $judul_posting='Profil Desa';
      $icon = 'fa-tasks';
      $urut='1';
      $parent=$this->insert_parrent($urut, $judul_posting, $icon);
        $judul_posting='Sejarah Desa';
        $icon = 'fa-tasks';
        $urut='1';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
        $judul_posting='Profil Potensi Desa';
        $icon = 'fa-tasks';
        $urut='2';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
      ////////////////////////
      $judul_posting='Pemerintahan';
      $icon = 'fa-tasks';
      $urut='2';
      $parent=$this->insert_parrent($urut, $judul_posting, $icon);
        $judul_posting='Visi Dan Misi';
        $icon = 'fa-tasks';
        $urut='1';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
        $judul_posting='Pemerintah Desa';
        $icon = 'fa-tasks';
        $urut='2';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
        $judul_posting='Badan Permusyawaratan Desa';
        $icon = 'fa-tasks';
        $urut='3';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
        $judul_posting='Sarana Prasarana';
        $icon = 'fa-tasks';
        $urut='4';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
        $judul_posting='Sambutan Kepala Desa';
        $icon = 'fa-tasks';
        $urut='5';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
      $judul_posting='Kelembagaan';
      $icon = 'fa-tasks';
      $urut='3';
      $parent=$this->insert_parrent($urut, $judul_posting, $icon);
        $judul_posting='Karang Taruna';
        $icon = 'fa-tasks';
        $urut='1';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
        $judul_posting='Satgas Linmas';
        $icon = 'fa-tasks';
        $urut='2';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
        $judul_posting='PKK Desa';
        $icon = 'fa-tasks';
        $urut='3';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
        $judul_posting='LPMD';
        $icon = 'fa-tasks';
        $urut='4';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
        $judul_posting='RW/RT';
        $icon = 'fa-tasks';
        $urut='5';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
      $judul_posting='Data Desa';
      $icon = 'fa-tasks';
      $urut='4';
      $parent=$this->insert_parrent($urut, $judul_posting, $icon);
        $judul_posting='Data Wilayah Administratif';
        $icon = 'fa-tasks';
        $urut='1';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
        $judul_posting='Data Penduduk';
        $icon = 'fa-tasks';
        $urut='2';
        $this->insert_child($parent, $urut, $judul_posting, $icon);
        ////////////////////////
		}
    else{
    echo 'Halaman default Sudah ada';
    }
	}
   
  function load_the_option()
	{
		$a = 0;
		echo $this->option_posting(0,$h="", $a);
	}
  
  private function option_posting($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from posting where parent='".$parent."' and domain='".$web."' and status = 1 order by urut");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_posting.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_posting.'';
			}
			else{
			$hasil .= ''.$h->judul_posting.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_posting($h->id_posting,$hasil, $a);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
  function load_the_option_by_posisi()
	{
    $web=$this->uut->namadomain(base_url());
    $posisi = trim($this->input->post('posisi'));
		$a = 0;
		echo $this->option_posting_by_posisi(0,$h="", $a, $posisi);
	}
  
  private function option_posting_by_posisi($parent=0,$hasil, $a, $posisi){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from posting 
                          where parent='".$parent."' 
                          and status = 1 
                          and posisi='".$posisi."'
                          and domain='".$web."'
                          order by urut");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_posting.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_posting.'';
			}
			else{
			$hasil .= ''.$h->judul_posting.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_posting_by_posisi($h->id_posting,$hasil, $a, $posisi);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
  public function simpan_posting()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_posting', 'judul_posting', 'required');
		$this->form_validation->set_rules('highlight', 'highlight', 'required');
		$this->form_validation->set_rules('tampil_menu', 'tampil_menu', 'required');
		$this->form_validation->set_rules('parent', 'parent', 'required');
		$this->form_validation->set_rules('urut', 'urut', 'required');
		$this->form_validation->set_rules('posisi', 'posisi', 'required');
		$this->form_validation->set_rules('icon', 'icon', 'required');
		$this->form_validation->set_rules('kata_kunci', 'kata_kunci', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('isi_posting', 'isi_posting', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$this->form_validation->set_rules('status', 'status', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'judul_posting' => trim($this->input->post('judul_posting')),
				'highlight' => trim($this->input->post('highlight')),
				'tampil_menu' => trim($this->input->post('tampil_menu')),
				'parent' => trim($this->input->post('parent')),
				'kata_kunci' => trim(str_replace('.', '', str_replace(',', '', $this->input->post('kata_kunci')))),
        'temp' => trim($this->input->post('temp')),
        'urut' => trim($this->input->post('urut')),
        'posisi' => trim($this->input->post('posisi')),
        'icon' => trim($this->input->post('icon')),
        'isi_posting' => trim($this->input->post('isi_posting')),
        'keterangan' => trim($this->input->post('keterangan')),
        'status' => trim($this->input->post('status')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'tampil_menu_atas	' => 1
        
				);
      $table_name = 'posting';
      $id         = $this->Halaman_model->simpan_posting($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'posting',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
   
  public function update_posting()
   {
    $web=$this->uut->namadomain(base_url());
    $this->form_validation->set_rules('judul_posting', 'judul_posting', 'required');
    $this->form_validation->set_rules('highlight', 'highlight', 'required');
    $this->form_validation->set_rules('tampil_menu', 'tampil_menu', 'required');
		$this->form_validation->set_rules('urut', 'urut', 'required');
		$this->form_validation->set_rules('posisi', 'posisi', 'required');
		$this->form_validation->set_rules('icon', 'icon', 'required');
		$this->form_validation->set_rules('kata_kunci', 'kata_kunci', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
				'judul_posting' => trim($this->input->post('judul_posting')),
				'highlight' => trim($this->input->post('highlight')),
				'tampil_menu' => trim($this->input->post('tampil_menu')),
				'parent' => trim($this->input->post('parent')),
				'kata_kunci' => trim(str_replace('.', '', str_replace(',', '', $this->input->post('kata_kunci')))),
        'temp' => trim($this->input->post('temp')),
        'urut' => trim($this->input->post('urut')),
        'posisi' => trim($this->input->post('posisi')),
        'icon' => trim($this->input->post('icon')),
        'isi_posting' => trim($this->input->post('isi_posting')),
        'keterangan' => trim($this->input->post('keterangan')),
        'status' => trim($this->input->post('status')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'posting';
      $where       = array(
        'posting.id_posting' => trim($this->input->post('id_posting')),
        'posting.domain' => $web
			);
      $this->Halaman_model->update_data_posting($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'id_posting' => $this->input->post('id_posting'),
        'posting.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_posting');
      $result = $this->db->get('posting');
      echo json_encode($result->result_array());
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_posting = $this->input->post('id_posting');
      $where = array(
        'id_posting' => $id_posting,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'posting';
        $this->Halaman_model->update_data_posting($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function restore()
		{
      $web=$this->uut->namadomain(base_url());
			$id_posting = $this->input->post('id_posting');
      $where = array(
        'id_posting' => $id_posting,
        'domain' => $web
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'posting';
        $this->Halaman_model->update_data_posting($data_update, $where, $table_name);
        echo 1;
        }
		}
    
  
 }