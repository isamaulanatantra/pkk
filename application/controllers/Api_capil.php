<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api_capil extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
   }
   
  public function index()
  {
    $headers = array(
        'header'=>"X-cons-id: $consumerID\r\n" .
										"X-Timestamp: $tStamp\r\n" .
										"X-Signature: $encodedSignature\r\n" .
										"X-Authorization: Basic $Authorization\r\n" .
										"Content-Type: application/json\r\n"
    );
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_close($ch);
    print_r($result);
	}

   public function getNIK()
    {
    $nik = $this->input->post('nik');
    $url = 'http://36.82.104.23:8181/ws_server/get_json/kominfo/callnik?USER_ID=KOMINFO&PASSWORD=kominfo12346&NIK='.$nik.'';    
    //$url = $this->input->post('url');
    $file = file_get_contents($url, false);
    echo $file;
    }
   public function getNIK111()
    {
    $nik = $this->input->post('nik');
    $url = 'http://capilwonosob0.ddns.net:8181/ws_server/get_json/kominfo/callnik?USER_ID=KOMINFO&PASSWORD=kominfo12346&NIK='.$nik.'';    
    //$url = $this->input->post('url');
    $file = file_get_contents($url, false);
    echo $file;
    }
    
   public function getNIKa()
		{
    $url = $this->input->post('url');
    
    $url = ''.$url.'';
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);
    curl_close($ch);
    print_r($result);
		}

   public function webservice()
    {
      $nik = $this->input->post('nik');
      $url = 'http://capilwonosob0.ddns.net:8181/ws_server/get_json/kominfo/callnik?USER_ID=KOMINFO&PASSWORD=kominfo12346&NIK='.$nik.'';    
      //$url = $this->input->post('url');
      $file = json_decode(file_get_contents($url, false));
      //$file = json_decode('{"content":[{"NIK":"3307110807920002","NO_KK":"3307112912073514","NAMA_LGKP":"ABDUL HANAN","JENIS_KLMIN":"LAKI-LAKI","TMPT_LHR":"WONOSOBO","TGL_LHR":"1992-07-08","GOL_DARAH":"TIDAK TAHU","AGAMA":"ISLAM","STATUS_KAWIN":"BELUM KAWIN","STAT_HBKEL":"ANAK","PDDK_AKH":"SLTP\/SEDERAJAT","JENIS_PKRJN":"PELAJAR\/MAHASISWA","NO_PROP":"33","PROP_NAME":"JAWA TENGAH","NO_KAB":"7","KAB_NAME":"WONOSOBO","NO_KEC":"11","KEC_NAME":"MOJOTENGAH","NO_KEL":"1009","KEL_NAME":"KALIBEBER","ALAMAT":"KALIBEBER","NO_RT":"2","NO_RW":"2","DUSUN":null,"KODE_POS":"56351"}],"lastPage":true,"numberOfElements":1,"sort":null,"totalElements":1,"firstPage":true,"number":0,"size":"10"}');
      //$from_database = '[{"id_penduduk":"737157","id_kartu_keluarga":"","nomor_kartu_keluarga":"3307112912073514","nama_penduduk":"ABDUL HANAN","nik":"3307110807920002","tempat_lahir":"WONOSOBO","tanggal_lahir_asli_disduk":"07\/08\/1992","tanggal_lahir":"1992-07-08","id_jenis_kelamin":"1","id_shdk":"4","alamat":"KALIBEBER","rt":"2","rw":"2","dusun":"NULL","id_desa":"193","kode_desa":"1009","id_kecamatan":"11","kode_kecamatan":"11","id_kabupaten":"1","kode_kabupaten":"7","id_propinsi":"1","kode_propinsi":"33","id_agama":"1","id_pendidikan":"4","id_pekerjaan":"3","nama_ibu_kandung":"SITI NGAISAH","id_kewarganegaraan":"0","foto":"","last_update":"2014-01-05 00:00:00","id_ibu_kandung":"0","id_bapak_kandung":"0","nama_bapak_kandung":"","id_golongan_darah":"0","nomor_akta_lahir":"","tgl_akta_lahir":"","id_status_pernikahan":"0","created_by":"0","created_time":"0000-00-00 00:00:00","updated_by":"0","updated_time":"0000-00-00 00:00:00","deleted_by":"0","deleted_time":"0000-00-00 00:00:00","status":"1"}]';
      foreach ($file->content as $row) {
        $data = array(
          'id_penduduk' => '', 
          'id_kartu_keluarga' => '' , 
          'nomor_kartu_keluarga' => $row->NO_KK, 
          'nama_penduduk' => $row->NAMA_LGKP, 
          'nik' => $row->NIK, 
          'tempat_lahir' => $row->TMPT_LHR, 
          'tanggal_lahir' => $row->TGL_LHR, 
          'id_jenis_kelamin' => $this->get_data('jenis_kelamin', $row->JENIS_KLMIN), 
          'id_shdk' => $this->get_data('shdk', $row->STAT_HBKEL), 
          'alamat' => $row->ALAMAT, 
          'rt' => $row->NO_RT, 
          'rw' => $row->NO_RW, 
          'dusun' => $row->DUSUN, 
          'id_desa' => $this->get_data('desa', $row->KEL_NAME), 
          'kode_desa' => $row->NO_KEL, 
          'id_kecamatan' => $this->get_data('kecamatan', $row->KEC_NAME),  
          'kode_kecamatan' => $row->NO_KEC, 
          'id_kabupaten' => $this->get_data('kabupaten', $row->KAB_NAME), 
          'kode_kabupaten' => $row->NO_KAB, 
          'id_propinsi' => $this->get_data('propinsi', $row->PROP_NAME),  
          'kode_propinsi' => $row->NO_PROP, 
          'id_agama' => $this->get_data('agama', $row->AGAMA), 
          'id_pendidikan' => $this->get_data('pendidikan', $row->PDDK_AKH), 
          'id_pekerjaan' => $this->get_data('pekerjaan', $row->JENIS_PKRJN), 
          'id_kewarganegaraan' => '',
          'nama_ibu_kandung' => '', 
          'nama_bapak_kandung' => '', 
          'id_golongan_darah' => $this->get_data('golongan_darah', $row->GOL_DARAH), 
          'nomor_akta_lahir' => '', 
          'tgl_akta_lahir' => '', 
          'id_status_pernikahan' => $this->get_data('status_pernikahan', $row->STATUS_KAWIN), 
          'kode_pos' => $row->KODE_POS, 
        );
      }
      echo "[".json_encode($data)."]";
    }

   public function get_data($table, $keyword)
    {
      $value1 = "id_".$table;
      $value2 = "nama_".$table;
      $this->db->select("$value1 as id");
      $this->db->like($value2, $keyword, 'both'); 
      $result = $this->db->get($table);
      $id = 0;
      foreach ($result->result() as $row) {
        $id = $row->id;
      }
      return $id;
    }
}