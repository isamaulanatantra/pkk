<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tabel_perijinan extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Tabel_perijinan_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'tabel_perijinan/home';
    $this->load->view('tes', $data);
   }
   
  public function json_all_tabel_perijinan()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $start      = ($halaman - 1) * $limit;
    $fields     = "*";
    $where      = array(
      'tabel_perijinan.status!=' => 99,
      'tabel_perijinan.domain' => $web
    );
    $order_by   = 'tabel_perijinan.isi_tabel_perijinan';
    echo json_encode($this->Tabel_perijinan_model->json_all_tabel_perijinan($where, $limit, $start, $fields, $order_by));
   }
   
  public function simpan_tabel_perijinan()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_tabel_perijinan', 'judul_tabel_perijinan', 'required');
		$this->form_validation->set_rules('isi_tabel_perijinan', 'isi_tabel_perijinan', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'judul_tabel_perijinan' => trim($this->input->post('judul_tabel_perijinan')),
        'temp' => trim($this->input->post('temp')),
        'icon' => trim($this->input->post('icon')),
        'isi_tabel_perijinan' => trim($this->input->post('isi_tabel_perijinan')),
        'keterangan' => trim($this->input->post('keterangan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'tabel_perijinan';
      $id         = $this->Tabel_perijinan_model->simpan_tabel_perijinan($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'tabel_perijinan',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
  public function update_tabel_perijinan()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_tabel_perijinan', 'judul_tabel_perijinan', 'required');
		$this->form_validation->set_rules('isi_tabel_perijinan', 'isi_tabel_perijinan', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'judul_tabel_perijinan' => trim($this->input->post('judul_tabel_perijinan')),
        'isi_tabel_perijinan' => trim($this->input->post('isi_tabel_perijinan')),
        'icon' => trim($this->input->post('icon')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'tabel_perijinan';
      $where       = array(
        'tabel_perijinan.id_tabel_perijinan' => trim($this->input->post('id_tabel_perijinan')),
        'tabel_perijinan.domain' => $web
			);
      $this->Tabel_perijinan_model->update_data_tabel_perijinan($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'tabel_perijinan.id_tabel_perijinan' => $this->input->post('id_tabel_perijinan'),
        'tabel_perijinan.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_tabel_perijinan');
      $result = $this->db->get('tabel_perijinan');
      echo json_encode($result->result_array());
		}
   
   public function total_tabel_perijinan()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('tabel_perijinan');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function cek_tabel_perijinan()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('master_tabel_perijinan');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'judul_tabel_perijinan' => $row->nama_master_tabel_perijinan,
          'domain' => $web
          );
        $this->db->from('tabel_perijinan');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'domain' => $web,
            'judul_tabel_perijinan' => $row->nama_master_tabel_perijinan,
            'isi_tabel_perijinan' => $row->isi_master_tabel_perijinan_default,
            'temp' => '-',
            'created_by' => 1,
            'created_time' => date('Y-m-d H:i:s'),
            'updated_by' => 1,
            'updated_time' => date('Y-m-d H:i:s'),
            'deleted_by' => 1,
            'deleted_time' => date('Y-m-d H:i:s'),
            'keterangan' => '-',
            'icon' => 'fa-home',
            'status' => 1
                    
            );
          $table_name = 'tabel_perijinan';
          $this->Tabel_perijinan_model->simpan_tabel_perijinan($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
  public function inaktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_tabel_perijinan = $this->input->post('id_tabel_perijinan');
      $where = array(
        'id_tabel_perijinan' => $id_tabel_perijinan
        );
      $this->db->from('tabel_perijinan');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'tabel_perijinan';
        if( $cek <> '' ){
          $this->Tabel_perijinan_model->update_data_tabel_perijinan($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_tabel_perijinan = $this->input->post('id_tabel_perijinan');
      $where = array(
        'id_tabel_perijinan' => $id_tabel_perijinan
        );
      $this->db->from('tabel_perijinan');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'tabel_perijinan';
        if( $cek <> '' ){
          $this->Tabel_perijinan_model->update_data_tabel_perijinan($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
    
 }