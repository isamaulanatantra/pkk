<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Komponen extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Komponen_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'komponen/home';
    $this->load->view('tes', $data);
   }
   
  public function json_all_komponen()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $start      = ($halaman - 1) * $limit;
    $fields     = "*";
    $where      = array(
      'komponen.status!=' => 99,
      'komponen.domain' => $web
    );
    $order_by   = 'komponen.isi_komponen';
    echo json_encode($this->Komponen_model->json_all_komponen($where, $limit, $start, $fields, $order_by));
   }
   
  public function update_komponen()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('isi_komponen', 'isi_komponen', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'isi_komponen' => trim($this->input->post('isi_komponen')),
        'icon' => trim($this->input->post('icon')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'komponen';
      $where       = array(
        'komponen.id_komponen' => trim($this->input->post('id_komponen')),
        'komponen.domain' => $web
			);
      $this->Komponen_model->update_data_komponen($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'komponen.id_komponen' => $this->input->post('id_komponen'),
        'komponen.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_komponen');
      $result = $this->db->get('komponen');
      echo json_encode($result->result_array());
		}
   
   public function total_komponen()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('komponen');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function cek_komponen()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('master_komponen');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'judul_komponen' => $row->nama_master_komponen,
          'domain' => $web
          );
        $this->db->from('komponen');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'domain' => $web,
            'judul_komponen' => $row->nama_master_komponen,
            'isi_komponen' => $row->isi_master_komponen_default,
            'temp' => '-',
            'created_by' => 1,
            'created_time' => date('Y-m-d H:i:s'),
            'updated_by' => 1,
            'updated_time' => date('Y-m-d H:i:s'),
            'deleted_by' => 1,
            'deleted_time' => date('Y-m-d H:i:s'),
            'keterangan' => '-',
            'icon' => 'fa-home',
            'status' => 1
                    
            );
          $table_name = 'komponen';
          $this->Komponen_model->simpan_komponen($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
  public function inaktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_komponen = $this->input->post('id_komponen');
      $where = array(
        'id_komponen' => $id_komponen
        );
      $this->db->from('komponen');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'komponen';
        if( $cek <> '' ){
          $this->Komponen_model->update_data_komponen($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_komponen = $this->input->post('id_komponen');
      $where = array(
        'id_komponen' => $id_komponen
        );
      $this->db->from('komponen');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'komponen';
        if( $cek <> '' ){
          $this->Komponen_model->update_data_komponen($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
    
 }