<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Wilayah extends CI_Controller
{

  function __construct(){
  parent::__construct();
  $this->load->model('Crud_model');
  }
  public function json_all_propinsi(){
		$table = 'propinsi';
		$hak_akses = $this->session->userdata('hak_akses');
		if($hak_akses=='root'){
			$where   = array(
				'propinsi.status !=' => 99
				);
		}
		else{
			$where   = array(
				'propinsi.status !=' => 99
				);
			/*$where   = array(
				'propinsi.id_propinsi' => $this->session->userdata('id_propinsi')
				);*/
		}
		$fields  = 
			"
			propinsi.id_propinsi,
			propinsi.kode_propinsi,
			propinsi.nama_propinsi
			";
		$order_by  = 'propinsi.nama_propinsi';
		echo json_encode($this->Crud_model->opsi($table, $where, $fields, $order_by));
  }
  public function json_all_kabupaten_by_id_propinsi(){
		$id_propinsi  = $this->input->post('id_propinsi');
		$table = 'kabupaten';
		$where   = array(
			'kabupaten.status !=' => 99,
			'kabupaten.id_propinsi' => $id_propinsi
			);
		$fields  = 
			"
			kabupaten.id_kabupaten,
			kabupaten.kode_kabupaten,
			kabupaten.nama_kabupaten
			";
		$order_by  = 'kabupaten.nama_kabupaten';
		echo json_encode($this->Crud_model->json_all($table, $where, $fields, $order_by));
  }
  public function json_all_kecamatan_by_id_kabupaten(){
		$id_kabupaten  = $this->input->post('id_kabupaten');
		$table = 'kecamatan';
		$where   = array(
			'kecamatan.status !=' => 99,
			'kecamatan.id_kabupaten' => $id_kabupaten
			);
		$fields  = 
			"
			kecamatan.id_kecamatan,
			kecamatan.kode_kecamatan,
			kecamatan.nama_kecamatan
			";
		$order_by  = 'kecamatan.nama_kecamatan';
		echo json_encode($this->Crud_model->json_all($table, $where, $fields, $order_by));
  }
  public function json_all_desa_by_id_kecamatan(){
		$id_kecamatan  = $this->input->post('id_kecamatan');
		$table = 'desa';
		$where   = array(
			'desa.status !=' => 99,
			'desa.id_kecamatan' => $id_kecamatan
			);
		$fields  = 
			"
			desa.id_desa,
			desa.kode_desa,
			desa.nama_desa
			";
		$order_by  = 'desa.nama_desa';
		echo json_encode($this->Crud_model->json_all($table, $where, $fields, $order_by));
  }
  public function json_all_propinsi_id_propinsi_perusahaan(){
		$table = 'propinsi';
		$hak_akses = $this->session->userdata('hak_akses');
		if($hak_akses=='root'){
			$where   = array(
				'propinsi.status !=' => 99
				);
		}
		else{
			$where   = array(
				'propinsi.status !=' => 99
				);
			/*$where   = array(
				'propinsi.id_propinsi' => $this->session->userdata('id_propinsi')
				);*/
		}
		$fields  = 
			"
			propinsi.id_propinsi,
			propinsi.kode_propinsi,
			propinsi.nama_propinsi
			";
		$order_by  = 'propinsi.nama_propinsi';
		echo json_encode($this->Crud_model->opsi($table, $where, $fields, $order_by));
  }
  public function json_all_kabupaten_by_id_propinsi_id_kabupaten_perusahaan(){
		$id_propinsi  = $this->input->post('id_propinsi');
		$table = 'kabupaten';
		$where   = array(
			'kabupaten.status !=' => 99,
			'kabupaten.id_propinsi' => $id_propinsi
			);
		$fields  = 
			"
			kabupaten.id_kabupaten,
			kabupaten.kode_kabupaten,
			kabupaten.nama_kabupaten
			";
		$order_by  = 'kabupaten.nama_kabupaten';
		echo json_encode($this->Crud_model->json_all($table, $where, $fields, $order_by));
  }
  public function json_all_kecamatan_by_id_kabupaten_id_kecamatan_perusahaan(){
		$id_kabupaten  = $this->input->post('id_kabupaten');
		$table = 'kecamatan';
		$where   = array(
			'kecamatan.status !=' => 99,
			'kecamatan.id_kabupaten' => $id_kabupaten
			);
		$fields  = 
			"
			kecamatan.id_kecamatan,
			kecamatan.kode_kecamatan,
			kecamatan.nama_kecamatan
			";
		$order_by  = 'kecamatan.nama_kecamatan';
		echo json_encode($this->Crud_model->json_all($table, $where, $fields, $order_by));
  }
  public function json_all_desa_by_id_kecamatan_id_desa_perusahaan(){
		$id_kecamatan  = $this->input->post('id_kecamatan');
		$table = 'desa';
		$where   = array(
			'desa.status !=' => 99,
			'desa.id_kecamatan' => $id_kecamatan
			);
		$fields  = 
			"
			desa.id_desa,
			desa.kode_desa,
			desa.nama_desa
			";
		$order_by  = 'desa.nama_desa';
		echo json_encode($this->Crud_model->json_all($table, $where, $fields, $order_by));
  }
  public function load_wilayah_by_id_desa(){
		$id_desa  = $this->input->post('id_desa');
		$table = 'desa';
		$where   = array(
			'desa.status !=' => 99,
			'desa.id_desa' => $id_desa
			);
		$fields  = 
			"
			desa.id_desa,
			desa.kode_desa,
			desa.nama_desa,
			desa.id_kecamatan,
			desa.id_kabupaten,
			desa.id_propinsi,
			(select kecamatan.nama_kecamatan from kecamatan where kecamatan.id_kecamatan=desa.id_kecamatan) as nama_kecamatan,
			(select kabupaten.nama_kabupaten from kabupaten where kabupaten.id_kabupaten=desa.id_kabupaten) as nama_kabupaten,
			(select propinsi.nama_propinsi from propinsi where propinsi.id_propinsi=desa.id_propinsi) as nama_propinsi
			";
		$order_by  = 'desa.nama_desa';
		echo json_encode($this->Crud_model->json_all($table, $where, $fields, $order_by));
  }
  public function load_dusun_by_id_desa(){
		$id_dusun  = $this->input->post('id_dusun');
		$table = 'dusun';
		$where   = array(
			'dusun.status !=' => 99,
			'dusun.id_dusun' => $id_dusun
			);
		$fields  = 
			"
			*,
			(select desa.nama_desa from desa where desa.id_desa=dusun.id_desa) as nama_desa,
			(select kecamatan.nama_kecamatan from kecamatan where kecamatan.id_kecamatan=dusun.id_kecamatan) as nama_kecamatan,
			(select kabupaten.nama_kabupaten from kabupaten where kabupaten.id_kabupaten=dusun.id_kabupaten) as nama_kabupaten,
			(select propinsi.nama_propinsi from propinsi where propinsi.id_propinsi=dusun.id_propinsi) as nama_propinsi
			";
		$order_by  = 'dusun.nama_dusun';
		echo json_encode($this->Crud_model->json_all($table, $where, $fields, $order_by));
  }
  public function get_wilayah_by_id_desa(){
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$id_desa  = trim($this->input->post('id_desa'));
		$table = 'desa';
		$join = 3;
		$order_by = 'propinsi.nama_propinsi'; 
		$short_by = 'asc';
		$fields  = "desa.id_desa, desa.nama_desa, kecamatan.id_kecamatan, kecamatan.nama_kecamatan, kabupaten.id_kabupaten, kabupaten.nama_kabupaten, propinsi.id_propinsi, propinsi.nama_propinsi";
		$where   = array(
			'desa.id_desa' => $id_desa
			);

		if($this->form_validation->run() == FALSE)
			{
				echo 0;
			}
		else
			{
				echo json_encode($this->Crud_model->get_by_id($table, $where, $fields, $join, $order_by, $short_by));
			}
  }
  
  public function get_kabupaten_by_id_propinsi()
  {
  $this->form_validation->set_rules('id_propinsi', 'id_propinsi', 'required');
  $id_propinsi  = trim($this->input->post('id_propinsi'));
  $table = 'kabupaten';
  $join = 0;
  $order_by = 'kabupaten.nama_kabupaten'; 
  $short_by = 'asc';
  $fields  = "kabupaten.id_kabupaten, kabupaten.nama_kabupaten";
  $where   = array(
    'kabupaten.id_propinsi' => $id_propinsi
    );

  if($this->form_validation->run() == FALSE)
    {
      echo 0;
    }
  else
    {
      echo json_encode($this->Crud_model->get_by_id($table, $where, $fields, $join, $order_by, $short_by));
    }
  }
  
  public function get_kecamatan_by_id_kabupaten()
  {
  $this->form_validation->set_rules('id_kabupaten', 'id_kabupaten', 'required');
  $id_kabupaten  = trim($this->input->post('id_kabupaten'));
  $table = 'kecamatan';
  $join = 0;
  $order_by = 'kecamatan.nama_kecamatan'; 
  $short_by = 'asc';
  $fields  = "kecamatan.id_kecamatan, kecamatan.nama_kecamatan";
  $where   = array(
    'kecamatan.id_kabupaten' => $id_kabupaten
    );

  if($this->form_validation->run() == FALSE)
    {
      echo 0;
    }
  else
    {
      echo json_encode($this->Crud_model->get_by_id($table, $where, $fields, $join, $order_by, $short_by));
    }
  }
  
  public function get_desa_by_id_kecamatan()
  {
  $this->form_validation->set_rules('id_kecamatan', 'id_kecamatan', 'required');
  $id_kecamatan  = trim($this->input->post('id_kecamatan'));
  $table = 'desa';
  $join = 0;
  $order_by = 'desa.nama_desa'; 
  $short_by = 'asc';
  $fields  = "desa.id_desa, desa.nama_desa";
  $where   = array(
    'desa.id_kecamatan' => $id_kecamatan
    );

  if($this->form_validation->run() == FALSE)
    {
      echo 0;
    }
  else
    {
      echo json_encode($this->Crud_model->get_by_id($table, $where, $fields, $join, $order_by, $short_by));
    }
  }

  public function get_penduduk_by_nik()
  {
  $table_name = 'penduduk';
  $nik  = trim($this->input->post('nik'));
  $fields  = "*";
  $where   = array(
    'penduduk.nik' => $nik
    );
  $order_by  = 'penduduk.nama_penduduk';
  echo json_encode($this->Wilayah_model->get_penduduk_by_nik($table_name, $where, $fields, $order_by));
  }
  
  public function get_propinsi_by_id()
  {
  $id_propinsi  = $this->input->post('id_propinsi');
  $table = 'propinsi';
  $where   = array(
    'propinsi.status !=' => 99,
    'propinsi.id_propinsi' => $id_propinsi
    );
  $fields  = 
    "
    propinsi.id_propinsi,
    propinsi.kode_propinsi,
    propinsi.nama_propinsi
    ";
  $order_by  = 'propinsi.nama_propinsi';
  echo json_encode($this->Wilayah_model->json_all_wilayah($table, $where, $fields, $order_by));
  }
  public function json_all_kabupaten_by_id_propinsi_izin()
  {
  $id_propinsi  = $this->input->post('id_propinsi');
  $table = 'kabupaten';
  $where   = array(
    'kabupaten.status !=' => 99,
    'kabupaten.id_propinsi' => $id_propinsi
    );
  $fields  = 
    "
    kabupaten.id_kabupaten,
    kabupaten.kode_kabupaten,
    kabupaten.nama_kabupaten
    ";
  $order_by  = 'kabupaten.nama_kabupaten';
  echo json_encode($this->Wilayah_model->json_all_wilayah($table, $where, $fields, $order_by));
  }
	
 
  public function json_all_kecamatan_by_id_kabupaten_izin()
  {
  $id_kabupaten  = $this->input->post('id_kabupaten');
  $table = 'kecamatan';
  $where   = array(
    'kecamatan.status !=' => 99,
    'kecamatan.id_kabupaten' => $id_kabupaten
    );
  $fields  = 
    "
    kecamatan.id_kecamatan,
    kecamatan.kode_kecamatan,
    kecamatan.nama_kecamatan
    ";
  $order_by  = 'kecamatan.nama_kecamatan';
  echo json_encode($this->Wilayah_model->json_all_wilayah($table, $where, $fields, $order_by));
  }
  
  public function get_kabupaten_by_id()
  {
  $id_kabupaten  = $this->input->post('id_kabupaten');
  $table = 'kabupaten';
  $where   = array(
    'kabupaten.status !=' => 99,
    'kabupaten.id_kabupaten' => $id_kabupaten
    );
  $fields  = 
    "
    kabupaten.id_kabupaten,
    kabupaten.kode_kabupaten,
    kabupaten.nama_kabupaten
    ";
  $order_by  = 'kabupaten.nama_kabupaten';
  echo json_encode($this->Wilayah_model->json_all_wilayah($table, $where, $fields, $order_by));
  }

  public function get_kabupaten_by_idi_izin()
  {
  $id_kabupaten  = $this->input->post('id_kabupaten');
  $table = 'kabupaten';
  $where   = array(
    'kabupaten.status !=' => 99,
    'kabupaten.id_kabupaten' => $id_kabupaten
    );
  $fields  = 
    "
    kabupaten.id_kabupaten,
    kabupaten.kode_kabupaten,
    kabupaten.nama_kabupaten
    ";
  $order_by  = 'kabupaten.nama_kabupaten';
  echo json_encode($this->Wilayah_model->json_all_wilayah($table, $where, $fields, $order_by));
  }
	

  public function json_all_desa_by_id_kecamatan_izin()
  {
  $id_kecamatan  = $this->input->post('id_kecamatan');
  $table = 'desa';
  $where   = array(
    'desa.status !=' => 99,
    'desa.id_kecamatan' => $id_kecamatan
    );
  $fields  = 
    "
    desa.id_desa,
    desa.kode_desa,
    desa.nama_desa
    ";
  $order_by  = 'desa.nama_desa';
  echo json_encode($this->Wilayah_model->json_all_wilayah($table, $where, $fields, $order_by));
  }
  
  public function get_kecamatan_by_id()
  {
  $id_kecamatan  = $this->input->post('id_kecamatan');
  $table = 'kecamatan';
  $where   = array(
    'kecamatan.status !=' => 99,
    'kecamatan.id_kecamatan' => $id_kecamatan
    );
  $fields  = 
    "
    kecamatan.id_kecamatan,
    kecamatan.kode_kecamatan,
    kecamatan.nama_kecamatan
    ";
  $order_by  = 'kecamatan.nama_kecamatan';
  echo json_encode($this->Wilayah_model->json_all_wilayah($table, $where, $fields, $order_by));
  }

  public function get_kecamatan_by_id_izin()
  {
  $id_kecamatan  = $this->input->post('id_kecamatan');
  $table = 'kecamatan';
  $where   = array(
    'kecamatan.status !=' => 99,
    'kecamatan.id_kecamatan' => $id_kecamatan
    );
  $fields  = 
    "
    kecamatan.id_kecamatan,
    kecamatan.kode_kecamatan,
    kecamatan.nama_kecamatan
    ";
  $order_by  = 'kecamatan.nama_kecamatan';
  echo json_encode($this->Wilayah_model->json_all_wilayah($table, $where, $fields, $order_by));
  }
  
  public function get_desa_by_id()
  {
  $id_desa  = $this->input->post('id_desa');
  $table = 'desa';
  $where   = array(
    'desa.status !=' => 99,
    'desa.id_desa' => $id_desa
    );
  $fields  = 
    "
    desa.id_desa,
    desa.kode_desa,
    desa.nama_desa
    ";
  $order_by  = 'desa.nama_desa';
  echo json_encode($this->Wilayah_model->json_all_wilayah($table, $where, $fields, $order_by));
  }

 public function get_desa_by_id_izin()
  {
  $id_desa  = $this->input->post('id_desa');
  $table = 'desa';
  $where   = array(
    'desa.status !=' => 99,
    'desa.id_desa' => $id_desa
    );
  $fields  = 
    "
    desa.id_desa,
    desa.kode_desa,
    desa.nama_desa
    ";
  $order_by  = 'desa.nama_desa';
  echo json_encode($this->Wilayah_model->json_all_wilayah($table, $where, $fields, $order_by));
  }
  
}