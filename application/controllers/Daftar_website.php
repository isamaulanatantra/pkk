<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Daftar_website extends CI_Controller
{
  function __construct(){
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('User_model');
    
  }
   
  public function index(){
    $data['main_view'] = 'daftar_website/home';
    $this->load->view('back_bone', $data);
  }
  
  public functio load_table_daftar_informasi_publik_pembantu()
	{
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $id_kecamatan    = $this->input->post('id_kecamatan');
    $klasifikasi_informasi_publik_pembantu    = $this->input->post('klasifikasi_informasi_publik_pembantu');
		if($klasifikasi_informasi_publik_pembantu==''){
			$where_hak_akses="";
			$select_domain="";
		}
		elseif($klasifikasi_informasi_publik_pembantu=='web_desa'){
			$where_hak_akses="and users.hak_akses='".$this->input->post('klasifikasi_informasi_publik_pembantu')."'";
			$select_domain=",(select data_desa.desa_website from data_desa where data_desa.id_desa=users.id_desa) as data_website";
		}
		else{
			$where_hak_akses="and users.hak_akses='".$this->input->post('klasifikasi_informasi_publik_pembantu')."'";
			$select_domain=",(select data_skpd.skpd_website from data_skpd where data_skpd.id_skpd=users.id_skpd) as data_website";
		}
		if($id_kecamatan==''){
			$where_kecamatan="";
		}
		else{
			$where_kecamatan="and data_kecamatan.kecamatan_website=dasar_website.domain and data_kecamatan.id_kecamatan='".$this->input->post('id_kecamatan')."'";
		}
		$w = $this->db->query("
		SELECT 
				*
		from dasar_website, data_kecamatan
		where dasar_website.status=1
    ".$where_kecamatan."
		limit ".$limit."
		");
		
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			echo '<tr style="padding: 2px;" id_dasar_website="'.$h->id_dasar_website.'" id="'.$h->id_dasar_website.'" >';
				echo '<td style="padding: 2px;">'.$nomor.'</td>';
        echo '<td style="padding: 2px;"><a target="_blank" href="https://'.$h->domain.'" class="btn btn-flat">'.$h->domain.'</td>';
        echo '<td style="padding: 2px;">'.$h->keterangan.'</td>';
        echo '<td style="padding: 2px;">'.$h->alamat.'</td>';
			echo '</tr>';
		}
	}
  public functio option_kecamatan(){
		$w = $this->db->query("
		SELECT *
		from kecamatan
		where kecamatan.status = 1 
		order by id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
}