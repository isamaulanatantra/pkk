<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pasar extends CI_Controller
 {
  function __construct()
   {
    parent::__construct();
    //$this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Cetak_model');
    $this->load->model('Crud_model');
    $this->load->model('Pasar_model');
   }
  public function check_login()
   {
    $table_name = 'users';
    $where      = array(
      'users.id_users' => $this->session->userdata('id_users')
    );
    $a          = $this->Crud_model->semua_data($where, $table_name);
    if ($a == 0)
     {
        return redirect(''.base_url().'user/login');
     }
   }
  public function index()
   {
    $this->check_login();
    $where             = array(
      'pasar.status !=' => 99
    );
    $a                 = $this->Pasar_model->json_semua_pasar($where);
    $data['per_page']  = 20;
    $data['total']     = ceil($a / 20);
    $data['main_view'] = 'pasar/home';
    $this->load->view('backbone_mdb', $data);
   }
  public function load_id_pasar_by_filter()
   {
    $halaman    = 1;
    $limit      = 20;
    $start      = ($halaman - 1) * $limit;
    $fields     = "
				
				pasar.id_pasar,
				pasar.kode_pasar,
				pasar.nama_pasar,
								
				";
    $where      = array(
      'pasar.status !=' => 99
    );
    $order_by   = 'pasar.id_pasar';
    echo json_encode($this->Pasar_model->load_pasar($where, $limit, $start, $fields, $order_by));
   }
  public function json_all_pasar()
   {
    $where      = array(
      'pasar.status !=' => 99
    );
    $a          = $this->Pasar_model->json_semua_pasar($where);
    $halaman    = $this->input->get('halaman');
    $limit      = 20;
    $start      = ($halaman - 1) * $limit;
    $fields     = "
				
				pasar.id_pasar,
				pasar.kode_pasar,
				pasar.nama_pasar,
				pasar.inserted_by,
				pasar.inserted_time,
				pasar.updated_by,
				pasar.updated_time,
				pasar.deleted_by,
				pasar.deleted_time,
				pasar.temp,
				pasar.keterangan,
				pasar.status,
								
				";
    $where      = array(
      'pasar.status !=' => 99
    );
    $order_by   = 'pasar.kode_pasar';
    echo json_encode($this->Pasar_model->json_all_pasar($where, $limit, $start, $fields, $order_by));
   }
  public function simpan_pasar()
   {
		//$this->form_validation->set_rules('id_pasar', 'id_pasar', 'required');
		$id_pasar         = '';
		$this->form_validation->set_rules('kode_pasar', 'kode_pasar', 'required');
		$kode_pasar         = trim($this->input->post('kode_pasar'));
		$this->form_validation->set_rules('nama_pasar', 'nama_pasar', 'required');
		$nama_pasar         = trim($this->input->post('nama_pasar'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = '';
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = '';
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = '';
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = '';
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = trim($this->input->post('temp'));
		//$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '1';
				if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
			
				'kode_pasar' => $kode_pasar,
				'nama_pasar' => $nama_pasar,
				'inserted_by' => $inserted_by,
				'inserted_time' => $inserted_time,
				'temp' => $temp,
				'keterangan' => $keterangan,
				'status' => $status,
								
				);
      $table_name = 'pasar';
      $id         = $this->Pasar_model->simpan_pasar($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'pasar',
        'temp' => $temp
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
  public function update_data_pasar()
   {
    $this->form_validation->set_rules('id_pasar', 'id_pasar', 'required');
		$id_pasar         = trim($this->input->post('id_pasar'));
		$this->form_validation->set_rules('kode_pasar', 'kode_pasar', 'required');
		$kode_pasar         = trim($this->input->post('kode_pasar'));
		$this->form_validation->set_rules('nama_pasar', 'nama_pasar', 'required');
		$nama_pasar         = trim($this->input->post('nama_pasar'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = '';
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '';
				if ($this->form_validation->run() == FALSE)
     {
      echo '[]';
     }
    else
     {
      $data_update = array(
			
				'kode_pasar' => $kode_pasar,
				'nama_pasar' => $nama_pasar,
				'updated_by' => $updated_by,
				'updated_time' => $updated_time,
				'keterangan' => $keterangan,
								
				);
      $table_name  = 'pasar';
      $where       = array(
        'pasar.id_pasar' => $id_pasar      
			);
      $this->Pasar_model->update_data_pasar($data_update, $where, $table_name);
      echo '[{"save":"ok"}]';
     }
   }
  public function il_simpan_pasar()
   {
		$this->form_validation->set_rules('id_pasar', 'id_pasar', 'required');
		$id_pasar         = trim($this->input->post('id_pasar'));
		$this->form_validation->set_rules('kode_pasar', 'kode_pasar', 'required');
		$kode_pasar         = trim($this->input->post('kode_pasar'));
		$this->form_validation->set_rules('nama_pasar', 'nama_pasar', 'required');
		$nama_pasar         = trim($this->input->post('nama_pasar'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = '';
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '';
				
		if ($this->form_validation->run() == FALSE)
     {
      echo 'Error';
     }
    else
     {
      $data_update = array(
        
				'kode_pasar' => $kode_pasar,
				'nama_pasar' => $nama_pasar,
				'updated_by' => $updated_by,
				'updated_time' => $updated_time,
				'keterangan' => $keterangan,
								
				);
      $table_name  = 'pasar';
      $where       = array(
        'pasar.id_pasar' => $id_pasar      );
      $this->Pasar_model->update_data_pasar($data_update, $where, $table_name);
      echo 'Success';
     }
   }
  public function ag_simpan_pasar()
   {
		$this->form_validation->set_rules('id_pasar', 'id_pasar', 'required');
		$id_pasar         = trim($this->input->post('id_pasar'));
		$this->form_validation->set_rules('kode_pasar', 'kode_pasar', 'required');
		$kode_pasar         = trim($this->input->post('kode_pasar'));
		$this->form_validation->set_rules('nama_pasar', 'nama_pasar', 'required');
		$nama_pasar         = trim($this->input->post('nama_pasar'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = '';
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '';
				if ($this->form_validation->run() == FALSE)
     {
      echo '[]';
     }
    else
     {
      $data_update = array(
        
				'kode_pasar' => $kode_pasar,
				'nama_pasar' => $nama_pasar,
				'updated_by' => $updated_by,
				'updated_time' => $updated_time,
				'keterangan' => $keterangan,
								
				 );
      $table_name  = 'pasar';
      $where       = array(
        'pasar.id_pasar' => $id_pasar      
				);
      $this->Pasar_model->update_data_pasar($data_update, $where, $table_name);
      echo '[{"pasar":"ok", "jumlah":"ok"}]';
     }
   }
  public function cetak()
   {
    $where             = array(
      'pasar.status !=' => 99
    );
    $data['data_pasar'] = $this->Pasar_model->data_pasar($where);
    $this->load->view('pasar/cetak', $data);
   }
  public function pasar_get_by_id()
   {
    $table_name = 'pasar';
    $id         = $_GET['id'];
    $fields     = "
				pasar.id_pasar,
				pasar.kode_pasar,
				pasar.nama_pasar,
				pasar.inserted_by,
				pasar.inserted_time,
				pasar.updated_by,
				pasar.updated_time,
				pasar.deleted_by,
				pasar.deleted_time,
				pasar.temp,
				pasar.keterangan,
				pasar.status,
								";
    $where      = array(
      'pasar.id_pasar' => $id
    );
    $order_by   = 'pasar.nama_pasar';
    echo json_encode($this->Pasar_model->get_by_id($table_name, $where, $fields, $order_by));
   }
  public function hapus_pasar()
   {
    $table_name = 'pasar';
    $id         = $_GET['id'];
    $where      = array(
      'pasar.id_pasar' => $id
    );
    $t          = $this->Pasar_model->json_semua_pasar($where, $table_name);
    if ($t == 0)
     {
      echo ' {"errors":"Yes"} ';
     }
    else
     {
      $data_update = array(
        'pasar.status' => 99,
        'pasar.deleted_by' => $this->session->userdata('id_users'),
        'pasar.deleted_time' => date('Y-m-d H:i:s')
      );
      $where       = array(
        'pasar.id_pasar' => $id
      );
      $this->Pasar_model->update_data_pasar($data_update, $where, $table_name);
      echo ' {"errors":"No"} ';
     }
   }
  public function cari_pasar()
   {
    $table_name = 'pasar';
    $key_word   = $_GET['key_word'];
    $where      = array(
      'pasar.status !=' => 99
    );
    $field      = 'pasar.nama_pasar';
    $a          = $this->Pasar_model->count_all_search_pasar($where, $key_word, $table_name, $field);
    if (empty($key_word))
     {
      echo '[]';
      exit;
     }
    else if ($a == 0)
     {
      echo '[{"jumlah":"0"}]';
      exit;
     }
    $fields     = "
				pasar.id_pasar,
				pasar.kode_pasar,
				pasar.nama_pasar,
				pasar.inserted_by,
				pasar.inserted_time,
				pasar.updated_by,
				pasar.updated_time,
				pasar.deleted_by,
				pasar.deleted_time,
				pasar.temp,
				pasar.keterangan,
				pasar.status,
								";
    $where      = array(
      'pasar.status !=' => 99
    );
    $field_like = 'pasar.nama_pasar';
    $limit      = $_GET['limit'];
    $start      = (($_GET['start']) * $limit);
    $order_by   = ''.$field_like.'';
    echo json_encode($this->Pasar_model->search($table_name, $fields, $where, $limit, $start, $field_like, $key_word, $order_by));
   }
  public function count_all_cari_pasar()
   {
    $table_name = 'pasar';
    $key_word   = $_GET['key_word'];
    if (empty($key_word))
     {
      echo '[]';
      exit;
     }
    $where = array(
      'pasar.status !=' => 99
    );
    $field = 'pasar.nama_pasar';
    $a     = $this->Pasar_model->count_all_search_pasar($where, $key_word, $table_name, $field);
    $limit = $_GET['limit'];
    echo '[{"pasar":"' . ceil($a / $limit) .'", "jumlah":"'.$a .'"}]';
   }
  public function download_xls()
   {
    $this->load->library('excel');
    $objPHPExcel = new PHPExcel();
    $objPHPExcel
			->getProperties()
			->setCreator("Budi Utomo")
			->setLastModifiedBy("Budi Utomo")
			->setTitle("Laporan I")
			->setSubject("Office 2007 XLSX Document")
			->setDescription("Laporan untuk menampilkan kunjungan pasien")
			->setKeywords("Laporan Halaman")
			->setCategory("Bentuk XLS");
    $objPHPExcel
			->setActiveSheetIndex(0)
			->setCellValue('A1', 'Cetak')
			->mergeCells('A1:G1')
			->setCellValue('A2', 'Laporan Data Halaman')
			->mergeCells('A2:G2')
			->setCellValue('A3', 'Tanggal Download : ' . date('d/m/Y') .' ')
			->mergeCells('A3:G3')
			->setCellValue('A6', 'No')
			->setCellValue('B6', 'Nama Pasar')
			->setCellValue('C6', 'Kode Pasar')
			->setCellValue('D6', 'Keterangan');
    $table    = 'pasar';
    $fields     = "
				pasar.id_pasar,
				pasar.kode_pasar,
				pasar.nama_pasar,
				pasar.inserted_by,
				pasar.inserted_time,
				pasar.updated_by,
				pasar.updated_time,
				pasar.deleted_by,
				pasar.deleted_time,
				pasar.temp,
				pasar.keterangan,
				pasar.status,
								";
    $where    = array(
      'pasar.status !=' => 99
    );
    $order_by = 'pasar.nama_pasar';
    $b        = $this->Cetak_model->cetak_single_table($table, $fields, $where, $order_by);
    $i        = 7;
    $no       = 0;
    foreach ($b->result() as $b1)
     {
      $no = $no + 1;
      $objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, ''.$no.'')
				->setCellValue('B'.$i, ''.$b1->nama_pasar.'')
				->setCellValue('C'.$i, '\''.$b1->kode_pasar.'')
				->setCellValue('D'.$i, ''.$b1->keterangan.'')
			;
				
      $objPHPExcel
				->getActiveSheet()
				->getStyle('A'.$i.':D'.$i.'')
				->getBorders()
				->getAllBorders()
				->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
     }
    $objPHPExcel->getActiveSheet()->setTitle('Probable Clients');
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
    $objPHPExcel->getActiveSheet()->getStyle('A6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('C6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('D6')->getFont()->setBold(true);
		
    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->getStartColor()->setARGB('cccccc');
    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		
    $objPHPExcel->setActiveSheetIndex(0);
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Halaman_' . date('ymdhis') .'.xls"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') .' GMT');
    header('Cache-Control: cache, must-revalidate');
    header('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
   }
	 
  public function cetak_pdf()
   {
    $table             = 'pasar';
    $fields     = "
				pasar.id_pasar,
				pasar.kode_pasar,
				pasar.nama_pasar,
				pasar.inserted_by,
				pasar.inserted_time,
				pasar.updated_by,
				pasar.updated_time,
				pasar.deleted_by,
				pasar.deleted_time,
				pasar.temp,
				pasar.keterangan,
				pasar.status,
								";
    $where             = array(
      'pasar.status !=' => 99
    );
    $order_by          = 'pasar.nama_pasar';
    $data['data_pasar'] = $this->Cetak_model->cetak_single_table($table, $fields, $where, $order_by);
    $this->load->view('pasar/pdf', $data);
    $html = $this->output->get_output();
    $this->load->library('dompdf_gen');
    $this->dompdf->load_html($html);
    $this->dompdf->set_paper('a4', 'portrait');
    $this->dompdf->render();
    $this->dompdf->stream("cetak_pasar_" . date('d_m_y') . ".pdf");
   } 
	public function search_pasar()
		{
		$key_word = trim($this->input->post('key_word'));
    $halaman    = $this->input->post('halaman');
    $limit      = 20;
    $start      = ($halaman - 1) * $limit;
    $fields     = "
				pasar.id_pasar,
				pasar.kode_pasar,
				pasar.nama_pasar,
				pasar.inserted_by,
				pasar.inserted_time,
				pasar.updated_by,
				pasar.updated_time,
				pasar.deleted_by,
				pasar.deleted_time,
				pasar.temp,
				pasar.keterangan,
				pasar.status,
								";
    $where      = array(
      'pasar.status !=' => 99
    );
    $order_by   = 'pasar.nama_pasar';
    echo json_encode($this->Pasar_model->search_pasar($fields, $where, $limit, $start, $key_word, $order_by));
		}
	public function count_all_search_pasar()
		{
			$table_name = 'pasar';
			$key_word = $_GET['key_word'];
			$field = 'nama_pasar';
			$where = array(
					''.$table_name.'.status' => 1
				);
			$a = $this->Pasar_model->count_all_search_pasar($where, $key_word, $table_name, $field);
			$limit = 20;
			echo ceil($a/$limit); 
		}
	public function auto_suggest()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
			$where      = array(
				'pasar.status !=' => 99
			);
			$fields     = "
				pasar.id_pasar,
				pasar.kode_pasar,
				pasar.nama_pasar,
				pasar.inserted_by,
				pasar.inserted_time,
				pasar.updated_by,
				pasar.updated_time,
				pasar.deleted_by,
				pasar.deleted_time,
				pasar.temp,
				pasar.keterangan,
				pasar.status,
								pasar.nama_pasar as value
				";
			echo json_encode( $this->Pasar_model->auto_suggest($q, $where, $fields) );
			}
		}
 }