<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome_admin extends CI_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->library('Bcrypt');
		$this->load->library('Uut');
		$this->load->library('Crud_model');
		$this->load->model('Posting_model');
	}

	public function index()
	{
		$web = $this->uut->namadomain(base_url());
		$data['judul'] = 'Selamat datang';
		$data['website'] = ' ' . $web . ' ';
		$where = array(
			'id_users' => $this->session->userdata('id_users')
		);
		$this->db->where($where);
		$this->db->from('users');
		$jml = $this->db->count_all_results();
		if ($jml > 0) {
			$this->db->where($where);
			$query = $this->db->get('users');
			foreach ($query->result() as $row) {
				if ($row->hak_akses == 'admin_web_dinkes') {
					$data['main_view'] = 'welcome/welcome_admin';
					$this->load->view('tes', $data);
				} elseif ($row->hak_akses == 'register') {
					$data['main_view'] = 'welcome/welcome_register';
					$this->load->view('back_bone', $data);
				} elseif ($row->hak_akses == 'web_pkk_kecamatan') {
					$data['main_view'] = 'welcome/welcome_pkk';
					$this->load->view('back_bone1', $data);
				} elseif ($row->hak_akses == 'admin_web_diskominfo') {
					$data1 = '';
					$where1 = array(
						'posting.status =' => 1,
						'data_skpd.status' => 1,
						'data_skpd.id_kecamatan' => 0
					);
					$this->db->where($where1);
					$this->db->join('data_skpd', 'data_skpd.skpd_website=posting.domain');
					$query1 = $this->db->get('posting');

					$data['data1'] = $data1;
					foreach ($query1->result() as $row1) {
						$where1 = array(
							'posting.status =' => 1,
							'data_skpd.status' => 1,
							'posting.domain' => '' . $row1->domain . '',
							'data_skpd.id_kecamatan' => 0
						);
						$this->db->where($where1);
						$this->db->join('data_skpd', 'data_skpd.skpd_website=posting.domain');
						$this->db->from('posting');
						$data1 .= '"' . $this->db->count_all_results() . '", ';
						$data['domain'] = $row1->domain;
						$data['data1'] = $data1;
					}

					$data['main_view'] = 'welcome/welcome_diskominfo';
					$this->load->view('tes', $data);
				} else {
					$data['main_view'] = 'welcome/welcome_admin';
					$this->load->view('tes', $data);
				}
			}
		} else {
			exit;
		}
	}
}
