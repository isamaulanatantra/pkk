<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Header_website extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Header_website_model');
    $this->load->library('Uut');
    $this->load->library('upload', 'image_lib');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->helper('file');
    
   }
  
  public function upload()
		{
      $web=$this->uut->namadomain(base_url());
      $config['upload_path'] = './media/upload/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '50000';
			$this->upload->initialize($config);
			$uploadFiles = array('img_1' => 'myfile', 'img_2' => 'e_myfile', );		
			$this->load->library('image_lib');
			$newName = '-';
			$mode = $this->input->post('mode');
			$this->form_validation->set_rules('remake', 'remake', 'required');
			if ($this->form_validation->run() == FALSE)
				{
					echo
					'
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
						Mohon Keterangan Lampiran Diisi
					</div>
					';
				}
			else
				{
					foreach($uploadFiles as $key => $files)
					{
					if ($this->upload->do_upload($files)) 
						{
              
							$upload = $this->upload->data();
							$file = explode(".", $upload['file_name']);
							$prefix = date('Ymdhis');
							$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
							$filePath =  $upload['file_path'].$newName;
							rename($upload['full_path'],$filePath);
              if( $mode == 'edit' ){
							$data_input = array(
								'domain' => $web,
								'file_name' => $newName,
								'keterangan' => $this->input->post('remake'),
								'created_time' => date('Y-m-d H:i:s'),
								'created_by' => $this->session->userdata('id_users'),
                'updated_by' => 0,
                'updated_time' => date('Y-m-d H:i:s'),
                'deleted_by' => 0,
                'deleted_time' => date('Y-m-d H:i:s'),
                'status' => 1
								);
              }
              else{
							$data_input = array(
								'domain' => $web,
								'file_name' => $newName,
								'keterangan' => $this->input->post('remake'),
                'created_time' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('id_users'),
                'updated_by' => 0,
                'updated_time' => date('Y-m-d H:i:s'),
                'deleted_by' => 0,
                'deleted_time' => date('Y-m-d H:i:s'),
								'status' => 1
								);
              }
							$table_name = 'header_website';
							$id         = $this->Crud_model->save_data($data_input, $table_name);
							$config['image_library'] = 'gd2';
							$config['source_image']  = './media/upload/'.$newName.'';
							$config['new_image']  = './media/upload/s_'.$newName.'';						
							$config['width']	 = 300;
							$config['height']	= 300;
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
							$file = './media/upload/s_'.$newName.'';
							$j = get_mime_by_extension($file);
							$ex = explode('/', $j);
							$e = $ex[0];
							echo
							'
							<div class="alert alert-success alert-dismissable" id="img_upload">
							<i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							'; 
							if( $e == 'image'){
								echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"><img src="'.base_url().'media/upload/s_'.$newName.'" /></a>';
								}
							else{
								echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"> '.$newName.' </a>';
								}
							echo '</div>';
						}
					}
				}
		}
    
	public function load_lampiran()
		{
      $web=$this->uut->namadomain(base_url());
      $mode = $this->input->post('mode');
      $where    = array(
        'domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_header_website');
      $result = $this->db->get('header_website');
      echo json_encode($result->result_array());
		}
  
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_header_website = $this->input->post('id_header_website');
      $where = array(
        'id_header_website' => $id_header_website,
				'domain' => $web
        );
      $this->db->from('header_website');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $this->db->where($where);
        $this->db->delete('header_website');
        echo 1;
        }
		}
    
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'header_website/home';
    // $this->load->view('back_bone', $data);
    $this->load->view('tes', $data);
   }
   
  public function inaktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_header_website = $this->input->post('id_header_website');
      $where = array(
        'id_header_website' => $id_header_website
        );
      $this->db->from('header_website');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'header_website';
        if( $cek <> '' ){
          $this->Header_website_model->update_data_header_website($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_header_website = $this->input->post('id_header_website');
      $where = array(
        'id_header_website' => $id_header_website
        );
      $this->db->from('header_website');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'header_website';
        if( $cek <> '' ){
          $this->Header_website_model->update_data_header_website($data_update, $where, $table_name);
          echo 1;
          
          $web=$this->uut->namadomain(base_url());
          $data_update = array(
            'status' => 0
            );
          $where = array(
            'id_header_website !=' => $id_header_website,
            'domain' => $web
            );
          $table_name  = 'header_website';
          $this->Header_website_model->update_data_header_website($data_update, $where, $table_name);
          }
        else{
          echo 0;
          }
        }
		}
    
 }