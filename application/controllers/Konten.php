<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Konten extends CI_Controller
 {
    
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Posting_model');
   }
  
  public function index()
   {
    $data['main_view'] = 'konten/home';
    $this->load->view('back_bone', $data);
   }
  
  public function opd()
   {
    $data['main_view'] = 'konten/opd';
    $this->load->view('back_bone', $data);
   }
  
  function load_table()
	{
		$a = 0;
				$where = array(
				'id_users' => $this->session->userdata('id_users')
				);
				$this->db->where($where);
				$this->db->from('users');
				$jml = $this->db->count_all_results();
				if( $jml > 0 ){
					$this->db->where($where);
					$query = $this->db->get('users');
					foreach ($query->result() as $row)
						{
              if($row->hak_akses=='admin_web_skpd'){
                echo $this->posting();
              }
              else{
                echo $this->posting_desa();
              }
						// $this->load->view('menu/'.$row->hak_akses.''); admin_web_kecamatan
						}
					}
				else{
						exit;
					}
	}
  
  private function posting(){
    $aw = $this->db->query("
    SELECT * FROM data_skpd WHERE status = 1
    ");
    foreach($aw->result() as $ah){
      $web=$ah->skpd_website;
      $w = $this->db->query("
      SELECT *, (select users.user_name from users where users.id_users=posting.created_by) as oleh from posting
                    
      where posting.status = 1 
      and posting.domain='".$web."'
      order by posting.created_time desc
      limit 2
      ");
      
      $nomor = 0;
      foreach($w->result() as $h)
      {
        $nomor = $nomor + 1;
        echo '
        <tr id_posting="'.$h->id_posting.'" id="'.$h->id_posting.'" >
          <td>#</td>
          <td>'.$h->domain.' </td>
          <td><a target="_blank" href="https://'.$h->domain.'/postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.'</a></td>
          <td>'.$h->created_time.' </td>
          <td>'.$h->oleh.' </td>
        </tr>
        ';
      }
    }
		
	}
  private function posting_desa(){
    $aw = $this->db->query("
    SELECT data_desa.desa_website, desa.nama_desa, kecamatan.nama_kecamatan
    FROM data_desa, desa, kecamatan
    WHERE data_desa.id_desa = desa.id_desa
    AND desa.id_kecamatan = kecamatan.id_kecamatan
    AND kecamatan.id_kecamatan ='".$this->session->userdata('id_kecamatan')."'
    ");
    foreach($aw->result() as $ah){
      $web=$ah->desa_website;
      $w = $this->db->query("
      SELECT *, (select users.user_name from users where users.id_users=posting.created_by) as oleh from posting
                    
      where posting.status = 1 
      and posting.domain='".$web."'
      order by posting.created_time desc
      limit 1
      ");
      
      $nomor = 0;
      foreach($w->result() as $h)
      {
        $nomor = $nomor + 1;
        echo '
        <tr id_posting="'.$h->id_posting.'" id="'.$h->id_posting.'" >
          <td># </td>
          <td>'.$h->domain.' </td>
          <td><a target="_blank" href="https://'.$h->domain.'/postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.'</a></td>
          <td>'.$h->created_time.' </td>
          <td>'.$h->oleh.' </td>
        </tr>
        ';
      }
    }
		
	}
  
  public function json_all_posting()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $start      = ($halaman - 1) * $limit;
    $fields     = "*";
    $where      = array(
      'posting.status !=' => 99,
      'domain' => $web
    );
    $order_by   = 'posting.parent';
    echo json_encode($this->Posting_model->json_all_posting($where, $limit, $start, $fields, $order_by));
   }
   
   public function total_posting()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $this->db->from('posting');
      $where    = array(
        'status' => 1,
        'domain' => $web
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
    
 }