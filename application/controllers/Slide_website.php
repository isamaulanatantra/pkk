<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Slide_website extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Slide_website_model');
    $this->load->library('Uut');
    $this->load->library('upload', 'image_lib');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$this->load->helper('file');
    
   }
  
  public function upload()
		{
      $web=$this->uut->namadomain(base_url());
      $config['upload_path'] = './media/upload/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '50000';
			$this->upload->initialize($config);
			$uploadFiles = array('img_1' => 'myfile', 'img_2' => 'e_myfile', );		
			$this->load->library('image_lib');
			$newName = '-';
			$mode = $this->input->post('mode');
			$this->form_validation->set_rules('remake', 'remake', 'required');
			if ($this->form_validation->run() == FALSE)
				{
					echo
					'
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
						Mohon Keterangan Lampiran Diisi
					</div>
					';
				}
			else
				{
					foreach($uploadFiles as $key => $files)
					{
					if ($this->upload->do_upload($files)) 
						{
              
							$upload = $this->upload->data();
							$file = explode(".", $upload['file_name']);
							$prefix = date('Ymdhis');
							$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
							$filePath =  $upload['file_path'].$newName;
							rename($upload['full_path'],$filePath);
              if( $mode == 'edit' ){
							$data_input = array(
								'domain' => $web,
								'file_name' => $newName,
								'keterangan' => $this->input->post('remake'),
								'url_redirection' => $this->input->post('url_redirection'),
								'parent' => $this->input->post('parent'),
								'created_time' => date('Y-m-d H:i:s'),
								'created_by' => $this->session->userdata('id_users'),
								'updated_time' => date('Y-m-d H:i:s'),
								'updated_by' => $this->session->userdata('id_users'),
								'deleted_time' => date('Y-m-d H:i:s'),
								'deleted_by' => $this->session->userdata('id_users'),
								'status' => 1
								);
              }
              else{
							$data_input = array(
								'domain' => $web,
								'file_name' => $newName,
								'keterangan' => $this->input->post('remake'),
								'parent' => $this->input->post('parent'),
								'url_redirection' => $this->input->post('url_redirection'),
								'created_time' => date('Y-m-d H:i:s'),
								'created_by' => $this->session->userdata('id_users'),
								'updated_time' => date('Y-m-d H:i:s'),
								'updated_by' => $this->session->userdata('id_users'),
								'deleted_time' => date('Y-m-d H:i:s'),
								'deleted_by' => $this->session->userdata('id_users'),
								'status' => 1
								);
              }
							$table_name = 'slide_website';
							$id         = $this->Crud_model->save_data($data_input, $table_name);
							$config['image_library'] = 'gd2';
							$config['source_image']  = './media/upload/'.$newName.'';
							$config['new_image']  = './media/upload/s_'.$newName.'';						
							$config['width']	 = 300;
							$config['height']	= 300;
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
							$file = './media/upload/s_'.$newName.'';
							$j = get_mime_by_extension($file);
							$ex = explode('/', $j);
							$e = $ex[0];
							echo
							'
							<div class="alert alert-success alert-dismissable" id="img_upload">
							<i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							'; 
							if( $e == 'image'){
								echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"><img src="'.base_url().'media/upload/s_'.$newName.'" /></a>';
								}
							else{
								echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"> '.$newName.' </a>';
								}
							echo '</div>';
						}
					}
				}
		}
    
	public function load_lampiran()
		{
      $web=$this->uut->namadomain(base_url());
      $mode = $this->input->post('mode');
      $where    = array(
        'domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_slide_website');
      $result = $this->db->get('slide_website');
      echo json_encode($result->result_array());
		}
  
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_slide_website = $this->input->post('id_slide_website');
      $where = array(
        'id_slide_website' => $id_slide_website,
				'domain' => $web
        );
      $this->db->from('slide_website');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $this->db->where($where);
        $this->db->delete('slide_website');
        echo 1;
        }
		}
    
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'slide_website/home';
    $this->load->view('tes', $data);
   }
   
  public function inaktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_slide_website = $this->input->post('id_slide_website');
      $where = array(
        'id_slide_website' => $id_slide_website
        );
      $this->db->from('slide_website');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'slide_website';
        if( $cek <> '' ){
          $this->Slide_website_model->update_data_slide_website($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_slide_website = $this->input->post('id_slide_website');
      $where = array(
        'id_slide_website' => $id_slide_website
        );
      $this->db->from('slide_website');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'slide_website';
        if( $cek <> '' ){
          $this->Slide_website_model->update_data_slide_website($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
    
  function load_the_option()
	{
		$a = 0;
		echo $this->option_slide_website(0,$h="", $a);
	}
  
  private function option_slide_website($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from slide_website where parent='".$parent."' and domain='".$web."' and status != 99 order by id_slide_website");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_slide_website.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->keterangan.'';
			}
			else{
			$hasil .= ''.$h->keterangan.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_slide_website($h->id_slide_website,$hasil, $a);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
	
	
  function load_table_slide_website()
	{
    $web=$this->uut->namadomain(base_url());
		$mode = $this->input->post('mode');
		$a = 0;
		echo $this->slide_website(0,$h="", $a);
	}
	
  private function slide_website($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from slide_website
		where parent='".$parent."' 
		and status != 99
    and domain='".$web."'
    order by id_slide_website
		");
			$a = $a + 1;
		$nomor=0;
		foreach($w->result() as $h)
		{
			$nomor=$nomor+1;
			$hasil .= '<tr id_slide_website="'.$h->id_slide_website.'" id="'.$h->id_slide_website.'" >';
			$hasil .= '<td style="padding: 2px;"> '.$nomor.' </td>';
      if( $a > 1 ){
        $hasil .= '<td style="padding: 2px '.($a * 20).'px ;"><a href="'.$h->url_redirection.'" target="_blank">'.$h->keterangan.'</a></td>';
        }
			else{
        $hasil .= '<td style="padding: 2px;"> <a href="'.$h->url_redirection.'" target="_blank">'.$h->keterangan.'</a> </td>';
        }
      if( $h->status == 0 ){
        $hasil .= '<td style="padding: 2px;"><a href="#" id="aktifkan" class="text-danger"><i class="fa fa-times-circle"></i> Tidak Aktif </a></td>';
      }
      else{
        $hasil .= '<td style="padding: 2px;"><a href="#" id="inaktifkan" class="text-success"><i class="fa fa-check-circle"></i> Aktif</a></td>';
      }
			$hasil .= '<td style="padding: 2px;"> <a href="'.base_url().'media/upload/'.$h->file_name.'" target="_blank"> Download </td>';
			$hasil .= 
			'
			<td style="padding: 2px;"> 
			<a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> 
			</td>
			';
			$hasil .= '</tr>';
			$hasil = $this->slide_website($h->id_slide_website,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
	
 }