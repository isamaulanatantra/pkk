<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pendaftaran_pasar extends CI_Controller
 {
    
  function __construct(){
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Pendaftaran_pasar_model');
   }
  
  public function op(){
    $web=$this->uut->namadomain(base_url());
	$tanggal = date('Y-m-d H:i:s');
    $data['hari_ini'] = $tanggal;
    $data['tanggal_hari_ini'] = ''.$this->Crud_model->dateBahasaIndo($tanggal).' ';
    $data['judul_halaman'] = 'PENDAFTARAN PEDAGANG PASAR INDUK';
    $data['nama_halaman'] = 'pendaftaran_pasar';
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pengaduan_masyarakat/form_pendaftaran_pasar';
    $this->load->view('pengaduan_masyarakat', $data);
   }
  public function index(){
    $web=$this->uut->namadomain(base_url());
	$tanggal = date('Y-m-d H:i:s');
    $data['hari_ini'] = $tanggal;
    $data['tanggal_hari_ini'] = ''.$this->Crud_model->dateBahasaIndo($tanggal).' ';
    $data['judul_halaman'] = 'PENDAFTARAN PEDAGANG PASAR INDUK';
    $data['nama_halaman'] = 'pendaftaran_pasar';
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->status == 1 ){
            $data['status_komponen'] = $row1->status;
          }
          else{ 
          }
        }
    $data['main_view'] = 'pengaduan_masyarakat/form_pendaftaran_pasar1';
    $this->load->view('welcome_layanan', $data);
   }
  
  public function simpan_pelayanan_informasi()   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('tanggal_permohonan', 'tanggal_permohonan', 'required');
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('nomor_telp', 'nomor_telp', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('nik', 'nik', 'required');
		$this->form_validation->set_rules('no_kk', 'no_kk', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		
        $healthy = array("#", ",", ":", "=", "!", "?", "(", ")", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
        $yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
        $tanggal_permohonan = str_replace($healthy, $yummy, $this->input->post('tanggal_permohonan'));
        $nama = str_replace($healthy, $yummy, $this->input->post('nama'));
        $alamat = str_replace($healthy, $yummy, $this->input->post('alamat'));
        $nomor_telp = str_replace($healthy, $yummy, $this->input->post('nomor_telp'));
        $email = str_replace($healthy, $yummy, $this->input->post('email'));
        $nik = str_replace($healthy, $yummy, $this->input->post('nik'));
        $no_kk = str_replace($healthy, $yummy, $this->input->post('no_kk'));
				
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        		'domain' => $web,
				'forward' => '',
				'id_posting' => $this->input->post('id_posting'),
				'parent' => 0,
				'tanggal_permohonan' => $tanggal_permohonan,
				'nama' => $nama,
				'alamat' => $alamat,
				'nomor_telp' => $nomor_telp,
				'email' => $email,
				'nik' => $nik,
				'no_kk' => $no_kk,
        		'temp' => trim($this->input->post('temp')),
				'created_by' => $this->session->userdata('id_users'),
				'created_time' => trim($this->input->post('created_time')),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1,
				'balas' => 2

				);
      $table_name = 'pendaftaran_pasar';
      $id         = $this->Pendaftaran_pasar_model->simpan_pendaftaran_pasar($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'pendaftaran_pasar',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
	
  public function simpan_pendaftaran_pasar_no()   {
    $web=$this->uut->namadomain(base_url());
	$this->form_validation->set_rules('tanggal_permohonan', 'tanggal_permohonan', 'required');
	$this->form_validation->set_rules('nama', 'nama', 'required');
	$this->form_validation->set_rules('alamat', 'alamat', 'required');
	$this->form_validation->set_rules('nomor_telp', 'nomor_telp', 'required');
	$this->form_validation->set_rules('email', 'email', 'required');
	$this->form_validation->set_rules('nik', 'nik', 'required');
	$this->form_validation->set_rules('no_kk', 'no_kk', 'required');
	$this->form_validation->set_rules('temp', 'temp', 'required');
	
	$healthy = array("#", ",", ":", "=", "!", "?", "(", ")", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
	$yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
	$tanggal_permohonan = str_replace($healthy, $yummy, $this->input->post('tanggal_permohonan'));
	$nama = str_replace($healthy, $yummy, $this->input->post('nama'));
	$alamat = str_replace($healthy, $yummy, $this->input->post('alamat'));
	$nomor_telp = str_replace($healthy, $yummy, $this->input->post('nomor_telp'));
	$email = str_replace($healthy, $yummy, $this->input->post('email'));
	$nik = str_replace($healthy, $yummy, $this->input->post('nik'));
	$no_kk = str_replace($healthy, $yummy, $this->input->post('no_kk'));
				
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        		'domain' => $web,
				'forward' => '',
				'id_posting' => 0,
				'parent' => 0,
				'tanggal_permohonan' => $tanggal_permohonan,
				'nama' => $nama,
				'alamat' => $alamat,
				'nomor_telp' => $nomor_telp,
				'email' => $email,
				'nik' => $nik,
				'no_kk' => $no_kk,
        		'temp' => trim($this->input->post('temp')),
				'created_by' => $this->input->post('created_by'),
				'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1,
				'balas' => 0

				);
      	$table_name = 'pendaftaran_pasar';
      	$id         = $this->Pendaftaran_pasar_model->simpan_pendaftaran_pasar($data_input, $table_name);
      	echo $id;
	 	$table_name  = 'lampiran_pendaftaran_pasar';
      	$where       = array(
        	'table_name' => 'lampiran_pendaftaran_pasar',
        	'temp' => trim($this->input->post('temp'))
		);
		$data_update = array(
			'id_tabel' => $id
		);
      	$this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
	
  public function simpan_pendaftaran_pasar_no_balasan()   {
    $web=$this->uut->namadomain(base_url());
	$this->form_validation->set_rules('tanggal_permohonan', 'tanggal_permohonan', 'required');
	$this->form_validation->set_rules('nama', 'nama', 'required');
	$this->form_validation->set_rules('alamat', 'alamat', 'required');
	$this->form_validation->set_rules('nomor_telp', 'nomor_telp', 'required');
	$this->form_validation->set_rules('email', 'email', 'required');
	$this->form_validation->set_rules('nik', 'nik', 'required');
	$this->form_validation->set_rules('no_kk', 'no_kk', 'required');
	$this->form_validation->set_rules('temp', 'temp', 'required');
	
	$healthy = array("#", ",", ":", "=", "!", "?", "(", ")", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
	$yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
	$tanggal_permohonan = str_replace($healthy, $yummy, $this->input->post('tanggal_permohonan'));
	$nama = str_replace($healthy, $yummy, $this->input->post('nama'));
	$alamat = str_replace($healthy, $yummy, $this->input->post('alamat'));
	$nomor_telp = str_replace($healthy, $yummy, $this->input->post('nomor_telp'));
	$email = str_replace($healthy, $yummy, $this->input->post('email'));
	$nik = str_replace($healthy, $yummy, $this->input->post('nik'));
	$no_kk = str_replace($healthy, $yummy, $this->input->post('no_kk'));
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        		'domain' => $web,
				'forward' => '',
				'id_posting' => $this->input->post('id_posting'),
				'parent' => $this->input->post('parent'),
				'tanggal_permohonan' => $tanggal_permohonan,
				'nama' => $nama,
				'alamat' => $alamat,
				'nomor_telp' => $nomor_telp,
				'email' => $email,
				'nik' => $nik,
				'no_kk' => $no_kk,
				'temp' => trim($this->input->post('temp')),
						'created_by' => $this->input->post('created_by'),
				'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1,
				'balas' => 1

				);
      $table_name = 'pendaftaran_pasar';
      $id         = $this->Pendaftaran_pasar_model->simpan_pendaftaran_pasar($data_input, $table_name);
      echo $id;
			$table_name  = 'pendaftaran_pasar';
      $where       = array(
        'id_pendaftaran_pasar' => trim($this->input->post('parent'))
				);
			$data_update = array(
				'balas' => 1
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
			$table_name_attachment  = 'attachment';
      $where_attachment       = array(
        'table_name' => 'pendaftaran_pasar',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update_attachment = array(
				'id_tabel' => $id
				);
      $this->Pendaftaran_pasar_model->update_data_attachment($data_update_attachment, $where_attachment, $table_name_attachment);
     }
   }
	
  function load_table_pendaftaran_pasar(){
    $web=$this->uut->namadomain(base_url());
		if($web=='ppid.wonosobokab.go.id'){
			$web_aduan='';
		}else{
			$web_aduan="and domain ='".$web."'";
		}
		$w = $this->db->query("
		SELECT *, 
		(select user_name from users where users.id_users=pendaftaran_pasar.created_by) as nama_pengguna, 
		(select judul_posting from posting where posting.id_posting=pendaftaran_pasar.id_posting) as judul_posting_permohonan
		from pendaftaran_pasar
		where status != 99
		and parent = 0
		and balas != 2
		".$web_aduan."
		and tujuan_penggunaan_informasi != 'Pengaduan Masyarakat'
		order by created_time desc
		");
		$qforward = $this->db->query("
		SELECT *, 
		(select user_name from users where users.id_users=pendaftaran_pasar.created_by) as nama_pengguna, 
		(select judul_posting from posting where posting.id_posting=pendaftaran_pasar.id_posting) as judul_posting_permohonan
		from pendaftaran_pasar
		where status != 99
		and parent = 0
		and balas != 2
		and forward = '".$web."'
		and tujuan_penggunaan_informasi != 'Pengaduan Masyarakat'
		order by created_time desc
		");
		
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h){
			$tujuan = $h->tujuan_penggunaan_informasi;
			if($tujuan=='Komentar'){
			$costum = 'warning';
			$fa = 'comments';
			}
			elseif($tujuan=='Pengaduan Masyarakat'){
			$costum = 'success';
			$fa = 'user';
			}
			else{
			$costum = 'primary';
			$fa = 'envelope';
			}
			echo '
				<li id_pendaftaran_pasar="'.$h->id_pendaftaran_pasar.'" id="'.$h->id_pendaftaran_pasar.'">
									<i class="fa fa-'.$fa.' bg-'.$costum.'"></i>
									<div class="timeline-item card">
									  <div class="timeline-header">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm text-'.$costum.'" src="'.base_url().'assets/images/blank_user.png" alt="user image">
										<span class="username">
										  <a href="#">'.$h->nama.'</a>
										  <a href="#" class="float-right btn-tool"></a>
										</span>
										<span class="description">';
										$ses=$this->session->userdata('id_users');
										if(!$ses) { echo'...'; }
										else{echo ''.$h->alamat.'';} 
										echo ' - '.$this->Crud_model->dateBahasaIndo1($h->created_time).'
										</span>
									  </div>
									  </div>

									  <div class="timeline-body">';
										if(!$ses) { echo''; }
										else{
											echo '<p><u>Pekerjaan:</u> '.$h->pekerjaan.'</p>';
											echo '<p><u>Nomor Telp.:</u> '.$h->nomor_telp.'</p>';
											echo '<p><u>Email:</u> '.$h->email.'</p>';
											echo '<p><u>Judul Posting:</u> '.$h->judul_posting_permohonan.'</p>';
											echo '<p><u>Tujuan:</u> <span class="text-'.$costum.'">'.$h->tujuan_penggunaan_informasi.'</span></p>';
											// echo '<p><u>Website:</u> <a href="https://'.$h->domain.'/pendaftaran_pasar" class="text-muted">'.$h->domain.'</a></p>';
											echo '<p><u>Rincian:</u></p>';
											} 
										echo '
										<p>'.$h->rincian_informasi_yang_diinginkan.'</p>
									  </div>
									  <div class="timeline-footer card-comment">
										  <p>
											<a href="#" class="link-black text-sm"><i class="fa fa-thumbs-up mr-1 text-'.$costum.'"></i></a>
											<a href="#" class="link-black text-sm mr-2"><i class="fa fa-thumbs-down mr-1 text-'.$costum.'"></i></a>
											<a href="https://'.$h->domain.'/pendaftaran_pasar" class="text-muted link-black text-sm mr-2"><i class="fa fa-tags mr-1 text-'.$costum.'"></i>'.$h->domain.'</a>';
											if($h->forward==''){
												echo'';
											}else{
												echo'
												<a href="https://'.$h->forward.'/pendaftaran_pasar/detail/'.$h->id_pendaftaran_pasar.'" class="text-muted link-black text-sm mr-2"><i class="fa fa-mail-reply mr-1 text-warning"></i> '.$h->forward.'</a>
												';
											}
											echo'
											<a href="https://'.$h->domain.'/pendaftaran_pasar/detail/'.$h->id_pendaftaran_pasar.'" class="text-muted link-black text-sm mr-2">';
													$w11 = $this->db->query("
													SELECT *
													FROM pendaftaran_pasar
													WHERE parent = ".$h->id_pendaftaran_pasar."
													and status=1
													");
													$query_like_admin = $this->db->query("
													SELECT *
													FROM pendaftaran_pasar
													WHERE parent = ".$h->id_pendaftaran_pasar."
													and status=1
													and nama
													LIKE 'Admin%'
													");
													$jumlah_comments = $w11->num_rows(); 
													$jumlah_like = $query_like_admin->num_rows();
													if(($query_like_admin->num_rows())>0){
														echo '<i class="fa fa-eye mr-1 text-success"></i>';
													}
													else{
														echo '<i class="fa fa-eye mr-1 text-warning"></i>';
													}
												echo ' '.$jumlah_comments.' Baca Selengkapnya
											</a>
											</p>
									  </div>
									  
									</div>
								  </li>
                      <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                      </li>
			';
		}
		foreach($qforward->result() as $rforward){
			echo '
				<li id_pendaftaran_pasar="'.$rforward->id_pendaftaran_pasar.'" id="'.$rforward->id_pendaftaran_pasar.'">
									<i class="fa fa-'.$fa.' bg-'.$costum.'"></i>
									<div class="timeline-item card">
									  <div class="timeline-header">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm text-'.$costum.'" src="'.base_url().'assets/images/blank_user.png" alt="user image">
										<span class="username">
										  <a href="#">'.$rforward->nama.'</a>
										  <a href="#" class="float-right btn-tool"></a>
										</span>
										<span class="description">';
										$ses=$this->session->userdata('id_users');
										if(!$ses) { echo'...'; }
										else{echo ''.$rforward->alamat.'';} 
										echo ' - '.$this->Crud_model->dateBahasaIndo1($rforward->created_time).'
										</span>
									  </div>
									  </div>

									  <div class="timeline-body">';
										if(!$ses) { echo''; }
										else{
											echo '<p><u>Pekerjaan:</u> '.$rforward->pekerjaan.'</p>';
											echo '<p><u>Nomor Telp.:</u> '.$rforward->nomor_telp.'</p>';
											echo '<p><u>Email:</u> '.$rforward->email.'</p>';
											echo '<p><u>Judul Posting:</u> '.$rforward->judul_posting_permohonan.'</p>';
											echo '<p><u>Tujuan:</u> <span class="text-'.$costum.'">'.$rforward->tujuan_penggunaan_informasi.'</span></p>';
											echo '<p><u>Rincian:</u></p>';
											} 
										echo '
										<p>'.$rforward->rincian_informasi_yang_diinginkan.'</p>
									  </div>
									  <div class="timeline-footer card-comment">
										  <p>
											<a href="#" class="link-black text-sm"><i class="fa fa-thumbs-up mr-1 text-'.$costum.'"></i></a>
											<a href="#" class="link-black text-sm mr-2"><i class="fa fa-thumbs-down mr-1 text-'.$costum.'"></i></a>
											<a href="https://'.$rforward->domain.'/pendaftaran_pasar" class="text-muted link-black text-sm mr-2"><i class="fa fa-tags mr-1 text-'.$costum.'"></i>'.$rforward->domain.'</a>';
											if($rforward->forward==''){
												echo'';
											}else{
												echo'
												<a href="https://'.$rforward->forward.'/pendaftaran_pasar/detail/'.$rforward->id_pendaftaran_pasar.'" class="text-muted link-black text-sm mr-2"><i class="fa fa-mail-reply mr-1 text-warning"></i> '.$rforward->forward.'</a>
												';
											}
											echo'
											<a href="https://'.$rforward->domain.'/pendaftaran_pasar/detail/'.$rforward->id_pendaftaran_pasar.'" class="text-muted link-black text-sm mr-2">';
													$w11a = $this->db->query("
													SELECT *
													FROM pendaftaran_pasar
													WHERE parent = ".$rforward->id_pendaftaran_pasar."
													and status=1
													");
													$query_like_admina = $this->db->query("
													SELECT *
													FROM pendaftaran_pasar
													WHERE parent = ".$rforward->id_pendaftaran_pasar."
													and status=1
													and nama
													LIKE 'Admin%'
													");
													$jumlah_commentsa = $w11a->num_rows(); 
													$jumlah_likea = $query_like_admina->num_rows();
													if(($query_like_admina->num_rows())>0){
														echo '<i class="fa fa-eye mr-1 text-success"></i>';
													}
													else{
														echo '<i class="fa fa-eye mr-1 text-warning"></i>';
													}
												echo ' '.$jumlah_commentsa.' Baca Selengkapnya
											</a>
											</p>
									  </div>
									  
									</div>
								  </li>
                      <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                      </li>
			';
		}
	}
	
  function load_table_pelayanan_informasi(){
    $web=$this->uut->namadomain(base_url());
    $tahun=$this->input->post('tahun');
		if($web=='ppid.wonosobokab.go.id'){
			$web_aduan='';
		}
		else{
			$web_aduan="and domain ='".$web."'";
		}
		$w = $this->db->query("
		SELECT *, 
		(select user_name from users where users.id_users=pendaftaran_pasar.created_by) as nama_pengguna, 
		(select judul_posting from posting where posting.id_posting=pendaftaran_pasar.id_posting) as judul_posting_permohonan
		from pendaftaran_pasar
		where status = 1
		and parent = 0
		".$web_aduan."
		and tujuan_penggunaan_informasi = 'Pelayanan Informasi'
		and created_time
		like '".$tahun."%'
		order by created_time desc
		");
		
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h){
			$nomor++;
			$tujuan = $h->tujuan_penggunaan_informasi;
			if($tujuan=='Komentar'){
			$costum = 'warning';
			$fa = 'comments';
			}
			elseif($tujuan=='Pengaduan Masyarakat'){
			$costum = 'success';
			$fa = 'user';
			}
			else{
			$costum = 'primary';
			$fa = 'envelope';
			}
			echo '
				<tr id_pendaftaran_pasar="'.$h->id_pendaftaran_pasar.'" id="'.$h->id_pendaftaran_pasar.'">
					<td style="padding: 2px;">
						'.$nomor.'
					</td>
					<td style="padding: 2px;">
						<p>'.$this->Crud_model->dateBahasaIndo1($h->created_time).'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->instansi.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->rincian_informasi_yang_diinginkan.'</p>
					</td>
					<td style="padding: 2px;">
						<p>';if($h->balas==2){echo'<span class="bg-success">Selesai</span>';}else{echo'<span class="bg-danger">Belum</span>';}echo'</p>
					</td>
				</tr>
			';
		}
	}
	
  public function total_pelayanan_informasi()
   {
    $web=$this->uut->namadomain(base_url());
    $tahun = trim($this->input->post('tahun'));
    $table_name = 'pendaftaran_pasar';
		$b = $this->total_hit_pelayanan_informasi($tahun);
      echo $b;
   }
  function total_hit_pelayanan_informasi($tahun)
		{
      $web=$this->uut->namadomain(base_url());
      $tahun = trim($tahun);
      $this->db->from('pendaftaran_pasar');
      $where    = array(
        'status' => 1,
        'domain' => $web,
        'tujuan_penggunaan_informasi' => 'Pelayanan Informasi',
        'created_time'
				);
      $this->db->where($where);
			$this->db->like('created_time', $tahun); 
      $a = $this->db->count_all_results(); 
      return $a;
		}
		
  function load_table_pendaftaran_pasar_balasan() {
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
    $id_pendaftaran_pasar=$this->input->post('id_pendaftaran_pasar');
		$w = $this->db->query("
		SELECT *, 
		(select user_name from users where users.id_users=pendaftaran_pasar.created_by) as nama_pengguna, 
		(select judul_posting from posting where posting.id_posting=pendaftaran_pasar.id_posting) as judul_posting_permohonan
		from pendaftaran_pasar
		where status != 99
		and parent='".$id_pendaftaran_pasar."'
		order by created_time desc
		");
		
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h){
				echo '
						<tr class="" id_pendaftaran_pasar="'.$h->id_pendaftaran_pasar.'" id="'.$h->id_pendaftaran_pasar.'">
							<td>
									<div class="card-comment">
										<img class="img-circle img-sm" src="'.base_url().'assets/images/blank_user.png" alt="User Image">
										<div class="comment-text">
											<span class="username">';
												if(!$ses) {
													echo'
													'.$h->nama.'
													<span class="text-muted float-right">'.$this->Crud_model->dateBahasaIndo1($h->created_time).'
													</span>
													'; 
												}
												else{
													echo '
													Nama: '.$h->nama.'
													<span class="text-muted float-right">'.$this->Crud_model->dateBahasaIndo1($h->created_time).'
													<a href="#" id="del_ajax"><i class="fa fa-remove"></i></a>
													</span>
													';
													} 
												echo '</span>';
											if(!$ses){echo ''.$h->rincian_informasi_yang_diinginkan.'<br /> ';}
											else{
												echo '
												Alamat: '.$h->alamat.'<br />
												No.Telp: '.$h->nomor_telp.'<br />
												Email: '.$h->email.'<br />
												Pekerjaan: '.$h->pekerjaan.'<br /> 
												Rindian: '.$h->rincian_informasi_yang_diinginkan.'<br /> 
												';}
                        $where11 = array(
                          'id_tabel' => $h->id_pendaftaran_pasar,
                          'table_name' => 'pendaftaran_pasar'
                          );
                        $this->db->where($where11);
                        $this->db->order_by('uploaded_time desc');
                        $query11 = $this->db->get('attachment');
                        foreach ($query11->result() as $row11)
                        {
                          echo '
                          <a class="btn btn-app" href="'.base_url().'media/upload/'.$row11->file_name.'">
                            <i class="fa fa-paperclip"></i> '.$row11->keterangan.'
                          </a>
                          ';
                        }
											echo '
										</div>
									</div>
						</td>
					</tr>
				';
		}
	}
	
  public function detail() {
    $web=$this->uut->namadomain(base_url());
	$tanggal = date('Y-m-d H:i:s');
    $data['hari_ini'] = $tanggal;
    $data['tanggal_hari_ini'] = ''.$this->Crud_model->dateBahasaIndo($tanggal).' ';
    $data['judul_halaman'] = 'PENDAFTARAN PEDAGANG PASAR INDUK';
    $data['nama_halaman'] = 'pendaftaran_pasar';
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $where = array(
			'id_pendaftaran_pasar' => $this->uri->segment(3),
            'status!=' => 99
	);
    $d = $this->Pendaftaran_pasar_model->get_data($where);
    if(!$d){
		$data['id_pendaftaran_pasar'] = '';
		$data['tanggal_permohonan'] = '';
		$data['nama'] = '';
		$data['alamat'] = '';
		$data['pekerjaan'] = '';
		$data['nomor_telp'] = '';
		$data['email'] = '';
		$data['nik'] = '';
		$data['no_kk'] = '';
		$data['created_time'] = '';
	  }
	else{
		$ses=$this->session->userdata('id_users');
		if(!$ses) {
		$data['alamat'] = '';
		$data['tanggal_permohonan'] = '';
		$data['nomor_telp'] = '';
		$data['email'] = '';
		$data['nama'] = $d['nama'];
		$data['nik'] = '';
		$data['no_kk'] = '';
		$data['created_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['created_time']).'';
		}
		else{
		$data['id_pendaftaran_pasar'] = $d['id_pendaftaran_pasar'];
		$data['tanggal_permohonan'] = 'Tanggal permohonan: '.$d['tanggal_permohonan'].' ';
		$data['nama'] = 'Nama Lengkap: '.$d['nama'].' ';
		$data['alamat'] = 'Alamat: '.$d['alamat'].' <br />';
		$data['nomor_telp'] = 'Nomor Telp.: '.$d['nomor_telp'].' <br />';
		$data['email'] = 'Email: '.$d['email'].' <br />';
		$data['nik'] = 'NIK: '.$d['nik'].' <br />';
		$data['no_kk'] = 'No KK: '.$d['no_kk'].' <br />';
		$data['created_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['created_time']).'';
		}
	  }
		$w = $this->db->query("
		SELECT *
		from pendaftaran_pasar
		where status = 1
		and domain='".$web."'
		and parent=".$this->uri->segment(3)."
		");
		$jumlah_komentar = $w->num_rows();
    $data['jumlah_komentar'] = $jumlah_komentar;
    $data['main_view'] = 'pengaduan_masyarakat/detail_pendaftaran_pasar';
    $this->load->view('pengaduan_masyarakat', $data);
   }
	
  public function hapus(){
		$web=$this->uut->namadomain(base_url());
		$id_pendaftaran_pasar = $this->input->post('id_pendaftaran_pasar');
		$where = array(
			'id_pendaftaran_pasar' => $id_pendaftaran_pasar,
			'domain' => ''.$web.''
			);
		$this->db->from('pendaftaran_pasar');
		$this->db->where($where);
		$a = $this->db->count_all_results();
		if($a == 0){
			echo 0;
			}
		else{
			$data_update = array(
			
				'status' => 99
								
				);
			$table_name  = 'pendaftaran_pasar';
			$this->Pendaftaran_pasar_model->update_data_pendaftaran_pasar($data_update, $where, $table_name);
			echo 1;
			}
	}
	public function forward(){
    $web=$this->uut->namadomain(base_url());
		$tanggal = date('Y-m-d H:i:s');
    $data['hari_ini'] = $tanggal;
    $data['tanggal_hari_ini'] = ''.$this->Crud_model->dateBahasaIndo($tanggal).' ';
    $data['judul_halaman'] = 'PENDAFTARAN PEDAGANG PASAR INDUK';
    $data['nama_halaman'] = 'pendaftaran_pasar';
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $where = array(
			'id_pendaftaran_pasar' => $this->uri->segment(3),
            'status' => 1
		);
    $d = $this->Pendaftaran_pasar_model->get_data($where);
    if(!$d){
		$data['id_pendaftaran_pasar'] = '';
		$data['tanggal_permohonan'] = '';
		$data['nama'] = '';
		$data['alamat'] = '';
		$data['pekerjaan'] = '';
		$data['nomor_telp'] = '';
		$data['email'] = '';
		$data['nik'] = '';
		$data['no_kk'] = '';
		$data['created_time'] = '';
	  }
	else{
		$ses=$this->session->userdata('id_users');
		if(!$ses) {
			$data['alamat'] = '';
			$data['tanggal_permohonan'] = '';
			$data['nomor_telp'] = '';
			$data['email'] = '';
			$data['nama'] = $d['nama'];
			$data['nik'] = '';
			$data['no_kk'] = '';
			$data['created_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['created_time']).'';
		}else{
			$data['id_pendaftaran_pasar'] = $d['id_pendaftaran_pasar'];
			$data['tanggal_permohonan'] = 'Tanggal permohonan: '.$d['tanggal_permohonan'].' ';
			$data['nama'] = 'Nama Lengkap: '.$d['nama'].' ';
			$data['alamat'] = 'Alamat: '.$d['alamat'].' <br />';
			$data['nomor_telp'] = 'Nomor Telp.: '.$d['nomor_telp'].' <br />';
			$data['email'] = 'Email: '.$d['email'].' <br />';
			$data['nik'] = 'NIK: '.$d['nik'].' <br />';
			$data['no_kk'] = 'No KK: '.$d['no_kk'].' <br />';
			$data['created_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['created_time']).'';
		}
	  }
    $data['main_view'] = 'pengaduan_masyarakat/forward';
    $this->load->view('pengaduan_masyarakat', $data);
	}
  function option_skpd(){
    $web=$this->uut->namadomain(base_url());
		if($web=='ppid.wonosobokab.go.id'){
			$where_id_skpd="";
		}
		else{
			$where_id_skpd="and data_skpd.id_skpd=".$this->session->userdata('id_skpd')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_skpd, skpd
		where data_skpd.status != 99
		and skpd.id_skpd=data_skpd.id_skpd
    ".$where_id_skpd."
		order by skpd.id_skpd 
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->skpd_website.'">'.$h->nama_skpd.'</option>';
		}
	}
  public function simpan_forward(){
		$web=$this->uut->namadomain(base_url());
		$id_skpd = $this->input->post('id_skpd');
		$id_pendaftaran_pasar = $this->input->post('id_pendaftaran_pasar');
		$where = array(
			'id_pendaftaran_pasar' => $id_pendaftaran_pasar,
			'domain' => ''.$web.''
			);
		$this->db->from('pendaftaran_pasar');
		$this->db->where($where);
		$a = $this->db->count_all_results();
		if($a == 0){
			echo 0;
			}
		else{
			$data_update = array(
			
				'status' => 3,
				'balas' => 0,
				'forward' => ''.$id_skpd.'',
				'updated_by' => $this->session->userdata('id_users'),
				'updated_time' => date('Y-m-d H:i:s')
								
				);
			$table_name  = 'pendaftaran_pasar';
			$this->Pendaftaran_pasar_model->update_data_pendaftaran_pasar($data_update, $where, $table_name);
			echo 1;
			}
	}
	
  function load_table1(){
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
		if($web=='ppid.wonosobokab.go.id'){
			$web_aduan='';
		}
		else{
			$web_aduan="and domain ='".$web."'";
		}
		$w = $this->db->query("
		SELECT *, 
		(select user_name from users where users.id_users=pendaftaran_pasar.created_by) as nama_pengguna, 
		(select judul_posting from posting where posting.id_posting=pendaftaran_pasar.id_posting) as judul_posting_permohonan
		from pendaftaran_pasar
		where status != 99
		and parent = 0
		".$web_aduan."
		order by created_time desc
		");
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h){
			$tujuan = $h->tujuan_penggunaan_informasi;
			if($tujuan=='Komentar'){
			$costum = 'warning';
			$fa = 'comments';
			}
			elseif($tujuan=='Permohonan Informasi Publik'){
			$costum = 'success';
			$fa = 'user';
			}
			else{
			$costum = 'primary';
			$fa = 'envelope';
			}
			echo '
				<tr id_pendaftaran_pasar="'.$h->id_pendaftaran_pasar.'" id="'.$h->id_pendaftaran_pasar.'">
					<td>
									<div class="card">
									  <div class="card-header">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm text-'.$costum.'" src="'.base_url().'assets/images/blank_user.png" alt="user image">
										<span class="username">';
											if(!$ses) { echo'
										  <a href="#">'.$h->nama.'</a>
											'; }
											else{echo '
										  <a href="#">'.$h->nama.'</a>
										  <a href="#" class="float-right btn-tool" id="del_ajax"><i class="fa fa-times"></i></a>
											';} 
											echo '
										</span>
										<span class="description">';
										if(!$ses) { echo''; }
										else{echo ''.$h->alamat.'';} 
										echo ' '.$this->Crud_model->dateBahasaIndo1($h->created_time).'
										</span>
									  </div>
									  </div>

									  <div class="card-body">';
										if(!$ses) { echo''; }
										else{
											echo '<p><u>Pekerjaan:</u> '.$h->pekerjaan.'</p>';
											echo '<p><u>Nomor Telp.:</u> '.$h->nomor_telp.'</p>';
											echo '<p><u>Email:</u> '.$h->email.'</p>';
											echo '<p><u>Judul Posting:</u> '.$h->judul_posting_permohonan.'</p>';
											echo '<p><u>Tujuan:</u> <span class="text-'.$costum.'">'.$h->tujuan_penggunaan_informasi.'</span></p>';
											// echo '<p><u>Website:</u> </p>';
											echo '<p><u>Rincian:</u></p>';
											} 
										echo '
										<p>'.$h->rincian_informasi_yang_diinginkan.'</p>
										<a target="_blank" href="https://'.$h->domain.'" class="btn btn-default btn-sm"><i class="fa fa-link mr-1 text-info"></i>'.$h->domain.'</a>';
										if($web=='ppid.wonosobokab.go.id'){
											if(!$ses) {
												if($h->forward==''){echo'';}else{echo'<button class="btn btn-default btn-sm"><i class="fa fa-share-alt"></i> '.$h->forward.'</button>';}
												echo'
														
														';
											}else{
												echo'
														<a class="btn btn-default btn-sm" href="'.base_url().'pendaftaran_pasar/forward/'.$h->id_pendaftaran_pasar.'"><i class="fa fa-share-alt"></i> '; if($h->forward==''){echo'Forward';}else{echo''.$h->forward.'';} echo '</a>
														';
											}
										}
										echo'
										<a href="'.base_url().'pendaftaran_pasar/detail/'.$h->id_pendaftaran_pasar.'" class="btn btn-default btn-sm"><i class="fa fa-thumbs-o-up"></i> Baca Selengkapnya</a>
										<span class="float-right text-muted">
												<a href="'.base_url().'pendaftaran_pasar/detail/'.$h->id_pendaftaran_pasar.'" class="text-muted link-black text-sm mr-2">';
														$w11 = $this->db->query("
														SELECT *
														FROM pendaftaran_pasar
														WHERE parent = ".$h->id_pendaftaran_pasar."
														and status=1
														");
														$query_like_admin = $this->db->query("
														SELECT *
														FROM pendaftaran_pasar
														WHERE parent = ".$h->id_pendaftaran_pasar."
														and nama
														LIKE 'Admin%'
														");
														$jumlah_comments = $w11->num_rows(); 
														$jumlah_like = $query_like_admin->num_rows();
														if(($query_like_admin->num_rows())>0){
															echo '<i class="fa fa-comments mr-1 text-success"></i>';
														}
														else{
															echo '<i class="fa fa-comment mr-1 text-warning"></i>';
														}
													echo ' '.$jumlah_comments.' Komentar';
													echo ' 
												</a>
										</span>
									  </div>
									  <div class="card-footer card-comment">';
										foreach($w11->result() as $row_komen){
											echo'
										  <div class="card-comment">
												<img class="img-circle img-sm" src="'.base_url().'assets/images/blank_user.png" alt="User Image">
												<div class="comment-text">
													<span class="username">
														'.$row_komen->nama.'
														<span class="text-muted float-right">'.$this->Crud_model->dateBahasaIndo1($row_komen->created_time).'</span>
													</span>
													<p>'.$row_komen->rincian_informasi_yang_diinginkan.'</p>
												</div>
											</div>
											';
											if(($w11->num_rows())>1){echo '<hr />';}else{echo '';}
										}
										echo'
									  </div>
									</div>
					</td>
				</tr>
			';
		}
	}
  function load_table_forward(){
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
		if($web=='ppid.wonosobokab.go.id'){
			$web_aduan='';
		}
		else{
			$web_aduan="and forward ='".$web."'";
		}
		$w = $this->db->query("
		SELECT *, 
		(select user_name from users where users.id_users=pendaftaran_pasar.created_by) as nama_pengguna, 
		(select judul_posting from posting where posting.id_posting=pendaftaran_pasar.id_posting) as judul_posting_permohonan
		from pendaftaran_pasar
		where status != 99
		and parent = 0
		".$web_aduan."
		order by created_time desc
		");
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h){
			$tujuan = $h->tujuan_penggunaan_informasi;
			if($tujuan=='Komentar'){
			$costum = 'warning';
			$fa = 'comments';
			}
			elseif($tujuan=='Pengaduan Masyarakat'){
			$costum = 'success';
			$fa = 'user';
			}
			else{
			$costum = 'primary';
			$fa = 'envelope';
			}
			echo '
				<tr id_pendaftaran_pasar_forward="'.$h->id_pendaftaran_pasar.'" id="'.$h->id_pendaftaran_pasar.'">
					<td>
                <div class="row">
									<div class="card">
									  <div class="card-header">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm text-'.$costum.'" src="'.base_url().'assets/images/blank_user.png" alt="user image">
										<span class="username">';
											if(!$ses) { echo'
										  <a href="#">'.$h->nama.'</a>
											'; }
											else{echo '
										  <a href="#">'.$h->nama.'</a>
										  <a href="#" class="float-right btn-tool" id="del_ajax1"><i class="fa fa-times"></i></a>
											';} 
											echo '
										</span>
										<span class="description">';
										if(!$ses) { echo''; }
										else{echo ''.$h->alamat.'';} 
										echo ' '.$this->Crud_model->dateBahasaIndo1($h->created_time).'
										</span>
									  </div>
									  </div>

									  <div class="card-body">';
										if(!$ses) { echo''; }
										else{
											echo '<p><u>Pekerjaan:</u> '.$h->pekerjaan.'</p>';
											echo '<p><u>Nomor Telp.:</u> '.$h->nomor_telp.'</p>';
											echo '<p><u>Email:</u> '.$h->email.'</p>';
											echo '<p><u>Judul Posting:</u> '.$h->judul_posting_permohonan.'</p>';
											echo '<p><u>Tujuan:</u> <span class="text-'.$costum.'">'.$h->tujuan_penggunaan_informasi.'</span></p>';
											// echo '<p><u>Website:</u> </p>';
											echo '<p><u>Rincian:</u></p>';
											} 
										echo '
										<p>'.$h->rincian_informasi_yang_diinginkan.'</p>
										<a target="_blank" href="https://'.$h->domain.'" class="btn btn-default btn-sm"><i class="fa fa-share-alt mr-1 text-info"></i>'.$h->domain.'</a>';
										if($web=='ppid.wonosobokab.go.id'){
											if(!$ses) {
												if($h->forward==''){}else{echo'<button class="btn btn-default btn-sm"><i class="fa fa-share-alt"></i> '.$h->forward.'</button>';}
											}else{
												echo'
														<a class="btn btn-default btn-sm" href="'.base_url().'pendaftaran_pasar/forward/'.$h->id_pendaftaran_pasar.'"><i class="fa fa-share-alt"></i> '; if($h->forward==''){echo'Forward';}else{echo''.$h->forward.'';} echo '</a>
														';
											}
										}
										else{
											if(!$ses) {
											}else{
												if($h->forward==''){}else{echo'<button class="btn btn-default btn-sm"><i class="fa fa-link"></i> '.$h->forward.'</button>';}
											}
										}
										echo'
										<a href="'.base_url().'pendaftaran_pasar/detail/'.$h->id_pendaftaran_pasar.'" class="btn btn-default btn-sm"><i class="fa fa-thumbs-o-up"></i> Baca Selengkapnya</a>
										<span class="float-right text-muted">
												<a href="'.base_url().'pendaftaran_pasar/detail/'.$h->id_pendaftaran_pasar.'" class="text-muted link-black text-sm mr-2">';
														$w11 = $this->db->query("
														SELECT *
														FROM pendaftaran_pasar
														WHERE parent = ".$h->id_pendaftaran_pasar."
														and status=1
														");
														$query_like_admin = $this->db->query("
														SELECT *
														FROM pendaftaran_pasar
														WHERE parent = ".$h->id_pendaftaran_pasar."
														and nama
														LIKE 'Admin%'
														");
														$jumlah_comments = $w11->num_rows(); 
														$jumlah_like = $query_like_admin->num_rows();
														if(($query_like_admin->num_rows())>0){
															echo '<i class="fa fa-comments mr-1 text-success"></i>';
														}
														else{
															echo '<i class="fa fa-comment mr-1 text-warning"></i>';
														}
													echo ' '.$jumlah_comments.' Komentar';
													echo ' 
												</a>
										</span>
									  </div>
									  <div class="card-footer card-comment">';
										foreach($w11->result() as $row_komen){
											echo'
										  <div class="card-comment">
												<img class="img-circle img-sm" src="'.base_url().'assets/images/blank_user.png" alt="User Image">
												<div class="comment-text">
													<span class="username">
														'.$row_komen->nama.'
														<span class="text-muted float-right">'.$this->Crud_model->dateBahasaIndo1($row_komen->created_time).'</span>
													</span>
													<p>'.$row_komen->rincian_informasi_yang_diinginkan.'</p>
												</div>
											</div>
											';
											if(($w11->num_rows())>1){echo '<hr />';}else{echo '';}
										}
										echo'
									  </div>
									</div>
					</td>
				</tr>
			';
		}
	}
  public function hapus_forward()
		{
      $web=$this->uut->namadomain(base_url());
			$id_pendaftaran_pasar = $this->input->post('id_pendaftaran_pasar_forward');
      $where = array(
        'id_pendaftaran_pasar' => $id_pendaftaran_pasar,
        'forward' => ''.$web.''
        );
      $this->db->from('pendaftaran_pasar');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'pendaftaran_pasar';
        $this->Pendaftaran_pasar_model->update_data_pendaftaran_pasar($data_update, $where, $table_name);
        echo 1;
        }
		}
  
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $tahun    = $this->input->post('tahun');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
		if($web=='ppid.wonosobokab.go.id'){
      $where0 = array(
        'pendaftaran_pasar.status !=' => 99,
        'pendaftaran_pasar.parent=' => 0,
        'pendaftaran_pasar.created_time'
      );

		}
		else{
      $where0 = array(
        'pendaftaran_pasar.status !=' => 99,
        'pendaftaran_pasar.domain=' => $web,
        'pendaftaran_pasar.parent=' => 0,
        'pendaftaran_pasar.created_time'
      );
		}
    $this->db->like('pendaftaran_pasar.created_time', $tahun);
    $this->db->where($where0);
    $query0 = $this->db->get('pendaftaran_pasar');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_pendaftaran_pasar(){
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
		$table = 'pendaftaran_pasar';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $tahun    = $this->input->post('tahun');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
		if($web=='ppid.wonosobokab.go.id'){
      $where      = array(
        'pendaftaran_pasar.status !=' => 99,
        'pendaftaran_pasar.parent=' => 0,
        'pendaftaran_pasar.created_time'
      );

		}
		else{
      $where      = array(
        'pendaftaran_pasar.status !=' => 99,
        'pendaftaran_pasar.domain=' => $web,
        'pendaftaran_pasar.parent=' => 0,
        'pendaftaran_pasar.created_time'
      );
		}
    $orderby   = ''.$order_by.'';
    $query = $this->Pendaftaran_pasar_model->html_all_pendaftaran_pasar($table, $where, $limit, $start, $fields, $orderby, $keyword, $tahun);
    $urut=$start;
    foreach ($query->result() as $h)
      {
      $urut=$urut+1;
			$costum = 'primary';
			$fa = 'envelope';
			echo '
									<div class="attachment-block clearfix" id_pendaftaran_pasar="'.$h->id_pendaftaran_pasar.'" id="'.$h->id_pendaftaran_pasar.'">
									  <div class="card-header">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm text-'.$costum.'" src="'.base_url().'assets/images/blank_user.png" alt="user image">
										<span class="username">';
											if(!$ses) { echo'
										  <a href="#">'.$h->nama.'</a>
											'; }
											else{echo '
										  <a href="#">'.$h->nama.'</a>
                      <div id_pendaftaran_pasar="'.$h->id_pendaftaran_pasar.'" id="'.$h->id_pendaftaran_pasar.'">
										  <a href="#" class="float-right btn-tool" id="del_ajax"><i class="fa fa-times"></i></a>
                      </div>
											';} 
											echo '
										</span>
										<span class="description">';
										if(!$ses) { echo''; }
										else{echo ''.$h->alamat.'';} 
										echo ' '.$this->Crud_model->dateBahasaIndo1($h->created_time).'
										</span>
									  </div>
									  </div>

									  <div class="card-body">';
										if(!$ses) { echo''; }
										else{
											echo '<p><u>Nomor Telp.:</u> '.$h->nomor_telp.'</p>';
											echo '<p><u>Email:</u> '.$h->email.'</p>';
											echo '<p><u>NIK:</u> '.$h->nik.'</p>';
											echo '<p><u>NO KK:</u> '.$h->no_kk.'</p>';
											//echo '<p><u>Judul Posting:</u> '.$h->judul_posting_permohonan.'</p>';
											// echo '<p><u>Website:</u> </p>';
											echo '<p><u>Rincian:</u></p>';
											} 
										echo '
										<a target="_blank" href="https://'.$h->domain.'" class="btn btn-default btn-sm"><i class="fa fa-link mr-1 text-info"></i>'.$h->domain.'</a>';
										if($web=='ppid.wonosobokab.go.id'){
											if(!$ses) {
												if($h->forward==''){echo'';}else{echo'<button class="btn btn-default btn-sm"><i class="fa fa-share-alt"></i> '.$h->forward.'</button>';}
												echo'
														
														';
											}else{
												echo'
														<a class="btn btn-default btn-sm" href="'.base_url().'pendaftaran_pasar/forward/'.$h->id_pendaftaran_pasar.'"><i class="fa fa-share-alt"></i> '; if($h->forward==''){echo'Forward';}else{echo''.$h->forward.'';} echo '</a>
														';
											}
										}
										echo'
										<a href="'.base_url().'pendaftaran_pasar/detail/'.$h->id_pendaftaran_pasar.'" class="btn btn-default btn-sm"><i class="fa fa-thumbs-o-up"></i> Baca Selengkapnya</a>
										<span class="float-right text-muted">
												<a href="'.base_url().'pendaftaran_pasar/detail/'.$h->id_pendaftaran_pasar.'" class="text-muted link-black text-sm mr-2">';
														$w11 = $this->db->query("
														SELECT *
														FROM pendaftaran_pasar
														WHERE parent = ".$h->id_pendaftaran_pasar."
														and status=1
														");
														$query_like_admin = $this->db->query("
														SELECT *
														FROM pendaftaran_pasar
														WHERE parent = ".$h->id_pendaftaran_pasar."
														and nama
														LIKE 'Admin%'
														");
														$jumlah_comments = $w11->num_rows(); 
														$jumlah_like = $query_like_admin->num_rows();
														if(($query_like_admin->num_rows())>0){
															echo '<i class="fa fa-comments mr-1 text-success"></i>';
														}
														else{
															echo '<i class="fa fa-comment mr-1 text-warning"></i>';
														}
													echo ' '.$jumlah_comments.' Komentar';
													echo ' 
												</a>
										</span>
									  </div>
									  <div class="card-footer card-comment">';
										foreach($w11->result() as $row_komen){
											echo'
										  <div class="card-comment">
												<img class="img-circle img-sm" src="'.base_url().'assets/images/blank_user.png" alt="User Image">
												<div class="comment-text">
													<span class="username">
														'.$row_komen->nama.'
														<span class="text-muted float-right">'.$this->Crud_model->dateBahasaIndo1($row_komen->created_time).'</span>
													</span>
													<p>'.$row_komen->rincian_informasi_yang_diinginkan.'</p>
												</div>
											</div>
											';
											if(($w11->num_rows())>1){echo '<hr />';}else{echo '';}
										}
										echo'
									  </div>
									</div>
                </div>
			';
      }
          /*echo '
          <tr>
          <td valign="top" colspan="4" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="',base_url(),'dusun/xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>
          </tr>
          ';*/
          
   } 
 }