<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Tema_website extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Tema_website_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'tema_website/home';
    $this->load->view('tes', $data);
   }
   
  public function json_all_tema_website()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_tema_website    = $this->input->post('urut_data_tema_website');
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  tema_website.id_tema_website,
                  tema_website.domain,
                  tema_website.nama_tema_website,
                  tema_website.keterangan,
                  tema_website.created_by,
                  tema_website.created_time,
                  tema_website.updated_by,
                  tema_website.updated_time,
                  tema_website.deleted_by,
                  tema_website.deleted_time,
                  tema_website.status
                  ";
    $where      = array(
      'domain' => $web
    );
    $order_by   = 'tema_website.'.$urut_data_tema_website.'';
    echo json_encode($this->Tema_website_model->json_all_tema_website($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
   
  public function inaktifkan()
		{
      $where = array(
        'id_tema_website' => $this->input->post('id_tema_website')
        );
      $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'tema_website';
        $this->Tema_website_model->update_data_tema_website($data_update, $where, $table_name);
		}
   
  public function aktifkan()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
        'id_tema_website' => $this->input->post('id_tema_website')
        );
      $data_update = array(
      
        'status' => 1
                
        );
      $table_name  = 'tema_website';
      $this->Tema_website_model->update_data_tema_website($data_update, $where, $table_name);
      
      $where = array(
        'id_tema_website !=' => $this->input->post('id_tema_website'),
        'domain' => $web
        );
      $data_update = array(
      
        'status' => 0
                
        );
      $table_name  = 'tema_website';
      $this->Tema_website_model->update_data_tema_website($data_update, $where, $table_name);
      
		}
   
  public function cek_tema()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('tema');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'nama_tema_website' => $row->nama_tema,
          'domain' => $web
          );
        $this->db->from('tema_website');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'domain' => $web,
            'nama_tema_website' => $row->nama_tema,
            'keterangan' => $row->judul,
            'created_by' => 1,
            'created_time' => date('Y-m-d H:i:s'),
            'updated_by' => 0,
            'updated_time' => date('Y-m-d H:i:s'),
            'deleted_by' => 0,
            'deleted_time' => date('Y-m-d H:i:s'),
            'status' => 0
                    
            );
          $table_name = 'tema_website';
          $this->Tema_website_model->simpan_tema_website($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
 }