<?php
class Posting2 extends CI_Controller
{

    public function plekentung($id)
    {
        $isa = [];
        $this->db->select('file_name');
        $this->db->where('id_tabel', $id);
        $query = $this->db->get('attachment');
        foreach ($query->result() as $ratna) {
            array_push($isa, $ratna->file_name);
        }
        return json_encode($isa);
    }
    public function index()
    {
        $this->copyFile();
        $this->copyData();
    }

    public function copyFile()
    {
        // copy data file ke folder migrasi
        $data = $this->db->where('uploaded_by', '699');
        $data = $this->db->get('attachment')->result();
        foreach ($data as $dt) {
            copy('media/upload/' . $dt->file_name, 'media/migrasi/' . $dt->file_name);
        }
        echo 'Copy file selesai...';
    }

    public function copyData()
    {
        // insert data posting ke tabel migrasi
        $data = $this->db->like('domain', 'capil');
        $data = $this->db->get('posting')->result();
        foreach ($data as $dt) {
            $data = [
                'judul_posting' => $dt->judul_posting,
                'isi_posting' => $dt->isi_posting,
                'file_name' => $this->plekentung($dt->id_posting),
            ];
            $this->db->insert('migrasi', $data);
        }
        echo 'Copy data selesai...';
    }
}
