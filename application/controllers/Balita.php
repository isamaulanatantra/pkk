<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Balita extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Balita_model');
   }
   
  public function index(){
    $data['main_view'] = 'balita/home';
    $this->load->view('tes', $data);
	}
  public function cetak_balita(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'balita/cetak_balita';
    $this->load->view('print', $data);
   }
	public function load_balita(){
      $mode_balita = $this->input->post('mode_balita');
      if( $mode_balita == 'edit' ){
      $where    = array(
				'id_data_keluarga' => $this->input->post('value')
				);
      }
      elseif ( $mode_balita == 'edit_balita' ){
      $where    = array(
				'temp' => $this->input->post('value')
				);
      }
      else{
      $where    = array(
				'temp' => $this->input->post('value')
				);
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_balita');
      $result = $this->db->get('balita');
      echo json_encode($result->result_array());
	}
	public function json_all_balita(){
    $web=$this->uut->namadomain(base_url());
		$table = 'balita';
    $id_data_keluarga    = $this->input->post('id_data_keluarga');
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where = array(
      'balita.status !=' => 99,
      'balita.id_data_keluarga' => $id_data_keluarga
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_balita="'.$row->id_balita.'" id="'.$row->id_balita.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$row->nik.'</td>';
				echo '<td valign="top">'.$row->nama_balita.'</td>';
				echo '<td valign="top">'.$row->jenis_kelamin.'</td>';
				echo '<td valign="top">'.$row->tempat_lahir.'</td>';
				echo '<td valign="top">'.$row->tanggal_lahir.'</td>';
				echo '<td valign="top">
				<a class="badge bg-warning btn-sm" href="'.base_url().'balita/cetak/?id_balita='.$row->id_balita.'" ><i class="fa fa-print"></i> Cetak</a>
				<a href="#tab_form_balita" data-toggle="tab" class="update_id_balita badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax_balita" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			echo '
      <tr>
        <td valign="top" colspan="10" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'balita/cetak_balita/?id_data_keluarga='.$id_data_keluarga.'&page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Anggota Balita</a>
        </td>
      </tr>
      ';
          
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $id_data_keluarga = trim($this->input->post('id_data_keluarga'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='demoopd.wonosobokab.go.id'){
      $where0 = array(
        'balita.status !=' => 99,
        'balita.id_data_keluarga' => $id_data_keluarga
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where0 = array(
        'balita.status !=' => 99,
        'balita.id_data_keluarga' => $id_data_keluarga
        );
    }else{
      $where0 = array(
        'balita.status !=' => 99,
        'balita.id_data_keluarga' => $id_data_keluarga
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('balita');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_balita(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nik', 'nik', 'required');
		$this->form_validation->set_rules('nama_balita', 'nama_balita', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'id_data_keluarga' => trim($this->input->post('id_data_keluarga')),
        'nik' => trim($this->input->post('nik')),
        'nama_balita' => trim($this->input->post('nama_balita')),
        'jenis_kelamin' => trim($this->input->post('jenis_kelamin')),
        'tempat_lahir' => trim($this->input->post('tempat_lahir')),
        'tanggal_lahir' => trim($this->input->post('tanggal_lahir')),
        'berkebutuhan_khusus' => trim($this->input->post('berkebutuhan_khusus')),
        'berkebutuhan_khusus_fisik' => trim($this->input->post('berkebutuhan_khusus_fisik')),
        'temp' => trim($this->input->post('temp')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1
				);
      $table_name = 'balita';
      $id         = $this->Crud_model->save_data($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'balita',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
	}
	public function get_by_id(){
    $web=$this->uut->namadomain(base_url());
    if($web=='demoopd.wonosobokab.go.id'){
      $where    = array(
        'balita.id_balita' => $this->input->post('id_balita')
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where    = array(
        'balita.id_balita' => $this->input->post('id_balita')
        );
    }else{
      $where    = array(
        'balita.id_balita' => $this->input->post('id_balita')
        );
    }
    $this->db->select("*");
    $this->db->where($where);
    $this->db->order_by('id_balita');
    $result = $this->db->get('balita');
    echo json_encode($result->result_array());
	}
  public function update_balita(){
		$this->form_validation->set_rules('id_balita', 'id_balita', 'required');
		$this->form_validation->set_rules('nik', 'nik', 'required');
		$this->form_validation->set_rules('nama_balita', 'nama_balita', 'required');
    if ($this->form_validation->run() == FALSE){
      echo 0;
		}
    else{
      $data_update = array(
			
				'id_data_keluarga' => trim($this->input->post('id_data_keluarga')),
        'nik' => trim($this->input->post('nik')),
        'nama_balita' => trim($this->input->post('nama_balita')),
        'jenis_kelamin' => trim($this->input->post('jenis_kelamin')),
        'tempat_lahir' => trim($this->input->post('tempat_lahir')),
        'tanggal_lahir' => trim($this->input->post('tanggal_lahir')),
        'berkebutuhan_khusus' => trim($this->input->post('berkebutuhan_khusus')),
        'berkebutuhan_khusus_fisik' => trim($this->input->post('berkebutuhan_khusus_fisik')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
			);
      $table_name  = 'balita';
      $where       = array(
        'balita.id_balita' => trim($this->input->post('id_balita'))
			);
      $this->Balita_model->update_balita($data_update, $where, $table_name);
      echo 1;
		}
  }
  public function hapus(){
    $web=$this->uut->namadomain(base_url());
    $id_balita = $this->input->post('id_balita');
    $where = array(
      'id_balita' => $id_balita,
      'created_by' => $this->session->userdata('id_users')
      );
    $this->db->from('balita');
    $this->db->where($where);
    $a = $this->db->count_all_results();
    if($a == 0){
      echo 0;
    }
    else{
      $data_update = array(
      
        'status' => 99
                
        );
      $table_name  = 'balita';
      $this->Balita_model->update_balita($data_update, $where, $table_name);
      echo 1;
    }
  }
}