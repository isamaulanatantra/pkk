<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pokja_satu extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Pokja_satu_model');
   }
   
  public function index(){
    $data['main_view'] = 'pokja_satu/home';
    $this->load->view('tes', $data);
   }
  public function rekap(){
    $data['main_view'] = 'pokja_satu/rekap';
    $this->load->view('tes', $data);
   }
  public function cetak_pokja_satu(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_satu/cetak_pokja_satu';
    $this->load->view('print', $data);
   }
  public function cetak_semua_pokja_satu(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_satu/cetak_semua_pokja_satu';
    $this->load->view('print', $data);
   }
  public function cetak_pokja_satu_pekarangan(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_satu/cetak_pokja_satu_pekarangan';
    $this->load->view('print', $data);
   }
  public function cetak(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_satu/cetak';
    $this->load->view('print', $data);
   }
	public function json_all_pokja_satu(){
    $web=$this->uut->namadomain(base_url());
		$table = 'pokja_satu';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
    ( select (dusun.nama_dusun) from dusun where dusun.id_dusun=pokja_satu.id_dusun limit 1) as nama_dusun,
    ( select (desa.nama_desa) from desa where desa.id_desa=pokja_satu.id_desa limit 1) as nama_desa,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=pokja_satu.id_kecamatan limit 1) as nama_kecamatan
    ";
    if($web=='demoopd.wonosobokab.go.id'){
      $where = array(
        'pokja_satu.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where = array(
        'pokja_satu.status !=' => 99
        );
    }else{
      $where = array(
        'pokja_satu.status !=' => 99,
        'pokja_satu.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        $query_desa = $this->db->query("SELECT desa.nama_desa FROM desa WHERE desa.id_desa = ".$row->id_desa."");
        echo '
        <tr id_pokja_satu="'.$row->id_pokja_satu.'" id="'.$row->id_pokja_satu.'" name="id_pokja_satu">
          <td>'.$urut.'</td>
          <td>'; 
            $id_desa = $row->id_desa; 
            if($id_desa=='9999'){
              echo 'TP PKK Kecamatan';
            } 
            else{
              foreach ($query_desa->result() as $row_desa){
                echo ''.$row_desa->nama_desa.'';
              }
            } 
            echo'</td>
          <td>'.$row->jumlah_kader_pkbn.'</td>
          <td>'.$row->jumlah_kader_pkdrt.'</td>
          <td>'.$row->jumlah_kader_polaasuh.'</td>
          <td>'.$row->pkbn_klpsimulasi.'</td>
          <td>'.$row->pkbn_anggota.'</td>
          <td>'.$row->pkdrt_klp.'</td>
          <td>'.$row->pkdrt_anggota.'</td>
          <td>'.$row->polaasuh_klp.'</td>
          <td>'.$row->polaasuh_anggota.'</td>
          <td>'.$row->lansia_klp.'</td>
          <td>'.$row->lansia_anggota.'</td>
          <td>'.$row->kerjabakti.'</td>
          <td>'.$row->rukunkematian.'</td>
          <td>'.$row->keagamaan.'</td>
          <td>'.$row->jimpitan.'</td>
          <td>'.$row->arisan.'</td>
          <td>'.$row->keterangan.'</td>
          <td>
            <a href="#tab_form_pokja_satu" data-toggle="tab" class="update_id_pokja_satu badge bg-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>
            <a href="#" id="del_ajax_pokja_satu" class="badge bg-danger btn-sm"><i class="fa fa-cut"></i></a>
            <a href="#" class="badge bg-gray btn-sm">'.$row->tahun.'</a>
          </td>
        </tr>
        ';
      }
			/*echo '
      <tr>
        <td valign="top" colspan="10" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'pokja_satu/cetak_pokja_satu/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Keluarga</a>
        </td>
      </tr>
      ';*/
          
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='demoopd.wonosobokab.go.id'){
      $where0 = array(
        'pokja_satu.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where0 = array(
        'pokja_satu.status !=' => 99
        );
    }else{
      $where0 = array(
        'pokja_satu.status !=' => 99,
        'pokja_satu.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('pokja_satu');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_rekap_pokja_satu(){
    $web=$this->uut->namadomain(base_url());
    $query = $this->db->query("SELECT SUM(jumlah_kader_pkbn)as jumlah_kader_pkbn, 
     SUM(jumlah_kader_pkdrt)as jumlah_kader_pkdrt,
     SUM(jumlah_kader_polaasuh)as jumlah_kader_polaasuh,
     SUM(jumlah_kader_polaasuh)as jumlah_kader_polaasuh,
     SUM(pkbn_klpsimulasi)as pkbn_klpsimulasi,
     SUM(pkbn_anggota)as pkbn_anggota,
     SUM(pkdrt_klp)as pkdrt_klp,
     SUM(pkdrt_klp)as pkdrt_klp,
     SUM(pkdrt_anggota)as pkdrt_anggota,
     SUM(polaasuh_klp)as polaasuh_klp,
     SUM(polaasuh_anggota)as polaasuh_anggota,
     SUM(lansia_klp)as lansia_klp,
     SUM(lansia_anggota)as lansia_anggota,
     SUM(kerjabakti)as kerjabakti,
     SUM(rukunkematian)as rukunkematian,
     SUM(keagamaan)as keagamaan,
     SUM(jimpitan)as jimpitan,
     SUM(arisan)as arisan,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=pokja_satu.id_kecamatan limit 1) as nama_kecamatan
    FROM pokja_satu WHERE pokja_satu.id_kabupaten=1 AND pokja_satu.id_propinsi=1 GROUP BY pokja_satu.id_kecamatan");
    $urut=0;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        echo '
        <tr>
          <td>'.$urut.'</td>
          <td>'.$row->nama_kecamatan.'</td>
          <td>'.$row->jumlah_kader_pkbn.'</td>
          <td>'.$row->jumlah_kader_pkdrt.'</td>
          <td>'.$row->jumlah_kader_polaasuh.'</td>
          <td>'.$row->pkbn_klpsimulasi.'</td>
          <td>'.$row->pkbn_anggota.'</td>
          <td>'.$row->pkdrt_klp.'</td>
          <td>'.$row->pkdrt_anggota.'</td>
          <td>'.$row->polaasuh_klp.'</td>
          <td>'.$row->polaasuh_anggota.'</td>
          <td>'.$row->lansia_klp.'</td>
          <td>'.$row->lansia_anggota.'</td>
          <td>'.$row->kerjabakti.'</td>
          <td>'.$row->rukunkematian.'</td>
          <td>'.$row->keagamaan.'</td>
          <td>'.$row->jimpitan.'</td>
          <td>'.$row->arisan.'</td>
          <td></td>
        </tr>
        ';
      }
	}
  public function total_rekap_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='demoopd.wonosobokab.go.id'){
      $where0 = array(
        'pokja_satu.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where0 = array(
        'pokja_satu.status !=' => 99
        );
    }else{
      $where0 = array(
        'pokja_satu.status !=' => 99,
        'pokja_satu.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('pokja_satu');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_pokja_satu()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('jumlah_kader_pkbn', 'jumlah_kader_pkbn', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => 1,
        'id_kabupaten' => 1,
        'id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'tahun' => trim($this->input->post('tahun')),
        'jumlah_kader_pkbn' => trim($this->input->post('jumlah_kader_pkbn')),
        'jumlah_kader_pkdrt' => trim($this->input->post('jumlah_kader_pkdrt')),
        'jumlah_kader_polaasuh' => trim($this->input->post('jumlah_kader_polaasuh')),
        'pkbn_klpsimulasi' => trim($this->input->post('pkbn_klpsimulasi')),
        'pkbn_anggota' => trim($this->input->post('pkbn_anggota')),
        'pkdrt_klp' => trim($this->input->post('pkdrt_klp')),
        'pkdrt_anggota' => trim($this->input->post('pkdrt_anggota')),
        'polaasuh_klp' => trim($this->input->post('polaasuh_klp')),
        'polaasuh_anggota' => trim($this->input->post('polaasuh_anggota')),
        'lansia_klp' => trim($this->input->post('lansia_klp')),
        'lansia_anggota' => trim($this->input->post('lansia_anggota')),
        'kerjabakti' => trim($this->input->post('kerjabakti')),
        'rukunkematian' => trim($this->input->post('rukunkematian')),
        'keagamaan' => trim($this->input->post('keagamaan')),
        'jimpitan' => trim($this->input->post('jimpitan')),
        'arisan' => trim($this->input->post('arisan')),
        'keterangan' => trim($this->input->post('keterangan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'pokja_satu';
      $this->Pokja_satu_model->simpan_pokja_satu($data_input, $table_name);
      /*echo $id;
			$table_name  = 'anggota_keluarga';
      $where       = array(
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_pokja_satu' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);*/
     }
   }
  public function update_pokja_satu()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('jumlah_kader_pkbn', 'jumlah_kader_pkbn', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => 1,
        'id_kabupaten' => 1,
        'id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'tahun' => trim($this->input->post('tahun')),
        'jumlah_kader_pkbn' => trim($this->input->post('jumlah_kader_pkbn')),
        'jumlah_kader_pkdrt' => trim($this->input->post('jumlah_kader_pkdrt')),
        'jumlah_kader_polaasuh' => trim($this->input->post('jumlah_kader_polaasuh')),
        'pkbn_klpsimulasi' => trim($this->input->post('pkbn_klpsimulasi')),
        'pkbn_anggota' => trim($this->input->post('pkbn_anggota')),
        'pkdrt_klp' => trim($this->input->post('pkdrt_klp')),
        'pkdrt_anggota' => trim($this->input->post('pkdrt_anggota')),
        'polaasuh_klp' => trim($this->input->post('polaasuh_klp')),
        'polaasuh_anggota' => trim($this->input->post('polaasuh_anggota')),
        'lansia_klp' => trim($this->input->post('lansia_klp')),
        'lansia_anggota' => trim($this->input->post('lansia_anggota')),
        'kerjabakti' => trim($this->input->post('kerjabakti')),
        'rukunkematian' => trim($this->input->post('rukunkematian')),
        'keagamaan' => trim($this->input->post('keagamaan')),
        'jimpitan' => trim($this->input->post('jimpitan')),
        'arisan' => trim($this->input->post('arisan')),
        'keterangan' => trim($this->input->post('keterangan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'pokja_satu';
      $where       = array(
        'pokja_satu.id_pokja_satu' => trim($this->input->post('id_pokja_satu')),
        'pokja_satu.domain' => $web
			);
      $this->Pokja_satu_model->update_data_pokja_satu($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      if($web=='demoopd.wonosobokab.go.id'){
        $where = array(
        'pokja_satu.id_pokja_satu' => $this->input->post('id_pokja_satu'),
          );
      }elseif($web=='pkk.wonosobokab.go.id'){
        $where = array(
        'pokja_satu.id_pokja_satu' => $this->input->post('id_pokja_satu'),
          );
      }else{
        $where = array(
        'pokja_satu.id_pokja_satu' => $this->input->post('id_pokja_satu'),
          'pokja_satu.id_kecamatan' => $this->session->userdata('id_kecamatan')
          );
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_pokja_satu');
      $result = $this->db->get('pokja_satu');
      echo json_encode($result->result_array());
		}
   
   public function total_pokja_satu()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('pokja_satu');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_pokja_satu = $this->input->post('id_pokja_satu');
      $where = array(
        'id_pokja_satu' => $id_pokja_satu,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('pokja_satu');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'pokja_satu';
        $this->Pokja_satu_model->update_data_pokja_satu($data_update, $where, $table_name);
        echo 1;
        }
		}
  function option_desa_by_id_kecamatan(){
		$id_kecamatan = $this->session->userdata('id_kecamatan');
		$w = $this->db->query("
		SELECT id_desa, nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function option_desa_by_id_desa(){
		$id_kecamatan = $this->input->post('id_kecamatan');
		$w = $this->db->query("
		SELECT id_desa, nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function option_propinsi(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_propinsi="";
		}
		else{
			//$where_id_propinsi="and propinsi.id_propinsi=".$this->session->userdata('id_propinsi')."";
			$where_id_propinsi="";
		}
		$w = $this->db->query("
		SELECT *
		from propinsi
		where propinsi.status = 1
    ".$where_id_propinsi."
		order by propinsi.id_propinsi
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_propinsi.'">'.$h->nama_propinsi.'</option>';
		}
	}
  function id_kabupaten_by_id_propinsi(){
		$id_propinsi = $this->input->post('id_propinsi');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kabupaten
		where kabupaten.status = 1
    and kabupaten.id_propinsi=".$id_propinsi."
		order by kabupaten.id_kabupaten
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kabupaten.'">'.$h->nama_kabupaten.'</option>';
		}
	}
  function id_kecamatan_by_id_kabupaten(){
		$id_kabupaten = $this->input->post('id_kabupaten');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kecamatan
		where kecamatan.status = 1
		and kecamatan.id_kabupaten=".$id_kabupaten."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function id_desa_by_id_kecamatan(){
		$id_kecamatan = $this->session->userdata('id_kecamatan');
		$w = $this->db->query("
		SELECT nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function id_dusun_by_id_desa(){
		$id_desa = $this->input->post('id_desa');
		$w = $this->db->query("
		SELECT *
		from dusun
		where dusun.status = 1
		and dusun.id_desa = ".$id_desa."
		order by id_dusun");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_dusun.'">'.$h->nama_dusun.'</option>';
		}
	}
  function option_kecamatan(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_kecamatan="";
		}
		else{
			$where_id_kecamatan="and data_kecamatan.id_kecamatan=".$this->session->userdata('id_kecamatan')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_kecamatan, kecamatan
		where data_kecamatan.status = 1
		and kecamatan.id_kecamatan=data_kecamatan.id_kecamatan
    ".$where_id_kecamatan."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function status_dalam_keluarga(){
		$w = $this->db->query("
		SELECT *
		from status_dalam_keluarga
		where status_dalam_keluarga.status = 1
		order by status_dalam_keluarga.id_status_dalam_keluarga
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_dalam_keluarga.'">'.$h->nama_status_dalam_keluarga.'</option>';
		}
	}
  function pekerjaan(){
		$w = $this->db->query("
		SELECT *
		from pekerjaan
		where pekerjaan.status = 1
		order by pekerjaan.id_pekerjaan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pekerjaan.'">'.$h->nama_pekerjaan.'</option>';
		}
	}
  function agama(){
		$w = $this->db->query("
		SELECT *
		from agama
		where agama.status = 1
		order by agama.id_agama
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_agama.'">'.$h->nama_agama.'</option>';
		}
	}
  function status_pernikahan(){
		$w = $this->db->query("
		SELECT *
		from status_pernikahan
		where status_pernikahan.status = 1
		order by status_pernikahan.id_status_pernikahan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_pernikahan.'">'.$h->nama_status_pernikahan.'</option>';
		}
	}
  function pendidikan(){
		$w = $this->db->query("
		SELECT *
		from pendidikan
		where pendidikan.status = 1
		order by pendidikan.id_pendidikan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pendidikan.'">'.$h->nama_pendidikan.'</option>';
		}
	}
 }