<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lokasi extends CI_Controller {

	function __construct(){
		parent::__construct();
		// $this->load->library('fpdf');
		$this->load->library('Bcrypt');
		$this->load->library('Uut');
		$this->load->library('Excel');
		$this->load->model('Crud_model');
		$this->load->model('Posting_model');
	}
	
	public function index(){
	 	$data['keterangan'] = 'Lokasi';
	 	$data['judul_halaman'] = 'Lokasi';
	 	$data['nama_halaman'] = 'lokasi';
		$data['main_view'] = 'lokasi/lokasi';
	 	$this->load->view('lokasi', $data); 
	}
	
	public function opd(){
    $web=$this->uut->namadomain(base_url());
    if($web=='diskominfo.wonosobokab.go.id'){
    }
    else{
			$where0 = array(
				'domain' => $web
				);
    $this->db->where($where0);
    }
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
	 	$data['keterangan'] = 'Lokasi';
	 	$data['judul_halaman'] = 'Lokasi';
	 	$data['nama_halaman'] = 'lokasi';
		$data['main_view'] = 'lokasi_opd';
	 	$this->load->view('lokasi', $data); 
	}
	
	public function mapmarkers(){
    $web=$this->uut->namadomain(base_url());
    if($web=='diskominfo.wonosobokab.go.id'){
			$where0 = array(
				'status' => 1
				);
    $this->db->where($where0);
    }
    else{
			$where0 = array(
				'domain' => $web,
				'status' => 1
				);
    $this->db->where($where0);
    $this->db->limit(1);
    }
    $query0 = $this->db->get('dasar_website');
		
			header('Content-type: text/xml defaults-disabled="true"');
			echo '<?xml version="1.0" encoding="UTF-8"?>';
	 	echo '
		
			<markers>
		';
		$a=0;
    foreach ($query0->result() as $row0)
      {
				$a=$a+1;
				echo '
							<marker id="'.$a.'" domain="'.$row0->domain.'" telpon="'.$row0->telpon.'" email="'.$row0->email.'" twitter="'.$row0->twitter.'"  facebook="'.$row0->facebook.'" google="'.$row0->google.'" instagram="'.$row0->instagram.'" name="'.$row0->keterangan.'" address="'.$row0->alamat.'" '.$row0->peta.' type="bar" />
				';
      }
	 	echo '
			</markers>
		';
	}
	
	public function menara(){
	 	$data['keterangan'] = 'Lokasi';
	 	$data['judul_halaman'] = 'Lokasi';
	 	$data['nama_halaman'] = 'lokasi';
		$data['main_view'] = 'lokasi/menara';
	 	$this->load->view('menara', $data); 
	}
	
 	public function option_kecamatan(){
    	$id_kabupaten = 1;
		$kecamatan     = $this->db->query(
			"
			select *
			from kecamatan
			where id_kabupaten=$id_kabupaten
			"
			);
		echo '<option value="semua"> Pilih Kecamatan </option>';
		foreach ($kecamatan->result() as $b1) {
			echo '<option value="'.$b1->id_kecamatan.'">'.$b1->nama_kecamatan.'</option>';
		}
  	}
	
  public function peta_kita(){
    $this->form_validation->set_rules('id_kabupaten', 'id_kabupaten', 'required');
		$id_kabupaten = trim($this->input->post('id_kabupaten'));
		$id_kecamatan = trim($this->input->post('id_kecamatan'));
		if ($this->form_validation->run() == FALSE) {}
		else {
		echo '
		<div id="mapContainer" style="width:100%;height:600px;"></div>
		<script language="javascript" type="text/javascript">
		function initialize() {
			var mapAttr = {
					center: new google.maps.LatLng(-7.3587905,109.9030928),
					zoom: 11,
					draggableCursor: \'crosshair\',
					mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			map = new google.maps.Map(document.getElementById("mapContainer"), mapAttr);
			zonemarkers();
			towermarkers();
				';
				$id_kabupaten = '1';
				//$id_kecamatan = '1';
				if( $id_kecamatan == '' ){
					$where = array(
						'id_kabupaten' => $id_kabupaten
					);
					//$peta_kabupaten = $this->Peta_desa_model->by_peta_kabupaten($where);
					$peta_kecamatan = $this->Peta_desa_model->by_peta_kecamatan($where);
				}
				else if($id_kecamatan > 0){
					$where = array(
						'peta_kecamatan.id_kecamatan' => $id_kecamatan
					);
					$peta_kecamatan = $this->Peta_desa_model->by_peta_kecamatan($where);
				}
				else{
					$where = array(
						'peta_kecamatan.id_kabupaten' => $id_kabupaten
					);
					$peta_kecamatan = $this->Peta_desa_model->by_peta_kecamatan($where);
				}
				$a = 0;
				foreach ($peta_kecamatan->result() as $r) {
					$a = $a + 1;
					echo '
			var contentPeta = \'<b>Info Peta</b><br /> '.$r->center.'\';
			kecMarker = new google.maps.Marker(
			{
				position: map.getCenter(),
				map: map,
				title: \'Starting Point\',
				draggable: false
			});
			var infoPeta = new google.maps.InfoWindow({
				content: contentPeta,
				maxWidth: 200
			});
			kecMarker.addListener(\'click\', function() {
				infoPeta.open(map, kecMarker);
			});

		var wonosoboCoords = [';
					echo ''.$r->koordinat.'';
					echo '
				];
		var peta_desa = new google.maps.Polygon({
				map: map,
				paths: wonosoboCoords,
				strokeColor: \''.$r->fillcolor.'\',
				strokeOpacity: 0.8,
				strokeWeight: 2,
				fillColor: \''.$r->fillcolor.'\',
				fillOpacity: 0.35
			});
			peta_desa.setMap(map);
			peta_desa.addListener(\'click\', showArrays);
			infopeta_desa = new google.maps.InfoWindow;
			function showArrays(event) {
				var vertices = this.getPath();
				var contentString = \'\' + 
					\'<b>Clicked location: </b><br>\' + event.latLng.lat() + \',\' + event.latLng.lng() +
					\'\';
				infopeta_desa.setContent(contentString);
				infopeta_desa.setPosition(event.latLng);
				infopeta_desa.open(map);
				}
					';
				}
		echo
				'
		}
		google.maps.event.addDomListener(window, \'load\', initialize);
	</script>
  
		';
		}
  }
}
