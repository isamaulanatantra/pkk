<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Halaman extends CI_Controller {

	function __construct(){
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Posting_model');
		}
  
  public function index()
  {
    $web=$this->uut->namadomain(base_url());
      $where0 = array(
        'domain' => $web
        );
      $this->db->where($where0);
      $this->db->limit(1);
      $query0 = $this->db->get('dasar_website');
      foreach ($query0->result() as $row0)
        {
          $data['domain'] = $row0->domain;
          $data['alamat'] = $row0->alamat;
          $data['telpon'] = $row0->telpon;
          $data['email'] = $row0->email;
          $data['twitter'] = $row0->twitter;
          $data['facebook'] = $row0->facebook;
          $data['google'] = $row0->google;
          $data['instagram'] = $row0->instagram;
          $data['peta'] = $row0->peta;
          $data['keterangan'] = $row0->keterangan;
        }
        
      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->status == 1 ){
            if( $row1->judul_komponen == 'Header' ){ //Header
              $data['Header'] = '
            <div id="icons" class="row">
              <div class="container background-grey bottom-border">
                <div class="row">
                  <center class="animate fadeInRightBig animated">
                  '.$row1->isi_komponen.'
                  </center>
                  <!-- End Icons -->
                </div>
              </div>
            </div>
              ';
            }
            else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
              $data['KolomKiriAtas'] = '
            <div id="icons" class="row">
              <div class="container background-grey bottom-border">
                <div class="row">
                  <center class="animate fadeInRightBig animated">
                  '.$row1->isi_komponen.'
                  </center>
                  <!-- End Icons -->
                </div>
              </div>
            </div>
              ';
            }
            else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
              $data['KolomKananAtas'] = '
            <div class="container background-white bottom-border">
              <center class="animate fadeInRightBig animated"><br />
                '.$row1->isi_komponen.'
              </center>
            </div>
              ';
            }
            else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
              $data['KolomKiriBawah'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
              $data['KolomPalingBawah'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
              if($web=='pmi.wonosobokab.go.id'){
                $data['KolomKananBawah'] = ''.$row1->isi_komponen.'';
              }else{
                $data['KolomKananBawah'] = '
                <div id="icons" class="row">
                  <div class="container background-grey bottom-border">
                    <div class="row">
                      <center class="animate fadeInRightBig animated"><br />
                    '.$row1->isi_komponen.'
                      </center>
                      <!-- End Icons -->
                    </div>
                  </div>
                </div>
                ';
                
              }
            }
            else{ 
            }
          }
          else{ 
          }
        }
        
      $a = 0;
      $data['total'] = 10;
        
      if($web=='pkk.wonosobokab.go.id'){
        $data['menu_atas'] = $this->menu_atas_pkk(0,$h="", $a);
        $data['menu_mobile'] = $this->menu_mobile_pkk(0,$h="", $a);
        $data['galery_berita'] = $this->option_posting_terbarukan_pkk($h="", $a);
        $data['judul'] = 'Selamat datang';
        $data['main_view'] = 'welcome/dashboard_pkk';
        $this->load->view('pkk', $data);
      }
      else if($web=='pmi.wonosobokab.go.id'){
        $data['menu_atas'] = $this->menu_atas_pmi(0,$h="", $a);
        $data['highlight'] = $this->highlight_pmi($h="", $a);
        $data['galery_berita'] = $this->option_posting_disparbud($h="", $a);
        $data['terbarukan'] = $this->option_posting_terbarukan_pmi(0, $h="", $a);
        $data['judul'] = 'Selamat datang';
        $data['main_view'] = 'halaman/hallo';
        $this->load->view('halaman', $data);
      }
    else{
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['menu_mobile'] = $this->menu_mobile_pkk(0,$h="", $a);
      $data['highlight'] = $this->highlight($h="", $a);
      if($web=='dikpora.wonosobokab.go.id'){
      $data['galery_berita'] = $this->option_posting_dikpora($h="", $a);
      }else{
      $data['galery_berita'] = $this->option_posting_disparbud($h="", $a);
      }
      $data['terbarukan'] = $this->option_posting_terbarukan($h="", $a);
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'halaman/hallo';
      $this->load->view('halaman', $data);
      }
  }
  
  public function halaman2()
  {
    $web=$this->uut->namadomain(base_url());
      $where0 = array(
        'domain' => $web
        );
      $this->db->where($where0);
      $this->db->limit(1);
      $query0 = $this->db->get('dasar_website');
      foreach ($query0->result() as $row0)
        {
          $data['domain'] = $row0->domain;
          $data['alamat'] = $row0->alamat;
          $data['telpon'] = $row0->telpon;
          $data['email'] = $row0->email;
          $data['twitter'] = $row0->twitter;
          $data['facebook'] = $row0->facebook;
          $data['google'] = $row0->google;
          $data['instagram'] = $row0->instagram;
          $data['peta'] = $row0->peta;
          $data['keterangan'] = $row0->keterangan;
        }
        
      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->status == 1 ){
            if( $row1->judul_komponen == 'Header' ){ //Header
              $data['Header'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
              $data['KolomKiriAtas'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
              $data['KolomKananAtas'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
              $data['KolomKiriBawah'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
              $data['KolomPalingBawah'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
              $data['KolomKananBawah'] = $row1->isi_komponen;
            }
            else{ 
            }
          }
          else{ 
          }
        }
        
      $a = 0;
      $data['total'] = 10;
        
      if($web=='pkk.wonosobokab.go.id'){
        $data['menu_atas'] = $this->menu_atas_pkk(0,$h="", $a);
        $data['menu_mobile'] = $this->menu_mobile_pkk(0,$h="", $a);
        $data['galery_berita'] = $this->option_posting_terbarukan_pkk($h="", $a);
        $data['judul'] = 'Selamat datang';
        $data['main_view'] = 'welcome/welcome_pkk';
        $this->load->view('pkk', $data);
      }
    else{
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['highlight'] = $this->highlight($h="", $a);
      if($web=='dikpora.wonosobokab.go.id'){
      $data['galery_berita'] = $this->option_posting_dikpora($h="", $a);
      }else{
      $data['galery_berita'] = $this->option_posting_disparbud($h="", $a);
      }
      $data['terbarukan'] = $this->option_posting_terbarukan($h="", $a);
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'halaman/hallo';
      $this->load->view('halaman2', $data);
      }
  }

private function menu_atas_pkk($parent=NULL,$hasil, $a){
	$web=$this->uut->namadomain(base_url());
	$a = $a + 1;
	$w = $this->db->query("
	SELECT * from posting
	where posisi='menu_atas'
	and parent='".$parent."' 
	and status = 1 
	and tampil_menu_atas = 1 
	and domain='".$web."'
	order by urut
	");
	$x = $w->num_rows();
	if(($w->num_rows())>0)
	{
	if($parent == 0){
      $hasil .= '
      <ul id="responsivemenu">
      <li class="active"><a href="'.base_url().'"><i class="icon-home homeicon"></i><span class="showmobile">Home</span></a></li>
      ';
	}
	else{
  if($a>2){
  $hasil .= '
  <ul style="left: 100%; top: 10px; display: none;">
    ';
  }else{
  $hasil .= '
  <ul style="display: none;">
    ';
  }
	  }
	  }
	  $nomor = 0;
	  foreach($w->result() as $h)
	  {
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
	  $r = $this->db->query("
	  SELECT * from posting
	  where parent='".$h->id_posting."' 
	  and status = 1 
	  and tampil_menu_atas = 1 
	  and domain='".$web."'
	  order by urut
	  ");
	  $xx = $r->num_rows();
	  $nomor = $nomor + 1;
	  if( $a > 1 ){
			if ($xx == 0){
					if($h->judul_posting == 'POKJA 4'){
						$hasil .= '
						  <li><a href="'.base_url().'pkk/pokja4" role="button" aria-expanded="false" target="_blank"> '.$h->judul_posting.'</a>
						';
					}
        elseif($h->judul_posting == 'POKJA 1'){
						$hasil .= '
						  <li><a href="'.base_url().'pkk/pokja1" role="button" aria-expanded="false" target="_blank"> '.$h->judul_posting.'</a>
						';
					}
        elseif($h->judul_posting == 'POKJA 2'){
						$hasil .= '
						  <li><a href="'.base_url().'pkk/pokja2" role="button" aria-expanded="false" target="_blank"> '.$h->judul_posting.'</a>
						';
					}
        elseif($h->judul_posting == 'POKJA 3'){
						$hasil .= '
						  <li><a href="'.base_url().'pkk/pokja3" role="button" aria-expanded="false" target="_blank"> '.$h->judul_posting.'</a>
						';
					}
          else{
            $hasil .= '
              <li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> '.$h->judul_posting.' </a>
            ';
          }
			}
			else{
			$hasil .= '
        <li> <a href="">'.$h->judul_posting.' </a>
      ';
			} 
		}
		else{
      if ($xx == 0){
        if($h->judul_posting == 'SIM PKK'){
          $hasil .= '
            <li><a href="'.base_url().'login"> '.$h->judul_posting.' </a>
          ';
        }
        elseif($h->judul_posting == 'Pokja 4'){
						$hasil .= '
						  <li><a href="'.base_url().'pkk/pokja4" role="button" aria-expanded="false" target="_blank"> '.$h->judul_posting.'</a>
						';
					}
        else{
          $hasil .= '
          <li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> '.$h->judul_posting.' </a>
          ';
        }
      }
      else{
        $hasil .= '
          <li> <a href="">'.$h->judul_posting.' </a>
        ';
      }
		}
		$hasil = $this->menu_atas_pkk($h->id_posting,$hasil, $a);
		$hasil .= '
	  </li>
	  ';
	  }
	  if(($w->num_rows)>0)
	  {
	  $hasil .= "
	</ul>
	";
	}
	else{
	}
	return $hasil;
}
  private function menu_mobile_pkk($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
					$hasil .= '<ul> <li><a href="'.base_url().'" role="button" aria-expanded="false">HOME</a></li>';
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
        $hasil .= '
          <li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
        }
			else{
				if($h->judul_posting == 'Berita'){
					$hasil .= '
					  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
				}
				elseif($h->judul_posting == 'Pengumuman'){
					$hasil .= '
					  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
				}
				elseif($h->judul_posting == 'Pengaduan Masyarakat'){
					$hasil .= '
				  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				elseif($h->judul_posting == 'Permohonan Informasi Publik'){
					$hasil .= '
				  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				elseif($h->judul_posting == 'Permohonan Informasi'){
					$hasil .= '
				  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				else{
				  if ($xx == 0){
					if($h->judul_posting == 'Berita'){
						$hasil .= '
						  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengumuman'){
						$hasil .= '
							<li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengaduan Masyarakat'){
						$hasil .= '
					  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi Publik'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
				  	else{
					$hasil .= '
					  <li><a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span></a>';
					  }
				  }
				  else{
					if($h->judul_posting == 'Berita'){
						$hasil .= '
						  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengumuman'){
						$hasil .= '
							<li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengaduan Masyarakat'){
						$hasil .= '
					  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi Publik'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
				  	else{
						$hasil .= '
						  <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
						} 
					  }
				}
      }
      
			$hasil = $this->menu_mobile_pkk($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  private function option_posting_terbarukan_pkk($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 9
		");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
			$hasil .= 
			'
      
          <div class="c4" style="text-align:center;">
            <div class="featured-projects">
              <div class="featured-projects-image"><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><img class="imgOpa teamimage" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="Image"></a></div>
              <div class="featured-projects-content">
                <span class="hirefor"><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></span>
              </div>
            </div>
          </div>
			';
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  private function option_posting_dikpora($hasil, $a){
    $web=$this->uut->namadomain(base_url());
    $a = $a + 1;
    $w = $this->db->query("
    SELECT *
    from posting
    where posting.status = 1 
    and posting.highlight = 1
    and posting.parent != 0
    and posting.domain = '".$web."'
    order by created_time desc
    limit 10
    ");
    if(($w->num_rows())>0){
    } 
    $nomor = 0;
    foreach($w->result() as $h)
    {
      $nomor = $nomor + 1;
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_","","","","","","","","","","","","","");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
        $hasil .= 
        '
            <article class="simple-post clearfix">
              <div class="simple-thumb">
                <a href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML">
                <img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt=""/>
                </a>
              </div>
              <header>
                <h4>
                  <a href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a>
                </h4>
                <p class="simple-share">
                  <span><i class="fa fa-clock-o"></i> '.$created_time.'</span>
                </p>
                <p style="text-align:justify"><span style="font-size:14px">'.substr(strip_tags($h->isi_posting), 0, 200).'...</span></p>
              </header>
            </article>
            
        ';/*
      if( $nomor == 1 ){
        $hasil .= 
        '
          <article class="news-block">
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="overlay-link">
              <figure class="image-overlay">
                <img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" width="870" height="500" alt=""/>
              </figure>
            </a>
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="category">'.$h->judul_posting.'</a>
            <div class="news-details">
              <h2 class="news-title">
                <a href="#">
                </a>
              </h2>
              <p>
              <p style="text-align:justify"><span style="font-size:14px">'.substr(strip_tags($h->isi_posting), 0, 200).'</span></p>
              <p class="simple-share">
                <span class="article-date"><i class="fa fa-clock-o"></i> '.$created_time.'</span>
              </p>
            </div>
          </article>
        ';
      }
      else{
        $hasil .= 
        '
            <article class="simple-post clearfix">
              <div class="simple-thumb">
                <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
                <img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt=""/>
                </a>
              </div>
              <header>
                <h2>
                  <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a>
                </h2>
                <p class="simple-share">
                  <span><i class="fa fa-clock-o"></i> '.$created_time.'</span>
                </p>
              </header>
            </article>
            
        ';
      }*/
    }
    if(($w->num_rows)>0){
    }
    return $hasil;
  }
  private function option_posting_disparbud($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 10
		");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
				$hasil .= 
				'
						<article class="simple-post clearfix">
							<div class="simple-thumb">
								<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
								<img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt=""/>
								</a>
							</div>
							<header>
								<h3>
									<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a>
								</h3>
								<p class="simple-share">
									<span><i class="fa fa-clock-o"></i> '.$created_time.'</span>
								</p>
                <p style="text-align:justify"><span style="font-size:14px">'.substr(strip_tags($h->isi_posting), 0, 200).'</span></p>
							</header>
						</article>
						
				';/*
			if( $nomor == 1 ){
				$hasil .= 
				'
          <article class="news-block">
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="overlay-link">
              <figure class="image-overlay">
                <img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" width="870" height="500" alt=""/>
              </figure>
            </a>
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="category">'.$h->judul_posting.'</a>
            <div class="news-details">
              <h2 class="news-title">
                <a href="#">
                </a>
              </h2>
              <p>
              <p style="text-align:justify"><span style="font-size:14px">'.substr(strip_tags($h->isi_posting), 0, 200).'</span></p>
              <p class="simple-share">
                <span class="article-date"><i class="fa fa-clock-o"></i> '.$created_time.'</span>
              </p>
            </div>
          </article>
				';
			}
			else{
				$hasil .= 
				'
						<article class="simple-post clearfix">
							<div class="simple-thumb">
								<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
								<img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt=""/>
								</a>
							</div>
							<header>
								<h2>
									<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a>
								</h2>
								<p class="simple-share">
									<span><i class="fa fa-clock-o"></i> '.$created_time.'</span>
								</p>
							</header>
						</article>
						
				';
			}*/
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  private function highlight($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 9
		");
		if(($w->num_rows())>0){
    $hasil .= '<ul class="portfolio-group">';
		}
    else{
    $hasil .= '<ul>';
    }
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
			$yummy   = array("_","","","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
        $hasil .= '
          <li class="col-md-4 col-sm-6 col-xs-12 portfolio-item no-padding no-margin">
            <a class="" href="'.base_url().'postings/details/'.$h->id_posting.'/'.str_replace(' ', '_', $newphrase ).'.HTML">
                <figure class="animate fadeInLeft">
                    <img alt="'.substr(strip_tags($h->judul_posting), 0, 100).'" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'">
                    <figcaption>
                        <u><strong>'.$h->judul_posting.'</strong></u><br />
                        <span>'.substr(strip_tags($h->isi_posting), 0, 150).'</span>
                    </figcaption>
                </figure>
            </a>
          </li>';
		}
		if(($w->num_rows)>0){
	  $hasil .= "</ul>";
		}
		return $hasil;
	}
  private function highlight_pmi($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT id_posting, created_time, judul_posting, isi_posting, domain, keterangan
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 9
		");
		if(($w->num_rows())>0){
    $hasil .= '<ul class="portfolio-group">';
		}
    else{
    $hasil .= '<ul>';
    }
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
			$yummy   = array("_","","","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
        $hasil .= '
                      <div class="column is-12">
                        <a href="'.base_url().'postings/details/'.$h->id_posting.'" class="card is-horizontal">
                          <div class="card-image">
                            <figure class="image"><img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="5d36c7767064f"></figure>
                          </div>
                          <div class="card-content">
                            <div class="content">
                              <h5>'.$h->judul_posting.'</h5>
                              <div class="has-text-grey has-margin-b-6"><small class="has-text-danger">'.$h->keterangan.'</small><span class="has-margin-l-6">|</span><small class="has-margin-l-6"><i class="ion-clock has-margin-r-6"></i><time datetime="'.$created_time.'">'.$created_time.'</time></small></div>
                              '.substr(strip_tags($h->isi_posting), 0, 300).'
                            </div>
                          </div>
                        </a>
                      </div>
        ';
		}
		if(($w->num_rows)>0){
	  $hasil .= "</ul>";
		}
		return $hasil;
	}
  private function option_posting_terbarukan($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 12
		");
		if(($w->num_rows())>0){
    }
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
			$yummy   = array("_","","","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
      if($nomor==1){
      $hasil .='<div class="item active"><div class="row">';
      }
      elseif($nomor==7){
      $hasil .='<div class="item"><div class="row">';
      }
      $hasil .='
      
      <div class="col-md-2 col-sm-4 col-xs-4">
        <a data-toggle="tooltip" title="'.$h->judul_posting.'" data-placement="bottom" href="'.base_url().'postings/details/'.$h->id_posting.'/'.str_replace(' ', '_', $newphrase ).'.HTML">
          <img alt="'.substr(strip_tags($h->judul_posting), 0, 80).'" class="well" style="height: 120px;" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'">
        </a>
      </div>
      
      ';
      if($nomor==6 or $nomor==12){
      $hasil .='</div></div>';
      }
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  private function option_posting_terbarukan_pmi($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    order by urut asc, created_time desc limit 3
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
		}
    if($parent == 0){
        $hasil .= '<div class="box-sidebar has-margin-b-1">';
    }
    else{
    $hasil .= '
      <div class="box-sidebar has-margin-b-1">
      ';
      }
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
		and status = 1 
    and domain='".$web."'
    order by urut
    
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
          if ($xx == 0){
            $hasil .= '
                        <div class="column is-full">
                          <a href="'.base_url().'postings/details/'.$h->id_posting.'" class="has-text-dark">
                            <div class="menu-sidebar">'.$h->judul_posting.'</div>
                          </a>
                        </div>
                      ';
            }
          else{
            $hasil .= '
                        <div class="column is-full">
                          <a href="'.base_url().'postings/details/'.$h->id_posting.'" class="has-text-dark">
                            <div class="menu-sidebar">'.$h->judul_posting.'</div>
                          </a>
                        </div>
                      ';
            }
        }
			else{
          if ($xx == 0){
            $hasil .= '
                      <div class="title-pmi has-margin-b-1">
                        <div class="decor">
                          <h3 class="title is-spaced">'.$h->judul_posting.'</h3>
                          <div class="border-pmi"><span></span><span></span><span></span><span></span></div>
                        </div>
                      </div>
                      ';
            }
          else{
            $hasil .= '
                      <div class="title-pmi has-margin-b-1">
                        <div class="decor">
                          <h3 class="title is-spaced">'.$h->judul_posting.'</h3>
                          <div class="border-pmi"><span></span><span></span><span></span><span></span></div>
                        </div>
                      </div>
                      ';
            }
        }
			$hasil = $this->option_posting_terbarukan_pmi($h->id_posting,$hasil, $a);
      $hasil .= '</div>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= '</div>';
        $hasil .= '<div class="box-sidebar has-margin-b-1">';
		}
    else{
      
    }
		
		return $hasil;
	}
 
  public function get_attachment_by_id_posting($id_posting)
   {
     $where = array(
						'id_tabel' => $id_posting,
						'table_name' => 'posting'
						);
    $d = $this->Posting_model->get_attachment_by_id_posting($where);
    $no=0;
    if(!$d)
          {
						return 'logo-the-soul-of-java-'.$no.'.png';
          }
        else
          {
						return $d['file_name'];
          }
   } 
 
private function menu_atas($parent=NULL,$hasil, $a){
	$web=$this->uut->namadomain(base_url());
  if($web=='disparbud.wonosobokab.go.id' or $web=='dikpora.wonosobokab.go.id'){
    $url_baca='post/detail';
    $url_baca_list='post/galeri';
  }else{
    $url_baca='postings/details';
    $url_baca_list='postings/categoris';
  }
	$a = $a + 1;
	$w = $this->db->query("
	SELECT * from posting
	where posisi='menu_atas'
	and parent='".$parent."' 
	and status = 1 
	and tampil_menu_atas = 1 
	and domain='".$web."'
	order by urut
	");
	$x = $w->num_rows();
	if(($w->num_rows())>0)
	{
	if($parent == 0){
	if($web == 'zb.wonosobokab.go.id'){
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'website" class="btn btn-primary">BERANDA</a></li>
	'; 
	}
	elseif ($web == 'ppiddemo.wonosobokab.go.id'){
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'"><i class=""></i>BERANDA</a></li>
	<li class="parent">
	  <span class="">PPID </span>
	  <ul>
		<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid">PPID </a></span></li>
		<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid_pembantu">PPID Pembantu</a></span></li>
	  </ul>
	</li>
	';
	}
	else{
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'" class="">BERANDA</a></li>
	'; 
	}
	}
	else{
	$hasil .= '
	<ul>
	  ';
	  }
	  }
	  $nomor = 0;
	  foreach($w->result() as $h)
	  {
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
	  $r = $this->db->query("
	  SELECT * from posting
	  where parent='".$h->id_posting."' 
	  and status = 1 
	  and tampil_menu_atas = 1 
	  and domain='".$web."'
	  order by urut
	  ");
	  $xx = $r->num_rows();
	  $nomor = $nomor + 1;
	  if( $a > 1 ){
			if ($xx == 0){
        if($h->judul_posting == 'Pengaduan Masyarakat'){
				$hasil .= '
				<li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Permohonan Informasi Publik'){
				$hasil .= '
				<li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
        elseif($h->judul_posting == 'Permohonan Informasi'){
        $hasil .= '
        <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
        ';
        }
				elseif($h->judul_posting == 'Sitemap'){
				$hasil .= '
				<li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}else{
					$hasil .= '
					<li><a href="'.base_url().''.$url_baca.'/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class="">'.$h->judul_posting.' </span></a>';
        }
      }
			else{
			$hasil .= '
			<li class="parent" > <span class="">'.$h->judul_posting.' </span>';
			} 
		}
		else{
      
				if ($xx == 0){
          if($h->judul_posting == 'Prestasi Entitas Pendidikan'){
            $hasil .= '
              <li><a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
          }
          elseif($h->judul_posting == 'Sitemap'){
          $hasil .= '
          <li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
          ';
          }else{
    				$hasil .= '
    				<li><a href="'.base_url().''.$url_baca_list.'/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
    			}
        }
				else{
					$hasil .= '
					<li><a href="'.base_url().''.$url_baca.'/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
				}
    }
		$hasil = $this->menu_atas($h->id_posting,$hasil, $a);
		$hasil .= '
	  </li>
	  ';
	  }
	  if(($w->num_rows)>0)
	  {
	  $hasil .= "
	</ul>
	";
	}
	else{
	}
	return $hasil;
}
  
private function menu_atas_pmi($parent=NULL,$hasil, $a){
	$web=$this->uut->namadomain(base_url());
  if($web=='disparbud.wonosobokab.go.id' or $web=='dikpora.wonosobokab.go.id'){
    $url_baca='post/detail';
    $url_baca_list='post/galeri';
  }else{
    $url_baca='postings/details';
    $url_baca_list='postings/categoris';
  }
	$a = $a + 1;
	$w = $this->db->query("
	SELECT * from posting
	where posisi='menu_atas'
	and parent='".$parent."' 
	and status = 1 
	and tampil_menu_atas = 1 
	and domain='".$web."'
	order by urut
	");
    $x = $w->num_rows();
    if(($w->num_rows())>0)
    {
    if($parent == 0){
      $hasil .= '
      </div>
      </div>
      <div class="navbar-item dropdown is-hoverable is-hidden-mobile">
        <div class="dropdown-trigger">
      ';
    }
    else{
    $hasil .= '
      </div>
      <div id="dropdown-menu" role="menu" class="dropdown-menu"><div class="dropdown-content"><p>
      ';
      }
      }
      $nomor = 0;
      /*$hasil .= '
      ';*/
      foreach($w->result() as $h)
      {
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
        $yummy   = array("_","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
        $r = $this->db->query("
          SELECT * from posting
          where parent='".$h->id_posting."' 
          and status = 1 
          and tampil_menu_atas = 1 
          and domain='".$web."'
          order by urut
          ");
        $xx = $r->num_rows();
        $nomor = $nomor + 1;
        if( $a > 1 ){
          $hasil .= '
              <a href="'.base_url().''.$url_baca.'/'.$h->id_posting.'" class="dropdown-item">'.$h->judul_posting.'</a>
          ';
        }
        else{
          $hasil .= '
            <a href="'.base_url().''.$url_baca.'/'.$h->id_posting.'" class="has-text-dark nuxt-link-exact-active nuxt-link-active"><span aria-haspopup="true" aria-controls="dropdown-menu">'.$h->judul_posting.'</span></a>
          ';
          /*$hasil .= '<div class="navbar-item dropdown is-hoverable is-hidden-mobile"><div class="dropdown-trigger">';
          if ($xx == 0){
          $hasil .= '
            <a href="'.base_url().''.$url_baca.'/'.$h->id_posting.'" class="has-text-dark nuxt-link-exact-active nuxt-link-active"><span aria-haspopup="true" aria-controls="dropdown-menu">'.$h->judul_posting.'</span></a>
        </div>
      </div>
          ';
          $hasil .= '<div class="navbar-item dropdown is-hoverable is-hidden-mobile"><div class="dropdown-trigger">';
          }
          else{
          $hasil .= '
            <a href="'.base_url().''.$url_baca.'/'.$h->id_posting.'" class="has-text-dark nuxt-link-exact-active nuxt-link-active"><span aria-haspopup="true" aria-controls="dropdown-menu">'.$h->judul_posting.'</span></a>
          </div>
          ';
          $hasil .= '<div class="navbar-item dropdown is-hoverable is-hidden-mobile"><div class="dropdown-trigger">';
          }
          $hasil .= '</div>'; */
        }
        $hasil = $this->menu_atas_pmi($h->id_posting,$hasil, $a);
      }
      if(($w->num_rows)>0)
      {
        $hasil .= '
          </p>
        </div>
      </div>
      </div>
      <div class="navbar-item dropdown is-hoverable is-hidden-mobile">
        <div class="dropdown-trigger">
        ';
      }
      else{
          $hasil .= "
          ";
      }
    return $hasil;
  }

}