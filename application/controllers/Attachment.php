<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attachment extends CI_Controller {

	function __construct()
		{
		parent::__construct();
    $this->load->library('upload', 'image_lib');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
    $this->load->library('Bcrypt');
    $this->load->model('Crud_model');
		$this->load->helper('file');
		}
  
	public function index()
		{
    }
		
  public function upload_foto_profil()
		{
		$where = array(
              'id_tabel' => $this->session->userdata('id_users'),
              'keterangan' => 'foto_profil',
							'table_name' => 'foto_profil'
              );
							
			$this->db->where($where);
			$this->db->delete('attachment');
			
      $config['upload_path'] = './media/upload/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '50000';
			$this->upload->initialize($config);
			$uploadFiles = array('img_1' => 'myfile', 'img_2' => 'e_myfile', );		
			$this->load->library('image_lib');
			$newName = '-';
			$table_name = $_GET['table_name'];
			foreach($uploadFiles as $key => $files)
				{
				if ($this->upload->do_upload($files)) 
					{
						$upload = $this->upload->data();
						$file = explode(".", $upload['file_name']);
						$prefix = date('Ymdhis');
						$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
						$filePath =  $upload['file_path'].$newName;
						rename($upload['full_path'],$filePath);
            $data_input = array(
              'table_name' => $table_name,
              'file_name' => $newName,
              'keterangan' => 'foto_profil',
              'id_tabel' => $this->session->userdata('id_users'),
              'temp' => 'foto_profil',
              'uploaded_time' => date('Y-m-d H:i:s'),
              'uploaded_by' => $this->session->userdata('id_users')
              );
						$table_name = 'attachment';
						$id         = $this->Crud_model->save($data_input, $table_name);
						$config['image_library'] = 'gd2';
						$config['source_image']  = './media/upload/'.$newName.'';
						$config['new_image']  = './media/upload/s_'.$newName.'';						
						
            $config['width']	 = 128;
						$config['height']	= 128;
						$this->image_lib->initialize($config); 
						$this->image_lib->resize();
						$file = './media/upload/s_'.$newName.'';
						$j = get_mime_by_extension($file);
						$ex = explode('/', $j);
						$e = $ex[0];
            echo
            '
						<div class="alert alert-success alert-dismissable" id="img_upload">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						'; 
						if( $e == 'image'){
							echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"><img src="'.base_url().'media/upload/s_'.$newName.'" /></a>';
							}
						else{
							echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"> '.$newName.' </a>';
							}
            echo '</div>';
						echo 'OK';
					}
					
				}
				
		}
		
  public function upload()
		{
      $config['upload_path'] = './media/upload/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '50000';
			$this->upload->initialize($config);
			$uploadFiles = array('img_1' => 'myfile', 'img_2' => 'e_myfile', );		
			$this->load->library('image_lib');
			$newName = '-';
			$mode = $this->input->post('mode');
			$this->form_validation->set_rules('remake', 'remake', 'required');
			if ($this->form_validation->run() == FALSE)
				{
					echo
					'
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
						Mohon Keterangan Lampiran Diisi
					</div>
					';
				}
			else
				{
					foreach($uploadFiles as $key => $files)
					{
					if ($this->upload->do_upload($files)) 
						{
							$upload = $this->upload->data();
							$file = explode(".", $upload['file_name']);
              $ext= ( count($file) - 1 );
							$prefix = date('Ymdhis');
							$newName =$prefix.'_'.$this->session->userdata('id_users').'.'. $file[$ext]; 
							$filePath =  $upload['file_path'].$newName;
							rename($upload['full_path'],$filePath);
              
              
              if( $mode == 'edit' ){
							$data_input = array(
								'table_name' => $this->input->get('table_name'),
								'file_name' => $newName,
								'keterangan' => $this->input->post('remake'),
								'id_tabel' => $this->input->post('id'),
								'temp' => $this->input->post('temp'),
								'uploaded_time' => date('Y-m-d H:i:s'),
								'uploaded_by' => $this->session->userdata('id_users')
								);
              }
              else{
							$data_input = array(
								'table_name' => $this->input->get('table_name'),
								'file_name' => $newName,
								'keterangan' => $this->input->post('remake'),
								'id_tabel' => 0,
								'temp' => $this->input->post('temp'),
								'uploaded_time' => date('Y-m-d H:i:s'),
								'uploaded_by' => $this->session->userdata('id_users')
								);
              }
							$table_name = 'attachment';
							$id         = $this->Crud_model->save($data_input, $table_name);
							$config['image_library'] = 'gd2';
							$config['source_image']  = './media/upload/'.$newName.'';
							$config['new_image']  = './media/upload/k_'.$newName.'';						
							$config['width']	 = 100;
							$config['height']	= 100;
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
							$file = './media/upload/s_'.$newName.'';
              
							$config['source_image']  = './media/upload/'.$newName.'';
							$config['new_image']  = './media/upload/s_'.$newName.'';						
							$config['width']	 = 474;
							$config['height']	= 474;
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
              
							$config['source_image']  = './media/upload/'.$newName.'';
							$config['new_image']  = './media/upload/sb_'.$newName.'';						
							$config['width']	 = 360;
							$config['height']	= 360;
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
              
							$config['source_image']  = './media/upload/'.$newName.'';
							$config['new_image']  = './media/upload/b_'.$newName.'';						
							$config['width']	 = 555;
							$config['height']	= 555;
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
              
							$config['source_image']  = './media/upload/'.$newName.'';
							$config['new_image']  = './media/upload/sb_'.$newName.'';						
							$config['width']	 = 750;
							$config['height']	= 750;
							$this->image_lib->initialize($config); 
							$this->image_lib->resize();
							//100
              //474
              //555
              
              
							$j = get_mime_by_extension($file);
							$ex = explode('/', $j);
							$e = $ex[0];
							echo
							'
							<div class="alert alert-success alert-dismissable" id="img_upload">
							<i class="fa fa-check"></i>
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							'; 
							if( $e == 'image'){
								echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"><img src="'.base_url().'media/upload/s_'.$newName.'" /></a>';
								}
							else{
								echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"> '.$newName.' </a>';
								}
							echo '</div>';
						}
					}
				}
		}	
		
	public function foto_profil()
		{
      $table_name = $_GET['table_name'];
			$limit    = 200;
      $start    = 0;
      $fields = 
				'
				id_attachment,
				table_name,
				file_name,
				keterangan,
				id_tabel,
				temp,
				uploaded_time,
				uploaded_by
				';
      $where    = array(
        'attachment.id_tabel' => $this->session->userdata('id_users'),
				'table_name' => $table_name
				);
      $this->db->select("$fields");
      $this->db->where($where);
      $this->db->order_by('attachment.id_attachment', 'DESC');
      $result = $this->db->get('attachment');
      echo json_encode($result->result_array());
		}
    
	public function load_lampiran()
		{
      $mode = $this->input->post('mode');
      if( $mode == 'edit' ){
      $where    = array(
        'table_name' => $this->input->post('table'),
				'id_tabel' => $this->input->post('value')
				);
      }
      else{
      $where    = array(
        'table_name' => $this->input->post('table'),
				'temp' => $this->input->post('value')
				);
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_attachment');
      $result = $this->db->get('attachment');
      echo json_encode($result->result_array());
		}
		
		  public function e_upload()
				{
			  if(empty($this->session->userdata('id_users'))){ echo 'error';  exit;}
			  $config['upload_path'] = './media/upload/';
					$config['allowed_types'] = '*';
					$config['max_size']	= '20000';
					$this->upload->initialize($config);
					$uploadFiles = array('img_1' => 'myfile', 'img_2' => 'e_myfile', );		
					$this->load->library('image_lib');
					$newName = '-';
					$mode = $this->input->post('mode');
					$this->form_validation->set_rules('e_remake', 'e_remake', 'required');
					if ($this->form_validation->run() == FALSE)
						{
							echo
							'
							<div class="alert alert-danger alert-dismissable">
								<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								<h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
								Mohon Keterangan Lampiran Diisi
							</div>
							';
						}
					else
						{
							foreach($uploadFiles as $key => $files)
							{
							if ($this->upload->do_upload($files)) 
								{
									$upload = $this->upload->data();
									$file = explode(".", $upload['file_name']);
									$prefix = date('Ymdhis');
									$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
									$filePath =  $upload['file_path'].$newName;
									rename($upload['full_path'],$filePath);
					  if($file[1] == 'php'){ echo 'Asem Lu'; exit;}
					  if( $mode == 'edit' ){
									$data_input = array(
										'table_name' => $this->input->get('table_name'),
										'file_name' => $newName,
										'keterangan' => $this->input->post('e_remake'),
										'id_tabel' => $this->input->post('id'),
										'temp' => $this->input->post('e_temp'),
										'uploaded_time' => date('Y-m-d H:i:s'),
										'uploaded_by' => $this->session->userdata('id_users')
										);
					  }
					  else{
									$data_input = array(
										'table_name' => $this->input->get('table_name'),
										'file_name' => $newName,
										'keterangan' => $this->input->post('e_remake'),
										'id_tabel' => $this->input->post('id'),
										'temp' => $this->input->post('e_temp'),
										'uploaded_time' => date('Y-m-d H:i:s'),
										'uploaded_by' => $this->session->userdata('id_users')
										);
					  }
									$table_name = 'attachment';
									$id         = $this->Crud_model->save_data($data_input, $table_name);
									$config['image_library'] = 'gd2';
									$config['source_image']  = './media/upload/'.$newName.'';
									$config['new_image']  = './media/upload/s_'.$newName.'';						
									$config['width']	 = 300;
									$config['height']	= 300;
									$this->image_lib->initialize($config); 
									$this->image_lib->resize();
									$file = './media/upload/s_'.$newName.'';
									$j = get_mime_by_extension($file);
									$ex = explode('/', $j);
									$e = $ex[0];
									echo
									'
									<div class="alert alert-success alert-dismissable" id="img_upload">
									<i class="fa fa-check"></i>
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									'; 
									if( $e == 'image'){
										echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"><img src="'.base_url().'media/upload/s_'.$newName.'" /></a>';
										}
									else{
										echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"> '.$newName.' </a>';
										}
									echo '</div>';
								}
							}
						}
				}
		public function load_lampiran_peraturan()
			{
		  $the_table_name = $this->input->get('table');
		  $temp = $this->input->post('temp');
				$limit    = 200;
		  $start    = 0;
		  $fields = 
					'
					id_attachment,
					table_name,
					file_name,
					keterangan,
					id_tabel,
					temp,
					uploaded_time,
					uploaded_by
					';
		  $where    = array(
			'attachment.table_name' => $the_table_name,
					'temp' => $temp
					);
		  $order_by = 'attachment.id_attachment';
				$table_name = 'attachment';
		  echo json_encode($this->Crud_model->json($where, $limit, $start, $table_name, $fields, $order_by));
			}

		public function e_load_lampiran_peraturan()
			{
		  $the_table_name = $this->input->get('table');
		  $id_tabel = $this->input->post('id');
				$limit    = 200;
		  $start    = 0;
		  $fields = 
					'
					id_attachment,
					table_name,
					file_name,
					keterangan,
					id_tabel,
					temp,
					uploaded_time,
					uploaded_by
					';
		  $where    = array(
			'attachment.table_name' => $the_table_name,
					'id_tabel' => $id_tabel
					);
		  $order_by = 'attachment.id_attachment';
				$table_name = 'attachment';
		  echo json_encode($this->Crud_model->json($where, $limit, $start, $table_name, $fields, $order_by));
			}
		
		public function delete()
		{
			$id_attachment = $this->input->get('id');
      $where = array(
        'id_attachment' => $id_attachment
        );
      $this->db->from('attachment');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $this->db->where($where);
        $this->db->delete('attachment');
        echo 1;
        }
		}

		public function hapus()
		{
			$id_attachment = $this->input->post('id_attachment');
      $where = array(
        'id_attachment' => $id_attachment,
				'uploaded_by' => $this->session->userdata('id_users')
        );
      $this->db->from('attachment');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $this->db->where($where);
        $this->db->delete('attachment');
        echo 1;
        }
		}
    public function load_html_lampiran_by_id()
    {
      $id_tabel=$this->input->post('id_tabel');
      $table_name=$this->input->post('table_name');
      $fields     = "*";
      $where      = array(
        'id_tabel' => trim($id_tabel),
        'table_name' => trim($table_name)
      );
      $this->db->where($where);
      $this->db->select("$fields");
      $b= $this->db->get('attachment');
      foreach ($b->result() as $b1) {
        $file = './media/upload/'.$b1->file_name.'';
        $file_info = new finfo(FILEINFO_MIME);  // object oriented approach!
        $mime_type = $file_info->buffer(file_get_contents($file));  // e.g. gives "image/jpeg"
        if (strpos($mime_type, 'image') !== false) {
          echo '<li id="'.$b1->id_attachment.'" id_attachment="'.$b1->id_attachment.'">';
            echo '<span class="mailbox-attachment-icon has-img"><img src="'.base_url().'media/upload/'.$b1->file_name.'" alt="Attachment"></span>';
            echo '<div class="mailbox-attachment-info">';
              echo '<a target="_blank" href="'.base_url().'media/upload/'.$b1->file_name.'" class="mailbox-attachment-name"><i class="fa fa-camera"></i> '.$b1->keterangan.'</a>';
              echo '<span class="mailbox-attachment-size">';
                echo ''.round((filesize($file))/(1024), 2).' KB';
                echo '<a id="del_ajax" target="_blank" href="#" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i></a>';
              echo '</span>';
            echo '</div>';
          echo '</li>';
        }
        else{
          echo '<li id="'.$b1->id_attachment.'" id_attachment="'.$b1->id_attachment.'">';
            echo '<span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>';
            echo '<div class="mailbox-attachment-info">';
              echo '<a target="_blank" href="'.base_url().'media/upload/'.$b1->file_name.'" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> '.$b1->keterangan.'</a>';
              echo '<span class="mailbox-attachment-size">';
                echo ''.round((filesize($file))/(1024), 2).' KB';
                echo '<a id="del_ajax" target="_blank" href="#" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i></a>';
              echo '</span>';
            echo '</div>';
          echo '</li>';
        }
      }
    }
    
  public function load_html_lampiran_by_temp()
    {
      $temp=$this->input->post('temp');
      $table_name=$this->input->post('table_name');
      $fields     = "*";
      $where      = array(
        'temp' => trim($temp),
        'table_name' => trim($table_name)
      );
      $this->db->where($where);
      $this->db->select("$fields");
      $b= $this->db->get('attachment');
      foreach ($b->result() as $b1) {
        $file = './media/upload/'.$b1->file_name.'';
        $file_info = new finfo(FILEINFO_MIME);  // object oriented approach!
        $mime_type = $file_info->buffer(file_get_contents($file));  // e.g. gives "image/jpeg"
        if (strpos($mime_type, 'image') !== false) {
          echo '<li id="'.$b1->id_attachment.'" id_attachment="'.$b1->id_attachment.'">';
            echo '<span class="mailbox-attachment-icon has-img"><img src="'.base_url().'media/upload/'.$b1->file_name.'" alt="Attachment"></span>';
            echo '<div class="mailbox-attachment-info">';
              echo '<a target="_blank" href="'.base_url().'media/upload/'.$b1->file_name.'" class="mailbox-attachment-name"><i class="fa fa-camera"></i> '.$b1->keterangan.'</a>';
              echo '<span class="mailbox-attachment-size">';
                echo ''.round((filesize($file))/(1024), 2).' KB';
                echo '<a id="del_ajax" target="_blank" href="#" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i></a>';
              echo '</span>';
            echo '</div>';
          echo '</li>';
        }
        else{
          echo '<li id="'.$b1->id_attachment.'" id_attachment="'.$b1->id_attachment.'">';
            echo '<span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>';
            echo '<div class="mailbox-attachment-info">';
              echo '<a target="_blank" href="'.base_url().'media/upload/'.$b1->file_name.'" class="mailbox-attachment-name"><i class="fa fa-paperclip"></i> '.$b1->keterangan.'</a>';
              echo '<span class="mailbox-attachment-size">';
                echo ''.round((filesize($file))/(1024), 2).' KB';
                echo '<a id="del_ajax" target="_blank" href="#" class="btn btn-danger btn-xs pull-right"><i class="fa fa-trash"></i></a>';
              echo '</span>';
            echo '</div>';
          echo '</li>';
        }
      }
    }
		
  public function upload_video()
		{
		$where = array(
              'id_tabel' => $this->session->userdata('id_users'),
              'keterangan' => $this->input->post('judul_iklan'),
							'table_name' => 'video'
              );
			
      $config['upload_path'] = './media/upload/';
			$config['allowed_types'] = '*';
			$config['max_size']	= '50000';
			$this->upload->initialize($config);
			$uploadFiles = array('img_1' => 'myfile', 'img_2' => 'e_myfile', );		
			$this->load->library('image_lib');
			$newName = '-';
			$table_name = $_GET['table_name'];
			foreach($uploadFiles as $key => $files)
				{
				if ($this->upload->do_upload($files)) 
					{
						$upload = $this->upload->data();
						$file = explode(".", $upload['file_name']);
						$prefix = date('Ymdhis');
						$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
						$filePath =  $upload['file_path'].$newName;
						rename($upload['full_path'],$filePath);
            $data_input = array(
              'table_name' => $table_name,
              'file_name' => $newName,
              'keterangan' => $this->input->post('judul_iklan'),
              'id_tabel' => $this->session->userdata('id_users'),
              'temp' => 'video',
              'uploaded_time' => date('Y-m-d H:i:s'),
              'uploaded_by' => $this->session->userdata('id_users')
              );
						$table_name = 'attachment';
						$id         = $this->Crud_model->save($data_input, $table_name);
						$config['image_library'] = 'gd2';
						$config['source_image']  = './media/upload/'.$newName.'';
						$config['new_image']  = './media/upload/s_'.$newName.'';						
						$config['width']	 = 128;
						$config['height']	= 128;
						$this->image_lib->initialize($config); 
						$this->image_lib->resize();
						$file = './media/upload/s_'.$newName.'';
						$j = get_mime_by_extension($file);
						$ex = explode('/', $j);
						$e = $ex[0];
            echo
            '
						<div class="alert alert-success alert-dismissable" id="img_upload">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						'; 
						if( $e == 'image'){
							echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"><img src="'.base_url().'media/upload/s_'.$newName.'" /></a>';
							}
						else{
							echo '<a target="_blank" href="'.base_url().'media/upload/'.$newName.'"> '.$newName.' </a>';
							}
            echo '</div>';
						echo 'OK';
					}
					
				}
				
		}
    
  public function video()
		{
      $table_name = $_GET['table_name'];
			$limit    = 200;
      $start    = 0;
      $fields = 
				'
				id_attachment,
				table_name,
				file_name,
				keterangan,
				id_tabel,
				temp,
				uploaded_time,
				uploaded_by
				';
      $where    = array(
        'attachment.id_tabel' => $this->session->userdata('id_users'),
				'table_name' => $table_name
				);
      $this->db->select("$fields");
      $this->db->where($where);
      $this->db->order_by('attachment.id_attachment', 'DESC');
      $result = $this->db->get('attachment');
      echo json_encode($result->result_array());
		}
		
		public function upload1()
			  {
			$config['upload_path'] = './media/lampiran_pendaftaran_pasar/';
			$config['allowed_types'] = 'jpg|png|pdf|jpeg';
				  $config['max_size']	= '50000';
				  $this->upload->initialize($config);
				  $uploadFiles = array('img_1' => 'myfile', 'img_2' => 'e_myfile', );		
				  $this->load->library('image_lib');
				  $newName = '-';
				  $mode = $this->input->post('mode1');
				  $this->form_validation->set_rules('remake1', 'remake1', 'required');
				  if ($this->form_validation->run() == FALSE)
					  {
						  echo
						  '
						  <div class="alert alert-danger alert-dismissable">
							  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
							  <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
							  Mohon Keterangan Lampiran Diisi
						  </div>
						  ';
					  }
				  else
					  {
						  foreach($uploadFiles as $key => $files)
						  {
						  if ($this->upload->do_upload($files)) 
							  {
								  $upload = $this->upload->data();
								  $file = explode(".", $upload['file_name']);
					$ext= ( count($file) - 1 );
								  $prefix = date('Ymdhis');
								  $newName =$prefix.'_'.$this->session->userdata('id_users').'.'. $file[$ext]; 
								  $filePath =  $upload['file_path'].$newName;
								  rename($upload['full_path'],$filePath);
								  if($file[1] == 'php'){ echo 'Asem Lu'; exit;}
								  elseif($file[1] == 'php5'){ echo 'Asem Lu'; exit;}
								  elseif($file[1] == 'php4'){ echo 'Asem Lu'; exit;}
								  elseif($file[1] == 'php3'){ echo 'Asem Lu'; exit;}
					
					
					if( $mode == 'edit' ){
								  $data_input = array(
									  'table_name' => $this->input->get('table_name'),
									  'file_name' => $newName,
									  'keterangan' => $this->input->post('remake1'),
									  'id_tabel' => 0,
									  'temp' => $this->input->post('temp1'),
									  'uploaded_time' => date('Y-m-d H:i:s'),
									  'uploaded_by' => $this->session->userdata('id_users'),
									  'id' => $this->input->get('id')
									  );
					}
					else{
								  $data_input = array(
									  'table_name' => $this->input->get('table_name'),
									  'file_name' => $newName,
									  'keterangan' => $this->input->post('remake1'),
									  'id_tabel' => 0,
									  'temp' => $this->input->post('temp1'),
									  'uploaded_time' => date('Y-m-d H:i:s'),
									  'uploaded_by' => $this->session->userdata('id_users'),
									  'id' => $this->input->get('id')
									  );
					}
								  $table_name = 'lampiran_pendaftaran_pasar';
								  $id         = $this->Crud_model->save($data_input, $table_name);
								  $config['image_library'] = 'gd2';
								  $config['source_image']  = './media/lampiran_pendaftaran_pasar/'.$newName.'';
								  $config['new_image']  = './media/lampiran_pendaftaran_pasar/k_'.$newName.'';						
								  $config['width']	 = 100;
								  $config['height']	= 100;
								  $this->image_lib->initialize($config); 
								  $this->image_lib->resize();
								  $file = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';
					
								  $config['source_image']  = './media/lampiran_pendaftaran_pasar/'.$newName.'';
								  $config['new_image']  = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';						
								  $config['width']	 = 474;
								  $config['height']	= 474;
								  $this->image_lib->initialize($config); 
								  $this->image_lib->resize();
					
								  $config['source_image']  = './media/lampiran_pendaftaran_pasar/'.$newName.'';
								  $config['new_image']  = './media/lampiran_pendaftaran_pasar/sb_'.$newName.'';						
								  $config['width']	 = 360;
								  $config['height']	= 360;
								  $this->image_lib->initialize($config); 
								  $this->image_lib->resize();
					
								  $config['source_image']  = './media/lampiran_pendaftaran_pasar/'.$newName.'';
								  $config['new_image']  = './media/lampiran_pendaftaran_pasar/b_'.$newName.'';						
								  $config['width']	 = 555;
								  $config['height']	= 555;
								  $this->image_lib->initialize($config); 
								  $this->image_lib->resize();
					
								  $config['source_image']  = './media/lampiran_pendaftaran_pasar/'.$newName.'';
								  $config['new_image']  = './media/lampiran_pendaftaran_pasar/sb_'.$newName.'';						
								  $config['width']	 = 750;
								  $config['height']	= 750;
								  $this->image_lib->initialize($config); 
								  $this->image_lib->resize();
								  //100
					//474
					//555
					
					
								  $j = get_mime_by_extension($file);
								  $ex = explode('/', $j);
								  $e = $ex[0];
								  echo
								  '
								  <div class="alert alert-success alert-dismissable" id="img_upload">
								  <i class="fa fa-check"></i>
								  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								  '; 
								  if( $e == 'image'){
									  echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"><img src="'.base_url().'media/lampiran_pendaftaran_pasar/s_'.$newName.'" /></a>';
									  }
								  else{
									  echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"> '.$newName.' </a>';
									  }
								  echo '</div>';
							  }
						  }
					  }
			  }	
    
}