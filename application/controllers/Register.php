<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	function __construct()
		{
		parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->model('Register_model');
		$this->load->model('Crud_model');
		}
		
	public function index()
	{
    
		$cek_active_login = $this->session->userdata('id_users');
		if(!empty($cek_active_login))
			{ 
				return redirect(''.base_url().'dashboard'); 
			}
		$data['judul'] = 'Register';
		$data['main_view'] = 'welcome_message';
		$this->load->view('register', $data);
	}
	
  public function simpan_register(){
   		$web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('nik', 'nik', 'required');
		$this->form_validation->set_rules('nomot_telp', 'nomot_telp', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('id_kecamatan', 'id_kecamatan', 'required');
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
	  
        $healthy = array("#", ":", "=", "!", "?", "%", "&", "[", "]", "<", ">", "+", "`", "^", "*", "'");
        $yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
        $nik = str_replace($healthy, $yummy, $this->input->post('nik'));
        $nama = str_replace($healthy, $yummy, $this->input->post('nama'));
        $nomot_telp = str_replace($healthy, $yummy, $this->input->post('nomot_telp'));
        $email = str_replace($healthy, $yummy, $this->input->post('email'));
        $alamat = str_replace($healthy, $yummy, $this->input->post('alamat'));
        $keterangan = str_replace($healthy, $yummy, $this->input->post('keterangan'));
        $id_kecamatan = $this->input->post('id_kecamatan');
        $id_desa = $this->input->post('id_desa');
		
		if ($this->form_validation->run() == FALSE)
      {
        echo 0;
      }
		else
      {
		  	$data_input = array(
				'domain' => $web,
				'nik' => $nik,
				'nama' => $nama,
				'nomot_telp' => $nomot_telp,
				'email' => $email,
				'alamat' => $alamat,
				'id_kecamatan' => $id_kecamatan,
				'id_desa' => $id_desa,
				'temp' => trim($this->input->post('temp')),
				'created_by' => 0,
				'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1,
				'keterangan' => $keterangan
				);
     		$table_name = 'register';
      $this->Register_model->simpan_register($data_input, $table_name);
      }
  }
  public function data(){
    $data['main_view'] = 'register/home';
    $this->load->view('tes', $data);
	}
	public function json_all_register(){
    $web=$this->uut->namadomain(base_url());
		$table = 'register';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    if($web=='diskominfo.wonosobokab.go.id'){
      $where = array(
        'register.status !=' => 99
        );
    }else{
      $where = array(
        'register.status !=' => 99,
        'register.domain' => $web
        );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_register="'.$row->id_register.'" id="'.$row->id_register.'" >';
				echo '<td valign="top">'.($urut).' <br /><span class="pull-right badge bg-blue">'.$row->domain.'</span><br />'.$this->Crud_model->dateBahasaIndo1($row->created_time).'</td>';
				echo '<td valign="top">'.$row->nik.'</td>';
				echo '<td valign="top">'.$row->nama.'</td>';
				// echo '<td valign="top">'.$row->email.'</td>';
				echo '<td valign="top">'.$row->nomot_telp.'</td>';
				echo '<td valign="top">'.$row->alamat.'</td>';
				echo '<td valign="top">'.$row->keterangan.'</td>';
				echo '<td valign="top">
				<a class="badge bg-warning btn-sm" href="'.base_url().'register/cetak/?id_register='.$row->id_register.'" ><i class="fa fa-print"></i> Cetak</a>
				<a href="#tab_form_register" data-toggle="tab" class="update_id_register badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax_register" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				<a href="'.base_url().'register/buat_user_register/'.$row->id_register.'" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Buat User</a>
				</td>';
				echo '</tr>';
      }
          
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='diskominfo.wonosobokab.go.id'){
      $where0 = array(
        'register.status !=' => 99
        );
    }else{
      $where0 = array(
        'register.status !=' => 99,
        'register.domain' => $web
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('register');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function hapus(){
    $web=$this->uut->namadomain(base_url());
    $id_register = $this->input->post('id_register');
    $where = array(
      'id_register' => $id_register
      );
    $this->db->from('register');
    $this->db->where($where);
    $a = $this->db->count_all_results();
    if($a == 0){
      echo 0;
    }
    else{
      $data_update = array(
      
        'status' => 99,
        'deleted_by' => $this->session->userdata('id_users')
                
        );
      $table_name  = 'register';
      $this->Register_model->update_register($data_update, $where, $table_name);
      echo 1;
    }
  }
  
	public function buat_user_register()
	{
		$data['main_view'] = 'register/buat_user_register';
		$this->load->view('back_bone', $data);
	}
  public function json_all_buat_user_register()
   {
    $web=$this->uut->namadomain(base_url());
    $id_register = $this->input->post('id_register');
    $fields     = "
                  *
                  ";
    $where      = array(
      'register.id_register' => $id_register
    );
    $this->db->select("$fields");
    $this->db->where($where);
		$this->db->limit(1);
		$result = $this->db->get('register');
		echo json_encode($result->result_array());
   }
  public function simpan_buat_user_register(){
		$this->form_validation->set_rules('nik', 'nik', 'required');
		$this->form_validation->set_rules('nomot_telp', 'nomot_telp', 'required');
    $this->form_validation->set_rules('nama', 'nama', 'required');
    $this->form_validation->set_rules('domain', 'domain', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
			{
        // $web=$this->uut->namadomain(base_url());
        $id_skpd=0;
        $id_kecamatan=0;
        $r = $this->db->query("
        SELECT * from data_skpd
        where skpd_website='".trim($this->input->post('domain'))."'
        ");
        foreach($r->result() as $h){$id_skpd = $h->id_skpd; $id_kecamatan = $h->id_kecamatan;}
				$data_input = array(
				'user_name' => trim($this->input->post('nik')),
				'password' => $this->bcrypt->hash_password($this->input->post('nomot_telp')),
				'hak_akses' => 'register',
				'nip' => '0',
				'nama' => trim($this->input->post('nama')),
				'jabatan' => '-',
				'golongan' => '-',
				'pangkat' => '-',
				'id_skpd' => $id_skpd,
				'id_propinsi' => 1,
				'id_kabupaten' => 1,
				'id_kecamatan' => trim($this->input->post('id_kecamatan')),
				'id_desa' => trim($this->input->post('id_desa')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'last_login' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'users';
      $this->Register_model->simpan_buat_user_register($data_input, $table_name);
     }
   }
}
