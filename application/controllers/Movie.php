<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Movie extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->library('Uut');
    $this->load->model('Crud_model');
  }

	public function index()
	{
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
      
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
		$data['judul'] = 'Selamat datang';
    $data['main_view'] = 'movie/home';
    $this->load->view('movie', $data);
  }

}