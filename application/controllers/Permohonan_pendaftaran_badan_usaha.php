<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Permohonan_pendaftaran_badan_usaha extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Permohonan_pendaftaran_badan_usaha_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'permohonan_pendaftaran_badan_usaha/home';
    $this->load->view('tes', $data);
   }
   
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $perihal_surat    = $this->input->post('perihal_surat');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($perihal_surat == '0'){
    $where0 = array(
      'permohonan_pendaftaran_badan_usaha.status !=' => 99,
      'permohonan_pendaftaran_badan_usaha.domain' => $web
      );
    }else{
    $where0 = array(
      'permohonan_pendaftaran_badan_usaha.status !=' => 99,
      'permohonan_pendaftaran_badan_usaha.domain' => $web,
      'permohonan_pendaftaran_badan_usaha.perihal_permohonan_pendaftaran_badan_usaha' => $perihal_surat
      );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('permohonan_pendaftaran_badan_usaha');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_permohonan_pendaftaran_badan_usaha(){
    $web=$this->uut->namadomain(base_url());
		$table = 'permohonan_pendaftaran_badan_usaha';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $perihal_surat    = $this->input->post('perihal_surat');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
		(select perihal_permohonan_pendaftaran_badan_usaha.judul_perihal_permohonan_pendaftaran_badan_usaha from perihal_permohonan_pendaftaran_badan_usaha where perihal_permohonan_pendaftaran_badan_usaha.id_perihal_permohonan_pendaftaran_badan_usaha=permohonan_pendaftaran_badan_usaha.perihal_permohonan_pendaftaran_badan_usaha) as judul_perihal_permohonan_pendaftaran_badan_usaha
    ";
    if($perihal_surat == '0'){
    $where = array(
      'permohonan_pendaftaran_badan_usaha.status !=' => 99,
      'permohonan_pendaftaran_badan_usaha.domain' => $web
      );
    }else{
    $where = array(
      'permohonan_pendaftaran_badan_usaha.status !=' => 99,
      'permohonan_pendaftaran_badan_usaha.domain' => $web,
      'permohonan_pendaftaran_badan_usaha.perihal_permohonan_pendaftaran_badan_usaha' => $perihal_surat
      );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_permohonan_pendaftaran_badan_usaha="'.$row->id_permohonan_pendaftaran_badan_usaha.'" id="'.$row->id_permohonan_pendaftaran_badan_usaha.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$row->nomor_permohonan_pendaftaran_badan_usaha.'</td>';
				echo '<td valign="top">'.$row->tanggal_permohonan_pendaftaran_badan_usaha.'</td>';
				echo '<td valign="top">'.$row->judul_perihal_permohonan_pendaftaran_badan_usaha.'</td>';
				echo '<td valign="top">'.$row->nama_pengusaha.'</td>';
				echo '<td valign="top">'.$row->nama_perusahaan.'</td>';
				echo '<td valign="top">
				<a class="badge bg-warning btn-sm" href="'.base_url().'permohonan_pendaftaran_badan_usaha/cetak/?id_permohonan_pendaftaran_badan_usaha='.$row->id_permohonan_pendaftaran_badan_usaha.'" ><i class="fa fa-print"></i> Cetak</a>
				<a href="#tab_form_permohonan_pendaftaran_badan_usaha" data-toggle="tab" class="update_id badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			echo '<tr>';
			if(($query->num_rows)>0)
			{
				echo '<td valign="top" colspan="7" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'permohonan_pendaftaran_badan_usaha/cetak_xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
			}else{
				echo'<td valign="top" colspan="7" style="text-align:right;">Data Kosong</td>';
			}
			echo '</tr>';
          
	}
  public function simpan_permohonan_pendaftaran_badan_usaha(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_permohonan_pendaftaran_badan_usaha', 'judul_permohonan_pendaftaran_badan_usaha', 'required');
		$this->form_validation->set_rules('isi_permohonan_pendaftaran_badan_usaha', 'isi_permohonan_pendaftaran_badan_usaha', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'judul_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('judul_permohonan_pendaftaran_badan_usaha')),
        'temp' => trim($this->input->post('temp')),
        'icon' => trim($this->input->post('icon')),
        'isi_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('isi_permohonan_pendaftaran_badan_usaha')),
        'keterangan' => trim($this->input->post('keterangan')),
        'nomor_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('nomor_permohonan_pendaftaran_badan_usaha')),
        'perihal_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('perihal_permohonan_pendaftaran_badan_usaha')),
        'yth_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('yth_permohonan_pendaftaran_badan_usaha')),
        'cq_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('cq_permohonan_pendaftaran_badan_usaha')),
        'di_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('di_permohonan_pendaftaran_badan_usaha')),
        'tanggal_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('tanggal_permohonan_pendaftaran_badan_usaha')),
        'nama_pengusaha' => trim($this->input->post('nama_pengusaha')),
        'tempat_lahir_pengusaha' => trim($this->input->post('tempat_lahir_pengusaha')),
        'tanggal_lahir_pengusaha' => trim($this->input->post('tanggal_lahir_pengusaha')),
        'pekerjaan_pengusaha' => trim($this->input->post('pekerjaan_pengusaha')),
        'nomor_telp_pengusaha' => trim($this->input->post('nomor_telp_pengusaha')),
        'alamat_pengusaha' => trim($this->input->post('alamat_pengusaha')),
        'id_desa_pengusaha' => trim($this->input->post('id_desa_pengusaha')),
        'id_kecamatan_pengusaha' => trim($this->input->post('id_kecamatan_pengusaha')),
        'id_kabupaten_pengusaha' => trim($this->input->post('id_kabupaten_pengusaha')),
        'id_propinsi_pengusaha' => trim($this->input->post('id_propinsi_pengusaha')),
        'nama_perusahaan' => trim($this->input->post('nama_perusahaan')),
        'id_jenis_usaha_jasa_perusahaan' => trim($this->input->post('id_jenis_usaha_jasa_perusahaan')),
        'alamat_perusahaan' => trim($this->input->post('alamat_perusahaan')),
        'id_desa_perusahaan' => trim($this->input->post('id_desa_perusahaan')),
        'id_kecamatan_perusahaan' => trim($this->input->post('id_kecamatan_perusahaan')),
        'id_kabupaten_perusahaan' => trim($this->input->post('id_kabupaten_perusahaan')),
        'id_propinsi_perusahaan' => trim($this->input->post('id_propinsi_perusahaan')),
        'nomor_telp_perusahaan' => trim($this->input->post('nomor_telp_perusahaan')),
        'daerah_domisili_pemohon' => trim($this->input->post('daerah_domisili_pemohon')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'permohonan_pendaftaran_badan_usaha';
      $id         = $this->Permohonan_pendaftaran_badan_usaha_model->simpan_permohonan_pendaftaran_badan_usaha($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'permohonan_pendaftaran_badan_usaha',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
	}
  public function update_permohonan_pendaftaran_badan_usaha(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_permohonan_pendaftaran_badan_usaha', 'judul_permohonan_pendaftaran_badan_usaha', 'required');
		$this->form_validation->set_rules('isi_permohonan_pendaftaran_badan_usaha', 'isi_permohonan_pendaftaran_badan_usaha', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'judul_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('judul_permohonan_pendaftaran_badan_usaha')),
        'isi_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('isi_permohonan_pendaftaran_badan_usaha')),
        'icon' => trim($this->input->post('icon')),
        'keterangan' => trim($this->input->post('keterangan')),
        'nomor_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('nomor_permohonan_pendaftaran_badan_usaha')),
        'perihal_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('perihal_permohonan_pendaftaran_badan_usaha')),
        'yth_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('yth_permohonan_pendaftaran_badan_usaha')),
        'cq_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('cq_permohonan_pendaftaran_badan_usaha')),
        'di_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('di_permohonan_pendaftaran_badan_usaha')),
        'tanggal_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('tanggal_permohonan_pendaftaran_badan_usaha')),
        'nama_pengusaha' => trim($this->input->post('nama_pengusaha')),
        'tempat_lahir_pengusaha' => trim($this->input->post('tempat_lahir_pengusaha')),
        'tanggal_lahir_pengusaha' => trim($this->input->post('tanggal_lahir_pengusaha')),
        'pekerjaan_pengusaha' => trim($this->input->post('pekerjaan_pengusaha')),
        'nomor_telp_pengusaha' => trim($this->input->post('nomor_telp_pengusaha')),
        'alamat_pengusaha' => trim($this->input->post('alamat_pengusaha')),
        'id_desa_pengusaha' => trim($this->input->post('id_desa_pengusaha')),
        'id_kecamatan_pengusaha' => trim($this->input->post('id_kecamatan_pengusaha')),
        'id_kabupaten_pengusaha' => trim($this->input->post('id_kabupaten_pengusaha')),
        'id_propinsi_pengusaha' => trim($this->input->post('id_propinsi_pengusaha')),
        'nama_perusahaan' => trim($this->input->post('nama_perusahaan')),
        'id_jenis_usaha_jasa_perusahaan' => trim($this->input->post('id_jenis_usaha_jasa_perusahaan')),
        'alamat_perusahaan' => trim($this->input->post('alamat_perusahaan')),
        'id_desa_perusahaan' => trim($this->input->post('id_desa_perusahaan')),
        'id_kecamatan_perusahaan' => trim($this->input->post('id_kecamatan_perusahaan')),
        'id_kabupaten_perusahaan' => trim($this->input->post('id_kabupaten_perusahaan')),
        'id_propinsi_perusahaan' => trim($this->input->post('id_propinsi_perusahaan')),
        'nomor_telp_perusahaan' => trim($this->input->post('nomor_telp_perusahaan')),
        'daerah_domisili_pemohon' => trim($this->input->post('daerah_domisili_pemohon')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'permohonan_pendaftaran_badan_usaha';
      $where       = array(
        'permohonan_pendaftaran_badan_usaha.id_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('id_permohonan_pendaftaran_badan_usaha')),
        'permohonan_pendaftaran_badan_usaha.domain' => $web
			);
      $this->Permohonan_pendaftaran_badan_usaha_model->update_data_permohonan_pendaftaran_badan_usaha($data_update, $where, $table_name);
      echo 1;
     }
  }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'permohonan_pendaftaran_badan_usaha.id_permohonan_pendaftaran_badan_usaha' => $this->input->post('id_permohonan_pendaftaran_badan_usaha'),
        'permohonan_pendaftaran_badan_usaha.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_permohonan_pendaftaran_badan_usaha');
      $result = $this->db->get('permohonan_pendaftaran_badan_usaha');
      echo json_encode($result->result_array());
		}
   
   public function total_permohonan_pendaftaran_badan_usaha()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('permohonan_pendaftaran_badan_usaha');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function cek_permohonan_pendaftaran_badan_usaha()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('master_permohonan_pendaftaran_badan_usaha');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'judul_permohonan_pendaftaran_badan_usaha' => $row->nama_master_permohonan_pendaftaran_badan_usaha,
          'domain' => $web
          );
        $this->db->from('permohonan_pendaftaran_badan_usaha');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'domain' => $web,
            'judul_permohonan_pendaftaran_badan_usaha' => $row->nama_master_permohonan_pendaftaran_badan_usaha,
            'isi_permohonan_pendaftaran_badan_usaha' => $row->isi_master_permohonan_pendaftaran_badan_usaha_default,
            'temp' => '-',
            'created_by' => 1,
            'created_time' => date('Y-m-d H:i:s'),
            'updated_by' => 1,
            'updated_time' => date('Y-m-d H:i:s'),
            'deleted_by' => 1,
            'deleted_time' => date('Y-m-d H:i:s'),
            'keterangan' => '-',
            'icon' => 'fa-home',
            'status' => 1
                    
            );
          $table_name = 'permohonan_pendaftaran_badan_usaha';
          $this->Permohonan_pendaftaran_badan_usaha_model->simpan_permohonan_pendaftaran_badan_usaha($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
  public function inaktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_permohonan_pendaftaran_badan_usaha = $this->input->post('id_permohonan_pendaftaran_badan_usaha');
      $where = array(
        'id_permohonan_pendaftaran_badan_usaha' => $id_permohonan_pendaftaran_badan_usaha
        );
      $this->db->from('permohonan_pendaftaran_badan_usaha');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'permohonan_pendaftaran_badan_usaha';
        if( $cek <> '' ){
          $this->Permohonan_pendaftaran_badan_usaha_model->update_data_permohonan_pendaftaran_badan_usaha($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_permohonan_pendaftaran_badan_usaha = $this->input->post('id_permohonan_pendaftaran_badan_usaha');
      $where = array(
        'id_permohonan_pendaftaran_badan_usaha' => $id_permohonan_pendaftaran_badan_usaha
        );
      $this->db->from('permohonan_pendaftaran_badan_usaha');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'permohonan_pendaftaran_badan_usaha';
        if( $cek <> '' ){
          $this->Permohonan_pendaftaran_badan_usaha_model->update_data_permohonan_pendaftaran_badan_usaha($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
    
  public function pdf()
  {
  $data['IconAbout']='ok';
  
  $this->load->view('permohonan_pendaftaran_badan_usaha/pdf');
  $html = $this->output->get_output();
  $this->load->library('dompdf_gen');
  $this->dompdf->load_html($html);
  $width =8.5 * 72;
  $height = 13.88 *72;
  $this->dompdf->set_paper(array(0,0,$width,$height));
  $this->dompdf->render();
  $this->dompdf->stream("cetak_permohonan_pendaftaran_badan_usaha_" . date('d_m_y') . ".pdf");
  
  }
	public function cetak_xls(){
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
			->setCreator("Budi Utomo")
			->setLastModifiedBy("Budi Utomo")
			->setTitle("Laporan I")
			->setSubject("Office 2007 XLSX Document")
			->setDescription("Laporan Permohonan Pendaftaran Badan Usaha")
			->setKeywords("Laporan Halaman")
			->setCategory("Bentuk XLS");
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Download')
			->mergeCells('A1:G1')
			->setCellValue('A2', 'Permohonan Pendaftaran Badan Usaha')
			->mergeCells('A2:G2')
			->setCellValue('A3', 'Tanggal Download : '.date('d/m/Y').' ')
			->mergeCells('A3:G3')
			
			->setCellValue('A6', 'No')
			->setCellValue('B6', 'Nomor Permohonan Pendaftaran Badan Usaha')
			->setCellValue('C6', 'Tanggal Permohonan Pendaftaran Badan Usaha')
      ;
		
		$web=$this->uut->namadomain(base_url());
		
    $table    = 'permohonan_pendaftaran_badan_usaha';
    $page    = $this->input->get('page');
    $limit    = $this->input->get('limit');
    $keyword    = $this->input->get('keyword');
    $order_by    = $this->input->get('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where      = array(
      'permohonan_pendaftaran_badan_usaha.status !=' => 99,
      'permohonan_pendaftaran_badan_usaha.status !=' => 999
    );
    $orderby   = ''.$order_by.'';
    $query = $this->Permohonan_pendaftaran_badan_usaha_model->html_all_permohonan_pendaftaran_badan_usaha($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $i = 7;
    $urut=$start;
    foreach ($query->result() as $row)
      {
        $urut=$urut+1;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i, ''.$urut.'')
        ->setCellValue('B'.$i, ''.$row->nomor_permohonan_pendaftaran_badan_usaha.'')
        ->setCellValueExplicit('C'.$i, ''.$this->Crud_model->dateBahasaIndo($row->tanggal_permohonan_pendaftaran_badan_usaha).'', PHPExcel_Cell_DataType::TYPE_STRING)
        ;
        $objPHPExcel->getActiveSheet()
        ->getStyle('A'.$i.':C'.$i.'')
        ->getBorders()->getAllBorders()
        ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
      }
		
		$objPHPExcel->getActiveSheet()->setTitle('Probable Clients');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getFill()->getStartColor()->setARGB('cccccc');
								
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="permohonan_pendaftaran_badan_usaha_'.date('ymdhis').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output'); 
          
   }
	 
  public function cetak(){
    $id_permohonan_pendaftaran_badan_usaha    = $this->input->get('id_permohonan_pendaftaran_badan_usaha');
    //$id_permohonan_pendaftaran_badan_usaha    = $this->uri->segment(3);
    $where = array(
		'id_permohonan_pendaftaran_badan_usaha' => $id_permohonan_pendaftaran_badan_usaha,
		'status !=' => 99
		);
    $d = $this->Permohonan_pendaftaran_badan_usaha_model->get_data($where);
			if(!$d){
				$data['judul_permohonan_pendaftaran_badan_usaha'] = '';
				$data['isi_permohonan_pendaftaran_badan_usaha'] = '';
				$data['perihal_permohonan_pendaftaran_badan_usaha'] = '';
				$data['nomor_permohonan_pendaftaran_badan_usaha'] = '';
				$data['yth_permohonan_pendaftaran_badan_usaha'] = '';
				$data['cq_permohonan_pendaftaran_badan_usaha'] = '';
				$data['di_permohonan_pendaftaran_badan_usaha'] = '';
				$data['tanggal_permohonan_pendaftaran_badan_usaha'] = '';
				$data['nama_pengusaha'] = '';
				$data['tempat_lahir_pengusaha'] = '';
				$data['tanggal_lahir_pengusaha'] = '';
				$data['pekerjaan_pengusaha'] = '';
				$data['nomor_telp_pengusaha'] = '';
				$data['alamat_pengusaha'] = '';
				$data['nama_desa_pengusaha'] = '';
				$data['nama_kecamatan_pengusaha'] = '';
				$data['nama_kabupaten_pengusaha'] = '';
				$data['nama_propinsi_pengusaha'] = '';
				$data['nama_perusahaan'] = '';
				$data['id_jenis_usaha_jasa_perusahaan'] = '';
				$data['alamat_perusahaan'] = '';
				$data['nama_desa_perusahaan'] = '';
				$data['nama_kecamatan_perusahaan'] = '';
				$data['nama_kabupaten_perusahaan'] = '';
				$data['nama_propinsi_perusahaan'] = '';
				$data['nomor_telp_perusahaan'] = '';
				$data['daerah_domisili_pemohon'] = '';
				$data['created_time'] = '';
				$data['pengedit'] = '';
				$data['updated_time'] = '';
			}
			else{
				$data['judul_permohonan_pendaftaran_badan_usaha'] = $d['judul_permohonan_pendaftaran_badan_usaha'];
				$data['isi_permohonan_pendaftaran_badan_usaha'] = $d['isi_permohonan_pendaftaran_badan_usaha'];
				$data['perihal_permohonan_pendaftaran_badan_usaha'] = ''.$d['perihal_permohonan_pendaftaran_badan_usaha'].' ';
				$data['nomor_permohonan_pendaftaran_badan_usaha'] = ''.$d['nomor_permohonan_pendaftaran_badan_usaha'].' ';
				$data['yth_permohonan_pendaftaran_badan_usaha'] = ''.$d['yth_permohonan_pendaftaran_badan_usaha'].' ';
				$data['cq_permohonan_pendaftaran_badan_usaha'] = ''.$d['cq_permohonan_pendaftaran_badan_usaha'].' ';
				$data['di_permohonan_pendaftaran_badan_usaha'] = ''.$d['di_permohonan_pendaftaran_badan_usaha'].' ';
				$data['tanggal_permohonan_pendaftaran_badan_usaha'] = ''.$this->Crud_model->dateBahasaIndo($d['tanggal_permohonan_pendaftaran_badan_usaha']).'';
				$data['tanggal_lahir_pengusaha'] = ''.$this->Crud_model->dateBahasaIndo($d['tanggal_lahir_pengusaha']).'';
				$data['nama_pengusaha'] = $d['nama_pengusaha'];
				$data['tempat_lahir_pengusaha'] = $d['tempat_lahir_pengusaha'];
				$data['pekerjaan_pengusaha'] = $d['pekerjaan_pengusaha'];
				$data['nomor_telp_pengusaha'] = $d['nomor_telp_pengusaha'];
				$data['alamat_pengusaha'] = $d['alamat_pengusaha'];
				$data['nama_desa_pengusaha'] = $d['nama_desa_pengusaha'];
				$data['nama_kecamatan_pengusaha'] = $d['nama_kecamatan_pengusaha'];
				$data['nama_kabupaten_pengusaha'] = $d['nama_kabupaten_pengusaha'];
				$data['nama_propinsi_pengusaha'] = $d['nama_propinsi_pengusaha'];
				$data['nama_perusahaan'] = $d['nama_perusahaan'];
				$data['id_jenis_usaha_jasa_perusahaan'] = $d['id_jenis_usaha_jasa_perusahaan'];
				$data['alamat_perusahaan'] = $d['alamat_perusahaan'];
				$data['nama_desa_perusahaan'] = $d['nama_desa_perusahaan'];
				$data['nama_kecamatan_perusahaan'] = $d['nama_kecamatan_perusahaan'];
				$data['nama_kabupaten_perusahaan'] = $d['nama_kabupaten_perusahaan'];
				$data['nama_propinsi_perusahaan'] = $d['nama_propinsi_perusahaan'];
				$data['nomor_telp_perusahaan'] = $d['nomor_telp_perusahaan'];
				$data['daerah_domisili_pemohon'] = $d['daerah_domisili_pemohon'];
				$data['created_time'] = $this->Crud_model->dateBahasaIndo1($d['created_time']);
				$data['pengedit'] = $d['pengedit'];
				$data['updated_time'] = $d['updated_time'];
			}
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'permohonan_pendaftaran_badan_usaha/cetak';
      $this->load->view('print', $data);
  }
	
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_permohonan_pendaftaran_badan_usaha = $this->input->post('id_permohonan_pendaftaran_badan_usaha');
      $where = array(
        'id_permohonan_pendaftaran_badan_usaha' => $id_permohonan_pendaftaran_badan_usaha,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('permohonan_pendaftaran_badan_usaha');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'permohonan_pendaftaran_badan_usaha';
        $this->Permohonan_pendaftaran_badan_usaha_model->update_data_permohonan_pendaftaran_badan_usaha($data_update, $where, $table_name);
        echo 1;
        }
		}
 }