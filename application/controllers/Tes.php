<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tes extends CI_Controller {

	function __construct()
		{
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Posting_model');
		}    
 
	public function index()
	{
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'tes/home';
      $this->load->view('tes', $data); 
	}
	public function slide()
	{
      $data['judul'] = 'Selamat datang';
      $this->load->view('tes_post', $data); 
	}
  
}
