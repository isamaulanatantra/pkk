<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dokumentasi extends CI_Controller
 {
    
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Dokumentasi_model');
   }
  
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'dokumentasi/home';
    $this->load->view('tes', $data);
   }
  
  public function load_the_option()
	{
		$a = 0;
		echo $this->option_dokumentasi(0,$h="", $a);
	}
  
  private function option_dokumentasi($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from dokumentasi where parent='".$parent."' and domain='".$web."' and status = 1 order by urut");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_dokumentasi.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_dokumentasi.'';
			}
			else{
			$hasil .= ''.$h->judul_dokumentasi.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_dokumentasi($h->id_dokumentasi,$hasil, $a);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
  public function load_the_option_by_posisi()
	{
    $web=$this->uut->namadomain(base_url());
    $posisi = trim($this->input->post('posisi'));
		$a = 0;
		echo $this->option_dokumentasi_by_posisi(0,$h="", $a, $posisi);
	}
  
  private function option_dokumentasi_by_posisi($parent=0,$hasil, $a, $posisi){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from dokumentasi 
                          where parent='".$parent."' 
                          and status = 1 
                          and posisi='".$posisi."'
                          and domain='".$web."'
                          order by urut
                          limit 3
                          ");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_dokumentasi.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_dokumentasi.'';
			}
			else{
			$hasil .= ''.$h->judul_dokumentasi.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_dokumentasi_by_posisi($h->id_dokumentasi,$hasil, $a, $posisi);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
  public function load_table()
	{
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		echo $this->dokumentasi(0,$h="", $a);
	}
  
  public function load_table_arsip()
	{
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		echo $this->dokumentasi_arsip(0,$h="", $a);
	}
  
  private function dokumentasi($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
	  if($web=='zb.wonosobokab.go.id'){$where_web = "";}
	  else{$where_web = "and posisi = 'menu_atas'";}
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *, (select user_name from users where users.id_users=dokumentasi.created_by) as nama_pengguna from dokumentasi
									
		where parent='".$parent."' 
		and status = 1 
		".$where_web."
		order by posisi, urut
		");
		
		if(($w->num_rows())>0)
		{
			$hasil .= '
					<ul class="timeline timeline-inverse">
			';
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
        $yummy   = array("_","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_dokumentasi);
			$hcreated_by = $this->session->userdata('id_users');
			$nomor = $nomor + 1;
			// $hasil .= '<tr id_dokumentasi="'.$h->id_dokumentasi.'" id="'.$h->id_dokumentasi.'" >';
			if($web=='zb.wonosobokab.go.id'){
			  if( $a > 1 ){
				$hasil .= '
						  <li>
							<i class="fa fa-envelope bg-blue"></i>

							<div class="timeline-item">
								  '.$h->judul_dokumentasi.'
								<i class="fa fa-clock-o"></i> '.$h->created_time.'
								<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a>
								<a class="" href="#" id="del_ajax"><i class="fa fa-cut"></i></a>
							</div>
						  </li>
					';
			  }
			  else{
				$hasil .= '
							  <li class="time-label">
								<span class="bg-info">
								  '.$h->judul_dokumentasi.'
								<i class="fa fa-clock-o"></i> '.$h->created_time.'
								<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a>
								<a class="" href="#" id="del_ajax"><i class="fa fa-cut"></i></a> 
								</span>
						  	</li>
					';
			  }
			}
			else{
			  if( $a > 1 ){
				$hasil .= '
						  <li>
							<i class="fa fa-envelope bg-blue"></i>

							<div class="timeline-item">
							  <span class="time"><i class="fa fa-clock-o"></i> '.$h->updated_time.'</span>

							  <h3 class="timeline-header"><a href="#">'.$h->judul_dokumentasi.'</a> </h3>

							  <div class="timeline-body">
								'.$h->isi_dokumentasi.'
							  </div>
							</div>
						  </li>
					';
					}
					else{
				$hasil .= '
							  <li class="time-label">
								<span class="bg-info">
								  '.$h->judul_dokumentasi.'
								<i class="fa fa-clock-o"></i> '.$h->updated_time.'
							</span>
							</div>
						  </li>
					';
					
				}
			}
			$hasil = $this->dokumentasi($h->id_dokumentasi,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			$hasil .= '</ul>';
		}
		
		return $hasil;
	}
  
  private function dokumentasi_arsip($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from dokumentasi
									
		where parent='".$parent."' 
    and domain='".$web."'
		order by posisi, urut
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<tr id_dokumentasi="'.$h->id_dokumentasi.'" id="'.$h->id_dokumentasi.'" >';
      if( $h->posisi == 'menu_kiri' ){
      $hasil .= '<td style="padding: 2px;"> Menu-Kiri </td>';
      }
      elseif( $h->posisi == 'menu_atas' ){
      $hasil .= '<td style="padding: 2px ;"> Menu-Atas </td>';
      }
      elseif( $h->posisi == 'menu_kanan' ){
      $hasil .= '<td style="padding: 2px ;"> Menu-Kanan </td>';
      }
      elseif( $h->posisi == ' independen' ){
      $hasil .= '<td style="padding: 2px ;"> Independen </td>';
      }
      else{
      $hasil .= '<td style="padding: 2px ;"> Unknown </td>';
      }
      if( $a > 1 ){
        $hasil .= '<td style="padding: 2px '.($a * 20).'px ;"> <a target="_blank" href="'.base_url().'dokumentasis/detail/'.$h->id_dokumentasi.'/'.str_replace(' ', '_', $h->judul_dokumentasi ).'.HTML">'.$h->judul_dokumentasi.' </a> </td>';
        }
			else{
        $hasil .= '<td style="padding: 2px ;"> '.$h->judul_dokumentasi.' </td>';
        }
      //$hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->urut.' </td>';
      if( $h->status == 99 ){
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> <a href="#" id="restore_ajax"><i class="fa fa-cut"></i> Restore</a> </td>';
      }
      else{
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> &nbsp; </td>';
      }
      
			$hasil .= '</tr>';
			$hasil = $this->dokumentasi_arsip($h->id_dokumentasi,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
   
  public function simpan_dokumentasi()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_dokumentasi', 'judul_dokumentasi', 'required');
		$this->form_validation->set_rules('highlight', 'highlight', 'required');
		$this->form_validation->set_rules('tampil_menu', 'tampil_menu', 'required');
		$this->form_validation->set_rules('pilih_tampil_menu_atas', 'pilih_tampil_menu_atas', 'required');
    $this->form_validation->set_rules('informasi_berkala', 'informasi_berkala', 'required');
    $this->form_validation->set_rules('informasi_serta_merta', 'informasi_serta_merta', 'required');
    $this->form_validation->set_rules('informasi_setiap_saat', 'informasi_setiap_saat', 'required');
    $this->form_validation->set_rules('informasi_dikecualikan', 'informasi_dikecualikan', 'required');
		$this->form_validation->set_rules('parent', 'parent', 'required');
		$this->form_validation->set_rules('urut', 'urut', 'required');
		$this->form_validation->set_rules('posisi', 'posisi', 'required');
		$this->form_validation->set_rules('icon', 'icon', 'required');
		$this->form_validation->set_rules('kata_kunci', 'kata_kunci', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('isi_dokumentasi', 'isi_dokumentasi', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'judul_dokumentasi' => trim($this->input->post('judul_dokumentasi')),
				'highlight' => trim($this->input->post('highlight')),
				'tampil_menu' => trim($this->input->post('tampil_menu')),
				'tampil_menu_atas' => trim($this->input->post('pilih_tampil_menu_atas')),
				'parent' => trim($this->input->post('parent')),
				'kata_kunci' => trim(str_replace('.', '', str_replace(',', '', $this->input->post('kata_kunci')))),
        'temp' => trim($this->input->post('temp')),
        'urut' => trim($this->input->post('urut')),
        'posisi' => trim($this->input->post('posisi')),
        'icon' => trim($this->input->post('icon')),
        'isi_dokumentasi' => trim($this->input->post('isi_dokumentasi')),
        'keterangan' => trim($this->input->post('keterangan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
				'informasi_berkala' => trim($this->input->post('informasi_berkala')),
				'informasi_serta_merta' => trim($this->input->post('informasi_serta_merta')),
				'informasi_setiap_saat' => trim($this->input->post('informasi_setiap_saat')),
				'informasi_dikecualikan' => trim($this->input->post('informasi_dikecualikan')),
        'status' => 1

				);
      $table_name = 'dokumentasi';
      $id         = $this->Dokumentasi_model->simpan_dokumentasi($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'dokumentasi',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
   
  public function update_dokumentasi()
   {
    $web=$this->uut->namadomain(base_url());
    $this->form_validation->set_rules('judul_dokumentasi', 'judul_dokumentasi', 'required');
    $this->form_validation->set_rules('highlight', 'highlight', 'required');
    $this->form_validation->set_rules('tampil_menu', 'tampil_menu', 'required');
    $this->form_validation->set_rules('pilih_tampil_menu_atas', 'pilih_tampil_menu_atas', 'required');
    $this->form_validation->set_rules('informasi_berkala', 'informasi_berkala', 'required');
    $this->form_validation->set_rules('informasi_serta_merta', 'informasi_serta_merta', 'required');
    $this->form_validation->set_rules('informasi_setiap_saat', 'informasi_setiap_saat', 'required');
    $this->form_validation->set_rules('informasi_dikecualikan', 'informasi_dikecualikan', 'required');
		$this->form_validation->set_rules('urut', 'urut', 'required');
		$this->form_validation->set_rules('posisi', 'posisi', 'required');
		$this->form_validation->set_rules('icon', 'icon', 'required');
		$this->form_validation->set_rules('kata_kunci', 'kata_kunci', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
				'judul_dokumentasi' => trim($this->input->post('judul_dokumentasi')),
				'highlight' => trim($this->input->post('highlight')),
				'tampil_menu' => trim($this->input->post('tampil_menu')),
				'tampil_menu_atas' => trim($this->input->post('pilih_tampil_menu_atas')),
				'informasi_berkala' => trim($this->input->post('informasi_berkala')),
				'informasi_serta_merta' => trim($this->input->post('informasi_serta_merta')),
				'informasi_setiap_saat' => trim($this->input->post('informasi_setiap_saat')),
				'informasi_dikecualikan' => trim($this->input->post('informasi_dikecualikan')),
				'parent' => trim($this->input->post('parent')),
				'kata_kunci' => trim(str_replace('.', '', str_replace(',', '', $this->input->post('kata_kunci')))),
        'temp' => trim($this->input->post('temp')),
        'urut' => trim($this->input->post('urut')),
        'posisi' => trim($this->input->post('posisi')),
        'icon' => trim($this->input->post('icon')),
        'isi_dokumentasi' => trim($this->input->post('isi_dokumentasi')),
        'keterangan' => trim($this->input->post('keterangan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'dokumentasi';
      $where       = array(
        'dokumentasi.id_dokumentasi' => trim($this->input->post('id_dokumentasi')),
        'dokumentasi.domain' => $web
			);
      $this->Dokumentasi_model->update_data_dokumentasi($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'id_dokumentasi' => $this->input->post('id_dokumentasi'),
        'dokumentasi.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_dokumentasi');
      $result = $this->db->get('dokumentasi');
      echo json_encode($result->result_array());
		}
   
   public function total_dokumentasi()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $this->db->from('dokumentasi');
      $where    = array(
        'status' => 1,
        'domain' => $web
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dokumentasi = $this->input->post('id_dokumentasi');
      $where = array(
        'id_dokumentasi' => $id_dokumentasi,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('dokumentasi');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'dokumentasi';
        $this->Dokumentasi_model->update_data_dokumentasi($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function restore()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dokumentasi = $this->input->post('id_dokumentasi');
      $where = array(
        'id_dokumentasi' => $id_dokumentasi,
        'domain' => $web
        );
      $this->db->from('dokumentasi');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'dokumentasi';
        $this->Dokumentasi_model->update_data_dokumentasi($data_update, $where, $table_name);
        echo 1;
        }
		}
  
  
  public function inaktifkan_menu_atas()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dokumentasi = $this->input->post('id_dokumentasi');
      $where = array(
        'id_dokumentasi' => $id_dokumentasi,
        'domain' => $web
        );
      $this->db->from('dokumentasi');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'tampil_menu_atas' => 0
                  
          );
        $table_name  = 'dokumentasi';
        $this->Dokumentasi_model->update_data_dokumentasi($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function aktifkan_menu_atas()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dokumentasi = $this->input->post('id_dokumentasi');
      $where = array(
        'id_dokumentasi' => $id_dokumentasi,
        'domain' => $web
        );
      $this->db->from('dokumentasi');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'tampil_menu_atas' => 1
                  
          );
        $table_name  = 'dokumentasi';
        $this->Dokumentasi_model->update_data_dokumentasi($data_update, $where, $table_name);
        echo 1;
        }
		}
	
  public function inaktifkan_highlight()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dokumentasi = $this->input->post('id_dokumentasi');
      $where = array(
        'id_dokumentasi' => $id_dokumentasi,
        'domain' => $web
        );
      $this->db->from('dokumentasi');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'highlight' => 0
                  
          );
        $table_name  = 'dokumentasi';
        $this->Dokumentasi_model->update_data_dokumentasi($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function aktifkan_highlight()
		{
      $web=$this->uut->namadomain(base_url());
			$id_dokumentasi = $this->input->post('id_dokumentasi');
      $where = array(
        'id_dokumentasi' => $id_dokumentasi,
        'domain' => $web
        );
      $this->db->from('dokumentasi');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'highlight' => 1
                  
          );
        $table_name  = 'dokumentasi';
        $this->Dokumentasi_model->update_data_dokumentasi($data_update, $where, $table_name);
        echo 1;
        }
		}
 }