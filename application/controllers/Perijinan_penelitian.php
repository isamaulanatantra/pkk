<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Perijinan_penelitian extends CI_Controller
 {
    
  function __construct(){
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->library('Crud_model');
    $this->load->model('Perijinan_penelitian_model');
   }
  
  public function index(){
    $data['total'] = 10;
    $data['main_view'] = 'perijinan_penelitian/home';
    $this->load->view('tes', $data);
   }
  
  public function simpan_perijinan_penelitian(){
   		$web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('nomot_telp', 'nomot_telp', 'required');
		$this->form_validation->set_rules('kebangsaan', 'kebangsaan', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('pekerjaan', 'pekerjaan', 'required');
		$this->form_validation->set_rules('penanggung_jawab', 'penanggung_jawab', 'required');
		$this->form_validation->set_rules('lokasi', 'lokasi', 'required');
		$this->form_validation->set_rules('judul_penelitian', 'judul_penelitian', 'required');
		$this->form_validation->set_rules('asal_universitas', 'asal_universitas', 'required');
		$this->form_validation->set_rules('nomor_surat_kesbangpol', 'nomor_surat_kesbangpol', 'required');
		$this->form_validation->set_rules('tanggal_surat_kesbangpol', 'tanggal_surat_kesbangpol', 'required');
		$this->form_validation->set_rules('surat_ditujukan_kepada', 'surat_ditujukan_kepada', 'required');
		$this->form_validation->set_rules('tembusan_kepada_1', 'tembusan_kepada_1', 'required');
		$this->form_validation->set_rules('tembusan_kepada_2', 'tembusan_kepada_2', 'required');
		$this->form_validation->set_rules('tembusan_kepada_3', 'tembusan_kepada_3', 'required');
		$this->form_validation->set_rules('tembusan_kepada_4', 'tembusan_kepada_4', 'required');
		$this->form_validation->set_rules('tembusan_kepada_5', 'tembusan_kepada_5', 'required');
		$this->form_validation->set_rules('tembusan_kepada_6', 'tembusan_kepada_6', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
	  
        $healthy = array("#", ":", "=", "!", "?", "@", "%", "&", "[", "]", "<", ">", "+", "`", "^", "*", "'");
        $yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
        $nama = str_replace($healthy, $yummy, $this->input->post('nama'));
        $nomot_telp = str_replace($healthy, $yummy, $this->input->post('nomot_telp'));
        $kebangsaan = str_replace($healthy, $yummy, $this->input->post('kebangsaan'));
        $alamat = str_replace($healthy, $yummy, $this->input->post('alamat'));
        $pekerjaan = str_replace($healthy, $yummy, $this->input->post('pekerjaan'));
        $penanggung_jawab = str_replace($healthy, $yummy, $this->input->post('penanggung_jawab'));
        $lokasi = str_replace($healthy, $yummy, $this->input->post('lokasi'));
        $judul_penelitian = str_replace($healthy, $yummy, $this->input->post('judul_penelitian'));
        $asal_universitas = str_replace($healthy, $yummy, $this->input->post('asal_universitas'));
        $nomor_surat_kesbangpol = str_replace($healthy, $yummy, $this->input->post('nomor_surat_kesbangpol'));
        $tanggal_surat_kesbangpol = str_replace($healthy, $yummy, $this->input->post('tanggal_surat_kesbangpol'));
        $surat_ditujukan_kepada = str_replace($healthy, $yummy, $this->input->post('surat_ditujukan_kepada'));
        $tembusan_kepada_1 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_1'));
        $tembusan_kepada_2 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_2'));
        $tembusan_kepada_3 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_3'));
        $tembusan_kepada_4 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_4'));
        $tembusan_kepada_5 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_5'));
        $tembusan_kepada_6 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_6'));
		
		if ($this->form_validation->run() == FALSE)
		 {
		  echo 0;
		 }
		else
		 {
		  	$data_input = array(
        		'domain' => $web,
				'nomor_surat' => '-',
				'perihal_surat' => '-',
				'tanggal_surat' => date('Y-m-d'),
				'nama' => $nama,
				'nomot_telp' => $nomot_telp,
				'kebangsaan' => $kebangsaan,
				'alamat' => $alamat,
				'pekerjaan' => $pekerjaan,
				'penanggung_jawab' => $penanggung_jawab,
				'lokasi' => $lokasi,
				'judul_penelitian' => $judul_penelitian,
				'asal_universitas' => $asal_universitas,
				'nomor_surat_kesbangpol' => $nomor_surat_kesbangpol,
				'tanggal_surat_kesbangpol' => $tanggal_surat_kesbangpol,
				'surat_ditujukan_kepada' => $surat_ditujukan_kepada,
				'tembusan_kepada_1' => $tembusan_kepada_1,
				'tembusan_kepada_2' => $tembusan_kepada_2,
				'tembusan_kepada_3' => $tembusan_kepada_3,
				'tembusan_kepada_4' => $tembusan_kepada_4,
				'tembusan_kepada_5' => $tembusan_kepada_5,
				'tembusan_kepada_6' => $tembusan_kepada_6,
				'temp' => trim($this->input->post('temp')),
				'created_by' => 0,
				'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 0
				);
     		$table_name = 'perijinan_penelitian';
      		$id = $this->Perijinan_penelitian_model->simpan_perijinan_penelitian($data_input, $table_name);
      		echo $id;
			$table_name  = 'lampiran_perijinan_penelitian';
		  	$where       = array(
				'table_name' => 'perijinan_penelitian',
				'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      	$this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
   
  public function update_perijinan_penelitian(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		//$this->form_validation->set_rules('nomor_surat', 'nomor_surat', 'required');
		$this->form_validation->set_rules('perihal_surat', 'perihal_surat', 'required');
		$this->form_validation->set_rules('tanggal_surat', 'tanggal_surat', 'required');
		$this->form_validation->set_rules('nomot_telp', 'nomot_telp', 'required');
		$this->form_validation->set_rules('kebangsaan', 'kebangsaan', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('pekerjaan', 'pekerjaan', 'required');
		$this->form_validation->set_rules('penanggung_jawab', 'penanggung_jawab', 'required');
		$this->form_validation->set_rules('lokasi', 'lokasi', 'required');
		$this->form_validation->set_rules('judul_penelitian', 'judul_penelitian', 'required');
		$this->form_validation->set_rules('asal_universitas', 'asal_universitas', 'required');
		$this->form_validation->set_rules('nomor_surat_kesbangpol', 'nomor_surat_kesbangpol', 'required');
		$this->form_validation->set_rules('tanggal_surat_kesbangpol', 'tanggal_surat_kesbangpol', 'required');
		$this->form_validation->set_rules('surat_ditujukan_kepada', 'surat_ditujukan_kepada', 'required');
		$this->form_validation->set_rules('tembusan_kepada_1', 'tembusan_kepada_1', 'required');
		$this->form_validation->set_rules('tembusan_kepada_2', 'tembusan_kepada_2', 'required');
		$this->form_validation->set_rules('tembusan_kepada_3', 'tembusan_kepada_3', 'required');
		$this->form_validation->set_rules('tembusan_kepada_4', 'tembusan_kepada_4', 'required');
		$this->form_validation->set_rules('tembusan_kepada_5', 'tembusan_kepada_5', 'required');
		$this->form_validation->set_rules('tembusan_kepada_6', 'tembusan_kepada_6', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('header_ttd', 'header_ttd', 'required');
		$this->form_validation->set_rules('jabatan_ttd', 'jabatan_ttd', 'required');
		$this->form_validation->set_rules('nama_ttd', 'nama_ttd', 'required');
		$this->form_validation->set_rules('jabatan2_ttd', 'jabatan2_ttd', 'required');
		$this->form_validation->set_rules('nip_ttd', 'nip_ttd', 'required');
	 
	 if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
		'id_perijinan_penelitian' => trim($this->input->post('id_perijinan_penelitian')),
		'nama' => trim($this->input->post('nama')),
		'nomor_surat' => trim($this->input->post('nomor_surat')),
		'perihal_surat' => trim($this->input->post('perihal_surat')),
		'tanggal_surat' => trim($this->input->post('tanggal_surat')),
		'nomot_telp' => trim($this->input->post('nomot_telp')),
		'kebangsaan' => trim($this->input->post('kebangsaan')),
		'alamat' => trim($this->input->post('alamat')),
		'pekerjaan' => trim($this->input->post('pekerjaan')),
		'penanggung_jawab' => trim($this->input->post('penanggung_jawab')),
		'lokasi' => trim($this->input->post('lokasi')),
        'temp' => trim($this->input->post('temp')),
        'judul_penelitian' => trim($this->input->post('judul_penelitian')),
        'asal_universitas' => trim($this->input->post('asal_universitas')),
        'nomor_surat_kesbangpol' => trim($this->input->post('nomor_surat_kesbangpol')),
        'tanggal_surat_kesbangpol' => trim($this->input->post('tanggal_surat_kesbangpol')),
        'surat_ditujukan_kepada' => trim($this->input->post('surat_ditujukan_kepada')),
        'tembusan_kepada_1' => trim($this->input->post('tembusan_kepada_1')),
        'tembusan_kepada_2' => trim($this->input->post('tembusan_kepada_2')),
        'tembusan_kepada_3' => trim($this->input->post('tembusan_kepada_3')),
        'tembusan_kepada_4' => trim($this->input->post('tembusan_kepada_4')),
        'tembusan_kepada_5' => trim($this->input->post('tembusan_kepada_5')),
        'tembusan_kepada_6' => trim($this->input->post('tembusan_kepada_6')),
        'judul_ttd' => trim($this->input->post('header_ttd')),
        'jabatan_ttd' => trim($this->input->post('jabatan_ttd')),
        'nama_ttd' => trim($this->input->post('nama_ttd')),
        'jabatan2_ttd' => trim($this->input->post('jabatan2_ttd')),
        'nip_ttd' => trim($this->input->post('nip_ttd')),
		'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'perijinan_penelitian';
      $where       = array(
        'perijinan_penelitian.id_perijinan_penelitian' => trim($this->input->post('id_perijinan_penelitian')),
        'perijinan_penelitian.domain' => $web
			);
      $this->Perijinan_penelitian_model->update_data_perijinan_penelitian($data_update, $where, $table_name);
      echo 1;
     }
   }
   
  function load_table(){
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("SELECT * from perijinan_penelitian 
                          where status != 99 
                          and domain='".$web."'
                          order by id_perijinan_penelitian DESC
                          ");
		if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			echo '
				<tr id_perijinan_penelitian="'.$h->id_perijinan_penelitian.'" id="'.$h->id_perijinan_penelitian.'">
					<td style="padding: 2px;">
						<a href="#" id="verif_ajax" class="btn btn-success btn-md">Verifikasi</a>
						<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="btn btn-info btn-md">Edit Data</i></a><br>
						<a href="'.base_url().'perijinan_penelitian/cetak/'.$h->id_perijinan_penelitian.'" class="btn btn-primary btn-md">Cetak Surat</a>
						<a href="'.base_url().'perijinan_penelitian/cetak_survey/'.$h->id_perijinan_penelitian.'" class="btn btn-warning btn-md">Cetak Survey </a>
						<a href="#" id="del_ajax"><i class="btn btn-danger btn-md">Hapus Data</i></a> 

						
					</td>
					<td style="padding: 2px;">
						<p>';
						if($h->status==0){echo'<span class="bg-warning">Menunggu Verifikasi</span>';}
						elseif($h->status==1){echo'<span class="bg-success">Terverifikasi</span>';}
						elseif($h->status==2){echo'<span class="bg-success">Sudah Dibuat</span>';}
						else{echo'<span class="bg-warning">Something Wrong</span>';}
						echo'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nomor_surat.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->perihal_surat.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->tanggal_surat.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nama.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->kebangsaan.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->alamat.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->pekerjaan.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->penanggung_jawab.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->lokasi.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->judul_penelitian.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->asal_universitas.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nomor_surat_kesbangpol.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->tanggal_surat_kesbangpol.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->surat_ditujukan_kepada.'</p>
					</td>
				</tr>
			';
		}
	}
  
  function load_table_perijinan_penelitian6(){
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("SELECT * from perijinan_penelitian 
                          where status != 0
                          and domain='".$web."'
                          order by id_perijinan_penelitian DESC
                          ");
		if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			$a++;
			echo '
				<tr id_perijinan_penelitian="'.$h->id_perijinan_penelitian.'" id="'.$h->id_perijinan_penelitian.'">
					<td style="padding: 2px;">
						'.$a.'
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nama.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->judul_penelitian.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->lokasi.'</p>
					</td>
					<td style="padding: 2px;">
						<p>';
						if($h->status==0){echo'<span class="bg-warning">Menunggu Verifikasi</span>';}
						elseif($h->status==1){echo'<span class="bg-success">Terverifikasi</span>';}
						elseif($h->status==2){echo'<span class="bg-success">Sudah Dibuat</span>';}
						else{echo'<span class="bg-warning">Something Wrong</span>';}
						echo'</p>
					</td>
					<td>
						<a href="'.base_url().'perijinan_penelitian/cetak/'.$h->id_perijinan_penelitian.'" class="btn btn-success btn-md">Cetak</a>
						

					</td>
				</tr>
			';
		}
	}
	//<a href="#" id="selesai_ajax" class="btn btn-danger btn-sm">Selesai</a>

	function load_table_perijinan_penelitian8(){
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("SELECT * from perijinan_penelitian 
                          where status != 99
                          and domain='".$web."'
                          order by id_perijinan_penelitian DESC
                          ");
		if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			$a++;
			echo '
				<tr id_perijinan_penelitian="'.$h->id_perijinan_penelitian.'" id="'.$h->id_perijinan_penelitian.'">
					<td style="padding: 2px;">
						'.$a.'
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nama.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->judul_penelitian.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->lokasi.'</p>
					</td>
					<td style="padding: 2px;">
						<p>';
						if($h->status==0){echo'<span class="bg-warning">Menunggu Verifikasi</span>';}
						elseif($h->status==1){echo'<span class="bg-success">Terverifikasi</span>';}
						elseif($h->status==2){echo'<span class="bg-success">Sudah Dibuat</span>';}
						else{echo'<span class="bg-warning">Something Wrong</span>';}
						echo'</p>
					</td>					
				</tr>
			';
		}
	}

	function load_table_perijinan_penelitian7(){
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("SELECT * from perijinan_penelitian 
                          where status = 2 and status=99
                          and domain='".$web."'
                          order by id_perijinan_penelitian DESC
                          ");
		if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			$a++;
			echo '
				<tr id_perijinan_penelitian="'.$h->id_perijinan_penelitian.'" id="'.$h->id_perijinan_penelitian.'">
					<td style="padding: 2px;">
						'.$a.'
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nama.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->judul_penelitian.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->lokasi.'</p>
					</td>
					<td style="padding: 2px;">
						<p>';
						if($h->status==0){echo'<span class="bg-warning">Menunggu Verifikasi</span>';}
						elseif($h->status==1){echo'<span class="bg-success">Terverifikasi</span>';}
						elseif($h->status==2){echo'<span class="bg-success">Sudah Dibuat</span>';}
						elseif($h->status==99){echo'<span class="bg-success">Dihapus</span>';}
						else{echo'<span class="bg-warning">Something Wrong</span>';}
						echo'</p>
					</td>					
				</tr>
			';
		}
	}
  
  public function cetak(){
    $where = array(
		'id_perijinan_penelitian' => $this->uri->segment(3),
		'status' => 1
		);
    $d = $this->Perijinan_penelitian_model->get_data($where);
    if(!$d){
			$data['nama'] = '';
			$data['nomor_surat'] = '';
			$data['perihal_surat'] = '';
			$data['tanggal_surat'] = '';
			$data['nomot_telp'] = '';
			$data['kebangsaan'] = '';
			$data['alamat'] = '';
			$data['pekerjaan'] = '';
			$data['penanggung_jawab'] = '';
			$data['lokasi'] = '';
			$data['judul_penelitian'] = '';
			$data['asal_universitas'] = '';
			$data['nomor_surat_kesbangpol'] = '';
			$data['tanggal_surat_kesbangpol'] = '';
			$data['surat_ditujukan_kepada'] = '';
			$data['tembusan_kepada_1'] = '';
			$data['tembusan_kepada_2'] = '';
			$data['tembusan_kepada_3'] = '';
			$data['tembusan_kepada_4'] = '';
			$data['tembusan_kepada_5'] = '';
			$data['tembusan_kepada_6'] = '';
			$data['judul_ttd'] = '';
			$data['jabatan_ttd'] = '';
			$data['nama_ttd'] = '';
			$data['jabatan2_ttd'] = '';
			$data['nip_ttd'] = '';
			$data['pembuat'] = '';
			$data['created_time'] = '';
			$data['pengedit'] = '';
			$data['updated_time'] = '';
          }
        else{
			$data['nama'] = $d['nama'];
			$data['nomor_surat'] = $d['nomor_surat'];
			$data['perihal_surat'] = ''.$d['perihal_surat'].' ';
			//$data['tanggal_surat'] = $d['tanggal_surat'];
			// $date = date("Y-m-d");
			$data['tanggal_surat'] = ''.$this->Crud_model->dateBahasaIndo(date("Y-m-d")).'';
            $data['nomot_telp'] = $d['nomot_telp'];
            $data['kebangsaan'] = $d['kebangsaan'];
            $data['alamat1'] = $d['alamat'];
            $data['pekerjaan'] = $d['pekerjaan'];
            $data['penanggung_jawab'] = $d['penanggung_jawab'];
			$data['lokasi'] = $d['lokasi'];
			$data['judul_penelitian'] = $d['judul_penelitian'];
			$data['asal_universitas'] = $d['asal_universitas'];
			$data['nomor_surat_kesbangpol'] = $d['nomor_surat_kesbangpol'];
			// $data['tanggal_surat_kesbangpol'] = $d['tanggal_surat_kesbangpol'];
			$data['tanggal_surat_kesbangpol'] = $this->Crud_model->dateBahasaIndo($d['tanggal_surat_kesbangpol']);
			$data['surat_ditujukan_kepada'] = $d['surat_ditujukan_kepada'];
			$data['tembusan_kepada_1'] = $d['tembusan_kepada_1'];
			$data['tembusan_kepada_2'] = $d['tembusan_kepada_2'];
			$data['tembusan_kepada_3'] = $d['tembusan_kepada_3'];
			$data['tembusan_kepada_4'] = $d['tembusan_kepada_4'];
			$data['tembusan_kepada_5'] = $d['tembusan_kepada_5'];
			$data['tembusan_kepada_6'] = $d['tembusan_kepada_6'];
			$data['judul_ttd'] = $d['judul_ttd'];
			$data['jabatan_ttd'] = $d['jabatan_ttd'];
			$data['nama_ttd'] = $d['nama_ttd'];
			$data['jabatan2_ttd'] = $d['jabatan2_ttd'];
			$data['nip_ttd'] = $d['nip_ttd'];
			$data['pembuat'] = $d['pembuat'];
			$data['created_time'] = $this->Crud_model->dateBahasaIndo1($d['created_time']);
			$data['pengedit'] = $d['pengedit'];
			$data['updated_time'] = $d['updated_time'];
          }
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'perijinan_penelitian/cetak';
      $this->load->view('print', $data);
  }
  private function perijinan_penelitian($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from perijinan_penelitian 
                          where status = 1 
                          and domain='".$web."'
                          order by id_perijinan_penelitian DESC
                          ");
		if(($w->num_rows())>0){
		} 
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			if( $a > 1 ){
			$hasil =  '
				<tr>                    
					<td style="padding: 2px '.($a * 20).'px ;">
						<span><i class="fa fa-user"></i> <a href="#">'.$h->nama.' </a></span>
						<p>'.$h->alamat.'</p>
					</td>
				</tr>
			';
			  }
			else{
				$hasil =  '
					<tr>                    
						<td>
						<span><i class="fa fa-user"></i> '.$h->nama.' </span>
							<p>'.$h->alamat.'</p>
						</td>
					</tr>
				';
			}
			$hasil = $this->perijinan_penelitian($h->id_perijinan_penelitian,$hasil, $a, $id_posting);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'id_perijinan_penelitian' => $this->input->post('id_perijinan_penelitian'),
        'perijinan_penelitian.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_perijinan_penelitian');
      $result = $this->db->get('perijinan_penelitian');
      echo json_encode($result->result_array());
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_perijinan_penelitian = $this->input->post('id_perijinan_penelitian');
      $where = array(
        'id_perijinan_penelitian' => $id_perijinan_penelitian,
        'domain' => $web
        );
      $this->db->from('perijinan_penelitian');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          	'status' => 99,
			'updated_by' => $this->session->userdata('id_users'),
                  
          );
        $table_name  = 'perijinan_penelitian';
        $this->Perijinan_penelitian_model->update_data_perijinan_penelitian($data_update, $where, $table_name);
        echo 1;
        }
	}

	public function verifikasi()
		{
      $web=$this->uut->namadomain(base_url());
			$id_perijinan_penelitian = $this->input->post('id_perijinan_penelitian');
      $where = array(
        'id_perijinan_penelitian' => $id_perijinan_penelitian,
        'domain' => $web
        );
      $this->db->from('perijinan_penelitian');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          	'status' => 1,
			'updated_by' => $this->session->userdata('id_users'),
			'updated_time' => date('Y-m-d H:i:s')
                  
          );
        $table_name  = 'perijinan_penelitian';
        $this->Perijinan_penelitian_model->update_data_perijinan_penelitian($data_update, $where, $table_name);
        echo 1;
        }
	}

	public function selesai()
		{
      $web=$this->uut->namadomain(base_url());
			$id_perijinan_penelitian = $this->input->post('id_perijinan_penelitian');
      $where = array(
        'id_perijinan_penelitian' => $id_perijinan_penelitian,
        'domain' => $web
        );
      $this->db->from('perijinan_penelitian');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 1){
        echo 0;
        }
      else{
        $data_update = array(
        
          	'status' => 2,
			'updated_by' => $this->session->userdata('id_users'),
			'updated_time' => date('Y-m-d H:i:s')
                  
          );
        $table_name  = 'perijinan_penelitian';
        $this->Perijinan_penelitian_model->update_data_perijinan_penelitian($data_update, $where, $table_name);
        echo 1;
        }
	}

	public function simpan_perijinan_penelitian_sementara(){
   		$web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('perihal_surat', 'perihal_surat', 'required');
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('nomot_telp', 'nomot_telp', 'required');
		$this->form_validation->set_rules('kebangsaan', 'kebangsaan', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('pekerjaan', 'pekerjaan', 'required');
		$this->form_validation->set_rules('penanggung_jawab', 'penanggung_jawab', 'required');
		$this->form_validation->set_rules('lokasi', 'lokasi', 'required');
		$this->form_validation->set_rules('judul_penelitian', 'judul_penelitian', 'required');
		$this->form_validation->set_rules('asal_universitas', 'asal_universitas', 'required');
		$this->form_validation->set_rules('nomor_surat_kesbangpol', 'nomor_surat_kesbangpol', 'required');
		$this->form_validation->set_rules('tanggal_surat_kesbangpol', 'tanggal_surat_kesbangpol', 'required');
		$this->form_validation->set_rules('surat_ditujukan_kepada', 'surat_ditujukan_kepada', 'required');
		$this->form_validation->set_rules('tembusan_kepada_1', 'tembusan_kepada_1', 'required');
		$this->form_validation->set_rules('tembusan_kepada_2', 'tembusan_kepada_2', 'required');
		$this->form_validation->set_rules('tembusan_kepada_3', 'tembusan_kepada_3', 'required');
		$this->form_validation->set_rules('tembusan_kepada_4', 'tembusan_kepada_4', 'required');
		$this->form_validation->set_rules('tembusan_kepada_5', 'tembusan_kepada_5', 'required');
		$this->form_validation->set_rules('tembusan_kepada_6', 'tembusan_kepada_6', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
	  
        $healthy = array("#", ":", "=", "!", "?", "@", "%", "&", "[", "]", "<", ">", "+", "`", "^", "*", "'");
        $yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
        $perihal_surat = str_replace($healthy, $yummy, $this->input->post('perihal_surat'));
        $nama = str_replace($healthy, $yummy, $this->input->post('nama'));
        $nomot_telp = str_replace($healthy, $yummy, $this->input->post('nomot_telp'));
        $kebangsaan = str_replace($healthy, $yummy, $this->input->post('kebangsaan'));
        $alamat = str_replace($healthy, $yummy, $this->input->post('alamat'));
        $pekerjaan = str_replace($healthy, $yummy, $this->input->post('pekerjaan'));
        $penanggung_jawab = str_replace($healthy, $yummy, $this->input->post('penanggung_jawab'));
        $lokasi = str_replace($healthy, $yummy, $this->input->post('lokasi'));
        $judul_penelitian = str_replace($healthy, $yummy, $this->input->post('judul_penelitian'));
        $asal_universitas = str_replace($healthy, $yummy, $this->input->post('asal_universitas'));
        $nomor_surat_kesbangpol = str_replace($healthy, $yummy, $this->input->post('nomor_surat_kesbangpol'));
        $tanggal_surat_kesbangpol = str_replace($healthy, $yummy, $this->input->post('tanggal_surat_kesbangpol'));
        $surat_ditujukan_kepada = str_replace($healthy, $yummy, $this->input->post('surat_ditujukan_kepada'));
        $tembusan_kepada_1 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_1'));
        $tembusan_kepada_2 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_2'));
        $tembusan_kepada_3 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_3'));
        $tembusan_kepada_4 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_4'));
        $tembusan_kepada_5 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_5'));
        $tembusan_kepada_6 = str_replace($healthy, $yummy, $this->input->post('tembusan_kepada_6'));
		
		if ($this->form_validation->run() == FALSE)
		 {
		  echo 0;
		 }
		else
		 {
		  	$data_input = array(
        		'domain' => $web,
				'nomor_surat' => '-',
				'perihal_surat' => $perihal_surat,
				'tanggal_surat' => date('Y-m-d'),
				'nama' => $nama,
				'nomot_telp' => $nomot_telp,
				'kebangsaan' => $kebangsaan,
				'alamat' => $alamat,
				'pekerjaan' => $pekerjaan,
				'penanggung_jawab' => $penanggung_jawab,
				'lokasi' => $lokasi,
				'judul_penelitian' => $judul_penelitian,
				'asal_universitas' => $asal_universitas,
				'nomor_surat_kesbangpol' => $nomor_surat_kesbangpol,
				'tanggal_surat_kesbangpol' => $tanggal_surat_kesbangpol,
				'surat_ditujukan_kepada' => $surat_ditujukan_kepada,
				'tembusan_kepada_1' => $tembusan_kepada_1,
				'tembusan_kepada_2' => $tembusan_kepada_2,
				'tembusan_kepada_3' => $tembusan_kepada_3,
				'tembusan_kepada_4' => $tembusan_kepada_4,
				'tembusan_kepada_5' => $tembusan_kepada_5,
				'tembusan_kepada_6' => $tembusan_kepada_6,
				'temp' => trim($this->input->post('temp')),
				'created_by' => 0,
				'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 0
				);
     		$table_name = 'perijinan_penelitian';
      		$id = $this->Perijinan_penelitian_model->simpan_perijinan_penelitian($data_input, $table_name);
      		echo $id;
			$table_name  = 'lampiran_perijinan_penelitian';
		  	$where       = array(
				'table_name' => 'perijinan_penelitian',
				'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      	$this->Crud_model->update_data($data_update, $where, $table_name);
      	redirect('postings/details/1029981/Penelitian.HTML');
     }
   }

   public function cetak_survey(){
	    $where = array(
			'id_perijinan_penelitian' => $this->uri->segment(3),
			'status' => 1
			);
	    $d = $this->Perijinan_penelitian_model->get_data($where);
	    if(!$d){
				$data['nama'] = '';
				$data['nomor_surat'] = '';
				$data['perihal_surat'] = '';
				$data['tanggal_surat'] = '';
				$data['nomot_telp'] = '';
				$data['kebangsaan'] = '';
				$data['alamat'] = '';
				$data['pekerjaan'] = '';
				$data['penanggung_jawab'] = '';
				$data['lokasi'] = '';
				$data['judul_penelitian'] = '';
				$data['asal_universitas'] = '';
				$data['nomor_surat_kesbangpol'] = '';
				$data['tanggal_surat_kesbangpol'] = '';
				$data['surat_ditujukan_kepada'] = '';
				$data['tembusan_kepada_1'] = '';
				$data['tembusan_kepada_2'] = '';
				$data['tembusan_kepada_3'] = '';
				$data['tembusan_kepada_4'] = '';
				$data['tembusan_kepada_5'] = '';
				$data['tembusan_kepada_6'] = '';
				$data['judul_ttd'] = '';
				$data['jabatan_ttd'] = '';
				$data['nama_ttd'] = '';
				$data['jabatan2_ttd'] = '';
				$data['nip_ttd'] = '';
				$data['pembuat'] = '';
				$data['created_time'] = '';
				$data['pengedit'] = '';
				$data['updated_time'] = '';
	          }
	        else{
				$data['nama'] = $d['nama'];
				$data['nomor_surat'] = $d['nomor_surat'];
				$data['perihal_surat'] = ''.$d['perihal_surat'].' ';
				//$data['tanggal_surat'] = $d['tanggal_surat'];
				$data['tanggal_surat'] = ''.$this->Crud_model->dateBahasaIndo($d['tanggal_surat']).'';
	            $data['nomot_telp'] = $d['nomot_telp'];
	            $data['kebangsaan'] = $d['kebangsaan'];
	            $data['alamat1'] = $d['alamat'];
	            $data['pekerjaan'] = $d['pekerjaan'];
	            $data['penanggung_jawab'] = $d['penanggung_jawab'];
				$data['lokasi'] = $d['lokasi'];
				$data['judul_penelitian'] = $d['judul_penelitian'];
				$data['asal_universitas'] = $d['asal_universitas'];
				$data['nomor_surat_kesbangpol'] = $d['nomor_surat_kesbangpol'];
				// $data['tanggal_surat_kesbangpol'] = $d['tanggal_surat_kesbangpol'];
				$data['tanggal_surat_kesbangpol'] = $this->Crud_model->dateBahasaIndo($d['tanggal_surat_kesbangpol']);
				$data['surat_ditujukan_kepada'] = $d['surat_ditujukan_kepada'];
				$data['tembusan_kepada_1'] = $d['tembusan_kepada_1'];
				$data['tembusan_kepada_2'] = $d['tembusan_kepada_2'];
				$data['tembusan_kepada_3'] = $d['tembusan_kepada_3'];
				$data['tembusan_kepada_4'] = $d['tembusan_kepada_4'];
				$data['tembusan_kepada_5'] = $d['tembusan_kepada_5'];
				$data['tembusan_kepada_6'] = $d['tembusan_kepada_6'];
				$data['judul_ttd'] = $d['judul_ttd'];
				$data['jabatan_ttd'] = $d['jabatan_ttd'];
				$data['nama_ttd'] = $d['nama_ttd'];
				$data['jabatan2_ttd'] = $d['jabatan2_ttd'];
				$data['nip_ttd'] = $d['nip_ttd'];
				$data['pembuat'] = $d['pembuat'];
				$data['created_time'] = $this->Crud_model->dateBahasaIndo1($d['created_time']);
				$data['pengedit'] = $d['pengedit'];
				$data['updated_time'] = $d['updated_time'];
	          }
	    $web=$this->uut->namadomain(base_url());
	    $where0 = array(
	      'domain' => $web
	      );
	    $this->db->where($where0);
	    $this->db->limit(1);
	    $query0 = $this->db->get('dasar_website');
	    foreach ($query0->result() as $row0)
	      {
	        $data['domain'] = $row0->domain;
	        $data['alamat'] = $row0->alamat;
	        $data['telpon'] = $row0->telpon;
	        $data['email'] = $row0->email;
	        $data['twitter'] = $row0->twitter;
	        $data['facebook'] = $row0->facebook;
	        $data['google'] = $row0->google;
	        $data['instagram'] = $row0->instagram;
	        $data['peta'] = $row0->peta;
	        $data['keterangan'] = $row0->keterangan;
	      }
	      $data['judul'] = 'Selamat datang';
	      $data['main_view'] = 'perijinan_penelitian/cetak_survey';
	      $this->load->view('print', $data);
	  }
   
 }