<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Anggota_organisasi extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Anggota_organisasi_model');
   }
   
  public function index(){
    $data['total'] = 10;
    $data['main_view'] = 'anggota_organisasi/home';
    $this->load->view('tes', $data);
	}
	public function load_anggota_organisasi(){
      $mode = $this->input->post('mode');
      if( $mode == 'edit' ){
      $where    = array(
				'id_permohonan_organisasi' => $this->input->post('value')
				);
      }
      elseif ( $mode == 'edit_anggota_organisasi' ){
      $where    = array(
				'temp' => $this->input->post('value')
				);
      }
      else{
      $where    = array(
				'temp' => $this->input->post('value')
				);
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_anggota_organisasi');
      $result = $this->db->get('anggota_organisasi');
      echo json_encode($result->result_array());
	}
  public function json_all_anggota_organisasi()
   {
    $where      = array(
      'anggota_organisasi.status !=' => 99,
			'anggota_organisasi.temp' => trim($this->input->post('temp'))
    );
    $a          = $this->Anggota_organisasi_model->json_semua_anggota_organisasi($where);
    $halaman    = 1;
    $limit      = 200000000000;
    $start      = ($halaman - 1) * $limit;
    $fields     = "
				*
				";
    $where      = array(
      'anggota_organisasi.status !=' => 99,
			'anggota_organisasi.temp' => trim($this->input->post('temp'))
    );
    $order_by   = 'anggota_organisasi.nama_anggota_organisasi';
    echo json_encode($this->Anggota_organisasi_model->json_all_anggota_organisasi($where, $limit, $start, $fields, $order_by));
   }
  public function simpan_anggota_organisasi(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama_anggota_organisasi', 'nama_anggota_organisasi', 'required');
		$this->form_validation->set_rules('umur_anggota_organisasi', 'umur_anggota_organisasi', 'required');
		$this->form_validation->set_rules('jabatan_anggota_organisasi', 'jabatan_anggota_organisasi', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'id_permohonan_organisasi' => 0,
				'nama_anggota_organisasi' => trim($this->input->post('nama_anggota_organisasi')),
        'umur_anggota_organisasi' => trim($this->input->post('umur_anggota_organisasi')),
        'jabatan_anggota_organisasi' => trim($this->input->post('jabatan_anggota_organisasi')),
        'temp' => trim($this->input->post('temp')),
        'keterangan_anggota_organisasi' => trim($this->input->post('keterangan_anggota_organisasi')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1
				);
      $table_name = 'anggota_organisasi';
      $id         = $this->Crud_model->save_data($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'permohonan_organisasi',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
	}
	public function get_by_id(){
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'anggota_organisasi.id_anggota_organisasi' => $this->input->post('id_anggota_organisasi'),
        'anggota_organisasi.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_anggota_organisasi');
      $result = $this->db->get('anggota_organisasi');
      echo json_encode($result->result_array());
	}
  public function update_anggota_organisasi(){
		$this->form_validation->set_rules('nama_anggota_organisasi', 'nama_anggota_organisasi', 'required');
		$this->form_validation->set_rules('keterangan_anggota_organisasi', 'keterangan_anggota_organisasi', 'required');
    if ($this->form_validation->run() == FALSE){
      echo 0;
		}
    else{
      $data_update = array(
			
        'nama_anggota_organisasi' => trim($this->input->post('nama_anggota_organisasi')),
        'umur_anggota_organisasi' => trim($this->input->post('umur_anggota_organisasi')),
        'temp' => trim($this->input->post('temp')),
        'keterangan' => trim($this->input->post('keterangan')),
        'jabatan_anggota_organisasi' => trim($this->input->post('jabatan_anggota_organisasi')),
        'keterangan_anggota_organisasi' => trim($this->input->post('keterangan_anggota_organisasi')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
			);
      $table_name  = 'anggota_organisasi';
      $where       = array(
        'anggota_organisasi.id_anggota_organisasi' => trim($this->input->post('id_anggota_organisasi'))
			);
      $this->Anggota_organisasi_model->update_anggota_organisasi($data_update, $where, $table_name);
      echo 1;
		}
  }
	public function hapus(){
		$id_anggota_organisasi = $this->input->post('id_anggota_organisasi');
		$where = array(
			'id_anggota_organisasi' => $id_anggota_organisasi,
			'created_by' => $this->session->userdata('id_users')
			);
		$this->db->from('anggota_organisasi');
		$this->db->where($where);
		$a = $this->db->count_all_results();
		if($a == 0){
			echo 0;
			}
		else{
			$this->db->where($where);
			$this->db->delete('anggota_organisasi');
			echo 1;
			}
	}
}