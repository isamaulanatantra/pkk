<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data_website extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Data_website_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'data_website/home';
    $this->load->view('back_bone', $data);
   }
   
  public function json_all_data_website()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_data_website    = $this->input->post('urut_data_data_website');
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  data_website.id_data_website,
                  data_website.jabatan_penandatangan,
                  data_website.nama_penandatangan,
                  data_website.pangkat_penandatangan,
                  data_website.nip_penandatangan,
                  data_website.nama_data_website,
                  data_website.telpn,
                  data_website.alamat,
                  data_website.fax,
                  data_website.kode_pos,
                  data_website.nomor,
                  data_website.website,
                  data_website.email,
                  data_website.id_skpd,
                  data_website.id_desa,
                  data_website.jenis_pemerintahan,
                  data_website.status
                  ";
    $where      = array(
      'status!=' => 99
    );
    $order_by   = 'data_website.'.$urut_data_data_website.'';
    echo json_encode($this->Data_website_model->json_all_data_website($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
   
  public function inaktifkan()
		{
      $where = array(
        'id_data_website' => $this->input->post('id_data_website')
        );
      $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'data_website';
        $this->Data_website_model->update_data_data_website($data_update, $where, $table_name);
		}
   
  public function aktifkan()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
        'id_data_website' => $this->input->post('id_data_website')
        );
      $data_update = array(
      
        'status' => 1
                
        );
      $table_name  = 'data_website';
      $this->Data_website_model->update_data_data_website($data_update, $where, $table_name);
      
      $where = array(
        'id_data_website !=' => $this->input->post('id_data_website'),
        'website' => $web
        );
      $data_update = array(
      
        'status' => 0
                
        );
      $table_name  = 'data_website';
      $this->Data_website_model->update_data_data_website($data_update, $where, $table_name);
      
		}
   
  public function cek_tema()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('tema');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'website' => $web
          );
        $this->db->from('data_website');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'website' => $web,
            'created_by' => 1,
            'created_time' => date('Y-m-d H:i:s'),
            'status' => 1
                    
            );
          $table_name = 'data_website';
          $this->Data_website_model->simpan_data_website($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
 }