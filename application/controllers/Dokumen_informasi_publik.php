<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dokumen_informasi_publik extends CI_Controller
 {
  function __construct()
   {
    parent::__construct();
    $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->model('Cetak_model');
    $this->load->model('Crud_model');
    $this->load->model('Dokumen_informasi_publik_model');
   }
  public function check_login()
   {
    $table_name = 'users';
    $where      = array(
      'users.id_users' => $this->session->userdata('id_users')
    );
    $a          = $this->Crud_model->semua_data($where, $table_name);
    if ($a == 0)
     {
        return redirect(''.base_url().'login');
     }
   }
  public function index()
   {
    $this->check_login();
    $where             = array(
      'dokumen_informasi_publik.status !=' => 99
      );
    $a                 = $this->Dokumen_informasi_publik_model->json_semua_dokumen_informasi_publik($where);
    $data['per_page']  = 20;
    $data['total']     = ceil($a / 20);
    $data['main_view'] = 'dokumen_informasi_publik/home';
    $this->load->view('back_bone_admin', $data);
   }
  public function json_all_dokumen_informasi_publik()
   {
    $where      = array(
      'dokumen_informasi_publik.status !=' => 99
    );
    $a          = $this->Dokumen_informasi_publik_model->json_semua_dokumen_informasi_publik($where);
    $halaman    = $this->input->get('halaman');
    $limit      = 20;
    $start      = ($halaman - 1) * $limit;
    $fields     = "
				
				dokumen_informasi_publik.id_dokumen_informasi_publik,
				dokumen_informasi_publik.kode_dokumen_informasi_publik,
				dokumen_informasi_publik.nama_dokumen_informasi_publik,
				dokumen_informasi_publik.inserted_by,
				dokumen_informasi_publik.inserted_time,
				dokumen_informasi_publik.updated_by,
				dokumen_informasi_publik.updated_time,
				dokumen_informasi_publik.deleted_by,
				dokumen_informasi_publik.deleted_time,
				dokumen_informasi_publik.temp,
				dokumen_informasi_publik.keterangan,
				dokumen_informasi_publik.status,
        (select attachment.file_name from attachment where attachment.id_tabel=dokumen_informasi_publik.id_dokumen_informasi_publik and attachment.table_name='dokumen_informasi_publik') as attachment_dokumen_informasi_publik,
								
				";
    $where      = array(
      'dokumen_informasi_publik.status !=' => 99
    );
    $order_by   = 'dokumen_informasi_publik.inserted_time';
    echo json_encode($this->Dokumen_informasi_publik_model->json_all_dokumen_informasi_publik($where, $limit, $start, $fields, $order_by));
   }
  public function simpan_dokumen_informasi_publik()
  {
		//$this->form_validation->set_rules('id_dokumen_informasi_publik', 'id_dokumen_informasi_publik', 'required');
		$id_dokumen_informasi_publik         = '';
		$this->form_validation->set_rules('kode_dokumen_informasi_publik', 'kode_dokumen_informasi_publik', 'required');
		$kode_dokumen_informasi_publik         = trim($this->input->post('kode_dokumen_informasi_publik'));
		$this->form_validation->set_rules('nama_dokumen_informasi_publik', 'nama_dokumen_informasi_publik', 'required');
		$nama_dokumen_informasi_publik         = trim($this->input->post('nama_dokumen_informasi_publik'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = '';
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = '';
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = '';
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = '';
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = trim($this->input->post('temp'));
		//$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '0';
    if ($this->form_validation->run() == FALSE)
    {
      echo 0;
    }
    else
    {
      $data_input = array(
				'kode_dokumen_informasi_publik' => $kode_dokumen_informasi_publik,
				'nama_dokumen_informasi_publik' => $nama_dokumen_informasi_publik,
				'inserted_by' => $inserted_by,
				'inserted_time' => $inserted_time,
				'temp' => $temp,
				'keterangan' => $keterangan,
				'status' => $status,
								
				);
      $table_name = 'dokumen_informasi_publik';
      $id         = $this->Dokumen_informasi_publik_model->simpan_dokumen_informasi_publik($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'dokumen_informasi_publik',
        'temp' => $temp
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
    }
  }
  public function update_data_dokumen_informasi_publik()
  {
    $this->form_validation->set_rules('id_dokumen_informasi_publik', 'id_dokumen_informasi_publik', 'required');
		$id_dokumen_informasi_publik         = trim($this->input->post('id_dokumen_informasi_publik'));
		$this->form_validation->set_rules('kode_dokumen_informasi_publik', 'kode_dokumen_informasi_publik', 'required');
		$kode_dokumen_informasi_publik         = trim($this->input->post('kode_dokumen_informasi_publik'));
		$this->form_validation->set_rules('nama_dokumen_informasi_publik', 'nama_dokumen_informasi_publik', 'required');
		$nama_dokumen_informasi_publik         = trim($this->input->post('nama_dokumen_informasi_publik'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = '';
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '';
/*     $id_kabupaten = $this->input->get('id_kabupaten');
    $id_kecamatan = $this->input->get('id_kecamatan');
    $id_desa  = $this->input->get('id_desa');
 */    
    $id_kabupaten = $this->session->userdata('id_kabupaten');
    $id_kecamatan = $this->session->userdata('id_kecamatan');
    $id_desa  = $this->session->userdata('id_desa');
    if ($this->form_validation->run() == FALSE)
    {
      echo '[]';
    }
    else
    {
      $data_update = array(
			
        'id_kabupaten' => $id_kabupaten,
        'id_kecamatan' => $id_kecamatan,
        'id_desa' => $id_desa,
				'kode_dokumen_informasi_publik' => $kode_dokumen_informasi_publik,
				'nama_dokumen_informasi_publik' => $nama_dokumen_informasi_publik,
				'updated_by' => $updated_by,
				'updated_time' => $updated_time,
				'keterangan' => $keterangan,
								
				);
      $table_name  = 'dokumen_informasi_publik';
      $where       = array(
        'dokumen_informasi_publik.id_dokumen_informasi_publik' => $id_dokumen_informasi_publik      
			);
      $this->Dokumen_informasi_publik_model->update_data_dokumen_informasi_publik($data_update, $where, $table_name);
      echo '[{"save":"ok"}]';
    }
  }
  public function il_simpan_dokumen_informasi_publik()
   {
		$this->form_validation->set_rules('id_dokumen_informasi_publik', 'id_dokumen_informasi_publik', 'required');
		$id_dokumen_informasi_publik         = trim($this->input->post('id_dokumen_informasi_publik'));
		$this->form_validation->set_rules('kode_dokumen_informasi_publik', 'kode_dokumen_informasi_publik', 'required');
		$kode_dokumen_informasi_publik         = trim($this->input->post('kode_dokumen_informasi_publik'));
		$this->form_validation->set_rules('nama_dokumen_informasi_publik', 'nama_dokumen_informasi_publik', 'required');
		$nama_dokumen_informasi_publik         = trim($this->input->post('nama_dokumen_informasi_publik'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = '';
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '';
				
		if ($this->form_validation->run() == FALSE)
     {
      echo 'Error';
     }
    else
     {
      $data_update = array(
        
				'kode_dokumen_informasi_publik' => $kode_dokumen_informasi_publik,
				'nama_dokumen_informasi_publik' => $nama_dokumen_informasi_publik,
				'updated_by' => $updated_by,
				'updated_time' => $updated_time,
				'keterangan' => $keterangan,
								
				);
      $table_name  = 'dokumen_informasi_publik';
      $where       = array(
        'dokumen_informasi_publik.id_dokumen_informasi_publik' => $id_dokumen_informasi_publik      );
      $this->Dokumen_informasi_publik_model->update_data_dokumen_informasi_publik($data_update, $where, $table_name);
      echo 'Success';
     }
   }
  public function ag_simpan_dokumen_informasi_publik()
   {
		$this->form_validation->set_rules('id_dokumen_informasi_publik', 'id_dokumen_informasi_publik', 'required');
		$id_dokumen_informasi_publik         = trim($this->input->post('id_dokumen_informasi_publik'));
		$this->form_validation->set_rules('kode_dokumen_informasi_publik', 'kode_dokumen_informasi_publik', 'required');
		$kode_dokumen_informasi_publik         = trim($this->input->post('kode_dokumen_informasi_publik'));
		$this->form_validation->set_rules('nama_dokumen_informasi_publik', 'nama_dokumen_informasi_publik', 'required');
		$nama_dokumen_informasi_publik         = trim($this->input->post('nama_dokumen_informasi_publik'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = '';
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '';
				if ($this->form_validation->run() == FALSE)
     {
      echo '[]';
     }
    else
     {
      $data_update = array(
        
				'kode_dokumen_informasi_publik' => $kode_dokumen_informasi_publik,
				'nama_dokumen_informasi_publik' => $nama_dokumen_informasi_publik,
				'updated_by' => $updated_by,
				'updated_time' => $updated_time,
				'keterangan' => $keterangan,
								
				 );
      $table_name  = 'dokumen_informasi_publik';
      $where       = array(
        'dokumen_informasi_publik.id_dokumen_informasi_publik' => $id_dokumen_informasi_publik      
				);
      $this->Dokumen_informasi_publik_model->update_data_dokumen_informasi_publik($data_update, $where, $table_name);
      echo '[{"dokumen_informasi_publik":"ok", "jumlah":"ok"}]';
     }
   }
  public function cetak()
   {
    $where             = array(
      'dokumen_informasi_publik.status !=' => 99
    );
    $data['data_dokumen_informasi_publik'] = $this->Dokumen_informasi_publik_model->data_dokumen_informasi_publik($where);
    $this->load->view('dokumen_informasi_publik/cetak', $data);
   }
  public function dokumen_informasi_publik_get_by_id()
   {
    $table_name = 'dokumen_informasi_publik';
    $id         = $_GET['id'];
    $fields     = "
				dokumen_informasi_publik.id_dokumen_informasi_publik,
				dokumen_informasi_publik.kode_dokumen_informasi_publik,
				dokumen_informasi_publik.nama_dokumen_informasi_publik,
				dokumen_informasi_publik.inserted_by,
				dokumen_informasi_publik.inserted_time,
				dokumen_informasi_publik.updated_by,
				dokumen_informasi_publik.updated_time,
				dokumen_informasi_publik.deleted_by,
				dokumen_informasi_publik.deleted_time,
				dokumen_informasi_publik.temp,
				dokumen_informasi_publik.keterangan,
				dokumen_informasi_publik.status,
								";
    $where      = array(
      'dokumen_informasi_publik.id_dokumen_informasi_publik' => $id
    );
    $order_by   = 'dokumen_informasi_publik.nama_dokumen_informasi_publik';
    echo json_encode($this->Dokumen_informasi_publik_model->get_by_id($table_name, $where, $fields, $order_by));
   }
  public function hapus_dokumen_informasi_publik()
   {
    $table_name = 'dokumen_informasi_publik';
    $id         = $_GET['id'];
    $where      = array(
      'dokumen_informasi_publik.id_dokumen_informasi_publik' => $id
    );
    $t          = $this->Dokumen_informasi_publik_model->json_semua_dokumen_informasi_publik($where, $table_name);
    if ($t == 0)
     {
      echo ' {"errors":"Yes"} ';
     }
    else
     {
      $data_update = array(
        'dokumen_informasi_publik.status' => 99
      );
      $where       = array(
        'dokumen_informasi_publik.id_dokumen_informasi_publik' => $id
      );
      $this->Dokumen_informasi_publik_model->update_data_dokumen_informasi_publik($data_update, $where, $table_name);
      echo ' {"errors":"No"} ';
     }
   }
  public function cari_dokumen_informasi_publik()
   {
    $table_name = 'dokumen_informasi_publik';
    $key_word   = $_GET['key_word'];
    $where      = array(
      'dokumen_informasi_publik.status !=' => 99
    );
    $field      = 'dokumen_informasi_publik.nama_dokumen_informasi_publik';
    $a          = $this->Dokumen_informasi_publik_model->count_all_search_dokumen_informasi_publik($where, $key_word, $table_name, $field);
    if (empty($key_word))
     {
      echo '[]';
      exit;
     }
    else if ($a == 0)
     {
      echo '[{"jumlah":"0"}]';
      exit;
     }
    $fields     = "
				dokumen_informasi_publik.id_dokumen_informasi_publik,
				dokumen_informasi_publik.kode_dokumen_informasi_publik,
				dokumen_informasi_publik.nama_dokumen_informasi_publik,
				dokumen_informasi_publik.inserted_by,
				dokumen_informasi_publik.inserted_time,
				dokumen_informasi_publik.updated_by,
				dokumen_informasi_publik.updated_time,
				dokumen_informasi_publik.deleted_by,
				dokumen_informasi_publik.deleted_time,
				dokumen_informasi_publik.temp,
				dokumen_informasi_publik.keterangan,
				dokumen_informasi_publik.status,
								";
    $where      = array(
      'dokumen_informasi_publik.status !=' => 99
    );
    $field_like = 'dokumen_informasi_publik.nama_dokumen_informasi_publik';
    $limit      = $_GET['limit'];
    $start      = (($_GET['start']) * $limit);
    $order_by   = ''.$field_like.'';
    echo json_encode($this->Dokumen_informasi_publik_model->search($table_name, $fields, $where, $limit, $start, $field_like, $key_word, $order_by));
   }
  public function count_all_cari_dokumen_informasi_publik()
   {
    $table_name = 'dokumen_informasi_publik';
    $key_word   = $_GET['key_word'];
    if (empty($key_word))
     {
      echo '[]';
      exit;
     }
    $where = array(
      'dokumen_informasi_publik.status !=' => 99
    );
    $field = 'dokumen_informasi_publik.nama_dokumen_informasi_publik';
    $a     = $this->Dokumen_informasi_publik_model->count_all_search_dokumen_informasi_publik($where, $key_word, $table_name, $field);
    $limit = $_GET['limit'];
    echo '[{"dokumen_informasi_publik":"' . ceil($a / $limit) .'", "jumlah":"'.$a .'"}]';
   }
  public function download_xls()
   {
    $this->load->library('excel');
    $objPHPExcel = new PHPExcel();
    $objPHPExcel
			->getProperties()
			->setCreator("Budi Utomo")
			->setLastModifiedBy("Budi Utomo")
			->setTitle("Laporan I")
			->setSubject("Office 2007 XLSX Document")
			->setDescription("Laporan untuk menampilkan kunjungan pasien")
			->setKeywords("Laporan Halaman")
			->setCategory("Bentuk XLS");
    $objPHPExcel
			->setActiveSheetIndex(0)
			->setCellValue('A1', 'Cetak')
			->mergeCells('A1:G1')
			->setCellValue('A2', 'Laporan Data Halaman')
			->mergeCells('A2:G2')
			->setCellValue('A3', 'Tanggal Download : ' . date('d/m/Y') .' ')
			->mergeCells('A3:G3')
			->setCellValue('A6', 'No')
			->setCellValue('B6', 'Akhir Tahun Periode')
			->setCellValue('C6', 'Awal Tahun Periode')
			->setCellValue('D6', 'Keterangan');
    $table    = 'dokumen_informasi_publik';
    $fields     = "
				dokumen_informasi_publik.id_dokumen_informasi_publik,
				dokumen_informasi_publik.kode_dokumen_informasi_publik,
				dokumen_informasi_publik.nama_dokumen_informasi_publik,
				dokumen_informasi_publik.inserted_by,
				dokumen_informasi_publik.inserted_time,
				dokumen_informasi_publik.updated_by,
				dokumen_informasi_publik.updated_time,
				dokumen_informasi_publik.deleted_by,
				dokumen_informasi_publik.deleted_time,
				dokumen_informasi_publik.temp,
				dokumen_informasi_publik.keterangan,
				dokumen_informasi_publik.status,
								";
    $where    = array(
      'dokumen_informasi_publik.status !=' => 99
    );
    $order_by = 'dokumen_informasi_publik.nama_dokumen_informasi_publik';
    $b        = $this->Cetak_model->cetak_single_table($table, $fields, $where, $order_by);
    $i        = 7;
    $no       = 0;
    foreach ($b->result() as $b1)
     {
      $no = $no + 1;
      $objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, ''.$no.'')
				->setCellValue('B'.$i, ''.$b1->nama_dokumen_informasi_publik.'')
				->setCellValue('C'.$i, '\''.$b1->kode_dokumen_informasi_publik.'')
				->setCellValue('D'.$i, ''.$b1->keterangan.'')
			;
				
      $objPHPExcel
				->getActiveSheet()
				->getStyle('A'.$i.':D'.$i.'')
				->getBorders()
				->getAllBorders()
				->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
     }
    $objPHPExcel->getActiveSheet()->setTitle('Probable Clients');
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
    $objPHPExcel->getActiveSheet()->getStyle('A6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('C6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('D6')->getFont()->setBold(true);
		
    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->getStartColor()->setARGB('cccccc');
    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		
    $objPHPExcel->setActiveSheetIndex(0);
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Halaman_' . date('ymdhis') .'.xls"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') .' GMT');
    header('Cache-Control: cache, must-revalidate');
    header('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
   }
	 
  public function cetak_pdf()
   {
    $table             = 'dokumen_informasi_publik';
    $fields     = "
				dokumen_informasi_publik.id_dokumen_informasi_publik,
				dokumen_informasi_publik.kode_dokumen_informasi_publik,
				dokumen_informasi_publik.nama_dokumen_informasi_publik,
				dokumen_informasi_publik.inserted_by,
				dokumen_informasi_publik.inserted_time,
				dokumen_informasi_publik.updated_by,
				dokumen_informasi_publik.updated_time,
				dokumen_informasi_publik.deleted_by,
				dokumen_informasi_publik.deleted_time,
				dokumen_informasi_publik.temp,
				dokumen_informasi_publik.keterangan,
				dokumen_informasi_publik.status,
								";
    $where             = array(
      'dokumen_informasi_publik.status !=' => 99
    );
    $order_by          = 'dokumen_informasi_publik.nama_dokumen_informasi_publik';
    $data['data_dokumen_informasi_publik'] = $this->Cetak_model->cetak_single_table($table, $fields, $where, $order_by);
    $this->load->view('dokumen_informasi_publik/pdf', $data);
    $html = $this->output->get_output();
    $this->load->library('dompdf_gen');
    $this->dompdf->load_html($html);
    $this->dompdf->set_paper('a4', 'portrait');
    $this->dompdf->render();
    $this->dompdf->stream("cetak_dokumen_informasi_publik_" . date('d_m_y') . ".pdf");
   } 
	public function search_dokumen_informasi_publik()
		{
		$key_word = trim($this->input->post('key_word'));
    $halaman    = $this->input->post('halaman');
    $limit      = 20;
    $start      = ($halaman - 1) * $limit;
    $fields     = "
				dokumen_informasi_publik.id_dokumen_informasi_publik,
				dokumen_informasi_publik.kode_dokumen_informasi_publik,
				dokumen_informasi_publik.nama_dokumen_informasi_publik,
				dokumen_informasi_publik.inserted_by,
				dokumen_informasi_publik.inserted_time,
				dokumen_informasi_publik.updated_by,
				dokumen_informasi_publik.updated_time,
				dokumen_informasi_publik.deleted_by,
				dokumen_informasi_publik.deleted_time,
				dokumen_informasi_publik.temp,
				dokumen_informasi_publik.keterangan,
				dokumen_informasi_publik.status,
								";
    $where      = array(
      'dokumen_informasi_publik.status !=' => 99
    );
    $order_by   = 'dokumen_informasi_publik.nama_dokumen_informasi_publik';
    echo json_encode($this->Dokumen_informasi_publik_model->search_dokumen_informasi_publik($fields, $where, $limit, $start, $key_word, $order_by));
		}
	public function count_all_search_dokumen_informasi_publik()
		{
			$table_name = 'dokumen_informasi_publik';
			$key_word = $_GET['key_word'];
			$field = 'nama_dokumen_informasi_publik';
			$where = array(
					''.$table_name.'.status' => 1
				);
			$a = $this->Dokumen_informasi_publik_model->count_all_search_dokumen_informasi_publik($where, $key_word, $table_name, $field);
			$limit = 20;
			echo ceil($a/$limit); 
		}
	public function auto_suggest()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
			$where      = array(
				'dokumen_informasi_publik.status !=' => 99
			);
			$fields     = "
				dokumen_informasi_publik.id_dokumen_informasi_publik,
				dokumen_informasi_publik.kode_dokumen_informasi_publik,
				dokumen_informasi_publik.nama_dokumen_informasi_publik,
				dokumen_informasi_publik.inserted_by,
				dokumen_informasi_publik.inserted_time,
				dokumen_informasi_publik.updated_by,
				dokumen_informasi_publik.updated_time,
				dokumen_informasi_publik.deleted_by,
				dokumen_informasi_publik.deleted_time,
				dokumen_informasi_publik.temp,
				dokumen_informasi_publik.keterangan,
				dokumen_informasi_publik.status,
								dokumen_informasi_publik.nama_dokumen_informasi_publik as value
				";
			echo json_encode( $this->Dokumen_informasi_publik_model->auto_suggest($q, $where, $fields) );
			}
		}
 }