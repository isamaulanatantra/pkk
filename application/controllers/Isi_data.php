<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Isi_data extends CI_Controller
{

  function __construct(){
		parent::__construct();
		$this->load->model('Isi_data_model');
  }
  public function index(){
    $data['main_view'] = 'isi_data/home';
    $this->load->view('back_bone', $data);
  }
  public function views(){
		$where0 = array(
			'id_kategori_data' => $this->uri->segment(3)
			);
		$this->db->where($where0);
		$this->db->limit(1);
		$query0 = $this->db->get('kategori_data');
		foreach ($query0->result() as $row0){
				$data['nama_kategori_data'] = $row0->nama_kategori_data;
			}
    $data['main_view'] = 'isi_data/views';
    $this->load->view('back_bone', $data);
  }
  public function json_option_isi_data(){
		$table = 'isi_data';
		$hak_akses = $this->session->userdata('hak_akses');
		if($hak_akses=='root'){
			$where   = array(
				'isi_data.status !=' => 99
				);
		}
		else{
			$where   = array(
				'isi_data.id_isi_data' => $this->session->userdata('id_isi_data')
				);
		}
		$fields  = 
			"
			isi_data.id_isi_data,
			isi_data.nama_isi_data
			";
		$order_by  = 'isi_data.nama_isi_data';
		echo json_encode($this->Isi_data_model->opsi($table, $where, $fields, $order_by));
  }
  public function total_data(){
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'isi_data.status !=' => 99,
      'isi_data.status !=' => 999
      );
    $this->db->where($where0);
    $query0 = $this->db->get('isi_data');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_isi_data(){
		$table = 'isi_data';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
		(select kategori_data.nama_kategori_data FROM kategori_data where kategori_data.id_kategori_data=isi_data.id_kategori_data) as nama_kategori_data
    ";
    $where      = array(
      'isi_data.status !=' => 99
    );
    $orderby   = ''.$order_by.'';
    $query = $this->Isi_data_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
          $urut=$urut+1;
          echo '<tr id_isi_data="'.$row->id_isi_data.'" id="'.$row->id_isi_data.'" >';
          echo '<td valign="top">'.($urut).'</td>';
					echo '<td valign="top"><a href="'.base_url().'isi_data/views/'.$row->id_kategori_data.'">'.$row->nama_kategori_data.'</a></td>';
					echo '<td valign="top">'.$row->nama_isi_data.'</td>';
					echo '<td valign="top">'.$row->tahun_isi_data.'</td>';
					echo '<td valign="top">'.$row->jumlah_isi_data.'</td>';
					echo '<td valign="top"><a href="#tab_form_isi_data" data-toggle="tab" class="update_id btn btn-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>';
					echo '<a href="#" id="del_ajax" class="btn btn-danger btn-sm"><i class="fa fa-cut"></i></a>';
          echo '<a class="btn btn-warning btn-sm" href="'.base_url().'isi_data/pdf/?id_isi_data='.$row->id_isi_data.'" ><i class="fa fa-file-pdf-o"></i></a></td>';
          echo '</tr>';
      }
          echo '<tr>';
          echo '<td valign="top" colspan="6" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="',base_url(),'isi_data/xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
          echo '</tr>';
          
   }
  public function total_data_tahun(){
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $tahun    = $this->input->post('tahun');
    $id_kategori_data    = $this->input->post('id_kategori_data');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $tahun, $keyword);
    }
    $where0 = array(
      'isi_data.status !=' => 99,
      'isi_data.status !=' => 999,
			'id_kategori_data' => $id_kategori_data
      );
    $this->db->where($where0);
    $query0 = $this->db->get('isi_data');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_views_isi_data(){
		$table = 'isi_data';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $tahun    = $this->input->post('tahun');
    $id_kategori_data    = $this->input->post('id_kategori_data');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
		";
		if($tahun=='semua'){
    $where      = array(
      'isi_data.status !=' => 99,
			'id_kategori_data' => $id_kategori_data
    );
		}
		else{
			$where      = array(
				'isi_data.status !=' => 99,
				'isi_data.tahun_isi_data' => $tahun,
				'id_kategori_data' => $id_kategori_data
			);
		}
    $orderby   = ''.$order_by.'';
    $query = $this->Isi_data_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
          $urut=$urut+1;
          echo '<tr id_isi_data="'.$row->id_isi_data.'" id="'.$row->id_isi_data.'" >';
          echo '<td valign="top">'.($urut).'</td>';
					echo '<td valign="top">'.$row->nama_isi_data.'</td>';
					echo '<td valign="top">'.$row->tahun_isi_data.'</td>';
					echo '<td valign="top">'.$row->jumlah_isi_data.'</td>';
					echo '</tr>';
      }
          echo '<tr>';
          echo '<td valign="top" colspan="4" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="',base_url(),'isi_data/xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
          echo '</tr>';
          
   }
  public function simpan_isi_data(){
    $this->form_validation->set_rules('nama_isi_data', 'nama_isi_data', 'required');
    $this->form_validation->set_rules('id_kategori_data', 'id_kategori_data', 'required');
    $this->form_validation->set_rules('tahun_isi_data', 'tahun_isi_data', 'required');
    $this->form_validation->set_rules('jumlah_isi_data', 'jumlah_isi_data', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
			{
				$data_input = array(
				'nama_isi_data' => trim($this->input->post('nama_isi_data')),
				'id_kategori_data' => trim($this->input->post('id_kategori_data')),
				'tahun_isi_data' => trim($this->input->post('tahun_isi_data')),
				'jumlah_isi_data' => trim($this->input->post('jumlah_isi_data')),
				'keterangan' => '',
				'temp' => '',
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'isi_data';
      $this->Isi_data_model->save_data($data_input, $table_name);
     }
	}
	public function xls(){
		$table = 'isi_data';
		$this->load->library('excel');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
			->setCreator("Budi Utomo")
			->setLastModifiedBy("Budi Utomo")
			->setTitle("Laporan I")
			->setSubject("Office 2007 XLSX Document")
			->setDescription("Laporan Pemilih")
			->setKeywords("Laporan Halaman")
			->setCategory("Bentuk XLS");
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Download')
			->mergeCells('A1:G1')
			->setCellValue('A2', 'Data')
			->mergeCells('A2:G2')
			->setCellValue('A3', 'Tanggal Download : '.date('d/m/Y').' ')
			->mergeCells('A3:G3')
			
			->setCellValue('A6', 'No')
			->setCellValue('B6', 'Nama Pemilih')
			->setCellValue('C6', 'Nama PC')
      ;
		
    $page    = $this->input->get('page');
    $limit    = $this->input->get('limit');
    $keyword    = $this->input->get('keyword');
    $order_by    = $this->input->get('orderby');
    $tahun    = $this->input->get('tahun');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
		if($tahun=='semua'){
    $where      = array(
      'isi_data.status !=' => 99
    );
		}
		else{
			$where      = array(
				'isi_data.status !=' => 99,
				'isi_data.tahun_isi_data' => $tahun
			);
		}
    $orderby   = ''.$order_by.'';
    $query = $this->Isi_data_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $i = 7;
    $urut=$start;
    foreach ($query->result() as $row)
      {
        $urut=$urut+1;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i, ''.$urut.'')
        ->setCellValue('B'.$i, ''.$row->tahun_isi_data.'')
        ->setCellValueExplicit('C'.$i, ''.$row->tahun_isi_data.'', PHPExcel_Cell_DataType::TYPE_STRING)
        ;
        $objPHPExcel->getActiveSheet()
        ->getStyle('A'.$i.':C'.$i.'')
        ->getBorders()->getAllBorders()
        ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
      }
		
		$objPHPExcel->getActiveSheet()->setTitle('Probable Clients');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->getStartColor()->setARGB('cccccc');
								
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="DataPesertaKejuaraan'.date('ymdhis').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output');
	}
}