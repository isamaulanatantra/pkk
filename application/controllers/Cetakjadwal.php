<?php
Class Cetakjadwal extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('Pdf');
        // $this->load->model('Data_pasien_model');
    }
    
    function index(){
        // $input=Input::all();
        // $id_pus_pendaftaran=!empty($_GET['id_pus_pendaftaran'])?$_GET['id_pus_pendaftaran']:0;
        $awal=!empty($_GET['awal'])?$_GET['awal']:0;
        $akhir=!empty($_GET['akhir'])?$_GET['akhir']:0;

        // $fpdf = new PDF_EAN13('P','cm','kartuanggota');  
        $fpdf=new Fpdf('P','cm','A4');
        // $fpdf->SetMargins(13.175, 3.175, 3.175, 3.175);
        // $fpdf->SetAutoPageBreak(false); 
        $fpdf->AddPage();

        $fpdf -> Image(base_url().'media/logo_wsb.png',1.6,1.0,2.5,2.5);
        // $fpdf -> Image(base_url().'media/logo_kemkes1.png',16,0.5,1.4,1.4);

      
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','I',7);
        $fpdf -> ln();

        $fpdf -> SetFont('Arial','B',11);
        $fpdf -> Cell(20,0.5,'PEMERINTAH KABUPATEN WONOSOBO','',1,'C');

        $fpdf -> SetFont('Arial','B',15);
        $fpdf -> Cell(20,0.7,'DINAS KESEHATAN','',1,'C');

        $fpdf -> SetFont('Arial','',11);
        $fpdf -> Cell(20,0.7,'Alamat : Jl. T.Jogonegoro 2-4 Telp./Faks. : (0286) 321033 / 321319','',1,'C');
        $fpdf -> Cell(20,0.7,'Email : dinkes@wonosobokab.go.id','',1,'C');
        $fpdf -> Cell(20,0.7,'Wonosobo-56311','',1,'C');

        $fpdf -> Sety(4.1);
        $fpdf -> Cell(0,0.5,'','B',0,'C');
        $fpdf -> Sety(4.0);
        $fpdf -> Cell(0,0.5,'','B',0,'C');

        $fpdf -> Sety(5.0);
        $fpdf -> SetFont('Arial','B',11);
        $fpdf -> Cell(20,0.5,'JADWAL PENGGUNAAN AULA','',1,'C');
        $fpdf -> SetFont('Arial','',10);
        $fpdf -> Cell(20,0.5,'Periode : '.$this->tanggalCetak($awal)." - ".$this->tanggalCetak($akhir),'',1,'C');
        
        $fpdf -> Sety(6.4);
        $fpdf -> SetFont('Arial','B',10);
        $fpdf -> Cell(1,0.7,'NO','LRTB',0,'C');
        $fpdf -> Cell(2.2,0.7,'TANGGAL','LRTB',0,'C');
        $fpdf -> Cell(2.2,0.7,'TEMPAT','LRTB',0,'C');
        $fpdf -> Cell(9.5,0.7,'NAMA KEGIATAN','LRTB',0,'C');
        // $fpdf -> Cell(4,0.7,'SEKSI','LRTB',0,'C');
        $fpdf -> Cell(4.2,0.7,'PENANGGUNG JAWAB','LRTB',0,'C');
        $fpdf -> ln();
        

        $w = $this->db->query("SELECT * from perijinan_penggunaan_aula 
                          where status != 99                          
                          and tanggal_penggunaan >= '".$awal."'
                          and tanggal_penggunaan <= '".$akhir."'
                          order by tanggal_penggunaan asc
                          ");
        $no_urut=0;
        
        foreach($w->result() as $h){
            $no_urut++;
            $aula = $h->jenis_aula;
            if ($aula ==1) {
                $tempat = 'Aula Utama';
            }else{
                $tempat = 'Aula Rapat';
            }

            $nama = substr($h->nama_kegiatan, 0,60);

            $fpdf -> SetFont('Arial','',9);
            $fpdf -> Cell(1,0.7,$no_urut,'LRTB',0,'C');
            $fpdf -> Cell(2.2,0.7,$this->tanggalCetak($h->tanggal_penggunaan),'LRTB',0,'L');
            $fpdf -> Cell(2.2,0.7,' '.$tempat,'LRTB',0,'L');
            $fpdf -> Cell(9.5,0.7,$nama,'LRTB',0,'L');
            // $fpdf -> Cell(4,0.7,$h->seksi_penggunaan,'LRTB',0,'L');
            $fpdf -> Cell(4.2,0.7,$h->penanggung_jawab,'LRTB',0,'L');
            $fpdf -> ln();
        }
        
        
        

        

        

        $fpdf->Output();
    }

    public function tanggalCetak( $tgl ) {

        if ( substr($tgl, 2, 1) == '/' ) {
            $a=explode('/',$tgl);
        } else {
            $a=explode('-',$tgl);
            $d=$a[2];
            $m=$a[1];
            $y=$a[0];
            $a[0]=$m+0;
            $a[1]=$d;
            $a[2]=$y;
        }

        $nmbulan='';

        switch ( $a[0] ) {
            case 1 : $nmbulan   = 'Jan';    break;
            case 2 : $nmbulan   = 'Feb';   break;
            case 3 : $nmbulan   = 'Mar';      break;
            case 4 : $nmbulan   = 'Apr';      break;
            case 5 : $nmbulan   = 'Mei';        break;
            case 6 : $nmbulan   = 'Juni';       break;
            case 7 : $nmbulan   = 'Juli';       break;
            case 8 : $nmbulan   = 'Agus';    break;
            case 9 : $nmbulan   = 'Sept';  break;
            case 10: $nmbulan   = 'Okt';    break;
            case 11: $nmbulan   = 'Nov';   break;
            case 12: $nmbulan   = 'Des';   break;
        }

        $res=$a[1] . ' ' . $nmbulan . ' ' . $a['2'];
        return $res;

    }

    public function convert_number_to_words($number, $nd = false)   {
        $words[]='';
        if($number == 0 || $number == "")
        return "zero";
        if($number < 0)
        {
            $number = -$number;
            $words[] = "minus";
        }
        $thousands = array("", "ribu", "juta", "milyar", "triliun", "bilyun");
        $numbers[0] = array("", "seratus", "dua ratus", "tiga ratus", "empat ratus", "lima ratus", "enam ratus", "tujuh ratus", "delapan ratus", "sembilan ratus");
        $numbers[1] = array("", "sepuluh", "dua puluh", "tiga puluh", "empat puluh", "lima puluh", "enam puluh", "tujuh puluh", "delapan puluh", "sembilan puluh");
        $numbers[2] = array("se", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan",
        "", "sebelas", "dua belas", "tiga belas", "empat belas", "lima belas", "enam belas", "tujuh belas", "delapan belas", "sembilan belas");
        $translate[0] = array("", "seratus", "dua ratus", "tiga ratus", "empat ratus", "lima ratus", "enam ratus", "tujuh ratus", "delapan ratus", "sembilan ratus");
        $translate[1] = array("", "sepuluh", "dua puluh", "tiga puluh", "empat puluh", "lima puluh", "enam puluh", "tujuh puluh", "delapan puluh", "sembilan puluh");
        $translate[2] = array( "", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan",
        "", "sebelas", "dua belas", "tiga belas", "empat belas", "lima belas", "enam belas", "tujuh belas", "delapan belas", "sembilan belas");
        $a          = number_format($number, 2, ".", ",");
        $dec        = explode(".", $a);
        $desimal    = $dec[1]+0;
        $arr        = explode(",", $dec[0]);
        $arr = array_reverse($arr);
        for($key = count($arr)-1; $key >= 0; $key--)
        {
            $segment = $arr[$key];
            $segment = str_pad($segment, 3, "0", STR_PAD_LEFT);
            for($p = 0; $p < strlen($segment); $p++)
            {
                $s = substr($segment, $p, 1);
                $nexts = substr($segment, $p+1, 1);
                if($p == 1 && $s == 0 && $nexts != 0 && count($words) > 0)
                    {$words[] = "";}
                    if($s > 0)
                    {
                        $sub = 2 - $p;
                        if($p == 1 && $s == 1 && $nexts != 0)
                        {
                            //special handling for eleven to nineteen
                            $nset = $numbers[2];
                            $words[] = $nset[10 + $nexts];
                            break;
                        }
                        $nset = $numbers[$p];
                        $words[] = $nset[$s];
                    }
            }

            if($segment!="000") $words[] = ($thousands[$key] == "" ? "" : "".$thousands[$key]);
        }
        $result   = implode(" ", $words);
        if ( $desimal > 0 ) {
            $result  .= 'koma' . $this->convert_number_to_words($desimal);
        }
        return $result;
    }


}
