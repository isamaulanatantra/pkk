<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jenis_kesenian extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Jenis_kesenian_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'jenis_kesenian/home';
    $this->load->view('tes', $data);
   }
   
  public function perihal_surat(){
		$table = 'jenis_kesenian';
		$where   = array(
			'jenis_kesenian.status !=' => 99
			);
		$fields  = 
			"
			*
			";
		$order_by  = 'jenis_kesenian.judul_jenis_kesenian';
		echo json_encode($this->Crud_model->opsi($table, $where, $fields, $order_by));
  }
  public function perihal_surat_permohonan_organisasi(){
		$table = 'jenis_kesenian';
		$where   = array(
			'jenis_kesenian.status !=' => 99,
			'jenis_kesenian.keterangan' => 'permohonan_organisasi'
			);
		$fields  = 
			"
			*
			";
		$order_by  = 'jenis_kesenian.judul_jenis_kesenian';
		echo json_encode($this->Crud_model->opsi($table, $where, $fields, $order_by));
  }
  public function json_option(){
		$table = 'jenis_kesenian';
		$hak_akses = $this->session->userdata('hak_akses');
		$where   = array(
			'jenis_kesenian.status !=' => 99
			);
		$fields  = 
			"
			*
			";
		$order_by  = 'jenis_kesenian.judul_jenis_kesenian';
		echo json_encode($this->Crud_model->opsi($table, $where, $fields, $order_by));
  }
	public function json_all_jenis_kesenian(){
    $web=$this->uut->namadomain(base_url());
		$table = 'jenis_kesenian';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where = array(
      'jenis_kesenian.status !=' => 99,
      'jenis_kesenian.domain' => $web
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_jenis_kesenian="'.$row->id_jenis_kesenian.'" id="'.$row->id_jenis_kesenian.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$row->judul_jenis_kesenian.'</td>';
				echo '<td valign="top">'.$row->keterangan.'</td>';
				echo '<td valign="top">
				<a class="badge bg-warning btn-sm" href="'.base_url().'jenis_kesenian/cetak/?id_jenis_kesenian='.$row->id_jenis_kesenian.'" ><i class="fa fa-print"></i> Cetak</a>
				<a href="#tab_form_jenis_kesenian" data-toggle="tab" class="update_id badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			echo '<tr>';
				echo '<td valign="top" colspan="7" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'jenis_kesenian/cetak_xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
			echo '</tr>';
          
	}
  public function total_data()
  {
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'jenis_kesenian.status !=' => 99,
      'jenis_kesenian.status !=' => 999
      );
    $this->db->where($where0);
    $query0 = $this->db->get('jenis_kesenian');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_jenis_kesenian()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_jenis_kesenian', 'judul_jenis_kesenian', 'required');
		$this->form_validation->set_rules('isi_jenis_kesenian', 'isi_jenis_kesenian', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'judul_jenis_kesenian' => trim($this->input->post('judul_jenis_kesenian')),
        'temp' => trim($this->input->post('temp')),
        'icon' => trim($this->input->post('icon')),
        'isi_jenis_kesenian' => trim($this->input->post('isi_jenis_kesenian')),
        'keterangan' => trim($this->input->post('keterangan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'jenis_kesenian';
      $id         = $this->Jenis_kesenian_model->simpan_jenis_kesenian($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'jenis_kesenian',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
  public function update_jenis_kesenian()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_jenis_kesenian', 'judul_jenis_kesenian', 'required');
		$this->form_validation->set_rules('isi_jenis_kesenian', 'isi_jenis_kesenian', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'judul_jenis_kesenian' => trim($this->input->post('judul_jenis_kesenian')),
        'isi_jenis_kesenian' => trim($this->input->post('isi_jenis_kesenian')),
        'icon' => trim($this->input->post('icon')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'jenis_kesenian';
      $where       = array(
        'jenis_kesenian.id_jenis_kesenian' => trim($this->input->post('id_jenis_kesenian')),
        'jenis_kesenian.domain' => $web
			);
      $this->Jenis_kesenian_model->update_data_jenis_kesenian($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'jenis_kesenian.id_jenis_kesenian' => $this->input->post('id_jenis_kesenian'),
        'jenis_kesenian.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_jenis_kesenian');
      $result = $this->db->get('jenis_kesenian');
      echo json_encode($result->result_array());
		}
   
   public function total_jenis_kesenian()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('jenis_kesenian');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function cek_jenis_kesenian()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('master_jenis_kesenian');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'judul_jenis_kesenian' => $row->nama_master_jenis_kesenian,
          'domain' => $web
          );
        $this->db->from('jenis_kesenian');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'domain' => $web,
            'judul_jenis_kesenian' => $row->nama_master_jenis_kesenian,
            'isi_jenis_kesenian' => $row->isi_master_jenis_kesenian_default,
            'temp' => '-',
            'created_by' => 1,
            'created_time' => date('Y-m-d H:i:s'),
            'updated_by' => 1,
            'updated_time' => date('Y-m-d H:i:s'),
            'deleted_by' => 1,
            'deleted_time' => date('Y-m-d H:i:s'),
            'keterangan' => '-',
            'icon' => 'fa-home',
            'status' => 1
                    
            );
          $table_name = 'jenis_kesenian';
          $this->Jenis_kesenian_model->simpan_jenis_kesenian($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
  public function inaktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_jenis_kesenian = $this->input->post('id_jenis_kesenian');
      $where = array(
        'id_jenis_kesenian' => $id_jenis_kesenian
        );
      $this->db->from('jenis_kesenian');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'jenis_kesenian';
        if( $cek <> '' ){
          $this->Jenis_kesenian_model->update_data_jenis_kesenian($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_jenis_kesenian = $this->input->post('id_jenis_kesenian');
      $where = array(
        'id_jenis_kesenian' => $id_jenis_kesenian
        );
      $this->db->from('jenis_kesenian');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'jenis_kesenian';
        if( $cek <> '' ){
          $this->Jenis_kesenian_model->update_data_jenis_kesenian($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
    
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_jenis_kesenian = $this->input->post('id_jenis_kesenian');
      $where = array(
        'id_jenis_kesenian' => $id_jenis_kesenian,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('jenis_kesenian');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'jenis_kesenian';
        $this->Jenis_kesenian_model->update_data_jenis_kesenian($data_update, $where, $table_name);
        echo 1;
        }
		}
 }