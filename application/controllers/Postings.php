<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Postings extends CI_Controller
 {
    
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->library('Crud_model');
    $this->load->model('Posting_model');
    $this->load->model('Komponen_model');
    $this->load->model('Halaman_model');
    $this->load->model('Modul_model');
   }
  
  public function get_attachment_by_id_posting($id_posting)
   {
    $where = array(
      'id_tabel' => $id_posting
      );
    $d = $this->Posting_model->get_attachment_by_id_posting($where);
    if(!$d)
      {
        return 'logo_wonosobokab.png';
      }
    else
      {
        return $d['file_name'];
      }
   }
  
  public function index()
   {
    $url_modul = $this->uri->segment(4);
    $wheres = array(
						'alamat_url' => $url_modul,
            'status' => 1
						);
    $c = $this->Modul_model->get_data($wheres);
    if(!$c)
          {
						$data['nama_modul'] = 'kosong';
          }
        else
          {
						$data['nama_modul'] = $c['nama_modul'];
          }
    $web=$this->uut->namadomain(base_url());
    $a = 0;
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
    $data['menu_kiri'] = $this->menu_kiri(0,$h="", $a);
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }

    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
      
    $data['main_view'] = 'postings/home';
    $this->load->view('postings', $data);
   }
   
private function menu_atas_pkk($parent=NULL,$hasil, $a){
	$web=$this->uut->namadomain(base_url());
	$a = $a + 1;
	$w = $this->db->query("
	SELECT * from posting
	where posisi='menu_atas'
	and parent='".$parent."' 
	and status = 1 
	and tampil_menu_atas = 1 
	and domain='".$web."'
	order by urut
	");
	$x = $w->num_rows();
	if(($w->num_rows())>0)
	{
	if($parent == 0){
      $hasil .= '
      <ul id="responsivemenu">
      <li class="active"><a href="'.base_url().'"><i class="icon-home homeicon"></i><span class="showmobile">Home</span></a></li>
      ';
	}
	else{
  if($a>2){
  $hasil .= '
  <ul style="left: 100%; top: 10px; display: none;">
    ';
  }else{
  $hasil .= '
  <ul style="display: none;">
    ';
  }
	  }
	  }
	  $nomor = 0;
	  foreach($w->result() as $h)
	  {
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
	  $r = $this->db->query("
	  SELECT * from posting
	  where parent='".$h->id_posting."' 
	  and status = 1 
	  and tampil_menu_atas = 1 
	  and domain='".$web."'
	  order by urut
	  ");
	  $xx = $r->num_rows();
	  $nomor = $nomor + 1;
	  if( $a > 1 ){
			if ($xx == 0){
					if($h->judul_posting == 'POKJA 4'){
						$hasil .= '
						  <li><a href="'.base_url().'pkk/pokja4" role="button" aria-expanded="false" target="_blank"> '.$h->judul_posting.'</a>
						';
					}
        elseif($h->judul_posting == 'POKJA 1'){
						$hasil .= '
						  <li><a href="'.base_url().'pkk/pokja1" role="button" aria-expanded="false" target="_blank"> '.$h->judul_posting.'</a>
						';
					}
        elseif($h->judul_posting == 'POKJA 2'){
						$hasil .= '
						  <li><a href="'.base_url().'pkk/pokja2" role="button" aria-expanded="false" target="_blank"> '.$h->judul_posting.'</a>
						';
					}
        elseif($h->judul_posting == 'POKJA 3'){
						$hasil .= '
						  <li><a href="'.base_url().'pkk/pokja3" role="button" aria-expanded="false" target="_blank"> '.$h->judul_posting.'</a>
						';
					}
          else{
            $hasil .= '
              <li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> '.$h->judul_posting.' </a>
            ';
          }
			}
			else{
			$hasil .= '
        <li> <a href="">'.$h->judul_posting.' </a>
      ';
			} 
		}
		else{
      if ($xx == 0){
        if($h->judul_posting == 'SIM PKK'){
          $hasil .= '
            <li><a href="'.base_url().'login"> '.$h->judul_posting.' </a>
          ';
        }else{
          $hasil .= '
          <li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> '.$h->judul_posting.' </a>
          ';
        }
      }
      else{
        $hasil .= '
          <li> <a href="">'.$h->judul_posting.' </a>
        ';
      }
		}
		$hasil = $this->menu_atas_pkk($h->id_posting,$hasil, $a);
		$hasil .= '
	  </li>
	  ';
	  }
	  if(($w->num_rows)>0)
	  {
	  $hasil .= "
	</ul>
	";
	}
	else{
	}
	return $hasil;
}
  private function menu_mobile_pkk($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
					$hasil .= '<ul> <li><a href="'.base_url().'" role="button" aria-expanded="false">HOME</a></li>';
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
        $hasil .= '
          <li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
        }
			else{
				if($h->judul_posting == 'Berita'){
					$hasil .= '
					  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
				}
				elseif($h->judul_posting == 'Pengumuman'){
					$hasil .= '
					  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
				}
				elseif($h->judul_posting == 'Pengaduan Masyarakat'){
					$hasil .= '
				  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				elseif($h->judul_posting == 'Permohonan Informasi Publik'){
					$hasil .= '
				  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				elseif($h->judul_posting == 'Permohonan Informasi'){
					$hasil .= '
				  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				else{
				  if ($xx == 0){
					if($h->judul_posting == 'Berita'){
						$hasil .= '
						  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengumuman'){
						$hasil .= '
							<li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengaduan Masyarakat'){
						$hasil .= '
					  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi Publik'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
				  	else{
					$hasil .= '
					  <li><a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span></a>';
					  }
				  }
				  else{
					if($h->judul_posting == 'Berita'){
						$hasil .= '
						  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengumuman'){
						$hasil .= '
							<li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengaduan Masyarakat'){
						$hasil .= '
					  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi Publik'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
				  	else{
						$hasil .= '
						  <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
						} 
					  }
				}
      }
      
			$hasil = $this->menu_mobile_pkk($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  private function menu_atas_posting($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
			$hasil .= '
        <ul id="responsivemenu"> <li class="active"><a href="'.base_url().'welcome"><i class="icon-home homeicon"></i><span class="showmobile">Home</span></a></li>'; 
      }
      else{
        $hasil .= '
        <ul id="responsivemenu"> ';
      }
		}
      $nomor = 0;
      foreach($w->result() as $h)
      {
      
      $r = $this->db->query("
      SELECT * from posting
                    
      where parent='".$h->id_posting."' 
      and status = 1 
      and domain='".$web."'
      order by urut
      ");
      $xx = $r->num_rows();
      $nomor = $nomor + 1;
        if( $a > 1 ){
          $hasil .= '
          <li> <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a>';
        }
        else{
          if ($xx == 0){
          $hasil .= '
            <li> <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.'</a>';
          }
          else{
          $hasil .= '
            <li> <a>'.$h->judul_posting.'</a>';
          } 
        }
        $hasil = $this->menu_atas($h->id_posting,$hasil, $a);
        $hasil .= '</li>';
      }
		if(($w->num_rows)>0)
		{
			$hasil .= "
      </ul>";
		}
    else{      
    }
		
		return $hasil;
	}
  

private function menu_atas($parent=NULL,$hasil, $a){
	$web=$this->uut->namadomain(base_url());
	$a = $a + 1;
	$w = $this->db->query("
	SELECT * from posting
	where posisi='menu_atas'
	and parent='".$parent."' 
	and status = 1 
	and tampil_menu_atas = 1 
	and domain='".$web."'
	order by urut
	");
	$x = $w->num_rows();
	if(($w->num_rows())>0)
	{
	if($parent == 0){
	if($web == 'zb.wonosobokab.go.id'){
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'website" class="">HOME</a></li>
	'; 
	}
	elseif ($web == 'ppiddemo.wonosobokab.go.id'){
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'">HOME</a></li>
	<li class="parent">
	  <span class="">PPID </span>
	  <ul>
		<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid">PPID </a></span></li>
		<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid_pembantu">PPID Pembantu</a></span></li>
	  </ul>
	</li>
	';
	}
	else{
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'" class="">BERANDA</a></li>
	'; 
	}
	}
	else{
	$hasil .= '
	<ul>
	  ';
	  }
	  }
	  $nomor = 0;
	  foreach($w->result() as $h)
	  {
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
	  $r = $this->db->query("
	  SELECT * from posting
	  where parent='".$h->id_posting."' 
	  and status = 1 
	  and tampil_menu_atas = 1 
	  and domain='".$web."'
	  order by urut
	  ");
	  $xx = $r->num_rows();
	  $nomor = $nomor + 1;
	  if( $a > 1 ){
	  if ($xx == 0){
			if($h->judul_posting == 'Data Rekap Kunjungan Pasien'){
			$hasil .= '
		  <li><a href="'.base_url().'grafik/data_rekap_kunjungan_pasien"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Data Kunjungan Hari Ini'){
			$hasil .= '
		  <li><a href="'.base_url().'grafik"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Artikel'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Sitemap'){
			$hasil .= '
			<li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Informasi Publik'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Data Penyakit'){
			$hasil .= '
		  <li><a href="'.base_url().'grafik/data_penyakit"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}

			elseif($h->judul_posting == 'Data Pegawai'){
			$hasil .= '
		  <li><a href="'.base_url().'grafik/data_pegawai"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}

			elseif($h->judul_posting == 'Pengaduan Masyarakat'){
			$hasil .= '
		  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Permohonan Informasi Publik'){
			$hasil .= '
		  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Permohonan Informasi'){
			$hasil .= '
		  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			else{
        if($nomor < 21){
					$hasil .= '
					<li><a href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
        }
			}
		}
		else{
      if($xx > 3){
        $hasil .= '
        <li><a href="' . base_url() . 'postings/galeri/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span></a>';
      }else{
        $hasil .= '
          <li class="parent" > <span class="">' . $h->judul_posting . ' </span>';
      }
		} 
		}
		else{
		if($h->judul_posting == 'Artikel'){
		$hasil .= '
	  <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
		';
		}
		elseif($h->judul_posting == 'Sitemap'){
		$hasil .= '
		<li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
		';
		}
		elseif($h->judul_posting == 'Informasi Publik'){
		$hasil .= '
	  <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
		';
		}
		elseif($h->judul_posting == 'FAQ'){
		$hasil .= '
	  <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
		';
		}
		elseif($h->judul_posting == 'Info'){
		$hasil .= '
	  <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
		';
		}
		elseif($h->judul_posting == 'Pengaduan Masyarakat'){
			$hasil .= '
		  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
		elseif($h->judul_posting == 'Permohonan Informasi Publik'){
			$hasil .= '
		  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
		elseif($h->judul_posting == 'Permohonan Informasi'){
			$hasil .= '
		  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
		else{
		if ($xx == 0){
			if($h->judul_posting == 'Data Rekap Kunjungan Pasien'){
			$hasil .= '
		  <li><a href="http://180.250.150.76/simpus/charts/grafik_1b"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Data Penyakit'){
			$hasil .= '
		  <li><a href="http://180.250.150.76/simpus/charts/grafik_1d"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Artikel'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Sitemap'){
			$hasil .= '
			<li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Informasi Publik'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			else{
        if($nomor < 21){
          $hasil .= '
          <li><a href="'.base_url().'postings/categoris/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
        }
			}
		}
		else{
      if($nomor < 21){
					$hasil .= '
					<li><a href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
      }
		} 
		}
		}
		$hasil = $this->menu_atas($h->id_posting,$hasil, $a);
		$hasil .= '
	  </li>
	  ';
      
      $z = $this->db->query("
      SELECT * from posting
      where id_posting='".$parent."'
      ");    
      foreach($z->result() as $z1)
      { 
        if($z1->judul_posting == 'PPID' && $nomor == 4){            
        $hasil .= '
          <li>  <a href="#"> Produk Hukum </a>
            <ul>
              <li><a href="'.base_url().'peraturan/daftar_peraturan/?&id_peraturan=7&tentang=Peraturan_Daerah.html">Peraturan Daerah</a></li>
            </ul>
          </li>
        ';
        }
      }
	  }
	  if(($w->num_rows)>0)
	  {
	  $hasil .= "
	</ul>
	";
	}
	else{
	}
	return $hasil;
}
  
  public function get_komponen($judul_komponen)
   {
    $web=$this->uut->namadomain(base_url());
     $where = array(
						'judul_komponen' => $judul_komponen,
						'status' => 1
						);
    $d = $this->Komponen_model->get_data($where);
    if(!$d)
          {
						return '';
          }
        else
          {
						return $d['isi_komponen'];
          }
   }
   
  public function detail()
   {
    $url_modul = $this->uri->segment(4);
    $wheres = array(
						'alamat_url' => $url_modul,
            'status' => 1
						);
    $c = $this->Modul_model->get_data($wheres);
    if(!$c)
          {
						$data['nama_modul'] = 'kosong';
          }
        else
          {
						$data['nama_modul'] = $c['nama_modul'];
          }
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
			
          
    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['tampil_menu_atas'] = '';
						$data['kata_kunci'] = '';
						$data['gambar'] = 'logo wonosobo.png';
						$data['pembuat'] = '';
						$data['created_time'] = '';
						$data['pengedit'] = '';
						$data['updated_time'] = '';
          }
        else
          {
						$data['tampil_menu_atas'] = $d['tampil_menu_atas'];
						$data['judul_posting'] = $d['judul_posting'];
						$data['isi_posting'] = ''.$d['isi_posting'].' ';
						$data['kata_kunci'] = $d['kata_kunci'];
						$data['pembuat'] = $d['pembuat'];
						// $data['created_time'] = $d['created_time'];
						$data['created_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['created_time']).'';
						$data['pengedit'] = $d['pengedit'];
						// $data['updated_time'] = $d['updated_time'];
						$data['updated_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['updated_time']).'';
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
    $r = $this->uri->segment(5);
    $p=$this->uri->segment(3);
    //PAGINATION
    $page =  $r;
    $noPage =  $page;
    $showPage = 0;
    $s = $this->db->query("SELECT COUNT(*) as jumlah_data from posting where parent='".$this->uri->segment(3)."' ");
    $jumPage = $s->row("jumlah_data")/10;

    $url = 'https://'.$web.'/postings/details/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'';

    $pagination_top = '
    <div class="text-right">
      <ul class="pagination">
    ';
    $pagination_bottom = '
    <div class="text-center">
      <ul class="pagination">
    ';

    //menampilkan link halaman awal
    if ($page != 1 || empty($page)) { $pagination_bottom .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>'; }
 
    // menampilkan link previous
 
    if ($page > 1) { $pagination_bottom .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>'; }
 
    // memunculkan nomor halaman dan linknya
    for($page = 1; $page <= $jumPage; $page++)
    {
         if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) 
         {   
            if (($showPage == 1) && ($page != 2))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top.= '<li><a href="#">...</a></li>'; }
            if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top .= '<li><a href="#">...</a></li>'; }
            if ($page == $r) { $pagination_bottom .= '<li class="active"><a href="#">'.$page.'</a></li>';$pagination_top.= '<li class="active"><a href="#">'.$page.'</a></li>'; }
            else { $pagination_bottom .= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>';$pagination_top.= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>'; }
            $showPage = $page;          
         }
    }
 
    // menampilkan link next
 
    if ($noPage < $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>'; }
 
    //menampilkan link halaman akhir
    if ($noPage != $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>'; }
 
    $pagination_top .= '
      </ul>
    </div>
    ';
    $pagination_bottom .= '
      </ul>
    </div>
    ';
    $a = 0;
    $r = $this->uri->segment(5);
    $p=$this->uri->segment(3);
    $data['isi_halaman'] = $this->option_posting_terbarukan($p,$q="", $r);
    $data['menu_kiri'] = $this->menu_kiri(0,$h="", $a);
    $data['welcome1'] = $this->get_komponen('welcome1');
    $data['welcome2'] = $this->get_komponen('welcome2');
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $data['slide_show'] = $this->get_komponen('slide_show');
    if($web=='zb.wonosobokab.go.id'){ 
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['menu_atas_posting'] = $this->menu_atas_posting(0,$h="", $a);
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'postings/detail';
      $this->load->view('postings', $data);
    }
    elseif($web=='pkk.wonosobokab.go.id'){
      $data['menu_atas'] = $this->menu_atas_pkk(0,$h="", $a);
      $data['menu_mobile'] = $this->menu_mobile_pkk(0,$h="", $a);
			$data['pagination_top'] = $pagination_top;
			$data['pagination_bottom'] = $pagination_bottom;
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'postings/detail_pkk';
      $this->load->view('pkks', $data);
    }
    elseif($web=='pkk.wonosobokab.go.id'){
      $data['menu_atas'] = $this->menu_atas_pkk(0,$h="", $a);
      $data['menu_mobile'] = $this->menu_mobile_pkk(0,$h="", $a);
			$data['pagination_top'] = $pagination_top;
			$data['pagination_bottom'] = $pagination_bottom;
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'postings/detail_pkk';
      $this->load->view('pkks', $data);
    }
    else{
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['menu_atas_posting'] = $this->menu_atas_posting(0,$h="", $a);
			$data['pagination_top'] = $pagination_top;
			$data['pagination_bottom'] = $pagination_bottom;
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'postings/detail';
      $this->load->view('postings', $data);
    }
    //$this->load->view('backbone_postings', $data);
   }
  
  private function option_posting_terbarukan($parent,$hasil, $r){
    $web=$this->uut->namadomain(base_url());
    if(empty($r) || $r == 1 || $r < 1){
       $offset = 0;
    }elseif($r > 1){
      $offset = ($r*10);
    }
		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
    and posisi='menu_atas'
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",10
		");
		$x = $w->num_rows();
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$rs = $this->db->query("
			SELECT * from posting
			where parent='".$h->id_posting."' 
			and posisi='menu_atas'
			and status = 1 
			and domain='".$web."'
			order by created_time desc
			limit ".$offset.",12
			");
			$xx = $rs->num_rows();

			$hasil = $this->option_posting_terbarukan($h->id_posting,$hasil, $r);
			if ($xx == 0){
				$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
				$yummy   = array("_","","","","","","","","","","","","","");
				$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
				$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
				$hasil .= 
				'
				<div class="row">
					<div class="col-md-4 col-sm-4">
						<!-- BEGIN CAROUSEL -->            
						<div class="front-carousel">
							<div class="carousel slide" id="myCarousel">
								<!-- Carousel items -->
								<div class="carousel-inner">
									<div class="item active">
										<img alt="" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'">
									</div>
								</div>
								<!-- Carousel nav -->
								<a data-slide="prev" href="#myCarousel" class="carousel-control left">
									<i class="fa fa-angle-left"></i>
								</a>
								<a data-slide="next" href="#myCarousel" class="carousel-control right">
									<i class="fa fa-angle-right"></i>
								</a>
							</div>                
						</div>
						<!-- END CAROUSEL -->             
					</div>
					<div class="col-md-8 col-sm-8">
						<h2><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></h2>
						<ul class="blog-info">
							<li><i class="fa fa-calendar"></i> '.$created_time.'</li>
						</ul>
						<p>'.substr(strip_tags($h->isi_posting), 0, 200).'</p>
						<a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="more">Baca Selengkapnya <i class="icon-angle-right"></i></a>
					</div>
				</div>
				<hr class="blog-post-sep">
				';
			}
		}

		return $hasil;
	}
  private function posting($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    order by urut asc, created_time desc limit 10
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
        $hasil .= '<ul class="list-group sidebar-nav" id="sidebar-nav">';
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
		and status = 1 
    and domain='".$web."'
    order by urut
    
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
          if ($xx == 0){
            $hasil .= '<li class="list-group-item"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'">'.$h->judul_posting.' </a>';
            }
          else{
            $hasil .= '<li class="list-group-item list-toggle"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'"><i class="fa '.$h->icon.'"></i>  '.$h->judul_posting.' </a>';
            }
        }
			else{
          if ($xx == 0){
            $hasil .= '<li class="list-group-item"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'">  '.$h->judul_posting.' </a>';
            }
          else{
            $hasil .= '<li class="list-group-item list-toggle"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'"><i class="fa '.$h->icon.'"></i>  '.$h->judul_posting.' </a>';
            }
        }
			$hasil = $this->posting($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= '</ul>';
		}
    else{
      
    }
		
		return $hasil;
	}
  
  private function menu_kiri($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    and tampil_menu = 1
    and posisi='menu_kiri'
    order by urut asc, created_time desc limit 10
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
        $hasil .= '<ul class="list-group sidebar-nav" id="sidebar-nav">';
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
		and status = 1 
    and domain='".$web."'
    and tampil_menu = 1
    and posisi='menu_kiri'
    order by urut
    
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
          if ($xx == 0){
            $hasil .= '<li class="list-group-item"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'">'.$h->judul_posting.' </a>';
            }
          else{
            $hasil .= '<li class="list-group-item list-toggle"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'"><i class="fa '.$h->icon.'"></i>  '.$h->judul_posting.' </a>';
            }
        }
			else{
          if ($xx == 0){
            $hasil .= '<li class="list-group-item"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'">  '.$h->judul_posting.' </a>';
            }
          else{
            $hasil .= '<li class="list-group-item list-toggle"><a href="'.base_url().'postings/categories/'.$h->id_posting.'/'.url_title($h->judul_posting).'"><i class="fa '.$h->icon.'"></i>  '.$h->judul_posting.' </a>';
            }
        }
			$hasil = $this->menu_kiri($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= '</ul>';
		}
    else{
      
    }
		
		return $hasil;
	}
  
  public function categories()
   {
    $web=$this->uut->namadomain(base_url());
    $a = 0;
    $r = $this->uri->segment(5);
    $p=$this->uri->segment(3);
    $data['isi_halaman'] = $this->posting_categories($p,$q="", $r);
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
    $data['menu_atas_posting'] = $this->menu_atas_posting(0,$h="", $a);
    $data['menu_kiri'] = $this->menu_kiri(0,$h="", $a);
    $data['welcome1'] = $this->get_komponen('welcome1');
    $data['welcome2'] = $this->get_komponen('welcome2');
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $data['slide_show'] = $this->get_komponen('slide_show');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }


    //PAGINATION
    $page =  $r;
    $noPage =  $page;
    $showPage = 0;
    $s = $this->db->query("SELECT COUNT(*) as jumlah_data from posting where parent='".$this->uri->segment(3)."' ");
    $jumPage = $s->row("jumlah_data")/10;

    $url = 'https://'.$web.'/postings/categories/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'';

    $pagination_top = '
    <div class="text-right">
      <ul class="pagination">
    ';
    $pagination_bottom = '
    <div class="text-center">
      <ul class="pagination">
    ';

    //menampilkan link halaman awal
    if ($page != 1 || empty($page)) { $pagination_bottom .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>'; }
 
    // menampilkan link previous
 
    if ($page > 1) { $pagination_bottom .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>'; }
 
    // memunculkan nomor halaman dan linknya
    for($page = 1; $page <= $jumPage; $page++)
    {
         if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) 
         {   
            if (($showPage == 1) && ($page != 2))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top.= '<li><a href="#">...</a></li>'; }
            if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top .= '<li><a href="#">...</a></li>'; }
            if ($page == $r) { $pagination_bottom .= '<li class="active"><a href="#">'.$page.'</a></li>';$pagination_top.= '<li class="active"><a href="#">'.$page.'</a></li>'; }
            else { $pagination_bottom .= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>';$pagination_top.= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>'; }
            $showPage = $page;          
         }
    }
 
    // menampilkan link next
 
    if ($noPage < $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>'; }
 
    //menampilkan link halaman akhir
    if ($noPage != $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>'; }
 
 

    $pagination_top .= '
      </ul>
    </div>
    ';
    $pagination_bottom .= '
      </ul>
    </div>
    ';
    $data['pagination_top'] = $pagination_top;
    $data['pagination_bottom'] = $pagination_bottom;
    $data['main_view'] = 'postings/categories';
    $this->load->view('postings', $data);
    //$this->load->view('backbone_postings', $data);
   }
   
  private function posting_categories($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
    if(empty($r) || $r == 1 || $r < 1)
    {
       $offset = 0;
    }elseif($r > 1){
      $offset = ($r*10);
    }

		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",10
		");
		$x = $w->num_rows();
		$nomor = 0;

		foreach($w->result() as $h)
		{ 
    
    $rs = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",10
		");
		$xx = $rs->num_rows();

    $hasils = $this->posting_categories($h->id_posting,$hasils, $r);
			if ($xx == 0){
            $hasils .= 
            '
            <div class="blog-post padding-bottom-20">
              <div class="blog-item-header">
                  <h2>'.$h->judul_posting.'</h2>
              </div>
              <div class="blog">
                  <div class="clearfix"></div>
                  <div class="blog-post-body row margin-top-15">
                      <div class="col-md-5">
                          <img class="margin-bottom-20" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="'.$h->judul_posting.'">
                      </div>
                      <div class="col-md-7">
                          <p><a href="#">'.$h->created_time.'</a></p>
                          <p>
                          '.substr(strip_tags($h->isi_posting), 0, 100).'
                          </p>
                          <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.url_title($h->judul_posting).'" class="btn btn-primary"><i class="icon-chevron-right readmore-icon"></i> Baca Detail</a>
                      </div>
                  </div>
              </div>
            </div>
            ';
            }

			
      
		}

		return $hasils;
	}
  
  public function faqs()
   {
    $web=$this->uut->namadomain(base_url());
    $a = 0;
    $r = $this->uri->segment(5);
    $p=$this->uri->segment(3);
    $data['isi_halaman'] = $this->posting_faqs($p,$q="", $r);
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
    $data['menu_atas_posting'] = $this->menu_atas_posting(0,$h="", $a);
    $data['menu_kiri'] = $this->posting(0,$h="", $a);
    $data['welcome1'] = $this->get_komponen('welcome1');
    $data['welcome2'] = $this->get_komponen('welcome2');
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $data['slide_show'] = $this->get_komponen('slide_show');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }

    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['kata_kunci'] = '';
						$data['gambar'] = 'blankgambar.jpg';
          }
        else
          {
						$data['judul_posting'] = $d['judul_posting'];
						$data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
						$data['kata_kunci'] = $d['kata_kunci'];
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }

    //PAGINATION
    $page =  $r;
    $noPage =  $page;
    $showPage = 0;
    $s = $this->db->query("SELECT COUNT(*) as jumlah_data from posting where parent='".$this->uri->segment(3)."' ");
    $jumPage = $s->row("jumlah_data")/10;

    $url = 'https://'.$web.'/postings/faqs/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'';

    $pagination_top = '
    <div class="text-right">
      <ul class="pagination">
    ';
    $pagination_bottom = '
    <div class="text-center">
      <ul class="pagination">
    ';

    //menampilkan link halaman awal
    if ($page != 1 || empty($page)) { $pagination_bottom .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>'; }
 
    // menampilkan link previous
 
    if ($page > 1) { $pagination_bottom .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>'; }
 
    // memunculkan nomor halaman dan linknya
    for($page = 1; $page <= $jumPage; $page++)
    {
         if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) 
         {   
            if (($showPage == 1) && ($page != 2))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top.= '<li><a href="#">...</a></li>'; }
            if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top .= '<li><a href="#">...</a></li>'; }
            if ($page == $r) { $pagination_bottom .= '<li class="active"><a href="#">'.$page.'</a></li>';$pagination_top.= '<li class="active"><a href="#">'.$page.'</a></li>'; }
            else { $pagination_bottom .= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>';$pagination_top.= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>'; }
            $showPage = $page;          
         }
    }
 
    // menampilkan link next
 
    if ($noPage < $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>'; }
 
    //menampilkan link halaman akhir
    if ($noPage != $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>'; }
 
    $pagination_top .= '
      </ul>
    </div>
    ';
    $pagination_bottom .= '
      </ul>
    </div>
    ';
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
    $data['pagination_top'] = $pagination_top;
    $data['pagination_bottom'] = $pagination_bottom;
    $data['main_view'] = 'postings/faqs';
    $this->load->view('halaman', $data);
    // $this->load->view('postings', $data);
    // $this->load->view('backbone_postings', $data);
   }
   
  private function posting_faqs($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
    if(empty($r) || $r == 1 || $r < 1)
    {
       $offset = 0;
    }elseif($r > 1){
      $offset = ($r*10);
    }

		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
    and posisi='menu_atas'
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",10
		");
		$x = $w->num_rows();
		$nomor = 0;

		foreach($w->result() as $h)
		{ 
    
    $rs = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
    and posisi='menu_atas'
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",12
		");
		$xx = $rs->num_rows();

    $hasils = $this->posting_category($h->id_posting,$hasils, $r);
			if ($xx == 0){
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
        $yummy   = array("_","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
        $hasils .= 
        '
                    <div class="media">                    
                      <a href="javascript:;" class="pull-left">
                      <img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="" class="media-object" alt="'.$h->judul_posting.'">
                      </a>
                      <div class="media-body">
                        <h4 class="media-heading"><a href="'.base_url().'postings/faqs/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.' </a></h4>
                        <p>'.substr(strip_tags($h->isi_posting), 0, 100).'</p>
                      </div>
                    </div>
                    <br />
        ';
        }
		}

		return $hasils;
	}
  
  public function category()
   {
    $web=$this->uut->namadomain(base_url());
    $a = 0;
    $r = $this->uri->segment(5);
    $p=$this->uri->segment(3);
    $data['isi_halaman'] = $this->posting_category($p,$q="", $r);
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
    $data['menu_atas_posting'] = $this->menu_atas_posting(0,$h="", $a);
    $data['menu_kiri'] = $this->posting(0,$h="", $a);
    $data['welcome1'] = $this->get_komponen('welcome1');
    $data['welcome2'] = $this->get_komponen('welcome2');
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $data['slide_show'] = $this->get_komponen('slide_show');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }


    //PAGINATION
    $page =  $r;
    $noPage =  $page;
    $showPage = 0;
    $s = $this->db->query("SELECT COUNT(*) as jumlah_data from posting where parent='".$this->uri->segment(3)."' ");
    $jumPage = $s->row("jumlah_data")/10;

    $url = 'https://'.$web.'/postings/categories/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'';

    $pagination_top = '
    <div class="text-right">
      <ul class="pagination">
    ';
    $pagination_bottom = '
    <div class="text-center">
      <ul class="pagination">
    ';

    //menampilkan link halaman awal
    if ($page != 1 || empty($page)) { $pagination_bottom .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>'; }
 
    // menampilkan link previous
 
    if ($page > 1) { $pagination_bottom .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>'; }
 
    // memunculkan nomor halaman dan linknya
    for($page = 1; $page <= $jumPage; $page++)
    {
         if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) 
         {   
            if (($showPage == 1) && ($page != 2))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top.= '<li><a href="#">...</a></li>'; }
            if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top .= '<li><a href="#">...</a></li>'; }
            if ($page == $r) { $pagination_bottom .= '<li class="active"><a href="#">'.$page.'</a></li>';$pagination_top.= '<li class="active"><a href="#">'.$page.'</a></li>'; }
            else { $pagination_bottom .= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>';$pagination_top.= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>'; }
            $showPage = $page;          
         }
    }
 
    // menampilkan link next
 
    if ($noPage < $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>'; }
 
    //menampilkan link halaman akhir
    if ($noPage != $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>'; }
 
 

    $pagination_top .= '
      </ul>
    </div>
    ';
    $pagination_bottom .= '
      </ul>
    </div>
    ';
    $data['pagination_top'] = $pagination_top;
    $data['pagination_bottom'] = $pagination_bottom;
    $data['main_view'] = 'postings/categories';
    $this->load->view('postings', $data);
    //$this->load->view('backbone_postings', $data);
   }
   
  public function galeri()
   {
    $web=$this->uut->namadomain(base_url());
    $a = 0;
    $r = $this->uri->segment(6);
    $p=$this->uri->segment(3);
		$pencarian = $this->input->post('cari');
    $data['isi_halaman'] = $this->pencarian_galeri($p, $q="", $r, $pencarian);
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
    $data['menu_atas_posting'] = $this->menu_atas_posting(0,$h="", $a);
    $data['menu_kiri'] = $this->posting(0,$h="", $a);
    $data['welcome1'] = $this->get_komponen('welcome1');
    $data['welcome2'] = $this->get_komponen('welcome2');
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $data['slide_show'] = $this->get_komponen('slide_show');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }


    //PAGINATION
    $page =  $r;
    $noPage =  $page;
    $showPage = 0;
    $s = $this->db->query("SELECT COUNT(*) as jumlah_data from posting where parent='".$this->uri->segment(3)."' and status=1 and domain='".$web."' and judul_posting like '%".$pencarian."%'");
    $jumlah_pencarian = $s->row("jumlah_data");
    $jumPage = $s->row("jumlah_data")/10;

    $url = 'https://'.$web.'/pencarian/galeri/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'';

    $pagination_top = '
    <div class="text-right">
      <ul class="pagination">
    ';
    $pagination_bottom = '
    <div class="text-center">
      <ul class="pagination">
    ';

    //menampilkan link halaman awal
    if ($page != 1 || empty($page)) { $pagination_bottom .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>'; }
 
    // menampilkan link previous
 
    if ($page > 1) { $pagination_bottom .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>'; }
 
    // memunculkan nomor halaman dan linknya
    for($page = 1; $page <= $jumPage; $page++)
    {
         if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) 
         {   
            if (($showPage == 1) && ($page != 2))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top.= '<li><a href="#">...</a></li>'; }
            if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top .= '<li><a href="#">...</a></li>'; }
            if ($page == $r) { $pagination_bottom .= '<li class="active"><a href="#">'.$page.'</a></li>';$pagination_top.= '<li class="active"><a href="#">'.$page.'</a></li>'; }
            else { $pagination_bottom .= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>';$pagination_top.= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>'; }
            $showPage = $page;          
         }
    }
 
    // menampilkan link next
 
    if ($noPage < $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>'; }
 
    //menampilkan link halaman akhir
    if ($noPage != $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>'; }
 
    $pagination_top .= '
      </ul>
    </div>
    ';
    $pagination_bottom .= '
      </ul>
    </div>
    ';
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
    $data['pagination_top'] = $pagination_top;
    $data['pagination_bottom'] = $pagination_bottom;
    $data['pencarian'] = $pencarian;
    $data['jumlah_pencarian'] = $jumlah_pencarian;
    $data['main_view'] = 'postings/pencarian';
    $this->load->view('postings', $data);
   }
   
  private function pencarian_galeri($parent, $hasils, $r, $pencarian){
    $web=$this->uut->namadomain(base_url());
    if($r > 1){
      $offset = ($r*10);
    }else{
			$offset = 0;
		}

		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
    and posisi='menu_atas'
		and status = 1 
    and domain='".$web."'
		and judul_posting
		like '%".$pencarian."%'
    order by created_time desc
    limit ".$offset.",10
		");
		$x = $w->num_rows();
		$nomor = 0;

		foreach($w->result() as $h)
		{ 
    
    $rs = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
		");
		$xx = $rs->num_rows();

    $hasils = $this->pencarian_galeri($h->id_posting, $hasils, $r, $pencarian);
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = $this->Crud_model->dateBahasaIndo1($h->created_time);
            $hasils .= 
            '
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<!-- BEGIN CAROUSEL -->            
								<div class="front-carousel">
									<div class="carousel slide" id="myCarousel">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<div class="item active">
												<img alt="" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'">
											</div>
										</div>
										<!-- Carousel nav -->
										<a data-slide="prev" href="#myCarousel" class="carousel-control left">
											<i class="fa fa-angle-left"></i>
										</a>
										<a data-slide="next" href="#myCarousel" class="carousel-control right">
											<i class="fa fa-angle-right"></i>
										</a>
									</div>                
								</div>
								<!-- END CAROUSEL -->             
							</div>
							<div class="col-md-8 col-sm-8">
								<h2><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></h2>
								<p>'.substr(strip_tags($h->isi_posting), 0, 200).'</p>
								<i class="fa fa-calendar"></i> '.$created_time.'
								<a class="btn btn-info" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="more"><i class="fa fa-eye"></i> Baca Selengkapnya <i class="icon-angle-right"></i></a>
								
							</div>
						</div>
						<hr class="blog-post-sep">
            ';
		}

		return $hasils;
	}
  public function sitemap()
   {
    $web=$this->uut->namadomain(base_url());
    $a = 0;
    $r = $this->uri->segment(5);
    $p=$this->uri->segment(3);
    $data['isi_halaman'] = $this->posting_sitemap($p,$q="", $r);
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
    $data['menu_atas_posting'] = $this->menu_atas_posting(0,$h="", $a);
    $data['menu_kiri'] = $this->posting(0,$h="", $a);
    $data['welcome1'] = $this->get_komponen('welcome1');
    $data['welcome2'] = $this->get_komponen('welcome2');
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $data['slide_show'] = $this->get_komponen('slide_show');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }


    //PAGINATION
    $page =  $r;
    $noPage =  $page;
    $showPage = 0;
    $s = $this->db->query("SELECT COUNT(*) as jumlah_data from posting where status=1 ");
    $jumPage = $s->row("jumlah_data")/10;

    $url = 'https://'.$web.'/postings/sitemap/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'';

    $pagination_top = '
    <div class="text-right">
      <ul class="pagination">
    ';
    $pagination_bottom = '
    <div class="text-center">
      <ul class="pagination">
    ';

    //menampilkan link halaman awal
    if ($page != 1 || empty($page)) { $pagination_bottom .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>'; }
 
    // menampilkan link previous
 
    if ($page > 1) { $pagination_bottom .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>'; }
 
    // memunculkan nomor halaman dan linknya
    for($page = 1; $page <= $jumPage; $page++)
    {
         if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) 
         {   
            if (($showPage == 1) && ($page != 2))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top.= '<li><a href="#">...</a></li>'; }
            if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top .= '<li><a href="#">...</a></li>'; }
            if ($page == $r) { $pagination_bottom .= '<li class="active"><a href="#">'.$page.'</a></li>';$pagination_top.= '<li class="active"><a href="#">'.$page.'</a></li>'; }
            else { $pagination_bottom .= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>';$pagination_top.= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>'; }
            $showPage = $page;          
         }
    }
 
    // menampilkan link next
 
    if ($noPage < $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>'; }
 
    //menampilkan link halaman akhir
    if ($noPage != $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>'; }
 
    $pagination_top .= '
      </ul>
    </div>
    ';
    $pagination_bottom .= '
      </ul>
    </div>
    ';
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
    $data['pagination_top'] = $pagination_top;
    $data['pagination_bottom'] = $pagination_bottom;
    $data['main_view'] = 'postings/sitemap';
    $this->load->view('postings', $data);
    // $this->load->view('postings', $data);
    // $this->load->view('backbone_postings', $data);
   }
   
  function load_sitemap()
	{
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		echo $this->posting_sitemap(0,$h="", $a);
	}
  private function posting_sitemap($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where parent='".$parent."' 
    and domain='".$web."'
    and status=1
		order by posisi, urut
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$created_time = $this->Crud_model->dateBahasaIndo1($h->created_time);
			$nomor = $nomor + 1;
			$hasil .= '<tr id_posting="'.$h->id_posting.'" id="'.$h->id_posting.'" >';
      if( $a > 1 ){
        $hasil .= '<td style="padding: 2px '.($a * 20).'px ;"> <a target="_blank" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a> </td>';
        }
			else{
        $hasil .= '<td> <a target="_blank" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.'</a> </td>';
        }
			$hasil .= '<td> '.$created_time.'&nbsp; </td>';
      
			$hasil .= '</tr>';
			$hasil = $this->posting_sitemap($h->id_posting,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  private function posting_category($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
    if(empty($r) || $r == 1 || $r < 1)
    {
       $offset = 0;
    }elseif($r > 1){
      $offset = ($r*10);
    }

		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
    and posisi='menu_atas'
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",10
		");
		$x = $w->num_rows();
		$nomor = 0;

		foreach($w->result() as $h)
		{ 
    
    $rs = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
    and posisi='menu_atas'
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",10
		");
		$xx = $rs->num_rows();

    $hasils = $this->posting_category($h->id_posting,$hasils, $r);
			if ($xx == 0){
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
        $yummy   = array("_","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
        $hasils .= 
        '

          <li class="list-group-item">
              <div class="row">
                  <div class="col-md-2 profile-thumb">
                      <a href="'.base_url().'postings/faqs/'.$h->id_posting.'/'.$newphrase.'.HTML">
                          <img class="media-object" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="'.$h->judul_posting.'">
                      </a>
                  </div>
                  <div class="col-md-10">
                      <h4><a href="'.base_url().'postings/faqs/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></h4>
                      <p>'.substr(strip_tags($h->isi_posting), 0, 350).'</p>
                      <a href="'.base_url().'postings/faqs/'.$h->id_posting.'/'.$newphrase.'.HTML" class="btn btn-primary btn-sm">Baca Selengkapnya</a>
                  </div>
              </div>
          </li>
        ';
      }

			
      
		}

		return $hasils;
	}
  
  public function informasi_lainnya()
   {
    $web=$this->uut->namadomain(base_url());
    $r = 0;
    $informasi_terkait = $this->list_informasi_lainnya(0, $h="", $r);
    echo $informasi_terkait;
   }
  
  function getFileType( $file ) {
    return image_type_to_mime_type( exif_imagetype( $file ) );
  }

  private function list_informasi_lainnya($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    order by created_time desc limit 7
		");
		$x = $w->num_rows();
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $rs = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
		and status = 1 
    and domain='".$web."'
    order by created_time desc limit 7
		");
		$xx = $rs->num_rows();
    $hasils = $this->list_informasi_lainnya($h->id_posting,$hasils, $r);
    if( $h->highlight == 1 ){
			if ($xx == 0){
                                $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
                                $yummy   = array("_","","","","","","","","","","","","","");
                                $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
        
            $filename = './media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'';
            if (file_exists($filename)) {
              $x= 's_'.$this->get_attachment_by_id_posting($h->id_posting).'';
            } else {
              $x= 'blankgambar.jpg';
            }
            $hasils .= 
            '
                    <div class="row margin-bottom-10">
                      <div class="col-md-3">
                        <img class="img-responsive" alt="'.$h->judul_posting.'" src="'.base_url().'media/upload/'.$x.'">                        
                      </div>
                      <div class="col-md-9 recent-news-inner">
                        <h3><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></h3>
                        <p>'.substr(strip_tags($h->isi_posting), 0, 100).'</p>
                      </div>                        
                    </div>
            ';
            }
      }
		}
		return $hasils;
	}
  
  public function informasi_terkait()
   {
    $web=$this->uut->namadomain(base_url());
    $parent = $this->input->post('parent');
    $r = 0;
    $informasi_terkait = $this->list_informasi_terkait($parent, $h="", $r);
    echo $informasi_terkait;
   }
  
  public function informasi_terkait_by_id()
   {
    $web=$this->uut->namadomain(base_url());
    $id_posting = $this->input->post('parent');
    $where = array(
						'id_posting' => $this->input->post('parent')
						);
    $d = $this->Posting_model->get_data_informasi_terkait_by_id($where);
    if(!$d)
          {
						$parent = 0;
          }
        else
          {
						$parent = $d['parent'];
          }
    $r = 0;
    $informasi_terkait = $this->list_informasi_terkait($parent, $h="", $r);
    echo $informasi_terkait;
   }
   
  private function list_informasi_terkait($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."'
		and status = 1 
    and domain='".$web."'
    order by created_time desc limit 7
		");
		$x = $w->num_rows();
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $rs = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
		and status = 1 
    and domain='".$web."'
    order by created_time desc limit 10
		");
		$xx = $rs->num_rows();
    $hasils = $this->list_informasi_terkait($h->id_posting,$hasils, $r);
			if ($xx == 0){
                                $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
                                $yummy   = array("_","","","","","","","","","","","","","");
                                $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
            $filename = './media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'';
            if (file_exists($filename)) {
              $x= 's_'.$this->get_attachment_by_id_posting($h->id_posting).'';
            } else {
              $x= 'logo wonosobo.png';
            }
            $hasils .= 
            '
                    <div class="row margin-bottom-10">
                      <div class="col-md-3">
                        <img class="img-responsive" alt="'.$h->judul_posting.'" src="'.base_url().'media/upload/'.$x.'">                        
                      </div>
                      <div class="col-md-9 recent-news-inner">
                        <h3><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></h3>
                        <p>'.substr(strip_tags($h->isi_posting), 0, 100).'</p>
                      </div>                        
                    </div>
            ';
            }
		}
		return $hasils;
	}
  
  public function details()
   {
    $url_modul = $this->uri->segment(4);
    $wheres = array(
						'alamat_url' => $url_modul,
            'status' => 1
						);
    $c = $this->Modul_model->get_data($wheres);
    if(!$c)
          {
						$data['nama_modul'] = 'kosong';
          }
        else
          {
						$data['nama_modul'] = $c['nama_modul'];
          }
    $web=$this->uut->namadomain(base_url());
    $a = 0;
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }

    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['kata_kunci'] = '';
						$data['created_time'] = '';
						$data['gambar'] = 'blankgambar.jpg';
          }
        else
          {
						$data['judul_posting'] = $d['judul_posting'];
						$data['created_time'] = $this->Crud_model->dateBahasaIndo1($d['created_time']);
						// $data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
						$data['isi_posting'] = ''.$d['isi_posting'].'';
						$data['kata_kunci'] = $d['kata_kunci'];
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }

    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
      
    if($web=='pmi.wonosobokab.go.id'){
      $data['menu_atas'] = $this->menu_atas_pmi(0,$h="", $a);
      $data['menu_kiri'] = $this->menu_kiri_pmi(0,$h="", $a);
      $data['terbarukan'] = $this->option_posting_terbarukan_pmi($h="", $a);
      $data['main_view'] = 'postings/details_pmi';
      $this->load->view('pmi_postings', $data);
    }else{
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['menu_kiri'] = $this->menu_kiri(0,$h="", $a);
      $data['main_view'] = 'postings/details';
      $this->load->view('postings', $data);
    }
   }
   
  public function categoris()
   {
    $url_modul = $this->uri->segment(4);
    $wheres = array(
						'alamat_url' => $url_modul,
            'status' => 1
						);
    $c = $this->Modul_model->get_data($wheres);
    if(!$c)
          {
						$data['nama_modul'] = 'kosong';
          }
        else
          {
						$data['nama_modul'] = $c['nama_modul'];
          }
    $web=$this->uut->namadomain(base_url());
    $a = 0;
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
    $data['menu_kiri'] = $this->menu_kiri(0,$h="", $a);
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }

    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['kata_kunci'] = '';
						$data['gambar'] = 'blankgambar.jpg';
          }
        else
          {
						$data['judul_posting'] = $d['judul_posting'];
						$data['isi_posting'] = ''.$d['isi_posting'].'';
						$data['kata_kunci'] = $d['kata_kunci'];
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }

    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
      
    $data['main_view'] = 'postings/categoris';
    $this->load->view('postings', $data);
   }
   
  private function posting_details($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
    if(empty($r) || $r == 1 || $r < 1)
    {
       $offset = 0;
    }elseif($r > 1){
      $offset = ($r*10);
    }

		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
    and posisi='menu_atas'
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",10
		");
		$x = $w->num_rows();
		$nomor = 0;

		foreach($w->result() as $h)
		{ 
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
        $yummy   = array("_","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
        $hasils .= 
        '
          <li class="list-group-item">
              <div class="row">
                  <div class="col-md-2 profile-thumb">
                      <a href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML">
                          <img class="media-object" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="'.$h->judul_posting.'">
                      </a>
                  </div>
                  <div class="col-md-10">
                      <h4><a href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></h4>
                      <p>'.substr(strip_tags($h->isi_posting), 0, 350).'</p>
                      <a href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML" class="btn btn-primary btn-sm">Baca Selengkapnya</a>
                  </div>
              </div>
          </li>
        ';
		}

		return $hasils;
	}
  
  public function info_lain_details_by_id()
   {
    $web=$this->uut->namadomain(base_url());
    $id_posting = $this->input->post('parent');
    $where = array(
						'id_posting' => $this->input->post('parent')
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$parent = 0;
          }
        else
          {
						$parent = $d['parent'];
          }
    $r = 0;
    $informasi_terkait = $this->list_info_lain_details_by_id($parent, $h="", $r);
    echo $informasi_terkait;
   }
   
  private function list_info_lain_details_by_id($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."'
		and status = 1 
    and domain='".$web."'
    order by created_time desc limit 7
		");
		$x = $w->num_rows();
		foreach($w->result() as $h)
      { 
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
        $yummy   = array("_","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
        $filename = './media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'';
        if (file_exists($filename)) {
          $x= 's_'.$this->get_attachment_by_id_posting($h->id_posting).'';
        } else {
          $x= 'blankgambar.jpg';
        }
        $hasils .= 
        '
          <li>
              <div class="recent-post">
                  <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
                      <img class="pull-left" src="'.base_url().'media/upload/'.$x.'" alt="'.$h->judul_posting.'">
                  </a>
                  <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="posts-list-title">'.$h->judul_posting.'</a>
              </div>
              <div class="clearfix"></div>
          </li>
        ';
      return $hasils;
    }
	}
  
  public function pencarian()
   {
    $web=$this->uut->namadomain(base_url());
    $a = 0;
    $r = $this->uri->segment(5);
    $p=$this->uri->segment(3);
    // $data['isi_halaman'] = $this->posting_pencarian($p,$q="", $r);
    // $data['isi_halaman'] = $this->posting_pencarian(0,$h="", $a);
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
    $data['menu_kiri'] = $this->posting(0,$h="", $a);
    $data['welcome1'] = $this->get_komponen('welcome1');
    $data['welcome2'] = $this->get_komponen('welcome2');
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $data['slide_show'] = $this->get_komponen('slide_show');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
      
    $s = $this->db->query("SELECT COUNT(*) as jumlah_data from posting where parent=0 ");
    $jumPage = $s->row("jumlah_data");
    $data['jumlah_posting'] = $s->row("jumlah_data");
    
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
    $data['main_view'] = 'postings/pencarian';
    $this->load->view('halaman', $data);
   }
   
  public function daftar_pencarian()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_posting    = $this->input->post('urut_data_posting');
    $posisi    = $this->input->post('posisi');
    $parent    = $this->input->post('parent');
    if($halaman==''){$halaman=1;}else{$halaman;}
    $start      = ($halaman - 1) * $limit;
    $fields     = "
                  *,
                  (select user_name from users where users.id_users=posting.created_by) as nama_pengguna
                  ";
    if($parent==''){$parent=0;}else{$parent;}
    $where      = array(
      'domain' => $web,
      'posisi' => $posisi,
      'parent' => $parent,
      'status!=' => 99
    );
    $order_by   = 'posting.'.$urut_data_posting.'';
    echo json_encode($this->Halaman_model->json_all_posting($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
   
	public function json_all_posting(){
    $web=$this->uut->namadomain(base_url());
		$table = 'posting';
    $id_posting = $this->input->post('id_posting');
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where = array(
      'posting.status !=' => 99,
      'posting.parent' => $id_posting,
      'posting.domain' => $web
      );
    $orderby   = ''.$order_by.'';
    $orderby1   = 'posting.created_time desc';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $h)
      {
				$urut=$urut+1;
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
        $yummy   = array("_","","","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
				echo '
        
            <li class="col-md-6 col-sm-6 col-xs-12 portfolio-item no-padding no-margin" id_permohonan_informasi_publik="'.$h->id_posting.'" id="'.$h->id_posting.'">
              <a class="" href="'.base_url().'postings/details/'.$h->id_posting.'/'.str_replace(' ', '_', $newphrase ).'.HTML">
                  <figure class="animate fadeInLeft">
                      <img alt="'.substr(strip_tags($h->judul_posting), 0, 100).'" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'">
                    <figcaption>
                        <u><strong>'.$h->judul_posting.'</strong></u><br />
                        <span>'.substr(strip_tags($h->isi_posting), 0, 150).'...</span>
                    </figcaption>
                  </figure>
              </a>
            </li>
        ';
      }
          
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $id_posting = trim($this->input->post('id_posting'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'posting.status !=' => 99,
      'posting.parent' => $id_posting,
      'posting.domain' => $web
      );
    $this->db->where($where0);
    $query0 = $this->db->get('posting');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_wonosobo_menuju_smart_city(){
    $web=$this->uut->namadomain(base_url());
		$table = 'posting';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where = array(
      'posting.status !=' => 99,
      'posting.domain' => $web
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all_wonosobo_menuju_smart_city($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $h)
      {
				$urut=$urut+1;
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
        $yummy   = array("_","","","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
				$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
				echo '
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<!-- BEGIN CAROUSEL -->            
								<div class="front-carousel">
									<div class="carousel slide" id="myCarousel">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<div class="item active">
												<img alt="" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'">
											</div>
										</div>
									</div>                
								</div>
								<!-- END CAROUSEL -->             
							</div>
							<div class="col-md-8 col-sm-8">
								<h2><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $newphrase ).'.HTML">'.$h->judul_posting.'</a></h2>
								<p>'.substr(strip_tags($h->isi_posting), 0, 200).'</p>
								<i class="fa fa-calendar"></i> '.$created_time.'
								<a class="btn btn-info" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $newphrase ).'.HTML" class="more"><i class="fa fa-eye"></i> Baca Selengkapnya <i class="icon-angle-right"></i></a>
								
							</div>
						</div>
						<hr class="blog-post-sep">
        ';
      }
          
	}
  public function total_data_wonosobo_menuju_smart_city()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'posting.status !=' => 99,
      'posting.domain' => $web
      );
    $this->db->where($where0);
    $query0 = $this->db->get('posting');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_posting_get(){
    $web=$this->uut->namadomain(base_url());
		$table = 'posting';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where = array(
      'posting.status' => 1,
      'posting.domain' => $web
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $h)
      {
				$urut=$urut+1;
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
        $yummy   = array("_","","","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
				echo '
        
            <li class="col-md-6 col-sm-6 col-xs-12 portfolio-item no-padding no-margin" id_posting="'.$h->id_posting.'" id="'.$h->id_posting.'">
              <a class="" href="'.base_url().'postings/details/'.$h->id_posting.'/'.str_replace(' ', '_', $newphrase ).'.HTML">
                  <figure class="animate fadeInLeft">
                      <img alt="'.substr(strip_tags($h->judul_posting), 0, 100).'" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'">
                    <figcaption>
                        <u><strong>'.$h->judul_posting.'</strong></u><br />
                        <span>'.substr(strip_tags($h->isi_posting), 0, 150).'...</span>
                    </figcaption>
                  </figure>
              </a>
            </li>
        ';
      }
          
	}
  public function total_data_get()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'posting.status' => 1,
      'posting.domain' => $web
      );
    $this->db->where($where0);
    $query0 = $this->db->get('posting');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  
private function menu_atas_pmi($parent=NULL,$hasil, $a){
	$web=$this->uut->namadomain(base_url());
  if($web=='disparbud.wonosobokab.go.id' or $web=='dikpora.wonosobokab.go.id'){
    $url_baca='post/detail';
    $url_baca_list='post/galeri';
  }else{
    $url_baca='postings/details';
    $url_baca_list='postings/categoris';
  }
	$a = $a + 1;
	$w = $this->db->query("
	SELECT * from posting
	where posisi='menu_atas'
	and parent='".$parent."' 
	and status = 1 
	and tampil_menu_atas = 1 
	and domain='".$web."'
	order by urut
	");
    $x = $w->num_rows();
    if(($w->num_rows())>0)
    {
    if($parent == 0){
      $hasil .= '
      </div>
      </div>
      <div class="navbar-item dropdown is-hoverable is-hidden-mobile">
        <div class="dropdown-trigger">
      ';
    }
    else{
    $hasil .= '
      </div>
      <div id="dropdown-menu" role="menu" class="dropdown-menu"><div class="dropdown-content"><p>
      ';
      }
      }
      $nomor = 0;
      /*$hasil .= '
      ';*/
      foreach($w->result() as $h)
      {
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
        $yummy   = array("_","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
        $r = $this->db->query("
          SELECT * from posting
          where parent='".$h->id_posting."' 
          and status = 1 
          and tampil_menu_atas = 1 
          and domain='".$web."'
          order by urut
          ");
        $xx = $r->num_rows();
        $nomor = $nomor + 1;
        if( $a > 1 ){
          $hasil .= '
              <a href="'.base_url().''.$url_baca.'/'.$h->id_posting.'" class="dropdown-item">'.$h->judul_posting.'</a>
          ';
        }
        else{
          $hasil .= '
            <a href="'.base_url().''.$url_baca.'/'.$h->id_posting.'" class="has-text-dark nuxt-link-exact-active nuxt-link-active"><span aria-haspopup="true" aria-controls="dropdown-menu">'.$h->judul_posting.'</span></a>
          ';
          /*$hasil .= '<div class="navbar-item dropdown is-hoverable is-hidden-mobile"><div class="dropdown-trigger">';
          if ($xx == 0){
          $hasil .= '
            <a href="'.base_url().''.$url_baca.'/'.$h->id_posting.'" class="has-text-dark nuxt-link-exact-active nuxt-link-active"><span aria-haspopup="true" aria-controls="dropdown-menu">'.$h->judul_posting.'</span></a>
        </div>
      </div>
          ';
          $hasil .= '<div class="navbar-item dropdown is-hoverable is-hidden-mobile"><div class="dropdown-trigger">';
          }
          else{
          $hasil .= '
            <a href="'.base_url().''.$url_baca.'/'.$h->id_posting.'" class="has-text-dark nuxt-link-exact-active nuxt-link-active"><span aria-haspopup="true" aria-controls="dropdown-menu">'.$h->judul_posting.'</span></a>
          </div>
          ';
          $hasil .= '<div class="navbar-item dropdown is-hoverable is-hidden-mobile"><div class="dropdown-trigger">';
          }
          $hasil .= '</div>'; */
        }
        $hasil = $this->menu_atas_pmi($h->id_posting,$hasil, $a);
      }
      if(($w->num_rows)>0)
      {
        $hasil .= '
          </p>
        </div>
      </div>
      </div>
      <div class="navbar-item dropdown is-hoverable is-hidden-mobile">
        <div class="dropdown-trigger">
        ';
      }
      else{
          $hasil .= "
          ";
      }
    return $hasil;
  }

  private function menu_kiri_pmi($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    and tampil_menu = 1
    and posisi='menu_kiri'
    order by urut asc, created_time desc limit 10
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
        $hasil .= '<ul class="list-group sidebar-nav" id="sidebar-nav">';
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
		and status = 1 
    and domain='".$web."'
    and tampil_menu = 1
    and posisi='menu_kiri'
    order by urut
    
		");
		$xx = $r->num_rows();
    $nomor=$nomor+1;
			if( $a > 1 ){
          if($nomor == 2){
            $hasil .= '
              <div class="column is-full">
                <a href="'.base_url().'postings/details/'.$h->id_posting.'" class="nuxt-link-exact-active nuxt-link-active">
                  <div class="menu-sidebar layanan-sidebar"><span><i class="icon icon-tanggap-bencana"></i>'.$h->judul_posting.'</span><i class="ion-chevron-right"></i></div>
                </a>
              </div>
            ';
          }
          else if($nomor == 3){
            $hasil .= '
              <div class="column is-full">
                <a href="'.base_url().'postings/details/'.$h->id_posting.'" class="nuxt-link-exact-active nuxt-link-active">
                  <div class="menu-sidebar layanan-sidebar"><span><i class="icon icon-pmr-dan-relawan"></i>'.$h->judul_posting.'</span><i class="ion-chevron-right"></i></div>
                </a>
              </div>
            ';
          }
          else if($nomor == 4){
            $hasil .= '
              <div class="column is-full">
                <a href="'.base_url().'postings/details/'.$h->id_posting.'" class="nuxt-link-exact-active nuxt-link-active">
                  <div class="menu-sidebar layanan-sidebar"><span><i class="icon icon-kemitraaan"></i>'.$h->judul_posting.'</span><i class="ion-chevron-right"></i></div>
                </a>
              </div>
            ';
          }
          else{
            $hasil .= '
              <div class="column is-full">
                <a href="'.base_url().'postings/details/'.$h->id_posting.'" class="nuxt-link-exact-active nuxt-link-active">
                  <div class="menu-sidebar layanan-sidebar"><span><i class="icon icon-layanan-ambulance"></i>'.$h->judul_posting.'</span><i class="ion-chevron-right"></i></div>
                </a>
              </div>
            ';
          }
        }
			else{
        $hasil .='<h5 class="title is-5 is-spaced has-text-weight-bold">'.$h->judul_posting.'</h5>';
        }
			$hasil = $this->menu_kiri_pmi($h->id_posting,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			$hasil .= '';
		}
    else{
      
    }
		
		return $hasil;
	}
 
  private function option_posting_terbarukan_pmi($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT id_posting, judul_posting, created_time from posting
		where parent!=0 
		and status = 1 
    and domain='".$web."'
    order by urut asc, created_time desc limit 1
		");
		if(($w->num_rows())>0){
      $hasil .= '
      ';
		}
    else{
    // $hasil .= '<ul>';
    }
		foreach($w->result() as $h)
		{
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
      $hasil .= '
        <div class="category">
          <h3 class="category-current">TERKINI</h3>
          <div class="category-nav">
            <a href="#" class="icon"><i class="ion-chevron-up"></i></a>
            <a href="#" class="icon"><i class="ion-chevron-down"></i></a>
          </div>
        </div>
        <div class="ticker-content-news">
          <div>
            <a href="'.base_url().'postings/details/'.$h->id_posting.'" class="news-title">'.$h->judul_posting.'</a>
            <small class="news-time has-text-centered">'.$created_time.'</small>
          </div>
        </div>
      ';
      
			// $hasil = $this->option_posting_terbarukan_pmi($h->id_posting,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
      // $hasil .= '</div>';
		}
    else{
    }
		
		return $hasil;
	} 
}