<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Laporan_perkembangan_bulanan_semua_pasar extends CI_Controller
 {
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->model('Cetak_model');
    $this->load->model('Crud_model');
    $this->load->model('Harga_kebutuhan_pokok_model');
   }
   
  public function index()
   {
    
    $where             = array(
      'harga_kebutuhan_pokok.status !=' => 99
    );
    $a                 = $this->Harga_kebutuhan_pokok_model->json_semua_harga_kebutuhan_pokok($where);
    $data['per_page']  = 20;
    $data['total']     = ceil($a / 20);
    $data['main_view'] = 'laporan_perkembangan/bulanan_semua_pasar';
    $this->load->view('back_bone', $data);
   }
  public function html_harga_kebutuhan_pokok()
    {
      $id_pasar         = trim($this->input->post('id_pasar_by_filter'));
      $tanggal         = trim($this->input->post('dari_tanggal_1'));
      $tanggal2         = trim($this->input->post('sampai_tanggal_1'));
      if($id_pasar==0){
        $where_pasar = array(
            'pasar.status !=' => 99,
        );
        $this->db->select("
        pasar.id_pasar, 
        pasar.kode_pasar, 
        pasar.nama_pasar
        ");
        $this->db->where($where_pasar);
        $this->db->order_by('pasar.id_pasar');
        $b_pasar = $this->db->get('pasar');
        $urut=$b_pasar->num_rows();
        echo '
          <div class="col-xs-12">
            <b>
              <center>LAPORAN PERKEMBANGAN HARGA BULANAN<br /> BARANG KEBUTUHAN POKOK, BARANG PENTING, DAN BARANG STRATEGIS LAINNYA<br /> KABUPATEN WONOSOBO<br /></center>
            </b>
            ';
        echo '
          <table class="table table-bordered table-striped table-responsive table-sm">
            <thead>
            <tr class="table-info">
              <th rowspan="2">No</th>
              <th rowspan="2">Nama Barang / Bahan Pokok</th>
              <th rowspan="2">Satuan</th>
              <th colspan="'.$urut.'">Pasar</th>
              <th rowspan="2">Rata-rata</th>
              <th rowspan="2">Ket. Ketersediaan</th>
            </tr>
            <tr class="table-info">
            ';
        foreach ($b_pasar->result() as $r_pasar)
          {
            $id_pasar_by = $r_pasar->id_pasar;
            $nama_pasar = $r_pasar->nama_pasar;
            echo'<th>'.$nama_pasar.'</th>';
          }
          echo'
            </tr>
            </thead>
            ';
            $where1    = array(
                'komoditi.status !=' => 99, 'komoditi.parent' => 0,
            );
            $this->db->select("
            komoditi.id_komoditi, 
            komoditi.urut, 
            komoditi.judul_komoditi
            ");
            $this->db->where($where1);
            $this->db->order_by('komoditi.urut');
            $b1 = $this->db->get('komoditi');
            $no=0;
            foreach ($b1->result() as $r1)
              {
              $no=$no+1;
              echo '
                <tr>
                  <td>'.$r1->urut.'</td>
                  <td><b>'.strtoupper($r1->judul_komoditi).'</b></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>';
                $where2    = array(
                  'komoditi.status !=' => 99,
                  'komoditi.parent' => $r1->id_komoditi
                );
                $this->db->select("
                komoditi.id_komoditi, komoditi.urut, komoditi.judul_komoditi
                ");
                $this->db->where($where2);
                $this->db->order_by('komoditi.urut');
                $b2 = $this->db->get('komoditi');
                  foreach ($b2->result() as $r2)
                  {
                    $id_komoditi = $r2->id_komoditi;
                    echo '
                      <tr>
                        <td></td>
                        <td>'.$r2->judul_komoditi.'</td>
                        ';
                        $where_harga    = array(
                          'harga_kebutuhan_pokok.status !=' => 99,
                          'harga_kebutuhan_pokok.id_komoditi' => $r2->id_komoditi,
                          'harga_kebutuhan_pokok.id_pasar' => 1,
                          'harga_kebutuhan_pokok.tanggal' => $tanggal,
                        );
                        $this->db->select("
                        harga_kebutuhan_pokok.harga, 
                        harga_kebutuhan_pokok.id_satuan, 
                        ");
                        $this->db->where($where_harga);
                        $this->db->order_by('harga_kebutuhan_pokok.id_komoditi');
                        $b_harga = $this->db->get('harga_kebutuhan_pokok');
                        $jml_row = $this->db->count_all_results();
                        foreach ($b_harga->result() as $r_harga)
                        {
                          $harga = $r_harga->harga;
                          $id_satuan = $r_harga->id_satuan;
                        }
                        echo'
                        <td>';
                            if(!empty( $id_satuan )){ echo $id_satuan; }else{echo $id_satuan='0';}
                        echo'
                        </td>
                        <td>';
                            if(!empty( $harga )){ echo $harga; }else{echo $harga='0';}
                        echo'
                        </td>
                        <td>
                        ';
                        $where_harga2 = array(
                          'harga_kebutuhan_pokok.status !=' => 99,
                          'harga_kebutuhan_pokok.id_komoditi' => $r2->id_komoditi,
                          'harga_kebutuhan_pokok.id_pasar' => 2,
                          'harga_kebutuhan_pokok.tanggal' => $tanggal,
                        );
                        $this->db->select("
                        harga_kebutuhan_pokok.harga, 
                        ");
                        $this->db->where($where_harga2);
                        $this->db->order_by('harga_kebutuhan_pokok.id_komoditi');
                        $b_harga2 = $this->db->get('harga_kebutuhan_pokok');
                        $jml_row2 = $this->db->count_all_results();
                        foreach ($b_harga2->result() as $r_harga2)
                        {
                          $harga_2 = $r_harga2->harga;
                          if(!empty( $harga_2 )){ echo $harga_2; }else{echo $harga_2='0';}
                        }
                        echo'
                        </td>
                        <td>
                        ';
                        $where_harga3    = array(
                          'harga_kebutuhan_pokok.status !=' => 99,
                          'harga_kebutuhan_pokok.id_komoditi' => $r2->id_komoditi,
                          'harga_kebutuhan_pokok.id_pasar' => 3,
                          'harga_kebutuhan_pokok.tanggal' => $tanggal,
                        );
                        $this->db->where($where_harga3);
                        // $this->db->limit(1);
                        $this->db->order_by('harga_kebutuhan_pokok.id_komoditi');
                        $b_harga3 = $this->db->get('harga_kebutuhan_pokok');
                        $jml_row3 = $this->db->count_all_results();
                          foreach ($b_harga3->result() as $r_harga3)
                          {
                            $harga_3 = $r_harga3->harga;
                            $keterangan = $r_harga3->keterangan;
                          if(!empty( $harga_3 )){ echo $harga_3; }else{echo $harga_3='0';}
                          }
                        echo'
                        </td>
                        <td>
                        ';
                        $where_avg    = array(
                          'harga_kebutuhan_pokok.status !=' => 99,
                          'harga_kebutuhan_pokok.id_komoditi' => $r2->id_komoditi,
                          'harga_kebutuhan_pokok.tanggal' => $tanggal,
                        );
                        $this->db->select_avg('harga_kebutuhan_pokok.harga');
                        $this->db->where($where_avg);
                        // $this->db->limit(1);
                        $this->db->order_by('harga_kebutuhan_pokok.id_komoditi');
                        $b_harga_avg = $this->db->get('harga_kebutuhan_pokok');
                        $jml_row_avg = $this->db->count_all_results();
                          foreach ($b_harga_avg->result() as $r_harga_avg)
                          {
                            $harga_avg = $r_harga_avg->harga;
														if(!empty( $harga_avg )){ echo round($harga_avg); }else{echo round($harga);}
                          }
                        echo'
                        </td>
                        <td>';if(!empty( $keterangan )){ echo $keterangan; }else{echo $keterangan='';}echo'</td>
                      </tr>
                      ';
                  }
             }
        echo '</table>';
        // date("l jS \of F Y h:i:s A");
        $jabatan = 'Kepala Bidang Perdagangan';
        $where_pegawai    = array(
          'pegawai.status !=' => 99,
          'pegawai.jabatan' => 'Kepala Bidang  Perdagangan',
        );
        $this->db->select("
        pegawai.jabatan, 
        pegawai.golongan, 
        pegawai.nama_pegawai, 
        pegawai.nip, 
        ");
        $this->db->where($where_pegawai);
        $this->db->order_by('pegawai.id_pegawai');
        $b_pegawai = $this->db->get('pegawai');
        if($b_pegawai->num_rows() > 0){
          foreach ($b_pegawai->result() as $r_pegawai)
          {
          $jabatan = $r_pegawai->jabatan;
          $nama_pegawai = $r_pegawai->nama_pegawai;
          $nip = $r_pegawai->nip;
          $golongan = $r_pegawai->golongan;
          echo '
          <div class="row">
            <div class="col-md-6">
              <p class="text-muted well well-sm no-shadow"><i>Sumber data: Pemantauan di ';
        $where_pasar = array(
            'pasar.status !=' => 99,
        );
        $this->db->select("
        pasar.id_pasar, 
        pasar.kode_pasar, 
        pasar.nama_pasar
        ");
        $this->db->where($where_pasar);
        $this->db->order_by('pasar.id_pasar');
        $b_pasar = $this->db->get('pasar');
        foreach ($b_pasar->result() as $r_pasar)
          {
            $nama_pasar = $r_pasar->nama_pasar;
            echo''.$nama_pasar.', ';
          }echo' Kab. Wonosobo Dinas Perdagangan, Koperasi, UKM Kab. Wonosobo</i></smal>
            </div>
            <div class="col-md-6">
              <div class="text-center">
                <p>Wonosobo, '.date("d F Y").'<p>
                <h5>
                  a.n KEPALA DINAS PERDAGANGAN, KOPERASI, UKM <br />KABUPATEN WONOSOBO <br />'.$jabatan.'
                  <br />
                  <br />
                  <br />
                  <u><b>'.strtoupper($nama_pegawai).'</b></u>
                  <br />
                  '.$golongan.'
                  <br />
                  NIP. '.$nip.'
                </h5>
              </div>
            </div>
          </div>
          ';
          }
        }else{echo'-';}
      }
      else{
      }
    }
   
 }