<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Perijinan_penggunaan_aula extends CI_Controller
 {
    
  function __construct(){
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->library('Crud_model');
    $this->load->model('Perijinan_penggunaan_aula_model');
   }
  
  public function index(){
    $data['total'] = 10;
    $data['main_view'] = 'perijinan_penggunaan_aula/home';
    $this->load->view('tes', $data);
   }

    public function simpan_booking_aula(){
   		$web=$this->uut->namadomain(base_url());
		//  $this->form_validation->set_rules('tanggal_penggunaan', 'tanggal_penggunaan', 'required');
		// $this->form_validation->set_rules('nama_kegiatan', 'nama_kegiatan', 'required');
		// $this->form_validation->set_rules('seksi_penggunaan', 'seksi_penggunaan', 'required');
		// $this->form_validation->set_rules('penanggung_jawab', 'penanggung_jawab', 'required');
		// $this->form_validation->set_rules('temp', 'temp', 'required');
	  
        $healthy = array("#", ":", "=", "!", "?", "@", "%", "&", "[", "]", "<", ">", "+", "`", "^", "*", "'");
        $yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
        $tanggal_penggunaan = str_replace($healthy, $yummy, $this->input->post('tanggal_penggunaan'));

        $tanggal_selisih = str_replace($healthy, $yummy, $this->input->post('tanggal_selisih'));

        $tempat = str_replace($healthy, $yummy, $this->input->post('tempat'));
        $nama_kegiatan = str_replace($healthy, $yummy, $this->input->post('nama_kegiatan'));
        $seksi_penggunaan = str_replace($healthy, $yummy, $this->input->post('seksi_penggunaan'));
        $penanggung_jawab = str_replace($healthy, $yummy, $this->input->post('penanggung_jawab'));
        $jumlah_peserta = str_replace($healthy, $yummy, $this->input->post('jumlah_peserta'));
        $sasaran = str_replace($healthy, $yummy, $this->input->post('sasaran'));

        $w = $this->db->query("SELECT * from perijinan_penggunaan_aula 
                          where status !=3 and status !=99 and jenis_aula='".$tempat."'  and tanggal_penggunaan ='".$tanggal_penggunaan."'
                          ");

        
		
        if(($w->num_rows())>0){
			//echo 0;		

			$data['hasil'] = 'dobel';
		 	echo json_encode($data);

			// echo "<script>
			// 	alert('Tanggal ini sudah ada acara');
			// 	window.location.href='https://dinkes.wonosobokab.go.id/index.php/postings/details/1033910/Jadwal_Penggunaan_Aula.HTML';
			// 	</script>";	
		}elseif ($tanggal_selisih >31) {
			$data['hasil'] = 'maksimal';
		 	echo json_encode($data);
		}
		else
		 {
		  	$data_input = array(
        		'domain' => $web,
				'tanggal_penggunaan' => $tanggal_penggunaan,
				'jenis_aula' => $tempat,
				'nama_kegiatan' => $nama_kegiatan,
				'seksi_penggunaan' => $seksi_penggunaan,
				'penanggung_jawab' => $penanggung_jawab,
				'jumlah_peserta' => $jumlah_peserta,
				'sasaran' => $sasaran,
				'created_by' => '0',
				'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1
				);
     		$table_name = 'perijinan_penggunaan_aula';
      		$id = $this->Perijinan_penggunaan_aula_model->simpan_perijinan_penggunaan_aula($data_input, $table_name);
       		//echo $id;
       		$data['hasil'] = 'sukses';
		 	echo json_encode($data);
			//redirect('postings/details/1033910/Jadwal_Penggunaan_Aula.HTML');

     }
   }
  
  public function simpan_perijinan_penggunaan_aula(){
   		$web=$this->uut->namadomain(base_url());
		//  $this->form_validation->set_rules('tanggal_penggunaan', 'tanggal_penggunaan', 'required');
		// $this->form_validation->set_rules('nama_kegiatan', 'nama_kegiatan', 'required');
		// $this->form_validation->set_rules('seksi_penggunaan', 'seksi_penggunaan', 'required');
		// $this->form_validation->set_rules('penanggung_jawab', 'penanggung_jawab', 'required');
		// $this->form_validation->set_rules('temp', 'temp', 'required');
	  
        $healthy = array("#", ":", "=", "!", "?", "@", "%", "&", "[", "]", "<", ">", "+", "`", "^", "*", "'");
        $yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
        $tanggal_penggunaan = str_replace($healthy, $yummy, $this->input->post('tanggal_penggunaan'));
         $tempat = str_replace($healthy, $yummy, $this->input->post('tempat'));
        $nama_kegiatan = str_replace($healthy, $yummy, $this->input->post('nama_kegiatan'));
        $seksi_penggunaan = str_replace($healthy, $yummy, $this->input->post('seksi_penggunaan'));
        $penanggung_jawab = str_replace($healthy, $yummy, $this->input->post('penanggung_jawab'));

        $w = $this->db->query("SELECT * from perijinan_penggunaan_aula 
                          where status !=3 and status !=99 and jenis_aula='".$tempat."' and tanggal_penggunaan ='".$tanggal_penggunaan."'
                          ");
		if(($w->num_rows())>0){
			//echo 0;	
			$data['hasil'] = 'dobel';
		 	echo json_encode($data);			
		}
		else
		 {
		  	$data_input = array(
        		'domain' => $web,
				'tanggal_penggunaan' => $tanggal_penggunaan,
				'jenis_aula' => $tempat,
				'nama_kegiatan' => $nama_kegiatan,
				'seksi_penggunaan' => $seksi_penggunaan,
				'penanggung_jawab' => $penanggung_jawab,
				'created_by' => '0',
				'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1
				);
     		$table_name = 'perijinan_penggunaan_aula';
      		$id = $this->Perijinan_penggunaan_aula_model->simpan_perijinan_penggunaan_aula($data_input, $table_name);
       		//echo $id;
	   		//redirect('perijinan_penggunaan_aula');
	   		$data['hasil'] = 'sukses';
		 	echo json_encode($data);
     }
   }
    
   
  function load_table(){
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("SELECT * from perijinan_penggunaan_aula 
                          where status = 1 
                          and domain='".$web."' order by tanggal_penggunaan asc
                          ");
		if(($w->num_rows())>0){
		}
	  	$a=0;
	  	$no_urut = 0;
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			$no_urut++;
			$aula = $h->jenis_aula;
			if ($aula ==1) {
				$tempat = 'Aula Utama';
			}else if ($aula ==2){
				$tempat = 'Aula Rapat';
			}

			$seksi = $h->seksi_penggunaan;
			if ($seksi == 1) {
				$nama_seksi = 'Bidang P2P';
			}else if ($seksi == 2) {
				$nama_seksi = 'Bidang Yan SDK';
			}else if ($seksi == 3) {
				$nama_seksi = 'Bidang Kesmas';
			}else if ($seksi == 4) {
				$nama_seksi = 'Bidang Sekretariat';
			}

			echo '
				<tr id_perijinan_penggunaan_aula="'.$h->id_perijinan_penggunaan_aula.'" id="'.$h->id_perijinan_penggunaan_aula.'">
					
					<td style="padding: 2px;">
						<p>'.$no_urut.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$this->reversdate($h->tanggal_penggunaan).'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$tempat.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nama_kegiatan.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$nama_seksi.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->penanggung_jawab.'</p>
					</td>
					<td style="padding: 2px;">
						
						<a href="#" id="verif_ajax" class="btn btn-success btn-md">Konfirmasi</a> 
						<a href="#" id="noverif_ajax" class="btn btn-info btn-md">Tidak Konfirmasi</a>
						<a href="#" id="del_ajax" class="btn btn-danger btn-md">Hapus</a> 
						
					</td>
					
				</tr>
			';
		}
	}

	//<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i>Edit</a><br>
	//<a href="#" id="del_ajax"><i class="fa fa-cut"></i>Hapus</a>
  
  function load_table_perijinan_penggunaan_aula(){
    $web=$this->uut->namadomain(base_url());
    $tglnow = date('Y-m-d');
		$w = $this->db->query("SELECT * from perijinan_penggunaan_aula 
                          where status != 99                          
                          and domain='".$web."' 
                          and tanggal_penggunaan >= '".$tglnow."'
                          order by tanggal_penggunaan asc
                          ");
		if(($w->num_rows())>0){
		}
	  	$a=0;
	  	$no_urut = 0;
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			$a++;
			$no_urut++;
			$aula = $h->jenis_aula;
			if ($aula ==1) {
				$tempat = 'Aula Utama';
			}else{
				$tempat = 'Aula Rapat';
			}

			$seksi = $h->seksi_penggunaan;
			if ($seksi == 1) {
				$nama_seksi = 'Bidang P2P';
			}else if ($seksi == 2) {
				$nama_seksi = 'Bidang Yan SDK';
			}else if ($seksi == 3) {
				$nama_seksi = 'Bidang Kesmas';
			}else if ($seksi == 4) {
				$nama_seksi = 'Bidang Sekretariat';
			}

			echo '
				<tr id_perijinan_penggunaan_aula="'.$h->id_perijinan_penggunaan_aula.'" id="'.$h->id_perijinan_penggunaan_aula.'">
					<td style="padding: 2px;">
						<p>'.$no_urut.'</p>
					</td>
					<td style="padding: 2px;" width="18%">
						<p>'.$this->reversdate($h->tanggal_penggunaan).'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$tempat.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nama_kegiatan.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$nama_seksi.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->penanggung_jawab.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->jumlah_peserta.' orang</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->sasaran.'</p>
					</td>
					<td style="padding: 2px;">
						<p>';
						if($h->status==1){echo'<span class="bg-danger">Menunggu Konfirmasi</span>';}
						elseif($h->status==2){echo'<span class="bg-success">Terkonfirmasi</span>';}
						elseif($h->status==3){echo'<span class="bg-danger">Dibatalkan</span>';}
						else{echo'<span class="bg-warning">Sudah Selesai</span>';}
						echo'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->created_time.'</p>
					</td>
				</tr>
			';
		}
	}
  
  public function cetak(){
    $where = array(
		'id_perijinan_penggunaan_aula' => $this->uri->segment(3),
		'status' => 1
		);
    $d = $this->perijinan_penggunaan_aula_model->get_data($where);
    if(!$d){
			$data['nama'] = '';
			$data['nomor_surat'] = '';
			$data['perihal_surat'] = '';
			$data['tanggal_surat'] = '';
			$data['nomot_telp'] = '';
			$data['kebangsaan'] = '';
			$data['alamat'] = '';
			$data['pekerjaan'] = '';
			$data['penanggung_jawab'] = '';
			$data['lokasi'] = '';
			$data['judul_penelitian'] = '';
			$data['asal_universitas'] = '';
			$data['nomor_surat_kesbangpol'] = '';
			$data['tanggal_surat_kesbangpol'] = '';
			$data['surat_ditujukan_kepada'] = '';
			$data['tembusan_kepada_1'] = '';
			$data['tembusan_kepada_2'] = '';
			$data['tembusan_kepada_3'] = '';
			$data['tembusan_kepada_4'] = '';
			$data['tembusan_kepada_5'] = '';
			$data['tembusan_kepada_6'] = '';
			$data['pembuat'] = '';
			$data['created_time'] = '';
			$data['pengedit'] = '';
			$data['updated_time'] = '';
          }
        else{
			$data['nama'] = $d['nama'];
			$data['nomor_surat'] = $d['nomor_surat'];
			$data['perihal_surat'] = ''.$d['perihal_surat'].' ';
			//$data['tanggal_surat'] = $d['tanggal_surat'];
			$data['tanggal_surat'] = ''.$this->Crud_model->dateBahasaIndo($d['tanggal_surat']).'';
            $data['nomot_telp'] = $d['nomot_telp'];
            $data['kebangsaan'] = $d['kebangsaan'];
            $data['alamat'] = $d['alamat'];
            $data['pekerjaan'] = $d['pekerjaan'];
            $data['penanggung_jawab'] = $d['penanggung_jawab'];
			$data['lokasi'] = $d['lokasi'];
			$data['judul_penelitian'] = $d['judul_penelitian'];
			$data['asal_universitas'] = $d['asal_universitas'];
			$data['nomor_surat_kesbangpol'] = $d['nomor_surat_kesbangpol'];
			// $data['tanggal_surat_kesbangpol'] = $d['tanggal_surat_kesbangpol'];
			$data['tanggal_surat_kesbangpol'] = $this->Crud_model->dateBahasaIndo($d['tanggal_surat_kesbangpol']);
			$data['surat_ditujukan_kepada'] = $d['surat_ditujukan_kepada'];
			$data['tembusan_kepada_1'] = $d['tembusan_kepada_1'];
			$data['tembusan_kepada_2'] = $d['tembusan_kepada_2'];
			$data['tembusan_kepada_3'] = $d['tembusan_kepada_3'];
			$data['tembusan_kepada_4'] = $d['tembusan_kepada_4'];
			$data['tembusan_kepada_5'] = $d['tembusan_kepada_5'];
			$data['tembusan_kepada_6'] = $d['tembusan_kepada_6'];
			$data['pembuat'] = $d['pembuat'];
			$data['created_time'] = $this->Crud_model->dateBahasaIndo1($d['created_time']);
			$data['pengedit'] = $d['pengedit'];
			$data['updated_time'] = $d['updated_time'];
          }
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'perijinan_penggunaan_aula/cetak';
      $this->load->view('print', $data);
  }
  private function perijinan_penggunaan_aula($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from perijinan_penggunaan_aula 
                          where status = 1 
                          and domain='".$web."'
                          ");
		if(($w->num_rows())>0){
		} 
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			if( $a > 1 ){
			$hasil =  '
				<tr>                    
					<td style="padding: 2px '.($a * 20).'px ;">
						<span><i class="fa fa-user"></i> <a href="#">'.$h->nama.' </a></span>
						<p>'.$h->alamat.'</p>
					</td>
				</tr>
			';
			  }
			else{
				$hasil =  '
					<tr>                    
						<td>
						<span><i class="fa fa-user"></i> '.$h->nama.' </span>
							<p>'.$h->alamat.'</p>
						</td>
					</tr>
				';
			}
			$hasil = $this->perijinan_penggunaan_aula($h->id_perijinan_penggunaan_aula,$hasil, $a, $id_posting);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'id_perijinan_penggunaan_aula' => $this->input->post('id_perijinan_penggunaan_aula'),
        'perijinan_penggunaan_aula.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_perijinan_penggunaan_aula');
      $result = $this->db->get('perijinan_penggunaan_aula');
      echo json_encode($result->result_array());
		}
   
  public function hapus()
	{
      $web=$this->uut->namadomain(base_url());
	  $id_perijinan_penggunaan_aula = $this->input->post('id_perijinan_penggunaan_aula');
      $where = array(
        'id_perijinan_penggunaan_aula' => $id_perijinan_penggunaan_aula,
        'domain' => $web
        );
      $this->db->from('perijinan_penggunaan_aula');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          	'status' => 99,
			'deleted_by' => $this->session->userdata('id_users'),
                  
          );
        $table_name  = 'perijinan_penggunaan_aula';
        $this->Perijinan_penggunaan_aula_model->update_data_perijinan_penggunaan_aula($data_update, $where, $table_name);
        echo 1;
        }
	}

   public function update_perijinan_penggunaan_aula(){
    $web=$this->uut->namadomain(base_url());		
    $id_perijinan_penggunaan_aula = $this->input->post('id_perijinan_penggunaan_aula');
    $tanggal_penggunaan = $this->input->post('penanggung_jawab');

	  
    if ($tanggal_penggunaan == "")
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
		'tanggal_penggunaan' => $this->input->post('tanggal_penggunaan'),
		'nama_kegiatan' => trim($this->input->post('nama_kegiatan')),
		'seksi_penggunaan' => trim($this->input->post('seksi_penggunaan')),
		'penanggung_jawab' => trim($this->input->post('penanggung_jawab')),
		'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'perijinan_penggunaan_aula';
      $where       = array(
        'perijinan_penggunaan_aula.id_perijinan_penggunaan_aula' => trim($this->input->post('id_perijinan_penggunaan_aula')),
        'perijinan_penggunaan_aula.domain' => $web
			);
      $this->Perijinan_penggunaan_aula_model->update_data_perijinan_penggunaan_aula($data_update, $where, $table_name);
      echo 1;
      redirect('perijinan_penggunaan_aula');
     }
   }


   public function verifikasi()
	{
      $web=$this->uut->namadomain(base_url());
	  $id_perijinan_penggunaan_aula = $this->input->post('id_perijinan_penggunaan_aula');
      $where = array(
        'id_perijinan_penggunaan_aula' => $id_perijinan_penggunaan_aula,
        'domain' => $web
        );
      $this->db->from('perijinan_penggunaan_aula');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          	'status' => 2,
			'updated_by' => $this->session->userdata('id_users'),
			'updated_time' => date('Y-m-d H:i:s')
                  
          );
        $table_name  = 'perijinan_penggunaan_aula';
        $this->Perijinan_penggunaan_aula_model->update_data_perijinan_penggunaan_aula($data_update, $where, $table_name);
        echo 1;
        }
	}

	public function noverifikasi()
	{
      $web=$this->uut->namadomain(base_url());
	  $id_perijinan_penggunaan_aula = $this->input->post('id_perijinan_penggunaan_aula');
      $where = array(
        'id_perijinan_penggunaan_aula' => $id_perijinan_penggunaan_aula,
        'domain' => $web
        );
      $this->db->from('perijinan_penggunaan_aula');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          	'status' => 3,
			'updated_by' => $this->session->userdata('id_users'),
			'updated_time' => date('Y-m-d H:i:s')
                  
          );
        $table_name  = 'perijinan_penggunaan_aula';
        $this->Perijinan_penggunaan_aula_model->update_data_perijinan_penggunaan_aula($data_update, $where, $table_name);
        echo 1;
        }
	}

	public function deleteaula()
	{
      $web=$this->uut->namadomain(base_url());
	  $id_perijinan_penggunaan_aula = $this->input->post('id_perijinan_penggunaan_aula');
      $where = array(
        'id_perijinan_penggunaan_aula' => $id_perijinan_penggunaan_aula,
        'domain' => $web
        );
      $this->db->from('perijinan_penggunaan_aula');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          	'status' => 99,
			'updated_by' => $this->session->userdata('id_users'),
			'updated_time' => date('Y-m-d H:i:s')
                  
          );
        $table_name  = 'perijinan_penggunaan_aula';
        $this->Perijinan_penggunaan_aula_model->update_data_perijinan_penggunaan_aula($data_update, $where, $table_name);
        echo 1;
        }
	}



	function load_table99(){
		$datenow = date('Y-m-d');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("SELECT * from perijinan_penggunaan_aula 
                          where status != 1 and status !=99 and status !=3
                          and domain='".$web."' 
                          and tanggal_penggunaan >= '".$datenow."'
                          order by tanggal_penggunaan asc
                          ");
		if(($w->num_rows())>0){
		}
	  	$a=0;
	  	$no_urut = 0;
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			$a++;
			$no_urut++;
			$aula = $h->jenis_aula;
			if ($aula =1) {
				$tempat = 'Aula Utama';
			}else{
				$tempat = 'Aula Rapat';
			}
			echo '
				<tr id_perijinan_penggunaan_aula="'.$h->id_perijinan_penggunaan_aula.'" id="'.$h->id_perijinan_penggunaan_aula.'">
					<td style="padding: 2px;">
						<p>'.$no_urut.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$this->Crud_model->dateBahasaIndo($h->tanggal_penggunaan).'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$tempat.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nama_kegiatan.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->seksi_penggunaan.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->penanggung_jawab.'</p>
					</td>
					<td style="padding: 2px;">
						<p>';
						if($h->status==1){echo'<span class="bg-success">Menunggu Konfirmasi</span>';}
						elseif($h->status==2){echo'<span class="bg-success">Terkonfirmasi</span>';}
						elseif($h->status==3){echo'<span class="bg-danger">Tidak Dikonfirmasi</span>';}
						else{echo'<span class="bg-warning">Sudah Selesai</span>';}
						echo'</p>
					</td>
					<td style="padding: 2px;">
						<a href="#" id="batalkan_ajax" class="btn btn-danger btn-md">Batalkan</a> 
					</td>
				</tr>
			';
		}
	}

	public function reversdate($tanggal){
        if(substr($tanggal, 2,1)=='/'){
            $a=explode("/",$tanggal);
            $result=$a[2].'-'.$a[0].'-'.$a[1];
        }else{
            $a=explode("-",$tanggal);
            $bln = 'Kosong';
            if ($a[1]=='01') {
            	$bln = 'Januari';
            }else if ($a[1]=='02') {
            	$bln = 'Februari';
            }else if ($a[1]=='03') {
            	$bln = 'Maret';
            }else if ($a[1]=='04') {
            	$bln = 'April';
            }else if ($a[1]=='05') {
            	$bln = 'Mei';
            }else if ($a[1]=='06') {
            	$bln = 'Juni';
            }else if ($a[1]=='07') {
            	$bln = 'Juli';
            }else if ($a[1]=='08') {
            	$bln = 'Agustus';
            }else if ($a[1]=='09') {
            	$bln = 'September';
            }else if ($a[1]=='10') {
            	$bln = 'Oktober';
            }else if ($a[1]=='11') {
            	$bln = 'November';
            }else if ($a[1]=='12') {
            	$bln = 'Desember';
            }

            $result=$a[2].' '.$bln.' '.$a[0];
        }
        return $result;
     }
  
   
 }

