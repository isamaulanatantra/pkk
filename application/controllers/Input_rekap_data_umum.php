<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Input_rekap_data_umum extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Input_rekap_data_umum_model');
   }
   
  public function index(){
    $data['main_view'] = 'input_rekap_data_umum/home';
    $this->load->view('tes', $data);
   }
  public function rekap(){
    $data['main_view'] = 'input_rekap_data_umum/rekap';
    $this->load->view('tes', $data);
   }
  public function cetak_input_rekap_data_umum(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'input_rekap_data_umum/cetak_input_rekap_data_umum';
    $this->load->view('print', $data);
   }
  public function cetak_semua_input_rekap_data_umum(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'input_rekap_data_umum/cetak_semua_input_rekap_data_umum';
    $this->load->view('print', $data);
   }
  public function cetak_input_rekap_data_umum_pekarangan(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'input_rekap_data_umum/cetak_input_rekap_data_umum_pekarangan';
    $this->load->view('print', $data);
   }
  public function cetak(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'input_rekap_data_umum/cetak';
    $this->load->view('print', $data);
   }
	public function json_all_input_rekap_data_umum(){
    $web=$this->uut->namadomain(base_url());
		$table = 'input_rekap_data_umum';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
    ( select (dusun.nama_dusun) from dusun where dusun.id_dusun=input_rekap_data_umum.id_dusun limit 1) as nama_dusun,
    ( select (desa.nama_desa) from desa where desa.id_desa=input_rekap_data_umum.id_desa limit 1) as nama_desa,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=input_rekap_data_umum.id_kecamatan limit 1) as nama_kecamatan
    ";
    if($web=='demoopd.wonosobokab.go.id'){
      $where = array(
        'input_rekap_data_umum.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where = array(
        'input_rekap_data_umum.status !=' => 99
        );
    }else{
      $where = array(
        'input_rekap_data_umum.status !=' => 99,
        'input_rekap_data_umum.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        $query_desa = $this->db->query("SELECT desa.nama_desa, desa.id_desa FROM desa WHERE desa.id_desa = ".$row->id_desa."");
        echo '
        <tr id_input_rekap_data_umum="'.$row->id_input_rekap_data_umum.'" id="'.$row->id_input_rekap_data_umum.'" name="id_input_rekap_data_umum">
          <td>'.$urut.'</td>
          <td>'; foreach ($query_desa->result() as $row_desa){
            $id_desa = $row_desa->id_desa; 
            if($id_desa=='9999'){echo 'TP PKK Kecamatan';} else{echo ''.$row_desa->nama_desa.'';} 
            } echo'</td>
          <td>'.$row->jumlah_kelompok_pkk_dusun.'</td>
          <td>'.$row->jumlah_kelompok_pkk_rw.'</td>
          <td>'.$row->jumlah_kelompok_pkk_rt.'</td>
          <td>'.$row->jumlah_kelompok_pkk_dawis.'</td>
          <td>'.$row->jumlah_krt.'</td>
          <td>'.$row->jumlah_kk.'</td>
          <td>'.$row->jumlah_jiwa_l.'</td>
          <td>'.$row->jumlah_jiwa_p.'</td>
          <td>'.$row->jumlah_kader_pkk_anggota_pkk_l.'</td>
          <td>'.$row->jumlah_kader_pkk_anggota_pkk_p.'</td>
          <td>'.$row->jumlah_kader_pkk_umum_l.'</td>
          <td>'.$row->jumlah_kader_pkk_umum_p.'</td>
          <td>'.$row->jumlah_khusus_l.'</td>
          <td>'.$row->jumlah_khusus_p.'</td>
          <td>'.$row->jumlah_tenaga_skr_honorer_l.'</td>
          <td>'.$row->jumlah_tenaga_skr_honorer_p.'</td>
          <td>'.$row->jumlah_tenaga_skr_bantuan_relawan_l.'</td>
          <td>'.$row->jumlah_tenaga_skr_bantuan_relawan_p.'</td>
          <td>'.$row->keterangan.'</td>
          <td>
            <a href="#tab_form_input_rekap_data_umum" data-toggle="tab" class="update_id_input_rekap_data_umum badge bg-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>
            <a href="#" id="del_ajax_input_rekap_data_umum" class="badge bg-danger btn-sm"><i class="fa fa-cut"></i></a>
            <a href="#" class="badge bg-gray btn-sm">'.$row->tahun.'</a>
          </td>
        </tr>
        ';
      }
			/*echo '
      <tr>
        <td valign="top" colspan="10" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'input_rekap_data_umum/cetak_input_rekap_data_umum/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Keluarga</a>
        </td>
      </tr>
      ';*/
          
	}
	public function json_all_rekap_input_rekap_data_umum(){
    $web=$this->uut->namadomain(base_url());
    $query = $this->db->query("SELECT SUM(jumlah_kelompok_pkk_dusun)as jumlah_kelompok_pkk_dusun, 
     SUM(jumlah_kelompok_pkk_rw)as jumlah_kelompok_pkk_rw,
     SUM(jumlah_kelompok_pkk_rt)as jumlah_kelompok_pkk_rt,
     SUM(jumlah_kelompok_pkk_dawis)as jumlah_kelompok_pkk_dawis,
     SUM(jumlah_krt)as jumlah_krt,
     SUM(jumlah_kk)as jumlah_kk,
     SUM(jumlah_jiwa_l)as jumlah_jiwa_l,
     SUM(jumlah_jiwa_p)as jumlah_jiwa_p,
     SUM(jumlah_kader_pkk_anggota_pkk_l)as jumlah_kader_pkk_anggota_pkk_l,
     SUM(jumlah_kader_pkk_anggota_pkk_p)as jumlah_kader_pkk_anggota_pkk_p,
     SUM(jumlah_kader_pkk_umum_l)as jumlah_kader_pkk_umum_l,
     SUM(jumlah_kader_pkk_umum_p)as jumlah_kader_pkk_umum_p,
     SUM(jumlah_khusus_l)as jumlah_khusus_l,
     SUM(jumlah_khusus_p)as jumlah_khusus_p,
     SUM(jumlah_tenaga_skr_honorer_l)as jumlah_tenaga_skr_honorer_l,
     SUM(jumlah_tenaga_skr_honorer_p)as jumlah_tenaga_skr_honorer_p,
     SUM(jumlah_tenaga_skr_bantuan_relawan_l)as jumlah_tenaga_skr_bantuan_relawan_l,
     SUM(jumlah_tenaga_skr_bantuan_relawan_p)as jumlah_tenaga_skr_bantuan_relawan_p,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=input_rekap_data_umum.id_kecamatan limit 1) as nama_kecamatan
    FROM input_rekap_data_umum WHERE input_rekap_data_umum.id_kabupaten=1 AND input_rekap_data_umum.id_propinsi=1 GROUP BY input_rekap_data_umum.id_kecamatan");
    $urut=0;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        echo '
        <tr>
          <td>'.$urut.'</td>
          <td>'.$row->nama_kecamatan.'</td>
          <td>'.$row->jumlah_kelompok_pkk_dusun.'</td>
          <td>'.$row->jumlah_kelompok_pkk_rw.'</td>
          <td>'.$row->jumlah_kelompok_pkk_rt.'</td>
          <td>'.$row->jumlah_kelompok_pkk_dawis.'</td>
          <td>'.$row->jumlah_krt.'</td>
          <td>'.$row->jumlah_kk.'</td>
          <td>'.$row->jumlah_jiwa_l.'</td>
          <td>'.$row->jumlah_jiwa_p.'</td>
          <td>'.$row->jumlah_kader_pkk_anggota_pkk_l.'</td>
          <td>'.$row->jumlah_kader_pkk_anggota_pkk_p.'</td>
          <td>'.$row->jumlah_kader_pkk_umum_l.'</td>
          <td>'.$row->jumlah_kader_pkk_umum_p.'</td>
          <td>'.$row->jumlah_khusus_l.'</td>
          <td>'.$row->jumlah_khusus_p.'</td>
          <td>'.$row->jumlah_tenaga_skr_honorer_l.'</td>
          <td>'.$row->jumlah_tenaga_skr_honorer_p.'</td>
          <td>'.$row->jumlah_tenaga_skr_bantuan_relawan_l.'</td>
          <td>'.$row->jumlah_tenaga_skr_bantuan_relawan_p.'</td>
          <td></td>
        </tr>
        ';
      }
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='demoopd.wonosobokab.go.id'){
      $where0 = array(
        'input_rekap_data_umum.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where0 = array(
        'input_rekap_data_umum.status !=' => 99
        );
    }else{
      $where0 = array(
        'input_rekap_data_umum.status !=' => 99,
        'input_rekap_data_umum.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('input_rekap_data_umum');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_input_rekap_data_umum()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('jumlah_kelompok_pkk_dusun', 'jumlah_kelompok_pkk_dusun', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => 1,
        'id_kabupaten' => 1,
        'id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'tahun' => trim($this->input->post('tahun')),
        'jumlah_kelompok_pkk_dusun' => trim($this->input->post('jumlah_kelompok_pkk_dusun')),
        'jumlah_kelompok_pkk_rw' => trim($this->input->post('jumlah_kelompok_pkk_rw')),
        'jumlah_kelompok_pkk_rt' => trim($this->input->post('jumlah_kelompok_pkk_rt')),
        'jumlah_kelompok_pkk_dawis' => trim($this->input->post('jumlah_kelompok_pkk_dawis')),
        'jumlah_krt' => trim($this->input->post('jumlah_krt')),
        'jumlah_kk' => trim($this->input->post('jumlah_kk')),
        'jumlah_jiwa_l' => trim($this->input->post('jumlah_jiwa_l')),
        'jumlah_jiwa_p' => trim($this->input->post('jumlah_jiwa_p')),
        'jumlah_kader_pkk_anggota_pkk_l' => trim($this->input->post('jumlah_kader_pkk_anggota_pkk_l')),
        'jumlah_kader_pkk_anggota_pkk_p' => trim($this->input->post('jumlah_kader_pkk_anggota_pkk_p')),
        'jumlah_kader_pkk_umum_l' => trim($this->input->post('jumlah_kader_pkk_umum_l')),
        'jumlah_kader_pkk_umum_p' => trim($this->input->post('jumlah_kader_pkk_umum_p')),
        'jumlah_khusus_l' => trim($this->input->post('jumlah_khusus_l')),
        'jumlah_khusus_p' => trim($this->input->post('jumlah_khusus_p')),
        'jumlah_tenaga_skr_honorer_l' => trim($this->input->post('jumlah_tenaga_skr_honorer_l')),
        'jumlah_tenaga_skr_honorer_p' => trim($this->input->post('jumlah_tenaga_skr_honorer_p')),
        'jumlah_tenaga_skr_bantuan_relawan_l' => trim($this->input->post('jumlah_tenaga_skr_bantuan_relawan_l')),
        'jumlah_tenaga_skr_bantuan_relawan_p' => trim($this->input->post('jumlah_tenaga_skr_bantuan_relawan_p')),
        'keterangan' => trim($this->input->post('keterangan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'input_rekap_data_umum';
      $this->Input_rekap_data_umum_model->simpan_input_rekap_data_umum($data_input, $table_name);
      /*echo $id;
			$table_name  = 'anggota_keluarga';
      $where       = array(
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_input_rekap_data_umum' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);*/
     }
   }
  public function update_input_rekap_data_umum()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('jumlah_kelompok_pkk_dusun', 'jumlah_kelompok_pkk_dusun', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => 1,
        'id_kabupaten' => 1,
        'id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'tahun' => trim($this->input->post('tahun')),
        'jumlah_kelompok_pkk_dusun' => trim($this->input->post('jumlah_kelompok_pkk_dusun')),
        'jumlah_kelompok_pkk_rw' => trim($this->input->post('jumlah_kelompok_pkk_rw')),
        'jumlah_kelompok_pkk_rt' => trim($this->input->post('jumlah_kelompok_pkk_rt')),
        'jumlah_kelompok_pkk_dawis' => trim($this->input->post('jumlah_kelompok_pkk_dawis')),
        'jumlah_krt' => trim($this->input->post('jumlah_krt')),
        'jumlah_kk' => trim($this->input->post('jumlah_kk')),
        'jumlah_jiwa_l' => trim($this->input->post('jumlah_jiwa_l')),
        'jumlah_jiwa_p' => trim($this->input->post('jumlah_jiwa_p')),
        'jumlah_kader_pkk_anggota_pkk_l' => trim($this->input->post('jumlah_kader_pkk_anggota_pkk_l')),
        'jumlah_kader_pkk_anggota_pkk_p' => trim($this->input->post('jumlah_kader_pkk_anggota_pkk_p')),
        'jumlah_kader_pkk_umum_l' => trim($this->input->post('jumlah_kader_pkk_umum_l')),
        'jumlah_kader_pkk_umum_p' => trim($this->input->post('jumlah_kader_pkk_umum_p')),
        'jumlah_khusus_l' => trim($this->input->post('jumlah_khusus_l')),
        'jumlah_khusus_p' => trim($this->input->post('jumlah_khusus_p')),
        'jumlah_tenaga_skr_honorer_l' => trim($this->input->post('jumlah_tenaga_skr_honorer_l')),
        'jumlah_tenaga_skr_honorer_p' => trim($this->input->post('jumlah_tenaga_skr_honorer_p')),
        'jumlah_tenaga_skr_bantuan_relawan_l' => trim($this->input->post('jumlah_tenaga_skr_bantuan_relawan_l')),
        'jumlah_tenaga_skr_bantuan_relawan_p' => trim($this->input->post('jumlah_tenaga_skr_bantuan_relawan_p')),
        'keterangan' => trim($this->input->post('keterangan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'input_rekap_data_umum';
      $where       = array(
        'input_rekap_data_umum.id_input_rekap_data_umum' => trim($this->input->post('id_input_rekap_data_umum')),
        'input_rekap_data_umum.domain' => $web
			);
      $this->Input_rekap_data_umum_model->update_data_input_rekap_data_umum($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      if($web=='demoopd.wonosobokab.go.id'){
        $where = array(
        'input_rekap_data_umum.id_input_rekap_data_umum' => $this->input->post('id_input_rekap_data_umum'),
          );
      }elseif($web=='pkk.wonosobokab.go.id'){
        $where = array(
        'input_rekap_data_umum.id_input_rekap_data_umum' => $this->input->post('id_input_rekap_data_umum'),
          );
      }else{
        $where = array(
        'input_rekap_data_umum.id_input_rekap_data_umum' => $this->input->post('id_input_rekap_data_umum'),
          'input_rekap_data_umum.id_kecamatan' => $this->session->userdata('id_kecamatan')
          );
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_input_rekap_data_umum');
      $result = $this->db->get('input_rekap_data_umum');
      echo json_encode($result->result_array());
		}
   
   public function total_input_rekap_data_umum()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('input_rekap_data_umum');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_input_rekap_data_umum = $this->input->post('id_input_rekap_data_umum');
      $where = array(
        'id_input_rekap_data_umum' => $id_input_rekap_data_umum,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('input_rekap_data_umum');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'input_rekap_data_umum';
        $this->Input_rekap_data_umum_model->update_data_input_rekap_data_umum($data_update, $where, $table_name);
        echo 1;
        }
		}
  function option_desa_by_id_kecamatan(){
		$id_kecamatan = $this->session->userdata('id_kecamatan');
		$w = $this->db->query("
		SELECT id_desa, nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function option_desa_by_id_desa(){
		$id_kecamatan = $this->input->post('id_kecamatan');
		$w = $this->db->query("
		SELECT id_desa, nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function option_propinsi(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_propinsi="";
		}
		else{
			//$where_id_propinsi="and propinsi.id_propinsi=".$this->session->userdata('id_propinsi')."";
			$where_id_propinsi="";
		}
		$w = $this->db->query("
		SELECT *
		from propinsi
		where propinsi.status = 1
    ".$where_id_propinsi."
		order by propinsi.id_propinsi
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_propinsi.'">'.$h->nama_propinsi.'</option>';
		}
	}
  function id_kabupaten_by_id_propinsi(){
		$id_propinsi = $this->input->post('id_propinsi');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kabupaten
		where kabupaten.status = 1
    and kabupaten.id_propinsi=".$id_propinsi."
		order by kabupaten.id_kabupaten
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kabupaten.'">'.$h->nama_kabupaten.'</option>';
		}
	}
  function id_kecamatan_by_id_kabupaten(){
		$id_kabupaten = $this->input->post('id_kabupaten');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kecamatan
		where kecamatan.status = 1
		and kecamatan.id_kabupaten=".$id_kabupaten."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function id_desa_by_id_kecamatan(){
		$id_kecamatan = $this->session->userdata('id_kecamatan');
		$w = $this->db->query("
		SELECT nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function id_dusun_by_id_desa(){
		$id_desa = $this->input->post('id_desa');
		$w = $this->db->query("
		SELECT *
		from dusun
		where dusun.status = 1
		and dusun.id_desa = ".$id_desa."
		order by id_dusun");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_dusun.'">'.$h->nama_dusun.'</option>';
		}
	}
  function option_kecamatan(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_kecamatan="";
		}
		else{
			$where_id_kecamatan="and data_kecamatan.id_kecamatan=".$this->session->userdata('id_kecamatan')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_kecamatan, kecamatan
		where data_kecamatan.status = 1
		and kecamatan.id_kecamatan=data_kecamatan.id_kecamatan
    ".$where_id_kecamatan."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function status_dalam_keluarga(){
		$w = $this->db->query("
		SELECT *
		from status_dalam_keluarga
		where status_dalam_keluarga.status = 1
		order by status_dalam_keluarga.id_status_dalam_keluarga
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_dalam_keluarga.'">'.$h->nama_status_dalam_keluarga.'</option>';
		}
	}
  function pekerjaan(){
		$w = $this->db->query("
		SELECT *
		from pekerjaan
		where pekerjaan.status = 1
		order by pekerjaan.id_pekerjaan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pekerjaan.'">'.$h->nama_pekerjaan.'</option>';
		}
	}
  function agama(){
		$w = $this->db->query("
		SELECT *
		from agama
		where agama.status = 1
		order by agama.id_agama
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_agama.'">'.$h->nama_agama.'</option>';
		}
	}
  function status_pernikahan(){
		$w = $this->db->query("
		SELECT *
		from status_pernikahan
		where status_pernikahan.status = 1
		order by status_pernikahan.id_status_pernikahan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_pernikahan.'">'.$h->nama_status_pernikahan.'</option>';
		}
	}
  function pendidikan(){
		$w = $this->db->query("
		SELECT *
		from pendidikan
		where pendidikan.status = 1
		order by pendidikan.id_pendidikan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pendidikan.'">'.$h->nama_pendidikan.'</option>';
		}
	}
 }