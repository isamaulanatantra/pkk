<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Typeahead extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
  
  public function index()
		{
		}
  
  public function search_propinsi()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          propinsi.id_propinsi,
          propinsi.kode_propinsi,
          propinsi.nama_propinsi,
          propinsi.keterangan,

          propinsi.nama_propinsi as value
          ");
        $this->db->like('propinsi.nama_propinsi', $q);
        $this->db->order_by('nama_propinsi', 'asc');
        $result = $this->db->get('propinsi');
        echo json_encode( $result->result_array() );
			}
		}
  
  public function search_kabupaten()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          kabupaten.id_kabupaten,
          kabupaten.id_propinsi,
          kabupaten.kode_kabupaten,
          kabupaten.nama_kabupaten,

          kabupaten.nama_kabupaten as value
          ");
        $this->db->like('kabupaten.nama_kabupaten', $q);
        $this->db->order_by('nama_kabupaten', 'asc');
        $result = $this->db->get('kabupaten');
        echo json_encode( $result->result_array() );
			}
		}
  
  public function search_kecamatan()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          kecamatan.id_desa,
          kecamatan.id_kecamatan,
          kecamatan.kode_kecamatan,
          kecamatan.nama_kecamatan,

          kecamatan.nama_kecamatan as value
          ");
        $this->db->like('kecamatan.nama_kecamatan', $q);
        $this->db->order_by('nama_kecamatan', 'asc');
        $result = $this->db->get('kecamatan');
        echo json_encode( $result->result_array() );
			}
		}
   
  public function search_desa()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          desa.id_desa,
          desa.id_kecamatan,
          desa.kode_desa,
          desa.nama_desa,

          desa.nama_desa as value
          ");
        $this->db->like('desa.nama_desa', $q);
        $this->db->order_by('nama_desa', 'asc');
        $result = $this->db->get('desa');
        echo json_encode( $result->result_array() );
			}
		}
   
  public function search_icd_x()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          icd_x.id_icd_x,
          icd_x.nama_icd_x_inggris,
          icd_x.kelompok,
          icd_x.keterangan,
          icd_x.kode_icd_x,
          icd_x.nama_icd_x,

          icd_x.nama_icd_x as value
          ");
        $this->db->like('icd_x.nama_icd_x', $q);
        $this->db->or_like('icd_x.kode_icd_x', $q);
        $this->db->order_by('nama_icd_x', 'asc');
        $this->db->limit(20);
        $result = $this->db->get('icd_x');
        echo json_encode( $result->result_array() );
			}
		}
  
  public function search_agama()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          agama.id_agama,
          agama.kode_agama,
          agama.nama_agama,
          agama.keterangan,

          agama.nama_agama as value
          ");
        $this->db->like('agama.nama_agama', $q);
        $this->db->order_by('nama_agama', 'asc');
        $result = $this->db->get('agama');
        echo json_encode( $result->result_array() );
			}
		}
  
  public function search_golongan_darah()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          golongan_darah.id_golongan_darah,
          golongan_darah.kode_golongan_darah,
          golongan_darah.nama_golongan_darah,
          golongan_darah.keterangan,

          golongan_darah.nama_golongan_darah as value
          ");
        $this->db->like('golongan_darah.nama_golongan_darah', $q);
        $this->db->order_by('nama_golongan_darah', 'asc');
        $result = $this->db->get('golongan_darah');
        echo json_encode( $result->result_array() );
			}
		}
  
  public function search_jenis_kelamin()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          jenis_kelamin.id_jenis_kelamin,
          jenis_kelamin.kode_jenis_kelamin,
          jenis_kelamin.nama_jenis_kelamin,
          jenis_kelamin.keterangan,

          jenis_kelamin.nama_jenis_kelamin as value
          ");
        $this->db->like('jenis_kelamin.nama_jenis_kelamin', $q);
        $this->db->order_by('nama_jenis_kelamin', 'asc');
        $result = $this->db->get('jenis_kelamin');
        echo json_encode( $result->result_array() );
			}
		}
  
  public function search_kewarganegaraan()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          kewarganegaraan.id_kewarganegaraan,
          kewarganegaraan.kode_kewarganegaraan,
          kewarganegaraan.nama_kewarganegaraan,
          kewarganegaraan.keterangan,

          kewarganegaraan.nama_kewarganegaraan as value
          ");
        $this->db->like('kewarganegaraan.nama_kewarganegaraan', $q);
        $this->db->order_by('nama_kewarganegaraan', 'asc');
        $result = $this->db->get('kewarganegaraan');
        echo json_encode( $result->result_array() );
			}
		}
  
  public function search_pendidikan()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          pendidikan.id_pendidikan,
          pendidikan.kode_pendidikan,
          pendidikan.nama_pendidikan,
          pendidikan.keterangan,

          pendidikan.nama_pendidikan as value
          ");
        $this->db->like('pendidikan.nama_pendidikan', $q);
        $this->db->order_by('nama_pendidikan', 'asc');
        $result = $this->db->get('pendidikan');
        echo json_encode( $result->result_array() );
			}
		}
  
  public function search_shdk()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          shdk.id_shdk,
          shdk.kode_shdk,
          shdk.nama_shdk,
          shdk.keterangan,

          shdk.nama_shdk as value
          ");
        $this->db->like('shdk.nama_shdk', $q);
        $this->db->order_by('nama_shdk', 'asc');
        $result = $this->db->get('shdk');
        echo json_encode( $result->result_array() );
			}
		}
  
  public function search_status_pernikahan()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          status_pernikahan.id_status_pernikahan,
          status_pernikahan.kode_status_pernikahan,
          status_pernikahan.nama_status_pernikahan,
          status_pernikahan.keterangan,

          status_pernikahan.nama_status_pernikahan as value
          ");
        $this->db->like('status_pernikahan.nama_status_pernikahan', $q);
        $this->db->order_by('nama_status_pernikahan', 'asc');
        $result = $this->db->get('status_pernikahan');
        echo json_encode( $result->result_array() );
			}
		}
  
  public function search_pekerjaan()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          pekerjaan.id_pekerjaan,
          pekerjaan.kode_pekerjaan,
          pekerjaan.nama_pekerjaan,
          pekerjaan.keterangan,

          pekerjaan.nama_pekerjaan as value
          ");
        $this->db->like('pekerjaan.nama_pekerjaan', $q);
        $this->db->order_by('nama_pekerjaan', 'asc');
        $result = $this->db->get('pekerjaan');
        echo json_encode( $result->result_array() );
			}
		}
  
  public function search_kemasan_obat()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          kemasan_obat.id_kemasan_obat,
          kemasan_obat.kode_kemasan_obat,
          kemasan_obat.nama_kemasan_obat,
          kemasan_obat.nama_kemasan_obat_inggris,
          kemasan_obat.keterangan,
          kemasan_obat.kelompok,

          kemasan_obat.nama_kemasan_obat as value
          ");
        $this->db->like('kemasan_obat.nama_kemasan_obat', $q);
        $this->db->order_by('nama_kemasan_obat', 'asc');
        $result = $this->db->get('kemasan_obat');
        echo json_encode( $result->result_array() );
			}
		}
  public function search_bagian_puskesmas()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          bagian_puskesmas.id_bagian_puskesmas,
          bagian_puskesmas.kode_bagian_puskesmas,
          bagian_puskesmas.nama_bagian_puskesmas,
          bagian_puskesmas.keterangan,

          bagian_puskesmas.nama_bagian_puskesmas as value
          ");
        $this->db->like('bagian_puskesmas.nama_bagian_puskesmas', $q);
        $this->db->order_by('nama_bagian_puskesmas', 'asc');
        $result = $this->db->get('bagian_puskesmas');
        echo json_encode( $result->result_array() );
			}
		}
  public function search_obat()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          obat.id_obat,
          obat.kode_obat,
          obat.nama_obat,
          obat.unit,
          obat.harga,
          obat.keterangan,

          obat.nama_obat as value
          ");
        $this->db->like('obat.nama_obat', $q);
        $this->db->order_by('nama_obat', 'asc');
        $result = $this->db->get('obat');
        echo json_encode( $result->result_array() );
			}
		}
    
  public function search_pabrik_obat()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          pabrik_obat.id_pabrik_obat,
          pabrik_obat.kode_pabrik_obat,
          pabrik_obat.nama_pabrik_obat,
          pabrik_obat.nama_pabrik_obat_inggris,
          pabrik_obat.kelompok,
          pabrik_obat.keterangan,

          pabrik_obat.nama_pabrik_obat as value
          ");
        $this->db->like('pabrik_obat.nama_pabrik_obat', $q);
        $this->db->order_by('nama_pabrik_obat', 'asc');
        $result = $this->db->get('pabrik_obat');
        echo json_encode( $result->result_array() );
			}
		}
    
  public function search_satuan_obat()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          satuan_obat.id_satuan_obat,
          satuan_obat.kode_satuan_obat,
          satuan_obat.nama_satuan_obat,
          satuan_obat.nama_satuan_obat_inggris,
          satuan_obat.kelompok,
          satuan_obat.keterangan,

          satuan_obat.nama_satuan_obat as value
          ");
        $this->db->like('satuan_obat.nama_satuan_obat', $q);
        $this->db->order_by('nama_satuan_obat', 'asc');
        $result = $this->db->get('satuan_obat');
        echo json_encode( $result->result_array() );
			}
		}
    
  public function search_sumber_anggaran()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          sumber_anggaran.id_sumber_anggaran,
          sumber_anggaran.kode_sumber_anggaran,
          sumber_anggaran.nama_sumber_anggaran,
          sumber_anggaran.nama_sumber_anggaran_inggris,
          sumber_anggaran.kelompok,
          sumber_anggaran.keterangan,

          sumber_anggaran.nama_sumber_anggaran as value
          ");
        $this->db->like('sumber_anggaran.nama_sumber_anggaran', $q);
        $this->db->order_by('nama_sumber_anggaran', 'asc');
        $result = $this->db->get('sumber_anggaran');
        echo json_encode( $result->result_array() );
			}
		}
    
  public function search_tindakan_medis()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          tindakan_medis.id_tindakan_medis,
          tindakan_medis.kode_tindakan_medis,
          tindakan_medis.nama_tindakan_medis,
          tindakan_medis.harga,
          tindakan_medis.keterangan,

          tindakan_medis.nama_tindakan_medis as value
          ");
        $this->db->like('tindakan_medis.nama_tindakan_medis', $q);
        $this->db->order_by('nama_tindakan_medis', 'asc');
        $result = $this->db->get('tindakan_medis');
        echo json_encode( $result->result_array() );
			}
		}
    
  public function search_pegawai()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          pegawai.id_pegawai,
          pegawai.nip_pegawai,
          pegawai.nama_pegawai, 
          pegawai.nama_pegawai as value
          ");
        $this->db->like('pegawai.nama_pegawai', $q);
        $this->db->limit(15);
        $this->db->order_by('nama_pegawai', 'asc');
        $result = $this->db->get('pegawai');
        echo json_encode( $result->result_array() );
			}
		}
  public function search_pemeriksaan_laborat()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          pemeriksaan_laborat.id_pemeriksaan_laborat,
          pemeriksaan_laborat.kode_pemeriksaan_laborat,
          pemeriksaan_laborat.nama_pemeriksaan_laborat,
          pemeriksaan_laborat.satuan,
          pemeriksaan_laborat.nilai_minimum,
          pemeriksaan_laborat.nilai_maximum,
          pemeriksaan_laborat.harga,
          pemeriksaan_laborat.keterangan,


          pemeriksaan_laborat.nama_pemeriksaan_laborat as value
          ");
        $this->db->like('pemeriksaan_laborat.nama_pemeriksaan_laborat', $q);
        $this->db->order_by('pemeriksaan_laborat.nama_pemeriksaan_laborat', 'asc');
        $result = $this->db->get('pemeriksaan_laborat');
        echo json_encode( $result->result_array() );
			}
		}
  public function search_jenis_diklat()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          jenis_diklat.id_jenis_diklat,
          jenis_diklat.kode_jenis_diklat,
          jenis_diklat.nama_jenis_diklat,

          jenis_diklat.nama_jenis_diklat as value
          ");
        $this->db->like('jenis_diklat.nama_jenis_diklat', $q);
        $this->db->order_by('jenis_diklat.nama_jenis_diklat', 'asc');
        $result = $this->db->get('jenis_diklat');
        echo json_encode( $result->result_array() );
			}
		}
  public function search_pangkat_pegawai()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
        $this->db->select("
          pangkat_pegawai.id_pangkat_pegawai,
          pangkat_pegawai.kode_pangkat_pegawai,
          pangkat_pegawai.nama_pangkat_pegawai,

          pangkat_pegawai.nama_pangkat_pegawai as value
          ");
        $this->db->like('pangkat_pegawai.nama_pangkat_pegawai', $q);
        $this->db->order_by('pangkat_pegawai.nama_pangkat_pegawai', 'asc');
        $result = $this->db->get('pangkat_pegawai');
        echo json_encode( $result->result_array() );
			}
		}
    
  public function search_golongan_pegawai()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 0)
			{
        $this->db->select("
          golongan_pegawai.id_golongan_pegawai,
          golongan_pegawai.kode_golongan_pegawai,
          golongan_pegawai.nama_golongan_pegawai,

          golongan_pegawai.nama_golongan_pegawai as value
          ");
        $this->db->like('golongan_pegawai.nama_golongan_pegawai', $q);
        $this->db->order_by('golongan_pegawai.nama_golongan_pegawai', 'asc');
        $result = $this->db->get('golongan_pegawai');
        echo json_encode( $result->result_array() );
			}
		}
    
  public function search_unit_kerja()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 0)
			{
        $this->db->select("
          unit_kerja.id_unit_kerja,
          unit_kerja.kode_unit_kerja,
          unit_kerja.nama_unit_kerja,

          unit_kerja.nama_unit_kerja as value
          ");
        $this->db->like('unit_kerja.nama_unit_kerja', $q);
        $this->db->order_by('unit_kerja.nama_unit_kerja', 'asc');
        $result = $this->db->get('unit_kerja');
        echo json_encode( $result->result_array() );
			}
		}
    
}