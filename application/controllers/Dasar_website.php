<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dasar_website extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Dasar_website_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'dasar_website/home';
    $this->load->view('tes', $data);
   }
   
  public function data()
   {
    $data['total'] = 10;
    $data['main_view'] = 'dasar_website/data';
    $this->load->view('tes', $data);
   }
   
  public function json_all_dasar_website()
   {
    $web=$this->uut->namadomain(base_url());
    $fields     = 
                  "
                  dasar_website.id_dasar_website,
                  dasar_website.domain,
                  dasar_website.alamat,
                  dasar_website.telpon,
                  dasar_website.email,
                  dasar_website.twitter,
                  dasar_website.facebook,
                  dasar_website.google,
                  dasar_website.instagram,
                  dasar_website.peta,
                  dasar_website.keterangan,
                  dasar_website.created_by,
                  dasar_website.created_time,
                  dasar_website.updated_by,
                  dasar_website.updated_time,
                  dasar_website.deleted_by,
                  dasar_website.deleted_time,
                  dasar_website.status,
                  ";
    $where      = array(
      'domain' => $web
    );
    $this->db->select("$fields");
    $this->db->where($where);
		$this->db->limit(1);
		$result = $this->db->get('dasar_website');
		echo json_encode($result->result_array());
   }
   
  public function cek_tema()
		{
      $web=$this->uut->namadomain(base_url());
      $where2 = array(
        'domain' => $web
        );
      $this->db->from('dasar_website');
      $this->db->where($where2);
      $a = $this->db->count_all_results();
      if($a == 0){
        
        $data_input = array(
    
          'domain' => $web,
          'alamat' => '-',
          'telpon' => '-',
          'email' => '-',
          'twitter' => '-',
          'facebook' => '-',
          'google' => '-',
          'instagram' => '-',
          'peta' => '-',
          'keterangan' => '',
          'created_by' => 1,
          'created_time' => date('Y-m-d H:i:s'),
          'updated_by' => 0,
          'updated_time' => date('Y-m-d H:i:s'),
          'deleted_by' => 0,
          'deleted_time' => date('Y-m-d H:i:s'),
          'status' => 1
                  
          );
        $table_name = 'dasar_website';
        $this->Dasar_website_model->simpan_dasar_website($data_input, $table_name);
        }
        echo 'OK';
		}
   
  public function update_dasar_website()
		{
    $web=$this->uut->namadomain(base_url());
    $this->form_validation->set_rules('alamat', 'alamat', 'required');
    if ($this->form_validation->run() == FALSE)
      {
      echo 0;
      }
    else
      {
      $where = array(
        'domain' => $web
        );
      $data_update = array(
        
        'alamat' => trim($this->input->post('alamat')),
        'telpon' => trim($this->input->post('telpon')),
        'email' => trim($this->input->post('email')),
        'twitter' => trim($this->input->post('twitter')),
        'facebook' => trim($this->input->post('facebook')),
        'google' => trim($this->input->post('google')),
        'instagram' => trim($this->input->post('instagram')),
        'peta' => trim($this->input->post('peta')),
        'keterangan' => trim($this->input->post('keterangan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'dasar_website';
      $where       = array(
        'domain' => $web
			);
      $this->Dasar_website_model->update_data_dasar_website($data_update, $where, $table_name);
      echo 1;
      }
    }
  
  public function total_data()
  {
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'dasar_website.status !=' => 99
      );
    $this->db->where($where0);
    $query0 = $this->db->get('dasar_website');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_data_dasar_website(){
		$table = 'dasar_website';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where      = array(
    );
    $orderby   = ''.$order_by.'';
    $query = $this->Dasar_website_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
          $urut=$urut+1;
          echo '<tr id_dasar_website="'.$row->id_dasar_website.'" id="'.$row->id_dasar_website.'" >';
          echo '<td valign="top">'.($urut).'</td>';
					echo '<td valign="top">'.$row->domain.'</td>';
					echo '<td valign="top">'.$row->keterangan.'</td>';
					echo '<td valign="top"><a href="#tab_form_dasar_website" data-toggle="tab" id="update_id" class="update_id badge bg-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>';
					echo '<a href="#" id="del_ajax" class="badge bg-danger btn-sm"><i class="fa fa-cut"></i></a>';
          echo '<a class="badge bg-warning btn-sm" href="'.base_url().'dasar_website/pdf/?id_dasar_website='.$row->id_dasar_website.'" ><i class="fa fa-file-pdf-o"></i></a></td>';
          echo '</tr>';
      }
          echo '<tr>';
          echo '<td valign="top" colspan="7" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="',base_url(),'dasar_website/xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
          echo '</tr>';
          
   }  
 }