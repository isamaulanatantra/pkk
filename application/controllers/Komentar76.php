<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Komentar extends CI_Controller
 {
    
  function __construct()
   {
    parent::__construct();
    $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Komentar_model');
   }
  
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'komentar/home';
    $this->load->view('back_bone', $data);
   }
  
  public function json_all_komentar()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $start      = ($halaman - 1) * $limit;
    $fields     = "*,(select user_name from users where users.id_users=komentar.created_by)as nama_pengguna";
    $where      = array(
      'komentar.status !=' => 99,
      'domain' => $web
    );
    $order_by   = 'komentar.parent';
    echo json_encode($this->Komentar_model->json_all_komentar($where, $limit, $start, $fields, $order_by));
   }
   
  public function simpan_komentar()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama_komentator', 'nama_komentator', 'required');
		$this->form_validation->set_rules('email_komentator', 'email_komentator', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('isi_komentar', 'isi_komentar', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'nama_komentator' => trim($this->input->post('nama_komentator')),
				'email_komentator' => trim($this->input->post('email_komentator')),
				'isi_komentar' => trim($this->input->post('isi_komentar')),
        'temp' => trim($this->input->post('temp')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'komentar';
      $id         = $this->Komentar_model->simpan_komentar($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'komentar',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
   
  public function update_komentar()
   {
    $web=$this->uut->namadomain(base_url());
    $this->form_validation->set_rules('nama_komentator', 'nama_komentator', 'required');
    $this->form_validation->set_rules('highlight', 'highlight', 'required');
    $this->form_validation->set_rules('tampil_menu', 'tampil_menu', 'required');
    $this->form_validation->set_rules('tampil_menu_atas', 'tampil_menu_atas', 'required');
		$this->form_validation->set_rules('urut', 'urut', 'required');
		$this->form_validation->set_rules('posisi', 'posisi', 'required');
		$this->form_validation->set_rules('icon', 'icon', 'required');
		$this->form_validation->set_rules('kata_kunci', 'kata_kunci', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
				'nama_komentator' => trim($this->input->post('nama_komentator')),
				'highlight' => trim($this->input->post('highlight')),
				'tampil_menu' => trim($this->input->post('tampil_menu')),
				'tampil_menu_atas' => trim($this->input->post('tampil_menu_atas')),
				'parent' => trim($this->input->post('parent')),
				'kata_kunci' => trim(str_replace('.', '', str_replace(',', '', $this->input->post('kata_kunci')))),
        'temp' => trim($this->input->post('temp')),
        'urut' => trim($this->input->post('urut')),
        'posisi' => trim($this->input->post('posisi')),
        'icon' => trim($this->input->post('icon')),
        'isi_komentar' => trim($this->input->post('isi_komentar')),
        'keterangan' => trim($this->input->post('keterangan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'komentar';
      $where       = array(
        'komentar.id_komentar' => trim($this->input->post('id_komentar')),
        'komentar.domain' => $web
			);
      $this->Komentar_model->update_data_komentar($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'id_komentar' => $this->input->post('id_komentar'),
        'komentar.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_komentar');
      $result = $this->db->get('komentar');
      echo json_encode($result->result_array());
		}
   
   public function total_komentar()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $this->db->from('komentar');
      $where    = array(
        'status' => 1,
        'domain' => $web
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_komentar = $this->input->post('id_komentar');
      $where = array(
        'id_komentar' => $id_komentar,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('komentar');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'komentar';
        $this->Komentar_model->update_data_komentar($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function restore()
		{
      $web=$this->uut->namadomain(base_url());
			$id_komentar = $this->input->post('id_komentar');
      $where = array(
        'id_komentar' => $id_komentar,
        'domain' => $web
        );
      $this->db->from('komentar');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'komentar';
        $this->Komentar_model->update_data_komentar($data_update, $where, $table_name);
        echo 1;
        }
		}
    
 }