<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Activated extends CI_Controller {

  function __construct()
    {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->model('Login_model');
    }
    
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
    $web = $this->uut->namadomain(base_url());
    $where = array(
            'user_name' => trim($this->input->get('e')),
            'temp' => trim($this->input->get('c'))
            );
    $this->db->select('*');
    $this->db->from('users');
    $this->db->where($where);
		$data_user = $this->db->get()->row_array();
    if(!$data_user)
      {
        exit;
      }
    else
      {
        $this->session->set_userdata( array(
                'id_users' => $data_user['id_users']
                ));                
        $data_update = array(
          'activated_time' => trim(date('Y-m-d H:i:s')),
          'status' => 1
        );
        $where       = array(
          'id_users' => trim($data_user['id_users'])
        );
        $this->db->where($where);
        $this->db->update('user', $data_update);
        
				$data_inputx = array(
					'id_users' => trim($data_user['id_users']),
					'keterangan' => trim('Anda melakukan aktivasi akun dengan email: '.trim($this->input->get('e')).', Melalui website: '.$web.' '),
					'domain' => ''.$web.'',
					'hak_akses' => trim('register'),
					'created_by' => trim($data_user['id_users']),
					'created_time' => date('Y-m-d H:i:s'),
					'status' => 1								
				);
				$this->db->insert( 'tracker_users', $data_inputx );
          
        return redirect(''.base_url().'sosegov'); 
      }
	}
  
}
