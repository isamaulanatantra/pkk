<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data_desa extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Data_desa_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'data_desa/home';
    $this->load->view('back_bone', $data);
   }
   
  public function load_the_option()
	{
		$a = 0;
		echo $this->option_desa();
	}
  
  private function option_desa(){
		$w = $this->db->query("SELECT * from desa where status = 1 order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'"> '.$h->nama_desa.' </option>';
		}
	}
  
  public function json_all_data_desa()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_data_desa    = $this->input->post('urut_data_data_desa');
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  data_desa.id_data_desa,
                  data_desa.jabatan_penandatangan,
                  data_desa.nama_penandatangan,
                  data_desa.pangkat_penandatangan,
                  data_desa.nip_penandatangan,
                  data_desa.desa_nama,
                  data_desa.desa_telpn,
                  data_desa.desa_alamat,
                  data_desa.desa_fax,
                  data_desa.desa_kode_pos,
                  data_desa.desa_nomor,
                  data_desa.desa_website,
                  data_desa.desa_email,
                  data_desa.id_desa,
                  data_desa.status
                  ";
    $where      = array(
      'status!=' => 99
    );
    $order_by   = 'data_desa.'.$urut_data_data_desa.'';
    echo json_encode($this->Data_desa_model->json_all_data_desa($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
   
  public function inaktifkan()
		{
      $where = array(
        'id_data_desa' => $this->input->post('id_data_desa')
        );
      $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'data_desa';
        $this->Data_desa_model->update_data_data_desa($data_update, $where, $table_name);
		}
   
  public function aktifkan()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
        'id_data_desa' => $this->input->post('id_data_desa')
        );
      $data_update = array(
      
        'status' => 1
                
        );
      $table_name  = 'data_desa';
      $this->Data_desa_model->update_data_data_desa($data_update, $where, $table_name);
      
      $where = array(
        'id_data_desa !=' => $this->input->post('id_data_desa')
        );
      $data_update = array(
      
        'status' => 0
                
        );
      $table_name  = 'data_desa';
      $this->Data_desa_model->update_data_data_desa($data_update, $where, $table_name);
      
		}
   
  public function cek_desa()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('desa');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'id_desa' => $row->id_desa
          );
        $this->db->from('data_desa');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'id_desa' => $row->id_desa,
            'desa_nama' => $row->nama_desa,
            'status' => 1
                    
            );
          $table_name = 'data_desa';
          $this->Data_desa_model->simpan_data_desa($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
   public function get_by_id()
		{
      $where    = array(
        'id_data_desa' => $this->input->post('id_data_desa')
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_data_desa');
      $result = $this->db->get('data_desa');
      echo json_encode($result->result_array());
		}
    
  public function update_data_desa()
   {
    $web=$this->uut->namadomain(base_url());
    $this->form_validation->set_rules('jabatan_penandatangan', 'jabatan_penandatangan', 'required');
    $this->form_validation->set_rules('nama_penandatangan', 'nama_penandatangan', 'required');
    $this->form_validation->set_rules('pangkat_penandatangan', 'pangkat_penandatangan', 'required');
    $this->form_validation->set_rules('nip_penandatangan', 'nip_penandatangan', 'required');
		$this->form_validation->set_rules('desa_nama', 'desa_nama', 'required');
		$this->form_validation->set_rules('desa_telpn', 'desa_telpn', 'required');
		$this->form_validation->set_rules('desa_alamat', 'desa_alamat', 'required');
		$this->form_validation->set_rules('desa_fax', 'desa_fax', 'required');
		$this->form_validation->set_rules('desa_kode_pos', 'desa_kode_pos', 'required');
		$this->form_validation->set_rules('desa_nomor', 'desa_nomor', 'required');
		$this->form_validation->set_rules('desa_website', 'desa_website', 'required');
		$this->form_validation->set_rules('desa_email', 'desa_email', 'required');
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('jenis_pemerintahan', 'jenis_pemerintahan', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
				'jabatan_penandatangan' => trim($this->input->post('jabatan_penandatangan')),
				'nama_penandatangan' => trim($this->input->post('nama_penandatangan')),
				'pangkat_penandatangan' => trim($this->input->post('pangkat_penandatangan')),
				'nip_penandatangan' => trim($this->input->post('nip_penandatangan')),
				'parent' => trim($this->input->post('parent')),
				'desa_fax' => trim(str_replace('.', '', str_replace(',', '', $this->input->post('desa_fax')))),
        'temp' => trim($this->input->post('temp')),
        'desa_nama' => trim($this->input->post('desa_nama')),
        'desa_telpn' => trim($this->input->post('desa_telpn')),
        'desa_alamat' => trim($this->input->post('desa_alamat')),
        'desa_kode_pos' => trim($this->input->post('desa_kode_pos')),
        'desa_nomor' => trim($this->input->post('desa_nomor')),
        'desa_website' => trim($this->input->post('desa_website')),
        'desa_email' => trim($this->input->post('desa_email')),
        'id_desa' => trim($this->input->post('id_desa')),
        'jenis_pemerintahan' => trim($this->input->post('jenis_pemerintahan')),
        'status' => 1
								
				);
      $table_name  = 'data_desa';
      $where       = array(
        'data_desa.id_data_desa' => trim($this->input->post('id_data_desa'))
			);
      $this->Posting_model->update_data_data_desa($data_update, $where, $table_name);
      echo 1;
     }
   }
  public function simpan_data_desa()
   {
    $web=$this->uut->namadomain(base_url());
    $this->form_validation->set_rules('jabatan_penandatangan', 'jabatan_penandatangan', 'required');
    $this->form_validation->set_rules('nama_penandatangan', 'nama_penandatangan', 'required');
    $this->form_validation->set_rules('pangkat_penandatangan', 'pangkat_penandatangan', 'required');
    $this->form_validation->set_rules('nip_penandatangan', 'nip_penandatangan', 'required');
		$this->form_validation->set_rules('desa_nama', 'desa_nama', 'required');
		$this->form_validation->set_rules('desa_telpn', 'desa_telpn', 'required');
		$this->form_validation->set_rules('desa_alamat', 'desa_alamat', 'required');
		$this->form_validation->set_rules('desa_fax', 'desa_fax', 'required');
		$this->form_validation->set_rules('desa_kode_pos', 'desa_kode_pos', 'required');
		$this->form_validation->set_rules('desa_nomor', 'desa_nomor', 'required');
		$this->form_validation->set_rules('desa_website', 'desa_website', 'required');
		$this->form_validation->set_rules('desa_email', 'desa_email', 'required');
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('jenis_pemerintahan', 'jenis_pemerintahan', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
			
				'jabatan_penandatangan' => trim($this->input->post('jabatan_penandatangan')),
				'nama_penandatangan' => trim($this->input->post('nama_penandatangan')),
				'pangkat_penandatangan' => trim($this->input->post('pangkat_penandatangan')),
				'nip_penandatangan' => trim($this->input->post('nip_penandatangan')),
				'desa_fax' => trim($this->input->post('desa_fax')),
        'desa_nama' => trim($this->input->post('desa_nama')),
        'desa_telpn' => trim($this->input->post('desa_telpn')),
        'desa_alamat' => trim($this->input->post('desa_alamat')),
        'desa_kode_pos' => trim($this->input->post('desa_kode_pos')),
        'desa_nomor' => trim($this->input->post('desa_nomor')),
        'desa_website' => trim($this->input->post('desa_website')),
        'desa_email' => trim($this->input->post('desa_email')),
        'id_desa' => trim($this->input->post('id_desa')),
        'jenis_pemerintahan' => trim($this->input->post('jenis_pemerintahan')),
        'status' => 1
								
				);
      $table_name  = 'data_desa';
      $id         = $this->Data_desa_model->simpan_data_desa($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'posting',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
  public function option_kecamatan(){
    $web=$this->uut->namadomain(base_url());
		if($web=='diskominfo.wonosobokab.go.id'){
			$where_id_kecamatan="";
		}
		else{
			$where_id_kecamatan="and data_kecamatan.id_kecamatan=".$this->session->userdata('id_kecamatan')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_kecamatan, kecamatan
		where data_kecamatan.status = 1
		and kecamatan.id_kecamatan=data_kecamatan.id_kecamatan
    ".$where_id_kecamatan."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
 }