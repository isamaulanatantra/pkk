<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Anggota_keluarga extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Anggota_keluarga_model');
   }
   
  public function index(){
    $data['main_view'] = 'anggota_keluarga/home';
    $this->load->view('tes', $data);
	}
  public function cetak_anggota_keluarga(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'anggota_keluarga/cetak_anggota_keluarga';
    $this->load->view('print', $data);
   }
	public function load_anggota_keluarga(){
      $mode_anggota_keluarga = $this->input->post('mode_anggota_keluarga');
      if( $mode_anggota_keluarga == 'edit' ){
      $where    = array(
				'id_data_keluarga' => $this->input->post('value')
				);
      }
      elseif ( $mode_anggota_keluarga == 'edit_anggota_keluarga' ){
      $where    = array(
				'temp' => $this->input->post('value')
				);
      }
      else{
      $where    = array(
				'temp' => $this->input->post('value')
				);
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_anggota_keluarga');
      $result = $this->db->get('anggota_keluarga');
      echo json_encode($result->result_array());
	}
	public function json_all_anggota_keluarga(){
    $web=$this->uut->namadomain(base_url());
		$table = 'anggota_keluarga';
    $id_data_keluarga    = $this->input->post('id_data_keluarga');
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
    ( select (desa.nama_desa) from desa where desa.id_desa=anggota_keluarga.id_desa limit 1) as nama_desa,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=anggota_keluarga.id_kecamatan limit 1) as nama_kecamatan,
    ( select (kabupaten.nama_kabupaten) from kabupaten where kabupaten.id_kabupaten=anggota_keluarga.id_kabupaten limit 1) as nama_kabupaten,
    ( select (propinsi.nama_propinsi) from propinsi where propinsi.id_propinsi=anggota_keluarga.id_propinsi limit 1) as nama_propinsi
    ";
    if($web=='demoopd.wonosobokab.go.id'){
      $where = array(
        'anggota_keluarga.status !=' => 99,
        'anggota_keluarga.id_data_keluarga' => $id_data_keluarga
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where = array(
        'anggota_keluarga.status !=' => 99,
        'anggota_keluarga.id_data_keluarga' => $id_data_keluarga
        );
    }else{
      $where = array(
        'anggota_keluarga.status !=' => 99,
        'anggota_keluarga.id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'anggota_keluarga.id_data_keluarga' => $id_data_keluarga
        );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_anggota_keluarga="'.$row->id_anggota_keluarga.'" id="'.$row->id_anggota_keluarga.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$row->nomor_registrasi.'</td>';
				echo '<td valign="top">'.$row->nama_anggota.'</td>';
				echo '<td valign="top">'.$row->status_dalam_keluarga.'</td>';
				echo '<td valign="top">'.$row->status_perkawinan.'</td>';
				echo '<td valign="top">'.$row->jenis_kelamin.'</td>';
				echo '<td valign="top">'.$row->tanggal_lahir.'</td>';
				echo '<td valign="top">'.$row->pendidikan.'</td>';
				echo '<td valign="top">'.$row->pekerjaan.'</td>';
				echo '<td valign="top">
				<a class="badge bg-warning btn-sm" href="'.base_url().'anggota_keluarga/cetak/?id_anggota_keluarga='.$row->id_anggota_keluarga.'" ><i class="fa fa-print"></i> Cetak</a>
				<a href="#tab_form_anggota_keluarga" data-toggle="tab" class="update_id_anggota_keluarga badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax_anggota_keluarga" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			echo '
      <tr>
        <td valign="top" colspan="10" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'anggota_keluarga/cetak_anggota_keluarga/?id_data_keluarga='.$id_data_keluarga.'&page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Anggota Keluarga</a>
        </td>
      </tr>
      ';
          
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $id_data_keluarga = trim($this->input->post('id_data_keluarga'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='demoopd.wonosobokab.go.id'){
      $where0 = array(
        'anggota_keluarga.status !=' => 99,
        'anggota_keluarga.id_data_keluarga' => $id_data_keluarga
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where0 = array(
        'anggota_keluarga.status !=' => 99,
        'anggota_keluarga.id_data_keluarga' => $id_data_keluarga
        );
    }else{
      $where0 = array(
        'anggota_keluarga.status !=' => 99,
        'anggota_keluarga.id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'anggota_keluarga.id_data_keluarga' => $id_data_keluarga
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('anggota_keluarga');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_anggota_keluarga(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nomor_registrasi', 'nomor_registrasi', 'required');
		$this->form_validation->set_rules('nik', 'nik', 'required');
		$this->form_validation->set_rules('nama_anggota', 'nama_anggota', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'id_data_keluarga' => trim($this->input->post('id_data_keluarga')),
				'nomor_registrasi' => trim($this->input->post('nomor_registrasi')),
        'nik' => trim($this->input->post('nik')),
        'nama_anggota' => trim($this->input->post('nama_anggota')),
        'jabatan' => trim($this->input->post('jabatan')),
        'jenis_kelamin' => trim($this->input->post('jenis_kelamin')),
        'tempat_lahir' => trim($this->input->post('tempat_lahir')),
        'tanggal_lahir' => trim($this->input->post('tanggal_lahir')),
        'status_perkawinan' => trim($this->input->post('status_perkawinan')),
        'status_dalam_keluarga' => trim($this->input->post('status_dalam_keluarga')),
        'agama' => trim($this->input->post('agama')),
        'alamat_anggota_keluarga' => trim($this->input->post('alamat_anggota_keluarga')),
        'status_tinggal' => trim($this->input->post('status_tinggal')),
        'id_propinsi' => trim($this->input->post('id_propinsi_anggota_keluarga')),
        'id_kabupaten' => trim($this->input->post('id_kabupaten_anggota_keluarga')),
        'id_kecamatan' => trim($this->input->post('id_kecamatan_anggota_keluarga')),
        'id_desa' => trim($this->input->post('id_desa_anggota_keluarga')),
        'pendidikan' => trim($this->input->post('pendidikan')),
        'pekerjaan' => trim($this->input->post('pekerjaan')),
        'aseptor_kb' => trim($this->input->post('aseptor_kb')),
        'jenis_aseptor_kb' => trim($this->input->post('jenis_aseptor_kb')),
        'aktif_dalam_kegiatan_posyandu' => trim($this->input->post('aktif_dalam_kegiatan_posyandu')),
        'frekuensi_volume_posyandu' => trim($this->input->post('frekuensi_volume_posyandu')),
        'mengikuti_program_bina_keluarga_balita' => trim($this->input->post('mengikuti_program_bina_keluarga_balita')),
        'memiliki_tabungan' => trim($this->input->post('memiliki_tabungan')),
        'mengikuti_kelompok_belajar' => trim($this->input->post('mengikuti_kelompok_belajar')),
        'jenis_kelompok_belajar' => trim($this->input->post('jenis_kelompok_belajar')),
        'mengikuti_paud_sejenis' => trim($this->input->post('mengikuti_paud_sejenis')),
        'ikut_dalam_kegiatan_koperasi' => trim($this->input->post('ikut_dalam_kegiatan_koperasi')),
        'jenis_koperasi' => trim($this->input->post('jenis_koperasi')),
        'berkebutuhan_khusus' => trim($this->input->post('berkebutuhan_khusus')),
        'berkebutuhan_khusus_fisik' => trim($this->input->post('berkebutuhan_khusus_fisik')),
        'temp' => trim($this->input->post('temp')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1
				);
      $table_name = 'anggota_keluarga';
      $id         = $this->Crud_model->save_data($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'anggota_keluarga',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
	}
	public function get_by_id(){
    $web=$this->uut->namadomain(base_url());
    if($web=='demoopd.wonosobokab.go.id'){
      $where    = array(
        'anggota_keluarga.id_anggota_keluarga' => $this->input->post('id_anggota_keluarga')
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where    = array(
        'anggota_keluarga.id_anggota_keluarga' => $this->input->post('id_anggota_keluarga')
        );
    }else{
      $where    = array(
        'anggota_keluarga.id_anggota_keluarga' => $this->input->post('id_anggota_keluarga'),
        'anggota_keluarga.domain' => $web
        );
    }
    $this->db->select("*");
    $this->db->where($where);
    $this->db->order_by('id_anggota_keluarga');
    $result = $this->db->get('anggota_keluarga');
    echo json_encode($result->result_array());
	}
  public function update_anggota_keluarga(){
		$this->form_validation->set_rules('id_anggota_keluarga', 'id_anggota_keluarga', 'required');
		$this->form_validation->set_rules('nomor_registrasi', 'nomor_registrasi', 'required');
		$this->form_validation->set_rules('nik', 'nik', 'required');
		$this->form_validation->set_rules('nama_anggota', 'nama_anggota', 'required');
    if ($this->form_validation->run() == FALSE){
      echo 0;
		}
    else{
      $data_update = array(
			
				'id_data_keluarga' => trim($this->input->post('id_data_keluarga')),
				'nomor_registrasi' => trim($this->input->post('nomor_registrasi')),
        'nik' => trim($this->input->post('nik')),
        'nama_anggota' => trim($this->input->post('nama_anggota')),
        'jabatan' => trim($this->input->post('jabatan')),
        'jenis_kelamin' => trim($this->input->post('jenis_kelamin')),
        'tempat_lahir' => trim($this->input->post('tempat_lahir')),
        'tanggal_lahir' => trim($this->input->post('tanggal_lahir')),
        'status_perkawinan' => trim($this->input->post('status_perkawinan')),
        'status_dalam_keluarga' => trim($this->input->post('status_dalam_keluarga')),
        'agama' => trim($this->input->post('agama')),
        'alamat_anggota_keluarga' => trim($this->input->post('alamat_anggota_keluarga')),
        'status_tinggal' => trim($this->input->post('status_tinggal')),
        'id_propinsi' => trim($this->input->post('id_propinsi_anggota_keluarga')),
        'id_kabupaten' => trim($this->input->post('id_kabupaten_anggota_keluarga')),
        'id_kecamatan' => trim($this->input->post('id_kecamatan_anggota_keluarga')),
        'id_desa' => trim($this->input->post('id_desa_anggota_keluarga')),
        'pendidikan' => trim($this->input->post('pendidikan')),
        'pekerjaan' => trim($this->input->post('pekerjaan')),
        'aseptor_kb' => trim($this->input->post('aseptor_kb')),
        'jenis_aseptor_kb' => trim($this->input->post('jenis_aseptor_kb')),
        'aktif_dalam_kegiatan_posyandu' => trim($this->input->post('aktif_dalam_kegiatan_posyandu')),
        'frekuensi_volume_posyandu' => trim($this->input->post('frekuensi_volume_posyandu')),
        'mengikuti_program_bina_keluarga_balita' => trim($this->input->post('mengikuti_program_bina_keluarga_balita')),
        'memiliki_tabungan' => trim($this->input->post('memiliki_tabungan')),
        'mengikuti_kelompok_belajar' => trim($this->input->post('mengikuti_kelompok_belajar')),
        'jenis_kelompok_belajar' => trim($this->input->post('jenis_kelompok_belajar')),
        'mengikuti_paud_sejenis' => trim($this->input->post('mengikuti_paud_sejenis')),
        'ikut_dalam_kegiatan_koperasi' => trim($this->input->post('ikut_dalam_kegiatan_koperasi')),
        'jenis_koperasi' => trim($this->input->post('jenis_koperasi')),
        'berkebutuhan_khusus' => trim($this->input->post('berkebutuhan_khusus')),
        'berkebutuhan_khusus_fisik' => trim($this->input->post('berkebutuhan_khusus_fisik')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
			);
      $table_name  = 'anggota_keluarga';
      $where       = array(
        'anggota_keluarga.id_anggota_keluarga' => trim($this->input->post('id_anggota_keluarga'))
			);
      $this->Anggota_keluarga_model->update_anggota_keluarga($data_update, $where, $table_name);
      echo 1;
		}
  }
	public function hapus1(){
		$id_anggota_keluarga = $this->input->post('id_anggota_keluarga');
		$where = array(
			'id_anggota_keluarga' => $id_anggota_keluarga,
			'created_by' => $this->session->userdata('id_users')
			);
		$this->db->from('anggota_keluarga');
		$this->db->where($where);
		$a = $this->db->count_all_results();
		if($a == 0){
			echo 0;
			}
		else{
			$this->db->where($where);
			$this->db->delete('anggota_keluarga');
			echo 1;
			}
	}
  public function hapus(){
    $web=$this->uut->namadomain(base_url());
    $id_anggota_keluarga = $this->input->post('id_anggota_keluarga');
    $where = array(
      'id_anggota_keluarga' => $id_anggota_keluarga,
      'created_by' => $this->session->userdata('id_users')
      );
    $this->db->from('anggota_keluarga');
    $this->db->where($where);
    $a = $this->db->count_all_results();
    if($a == 0){
      echo 0;
    }
    else{
      $data_update = array(
      
        'status' => 99
                
        );
      $table_name  = 'anggota_keluarga';
      $this->Anggota_keluarga_model->update_anggota_keluarga($data_update, $where, $table_name);
      echo 1;
    }
  }
}