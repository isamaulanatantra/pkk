<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pengaduan_masyarakat extends CI_Controller {
    
  function __construct() {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->library('Crud_model');
    $this->load->model('Pengaduan_masyarakat_model');
   }
  
  public function index() {
    $web=$this->uut->namadomain(base_url());
	$tanggal = date('Y-m-d H:i:s');
    $data['hari_ini'] = $tanggal;
    $data['tanggal_hari_ini'] = ''.$this->Crud_model->dateBahasaIndo($tanggal).' ';
    $data['judul_halaman'] = 'PENGADUAN MASYARAKAT';
    $data['nama_halaman'] = 'pengaduan_masyarakat';
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->status == 1 ){
            $data['status_komponen'] = $row1->status;
          }
          else{ 
          }
        }
    $data['main_view'] = 'pengaduan_masyarakat/form';
    $this->load->view('welcome_layanan', $data);
   }
	
  public function op() {
    $web=$this->uut->namadomain(base_url());
		$tanggal = date('Y-m-d H:i:s');
    $data['hari_ini'] = $tanggal;
    $data['tanggal_hari_ini'] = ''.$this->Crud_model->dateBahasaIndo($tanggal).' ';
    $data['judul_halaman'] = 'PENGADUAN MASYARAKAT';
    $data['nama_halaman'] = 'pengaduan_masyarakat';
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pengaduan_masyarakat/form1';
    $this->load->view('pengaduan_masyarakat', $data);
   }
	 
  public function simpan_pengaduan_masyarakat(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('pekerjaan', 'pekerjaan', 'required');
		$this->form_validation->set_rules('nomor_telp', 'nomor_telp', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('rincian_informasi_yang_diinginkan', 'rincian_informasi_yang_diinginkan', 'required');
		$this->form_validation->set_rules('tujuan_penggunaan_informasi', 'tujuan_penggunaan_informasi', 'required');
		$this->form_validation->set_rules('kategori_permohonan_informasi_publik', 'kategori_permohonan_informasi_publik', 'required');
		$this->form_validation->set_rules('cara_melihat_membaca_mendengarkan_mencatat', 'cara_melihat_membaca_mendengarkan_mencatat', 'required');
		$this->form_validation->set_rules('cara_hardcopy_softcopy', 'cara_hardcopy_softcopy', 'required');
		$this->form_validation->set_rules('cara_mengambil_langsung', 'cara_mengambil_langsung', 'required');
		$this->form_validation->set_rules('cara_kurir', 'cara_kurir', 'required');
		$this->form_validation->set_rules('cara_pos', 'cara_pos', 'required');
		$this->form_validation->set_rules('cara_faksimili', 'cara_faksimili', 'required');
		$this->form_validation->set_rules('cara_email', 'cara_email', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('created_by', 'created_by', 'required');
		
        $healthy = array("#", ",", "=", "!", "?", "(", ")", "%", "&", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
        $yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
        $nama = str_replace($healthy, $yummy, $this->input->post('nama'));
        $alamat = str_replace($healthy, $yummy, $this->input->post('alamat'));
        $pekerjaan = str_replace($healthy, $yummy, $this->input->post('pekerjaan'));
        $nomor_telp = str_replace($healthy, $yummy, $this->input->post('nomor_telp'));
        $email = str_replace($healthy, $yummy, $this->input->post('email'));
        $rincian_informasi_yang_diinginkan = str_replace($healthy, $yummy, $this->input->post('rincian_informasi_yang_diinginkan'));
        $tujuan_penggunaan_informasi = str_replace($healthy, $yummy, $this->input->post('tujuan_penggunaan_informasi'));
        $kategori_permohonan_informasi_publik = str_replace($healthy, $yummy, $this->input->post('kategori_permohonan_informasi_publik'));
				
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'forward' => '',
				'id_posting' => $this->input->post('id_posting'),
				'parent' => 0,
				'nama' => $nama,
				'instansi' => 'Umum',
				'pembimbing' => '',
				'alamat' => $alamat,
				'pekerjaan' => $pekerjaan,
				'nomor_telp' => $nomor_telp,
				'email' => $email,
				'rincian_informasi_yang_diinginkan' => $rincian_informasi_yang_diinginkan,
				'tujuan_penggunaan_informasi' => $tujuan_penggunaan_informasi,
				'cara_melihat_membaca_mendengarkan_mencatat' => trim($this->input->post('cara_melihat_membaca_mendengarkan_mencatat')),
				'cara_hardcopy_softcopy' => trim($this->input->post('cara_hardcopy_softcopy')),
				'cara_mengambil_langsung' => trim($this->input->post('cara_mengambil_langsung')),
				'cara_kurir' => trim($this->input->post('cara_kurir')),
				'cara_pos' => trim($this->input->post('cara_pos')),
				'cara_faksimili' => trim($this->input->post('cara_faksimili')),
				'cara_email' => trim($this->input->post('cara_email')),
				'kategori_permohonan_informasi_publik' => $kategori_permohonan_informasi_publik,
        'temp' => trim($this->input->post('temp')),
				'created_by' => $this->input->post('created_by'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1,
        'balas' => 0

				);
      $table_name = 'permohonan_informasi_publik';
      $id         = $this->Pengaduan_masyarakat_model->simpan_permohonan_informasi_publik($data_input, $table_name);
      echo $id;
			$table_name  = 'lampiran_pengaduan_masyarakat';
      $where       = array(
        'table_name' => 'permohonan_informasi_publik',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
	
  public function simpan_pengaduan_masyarakat_comment(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('pekerjaan', 'pekerjaan', 'required');
		$this->form_validation->set_rules('nomor_telp', 'nomor_telp', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('rincian_informasi_yang_diinginkan', 'rincian_informasi_yang_diinginkan', 'required');
		$this->form_validation->set_rules('tujuan_penggunaan_informasi', 'tujuan_penggunaan_informasi', 'required');
		$this->form_validation->set_rules('kategori_permohonan_informasi_publik', 'kategori_permohonan_informasi_publik', 'required');
		$this->form_validation->set_rules('cara_melihat_membaca_mendengarkan_mencatat', 'cara_melihat_membaca_mendengarkan_mencatat', 'required');
		$this->form_validation->set_rules('cara_hardcopy_softcopy', 'cara_hardcopy_softcopy', 'required');
		$this->form_validation->set_rules('cara_mengambil_langsung', 'cara_mengambil_langsung', 'required');
		$this->form_validation->set_rules('cara_kurir', 'cara_kurir', 'required');
		$this->form_validation->set_rules('cara_pos', 'cara_pos', 'required');
		$this->form_validation->set_rules('cara_faksimili', 'cara_faksimili', 'required');
		$this->form_validation->set_rules('cara_email', 'cara_email', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('created_by', 'created_by', 'required');
		
        $healthy = array("#", ",", "=", "!", "?", "(", ")", "%", "&", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
        $yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
        $nama = str_replace($healthy, $yummy, $this->input->post('nama'));
        $alamat = str_replace($healthy, $yummy, $this->input->post('alamat'));
        $pekerjaan = str_replace($healthy, $yummy, $this->input->post('pekerjaan'));
        $nomor_telp = str_replace($healthy, $yummy, $this->input->post('nomor_telp'));
        $email = str_replace($healthy, $yummy, $this->input->post('email'));
        $rincian_informasi_yang_diinginkan = str_replace($healthy, $yummy, $this->input->post('rincian_informasi_yang_diinginkan'));
        $tujuan_penggunaan_informasi = str_replace($healthy, $yummy, $this->input->post('tujuan_penggunaan_informasi'));
        $kategori_permohonan_informasi_publik = str_replace($healthy, $yummy, $this->input->post('kategori_permohonan_informasi_publik'));
				
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'forward' => '',
				'id_posting' => $this->input->post('id_posting'),
				'parent' => $this->input->post('parent'),
				'nama' => $nama,
				'instansi' => 'Umum',
				'pembimbing' => '',
				'alamat' => $alamat,
				'pekerjaan' => $pekerjaan,
				'nomor_telp' => $nomor_telp,
				'email' => $email,
				'rincian_informasi_yang_diinginkan' => $rincian_informasi_yang_diinginkan,
				'tujuan_penggunaan_informasi' => $tujuan_penggunaan_informasi,
				'cara_melihat_membaca_mendengarkan_mencatat' => trim($this->input->post('cara_melihat_membaca_mendengarkan_mencatat')),
				'cara_hardcopy_softcopy' => trim($this->input->post('cara_hardcopy_softcopy')),
				'cara_mengambil_langsung' => trim($this->input->post('cara_mengambil_langsung')),
				'cara_kurir' => trim($this->input->post('cara_kurir')),
				'cara_pos' => trim($this->input->post('cara_pos')),
				'cara_faksimili' => trim($this->input->post('cara_faksimili')),
				'cara_email' => trim($this->input->post('cara_email')),
				'kategori_permohonan_informasi_publik' => $kategori_permohonan_informasi_publik,
        'temp' => trim($this->input->post('temp')),
				'created_by' => $this->input->post('created_by'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1,
        'balas' => 1

				);
      $table_name = 'permohonan_informasi_publik';
      $id         = $this->Pengaduan_masyarakat_model->simpan_permohonan_informasi_publik($data_input, $table_name);
      echo $id;
			$table_name  = 'permohonan_informasi_publik';
      $where       = array(
        'id_permohonan_informasi_publik' => trim($this->input->post('parent'))
				);
			$data_update = array(
				'balas' => 1
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
	
  function load_table(){
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
		if($web=='ppid.wonosobokab.go.id'){
			$w = $this->db->query("
			SELECT *
			from permohonan_informasi_publik, data_skpd
			where permohonan_informasi_publik.status = 1
			and permohonan_informasi_publik.parent = 0
			and permohonan_informasi_publik.tujuan_penggunaan_informasi ='Pengaduan Masyarakat'
			and permohonan_informasi_publik.domain=data_skpd.skpd_website
			order by permohonan_informasi_publik.created_time desc
			");
		}
		else{
			$w = $this->db->query("
			SELECT *
			from permohonan_informasi_publik
			where status = 1
			and parent = 0
			and domain ='".$web."'
			and tujuan_penggunaan_informasi ='Pengaduan Masyarakat'
			order by created_time desc
			");
		}
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h){
			$tujuan = $h->tujuan_penggunaan_informasi;
			if($tujuan=='Komentar'){
			$costum = 'warning';
			$fa = 'comments';
			}
			elseif($tujuan=='Pengaduan Masyarakat'){
			$costum = 'success';
			$fa = 'user';
			}
			else{
			$costum = 'primary';
			$fa = 'envelope';
			}
			echo '
				<li id_permohonan_informasi_publik="'.$h->id_permohonan_informasi_publik.'" id="'.$h->id_permohonan_informasi_publik.'">
									<i class="fa fa-'.$fa.' bg-'.$costum.'"></i>
									<div class="timeline-item card">
									  <div class="timeline-header">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm text-'.$costum.'" src="'.base_url().'assets/images/blank_user.png" alt="user image">
										<span class="username">';
											if(!$ses) { echo'
										  <a href="#">'.$h->nama.'</a>
											'; }
											else{echo '
										  <a href="#">'.$h->nama.'</a>
										  <a href="#" class="float-right btn-tool" id="del_ajax"><i class="fa fa-times"></i></a>
											';} 
											echo '
										</span>
										<span class="description">';
										if(!$ses) { echo'...'; }
										else{echo ''.$h->alamat.'';} 
										echo ' '.$this->Crud_model->dateBahasaIndo1($h->created_time).'
										</span>
									  </div>
									  </div>

									  <div class="timeline-body">';
										if(!$ses) { echo''; }
										else{
											echo '<p><u>Pekerjaan:</u> '.$h->pekerjaan.'</p>';
											echo '<p><u>Nomor Telp.:</u> '.$h->nomor_telp.'</p>';
											echo '<p><u>Email:</u> '.$h->email.'</p>';
											echo '<p><u>Judul Posting:</u> '.$h->judul_posting_permohonan.'</p>';
											echo '<p><u>Tujuan:</u> <span class="text-'.$costum.'">'.$h->tujuan_penggunaan_informasi.'</span></p>';
											// echo '<p><u>Website:</u> </p>';
											echo '<p><u>Rincian:</u></p>';
											} 
										echo '
										<p>'.$h->rincian_informasi_yang_diinginkan.'</p>
									  </div>
									  <div class="timeline-footer card-comment">
										  <p>
												<a href="#" class="link-black text-sm"><i class="fa fa-thumbs-up mr-1 text-'.$costum.'"></i></a>
												<a href="#" class="link-black text-sm mr-2"><i class="fa fa-thumbs-down mr-1 text-danger"></i></a>
												<a href="https://'.$h->domain.'/pengaduan_masyarakat" class="text-muted link-black text-sm mr-2"><i class="fa fa-tags mr-1 text-info"></i>'.$h->domain.'</a>
												<a href="'.base_url().'pengaduan_masyarakat/detail/'.$h->id_permohonan_informasi_publik.'" class="text-muted link-black text-sm mr-2">';
														$w11 = $this->db->query("
														SELECT *
														FROM permohonan_informasi_publik
														WHERE parent = $h->id_permohonan_informasi_publik
														and status=1
														");
														$query_like_admin = $this->db->query("
														SELECT *
														FROM permohonan_informasi_publik
														WHERE parent = ".$h->id_permohonan_informasi_publik."
														and nama
														LIKE 'Admin%'
														");
														$jumlah_comments = $w11->num_rows(); 
														$jumlah_like = $query_like_admin->num_rows();
														if(($query_like_admin->num_rows())>0){
															echo '<i class="fa fa-toggle-on mr-1 text-success"></i>';
														}
														else{
															echo '<i class="fa fa-toggle-off mr-1 text-warning"></i>';
														}
													echo ' '.$jumlah_comments.' Baca Selengkapnya';
													echo ' 
												</a>
										  </p>
									  </div>
									  
									</div>
								  </li>
                      <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                      </li>
			';
		}
	}
	
  function load_table1(){
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
		if($web=='ppid.wonosobokab.go.id'){
			$w = $this->db->query("
			SELECT *
			from permohonan_informasi_publik, data_skpd
			where permohonan_informasi_publik.status = 1
			and permohonan_informasi_publik.parent = 0
			and permohonan_informasi_publik.tujuan_penggunaan_informasi ='Pengaduan Masyarakat'
			and permohonan_informasi_publik.domain=data_skpd.skpd_website
			order by permohonan_informasi_publik.created_time desc
			");
		}
		else{
			$w = $this->db->query("
			SELECT *
			from permohonan_informasi_publik
			where status = 1
			and parent = 0
			and domain ='".$web."'
			and tujuan_penggunaan_informasi ='Pengaduan Masyarakat'
			order by created_time desc
			");
		}
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h){
			$tujuan = $h->tujuan_penggunaan_informasi;
			if($tujuan=='Komentar'){
			$costum = 'warning';
			$fa = 'comments';
			}
			elseif($tujuan=='Pengaduan Masyarakat'){
			$costum = 'success';
			$fa = 'user';
			}
			else{
			$costum = 'primary';
			$fa = 'envelope';
			}
			echo '
				<tr id_permohonan_informasi_publik="'.$h->id_permohonan_informasi_publik.'" id="'.$h->id_permohonan_informasi_publik.'">
					<td>
									<div class="card">
									  <div class="card-header">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm text-'.$costum.'" src="'.base_url().'assets/images/blank_user.png" alt="user image">
										<span class="username">';
											if(!$ses) { echo'
										  <a href="#">'.$h->nama.'</a>
											'; }
											else{echo '
										  <a href="#">'.$h->nama.'</a>
										  <a href="#" class="float-right btn-tool" id="del_ajax"><i class="fa fa-times"></i></a>
											';} 
											echo '
										</span>
										<span class="description">';
										if(!$ses) { echo''; }
										else{echo ''.$h->alamat.'';} 
										echo ' '.$this->Crud_model->dateBahasaIndo1($h->created_time).'
										</span>
									  </div>
									  </div>

									  <div class="card-body">';
										if(!$ses) { echo''; }
										else{
											echo '<p><u>Pekerjaan:</u> '.$h->pekerjaan.'</p>';
											echo '<p><u>Nomor Telp.:</u> '.$h->nomor_telp.'</p>';
											echo '<p><u>Email:</u> '.$h->email.'</p>';
											echo '<p><u>Tujuan:</u> <span class="text-'.$costum.'">'.$h->tujuan_penggunaan_informasi.'</span></p>';
											// echo '<p><u>Website:</u> </p>';
											echo '<p><u>Rincian:</u></p>';
											} 
										echo '
										<p>'.$h->rincian_informasi_yang_diinginkan.'</p>
										<div class="col-md-6">';
										$where1 = array(
											'id_tabel' => $h->id_permohonan_informasi_publik,
											'table_name' => 'permohonan_informasi_publik'
											);
										$this->db->where($where1);
										$this->db->order_by('uploaded_time desc');
										$query1 = $this->db->get('lampiran_pengaduan_masyarakat');
										if(($query1->num_rows())>0){
											echo'
											<div id="pengaduan_masyarakat" class="carousel slide" data-ride="carousel">
															<ol class="carousel-indicators">
																<li data-target="#pengaduan_masyarakat" data-slide-to="0" class="active"></li>
																<li data-target="#pengaduan_masyarakat" data-slide-to="1" class=""></li>
																<li data-target="#pengaduan_masyarakat" data-slide-to="2" class=""></li>
															</ol>
															<div class="carousel-inner">
											';
											$a = 0;
											foreach ($query1->result() as $row1)
												{
													$a = $a+1;
													if( $a == 1 ){
														echo
														'
																<div class="carousel-item active">
																	<img class="d-block w-100" src="'.base_url().'media/lampiran_pengaduan_masyarakat/'.$row1->file_name.'" alt="First slide">
																	<div class="carousel-caption bg-gray">
																		<span class="text-danger">'.$row1->keterangan.'
																	</div>
																</div>
														';
														}
													else{
														echo
														'
																<div class="carousel-item">
																	<img class="d-block w-100" src="'.base_url().'media/lampiran_pengaduan_masyarakat/'.$row1->file_name.'" alt="Second slide">
																	<div class="carousel-caption bg-gray">
																		<span class="text-danger">'.$row1->keterangan.'
																	</div>
																</div>
														';
														} 
												}
											echo '
															</div>
															<a class="carousel-control-prev" href="#pengaduan_masyarakat" role="button" data-slide="prev">
																<span class="carousel-control-prev-icon" aria-hidden="true"></span>
																<span class="sr-only">Previous</span>
															</a>
															<a class="carousel-control-next" href="#pengaduan_masyarakat" role="button" data-slide="next">
																<span class="carousel-control-next-icon" aria-hidden="true"></span>
																<span class="sr-only">Next</span>
															</a>
														</div>
											';
										}
										echo '
										</div>
										<a target="_blank" href="https://'.$h->domain.'" class="btn btn-default btn-sm"><i class="fa fa-link mr-1 text-info"></i>'.$h->domain.'</a>';
										if($web=='ppid.wonosobokab.go.id'){
											if(!$ses) {
											}else{
												echo'
														<a class="btn btn-default btn-sm" href="'.base_url().'pengaduan_masyarakat/forward/'.$h->id_permohonan_informasi_publik.'" target="_blank"><i class="fa fa-share"></i> Forward</a>
														';
											}
										}
										echo'
										<a href="'.base_url().'pengaduan_masyarakat/detail/'.$h->id_permohonan_informasi_publik.'" class="btn btn-default btn-sm"><i class="fa fa-thumbs-o-up"></i> Baca Selengkapnya</a>
										<span class="float-right text-muted">
												<a href="'.base_url().'pengaduan_masyarakat/detail/'.$h->id_permohonan_informasi_publik.'" class="text-muted link-black text-sm mr-2">';
														$w11 = $this->db->query("
														SELECT *
														FROM permohonan_informasi_publik
														WHERE parent = ".$h->id_permohonan_informasi_publik."
														and status=1
														");
														$query_like_admin = $this->db->query("
														SELECT *
														FROM permohonan_informasi_publik
														WHERE parent = ".$h->id_permohonan_informasi_publik."
														and nama
														LIKE 'Admin%'
														");
														$jumlah_comments = $w11->num_rows(); 
														$jumlah_like = $query_like_admin->num_rows();
														if(($query_like_admin->num_rows())>0){
															echo '<i class="fa fa-comments mr-1 text-success"></i>';
														}
														else{
															echo '<i class="fa fa-comment mr-1 text-warning"></i>';
														}
													echo ' '.$jumlah_comments.' Komentar';
													echo ' 
												</a>
										</span>
									  </div>
									  <div class="card-footer card-comment">';
										foreach($w11->result() as $row_komen){
											echo'
										  <div class="card-comment">
												<img class="img-circle img-sm" src="'.base_url().'assets/images/blank_user.png" alt="User Image">
												<div class="comment-text">
													<span class="username">
														'.$row_komen->nama.'
														<span class="text-muted float-right">'.$this->Crud_model->dateBahasaIndo1($row_komen->created_time).'</span>
													</span>
													<p>'.$row_komen->rincian_informasi_yang_diinginkan.'</p>
												</div>
											</div>
											';
											if(($w11->num_rows())>1){echo '<hr />';}else{echo '';}
										}
										echo'
									  </div>
									</div>
					</td>
				</tr>
			';
		}
	}
  function load_table_forward(){
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
		if($web=='ppid.wonosobokab.go.id'){
			$web_aduan='';
		}
		else{
			$web_aduan="and forward ='".$web."'";
		}
		$w = $this->db->query("
		SELECT *, 
		(select user_name from users where users.id_users=permohonan_informasi_publik.created_by) as nama_pengguna, 
		(select judul_posting from posting where posting.id_posting=permohonan_informasi_publik.id_posting) as judul_posting_permohonan
		from permohonan_informasi_publik
		where status != 99
		and parent = 0
		".$web_aduan."
		and kategori_permohonan_informasi_publik ='Pengaduan Masyarakat'
		order by created_time desc
		");
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h){
			$tujuan = $h->tujuan_penggunaan_informasi;
			if($tujuan=='Komentar'){
			$costum = 'warning';
			$fa = 'comments';
			}
			elseif($tujuan=='Pengaduan Masyarakat'){
			$costum = 'success';
			$fa = 'user';
			}
			else{
			$costum = 'primary';
			$fa = 'envelope';
			}
			echo '
				<tr id_permohonan_informasi_publik_forward="'.$h->id_permohonan_informasi_publik.'" id="'.$h->id_permohonan_informasi_publik.'">
					<td>
									<div class="card">
									  <div class="card-header">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm text-'.$costum.'" src="'.base_url().'assets/images/blank_user.png" alt="user image">
										<span class="username">';
											if(!$ses) { echo'
										  <a href="#">'.$h->nama.'</a>
											'; }
											else{echo '
										  <a href="#">'.$h->nama.'</a>
										  <a href="#" class="float-right btn-tool" id="del_ajax1"><i class="fa fa-times"></i></a>
											';} 
											echo '
										</span>
										<span class="description">';
										if(!$ses) { echo''; }
										else{echo ''.$h->alamat.'';} 
										echo ' '.$this->Crud_model->dateBahasaIndo1($h->created_time).'
										</span>
									  </div>
									  </div>

									  <div class="card-body">';
										if(!$ses) { echo''; }
										else{
											echo '<p><u>Pekerjaan:</u> '.$h->pekerjaan.'</p>';
											echo '<p><u>Nomor Telp.:</u> '.$h->nomor_telp.'</p>';
											echo '<p><u>Email:</u> '.$h->email.'</p>';
											echo '<p><u>Judul Posting:</u> '.$h->judul_posting_permohonan.'</p>';
											echo '<p><u>Tujuan:</u> <span class="text-'.$costum.'">'.$h->tujuan_penggunaan_informasi.'</span></p>';
											// echo '<p><u>Website:</u> </p>';
											echo '<p><u>Rincian:</u></p>';
											} 
										echo '
										<p>'.$h->rincian_informasi_yang_diinginkan.'</p>
										<a target="_blank" href="https://'.$h->domain.'" class="btn btn-default btn-sm"><i class="fa fa-link mr-1 text-info"></i>'.$h->domain.'</a>';
										if($web=='ppid.wonosobokab.go.id'){
											if(!$ses) {
											}else{
												echo'
														<a class="btn btn-default btn-sm" href="'.base_url().'pengaduan_masyarakat/forward/'.$h->id_permohonan_informasi_publik.'" target="_blank"><i class="fa fa-share"></i> Forward</a>
														';
											}
										}
										echo'
										<a href="'.base_url().'pengaduan_masyarakat/detail/'.$h->id_permohonan_informasi_publik.'" class="btn btn-default btn-sm"><i class="fa fa-thumbs-o-up"></i> Baca Selengkapnya</a>
										<span class="float-right text-muted">
												<a href="'.base_url().'pengaduan_masyarakat/detail/'.$h->id_permohonan_informasi_publik.'" class="text-muted link-black text-sm mr-2">';
														$w11 = $this->db->query("
														SELECT *
														FROM permohonan_informasi_publik
														WHERE parent = ".$h->id_permohonan_informasi_publik."
														and status=1
														");
														$query_like_admin = $this->db->query("
														SELECT *
														FROM permohonan_informasi_publik
														WHERE parent = ".$h->id_permohonan_informasi_publik."
														and nama
														LIKE 'Admin%'
														");
														$jumlah_comments = $w11->num_rows(); 
														$jumlah_like = $query_like_admin->num_rows();
														if(($query_like_admin->num_rows())>0){
															echo '<i class="fa fa-comments mr-1 text-success"></i>';
														}
														else{
															echo '<i class="fa fa-comment mr-1 text-warning"></i>';
														}
													echo ' '.$jumlah_comments.' Komentar';
													echo ' 
												</a>
										</span>
									  </div>
									  <div class="card-footer card-comment">';
										foreach($w11->result() as $row_komen){
											echo'
										  <div class="card-comment">
												<img class="img-circle img-sm" src="'.base_url().'assets/images/blank_user.png" alt="User Image">
												<div class="comment-text">
													<span class="username">
														'.$row_komen->nama.'
														<span class="text-muted float-right">'.$this->Crud_model->dateBahasaIndo1($row_komen->created_time).'</span>
													</span>
													<p>'.$row_komen->rincian_informasi_yang_diinginkan.'</p>
												</div>
											</div>
											';
											if(($w11->num_rows())>1){echo '<hr />';}else{echo '';}
										}
										echo'
									  </div>
									</div>
					</td>
				</tr>
			';
		}
	}
	
  function load_table_comment() {
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
		if($web=='ppid.wonosobokab.go.id'){
			$web_aduan='';
		}
		else{
			$web_aduan="and domain ='".$web."'";
		}
    $id_permohonan_informasi_publik=$this->input->post('id_permohonan_informasi_publik');
		$w = $this->db->query("
		SELECT *, 
		(select user_name from users where users.id_users=permohonan_informasi_publik.created_by) as nama_pengguna, 
		(select judul_posting from posting where posting.id_posting=permohonan_informasi_publik.id_posting) as judul_posting_permohonan
		from permohonan_informasi_publik
		where status = 1
		".$web_aduan."
		and parent='".$id_permohonan_informasi_publik."'
		order by created_time desc
		");
		
		if(($w->num_rows())>0){
			$nomor = 0;
			echo '<h4>Balasan</h4>';
			foreach($w->result() as $h){
				echo '
						<tr class="" id_permohonan_informasi_publik="'.$h->id_permohonan_informasi_publik.'" id="'.$h->id_permohonan_informasi_publik.'">
							<td>
									<div class="card-comment">
										<img class="img-circle img-sm" src="'.base_url().'assets/images/blank_user.png" alt="User Image">
										<div class="comment-text">
											<span class="username">';
												if(!$ses) {
													echo'
													'.$h->nama.'
													<span class="text-muted float-right">'.$this->Crud_model->dateBahasaIndo1($h->created_time).'
													</span>
													'; 
												}
												else{
													echo '
													Nama: '.$h->nama.'
													<span class="text-muted float-right">'.$this->Crud_model->dateBahasaIndo1($h->created_time).'
													<a href="#" id="del_ajax"><i class="fa fa-remove"></i></a>
													</span>
													';
													} 
												echo '</span>';
											if(!$ses){echo ''.$h->rincian_informasi_yang_diinginkan.'';}
											else{
												echo '
												Alamat: '.$h->alamat.'<br />
												No.Telp: '.$h->nomor_telp.'<br />
												Email: '.$h->email.'<br />
												Pekerjaan: '.$h->pekerjaan.'<br /> 
												Rindian: '.$h->rincian_informasi_yang_diinginkan.'
												';}
											echo '
										</div>
									</div>
						</td>
					</tr>
				';
			}
		}
		else{
		}
	}
	
  public function detail() {
    $web=$this->uut->namadomain(base_url());
	$tanggal = date('Y-m-d H:i:s');
    $data['hari_ini'] = $tanggal;
    $data['tanggal_hari_ini'] = ''.$this->Crud_model->dateBahasaIndo($tanggal).' ';
    $data['judul_halaman'] = 'PENGADUAN MASYARAKAT';
    $data['nama_halaman'] = 'pengaduan_masyarakat';
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $where = array(
			'id_permohonan_informasi_publik' => $this->uri->segment(3),
            'status!=' => 99
	);
    $d = $this->Pengaduan_masyarakat_model->get_data($where);
    if(!$d){
		$data['id_permohonan_informasi_publik'] = '';
		$data['nama'] = '';
		$data['alamat'] = '';
		$data['pekerjaan'] = '';
		$data['nomor_telp'] = '';
		$data['email'] = '';
		$data['created_time'] = '';
		$data['tujuan_penggunaan_informasi'] = '';
		$data['rincian_informasi_yang_diinginkan'] = '';
	  }
	else{
		$ses=$this->session->userdata('id_users');
		if(!$ses) {
		$data['alamat'] = '';
		$data['pekerjaan'] = '';
		$data['nomor_telp'] = '';
		$data['email'] = '';
		$data['tujuan_penggunaan_informasi'] = '';
		$data['nama'] = $d['nama'];
		$data['created_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['created_time']).'';
		$data['rincian_informasi_yang_diinginkan'] = $d['rincian_informasi_yang_diinginkan'];
		}
		else{
		$data['id_permohonan_informasi_publik'] = $d['id_permohonan_informasi_publik'];
		$data['nama'] = 'Nama: '.$d['nama'].' ';
		$data['alamat'] = 'Alamat: '.$d['alamat'].' <br />';
		$data['pekerjaan'] = 'Pekerjaan: '.$d['pekerjaan'].' <br />';
		$data['nomor_telp'] = 'Nomor Telp.: '.$d['nomor_telp'].' <br />';
		$data['email'] = 'Email: '.$d['email'].' <br />';
		$data['tujuan_penggunaan_informasi'] = 'Tujuan: '.$d['tujuan_penggunaan_informasi'].' <br />';
		$data['created_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['created_time']).'';
		$data['rincian_informasi_yang_diinginkan'] = 'Rincian: '.$d['rincian_informasi_yang_diinginkan'].' ';
		}
	  }
		$w = $this->db->query("
		SELECT *
		from permohonan_informasi_publik
		where status = 1
		and domain='".$web."'
		and parent=".$this->uri->segment(3)."
		");
		$jumlah_komentar = $w->num_rows();
    $data['jumlah_komentar'] = $jumlah_komentar;
    $data['main_view'] = 'pengaduan_masyarakat/detail';
    $this->load->view('pengaduan_masyarakat', $data);
   }
	
  public function index_tes() {
    $web=$this->uut->namadomain(base_url());
		$tanggal = date('Y-m-d H:i:s');
    $data['hari_ini'] = $tanggal;
    $data['tanggal_hari_ini'] = ''.$this->Crud_model->dateBahasaIndo($tanggal).' ';
    $data['judul_halaman'] = 'PENGADUAN MASYARAKAT';
    $data['nama_halaman'] = 'pengaduan_masyarakat';
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pengaduan_masyarakat/form';
    $this->load->view('pengaduan_masyarakat', $data);
   }
	 
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_permohonan_informasi_publik = $this->input->post('id_permohonan_informasi_publik');
      $where = array(
        'id_permohonan_informasi_publik' => $id_permohonan_informasi_publik,
        'domain' => ''.$web.''
        );
      $this->db->from('permohonan_informasi_publik');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'permohonan_informasi_publik';
        $this->Pengaduan_masyarakat_model->update_data_permohonan_informasi_publik($data_update, $where, $table_name);
        echo 1;
        }
		}
  public function hapus_forward()
		{
      $web=$this->uut->namadomain(base_url());
			$id_permohonan_informasi_publik = $this->input->post('id_permohonan_informasi_publik_forward');
      $where = array(
        'id_permohonan_informasi_publik' => $id_permohonan_informasi_publik,
        'forward' => ''.$web.''
        );
      $this->db->from('permohonan_informasi_publik');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'permohonan_informasi_publik';
        $this->Pengaduan_masyarakat_model->update_data_permohonan_informasi_publik($data_update, $where, $table_name);
        echo 1;
        }
		}
	public function forward(){
    $web=$this->uut->namadomain(base_url());
		$tanggal = date('Y-m-d H:i:s');
    $data['hari_ini'] = $tanggal;
    $data['tanggal_hari_ini'] = ''.$this->Crud_model->dateBahasaIndo($tanggal).' ';
    $data['judul_halaman'] = 'PENGADUAN MASYARAKAT';
    $data['nama_halaman'] = 'pengaduan_masyarakat';
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $where = array(
			'id_permohonan_informasi_publik' => $this->uri->segment(3),
            'status' => 1
		);
    $d = $this->Pengaduan_masyarakat_model->get_data($where);
    if(!$d){
			$data['id_permohonan_informasi_publik'] = '';
			$data['nama'] = '';
			$data['alamat'] = '';
			$data['pekerjaan'] = '';
			$data['nomor_telp'] = '';
			$data['email'] = '';
			$data['created_time'] = '';
			$data['tujuan_penggunaan_informasi'] = '';
			$data['rincian_informasi_yang_diinginkan'] = '';
	  }else{
			$ses=$this->session->userdata('id_users');
			if(!$ses) {
				$data['alamat'] = '';
				$data['pekerjaan'] = '';
				$data['nomor_telp'] = '';
				$data['email'] = '';
				$data['tujuan_penggunaan_informasi'] = '';
				$data['nama'] = $d['nama'];
				$data['created_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['created_time']).'';
				$data['rincian_informasi_yang_diinginkan'] = $d['rincian_informasi_yang_diinginkan'];
			}else{
				$data['id_permohonan_informasi_publik'] = $d['id_permohonan_informasi_publik'];
				$data['nama'] = 'Nama: '.$d['nama'].' ';
				$data['alamat'] = 'Alamat: '.$d['alamat'].' <br />';
				$data['pekerjaan'] = 'Pekerjaan: '.$d['pekerjaan'].' <br />';
				$data['nomor_telp'] = 'Nomor Telp.: '.$d['nomor_telp'].' <br />';
				$data['email'] = 'Email: '.$d['email'].' <br />';
				$data['tujuan_penggunaan_informasi'] = 'Tujuan: '.$d['tujuan_penggunaan_informasi'].' <br />';
				$data['created_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['created_time']).'';
				$data['rincian_informasi_yang_diinginkan'] = 'Rincian: '.$d['rincian_informasi_yang_diinginkan'].' ';
			}
	  }
    $data['main_view'] = 'pengaduan_masyarakat/forward';
    $this->load->view('pengaduan_masyarakat', $data);
	}

	public function laporan(){
	  $data['main_view'] = 'pengaduan_masyarakat/laporan_pengaduan_masyarakat';
	  $this->load->view('tes', $data);
	}
	 public function view(){
	   $search = $_POST['search']['value']; // Ambil data yang di ketik user pada textbox pencarian
	   $limit = $_POST['length']; // Ambil data limit per page
	   $start = $_POST['start']; // Ambil data start
	   $order_index = $_POST['order'][0]['column']; // Untuk mengambil index yg menjadi acuan untuk sorting
	   $order_field = $_POST['columns'][$order_index]['data']; // Untuk mengambil nama field yg menjadi acuan untuk sorting
	   $order_ascdesc = $_POST['order'][0]['dir']; // Untuk menentukan order by "ASC" atau "DESC"
   
	   $sql_total = $this->Pengaduan_masyarakat_model->count_all(); // Panggil fungsi count_all pada Pengaduan_masyarakat_model
	   $sql_data = $this->Pengaduan_masyarakat_model->filter($search, $limit, $start, $order_field, $order_ascdesc); // Panggil fungsi filter pada Pengaduan_masyarakat_model
	   $sql_filter = $this->Pengaduan_masyarakat_model->count_filter($search); // Panggil fungsi count_filter pada Pengaduan_masyarakat_model
   
	   $callback = array(
		   'draw'=>$_POST['draw'], // Ini dari datatablenya
		   'recordsTotal'=>$sql_total,
		   'recordsFiltered'=>$sql_filter,
		   'data'=>$sql_data
	   );
   
	   header('Content-Type: application/json');
	   echo json_encode($callback); // Convert array $callback ke json
	 }
}