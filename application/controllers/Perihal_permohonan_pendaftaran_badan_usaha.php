<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Perihal_permohonan_pendaftaran_badan_usaha extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Perihal_permohonan_pendaftaran_badan_usaha_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'perihal_permohonan_pendaftaran_badan_usaha/home';
    $this->load->view('tes', $data);
   }
   
  public function perihal_surat(){
		$table = 'perihal_permohonan_pendaftaran_badan_usaha';
		$where   = array(
			'perihal_permohonan_pendaftaran_badan_usaha.status !=' => 99
			);
		$fields  = 
			"
			*
			";
		$order_by  = 'perihal_permohonan_pendaftaran_badan_usaha.judul_perihal_permohonan_pendaftaran_badan_usaha';
		echo json_encode($this->Crud_model->opsi($table, $where, $fields, $order_by));
  }
  public function perihal_surat_permohonan_organisasi(){
		$table = 'perihal_permohonan_pendaftaran_badan_usaha';
		$where   = array(
			'perihal_permohonan_pendaftaran_badan_usaha.status !=' => 99,
			'perihal_permohonan_pendaftaran_badan_usaha.keterangan' => 'permohonan_organisasi'
			);
		$fields  = 
			"
			*
			";
		$order_by  = 'perihal_permohonan_pendaftaran_badan_usaha.judul_perihal_permohonan_pendaftaran_badan_usaha';
		echo json_encode($this->Crud_model->opsi($table, $where, $fields, $order_by));
  }
  public function json_option(){
		$table = 'perihal_permohonan_pendaftaran_badan_usaha';
		$hak_akses = $this->session->userdata('hak_akses');
		if($hak_akses=='root'){
			$where   = array(
				'perihal_permohonan_pendaftaran_badan_usaha.status !=' => 99
				);
		}
		else{
			$where   = array(
				'perihal_permohonan_pendaftaran_badan_usaha.status !=' => 99
				);
		}
		$fields  = 
			"
			*
			";
		$order_by  = 'perihal_permohonan_pendaftaran_badan_usaha.judul_perihal_permohonan_pendaftaran_badan_usaha';
		echo json_encode($this->Crud_model->opsi($table, $where, $fields, $order_by));
  }
	public function json_all_perihal_permohonan_pendaftaran_badan_usaha(){
    $web=$this->uut->namadomain(base_url());
		$table = 'perihal_permohonan_pendaftaran_badan_usaha';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where = array(
      'perihal_permohonan_pendaftaran_badan_usaha.status !=' => 99,
      'perihal_permohonan_pendaftaran_badan_usaha.domain' => $web
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_perihal_permohonan_pendaftaran_badan_usaha="'.$row->id_perihal_permohonan_pendaftaran_badan_usaha.'" id="'.$row->id_perihal_permohonan_pendaftaran_badan_usaha.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$row->judul_perihal_permohonan_pendaftaran_badan_usaha.'</td>';
				echo '<td valign="top">'.$row->keterangan.'</td>';
				echo '<td valign="top">
				<a class="badge bg-warning btn-sm" href="'.base_url().'perihal_permohonan_pendaftaran_badan_usaha/cetak/?id_perihal_permohonan_pendaftaran_badan_usaha='.$row->id_perihal_permohonan_pendaftaran_badan_usaha.'" ><i class="fa fa-print"></i> Cetak</a>
				<a href="#tab_form_perihal_permohonan_pendaftaran_badan_usaha" data-toggle="tab" class="update_id badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			echo '<tr>';
				echo '<td valign="top" colspan="7" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'perihal_permohonan_pendaftaran_badan_usaha/cetak_xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
			echo '</tr>';
          
	}
  public function total_data()
  {
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'perihal_permohonan_pendaftaran_badan_usaha.status !=' => 99,
      'perihal_permohonan_pendaftaran_badan_usaha.status !=' => 999
      );
    $this->db->where($where0);
    $query0 = $this->db->get('perihal_permohonan_pendaftaran_badan_usaha');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_perihal_permohonan_pendaftaran_badan_usaha()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_perihal_permohonan_pendaftaran_badan_usaha', 'judul_perihal_permohonan_pendaftaran_badan_usaha', 'required');
		$this->form_validation->set_rules('isi_perihal_permohonan_pendaftaran_badan_usaha', 'isi_perihal_permohonan_pendaftaran_badan_usaha', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'judul_perihal_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('judul_perihal_permohonan_pendaftaran_badan_usaha')),
        'temp' => trim($this->input->post('temp')),
        'icon' => trim($this->input->post('icon')),
        'isi_perihal_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('isi_perihal_permohonan_pendaftaran_badan_usaha')),
        'keterangan' => trim($this->input->post('keterangan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'perihal_permohonan_pendaftaran_badan_usaha';
      $id         = $this->Perihal_permohonan_pendaftaran_badan_usaha_model->simpan_perihal_permohonan_pendaftaran_badan_usaha($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'perihal_permohonan_pendaftaran_badan_usaha',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
  public function update_perihal_permohonan_pendaftaran_badan_usaha()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_perihal_permohonan_pendaftaran_badan_usaha', 'judul_perihal_permohonan_pendaftaran_badan_usaha', 'required');
		$this->form_validation->set_rules('isi_perihal_permohonan_pendaftaran_badan_usaha', 'isi_perihal_permohonan_pendaftaran_badan_usaha', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'judul_perihal_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('judul_perihal_permohonan_pendaftaran_badan_usaha')),
        'isi_perihal_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('isi_perihal_permohonan_pendaftaran_badan_usaha')),
        'icon' => trim($this->input->post('icon')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'perihal_permohonan_pendaftaran_badan_usaha';
      $where       = array(
        'perihal_permohonan_pendaftaran_badan_usaha.id_perihal_permohonan_pendaftaran_badan_usaha' => trim($this->input->post('id_perihal_permohonan_pendaftaran_badan_usaha')),
        'perihal_permohonan_pendaftaran_badan_usaha.domain' => $web
			);
      $this->Perihal_permohonan_pendaftaran_badan_usaha_model->update_data_perihal_permohonan_pendaftaran_badan_usaha($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'perihal_permohonan_pendaftaran_badan_usaha.id_perihal_permohonan_pendaftaran_badan_usaha' => $this->input->post('id_perihal_permohonan_pendaftaran_badan_usaha'),
        'perihal_permohonan_pendaftaran_badan_usaha.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_perihal_permohonan_pendaftaran_badan_usaha');
      $result = $this->db->get('perihal_permohonan_pendaftaran_badan_usaha');
      echo json_encode($result->result_array());
		}
   
   public function total_perihal_permohonan_pendaftaran_badan_usaha()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('perihal_permohonan_pendaftaran_badan_usaha');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function cek_perihal_permohonan_pendaftaran_badan_usaha()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('master_perihal_permohonan_pendaftaran_badan_usaha');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'judul_perihal_permohonan_pendaftaran_badan_usaha' => $row->nama_master_perihal_permohonan_pendaftaran_badan_usaha,
          'domain' => $web
          );
        $this->db->from('perihal_permohonan_pendaftaran_badan_usaha');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'domain' => $web,
            'judul_perihal_permohonan_pendaftaran_badan_usaha' => $row->nama_master_perihal_permohonan_pendaftaran_badan_usaha,
            'isi_perihal_permohonan_pendaftaran_badan_usaha' => $row->isi_master_perihal_permohonan_pendaftaran_badan_usaha_default,
            'temp' => '-',
            'created_by' => 1,
            'created_time' => date('Y-m-d H:i:s'),
            'updated_by' => 1,
            'updated_time' => date('Y-m-d H:i:s'),
            'deleted_by' => 1,
            'deleted_time' => date('Y-m-d H:i:s'),
            'keterangan' => '-',
            'icon' => 'fa-home',
            'status' => 1
                    
            );
          $table_name = 'perihal_permohonan_pendaftaran_badan_usaha';
          $this->Perihal_permohonan_pendaftaran_badan_usaha_model->simpan_perihal_permohonan_pendaftaran_badan_usaha($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
  public function inaktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_perihal_permohonan_pendaftaran_badan_usaha = $this->input->post('id_perihal_permohonan_pendaftaran_badan_usaha');
      $where = array(
        'id_perihal_permohonan_pendaftaran_badan_usaha' => $id_perihal_permohonan_pendaftaran_badan_usaha
        );
      $this->db->from('perihal_permohonan_pendaftaran_badan_usaha');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'perihal_permohonan_pendaftaran_badan_usaha';
        if( $cek <> '' ){
          $this->Perihal_permohonan_pendaftaran_badan_usaha_model->update_data_perihal_permohonan_pendaftaran_badan_usaha($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_perihal_permohonan_pendaftaran_badan_usaha = $this->input->post('id_perihal_permohonan_pendaftaran_badan_usaha');
      $where = array(
        'id_perihal_permohonan_pendaftaran_badan_usaha' => $id_perihal_permohonan_pendaftaran_badan_usaha
        );
      $this->db->from('perihal_permohonan_pendaftaran_badan_usaha');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'perihal_permohonan_pendaftaran_badan_usaha';
        if( $cek <> '' ){
          $this->Perihal_permohonan_pendaftaran_badan_usaha_model->update_data_perihal_permohonan_pendaftaran_badan_usaha($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
    
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_perihal_permohonan_pendaftaran_badan_usaha = $this->input->post('id_perihal_permohonan_pendaftaran_badan_usaha');
      $where = array(
        'id_perihal_permohonan_pendaftaran_badan_usaha' => $id_perihal_permohonan_pendaftaran_badan_usaha,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('perihal_permohonan_pendaftaran_badan_usaha');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'perihal_permohonan_pendaftaran_badan_usaha';
        $this->Perihal_permohonan_pendaftaran_badan_usaha_model->update_data_perihal_permohonan_pendaftaran_badan_usaha($data_update, $where, $table_name);
        echo 1;
        }
		}
 }