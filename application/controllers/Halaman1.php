<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Halaman extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Halaman_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'halaman/home';
    $this->load->view('back_bone', $data);
   }
   
  public function json_all_posting()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_posting    = $this->input->post('urut_data_posting');
    $parent    = $this->input->post('parent');
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  *,
                  (select user_name from users where users.id_users=posting.created_by) as nama_pengguna
                  ";
    if($parent==''){$parent=0;}
    else{$parent;}
    $where      = array(
      'domain' => $web,
      'parent' => $parent,
      'status!=' => 99
    );
    $order_by   = 'posting.'.$urut_data_posting.'';
    echo json_encode($this->Halaman_model->json_all_posting($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
  public function json_all_posting_arsip()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_posting    = $this->input->post('urut_data_posting');
    $parent    = $this->input->post('parent');
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  *,
                  (select user_name from users where users.id_users=posting.created_by) as nama_pengguna
                  ";
    if($parent==''){$parent=0;}
    else{$parent;}
    $where      = array(
      'domain' => $web,
      'parent' => $parent,
      'status' => 99
    );
    $order_by   = 'posting.'.$urut_data_posting.'';
    echo json_encode($this->Halaman_model->json_all_posting($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
   
  public function inaktifkan()
		{
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Halaman_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
      $web=$this->uut->namadomain(base_url());
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Halaman_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function cek_tema()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('tema');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'judul_posting' => $row->nama_tema,
          'domain' => $web
          );
        $this->db->from('posting');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'domain' => $web,
            'judul_posting' => $row->nama_tema,
            'keterangan' => $row->judul,
            'created_by' => 1,
            'created_time' => date('Y-m-d H:i:s'),
            'status' => 1
                    
            );
          $table_name = 'posting';
          $this->Halaman_model->simpan_posting($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
   public function total_posting_tos()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $halaman    = $this->input->post('halaman');
      $limit    = $this->input->post('limit');
      $kata_kunci    = $this->input->post('kata_kunci');
      $urut_data_posting    = $this->input->post('urut_data_posting');
      $parent    = $this->input->post('parent');
      $this->db->from('posting');
      if( $kata_kunci <> '' ){
        $this->db->like('judul_posting', $kata_kunci);
        $this->db->or_like('domain', $kata_kunci);
      }
      if($parent==''){$parent=0;}
      else{$parent;}
      $where    = array(
        'status' => 1,
        'parent' => $parent,
        'domain' => $web
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
    
   public function total_posting()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $this->db->from('posting');
      $where    = array(
        'status' => 1,
        'domain' => $web
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  function load_table()
	{
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_posting    = $this->input->post('urut_data_posting');
    $start      = ($halaman - 1) * $limit;
		$a = 0;
		echo $this->posting(0,$h="", $a, $limit, $kata_kunci);
	}
  
  private function posting($parent=0,$hasil, $a, $limit, $kata_kunci){
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_posting    = $this->input->post('urut_data_posting');
    $start      = ($halaman - 1) * $limit;
		$a = $a + 1;
		$this->db->select("
    *, 
    (select user_name from users where users.id_users=posting.created_by) as nama_pengguna
    ");
    if( $kata_kunci <> '' ){
      $this->db->like('judul_posting', $kata_kunci);
      $this->db->or_like('domain', $kata_kunci);
    }
    $where      = array(
      'domain' => $web,
      'parent' => $parent
    );
    $this->db->where($where);
		$this->db->order_by('posting.'.$urut_data_posting.'');
		$this->db->limit($limit, $start);
		$w = $this->db->get('posting');
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<tr id_posting="'.$h->id_posting.'" id="'.$h->id_posting.'" >';
      if( $h->posisi == 'menu_kiri' ){
      $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> Menu-Kiri </td>';
      }
      elseif( $h->posisi == 'menu_atas' ){
      $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> Menu-Atas </td>';
      }
      elseif( $h->posisi == 'menu_kanan' ){
      $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> Menu-Kanan </td>';
      }
      elseif( $h->posisi == ' independen' ){
      $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> Independen </td>';
      }
      else{
      $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> Unknown </td>';
      }
      if( $a > 1 ){
        $hasil .= '<td style="padding: 2px 2px 2px '.($a * 20).'px ;"> <a target="_blank" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a> </td>';
        }
			else{
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->judul_posting.' </td>';
        }
      // $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->urut.' </td>';
      if( $h->tampil_menu_atas == 0 ){
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> Tidak </td>';
      }
      else{
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> Ya </td>';
      }
      if( $h->highlight == 0 ){
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> Tidak </td>';
      }
      else{
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> Ya </td>';
      }
      if( $h->tampil_menu == 0 ){
        //$hasil .= '<td style="padding: 2px 2px 2px 2px ;"> Tidak </td>';
      }
      else{
        //$hasil .= '<td style="padding: 2px 2px 2px 2px ;"> Ya </td>';
      }
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->nama_pengguna.' </td>';
      
			$hasil .= 
			'
			<td style="padding: 2px 2px 2px 2px ;"> 
			<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> 
			<a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> 
			</td>
			';
			$hasil .= '</tr>';
			$hasil = $this->posting($h->id_posting,$hasil, $a, $limit, $kata_kunci);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  
  function load_the_option_by_posisi_filter()
	{
    $web=$this->uut->namadomain(base_url());
    $posisi = trim($this->input->post('posisi'));
		$a = 0;
		echo $this->option_posting_by_posisi_filter(0,$h="", $a, $posisi);
	}
  
  private function option_posting_by_posisi_filter($parent=0,$hasil, $a, $posisi){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from posting 
                          where parent=0
                          and status = 1 
                          and posisi='".$posisi."'
                          and domain='".$web."'
                          order by urut");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_posting.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_posting.'';
			}
			else{
			$hasil .= ''.$h->judul_posting.'';
			}
			$hasil .= '</option>';
			// $hasil = $this->option_posting_by_posisi($h->id_posting,$hasil, $a, $posisi);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
 }