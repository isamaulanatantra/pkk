<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ekonomi_kreatif extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Ekonomi_kreatif_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'ekonomi_kreatif/home';
    $this->load->view('tes', $data);
   }
   
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "ekonomi_kreatif.id_ekonomi_kreatif";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'ekonomi_kreatif.status !=' => 99,
      'ekonomi_kreatif.domain' => $web
      );
    $this->db->where($where0);
    $query0 = $this->db->get('ekonomi_kreatif');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_ekonomi_kreatif(){
    $web=$this->uut->namadomain(base_url());
		$table = 'ekonomi_kreatif';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    ekonomi_kreatif.id_ekonomi_kreatif,
    ekonomi_kreatif.nama,
    ekonomi_kreatif.kategori,
    ekonomi_kreatif.jenis_kelamin,
    ekonomi_kreatif.alamat,
    ekonomi_kreatif.nomor_telepon,
    ekonomi_kreatif.email,
    ekonomi_kreatif.keterangan
    ";
    $where = array(
      'ekonomi_kreatif.status !=' => 99,
      'ekonomi_kreatif.domain' => $web
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_ekonomi_kreatif="'.$row->id_ekonomi_kreatif.'" id="'.$row->id_ekonomi_kreatif.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$row->nama.'</td>';
				echo '<td valign="top">'.$row->kategori.'</td>';
				echo '<td valign="top">'.$row->jenis_kelamin.'</td>';
				echo '<td valign="top">'.$row->alamat.'</td>';
				echo '<td valign="top">0'.$row->nomor_telepon.'</td>';
				echo '<td valign="top">'.$row->email.'</td>';
				echo '<td valign="top">
				<a class="badge bg-warning btn-sm" href="'.base_url().'ekonomi_kreatif/cetak/?id_ekonomi_kreatif='.$row->id_ekonomi_kreatif.'" ><i class="fa fa-print"></i> Cetak</a>
				<a href="#tab_form_ekonomi_kreatif" data-toggle="tab" class="update_id badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			echo '<tr>';
				echo '<td valign="top" colspan="8" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'ekonomi_kreatif/cetak_xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
			echo '</tr>';
          
	}
  public function simpan_ekonomi_kreatif(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('kategori', 'kategori', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'nama' => trim($this->input->post('nama')),
        'temp' => trim($this->input->post('temp')),
        'kategori' => trim($this->input->post('kategori')),
        'keterangan' => trim($this->input->post('keterangan')),
        'jenis_kelamin' => trim($this->input->post('jenis_kelamin')),
        'alamat' => trim($this->input->post('alamat')),
        'nomor_telepon' => trim($this->input->post('nomor_telepon')),
        'email' => trim($this->input->post('email')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1
				);
      $table_name = 'ekonomi_kreatif';
      $id         = $this->Ekonomi_kreatif_model->simpan_ekonomi_kreatif($data_input, $table_name);
      echo $id;
			$table_name  = 'anggota_organisasi';
      $where       = array(
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_ekonomi_kreatif' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
	}
  public function update_ekonomi_kreatif(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('kategori', 'kategori', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
        'nama' => trim($this->input->post('nama')),
        'temp' => trim($this->input->post('temp')),
        'kategori' => trim($this->input->post('kategori')),
        'jenis_kelamin' => trim($this->input->post('jenis_kelamin')),
        'keterangan' => trim($this->input->post('keterangan')),
        'alamat' => trim($this->input->post('alamat')),
        'nomor_telepon' => trim($this->input->post('nomor_telepon')),
        'email' => trim($this->input->post('email')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
				);
      $table_name  = 'ekonomi_kreatif';
      $where       = array(
        'ekonomi_kreatif.id_ekonomi_kreatif' => trim($this->input->post('id_ekonomi_kreatif')),
        'ekonomi_kreatif.domain' => $web
			);
      $id = $this->Ekonomi_kreatif_model->update_data_ekonomi_kreatif($data_update, $where, $table_name);
      echo $id;
			$table_name  = 'anggota_organisasi';
      $where       = array(
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_ekonomi_kreatif' => trim($this->input->post('id_ekonomi_kreatif'))
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
  }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'ekonomi_kreatif.id_ekonomi_kreatif' => $this->input->post('id_ekonomi_kreatif'),
        'ekonomi_kreatif.domain' => $web
				);
      $this->db->select("
      ekonomi_kreatif.id_ekonomi_kreatif,
      ekonomi_kreatif.nama,
      ekonomi_kreatif.kategori,
      ekonomi_kreatif.jenis_kelamin,
      ekonomi_kreatif.alamat,
      ekonomi_kreatif.nomor_telepon,
      ekonomi_kreatif.email,
      ekonomi_kreatif.keterangan
      ");
      $this->db->where($where);
      $this->db->order_by('id_ekonomi_kreatif');
      $result = $this->db->get('ekonomi_kreatif');
      echo json_encode($result->result_array());
		}
   
   public function total_ekonomi_kreatif()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('ekonomi_kreatif');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function inaktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_ekonomi_kreatif = $this->input->post('id_ekonomi_kreatif');
      $where = array(
        'id_ekonomi_kreatif' => $id_ekonomi_kreatif
        );
      $this->db->from('ekonomi_kreatif');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'ekonomi_kreatif';
        if( $cek <> '' ){
          $this->Ekonomi_kreatif_model->update_data_ekonomi_kreatif($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_ekonomi_kreatif = $this->input->post('id_ekonomi_kreatif');
      $where = array(
        'id_ekonomi_kreatif' => $id_ekonomi_kreatif
        );
      $this->db->from('ekonomi_kreatif');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'ekonomi_kreatif';
        if( $cek <> '' ){
          $this->Ekonomi_kreatif_model->update_data_ekonomi_kreatif($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
    
  public function pdf()
  {
  $data['IconAbout']='ok';
  
  $this->load->view('ekonomi_kreatif/pdf');
  $html = $this->output->get_output();
  $this->load->library('dompdf_gen');
  $this->dompdf->load_html($html);
  $width =8.5 * 72;
  $height = 13.88 *72;
  $this->dompdf->set_paper(array(0,0,$width,$height));
  $this->dompdf->render();
  $this->dompdf->stream("cetak_ekonomi_kreatif_" . date('d_m_y') . ".pdf");
  
  }
	public function cetak_xls(){
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
			->setCreator("Budi Utomo")
			->setLastModifiedBy("Budi Utomo")
			->setTitle("Laporan I")
			->setSubject("Office 2007 XLSX Document")
			->setDescription("Laporan Permohonan Pendaftaran Badan Usaha")
			->setKeywords("Laporan Halaman")
			->setCategory("Bentuk XLS");
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Download')
			->mergeCells('A1:G1')
			->setCellValue('A2', 'Permohonan Pendaftaran Badan Usaha')
			->mergeCells('A2:G2')
			->setCellValue('A3', 'Tanggal Download : '.date('d/m/Y').' ')
			->mergeCells('A3:G3')
			
			->setCellValue('A6', 'No')
			->setCellValue('B6', 'Nomor Permohonan Pendaftaran Badan Usaha')
			->setCellValue('C6', 'Tanggal Permohonan Pendaftaran Badan Usaha')
      ;
		
		$web=$this->uut->namadomain(base_url());
		
    $table    = 'ekonomi_kreatif';
    $page    = $this->input->get('page');
    $limit    = $this->input->get('limit');
    $keyword    = $this->input->get('keyword');
    $order_by    = $this->input->get('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where      = array(
      'ekonomi_kreatif.status !=' => 99,
      'ekonomi_kreatif.status !=' => 999
    );
    $orderby   = ''.$order_by.'';
    $query = $this->Ekonomi_kreatif_model->html_all_ekonomi_kreatif($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $i = 7;
    $urut=$start;
    foreach ($query->result() as $row)
      {
        $urut=$urut+1;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i, ''.$urut.'')
        ->setCellValue('B'.$i, ''.$row->nomor_ekonomi_kreatif.'')
        ->setCellValueExplicit('C'.$i, ''.$this->Crud_model->dateBahasaIndo($row->tanggal_ekonomi_kreatif).'', PHPExcel_Cell_DataType::TYPE_STRING)
        ;
        $objPHPExcel->getActiveSheet()
        ->getStyle('A'.$i.':C'.$i.'')
        ->getBorders()->getAllBorders()
        ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
      }
		
		$objPHPExcel->getActiveSheet()->setTitle('Probable Clients');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getFill()->getStartColor()->setARGB('cccccc');
								
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="ekonomi_kreatif_'.date('ymdhis').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output'); 
          
   }
	 
  public function cetak(){
    $id_ekonomi_kreatif    = $this->input->get('id_ekonomi_kreatif');
    //$id_ekonomi_kreatif    = $this->uri->segment(3);
    $where = array(
		'id_ekonomi_kreatif' => $id_ekonomi_kreatif,
		'status !=' => 99
		);
    $d = $this->Ekonomi_kreatif_model->get_data($where);
			if(!$d){
				$data['id_ekonomi_kreatif'] = '';
				$data['judul_ekonomi_kreatif'] = '';
				$data['nomor_ekonomi_kreatif'] = '';
				$data['perihal_ekonomi_kreatif'] = '';
				$data['yth_ekonomi_kreatif'] = '';
				$data['cq_ekonomi_kreatif'] = '';
				$data['di_ekonomi_kreatif'] = '';
				$data['tanggal_ekonomi_kreatif'] = '';
				$data['sifat_ekonomi_kreatif'] = '';
				$data['lampiran_ekonomi_kreatif'] = '';
				$data['nama_organisasi'] = '';
				$data['kesenian'] = '';
				$data['jenis_kesenian'] = '';
				$data['fungsi_kesenian'] = '';
				$data['nama_kesenian'] = '';
				$data['pengalaman_pentas'] = '';
				$data['penghargaan_yang_pernah_diterima'] = '';
				$data['fasilitas_peralatan_yang_dimiliki'] = '';
				$data['hambatan_kendala'] = '';
				$data['riwayat_sejarah'] = '';
				$data['alamat_organisasi'] = '';
				$data['nama_desa'] = '';
				$data['nama_kecamatan'] = '';
				$data['nama_kabupaten'] = '';
				$data['nama_propinsi'] = '';
				$data['nama_pemohon'] = '';
				$data['tempat_lahir_pemohon'] = '';
				$data['tanggal_lahir_pemohon'] = '';
				$data['pekerjaan_pemohon'] = '';
				$data['nomor_telp_pemohon'] = '';
				$data['alamat_pemohon'] = '';
				$data['nama_desa_pemohon'] = '';
				$data['nama_kecamatan_pemohon'] = '';
				$data['nama_kabupaten_pemohon'] = '';
				$data['nama_propinsi_pemohon'] = '';
				$data['kepertluan'] = '';
				$data['tanggal_pendirian'] = '';
				$data['jumlah_anggota_pria'] = '';
				$data['jumlah_anggota_wanita'] = '';
				$data['jumlah_anggota'] = '';
				$data['created_time'] = '';
				$data['pengedit'] = '';
				$data['updated_time'] = '';
			}
			else{
				$data['id_ekonomi_kreatif'] = $d['id_ekonomi_kreatif'];
				$data['judul_ekonomi_kreatif'] = $d['judul_ekonomi_kreatif'];
				$data['nomor_ekonomi_kreatif'] = $d['nomor_ekonomi_kreatif'];
				$data['perihal_ekonomi_kreatif'] = ''.$d['perihal_ekonomi_kreatif'].' ';
				$data['yth_ekonomi_kreatif'] = ''.$d['yth_ekonomi_kreatif'].' ';
				$data['cq_ekonomi_kreatif'] = ''.$d['cq_ekonomi_kreatif'].' ';
				$data['di_ekonomi_kreatif'] = ''.$d['di_ekonomi_kreatif'].' ';
				$data['tanggal_ekonomi_kreatif'] = ''.$this->Crud_model->dateBahasaIndo($d['tanggal_ekonomi_kreatif']).'';
				$data['tanggal_lahir_pemohon'] = ''.$this->Crud_model->dateBahasaIndo($d['tanggal_lahir_pemohon']).'';
				$data['sifat_ekonomi_kreatif'] = $d['sifat_ekonomi_kreatif'];
				$data['lampiran_ekonomi_kreatif'] = $d['lampiran_ekonomi_kreatif'];
				$data['nama_organisasi'] = $d['nama_organisasi'];
				$data['kesenian'] = $d['kesenian'];
				$data['jenis_kesenian'] = $d['jenis_kesenian'];
				$data['fungsi_kesenian'] = $d['fungsi_kesenian'];
				$data['nama_kesenian'] = $d['nama_kesenian'];
				$data['pengalaman_pentas'] = $d['pengalaman_pentas'];
				$data['penghargaan_yang_pernah_diterima'] = $d['penghargaan_yang_pernah_diterima'];
				$data['fasilitas_peralatan_yang_dimiliki'] = $d['fasilitas_peralatan_yang_dimiliki'];
				$data['hambatan_kendala'] = $d['hambatan_kendala'];
				$data['riwayat_sejarah'] = $d['riwayat_sejarah'];
				$data['alamat_organisasi'] = $d['alamat_organisasi'];
				$data['nama_desa'] = $d['nama_desa'];
				$data['nama_kecamatan'] = $d['nama_kecamatan'];
				$data['nama_kabupaten'] = $d['nama_kabupaten'];
				$data['nama_propinsi'] = $d['nama_propinsi'];
				$data['nama_pemohon'] = $d['nama_pemohon'];
				$data['tempat_lahir_pemohon'] = $d['tempat_lahir_pemohon'];
				$data['pekerjaan_pemohon'] = $d['pekerjaan_pemohon'];
				$data['nomor_telp_pemohon'] = $d['nomor_telp_pemohon'];
				$data['alamat_pemohon'] = $d['alamat_pemohon'];
				$data['nama_desa_pemohon'] = $d['nama_desa_pemohon'];
				$data['nama_kecamatan_pemohon'] = $d['nama_kecamatan_pemohon'];
				$data['nama_kabupaten_pemohon'] = $d['nama_kabupaten_pemohon'];
				$data['nama_propinsi_pemohon'] = $d['nama_propinsi_pemohon'];
				$data['kepertluan'] = $d['kepertluan'];
				$data['tanggal_pendirian'] = ''.$this->Crud_model->dateBahasaIndo($d['tanggal_pendirian']).'';
				$data['jumlah_anggota_pria'] = $d['jumlah_anggota_pria'];
				$data['jumlah_anggota_wanita'] = $d['jumlah_anggota_wanita'];
				$data['jumlah_anggota'] = $d['jumlah_anggota'];
				$data['created_time'] = $this->Crud_model->dateBahasaIndo1($d['created_time']);
				$data['pengedit'] = $d['pengedit'];
				$data['updated_time'] = $d['updated_time'];
			}
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan_'] = $row0->keterangan;
      }
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'ekonomi_kreatif/cetak';
      $this->load->view('print', $data);
  }
	
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_ekonomi_kreatif = $this->input->post('id_ekonomi_kreatif');
      $where = array(
        'id_ekonomi_kreatif' => $id_ekonomi_kreatif,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('ekonomi_kreatif');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'ekonomi_kreatif';
        $this->Ekonomi_kreatif_model->update_data_ekonomi_kreatif($data_update, $where, $table_name);
        echo 1;
        }
		}
 }