<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

	function __construct()
		{
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Posting_model');
    $this->load->model('Modul_model');
		}    
	
    public function index()
     {
      $url_modul = $this->uri->segment(4);
      $wheres = array(
              'alamat_url' => $url_modul,
              'status' => 1
              );
      $c = $this->Modul_model->get_data($wheres);
      if(!$c)
            {
              $data['nama_modul'] = 'kosong';
            }
          else
            {
              $data['nama_modul'] = $c['nama_modul'];
            }
      $web=$this->uut->namadomain(base_url());
      $a = 0;
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['menu_mobile'] = $this->menu_mobile(0,$h="", $a);
      $where0 = array(
        'domain' => $web
        );
      $this->db->where($where0);
      $this->db->limit(1);
      $query0 = $this->db->get('dasar_website');
      foreach ($query0->result() as $row0)
        {
          $data['domain'] = $row0->domain;
          $data['alamat'] = $row0->alamat;
          $data['telpon'] = $row0->telpon;
          $data['email'] = $row0->email;
          $data['twitter'] = $row0->twitter;
          $data['facebook'] = $row0->facebook;
          $data['google'] = $row0->google;
          $data['instagram'] = $row0->instagram;
          $data['peta'] = $row0->peta;
          $data['keterangan'] = $row0->keterangan;
        }
  
      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->judul_komponen == 'Header' ){ //Header
            $data['Header'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
            $data['KolomKiriAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
            $data['KolomKananAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
            $data['KolomKiriBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
            $data['KolomPalingBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
            $data['KolomKananBawah'] = $row1->isi_komponen;
          }
          else{ 
          }
        }
        
      $data['main_view'] = 'postings/home';
      $this->load->view('posts', $data);
     }

private function menu_atas($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
				$hasil .= '<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'">HOME</a></li>'; 
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			  $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			  $yummy   = array("_","","","","","","","","","","","","","");
			  $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			if( $a > 1 ){
					if ($xx == 0){
						if($h->judul_posting == 'Pengaduan Masyarakat'){
							$hasil .= '
						  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
							';
							}
          elseif($h->judul_posting == 'Pengumuman'){
            $hasil .= '
            <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
            }
          elseif($h->judul_posting == 'Permohonan Informasi Publik'){
            $hasil .= '
            <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
            }
          elseif($h->judul_posting == 'Permohonan Informasi'){
            $hasil .= '
            <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
            }
						else{
              if($nomor < 21){
                $hasil .= '
                <li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
              }
						}
					}
					else{
            if($xx > 3){
              $hasil .= '
              <li><a href="' . base_url() . 'post/galeri/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span></a>';
            }else{
              $hasil .= '
                <li class="parent" > <span class="">' . $h->judul_posting . ' </span>';
            }
					} 
			}
			else{
        if($h->judul_posting == 'Berita'){
            $hasil .= '
              <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
        }
      elseif($h->judul_posting == 'Pengumuman'){
      $hasil .= '
      <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
      elseif($h->judul_posting == 'AGENDA'){
      $hasil .= '
      <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
      ';
      }
		elseif($h->judul_posting == 'Pengaduan Masyarakat'){
			$hasil .= '
		  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
		elseif($h->judul_posting == 'Permohonan Informasi Publik'){
			$hasil .= '
		  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
		elseif($h->judul_posting == 'Permohonan Informasi'){
			$hasil .= '
		  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
        else{
          if ($xx == 0){
						if($h->judul_posting == 'Pengaduan Masyarakat'){
							$hasil .= '
						  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
							';
							}
						elseif($h->judul_posting == 'Permohonan Informasi Publik'){
							$hasil .= '
						  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
							';
							}
						elseif($h->judul_posting == 'Permohonan Informasi'){
							$hasil .= '
						  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
							';
							}
            elseif($h->judul_posting == 'Covid-19'){
              $hasil .= '
              <li><a href="'.base_url().'post/detail_covid/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>
              ';
              }
						else{
              if($nomor < 21){
                $hasil .= '
                  <li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
              
              }
            }
          }
          else{
            $hasil .= '
              <li class="parent" ><a><span class="">'.$h->judul_posting.'</a></span>';
          } 
        }
      }
      
			$hasil = $this->menu_atas($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
}
  private function menu_mobile($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
					$hasil .= '<ul> <li><a href="'.base_url().'" role="button" aria-expanded="false">HOME</a></li>';
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
			$r = $this->db->query("
			SELECT * from posting
										
			where parent='".$h->id_posting."' 
			and status = 1 
			and tampil_menu_atas = 1 
			and domain='".$web."'
			order by urut
			");
			$xx = $r->num_rows();
			
				$nomor = $nomor + 1;
					$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
					$yummy   = array("_","","","","","","","","","","","","","");
					$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			if( $a > 1 ){
				$hasil .= '
					<li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class="">'.$h->judul_posting.' </span></a>';
					}
			else{
				if($h->judul_posting == 'Berita'){
					$hasil .= '
						<li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
				}
				elseif($h->judul_posting == 'Pengaduan Masyarakat'){
					$hasil .= '
					<li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				elseif($h->judul_posting == 'Permohonan Informasi'){
					$hasil .= '
					<li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				elseif($h->judul_posting == 'Permohonan Informasi Publik'){
					$hasil .= '
					<li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				else{
					if ($xx == 0){
						$hasil .= '
						<li><a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span></a>';
					}
					else{
						$hasil .= '
						<li class="parent" > <span class="">'.$h->judul_posting.' </span>';
					} 
				}
			}
      
			$hasil = $this->menu_mobile($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
	
  public function get_attachment_by_id_posting($id_posting)
   {
     $where = array(
						'id_tabel' => $id_posting
						);
    $d = $this->Posting_model->get_attachment_by_id_posting($where);
    if(!$d)
          {
						return 'blankgambar.jpg';
          }
        else
          {
						return $d['file_name'];
          }
   } 
	
  private function option_posting_disparbud($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 10
		");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			if( $nomor == 1 ){
				$hasil .= 
				'
          <article class="news-block">
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="overlay-link">
              <figure class="image-overlay">
                <img src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'" width="870" height="500" alt=""/>
              </figure>
            </a>
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="category">'.$h->judul_posting.'</a>
            <div class="news-details">
              <h2 class="news-title">
                <a href="#">
                </a>
              </h2>
              <p>
              <p style="text-align:justify"><span style="font-size:14px">'.substr(strip_tags($h->isi_posting), 0, 200).'</span></p>
              <p class="simple-share">
                by <a href="#"><b>Adm, </b></a> -  
                <span class="article-date"><i class="fa fa-clock-o"></i> '.$h->created_time.' wib</span>
              </p>
            </div>
          </article>
				';
			}
			else{
				$hasil .= 
				'
						<article class="simple-post clearfix">
							<div class="simple-thumb">
								<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
								<img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt=""/>
								</a>
							</div>
							<header>
								<h2>
									<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a>
								</h2>
								<p class="simple-share">
									<a href="#"></a> 
									by <a href="#">Admin</a> - 
									<span><i class="fa fa-clock-o"></i> '.$h->created_time.' wib</span>
								</p>
							</header>
						</article>
						
				';
			}
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
	
  private function menu_atas_disparbud($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
				$hasil .= '<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'">HOME</a></li>'; 
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			  $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			  $yummy   = array("_","","","","","","","","","","","","","");
			  $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			if( $a > 1 ){
					if ($xx == 0){
						$hasil .= '
							<li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
					}
					else{
						$hasil .= '
							<li class="parent" > <span class="">'.$h->judul_posting.' </span>';
					} 
			}
			else{
        if($h->judul_posting == 'Berita'){
            $hasil .= '
              <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
        }
        elseif($h->judul_posting == 'Pengaduan Masyarakat'){
          $hasil .= '
          <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
          ';
          }
        elseif($h->judul_posting == 'Pengumuman'){
          $hasil .= '
          <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
          ';
          }
        else{
          if ($xx == 0){
            $hasil .= '
              <li><a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
          }
          else{
            $hasil .= '
              <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
          } 
        }
      }
      
			$hasil = $this->menu_atas_disparbud($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
	
  public function detail(){
    $web=$this->uut->namadomain(base_url());
    $url_modul = $this->uri->segment(4);
    $wheres = array(
						'alamat_url' => $url_modul,
            'status' => 1
						);
    $c = $this->Modul_model->get_data($wheres);
    if(!$c)
          {
						$data['nama_modul'] = 'kosong';
          }
        else
          {
						$data['nama_modul'] = $c['nama_modul'];
          }
      $where0 = array(
        'domain' => $web
        );
      $this->db->where($where0);
      $this->db->limit(1);
      $query0 = $this->db->get('dasar_website');
      foreach ($query0->result() as $row0)
        {
          $data['domain'] = $row0->domain;
          $data['alamat'] = $row0->alamat;
          $data['telpon'] = $row0->telpon;
          $data['email'] = $row0->email;
          $data['twitter'] = $row0->twitter;
          $data['facebook'] = $row0->facebook;
          $data['google'] = $row0->google;
          $data['instagram'] = $row0->instagram;
          $data['peta'] = $row0->peta;
          $data['keterangan'] = $row0->keterangan;
        }
        
    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['tampil_menu_atas'] = '';
						$data['kata_kunci'] = '';
						$data['gambar'] = 'logo wonosobo.png';
						$data['created_time'] = '';
          }
        else
          {
						$data['tampil_menu_atas'] = $d['tampil_menu_atas'];
						$data['judul_posting'] = $d['judul_posting'];
						$data['isi_posting'] = ''.$d['isi_posting'].' ';
						$data['tampil_menu_atas'] = ''.$d['tampil_menu_atas'].' ';
						$data['kata_kunci'] = $d['kata_kunci'];
						$data['created_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['created_time']).'';
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }
      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->judul_komponen == 'Header' ){ //Header
            $data['Header'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
            $data['KolomKiriAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
            $data['KolomKananAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
            $data['KolomKiriBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
            $data['KolomPalingBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
            $data['KolomKananBawah'] = $row1->isi_komponen;
          }
          else{ 
          }
        }
				
      $a = 0;
      $data['total'] = 10;
        
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['menu_mobile'] = $this->menu_mobile(0,$h="", $a);
			$data['galery_berita'] = $this->option_posting_disparbud($h="", $a);
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'post/detail';
      $this->load->view('posts', $data);
   }
	
   public function galeri()
    {
     $web=$this->uut->namadomain(base_url());
     $a = 0;
     $r = $this->uri->segment(5);
     $p=$this->uri->segment(3);
     $data['isi_halaman'] = $this->posting_galeri($p,$q="", $r);
     $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
     $data['menu_mobile'] = $this->menu_mobile(0,$h="", $a);
     $where0 = array(
       'domain' => $web
       );
     $this->db->where($where0);
     $this->db->limit(1);
     $query0 = $this->db->get('dasar_website');
     foreach ($query0->result() as $row0)
       {
         $data['domain'] = $row0->domain;
         $data['alamat'] = $row0->alamat;
         $data['telpon'] = $row0->telpon;
         $data['email'] = $row0->email;
         $data['twitter'] = $row0->twitter;
         $data['facebook'] = $row0->facebook;
         $data['google'] = $row0->google;
         $data['instagram'] = $row0->instagram;
         $data['peta'] = $row0->peta;
         $data['keterangan'] = $row0->keterangan;
       }
 
 
     //PAGINATION
     $page =  $r;
     $noPage =  $page;
     $showPage = 0;
     $s = $this->db->query("SELECT COUNT(*) as jumlah_data from posting where parent='".$this->uri->segment(3)."' ");
     $jumPage = $s->row("jumlah_data")/10;
 
     $url = 'https://'.$web.'/post/galeri/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'';
 
     $pagination_top = '
     <div class="text-right">
       <ul class="pagination">
     ';
     $pagination_bottom = '
     <div class="text-center">
       <ul class="pagination">
     ';
 
     //menampilkan link halaman awal
     if ($page != 1 || empty($page)) { $pagination_bottom .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>'; }
  
     // menampilkan link previous
  
     if ($page > 1) { $pagination_bottom .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>'; }
  
     // memunculkan nomor halaman dan linknya
     for($page = 1; $page <= $jumPage; $page++)
     {
          if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) 
          {   
             if (($showPage == 1) && ($page != 2))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top.= '<li><a href="#">...</a></li>'; }
             if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top .= '<li><a href="#">...</a></li>'; }
             if ($page == $r) { $pagination_bottom .= '<li class="active"><a href="#">'.$page.'</a></li>';$pagination_top.= '<li class="active"><a href="#">'.$page.'</a></li>'; }
             else { $pagination_bottom .= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>';$pagination_top.= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>'; }
             $showPage = $page;          
          }
     }
  
     // menampilkan link next
  
     if ($noPage < $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>'; }
  
     //menampilkan link halaman akhir
     if ($noPage != $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>'; }
  
     $pagination_top .= '
       </ul>
     </div>
     ';
     $pagination_bottom .= '
       </ul>
     </div>
     ';
     $where1 = array(
       'domain' => $web
       );
     $this->db->where($where1);
     $query1 = $this->db->get('komponen');
     foreach ($query1->result() as $row1)
       {
         if( $row1->judul_komponen == 'Header' ){ //Header
           $data['Header'] = $row1->isi_komponen;
         }
         else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
           $data['KolomKiriAtas'] = $row1->isi_komponen;
         }
         else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
           $data['KolomKananAtas'] = $row1->isi_komponen;
         }
         else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
           $data['KolomKiriBawah'] = $row1->isi_komponen;
         }
         else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
           $data['KolomPalingBawah'] = $row1->isi_komponen;
         }
         else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
           $data['KolomKananBawah'] = $row1->isi_komponen;
         }
         else{ 
         }
       }
     $data['pagination_top'] = $pagination_top;
     $data['pagination_bottom'] = $pagination_bottom;
     $data['main_view'] = 'post/categories';
     $this->load->view('posts', $data);
    }
	
    public function detail_covid(){
      $web=$this->uut->namadomain(base_url());
      $url_modul = $this->uri->segment(4);
      $wheres = array(
              'alamat_url' => $url_modul,
              'status' => 1
              );
      $c = $this->Modul_model->get_data($wheres);
      if(!$c)
            {
              $data['nama_modul'] = 'kosong';
            }
          else
            {
              $data['nama_modul'] = $c['nama_modul'];
            }
        $where0 = array(
          'domain' => $web
          );
        $this->db->where($where0);
        $this->db->limit(1);
        $query0 = $this->db->get('dasar_website');
        foreach ($query0->result() as $row0)
          {
            $data['domain'] = $row0->domain;
            $data['alamat'] = $row0->alamat;
            $data['telpon'] = $row0->telpon;
            $data['email'] = $row0->email;
            $data['twitter'] = $row0->twitter;
            $data['facebook'] = $row0->facebook;
            $data['google'] = $row0->google;
            $data['instagram'] = $row0->instagram;
            $data['peta'] = $row0->peta;
            $data['keterangan'] = $row0->keterangan;
          }
          
      $where = array(
              'id_posting' => $this->uri->segment(3),
              'status' => 1
              );
      $d = $this->Posting_model->get_data($where);
      if(!$d)
            {
              $data['judul_posting'] = '';
              $data['isi_posting'] = '';
              $data['tampil_menu_atas'] = '';
              $data['kata_kunci'] = '';
              $data['gambar'] = 'logo wonosobo.png';
              $data['created_time'] = '';
            }
          else
            {
              $data['tampil_menu_atas'] = $d['tampil_menu_atas'];
              $data['judul_posting'] = $d['judul_posting'];
              $data['isi_posting'] = ''.$d['isi_posting'].' ';
              $data['tampil_menu_atas'] = ''.$d['tampil_menu_atas'].' ';
              $data['kata_kunci'] = $d['kata_kunci'];
              $data['created_time'] = ''.$this->Crud_model->dateBahasaIndo1($d['created_time']).'';
              $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
            }
        $where1 = array(
          'domain' => $web
          );
        $this->db->where($where1);
        $query1 = $this->db->get('komponen');
        foreach ($query1->result() as $row1)
          {
            if( $row1->judul_komponen == 'Header' ){ //Header
              $data['Header'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
              $data['KolomKiriAtas'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
              $data['KolomKananAtas'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
              $data['KolomKiriBawah'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
              $data['KolomPalingBawah'] = $row1->isi_komponen;
            }
            else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
              $data['KolomKananBawah'] = $row1->isi_komponen;
            }
            else{ 
            }
          }
          
        $a = 0;
        $data['total'] = 10;
          
        $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
        $data['menu_mobile'] = $this->menu_mobile(0,$h="", $a);
        $data['galery_berita'] = $this->option_posting_disparbud($h="", $a);
        $data['judul'] = 'Selamat datang';
        $data['main_view'] = 'post/detail_custom_covid';
        $this->load->view('posts_custom_covid', $data);
     }
	
    public function galeri_covid()
     {
      $web=$this->uut->namadomain(base_url());
      $a = 0;
      $r = $this->uri->segment(5);
      $p=$this->uri->segment(3);
      $data['isi_halaman'] = $this->posting_galeri($p,$q="", $r);
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['menu_mobile'] = $this->menu_mobile(0,$h="", $a);
      $where0 = array(
        'domain' => $web
        );
      $this->db->where($where0);
      $this->db->limit(1);
      $query0 = $this->db->get('dasar_website');
      foreach ($query0->result() as $row0)
        {
          $data['domain'] = $row0->domain;
          $data['alamat'] = $row0->alamat;
          $data['telpon'] = $row0->telpon;
          $data['email'] = $row0->email;
          $data['twitter'] = $row0->twitter;
          $data['facebook'] = $row0->facebook;
          $data['google'] = $row0->google;
          $data['instagram'] = $row0->instagram;
          $data['peta'] = $row0->peta;
          $data['keterangan'] = $row0->keterangan;
        }
  
  
      //PAGINATION
      $page =  $r;
      $noPage =  $page;
      $showPage = 0;
      $s = $this->db->query("SELECT COUNT(*) as jumlah_data from posting where parent='".$this->uri->segment(3)."' ");
      $jumPage = $s->row("jumlah_data")/10;
  
      $url = 'https://'.$web.'/post/galeri/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'';
  
      $pagination_top = '
      <div class="text-right">
        <ul class="pagination">
      ';
      $pagination_bottom = '
      <div class="text-center">
        <ul class="pagination">
      ';
  
      //menampilkan link halaman awal
      if ($page != 1 || empty($page)) { $pagination_bottom .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>'; }
   
      // menampilkan link previous
   
      if ($page > 1) { $pagination_bottom .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>'; }
   
      // memunculkan nomor halaman dan linknya
      for($page = 1; $page <= $jumPage; $page++)
      {
           if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) 
           {   
              if (($showPage == 1) && ($page != 2))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top.= '<li><a href="#">...</a></li>'; }
              if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top .= '<li><a href="#">...</a></li>'; }
              if ($page == $r) { $pagination_bottom .= '<li class="active"><a href="#">'.$page.'</a></li>';$pagination_top.= '<li class="active"><a href="#">'.$page.'</a></li>'; }
              else { $pagination_bottom .= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>';$pagination_top.= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>'; }
              $showPage = $page;          
           }
      }
   
      // menampilkan link next
   
      if ($noPage < $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>'; }
   
      //menampilkan link halaman akhir
      if ($noPage != $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>'; }
   
      $pagination_top .= '
        </ul>
      </div>
      ';
      $pagination_bottom .= '
        </ul>
      </div>
      ';
      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->judul_komponen == 'Header' ){ //Header
            $data['Header'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
            $data['KolomKiriAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
            $data['KolomKananAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
            $data['KolomKiriBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
            $data['KolomPalingBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
            $data['KolomKananBawah'] = $row1->isi_komponen;
          }
          else{ 
          }
        }
      $data['pagination_top'] = $pagination_top;
      $data['pagination_bottom'] = $pagination_bottom;
      $data['main_view'] = 'post/custom_covid';
      $this->load->view('posts_custom_covid', $data);
     }
	 
  private function posting_galeri($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
    if(empty($r) || $r == 1 || $r < 1)
    {
       $offset = 0;
    }elseif($r > 1){
      $offset = ($r*10);
    }

		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
    and posisi='menu_atas'
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",10
		");
		$x = $w->num_rows();
		$nomor = 0;

		foreach($w->result() as $h)
		{ 
    
    $rs = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
    and posisi='menu_atas'
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",12
		");
		$xx = $rs->num_rows();

    $hasils = $this->posting_galeri($h->id_posting,$hasils, $r);
			if ($xx == 0){
						$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
						$yummy   = array("_","","","","","","","","","","","","","");
						$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
						$created_time = $this->Crud_model->dateBahasaIndo1($h->created_time);
            $hasils .= 
            '
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<!-- BEGIN CAROUSEL -->            
								<div class="front-carousel">
									<div class="carousel slide" id="myCarousel">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<div class="item active">
												<img alt="" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'">
											</div>
										</div>
										<!-- Carousel nav -->
										<a data-slide="prev" href="#myCarousel" class="carousel-control left">
											<i class="fa fa-angle-left"></i>
										</a>
										<a data-slide="next" href="#myCarousel" class="carousel-control right">
											<i class="fa fa-angle-right"></i>
										</a>
									</div>                
								</div>
								<!-- END CAROUSEL -->             
							</div>
							<div class="col-md-8 col-sm-8">
								<h2><a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></h2>
								<p>'.substr(strip_tags($h->isi_posting), 0, 200).'</p>
								<i class="fa fa-calendar"></i> '.$created_time.'
								<a class="btn btn-info" href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="more"><i class="fa fa-eye"></i> Baca Selengkapnya <i class="icon-angle-right"></i></a>
								
							</div>
						</div>
						<hr class="blog-post-sep">
            ';
            }
		}

		return $hasils;
	}
	
	
}
