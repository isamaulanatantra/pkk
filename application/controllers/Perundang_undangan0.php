<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Perundang_undangan extends CI_Controller
{
  
  function __construct()
  {
    parent::__construct();
  }
  
  public function index()
  {
    $data['test'] = '';
    $this->load->view('back_end/perundang_undangan/content', $data);
  }
  
  public function json()
  {
    $table       = $this->input->post('table');
    $page        = $this->input->post('page');
    $limit       = $this->input->post('limit');
    $keyword     = $this->input->post('keyword');
    $status      = $this->input->post('status');
    $order_by    = $this->input->post('order_by');
    $order_field = $this->input->post('order_field');
    $shorting    = $this->input->post('shorting');
    $start       = ($page - 1) * $limit;
    $fields      = "
            perundang_undangan.id_perundang_undangan,
            perundang_undangan.jenis_perundang_undangan_id,
            perundang_undangan.nomor_peraturan,
            perundang_undangan.tahun_pengundangan,
            perundang_undangan.status_peraturan,
            perundang_undangan.nomor_panggil,
            perundang_undangan.judul,
            perundang_undangan.nama_pengarang,
            perundang_undangan.tipe_pengarang,
            perundang_undangan.jenis_pengarang,
            perundang_undangan.jenis_peraturan,
            perundang_undangan.singkatan_jenis_peraturan,
            perundang_undangan.cetakan_edisi,
            perundang_undangan.tempat_terbit,
            perundang_undangan.penerbit,
            perundang_undangan.tanggal_pengundangan,
            perundang_undangan.kolasi_deskripsi_fisik,
            perundang_undangan.sumber,
            perundang_undangan.subjek,
            perundang_undangan.isbn,
            perundang_undangan.bahasa,
            perundang_undangan.bidang_hukum,
            perundang_undangan.nomor_induk_buku,
            perundang_undangan.nama_file_download,
            perundang_undangan.url_file_lampiran,
            perundang_undangan.url_halaman_peraturan,
            perundang_undangan.keterangan,
            perundang_undangan.inserted_by,
            perundang_undangan.inserted_time,
            perundang_undangan.updated_by,
            perundang_undangan.updated_time,
            perundang_undangan.deleted_by,
            perundang_undangan.deleted_time,
            perundang_undangan.status,
            
            jenis_perundang_undangan.jenis_perundang_undangan_code,
            jenis_perundang_undangan.jenis_perundang_undangan_name,
            
            (
            select attachment.file_name from attachment
            where attachment.id_tabel=perundang_undangan.id_perundang_undangan
            and attachment.table_name='" . $table . "'
            order by attachment.id_attachment DESC
            limit 1
            ) as file
            ";
    $where       = array(
      '' . $table . '.status' => $status
    );
    $this->db->select("$fields");
    if ($keyword == '') {
      $this->db->where("
            " . $table . ".status = " . $status . "
            ");
    } else {
      $this->db->where("
            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'
            AND
            " . $table . ".status = " . $status . "
            ");
    }
    $this->db->join('jenis_perundang_undangan', 'jenis_perundang_undangan.jenis_perundang_undangan_id=perundang_undangan.jenis_perundang_undangan_id');
    $this->db->order_by($order_by, $shorting);
    $this->db->limit($limit, $start);
    $result = $this->db->get($table);
    echo json_encode($result->result_array());
  }
  
  public function total()
  {
    $table       = $this->input->post('table');
    $page        = $this->input->post('page');
    $limit       = $this->input->post('limit');
    $keyword     = $this->input->post('keyword');
    $status      = $this->input->post('status');
    $order_by    = $this->input->post('order_by');
    $shorting    = $this->input->post('shorting');
    $order_field = $this->input->post('order_field');
    $start       = ($page - 1) * $limit;
    $fields      = "
    perundang_undangan.id_perundang_undangan,
    perundang_undangan.jenis_perundang_undangan_id,
    perundang_undangan.nomor_peraturan,
    perundang_undangan.tahun_pengundangan,
    perundang_undangan.status_peraturan,
    perundang_undangan.nomor_panggil,
    perundang_undangan.judul,
    perundang_undangan.nama_pengarang,
    perundang_undangan.tipe_pengarang,
    perundang_undangan.jenis_pengarang,
    perundang_undangan.jenis_peraturan,
    perundang_undangan.singkatan_jenis_peraturan,
    perundang_undangan.cetakan_edisi,
    perundang_undangan.tempat_terbit,
    perundang_undangan.penerbit,
    perundang_undangan.tanggal_pengundangan,
    perundang_undangan.kolasi_deskripsi_fisik,
    perundang_undangan.sumber,
    perundang_undangan.subjek,
    perundang_undangan.isbn,
    perundang_undangan.bahasa,
    perundang_undangan.bidang_hukum,
    perundang_undangan.nomor_induk_buku,
    perundang_undangan.nama_file_download,
    perundang_undangan.url_file_lampiran,
    perundang_undangan.url_halaman_peraturan,
    perundang_undangan.keterangan,
    perundang_undangan.inserted_by,
    perundang_undangan.inserted_time,
    perundang_undangan.updated_by,
    perundang_undangan.updated_time,
    perundang_undangan.deleted_by,
    perundang_undangan.deleted_time,
    perundang_undangan.status,
            ";
    $where       = array(
      '' . $table . '.status' => $status
    );
    $this->db->select("$fields");
    if ($keyword == '') {
      $this->db->where("
            " . $table . ".status = " . $status . "
            ");
    } else {
      $this->db->where("
            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'
            AND
            " . $table . ".status = " . $status . "
            ");
    }
    $this->db->join('jenis_perundang_undangan', 'jenis_perundang_undangan.jenis_perundang_undangan_id=perundang_undangan.jenis_perundang_undangan_id');
    $query = $this->db->get($table);
    $a     = $query->num_rows();
    echo trim(ceil($a / $limit));
  }
  
  public function simpan()
  {
    $this->form_validation->set_rules('jenis_perundang_undangan_id', 'jenis_perundang_undangan_id', 'required');
    $this->form_validation->set_rules('temp', 'temp', 'required');
    $this->form_validation->set_rules('nomor_peraturan', 'nomor_peraturan', 'required');
    $this->form_validation->set_rules('tahun_pengundangan', 'tahun_pengundangan', 'required');
    $this->form_validation->set_rules('status_peraturan', 'status_peraturan', 'required');
    $this->form_validation->set_rules('judul', 'judul', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_input = array(
        'jenis_perundang_undangan_id' => trim($this->input->post('jenis_perundang_undangan_id')),
        'nomor_peraturan' => trim($this->input->post('nomor_peraturan')),
        'status_peraturan' => trim($this->input->post('status_peraturan')),
        'nomor_panggil' => trim($this->input->post('nomor_panggil')),
        'tahun_pengundangan' => trim($this->input->post('tahun_pengundangan')),
        'judul' => trim($this->input->post('judul')),
        'nama_pengarang' => trim($this->input->post('nama_pengarang')),
        'tipe_pengarang' => trim($this->input->post('tipe_pengarang')),
        'jenis_pengarang' => trim($this->input->post('jenis_pengarang')),
        'jenis_peraturan' => trim($this->input->post('jenis_peraturan')),
        'singkatan_jenis_peraturan' => trim($this->input->post('singkatan_jenis_peraturan')),
        'cetakan_edisi' => trim($this->input->post('cetakan_edisi')),
        'tempat_terbit' => trim($this->input->post('tempat_terbit')),
        'penerbit' => trim($this->input->post('penerbit')),
        'tanggal_pengundangan' => trim($this->input->post('tanggal_pengundangan')),
        'kolasi_deskripsi_fisik' => trim($this->input->post('kolasi_deskripsi_fisik')),
        'sumber' => trim($this->input->post('sumber')),
        'subjek' => trim($this->input->post('subjek')),
        'isbn' => trim($this->input->post('isbn')),
        'bahasa' => trim($this->input->post('bahasa')),
        'bidang_hukum' => trim($this->input->post('bidang_hukum')),
        'nomor_induk_buku' => trim($this->input->post('nomor_induk_buku')),
        'keterangan' => trim($this->input->post('keterangan')),
        'temp' => trim($this->input->post('temp')),
        'inserted_by' => $this->session->userdata('id_users'),
        'inserted_time' => date('Y-m-d H:i:s'),
        'status' => 1
      );
      $this->db->insert( 'perundang_undangan', $data_input );
      $id= $this->db->insert_id();
      echo '' . $id . '';
      $where       = array(
        'table_name' => 'perundang_undangan',
        'temp' => trim($this->input->post('temp'))
      );
      $data_update = array(
        'id_tabel' => $id
      );
      $this->db->where($where);
      $this->db->update('attachment', $data_update);
    }
  }
  
  public function update()
  {
    $this->form_validation->set_rules('id_perundang_undangan', 'id_perundang_undangan', 'required');
    $this->form_validation->set_rules('jenis_perundang_undangan_id', 'jenis_perundang_undangan_id', 'required');
    $this->form_validation->set_rules('module_id', 'module_id', 'required');
    $this->form_validation->set_rules('information', 'information', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'jenis_perundang_undangan_id' => trim($this->input->post('jenis_perundang_undangan_id')),
        'module_id' => trim($this->input->post('module_id')),
        'information' => trim($this->input->post('information')),
        'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
      );
      $where       = array(
        'id_perundang_undangan' => trim($this->input->post('id_perundang_undangan'))
      );
      $this->db->where($where);
      $this->db->update('perundang_undangan', $data_update);
      echo 1;
    }
  }
  
  public function get_by_id()
  {
    $id_perundang_undangan = $this->input->post('id_perundang_undangan');
    $where   = array(
      'perundang_undangan.id_perundang_undangan' => $id_perundang_undangan
    );
    $this->db->select("
    perundang_undangan.id_perundang_undangan,
    perundang_undangan.jenis_perundang_undangan_id,
    perundang_undangan.nomor_peraturan,
    perundang_undangan.tahun_pengundangan,
    perundang_undangan.status_peraturan,
    perundang_undangan.nomor_panggil,
    perundang_undangan.judul,
    perundang_undangan.nama_pengarang,
    perundang_undangan.tipe_pengarang,
    perundang_undangan.jenis_pengarang,
    perundang_undangan.jenis_peraturan,
    perundang_undangan.singkatan_jenis_peraturan,
    perundang_undangan.cetakan_edisi,
    perundang_undangan.tempat_terbit,
    perundang_undangan.penerbit,
    perundang_undangan.tanggal_pengundangan,
    perundang_undangan.kolasi_deskripsi_fisik,
    perundang_undangan.sumber,
    perundang_undangan.subjek,
    perundang_undangan.isbn,
    perundang_undangan.bahasa,
    perundang_undangan.bidang_hukum,
    perundang_undangan.nomor_induk_buku,
    perundang_undangan.nama_file_download,
    perundang_undangan.url_file_lampiran,
    perundang_undangan.url_halaman_peraturan,
    perundang_undangan.keterangan,
    perundang_undangan.inserted_by,
    perundang_undangan.inserted_time,
    perundang_undangan.updated_by,
    perundang_undangan.updated_time,
    perundang_undangan.deleted_by,
    perundang_undangan.deleted_time,
    perundang_undangan.status,
    
    jenis_perundang_undangan.jenis_perundang_undangan_code,
    jenis_perundang_undangan.jenis_perundang_undangan_name,
    
      ");    	
    $this->db->where($where);
    $this->db->join('role', 'role.jenis_perundang_undangan_id=perundang_undangan.jenis_perundang_undangan_id');
    $this->db->join('module', 'module.module_id=perundang_undangan.module_id');
		$result = $this->db->get('perundang_undangan');
    echo json_encode($result->result_array());
  }
  
  public function delete_by_id()
  {
    $this->form_validation->set_rules('id_perundang_undangan', 'id_perundang_undangan', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 99
      );
      $where       = array(
        'id_perundang_undangan' => trim($this->input->post('id_perundang_undangan'))
      );
      $table_name  = 'perundang_undangan';
      $this->db->where($where);
      $this->db->update('perundang_undangan', $data_update);
      echo 1;
    }
  }
  
  public function restore_by_id()
  {
    $this->form_validation->set_rules('id_perundang_undangan', 'id_perundang_undangan', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 1
      );
      $where       = array(
        'id_perundang_undangan' => trim($this->input->post('id_perundang_undangan'))
      );
      $table_name  = 'perundang_undangan';
      $this->db->where($where);
      $this->db->update($table_name, $data_update);
      echo 1;
    }
  }
  
  
  public function perundang_undangan_by_jenis_perundang_undangan_id()
  {
    $fields      = "
    perundang_undangan.id_perundang_undangan,
    perundang_undangan.jenis_perundang_undangan_id,
    perundang_undangan.nomor_peraturan,
    perundang_undangan.tahun_pengundangan,
    perundang_undangan.status_peraturan,
    perundang_undangan.nomor_panggil,
    perundang_undangan.judul,
    perundang_undangan.nama_pengarang,
    perundang_undangan.tipe_pengarang,
    perundang_undangan.jenis_pengarang,
    perundang_undangan.jenis_peraturan,
    perundang_undangan.singkatan_jenis_peraturan,
    perundang_undangan.cetakan_edisi,
    perundang_undangan.tempat_terbit,
    perundang_undangan.penerbit,
    perundang_undangan.tanggal_pengundangan,
    perundang_undangan.kolasi_deskripsi_fisik,
    perundang_undangan.sumber,
    perundang_undangan.subjek,
    perundang_undangan.isbn,
    perundang_undangan.bahasa,
    perundang_undangan.bidang_hukum,
    perundang_undangan.nomor_induk_buku,
    perundang_undangan.nama_file_download,
    perundang_undangan.url_file_lampiran,
    perundang_undangan.url_halaman_peraturan,
    perundang_undangan.keterangan,
    perundang_undangan.inserted_by,
    perundang_undangan.inserted_time,
    perundang_undangan.updated_by,
    perundang_undangan.updated_time,
    perundang_undangan.deleted_by,
    perundang_undangan.deleted_time,
    perundang_undangan.status,
    
    jenis_perundang_undangan.jenis_perundang_undangan_code,
    jenis_perundang_undangan.jenis_perundang_undangan_name,
    
            
            ";
    $where       = array(
      'perundang_undangan.jenis_perundang_undangan_id' => $this->input->post('jenis_perundang_undangan_id')
    );
    $this->db->select("$fields");
    $this->db->where($where);
    $this->db->join('jenis_perundang_undangan', 'jenis_perundang_undangan.jenis_perundang_undangan_id=perundang_undangan.jenis_perundang_undangan_id');
    $this->db->order_by('perundang_undangan.jenis_perundang_undangan_id');
    $result = $this->db->get('perundang_undangan');
    echo json_encode($result->result_array());
  }
  
}