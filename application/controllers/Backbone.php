<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backbone extends CI_Controller {

	function __construct()
		{
    parent::__construct();
    $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Posting_model');
		}    
 
  private function menu_atas($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
			$hasil .= '
        <ul id="responsivemenu"> <li class="active"><a href="'.base_url().'"><i class="icon-home homeicon"></i><span class="showmobile">Home</span></a></li>'; 
      }
      else{
        $hasil .= '
        <ul id="responsivemenu"> ';
      }
		}
      $nomor = 0;
      foreach($w->result() as $h)
      {
      
      $r = $this->db->query("
      SELECT * from posting
                    
      where parent='".$h->id_posting."' 
      and status = 1 
      and domain='".$web."'
      order by urut
      ");
      $xx = $r->num_rows();
      $nomor = $nomor + 1;
        if( $a > 1 ){
          $hasil .= '
          <li> <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a>';
        }
        else{
          if ($xx == 0){
          $hasil .= '
            <li> <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.'</a>';
          }
          else{
          $hasil .= '
            <li> <a>'.$h->judul_posting.'</a>';
          } 
        }
        $hasil = $this->menu_atas($h->id_posting,$hasil, $a);
        $hasil .= '</li>';
      }
		if(($w->num_rows)>0)
		{
			$hasil .= "
      </ul>";
		}
    else{      
    }
		
		return $hasil;
	}
  
  public function get_attachment_by_id_posting($id_posting)
   {
     $where = array(
						'id_tabel' => $id_posting
						);
    $d = $this->Posting_model->get_attachment_by_id_posting($where);
    if(!$d)
          {
						return 'blankgambar.jpg';
          }
        else
          {
						return $d['file_name'];
          }
   } 
  
  private function posting_highlight($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where parent='".$parent."' 
		and posting.status = 1 
    and domain='".$web."'
    order by urut desc
    
    
		");
		$x = $w->num_rows();
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    $nomor = $nomor + 1;
    $rs = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."'
    and domain='".$web."'
    
		");
		$xx = $rs->num_rows();
      if( $h->highlight == 1 ){
       
            $hasils .= 
            '
				<div class="boxfourcolumns">
					<div class="boxcontainer">
						<span class="gallery">
						<a data-gal="prettyPhoto[gallery1]" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$h->judul_posting.'"><img src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="'.$h->id_posting.'" class="imgOpa"/></a>
						</span>
						<h1><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$h->judul_posting.'">'.substr(strip_tags($h->judul_posting), 0, 50).'</a></h1>
					</div>
				</div>
            ';
          
      }        
			
      $hasils = $this->posting_highlight($h->id_posting,$hasils, $r);
		}
		return $hasils;
	}
		
	public function index()
	{
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
      
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
    
    $r = 0;
    $p=0;
    $data['galery_berita'] = $this->posting_highlight($p,$q="", $r);
    $a = 0;
    $data['total'] = 10;
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      
		$data['judul'] = 'Selamat datang';
		$data['main_view'] = 'backbone/welcome_page';
		$this->load->view('backbone', $data);
	}
  
}
