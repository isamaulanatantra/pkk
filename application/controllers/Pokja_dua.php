<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pokja_dua extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Pokja_dua_model');
   }
   
  public function index(){
    $data['main_view'] = 'pokja_dua/home';
    $this->load->view('tes', $data);
   }
  public function rekap(){
    $data['main_view'] = 'pokja_dua/rekap';
    $this->load->view('tes', $data);
   }
  public function cetak_pokja_dua(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_dua/cetak_pokja_dua';
    $this->load->view('print', $data);
   }
  public function cetak_semua_pokja_dua(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_dua/cetak_semua_pokja_dua';
    $this->load->view('print', $data);
   }
  public function cetak_pokja_dua_pekarangan(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_dua/cetak_pokja_dua_pekarangan';
    $this->load->view('print', $data);
   }
  public function cetak(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_dua/cetak';
    $this->load->view('print', $data);
   }
	public function json_all_pokja_dua(){
    $web=$this->uut->namadomain(base_url());
		$table = 'pokja_dua';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
    ( select (dusun.nama_dusun) from dusun where dusun.id_dusun=pokja_dua.id_dusun limit 1) as nama_dusun,
    ( select (desa.nama_desa) from desa where desa.id_desa=pokja_dua.id_desa limit 1) as nama_desa,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=pokja_dua.id_kecamatan limit 1) as nama_kecamatan
    ";
    if($web=='demoopd.wonosobokab.go.id'){
      $where = array(
        'pokja_dua.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where = array(
        'pokja_dua.status !=' => 99
        );
    }else{
      $where = array(
        'pokja_dua.status !=' => 99,
        'pokja_dua.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        $query_desa = $this->db->query("SELECT desa.nama_desa FROM desa WHERE desa.id_desa = ".$row->id_desa."");
        echo '
        <tr id_pokja_dua="'.$row->id_pokja_dua.'" id="'.$row->id_pokja_dua.'" name="id_pokja_dua">
          <td>'.$urut.'</td>
          <td>'; 
            $id_desa = $row->id_desa; 
            if($id_desa=='9999'){
              echo 'TP PKK Kecamatan';
            } 
            else{
              foreach ($query_desa->result() as $row_desa){
                echo ''.$row_desa->nama_desa.'';
              }
            }
          echo'</td>
          <td>'.$row->warga_masih_3buta.'</td>
          <td>'.$row->paketa_kelbelajar.'</td>
          <td>'.$row->paketa_wargabelajar.'</td>
          <td>'.$row->paketb_kelbelajar.'</td>
          <td>'.$row->paketb_wargabelajar.'</td>
          <td>'.$row->paketc_kelbelajar.'</td>
          <td>'.$row->paketc_wargabelajar.'</td>
          <td>'.$row->kf_kelbelajar.'</td>
          <td>'.$row->kf_wargabelajar.'</td>
          <td>'.$row->paud_sejenis.'</td>
          <td>'.$row->tamanbacaan_perpustakaan.'</td>
          <td>'.$row->bkb_kel.'</td>
          <td>'.$row->bkb_ibu_peserta.'</td>
          <td>'.$row->bkb_ape.'</td>
          <td>'.$row->bkb_kecsimulasibkb.'</td>
          <td>'.$row->kaderkhusus_tutorkf.'</td>
          <td>'.$row->kaderkhusus_tutorpaud_sejenis.'</td>
          <td>'.$row->kaderkhusus_bkb.'</td>
          <td>'.$row->kaderkhusus_koperasi.'</td>
          <td>'.$row->kaderkhusus_keterampilan.'</td>
          <td>'.$row->kaderyangsudah_lp3pkk.'</td>
          <td>'.$row->kaderyangsudah_tpk3pkk.'</td>
          <td>'.$row->kaderyangsudah_damaspkk.'</td>
          <td>'.$row->up2k_pemula_klp.'</td>
          <td>'.$row->up2k_pemula_peserta.'</td>
          <td>'.$row->up2k_madya_klp.'</td>
          <td>'.$row->up2k_madya_peserta.'</td>
          <td>'.$row->up2k_utama_klp.'</td>
          <td>'.$row->up2k_utama_peserta.'</td>
          <td>'.$row->up2k_mandiri_klp.'</td>
          <td>'.$row->up2k_mandiri_peserta.'</td>
          <td>'.$row->koperasi_berbadan_klp.'</td>
          <td>'.$row->koperasi_berbadan_anggota.'</td>
          <td>'.$row->keterangan.'</td>
          <td>
            <a href="#tab_form_pokja_dua" data-toggle="tab" class="update_id_pokja_dua badge bg-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>
            <a href="#" id="del_ajax_pokja_dua" class="badge bg-danger btn-sm"><i class="fa fa-cut"></i></a>
            <a href="#" class="badge bg-gray btn-sm">'.$row->tahun.'</a>
          </td>
        </tr>
        ';
      }
			/*echo '
      <tr>
        <td valign="top" colspan="10" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'pokja_dua/cetak_pokja_dua/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Keluarga</a>
        </td>
      </tr>
      ';*/
          
	}
	public function json_all_rekap_pokja_dua(){
    $web=$this->uut->namadomain(base_url());
    $query = $this->db->query("SELECT SUM(warga_masih_3buta)as warga_masih_3buta, 
     SUM(paketa_kelbelajar)as paketa_kelbelajar,
     SUM(paketa_wargabelajar)as paketa_wargabelajar,
     SUM(paketb_kelbelajar)as paketb_kelbelajar,
     SUM(paketb_wargabelajar)as paketb_wargabelajar,
     SUM(paketc_kelbelajar)as paketc_kelbelajar,
     SUM(paketc_wargabelajar)as paketc_wargabelajar,
     SUM(kf_kelbelajar)as kf_kelbelajar,
     SUM(kf_wargabelajar)as kf_wargabelajar,
     SUM(paud_sejenis)as paud_sejenis,
     SUM(tamanbacaan_perpustakaan)as tamanbacaan_perpustakaan,
     SUM(bkb_kel)as bkb_kel,
     SUM(bkb_ibu_peserta)as bkb_ibu_peserta,
     SUM(bkb_ape)as bkb_ape,
     SUM(bkb_kecsimulasibkb)as bkb_kecsimulasibkb,
     SUM(kaderkhusus_tutorkf)as kaderkhusus_tutorkf,
     SUM(kaderkhusus_tutorpaud_sejenis)as kaderkhusus_tutorpaud_sejenis,
     SUM(kaderkhusus_bkb)as kaderkhusus_bkb,
     SUM(kaderkhusus_koperasi)as kaderkhusus_koperasi,
     SUM(kaderkhusus_keterampilan)as kaderkhusus_keterampilan,
     SUM(kaderyangsudah_lp3pkk)as kaderyangsudah_lp3pkk,
     SUM(kaderyangsudah_tpk3pkk)as kaderyangsudah_tpk3pkk,
     SUM(kaderyangsudah_damaspkk)as kaderyangsudah_damaspkk,
     SUM(up2k_pemula_klp)as up2k_pemula_klp,
     SUM(up2k_pemula_peserta)as up2k_pemula_peserta,
     SUM(up2k_madya_klp)as up2k_madya_klp,
     SUM(up2k_madya_peserta)as up2k_madya_peserta,
     SUM(up2k_utama_klp)as up2k_utama_klp,
     SUM(up2k_utama_peserta)as up2k_utama_peserta,
     SUM(up2k_mandiri_klp)as up2k_mandiri_klp,
     SUM(up2k_mandiri_peserta)as up2k_mandiri_peserta,
     SUM(koperasi_berbadan_klp)as koperasi_berbadan_klp,
     SUM(koperasi_berbadan_anggota)as koperasi_berbadan_anggota,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=pokja_dua.id_kecamatan limit 1) as nama_kecamatan
    FROM pokja_dua WHERE pokja_dua.id_kabupaten=1 AND pokja_dua.id_propinsi=1 GROUP BY pokja_dua.id_kecamatan");
    $urut=0;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        echo '
        <tr>
          <td>'.$urut.'</td>
          <td>'.$row->nama_kecamatan.'</td>
          <td>'.$row->warga_masih_3buta.'</td>
          <td>'.$row->paketa_kelbelajar.'</td>
          <td>'.$row->paketa_wargabelajar.'</td>
          <td>'.$row->paketb_kelbelajar.'</td>
          <td>'.$row->paketb_wargabelajar.'</td>
          <td>'.$row->paketc_kelbelajar.'</td>
          <td>'.$row->paketc_wargabelajar.'</td>
          <td>'.$row->kf_kelbelajar.'</td>
          <td>'.$row->kf_wargabelajar.'</td>
          <td>'.$row->paud_sejenis.'</td>
          <td>'.$row->tamanbacaan_perpustakaan.'</td>
          <td>'.$row->bkb_kel.'</td>
          <td>'.$row->bkb_ibu_peserta.'</td>
          <td>'.$row->bkb_ape.'</td>
          <td>'.$row->bkb_kecsimulasibkb.'</td>
          <td>'.$row->kaderkhusus_tutorkf.'</td>
          <td>'.$row->kaderkhusus_tutorpaud_sejenis.'</td>
          <td>'.$row->kaderkhusus_bkb.'</td>
          <td>'.$row->kaderkhusus_koperasi.'</td>
          <td>'.$row->kaderkhusus_keterampilan.'</td>
          <td>'.$row->kaderyangsudah_lp3pkk.'</td>
          <td>'.$row->kaderyangsudah_tpk3pkk.'</td>
          <td>'.$row->kaderyangsudah_damaspkk.'</td>
          <td>'.$row->up2k_pemula_klp.'</td>
          <td>'.$row->up2k_pemula_peserta.'</td>
          <td>'.$row->up2k_madya_klp.'</td>
          <td>'.$row->up2k_madya_peserta.'</td>
          <td>'.$row->up2k_utama_klp.'</td>
          <td>'.$row->up2k_utama_peserta.'</td>
          <td>'.$row->up2k_mandiri_klp.'</td>
          <td>'.$row->up2k_mandiri_peserta.'</td>
          <td>'.$row->koperasi_berbadan_klp.'</td>
          <td>'.$row->koperasi_berbadan_anggota.'</td>
          <td></td>
        </tr>
        ';
      }
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='demoopd.wonosobokab.go.id'){
      $where0 = array(
        'pokja_dua.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where0 = array(
        'pokja_dua.status !=' => 99
        );
    }else{
      $where0 = array(
        'pokja_dua.status !=' => 99,
        'pokja_dua.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('pokja_dua');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_pokja_dua()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('warga_masih_3buta', 'warga_masih_3buta', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => 1,
        'id_kabupaten' => 1,
        'id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'tahun' => trim($this->input->post('tahun')),
        'warga_masih_3buta' => trim($this->input->post('warga_masih_3buta')),
        'paketa_kelbelajar' => trim($this->input->post('paketa_kelbelajar')),
        'paketa_wargabelajar' => trim($this->input->post('paketa_wargabelajar')),
        'paketb_kelbelajar' => trim($this->input->post('paketb_kelbelajar')),
        'paketb_wargabelajar' => trim($this->input->post('paketb_wargabelajar')),
        'paketc_kelbelajar' => trim($this->input->post('paketc_kelbelajar')),
        'paketc_wargabelajar' => trim($this->input->post('paketc_wargabelajar')),
        'kf_kelbelajar' => trim($this->input->post('kf_kelbelajar')),
        'kf_wargabelajar' => trim($this->input->post('kf_wargabelajar')),
        'paud_sejenis' => trim($this->input->post('paud_sejenis')),
        'tamanbacaan_perpustakaan' => trim($this->input->post('tamanbacaan_perpustakaan')),
        'bkb_kel' => trim($this->input->post('bkb_kel')),
        'bkb_ibu_peserta' => trim($this->input->post('bkb_ibu_peserta')),
        'bkb_ape' => trim($this->input->post('bkb_ape')),
        'bkb_kecsimulasibkb' => trim($this->input->post('bkb_kecsimulasibkb')),
        'kaderkhusus_tutorkf' => trim($this->input->post('kaderkhusus_tutorkf')),
        'kaderkhusus_tutorpaud_sejenis' => trim($this->input->post('kaderkhusus_tutorpaud_sejenis')),
        'kaderkhusus_bkb' => trim($this->input->post('kaderkhusus_bkb')),
        'kaderkhusus_koperasi' => trim($this->input->post('kaderkhusus_koperasi')),
        'kaderkhusus_keterampilan' => trim($this->input->post('kaderkhusus_keterampilan')),
        'kaderyangsudah_lp3pkk' => trim($this->input->post('kaderyangsudah_lp3pkk')),
        'kaderyangsudah_tpk3pkk' => trim($this->input->post('kaderyangsudah_tpk3pkk')),
        'kaderyangsudah_damaspkk' => trim($this->input->post('kaderyangsudah_damaspkk')),
        'up2k_pemula_klp' => trim($this->input->post('up2k_pemula_klp')),
        'up2k_pemula_peserta' => trim($this->input->post('up2k_pemula_peserta')),
        'up2k_madya_klp' => trim($this->input->post('up2k_madya_klp')),
        'up2k_madya_peserta' => trim($this->input->post('up2k_madya_peserta')),
        'up2k_utama_klp' => trim($this->input->post('up2k_utama_klp')),
        'up2k_utama_peserta' => trim($this->input->post('up2k_utama_peserta')),
        'up2k_mandiri_klp' => trim($this->input->post('up2k_mandiri_klp')),
        'up2k_mandiri_peserta' => trim($this->input->post('up2k_mandiri_peserta')),
        'koperasi_berbadan_klp' => trim($this->input->post('koperasi_berbadan_klp')),
        'koperasi_berbadan_anggota' => trim($this->input->post('koperasi_berbadan_anggota')),
        'keterangan' => trim($this->input->post('keterangan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'pokja_dua';
      $this->Pokja_dua_model->simpan_pokja_dua($data_input, $table_name);
      /*echo $id;
			$table_name  = 'anggota_keluarga';
      $where       = array(
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_pokja_dua' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);*/
     }
   }
  public function update_pokja_dua()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('warga_masih_3buta', 'warga_masih_3buta', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => 1,
        'id_kabupaten' => 1,
        'id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'tahun' => trim($this->input->post('tahun')),
        'warga_masih_3buta' => trim($this->input->post('warga_masih_3buta')),
        'paketa_kelbelajar' => trim($this->input->post('paketa_kelbelajar')),
        'paketa_wargabelajar' => trim($this->input->post('paketa_wargabelajar')),
        'paketb_kelbelajar' => trim($this->input->post('paketb_kelbelajar')),
        'paketb_wargabelajar' => trim($this->input->post('paketb_wargabelajar')),
        'paketc_kelbelajar' => trim($this->input->post('paketc_kelbelajar')),
        'paketc_wargabelajar' => trim($this->input->post('paketc_wargabelajar')),
        'kf_kelbelajar' => trim($this->input->post('kf_kelbelajar')),
        'kf_wargabelajar' => trim($this->input->post('kf_wargabelajar')),
        'paud_sejenis' => trim($this->input->post('paud_sejenis')),
        'tamanbacaan_perpustakaan' => trim($this->input->post('tamanbacaan_perpustakaan')),
        'bkb_kel' => trim($this->input->post('bkb_kel')),
        'bkb_ibu_peserta' => trim($this->input->post('bkb_ibu_peserta')),
        'bkb_ape' => trim($this->input->post('bkb_ape')),
        'bkb_kecsimulasibkb' => trim($this->input->post('bkb_kecsimulasibkb')),
        'kaderkhusus_tutorkf' => trim($this->input->post('kaderkhusus_tutorkf')),
        'kaderkhusus_tutorpaud_sejenis' => trim($this->input->post('kaderkhusus_tutorpaud_sejenis')),
        'kaderkhusus_bkb' => trim($this->input->post('kaderkhusus_bkb')),
        'kaderkhusus_koperasi' => trim($this->input->post('kaderkhusus_koperasi')),
        'kaderkhusus_keterampilan' => trim($this->input->post('kaderkhusus_keterampilan')),
        'kaderyangsudah_lp3pkk' => trim($this->input->post('kaderyangsudah_lp3pkk')),
        'kaderyangsudah_tpk3pkk' => trim($this->input->post('kaderyangsudah_tpk3pkk')),
        'kaderyangsudah_damaspkk' => trim($this->input->post('kaderyangsudah_damaspkk')),
        'up2k_pemula_klp' => trim($this->input->post('up2k_pemula_klp')),
        'up2k_pemula_peserta' => trim($this->input->post('up2k_pemula_peserta')),
        'up2k_madya_klp' => trim($this->input->post('up2k_madya_klp')),
        'up2k_madya_peserta' => trim($this->input->post('up2k_madya_peserta')),
        'up2k_utama_klp' => trim($this->input->post('up2k_utama_klp')),
        'up2k_utama_peserta' => trim($this->input->post('up2k_utama_peserta')),
        'up2k_mandiri_klp' => trim($this->input->post('up2k_mandiri_klp')),
        'up2k_mandiri_peserta' => trim($this->input->post('up2k_mandiri_peserta')),
        'koperasi_berbadan_klp' => trim($this->input->post('koperasi_berbadan_klp')),
        'koperasi_berbadan_anggota' => trim($this->input->post('koperasi_berbadan_anggota')),
        'keterangan' => trim($this->input->post('keterangan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'pokja_dua';
      $where       = array(
        'pokja_dua.id_pokja_dua' => trim($this->input->post('id_pokja_dua')),
        'pokja_dua.domain' => $web
			);
      $this->Pokja_dua_model->update_data_pokja_dua($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      if($web=='demoopd.wonosobokab.go.id'){
        $where = array(
        'pokja_dua.id_pokja_dua' => $this->input->post('id_pokja_dua'),
          );
      }elseif($web=='pkk.wonosobokab.go.id'){
        $where = array(
        'pokja_dua.id_pokja_dua' => $this->input->post('id_pokja_dua'),
          );
      }else{
        $where = array(
        'pokja_dua.id_pokja_dua' => $this->input->post('id_pokja_dua'),
          'pokja_dua.id_kecamatan' => $this->session->userdata('id_kecamatan')
          );
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_pokja_dua');
      $result = $this->db->get('pokja_dua');
      echo json_encode($result->result_array());
		}
   
   public function total_pokja_dua()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('pokja_dua');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_pokja_dua = $this->input->post('id_pokja_dua');
      $where = array(
        'id_pokja_dua' => $id_pokja_dua,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('pokja_dua');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'pokja_dua';
        $this->Pokja_dua_model->update_data_pokja_dua($data_update, $where, $table_name);
        echo 1;
        }
		}
  function option_desa_by_id_kecamatan(){
		$id_kecamatan = $this->session->userdata('id_kecamatan');
		$w = $this->db->query("
		SELECT id_desa, nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function option_desa_by_id_desa(){
		$id_kecamatan = $this->input->post('id_kecamatan');
		$w = $this->db->query("
		SELECT id_desa, nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function option_propinsi(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_propinsi="";
		}
		else{
			//$where_id_propinsi="and propinsi.id_propinsi=".$this->session->userdata('id_propinsi')."";
			$where_id_propinsi="";
		}
		$w = $this->db->query("
		SELECT *
		from propinsi
		where propinsi.status = 1
    ".$where_id_propinsi."
		order by propinsi.id_propinsi
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_propinsi.'">'.$h->nama_propinsi.'</option>';
		}
	}
  function id_kabupaten_by_id_propinsi(){
		$id_propinsi = $this->input->post('id_propinsi');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kabupaten
		where kabupaten.status = 1
    and kabupaten.id_propinsi=".$id_propinsi."
		order by kabupaten.id_kabupaten
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kabupaten.'">'.$h->nama_kabupaten.'</option>';
		}
	}
  function id_kecamatan_by_id_kabupaten(){
		$id_kabupaten = $this->input->post('id_kabupaten');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kecamatan
		where kecamatan.status = 1
		and kecamatan.id_kabupaten=".$id_kabupaten."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function id_desa_by_id_kecamatan(){
		$id_kecamatan = $this->session->userdata('id_kecamatan');
		$w = $this->db->query("
		SELECT nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function id_dusun_by_id_desa(){
		$id_desa = $this->input->post('id_desa');
		$w = $this->db->query("
		SELECT *
		from dusun
		where dusun.status = 1
		and dusun.id_desa = ".$id_desa."
		order by id_dusun");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_dusun.'">'.$h->nama_dusun.'</option>';
		}
	}
  function option_kecamatan(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_kecamatan="";
		}
		else{
			$where_id_kecamatan="and data_kecamatan.id_kecamatan=".$this->session->userdata('id_kecamatan')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_kecamatan, kecamatan
		where data_kecamatan.status = 1
		and kecamatan.id_kecamatan=data_kecamatan.id_kecamatan
    ".$where_id_kecamatan."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function status_dalam_keluarga(){
		$w = $this->db->query("
		SELECT *
		from status_dalam_keluarga
		where status_dalam_keluarga.status = 1
		order by status_dalam_keluarga.id_status_dalam_keluarga
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_dalam_keluarga.'">'.$h->nama_status_dalam_keluarga.'</option>';
		}
	}
  function pekerjaan(){
		$w = $this->db->query("
		SELECT *
		from pekerjaan
		where pekerjaan.status = 1
		order by pekerjaan.id_pekerjaan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pekerjaan.'">'.$h->nama_pekerjaan.'</option>';
		}
	}
  function agama(){
		$w = $this->db->query("
		SELECT *
		from agama
		where agama.status = 1
		order by agama.id_agama
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_agama.'">'.$h->nama_agama.'</option>';
		}
	}
  function status_pernikahan(){
		$w = $this->db->query("
		SELECT *
		from status_pernikahan
		where status_pernikahan.status = 1
		order by status_pernikahan.id_status_pernikahan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_pernikahan.'">'.$h->nama_status_pernikahan.'</option>';
		}
	}
  function pendidikan(){
		$w = $this->db->query("
		SELECT *
		from pendidikan
		where pendidikan.status = 1
		order by pendidikan.id_pendidikan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pendidikan.'">'.$h->nama_pendidikan.'</option>';
		}
	}
 }