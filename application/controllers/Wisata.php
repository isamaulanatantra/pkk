<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Wisata extends CI_Controller
{

  function __construct(){
		parent::__construct();
    $this->load->library('Uut');
		$this->load->model('Wisata_model');
  }
  public function index(){
    $data['main_view'] = 'wisata/home';
    $this->load->view('tes', $data);
  }
  public function json_option_wisata(){
		$table = 'wisata';
    $id_skpd = trim($this->input->post('id_skpd'));
		$hak_akses = $this->session->userdata('hak_akses');
		$where   = array(
			'wisata.status !=' => 99
			);
		$fields  = 
			"
			wisata.id_wisata,
			wisata.nama_wisata
			";
		$order_by  = 'wisata.nama_wisata';
		echo json_encode($this->Wisata_model->opsi($table, $where, $fields, $order_by));
  }
  public function total_data()
  {
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'wisata.status !=' => 99
      );
    $this->db->where($where0);
    $query0 = $this->db->get('wisata');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_wisata(){
		$table = 'wisata';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where      = array(
      'wisata.status !=' => 99
    );
    $orderby   = ''.$order_by.'';
    $query = $this->Wisata_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
          $urut=$urut+1;
          echo '<tr id_wisata="'.$row->id_wisata.'" id="'.$row->id_wisata.'" >';
          echo '<td valign="top">'.($urut).'</td>';
					echo '<td valign="top">'.$row->id_jenis_wisata.'</td>';
					echo '<td valign="top">'.$row->nama_wisata.'</td>';
					echo '<td valign="top">';if($row->status_wisata=='1'){echo'Aktif';}else{echo'Menunggu Verifikasi';} echo'</td>';
					echo '<td valign="top">';
          $query_desa = $this->db->query("
          SELECT nama_desa
          from desa
          where desa.status = 1
          and desa.id_desa=".$row->id_desa."
          ");
          foreach($query_desa->result() as $row_desa){
            echo ''.$row_desa->nama_desa.'';
          }
          echo'</td>';
					echo '<td valign="top">';
          $query_kecamatan = $this->db->query("
          SELECT nama_kecamatan
          from kecamatan
          where kecamatan.status = 1
          and id_kecamatan=".$row->id_kecamatan."
          ");
          foreach($query_kecamatan->result() as $row_kecamatan){
            echo ''.$row_kecamatan->nama_kecamatan.'';
          }
          echo'</td>';
					echo '<td valign="top"><a href="#tab_form_wisata" data-toggle="tab" id="update_id" class="update_id badge bg-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>';
					echo '<a href="#" id="del_ajax" class="badge bg-danger btn-sm"><i class="fa fa-cut"></i></a>';
          echo '<a class="badge bg-warning btn-sm" href="'.base_url().'wisata/pdf/?id_wisata='.$row->id_wisata.'" ><i class="fa fa-file-pdf-o"></i></a></td>';
          echo '</tr>';
      }
          echo '<tr>';
          echo '<td valign="top" colspan="7" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="',base_url(),'wisata/xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
          echo '</tr>';
          
   }
  public function simpan_wisata(){
    $this->form_validation->set_rules('nama_wisata', 'nama_wisata', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
			{
				$data_input = array(
				'nama_wisata' => trim($this->input->post('nama_wisata')),
				'id_jenis_wisata' => trim($this->input->post('id_jenis_wisata')),
				'status_wisata' => trim($this->input->post('status_wisata')),
				'id_propinsi' => 1,
				'id_kabupaten' => 1,
				'id_kecamatan' => trim($this->input->post('id_kecamatan')),
				'id_desa' => trim($this->input->post('id_desa')),
				'id_dusun' => 0,
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'wisata';
      $this->Wisata_model->save_data($data_input, $table_name);
     }
   }
   public function get_by_id()
		{
      $where    = array(
        'id_wisata' => $this->input->post('id_wisata'),
				'created_by' => $this->session->userdata('id_users')
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_wisata');
      $result = $this->db->get('wisata');
      echo json_encode($result->result_array());
		}
  public function update_wisata(){
		$this->form_validation->set_rules('nama_wisata', 'nama_wisata', 'required');
    if ($this->form_validation->run() == FALSE){
      echo 0;
		}
    else{
      $data_update = array(
			
        'nama_wisata' => trim($this->input->post('nama_wisata')),
				'id_jenis_wisata' => trim($this->input->post('id_jenis_wisata')),
				'status_wisata' => trim($this->input->post('status_wisata')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
			);
      $table_name  = 'wisata';
      $where       = array(
        'id_wisata' => trim($this->input->post('id_wisata')),
				'created_by' => $this->session->userdata('id_users')
			);
      $this->Wisata_model->update_data($data_update, $where, $table_name);
      echo 1;
		}
  }
  public function hapus()
		{
			$id_wisata = $this->input->post('id_wisata');
      $where = array(
        'id_wisata' => $id_wisata,
				'created_by' => $this->session->userdata('id_users')
        );
      $this->db->from('wisata');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'wisata';
        $this->Wisata_model->update_data($data_update, $where, $table_name);
        echo 1;
        }
		}
  function option_kecamatan(){
    $web=$this->uut->namadomain(base_url());
		if($web=='diskominfo.wonosobokab.go.id'){
			$where_id_kecamatan="";
		}
		elseif($web=='disparbud.wonosobokab.go.id'){
			$where_id_kecamatan="";
		}
		else{
			$where_id_kecamatan="and data_kecamatan.id_kecamatan=".$this->session->userdata('id_kecamatan')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_kecamatan, kecamatan
		where data_kecamatan.status = 1
		and kecamatan.id_kecamatan=data_kecamatan.id_kecamatan
    ".$where_id_kecamatan."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
    
}