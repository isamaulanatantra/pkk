<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Kategori_data extends CI_Controller
{

  function __construct(){
		parent::__construct();
		$this->load->model('Kategori_data_model');
  }
  public function index(){
    $data['main_view'] = 'kategori_data/home';
    $this->load->view('back_bone', $data);
  }
  public function json_option_kategori_data(){
		$table = 'kategori_data';
    $id_skpd = trim($this->input->post('id_skpd'));
		$hak_akses = $this->session->userdata('hak_akses');
		$where   = array(
			'kategori_data.id_skpd' => $id_skpd
			);
		$fields  = 
			"
			kategori_data.id_kategori_data,
			kategori_data.nama_kategori_data
			";
		$order_by  = 'kategori_data.nama_kategori_data';
		echo json_encode($this->Kategori_data_model->opsi($table, $where, $fields, $order_by));
  }
  public function total_data()
  {
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'kategori_data.status !=' => 99,
      'kategori_data.status !=' => 999
      );
    $this->db->where($where0);
    $query0 = $this->db->get('kategori_data');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_kategori_data(){
		$table = 'kategori_data';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
		(select skpd.nama_skpd FROM skpd where skpd.id_skpd=kategori_data.id_skpd) as nama_skpd
    ";
    $where      = array(
      'kategori_data.status !=' => 99
    );
    $orderby   = ''.$order_by.'';
    $query = $this->Kategori_data_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
          $urut=$urut+1;
          echo '<tr id_kategori_data="'.$row->id_kategori_data.'" id="'.$row->id_kategori_data.'" >';
          echo '<td valign="top">'.($urut).'</td>';
					echo '<td valign="top">'.$row->nama_skpd.'</td>';
					echo '<td valign="top">'.$row->nama_kategori_data.'</td>';
					echo '<td valign="top"><a href="#tab_form_kategori_data" data-toggle="tab" class="update_id btn btn-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>';
					echo '<a href="#" id="del_ajax" class="btn btn-danger btn-sm"><i class="fa fa-cut"></i></a>';
          echo '<a class="btn btn-warning btn-sm" href="'.base_url().'kategori_data/pdf/?id_kategori_data='.$row->id_kategori_data.'" ><i class="fa fa-file-pdf-o"></i></a></td>';
          echo '</tr>';
      }
          echo '<tr>';
          echo '<td valign="top" colspan="4" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="',base_url(),'kategori_data/xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
          echo '</tr>';
          
   }
  public function simpan_kategori_data(){
    $this->form_validation->set_rules('nama_kategori_data', 'nama_kategori_data', 'required');
    $this->form_validation->set_rules('id_skpd', 'id_skpd', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
			{
				$data_input = array(
				'nama_kategori_data' => trim($this->input->post('nama_kategori_data')),
				'id_skpd' => trim($this->input->post('id_skpd')),
				'keterangan' => '',
				'temp' => '',
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'kategori_data';
      $this->Kategori_data_model->save_data($data_input, $table_name);
     }
   }
}