<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data_skpd extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Data_skpd_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'data_skpd/home';
    $this->load->view('back_bone', $data);
   }
   
  public function load_the_option()
	{
		$a = 0;
		echo $this->option_skpd();
	}
  
  private function option_skpd(){
		$w = $this->db->query("SELECT * from skpd where status = 1 order by id_skpd");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_skpd.'"> '.$h->nama_skpd.' </option>';
		}
	}
  
  public function json_all_data_skpd()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_data_skpd    = $this->input->post('urut_data_data_skpd');
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  data_skpd.id_data_skpd,
                  data_skpd.jabatan_penandatangan,
                  data_skpd.nama_penandatangan,
                  data_skpd.pangkat_penandatangan,
                  data_skpd.nip_penandatangan,
                  data_skpd.skpd_nama,
                  data_skpd.skpd_telpn,
                  data_skpd.skpd_alamat,
                  data_skpd.skpd_fax,
                  data_skpd.skpd_kode_pos,
                  data_skpd.skpd_nomor,
                  data_skpd.skpd_website,
                  data_skpd.skpd_email,
                  data_skpd.id_skpd,
                  data_skpd.status
                  ";
    $where      = array(
      'status!=' => 99
    );
    $order_by   = 'data_skpd.'.$urut_data_data_skpd.'';
    echo json_encode($this->Data_skpd_model->json_all_data_skpd($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
   
  public function inaktifkan()
		{
      $where = array(
        'id_data_skpd' => $this->input->post('id_data_skpd')
        );
      $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'data_skpd';
        $this->Data_skpd_model->update_data_data_skpd($data_update, $where, $table_name);
		}
   
  public function aktifkan()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
        'id_data_skpd' => $this->input->post('id_data_skpd')
        );
      $data_update = array(
      
        'status' => 1
                
        );
      $table_name  = 'data_skpd';
      $this->Data_skpd_model->update_data_data_skpd($data_update, $where, $table_name);
      
      $where = array(
        'id_data_skpd !=' => $this->input->post('id_data_skpd')
        );
      $data_update = array(
      
        'status' => 0
                
        );
      $table_name  = 'data_skpd';
      $this->Data_skpd_model->update_data_data_skpd($data_update, $where, $table_name);
      
		}
   
  public function cek_tema()
		{
      $web=$this->uut->namadomain(base_url());
      $where = array(
							'status' => 1
							);
      $this->db->where($where);
      $query = $this->db->get('skpd');
      foreach ($query->result() as $row)
        {
        $where2 = array(
          'id_skpd' => $row->id_skpd
          );
        $this->db->from('data_skpd');
        $this->db->where($where2);
        $a = $this->db->count_all_results();
        if($a == 0){
          
          $data_input = array(
      
            'id_skpd' => $row->id_skpd,
            'skpd_nama' => $row->nama_skpd,
            'status' => 1
                    
            );
          $table_name = 'data_skpd';
          $this->Data_skpd_model->simpan_data_skpd($data_input, $table_name);
          }
        }
        echo 'OK';
		}
    
   public function get_by_id()
		{
      $where    = array(
        'id_data_skpd' => $this->input->post('id_data_skpd')
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_data_skpd');
      $result = $this->db->get('data_skpd');
      echo json_encode($result->result_array());
		}
    
  public function update_data_skpd()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('skpd_nama', 'skpd_nama', 'required');
		$this->form_validation->set_rules('skpd_website', 'skpd_website', 'required');
		$this->form_validation->set_rules('id_skpd', 'id_skpd', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
				'jabatan_penandatangan' => trim($this->input->post('jabatan_penandatangan')),
				'nama_penandatangan' => trim($this->input->post('nama_penandatangan')),
				'pangkat_penandatangan' => trim($this->input->post('pangkat_penandatangan')),
				'nip_penandatangan' => trim($this->input->post('nip_penandatangan')),
				'parent' => trim($this->input->post('parent')),
				'skpd_fax' => trim(str_replace('.', '', str_replace(',', '', $this->input->post('skpd_fax')))),
        'temp' => trim($this->input->post('temp')),
        'skpd_nama' => trim($this->input->post('skpd_nama')),
        'skpd_telpn' => trim($this->input->post('skpd_telpn')),
        'skpd_alamat' => trim($this->input->post('skpd_alamat')),
        'skpd_kode_pos' => trim($this->input->post('skpd_kode_pos')),
        'skpd_nomor' => trim($this->input->post('skpd_nomor')),
        'skpd_website' => trim($this->input->post('skpd_website')),
        'skpd_email' => trim($this->input->post('skpd_email')),
        'id_skpd' => trim($this->input->post('id_skpd'))
								
				);
      $table_name  = 'data_skpd';
      $where       = array(
        'data_skpd.id_data_skpd' => trim($this->input->post('id_data_skpd')),
        'data_skpd.domain' => $web
			);
      $this->Posting_model->update_data_data_skpd($data_update, $where, $table_name);
      echo 1;
     }
   }
  public function simpan_data_skpd(){
    $this->form_validation->set_rules('nama_skpd', 'nama_skpd', 'required');
    $this->form_validation->set_rules('id_skpd', 'id_skpd', 'required');
    $this->form_validation->set_rules('skpd_website', 'skpd_website', 'required');
    if ($this->form_validation->run() == FALSE){
      echo 0;
			}
    else{
				$data_input = array(
				'jabatan_penandatangan' => trim($this->input->post('jabatan_penandatangan')),
				'nama_penandatangan' => trim($this->input->post('nama_penandatangan')),
				'pangkat_penandatangan' => trim($this->input->post('pangkat_penandatangan')),
				'nip_penandatangan' => trim($this->input->post('nip_penandatangan')),
				'skpd_fax' => trim($this->input->post('skpd_fax')),
				'skpd_telpn' => trim($this->input->post('skpd_telpn')),
				'skpd_alamat' => trim($this->input->post('skpd_alamat')),
				'skpd_kode_pos' => trim($this->input->post('skpd_kode_pos')),
				'skpd_nomor' => trim($this->input->post('skpd_nomor')),
				'nama_skpd' => trim($this->input->post('nama_skpd')),
				'skpd_website' => trim($this->input->post('skpd_website')),
				'skpd_email' => trim($this->input->post('skpd_email')),
				'id_skpd' => trim($this->input->post('id_skpd')),
				'keterangan' => '',
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1

				);
      $table_name = 'data_skpd';
      $this->Data_skpd_model->save_data($data_input, $table_name);
		}
	}
 }