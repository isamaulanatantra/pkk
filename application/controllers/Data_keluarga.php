<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data_keluarga extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Data_keluarga_model');
   }
   
  public function index(){
    $data['main_view'] = 'data_keluarga/home';
    $this->load->view('tes', $data);
   }
  public function cetak_data_keluarga(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'data_keluarga/cetak_data_keluarga';
    $this->load->view('print', $data);
   }
  public function cetak_semua_data_keluarga(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'data_keluarga/cetak_semua_data_keluarga';
    $this->load->view('print', $data);
   }
  public function cetak_data_keluarga_pekarangan(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'data_keluarga/cetak_data_keluarga_pekarangan';
    $this->load->view('print', $data);
   }
  public function cetak(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'data_keluarga/cetak';
    $this->load->view('print', $data);
   }
	public function json_all_data_keluarga(){
    $web=$this->uut->namadomain(base_url());
		$table = 'data_keluarga';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
    ( select (dusun.nama_dusun) from dusun where dusun.id_dusun=data_keluarga.id_dusun limit 1) as nama_dusun,
    ( select (desa.nama_desa) from desa where desa.id_desa=data_keluarga.id_desa limit 1) as nama_desa,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=data_keluarga.id_kecamatan limit 1) as nama_kecamatan
    ";
    if($web=='demoopd.wonosobokab.go.id'){
      $where = array(
        'data_keluarga.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where = array(
        'data_keluarga.status !=' => 99
        );
    }else{
      $where = array(
        'data_keluarga.status !=' => 99,
        'data_keluarga.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        $query_total_anggota_keluarga = $this->db->query("SELECT * FROM anggota_keluarga WHERE id_data_keluarga = ".$row->id_data_keluarga." AND status!=99");
        $query_total_balita = $this->db->query("SELECT * FROM balita WHERE id_data_keluarga = ".$row->id_data_keluarga." AND status!=99");
				echo '<tr id_data_keluarga="'.$row->id_data_keluarga.'" id="'.$row->id_data_keluarga.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$row->nama_kepala_rumah_tangga.'</td>';
				echo '<td valign="top">'.$row->jumlah_kartu_keluarga.'</td>';
				echo '<td valign="top">'.$row->jumlah_anggota_keluarga.'</td>';
				echo '<td valign="top">'.$row->kriteria_rumah.'</td>';
				echo '<td valign="top">'.$row->sumber_air_keluarga.'</td>';
				echo '<td valign="top">'.$row->makanan_pokok.'</td>';
				echo '<td valign="top">'.$row->kegiatan_up2k.'</td>';
				echo '<td valign="top">
				<a class="badge bg-success btn-sm" href="'.base_url().'anggota_keluarga/?id_data_keluarga='.$row->id_data_keluarga.'" ><i class="fa fa-user-plus"></i> <small class="label pull-right bg-red">'.$query_total_anggota_keluarga->num_rows().'</small> Anggota Keluarga</a>
				<a class="badge bg-success btn-sm" href="'.base_url().'balita/?id_data_keluarga='.$row->id_data_keluarga.'" ><small class="label pull-right bg-red">'.$query_total_balita->num_rows().'</small>  Balita </a>
				<a class="badge bg-warning btn-sm" href="'.base_url().'data_keluarga/cetak/?id_data_keluarga='.$row->id_data_keluarga.'" ><i class="fa fa-print"></i> Cetak</a>
				<a href="#tab_form_data_keluarga" data-toggle="tab" class="update_id_data_keluarga badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax_data_keluarga" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			echo '
      <tr>
        <td valign="top" colspan="10" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'data_keluarga/cetak_data_keluarga/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Keluarga</a>
        </td>
      </tr>
      ';
          
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='demoopd.wonosobokab.go.id'){
      $where0 = array(
        'data_keluarga.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where0 = array(
        'data_keluarga.status !=' => 99
        );
    }else{
      $where0 = array(
        'data_keluarga.status !=' => 99,
        'data_keluarga.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('data_keluarga');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_data_keluarga()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('dasa_wisma', 'dasa_wisma', 'required');
		$this->form_validation->set_rules('nama_kepala_rumah_tangga', 'nama_kepala_rumah_tangga', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => trim($this->input->post('id_propinsi')),
        'id_kabupaten' => trim($this->input->post('id_kabupaten')),
        'id_kecamatan' => trim($this->input->post('id_kecamatan')),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'rw' => trim($this->input->post('rw')),
        'dasa_wisma' => trim($this->input->post('dasa_wisma')),
        'nama_kepala_rumah_tangga' => trim($this->input->post('nama_kepala_rumah_tangga')),
        'jumlah_anggota_keluarga' => trim($this->input->post('jumlah_anggota_keluarga')),
        'jumlah_anggota_keluarga_laki_laki' => trim($this->input->post('jumlah_anggota_keluarga_laki_laki')),
        'jumlah_anggota_keluarga_perempuan' => trim($this->input->post('jumlah_anggota_keluarga_perempuan')),
        'jumlah_kartu_keluarga' => trim($this->input->post('jumlah_kartu_keluarga')),
        'jumlah_balita' => trim($this->input->post('jumlah_balita')),
        'jumlah_pus' => trim($this->input->post('jumlah_pus')),
        'jumlah_wus' => trim($this->input->post('jumlah_wus')),
        'jumlah_buta' => trim($this->input->post('jumlah_buta')),
        'jumlah_ibu_hamil' => trim($this->input->post('jumlah_ibu_hamil')),
        'jumlah_ibu_menyusui' => trim($this->input->post('jumlah_ibu_menyusui')),
        'jumlah_lansia' => trim($this->input->post('jumlah_lansia')),
        'makanan_pokok' => trim($this->input->post('makanan_pokok')),
        'jenis_makanan_pokok' => trim($this->input->post('jenis_makanan_pokok')),
        'mempunyai_jamban_keluarga' => trim($this->input->post('mempunyai_jamban_keluarga')),
        'jumlah_jamban_keluarga' => trim($this->input->post('jumlah_jamban_keluarga')),
        'sumber_air_keluarga' => trim($this->input->post('sumber_air_keluarga')),
        'memiliki_pembuangan_sampah' => trim($this->input->post('memiliki_pembuangan_sampah')),
        'mempunyai_saluran_pembuangan_air_limbah' => trim($this->input->post('mempunyai_saluran_pembuangan_air_limbah')),
        'menempel_stiker_p4k' => trim($this->input->post('menempel_stiker_p4k')),
        'kriteria_rumah' => trim($this->input->post('kriteria_rumah')),
        'kegiatan_up2k' => trim($this->input->post('kegiatan_up2k')),
        'jenis_usaha' => trim($this->input->post('jenis_usaha')),
        'aktifitas_kegiatan_usaha_kesehatan_lingkungan' => trim($this->input->post('aktifitas_kegiatan_usaha_kesehatan_lingkungan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'data_keluarga';
      $id         = $this->Data_keluarga_model->simpan_data_keluarga($data_input, $table_name);
      echo $id;
			$table_name  = 'anggota_keluarga';
      $where       = array(
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_data_keluarga' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
  public function update_data_keluarga()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('dasa_wisma', 'dasa_wisma', 'required');
		$this->form_validation->set_rules('nama_kepala_rumah_tangga', 'nama_kepala_rumah_tangga', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => trim($this->input->post('id_propinsi')),
        'id_kabupaten' => trim($this->input->post('id_kabupaten')),
        'id_kecamatan' => trim($this->input->post('id_kecamatan')),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'rw' => trim($this->input->post('rw')),
        'dasa_wisma' => trim($this->input->post('dasa_wisma')),
        'nama_kepala_rumah_tangga' => trim($this->input->post('nama_kepala_rumah_tangga')),
        'jumlah_anggota_keluarga' => trim($this->input->post('jumlah_anggota_keluarga')),
        'jumlah_anggota_keluarga_laki_laki' => trim($this->input->post('jumlah_anggota_keluarga_laki_laki')),
        'jumlah_anggota_keluarga_perempuan' => trim($this->input->post('jumlah_anggota_keluarga_perempuan')),
        'jumlah_kartu_keluarga' => trim($this->input->post('jumlah_kartu_keluarga')),
        'jumlah_balita' => trim($this->input->post('jumlah_balita')),
        'jumlah_pus' => trim($this->input->post('jumlah_pus')),
        'jumlah_wus' => trim($this->input->post('jumlah_wus')),
        'jumlah_buta' => trim($this->input->post('jumlah_buta')),
        'jumlah_ibu_hamil' => trim($this->input->post('jumlah_ibu_hamil')),
        'jumlah_ibu_menyusui' => trim($this->input->post('jumlah_ibu_menyusui')),
        'jumlah_lansia' => trim($this->input->post('jumlah_lansia')),
        'makanan_pokok' => trim($this->input->post('makanan_pokok')),
        'jenis_makanan_pokok' => trim($this->input->post('jenis_makanan_pokok')),
        'mempunyai_jamban_keluarga' => trim($this->input->post('mempunyai_jamban_keluarga')),
        'jumlah_jamban_keluarga' => trim($this->input->post('jumlah_jamban_keluarga')),
        'sumber_air_keluarga' => trim($this->input->post('sumber_air_keluarga')),
        'memiliki_pembuangan_sampah' => trim($this->input->post('memiliki_pembuangan_sampah')),
        'mempunyai_saluran_pembuangan_air_limbah' => trim($this->input->post('mempunyai_saluran_pembuangan_air_limbah')),
        'menempel_stiker_p4k' => trim($this->input->post('menempel_stiker_p4k')),
        'kriteria_rumah' => trim($this->input->post('kriteria_rumah')),
        'kegiatan_up2k' => trim($this->input->post('kegiatan_up2k')),
        'jenis_usaha' => trim($this->input->post('jenis_usaha')),
        'aktifitas_kegiatan_usaha_kesehatan_lingkungan' => trim($this->input->post('aktifitas_kegiatan_usaha_kesehatan_lingkungan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'data_keluarga';
      $where       = array(
        'data_keluarga.id_data_keluarga' => trim($this->input->post('id_data_keluarga')),
        'data_keluarga.domain' => $web
			);
      $this->Data_keluarga_model->update_data_data_keluarga($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      if($web=='demoopd.wonosobokab.go.id'){
        $where = array(
        'data_keluarga.id_data_keluarga' => $this->input->post('id_data_keluarga'),
          );
      }elseif($web=='pkk.wonosobokab.go.id'){
        $where = array(
        'data_keluarga.id_data_keluarga' => $this->input->post('id_data_keluarga'),
          );
      }else{
        $where = array(
        'data_keluarga.id_data_keluarga' => $this->input->post('id_data_keluarga'),
          'data_keluarga.id_kecamatan' => $this->session->userdata('id_kecamatan')
          );
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_data_keluarga');
      $result = $this->db->get('data_keluarga');
      echo json_encode($result->result_array());
		}
   
   public function total_data_keluarga()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('data_keluarga');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_data_keluarga = $this->input->post('id_data_keluarga');
      $where = array(
        'id_data_keluarga' => $id_data_keluarga,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('data_keluarga');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'data_keluarga';
        $this->Data_keluarga_model->update_data_data_keluarga($data_update, $where, $table_name);
        echo 1;
        }
		}
  public function option_desa_by_id_kecamatan(){
		$id_kecamatan = $this->input->post('id_kecamatan');
		$w = $this->db->query("
		SELECT *
		from desa
		where desa.status = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  public function option_dusun_by_id_desa(){
		$id_desa = $this->input->post('id_desa');
		$w = $this->db->query("
		SELECT *
		from dusun
		where dusun.status = 1
		and dusun.id_desa = ".$id_desa."
		order by id_dusun");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_dusun.'">'.$h->nama_dusun.'</option>';
		}
	}
  public function option_propinsi(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_propinsi="";
		}
		else{
			//$where_id_propinsi="and propinsi.id_propinsi=".$this->session->userdata('id_propinsi')."";
			$where_id_propinsi="";
		}
		$w = $this->db->query("
		SELECT *
		from propinsi
		where propinsi.status = 1
    ".$where_id_propinsi."
		order by propinsi.id_propinsi
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_propinsi.'">'.$h->nama_propinsi.'</option>';
		}
	}
  public function id_kabupaten_by_id_propinsi(){
		$id_propinsi = $this->input->post('id_propinsi');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kabupaten
		where kabupaten.status = 1
    and kabupaten.id_propinsi=".$id_propinsi."
		order by kabupaten.id_kabupaten
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kabupaten.'">'.$h->nama_kabupaten.'</option>';
		}
	}
  public function id_kecamatan_by_id_kabupaten(){
		$id_kabupaten = $this->input->post('id_kabupaten');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kecamatan
		where kecamatan.status = 1
		and kecamatan.id_kabupaten=".$id_kabupaten."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  public function id_desa_by_id_kecamatan(){
		$id_kecamatan = $this->input->post('id_kecamatan');
		$w = $this->db->query("
		SELECT *
		from desa
		where desa.status = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  public function id_dusun_by_id_desa(){
		$id_desa = $this->input->post('id_desa');
		$w = $this->db->query("
		SELECT *
		from dusun
		where dusun.status = 1
		and dusun.id_desa = ".$id_desa."
		order by id_dusun");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_dusun.'">'.$h->nama_dusun.'</option>';
		}
	}
  public function option_kecamatan(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_kecamatan="";
		}
		else{
			$where_id_kecamatan="and data_kecamatan.id_kecamatan=".$this->session->userdata('id_kecamatan')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_kecamatan, kecamatan
		where data_kecamatan.status = 1
		and kecamatan.id_kecamatan=data_kecamatan.id_kecamatan
    ".$where_id_kecamatan."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  public function status_dalam_keluarga(){
		$w = $this->db->query("
		SELECT *
		from status_dalam_keluarga
		where status_dalam_keluarga.status = 1
		order by status_dalam_keluarga.id_status_dalam_keluarga
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_dalam_keluarga.'">'.$h->nama_status_dalam_keluarga.'</option>';
		}
	}
  public function pekerjaan(){
		$w = $this->db->query("
		SELECT *
		from pekerjaan
		where pekerjaan.status = 1
		order by pekerjaan.id_pekerjaan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pekerjaan.'">'.$h->nama_pekerjaan.'</option>';
		}
	}
  public function agama(){
		$w = $this->db->query("
		SELECT *
		from agama
		where agama.status = 1
		order by agama.id_agama
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_agama.'">'.$h->nama_agama.'</option>';
		}
	}
  public function status_pernikahan(){
		$w = $this->db->query("
		SELECT *
		from status_pernikahan
		where status_pernikahan.status = 1
		order by status_pernikahan.id_status_pernikahan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_pernikahan.'">'.$h->nama_status_pernikahan.'</option>';
		}
	}
  public function pendidikan(){
		$w = $this->db->query("
		SELECT *
		from pendidikan
		where pendidikan.status = 1
		order by pendidikan.id_pendidikan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pendidikan.'">'.$h->nama_pendidikan.'</option>';
		}
	}
 }