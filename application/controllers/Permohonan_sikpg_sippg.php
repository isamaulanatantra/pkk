<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Permohonan_sikpg_sippg extends CI_Controller
 {
    
  function __construct()
   {
    parent::__construct();
    $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Permohonan_sikpg_sippg_model');
   }
  
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'permohonan_sikpg_sippg/home';
    $this->load->view('back_bone', $data);
   }
  
  function load_the_option()
	{
		$a = 0;
		echo $this->option_permohonan_sikpg_sippg(0,$h="", $a);
	}
  
  private function option_permohonan_sikpg_sippg($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from permohonan_sikpg_sippg where parent='".$parent."' and domain='".$web."' and status = 1 order by urut");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_permohonan_sikpg_sippg.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_permohonan_sikpg_sippg.'';
			}
			else{
			$hasil .= ''.$h->judul_permohonan_sikpg_sippg.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_permohonan_sikpg_sippg($h->id_permohonan_sikpg_sippg,$hasil, $a);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
  function load_the_option_by_posisi()
	{
    $web=$this->uut->namadomain(base_url());
    $posisi = trim($this->input->post('posisi'));
		$a = 0;
		echo $this->option_permohonan_sikpg_sippg_by_posisi(0,$h="", $a, $posisi);
	}
  
  private function option_permohonan_sikpg_sippg_by_posisi($parent=0,$hasil, $a, $posisi){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from permohonan_sikpg_sippg 
                          where parent='".$parent."' 
                          and status = 1 
                          and posisi='".$posisi."'
                          and domain='".$web."'
                          order by urut");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_permohonan_sikpg_sippg.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_permohonan_sikpg_sippg.'';
			}
			else{
			$hasil .= ''.$h->judul_permohonan_sikpg_sippg.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_permohonan_sikpg_sippg_by_posisi($h->id_permohonan_sikpg_sippg,$hasil, $a, $posisi);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
  public function json_all_permohonan_sikpg_sippg()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_permohonan_sikpg_sippg    = $this->input->post('urut_data_permohonan_sikpg_sippg');
    $start      = ($halaman - 1) * $limit;
    $fields     = "*";
    $where      = array(
      'permohonan_sikpg_sippg.status !=' => 99,
      'domain' => $web
    );
    $order_by   = 'permohonan_sikpg_sippg.'.$urut_data_permohonan_sikpg_sippg.'';
    echo json_encode($this->Permohonan_sikpg_sippg_model->json_all_permohonan_sikpg_sippg($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
   
  public function simpan_permohonan_sikpg_sippg()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_permohonan_sikpg_sippg', 'judul_permohonan_sikpg_sippg', 'required');
		$this->form_validation->set_rules('highlight', 'highlight', 'required');
		$this->form_validation->set_rules('tampil_menu', 'tampil_menu', 'required');
		$this->form_validation->set_rules('tampil_menu_atas', 'tampil_menu_atas', 'required');
		$this->form_validation->set_rules('parent', 'parent', 'required');
		$this->form_validation->set_rules('urut', 'urut', 'required');
		$this->form_validation->set_rules('posisi', 'posisi', 'required');
		$this->form_validation->set_rules('icon', 'icon', 'required');
		$this->form_validation->set_rules('kata_kunci', 'kata_kunci', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('isi_permohonan_sikpg_sippg', 'isi_permohonan_sikpg_sippg', 'required');
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'judul_permohonan_sikpg_sippg' => trim($this->input->post('judul_permohonan_sikpg_sippg')),
				'highlight' => trim($this->input->post('highlight')),
				'tampil_menu' => trim($this->input->post('tampil_menu')),
				'tampil_menu_atas' => trim($this->input->post('tampil_menu_atas')),
				'parent' => trim($this->input->post('parent')),
				'kata_kunci' => trim(str_replace('.', '', str_replace(',', '', $this->input->post('kata_kunci')))),
        'temp' => trim($this->input->post('temp')),
        'urut' => trim($this->input->post('urut')),
        'posisi' => trim($this->input->post('posisi')),
        'icon' => trim($this->input->post('icon')),
        'isi_permohonan_sikpg_sippg' => trim($this->input->post('isi_permohonan_sikpg_sippg')),
        'keterangan' => trim($this->input->post('keterangan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'permohonan_sikpg_sippg';
      $id         = $this->Permohonan_sikpg_sippg_model->simpan_permohonan_sikpg_sippg($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'permohonan_sikpg_sippg',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
   
  public function update_permohonan_sikpg_sippg()
   {
    $web=$this->uut->namadomain(base_url());
    $this->form_validation->set_rules('judul_permohonan_sikpg_sippg', 'judul_permohonan_sikpg_sippg', 'required');
    $this->form_validation->set_rules('highlight', 'highlight', 'required');
    $this->form_validation->set_rules('tampil_menu', 'tampil_menu', 'required');
    $this->form_validation->set_rules('tampil_menu_atas', 'tampil_menu_atas', 'required');
		$this->form_validation->set_rules('urut', 'urut', 'required');
		$this->form_validation->set_rules('posisi', 'posisi', 'required');
		$this->form_validation->set_rules('icon', 'icon', 'required');
		$this->form_validation->set_rules('kata_kunci', 'kata_kunci', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
				'judul_permohonan_sikpg_sippg' => trim($this->input->post('judul_permohonan_sikpg_sippg')),
				'highlight' => trim($this->input->post('highlight')),
				'tampil_menu' => trim($this->input->post('tampil_menu')),
				'tampil_menu_atas' => trim($this->input->post('tampil_menu_atas')),
				'parent' => trim($this->input->post('parent')),
				'kata_kunci' => trim(str_replace('.', '', str_replace(',', '', $this->input->post('kata_kunci')))),
        'temp' => trim($this->input->post('temp')),
        'urut' => trim($this->input->post('urut')),
        'posisi' => trim($this->input->post('posisi')),
        'icon' => trim($this->input->post('icon')),
        'isi_permohonan_sikpg_sippg' => trim($this->input->post('isi_permohonan_sikpg_sippg')),
        'keterangan' => trim($this->input->post('keterangan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'permohonan_sikpg_sippg';
      $where       = array(
        'permohonan_sikpg_sippg.id_permohonan_sikpg_sippg' => trim($this->input->post('id_permohonan_sikpg_sippg')),
        'permohonan_sikpg_sippg.domain' => $web
			);
      $this->Permohonan_sikpg_sippg_model->update_data_permohonan_sikpg_sippg($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'id_permohonan_sikpg_sippg' => $this->input->post('id_permohonan_sikpg_sippg'),
        'permohonan_sikpg_sippg.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_permohonan_sikpg_sippg');
      $result = $this->db->get('permohonan_sikpg_sippg');
      echo json_encode($result->result_array());
		}
   
   public function total_permohonan_sikpg_sippg()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $this->db->from('permohonan_sikpg_sippg');
      $where    = array(
        'status' => 1,
        'domain' => $web
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_permohonan_sikpg_sippg = $this->input->post('id_permohonan_sikpg_sippg');
      $where = array(
        'id_permohonan_sikpg_sippg' => $id_permohonan_sikpg_sippg,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('permohonan_sikpg_sippg');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'permohonan_sikpg_sippg';
        $this->Permohonan_sikpg_sippg_model->update_data_permohonan_sikpg_sippg($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function restore()
		{
      $web=$this->uut->namadomain(base_url());
			$id_permohonan_sikpg_sippg = $this->input->post('id_permohonan_sikpg_sippg');
      $where = array(
        'id_permohonan_sikpg_sippg' => $id_permohonan_sikpg_sippg,
        'domain' => $web
        );
      $this->db->from('permohonan_sikpg_sippg');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'permohonan_sikpg_sippg';
        $this->Permohonan_sikpg_sippg_model->update_data_permohonan_sikpg_sippg($data_update, $where, $table_name);
        echo 1;
        }
		}
    
 }