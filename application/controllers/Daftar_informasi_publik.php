<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Daftar_informasi_publik extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Daftar_informasi_publik_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'daftar_informasi_publik/home';
    $this->load->view('tes', $data);
   }
  public function json_all_data_informasi(){
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_daftar_informasi_publik    = $this->input->post('urut_data_daftar_informasi_publik');
    $opd_domain    = $this->input->post('opd_domain');
    $kategori_informasi_publik    = $this->input->post('kategori_informasi_publik');
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  *,
									skpd.nama_skpd
                  ";
			if($kategori_informasi_publik=='pilih_kategori_informasi'){
				$where      = array(
				'posting.parent !=' => 0,
				'posting.status' => 1,
				'posting.domain' => $web,
				);
			}
			else{
				$where      = array(
				'posting.parent !=' => 0,
				'posting.status' => 1,
				'posting.'.$kategori_informasi_publik.'' => 1,
				'posting.domain' => $web,
				);
			}
    $order_by   = 'posting.'.$urut_data_daftar_informasi_publik.'';
    echo json_encode($this->Daftar_informasi_publik_model->json_all_daftar_informasi_publik($where, $limit, $start, $fields, $order_by, $kata_kunci));
	}
   
  public function json_all_daftar_informasi_publik(){
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_daftar_informasi_publik    = $this->input->post('urut_data_daftar_informasi_publik');
    $kategori_informasi_publik    = $this->input->post('kategori_informasi_publik');
    $opd_domain    = $this->input->post('opd_domain');
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  *,
									skpd.nama_skpd
                  ";
			if($kategori_informasi_publik=='pilih_kategori_informasi'){
				$where      = array(
				'posting.parent !=' => 0,
				'posting.status' => 1,
				'posting.domain' => $web,
				);
			}
			else{
				$where      = array(
				'posting.parent !=' => 0,
				'posting.status' => 1,
				'posting.'.$kategori_informasi_publik.'' => 1,
				'posting.domain' => $web,
				);
			}
    $order_by   = 'posting.'.$urut_data_daftar_informasi_publik.'';
    echo json_encode($this->Daftar_informasi_publik_model->json_all_daftar_informasi_publik($where, $limit, $start, $fields, $order_by, $kata_kunci));
	}
   
  function opd_domain()
	{
		$a = 0;
		echo $this->option_opd($h="", $a);
	}
  private function option_opd($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT *, data_skpd.skpd_website from skpd, data_skpd where skpd.status = 1 and data_skpd.id_skpd=skpd.id_skpd and data_skpd.status=1 order by kode_skpd");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->skpd_website.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->nama_skpd.'';
			}
			else{
			$hasil .= ''.$h->nama_skpd.'';
			}
			$hasil .= '</option>';
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
	
  public function inaktif_informasi_berkala(){
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'informasi_berkala' => 0
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Daftar_informasi_publik_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktif_informasi_berkala(){
      $web=$this->uut->namadomain(base_url());
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'informasi_berkala' => 1
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Daftar_informasi_publik_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
		
  public function inaktif_informasi_serta_merta(){
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'informasi_serta_merta' => 0
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Daftar_informasi_publik_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktif_informasi_serta_merta(){
      $web=$this->uut->namadomain(base_url());
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'informasi_serta_merta' => 1
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Daftar_informasi_publik_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
		
  public function inaktif_informasi_setiap_saat(){
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'informasi_setiap_saat' => 0
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Daftar_informasi_publik_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktif_informasi_setiap_saat(){
      $web=$this->uut->namadomain(base_url());
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'informasi_setiap_saat' => 1
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Daftar_informasi_publik_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
		
  public function inaktif_informasi_dikecualikan(){
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'informasi_dikecualikan' => 0
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Daftar_informasi_publik_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktif_informasi_dikecualikan(){
      $web=$this->uut->namadomain(base_url());
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_posting' => $this->input->post('id_posting')
        );
      $this->db->from('posting');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'informasi_dikecualikan' => 1
                  
          );
        $table_name  = 'posting';
        if( $cek <> '' ){
          $this->Daftar_informasi_publik_model->update_data_posting($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
	public function json_all_posting_get(){
    $web=$this->uut->namadomain(base_url());
    if($web=='disparbud.wonosobokab.go.id' or $web=='dikpora.wonosobokab.go.id'){
      $url_baca='post/detail';
      $url_baca_list='post/galeri';
    }else{
      $url_baca='postings/details';
      $url_baca_list='postings/categoris';
    }
		$table = 'posting';
    $page = $this->input->post('page');
    $limit = $this->input->post('limit');
    $keyword = $this->input->post('keyword');
    $order_by = $this->input->post('orderby');
    $klasifikasi_informasi_publik = $this->input->post('klasifikasi_informasi_publik');
    $start = ($page - 1) * $limit;
    $fields = "
    *
    ";
    if(!$klasifikasi_informasi_publik){
      if($web=='ppid.wonosobokab.go.id'){
        $where = array(
          'posting.status' => 1
          );
      }else{
        $where = array(
          'posting.status' => 1,
          'posting.domain' => $web
          );
      }
    }else{
      if($web=='ppid.wonosobokab.go.id'){
        $where = array(
          'posting.status' => 1,
          'posting.'.$klasifikasi_informasi_publik.'' => 1
          );
      }else{
        $where = array(
          'posting.status' => 1,
          'posting.domain' => $web,
          'posting.'.$klasifikasi_informasi_publik.'' => 1
          );
      }
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $h)
      {
				$urut=$urut+1;
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
        $yummy   = array("_","","","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
        $pencacah = explode(".",$h->domain);
        $created_time = $this->Crud_model->dateBahasaIndo1($h->created_time);
				echo '
        <tr id_posting="'.$h->id_posting.'" id="'.$h->id_posting.'">
          <td><span type="span" class="label label-info">'.$urut.'</span></td>
          <td><a class="dip" href="'.base_url().''.$url_baca.'/'.$h->id_posting.'/'.str_replace(' ', '_', $newphrase ).'.HTML">'.$h->judul_posting.'</a></td>
          <td>
            <span type="span" class="label label-success">Oleh: '.$pencacah[0].'</span>
            ';
              $querypdf = $this->db->query("SELECT file_name from attachment where id_tabel=".$h->id_posting." and file_name LIKE '%.pdf' order by uploaded_time desc limit 1");
              if(($querypdf->num_rows())>0){
              $a = 0;
              foreach ($querypdf->result() as $rowpdf)
                {
                  echo'
                    <a href="'.base_url().'media/upload/'.$rowpdf->file_name.'" target="_blank"><span type="span" class="label label-info"><i class="fa fa-download"></i> download
                  </span></a>';
                }
              }
            echo'
          </td>
        </tr>
        ';
      }
          
	}
  public function total_data_get()
  {
    $web=$this->uut->namadomain(base_url());
    if($web=='disparbud.wonosobokab.go.id' or $web=='dikpora.wonosobokab.go.id'){
      $url_baca='post/detail';
      $url_baca_list='post/galeri';
    }else{
      $url_baca='postings/details';
      $url_baca_list='postings/categoris';
    }
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby = $this->input->post('orderby');
    $klasifikasi_informasi_publik = $this->input->post('klasifikasi_informasi_publik');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if(!$klasifikasi_informasi_publik){
      if($web=='ppid.wonosobokab.go.id'){
        $where0 = array(
          'posting.status' => 1
          );
      }else{
        $where0 = array(
          'posting.status' => 1,
          'posting.domain' => $web
          );
      }
    }else{
      if($web=='ppid.wonosobokab.go.id'){
        $where0 = array(
          'posting.status' => 1,
          'posting.'.$klasifikasi_informasi_publik.'' => 1
          );
      }else{
        $where0 = array(
          'posting.status' => 1,
          'posting.domain' => $web,
          'posting.'.$klasifikasi_informasi_publik.'' => 1
          );
      }
    }
    $this->db->where($where0);
    $query0 = $this->db->get('posting');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_posting_get_utama(){
    $web=$this->uut->namadomain(base_url());
		$table = 'posting';
    $page = $this->input->post('page');
    $limit = $this->input->post('limit');
    $keyword = $this->input->post('keyword');
    $order_by = $this->input->post('orderby');
    $klasifikasi_informasi_publik = $this->input->post('klasifikasi_informasi_publik');
    $start = ($page - 1) * $limit;
    $fields = "
    *
    ";
    if(!$klasifikasi_informasi_publik){
        $where = array(
          'posting.status' => 1,
          'posting.domain' => $web
          );
    }else{
        $where = array(
          'posting.status' => 1,
          'posting.domain' => $web,
          'posting.'.$klasifikasi_informasi_publik.'' => 1
          );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $h)
      {
				$urut=$urut+1;
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
        $yummy   = array("_","","","","","","","","","","","","","","","");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
        $pencacah = explode(".",$h->domain);
        $created_time = $this->Crud_model->dateBahasaIndo1($h->created_time);
				echo '
        <tr id_posting="'.$h->id_posting.'" id="'.$h->id_posting.'">
          <td><span type="span" class="label label-info">'.$urut.'</span></td>
          <td><a class="dip" href="'.base_url().''.$url_baca.'/'.$h->id_posting.'/'.str_replace(' ', '_', $newphrase ).'.HTML">'.$h->judul_posting.'</a></td>
          <td>
            <span type="span" class="label label-success">Oleh: '.$pencacah[0].'</span>
            ';
              $querypdf = $this->db->query("SELECT file_name from attachment where id_tabel=".$h->id_posting." and file_name LIKE '%.pdf' order by uploaded_time desc limit 1");
              if(($querypdf->num_rows())>0){
              $a = 0;
              foreach ($querypdf->result() as $rowpdf)
                {
                  echo'
                    <a href="'.base_url().'media/upload/'.$rowpdf->file_name.'" target="_blank"><span type="span" class="label label-info"><i class="fa fa-download"></i> download
                  </span></a>';
                }
              }
            echo'
          </td>
        </tr>
        ';
      }
          
	}
  public function total_data_get_utama()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby = $this->input->post('orderby');
    $klasifikasi_informasi_publik = $this->input->post('klasifikasi_informasi_publik');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if(!$klasifikasi_informasi_publik){
        $where0 = array(
          'posting.status' => 1,
          'posting.domain' => $web
          );
    }else{
        $where0 = array(
          'posting.status' => 1,
          'posting.domain' => $web,
          'posting.'.$klasifikasi_informasi_publik.'' => 1
          );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('posting');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
}