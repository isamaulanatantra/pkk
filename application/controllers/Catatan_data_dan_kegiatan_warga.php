<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Catatan_data_dan_kegiatan_warga extends CI_Controller 
{
  function __construct(){
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Data_keluarga_model');
    $this->load->model('Anggota_keluarga_model');
  }
  public function index(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'catatan_data_dan_kegiatan_warga/home';
    $this->load->view('tes', $data);
  }
  public function cetak_catatan_data_dan_kegiatan_warga(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'catatan_data_dan_kegiatan_warga/cetak';
    $this->load->view('print', $data);
  }
}