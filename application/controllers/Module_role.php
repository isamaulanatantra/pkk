<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Module_role extends CI_Controller
{
  
  function __construct()
  {
    parent::__construct();
  }
  
  public function index()
  {
    $data['test'] = '';
    $this->load->view('back_end/module_role/content', $data);
  }
  
  public function json()
  {
    $table       = $this->input->post('table');
    $page        = $this->input->post('page');
    $limit       = $this->input->post('limit');
    $keyword     = $this->input->post('keyword');
    $status      = $this->input->post('status');
    $order_by    = $this->input->post('order_by');
    $order_field = $this->input->post('order_field');
    $shorting    = $this->input->post('shorting');
    $start       = ($page - 1) * $limit;
    $fields      = "
            module_role.module_role_id,
            module_role.role_id,
            module_role.module_id,
            module_role.information,
            module_role.created_by,
            module_role.created_time,
            module_role.updated_by,
            module_role.updated_time,
            module_role.deleted_by,
            module_role.deleted_time,
            module_role.status,
            
            role.role_code,
            role.role_name,
            
            module.module_name,
            
            (
            select attachment.file_name from attachment
            where attachment.id_tabel=module_role.module_role_id
            and attachment.table_name='" . $table . "'
            order by attachment.id_attachment DESC
            limit 1
            ) as file
            ";
    $where       = array(
      '' . $table . '.status' => $status
    );
    $this->db->select("$fields");
    if ($keyword == '') {
      $this->db->where("
            " . $table . ".status = " . $status . "
            ");
    } else {
      $this->db->where("
            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'
            AND
            " . $table . ".status = " . $status . "
            ");
    }
    $this->db->join('role', 'role.role_id=module_role.role_id');
    $this->db->join('module', 'module.module_id=module_role.module_id');
    $this->db->order_by($order_by, $shorting);
    $this->db->limit($limit, $start);
    $result = $this->db->get($table);
    echo json_encode($result->result_array());
  }
  
  public function total()
  {
    $table       = $this->input->post('table');
    $page        = $this->input->post('page');
    $limit       = $this->input->post('limit');
    $keyword     = $this->input->post('keyword');
    $status      = $this->input->post('status');
    $order_by    = $this->input->post('order_by');
    $shorting    = $this->input->post('shorting');
    $order_field = $this->input->post('order_field');
    $start       = ($page - 1) * $limit;
    $fields      = "
            module_role.module_role_id,
            module_role.role_id,
            module_role.module_id,
            module_role.information,
            module_role.created_by,
            module_role.created_time,
            module_role.updated_by,
            module_role.updated_time,
            module_role.deleted_by,
            module_role.deleted_time,
            module_role.status
            ";
    $where       = array(
      '' . $table . '.status' => $status
    );
    $this->db->select("$fields");
    if ($keyword == '') {
      $this->db->where("
            " . $table . ".status = " . $status . "
            ");
    } else {
      $this->db->where("
            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'
            AND
            " . $table . ".status = " . $status . "
            ");
    }
    $this->db->join('role', 'role.role_id=module_role.role_id');
    $this->db->join('module', 'module.module_id=module_role.module_id');
    $query = $this->db->get($table);
    $a     = $query->num_rows();
    echo trim(ceil($a / $limit));
  }
  
  public function simpan()
  {
    $this->form_validation->set_rules('role_id', 'role_id', 'required');
    $this->form_validation->set_rules('temp', 'temp', 'required');
    $this->form_validation->set_rules('module_id', 'module_id', 'required');
    $this->form_validation->set_rules('information', 'information', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $fields      = "*";
      $this->db->where("
              role_id='".trim($this->input->post('role_id'))."'
              AND
              module_id='".trim($this->input->post('module_id'))."'
              ");
      $query = $this->db->get('module_role');
      $a     = $query->num_rows();
      if( $a > 0  ){
      echo 0;
      exit;
      }
      $data_input = array(
        'role_id' => trim($this->input->post('role_id')),
        'module_id' => trim($this->input->post('module_id')),
        'information' => trim($this->input->post('information')),
        'temp' => trim($this->input->post('temp')),
        'created_by' => $this->session->userdata('user_id'),
        'created_time' => date('Y-m-d H:i:s'),
        'status' => 1
      );
      $this->db->insert( 'module_role', $data_input );
      $id= $this->db->insert_id();
      echo '' . $id . '';
      $where       = array(
        'table_name' => 'module_role',
        'temp' => trim($this->input->post('temp'))
      );
      $data_update = array(
        'id_tabel' => $id
      );
      $this->db->where($where);
      $this->db->update('attachment', $data_update);
    }
  }
  
  public function update()
  {
    $this->form_validation->set_rules('module_role_id', 'module_role_id', 'required');
    $this->form_validation->set_rules('role_id', 'role_id', 'required');
    $this->form_validation->set_rules('module_id', 'module_id', 'required');
    $this->form_validation->set_rules('information', 'information', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'role_id' => trim($this->input->post('role_id')),
        'module_id' => trim($this->input->post('module_id')),
        'information' => trim($this->input->post('information')),
        'updated_by' => $this->session->userdata('user_id'),
        'updated_time' => date('Y-m-d H:i:s')
      );
      $where       = array(
        'module_role_id' => trim($this->input->post('module_role_id'))
      );
      $this->db->where($where);
      $this->db->update('module_role', $data_update);
      echo 1;
    }
  }
  
  public function get_by_id()
  {
    $module_role_id = $this->input->post('module_role_id');
    $where   = array(
      'module_role.module_role_id' => $module_role_id
    );
    $this->db->select("
      module_role.module_role_id,
      module_role.role_id,
      module_role.module_id,
      module_role.information,
      module_role.created_by,
      module_role.created_time,
      module_role.updated_by,
      module_role.updated_time,
      module_role.deleted_by,
      module_role.deleted_time,
      module_role.status,
      
      role.role_code,
      role.role_name,
      
      module.module_name
      ");    	
    $this->db->where($where);
    $this->db->join('role', 'role.role_id=module_role.role_id');
    $this->db->join('module', 'module.module_id=module_role.module_id');
		$result = $this->db->get('module_role');
    echo json_encode($result->result_array());
  }
  
  public function delete_by_id()
  {
    $this->form_validation->set_rules('module_role_id', 'module_role_id', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 99
      );
      $where       = array(
        'module_role_id' => trim($this->input->post('module_role_id'))
      );
      $table_name  = 'module_role';
      $this->db->where($where);
      $this->db->update('module_role', $data_update);
      echo 1;
    }
  }
  
  public function restore_by_id()
  {
    $this->form_validation->set_rules('module_role_id', 'module_role_id', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 1
      );
      $where       = array(
        'module_role_id' => trim($this->input->post('module_role_id'))
      );
      $table_name  = 'module_role';
      $this->db->where($where);
      $this->db->update($table_name, $data_update);
      echo 1;
    }
  }
  
  
  public function module_by_role_id()
  {
    $fields      = "
            module_role.module_role_id,
            module_role.role_id,
            module_role.module_id,
            module_role.information,
            module_role.created_by,
            module_role.created_time,
            module_role.updated_by,
            module_role.updated_time,
            module_role.deleted_by,
            module_role.deleted_time,
            module_role.status,
            
            role.role_code,
            role.role_name,
            
            module.module_name,
            module.module_code
            
            ";
    $where       = array(
      'module_role.role_id' => $this->input->post('role_id')
    );
    $this->db->select("$fields");
    $this->db->where($where);
    $this->db->join('role', 'role.role_id=module_role.role_id');
    $this->db->join('module', 'module.module_id=module_role.module_id');
    $this->db->order_by('module_role.role_id');
    $this->db->order_by('module_role.module_id');
    $this->db->order_by('module.module_name');
    $result = $this->db->get('module_role');
    echo json_encode($result->result_array());
  }
  
}