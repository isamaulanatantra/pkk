<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Warga_pkk extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Warga_pkk_model');
   }
   
  public function index(){
    $data['total'] = 10;
    $data['main_view'] = 'warga_pkk/home';
    $this->load->view('tes', $data);
   }
  public function cetak_warga_pkk(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'warga_pkk/cetak_warga_pkk';
    $this->load->view('print', $data);
   }
  public function cetak_semua_warga_pkk(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'warga_pkk/cetak_semua_warga_pkk';
    $this->load->view('print', $data);
   }
  public function cetak_warga_pkk_pekarangan(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'warga_pkk/cetak_warga_pkk_pekarangan';
    $this->load->view('print', $data);
   }
  public function cetak(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'warga_pkk/cetak';
    $this->load->view('print', $data);
   }
	public function json_all_warga_pkk(){
    $web=$this->uut->namadomain(base_url());
		$table = 'warga_pkk';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
    ( select (dusun.nama_dusun) from dusun where dusun.id_dusun=warga_pkk.id_dusun limit 1) as nama_dusun,
    ( select (desa.nama_desa) from desa where desa.id_desa=warga_pkk.id_desa limit 1) as nama_desa,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=warga_pkk.id_kecamatan limit 1) as nama_kecamatan
    ";
    if($web=='demoopd.wonosobokab.go.id'){
      $where = array(
        'warga_pkk.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where = array(
        'warga_pkk.status !=' => 99
        );
    }else{
      $where = array(
        'warga_pkk.status !=' => 99,
        'warga_pkk.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_warga_pkk="'.$row->id_warga_pkk.'" id="'.$row->id_warga_pkk.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$row->tahun_pendaftaran.'</td>';
				echo '<td valign="top">'.$row->tanggal_masuk_kader.'</td>';
				echo '<td valign="top">'.$row->nama.'</td>';
				echo '<td valign="top">'.$row->nomor_registrasi.'</td>';
				echo '<td valign="top">';if($row->nama_dusun!=''){echo'Dusun. '.$row->nama_dusun.' ';}if($row->rt!=''){echo'RT.'.$row->rt.'';}if($row->rw!=''){echo'RW.'.$row->rt.', ';}if($row->nama_desa!=''){echo'Desa '.$row->nama_desa.', ';}if($row->nama_kecamatan!=''){echo'Kecamatan '.$row->nama_kecamatan.'';}echo'</td>';
				echo '<td valign="top">
				<a class="badge bg-warning btn-sm" href="'.base_url().'warga_pkk/cetak/?id_warga_pkk='.$row->id_warga_pkk.'" ><i class="fa fa-print"></i> Cetak</a>
				<a href="#tab_form_warga_pkk" data-toggle="tab" class="update_id_warga_pkk badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax_warga_pkk" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			echo '
      <tr>
        <td valign="top" colspan="7" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'warga_pkk/cetak_warga_pkk/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data KK</a>
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'warga_pkk/cetak_semua_warga_pkk/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Semua Warga</a>
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'warga_pkk/cetak_warga_pkk_pekarangan/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Pemanfaatan Pekarangan Warga</a>
        </td>
      </tr>
      ';
          
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='demoopd.wonosobokab.go.id'){
      $where0 = array(
        'warga_pkk.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where0 = array(
        'warga_pkk.status !=' => 99
        );
    }else{
      $where0 = array(
        'warga_pkk.status !=' => 99,
        'warga_pkk.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('warga_pkk');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_warga_pkk()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('nik', 'nik', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('dasawisma', 'dasawisma', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'nama' => trim($this->input->post('nama')),
        'temp' => trim($this->input->post('temp')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'rw' => trim($this->input->post('rw')),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_kecamatan' => trim($this->input->post('id_kecamatan')),
        'id_kabupaten' => trim($this->input->post('id_kabupaten')),
        'dasawisma' => trim($this->input->post('dasawisma')),
        'id_jabatan' => trim($this->input->post('id_jabatan')),
        'tahun_pendaftaran' => trim($this->input->post('tahun_pendaftaran')),
        'kriteria_kader' => trim($this->input->post('kriteria_kader')),
        'tanggal_masuk_kader' => trim($this->input->post('tanggal_masuk_kader')),
        'kriteria_kader_pkbn' => trim($this->input->post('kriteria_kader_pkbn')),
        'kriteria_kader_kadarkum' => trim($this->input->post('kriteria_kader_kadarkum')),
        'kriteria_kader_pola_asuh' => trim($this->input->post('kriteria_kader_pola_asuh')),
        'kriteria_kader_pokja2' => trim($this->input->post('kriteria_kader_pokja2')),
        'kriteria_kader_tutor_kejar_paket' => trim($this->input->post('kriteria_kader_tutor_kejar_paket')),
        'kriteria_kader_tutor_kf' => trim($this->input->post('kriteria_kader_tutor_kf')),
        'kriteria_kader_tutor_paud' => trim($this->input->post('kriteria_kader_tutor_paud')),
        'kriteria_kader_bkb' => trim($this->input->post('kriteria_kader_bkb')),
        'kriteria_kader_koperasi' => trim($this->input->post('kriteria_kader_koperasi')),
        'kriteria_kader_keterampilan' => trim($this->input->post('kriteria_kader_keterampilan')),
        'kriteria_kader_lp3_pkk' => trim($this->input->post('kriteria_kader_lp3_pkk')),
        'kriteria_kader_tp3_pkk' => trim($this->input->post('kriteria_kader_tp3_pkk')),
        'kriteria_kader_damas' => trim($this->input->post('kriteria_kader_damas')),
        'kriteria_kader_mendongeng' => trim($this->input->post('kriteria_kader_mendongeng')),
        'kriteria_kader_pokja3' => trim($this->input->post('kriteria_kader_pokja3')),
        'kriteria_kader_pangan' => trim($this->input->post('kriteria_kader_pangan')),
        'kriteria_kader_sandang' => trim($this->input->post('kriteria_kader_sandang')),
        'kriteria_kader_tata_laksana_rt' => trim($this->input->post('kriteria_kader_tata_laksana_rt')),
        'kriteria_kader_pokja4' => trim($this->input->post('kriteria_kader_pokja4')),
        'kriteria_kader_posyandu_balita' => trim($this->input->post('kriteria_kader_posyandu_balita')),
        'kriteria_kader_posyandu_lansia' => trim($this->input->post('kriteria_kader_posyandu_lansia')),
        'kriteria_kader_gizi' => trim($this->input->post('kriteria_kader_gizi')),
        'kriteria_kader_kesling' => trim($this->input->post('kriteria_kader_kesling')),
        'kriteria_kader_peyuluhan_narkoba' => trim($this->input->post('kriteria_kader_peyuluhan_narkoba')),
        'kriteria_kader_kb' => trim($this->input->post('kriteria_kader_kb')),
        'kriteria_kader_phbs' => trim($this->input->post('kriteria_kader_phbs')),
        'kriteria_kader_tbc' => trim($this->input->post('kriteria_kader_tbc')),
        'jenis_kelamin' => trim($this->input->post('jenis_kelamin')),
        'tempat_lahir' => trim($this->input->post('tempat_lahir')),
        'tanggal_lahir' => trim($this->input->post('tanggal_lahir')),
        'status_perkawinan' => trim($this->input->post('status_perkawinan')),
        'status_dalam_keluarga' => trim($this->input->post('status_dalam_keluarga')),
        'id_agama' => trim($this->input->post('id_agama')),
        'id_pendidikan' => trim($this->input->post('id_pendidikan')),
        'id_pekerjaan' => trim($this->input->post('id_pekerjaan')),
        'alamat_warga' => trim($this->input->post('alamat_warga')),
        'id_desa_warga' => trim($this->input->post('id_desa_warga')),
        'id_kecamatan_warga' => trim($this->input->post('id_kecamatan_warga')),
        'id_kabupaten_warga' => trim($this->input->post('id_kabupaten_warga')),
        'aseptor_kb' => trim($this->input->post('aseptor_kb')),
        'jenis_aseptor_kb' => trim($this->input->post('jenis_aseptor_kb')),
        'aktif_kegiatan_osyandu_balita' => trim($this->input->post('aktif_kegiatan_osyandu_balita')),
        'memiliki_kartu_sehat' => trim($this->input->post('memiliki_kartu_sehat')),
        'aktif_kegiatan_osyandu_lansia' => trim($this->input->post('aktif_kegiatan_osyandu_lansia')),
        'mengikuti_program_bkb' => trim($this->input->post('mengikuti_program_bkb')),
        'memiliki_tabungan' => trim($this->input->post('memiliki_tabungan')),
        'jenis_tabungan' => trim($this->input->post('jenis_tabungan')),
        'memiliki_kartu_berobat_gratis' => trim($this->input->post('memiliki_kartu_berobat_gratis')),
        'memiliki_kelompok_belajar' => trim($this->input->post('memiliki_kelompok_belajar')),
        'mengikuti_kegiatan_paud' => trim($this->input->post('mengikuti_kegiatan_paud')),
        'mengikuti_kegiatan_koperasi' => trim($this->input->post('mengikuti_kegiatan_koperasi')),
        'aktifitas_up2k' => trim($this->input->post('aktifitas_up2k')),
        'aktifitas_tpk_wanita' => trim($this->input->post('aktifitas_tpk_wanita')),
        'aktifitas_kegiatan_keshatan_lingkungan' => trim($this->input->post('aktifitas_kegiatan_keshatan_lingkungan')),
        'penghayatan_dan_pengamalan_pancasila' => trim($this->input->post('penghayatan_dan_pengamalan_pancasila')),
        'kegiatan_keagamaan' => trim($this->input->post('kegiatan_keagamaan')),
        'rukun_kematian' => trim($this->input->post('rukun_kematian')),
        'arisan' => trim($this->input->post('arisan')),
        'kerja_bakti' => trim($this->input->post('kerja_bakti')),
        'jimpitan' => trim($this->input->post('jimpitan')),
        'bidang_pangan' => trim($this->input->post('bidang_pangan')),
        'bidang_sandang' => trim($this->input->post('bidang_sandang')),
        'bidang_tata_laksana_rt' => trim($this->input->post('bidang_tata_laksana_rt')),
        'pkbn' => trim($this->input->post('pkbn')),
        'kadarkum' => trim($this->input->post('kadarkum')),
        'pola_asuh' => trim($this->input->post('pola_asuh')),
        'mengikuti_pelatihan' => trim($this->input->post('mengikuti_pelatihan')),
        'nama_pelatihan' => trim($this->input->post('nama_pelatihan')),
        'tanggal_pelatihan' => trim($this->input->post('tanggal_pelatihan')),
        'penyelenggara_pelatihan' => trim($this->input->post('penyelenggara_pelatihan')),
        'jumlah_kepala_keluarga' => trim($this->input->post('jumlah_kepala_keluarga')),
        'jumlah_anggota_keluarga' => trim($this->input->post('jumlah_anggota_keluarga')),
        'jumlah_pria' => trim($this->input->post('jumlah_pria')),
        'jumlah_wanita' => trim($this->input->post('jumlah_wanita')),
        'jumlah_balita_pria' => trim($this->input->post('jumlah_balita_pria')),
        'jumlah_balita_wanita' => trim($this->input->post('jumlah_balita_wanita')),
        'jumlah_pus' => trim($this->input->post('jumlah_pus')),
        'jumlah_wus' => trim($this->input->post('jumlah_wus')),
        'tiga_buta_pria' => trim($this->input->post('tiga_buta_pria')),
        'tiga_buta_wanita' => trim($this->input->post('tiga_buta_wanita')),
        'ibu_hamil' => trim($this->input->post('ibu_hamil')),
        'ibu_menyusui' => trim($this->input->post('ibu_menyusui')),
        'jumlah_lansia' => trim($this->input->post('jumlah_lansia')),
        'makanan_pokok' => trim($this->input->post('makanan_pokok')),
        'memiliki_jamban' => trim($this->input->post('memiliki_jamban')),
        'jumlah_jamban' => trim($this->input->post('jumlah_jamban')),
        'sumber_air' => trim($this->input->post('sumber_air')),
        'memiliki_tempat_pembuangan_sampah' => trim($this->input->post('memiliki_tempat_pembuangan_sampah')),
        'memiliki_tempat_pembuangan_limbah' => trim($this->input->post('memiliki_tempat_pembuangan_limbah')),
        'lantai_rumah' => trim($this->input->post('lantai_rumah')),
        'dinding_rumah' => trim($this->input->post('dinding_rumah')),
        'ventilasi_rumah' => trim($this->input->post('ventilasi_rumah')),
        'memiliki_sekat_rumah' => trim($this->input->post('memiliki_sekat_rumah')),
        'kandang_terpisah' => trim($this->input->post('kandang_terpisah')),
        'menempel_stiker_p4k' => trim($this->input->post('menempel_stiker_p4k')),
        'kriteria_rumah_sehat' => trim($this->input->post('kriteria_rumah_sehat')),
        'pemanfaatan_tanah_pekarangan' => trim($this->input->post('pemanfaatan_tanah_pekarangan')),
        'pemanfaatan_tanah_pekarangan_perikanan' => trim($this->input->post('pemanfaatan_tanah_pekarangan_perikanan')),
        'pemanfaatan_tanah_pekarangan_warung_hidup' => trim($this->input->post('pemanfaatan_tanah_pekarangan_warung_hidup')),
        'pemanfaatan_tanah_pekarangan_tanaman_keras' => trim($this->input->post('pemanfaatan_tanah_pekarangan_tanaman_keras')),
        'pemanfaatan_tanah_pekarangan_peternakan' => trim($this->input->post('pemanfaatan_tanah_pekarangan_peternakan')),
        'pemanfaatan_tanah_pekarangan_toga' => trim($this->input->post('pemanfaatan_tanah_pekarangan_toga')),
        'industri_rumah_tangga' => trim($this->input->post('industri_rumah_tangga')),
        'industri_rumah_tangga_pangan' => trim($this->input->post('industri_rumah_tangga_pangan')),
        'industri_rumah_tangga_sandang' => trim($this->input->post('industri_rumah_tangga_sandang')),
        'industri_rumah_tangga_konveksi' => trim($this->input->post('industri_rumah_tangga_konveksi')),
        'industri_rumah_tangga_jasa' => trim($this->input->post('industri_rumah_tangga_jasa')),
        'nama_kepala_keluarga' => trim($this->input->post('nama_kepala_keluarga')),
        'nik' => trim($this->input->post('nik')),
        'nomor_registrasi' => trim($this->input->post('nomor_registrasi')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'warga_pkk';
      $id         = $this->Warga_pkk_model->simpan_warga_pkk($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'warga_pkk',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
  public function update_warga_pkk()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('nik', 'nik', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('dasawisma', 'dasawisma', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
				'nama' => trim($this->input->post('nama')),
        'temp' => trim($this->input->post('temp')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'rw' => trim($this->input->post('rw')),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_kecamatan' => trim($this->input->post('id_kecamatan')),
        'id_kabupaten' => trim($this->input->post('id_kabupaten')),
        'dasawisma' => trim($this->input->post('dasawisma')),
        'id_jabatan' => trim($this->input->post('id_jabatan')),
        'tahun_pendaftaran' => trim($this->input->post('tahun_pendaftaran')),
        'kriteria_kader' => trim($this->input->post('kriteria_kader')),
        'tanggal_masuk_kader' => trim($this->input->post('tanggal_masuk_kader')),
        'kriteria_kader_pkbn' => trim($this->input->post('kriteria_kader_pkbn')),
        'kriteria_kader_kadarkum' => trim($this->input->post('kriteria_kader_kadarkum')),
        'kriteria_kader_pola_asuh' => trim($this->input->post('kriteria_kader_pola_asuh')),
        'kriteria_kader_pokja2' => trim($this->input->post('kriteria_kader_pokja2')),
        'kriteria_kader_tutor_kejar_paket' => trim($this->input->post('kriteria_kader_tutor_kejar_paket')),
        'kriteria_kader_tutor_kf' => trim($this->input->post('kriteria_kader_tutor_kf')),
        'kriteria_kader_tutor_paud' => trim($this->input->post('kriteria_kader_tutor_paud')),
        'kriteria_kader_bkb' => trim($this->input->post('kriteria_kader_bkb')),
        'kriteria_kader_koperasi' => trim($this->input->post('kriteria_kader_koperasi')),
        'kriteria_kader_keterampilan' => trim($this->input->post('kriteria_kader_keterampilan')),
        'kriteria_kader_lp3_pkk' => trim($this->input->post('kriteria_kader_lp3_pkk')),
        'kriteria_kader_tp3_pkk' => trim($this->input->post('kriteria_kader_tp3_pkk')),
        'kriteria_kader_damas' => trim($this->input->post('kriteria_kader_damas')),
        'kriteria_kader_mendongeng' => trim($this->input->post('kriteria_kader_mendongeng')),
        'kriteria_kader_pokja3' => trim($this->input->post('kriteria_kader_pokja3')),
        'kriteria_kader_pangan' => trim($this->input->post('kriteria_kader_pangan')),
        'kriteria_kader_sandang' => trim($this->input->post('kriteria_kader_sandang')),
        'kriteria_kader_tata_laksana_rt' => trim($this->input->post('kriteria_kader_tata_laksana_rt')),
        'kriteria_kader_pokja4' => trim($this->input->post('kriteria_kader_pokja4')),
        'kriteria_kader_posyandu_balita' => trim($this->input->post('kriteria_kader_posyandu_balita')),
        'kriteria_kader_posyandu_lansia' => trim($this->input->post('kriteria_kader_posyandu_lansia')),
        'kriteria_kader_gizi' => trim($this->input->post('kriteria_kader_gizi')),
        'kriteria_kader_kesling' => trim($this->input->post('kriteria_kader_kesling')),
        'kriteria_kader_peyuluhan_narkoba' => trim($this->input->post('kriteria_kader_peyuluhan_narkoba')),
        'kriteria_kader_kb' => trim($this->input->post('kriteria_kader_kb')),
        'kriteria_kader_phbs' => trim($this->input->post('kriteria_kader_phbs')),
        'kriteria_kader_tbc' => trim($this->input->post('kriteria_kader_tbc')),
        'jenis_kelamin' => trim($this->input->post('jenis_kelamin')),
        'tempat_lahir' => trim($this->input->post('tempat_lahir')),
        'tanggal_lahir' => trim($this->input->post('tanggal_lahir')),
        'status_perkawinan' => trim($this->input->post('status_perkawinan')),
        'status_dalam_keluarga' => trim($this->input->post('status_dalam_keluarga')),
        'id_agama' => trim($this->input->post('id_agama')),
        'id_pendidikan' => trim($this->input->post('id_pendidikan')),
        'id_pekerjaan' => trim($this->input->post('id_pekerjaan')),
        'alamat_warga' => trim($this->input->post('alamat_warga')),
        'id_desa_warga' => trim($this->input->post('id_desa_warga')),
        'id_kecamatan_warga' => trim($this->input->post('id_kecamatan_warga')),
        'id_kabupaten_warga' => trim($this->input->post('id_kabupaten_warga')),
        'aseptor_kb' => trim($this->input->post('aseptor_kb')),
        'jenis_aseptor_kb' => trim($this->input->post('jenis_aseptor_kb')),
        'aktif_kegiatan_osyandu_balita' => trim($this->input->post('aktif_kegiatan_osyandu_balita')),
        'memiliki_kartu_sehat' => trim($this->input->post('memiliki_kartu_sehat')),
        'aktif_kegiatan_osyandu_lansia' => trim($this->input->post('aktif_kegiatan_osyandu_lansia')),
        'mengikuti_program_bkb' => trim($this->input->post('mengikuti_program_bkb')),
        'memiliki_tabungan' => trim($this->input->post('memiliki_tabungan')),
        'jenis_tabungan' => trim($this->input->post('jenis_tabungan')),
        'memiliki_kartu_berobat_gratis' => trim($this->input->post('memiliki_kartu_berobat_gratis')),
        'memiliki_kelompok_belajar' => trim($this->input->post('memiliki_kelompok_belajar')),
        'mengikuti_kegiatan_paud' => trim($this->input->post('mengikuti_kegiatan_paud')),
        'mengikuti_kegiatan_koperasi' => trim($this->input->post('mengikuti_kegiatan_koperasi')),
        'aktifitas_up2k' => trim($this->input->post('aktifitas_up2k')),
        'aktifitas_tpk_wanita' => trim($this->input->post('aktifitas_tpk_wanita')),
        'aktifitas_kegiatan_keshatan_lingkungan' => trim($this->input->post('aktifitas_kegiatan_keshatan_lingkungan')),
        'penghayatan_dan_pengamalan_pancasila' => trim($this->input->post('penghayatan_dan_pengamalan_pancasila')),
        'kegiatan_keagamaan' => trim($this->input->post('kegiatan_keagamaan')),
        'rukun_kematian' => trim($this->input->post('rukun_kematian')),
        'arisan' => trim($this->input->post('arisan')),
        'kerja_bakti' => trim($this->input->post('kerja_bakti')),
        'jimpitan' => trim($this->input->post('jimpitan')),
        'bidang_pangan' => trim($this->input->post('bidang_pangan')),
        'bidang_sandang' => trim($this->input->post('bidang_sandang')),
        'bidang_tata_laksana_rt' => trim($this->input->post('bidang_tata_laksana_rt')),
        'pkbn' => trim($this->input->post('pkbn')),
        'kadarkum' => trim($this->input->post('kadarkum')),
        'pola_asuh' => trim($this->input->post('pola_asuh')),
        'mengikuti_pelatihan' => trim($this->input->post('mengikuti_pelatihan')),
        'nama_pelatihan' => trim($this->input->post('nama_pelatihan')),
        'tanggal_pelatihan' => trim($this->input->post('tanggal_pelatihan')),
        'penyelenggara_pelatihan' => trim($this->input->post('penyelenggara_pelatihan')),
        'jumlah_kepala_keluarga' => trim($this->input->post('jumlah_kepala_keluarga')),
        'jumlah_anggota_keluarga' => trim($this->input->post('jumlah_anggota_keluarga')),
        'jumlah_pria' => trim($this->input->post('jumlah_pria')),
        'jumlah_wanita' => trim($this->input->post('jumlah_wanita')),
        'jumlah_balita_pria' => trim($this->input->post('jumlah_balita_pria')),
        'jumlah_balita_wanita' => trim($this->input->post('jumlah_balita_wanita')),
        'jumlah_pus' => trim($this->input->post('jumlah_pus')),
        'jumlah_wus' => trim($this->input->post('jumlah_wus')),
        'tiga_buta_pria' => trim($this->input->post('tiga_buta_pria')),
        'tiga_buta_wanita' => trim($this->input->post('tiga_buta_wanita')),
        'ibu_hamil' => trim($this->input->post('ibu_hamil')),
        'ibu_menyusui' => trim($this->input->post('ibu_menyusui')),
        'jumlah_lansia' => trim($this->input->post('jumlah_lansia')),
        'makanan_pokok' => trim($this->input->post('makanan_pokok')),
        'memiliki_jamban' => trim($this->input->post('memiliki_jamban')),
        'jumlah_jamban' => trim($this->input->post('jumlah_jamban')),
        'sumber_air' => trim($this->input->post('sumber_air')),
        'memiliki_tempat_pembuangan_sampah' => trim($this->input->post('memiliki_tempat_pembuangan_sampah')),
        'memiliki_tempat_pembuangan_limbah' => trim($this->input->post('memiliki_tempat_pembuangan_limbah')),
        'lantai_rumah' => trim($this->input->post('lantai_rumah')),
        'dinding_rumah' => trim($this->input->post('dinding_rumah')),
        'ventilasi_rumah' => trim($this->input->post('ventilasi_rumah')),
        'memiliki_sekat_rumah' => trim($this->input->post('memiliki_sekat_rumah')),
        'kandang_terpisah' => trim($this->input->post('kandang_terpisah')),
        'menempel_stiker_p4k' => trim($this->input->post('menempel_stiker_p4k')),
        'kriteria_rumah_sehat' => trim($this->input->post('kriteria_rumah_sehat')),
        'pemanfaatan_tanah_pekarangan' => trim($this->input->post('pemanfaatan_tanah_pekarangan')),
        'pemanfaatan_tanah_pekarangan_perikanan' => trim($this->input->post('pemanfaatan_tanah_pekarangan_perikanan')),
        'pemanfaatan_tanah_pekarangan_warung_hidup' => trim($this->input->post('pemanfaatan_tanah_pekarangan_warung_hidup')),
        'pemanfaatan_tanah_pekarangan_tanaman_keras' => trim($this->input->post('pemanfaatan_tanah_pekarangan_tanaman_keras')),
        'pemanfaatan_tanah_pekarangan_peternakan' => trim($this->input->post('pemanfaatan_tanah_pekarangan_peternakan')),
        'pemanfaatan_tanah_pekarangan_toga' => trim($this->input->post('pemanfaatan_tanah_pekarangan_toga')),
        'industri_rumah_tangga' => trim($this->input->post('industri_rumah_tangga')),
        'industri_rumah_tangga_pangan' => trim($this->input->post('industri_rumah_tangga_pangan')),
        'industri_rumah_tangga_sandang' => trim($this->input->post('industri_rumah_tangga_sandang')),
        'industri_rumah_tangga_konveksi' => trim($this->input->post('industri_rumah_tangga_konveksi')),
        'industri_rumah_tangga_jasa' => trim($this->input->post('industri_rumah_tangga_jasa')),
        'nama_kepala_keluarga' => trim($this->input->post('nama_kepala_keluarga')),
        'nik' => trim($this->input->post('nik')),
        'nomor_registrasi' => trim($this->input->post('nomor_registrasi')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'warga_pkk';
      $where       = array(
        'warga_pkk.id_warga_pkk' => trim($this->input->post('id_warga_pkk')),
        'warga_pkk.domain' => $web
			);
      $this->Warga_pkk_model->update_data_warga_pkk($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      if($web=='demoopd.wonosobokab.go.id'){
        $where = array(
        'warga_pkk.id_warga_pkk' => $this->input->post('id_warga_pkk'),
          );
      }elseif($web=='pkk.wonosobokab.go.id'){
        $where = array(
        'warga_pkk.id_warga_pkk' => $this->input->post('id_warga_pkk'),
          );
      }else{
        $where = array(
        'warga_pkk.id_warga_pkk' => $this->input->post('id_warga_pkk'),
          'warga_pkk.id_kecamatan' => $this->session->userdata('id_kecamatan')
          );
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_warga_pkk');
      $result = $this->db->get('warga_pkk');
      echo json_encode($result->result_array());
		}
   
   public function total_warga_pkk()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('warga_pkk');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_warga_pkk = $this->input->post('id_warga_pkk');
      $where = array(
        'id_warga_pkk' => $id_warga_pkk,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('warga_pkk');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'warga_pkk';
        $this->Warga_pkk_model->update_data_warga_pkk($data_update, $where, $table_name);
        echo 1;
        }
		}
  function option_desa_by_id_kecamatan(){
		$id_kecamatan = $this->input->post('id_kecamatan');
		$w = $this->db->query("
		SELECT *
		from desa
		where desa.status = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function option_dusun_by_id_desa(){
		$id_desa = $this->input->post('id_desa');
		$w = $this->db->query("
		SELECT *
		from dusun
		where dusun.status = 1
		and dusun.id_desa = ".$id_desa."
		order by id_dusun");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_dusun.'">'.$h->nama_dusun.'</option>';
		}
	}
  function option_kecamatan(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_kecamatan="";
		}
		else{
			$where_id_kecamatan="and data_kecamatan.id_kecamatan=".$this->session->userdata('id_kecamatan')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_kecamatan, kecamatan
		where data_kecamatan.status = 1
		and kecamatan.id_kecamatan=data_kecamatan.id_kecamatan
    ".$where_id_kecamatan."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function status_dalam_keluarga(){
		$w = $this->db->query("
		SELECT *
		from status_dalam_keluarga
		where status_dalam_keluarga.status = 1
		order by status_dalam_keluarga.id_status_dalam_keluarga
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_dalam_keluarga.'">'.$h->nama_status_dalam_keluarga.'</option>';
		}
	}
  function pekerjaan(){
		$w = $this->db->query("
		SELECT *
		from pekerjaan
		where pekerjaan.status = 1
		order by pekerjaan.id_pekerjaan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pekerjaan.'">'.$h->nama_pekerjaan.'</option>';
		}
	}
  function agama(){
		$w = $this->db->query("
		SELECT *
		from agama
		where agama.status = 1
		order by agama.id_agama
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_agama.'">'.$h->nama_agama.'</option>';
		}
	}
  function status_pernikahan(){
		$w = $this->db->query("
		SELECT *
		from status_pernikahan
		where status_pernikahan.status = 1
		order by status_pernikahan.id_status_pernikahan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_pernikahan.'">'.$h->nama_status_pernikahan.'</option>';
		}
	}
  function pendidikan(){
		$w = $this->db->query("
		SELECT *
		from pendidikan
		where pendidikan.status = 1
		order by pendidikan.id_pendidikan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pendidikan.'">'.$h->nama_pendidikan.'</option>';
		}
	}
 }