<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Skpd extends CI_Controller
{

  function __construct(){
		parent::__construct();
		$this->load->model('Skpda_model');
  }
  public function index(){
    $data['main_view'] = 'skpd/home';
    $this->load->view('back_bone', $data);
  }
  public function json_option_skpd(){
		$table = 'skpd';
		$hak_akses = $this->session->userdata('hak_akses');
		if($hak_akses=='root'){
			$where   = array(
				'skpd.status !=' => 99
				);
		}
		else{
			$where   = array(
				'skpd.id_skpd' => $this->session->userdata('id_skpd')
				);
		}
		$fields  = 
			"
			skpd.id_skpd,
			skpd.nama_skpd
			";
		$order_by  = 'skpd.nama_skpd';
		echo json_encode($this->Skpda_model->opsi($table, $where, $fields, $order_by));
  }
  public function total_data()
  {
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'skpd.status !=' => 99,
      'skpd.status !=' => 999
      );
    $this->db->where($where0);
    $query0 = $this->db->get('skpd');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_skpd(){
		$table = 'skpd';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where      = array(
      'skpd.status !=' => 99
    );
    $orderby   = ''.$order_by.'';
    $query = $this->Skpda_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
          $urut=$urut+1;
          echo '<tr id_skpd="'.$row->id_skpd.'" id="'.$row->id_skpd.'" >';
          echo '<td valign="top">'.($urut).'</td>';
					echo '<td valign="top">'.$row->nama_skpd.'</td>';
					echo '<td valign="top"><a href="#tab_form_skpd" data-toggle="tab" class="update_id btn btn-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>';
					echo '<a href="#" id="del_ajax" class="btn btn-danger btn-sm"><i class="fa fa-cut"></i></a>';
          echo '<a class="btn btn-warning btn-sm" href="'.base_url().'skpd/pdf/?id_skpd='.$row->id_skpd.'" ><i class="fa fa-file-pdf-o"></i></a></td>';
          echo '</tr>';
      }
          echo '<tr>';
          echo '<td valign="top" colspan="3" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="',base_url(),'skpd/xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
          echo '</tr>';
          
   }
  public function simpan_skpd(){
    $this->form_validation->set_rules('nama_skpd', 'nama_skpd', 'required');
    $this->form_validation->set_rules('status', 'status', 'required');
    if ($this->form_validation->run() == FALSE){
      echo 0;
			}
    else{
				$data_input = array(
        'kode_skpd' => 0,
				'nama_skpd' => trim($this->input->post('nama_skpd')),
				'keterangan' => '',
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
				'status' => trim($this->input->post('status'))

				);
      $table_name = 'skpd';
      $this->Skpda_model->save_data($data_input, $table_name);
		}
	}
}