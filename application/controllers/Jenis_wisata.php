<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jenis_wisata extends CI_Controller
{

  function __construct(){
		parent::__construct();
		$this->load->model('Jenis_wisata_model');
  }
  public function index(){
    $data['main_view'] = 'jenis_wisata/home';
    $this->load->view('back_bone', $data);
  }
  public function json_option_jenis_wisata(){
		$table = 'jenis_wisata';
    $id_skpd = trim($this->input->post('id_skpd'));
		$hak_akses = $this->session->userdata('hak_akses');
		$where   = array(
			'jenis_wisata.status!=' => 99
			);
		$fields  = 
			"
			jenis_wisata.id_jenis_wisata,
			jenis_wisata.nama_jenis_wisata
			";
		$order_by  = 'jenis_wisata.nama_jenis_wisata';
		echo json_encode($this->Jenis_wisata_model->opsi($table, $where, $fields, $order_by));
  }
  public function total_data()
  {
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'jenis_wisata.status !=' => 99
      );
    $this->db->where($where0);
    $query0 = $this->db->get('jenis_wisata');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_jenis_wisata(){
		$table = 'jenis_wisata';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where      = array(
      'jenis_wisata.status !=' => 99
    );
    $orderby   = ''.$order_by.'';
    $query = $this->Jenis_wisata_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
          $urut=$urut+1;
          echo '<tr id_jenis_wisata="'.$row->id_jenis_wisata.'" id="'.$row->id_jenis_wisata.'" >';
          echo '<td valign="top">'.($urut).'</td>';
					echo '<td valign="top">'.$row->nama_jenis_wisata.'</td>';
					echo '<td valign="top">
                  <div class="btn-group">
                    <a href="#tab_form_jenis_wisata" data-toggle="tab" id="update_id" class="update_id btn btn-warning btn-sm"><i class="fa fa-pencil-square-o"></i> Edt</a>
                    <a href="#" id="del_ajax" class="btn btn-danger btn-sm"><i class="fa fa-cut"></i> Del</button>
                    <a href="'.base_url().'jenis_wisata/pdf/?id_jenis_wisata='.$row->id_jenis_wisata.'" class="btn btn-info btn-sm"><i class="fa fa-file-pdf-o"></i> Ctk</a>
                  </div>
                  ';
          echo '</tr>';
      }
          echo '<tr>';
          echo '<td valign="top" colspan="4" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="',base_url(),'jenis_wisata/xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
          echo '</tr>';
          
   }
  public function simpan_jenis_wisata(){
    $this->form_validation->set_rules('nama_jenis_wisata', 'nama_jenis_wisata', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
			{
				$data_input = array(
				'nama_jenis_wisata' => trim($this->input->post('nama_jenis_wisata')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'jenis_wisata';
      $this->Jenis_wisata_model->save_data($data_input, $table_name);
     }
   }
   public function get_by_id()
		{
      $where    = array(
        'id_jenis_wisata' => $this->input->post('id_jenis_wisata')
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_jenis_wisata');
      $result = $this->db->get('jenis_wisata');
      echo json_encode($result->result_array());
		}
  public function update_jenis_wisata(){
		$this->form_validation->set_rules('nama_jenis_wisata', 'nama_jenis_wisata', 'required');
    if ($this->form_validation->run() == FALSE){
      echo 0;
		}
    else{
      $data_update = array(
			
        'nama_jenis_wisata' => trim($this->input->post('nama_jenis_wisata')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
			);
      $table_name  = 'jenis_wisata';
      $where       = array(
        'id_jenis_wisata' => trim($this->input->post('id_jenis_wisata'))
			);
      $this->Jenis_wisata_model->update_data($data_update, $where, $table_name);
      echo 1;
		}
  }
  public function hapus()
		{
			$id_jenis_wisata = $this->input->post('id_jenis_wisata');
      $where = array(
        'id_jenis_wisata' => $id_jenis_wisata,
				'created_by' => $this->session->userdata('id_users')
        );
      $this->db->from('jenis_wisata');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'jenis_wisata';
        $this->Jenis_wisata_model->update_data($data_update, $where, $table_name);
        echo 1;
        }
		}
}