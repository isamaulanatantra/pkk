<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sosegov extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Login_model');
  }

  public function index()
  {

    $web = $this->uut->namadomain(base_url());
    $wheredasar_website = array(
      'domain' => $web
    );
    $this->db->where($wheredasar_website);
    $this->db->limit(1);
    $querydasar_website = $this->db->get('dasar_website');
    foreach ($querydasar_website->result() as $rowdasar_website) {
      $data['domain'] = $rowdasar_website->domain;
      $data['alamat'] = $rowdasar_website->alamat;
      $data['telpon'] = $rowdasar_website->telpon;
      $data['email'] = $rowdasar_website->email;
      $data['twitter'] = $rowdasar_website->twitter;
      $data['facebook'] = $rowdasar_website->facebook;
      $data['google'] = $rowdasar_website->google;
      $data['instagram'] = $rowdasar_website->instagram;
      $data['peta'] = $rowdasar_website->peta;
      $data['title'] = $rowdasar_website->keterangan;
    }
    $fields0     = "*";
    $where0      = array(
      'users.id_users' => $this->session->userdata('id_users'),
      'module_role.status' => 1,
      'role_user.status' => 1
    );
    $this->db->join('role_user', 'role_user.user_id=users.id_users');
    $this->db->join('module_role', 'module_role.role_id=role_user.role_id');
    $this->db->join('module', 'module.module_id=module_role.module_id');
    $this->db->where($where0);
    $this->db->select("$fields0");
    $this->db->order_by('module.urutan_menu', 'asc');
    $data['menu'] = $this->db->get('users');
    /* Role */
    $fieldsrole     = "*";
    $whererole      = array(
      'role.status' => 1
    );
    $this->db->where($whererole);
    $this->db->select("$fieldsrole");
    $this->db->order_by('role.role_id', 'asc');
    foreach ($this->db->get('role')->result() as $r_role) {
      $fields     = "*";
      $where      = array(
        'users.id_users' => $this->session->userdata('id_users'),
        'module_role.role_id' => $r_role->role_id,
        'module_role.status' => 1,
        'role_user.status' => 1
      );
      $this->db->join('role_user', 'role_user.user_id=users.id_users');
      $this->db->join('module_role', 'module_role.role_id=role_user.role_id');
      $this->db->join('module', 'module.module_id=module_role.module_id');
      $this->db->where($where);
      $this->db->select("$fields");
      $this->db->order_by('module.urutan_menu', 'asc');
      $data['menu_' . $r_role->role_code . ''] = $this->db->get('users');
      $data['role_name'] = $r_role->role_name;
    }
    /* Role */
    $fieldsroles = "*";
    $whereroles = array(
      'role.status' => 1
    );
    $this->db->where($whereroles);
    $this->db->select("$fieldsroles");
    $this->db->order_by('role.role_id', 'asc');
    $data['roles'] = $this->db->get('role');

    $fields1     =
      "
            user_profile.first_name,
            user_profile.last_name,
            
            users.created_time,
            
            (
            select attachment.file_name from attachment
            where attachment.id_tabel=users.id_users
            and attachment.table_name='foto_profil'
            order by attachment.id_attachment DESC
            limit 1
            ) as foto
            
            ";

    $where1      = array(
      'users.id_users' => $this->session->userdata('id_users')
    );
    $this->db->join('user_profile', 'user_profile.user_id=users.id_users');
    $this->db->where($where1);
    $this->db->select("$fields1");
    foreach ($this->db->get('users')->result() as $b2) {
      $data['first_name'] = $b2->first_name;
      $data['last_name'] = $b2->last_name;
      $ex = explode(' ', $b2->created_time);
      $data['membersince'] = $ex[0];
      if ($b2->foto == null) {
        $data['foto'] = '' . base_url() . 'Template/AdminLTE-2.4.3/dist/img/user4-128x128.jpg';
      } else {
        $data['foto'] = '' . base_url() . 'media/upload/s_' . $b2->foto . '';
      }
    }
    $a = 0;
    $data['menu_atas'] = $this->menu_atas_front(0, $h = "", $a);
    $data['page'] = 'timeline.php';
    $data['id'] = '';
    $data['web'] = ''.$web.'';
    $this->load->view('sosegov', $data);
    // $this->load->view('nav_top', $data);
    // $this->load->view('back_end', $data);
  }

  public function details()
  {

    $web = $this->uut->namadomain(base_url());
    $wheredasar_website = array(
      'domain' => $web
    );
    $this->db->where($wheredasar_website);
    $this->db->limit(1);
    $querydasar_website = $this->db->get('dasar_website');
    foreach ($querydasar_website->result() as $rowdasar_website) {
      $data['domain'] = $rowdasar_website->domain;
      $data['alamat'] = $rowdasar_website->alamat;
      $data['telpon'] = $rowdasar_website->telpon;
      $data['email'] = $rowdasar_website->email;
      $data['twitter'] = $rowdasar_website->twitter;
      $data['facebook'] = $rowdasar_website->facebook;
      $data['google'] = $rowdasar_website->google;
      $data['instagram'] = $rowdasar_website->instagram;
      $data['peta'] = $rowdasar_website->peta;
      $data['title'] = $rowdasar_website->keterangan;
    }
    $fields0     = "*";
    $where0      = array(
      'users.id_users' => $this->session->userdata('id_users'),
      'module_role.status' => 1,
      'role_user.status' => 1
    );
    $this->db->join('role_user', 'role_user.user_id=users.id_users');
    $this->db->join('module_role', 'module_role.role_id=role_user.role_id');
    $this->db->join('module', 'module.module_id=module_role.module_id');
    $this->db->where($where0);
    $this->db->select("$fields0");
    $this->db->order_by('module.urutan_menu', 'asc');
    $data['menu'] = $this->db->get('users');
    /* Role */
    $fieldsrole     = "*";
    $whererole      = array(
      'role.status' => 1
    );
    $this->db->where($whererole);
    $this->db->select("$fieldsrole");
    $this->db->order_by('role.role_id', 'asc');
    foreach ($this->db->get('role')->result() as $r_role) {
      $fields     = "*";
      $where      = array(
        'users.id_users' => $this->session->userdata('id_users'),
        'module_role.role_id' => $r_role->role_id,
        'module_role.status' => 1,
        'role_user.status' => 1
      );
      $this->db->join('role_user', 'role_user.user_id=users.id_users');
      $this->db->join('module_role', 'module_role.role_id=role_user.role_id');
      $this->db->join('module', 'module.module_id=module_role.module_id');
      $this->db->where($where);
      $this->db->select("$fields");
      $this->db->order_by('module.urutan_menu', 'asc');
      $data['menu_' . $r_role->role_code . ''] = $this->db->get('users');
      $data['role_name'] = $r_role->role_name;
    }
    /* Role */
    $fieldsroles = "*";
    $whereroles = array(
      'role.status' => 1
    );
    $this->db->where($whereroles);
    $this->db->select("$fieldsroles");
    $this->db->order_by('role.role_id', 'asc');
    $data['roles'] = $this->db->get('role');

    $fields1     =
      "
            user_profile.first_name,
            user_profile.last_name,
            
            users.created_time,
            
            (
            select attachment.file_name from attachment
            where attachment.id_tabel=users.id_users
            and attachment.table_name='foto_profil'
            order by attachment.id_attachment DESC
            limit 1
            ) as foto
            
            ";

    $where1      = array(
      'users.id_users' => $this->session->userdata('id_users')
    );
    $this->db->join('user_profile', 'user_profile.user_id=users.id_users');
    $this->db->where($where1);
    $this->db->select("$fields1");
    foreach ($this->db->get('users')->result() as $b2) {
      $data['first_name'] = $b2->first_name;
      $data['last_name'] = $b2->last_name;
      $ex = explode(' ', $b2->created_time);
      $data['membersince'] = $ex[0];
      if ($b2->foto == null) {
        $data['foto'] = '' . base_url() . 'Template/AdminLTE-2.4.3/dist/img/user4-128x128.jpg';
      } else {
        $data['foto'] = '' . base_url() . 'media/upload/s_' . $b2->foto . '';
      }
    }
    $a = 0;
    $data['menu_atas'] = $this->menu_atas_front(0, $h = "", $a);
    $data['page'] = 'front_end/sosegov_details.php';
    $data['id'] = $this->uri->segment(3);
    $data['web'] = ''.$web.'';
    $this->load->view('sosegov', $data);
    // $this->load->view('nav_top', $data);
    // $this->load->view('back_end', $data);
  }

	function fetch_permohonan_informasi_publik()
	{
		$id = $this->input->post('id');
		if($id==''){$details_timeline="";}else{$details_timeline="AND permohonan_informasi_publik.id_permohonan_informasi_publik=".$id."";}
    $keyword     = $this->input->post('keyword');
    $web = $this->uut->namadomain(base_url());
			if($web=='wonosobokab.go.id'){$web_timeline = "";}
			elseif($web=='demoopd.wonosobokab.go.id'){$web_timeline = "AND permohonan_informasi_publik.domain='diskominfo.wonosobokab.go.id'";}
			else{$web_timeline = "AND permohonan_informasi_publik.domain='".$web."'";}
		$output = '';
		$limit = $this->input->post('limit');
		$start = $this->input->post('start');
		$this->db->select("
			*, 
			(
				select attachment.file_name from attachment
				where attachment.id_tabel=permohonan_informasi_publik.id_permohonan_informasi_publik
				and attachment.table_name='permohonan_informasi_publik'
				order by attachment.id_attachment DESC
				limit 1
			) as file, 
			(SELECT user_name FROM users WHERE users.id_users=permohonan_informasi_publik.created_by) AS nama_pengguna
		");
    if ($keyword == '') {
      $this->db->where("
            permohonan_informasi_publik.status = 1
						".$web_timeline."
						".$details_timeline."
            ");
    } else {
      $this->db->where("
            permohonan_informasi_publik.rincian_informasi_yang_diinginkan LIKE '%" . $keyword . "%'
						AND
            permohonan_informasi_publik.status = 1
						".$web_timeline."
						".$details_timeline."
            ");
    }
		$this->db->order_by("permohonan_informasi_publik.created_time", "DESC");
		$this->db->limit($limit, $start);
		$data = $this->db->get('permohonan_informasi_publik');
		if($data->num_rows() > 0)
		{
		foreach($data->result() as $row)
			{
				$filenya = './media/upload/'.$row->file.'';
				$file_info = new finfo(FILEINFO_MIME);  // object oriented approach!
				$mime_type = $file_info->buffer(file_get_contents($filenya));  // e.g. gives "image/jpeg"
				if (strpos($mime_type, 'image') !== false) {
					$file_foto=''.base_url().'media/upload/'.$row->file.'';
					$output .= '
						<div class="single-gallery-image" style="background: url('.$file_foto.'); margin-top: 1px; height: 250px;"></div>
					';
				}
				else{
					$file_foto=''.base_url().'media/imagekosong.png';
				}
				$ses=$this->session->userdata('id_users');
				$output .= '
				<div class="card mb-5">
					

					<div class="card-body p-4">
						<div class="mb-3">
							<a class="font-size-1" href="#">
								<span class="fas fa-map-marker-alt mr-1"></span>
								'.$row->nama.'
							</a>';
							if(!$ses) { $output .=''; }
							else{
								$output .= '<p>'.$row->alamat.'</p>
								<p><u>Pekerjaan:</u> '.$row->pekerjaan.'</p>
								<p><u>Nomor Telp.:</u> '.$row->nomor_telp.'</p>
								<p><u>Email:</u> '.$row->email.'</p>
								<p><u>Tujuan:</u> <span>'.$row->tujuan_penggunaan_informasi.'</span></p>
								<p><u>Website:</u> <a href="https://'.$row->domain.'/pengaduan_masyarakat" class="text-muted">'.$row->domain.'</a></p>
								<p><u>Rincian:</u></p>';
								}
								$output .= '
						</div>
						<p>'.$row->rincian_informasi_yang_diinginkan.'</p>
						<div class="d-flex align-items-center font-size-1">
							<a class="text-secondary mr-12" href="javascript:;">
								<span class="fas fa-star mr-1"></span>';
									$w11 = $this->db->query("
									SELECT *
									FROM permohonan_informasi_publik
									WHERE parent = ".$row->id_permohonan_informasi_publik."
									");
									$output .= ''.$jumlah_comments = $w11->num_rows().''; 
								$output .= '
							</a>
							<a class="btn btn-sm btn-soft-primary transition-3d-hover ml-auto" href="'.base_url().'sosegov/details/'.$row->id_permohonan_informasi_publik.'">
								Details
								<span class="fas fa-angle-right ml-1"></span>
							</a>
						</div>
						<!-- End Contacts -->
					</div>
				</div>
				';
			}
		}
		echo $output;
	}
	
  private function menu_atas_front($parent = NULL, $hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    if ($web == 'disparbud.wonosobokab.go.id' or $web == 'dikpora.wonosobokab.go.id') {
      $url_baca = 'front/detail';
      $url_baca_list = 'front/galeri';
    } else {
      $url_baca = 'front/details';
      $url_baca_list = 'front/categoris';
    }
    $a = $a + 1;
    $w = $this->db->query("
    SELECT * from posting
    where posisi='menu_atas'
    and parent='" . $parent . "' 
    and status = 1 
    and tampil_menu_atas = 1 
    and domain='" . $web . "'
    order by urut
    ");
    $x = $w->num_rows();
    if (($w->num_rows()) > 0) {
      if ($parent == 0) {
				if ($web == 'web.wonosobokab.go.id') {
        $hasil .= '
        <ul class="navbar-nav u-header__navbar-nav">
        <!-- Home -->
        <li class="nav-item hs-has-mega-menu u-header__nav-item"
            data-event="hover"
            data-animation-in="slideInUp"
            data-animation-out="fadeOut"
            data-position="left">
          <a id="homeMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false">Home</a>

          <!-- Home - Mega Menu -->
          <div class="hs-mega-menu w-100 u-header__sub-menu" aria-labelledby="homeMegaMenu">
            <div class="row no-gutters">
              <div class="col-lg-6">
                <!-- Banner Image -->
                <div class="u-header__banner" style="background-image: url(' . base_url() . 'media/upload/branding750x750.jpg);">
                  <div class="u-header__banner-content">
                    <div class="mb-4">
                      <span class="u-header__banner-title">Branding Works</span>
                      <p class="u-header__banner-text">Experience a level of our quality in both design &amp; customization works.</p>
                    </div>
                    <a class="btn btn-primary btn-sm transition-3d-hover" href="#">Learn More <span class="fas fa-angle-right ml-2"></span></a>
                  </div>
                </div>
                <!-- End Banner Image -->
              </div>

              <div class="col-lg-6">
                <div class="row u-header__mega-menu-wrapper">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <span class="u-header__sub-menu-title">Classic</span>
                    <ul class="u-header__sub-menu-nav-group mb-3">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . '">Classic Agency</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-business.html">Classic Business</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-marketing.html">Classic Marketing</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-consulting.html">Classic Consulting</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-start-up.html">Classic Start-up</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-studio.html">Classic Studio <span class="badge badge-success badge-pill ml-1">New</span></a></li>
                    </ul>

                    <span class="u-header__sub-menu-title">Corporate</span>
                    <ul class="u-header__sub-menu-nav-group mb-3">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="corporate-agency.html">Corporate Agency</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="corporate-start-up.html">Corporate Start-Up</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="corporate-business.html">Corporate Business</a></li>
                    </ul>

                    <span class="u-header__sub-menu-title">Portfolio</span>
                    <ul class="u-header__sub-menu-nav-group">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="portfolio-agency.html">Portfolio Agency</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="portfolio-profile.html">Portfolio Profile</a></li>
                    </ul>
                  </div>

                  <div class="col-sm-6">
                    <span class="u-header__sub-menu-title">App</span>
                    <ul class="u-header__sub-menu-nav-group mb-3">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="app-ui-kit.html">App UI kit</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="app-saas.html">App SaaS</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="app-workflow.html">App Workflow</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="app-payment.html">App Payment</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="app-software.html">App Software</a></li>
                    </ul>

                    <span class="u-header__sub-menu-title">Onepages</span>
                    <ul class="u-header__sub-menu-nav-group mb-3">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="onepage-corporate.html">Corporate <span class="badge badge-success badge-pill ml-1">New</span></a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="onepage-creative.html">Creative</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="onepage-saas.html">SaaS</a></li>
                    </ul>

                    <span class="u-header__sub-menu-title">Blog</span>
                    <ul class="u-header__sub-menu-nav-group">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="blog-agency.html">Blog Agency</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="blog-start-up.html">Blog Start-Up</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="blog-business.html">Blog Business</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- End Home - Mega Menu -->
        </li>
        <!-- End Home -->
        ';
				} else {
        $hasil .= '
        <ul class="navbar-nav u-header__navbar-nav">
        <!-- Home -->
        <li class="nav-item hs-has-mega-menu u-header__nav-item"
            data-event="hover"
            data-animation-in="slideInUp"
            data-animation-out="fadeOut"
            data-position="left">
          <a id="homeMegaMenu" class="nav-link u-header__nav-link" href="'.base_url().'" aria-haspopup="true" aria-expanded="false">Home</a>
        </li>
        ';
				}
      } else {
        $hasil .= '
         <ul id="pagesMegaMenu" class="hs-sub-menu u-header__sub-menu animated" aria-labelledby="pagesMegaMenu" style="min-width: 230px; display: none;">
        ';
      }
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $r = $this->db->query("
      SELECT * from posting
      where parent='" . $h->id_posting . "' 
      and status = 1 
      and tampil_menu_atas = 1 
      and domain='" . $web . "'
      order by urut
      ");
      $xx = $r->num_rows();
      $nomor = $nomor + 1;
      if ($a > 1) {
        if ($xx == 0) {
          if ($h->judul_posting == 'Pengaduan Masyarakat') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'sosegov"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Permohonan Informasi Publik') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'sosegov"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Permohonan Informasi') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'sosegov"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Sitemap') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Berita') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Struktur Organisasi') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/struktur_organisasi/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Peraturan Daerah') {
						$where6      = array(
							'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'perda'
						);
						$this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");    	
						$this->db->where($where6);
						$this->db->limit(1);
						$query6 = $this->db->get('jenis_perundang_undangan');
						$a6 = $query6->num_rows();
						if( $a6 == 0 ){
								$jenis_perundang_undangan='';
						}
						else{
							foreach ($query6->result() as $b6) {
								$jenis_perundang_undangan=$b6->jenis_perundang_undangan_id;
							}
						}
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id='.$jenis_perundang_undangan.'"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Peraturan Bupati') {
						$where6      = array(
							'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'perbup'
						);
						$this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");    	
						$this->db->where($where6);
						$this->db->limit(1);
						$query6 = $this->db->get('jenis_perundang_undangan');
						$a6 = $query6->num_rows();
						if( $a6 == 0 ){
								$jenis_perundang_undangan='';
						}
						else{
							foreach ($query6->result() as $b6) {
								$jenis_perundang_undangan=$b6->jenis_perundang_undangan_id;
							}
						}
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id='.$jenis_perundang_undangan.'"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Keputusan DPRD') {
						$where6      = array(
							'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'kepdprd'
						);
						$this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");    	
						$this->db->where($where6);
						$this->db->limit(1);
						$query6 = $this->db->get('jenis_perundang_undangan');
						$a6 = $query6->num_rows();
						if( $a6 == 0 ){
								$jenis_perundang_undangan='';
						}
						else{
							foreach ($query6->result() as $b6) {
								$jenis_perundang_undangan=$b6->jenis_perundang_undangan_id;
							}
						}
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id='.$jenis_perundang_undangan.'"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Peraturan Desa') {
						$where6      = array(
							'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'perdes'
						);
						$this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");    	
						$this->db->where($where6);
						$this->db->limit(1);
						$query6 = $this->db->get('jenis_perundang_undangan');
						$a6 = $query6->num_rows();
						if( $a6 == 0 ){
								$jenis_perundang_undangan='';
						}
						else{
							foreach ($query6->result() as $b6) {
								$jenis_perundang_undangan=$b6->jenis_perundang_undangan_id;
							}
						}
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id='.$jenis_perundang_undangan.'"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } else {
            $hasil .= '
              <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . '' . $url_baca . '/' . $h->id_posting . '">' . $h->judul_posting . '</a>
            ';
          }
        } else {
          $hasil .= '
        <li class="nav-item hs-has-sub-menu u-header__nav-item"
            data-event="hover"
            data-animation-in="slideInUp"
            data-animation-out="fadeOut">
          <a id="pagesMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="pagesSubMenu">' . $h->judul_posting . '</a>

        ';
        }
      } else {

        if ($xx == 0) {
          if ($h->judul_posting == 'Prestasi Entitas Pendidikan') {
            $hasil .= '
              <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/detail/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
              ';
          } elseif ($h->judul_posting == 'Sitemap') {
            $hasil .= '
            <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
            ';
          }elseif ($h->judul_posting == 'Berita') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          }elseif ($h->judul_posting == 'Pengumuman') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } else {
            $hasil .= '
              <li class="nav-item hs-has-sub-menu u-header__nav-item"><a class="nav-link u-header__nav-link" href="' . base_url() . '' . $url_baca . '/' . $h->id_posting . '">' . $h->judul_posting . '</a>
              ';
          }
        } else {
          $hasil .= '
            <li class="nav-item hs-has-sub-menu u-header__nav-item"
                data-event="hover"
                data-animation-in="slideInUp"
                data-animation-out="fadeOut">
              <a id="pagesMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="pagesSubMenu">' . $h->judul_posting . '</a>
    
            ';
        }
      }
      $hasil = $this->menu_atas_front($h->id_posting, $hasil, $a);
      $hasil .= '
      </li>
      ';
    }
    if (($w->num_rows) > 0) {
      $hasil .= "
    </ul>
    ";
    } else { }
    return $hasil;
  }

	public function proses_login()
	{
		$this->form_validation->set_rules('user_name', 'user_name', 'required');
    $this->form_validation->set_rules('password', 'password', 'required');
    $user_name = $this->input->post('user_name');
    $password = $this->input->post('password');
    $web=$this->uut->namadomain(base_url());
    $where = array(
						'users.user_name' => $user_name,
            'users.status' => 1
						);
    $data_user = $this->Login_model->get_login($where);
    $p = $this->bcrypt->hash_password($password);    
		if ($this->form_validation->run() == FALSE)
      {
        echo '[
					{
						"errors":"form_kosong",
						"user_name":"'.$this->input->post('user_name').'",
						"password":"'.$this->input->post('password').'"
					}
					]';
      }
		else
      {
        if(!$data_user)
          {
						echo '[
						{
							"errors":"user_tidak_ada",
							"user_name":"'.$this->input->post('user_name').'",
							"password":"'.$this->input->post('password').'"
						}
						]';
          }
        else
          {
						if ($this->bcrypt->check_password($password, $data_user['password']))
							{
								$this->session->set_userdata( array(
								'id_users' => $data_user['id_users'],
								'user_name' => $data_user['user_name'],
								'hak_akses' => $data_user['hak_akses'],
								'id_skpd' => $data_user['id_skpd'],
								'id_desa' => $data_user['id_desa'],
								'id_kecamatan' => $data_user['id_kecamatan'],
								'id_kabupaten' => $data_user['id_kabupaten'],
								'id_propinsi' => $data_user['id_propinsi'],
								'nama' => $data_user['nama']
								));
								$data_update = array(
									'last_login' => date('Y-m-d H:i:s')
								);
								$where = array(
								'id_users' => $data_user['id_users']
								);      
								$this->Login_model->update_last_login($data_update, $where);
                $_SESSION['ids'] = $data_user['id_users'];
								echo '[{"errors":"valid"}]';
							}
						else
							{
								echo '[{"errors":"miss_match"}]';
							}  
          }
      }
	}	
    
  public function logout(){
		$this->session->sess_destroy();
		return redirect(''.base_url().'sosegov'); 
	}
	
	function ip()
	{
	echo $this->server('REMOTE_ADDR'); exit;
	}
	
}
