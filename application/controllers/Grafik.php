<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grafik extends CI_Controller
{
  
  function __construct()
  {
    parent::__construct();
    //  $this->load->library('fpdf');
		$this->load->library('Bcrypt');
   		$this->load->library('Uut');
		$this->load->library('Crud_model');
    	$this->load->model('Posting_model');
    	$this->load->model('Pegawai_dinkes_model');
  }
  
	function getDatesFromRange($start, $end) {
    $interval = new DateInterval('P1D');
    $realEnd = new DateTime($end);
    $realEnd->add($interval);
    $period = new DatePeriod(
         new DateTime($start),
         $interval,
         $realEnd
    );
    foreach($period as $date) { 
        $array[] = $date->format('Y-m-d'); 
    }
    return $array;
	}
		
  private function menu_atas($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
        if($web == 'zb.wonosobokab.go.id'){
        $hasil .= '<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'website" class="fa-home ">HOME</a></li>'; 
        }
        elseif ($web == 'ppiddemo.wonosobokab.go.id'){
					$hasil .= '
						<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'">HOME</a></li>
						<li class="parent"> <span class="">PPID </span>
							<ul>
								<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid">PPID </a></span></li>
								<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid_pembantu">PPID Pembantu</a></span></li>
							</ul>
						</li>
					';
        }
        else{
        $hasil .= '<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'">HOME</a></li>'; 
        }
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
          if ($xx == 0){
			if($h->judul_posting == 'Data Rekap Kunjungan Pasien'){
			$hasil .= '
		  <li><a href="'.base_url().'grafik/data_rekap_kunjungan_pasien"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Data Kunjungan Hari Ini'){
			$hasil .= '
		  <li><a href="'.base_url().'grafik"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Data Penyakit'){
			$hasil .= '
		  <li><a href="'.base_url().'grafik/data_penyakit"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Data Pegawai'){
			$hasil .= '
		  <li><a href="'.base_url().'grafik/data_pegawai_puskesmas_bar"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}

			elseif($h->judul_posting == 'Data Pegawai Puskesmas'){
			$hasil .= '
		  <li><a href="'.base_url().'grafik/data_pegawai_puskesmas_bar"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}

			else{
			$hasil .= '
		  <li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
			}
          }
          else{
            $hasil .= '
              <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
          }
			}
			else{
        if($h->judul_posting == 'Berita'){
            $hasil .= '
              <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
        }
        elseif($h->judul_posting == 'Artikel'){
            $hasil .= '
              <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
        }
        elseif($h->judul_posting == 'Pengumuman'){
            $hasil .= '
              <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
        }
        elseif($h->judul_posting == 'FAQ'){
            $hasil .= '
              <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
        }
        elseif($h->judul_posting == 'Info'){
            $hasil .= '
              <li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
        }
        else{
          if ($xx == 0){
            $hasil .= '
              <li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
          }
          else{
            $hasil .= '
              <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
          } 
        }
      }
      
			$hasil = $this->menu_atas($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  
	public function index()
	{
		/*
		$this->load->database();
		$query = $this->db->get("skpd");
		echo "<pre>";
		print_r($query->result());


		$second_DB = $this->load->database('database_kedua', TRUE); 
		$query2 = $second_DB->get("puskesmas");
		print_r($query2->result());
		exit;
		*/
	$second_DB = $this->load->database('database_kedua', TRUE);
		
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
			
          
    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['tampil_menu_atas'] = '';
						$data['kata_kunci'] = '';
						$data['gambar'] = 'logo wonosobo.png';
          }
        else
          {
						$data['tampil_menu_atas'] = $d['tampil_menu_atas'];
						$data['judul_posting'] = $d['judul_posting'];
						$data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
						$data['kata_kunci'] = $d['kata_kunci'];
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }     
    $a = 0;
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
		
		$dari_tanggal = trim($this->input->post('dari_tanggal'));
		$sampai_tanggal = trim($this->input->post('sampai_tanggal'));
		$periodik = trim($this->input->post('periodik'));
		$hari = date('Y-m-d');
		$data['hari'] = date('Y-m-d');
		if($periodik == 'periodik'){
			$data['periode'] = 'Tanggal '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' - '.$this->Crud_model->dateBahasaIndo($sampai_tanggal).' ';
			$data['dari_tanggal'] = ''.$dari_tanggal.'';
			$data['sampai_tanggal'] = ''.$sampai_tanggal.'';
			}
		else{
			$data['periode'] = 'Tanggal '.$this->Crud_model->dateBahasaIndo(date('Y-m-d')).'';
			$data['dari_tanggal'] = ''.date('Y-m-d').'';
			$data['sampai_tanggal'] = ''.date('Y-m-d').'';
			}
		
		
		if( empty($dari_tanggal) ){
			$where = array(
			'pus_pendaftaran.status !=' => 99,
			'pus_pendaftaran.tanggal_pendaftaran' => $hari
			);
		}
		else{
		$where = array(
			'pus_pendaftaran.status !=' => 99,
			'pus_pendaftaran.tanggal_pendaftaran >=' => $dari_tanggal,
			'pus_pendaftaran.tanggal_pendaftaran <=' => $sampai_tanggal
			);
		}		
		// $query2 = $second_DB->get("puskesmas");
		$second_DB->select('puskesmas.nama_puskesmas, puskesmas.id_puskesmas, count(pus_pendaftaran.id_pus_pendaftaran) as total');
		$second_DB->join('puskesmas', 'puskesmas.id_puskesmas=pus_pendaftaran.id_puskesmas');
		$second_DB->where($where);
		$second_DB->group_by('pus_pendaftaran.id_puskesmas'); 
		$second_DB->order_by('total', 'desc'); 
		$data['test'] = $second_DB->get('pus_pendaftaran');
		$data['main_view'] = 'grafik_sik/pengunjung_pasien_hari_ini';
		$this->load->view('grafik', $data);
	}
		
	public function data_rekap_kunjungan_pasien()
	{
	$second_DB = $this->load->database('database_kedua', TRUE);
		
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
			
          
    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['tampil_menu_atas'] = '';
						$data['kata_kunci'] = '';
						$data['gambar'] = 'logo wonosobo.png';
          }
        else
          {
						$data['tampil_menu_atas'] = $d['tampil_menu_atas'];
						$data['judul_posting'] = $d['judul_posting'];
						$data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
						$data['kata_kunci'] = $d['kata_kunci'];
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }     
    $a = 0;
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
		
		// $second_DB
		$periodik = trim($this->input->post('periodik'));
		$id_puskesmas = trim($this->input->post('id_puskesmas'));
		
		$where_pus = array(
			'puskesmas.status !=' => 99,
			'puskesmas.jenis_puskesmas' => 'induk',
			'puskesmas.id_kecamatan !=' => '99'
			);
		$second_DB->select('puskesmas.nama_puskesmas, puskesmas.id_puskesmas');
		$second_DB->where($where_pus);
		$second_DB->order_by('puskesmas.nama_puskesmas');
		$x = $second_DB->get('puskesmas');
		$option = '';
		$option .= '<option value="semua">Semua Puskesmas</option>';
		foreach ($x->result() as $x1) 
			{
			if( $x1->id_puskesmas == $id_puskesmas ){
				$option .= '<option value="'.$x1->id_puskesmas.'" selected>'.$x1->nama_puskesmas.'</option>';
				}
			else{
				$option .= '<option value="'.$x1->id_puskesmas.'">'.$x1->nama_puskesmas.'</option>';
				}
			}
		$data['option'] = $option;	
		
		if($periodik == 'periodik'){
			$dari_tanggal = trim($this->input->post('dari_tanggal'));
			$sampai_tanggal = trim($this->input->post('sampai_tanggal'));
			$data['periode'] = 'Dari Tanggal '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' Sampai Tanggal '.$this->Crud_model->dateBahasaIndo($sampai_tanggal).' ';
			$data['dari_tanggal'] = ''.$dari_tanggal.'';
			$data['sampai_tanggal'] = ''.$sampai_tanggal.'';
			}
		else{
			$dari_tanggal = date("Y-m-d",strtotime("-1 week"));
			$sampai_tanggal = date('Y-m-d');
			$data['periode'] = ' Tanggal  : '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' sampai dengan '.$this->Crud_model->dateBahasaIndo(date('Y-m-d')).')';
			$data['dari_tanggal'] = $dari_tanggal;
			$data['sampai_tanggal'] = $sampai_tanggal;
			}
		$a = $this->getDatesFromRange($dari_tanggal, $sampai_tanggal);
		$s = 0;
		$data1 = '';
		$data2 = '';
		$labels = '';
		foreach ($a as $value) {
			$s = $s + 1;
			
			if($periodik == 'periodik'){
					if($id_puskesmas == 'semua'){
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						}
					else{
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						}
				}
			else{
					if($id_puskesmas == 'semua' || empty($id_puskesmas)){
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						}
					else{
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						}
				}
			
			$second_DB->where($where1);
			$second_DB->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
			$second_DB->from('pus_pelayanan');
			$labels .= '"'.$this->Crud_model->dateBahasaIndo($value).'",';
			$data1 .= '"'.$second_DB->count_all_results().'",'; 
			
			$second_DB->where($where2);
			$second_DB->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
			$second_DB->from('pus_pelayanan');
			$data2 .= ''.$second_DB->count_all_results().','; 
		}
		$data['labels'] = $labels;
		$data['data1'] = $data1;
		$data['data2'] = $data2;
		$data['main_view'] = 'grafik_sik/data_rekap_kunjungan_pasien';
		$this->load->view('grafik', $data);
	}
	
	public function tes_data_rekap_kunjungan_pasien()
	{
	$second_DB = $this->load->database('database_kedua', TRUE);
		 
		$puskesmas = $second_DB->query("
		SELECT *
		from puskesmas
		where jenis_puskesmas='induk'
		and status!=99
		");
	  echo $x = $puskesmas->num_rows();
      foreach($puskesmas->result() as $h)
      {
		  echo '
		  	id: '.$h->id_puskesmas.'<br />
		  	nama: '.$h->nama_puskesmas.'<br />
		  	status: '.$h->status.'<br /><br />
		  ';
	  }
		/*
      $data_input = array(
		'id_puskesmas' => 0,
		'tanggal' => $value,
		'jumlah_pasien_mendaftar' => $data1,
		'jumlah_pasien_selesai_pencatatan' => $data2

		);
      $table_name = 'data_rekap_kunjungan_pasien';
      $simpan_jumlah = $this->Crud_model->save_data($data_input, $table_name);
	  */
	}
	public function data_penyakit()
	{
	$second_DB = $this->load->database('database_kedua', TRUE);
		
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
			
          
    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['tampil_menu_atas'] = '';
						$data['kata_kunci'] = '';
						$data['gambar'] = 'logo wonosobo.png';
          }
        else
          {
						$data['tampil_menu_atas'] = $d['tampil_menu_atas'];
						$data['judul_posting'] = $d['judul_posting'];
						$data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
						$data['kata_kunci'] = $d['kata_kunci'];
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }     
    $a = 0;
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
		
		// $second_DB
		$periodik = trim($this->input->post('periodik'));
		$id_puskesmas = trim($this->input->post('id_puskesmas'));
		
		$where_pus = array(
			'puskesmas.status !=' => 99,
			'puskesmas.jenis_puskesmas' => 'induk',
			'puskesmas.id_kecamatan !=' => '99'
			);
		$second_DB->select('puskesmas.nama_puskesmas, puskesmas.id_puskesmas');
		$second_DB->where($where_pus);
		$second_DB->order_by('puskesmas.nama_puskesmas');
		$x = $second_DB->get('puskesmas');
		$option = '';
		$option .= '<option value="semua">Semua Puskesmas</option>';
		foreach ($x->result() as $x1) 
			{
			if( $x1->id_puskesmas == $id_puskesmas ){
				$option .= '<option value="'.$x1->id_puskesmas.'" selected>'.$x1->nama_puskesmas.'</option>';
				}
			else{
				$option .= '<option value="'.$x1->id_puskesmas.'">'.$x1->nama_puskesmas.'</option>';
				}
			}
		$data['option'] = $option;	
		
		if($periodik == 'periodik'){
			$dari_tanggal = trim($this->input->post('dari_tanggal'));
			$sampai_tanggal = trim($this->input->post('sampai_tanggal'));
			$data['periode'] = 'Dari Tanggal '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' Sampai Tanggal '.$this->Crud_model->dateBahasaIndo($sampai_tanggal).' ';
			$data['dari_tanggal'] = ''.$dari_tanggal.'';
			$data['sampai_tanggal'] = ''.$sampai_tanggal.'';
			if( $id_puskesmas == 'semua' ){
					$where = array(
					'pus_pelayanan_icd_x.status' => 1,
					'pus_pendaftaran.tanggal_pendaftaran >=' => $dari_tanggal,
					'pus_pendaftaran.tanggal_pendaftaran <=' => $sampai_tanggal
					);
				}
			else{
					$where = array(
					'pus_pelayanan_icd_x.status' => 1,
					'pus_pendaftaran.id_puskesmas' => $id_puskesmas,
					'pus_pendaftaran.tanggal_pendaftaran >=' => $dari_tanggal,
					'pus_pendaftaran.tanggal_pendaftaran <=' => $sampai_tanggal
					);
				}
			}
		else{
			$dari_tanggal = date('Y-m-d');
			$sampai_tanggal = date('Y-m-d');
			$data['periode'] = ' Tanggal  : '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' ';
			$data['dari_tanggal'] = $dari_tanggal;
			$data['sampai_tanggal'] = $sampai_tanggal;
			
			$where = array(
			'pus_pelayanan_icd_x.status' => 1,
			'pus_pendaftaran.tanggal_pendaftaran' =>  date('Y-m-d')
			);
			
			}
		
		
		$second_DB->select('icd_x.nama_icd_x, COUNT(pus_pelayanan_icd_x.id_icd_x) as total');
		$second_DB->join('icd_x', 'icd_x.id_icd_x=pus_pelayanan_icd_x.id_icd_x');
		$second_DB->join('pus_pelayanan', 'pus_pelayanan.id_pus_pelayanan=pus_pelayanan_icd_x.id_pus_pelayanan');
		$second_DB->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
		$second_DB->where($where);
		$second_DB->group_by('pus_pelayanan_icd_x.id_icd_x'); 
		$second_DB->order_by('total', 'desc'); 
		$second_DB->limit(10); 
		$x = $second_DB->get('pus_pelayanan_icd_x');
		
		$nmr = 0;
		$data1 = '';
		$tabel = '';
		foreach($x->result() as $r1) 
			{
			$nmr = $nmr + 1;
			$data1 .='
			{
					value: '.$r1->total.',
					label: "'.$r1->nama_icd_x.'"
				},
			';
			}
		$data['d'] = $data1;
		
		$second_DB->select('icd_x.nama_icd_x, COUNT(pus_pelayanan_icd_x.id_icd_x) as total');
		$second_DB->join('icd_x', 'icd_x.id_icd_x=pus_pelayanan_icd_x.id_icd_x');
		$second_DB->join('pus_pelayanan', 'pus_pelayanan.id_pus_pelayanan=pus_pelayanan_icd_x.id_pus_pelayanan');
		$second_DB->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
		$second_DB->where($where);
		$second_DB->group_by('pus_pelayanan_icd_x.id_icd_x'); 
		$second_DB->order_by('total', 'desc'); 
		$y = $second_DB->get('pus_pelayanan_icd_x');
		$tabel = '';
		foreach($y->result() as $r2) 
			{
			$tabel .= '<tr>';
			$tabel .= '<td>';
			$tabel .= ''.$r2->nama_icd_x.'';
			$tabel .= '</td>';
			$tabel .= '<td>';
			$tabel .= ''.$r2->total.'';
			$tabel .= '</td>';
			$tabel .= '</tr>';
			
			}
		$data['tabel'] = $tabel;
							
		$data['main_view'] = 'grafik_sik/data_penyakit';
		$this->load->view('grafik', $data);
	}

	// public function data_pegawai()
	// {		
	// 	$web=$this->uut->namadomain(base_url());
 //    $where0 = array(
 //      'domain' => $web
 //      );
 //    $this->db->where($where0);
 //    $this->db->limit(1);
 //    $query0 = $this->db->get('dasar_website');
 //    foreach ($query0->result() as $row0)
 //      {
 //        $data['domain'] = $row0->domain;
 //        $data['alamat'] = $row0->alamat;
 //        $data['telpon'] = $row0->telpon;
 //        $data['email'] = $row0->email;
 //        $data['twitter'] = $row0->twitter;
 //        $data['facebook'] = $row0->facebook;
 //        $data['google'] = $row0->google;
 //        $data['instagram'] = $row0->instagram;
 //        $data['peta'] = $row0->peta;
 //        $data['keterangan'] = $row0->keterangan;
 //      }
			
          
 //    $where = array(
	// 					'id_posting' => $this->uri->segment(3),
 //            'status' => 1
	// 					);
 //    $d = $this->Posting_model->get_data($where);
 //    if(!$d)
 //          {
	// 					$data['judul_posting'] = '';
	// 					$data['isi_posting'] = '';
	// 					$data['tampil_menu_atas'] = '';
	// 					$data['kata_kunci'] = '';
	// 					$data['gambar'] = 'logo wonosobo.png';
 //          }
 //        else
 //          {
	// 					$data['tampil_menu_atas'] = $d['tampil_menu_atas'];
	// 					$data['judul_posting'] = $d['judul_posting'];
	// 					$data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
	// 					$data['kata_kunci'] = $d['kata_kunci'];
 //            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
 //          }
 //    $where1 = array(
 //      'domain' => $web
 //      );
 //    $this->db->where($where1);
 //    $query1 = $this->db->get('komponen');
 //    foreach ($query1->result() as $row1)
 //      {
 //        if( $row1->judul_komponen == 'Header' ){ //Header
 //          $data['Header'] = $row1->isi_komponen;
 //        }
 //        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
 //          $data['KolomKiriAtas'] = $row1->isi_komponen;
 //        }
 //        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
 //          $data['KolomKananAtas'] = $row1->isi_komponen;
 //        }
 //        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
 //          $data['KolomKiriBawah'] = $row1->isi_komponen;
 //        }
 //        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
 //          $data['KolomPalingBawah'] = $row1->isi_komponen;
 //        }
 //        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
 //          $data['KolomKananBawah'] = $row1->isi_komponen;
 //        }
 //        else{ 
 //        }
 //      }     
 //    $a = 0;
 //    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);

 //    $nmr = 0;
 //    $data1 = '';
	// $data2 = '';



 //   $x = $this->db->query("SELECT msPegawaiUnitKerja, COUNT(msPegawaiUnitKerja) as total from tabel_dinkes_pegawai_instansi
 //   	group by msPegawaiUnitKerja 
 //                          ");

	// 	foreach($x->result() as $r1) 
	// 		{
	// 		$nmr = $nmr + 1;
	// 		$data1 .='
	// 		{
	// 				value: '.$r1->total.',
	// 				label: "'.$r1->msPegawaiUnitKerja.'"
	// 			},
	// 		';
	// 		}
	// 	$data['d'] = $data1;





	// 	$y = $this->db->query("SELECT * from tabel_dinkes_pegawai_instansi
 //                          ");

	// 	// $this->db->select('icd_x.nama_icd_x, COUNT(pus_pelayanan_icd_x.id_icd_x) as total');
		
	// 	// $second_DB->where($where);
	// 	// $second_DB->group_by('pus_pelayanan_icd_x.id_icd_x'); 
	// 	// $second_DB->order_by('total', 'desc'); 
	// 	// $y = $second_DB->get('pus_pelayanan_icd_x');
	// 	$tabel = '';
	// 	foreach($y->result() as $r2) 
	// 		{
	// 		$tabel .= '<tr>';
	// 		$tabel .= '<td>';
	// 		$tabel .= ''.$r2->msPegawaiNip.'';
	// 		$tabel .= '</td>';
	// 		$tabel .= '<td>';
	// 		$tabel .= ''.$r2->msPegawaiNama.'';
	// 		$tabel .= '</td>';
	// 		$tabel .= '</tr>';
			
	// 		}

	// 	$data['tabel'] = $tabel;
							
	// 	$data['main_view'] = 'modul/dinkes/data_pegawai';
	// 	$this->load->view('grafik', $data);
	// }

	public function data_pegawai()
	{		
		$web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
			
          
    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['tampil_menu_atas'] = '';
						$data['kata_kunci'] = '';
						$data['gambar'] = 'logo wonosobo.png';
          }
        else
          {
						$data['tampil_menu_atas'] = $d['tampil_menu_atas'];
						$data['judul_posting'] = $d['judul_posting'];
						$data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
						$data['kata_kunci'] = $d['kata_kunci'];
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }     
    $a = 0;
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);

    $nmr = 0;
    $data1 = '';
	$data2 = '';

	$where_bar = array(
		'status != ' => 99 
	);
	$data_bar = $this->Pegawai_dinkes_model->get_data($where_bar)->result();
   
		$data['dataBar'] = json_encode($data_bar);

		$y = $this->db->query("SELECT msPegawaiUnitKerja, COUNT(msPegawaiUnitKerja) as total 
								from tabel_dinkes_pegawai_instansi
								where status != 99
								GROUP BY msPegawaiUnitKerja DESC
								order by total desc
                          ");

		// $this->db->select('icd_x.nama_icd_x, COUNT(pus_pelayanan_icd_x.id_icd_x) as total');
		
		// $second_DB->where($where);
		// $second_DB->group_by('pus_pelayanan_icd_x.id_icd_x'); 
		// $second_DB->order_by('total', 'desc'); 
		// $y = $second_DB->get('pus_pelayanan_icd_x');
		$tabel = '';
		$no = 1;		
		foreach($y->result() as $r2) 
			{
			$tabel .= '<tr>';
			$tabel .= '<td class="text-center">';
			$tabel .= ''.$no.'';
			$tabel .= '</td>';
			$tabel .= '<td>';
			$tabel .= ''.$r2->msPegawaiUnitKerja.'';
			$tabel .= '</td>';
			$tabel .= '<td class="text-center">';
			$tabel .= ''.$r2->total.'';
			$tabel .= '</td>';
			$tabel .= '</tr>';
			 $no++;
			}


		$data['tabel'] = $tabel;

		// penididikan
		$where_pendidikan = array(
			'status != ' => 99 ,
			'msPegawaiPendidikan != ' => ''
		);
		$data_pendidikan = $this->Pegawai_dinkes_model->get_data_pendidikan($where_pendidikan)->result();
   
		$data['dataPendidikan'] = json_encode($data_pendidikan);

		$sql_pendidikan = $this->db->query("SELECT msPegawaiPendidikan, COUNT(msPegawaiPendidikan) as total 
								from tabel_dinkes_pegawai_instansi
								WHERE msPegawaiPendidikan !=''
								AND status !='99'
								GROUP BY msPegawaiPendidikan DESC
								order by total desc
                          ");
		
		$tabel_pendidikan = '';
		$no = 1;		
		foreach($sql_pendidikan->result() as $r2) 
			{
			$tabel_pendidikan .= '<tr>';
			$tabel_pendidikan .= '<td class="text-center">';
			$tabel_pendidikan .= ''.$no.'';
			$tabel_pendidikan .= '</td>';
			$tabel_pendidikan .= '<td>';
			$tabel_pendidikan .= ''.$r2->msPegawaiPendidikan.'';
			$tabel_pendidikan .= '</td>';
			$tabel_pendidikan .= '<td class="text-center">';
			$tabel_pendidikan .= ''.$r2->total.'';
			$tabel_pendidikan .= '</td>';
			$tabel_pendidikan .= '</tr>';
			 $no++;
			}


		$data['tabelPendidikan'] = $tabel_pendidikan;
		// akhir pendidikan
							
		$data['main_view'] = 'modul/dinkes/data_pegawai_bar';
		$this->load->view('grafik', $data);
	}

	public function data_pegawai_bar()
	{		
		$web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
			
          
    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['tampil_menu_atas'] = '';
						$data['kata_kunci'] = '';
						$data['gambar'] = 'logo wonosobo.png';
          }
        else
          {
						$data['tampil_menu_atas'] = $d['tampil_menu_atas'];
						$data['judul_posting'] = $d['judul_posting'];
						$data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
						$data['kata_kunci'] = $d['kata_kunci'];
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }     
    $a = 0;
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);

    $nmr = 0;
    $data1 = '';
	$data2 = '';

	$where_bar = array(
		'status != ' => 99 
	);
	$data_bar = $this->Pegawai_dinkes_model->get_data($where_bar)->result();
   
		$data['dataBar'] = json_encode($data_bar);

		$y = $this->db->query("SELECT msPegawaiUnitKerja, COUNT(msPegawaiUnitKerja) as total 
								from tabel_dinkes_pegawai_instansi
								where status != 99
								GROUP BY msPegawaiUnitKerja DESC
								order by total desc
                          ");

		// $this->db->select('icd_x.nama_icd_x, COUNT(pus_pelayanan_icd_x.id_icd_x) as total');
		
		// $second_DB->where($where);
		// $second_DB->group_by('pus_pelayanan_icd_x.id_icd_x'); 
		// $second_DB->order_by('total', 'desc'); 
		// $y = $second_DB->get('pus_pelayanan_icd_x');
		$tabel = '';
		$no = 1;		
		foreach($y->result() as $r2) 
			{
			$tabel .= '<tr>';
			$tabel .= '<td class="text-center">';
			$tabel .= ''.$no.'';
			$tabel .= '</td>';
			$tabel .= '<td>';
			$tabel .= ''.$r2->msPegawaiUnitKerja.'';
			$tabel .= '</td>';
			$tabel .= '<td class="text-center">';
			$tabel .= ''.$r2->total.'';
			$tabel .= '</td>';
			$tabel .= '</tr>';
			 $no++;
			}


		$data['tabel'] = $tabel;

		// penididikan
		$where_pendidikan = array(
			'status != ' => 99 ,
			'msPegawaiPendidikan != ' => ''
		);
		$data_pendidikan = $this->Pegawai_dinkes_model->get_data_pendidikan($where_pendidikan)->result();
   
		$data['dataPendidikan'] = json_encode($data_pendidikan);

		$sql_pendidikan = $this->db->query("SELECT msPegawaiPendidikan, COUNT(msPegawaiPendidikan) as total 
								from tabel_dinkes_pegawai_instansi
								WHERE msPegawaiPendidikan !=''
								AND status !='99'
								GROUP BY msPegawaiPendidikan DESC
								order by total desc
                          ");
		
		$tabel_pendidikan = '';
		$no = 1;		
		foreach($sql_pendidikan->result() as $r2) 
			{
			$tabel_pendidikan .= '<tr>';
			$tabel_pendidikan .= '<td class="text-center">';
			$tabel_pendidikan .= ''.$no.'';
			$tabel_pendidikan .= '</td>';
			$tabel_pendidikan .= '<td>';
			$tabel_pendidikan .= ''.$r2->msPegawaiPendidikan.'';
			$tabel_pendidikan .= '</td>';
			$tabel_pendidikan .= '<td class="text-center">';
			$tabel_pendidikan .= ''.$r2->total.'';
			$tabel_pendidikan .= '</td>';
			$tabel_pendidikan .= '</tr>';
			 $no++;
			}


		$data['tabelPendidikan'] = $tabel_pendidikan;
		// akhir pendidikan
							
		$data['main_view'] = 'modul/dinkes/data_pegawai_bar';
		$this->load->view('grafik', $data);
	}

	public function data_pegawai_puskesmas_bar()
	{		
		$web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
			
          
    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['tampil_menu_atas'] = '';
						$data['kata_kunci'] = '';
						$data['gambar'] = 'logo wonosobo.png';
          }
        else
          {
						$data['tampil_menu_atas'] = $d['tampil_menu_atas'];
						$data['judul_posting'] = $d['judul_posting'];
						$data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
						$data['kata_kunci'] = $d['kata_kunci'];
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }     
    $a = 0;
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);

    $id_instansi = trim($this->input->post('id_puskesmas'));
    if ($id_instansi=='') {
    	$id_puskesmas =0;
    }else{
    	$id_puskesmas =trim($this->input->post('id_puskesmas'));
    }

    $nmr = 0;
    $data1 = '';
	$data2 = '';

	if ($id_puskesmas==0) {
		$where_bar = array(
			'status != ' => 99 
			// 'msPegawaiIdInstansi !=' => 298
		);

		$order_bar = array(
			'msPegawaiIdInstansi ASC'
		);

		$y = $this->db->query("SELECT msPegawaiJabatan, COUNT(msPegawaiJabatan) as total 
								from tabel_dinkes_pegawai_instansi
								where status != 99
								-- and msPegawaiIdInstansi != 298
								GROUP BY msPegawaiJabatan
								order by msPegawaiJabatan ASC
                          ");

		$sql_pegawai = $this->db->query("SELECT * 
								from tabel_dinkes_pegawai_instansi
								where status != 99
								-- and msPegawaiIdInstansi != 298								
								order by msPegawaiNama ASC
                          ");

	}else{
		$where_bar = array(
			'status != ' => 99 ,
			'msPegawaiIdInstansi =' => $id_puskesmas
			// 'msPegawaiIdInstansi !=' => 298
		);

		$order_bar = array(
			'msPegawaiIdInstansi ASC'
		);

		$y = $this->db->query("SELECT msPegawaiJabatan, COUNT(msPegawaiJabatan) as total 
								from tabel_dinkes_pegawai_instansi
								where status != 99
								-- and msPegawaiIdInstansi != 298
								and msPegawaiIdInstansi = $id_puskesmas
								GROUP BY msPegawaiJabatan
								order by msPegawaiJabatan ASC
                          ");

		$sql_pegawai = $this->db->query("SELECT * 
								from tabel_dinkes_pegawai_instansi
								where status != 99
								-- and msPegawaiIdInstansi != 298
								and msPegawaiIdInstansi = $id_puskesmas
								order by msPegawaiNama ASC
                          ");

	}

	$data_bar = $this->Pegawai_dinkes_model->get_data_pegawai_puskesmas($where_bar)->result();
   
	$data['dataBar'] = json_encode($data_bar);
		
	$tabel = '';
	$no = 1;		
	foreach($y->result() as $r2) 
	{
		$tabel .= '<tr>';
		$tabel .= '<td class="text-center">';
		$tabel .= ''.$no.'';
		$tabel .= '</td>';
		$tabel .= '<td>';
		$tabel .= ''.$r2->msPegawaiJabatan.'';
		$tabel .= '</td>';
		$tabel .= '<td class="text-center">';
		$tabel .= ''.$r2->total.'';
		$tabel .= '</td>';
		$tabel .= '</tr>';
		 $no++;
	}


	$data['tabel'] = $tabel;

	$tabel_pegawai = '';
	$no = 1;		
	foreach($sql_pegawai->result() as $r2) 
	{
		$tabel_pegawai .= '<tr>';
		$tabel_pegawai .= '<td class="text-center">';
		$tabel_pegawai .= ''.$no.'';
		$tabel_pegawai .= '</td>';
		$tabel_pegawai .= '<td>';
		$tabel_pegawai .= ''.$r2->msPegawaiNamaInstansi.'';
		$tabel_pegawai .= '</td>';
		$tabel_pegawai .= '<td>';
		$tabel_pegawai .= ''.$r2->msPegawaiNip.'';
		$tabel_pegawai .= '</td>';		
		$tabel_pegawai .= '<td>';
		$tabel_pegawai .= ''.$r2->msPegawaiNama.'';
		$tabel_pegawai .= '</td>';
		$tabel_pegawai .= '<td>';
		$tabel_pegawai .= ''.$r2->msPegawaiJabatan.'';
		$tabel_pegawai .= '</td>';
		$tabel_pegawai .= '<td>';
		$tabel_pegawai .= ''.$r2->msPegawaiStatus.'';
		$tabel_pegawai .= '</td>';
		$tabel_pegawai .= '</tr>';
		 $no++;
	}


	$data['tabelPegawai'] = $tabel_pegawai;

		
		$sql_option = $this->db->query("SELECT msInstansiId, msInstansiNama
								from tabel_dinkes_instansi
								where status != 99
								-- and msInstansiId != 298
								order by msInstansiNama ASC
	                      ");

		$option = '';
		$option .= '<option value="0">Semua Puskesmas</option>';
		foreach ($sql_option->result() as $x) 
			{
			if( $x->msInstansiId == $id_puskesmas ){
				$option .= '<option value="'.$x->msInstansiId.'" selected>'.$x->msInstansiNama.'</option>';
				}
			else{
				$option .= '<option value="'.$x->msInstansiId.'">'.$x->msInstansiNama.'</option>';
				}
				// $option .= '<option value="'.$x->msInstansiId.'" selected>'.$x->msInstansiNama.'</option>';
			}
		$data['option'] = $option;
		
							
		$data['main_view'] = 'modul/dinkes/data_pegawai_puskesmas_bar';
		$this->load->view('grafik', $data);
	}
	
} 