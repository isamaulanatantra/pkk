<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lampiran_pendaftaran_pasar extends CI_Controller {

	function __construct()
		{
		parent::__construct();
    	$this->load->library('user_agent');
    	$this->load->library('upload', 'image_lib');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
    	$this->load->library('Bcrypt');
    	$this->load->model('Crud_model');
		$this->load->helper('file');
    	$this->load->library('Uut');
		}
  
	public function index()
		{
    }
		
  	public function upload1()
	{
     	$web=$this->uut->namadomain(base_url());
		$agent = $this->agent->browser().' '.$this->agent->version();
		$ip = $this->input->ip_address();
      	if(empty($this->session->userdata('id_users'))){ echo $sesi_pengunjung=0;}else{echo $sesi_pengunjung=$this->session->userdata('id_users');}
      	$config['upload_path'] = './media/lampiran_pendaftaran_pasar/';
		$config['allowed_types'] = 'jpg|png|pdf|jpeg';
		$config['max_size']	= '50000';
		$this->upload->initialize($config);
		$uploadFiles = array('img_1' => 'myfile1', );		
		//$uploadFiles = array('img_1' => 'myfile1', 'img_2' => 'e_myfile', );		
		$this->load->library('image_lib');
		$newName = '-';
		$mode = $this->input->post('mode1');
		$this->form_validation->set_rules('remake1', 'remake1', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			echo '
				<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
				Mohon Keterangan Lampiran Diisi
				</div>
			';
		}else{
			foreach($uploadFiles as $key => $files)
			{
				if ($this->upload->do_upload($files)) 
				{
					$upload = $this->upload->data();
					$file = explode(".", $upload['file_name']);
					$prefix = date('Ymdhis');
					$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
					$filePath =  $upload['file_path'].$newName;
					rename($upload['full_path'],$filePath);
					if($file[1] == 'php'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php5'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php4'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php3'){ echo 'Asem Lu'; exit;}
					if( $mode == 'edit' ){
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake1'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp1'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}else{
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake1'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp1'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}
					$table_name = 'lampiran_pendaftaran_pasar';
					$id = $this->Crud_model->save_data($data_input, $table_name);
					$config['image_library'] = 'gd2';
					$config['source_image']  = './media/lampiran_pendaftaran_pasar/'.$newName.'';
					$config['new_image']  = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';						
					$config['width']	 = 300;
					$config['height']	= 300;
					$this->image_lib->initialize($config); 
					$this->image_lib->resize();
					$file = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';
					$j = get_mime_by_extension($file);
					$ex = explode('/', $j);
					$e = $ex[0];
					echo '
						<div class="alert alert-success alert-dismissable" id="img_upload">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					'; 
					if( $e == 'image'){
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"><img src="'.base_url().'media/lampiran_pendaftaran_pasar/s_'.$newName.'" /></a>';
					}else{
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"> '.$newName.' </a>';
					}
					echo '</div>';
				}
			}
		}
	}

  	public function upload2()
	{
     	$web=$this->uut->namadomain(base_url());
		$agent = $this->agent->browser().' '.$this->agent->version();
		$ip = $this->input->ip_address();
      	if(empty($this->session->userdata('id_users'))){ echo $sesi_pengunjung=0;}else{echo $sesi_pengunjung=$this->session->userdata('id_users');}
      	$config['upload_path'] = './media/lampiran_pendaftaran_pasar/';
		$config['allowed_types'] = 'jpg|png|pdf|jpeg';
		$config['max_size']	= '50000';
		$this->upload->initialize($config);
		$uploadFiles = array('img_1' => 'myfile2', );		
		//$uploadFiles = array('img_1' => 'myfile2', 'img_2' => 'e_myfile', );	
		$this->load->library('image_lib');
		$newName = '-';
		$mode = $this->input->post('mode2');
		$this->form_validation->set_rules('remake2', 'remake2', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			echo '
				<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
				Mohon Keterangan Lampiran Diisi
				</div>
			';
		}else{
			foreach($uploadFiles as $key => $files)
			{
				if ($this->upload->do_upload($files)) 
				{
					$upload = $this->upload->data();
					$file = explode(".", $upload['file_name']);
					$prefix = date('Ymdhis');
					$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
					$filePath =  $upload['file_path'].$newName;
					rename($upload['full_path'],$filePath);
					if($file[1] == 'php'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php5'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php4'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php3'){ echo 'Asem Lu'; exit;}
					if( $mode == 'edit' ){
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake2'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp2'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}else{
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake2'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp2'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}
					$table_name = 'lampiran_pendaftaran_pasar';
					$id = $this->Crud_model->save_data($data_input, $table_name);
					$config['image_library'] = 'gd2';
					$config['source_image']  = './media/lampiran_pendaftaran_pasar/'.$newName.'';
					$config['new_image']  = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';						
					$config['width']	 = 300;
					$config['height']	= 300;
					$this->image_lib->initialize($config); 
					$this->image_lib->resize();
					$file = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';
					$j = get_mime_by_extension($file);
					$ex = explode('/', $j);
					$e = $ex[0];
					echo '
						<div class="alert alert-success alert-dismissable" id="img_upload">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					'; 
					if( $e == 'image'){
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"><img src="'.base_url().'media/lampiran_pendaftaran_pasar/s_'.$newName.'" /></a>';
					}else{
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"> '.$newName.' </a>';
					}
					echo '</div>';
				}
			}
		}
	}
		
  	public function upload3()
	{
     	$web=$this->uut->namadomain(base_url());
		$agent = $this->agent->browser().' '.$this->agent->version();
		$ip = $this->input->ip_address();
      	if(empty($this->session->userdata('id_users'))){ echo $sesi_pengunjung=0;}else{echo $sesi_pengunjung=$this->session->userdata('id_users');}
      	$config['upload_path'] = './media/lampiran_pendaftaran_pasar/';
		$config['allowed_types'] = 'jpg|png|pdf|jpeg';
		$config['max_size']	= '50000';
		$this->upload->initialize($config);
		$uploadFiles = array('img_1' => 'myfile3', );		
		//$uploadFiles = array('img_1' => 'myfile3', 'img_2' => 'e_myfile', );		
		$this->load->library('image_lib');
		$newName = '-';
		$mode = $this->input->post('mode3');
		$this->form_validation->set_rules('remake3', 'remake3', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			echo '
				<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
				Mohon Keterangan Lampiran Diisi
				</div>
			';
		}else{
			foreach($uploadFiles as $key => $files)
			{
				if ($this->upload->do_upload($files)) 
				{
					$upload = $this->upload->data();
					$file = explode(".", $upload['file_name']);
					$prefix = date('Ymdhis');
					$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
					$filePath =  $upload['file_path'].$newName;
					rename($upload['full_path'],$filePath);
					if($file[1] == 'php'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php5'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php4'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php3'){ echo 'Asem Lu'; exit;}
					if( $mode == 'edit' ){
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake3'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp3'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}else{
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake3'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp3'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}
					$table_name = 'lampiran_pendaftaran_pasar';
					$id = $this->Crud_model->save_data($data_input, $table_name);
					$config['image_library'] = 'gd2';
					$config['source_image']  = './media/lampiran_pendaftaran_pasar/'.$newName.'';
					$config['new_image']  = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';						
					$config['width']	 = 300;
					$config['height']	= 300;
					$this->image_lib->initialize($config); 
					$this->image_lib->resize();
					$file = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';
					$j = get_mime_by_extension($file);
					$ex = explode('/', $j);
					$e = $ex[0];
					echo '
						<div class="alert alert-success alert-dismissable" id="img_upload">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					'; 
					if( $e == 'image'){
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"><img src="'.base_url().'media/lampiran_pendaftaran_pasar/s_'.$newName.'" /></a>';
					}else{
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"> '.$newName.' </a>';
					}
					echo '</div>';
				}
			}
		}
	}
		
  	public function upload4()
	{
     	$web=$this->uut->namadomain(base_url());
		$agent = $this->agent->browser().' '.$this->agent->version();
		$ip = $this->input->ip_address();
      	if(empty($this->session->userdata('id_users'))){ echo $sesi_pengunjung=0;}else{echo $sesi_pengunjung=$this->session->userdata('id_users');}
      	$config['upload_path'] = './media/lampiran_pendaftaran_pasar/';
		$config['allowed_types'] = 'jpg|png|pdf|jpeg';
		$config['max_size']	= '50000';
		$this->upload->initialize($config);
		$uploadFiles = array('img_1' => 'myfile4', 'img_2' => 'e_myfile', );		
		//$uploadFiles = array('img_1' => 'myfile4', 'img_2' => 'e_myfile', );		
		$this->load->library('image_lib');
		$newName = '-';
		$mode = $this->input->post('mode4');
		$this->form_validation->set_rules('remake4', 'remake4', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			echo '
				<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
				Mohon Keterangan Lampiran Diisi
				</div>
			';
		}else{
			foreach($uploadFiles as $key => $files)
			{
				if ($this->upload->do_upload($files)) 
				{
					$upload = $this->upload->data();
					$file = explode(".", $upload['file_name']);
					$prefix = date('Ymdhis');
					$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
					$filePath =  $upload['file_path'].$newName;
					rename($upload['full_path'],$filePath);
					if($file[1] == 'php'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php5'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php4'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php3'){ echo 'Asem Lu'; exit;}
					if( $mode == 'edit' ){
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake4'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp4'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}else{
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake4'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp4'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}
					$table_name = 'lampiran_pendaftaran_pasar';
					$id = $this->Crud_model->save_data($data_input, $table_name);
					$config['image_library'] = 'gd2';
					$config['source_image']  = './media/lampiran_pendaftaran_pasar/'.$newName.'';
					$config['new_image']  = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';						
					$config['width']	 = 300;
					$config['height']	= 300;
					$this->image_lib->initialize($config); 
					$this->image_lib->resize();
					$file = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';
					$j = get_mime_by_extension($file);
					$ex = explode('/', $j);
					$e = $ex[0];
					echo '
						<div class="alert alert-success alert-dismissable" id="img_upload">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					'; 
					if( $e == 'image'){
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"><img src="'.base_url().'media/lampiran_pendaftaran_pasar/s_'.$newName.'" /></a>';
					}else{
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"> '.$newName.' </a>';
					}
					echo '</div>';
				}
			}
		}
	}
		
  	public function upload5()
	{
     	$web=$this->uut->namadomain(base_url());
		$agent = $this->agent->browser().' '.$this->agent->version();
		$ip = $this->input->ip_address();
      	if(empty($this->session->userdata('id_users'))){ echo $sesi_pengunjung=0;}else{echo $sesi_pengunjung=$this->session->userdata('id_users');}
      	$config['upload_path'] = './media/lampiran_pendaftaran_pasar/';
		$config['allowed_types'] = 'jpg|png|pdf|jpeg';
		$config['max_size']	= '50000';
		$this->upload->initialize($config);
		$uploadFiles = array('img_1' => 'myfile5', );		
		//$uploadFiles = array('img_1' => 'myfile5', 'img_2' => 'e_myfile', );		
		$this->load->library('image_lib');
		$newName = '-';
		$mode = $this->input->post('mode5');
		$this->form_validation->set_rules('remake5', 'remake5', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			echo '
				<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
				Mohon Keterangan Lampiran Diisi
				</div>
			';
		}else{
			foreach($uploadFiles as $key => $files)
			{
				if ($this->upload->do_upload($files)) 
				{
					$upload = $this->upload->data();
					$file = explode(".", $upload['file_name']);
					$prefix = date('Ymdhis');
					$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
					$filePath =  $upload['file_path'].$newName;
					rename($upload['full_path'],$filePath);
					if($file[1] == 'php'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php5'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php4'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php3'){ echo 'Asem Lu'; exit;}
					if( $mode == 'edit' ){
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake5'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp5'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}else{
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake5'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp5'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}
					$table_name = 'lampiran_pendaftaran_pasar';
					$id = $this->Crud_model->save_data($data_input, $table_name);
					$config['image_library'] = 'gd2';
					$config['source_image']  = './media/lampiran_pendaftaran_pasar/'.$newName.'';
					$config['new_image']  = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';						
					$config['width']	 = 300;
					$config['height']	= 300;
					$this->image_lib->initialize($config); 
					$this->image_lib->resize();
					$file = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';
					$j = get_mime_by_extension($file);
					$ex = explode('/', $j);
					$e = $ex[0];
					echo '
						<div class="alert alert-success alert-dismissable" id="img_upload">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					'; 
					if( $e == 'image'){
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"><img src="'.base_url().'media/lampiran_pendaftaran_pasar/s_'.$newName.'" /></a>';
					}else{
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"> '.$newName.' </a>';
					}
					echo '</div>';
				}
			}
		}
	}
		
  	public function upload6()
	{
     	$web=$this->uut->namadomain(base_url());
		$agent = $this->agent->browser().' '.$this->agent->version();
		$ip = $this->input->ip_address();
      	if(empty($this->session->userdata('id_users'))){ echo $sesi_pengunjung=0;}else{echo $sesi_pengunjung=$this->session->userdata('id_users');}
      	$config['upload_path'] = './media/lampiran_pendaftaran_pasar/';
		$config['allowed_types'] = 'jpg|png|pdf|jpeg';
		$config['max_size']	= '50000';
		$this->upload->initialize($config);
		$uploadFiles = array('img_1' => 'myfile6', );		
		//$uploadFiles = array('img_1' => 'myfile6', 'img_2' => 'e_myfile', );		
		$this->load->library('image_lib');
		$newName = '-';
		$mode = $this->input->post('mode6');
		$this->form_validation->set_rules('remake6', 'remake6', 'required');
		if ($this->form_validation->run() == FALSE)
		{
			echo '
				<div class="alert alert-danger alert-dismissable">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
				Mohon Keterangan Lampiran Diisi
				</div>
			';
		}else{
			foreach($uploadFiles as $key => $files)
			{
				if ($this->upload->do_upload($files)) 
				{
					$upload = $this->upload->data();
					$file = explode(".", $upload['file_name']);
					$prefix = date('Ymdhis');
					$newName =$prefix.'_'.$file[0].'.'. $file[1]; 
					$filePath =  $upload['file_path'].$newName;
					rename($upload['full_path'],$filePath);
					if($file[1] == 'php'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php5'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php4'){ echo 'Asem Lu'; exit;}
					elseif($file[1] == 'php3'){ echo 'Asem Lu'; exit;}
					if( $mode == 'edit' ){
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake6'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp6'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}else{
						$data_input = array(
							'table_name' => $this->input->get('table_name'),
							'file_name' => $newName,
							'keterangan' => $this->input->post('remake6'),
							'id_tabel' => 0,
							'temp' => $this->input->post('temp6'),
							'uploaded_time' => date('Y-m-d H:i:s'),
							'uploaded_by' => $sesi_pengunjung,
							'from_ip' => $ip,
							'user_agent' => $agent,
							'domain' => $web,
							'id' => $this->input->get('id')
						);
              		}
					$table_name = 'lampiran_pendaftaran_pasar';
					$id = $this->Crud_model->save_data($data_input, $table_name);
					$config['image_library'] = 'gd2';
					$config['source_image']  = './media/lampiran_pendaftaran_pasar/'.$newName.'';
					$config['new_image']  = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';						
					$config['width']	 = 300;
					$config['height']	= 300;
					$this->image_lib->initialize($config); 
					$this->image_lib->resize();
					$file = './media/lampiran_pendaftaran_pasar/s_'.$newName.'';
					$j = get_mime_by_extension($file);
					$ex = explode('/', $j);
					$e = $ex[0];
					echo '
						<div class="alert alert-success alert-dismissable" id="img_upload">
						<i class="fa fa-check"></i>
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					'; 
					if( $e == 'image'){
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"><img src="'.base_url().'media/lampiran_pendaftaran_pasar/s_'.$newName.'" /></a>';
					}else{
						echo '<a target="_blank" href="'.base_url().'media/lampiran_pendaftaran_pasar/'.$newName.'"> '.$newName.' </a>';
					}
					echo '</div>';
				}
			}
		}
	}
    
	public function load_lampiran()
	{
      	$mode = $this->input->post('mode');
      	$where    = array(
			'table_name' => $this->input->post('table'),
			'temp' => $this->input->post('value'),
			'id' => $this->input->post('id')
		);
      	$this->db->select("*");
      	$this->db->where($where);
      	$this->db->order_by('id_lampiran_pendaftaran_pasar');
      	$result = $this->db->get('lampiran_pendaftaran_pasar');
      	echo json_encode($result->result_array());
	}
		
	public function hapus()
	{
		$id = $this->input->post('id');
		$temp = $this->input->post('temp');
      	$where = array(
        'id' => $id,
        'temp' => $temp
        );
      	$this->db->from('lampiran_pendaftaran_pasar');
      	$this->db->where($where);
      	$a = $this->db->count_all_results();
      	if($a == 0){
        	echo 0;
        }else{
        	$this->db->where($where);
        	$this->db->delete('lampiran_pendaftaran_pasar');
        	echo 1;
        }
	}
    
}