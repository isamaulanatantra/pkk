<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Disposisi_surat extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Disposisi_surat_model');
   }
   
  public function index(){
    $data['main_view'] = 'disposisi_surat/home';
    $this->load->view('back_bone1', $data);
	}
  public function cetak_disposisi_surat(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'disposisi_surat/cetak_disposisi_surat';
    $this->load->view('print', $data);
   }
	public function load_disposisi_surat(){
      $mode_disposisi_surat = $this->input->post('mode_disposisi_surat');
      if( $mode_disposisi_surat == 'edit' ){
      $where    = array(
				'id_surat' => $this->input->post('value')
				);
      }
      elseif ( $mode_disposisi_surat == 'edit_disposisi_surat' ){
      $where    = array(
				'temp' => $this->input->post('value')
				);
      }
      else{
      $where    = array(
				'temp' => $this->input->post('value')
				);
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_disposisi_surat');
      $result = $this->db->get('disposisi_surat');
      echo json_encode($result->result_array());
	}
	public function json_all_disposisi_surat(){
    $web=$this->uut->namadomain(base_url());
		$table = 'disposisi_surat';
    $id_surat    = $this->input->post('id_surat');
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where = array(
      'disposisi_surat.status !=' => 99,
      'disposisi_surat.id_surat' => $id_surat
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_disposisi_surat="'.$row->id_disposisi_surat.'" id="'.$row->id_disposisi_surat.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$row->tujuan_disposisi.'</td>';
				echo '<td valign="top">'.$row->isi_disposisi.'</td>';
				echo '<td valign="top">'.$row->sifat.'<br />Batas waktu s.d. '.$this->Crud_model->TanggalBahasaIndo($row->batas_waktu).'</td>';
				echo '<td valign="top">
                <div class="btn-group">
                  <a href="#tab_form_disposisi_surat" data-toggle="tab" class="update_id_disposisi_surat btn btn-info btn-sm"><i class="fa fa-pencil-square-o"></i> Edt</a>
                  <a href="#" id="del_ajax_disposisi_surat" class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i> Del</a>
                </div>
              </td>';
				echo '</tr>';
      }
			/*echo '
      <tr>
        <td valign="top" colspan="10" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'disposisi_surat/cetak_disposisi_surat/?id_surat='.$id_surat.'&page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Anggota Keluarga</a>
        </td>
      </tr>
      ';*/
          
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $id_surat = trim($this->input->post('id_surat'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='demoopd.wonosobokab.go.id'){
      $where0 = array(
        'disposisi_surat.status !=' => 99,
        'disposisi_surat.id_surat' => $id_surat
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where0 = array(
        'disposisi_surat.status !=' => 99,
        'disposisi_surat.id_surat' => $id_surat
        );
    }else{
      $where0 = array(
        'disposisi_surat.status !=' => 99,
        'disposisi_surat.id_surat' => $id_surat
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('disposisi_surat');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_disposisi_surat(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('tujuan_disposisi', 'tujuan_disposisi', 'required');
		$this->form_validation->set_rules('sifat', 'sifat', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'id_surat' => trim($this->input->post('id_surat')),
				'tujuan_disposisi' => trim($this->input->post('tujuan_disposisi')),
        'isi_disposisi' => trim($this->input->post('isi_disposisi')),
        'sifat' => trim($this->input->post('sifat')),
        'batas_waktu' => trim($this->input->post('batas_waktu')),
        'catatan' => trim($this->input->post('catatan')),
        'temp' => trim($this->input->post('temp')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1
				);
      $table_name = 'disposisi_surat';
      $id         = $this->Crud_model->save_data($data_input, $table_name);
      echo $id;
			
     }
	}
	public function get_by_id(){
    $web=$this->uut->namadomain(base_url());
    if($web=='demoopd.wonosobokab.go.id'){
      $where    = array(
        'disposisi_surat.id_disposisi_surat' => $this->input->post('id_disposisi_surat')
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where    = array(
        'disposisi_surat.id_disposisi_surat' => $this->input->post('id_disposisi_surat')
        );
    }else{
      $where    = array(
        'disposisi_surat.id_disposisi_surat' => $this->input->post('id_disposisi_surat')
        );
    }
    $this->db->select("*");
    $this->db->where($where);
    $this->db->order_by('id_disposisi_surat');
    $result = $this->db->get('disposisi_surat');
    echo json_encode($result->result_array());
	}
  public function update_disposisi_surat(){
		$this->form_validation->set_rules('id_disposisi_surat', 'id_disposisi_surat', 'required');
		$this->form_validation->set_rules('tujuan_disposisi', 'tujuan_disposisi', 'required');
    if ($this->form_validation->run() == FALSE){
      echo 0;
		}
    else{
      $data_update = array(
			
				'id_surat' => trim($this->input->post('id_surat')),
				'tujuan_disposisi' => trim($this->input->post('tujuan_disposisi')),
        'isi_disposisi' => trim($this->input->post('isi_disposisi')),
        'sifat' => trim($this->input->post('sifat')),
        'batas_waktu' => trim($this->input->post('batas_waktu')),
        'catatan' => trim($this->input->post('catatan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
			);
      $table_name  = 'disposisi_surat';
      $where       = array(
        'disposisi_surat.id_disposisi_surat' => trim($this->input->post('id_disposisi_surat'))
			);
      $this->Disposisi_surat_model->update_disposisi_surat($data_update, $where, $table_name);
      echo 1;
		}
  }
	public function hapus1(){
		$id_disposisi_surat = $this->input->post('id_disposisi_surat');
		$where = array(
			'id_disposisi_surat' => $id_disposisi_surat,
			'created_by' => $this->session->userdata('id_users')
			);
		$this->db->from('disposisi_surat');
		$this->db->where($where);
		$a = $this->db->count_all_results();
		if($a == 0){
			echo 0;
			}
		else{
			$this->db->where($where);
			$this->db->delete('disposisi_surat');
			echo 1;
			}
	}
  public function hapus(){
    $web=$this->uut->namadomain(base_url());
    $id_disposisi_surat = $this->input->post('id_disposisi_surat');
    $where = array(
      'id_disposisi_surat' => $id_disposisi_surat
      );
    $this->db->from('disposisi_surat');
    $this->db->where($where);
    $a = $this->db->count_all_results();
    if($a == 0){
      echo 0;
    }
    else{
      $data_update = array(
      
        'status' => 99
                
        );
      $table_name  = 'disposisi_surat';
      $this->Disposisi_surat_model->update_disposisi_surat($data_update, $where, $table_name);
      echo 1;
    }
  }
}