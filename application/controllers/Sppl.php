<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Sppl extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Sppl_model');
    
   }
   
  public function index()
  {
    $data['total'] = 10;
    $data['main_view'] = 'sppl/home';
    $this->load->view('back_bone', $data);
  }
  
  public function daftar_sppl()
  {
    $data['total'] = 10;
    $data['main_view'] = 'sppl/daftar_sppl';
    $this->load->view('back_bone', $data);
  }
  
  public function json_all_sppl()
  {
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_sppl   = $this->input->post('urut_data_sppl');
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  *
                  ";
    $where      = array(
      'status !=' => 99
    );
    $order_by   = 'sppl.'.$urut_data_sppl.'';
    echo json_encode($this->Sppl_model->json_all_sppl($where, $limit, $start, $fields, $order_by, $kata_kunci));
  }
  
   public function total_sppl()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->get('limit'));
    $this->db->from('sppl');
    $where    = array(
      'status!=' => 99
      );
    $this->db->where($where);
    $a = $this->db->count_all_results(); 
    echo trim(ceil($a / $limit));
  }
  
  public function import()
  {
    $data['num_rows'] = $this->db->get('sppl')->num_rows();
    $data['total'] = 10;
    $data['main_view'] = 'sppl/import';
    $this->load->view('back_bone', $data);
  }
    
	public function import_data()
  {
		$config = array(
			'upload_path'   => FCPATH.'media/upload/',
			'allowed_types' => 'xls|xlsx|csv'
		);
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('file')) {
			$data = $this->upload->data();
			@chmod($data['full_path'], 0777);

			$this->load->library('Spreadsheet_Excel_Reader');
			$this->spreadsheet_excel_reader->setOutputEncoding('CP1251');

			$this->spreadsheet_excel_reader->read($data['full_path']);
			$sheets = $this->spreadsheet_excel_reader->sheets[0];
			error_reporting(0);

			$data_excel = array();
			for ($i = 2; $i <= $sheets['numRows']; $i++) {
				if ($sheets['cells'][$i][1] == '') break;

				$data_excel[$i - 1]['nama_pemohon']    = $sheets['cells'][$i][1];
				$data_excel[$i - 1]['nama_pimpinan']   = $sheets['cells'][$i][2];
				$data_excel[$i - 1]['alamat_pimpinan'] = $sheets['cells'][$i][3];
				$data_excel[$i - 1]['jenis_usaha_kegiatan'] = $sheets['cells'][$i][4];
				$data_excel[$i - 1]['kapasitas_produksi'] = $sheets['cells'][$i][5];
				$data_excel[$i - 1]['nama_usaha'] = $sheets['cells'][$i][6];
				$data_excel[$i - 1]['alamat_usaha'] = $sheets['cells'][$i][7];
				$data_excel[$i - 1]['nomor_sppl'] = $sheets['cells'][$i][8];
			}

			$this->db->insert_batch('sppl', $data_excel);
			// @unlink($data['full_path']);
			redirect('sppl/import');
		}
	}


 }