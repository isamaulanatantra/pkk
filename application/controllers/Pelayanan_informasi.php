<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pelayanan_informasi extends CI_Controller
 {
  function __construct(){
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Permohonan_informasi_publik_model');
   }
  public function index() {
    $data['total'] = 10;
    $data['main_view'] = 'pelayanan_informasi/home';
    $this->load->view('tes', $data);
	}
  function load_table_pelayanan_informasi(){
    $web=$this->uut->namadomain(base_url());
    $tahun=$this->input->post('tahun');
    $jenis_permohonan=$this->input->post('jenis_permohonan');
		if($web=='ppid.wonosobokab.go.id'){
			$web_aduan='and status=1';
		}
		else{
			$web_aduan="and domain ='".$web."' and status=1";
		}
		if($jenis_permohonan==''){
			$web_jenis_permohonan="and kategori_permohonan_informasi_publik = 'Pelayanan Informasi' and status=1";
		}
		elseif($jenis_permohonan=='Pelayanan Informasi'){
			$web_jenis_permohonan="and kategori_permohonan_informasi_publik = 'Pelayanan Informasi' and status=1";
		}
		elseif($jenis_permohonan=='Pengaduan Masyarakat'){
			$web_jenis_permohonan="and kategori_permohonan_informasi_publik = 'Pengaduan Masyarakat' and status=1";
		}
		elseif($jenis_permohonan=='Permohonan Informasi Publik'){
			$web_jenis_permohonan="and kategori_permohonan_informasi_publik = 'Permohonan Informasi Publik' and status=1";
		}
		else{
			$web_jenis_permohonan="and tujuan_penggunaan_informasi != 'Pengaduan Masyarakat' and status=1 and status!=2";
		}
		$w = $this->db->query("
		SELECT *, 
		(select user_name from users where users.id_users=permohonan_informasi_publik.created_by) as nama_pengguna, 
		(select judul_posting from posting where posting.id_posting=permohonan_informasi_publik.id_posting) as judul_posting_permohonan
		from permohonan_informasi_publik
		where parent = 0
		".$web_jenis_permohonan."
		and domain ='".$web."'
		and created_time
		like '".$tahun."%'
		order by created_time desc
		");
		
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h){
			$nomor++;
			$tujuan = $h->tujuan_penggunaan_informasi;
			if($tujuan=='Komentar'){
			$costum = 'warning';
			$fa = 'comments';
			}
			elseif($tujuan=='Pengaduan Masyarakat'){
			$costum = 'success';
			$fa = 'user';
			}
			else{
			$costum = 'primary';
			$fa = 'envelope';
			}
			echo '
				<tr id_permohonan_informasi_publik="'.$h->id_permohonan_informasi_publik.'" id="'.$h->id_permohonan_informasi_publik.'">
					<td style="padding: 2px;">
						'.$nomor.'
					</td>
					<td style="padding: 2px;">
						<ul class="nav nav-pills flex-column">
							<li class="nav-item">
								<a href="#" class="nav-link">
									Nama:
									<span class="float-right text-success">
										'.$h->nama.'
									</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link">
									Instansi:
									<span class="float-right text-success">
										'.$h->instansi.'
									</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link">
									Alamat:
									<span class="float-right text-success">
										'.$h->alamat.'
									</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link">
									Pekerjaan:
									<span class="float-right text-success">
										'.$h->pekerjaan.'
									</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link">
									No. Telp:
									<span class="float-right text-success">
										'.$h->nomor_telp.'
									</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link">
									Email:
									<span class="float-right text-success">
										'.$h->email.'
									</span>
								</a>
							</li>
							<li class="nav-item">
								<a href="#" class="nav-link">
									Tanggal:
									<span class="float-right text-warning">
										'.$this->Crud_model->dateBahasaIndo1($h->created_time).'
									</span>
								</a>
							</li>
						</ul>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->rincian_informasi_yang_diinginkan.'</p>
						<p>';
            $wq = $this->db->query("
            SELECT *, (select nama from users where users.id_users=permohonan_informasi_publik.created_by) as nama_pengguna FROM permohonan_informasi_publik WHERE parent = ".$h->id_permohonan_informasi_publik."
            ");
            foreach($wq->result() as $hq){
              echo'<b>Balasan</b> <br />'.$hq->rincian_informasi_yang_diinginkan.' by. <u>'.$hq->nama_pengguna.'</u>';
            }
            echo'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->tujuan_penggunaan_informasi.'</p>
					</td>
					<td style="padding: 2px;">
						<p><a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i> Edit</a></p>
						<p><a href="#" id="del_ajax"><i class="fa fa-cut"></i> Hapus</a></p>
						<p>';if($h->balas==1){echo'<span class="bg-success"><i class="fa fa-thumbs-o-up"></i> Selesai</span>';}else{echo'<span class="bg-danger">Belum</span>';}echo'</p>';
						if($web=='ppid.wonosobokab.go.id'){
							echo'
								<p><a href="#" id="tombol_forward"><i class="fa fa-mail-forward"></i> Forward</a></p>
							';
						}
						else{
							echo'';
						}
						echo'
					</td>
				</tr>
			';
		}
	}
  public function load_jenis_permohonan() {
      echo '
							<option value="Pelayanan Informasi">PELAYANAN INFORMASI</option>
							<option value="Pengaduan Masyarakat">PENGADUAN MASYARAKAT</option>
							<option value="Permohonan Informasi Publik">PERMOHONAN INFORMASI PUBLIK</option>
			';
	}
  public function jenis_permohonan() {
    $jenis_permohonan = trim($this->input->post('jenis_permohonan'));
		if($jenis_permohonan=='Pengaduan Masyarakat'){
      echo 'Pengaduan Masyarakat';
		}
		elseif($jenis_permohonan==''){
      echo 'Pelayanan Informasi';
		}
		elseif($jenis_permohonan=='Pelayanan Informasi'){
      echo 'Pelayanan Informasi';
		}
		else{
      echo 'Permohonan Informasi Publik';
		}
	}
  public function total_pelayanan_informasi() {
    $web=$this->uut->namadomain(base_url());
    $tahun = trim($this->input->post('tahun'));
    $jenis_permohonan = trim($this->input->post('jenis_permohonan'));
    $table_name = 'permohonan_informasi_publik';
		$b = $this->total_hit_pelayanan_informasi($tahun, $jenis_permohonan);
      echo $b;
	}
  function total_hit_pelayanan_informasi($tahun, $jenis_permohonan) {
      $web=$this->uut->namadomain(base_url());
      $tahun = trim($tahun);
      $jenis_permohonan = trim($jenis_permohonan);
      $this->db->from('permohonan_informasi_publik');
      if($jenis_permohonan==''){
      $where    = array(
        'status' => 1,
        'parent' => 0,
        'domain' => $web,
        'kategori_permohonan_informasi_publik' => 'Pelayanan Informasi',
        'created_time'
				);
      }
      else{
      $where    = array(
        'status' => 1,
        'parent' => 0,
        'domain' => $web,
        'kategori_permohonan_informasi_publik' => ''.$jenis_permohonan.'',
        'created_time'
				);
      }
      $this->db->where($where);
			$this->db->like('created_time', $tahun); 
      $a = $this->db->count_all_results(); 
      return $a;
	}
  public function hapus(){
      $web=$this->uut->namadomain(base_url());
			$id_permohonan_informasi_publik = $this->input->post('id_permohonan_informasi_publik');
      $where = array(
        'id_permohonan_informasi_publik' => $id_permohonan_informasi_publik,
        'domain' => $web
        );
      $this->db->from('permohonan_informasi_publik');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'permohonan_informasi_publik';
        $this->Permohonan_informasi_publik_model->update_data_permohonan_informasi_publik($data_update, $where, $table_name);
        echo 1;
        }
		}
  public function simpan_pelayanan_informasi()   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('nomor_telp', 'nomor_telp', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
				
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'forward' => '',
				'id_posting' => 0,
				'parent' => 0,
				'nama' => $this->input->post('nama'),
				'instansi' => $this->input->post('instansi'),
				'pembimbing' => $this->input->post('pembimbing'),
				'alamat' => $this->input->post('alamat'),
				'pekerjaan' => $this->input->post('pekerjaan'),
				'nomor_telp' => $this->input->post('nomor_telp'),
				'email' => $this->input->post('email'),
				'rincian_informasi_yang_diinginkan' => $this->input->post('rincian_informasi_yang_diinginkan'),
				'tujuan_penggunaan_informasi' => $this->input->post('tujuan_penggunaan_informasi'),
				'cara_melihat_membaca_mendengarkan_mencatat' => $this->input->post('cara_melihat_membaca_mendengarkan_mencatat'),
				'cara_hardcopy_softcopy' => $this->input->post('cara_hardcopy_softcopy'),
				'cara_mengambil_langsung' => $this->input->post('cara_mengambil_langsung'),
				'cara_kurir' => $this->input->post('cara_kurir'),
				'cara_pos' => $this->input->post('cara_pos'),
				'cara_faksimili' => $this->input->post('cara_faksimili'),
				'cara_email' => $this->input->post('cara_email'),
				'kategori_permohonan_informasi_publik' => $this->input->post('kategori_permohonan_informasi_publik'),
        'temp' => $this->input->post('temp'),
				'created_by' => $this->session->userdata('id_users'),
        'created_time' => $this->input->post('created_time'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1,
        'balas' => 2

				);
      $table_name = 'permohonan_informasi_publik';
      $id         = $this->Permohonan_informasi_publik_model->simpan_permohonan_informasi_publik($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'permohonan_informasi_publik',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
	public function get_by_id(){
    $web=$this->uut->namadomain(base_url());
    $where    = array(
      'permohonan_informasi_publik.id_permohonan_informasi_publik' => $this->input->post('id_permohonan_informasi_publik')
      );
    $this->db->select("*");
    $this->db->where($where);
    $this->db->order_by('id_permohonan_informasi_publik');
    $result = $this->db->get('permohonan_informasi_publik');
    echo json_encode($result->result_array());
	}
  public function update_permohonan_informasi_publik(){
		$this->form_validation->set_rules('id_permohonan_informasi_publik', 'id_permohonan_informasi_publik', 'required');
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('nomor_telp', 'nomor_telp', 'required');
    if ($this->form_validation->run() == FALSE){
      echo 0;
		}
    else{
      $data_update = array(
			
				'nama' => $this->input->post('nama'),
				'instansi' => $this->input->post('instansi'),
				'pembimbing' => $this->input->post('pembimbing'),
				'alamat' => $this->input->post('alamat'),
				'pekerjaan' => $this->input->post('pekerjaan'),
				'nomor_telp' => $this->input->post('nomor_telp'),
				'email' => $this->input->post('email'),
				'rincian_informasi_yang_diinginkan' => $this->input->post('rincian_informasi_yang_diinginkan'),
				'tujuan_penggunaan_informasi' => $this->input->post('tujuan_penggunaan_informasi'),
				'cara_melihat_membaca_mendengarkan_mencatat' => $this->input->post('cara_melihat_membaca_mendengarkan_mencatat'),
				'cara_hardcopy_softcopy' => $this->input->post('cara_hardcopy_softcopy'),
				'cara_mengambil_langsung' => $this->input->post('cara_mengambil_langsung'),
				'cara_kurir' => $this->input->post('cara_kurir'),
				'cara_pos' => $this->input->post('cara_pos'),
				'cara_faksimili' => $this->input->post('cara_faksimili'),
				'cara_email' => $this->input->post('cara_email'),
				'kategori_permohonan_informasi_publik' => $this->input->post('kategori_permohonan_informasi_publik'),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
			);
      $table_name  = 'permohonan_informasi_publik';
      $where       = array(
        'permohonan_informasi_publik.id_permohonan_informasi_publik' => trim($this->input->post('id_permohonan_informasi_publik'))
			);
      $this->Permohonan_informasi_publik_model->update_permohonan_informasi_publik($data_update, $where, $table_name);
      echo 1;
		}
  }
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'permohonan_informasi_publik.status !=' => 99,
      'permohonan_informasi_publik.tujuan_penggunaan_informasi' => 'Pelayanan Informasi'
      );
    $this->db->where($where0);
    $query0 = $this->db->get('permohonan_informasi_publik');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_permohonan_informasi_publik(){
    $web=$this->uut->namadomain(base_url());
		$table = 'permohonan_informasi_publik';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where = array(
      'permohonan_informasi_publik.status !=' => 99,
      'permohonan_informasi_publik.tujuan_penggunaan_informasi' => 'Pelayanan Informasi'
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Permohonan_informasi_publik_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $h)
      {
				$urut=$urut+1;
        $w = $this->db->query("
        SELECT *
        from attachment
        where id_tabel ='".$h->id_permohonan_informasi_publik."'
        and table_name = 'permohonan_informasi_publik'
        ");
        if(($w->num_rows())==0){
          echo '
            <li class="col-md-4 col-sm-6 col-xs-12 portfolio-item no-padding no-margin" id_permohonan_informasi_publik="'.$h->id_permohonan_informasi_publik.'" id="'.$h->id_permohonan_informasi_publik.'">
              <a class="" href="#">
                  <figure class="animate fadeInLeft">
              <div class="well">
                  <span><u>'.strtoupper(substr(strip_tags($h->instansi), 0, 100)).'</u><br />'.strtolower($h->rincian_informasi_yang_diinginkan).'</span>
              </div>
                  </figure>
              </a>
            </li>
          ';
        }
        else{
          echo '
            <li class="col-md-4 col-sm-6 col-xs-12 portfolio-item no-padding no-margin" id_permohonan_informasi_publik="'.$h->id_permohonan_informasi_publik.'" id="'.$h->id_permohonan_informasi_publik.'">
              <a class="" href="#">
                  <figure class="animate fadeInLeft">
                      <img alt="image1" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_permohonan_informasi_publik($h->id_permohonan_informasi_publik).'">
                      <figcaption><span><u>'.strtoupper(substr(strip_tags($h->instansi), 0, 100)).'</u><br />'.strtolower($h->rincian_informasi_yang_diinginkan).'</span></figcaption>
                  </figure>
              </a>
            </li>
          ';
        }
      }
          
	}
  public function get_attachment_by_id_permohonan_informasi_publik($id_permohonan_informasi_publik)
   {
    $where = array(
      'id_tabel' => $id_permohonan_informasi_publik,
      'table_name' => 'permohonan_informasi_publik'
      );
    $d = $this->Permohonan_informasi_publik_model->get_attachment_by_id_permohonan_informasi_publik($where);
    if(!$d)
      {
        return 'pelayanan.png';
      }
    else
      {
        return $d['file_name'];
      }
   }
}