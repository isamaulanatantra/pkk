<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Dusun extends CI_Controller
{

  function __construct(){
		parent::__construct();
		$this->load->model('Dusun_model');
  }
  public function index(){
    $data['main_view'] = 'dusun/home';
    $this->load->view('tes', $data);
  }
  public function json_option_dusun(){
		$table = 'dusun';
    $id_skpd = trim($this->input->post('id_skpd'));
		$hak_akses = $this->session->userdata('hak_akses');
		$where   = array(
			'dusun.id_skpd' => $id_skpd
			);
		$fields  = 
			"
			dusun.id_dusun,
			dusun.nama_dusun
			";
		$order_by  = 'dusun.nama_dusun';
		echo json_encode($this->Dusun_model->opsi($table, $where, $fields, $order_by));
  }
  public function total_data()
  {
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'dusun.status !=' => 99,
      'dusun.id_desa=' => $this->session->userdata('id_desa')
      );
    $this->db->where($where0);
    $query0 = $this->db->get('dusun');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_dusun(){
		$table = 'dusun';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where      = array(
      'dusun.status !=' => 99,
      'dusun.id_desa=' => $this->session->userdata('id_desa')
    );
    $orderby   = ''.$order_by.'';
    $query = $this->Dusun_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
          $urut=$urut+1;
          echo '<tr id_dusun="'.$row->id_dusun.'" id="'.$row->id_dusun.'" >';
          echo '<td valign="top">'.($urut).'</td>';
					echo '<td valign="top">'.$row->nama_dusun.'</td>';
					echo '<td valign="top"><a href="#tab_form_dusun" data-toggle="tab" class="update_id badge bg-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>';
					echo '<a href="#" id="del_ajax" class="badge bg-danger btn-sm"><i class="fa fa-cut"></i></a>';
          echo '<a class="badge bg-warning btn-sm" href="'.base_url().'dusun/pdf/?id_dusun='.$row->id_dusun.'" ><i class="fa fa-file-pdf-o"></i></a></td>';
          echo '</tr>';
      }
          echo '<tr>';
          echo '<td valign="top" colspan="4" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="',base_url(),'dusun/xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
          echo '</tr>';
          
   }
  public function simpan_dusun(){
    $this->form_validation->set_rules('nama_dusun', 'nama_dusun', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
			{
				$data_input = array(
				'nama_dusun' => trim($this->input->post('nama_dusun')),
				'id_desa' => $this->session->userdata('id_desa'),
				'id_kecamatan' => $this->session->userdata('id_kecamatan'),
				'id_kabupaten' => $this->session->userdata('id_kabupaten'),
				'id_propinsi' => $this->session->userdata('id_propinsi'),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'dusun';
      $this->Dusun_model->save_data($data_input, $table_name);
     }
   }
}