<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome_template extends CI_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Posting_model');
  }

  public function index()
  {
    $web = $this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
    );
    if ($web == 'diskominfo.wonosobokab.go.id') {
      header('location:https://diskominnfo.wonosobokab.go.id');
    }
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0) {
      $data['domain'] = $row0->domain;
      $data['alamat'] = $row0->alamat;
      $data['telpon'] = $row0->telpon;
      $data['email'] = $row0->email;
      $data['twitter'] = $row0->twitter;
      $data['facebook'] = $row0->facebook;
      $data['google'] = $row0->google;
      $data['instagram'] = $row0->instagram;
      $data['peta'] = $row0->peta;
      $data['title'] = $row0->keterangan;
      $data['keterangan'] = $row0->keterangan;
      /* template */
      $fieldstemplate     = "template.template_code,template.template_name,template.information,template.template_head,template.template_body,template.status";
      $wheretemplate      = array(
        'template_role.id_dasar_website' => $row0->id_dasar_website,
        'template.status' => 1,
        'template_role.status' => 1
      );
      $this->db->join('template_role', 'template_role.template_id=template.template_id');
      $this->db->where($wheretemplate);
      $this->db->limit(1);
      $this->db->select("$fieldstemplate");
      $query01 = $this->db->get('template');
      $a = $query01->num_rows();
      // foreach ($this->db->get('template')->result() as $rtemplate) {}
      if ($a == 0) {
        # code...
        $where1 = array(
          'domain' => $web
        );
        $this->db->where($where1);
        $query1 = $this->db->get('komponen');
        foreach ($query1->result() as $row1) {
          if ($row1->status == 1) {
            if ($row1->judul_komponen == 'Header') { //Header
              $data['Header'] = '
                  <div id="icons" class="row">
                    <div class="container background-grey bottom-border">
                      <div class="row">
                        <center class="animate fadeInRightBig animated">
                        ' . $row1->isi_komponen . '
                        </center>
                        <!-- End Icons -->
                      </div>
                    </div>
                  </div>
                    ';
            } else if ($row1->judul_komponen == 'Kolom Kiri Atas') { //Kolom Kiri Atas
              $data['KolomKiriAtas'] = '
                  <div id="icons" class="row">
                    <div class="container background-grey bottom-border">
                      <div class="row">
                        <center class="animate fadeInRightBig animated">
                        ' . $row1->isi_komponen . '
                        </center>
                        <!-- End Icons -->
                      </div>
                    </div>
                  </div>
                    ';
            } else if ($row1->judul_komponen == 'Kolom Kanan Atas') { //Kolom Kanan Atas
              $data['KolomKananAtas'] = '
                  <div class="container background-white bottom-border">
                    <center class="animate fadeInRightBig animated"><br />
                      ' . $row1->isi_komponen . '
                    </center>
                  </div>
                    ';
            } else if ($row1->judul_komponen == 'Kolom Kiri Bawah') { //Kolom Kiri Bawah
              $data['KolomKiriBawah'] = $row1->isi_komponen;
            } else if ($row1->judul_komponen == 'Kolom Paling Bawah') { //Kolom Paling Bawah
              $data['KolomPalingBawah'] = $row1->isi_komponen;
            } else if ($row1->judul_komponen == 'Kolom Kanan Bawah') { //Kolom Kanan Bawah
              if ($web == 'pmi.wonosobokab.go.id') {
                $data['KolomKananBawah'] = '' . $row1->isi_komponen . '';
              } else {
                $data['KolomKananBawah'] = '
                      <div id="icons" class="row">
                        <div class="container background-grey bottom-border">
                          <div class="row">
                            <center class="animate fadeInRightBig animated"><br />
                          ' . $row1->isi_komponen . '
                            </center>
                            <!-- End Icons -->
                          </div>
                        </div>
                      </div>
                      ';
              }
            } else {
            }
          } else {
          }
        }

        $a = 0;
        $data['total'] = 10;

        if ($web == 'pkk.wonosobokab.go.id') {
          $data['menu_atas'] = $this->menu_atas_pkk(0, $h = "", $a);
          $data['menu_mobile'] = $this->menu_mobile_pkk(0, $h = "", $a);
          $data['galery_berita'] = $this->option_posting_terbarukan_pkk($h = "", $a);
          $data['judul'] = 'Selamat datang';
          $data['main_view'] = 'welcome/dashboard_pkk';
          $this->load->view('pkk', $data);
        } else if ($web == 'pmi.wonosobokab.go.id') {
          $data['menu_atas'] = $this->menu_atas_pmi(0, $h = "", $a);
          $data['highlight'] = $this->highlight_pmi($h = "", $a);
          $data['galery_berita'] = $this->option_posting_disparbud($h = "", $a);
          $data['terbarukan'] = $this->option_posting_terbarukan_pmi(0, $h = "", $a);
          $data['judul'] = 'Selamat datang';
          $data['main_view'] = 'halaman/hallo';
          $this->load->view('halaman', $data);
        } else {
          $data['menu_atas'] = $this->menu_atas(0, $h = "", $a);
          $data['menu_mobile'] = $this->menu_mobile_pkk(0, $h = "", $a);
          $data['highlight'] = $this->highlight($h = "", $a);
          if ($web == 'dikpora.wonosobokab.go.id') {
            $data['galery_berita'] = $this->option_posting_dikpora($h = "", $a);
            $data['galery_artikel'] = $this->option_artikel_dikpora($h = "", $a);
          } else {
            $data['galery_berita'] = $this->option_posting_disparbud($h = "", $a);
          }
          $data['terbarukan'] = $this->option_posting_terbarukan($h = "", $a);
          $data['judul'] = 'Selamat datang';
          $data['main_view'] = 'halaman/hallo';
          $this->load->view('halaman', $data);
        }
      } else {
        # code... 
        foreach ($query01->result() as $rtemplate) {
          $template_code = $rtemplate->template_code;
          $data['template_name'] = $rtemplate->template_name;
          $data['template_head'] = $rtemplate->template_head;
          $data['template_body'] = $rtemplate->template_body;
          $data['template_information'] = $rtemplate->information;
          $data['title'] = $row0->keterangan;
          $data['keterangan'] = $row0->keterangan;
          $data['main_view'] = 'front_and/page.php';
          $a = 0;
          $data['menu_atas'] = $this->menu_atas_front(0, $h = "", $a);
          $data['highlight'] = $this->highlight_front($h = "", $a);
          $data['highlight_layanan'] = $this->highlight_layanan_front($h = "", $a);
          $data['pengumuman'] = $this->option_posting_terbarukan_front($h = "", $a);
          $data['pengumuman_berita1'] = $this->option_posting_terbarukan_pengumuman_berita_1front($h = "", $a);
          $data['web'] = $this->uut->namadomain(base_url());
          $this->load->view('front_end/' . $template_code . '', $data);
        }
      }
    }
  }

  private function menu_atas_front($parent = NULL, $hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    if ($web == 'disparbud.wonosobokab.go.id' or $web == 'dikpora.wonosobokab.go.id') {
      $url_baca = 'front/detail';
      $url_baca_list = 'front/galeri';
    } else {
      $url_baca = 'front/details';
      $url_baca_list = 'front/categoris';
    }
    $a = $a + 1;
    $w = $this->db->query("
    SELECT * from posting
    where posisi='menu_atas'
    and parent='" . $parent . "' 
    and status = 1 
    and tampil_menu_atas = 1 
    and domain='" . $web . "'
    order by urut
    ");
    $x = $w->num_rows();
    if (($w->num_rows()) > 0) {
      if ($parent == 0) {
        if ($web == 'web.wonosobokab.go.id') {
          $hasil .= '
        <ul class="navbar-nav u-header__navbar-nav">
        <!-- Home -->
        <li class="nav-item hs-has-mega-menu u-header__nav-item"
            data-event="hover"
            data-animation-in="slideInUp"
            data-animation-out="fadeOut"
            data-position="left">
          <a id="homeMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false">Home</a>

          <!-- Home - Mega Menu -->
          <div class="hs-mega-menu w-100 u-header__sub-menu" aria-labelledby="homeMegaMenu">
            <div class="row no-gutters">
              <div class="col-lg-6">
                <!-- Banner Image -->
                <div class="u-header__banner" style="background-image: url(' . base_url() . 'media/upload/branding750x750.jpg);">
                  <div class="u-header__banner-content">
                    <div class="mb-4">
                      <span class="u-header__banner-title">Branding Works</span>
                      <p class="u-header__banner-text">Experience a level of our quality in both design &amp; customization works.</p>
                    </div>
                    <a class="btn btn-primary btn-sm transition-3d-hover" href="#">Learn More <span class="fas fa-angle-right ml-2"></span></a>
                  </div>
                </div>
                <!-- End Banner Image -->
              </div>

              <div class="col-lg-6">
                <div class="row u-header__mega-menu-wrapper">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <span class="u-header__sub-menu-title">Classic</span>
                    <ul class="u-header__sub-menu-nav-group mb-3">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . '">Classic Agency</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-business.html">Classic Business</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-marketing.html">Classic Marketing</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-consulting.html">Classic Consulting</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-start-up.html">Classic Start-up</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="classic-studio.html">Classic Studio <span class="badge badge-success badge-pill ml-1">New</span></a></li>
                    </ul>

                    <span class="u-header__sub-menu-title">Corporate</span>
                    <ul class="u-header__sub-menu-nav-group mb-3">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="corporate-agency.html">Corporate Agency</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="corporate-start-up.html">Corporate Start-Up</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="corporate-business.html">Corporate Business</a></li>
                    </ul>

                    <span class="u-header__sub-menu-title">Portfolio</span>
                    <ul class="u-header__sub-menu-nav-group">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="portfolio-agency.html">Portfolio Agency</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="portfolio-profile.html">Portfolio Profile</a></li>
                    </ul>
                  </div>

                  <div class="col-sm-6">
                    <span class="u-header__sub-menu-title">App</span>
                    <ul class="u-header__sub-menu-nav-group mb-3">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="app-ui-kit.html">App UI kit</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="app-saas.html">App SaaS</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="app-workflow.html">App Workflow</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="app-payment.html">App Payment</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="app-software.html">App Software</a></li>
                    </ul>

                    <span class="u-header__sub-menu-title">Onepages</span>
                    <ul class="u-header__sub-menu-nav-group mb-3">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="onepage-corporate.html">Corporate <span class="badge badge-success badge-pill ml-1">New</span></a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="onepage-creative.html">Creative</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="onepage-saas.html">SaaS</a></li>
                    </ul>

                    <span class="u-header__sub-menu-title">Blog</span>
                    <ul class="u-header__sub-menu-nav-group">
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="blog-agency.html">Blog Agency</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="blog-start-up.html">Blog Start-Up</a></li>
                      <li><a class="nav-link u-header__sub-menu-nav-link" href="blog-business.html">Blog Business</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- End Home - Mega Menu -->
        </li>
        <!-- End Home -->
        ';
        } else {
          $hasil .= '
        <ul class="navbar-nav u-header__navbar-nav">
        <!-- Home -->
        <li class="nav-item hs-has-mega-menu u-header__nav-item"
            data-event="hover"
            data-animation-in="slideInUp"
            data-animation-out="fadeOut"
            data-position="left">
          <a id="homeMegaMenu" class="nav-link u-header__nav-link" href="' . base_url() . '" aria-haspopup="true" aria-expanded="false">Home</a>
        </li>
        ';
        }
      } else {
        $hasil .= '
         <ul id="pagesMegaMenu" class="hs-sub-menu u-header__sub-menu animated" aria-labelledby="pagesMegaMenu" style="min-width: 230px; display: none;">
        ';
      }
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $r = $this->db->query("
      SELECT * from posting
      where parent='" . $h->id_posting . "' 
      and status = 1 
      and tampil_menu_atas = 1 
      and domain='" . $web . "'
      order by urut
      ");
      $xx = $r->num_rows();
      $nomor = $nomor + 1;
      if ($a > 1) {
        if ($xx == 0) {
          if ($h->judul_posting == 'Pengaduan Masyarakat') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'sosegov"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Permohonan Informasi Publik') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'sosegov"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Permohonan Informasi') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'sosegov"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Sitemap') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Berita') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Struktur Organisasi') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/struktur_organisasi/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Peraturan Daerah') {
            $where6      = array(
              'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'perda'
            );
            $this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");
            $this->db->where($where6);
            $this->db->limit(1);
            $query6 = $this->db->get('jenis_perundang_undangan');
            $a6 = $query6->num_rows();
            if ($a6 == 0) {
              $jenis_perundang_undangan = '';
            } else {
              foreach ($query6->result() as $b6) {
                $jenis_perundang_undangan = $b6->jenis_perundang_undangan_id;
              }
            }
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id=' . $jenis_perundang_undangan . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Peraturan Bupati') {
            $where6      = array(
              'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'perbup'
            );
            $this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");
            $this->db->where($where6);
            $this->db->limit(1);
            $query6 = $this->db->get('jenis_perundang_undangan');
            $a6 = $query6->num_rows();
            if ($a6 == 0) {
              $jenis_perundang_undangan = '';
            } else {
              foreach ($query6->result() as $b6) {
                $jenis_perundang_undangan = $b6->jenis_perundang_undangan_id;
              }
            }
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id=' . $jenis_perundang_undangan . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Keputusan DPRD') {
            $where6      = array(
              'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'kepdprd'
            );
            $this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");
            $this->db->where($where6);
            $this->db->limit(1);
            $query6 = $this->db->get('jenis_perundang_undangan');
            $a6 = $query6->num_rows();
            if ($a6 == 0) {
              $jenis_perundang_undangan = '';
            } else {
              foreach ($query6->result() as $b6) {
                $jenis_perundang_undangan = $b6->jenis_perundang_undangan_id;
              }
            }
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id=' . $jenis_perundang_undangan . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Peraturan Desa') {
            $where6      = array(
              'jenis_perundang_undangan.jenis_perundang_undangan_code' => 'perdes'
            );
            $this->db->select("
								jenis_perundang_undangan.jenis_perundang_undangan_id,
								jenis_perundang_undangan.jenis_perundang_undangan_code
							");
            $this->db->where($where6);
            $this->db->limit(1);
            $query6 = $this->db->get('jenis_perundang_undangan');
            $a6 = $query6->num_rows();
            if ($a6 == 0) {
              $jenis_perundang_undangan = '';
            } else {
              foreach ($query6->result() as $b6) {
                $jenis_perundang_undangan = $b6->jenis_perundang_undangan_id;
              }
            }
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'peraturan/kategori/?id=' . $jenis_perundang_undangan . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } else {
            $hasil .= '
              <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . '' . $url_baca . '/' . $h->id_posting . '">' . $h->judul_posting . '</a>
            ';
          }
        } else {
          $hasil .= '
        <li class="nav-item hs-has-sub-menu u-header__nav-item"
            data-event="hover"
            data-animation-in="slideInUp"
            data-animation-out="fadeOut">
          <a id="pagesMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="pagesSubMenu">' . $h->judul_posting . '</a>

        ';
        }
      } else {

        if ($xx == 0) {
          if ($h->judul_posting == 'Prestasi Entitas Pendidikan') {
            $hasil .= '
              <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/detail/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
              ';
          } elseif ($h->judul_posting == 'Sitemap') {
            $hasil .= '
            <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
            ';
          } elseif ($h->judul_posting == 'Berita') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Pengumuman') {
            $hasil .= '
          <li><a class="nav-link u-header__sub-menu-nav-link" href="' . base_url() . 'front/sitemap/' . $h->id_posting . '"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } else {
            $hasil .= '
              <li class="nav-item hs-has-sub-menu u-header__nav-item"><a class="nav-link u-header__nav-link" href="' . base_url() . '' . $url_baca . '/' . $h->id_posting . '">' . $h->judul_posting . '</a>
              ';
          }
        } else {
          $hasil .= '
            <li class="nav-item hs-has-sub-menu u-header__nav-item"
                data-event="hover"
                data-animation-in="slideInUp"
                data-animation-out="fadeOut">
              <a id="pagesMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false" aria-labelledby="pagesSubMenu">' . $h->judul_posting . '</a>
    
            ';
        }
      }
      $hasil = $this->menu_atas_front($h->id_posting, $hasil, $a);
      $hasil .= '
      </li>
      ';
    }
    if (($w->num_rows) > 0) {
      $hasil .= "
    </ul>
    ";
    } else {
    }
    return $hasil;
  }

  private function menu_atas($parent = NULL, $hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    if ($web == 'disparbud.wonosobokab.go.id' or $web == 'dikpora.wonosobokab.go.id' or $web == 'dppkbpppa.wonosobokab.go.id' or $web == 'disperkimhub.wonosobokab.go.id') {
      $url_baca = 'post/detail';
      $url_baca_list = 'post/galeri';
      $url_baca_covid = 'post/detail_covid';
    } else {
      $url_baca = 'postings/details';
      $url_baca_list = 'postings/categoris';
    }
    $a = $a + 1;
    $w = $this->db->query("
    SELECT * from posting
    where posisi='menu_atas'
    and parent='" . $parent . "' 
    and status = 1 
    and tampil_menu_atas = 1 
    and domain='" . $web . "'
    order by urut
    ");
    $x = $w->num_rows();
    if (($w->num_rows()) > 0) {
      if ($parent == 0) {
        if ($web == 'zb.wonosobokab.go.id') {
          $hasil .= '
    <ul id="hornavmenu" class="nav navbar-nav" >
    <li><a href="' . base_url() . 'website" class="btn btn-primary">BERANDA</a></li>
    ';
        } elseif ($web == 'ppiddemo.wonosobokab.go.id') {
          $hasil .= '
    <ul id="hornavmenu" class="nav navbar-nav" >
    <li><a href="' . base_url() . '"><i class=""></i>BERANDA</a></li>
    <li class="parent">
      <span class="">PPID </span>
      <ul>
      <li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid">PPID </a></span></li>
      <li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid_pembantu">PPID Pembantu</a></span></li>
      </ul>
    </li>
    ';
        } else {
          $hasil .= '
    <ul id="hornavmenu" class="nav navbar-nav" >
    <li><a href="' . base_url() . '" class="">BERANDA</a></li>
    ';
        }
      } else {
        $hasil .= '
    <ul>
      ';
      }
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $r = $this->db->query("
      SELECT * from posting
      where parent='" . $h->id_posting . "' 
      and status = 1 
      and domain='" . $web . "'
      order by urut
      ");
      $xx = $r->num_rows();
      $nomor = $nomor + 1;
      if ($a > 1) {
        if ($xx == 0) {
          if ($h->judul_posting == 'Pengaduan Masyarakat') {
            $hasil .= '
          <li><a href="' . base_url() . 'pengaduan_masyarakat"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Permohonan Informasi Publik') {
            $hasil .= '
          <li><a href="' . base_url() . 'permohonan_informasi_publik"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } elseif ($h->judul_posting == 'Permohonan Informasi') {
            $hasil .= '
        <li><a href="' . base_url() . 'permohonan_informasi_publik"> <span class=""> ' . $h->judul_posting . ' </span> </a>
        ';
          } elseif ($h->judul_posting == 'Sitemap') {
            $hasil .= '
          <li><a href="' . base_url() . 'postings/sitemap/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span> </a>
          ';
          } else {
            if ($nomor < 21) {
              $hasil .= '
              <li><a href="' . base_url() . '' . $url_baca . '/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class="">' . $h->judul_posting . ' </span></a>';
            }
          }
        } else {
          if ($xx > 3) {
            $hasil .= '
            <li><a href="' . base_url() . '' . $url_baca_list . '/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span></a>';
          } else {
            $hasil .= '
              <li class="parent" > <span class="">' . $h->judul_posting . ' </span>';
          }
        }
      } else {

        if ($xx == 0) {
          if ($h->judul_posting == 'Prestasi Entitas Pendidikan') {
            $hasil .= '
                <li><a href="' . base_url() . 'post/detail/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span> </a>
              ';
          } elseif ($h->judul_posting == 'Sitemap') {
            $hasil .= '
            <li><a href="' . base_url() . 'postings/sitemap/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span> </a>
            ';
          } else {
            if ($h->judul_posting == 'Covid-19') {
              $hasil .= '
              <li><a href="' . base_url() . '' . $url_baca_covid . '/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span></a>
              ';
            } else {
              if ($nomor < 21) {
                $hasil .= '
                  <li><a href="' . base_url() . '' . $url_baca_list . '/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span></a>';
              }
            }
          }
        } else {
          if ($nomor < 21) {

            if ($xx > 3) {
              $hasil .= '
            <li><a href="' . base_url() . '' . $url_baca_list . '/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span></a>';
            } else {
              $hasil .= ' <li><a href="' . base_url() . '' . $url_baca . '/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span></a>';
            }



            /*if($xx > 3){
                $hasil .= '
                <li><a href="' . base_url() . '' . $url_baca_list . '/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span></a>';
              }else{
                $hasil .= '
                    <li><a href="' . base_url() . '' . $url_baca . '/' . $h->id_posting . '/' . $newphrase . '.HTML"> <span class=""> ' . $h->judul_posting . ' </span></a>';
              }*/
          }
        }
      }
      $hasil = $this->menu_atas($h->id_posting, $hasil, $a);
      $hasil .= '
      </li>
      ';

      $z = $this->db->query("
      SELECT * from posting
      where id_posting='" . $parent . "'
      ");
      foreach ($z->result() as $z1) {
        if ($z1->judul_posting == 'PPID' && $nomor == 4) {
          $hasil .= '
          <li>  <a href="#"> Produk Hukum </a>
            <ul>
              <li><a href="' . base_url() . 'peraturan/daftar_peraturan/?&id_peraturan=7&tentang=Peraturan_Daerah.html">Peraturan Daerah</a></li>
            </ul>
          </li>
        ';
        }
      }
    }
    if (($w->num_rows) > 0) {
      $hasil .= "
    </ul>
    ";
    } else {
    }
    return $hasil;
  }


  private function menu_mobile_pkk($parent = NULL, $hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='" . $parent . "' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='" . $web . "'
    order by urut
		");
    $x = $w->num_rows();

    if (($w->num_rows()) > 0) {
      if ($parent == 0) {
        $hasil .= '<ul> <li><a href="' . base_url() . '" role="button" aria-expanded="false">HOME</a></li>';
      } else {
        $hasil .= '<ul> ';
      }
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $r = $this->db->query("
		SELECT * from posting
									
		where parent='" . $h->id_posting . "' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='" . $web . "'
    order by urut
		");
      $xx = $r->num_rows();

      $nomor = $nomor + 1;
      if ($a > 1) {
        $hasil .= '
          <li> <a href="' . base_url() . 'post/detail/' . $h->id_posting . '/' . $newphrase . '.HTML"><span class=""> ' . $h->judul_posting . ' </span></a>';
      } else {
        if ($h->judul_posting == 'Berita') {
          $hasil .= '
					  <li><a href="' . base_url() . 'post/galeri/' . $h->id_posting . '/' . $newphrase . '.HTML" role="button" aria-expanded="false"> <span class=""> ' . $h->judul_posting . ' </span> </a>
					';
        } elseif ($h->judul_posting == 'Pengumuman') {
          $hasil .= '
					  <li><a href="' . base_url() . 'post/galeri/' . $h->id_posting . '/' . $newphrase . '.HTML" role="button" aria-expanded="false"> <span class=""> ' . $h->judul_posting . ' </span> </a>
					';
        } elseif ($h->judul_posting == 'Pengaduan Masyarakat') {
          $hasil .= '
				  <li><a href="' . base_url() . 'pengaduan_masyarakat"> <span class=""> ' . $h->judul_posting . ' </span> </a>
					';
        } elseif ($h->judul_posting == 'Permohonan Informasi Publik') {
          $hasil .= '
				  <li><a href="' . base_url() . 'permohonan_informasi_publik"> <span class=""> ' . $h->judul_posting . ' </span> </a>
					';
        } elseif ($h->judul_posting == 'Permohonan Informasi') {
          $hasil .= '
				  <li><a href="' . base_url() . 'permohonan_informasi_publik"> <span class=""> ' . $h->judul_posting . ' </span> </a>
					';
        } else {
          if ($xx == 0) {
            if ($h->judul_posting == 'Berita') {
              $hasil .= '
						  <li><a href="' . base_url() . 'post/galeri/' . $h->id_posting . '/' . $newphrase . '.HTML" role="button" aria-expanded="false"> <span class=""> ' . $h->judul_posting . ' </span> </a>
						';
            } elseif ($h->judul_posting == 'Pengumuman') {
              $hasil .= '
							<li><a href="' . base_url() . 'post/galeri/' . $h->id_posting . '/' . $newphrase . '.HTML" role="button" aria-expanded="false"> <span class=""> ' . $h->judul_posting . ' </span> </a>
						';
            } elseif ($h->judul_posting == 'Pengaduan Masyarakat') {
              $hasil .= '
					  <li><a href="' . base_url() . 'pengaduan_masyarakat"> <span class=""> ' . $h->judul_posting . ' </span> </a>
						';
            } elseif ($h->judul_posting == 'Permohonan Informasi Publik') {
              $hasil .= '
					  <li><a href="' . base_url() . 'permohonan_informasi_publik"> <span class=""> ' . $h->judul_posting . ' </span> </a>
						';
            } elseif ($h->judul_posting == 'Permohonan Informasi') {
              $hasil .= '
					  <li><a href="' . base_url() . 'permohonan_informasi_publik"> <span class=""> ' . $h->judul_posting . ' </span> </a>
						';
            } else {
              $hasil .= '
					  <li><a href="' . base_url() . 'post/detail/' . $h->id_posting . '/' . $newphrase . '.HTML" role="button" aria-expanded="false"> <span class=""> ' . $h->judul_posting . ' </span></a>';
            }
          } else {
            if ($h->judul_posting == 'Berita') {
              $hasil .= '
						  <li><a href="' . base_url() . 'post/galeri/' . $h->id_posting . '/' . $newphrase . '.HTML" role="button" aria-expanded="false"> <span class=""> ' . $h->judul_posting . ' </span> </a>
						';
            } elseif ($h->judul_posting == 'Pengumuman') {
              $hasil .= '
							<li><a href="' . base_url() . 'post/galeri/' . $h->id_posting . '/' . $newphrase . '.HTML" role="button" aria-expanded="false"> <span class=""> ' . $h->judul_posting . ' </span> </a>
						';
            } elseif ($h->judul_posting == 'Pengaduan Masyarakat') {
              $hasil .= '
					  <li><a href="' . base_url() . 'pengaduan_masyarakat"> <span class=""> ' . $h->judul_posting . ' </span> </a>
						';
            } elseif ($h->judul_posting == 'Permohonan Informasi Publik') {
              $hasil .= '
					  <li><a href="' . base_url() . 'permohonan_informasi_publik"> <span class=""> ' . $h->judul_posting . ' </span> </a>
						';
            } elseif ($h->judul_posting == 'Permohonan Informasi') {
              $hasil .= '
					  <li><a href="' . base_url() . 'permohonan_informasi_publik"> <span class=""> ' . $h->judul_posting . ' </span> </a>
						';
            } else {
              $hasil .= '
						  <li class="parent" > <span class="">' . $h->judul_posting . ' </span>';
            }
          }
        }
      }

      $hasil = $this->menu_mobile_pkk($h->id_posting, $hasil, $a);
      $hasil .= '</li>';
    }
    if (($w->num_rows) > 0) {
      $hasil .= "
    </ul>";
    } else {
    }

    return $hasil;
  }

  private function highlight_layanan_front($hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $skpd = $this->db->query("
		SELECT skpd.id_skpd, skpd.status, data_skpd.skpd_website
		from skpd, data_skpd
    where skpd.status = 1
    and data_skpd.id_skpd = skpd.id_skpd
		order by data_skpd.id_data_skpd desc
    limit 20
		");
    foreach ($skpd->result() as $hskpd) {
      $w = $this->db->query("
      SELECT permohonan_informasi_publik.id_permohonan_informasi_publik, permohonan_informasi_publik.created_time, permohonan_informasi_publik.nama, permohonan_informasi_publik.instansi, permohonan_informasi_publik.rincian_informasi_yang_diinginkan, permohonan_informasi_publik.domain, permohonan_informasi_publik.tujuan_penggunaan_informasi
      from permohonan_informasi_publik
      where permohonan_informasi_publik.status = 1 
      and permohonan_informasi_publik.parent = 0
      and permohonan_informasi_publik.domain = '" . $hskpd->skpd_website . "'
      order by permohonan_informasi_publik.created_time desc
      limit 1
      ");
      $nomor = 0;
      foreach ($w->result() as $h) {
        $nomor = $nomor + 1;
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
        $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        $created_time = '' . $this->Crud_model->dateBahasaIndotok($h->created_time) . '';
        $hasil .= '
          <div class="js-slide card border-0 mb-3">
            <div class="card-body p-5">
              <small class="d-block text-muted mb-2">' . $created_time . '</small>
              <small class="d-block text-muted mb-2">' . $h->domain . '</small>
              <h3 class="h5">
                <a href="https://' . $h->domain . '/postings/details/' . $h->id_permohonan_informasi_publik . '" tabindex="0">' . $h->tujuan_penggunaan_informasi . '</a>
              </h3>
              <p class="mb-0">' . substr(strip_tags($h->rincian_informasi_yang_diinginkan), 0, 150) . '</p>
            </div>

            <div class="card-footer pb-5 px-0 mx-5">
              <div class="media align-items-center">
                <div class="u-sm-avatar mr-3">
                  <img class="img-fluid rounded-circle" src="https://web.wonosobokab.go.id/front/assets/img/100x100/img4.jpg" alt="Image Description">
                </div>
                <div class="media-body">
                  <h4 class="small mb-0"><a href="https://' . $h->domain . '/postings/details/' . $h->id_permohonan_informasi_publik . '" tabindex="0">' . $h->nama . '</a> | ' . $h->instansi . '</h4>
                </div>
              </div>
            </div>
            <!-- End Blog Grid -->
          </div>
          ';
      }
    }
    return $hasil;
  }

  private function highlight_front($hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    if ($web = 'web.wonosobokab.go.id') {
      $where_web = "";
    } else {
      $where_web = "
      and data_skpd.id_skpd = '" . $this->session->userdata('id_skpd') . "'
      ";
    }
    $a = $a + 1;
    $skpd = $this->db->query("
		SELECT skpd.id_skpd, skpd.status, data_skpd.skpd_website
		from skpd, data_skpd
    where skpd.status = 1
    and data_skpd.id_skpd = skpd.id_skpd
    " . $where_web . "
		order by data_skpd.id_data_skpd asc
    limit 9
		");
    foreach ($skpd->result() as $hskpd) {
      $wberita = $this->db->query("
      SELECT posting.id_posting, posting.judul_posting
      from posting
      where posting.judul_posting like '%berita%'
      and posting.status = 1
      and posting.parent = 0
      and posting.domain = '" . $hskpd->skpd_website . "'
      order by created_time desc
      ");
      $xxberita = $wberita->num_rows();
      foreach ($wberita->result() as $hberita) {
        $w = $this->db->query("
        SELECT posting.id_posting, posting.judul_posting, posting.domain, posting.created_time, posting.isi_posting
        from posting
        where posting.status = 1 
        and posting.highlight = 1
        and posting.parent = $hberita->id_posting
        order by created_time desc
        limit 1
        ");
        $nomor = 0;
        foreach ($w->result() as $h) {
          $nomor = $nomor + 1;
          $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
          $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
          $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
          $created_time = '' . $this->Crud_model->dateBahasaIndotok($h->created_time) . '';
          $hasil .= '
              <div class="js-slide px-3">
                <!-- Team -->
                <div class="row">
                  <div class="col-sm-6 d-sm-flex align-content-sm-start flex-sm-column text-center text-sm-left mb-7 mb-sm-0">
                    <div class="w-100">
                      <h3 class="h5 mb-4">' . $h->judul_posting . '</h3>
                    </div>
                    <div class="d-inline-block">
                      <span class="badge badge-primary badge-pill badge-bigger mb-3">#' . $created_time . '</span>
                    </div>
                    <p class="font-size-1">' . substr(strip_tags($h->isi_posting), 0, 150) . '</p>
                    <p class="mr-3"><a class="text-black-70" href="https://' . $h->domain . '" target="_blank"><span class="fas fa-globe"></span>' . $h->domain . '</a></p>
                    <!-- Social Networks -->
                    <ul class="list-inline mt-auto mb-0">
                      <li class="list-inline-item mx-0">
                        <a class="btn btn-sm btn-soft-secondary" href="https://' . $h->domain . '/postings/details/' . $h->id_posting . '" target="_blank">
                          Selengkapnya
                        </a>
                      </li>
                    </ul>
                    <!-- End Social Networks -->
                  </div>
                  <div class="col-sm-6">
                    <img class="img-fluid rounded mx-auto" src="https://' . $h->domain . '/media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '" alt="Image ' . $h->judul_posting . '">
                  </div>
                </div>
                <!-- End Team -->
              </div>
            ';
        }
      }
    }
    return $hasil;
  }

  private function highlight($hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '" . $web . "'
		order by created_time desc
    limit 9
		");
    if (($w->num_rows()) > 0) {
      $hasil .= '<ul class="portfolio-group">';
    } else {
      $hasil .= '<ul>';
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $nomor = $nomor + 1;
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $created_time = '' . $this->Crud_model->dateBahasaIndo1($h->created_time) . '';
      $hasil .= '
          <li class="col-md-4 col-sm-6 col-xs-12 portfolio-item no-padding no-margin">
            <a class="" href="' . base_url() . 'postings/details/' . $h->id_posting . '/' . str_replace(' ', '_', $newphrase) . '.HTML">
                <figure class="animate fadeInLeft">
                    <img alt="' . substr(strip_tags($h->judul_posting), 0, 100) . '" src="' . base_url() . 'media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '">
                    <figcaption>
                        <u><strong>' . $h->judul_posting . '</strong></u><br />
                        <span>' . substr(strip_tags($h->isi_posting), 0, 150) . '</span>
                    </figcaption>
                </figure>
            </a>
          </li>';
    }
    if (($w->num_rows) > 0) {
      $hasil .= "</ul>";
    }
    return $hasil;
  }

  public function get_attachment_by_id_posting($id_posting)
  {
    $where = array(
      'id_tabel' => $id_posting,
      'table_name' => 'posting'
    );
    $d = $this->Posting_model->get_attachment_by_id_posting($where);
    $no = 0;
    if (!$d) {
      return 'logo-the-soul-of-java.png';
    } else {
      $file_name = explode('.', $d['file_name']);
      $file_type = $file_name[1];
      if ($file_type == 'jpg' || $file_type == 'png' || $file_type == 'jpeg') {
        return $d['file_name'];
      } else {
        return 'logo-the-soul-of-java.png';
      }
    }
  }

  private function option_posting_disparbud($hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '" . $web . "'
		order by created_time desc
    limit 10
		");
    if (($w->num_rows()) > 0) {
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $nomor = $nomor + 1;
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $created_time = '' . $this->Crud_model->dateBahasaIndo1($h->created_time) . '';
      $hasil .=
        '
						<article class="simple-post clearfix">
							<div class="simple-thumb">
								<a href="' . base_url() . 'post/detail/' . $h->id_posting . '/' . $newphrase . '.HTML">
								<img src="' . base_url() . 'media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '" alt=""/>
								</a>
							</div>
							<header>
								<h3>
									<a href="' . base_url() . 'post/detail/' . $h->id_posting . '/' . $newphrase . '.HTML">' . $h->judul_posting . '</a>
								</h3>
								<p class="simple-share">
									<span><i class="fa fa-clock-o"></i> ' . $created_time . '</span>
								</p>
                <p style="text-align:justify"><span style="font-size:14px">' . substr(strip_tags($h->isi_posting), 0, 200) . '</span></p>
							</header>
						</article>
						
				';
    }
    if (($w->num_rows) > 0) {
    }
    return $hasil;
  }

  private function option_posting_terbarukan_pengumuman_berita_1front($hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $wberita = $this->db->query("
		SELECT posting.id_posting, posting.judul_posting
		from posting
		where posting.judul_posting like '%berita%'
		and posting.status = 1
		and posting.domain = '" . $web . "'
		order by created_time desc
    limit 4
		");
    foreach ($wberita->result() as $hberita) {
      $id_posting = $hberita->id_posting;
      $w = $this->db->query("
      SELECT id_posting, created_time, judul_posting
      from posting
      where posting.status = 1
      and posting.parent = $id_posting
      and posting.domain = '" . $web . "'
      order by created_time desc
      limit 4
      ");
      if (($w->num_rows()) > 0) {
      }
      $nomor = 0;
      foreach ($w->result() as $h) {
        $nomor++;
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
        $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
        $created_time = '' . $this->Crud_model->dateBahasaIndotok($h->created_time) . '';
        if ($nomor == 1) {
          $hasil .= '
          <div class="col-5 align-self-end px-2 mb-3">
            <!-- Fancybox -->
            <a class="js-fancybox u-media-viewer" href="javascript:;"
              data-src="' . base_url() . 'media/upload/' . $this->get_attachment_by_id_posting($h->id_posting) . '"
              data-fancybox="lightbox-gallery-hidden"
              data-caption="' . $h->judul_posting . '"
              data-speed="700">
              <img class="img-fluid rounded" src="' . base_url() . 'media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '" alt="Image ' . $h->judul_posting . '">
  
              <span class="u-media-viewer__container">
                <span class="u-media-viewer__icon">
                  <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                </span>
              </span>
            </a>
            <!-- End Fancybox -->
          </div>
          ';
        } elseif ($nomor == 2) {
          $hasil .= '
          <div class="col-7 px-2 mb-3">
            <!-- Fancybox -->
            <a class="js-fancybox u-media-viewer" href="javascript:;"
              data-src="' . base_url() . 'media/upload/' . $this->get_attachment_by_id_posting($h->id_posting) . '"
              data-fancybox="lightbox-gallery-hidden"
              data-caption="' . $h->judul_posting . '"
              data-speed="700">
              <img class="img-fluid rounded" src="' . base_url() . 'media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '" alt="Image ' . $h->judul_posting . '">
  
              <span class="u-media-viewer__container">
                <span class="u-media-viewer__icon">
                  <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                </span>
              </span>
            </a>
            <!-- End Fancybox -->
          </div>
          ';
        } elseif ($nomor == 3) {
          $hasil .= '
          <div class="col-5 offset-1 px-2 mb-3">
            <!-- Fancybox -->
            <a class="js-fancybox u-media-viewer" href="javascript:;"
              data-src="' . base_url() . 'media/upload/' . $this->get_attachment_by_id_posting($h->id_posting) . '"
              data-fancybox="lightbox-gallery-hidden"
              data-caption="' . $h->judul_posting . '"
              data-speed="700">
              <img class="img-fluid rounded" src="' . base_url() . 'media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '" alt="Image ' . $h->judul_posting . '">
  
              <span class="u-media-viewer__container">
                <span class="u-media-viewer__icon">
                  <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                </span>
              </span>
            </a>
            <!-- End Fancybox -->
          </div>
          ';
        } else {
          $hasil .= '
          <div class="col-5 px-2 mb-3">
            <!-- Fancybox -->
            <a class="js-fancybox u-media-viewer" href="javascript:;"
              data-src="' . base_url() . 'media/upload/' . $this->get_attachment_by_id_posting($h->id_posting) . '"
              data-fancybox="lightbox-gallery-hidden"
              data-caption="' . $h->judul_posting . '"
              data-speed="700">
              <img class="img-fluid rounded" src="' . base_url() . 'media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '" alt="Image ' . $h->judul_posting . '">
  
              <span class="u-media-viewer__container">
                <span class="u-media-viewer__icon">
                  <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                </span>
              </span>
            </a>
            <!-- End Fancybox -->
          </div>
          ';
        }
      }
      if (($w->num_rows) > 0) {
      }
    }
    return $hasil;
  }

  private function option_posting_terbarukan_front($hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $wberita = $this->db->query("
		SELECT posting.id_posting, posting.judul_posting
		from posting
		where posting.judul_posting like '%pengumuman%'
		and posting.status = 1
		and posting.domain = '" . $web . "'
		order by created_time desc
    limit 1
		");
    foreach ($wberita->result() as $hberita) {
      $id_posting = $hberita->id_posting;
      $w = $this->db->query("
      SELECT *
      from posting
      where posting.status = 1
      and posting.parent = $id_posting
      and posting.domain = '" . $web . "'
      order by created_time desc
      limit 1
      ");
      if (($w->num_rows()) > 0) {
      }
      $nomor = 0;
      foreach ($w->result() as $h) {
        $nomor = $nomor + 1;
        $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
        $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
        $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
        $created_time = '' . $this->Crud_model->dateBahasaIndo1($h->created_time) . '';
        $hasil .= '
        <div class="pr-md-4">
          <!-- Title -->
          <div class="mb-7">
            <span class="btn btn-xs btn-soft-success btn-pill mb-2">Pengumuman</span>
            <h2 class="text-primary">' . $h->judul_posting . '</h2>
            <p>' . substr(strip_tags($h->isi_posting), 0, 300) . '</p>
          </div>
          <!-- End Title -->
          <a class="btn btn-sm btn-primary btn-wide transition-3d-hover" href="' . base_url() . 'postings/details/' . $h->id_posting . '">Selengkapnya <span class="fas fa-angle-right ml-2"></span></a>
        </div>
        ';
      }
      if (($w->num_rows) > 0) {
      }
    }
    return $hasil;
  }

  private function option_posting_terbarukan($hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1
		and posting.parent != 0
		and posting.domain = '" . $web . "'
		order by created_time desc
    limit 12
		");
    if (($w->num_rows()) > 0) {
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $nomor = $nomor + 1;
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $created_time = '' . $this->Crud_model->dateBahasaIndo1($h->created_time) . '';
      if ($nomor == 1) {
        $hasil .= '<div class="item active"><div class="row">';
      } elseif ($nomor == 7) {
        $hasil .= '<div class="item"><div class="row">';
      }
      $hasil .= '
      
      <div class="col-md-2 col-sm-4 col-xs-4">
        <a data-toggle="tooltip" title="' . $h->judul_posting . '" data-placement="bottom" href="' . base_url() . 'postings/details/' . $h->id_posting . '/' . str_replace(' ', '_', $newphrase) . '.HTML">
          <img alt="' . substr(strip_tags($h->judul_posting), 0, 80) . '" class="well" style="height: 120px;" src="' . base_url() . 'media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '">
        </a>
      </div>
      
      ';
      if ($nomor == 6 or $nomor == 12) {
        $hasil .= '</div></div>';
      }
    }
    if (($w->num_rows) > 0) {
    }
    return $hasil;
  }

  private function menu_atas_pkk($parent = NULL, $hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $w = $this->db->query("
    SELECT * from posting
    where posisi='menu_atas'
    and parent='" . $parent . "' 
    and status = 1 
    and tampil_menu_atas = 1 
    and domain='" . $web . "'
    order by urut
    ");
    $x = $w->num_rows();
    if (($w->num_rows()) > 0) {
      if ($parent == 0) {
        $hasil .= '
        <ul id="responsivemenu">
        <li class="active"><a href="' . base_url() . '"><i class="icon-home homeicon"></i><span class="showmobile">Home</span></a></li>
        ';
      } else {
        if ($a > 2) {
          $hasil .= '
    <ul style="left: 100%; top: 10px; display: none;">
      ';
        } else {
          $hasil .= '
    <ul style="display: none;">
      ';
        }
      }
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $r = $this->db->query("
      SELECT * from posting
      where parent='" . $h->id_posting . "' 
      and status = 1 
      and tampil_menu_atas = 1 
      and domain='" . $web . "'
      order by urut
      ");
      $xx = $r->num_rows();
      $nomor = $nomor + 1;
      if ($a > 1) {
        if ($xx == 0) {
          if ($h->judul_posting == 'POKJA 4') {
            $hasil .= '
                <li><a href="' . base_url() . 'pkk/pokja4" role="button" aria-expanded="false" target="_blank"> ' . $h->judul_posting . '</a>
              ';
          } elseif ($h->judul_posting == 'POKJA 1') {
            $hasil .= '
                <li><a href="' . base_url() . 'pkk/pokja1" role="button" aria-expanded="false" target="_blank"> ' . $h->judul_posting . '</a>
              ';
          } elseif ($h->judul_posting == 'POKJA 2') {
            $hasil .= '
                <li><a href="' . base_url() . 'pkk/pokja2" role="button" aria-expanded="false" target="_blank"> ' . $h->judul_posting . '</a>
              ';
          } elseif ($h->judul_posting == 'POKJA 3') {
            $hasil .= '
                <li><a href="' . base_url() . 'pkk/pokja3" role="button" aria-expanded="false" target="_blank"> ' . $h->judul_posting . '</a>
              ';
          } else {
            $hasil .= '
                <li><a href="' . base_url() . 'postings/detail/' . $h->id_posting . '/' . $newphrase . '.HTML"> ' . $h->judul_posting . ' </a>
              ';
          }
        } else {
          $hasil .= '
          <li> <a href="">' . $h->judul_posting . ' </a>
        ';
        }
      } else {
        if ($xx == 0) {
          if ($h->judul_posting == 'SIM PKK') {
            $hasil .= '
              <li><a href="' . base_url() . 'login"> ' . $h->judul_posting . ' </a>
            ';
          } elseif ($h->judul_posting == 'Pokja 4') {
            $hasil .= '
                <li><a href="' . base_url() . 'pkk/pokja4" role="button" aria-expanded="false" target="_blank"> ' . $h->judul_posting . '</a>
              ';
          } else {
            $hasil .= '
            <li><a href="' . base_url() . 'postings/detail/' . $h->id_posting . '/' . $newphrase . '.HTML"> ' . $h->judul_posting . ' </a>
            ';
          }
        } else {
          $hasil .= '
            <li> <a href="">' . $h->judul_posting . ' </a>
          ';
        }
      }
      $hasil = $this->menu_atas_pkk($h->id_posting, $hasil, $a);
      $hasil .= '
      </li>
      ';
    }
    if (($w->num_rows) > 0) {
      $hasil .= "
    </ul>
    ";
    } else {
    }
    return $hasil;
  }

  private function option_posting_terbarukan_pkk($hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '" . $web . "'
		order by created_time desc
    limit 9
		");
    if (($w->num_rows()) > 0) {
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $nomor = $nomor + 1;
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $created_time = '' . $this->Crud_model->dateBahasaIndo1($h->created_time) . '';
      $hasil .=
        '
      
          <div class="c4" style="text-align:center;">
            <div class="featured-projects">
              <div class="featured-projects-image"><a href="' . base_url() . 'postings/detail/' . $h->id_posting . '/' . $newphrase . '.HTML"><img class="imgOpa teamimage" src="' . base_url() . 'media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '" alt="Image"></a></div>
              <div class="featured-projects-content">
                <span class="hirefor"><a href="' . base_url() . 'postings/detail/' . $h->id_posting . '/' . $newphrase . '.HTML">' . $h->judul_posting . '</a></span>
              </div>
            </div>
          </div>
			';
    }
    if (($w->num_rows) > 0) {
    }
    return $hasil;
  }

  private function menu_atas_pmi($parent = NULL, $hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    if ($web == 'disparbud.wonosobokab.go.id' or $web == 'dikpora.wonosobokab.go.id') {
      $url_baca = 'post/detail';
      $url_baca_list = 'post/galeri';
    } else {
      $url_baca = 'postings/details';
      $url_baca_list = 'postings/categoris';
    }
    $a = $a + 1;
    $w = $this->db->query("
	SELECT * from posting
	where posisi='menu_atas'
	and parent='" . $parent . "' 
	and status = 1 
	and tampil_menu_atas = 1 
	and domain='" . $web . "'
	order by urut
	");
    $x = $w->num_rows();
    if (($w->num_rows()) > 0) {
      if ($parent == 0) {
        $hasil .= '
      </div>
      </div>
      <div class="navbar-item dropdown is-hoverable is-hidden-mobile">
        <div class="dropdown-trigger">
      ';
      } else {
        $hasil .= '
      </div>
      <div id="dropdown-menu" role="menu" class="dropdown-menu"><div class="dropdown-content"><p>
      ';
      }
    }
    $nomor = 0;
    /*$hasil .= '
      ';*/
    foreach ($w->result() as $h) {
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $r = $this->db->query("
          SELECT * from posting
          where parent='" . $h->id_posting . "' 
          and status = 1 
          and tampil_menu_atas = 1 
          and domain='" . $web . "'
          order by urut
          ");
      $xx = $r->num_rows();
      $nomor = $nomor + 1;
      if ($a > 1) {
        $hasil .= '
              <a href="' . base_url() . '' . $url_baca . '/' . $h->id_posting . '" class="dropdown-item">' . $h->judul_posting . '</a>
          ';
      } else {
        $hasil .= '
            <a href="' . base_url() . '' . $url_baca . '/' . $h->id_posting . '" class="has-text-dark nuxt-link-exact-active nuxt-link-active"><span aria-haspopup="true" aria-controls="dropdown-menu">' . $h->judul_posting . '</span></a>
          ';
      }
      $hasil = $this->menu_atas_pmi($h->id_posting, $hasil, $a);
    }
    if (($w->num_rows) > 0) {
      $hasil .= '
          </p>
        </div>
      </div>
      </div>
      <div class="navbar-item dropdown is-hoverable is-hidden-mobile">
        <div class="dropdown-trigger">
        ';
    } else {
      $hasil .= "
          ";
    }
    return $hasil;
  }

  private function highlight_pmi($hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $w = $this->db->query("
		SELECT id_posting, created_time, judul_posting, isi_posting, domain, keterangan
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '" . $web . "'
		order by created_time desc
    limit 9
		");
    if (($w->num_rows()) > 0) {
      $hasil .= '<ul class="portfolio-group">';
    } else {
      $hasil .= '<ul>';
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $nomor = $nomor + 1;
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $created_time = '' . $this->Crud_model->dateBahasaIndo1($h->created_time) . '';
      $hasil .= '
                      <div class="column is-12">
                        <a href="' . base_url() . 'postings/details/' . $h->id_posting . '" class="card is-horizontal">
                          <div class="card-image">
                            <figure class="image"><img src="' . base_url() . 'media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '" alt="5d36c7767064f"></figure>
                          </div>
                          <div class="card-content">
                            <div class="content">
                              <h5>' . $h->judul_posting . '</h5>
                              <div class="has-text-grey has-margin-b-6"><small class="has-text-danger">' . $h->keterangan . '</small><span class="has-margin-l-6">|</span><small class="has-margin-l-6"><i class="ion-clock has-margin-r-6"></i><time datetime="' . $created_time . '">' . $created_time . '</time></small></div>
                              ' . substr(strip_tags($h->isi_posting), 0, 300) . '
                            </div>
                          </div>
                        </a>
                      </div>
        ';
    }
    if (($w->num_rows) > 0) {
      $hasil .= "</ul>";
    }
    return $hasil;
  }

  private function option_posting_terbarukan_pmi($parent = NULL, $hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $w = $this->db->query("
		SELECT * from posting
		where parent='" . $parent . "' 
		and status = 1 
    and domain='" . $web . "'
    order by urut asc, created_time desc limit 3
		");
    $x = $w->num_rows();

    if (($w->num_rows()) > 0) {
    }
    if ($parent == 0) {
      $hasil .= '<div class="box-sidebar has-margin-b-1">';
    } else {
      $hasil .= '
      <div class="box-sidebar has-margin-b-1">
      ';
    }
    $nomor = 0;
    foreach ($w->result() as $h) {

      $r = $this->db->query("
		SELECT * from posting
		where parent='" . $h->id_posting . "' 
		and status = 1 
    and domain='" . $web . "'
    order by urut
    
		");
      $xx = $r->num_rows();

      $nomor = $nomor + 1;
      if ($a > 1) {
        if ($xx == 0) {
          $hasil .= '
                        <div class="column is-full">
                          <a href="' . base_url() . 'postings/details/' . $h->id_posting . '" class="has-text-dark">
                            <div class="menu-sidebar">' . $h->judul_posting . '</div>
                          </a>
                        </div>
                      ';
        } else {
          $hasil .= '
                        <div class="column is-full">
                          <a href="' . base_url() . 'postings/details/' . $h->id_posting . '" class="has-text-dark">
                            <div class="menu-sidebar">' . $h->judul_posting . '</div>
                          </a>
                        </div>
                      ';
        }
      } else {
        if ($xx == 0) {
          $hasil .= '
                      <div class="title-pmi has-margin-b-1">
                        <div class="decor">
                          <h3 class="title is-spaced">' . $h->judul_posting . '</h3>
                          <div class="border-pmi"><span></span><span></span><span></span><span></span></div>
                        </div>
                      </div>
                      ';
        } else {
          $hasil .= '
                      <div class="title-pmi has-margin-b-1">
                        <div class="decor">
                          <h3 class="title is-spaced">' . $h->judul_posting . '</h3>
                          <div class="border-pmi"><span></span><span></span><span></span><span></span></div>
                        </div>
                      </div>
                      ';
        }
      }
      $hasil = $this->option_posting_terbarukan_pmi($h->id_posting, $hasil, $a);
      $hasil .= '</div>';
    }
    if (($w->num_rows) > 0) {
      $hasil .= '</div>';
      $hasil .= '<div class="box-sidebar has-margin-b-1">';
    } else {
    }

    return $hasil;
  }


  private function option_posting_dikpora($hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $w = $this->db->query("
    SELECT *
    from posting
    where posting.status = 1 
    and posting.highlight = 1
    and posting.parent != 0
    and posting.domain = '" . $web . "'
    order by created_time desc
    limit 10
    ");
    if (($w->num_rows()) > 0) {
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $nomor = $nomor + 1;
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $created_time = '' . $this->Crud_model->dateBahasaIndo1($h->created_time) . '';
      $hasil .=
        '
            <article class="simple-post clearfix">
              <div class="simple-thumb">
                <a href="' . base_url() . 'post/detail/' . $h->id_posting . '/' . $newphrase . '.HTML">
                <img src="' . base_url() . 'media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '" alt=""/>
                </a>
              </div>
              <header>
                <h4>
                  <a href="' . base_url() . 'post/detail/' . $h->id_posting . '/' . $newphrase . '.HTML">' . $h->judul_posting . '</a>
                </h4>
                <p class="simple-share">
                  <span><i class="fa fa-clock-o"></i> ' . $created_time . '</span>
                </p>
                <p style="text-align:justify"><span style="font-size:14px">' . substr(strip_tags($h->isi_posting), 0, 200) . '...</span></p>
              </header>
            </article>
            
        ';/*
      if( $nomor == 1 ){
        $hasil .= 
        '
          <article class="news-block">
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="overlay-link">
              <figure class="image-overlay">
                <img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" width="870" height="500" alt=""/>
              </figure>
            </a>
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="category">'.$h->judul_posting.'</a>
            <div class="news-details">
              <h2 class="news-title">
                <a href="#">
                </a>
              </h2>
              <p>
              <p style="text-align:justify"><span style="font-size:14px">'.substr(strip_tags($h->isi_posting), 0, 200).'</span></p>
              <p class="simple-share">
                <span class="article-date"><i class="fa fa-clock-o"></i> '.$created_time.'</span>
              </p>
            </div>
          </article>
        ';
      }
      else{
        $hasil .= 
        '
            <article class="simple-post clearfix">
              <div class="simple-thumb">
                <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
                <img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt=""/>
                </a>
              </div>
              <header>
                <h2>
                  <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a>
                </h2>
                <p class="simple-share">
                  <span><i class="fa fa-clock-o"></i> '.$created_time.'</span>
                </p>
              </header>
            </article>
            
        ';
      }*/
    }
    if (($w->num_rows) > 0) {
    }
    return $hasil;
  }
  private function option_artikel_dikpora($hasil, $a)
  {
    $web = $this->uut->namadomain(base_url());
    $a = $a + 1;
    $w = $this->db->query("
    SELECT *
    from posting
    where posting.status = 1 
    and posting.parent = 1041645
    and posting.domain = '" . $web . "'
    order by created_time desc
    limit 3
    ");
    if (($w->num_rows()) > 0) {
    }
    $nomor = 0;
    foreach ($w->result() as $h) {
      $nomor = $nomor + 1;
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
      $created_time = '' . $this->Crud_model->dateBahasaIndo1($h->created_time) . '';
      $hasil .=
        '
            <article class="simple-post clearfix">
              <div class="simple-thumb">
                <a href="' . base_url() . 'post/detail/' . $h->id_posting . '/' . $newphrase . '.HTML">
                <img src="' . base_url() . 'media/upload/s_' . $this->get_attachment_by_id_posting($h->id_posting) . '" alt=""/>
                </a>
              </div>
              <header>
                <h4>
                  <a href="' . base_url() . 'post/detail/' . $h->id_posting . '/' . $newphrase . '.HTML">' . $h->judul_posting . '</a>
                </h4>
                <p class="simple-share">
                  <span><i class="fa fa-clock-o"></i> ' . $created_time . '</span>
                </p>
                <p style="text-align:justify"><span style="font-size:14px">' . substr(strip_tags($h->isi_posting), 0, 200) . '...</span></p>
              </header>
            </article>
            
        ';
    }
    if (($w->num_rows) > 0) {
    }
    return $hasil;
  }
}
