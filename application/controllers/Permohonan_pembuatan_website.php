<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Permohonan_pembuatan_website extends CI_Controller
 {
    
  public function __construct(){
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->library('Crud_model');
    $this->load->model('Permohonan_pembuatan_website_model');
   }
  
  public function index(){
    $where = array(
    'id_users' => $this->session->userdata('id_users')
    );
    $this->db->where($where);
    $this->db->from('users');
    $jml = $this->db->count_all_results();
    if( $jml > 0 ){
      $this->db->where($where);
      $query = $this->db->get('users');
      foreach ($query->result() as $row)
        {
          $hak_akses=$row->hak_akses;
          if($hak_akses=='register'){
            $data['main_view'] = 'permohonan_pembuatan_website/register';
            $this->load->view('back_bone', $data);
          }else{
            $data['main_view'] = 'permohonan_pembuatan_website/home';
            $this->load->view('tes', $data);
          }
        }
      }
    else{
        exit;
      }
   }
  
  public function simpan_permohonan_pembuatan_website(){
   		$web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('nomot_telp', 'nomot_telp', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('jabatan', 'jabatan', 'required');
		$this->form_validation->set_rules('id_kecamatan', 'id_kecamatan', 'required');
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
	  
        $healthy = array("#", ":", "=", "!", "?", "%", "&", "[", "]", "<", ">", "+", "`", "^", "*", "'");
        $yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
        $nama = str_replace($healthy, $yummy, $this->input->post('nama'));
        $nomot_telp = str_replace($healthy, $yummy, $this->input->post('nomot_telp'));
        $email = str_replace($healthy, $yummy, $this->input->post('email'));
        $alamat = str_replace($healthy, $yummy, $this->input->post('alamat'));
        $jabatan = str_replace($healthy, $yummy, $this->input->post('jabatan'));
        $id_kecamatan = $this->input->post('id_kecamatan');
        $id_desa = $this->input->post('id_desa');
		
		if ($this->form_validation->run() == FALSE)
		 {
		  echo 0;
		 }
		else
		 {
		  	$data_input = array(
				'domain' => $web,
				'nomor_surat' => '-',
				'perihal_surat' => '-',
				'tanggal_surat' => date('Y-m-d'),
				'nama' => $nama,
				'nomot_telp' => $nomot_telp,
				'email' => $email,
				'alamat' => $alamat,
				'jabatan' => $jabatan,
				'id_kecamatan' => $id_kecamatan,
				'id_desa' => $id_desa,
				'temp' => trim($this->input->post('temp')),
				'created_by' => $this->session->userdata('id_users'),
				'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1,
				'keterangan' => ''
				);
     		$table_name = 'permohonan_pembuatan_website';
      		$id = $this->Permohonan_pembuatan_website_model->simpan_permohonan_pembuatan_website($data_input, $table_name);
      		echo $id;
			$table_name  = 'lampiran_permohonan_pembuatan_website';
		  	$where       = array(
				'table_name' => 'permohonan_pembuatan_website',
				'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      	$this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
  public function simpan_permohonan_pembuatan_website_register(){
   		$web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('nomot_telp', 'nomot_telp', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('jabatan', 'jabatan', 'required');
		$this->form_validation->set_rules('id_kecamatan', 'id_kecamatan', 'required');
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
	  
        $healthy = array("#", ":", "=", "!", "?", "%", "&", "[", "]", "<", ">", "+", "`", "^", "*", "'");
        $yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
        $nama = str_replace($healthy, $yummy, $this->input->post('nama'));
        $nomot_telp = str_replace($healthy, $yummy, $this->input->post('nomot_telp'));
        $email = str_replace($healthy, $yummy, $this->input->post('email'));
        $alamat = str_replace($healthy, $yummy, $this->input->post('alamat'));
        $jabatan = str_replace($healthy, $yummy, $this->input->post('jabatan'));
        $id_kecamatan = $this->input->post('id_kecamatan');
        $id_desa = $this->input->post('id_desa');
		
		if ($this->form_validation->run() == FALSE)
		 {
		  echo 0;
		 }
		else
		 {
		  	$data_input = array(
				'domain' => $web,
				'nomor_surat' => '-',
				'perihal_surat' => '-',
				'tanggal_surat' => date('Y-m-d'),
				'nama' => $nama,
				'nomot_telp' => $nomot_telp,
				'email' => $email,
				'alamat' => $alamat,
				'jabatan' => $jabatan,
				'id_kecamatan' => $id_kecamatan,
				'id_desa' => $id_desa,
				'temp' => trim($this->input->post('temp')),
				'created_by' => $this->session->userdata('id_users'),
				'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1,
				'keterangan' => ''
				);
     		$table_name = 'permohonan_pembuatan_website';
      		$id = $this->Permohonan_pembuatan_website_model->simpan_permohonan_pembuatan_website($data_input, $table_name);
      		echo $id;
			$table_name  = 'lampiran_permohonan_pembuatan_website';
		  	$where       = array(
				'table_name' => 'permohonan_pembuatan_website',
				'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      	$this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
   
  public function update_permohonan_pembuatan_website(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('nomot_telp', 'nomot_telp', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('jabatan', 'jabatan', 'required');
		$this->form_validation->set_rules('id_kecamatan', 'id_kecamatan', 'required');
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
	  
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
				'id_permohonan_pembuatan_website' => trim($this->input->post('id_permohonan_pembuatan_website')),
				'nama' => trim($this->input->post('nama')),
				'nomot_telp' => trim($this->input->post('nomot_telp')),
				'email' => trim($this->input->post('email')),
				'alamat' => trim($this->input->post('alamat')),
				'jabatan' => trim($this->input->post('jabatan')),
				'temp' => trim($this->input->post('temp')),
				'id_kecamatan' => trim($this->input->post('id_kecamatan')),
				'id_desa' => trim($this->input->post('id_desa')),
				'updated_by' => $this->session->userdata('id_users'),
				'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'permohonan_pembuatan_website';
      $where       = array(
        'permohonan_pembuatan_website.id_permohonan_pembuatan_website' => trim($this->input->post('id_permohonan_pembuatan_website')),
        'permohonan_pembuatan_website.domain' => $web
			);
      $this->Permohonan_pembuatan_website_model->update_data_permohonan_pembuatan_website($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function load_table_data_website_desa(){
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
			SELECT *, data_desa.desa_website, kecamatan.nama_kecamatan, desa.nama_desa 
			from dasar_website, data_desa, kecamatan, desa
			where dasar_website.status = 1 
			and dasar_website.domain=data_desa.desa_website
			and data_desa.id_desa=data_desa.id_desa
			and data_desa.id_desa=desa.id_desa
			and data_desa.jenis_pemerintahan=0
			and desa.id_kecamatan=kecamatan.id_kecamatan
			order by kecamatan.id_kecamatan
			");
		if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			$a++;
			echo '
				<tr id_data_desa="'.$h->id_data_desa.'" id="'.$h->id_data_desa.'">
					<td style="padding: 2px;">
						'.$a.'
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nama_kecamatan.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nama_desa.'</p>
					</td>
					<td style="padding: 2px;">
						<a href="https://'.$h->desa_website.'" target="_blank">'.$h->desa_website.'</a>
					</td>
				</tr>
			';
		}
	}
  
	public function load_table(){
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
			SELECT *, kecamatan.nama_kecamatan, desa.nama_desa 
			from permohonan_pembuatan_website, kecamatan, desa
			where permohonan_pembuatan_website.status = 1 
			and permohonan_pembuatan_website.domain='".$web."'
			and permohonan_pembuatan_website.id_desa=desa.id_desa
			and permohonan_pembuatan_website.id_kecamatan=kecamatan.id_kecamatan
			order by permohonan_pembuatan_website.created_time desc
			");
		if(($w->num_rows())>0){
		}
	  	$a=0;
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			$a++;
			echo '
				<tr id_permohonan_pembuatan_website="'.$h->id_permohonan_pembuatan_website.'" id="'.$h->id_permohonan_pembuatan_website.'">
					<td style="padding: 2px;">
						'.$a.'
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nama_kecamatan.'</p>
					</td>
					<td style="padding: 2px;">
						<p>'.$h->nama_desa.'</p>
					</td>
					<td style="padding: 2px;">';
						$wa1 = $this->db->query("
						SELECT *
						from data_desa
						where data_desa.id_desa=".$h->id_desa."
						");
						foreach($wa1->result() as $ha1){
							echo '<a href="https://'.$ha1->desa_website.'" target="_blank">'.$ha1->desa_website.'</a>';
						}
					echo '
					</td>
					<td style="padding: 2px;">
						<ul>';
						$wa = $this->db->query("
						SELECT *
						from lampiran_permohonan_pembuatan_website
						where lampiran_permohonan_pembuatan_website.id_tabel=".$h->id_permohonan_pembuatan_website."
						");
						foreach($wa->result() as $ha){
							echo '<li>'.$ha->keterangan.'</li>';
						}
					echo '</ul>
					</td>
					<td style="padding: 2px;">';
						$keterangan=$h->keterangan;
						if( $keterangan == 'Belum Aktif') {
							echo'<span class="bg-danger">'.$keterangan.'</span>';
						}
						else{
							echo'<span class="bg-success">'.$keterangan.'</span>';
						}
					echo'
					</td>
				</tr>
			';
		}
	}
  
  private function load_table_permohonan_pembuatan_website(){
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
			SELECT *
			from permohonan_pembuatan_website
			where permohonan_pembuatan_website.status = 1 
			and permohonan_pembuatan_website.domain='".$web."'
			order by permohonan_pembuatan_website.created_time desc
			");
		$a=0;
		foreach($w->result() as $h)
		{
			$a++;
			echo '
				<tr id_permohonan_pembuatan_website="'.$h->id_permohonan_pembuatan_website.'" id="'.$h->id_permohonan_pembuatan_website.'">
					<td style="padding: 2px;">
						'.$a.'
					</td>
					<td style="padding: 2px;">
						<p>'.$this->Crud_model->dateBahasaIndo1($h->created_time).'</p>
					</td>
					<td style="padding: 2px;">';
						$wa1a = $this->db->query("
						SELECT *
						from desa
						where desa.id_desa=".$h->id_desa."
						");
						foreach($wa1a->result() as $ha1a){
							echo '<p>'.$ha1a->nama_desa.'-';
						}
						$wa1b = $this->db->query("
						SELECT *
						from kecamatan
						where kecamatan.id_kecamatan=".$h->id_kecamatan."
						");
						foreach($wa1b->result() as $ha1b){
							echo ''.$ha1b->nama_kecamatan.'</p>';
						}
					echo '
					</td>
					<td style="padding: 2px;">';
						$wa1 = $this->db->query("
						SELECT *
						from data_desa
						where data_desa.id_desa=".$h->id_desa."
						");
						foreach($wa1->result() as $ha1){
							echo '<a href="https://'.$ha1->desa_website.'" target="_blank">'.$ha1->desa_website.'</a>';
						}
					echo '
					</td>
					<td style="padding: 2px;">
						'.$h->nama.'
					</td>
					<td style="padding: 2px;">
						'.$h->alamat.'
					</td>
					<td style="padding: 2px;">
						'.$h->jabatan.'
					</td>
					<td style="padding: 2px;">
						'.$h->nomot_telp.'
					</td>
					<td style="padding: 2px;">
						'.$h->email.'
					</td>
					<td style="padding: 2px;"><ul>';
						$wa = $this->db->query("
						SELECT *
						from lampiran_permohonan_pembuatan_website
						where lampiran_permohonan_pembuatan_website.id_tabel=".$h->id_permohonan_pembuatan_website."
						");
						foreach($wa->result() as $ha){
							echo '<li><a target="_blank" href="https://diskominfo.wonosobokab.go.id/media/lampiran_permohonan_pembuatan_website/'.$ha->file_name.'">'.$ha->keterangan.'</a></li>';
						}
					echo '</ul>
					</td>
					<td style="padding: 2px;">
						<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> 
						<a href="#" id="del_ajax"><i class="fa fa-cut"></i></a>
					</td>
				</tr>
			';
		}
	}
  
  public function cetak(){
    $where = array(
		'id_permohonan_pembuatan_website' => $this->uri->segment(3),
		'status' => 1
		);
    $d = $this->Permohonan_pembuatan_website_model->get_data($where);
    if(!$d){
			$data['nama'] = '';
			$data['nomor_surat'] = '';
			$data['perihal_surat'] = '';
			$data['tanggal_surat'] = '';
			$data['nomot_telp'] = '';
			$data['email'] = '';
			$data['alamat'] = '';
			$data['jabatan'] = '';
			$data['penanggung_jawab'] = '';
			$data['lokasi'] = '';
			$data['judul_penelitian'] = '';
			$data['asal_universitas'] = '';
			$data['nomor_surat_kesbangpol'] = '';
			$data['tanggal_surat_kesbangpol'] = '';
			$data['surat_ditujukan_kepada'] = '';
			$data['tembusan_kepada_1'] = '';
			$data['tembusan_kepada_2'] = '';
			$data['tembusan_kepada_3'] = '';
			$data['tembusan_kepada_4'] = '';
			$data['tembusan_kepada_5'] = '';
			$data['tembusan_kepada_6'] = '';
			$data['pembuat'] = '';
			$data['created_time'] = '';
			$data['pengedit'] = '';
			$data['updated_time'] = '';
          }
        else{
			$data['nama'] = $d['nama'];
			$data['nomor_surat'] = $d['nomor_surat'];
			$data['perihal_surat'] = ''.$d['perihal_surat'].' ';
			//$data['tanggal_surat'] = $d['tanggal_surat'];
			$data['tanggal_surat'] = ''.$this->Crud_model->dateBahasaIndo($d['tanggal_surat']).'';
            $data['nomot_telp'] = $d['nomot_telp'];
            $data['email'] = $d['email'];
            $data['alamat'] = $d['alamat'];
            $data['jabatan'] = $d['jabatan'];
            $data['penanggung_jawab'] = $d['penanggung_jawab'];
			$data['lokasi'] = $d['lokasi'];
			$data['judul_penelitian'] = $d['judul_penelitian'];
			$data['asal_universitas'] = $d['asal_universitas'];
			$data['nomor_surat_kesbangpol'] = $d['nomor_surat_kesbangpol'];
			// $data['tanggal_surat_kesbangpol'] = $d['tanggal_surat_kesbangpol'];
			$data['tanggal_surat_kesbangpol'] = $this->Crud_model->dateBahasaIndo($d['tanggal_surat_kesbangpol']);
			$data['surat_ditujukan_kepada'] = $d['surat_ditujukan_kepada'];
			$data['tembusan_kepada_1'] = $d['tembusan_kepada_1'];
			$data['tembusan_kepada_2'] = $d['tembusan_kepada_2'];
			$data['tembusan_kepada_3'] = $d['tembusan_kepada_3'];
			$data['tembusan_kepada_4'] = $d['tembusan_kepada_4'];
			$data['tembusan_kepada_5'] = $d['tembusan_kepada_5'];
			$data['tembusan_kepada_6'] = $d['tembusan_kepada_6'];
			$data['pembuat'] = $d['pembuat'];
			$data['created_time'] = $this->Crud_model->dateBahasaIndo1($d['created_time']);
			$data['pengedit'] = $d['pengedit'];
			$data['updated_time'] = $d['updated_time'];
          }
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'permohonan_pembuatan_website/cetak';
      $this->load->view('print', $data);
  }
  private function permohonan_pembuatan_website($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from permohonan_pembuatan_website 
                          where status = 1 
                          and domain='".$web."'
                          ");
		if(($w->num_rows())>0){
		} 
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			if( $a > 1 ){
			$hasil =  '
				<tr>                    
					<td style="padding: 2px '.($a * 20).'px ;">
						<span><i class="fa fa-user"></i> <a href="#">'.$h->nama.' </a></span>
						<p>'.$h->alamat.'</p>
					</td>
				</tr>
			';
			  }
			else{
				$hasil =  '
					<tr>                    
						<td>
						<span><i class="fa fa-user"></i> '.$h->nama.' </span>
							<p>'.$h->alamat.'</p>
						</td>
					</tr>
				';
			}
			$hasil = $this->permohonan_pembuatan_website($h->id_permohonan_pembuatan_website,$hasil, $a, $id_posting);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'id_permohonan_pembuatan_website' => $this->input->post('id_permohonan_pembuatan_website'),
        'permohonan_pembuatan_website.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_permohonan_pembuatan_website');
      $result = $this->db->get('permohonan_pembuatan_website');
      echo json_encode($result->result_array());
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_permohonan_pembuatan_website = $this->input->post('id_permohonan_pembuatan_website');
      $where = array(
        'id_permohonan_pembuatan_website' => $id_permohonan_pembuatan_website
        );
      $this->db->from('permohonan_pembuatan_website');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          	'status' => 99
                  
          );
        $table_name  = 'permohonan_pembuatan_website';
        $this->Permohonan_pembuatan_website_model->update_data_permohonan_pembuatan_website($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function option_kecamatan_by_id_kabupaten()
	{
		$w = $this->db->query("
		SELECT *
		from kecamatan
		where kecamatan.status = 1
		and kecamatan.id_kabupaten = 1
		order by id_kecamatan");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  public function option_desa_by_id_kecamatan()
	{
		$id_kecamatan = $this->input->post('id_kecamatan');
		$w = $this->db->query("
		SELECT *
		from desa
		where desa.status = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  public function option_desa()
	{
		$id_desa = $this->input->post('id_desa');
		$w = $this->db->query("
		SELECT *
		from desa
		where desa.status = 1
		and desa.id_desa = ".$id_desa."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
	public function json_all_permohonan_pembuatan_website(){
    $web=$this->uut->namadomain(base_url());
		$table = 'permohonan_pembuatan_website';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
    ( select (desa.nama_desa) from desa where desa.id_desa=permohonan_pembuatan_website.id_desa limit 1) as nama_desa,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=permohonan_pembuatan_website.id_kecamatan limit 1) as nama_kecamatan
    ";
    $where = array(
      'permohonan_pembuatan_website.status !=' => 99
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_permohonan_pembuatan_website="'.$row->id_permohonan_pembuatan_website.'" id="'.$row->id_permohonan_pembuatan_website.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$this->Crud_model->dateBahasaIndo1($row->created_time).'</td>';
				// echo '<td valign="top">'.$row->tanggal_surat.'</td>';
				echo '<td valign="top">'.$row->nama_kecamatan.'</td>';
				echo '<td valign="top">'.$row->nama_desa.'</td>';
				// echo '<td valign="top">'.$row->domain.'</td>';
				echo '<td valign="top">'.$row->nama.'</td>';
				echo '<td valign="top">'.$row->alamat.'</td>';
				echo '<td valign="top">'.$row->jabatan.'</td>';
				echo '<td valign="top">'.$row->nomot_telp.'</td>';
				echo '<td valign="top">'.$row->email.'</td>';
				echo '<td style="padding: 2px;">';
						$wa = $this->db->query("
						SELECT *
						from lampiran_permohonan_pembuatan_website
						where lampiran_permohonan_pembuatan_website.id_tabel=".$row->id_permohonan_pembuatan_website."
            and lampiran_permohonan_pembuatan_website.table_name='permohonan_pembuatan_website'
						");
						foreach($wa->result() as $ha){
							echo '<a href="'.base_url().'media/lampiran_permohonan_pembuatan_website/'.$ha->file_name.'" class="blog-tag" target="_blank">'.$ha->keterangan.'</a><br />';
						}
        echo '</td>';
				echo '<td valign="top">
				<a class="badge bg-warning btn-sm" href="'.base_url().'permohonan_pembuatan_website/cetak/?id_permohonan_pembuatan_website='.$row->id_permohonan_pembuatan_website.'" ><i class="fa fa-print"></i> Cetak</a>
				<a href="#tab_form_permohonan_pembuatan_website" data-toggle="tab" class="update_id_permohonan_pembuatan_website badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax_permohonan_pembuatan_website" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			/*echo '
      <tr>
        <td valign="top" colspan="7" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'permohonan_pembuatan_website/cetak_permohonan_pembuatan_website/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data KK</a>
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'permohonan_pembuatan_website/cetak_semua_permohonan_pembuatan_website/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Semua Warga</a>
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'permohonan_pembuatan_website/cetak_permohonan_pembuatan_website_pekarangan/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Pemanfaatan Pekarangan Warga</a>
        </td>
      </tr>
      ';*/
          
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='diskominfo.wonosobokab.go.id'){
      $where0 = array(
        'permohonan_pembuatan_website.status !=' => 99
        );
    }else{
      $where0 = array(
        'permohonan_pembuatan_website.status !=' => 99,
        'permohonan_pembuatan_website.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('permohonan_pembuatan_website');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_permohonan_pembuatan_website_register(){
    $web=$this->uut->namadomain(base_url());
		$table = 'permohonan_pembuatan_website';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
    ( select (desa.nama_desa) from desa where desa.id_desa=permohonan_pembuatan_website.id_desa limit 1) as nama_desa,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=permohonan_pembuatan_website.id_kecamatan limit 1) as nama_kecamatan
    ";
    $where = array(
      'permohonan_pembuatan_website.status !=' => 99,
      'permohonan_pembuatan_website.created_by' => $this->session->userdata('id_users')
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_permohonan_pembuatan_website="'.$row->id_permohonan_pembuatan_website.'" id="'.$row->id_permohonan_pembuatan_website.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$this->Crud_model->dateBahasaIndo1($row->created_time).'</td>';
				echo '<td valign="top">'.$row->nama_kecamatan.'</td>';
				echo '<td valign="top">'.$row->nama_desa.'</td>';
				// echo '<td valign="top">'.$row->domain.'</td>';
				echo '<td valign="top">'.$row->nama.'</td>';
				echo '<td valign="top">'.$row->alamat.'</td>';
				echo '<td valign="top">'.$row->jabatan.'</td>';
				echo '<td valign="top">'.$row->nomot_telp.'</td>';
				echo '<td valign="top">'.$row->email.'</td>';
				echo '<td style="padding: 2px;">
                <ul class="nav nav-pills nav-stacked">
              ';
						$wa = $this->db->query("
						SELECT *
						from lampiran_permohonan_pembuatan_website
						where lampiran_permohonan_pembuatan_website.id_tabel=".$row->id_permohonan_pembuatan_website."
						");
						foreach($wa->result() as $ha){
							echo '<li><a href="'.base_url().'media/lampiran_permohonan_pembuatan_website/'.$ha->file_name.'" class="blog-tag">'.$ha->keterangan.'</li>';
						}
					echo '</ul>
					</td>';
				echo '<td valign="top">
				<a class="pull-right badge bg-green" href="'.base_url().'permohonan_pembuatan_website/cetak/?id_permohonan_pembuatan_website='.$row->id_permohonan_pembuatan_website.'" ><i class="fa fa-print"></i> Cetak</a><br />
				<a href="#tab_form_permohonan_pembuatan_website" data-toggle="tab" class="update_id_permohonan_pembuatan_website pull-right badge bg-blue"><i class="fa fa-pencil-square-o"></i> Sunting</a><br />
				<a href="#" id="del_ajax_permohonan_pembuatan_website" class="pull-right badge bg-red"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			/*echo '
      <tr>
        <td valign="top" colspan="7" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'permohonan_pembuatan_website/cetak_permohonan_pembuatan_website/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data KK</a>
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'permohonan_pembuatan_website/cetak_semua_permohonan_pembuatan_website/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Semua Warga</a>
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'permohonan_pembuatan_website/cetak_permohonan_pembuatan_website_pekarangan/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Pemanfaatan Pekarangan Warga</a>
        </td>
      </tr>
      ';*/
          
	}
  public function total_data_register()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'permohonan_pembuatan_website.status !=' => 99,
      'permohonan_pembuatan_website.created_by' => $this->session->userdata('id_users')
      );
    $this->db->where($where0);
    $query0 = $this->db->get('permohonan_pembuatan_website');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
}