<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller
{
  function __construct()
  {
  parent::__construct();
  // $this->load->library('fpdf');
  $this->load->library('Bcrypt');
  $this->load->library('Excel');
    $this->load->library('Uut');
  $this->load->model('Cetak_model');
  $this->load->model('Crud_model');
  $this->load->model('Users_model');
  }
  
  public function index()
  {
  $hak_akses  = trim($this->input->post('hak_akses'));
  $where   = array(
    'users.hak_akses' => $hak_akses
    );
  $a    = $this->Users_model->json_semua_users($where);
  $data['per_page']  = 300;
  $data['total']  = ceil($a / 10);
  $data['main_view'] = 'users/home';
  $this->load->view('back_bone', $data);
  }
  public function json_all_users()
  {
    $web=$this->uut->namadomain(base_url());
  $hak_akses  = trim($this->input->post('hak_akses'));
  $where   = array(
    'users.status !=' => 99,
    'data_skpd.skpd_website' => $web,
    );
  $a  = $this->Users_model->json_semua_users($where);
  $halaman  = $this->input->get('halaman');
  $limit   = 300;
  $start   = ($halaman - 1) * $limit;
  $fields  = "
    users.id_users,
    users.user_name,
    users.password,
    users.hak_akses,
    users.nip,
    users.nama,
    users.jabatan,
    users.golongan,
    users.pangkat,
    users.id_skpd,
    users.id_desa,
    users.id_kecamatan,
    users.id_kabupaten,
    users.id_propinsi,
    users.created_by,
    users.created_time,
    users.updated_by,
    users.updated_time,
    users.deleted_by,
    users.deleted_time,
    users.status,
    users.last_login,
    data_skpd.skpd_website,
    data_skpd.skpd_nama,
    
    ";
  $where   = array(
    'users.status !=' => 99,
    'data_skpd.skpd_website' => $web,
    );
  $order_by  = 'users.id_users';
  echo json_encode($this->Users_model->json_all_users($where, $limit, $start, $fields, $order_by));
  }
  public function simpan_users()
  {
  $temp  = trim($this->input->post('temp')); 
  $this->form_validation->set_rules('user_name', 'user_name', 'required');
  $user_name  = trim($this->input->post('user_name'));
  $this->form_validation->set_rules('password', 'password', 'required');
  $password  = trim($this->input->post('password'));
  $hak_akses  = trim($this->input->post('hak_akses'));
  $nip  = trim($this->input->post('nip'));
  $this->form_validation->set_rules('nama', 'nama', 'required');
  $nama  = trim($this->input->post('nama'));
  $jabatan  = trim($this->input->post('jabatan'));
  $golongan  = trim($this->input->post('golongan'));
  $pangkat  = trim($this->input->post('pangkat'));
  $created_by  = trim($this->input->post('created_by'));
  $created_time  = trim($this->input->post('created_time'));
  $updated_by  = '';
  $updated_time  = '';
  $deleted_by  = '';
  $deleted_time  = '';
  $status  = '1';
  $last_login  = trim($this->input->post('last_login'));
  $where   = array(
    'users.user_name' => $user_name
    );
  $a    = $this->Users_model->json_semua_users($where);
  
  //---------------------------
  if($this->form_validation->run() == FALSE){
    echo 0;
  }
  else if( $a <> 0 ){
    echo 'dobel';
  }
  else{
    $new_password = $this->bcrypt->hash_password($password); 
    $data_input = array(
		
    'user_name' => $user_name,
    'password' => $new_password,
    'hak_akses' => $hak_akses,
    'nip' => $nip,
    'nama' => $nama,
    'jabatan' => $jabatan,
    'golongan' => $golongan,
    'pangkat' => $pangkat,
    'id_skpd' => $this->session->userdata('id_skpd'),
    'id_desa' => $this->session->userdata('id_desa'),
    'id_kecamatan' => $this->session->userdata('id_kecamatan'),
    'id_kabupaten' => $this->session->userdata('id_kabupaten'),
    'id_propinsi' => $this->session->userdata('id_propinsi'),
    
    'created_by' => $this->session->userdata('id_users'),
    'created_time' => date('Y-m-d H:i:s'),
    'updated_by' => 0),
    'updated_time' => date('Y-m-d H:i:s'),
    'deleted_by' => 0,
    'deleted_time' => date('Y-m-d H:i:s'),
    'last_login' => date('Y-m-d H:i:s'),
    'status' => 1
    		
    );
    $table_name = 'users';
    $id  = $this->Users_model->simpan_users($data_input, $table_name);
    echo $id;
    $table_name  = 'attachment';
    $where  = array(
      'table_name' => 'users',
      'temp' => $temp
      );
    $data_update = array(
      'id_tabel' => $id
      );
    $this->Crud_model->update_data($data_update, $where, $table_name);
    }
  }
  
  public function update_data_users()
  {
  $id_users  = trim($this->input->post('id_users'));
  $temp  = trim($this->input->post('temp')); 
  $this->form_validation->set_rules('user_name', 'user_name', 'required');
  $user_name  = trim($this->input->post('user_name'));
  //$this->form_validation->set_rules('password', 'password', 'required');
  //$password  = trim($this->input->post('password'));
  $this->form_validation->set_rules('hak_akses', 'hak_akses', 'required');
  $hak_akses  = trim($this->input->post('hak_akses'));
  $this->form_validation->set_rules('nip', 'nip', 'required');
  $nip  = trim($this->input->post('nip'));
  $this->form_validation->set_rules('nama', 'nama', 'required');
  $nama  = trim($this->input->post('nama'));
  $this->form_validation->set_rules('jabatan', 'jabatan', 'required');
  $jabatan  = trim($this->input->post('jabatan'));
  $this->form_validation->set_rules('golongan', 'golongan', 'required');
  $golongan  = trim($this->input->post('golongan'));
  $this->form_validation->set_rules('pangkat', 'pangkat', 'required');
  $pangkat  = trim($this->input->post('pangkat'));
  
  $id_skpd  = trim($this->input->post('id_skpd'));
  
  $id_desa  = trim($this->input->post('id_desa'));
  
  $id_kecamatan  = trim($this->input->post('id_kecamatan'));
  
  $id_kabupaten  = trim($this->input->post('id_kabupaten'));
  
  $id_propinsi  = trim($this->input->post('id_propinsi'));
  
  
  $created_by  = trim($this->input->post('created_by'));
 
  $created_time  = trim($this->input->post('created_time'));
  
  
  
  if($this->form_validation->run() == FALSE)
    {
    echo '[]';
    }
  else
    {
     $data_update = array(
      'password' => $this->bcrypt->hash_password($user_name),
      'user_name' => $user_name,
      'hak_akses' => $hak_akses,
      'nip' => $nip,
      'nama' => $nama,
      'jabatan' => $jabatan,
      'golongan' => $golongan,
      'pangkat' => $pangkat,
      'id_skpd' => $id_skpd,
      'id_desa' => $id_desa,
      'id_kecamatan' => $id_kecamatan,
      'id_kabupaten' => $id_kabupaten,
      'id_propinsi' => $id_propinsi
      			 
      );
   $table_name  = 'users';
   $where  = array(
      'users.id_users' => $id_users   
      );
    $this->Users_model->update_data_users($data_update, $where, $table_name);
    echo '[{"save":"ok"}]';
    }
  }
  public function il_simpan_users()
  {
  //------------------------------------------------
  $this->form_validation->set_rules('id_users', 'id_users', 'required');
  $id_users  = trim($this->input->post('id_users'));
  $this->form_validation->set_rules('pelayanan_id_propinsi', 'pelayanan_id_propinsi', 'required');
  $pelayanan_id_propinsi  = trim($this->input->post('pelayanan_id_propinsi'));
  $this->form_validation->set_rules('pelayanan_id_kabupaten', 'pelayanan_id_kabupaten', 'required');
  $pelayanan_id_kabupaten  = trim($this->input->post('pelayanan_id_kabupaten'));
  $this->form_validation->set_rules('pelayanan_id_kecamatan', 'pelayanan_id_kecamatan', 'required');
  $pelayanan_id_kecamatan  = trim($this->input->post('pelayanan_id_kecamatan'));
  $this->form_validation->set_rules('pelayanan_id_desa', 'pelayanan_id_desa', 'required');
  $pelayanan_id_desa  = trim($this->input->post('pelayanan_id_desa'));
  $this->form_validation->set_rules('id_penduduk', 'id_penduduk', 'required');
  $id_penduduk  = trim($this->input->post('id_penduduk'));
  $this->form_validation->set_rules('id_kartu_keluarga', 'id_kartu_keluarga', 'required');
  $id_kartu_keluarga  = trim($this->input->post('id_kartu_keluarga'));
  $this->form_validation->set_rules('password', 'password', 'required');
  $password  = trim($this->input->post('password'));
  $this->form_validation->set_rules('nama', 'nama', 'required');
  $nama  = trim($this->input->post('nama'));
  $this->form_validation->set_rules('user_name', 'user_name', 'required');
  $user_name  = trim($this->input->post('user_name'));
  $this->form_validation->set_rules('tempat_lahir', 'tempat_lahir', 'required');
  $tempat_lahir  = trim($this->input->post('tempat_lahir'));
  $this->form_validation->set_rules('tanggal_lahir_asli_disduk', 'tanggal_lahir_asli_disduk', 'required');
  $tanggal_lahir_asli_disduk  = trim($this->input->post('tanggal_lahir_asli_disduk'));
  $this->form_validation->set_rules('tanggal_lahir', 'tanggal_lahir', 'required');
  $tanggal_lahir  = trim($this->input->post('tanggal_lahir'));
  $this->form_validation->set_rules('id_jenis_kelamin', 'id_jenis_kelamin', 'required');
  $id_jenis_kelamin  = trim($this->input->post('id_jenis_kelamin'));
  $this->form_validation->set_rules('id_shdk', 'id_shdk', 'required');
  $id_shdk  = trim($this->input->post('id_shdk'));
  $this->form_validation->set_rules('hak_akses', 'hak_akses', 'required');
  $hak_akses  = trim($this->input->post('hak_akses'));
  $this->form_validation->set_rules('rt', 'rt', 'required');
  $rt  = trim($this->input->post('rt'));
  $this->form_validation->set_rules('rw', 'rw', 'required');
  $rw  = trim($this->input->post('rw'));
  $this->form_validation->set_rules('dusun', 'dusun', 'required');
  $dusun  = trim($this->input->post('dusun'));
  $this->form_validation->set_rules('id_desa', 'id_desa', 'required');
  $id_desa  = trim($this->input->post('id_desa'));
  $this->form_validation->set_rules('kode_desa', 'kode_desa', 'required');
  $kode_desa  = trim($this->input->post('kode_desa'));
  $this->form_validation->set_rules('id_kecamatan', 'id_kecamatan', 'required');
  $id_kecamatan  = trim($this->input->post('id_kecamatan'));
  $this->form_validation->set_rules('kode_kecamatan', 'kode_kecamatan', 'required');
  $kode_kecamatan  = trim($this->input->post('kode_kecamatan'));
  $this->form_validation->set_rules('id_kabupaten', 'id_kabupaten', 'required');
  $id_kabupaten  = trim($this->input->post('id_kabupaten'));
  $this->form_validation->set_rules('kode_kabupaten', 'kode_kabupaten', 'required');
  $kode_kabupaten  = trim($this->input->post('kode_kabupaten'));
  $this->form_validation->set_rules('id_propinsi', 'id_propinsi', 'required');
  $id_propinsi  = trim($this->input->post('id_propinsi'));
  $this->form_validation->set_rules('kode_propinsi', 'kode_propinsi', 'required');
  $kode_propinsi  = trim($this->input->post('kode_propinsi'));
  $this->form_validation->set_rules('kode_pos', 'kode_pos', 'required');
  $kode_pos  = trim($this->input->post('kode_pos'));
  $this->form_validation->set_rules('id_agama', 'id_agama', 'required');
  $id_agama  = trim($this->input->post('id_agama'));
  $this->form_validation->set_rules('id_pendidikan', 'id_pendidikan', 'required');
  $id_pendidikan  = trim($this->input->post('id_pendidikan'));
  $this->form_validation->set_rules('id_pekerjaan', 'id_pekerjaan', 'required');
  $id_pekerjaan  = trim($this->input->post('id_pekerjaan'));
  $this->form_validation->set_rules('nama_ibu_kandung', 'nama_ibu_kandung', 'required');
  $nama_ibu_kandung  = trim($this->input->post('nama_ibu_kandung'));
  $this->form_validation->set_rules('id_kewarganegaraan', 'id_kewarganegaraan', 'required');
  $id_kewarganegaraan  = trim($this->input->post('id_kewarganegaraan'));
  $this->form_validation->set_rules('foto', 'foto', 'required');
  $foto  = trim($this->input->post('foto'));
  $this->form_validation->set_rules('last_update', 'last_update', 'required');
  $last_update  = trim($this->input->post('last_update'));
  $this->form_validation->set_rules('id_ibu_kandung', 'id_ibu_kandung', 'required');
  $id_ibu_kandung  = trim($this->input->post('id_ibu_kandung'));
  $this->form_validation->set_rules('id_bapak_kandung', 'id_bapak_kandung', 'required');
  $id_bapak_kandung  = trim($this->input->post('id_bapak_kandung'));
  $this->form_validation->set_rules('nama_bapak_kandung', 'nama_bapak_kandung', 'required');
  $nama_bapak_kandung  = trim($this->input->post('nama_bapak_kandung'));
  $this->form_validation->set_rules('id_golongan_darah', 'id_golongan_darah', 'required');
  $id_golongan_darah  = trim($this->input->post('id_golongan_darah'));
  $this->form_validation->set_rules('nomor_akta_lahir', 'nomor_akta_lahir', 'required');
  $nomor_akta_lahir  = trim($this->input->post('nomor_akta_lahir'));
  $this->form_validation->set_rules('tgl_akta_lahir', 'tgl_akta_lahir', 'required');
  $tgl_akta_lahir  = trim($this->input->post('tgl_akta_lahir'));
  $this->form_validation->set_rules('id_status_peruser_nameahan', 'id_status_peruser_nameahan', 'required');
  $id_status_peruser_nameahan  = trim($this->input->post('id_status_peruser_nameahan'));
  $this->form_validation->set_rules('created_by', 'created_by', 'required');
  $created_by  = trim($this->input->post('created_by'));
  $this->form_validation->set_rules('created_time', 'created_time', 'required');
  $created_time  = trim($this->input->post('created_time'));
  //$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
  $updated_by  = $this->session->userdata('id_users');
  //$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
  $updated_time  = date('Y-m-d H:i:s');
  //$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
  $deleted_by  = $this->session->userdata('id_users');
  //$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
  $deleted_time  = date('Y-m-d H:i:s');
  //$this->form_validation->set_rules('status', 'status', 'required');
  $status  = '';
  
  //------------------------------------------------
    
  if($this->form_validation->run() == FALSE)
    {
      echo 'Error';
    }
  else
    {
      $data_update = array(
			
        'pelayanan_id_propinsi' => $pelayanan_id_propinsi,
        'pelayanan_id_kabupaten' => $pelayanan_id_kabupaten,
        'pelayanan_id_kecamatan' => $pelayanan_id_kecamatan,
        'pelayanan_id_desa' => $pelayanan_id_desa,
        'id_penduduk' => $id_penduduk,
        'id_kartu_keluarga' => $id_kartu_keluarga,
        'password' => $password,
        'nama' => $nama,
        'user_name' => $user_name,
        'tempat_lahir' => $tempat_lahir,
        'tanggal_lahir_asli_disduk' => $tanggal_lahir_asli_disduk,
        'tanggal_lahir' => $tanggal_lahir,
        'id_jenis_kelamin' => $id_jenis_kelamin,
        'id_shdk' => $id_shdk,
        'hak_akses' => $hak_akses,
        'rt' => $rt,
        'rw' => $rw,
        'dusun' => $dusun,
        'id_desa' => $id_desa,
        'kode_desa' => $kode_desa,
        'id_kecamatan' => $id_kecamatan,
        'kode_kecamatan' => $kode_kecamatan,
        'id_kabupaten' => $id_kabupaten,
        'kode_kabupaten' => $kode_kabupaten,
        'id_propinsi' => $id_propinsi,
        'kode_propinsi' => $kode_propinsi,
        'kode_pos' => $kode_pos,
        'id_agama' => $id_agama,
        'id_pendidikan' => $id_pendidikan,
        'id_pekerjaan' => $id_pekerjaan,
        'nama_ibu_kandung' => $nama_ibu_kandung,
        'id_kewarganegaraan' => $id_kewarganegaraan,
        'foto' => $foto,
        'last_update' => $last_update,
        'id_ibu_kandung' => $id_ibu_kandung,
        'id_bapak_kandung' => $id_bapak_kandung,
        'nama_bapak_kandung' => $nama_bapak_kandung,
        'id_golongan_darah' => $id_golongan_darah,
        'nomor_akta_lahir' => $nomor_akta_lahir,
        'tgl_akta_lahir' => $tgl_akta_lahir,
        'id_status_peruser_nameahan' => $id_status_peruser_nameahan,
        'created_by' => $created_by,
        'created_time' => $created_time,
        'updated_by' => $updated_by,
        'updated_time' => $updated_time,
        				
        );
      $table_name  = 'users';
      $where  = array(
        'users.id_users' => $id_users   
        );
      $this->Users_model->update_data_users($data_update, $where, $table_name);
      echo 'Success';
    }
  }
  public function ag_simpan_users()
  {
  //--------------------------------------------------
  $this->form_validation->set_rules('id_users', 'id_users', 'required');
  $id_users  = trim($this->input->post('id_users'));
  $this->form_validation->set_rules('pelayanan_id_propinsi', 'pelayanan_id_propinsi', 'required');
  $pelayanan_id_propinsi  = trim($this->input->post('pelayanan_id_propinsi'));
  $this->form_validation->set_rules('pelayanan_id_kabupaten', 'pelayanan_id_kabupaten', 'required');
  $pelayanan_id_kabupaten  = trim($this->input->post('pelayanan_id_kabupaten'));
  $this->form_validation->set_rules('pelayanan_id_kecamatan', 'pelayanan_id_kecamatan', 'required');
  $pelayanan_id_kecamatan  = trim($this->input->post('pelayanan_id_kecamatan'));
  $this->form_validation->set_rules('pelayanan_id_desa', 'pelayanan_id_desa', 'required');
  $pelayanan_id_desa  = trim($this->input->post('pelayanan_id_desa'));
  $this->form_validation->set_rules('id_penduduk', 'id_penduduk', 'required');
  $id_penduduk  = trim($this->input->post('id_penduduk'));
  $this->form_validation->set_rules('id_kartu_keluarga', 'id_kartu_keluarga', 'required');
  $id_kartu_keluarga  = trim($this->input->post('id_kartu_keluarga'));
  $this->form_validation->set_rules('password', 'password', 'required');
  $password  = trim($this->input->post('password'));
  $this->form_validation->set_rules('nama', 'nama', 'required');
  $nama  = trim($this->input->post('nama'));
  $this->form_validation->set_rules('user_name', 'user_name', 'required');
  $user_name  = trim($this->input->post('user_name'));
  $this->form_validation->set_rules('tempat_lahir', 'tempat_lahir', 'required');
  $tempat_lahir  = trim($this->input->post('tempat_lahir'));
  $this->form_validation->set_rules('tanggal_lahir_asli_disduk', 'tanggal_lahir_asli_disduk', 'required');
  $tanggal_lahir_asli_disduk  = trim($this->input->post('tanggal_lahir_asli_disduk'));
  $this->form_validation->set_rules('tanggal_lahir', 'tanggal_lahir', 'required');
  $tanggal_lahir  = trim($this->input->post('tanggal_lahir'));
  $this->form_validation->set_rules('id_jenis_kelamin', 'id_jenis_kelamin', 'required');
  $id_jenis_kelamin  = trim($this->input->post('id_jenis_kelamin'));
  $this->form_validation->set_rules('id_shdk', 'id_shdk', 'required');
  $id_shdk  = trim($this->input->post('id_shdk'));
  $this->form_validation->set_rules('hak_akses', 'hak_akses', 'required');
  $hak_akses  = trim($this->input->post('hak_akses'));
  $this->form_validation->set_rules('rt', 'rt', 'required');
  $rt  = trim($this->input->post('rt'));
  $this->form_validation->set_rules('rw', 'rw', 'required');
  $rw  = trim($this->input->post('rw'));
  $this->form_validation->set_rules('dusun', 'dusun', 'required');
  $dusun  = trim($this->input->post('dusun'));
  $this->form_validation->set_rules('id_desa', 'id_desa', 'required');
  $id_desa  = trim($this->input->post('id_desa'));
  $this->form_validation->set_rules('kode_desa', 'kode_desa', 'required');
  $kode_desa  = trim($this->input->post('kode_desa'));
  $this->form_validation->set_rules('id_kecamatan', 'id_kecamatan', 'required');
  $id_kecamatan  = trim($this->input->post('id_kecamatan'));
  $this->form_validation->set_rules('kode_kecamatan', 'kode_kecamatan', 'required');
  $kode_kecamatan  = trim($this->input->post('kode_kecamatan'));
  $this->form_validation->set_rules('id_kabupaten', 'id_kabupaten', 'required');
  $id_kabupaten  = trim($this->input->post('id_kabupaten'));
  $this->form_validation->set_rules('kode_kabupaten', 'kode_kabupaten', 'required');
  $kode_kabupaten  = trim($this->input->post('kode_kabupaten'));
  $this->form_validation->set_rules('id_propinsi', 'id_propinsi', 'required');
  $id_propinsi  = trim($this->input->post('id_propinsi'));
  $this->form_validation->set_rules('kode_propinsi', 'kode_propinsi', 'required');
  $kode_propinsi  = trim($this->input->post('kode_propinsi'));
  $this->form_validation->set_rules('kode_pos', 'kode_pos', 'required');
  $kode_pos  = trim($this->input->post('kode_pos'));
  $this->form_validation->set_rules('id_agama', 'id_agama', 'required');
  $id_agama  = trim($this->input->post('id_agama'));
  $this->form_validation->set_rules('id_pendidikan', 'id_pendidikan', 'required');
  $id_pendidikan  = trim($this->input->post('id_pendidikan'));
  $this->form_validation->set_rules('id_pekerjaan', 'id_pekerjaan', 'required');
  $id_pekerjaan  = trim($this->input->post('id_pekerjaan'));
  $this->form_validation->set_rules('nama_ibu_kandung', 'nama_ibu_kandung', 'required');
  $nama_ibu_kandung  = trim($this->input->post('nama_ibu_kandung'));
  $this->form_validation->set_rules('id_kewarganegaraan', 'id_kewarganegaraan', 'required');
  $id_kewarganegaraan  = trim($this->input->post('id_kewarganegaraan'));
  $this->form_validation->set_rules('foto', 'foto', 'required');
  $foto  = trim($this->input->post('foto'));
  $this->form_validation->set_rules('last_update', 'last_update', 'required');
  $last_update  = trim($this->input->post('last_update'));
  $this->form_validation->set_rules('id_ibu_kandung', 'id_ibu_kandung', 'required');
  $id_ibu_kandung  = trim($this->input->post('id_ibu_kandung'));
  $this->form_validation->set_rules('id_bapak_kandung', 'id_bapak_kandung', 'required');
  $id_bapak_kandung  = trim($this->input->post('id_bapak_kandung'));
  $this->form_validation->set_rules('nama_bapak_kandung', 'nama_bapak_kandung', 'required');
  $nama_bapak_kandung  = trim($this->input->post('nama_bapak_kandung'));
  $this->form_validation->set_rules('id_golongan_darah', 'id_golongan_darah', 'required');
  $id_golongan_darah  = trim($this->input->post('id_golongan_darah'));
  $this->form_validation->set_rules('nomor_akta_lahir', 'nomor_akta_lahir', 'required');
  $nomor_akta_lahir  = trim($this->input->post('nomor_akta_lahir'));
  $this->form_validation->set_rules('tgl_akta_lahir', 'tgl_akta_lahir', 'required');
  $tgl_akta_lahir  = trim($this->input->post('tgl_akta_lahir'));
  $this->form_validation->set_rules('id_status_peruser_nameahan', 'id_status_peruser_nameahan', 'required');
  $id_status_peruser_nameahan  = trim($this->input->post('id_status_peruser_nameahan'));
  $this->form_validation->set_rules('created_by', 'created_by', 'required');
  $created_by  = trim($this->input->post('created_by'));
  $this->form_validation->set_rules('created_time', 'created_time', 'required');
  $created_time  = trim($this->input->post('created_time'));
  //$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
  $updated_by  = $this->session->userdata('id_users');
  //$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
  $updated_time  = date('Y-m-d H:i:s');
  //$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
  $deleted_by  = $this->session->userdata('id_users');
  //$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
  $deleted_time  = date('Y-m-d H:i:s');
  //$this->form_validation->set_rules('status', 'status', 'required');
  $status  = '';
  	
  //--------------------------------------------------
  
  if($this->form_validation->run() == FALSE)
    {
      echo '[]';
    }
  else
    {
      $data_update = array(
			
        'pelayanan_id_propinsi' => $pelayanan_id_propinsi,
        'pelayanan_id_kabupaten' => $pelayanan_id_kabupaten,
        'pelayanan_id_kecamatan' => $pelayanan_id_kecamatan,
        'pelayanan_id_desa' => $pelayanan_id_desa,
        'id_penduduk' => $id_penduduk,
        'id_kartu_keluarga' => $id_kartu_keluarga,
        'password' => $password,
        'nama' => $nama,
        'user_name' => $user_name,
        'tempat_lahir' => $tempat_lahir,
        'tanggal_lahir_asli_disduk' => $tanggal_lahir_asli_disduk,
        'tanggal_lahir' => $tanggal_lahir,
        'id_jenis_kelamin' => $id_jenis_kelamin,
        'id_shdk' => $id_shdk,
        'hak_akses' => $hak_akses,
        'rt' => $rt,
        'rw' => $rw,
        'dusun' => $dusun,
        'id_desa' => $id_desa,
        'kode_desa' => $kode_desa,
        'id_kecamatan' => $id_kecamatan,
        'kode_kecamatan' => $kode_kecamatan,
        'id_kabupaten' => $id_kabupaten,
        'kode_kabupaten' => $kode_kabupaten,
        'id_propinsi' => $id_propinsi,
        'kode_propinsi' => $kode_propinsi,
        'kode_pos' => $kode_pos,
        'id_agama' => $id_agama,
        'id_pendidikan' => $id_pendidikan,
        'id_pekerjaan' => $id_pekerjaan,
        'nama_ibu_kandung' => $nama_ibu_kandung,
        'id_kewarganegaraan' => $id_kewarganegaraan,
        'foto' => $foto,
        'last_update' => $last_update,
        'id_ibu_kandung' => $id_ibu_kandung,
        'id_bapak_kandung' => $id_bapak_kandung,
        'nama_bapak_kandung' => $nama_bapak_kandung,
        'id_golongan_darah' => $id_golongan_darah,
        'nomor_akta_lahir' => $nomor_akta_lahir,
        'tgl_akta_lahir' => $tgl_akta_lahir,
        'id_status_peruser_nameahan' => $id_status_peruser_nameahan,
        'created_by' => $created_by,
        'created_time' => $created_time,
        'updated_by' => $updated_by,
        'updated_time' => $updated_time,
        				
        );
      $table_name  = 'users';
      $where  = array(
        'users.id_users' => $id_users   
        );
      $this->Users_model->update_data_users($data_update, $where, $table_name);
      echo '[{"users":"ok", "jumlah":"ok"}]';
    }
  }
  public function cetak()
  {
  $where   = array(
    'users.status !=' => 99
    );
  $data['data_users'] = $this->Users_model->data_users($where);
  $this->load->view('users/cetak', $data);
  }
  public function users_get_by_id()
  {
  $table_name = 'users';
  $id  = $_GET['id'];
  $fields  = "
	
    users.id_users,
    users.user_name,
    users.password,
    users.hak_akses,
    users.nip,
    users.nama,
    users.jabatan,
    users.golongan,
    users.pangkat,
    users.id_skpd,
    users.id_desa,
    users.id_kecamatan,
    users.id_kabupaten,
    users.id_propinsi,
    users.created_by,
    users.created_time,
    users.updated_by,
    users.updated_time,
    users.deleted_by,
    users.deleted_time,
    users.status,
    users.last_login,
    
    (SELECT desa.nama_desa FROM desa WHERE desa.id_desa=users.id_desa) as nama_desa,
    (SELECT kecamatan.nama_kecamatan FROM kecamatan WHERE kecamatan.id_kecamatan=users.id_kecamatan) as nama_kecamatan,
    (SELECT kabupaten.nama_kabupaten FROM kabupaten WHERE kabupaten.id_kabupaten=users.id_kabupaten) as nama_kabupaten,
    (SELECT propinsi.nama_propinsi FROM propinsi WHERE propinsi.id_propinsi=users.id_propinsi) as nama_propinsi
    		
    ";
  $where   = array(
    'users.id_users' => $id
    );
  $order_by  = 'users.nama';
  echo json_encode($this->Users_model->get_by_id($table_name, $where, $fields, $order_by));
  }
  
  
  public function cek_user_name()
  {
  $table_name = 'penduduk';
  $user_name  = trim($this->input->post('user_name'));
  $fields  = "*";
  $where   = array(
    'penduduk.user_name' => $user_name
    );
  $order_by  = 'penduduk.nama';
  echo json_encode($this->Users_model->get_by_id($table_name, $where, $fields, $order_by));
  }
  
  public function hapus_users()
  {
  $table_name = 'users';
  $id  = $_GET['id'];
  $where   = array(
    'users.id_users' => $id
    );
  $t  = $this->Users_model->json_semua_users($where, $table_name);
  if ($t == 0)
    {
      echo ' {"errors":"Yes"} ';
    }
  else
    {
      $data_update = array(
        'users.status' => 99
        );
      $where  = array(
        'users.id_users' => $id
        );
      $this->Users_model->update_data_users($data_update, $where, $table_name);
      echo ' {"errors":"No"} ';
    }
  }
  public function cari_users()
  {
  $table_name = 'users';
  $key_word  = $_GET['key_word'];
  $where   = array(
    'users.status !=' => 99
    );
  $field   = 'users.nama';
  $a  = $this->Users_model->count_all_search_users($where, $key_word, $table_name, $field);
  if (empty($key_word))
    {
      echo '[]';
      exit;
    }
  else if ($a == 0)
    {
      echo '[{"jumlah":"0"}]';
      exit;
    }
  $fields  = "
    users.id_users,
    users.user_name,
    users.password,
    users.hak_akses,
    users.nip,
    users.nama,
    users.jabatan,
    users.golongan,
    users.pangkat,
    users.id_skpd,
    users.id_desa,
    users.id_kecamatan,
    users.id_kabupaten,
    users.id_propinsi,
    users.created_by,
    users.created_time,
    users.updated_by,
    users.updated_time,
    users.deleted_by,
    users.deleted_time,
    users.status,
    users.last_login,
    
    (SELECT desa.nama_desa FROM desa WHERE desa.id_desa=users.id_desa) as nama_desa,
    (SELECT kecamatan.nama_kecamatan FROM kecamatan WHERE kecamatan.id_kecamatan=users.id_kecamatan) as nama_kecamatan,
    (SELECT kabupaten.nama_kabupaten FROM kabupaten WHERE kabupaten.id_kabupaten=users.id_kabupaten) as nama_kabupaten,
    (SELECT propinsi.nama_propinsi FROM propinsi WHERE propinsi.id_propinsi=users.id_propinsi) as nama_propinsi
    		
    ";
  $where   = array(
    'users.status !=' => 99
    );
  $field_like = 'users.nama';
  $limit   = 10;
  $start   = (($_GET['start']) * $limit);
  $order_by  = ''.$field_like.'';
  echo json_encode($this->Users_model->search($table_name, $fields, $where, $limit, $start, $field_like, $key_word, $order_by));
  }
  public function count_all_cari_users()
  {
  $table_name = 'users';
  $key_word  = $_GET['key_word'];
  if (empty($key_word))
    {
      echo '[]';
      exit;
    }
  $where = array(
    'users.status !=' => 99
    );
  $field = 'users.nama';
  $a  = $this->Users_model->count_all_search_users($where, $key_word, $table_name, $field);
  $limit = 10;
  echo '[{"users":"' . ceil($a / $limit) .'", "jumlah":"'.$a .'"}]';
  }
  public function download_xls()
  {
  $this->load->library('excel');
  $objPHPExcel = new PHPExcel();
  $objPHPExcel
   ->getProperties()
   ->setCreator("Budi Utomo")
   ->setLastModifiedBy("Budi Utomo")
   ->setTitle("Laporan I")
   ->setSubject("Office 2007 XLSX Document")
   ->setDescription("Laporan untuk menampilkan kunjungan pasien")
   ->setKeywords("Laporan Halaman")
   ->setCategory("Bentuk XLS");
  $objPHPExcel
   ->setActiveSheetIndex(0)
   ->setCellValue('A1', 'Cetak')
   ->mergeCells('A1:G1')
   ->setCellValue('A2', 'Laporan Data Halaman')
   ->mergeCells('A2:G2')
   ->setCellValue('A3', 'Tanggal Download : ' . date('d/m/Y') .' ')
   ->mergeCells('A3:G3')
   ->setCellValue('A6', 'No')
   ->setCellValue('B6', 'Nama Users')
   ->setCellValue('C6', 'Kode Users')
   ->setCellValue('D6', 'Keterangan');
  $table  = 'users';
  $fields  = "
    users.id_users,
    users.user_name,
    users.password,
    users.hak_akses,
    users.nip,
    users.nama,
    users.jabatan,
    users.golongan,
    users.pangkat,
    users.id_skpd,
    users.id_desa,
    users.id_kecamatan,
    users.id_kabupaten,
    users.id_propinsi,
    users.created_by,
    users.created_time,
    users.updated_by,
    users.updated_time,
    users.deleted_by,
    users.deleted_time,
    users.status,
    users.last_login,
    		
    ";
  $where  = array(
    'users.status !=' => 99
    );
  $order_by = 'users.nama';
  $b   = $this->Cetak_model->cetak_single_table($table, $fields, $where, $order_by);
  $i   = 7;
  $no  = 0;
  foreach ($b->result() as $b1)
  {
   $no = $no + 1;
   $objPHPExcel->setActiveSheetIndex(0)
   ->setCellValue('A'.$i, ''.$no.'')
   ->setCellValue('B'.$i, ''.$b1->nama.'')
   ->setCellValue('C'.$i, '\''.$b1->nama.'')
   ->setCellValue('D'.$i, ''.$b1->keterangan.'')
   ;
   
   $objPHPExcel
   ->getActiveSheet()
   ->getStyle('A'.$i.':D'.$i.'')
   ->getBorders()
   ->getAllBorders()
   ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
   $i++;
  }
  $objPHPExcel->getActiveSheet()->setTitle('Probable Clients');
  $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
  $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
  
  $objPHPExcel->getActiveSheet()->getStyle('A6')->getFont()->setBold(true);
  $objPHPExcel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);
  $objPHPExcel->getActiveSheet()->getStyle('C6')->getFont()->setBold(true);
  $objPHPExcel->getActiveSheet()->getStyle('D6')->getFont()->setBold(true);
  
  $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
  $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->getStartColor()->setARGB('cccccc');
  $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
  
  $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
  $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
  $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(11);
  $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(11);
  
  $objPHPExcel->setActiveSheetIndex(0);
  header('Content-Type: application/vnd.ms-excel');
  header('Content-Disposition: attachment;filename="Halaman_' . date('ymdhis') .'.xls"');
  header('Cache-Control: max-age=0');
  header('Cache-Control: max-age=1');
  header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
  header('Last-Modified: ' . gmdate('D, d M Y H:i:s') .' GMT');
  header('Cache-Control: cache, must-revalidate');
  header('Pragma: public');
  $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
  $objWriter->save('php://output');
  }
  
  public function cetak_pdf()
  {
  $table   = 'users';
  $fields  = "
    users.id_users,
    users.user_name,
    users.password,
    users.hak_akses,
    users.nip,
    users.nama,
    users.jabatan,
    users.golongan,
    users.pangkat,
    users.id_skpd,
    users.id_desa,
    users.id_kecamatan,
    users.id_kabupaten,
    users.id_propinsi,
    users.created_by,
    users.created_time,
    users.updated_by,
    users.updated_time,
    users.deleted_by,
    users.deleted_time,
    users.status,
    users.last_login,
    		
    ";
  $where   = array(
    'users.status !=' => 99
    );
  $order_by  = 'users.nama';
  $data['data_users'] = $this->Cetak_model->cetak_single_table($table, $fields, $where, $order_by);
  $this->load->view('users/pdf', $data);
  $html = $this->output->get_output();
  $this->load->library('dompdf_gen');
  $this->dompdf->load_html($html);
  $this->dompdf->set_paper('a4', 'portrait');
  $this->dompdf->render();
  $this->dompdf->stream("cetak_users_" . date('d_m_y') . ".pdf");
  } 
  public function search_users()
  {
  $key_word = trim($this->input->post('key_word'));
  $halaman  = $this->input->post('halaman');
  $limit   = 10;
  $start   = ($halaman - 1) * $limit;
  $fields  = "
	
    users.id_users,
    users.user_name,
    users.password,
    users.hak_akses,
    users.nip,
    users.nama,
    users.jabatan,
    users.golongan,
    users.pangkat,
    users.id_skpd,
    users.id_desa,
    users.id_kecamatan,
    users.id_kabupaten,
    users.id_propinsi,
    users.created_by,
    users.created_time,
    users.updated_by,
    users.updated_time,
    users.deleted_by,
    users.deleted_time,
    users.status,
    users.last_login,
    (SELECT desa.nama_desa FROM desa WHERE desa.id_desa=users.id_desa) as nama_desa,
    (SELECT kecamatan.nama_kecamatan FROM kecamatan WHERE kecamatan.id_kecamatan=users.id_kecamatan) as nama_kecamatan,
    (SELECT kabupaten.nama_kabupaten FROM kabupaten WHERE kabupaten.id_kabupaten=users.id_kabupaten) as nama_kabupaten,
    (SELECT propinsi.nama_propinsi FROM propinsi WHERE propinsi.id_propinsi=users.id_propinsi) as nama_propinsi
    		
    ";
  $where   = array(
    'users.status !=' => 99
    );
  $order_by  = 'users.nama';
  echo json_encode($this->Users_model->search_users($fields, $where, $limit, $start, $key_word, $order_by));
  }
  public function count_page_all_search_users()
  {
  $table_name = 'users';
  $key_word = $_GET['key_word'];
  $field = 'nama';
  $where = array(
    ''.$table_name.'.status' => 1
    );
  $a = $this->Users_model->count_all_search_users($where, $key_word, $table_name, $field);
  $limit = 10;
  echo ceil($a/$limit); 
  }
  public function count_all_search_users()
  {
  $table_name = 'users';
  $key_word = $_GET['key_word'];
  $field = 'nama';
  $where = array(
    ''.$table_name.'.status' => 1
    );
  $a = $this->Users_model->count_all_search_users($where, $key_word, $table_name, $field);
  echo $a; 
  }
  public function auto_suggest()
  {
  $q = $_GET['q'];
  if(empty($_GET['q']))
    {
      exit;
    }
  if( strlen($q) > 2)
    {
      $where   = array(
        'users.status !=' => 99
        );
      $fields  = "
			
        users.id_users,
        users.user_name,
        users.password,
        users.hak_akses,
        users.nip,
        users.nama,
        users.jabatan,
        users.golongan,
        users.pangkat,
        users.id_skpd,
        users.id_desa,
        users.id_kecamatan,
        users.id_kabupaten,
        users.id_propinsi,
        users.created_by,
        users.created_time,
        users.updated_by,
        users.updated_time,
        users.deleted_by,
        users.deleted_time,
        users.status,
        users.last_login,
        users.nama as value
				
        ";
      echo json_encode( $this->Users_model->auto_suggest($q, $where, $fields) );
    }
  }
}