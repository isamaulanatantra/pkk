<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Daftar_informasi_publik_pembantu extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Daftar_informasi_publik_pembantu_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'daftar_informasi_publik_pembantu/home';
    $this->load->view('back_bone', $data);
   }
  function load_table_daftar_informasi_publik_pembantu_domain()
	{
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = 9999999999;
    $kata_kunci    = $this->input->post('kata_kunci');
    $klasifikasi_informasi_publik_pembantu_domain    = $this->input->post('klasifikasi_informasi_publik_pembantu_domain');
    $opd_domain    = $this->input->post('opd_domain');
		$a=0;
		$a = $a + 1;
		if($opd_domain==''){
			$skpd_website="";
		}
		else{
			$skpd_website="and posting.domain='".$this->input->post('opd_domain')."' ";
		}

		$w = $this->db->query("
		SELECT *, (select user_name from users where users.id_users=posting.created_by) as nama_pengguna 
		from posting, data_skpd
		where posting.status = 1 
		".$skpd_website."
		and posting.".$klasifikasi_informasi_publik_pembantu_domain." = 1
		and data_skpd.skpd_website=posting.domain 
		and data_skpd.status=1
    order by posting.posisi, posting.urut
		limit ".$limit."
		");
		
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", "#", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
			$yummy   = array("_", "", "", "", "","", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			$judul_posting = str_replace($healthy, $yummy, $h->judul_posting);
			echo '<tr style="padding: 2px;" id_daftar_informasi_publik_pembantu="'.$h->id_posting.'" id="'.$h->id_posting.'" >';
				echo '<td style="padding: 2px;">'.$nomor.'</td>';
				echo '<td style="padding: 2px"><u class="text-green">'.$h->judul_posting.'</u></td>';
        echo '<td style="padding: 2px;">';
				if($h->created_time=='0000-00-00 00:00:00'){echo '';}else{echo ''.$this->Crud_model->dateBahasaIndo0($h->created_time).'';}
				echo '
				</td>
				<td style="padding: 2px;">
					<a target="_blank" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$judul_posting.'.HTML" class="btn btn-info"><i class="fa fa-eye"></i> Tampil</a>';
					$query1 = $this->db->query("SELECT file_name from attachment where id_tabel=".$h->id_posting." and file_name LIKE '%.pdf' order by uploaded_time desc limit 1");
					$x = $query1->num_rows();
					if($x==0){echo'';}
					else{
						foreach ($query1->result() as $row1){
						echo'
						<a target="_blank" href="'.base_url().'media/upload/'.$row1->file_name.'" class="btn btn-danger"><i class="fa fa-download"></i> Unduh</a>';
						}
					}
					echo'
				</td>
			</tr>';
		}
	}
  function load_table_daftar_informasi_publik_pembantu()
	{
    $web=$this->uut->namadomain(base_url());
		if($web=='ppid.wonosobokab.go.id'){
			$web_ppid="ppid.wonosobokab.go.id";
		}
		else{
			$web_ppid= $web;
		}
    $halaman    = $this->input->post('halaman');
    $limit    = 9999999999;
    $kata_kunci    = $this->input->post('kata_kunci');
    $klasifikasi_informasi_publik_pembantu    = $this->input->post('klasifikasi_informasi_publik_pembantu');
		$a=0;
		$a = $a + 1;
		if($klasifikasi_informasi_publik_pembantu==''){
			$kategori_opd="informasi_setiap_saat=1";
		}
		else{
			$kategori_opd="and posting.".$this->input->post('klasifikasi_informasi_publik_pembantu')." = 1 ";
		}
		$w = $this->db->query("
		SELECT *, (select user_name from users where users.id_users=posting.created_by) as nama_pengguna 
		from posting
		where posting.status = 1 
    and posting.domain='".$web_ppid."'
    ".$kategori_opd."
    order by posting.created_time desc
		limit ".$limit."
		");
		
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", "#", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
			$yummy   = array("_", "", "", "", "","", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			$judul_posting = str_replace($healthy, $yummy, $h->judul_posting);
			echo '<tr style="padding: 2px;" id_daftar_informasi_publik_pembantu="'.$h->id_posting.'" id="'.$h->id_posting.'" >';
				echo '<td style="padding: 2px;">'.$nomor.'</td>';
				echo '<td style="padding: 2px '.($a * 20).'px ;"> <a target="_blank" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$judul_posting.'.HTML"><u class="text-green">'.$h->judul_posting.'</u></a> </td>';
        echo '<td style="padding: 2px;">';
				if($h->created_time=='0000-00-00 00:00:00'){echo '';}else{echo ''.$this->Crud_model->dateBahasaIndo0($h->created_time).'';}
				echo '
				</td>
				<td style="padding: 2px;">
					<a target="_blank" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$judul_posting.'.HTML" class="btn btn-info"><i class="fa fa-eye"></i> Tampil</a>';
					$query1 = $this->db->query("SELECT file_name from attachment where id_tabel=".$h->id_posting." and file_name LIKE '%.pdf' order by uploaded_time desc limit 1");
					$x = $query1->num_rows();
					if($x==0){echo'';}
					else{
						foreach ($query1->result() as $row1){
						echo'
						<a target="_blank" href="'.base_url().'media/upload/'.$row1->file_name.'" class="btn btn-danger"><i class="fa fa-download"></i> Unduh</a>';
						}
					}
					echo'
				</td>
			</tr>';
		}
	}
  private function daftar_informasi_publik_pembantu($parent=0,$hasil, $a){
	}
  
  public function json_all_data_informasi_pembantu(){
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_daftar_informasi_publik_pembantu    = $this->input->post('urut_data_daftar_informasi_publik_pembantu');
    $opd_domain    = $this->input->post('opd_domain');
    $klasifikasi_informasi_publik_pembantu    = $this->input->post('klasifikasi_informasi_publik_pembantu');
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  *,
									skpd.nama_skpd
                  ";
    if($klasifikasi_informasi_publik_pembantu=='informasi_berkala'){
      $where      = array(
      'posting.parent !=' => 0,
      'posting.status' => 1,
      'posting.informasi_berkala' => 1,
      'posting.domain' => $opd_domain,
      'data_skpd.status' => 1
      );
    }
    else if($klasifikasi_informasi_publik_pembantu=='informasi_serta_merta'){
      $where      = array(
      'posting.parent !=' => 0,
      'posting.status' => 1,
      'posting.informasi_serta_merta' => 1,
      'posting.domain' => $opd_domain,
      'data_skpd.status' => 1
      );
    }
    else if($klasifikasi_informasi_publik_pembantu=='informasi_setiap_saat'){
      $where      = array(
      'posting.parent !=' => 0,
      'posting.status' => 1,
      'posting.informasi_setiap_saat' => 1,
      'posting.domain' => $opd_domain,
      'data_skpd.status' => 1
      );
    }
    else if($klasifikasi_informasi_publik_pembantu=='informasi_dikecualikan'){
      $where      = array(
      'posting.parent !=' => 0,
      'posting.status' => 1,
      'posting.informasi_dikecualikan' => 1,
      'posting.domain' => $opd_domain,
      'data_skpd.status' => 1
      );
    }
    else{
      $where      = array(
      'posting.parent !=' => 0,
      'posting.status' => 1,
      'posting.domain' => $opd_domain,
      'data_skpd.status' => 1
      );
    }
    $order_by   = 'posting.'.$urut_data_daftar_informasi_publik_pembantu.'';
    echo json_encode($this->Daftar_informasi_publik_pembantu_model->json_all_daftar_informasi_publik_pembantu($where, $limit, $start, $fields, $order_by, $kata_kunci));
	}
  public function json_all_daftar_informasi_publik_pembantu()
   {
    $web=$this->uut->namadomain(base_url());
		if($web=='ppiddemo.wonosobokab.go.id'){
			$skpd_website='';
		}
		else{
			$skpd_website=''.$web.'';
		}
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $urut_data_daftar_informasi_publik_pembantu    = $this->input->post('urut_data_daftar_informasi_publik_pembantu');
    $klasifikasi_informasi_publik_pembantu    = $this->input->post('klasifikasi_informasi_publik_pembantu');
    $opd_domain    = $this->input->post('opd_domain');
    // $opd_domain    = $skpd_website;
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  *,
									skpd.nama_skpd
                  ";
    if($opd_domain==''){
      $where      = array(
      'posting.parent !=' => 0,
      'posting.status' => 1,
      'data_skpd.status' => 1,
      );
    }
    else{
			if($klasifikasi_informasi_publik_pembantu==''){
				$where      = array(
				'posting.parent !=' => 0,
				'posting.status' => 1,
				'posting.domain' => $opd_domain,
				'data_skpd.status' => 1,
				);
			}
			else{
				$where      = array(
				'posting.parent !=' => 0,
				'posting.status' => 1,
				'data_skpd.status' => 1,
				'posting.'.$klasifikasi_informasi_publik_pembantu.'' => 1,
				'posting.domain' => $opd_domain,
				);
			}
    }
    $order_by   = 'posting.'.$urut_data_daftar_informasi_publik_pembantu.'';
    echo json_encode($this->Daftar_informasi_publik_pembantu_model->json_all_daftar_informasi_publik_pembantu($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
	 
  public function json_all_daftar_informasi_publik_opd()
   {
    $web=$this->uut->namadomain(base_url());
    $halaman = $this->input->post('halaman');
    $limit = $this->input->post('limit');
    $kata_kunci = $this->input->post('kata_kunci');
    $urut_data_daftar_informasi_publik_pembantu = $this->input->post('urut_data_daftar_informasi_publik_pembantu');
    $klasifikasi_informasi_publik_pembantu = $this->input->post('klasifikasi_informasi_publik_pembantu');
    // if($this->input->post('klasifikasi_informasi_publik_pembantu') ==''){$klasifikasi_informasi_publik_pembantu='status';}else{$klasifikasi_informasi_publik_pembantu=$this->input->post('klasifikasi_informasi_publik_pembantu');}
    $start      = ($halaman - 1) * $limit;
    $fields     = 
                  "
                  *
                  ";
    if($klasifikasi_informasi_publik_pembantu=='informasi_berkala'){
      $where      = array(
      'posting.parent !=' => 0,
      'posting.status' => 1,
      'posting.informasi_berkala' => 1,
      'posting.domain' => $web,
      'data_skpd.status' => 1
      );
    }
    else if($klasifikasi_informasi_publik_pembantu=='informasi_serta_merta'){
      $where      = array(
      'posting.parent !=' => 0,
      'posting.status' => 1,
      'posting.informasi_serta_merta' => 1,
      'posting.domain' => $web,
      'data_skpd.status' => 1
      );
    }
    else if($klasifikasi_informasi_publik_pembantu=='informasi_setiap_saat'){
      $where      = array(
      'posting.parent !=' => 0,
      'posting.status' => 1,
      'posting.informasi_setiap_saat' => 1,
      'posting.domain' => $web,
      'data_skpd.status' => 1
      );
    }
    else if($klasifikasi_informasi_publik_pembantu=='informasi_dikecualikan'){
      $where      = array(
      'posting.parent !=' => 0,
      'posting.status' => 1,
      'posting.informasi_dikecualikan' => 1,
      'posting.domain' => $web,
      'data_skpd.status' => 1
      );
    }
    else{
      $where      = array(
      'posting.parent !=' => 0,
      'posting.status' => 1,
      'posting.domain' => $web,
      'data_skpd.status' => 1
      );
    }
    $order_by   = 'posting.'.$urut_data_daftar_informasi_publik_pembantu.'';
    echo json_encode($this->Daftar_informasi_publik_pembantu_model->json_all_daftar_informasi_publik_pembantu_opd($where, $limit, $start, $fields, $order_by, $kata_kunci));
   }
   
  function opd_domain()
	{
    $web=$this->uut->namadomain(base_url());
		if($web=='ppid.wonosobokab.go.id'){
			$skpd_website="";
		}
		else{
			$skpd_website="and data_skpd.skpd_website='".$skpd_website."'";
		}
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *, 
		data_skpd.skpd_website 
		from skpd, data_skpd 
		where skpd.status = 1 
		and data_skpd.id_skpd=skpd.id_skpd 
		and data_skpd.status=1 
		".$skpd_website."
		order by kode_skpd");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			echo '<option value="'.$h->skpd_website.'">';
			if( $a > 1 ){
			echo ''.str_repeat("--", $a).' '.$h->nama_skpd.'';
			}
			else{
			echo''.$h->nama_skpd.'';
			}
			echo '</option>';
		}
		if(($w->num_rows)>0){
		}
	}
  function opd_domain1()
	{
		$a = 0;
		echo $this->option_opd($h="", $a);
	}
  private function option_opd($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		if($web=='ppid.wonosobokab.go.id'){
			$skpd_website="";
		}
		else{
			$skpd_website="and data_skpd.skpd_website='".$skpd_website."'";
		}
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *, 
		data_skpd.skpd_website 
		from skpd, data_skpd 
		where skpd.status = 1 
		and data_skpd.id_skpd=skpd.id_skpd 
		and data_skpd.status=1 
		".$skpd_website."
		order by kode_skpd");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->skpd_website.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->nama_skpd.'';
			}
			else{
			$hasil .= ''.$h->nama_skpd.'';
			}
			$hasil .= '</option>';
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  function kategori_parent_posting()
	{
    $opd_domain    = $this->input->post('opd_domain');
		$a = 0;
		echo $this->option_kategori_parent_posting($h="", $a, $opd_domain);
	}
  private function option_kategori_parent_posting($hasil, $a, $opd_domain){
    $web=$this->uut->namadomain(base_url());
		if($opd_domain==''){$opd_domain=""; }else{$opd_domain="and posting.domain = '".$opd_domain."'";}
		$a = $a + 1;
		$w = $this->db->query("
			SELECT *
			from posting
			where posting.parent = 0 
			and posting.status = 1 
			".$opd_domain."
			order by urut");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_posting.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->judul_posting.'';
			}
			else{
			$hasil .= ''.$h->judul_posting.'';
			}
			$hasil .= '</option>';
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
 }