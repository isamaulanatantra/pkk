<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Struktur_organisasi extends CI_Controller
{
  
  function __construct()
  {
    parent::__construct();
  }
  
  public function index()
  {
    $data['test'] = '';
    $this->load->view('back_end/struktur_organisasi/content', $data);
  }
  
  public function json()
  {
    $table       = $this->input->post('table');
    $page        = $this->input->post('page');
    $limit       = $this->input->post('limit');
    $keyword     = $this->input->post('keyword');
    $status      = $this->input->post('status');
    $order_by    = $this->input->post('order_by');
    $order_field = $this->input->post('order_field');
    $shorting    = $this->input->post('shorting');
    $start       = ($page - 1) * $limit;
    $fields      = "
            struktur_organisasi.struktur_organisasi_id,
            struktur_organisasi.periode_struktur_organisasi_id,
            struktur_organisasi.information,
            struktur_organisasi.nomor_urut,
            struktur_organisasi.nama,
            struktur_organisasi.nip,
            struktur_organisasi.jabatan,
            struktur_organisasi.created_by,
            struktur_organisasi.created_time,
            struktur_organisasi.updated_by,
            struktur_organisasi.updated_time,
            struktur_organisasi.deleted_by,
            struktur_organisasi.deleted_time,
            struktur_organisasi.status,
            
            periode_struktur_organisasi.periode_struktur_organisasi_name,
            
            (
            select attachment.file_name from attachment
            where attachment.id_tabel=struktur_organisasi.struktur_organisasi_id
            and attachment.table_name='" . $table . "'
            order by attachment.id_attachment DESC
            limit 1
            ) as file
            ";
    $where       = array(
      '' . $table . '.status' => $status
    );
    $this->db->select("$fields");
    if ($keyword == '') {
      $this->db->where("
            " . $table . ".status = " . $status . "
            ");
    } else {
      $this->db->where("
            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'
            AND
            " . $table . ".status = " . $status . "
            ");
    }
    $this->db->join('periode_struktur_organisasi', 'periode_struktur_organisasi.periode_struktur_organisasi_id=struktur_organisasi.periode_struktur_organisasi_id');
    $this->db->order_by($order_by, $shorting);
    $this->db->limit($limit, $start);
    $result = $this->db->get($table);
    echo json_encode($result->result_array());
  }
  
  public function total()
  {
    $table       = $this->input->post('table');
    $page        = $this->input->post('page');
    $limit       = $this->input->post('limit');
    $keyword     = $this->input->post('keyword');
    $status      = $this->input->post('status');
    $order_by    = $this->input->post('order_by');
    $shorting    = $this->input->post('shorting');
    $order_field = $this->input->post('order_field');
    $start       = ($page - 1) * $limit;
    $fields      = "
            struktur_organisasi.struktur_organisasi_id,
            struktur_organisasi.periode_struktur_organisasi_id,
            struktur_organisasi.information,
            struktur_organisasi.nomor_urut,
            struktur_organisasi.nama,
            struktur_organisasi.nip,
            struktur_organisasi.jabatan,
            struktur_organisasi.created_by,
            struktur_organisasi.created_time,
            struktur_organisasi.updated_by,
            struktur_organisasi.updated_time,
            struktur_organisasi.deleted_by,
            struktur_organisasi.deleted_time,
            struktur_organisasi.status
            ";
    $where       = array(
      '' . $table . '.status' => $status
    );
    $this->db->select("$fields");
    if ($keyword == '') {
      $this->db->where("
            " . $table . ".status = " . $status . "
            ");
    } else {
      $this->db->where("
            " . $table . "." . $table . "_name LIKE '%" . $keyword . "%'
            AND
            " . $table . ".status = " . $status . "
            ");
    }
    $this->db->join('periode_struktur_organisasi', 'periode_struktur_organisasi.periode_struktur_organisasi_id=struktur_organisasi.periode_struktur_organisasi_id');
    $query = $this->db->get($table);
    $a     = $query->num_rows();
    echo trim(ceil($a / $limit));
  }
  
  public function simpan()
  {
    $this->form_validation->set_rules('temp', 'temp', 'required');
    $this->form_validation->set_rules('periode_struktur_organisasi_id', 'periode_struktur_organisasi_id', 'required');
    $this->form_validation->set_rules('information', 'information', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_input = array(
        'periode_struktur_organisasi_id' => trim($this->input->post('periode_struktur_organisasi_id')),
        'information' => trim($this->input->post('information')),
        'nomor_urut' => trim($this->input->post('nomor_urut')),
        'nama' => trim($this->input->post('nama')),
        'nip' => trim($this->input->post('nip')),
        'jabatan' => trim($this->input->post('jabatan')),
        'temp' => trim($this->input->post('temp')),
        'created_by' => $this->session->userdata('user_id'),
        'created_time' => date('Y-m-d H:i:s'),
        'status' => 1
      );
      $this->db->insert( 'struktur_organisasi', $data_input );
      $id= $this->db->insert_id();
      echo '' . $id . '';
      $where       = array(
        'table_name' => 'struktur_organisasi',
        'temp' => trim($this->input->post('temp'))
      );
      $data_update = array(
        'id_tabel' => $id
      );
      $this->db->where($where);
      $this->db->update('attachment', $data_update);
    }
  }
  
  public function update()
  {
    $this->form_validation->set_rules('struktur_organisasi_id', 'struktur_organisasi_id', 'required');
    $this->form_validation->set_rules('periode_struktur_organisasi_id', 'periode_struktur_organisasi_id', 'required');
    $this->form_validation->set_rules('information', 'information', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'periode_struktur_organisasi_id' => trim($this->input->post('periode_struktur_organisasi_id')),
        'information' => trim($this->input->post('information')),
        'nomor_urut' => trim($this->input->post('nomor_urut')),
        'nama' => trim($this->input->post('nama')),
        'nip' => trim($this->input->post('nip')),
        'jabatan' => trim($this->input->post('jabatan')),
        'updated_by' => $this->session->userdata('user_id'),
        'updated_time' => date('Y-m-d H:i:s')
      );
      $where       = array(
        'struktur_organisasi_id' => trim($this->input->post('struktur_organisasi_id'))
      );
      $this->db->where($where);
      $this->db->update('struktur_organisasi', $data_update);
      echo 1;
    }
  }
  
  public function get_by_id()
  {
    $struktur_organisasi_id = $this->input->post('struktur_organisasi_id');
    $where   = array(
      'struktur_organisasi.struktur_organisasi_id' => $struktur_organisasi_id
    );
    $this->db->select("
      struktur_organisasi.struktur_organisasi_id,
      struktur_organisasi.periode_struktur_organisasi_id,
      struktur_organisasi.information,
			struktur_organisasi.nomor_urut,
			struktur_organisasi.nama,
			struktur_organisasi.nip,
			struktur_organisasi.jabatan,
      struktur_organisasi.created_by,
      struktur_organisasi.created_time,
      struktur_organisasi.updated_by,
      struktur_organisasi.updated_time,
      struktur_organisasi.deleted_by,
      struktur_organisasi.deleted_time,
      struktur_organisasi.status,
      
      periode_struktur_organisasi.periode_struktur_organisasi_name
      ");    	
    $this->db->where($where);
    $this->db->join('periode_struktur_organisasi', 'periode_struktur_organisasi.periode_struktur_organisasi_id=struktur_organisasi.periode_struktur_organisasi_id');
		$result = $this->db->get('struktur_organisasi');
    echo json_encode($result->result_array());
  }
  
  public function delete_by_id()
  {
    $this->form_validation->set_rules('struktur_organisasi_id', 'struktur_organisasi_id', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 99
      );
      $where       = array(
        'struktur_organisasi_id' => trim($this->input->post('struktur_organisasi_id'))
      );
      $table_name  = 'struktur_organisasi';
      $this->db->where($where);
      $this->db->update('struktur_organisasi', $data_update);
      echo 1;
    }
  }
  
  public function restore_by_id()
  {
    $this->form_validation->set_rules('struktur_organisasi_id', 'struktur_organisasi_id', 'required');
    if ($this->form_validation->run() == FALSE) {
      echo 0;
    } else {
      $data_update = array(
        'status' => 1
      );
      $where       = array(
        'struktur_organisasi_id' => trim($this->input->post('struktur_organisasi_id'))
      );
      $table_name  = 'struktur_organisasi';
      $this->db->where($where);
      $this->db->update($table_name, $data_update);
      echo 1;
    }
  }
  
  
  
}