<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pemohon_info_publik extends CI_Controller
 {
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->model('Pemohon_info_publik_model');
   }
  public function check_login()
   {
    $table_name = 'users';
    $where      = array(
      'users.id_users' => $this->session->userdata('id_users')
    );
    $a          = $this->Crud_model->semua_data($where, $table_name);
    if ($a == 0)
     {
        return redirect(''.base_url().'login');
     }
   }
  public function index()
   {
    // $this->check_login();
    $where             = array(
      'pemohon_info_publik.status !=' => 99
    );
    $a                 = $this->Pemohon_info_publik_model->json_semua_pemohon_info_publik($where);
    $data['per_page']  = 20;
    $data['total']     = ceil($a / 20);
    $data['main_view'] = 'pemohon_info_publik/home';
    $this->load->view('back_bone', $data);
   }
  private function menus($parent=0, $hasil, $start)
  {
		$w = $this->db->query("SELECT * from pages where id_parent='".$parent."' and status!='99'");
		if(($w->num_rows())>0)
		{
			$hasil .= '<ul>';
		}
		foreach($w->result() as $h)
		{
      $start = $start + 1;
      $nama_pages=$h->nama_pages;
      $id_pages=$h->id_pages;
      $id_parent=$h->id_parent;
        $hasil .= '
            <li id_pages="'.$id_pages.'">
            <a href="'.base_url().'pages/baca?'.$nama_pages.'&q='.$id_pages.'">
                  '.$nama_pages.'
                </a>
        ';
			$hasil = $this->menus($id_pages, $hasil, $start);
			$hasil .= "</li>";
		}
		if(($w->num_rows())>0)
		{
			$hasil .= "</ul>";
		}
		return $hasil;
  }
  public function json_all_pemohon_info_publik()
  {
    $parent=0;
    $where      = array(
      'pemohon_info_publik.status !=' => 99
    );
    $a          = $this->Pemohon_info_publik_model->json_semua_pemohon_info_publik($where);
    $halaman    = $this->input->get('halaman');
    $limit      = 20;
    $start      = ($halaman - 1) * $limit;
    $fields     = "
				
				pemohon_info_publik.id_pemohon_info_publik,
				pemohon_info_publik.kode_pemohon_info_publik,
				pemohon_info_publik.nama_pemohon_info_publik,
				pemohon_info_publik.jenis_kelamin,
				pemohon_info_publik.alamat,
				pemohon_info_publik.pekerjaan,
				pemohon_info_publik.nomor_telpon,
				pemohon_info_publik.email,
				pemohon_info_publik.rincian_informasi_yang_dibutuhkan,
				pemohon_info_publik.tujuan_penggunaan_informasi,
				pemohon_info_publik.cara_memperoleh_informasi,
				pemohon_info_publik.cara_mendapatkan_salinan_informasi,
				pemohon_info_publik.tanggal_pengambilan_informasi,
				pemohon_info_publik.inserted_by,
				pemohon_info_publik.inserted_time,
				pemohon_info_publik.updated_by,
				pemohon_info_publik.updated_time,
				pemohon_info_publik.deleted_by,
				pemohon_info_publik.deleted_time,
				pemohon_info_publik.temp,
				pemohon_info_publik.keterangan,
				pemohon_info_publik.status,
								
				";
    $where      = array(
      'pemohon_info_publik.status !=' => 99
    );
    $order_by   = 'pemohon_info_publik.id_pemohon_info_publik';
    echo json_encode($this->Pemohon_info_publik_model->json_all_pemohon_info_publik($where, $limit, $start, $fields, $order_by));
   }
   
	function ambil_pemohon_info_publik($parent=0){

		$kondisi=array(
			"id_parent"=>$parent,
			"aktif"=>"Y",
			"terhapus"=>"N"
			);

		//query ke database untuk mencari pemohon_info_publik
		$this->CI->db->order_by("id_pemohon_info_publik","DESC");
		$query=$this->CI->db->get_where("pemohon_info_publik",$kondisi);

		//cek apakah memiliki hasil
		if($query->num_rows()>0){
			$this->pemohon_info_publik.="<ul>";

				//memulai membentuk pemohon_info_publik satu persatu
				foreach ($query->result_array() as $pemohon_info_publik) {
					$this->pemohon_info_publik.="<li>";
					$this->pemohon_info_publik.="<a href='".pemohon_info_publik_url($pemohon_info_publik['id_pemohon_info_publik'],$pemohon_info_publik['slug_pemohon_info_publik'])."'>$pemohon_info_publik[nama_pemohon_info_publik]</a>";

					// panggil fungsi secara recursive untuk mencari subpemohon_info_publik
					$this->ambil_pemohon_info_publik($pemohon_info_publik['id_pemohon_info_publik']);
					$this->pemohon_info_publik.="</li>";
				}

			$this->pemohon_info_publik.="</ul>";
		} else {
			return;
		}
		}
  public function simpan_pemohon_info_publik()
   {
		//$this->form_validation->set_rules('id_pemohon_info_publik', 'id_pemohon_info_publik', 'required');
		$id_pemohon_info_publik         = '';
		$kode_pemohon_info_publik         = trim($this->input->post('kode_pemohon_info_publik'));
		$this->form_validation->set_rules('nama_pemohon_info_publik', 'nama_pemohon_info_publik', 'required');
		$nama_pemohon_info_publik         = trim($this->input->post('nama_pemohon_info_publik'));
		$this->form_validation->set_rules('jenis_kelamin', 'jenis_kelamin', 'required');
		$jenis_kelamin         = trim($this->input->post('jenis_kelamin'));
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$alamat         = trim($this->input->post('alamat'));
		$this->form_validation->set_rules('pekerjaan', 'pekerjaan', 'required');
		$pekerjaan         = trim($this->input->post('pekerjaan'));
		$this->form_validation->set_rules('nomor_telpon', 'nomor_telpon', 'required');
		$nomor_telpon         = trim($this->input->post('nomor_telpon'));
		$this->form_validation->set_rules('email', 'email', 'required');
		$email         = trim($this->input->post('email'));
		$this->form_validation->set_rules('rincian_informasi_yang_dibutuhkan', 'rincian_informasi_yang_dibutuhkan', 'required');
		$rincian_informasi_yang_dibutuhkan         = trim($this->input->post('rincian_informasi_yang_dibutuhkan'));
		$this->form_validation->set_rules('tujuan_penggunaan_informasi', 'tujuan_penggunaan_informasi', 'required');
		$tujuan_penggunaan_informasi         = trim($this->input->post('tujuan_penggunaan_informasi'));
		$this->form_validation->set_rules('tanggal_pengambilan_informasi', 'tanggal_pengambilan_informasi', 'required');
		$tanggal_pengambilan_informasi         = trim($this->input->post('tanggal_pengambilan_informasi'));
		$this->form_validation->set_rules('tanggal_pengambilan_informasi', 'tanggal_pengambilan_informasi', 'required');
		$tanggal_pengambilan_informasi         = trim($this->input->post('tanggal_pengambilan_informasi'));
		$this->form_validation->set_rules('cara_memperoleh_informasi', 'cara_memperoleh_informasi', 'required');
		$cara_memperoleh_informasi         = trim($this->input->post('cara_memperoleh_informasi'));
		$this->form_validation->set_rules('cara_mendapatkan_salinan_informasi', 'cara_mendapatkan_salinan_informasi', 'required');
		$cara_mendapatkan_salinan_informasi         = trim($this->input->post('cara_mendapatkan_salinan_informasi'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = '';
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = '';
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = '';
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = '';
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = '';
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = trim($this->input->post('temp'));
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '1';
				if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
			
				'kode_pemohon_info_publik' => $kode_pemohon_info_publik,
				'nama_pemohon_info_publik' => $nama_pemohon_info_publik,
				'jenis_kelamin' => $jenis_kelamin,
				'alamat' => $alamat,
				'pekerjaan' => $pekerjaan,
				'nomor_telpon' => $nomor_telpon,
				'email' => $email,
				'rincian_informasi_yang_dibutuhkan' => $rincian_informasi_yang_dibutuhkan,
				'tujuan_penggunaan_informasi' => $tujuan_penggunaan_informasi,
				'cara_memperoleh_informasi' => $cara_memperoleh_informasi,
				'cara_mendapatkan_salinan_informasi' => $cara_mendapatkan_salinan_informasi,
				'tanggal_pengambilan_informasi' => $tanggal_pengambilan_informasi,
				'inserted_by' => $inserted_by,
				'inserted_time' => $inserted_time,
				'temp' => $temp,
				'keterangan' => $keterangan,
				'status' => $status,
								
				);
      $table_name = 'pemohon_info_publik';
      $id         = $this->Pemohon_info_publik_model->simpan_pemohon_info_publik($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'pemohon_info_publik',
        'temp' => $temp
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
  public function update_data_pemohon_info_publik()
   {
    $this->form_validation->set_rules('id_pemohon_info_publik', 'id_pemohon_info_publik', 'required');
		$id_pemohon_info_publik         = trim($this->input->post('id_pemohon_info_publik'));
		$this->form_validation->set_rules('nama_pemohon_info_publik', 'nama_pemohon_info_publik', 'required');
		$nama_pemohon_info_publik         = trim($this->input->post('nama_pemohon_info_publik'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = '';
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '';
				if ($this->form_validation->run() == FALSE)
     {
      echo '[]';
     }
    else
     {
      $data_update = array(
			
				'kode_pemohon_info_publik' => $kode_pemohon_info_publik,
				'nama_pemohon_info_publik' => $nama_pemohon_info_publik,
				'updated_by' => $updated_by,
				'updated_time' => $updated_time,
				'keterangan' => $keterangan,
								
				);
      $table_name  = 'pemohon_info_publik';
      $where       = array(
        'pemohon_info_publik.id_pemohon_info_publik' => $id_pemohon_info_publik      
			);
      $this->Pemohon_info_publik_model->update_data_pemohon_info_publik($data_update, $where, $table_name);
      echo '[{"save":"ok"}]';
     }
   }
  public function il_simpan_pemohon_info_publik()
   {
		$this->form_validation->set_rules('id_pemohon_info_publik', 'id_pemohon_info_publik', 'required');
		$id_pemohon_info_publik         = trim($this->input->post('id_pemohon_info_publik'));
		$this->form_validation->set_rules('kode_pemohon_info_publik', 'kode_pemohon_info_publik', 'required');
		$kode_pemohon_info_publik         = trim($this->input->post('kode_pemohon_info_publik'));
		$this->form_validation->set_rules('nama_pemohon_info_publik', 'nama_pemohon_info_publik', 'required');
		$nama_pemohon_info_publik         = trim($this->input->post('nama_pemohon_info_publik'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = '';
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '';
				
		if ($this->form_validation->run() == FALSE)
     {
      echo 'Error';
     }
    else
     {
      $data_update = array(
        
				'kode_pemohon_info_publik' => $kode_pemohon_info_publik,
				'nama_pemohon_info_publik' => $nama_pemohon_info_publik,
				'updated_by' => $updated_by,
				'updated_time' => $updated_time,
				'keterangan' => $keterangan,
								
				);
      $table_name  = 'pemohon_info_publik';
      $where       = array(
        'pemohon_info_publik.id_pemohon_info_publik' => $id_pemohon_info_publik      );
      $this->Pemohon_info_publik_model->update_data_pemohon_info_publik($data_update, $where, $table_name);
      echo 'Success';
     }
   }
  public function ag_simpan_pemohon_info_publik()
   {
		$this->form_validation->set_rules('id_pemohon_info_publik', 'id_pemohon_info_publik', 'required');
		$id_pemohon_info_publik         = trim($this->input->post('id_pemohon_info_publik'));
		$this->form_validation->set_rules('kode_pemohon_info_publik', 'kode_pemohon_info_publik', 'required');
		$kode_pemohon_info_publik         = trim($this->input->post('kode_pemohon_info_publik'));
		$this->form_validation->set_rules('nama_pemohon_info_publik', 'nama_pemohon_info_publik', 'required');
		$nama_pemohon_info_publik         = trim($this->input->post('nama_pemohon_info_publik'));
		//$this->form_validation->set_rules('inserted_by', 'inserted_by', 'required');
		$inserted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('inserted_time', 'inserted_time', 'required');
		$inserted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('updated_by', 'updated_by', 'required');
		$updated_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('updated_time', 'updated_time', 'required');
		$updated_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('deleted_by', 'deleted_by', 'required');
		$deleted_by         = $this->session->userdata('id_users');
		//$this->form_validation->set_rules('deleted_time', 'deleted_time', 'required');
		$deleted_time         = date('Y-m-d H:i:s');
		//$this->form_validation->set_rules('temp', 'temp', 'required');
		$temp         = '';
		$this->form_validation->set_rules('keterangan', 'keterangan', 'required');
		$keterangan         = trim($this->input->post('keterangan'));
		//$this->form_validation->set_rules('status', 'status', 'required');
		$status         = '';
				if ($this->form_validation->run() == FALSE)
     {
      echo '[]';
     }
    else
     {
      $data_update = array(
        
				'kode_pemohon_info_publik' => $kode_pemohon_info_publik,
				'nama_pemohon_info_publik' => $nama_pemohon_info_publik,
				'updated_by' => $updated_by,
				'updated_time' => $updated_time,
				'keterangan' => $keterangan,
								
				 );
      $table_name  = 'pemohon_info_publik';
      $where       = array(
        'pemohon_info_publik.id_pemohon_info_publik' => $id_pemohon_info_publik      
				);
      $this->Pemohon_info_publik_model->update_data_pemohon_info_publik($data_update, $where, $table_name);
      echo '[{"pemohon_info_publik":"ok", "jumlah":"ok"}]';
     }
   }
  public function cetak()
   {
    $where             = array(
      'pemohon_info_publik.status !=' => 99
    );
    $data['data_pemohon_info_publik'] = $this->Pemohon_info_publik_model->data_pemohon_info_publik($where);
    $this->load->view('pemohon_info_publik/cetak', $data);
   }
  public function pemohon_info_publik_get_by_id()
   {
    $table_name = 'pemohon_info_publik';
    $id         = $_GET['id'];
    $fields     = "
				pemohon_info_publik.id_pemohon_info_publik,
				pemohon_info_publik.kode_pemohon_info_publik,
				pemohon_info_publik.nama_pemohon_info_publik,
				pemohon_info_publik.inserted_by,
				pemohon_info_publik.inserted_time,
				pemohon_info_publik.updated_by,
				pemohon_info_publik.updated_time,
				pemohon_info_publik.deleted_by,
				pemohon_info_publik.deleted_time,
				pemohon_info_publik.temp,
				pemohon_info_publik.keterangan,
				pemohon_info_publik.status,
								";
    $where      = array(
      'pemohon_info_publik.id_pemohon_info_publik' => $id
    );
    $order_by   = 'pemohon_info_publik.nama_pemohon_info_publik';
    echo json_encode($this->Pemohon_info_publik_model->get_by_id($table_name, $where, $fields, $order_by));
   }
  public function hapus_pemohon_info_publik()
   {
    $table_name = 'pemohon_info_publik';
    $id         = $_GET['id'];
    $where      = array(
      'pemohon_info_publik.id_pemohon_info_publik' => $id
    );
    $t          = $this->Pemohon_info_publik_model->json_semua_pemohon_info_publik($where, $table_name);
    if ($t == 0)
     {
      echo ' {"errors":"Yes"} ';
     }
    else
     {
      $data_update = array(
        'pemohon_info_publik.status' => 99
      );
      $where       = array(
        'pemohon_info_publik.id_pemohon_info_publik' => $id
      );
      $this->Pemohon_info_publik_model->update_data_pemohon_info_publik($data_update, $where, $table_name);
      echo ' {"errors":"No"} ';
     }
   }
  public function cari_pemohon_info_publik()
   {
    $table_name = 'pemohon_info_publik';
    $key_word   = $_GET['key_word'];
    $where      = array(
      'pemohon_info_publik.status !=' => 99
    );
    $field      = 'pemohon_info_publik.nama_pemohon_info_publik';
    $a          = $this->Pemohon_info_publik_model->count_all_search_pemohon_info_publik($where, $key_word, $table_name, $field);
    if (empty($key_word))
     {
      echo '[]';
      exit;
     }
    else if ($a == 0)
     {
      echo '[{"jumlah":"0"}]';
      exit;
     }
    $fields     = "
				pemohon_info_publik.id_pemohon_info_publik,
				pemohon_info_publik.kode_pemohon_info_publik,
				pemohon_info_publik.nama_pemohon_info_publik,
				pemohon_info_publik.inserted_by,
				pemohon_info_publik.inserted_time,
				pemohon_info_publik.updated_by,
				pemohon_info_publik.updated_time,
				pemohon_info_publik.deleted_by,
				pemohon_info_publik.deleted_time,
				pemohon_info_publik.temp,
				pemohon_info_publik.keterangan,
				pemohon_info_publik.status,
								";
    $where      = array(
      'pemohon_info_publik.status !=' => 99
    );
    $field_like = 'pemohon_info_publik.nama_pemohon_info_publik';
    $limit      = $_GET['limit'];
    $start      = (($_GET['start']) * $limit);
    $order_by   = ''.$field_like.'';
    echo json_encode($this->Pemohon_info_publik_model->search($table_name, $fields, $where, $limit, $start, $field_like, $key_word, $order_by));
   }
  public function count_all_cari_pemohon_info_publik()
   {
    $table_name = 'pemohon_info_publik';
    $key_word   = $_GET['key_word'];
    if (empty($key_word))
     {
      echo '[]';
      exit;
     }
    $where = array(
      'pemohon_info_publik.status !=' => 99
    );
    $field = 'pemohon_info_publik.nama_pemohon_info_publik';
    $a     = $this->Pemohon_info_publik_model->count_all_search_pemohon_info_publik($where, $key_word, $table_name, $field);
    $limit = $_GET['limit'];
    echo '[{"pemohon_info_publik":"' . ceil($a / $limit) .'", "jumlah":"'.$a .'"}]';
   }
  public function download_xls()
   {
    $this->load->library('excel');
    $objPHPExcel = new PHPExcel();
    $objPHPExcel
			->getProperties()
			->setCreator("Budi Utomo")
			->setLastModifiedBy("Budi Utomo")
			->setTitle("Laporan I")
			->setSubject("Office 2007 XLSX Document")
			->setDescription("Laporan untuk menampilkan kunjungan pasien")
			->setKeywords("Laporan Halaman")
			->setCategory("Bentuk XLS");
    $objPHPExcel
			->setActiveSheetIndex(0)
			->setCellValue('A1', 'Cetak')
			->mergeCells('A1:G1')
			->setCellValue('A2', 'Laporan Data Halaman')
			->mergeCells('A2:G2')
			->setCellValue('A3', 'Tanggal Download : ' . date('d/m/Y') .' ')
			->mergeCells('A3:G3')
			->setCellValue('A6', 'No')
			->setCellValue('B6', 'Nama Pemohon_info_publik')
			->setCellValue('C6', 'Kode Pemohon_info_publik')
			->setCellValue('D6', 'Keterangan');
    $table    = 'pemohon_info_publik';
    $fields     = "
				pemohon_info_publik.id_pemohon_info_publik,
				pemohon_info_publik.kode_pemohon_info_publik,
				pemohon_info_publik.nama_pemohon_info_publik,
				pemohon_info_publik.inserted_by,
				pemohon_info_publik.inserted_time,
				pemohon_info_publik.updated_by,
				pemohon_info_publik.updated_time,
				pemohon_info_publik.deleted_by,
				pemohon_info_publik.deleted_time,
				pemohon_info_publik.temp,
				pemohon_info_publik.keterangan,
				pemohon_info_publik.status,
								";
    $where    = array(
      'pemohon_info_publik.status !=' => 99
    );
    $order_by = 'pemohon_info_publik.nama_pemohon_info_publik';
    $b        = $this->Cetak_model->cetak_single_table($table, $fields, $where, $order_by);
    $i        = 7;
    $no       = 0;
    foreach ($b->result() as $b1)
     {
      $no = $no + 1;
      $objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$i, ''.$no.'')
				->setCellValue('B'.$i, ''.$b1->nama_pemohon_info_publik.'')
				->setCellValue('C'.$i, '\''.$b1->kode_pemohon_info_publik.'')
				->setCellValue('D'.$i, ''.$b1->keterangan.'')
			;
				
      $objPHPExcel
				->getActiveSheet()
				->getStyle('A'.$i.':D'.$i.'')
				->getBorders()
				->getAllBorders()
				->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
      $i++;
     }
    $objPHPExcel->getActiveSheet()->setTitle('Probable Clients');
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
    $objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
    $objPHPExcel->getActiveSheet()->getStyle('A6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('B6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('C6')->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->getStyle('D6')->getFont()->setBold(true);
		
    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getFill()->getStartColor()->setARGB('cccccc');
    $objPHPExcel->getActiveSheet()->getStyle('A6:D6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
    
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
    $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
    $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		
    $objPHPExcel->setActiveSheetIndex(0);
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="Halaman_' . date('ymdhis') .'.xls"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') .' GMT');
    header('Cache-Control: cache, must-revalidate');
    header('Pragma: public');
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    $objWriter->save('php://output');
   }
	 
  public function cetak_pdf()
   {
    $table             = 'pemohon_info_publik';
    $fields     = "
				pemohon_info_publik.id_pemohon_info_publik,
				pemohon_info_publik.kode_pemohon_info_publik,
				pemohon_info_publik.nama_pemohon_info_publik,
				pemohon_info_publik.inserted_by,
				pemohon_info_publik.inserted_time,
				pemohon_info_publik.updated_by,
				pemohon_info_publik.updated_time,
				pemohon_info_publik.deleted_by,
				pemohon_info_publik.deleted_time,
				pemohon_info_publik.temp,
				pemohon_info_publik.keterangan,
				pemohon_info_publik.status,
								";
    $where             = array(
      'pemohon_info_publik.status !=' => 99
    );
    $order_by          = 'pemohon_info_publik.nama_pemohon_info_publik';
    $data['data_pemohon_info_publik'] = $this->Cetak_model->cetak_single_table($table, $fields, $where, $order_by);
    $this->load->view('pemohon_info_publik/pdf', $data);
    $html = $this->output->get_output();
    $this->load->library('dompdf_gen');
    $this->dompdf->load_html($html);
    $this->dompdf->set_paper('a4', 'portrait');
    $this->dompdf->render();
    $this->dompdf->stream("cetak_pemohon_info_publik_" . date('d_m_y') . ".pdf");
   } 
	public function search_pemohon_info_publik()
		{
		$key_word = trim($this->input->post('key_word'));
    $halaman    = $this->input->post('halaman');
    $limit      = 20;
    $start      = ($halaman - 1) * $limit;
    $fields     = "
				pemohon_info_publik.id_pemohon_info_publik,
				pemohon_info_publik.kode_pemohon_info_publik,
				pemohon_info_publik.nama_pemohon_info_publik,
				pemohon_info_publik.inserted_by,
				pemohon_info_publik.inserted_time,
				pemohon_info_publik.updated_by,
				pemohon_info_publik.updated_time,
				pemohon_info_publik.deleted_by,
				pemohon_info_publik.deleted_time,
				pemohon_info_publik.temp,
				pemohon_info_publik.keterangan,
				pemohon_info_publik.status,
								";
    $where      = array(
      'pemohon_info_publik.status !=' => 99
    );
    $order_by   = 'pemohon_info_publik.nama_pemohon_info_publik';
    echo json_encode($this->Pemohon_info_publik_model->search_pemohon_info_publik($fields, $where, $limit, $start, $key_word, $order_by));
		}
	public function count_all_search_pemohon_info_publik()
		{
			$table_name = 'pemohon_info_publik';
			$key_word = $_GET['key_word'];
			$field = 'nama_pemohon_info_publik';
			$where = array(
					''.$table_name.'.status' => 1
				);
			$a = $this->Pemohon_info_publik_model->count_all_search_pemohon_info_publik($where, $key_word, $table_name, $field);
			$limit = 20;
			echo ceil($a/$limit); 
		}
	public function auto_suggest()
		{
		$q = $_GET['q'];
		if(empty($_GET['q']))
		{
				exit;
		}
		if( strlen($q) > 2)
			{
			$where      = array(
				'pemohon_info_publik.status !=' => 99
			);
			$fields     = "
				pemohon_info_publik.id_pemohon_info_publik,
				pemohon_info_publik.kode_pemohon_info_publik,
				pemohon_info_publik.nama_pemohon_info_publik,
				pemohon_info_publik.inserted_by,
				pemohon_info_publik.inserted_time,
				pemohon_info_publik.updated_by,
				pemohon_info_publik.updated_time,
				pemohon_info_publik.deleted_by,
				pemohon_info_publik.deleted_time,
				pemohon_info_publik.temp,
				pemohon_info_publik.keterangan,
				pemohon_info_publik.status,
								pemohon_info_publik.nama_pemohon_info_publik as value
				";
			echo json_encode( $this->Pemohon_info_publik_model->auto_suggest($q, $where, $fields) );
			}
		}
 }