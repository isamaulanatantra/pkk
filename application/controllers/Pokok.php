<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Pokok extends CI_Controller {

	function __construct()
		{
		parent::__construct();
//    $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->model('Cetak_model');
    $this->load->model('Crud_model');
    $this->load->model('Kebutuhan_pokok_model');
		$this->load->model('login_model');
		}
	public function index()
		{
	    $data['main_view'] = 'kebutuhan_pokok/home';
      $this->load->view('back_bone', $data);
		}
  
  public function marquee_kebutuhan_publik()
    {
          echo'
          <div class="col-md-7">
            <span>
              Harga Kebutuhan Pokok Pasar Induk Wonosobo '.date('Y-m-d').'
            </span>
          </div>
          <div class="col-md-5 text-right">
            <span>
            <marquee direction="left" scrollamount="" align="center">
            ';
            $where1    = array(
                'komoditi.status !=' => 99, 'komoditi.parent' => 0,
            );
            $this->db->where($where1);
            $this->db->limit(9);
            $this->db->order_by('komoditi.id_komoditi');
            $b1 = $this->db->get('komoditi');
            $no=0;
            foreach ($b1->result() as $r1)
              {
              $no=$no+1;
                $where2    = array(
                  'komoditi.status !=' => 99,
                  'komoditi.parent' => $r1->id_komoditi
                );
                $this->db->where($where2);
                $this->db->order_by('komoditi.id_komoditi');
                $b2 = $this->db->get('komoditi');
                  foreach ($b2->result() as $r2)
                  {
                    $where_harga    = array(
                      'harga_kebutuhan_pokok_publik.status !=' => 99,
                      'harga_kebutuhan_pokok_publik.id_komoditi' => $r2->id_komoditi,
                      'harga_kebutuhan_pokok_publik.id_pasar' => 1,
                    );
                    $this->db->select("
                    *
                    ");
                    $this->db->where($where_harga);
                    $this->db->order_by('harga_kebutuhan_pokok_publik.id_komoditi');
                    $b_harga = $this->db->get('harga_kebutuhan_pokok_publik');
                    foreach ($b_harga->result() as $r_harga)
                    {
                      $harga_1 = $r_harga->harga;
                      $id_satuan = $r_harga->id_satuan;
                    }
                    echo '
                    '.$r2->judul_komoditi.' Rp. ';
                    if(!empty( $harga_1 )){ echo $harga_1; }
                    echo ' / ';
                    if(!empty( $id_satuan )){ echo $id_satuan; }
                    echo '   ';
                  }
             }
             echo'
              </marquee>
            </span>
          </div>';
    }
  public function kebutuhan_publik()
    {
      $id_pasar         = trim($this->input->post('id_pasar'));
      $tanggal         = trim($this->input->post('tanggal'));
      if($id_pasar==0){
          echo'
            <div class="panel panel-warning" style="margin-top:-11px;">
              <div class="panel-heading">
                <p class="panel-title">Pasar Induk Wonosobo | '.date('Y-m-d').'</p>
              </div>
            </div>
            <div class="panel panel-info table-wrapper-2" style="margin-top:-21px;">
            <!--<marquee direction="up" behavior="slide" onmouseover="this.stop()" onmouseout="this.start()" loop="1">-->
            <table class="table table-bordered table-striped table-responsive table-sm">
              <thead>
                <tr class="table-info">
                  <th>No</th>
                  <th>Bahan Pokok</th>
                  <th>Harga</th>
                  <th>Satuan</th>
                </tr>
              </thead>
              <tbody>
          ';
            $where1    = array(
                'komoditi.status !=' => 99, 'komoditi.parent' => 0,
            );
            $this->db->select("
            *
            ");
            $this->db->where($where1);
            // $this->db->limit(9);
            $this->db->order_by('komoditi.id_komoditi');
            $b1 = $this->db->get('komoditi');
            $no=0;
            foreach ($b1->result() as $r1)
              {
              $no=$no+1;
              echo '
                <tr>
                  <td>'.$no.'</td>
                  <td><b>'.strtoupper($r1->judul_komoditi).'</b></td>
                  <td></td>
                  <td></td>
                </tr>';
                $where2    = array(
                  'komoditi.status !=' => 99,
                  'komoditi.parent' => $r1->id_komoditi
                );
                $this->db->select("
                *
                ");
                $this->db->where($where2);
                $this->db->order_by('komoditi.id_komoditi');
                $b2 = $this->db->get('komoditi');
                  foreach ($b2->result() as $r2)
                  {
                    $id_komoditi = $r2->id_komoditi;
                    echo '
                      <tr>
                        <td></td>
                        <td>'.$r2->judul_komoditi.'</td>
                        <td>
                        ';
                        $where_harga    = array(
                          'harga_kebutuhan_pokok_publik.status !=' => 99,
                          'harga_kebutuhan_pokok_publik.id_komoditi' => $r2->id_komoditi,
                          'harga_kebutuhan_pokok_publik.id_pasar' => 1,
                        );
                        $this->db->select("
                        *
                        ");
                        $this->db->where($where_harga);
                        $this->db->order_by('harga_kebutuhan_pokok_publik.id_komoditi');
                        $b_harga = $this->db->get('harga_kebutuhan_pokok_publik');
                        $jml_row = $this->db->count_all_results();
                        if($b_harga->num_rows() > 0){
                          foreach ($b_harga->result() as $r_harga)
                          {
                            $harga_1 = $r_harga->harga;
                            $id_satuan = $r_harga->id_satuan;
                            echo ''.$harga_1.'';
                          }
                        }else{
                          echo '0';
                          $id_satuan='0';
                        }
                        echo'
                        </td>
                        <td>'.$id_satuan.'</td>
                      </tr>
                      ';
                  }
             }
        
          echo'
              </tbody>
            </table>
          <!--</marquee>-->
          </div>
          ';
          echo '
          <div class="panel panel-success">
            <div class="panel-heading">
              <p class="panel-title"> Sumber data: Pemantauan di ';
              $where_pasar = array(
                  'pasar.status !=' => 99,
                  'pasar.id_pasar' => $id_pasar,
              );
              $this->db->where($where_pasar);
              $this->db->order_by('pasar.id_pasar');
              $b_pasar = $this->db->get('pasar');
              foreach ($b_pasar->result() as $r_pasar)
                {
                  $nama_pasar = $r_pasar->nama_pasar;
                }
                if(!empty( $nama_pasar )){ echo $nama_pasar; }else{echo $nama_pasar=' - ';}
            echo' 
              Kab. Wonosobo Dinas Perdagangan, Koperasi, UKM Kab. Wonosobo </p>
            </div>
          </div>
          ';
      }
      else{
          echo'
          <p class="card-data info-color text-left">Pasar : 
            ';
        $where_pasar = array(
            'pasar.status !=' => 99,
            'pasar.id_pasar' => $id_pasar,
        );
        $this->db->select("
        pasar.id_pasar, 
        pasar.kode_pasar, 
        pasar.nama_pasar
        ");
        $this->db->where($where_pasar);
        $this->db->order_by('pasar.id_pasar');
        $b_pasar = $this->db->get('pasar');
        foreach ($b_pasar->result() as $r_pasar1){echo''.$r_pasar1->nama_pasar.'';}echo' | '.$tanggal.'</p>
                        <div class="table-wrapper-2">
                        <marquee direction="up" scrollamount="3" align="center">
                        <table class="table table-bordered table-striped table-responsive table-sm">
                            <thead>
                              <tr class="table-info">
                                <th>No</th>
                                <th>Nama Barang / Bahan Pokok</th>
                                <th>Satuan</th>
                                <th>Harga</th>
                              </tr>
                            </thead>
                            <tbody>
          ';
            $where1    = array(
                'komoditi.status !=' => 99, 'komoditi.parent' => 0,
            );
            $this->db->select("
            *
            ");
            $this->db->where($where1);
            $this->db->limit(9);
            $this->db->order_by('komoditi.id_komoditi');
            $b1 = $this->db->get('komoditi');
            $no=0;
            foreach ($b1->result() as $r1)
              {
              $no=$no+1;
              echo '
                <tr>
                  <td>'.$no.'</td>
                  <td><b>'.strtoupper($r1->judul_komoditi).'</b></td>
                  <td></td>
                  <td></td>
                </tr>';
                $where2    = array(
                  'komoditi.status !=' => 99,
                  'komoditi.parent' => $r1->id_komoditi
                );
                $this->db->select("
                *
                ");
                $this->db->where($where2);
                $this->db->order_by('komoditi.id_komoditi');
                $b2 = $this->db->get('komoditi');
                  foreach ($b2->result() as $r2)
                  {
                    $id_komoditi = $r2->id_komoditi;
                    echo '
                      <tr>
                        <td></td>
                        <td>'.$r2->judul_komoditi.'</td>
                        ';
                        $where_harga    = array(
                          'harga_kebutuhan_pokok.status !=' => 99,
                          'harga_kebutuhan_pokok.id_komoditi' => $r2->id_komoditi,
                          'harga_kebutuhan_pokok.id_pasar' => $id_pasar,
                          'harga_kebutuhan_pokok.tanggal' => $tanggal,
                        );
                        $this->db->select("*");
                        $this->db->where($where_harga);
                        $this->db->order_by('harga_kebutuhan_pokok.id_komoditi');
                        $b_harga = $this->db->get('harga_kebutuhan_pokok');
                        $jml_row = $this->db->count_all_results();
                        // if($b_harga->num_rows() > 0){ }else{ }
                        foreach ($b_harga->result() as $r_harga)
                        {
                          $harga = $r_harga->harga;
                          $id_satuan = $r_harga->id_satuan;
                          echo '
                          <td>';
                          if(!empty( $id_satuan )){ echo $id_satuan; }else{echo $id_satuan='0';}
                          echo '
                          </td>
                          <td>';
                          if(!empty( $harga )){ echo $harga; }else{echo $harga='0';}
                          echo '
                          </td>
                          ';
                        }
                        echo'
                      </tr>
                      ';
                  }
             }
        
          echo'
                            </tbody>
                          </table>
                        </marquee>
                        </div>
          ';
          echo '
          <div class="panel panel-success">
            <div class="panel-heading">
              <p class="panel-title"> Sumber data: Pemantauan di ';
              $where_pasar = array(
                  'pasar.status !=' => 99,
                  'pasar.id_pasar' => $id_pasar,
              );
              $this->db->where($where_pasar);
              $this->db->order_by('pasar.id_pasar');
              $b_pasar = $this->db->get('pasar');
              foreach ($b_pasar->result() as $r_pasar)
                {
                  $nama_pasar = $r_pasar->nama_pasar;
                }
                if(!empty( $nama_pasar )){ echo $nama_pasar; }else{echo $nama_pasar=' - ';}
            echo' 
              Kab. Wonosobo Dinas Perdagangan, Koperasi, UKM Kab. Wonosobo </p>
            </div>
          </div>
          ';
      }
    }
    
  public function mygrafik_bar1()
    {
      $id_pasar= 1;
      $tanggal_home= date('Y-m-d');
      //$tanggal_home= '2017-10-02';
      echo '
            
<canvas id="barChart"></canvas>

<script type="text/javascript">

//bar
var ctxB = document.getElementById("barChart").getContext(\'2d\');
var myBarChart = new Chart(ctxB, {
    type: \'bar\',
    data: {
        labels: [
        ';
            $where_komoditi1    = array(
              'komoditi.status !=' => 99,
              'komoditi.parent !=' => 0,
            );
            $this->db->select(" * ");
            $this->db->where($where_komoditi1);
            $this->db->limit(9);
            $this->db->order_by('komoditi.id_komoditi');
            $b_komoditi1 = $this->db->get('komoditi');
            $jumlah_komoditi1 = $b_komoditi1->num_rows();
              $no=0;
              foreach ($b_komoditi1->result() as $r_komoditi1)
              {
                $no=$no+1;
                $id_komoditi_by1 = $r_komoditi1->id_komoditi;
                $nama_komoditi_by1 = $r_komoditi1->nama_komoditi;
                echo'"'.$nama_komoditi_by1.'",';
              }
      echo'],';
      echo'
        datasets: [{
            label: \'# Grafik Harga Kebutuhan Pokok\',
            data: [';
              foreach ($b_komoditi1->result() as $r_komoditi1)
              {
                $no=$no+1;
                $id_komoditi_by1 = $r_komoditi1->id_komoditi;
                $nama_komoditi_by1 = $r_komoditi1->nama_komoditi;
                $where_komoditi2    = array(
                  'harga_kebutuhan_pokok.status !=' => 99,
                  'harga_kebutuhan_pokok.id_komoditi' => $id_komoditi_by1,
                  'harga_kebutuhan_pokok.id_pasar' => $id_pasar,
                  'harga_kebutuhan_pokok.tanggal' => $tanggal_home,
                );
                $this->db->select(" 
                harga_kebutuhan_pokok.id_harga_kebutuhan_pokok, 
                harga_kebutuhan_pokok.harga, 
                ");
                $this->db->where($where_komoditi2);
                $this->db->order_by('harga_kebutuhan_pokok.id_harga_kebutuhan_pokok');
                $b_komoditi2 = $this->db->get('harga_kebutuhan_pokok');
                $jumlah_komoditi2 = $b_komoditi2->num_rows();
                if($b_komoditi2->num_rows() > 0){
                  $no=0;
                  foreach ($b_komoditi2->result() as $r_komoditi2)
                  {
                    $no=$no+1;
                    $id_harga_kebutuhan_pokok_by2 = $r_komoditi2->id_harga_kebutuhan_pokok;
                    $harga = $r_komoditi2->harga;
                    echo''.$harga.',';
                  }
                  }else{echo'0,';}
              }
            echo'],
            backgroundColor: [
                \'rgba(255, 99, 132, 0.2)\',
                \'rgba(54, 162, 235, 0.2)\',
                \'rgba(255, 206, 86, 0.2)\',
                \'rgba(75, 192, 192, 0.2)\',
                \'rgba(153, 102, 255, 0.2)\',
                \'rgba(255, 159, 64, 0.2)\'
            ],
            borderColor: [
                \'rgba(255,99,132,1)\',
                \'rgba(54, 162, 235, 1)\',
                \'rgba(255, 206, 86, 1)\',
                \'rgba(75, 192, 192, 1)\',
                \'rgba(153, 102, 255, 1)\',
                \'rgba(255, 159, 64, 1)\'
            ],
            borderWidth: 1
        }]
    },
    optionss: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

      ';
    }
    
  public function mygrafik_bar()
    {
      $id_pasar= trim($this->input->post('id_pasar_home'));
      $tanggal= trim($this->input->post('tanggal_home'));
      //$tanggal= '2017-10-01';
      echo '
            
<canvas id="barChart"></canvas>

<script type="text/javascript">

//bar
var ctxB = document.getElementById("barChart").getContext(\'2d\');
var myBarChart = new Chart(ctxB, {
    type: \'bar\',
    data: {
        labels: [
        ';
            $where_komoditi1    = array(
              'komoditi.status !=' => 99,
              'komoditi.id_parent !=' => 0,
            );
            $this->db->select(" 
            komoditi.id_komoditi, 
            komoditi.nama_komoditi, 
            ");
            $this->db->where($where_komoditi1);
            $this->db->limit(9);
            $this->db->order_by('komoditi.id_komoditi');
            $b_komoditi1 = $this->db->get('komoditi');
            $jumlah_komoditi1 = $b_komoditi1->num_rows();
              $no=0;
              foreach ($b_komoditi1->result() as $r_komoditi1)
              {
                $no=$no+1;
                $id_komoditi_by1 = $r_komoditi1->id_komoditi;
                $nama_komoditi_by1 = $r_komoditi1->nama_komoditi;
                echo'"'.$nama_komoditi_by1.'",';
              }
      echo'],
        datasets: [{
            label: \'# Grafik Harga Kebutuhan Pokok\',
            data: [';
              foreach ($b_komoditi1->result() as $r_komoditi1)
              {
                $no=$no+1;
                $id_komoditi_by1 = $r_komoditi1->id_komoditi;
                $nama_komoditi_by1 = $r_komoditi1->nama_komoditi;
                $where_komoditi2    = array(
                  'harga_kebutuhan_pokok.status !=' => 99,
                  'harga_kebutuhan_pokok.id_komoditi' => $id_komoditi_by1,
                  'harga_kebutuhan_pokok.id_pasar' => $id_pasar,
                  'harga_kebutuhan_pokok.tanggal' => $tanggal,
                );
                $this->db->select(" 
                harga_kebutuhan_pokok.id_harga_kebutuhan_pokok, 
                harga_kebutuhan_pokok.harga, 
                harga_kebutuhan_pokok.id_pasar, 
                harga_kebutuhan_pokok.tanggal, 
                ");
                $this->db->where($where_komoditi2);
                $this->db->order_by('harga_kebutuhan_pokok.id_harga_kebutuhan_pokok');
                $b_komoditi2 = $this->db->get('harga_kebutuhan_pokok');
                $jumlah_komoditi2 = $b_komoditi2->num_rows();
                if($b_komoditi2->num_rows() > 0){
                  $no=0;
                  foreach ($b_komoditi2->result() as $r_komoditi2)
                  {
                    $no=$no+1;
                    $id_harga_kebutuhan_pokok_by2 = $r_komoditi2->id_harga_kebutuhan_pokok;
                    $harga = $r_komoditi2->harga;
                    echo''.$harga.',';
                  }
                  }else{echo'0,';}
              }
            echo'],
            backgroundColor: [
                \'rgba(255, 99, 132, 0.2)\',
                \'rgba(54, 162, 235, 0.2)\',
                \'rgba(255, 206, 86, 0.2)\',
                \'rgba(75, 192, 192, 0.2)\',
                \'rgba(153, 102, 255, 0.2)\',
                \'rgba(255, 159, 64, 0.2)\'
            ],
            borderColor: [
                \'rgba(255,99,132,1)\',
                \'rgba(54, 162, 235, 1)\',
                \'rgba(255, 206, 86, 1)\',
                \'rgba(75, 192, 192, 1)\',
                \'rgba(153, 102, 255, 1)\',
                \'rgba(255, 159, 64, 1)\'
            ],
            borderWidth: 1
        }]
    },
    optionss: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
</script>

      ';
    }
}
