<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ganti_password extends CI_Controller {

	function __construct()
		{
		parent::__construct();
		$this->load->model('Ganti_password_model');
		$this->load->library('Bcrypt');
		}
		
	public function index()
	{
		$data['judul'] = 'Data Ganti_password';
		$data['main_view'] = 'ganti_password/home';
		$this->load->view('back_bone', $data);
	}
	public function by_user()
	{
		$data['judul'] = 'Data Ganti_password';
		$data['main_view'] = 'ganti_password/by_user';
		$this->load->view('back_bone', $data);
	}
	
	public function proses_ganti_password()
	{
		
		$this->form_validation->set_rules('ulangi_password_lama', 'ulangi_password_lama', 'required');
		$this->form_validation->set_rules('password_baru', 'password_baru', 'required');
		$this->form_validation->set_rules('password_lama', 'password_lama', 'required'); 
		
		$password_lama = $this->input->post('password_lama');
		$ulangi_password_lama = $this->input->post('ulangi_password_lama');
		$password_baru = $this->input->post('password_baru');
		
		if ($this->form_validation->run() == FALSE)
      {
        echo '[
					{
						"errors":"form_kosong",
						"password_lama":"'.$password_lama.'",
						"ulangi_password_lama":"'.$ulangi_password_lama.'",
						"password_baru":"'.$password_baru.'"
					}
					]';
      }
		else
      {
			$where = array(
            'id_users' => $this->session->userdata('id_users')
						);
			$data_user = $this->Ganti_password_model->get_login($where);
			if ($this->bcrypt->check_password($password_lama, $data_user['password']))
				{
					$new_password = $this->bcrypt->hash_password($this->input->post('password_baru'));
					$change_passsword_edit = array(
									'password'			=> $new_password
								);
					$where_id_users = array(
									'id_users' => $data_user['id_users']
								);
					$this->Ganti_password_model->ganti_password($change_passsword_edit, $where_id_users);
					echo '[
					{
						"errors":"No",
						"password_lama":"'.$password_lama.'",
						"ulangi_password_lama":"'.$ulangi_password_lama.'",
						"password_baru":"'.$password_baru.'"
					}
					]';
				}
			 else
				{
					echo '[
					{
						"errors":"Yes",
						"password_lama":"'.$password_lama.'",
						"ulangi_password_lama":"'.$ulangi_password_lama.'",
						"password_baru":"'.$password_baru.'"
					}
					]';					
				}
      }
	}
	public function proses_ganti_password_by_user()
	{
		
		$this->form_validation->set_rules('password_baru', 'password_baru', 'required');
		
		$id_users = $this->input->post('id_users');
		$password_baru = $this->input->post('password_baru');
		
		if ($this->form_validation->run() == FALSE)
      {
        echo '[
					{
						"errors":"form_kosong",
						"password_baru":"'.$password_baru.'"
					}
					]';
      }
		else
      {
			$where = array(
            'id_users' => $id_users
						);
			$data_user = $this->Ganti_password_model->get_login($where);
					$new_password = $this->bcrypt->hash_password($this->input->post('password_baru'));
					$change_passsword_edit = array(
									'password'			=> $new_password
								);
					$where_id_users = array(
									'id_users' => $data_user['id_users']
								);
					$this->Ganti_password_model->ganti_password($change_passsword_edit, $where_id_users);
					echo '[
					{
						"errors":"No",
						"password_baru":"'.$password_baru.'"
					}
					]';
      }
	}
	
	
}
