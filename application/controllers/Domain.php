<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Domain extends CI_Controller {
    
  function __construct(){
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Domain_model');
    $this->load->model('Dashboard_model');
	}
	
  function desa(){
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'dashboard/domain/desa';
      $this->load->view('tes', $data);
	}
	public function json_all_data_desa(){
    $web=$this->uut->namadomain(base_url());
		$table = 'data_desa';
    $page = $this->input->post('page');
    $limit = $this->input->post('limit');
    $keyword = $this->input->post('keyword');
    $order_by = $this->input->post('orderby');
    $start = ($page - 1) * $limit;
    $fields = "
    *
    ";
    $id_kecamatan = $this->input->post('id_kecamatan');
		$a=0;
		$a = $a + 1;
		if($id_kecamatan==''){
      $where = array(
        'data_desa.status !=' => 99
        );
		}
		else{
      $where = array(
        'data_desa.status !=' => 99,
        'desa.id_kecamatan' => $this->input->post('id_kecamatan'),
        'desa.id_kabupaten' => 1,
        'desa.id_propinsi' => 1
        );
		}
    $orderby   = ''.$order_by.'';
    $query = $this->Domain_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_users="'.$row->id_data_desa.'" id="'.$row->id_data_desa.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				//echo '<td valign="top">'.$this->Crud_model->dateBahasaIndo1($row->created_time).'</td>';
				echo '<td valign="top">'.$row->desa_nama.'</td>';
				echo '<td valign="top"><a href="https://'.$row->desa_website.'" target="_blank">'.$row->desa_website.'</a></td>';
				echo '<td valign="top">'.$row->id_desa.'</td>';
				echo '<td valign="top">'.$row->status.'</td>';
				echo '</tr>';
      }
	}
  public function total_data_desa()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $where0 = array(
      'data_desa.status !=' => 99
      );
    $this->db->where($where0);
    $query0 = $this->db->get('data_desa');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
}