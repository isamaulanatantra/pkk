<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Domains extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Domains_model');

        $this->methods['domains_get']['limit'] = 500; // 500 requests per hour per user/key
    }

    public function domaindesa_get()
    {
        $id = $this->get('id');
        if($id === null){
            $domains = $this->Domains_model->getDomainDesa();
        }else{
            $domains = $this->Domains_model->getDomainDesa($id);
        }

        if($domains){            
            $this->response([
                'status' => true,
                'data' => $domains
            ], REST_Controller::HTTP_OK); 
        }else{            
            $this->response([
                'status' => false,
                'message' => 'data not found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    public function domainkecamatan_get()
    {
        $id = $this->get('id');
        if($id === null){
            $domains = $this->Domains_model->getDomainKecamatan();
        }else{
            $domains = $this->Domains_model->getDomainKecamatan($id);
        }

        if($domains){            
            $this->response([
                'status' => true,
                'data' => $domains
            ], REST_Controller::HTTP_OK); 
        }else{            
            $this->response([
                'status' => false,
                'message' => 'data not found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }
    public function domainskpd_get()
    {
        $id = $this->get('id');
        if($id === null){
            $domains = $this->Domains_model->getDomainSkpd();
        }else{
            $domains = $this->Domains_model->getDomainSkpd($id);
        }

        if($domains){            
            $this->response([
                'status' => true,
                'data' => $domains
            ], REST_Controller::HTTP_OK); 
        }else{            
            $this->response([
                'status' => false,
                'message' => 'data not found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

}
?>