<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Modul extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    //$this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Modul_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'modul/home';
    $this->load->view('back_bone', $data);
   }
   
  function load_table()
	{
    $web=$this->uut->namadomain(base_url());
		$a = 0;
		echo $this->modul(0,$h="", $a);
	}
  private function modul($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from modul
		where parent='".$parent."'
		and status = 1
    order by domain, id_modul
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<tr id_modul="'.$h->id_modul.'" id="'.$h->id_modul.'" >';
			$hasil .= '<td style="padding: 2px;"> '.$nomor.' </td>';
      if( $a > 1 ){
        $hasil .= '<td style="padding: 2px '.($a * 20).'px ;"> <a target="_blank" href="https://'.$h->domain.'/'.$h->alamat_url.'">'.$h->nama_modul.'</a> </td>';
        }
			else{
        $hasil .= '<td style="padding: 2px;"> '.$h->domain.' | '.$h->nama_modul.'</td>';
        }
			$hasil .= '<td style="padding: 2px;"> '.$h->alamat_url.' </td>';
      
			$hasil .= 
			'
			<td style="padding: 2px;"> 
			<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> 
			<a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> 
			</td>
			';
			$hasil .= '</tr>';
			$hasil = $this->modul($h->id_modul,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  public function simpan_modul()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('nama_modul', 'nama_modul', 'required');
		$this->form_validation->set_rules('alamat_url', 'alamat_url', 'required');
		$this->form_validation->set_rules('domain', 'domain', 'required');
		$this->form_validation->set_rules('parent', 'parent', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
			
				'parent' => trim($this->input->post('parent')),
				'domain' => trim($this->input->post('domain')),
				'nama_modul' => trim($this->input->post('nama_modul')),
        'temp' => trim($this->input->post('temp')),
        'alamat_url' => trim($this->input->post('alamat_url')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s'),
        'status' => 1
								
				);
      $table_name = 'modul';
      $id         = $this->Modul_model->simpan_modul($data_input, $table_name);
      echo $id;
			$table_name  = 'attachment';
      $where       = array(
        'table_name' => 'modul',
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
   
  public function update_modul()
   {
    $web=$this->uut->namadomain(base_url());
    $this->form_validation->set_rules('parent', 'parent', 'required');
    $this->form_validation->set_rules('nama_modul', 'nama_modul', 'required');
		$this->form_validation->set_rules('alamat_url', 'alamat_url', 'required');
		$this->form_validation->set_rules('domain', 'domain', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
				'parent' => trim($this->input->post('parent')),
				'domain' => trim($this->input->post('domain')),
				'nama_modul' => trim($this->input->post('nama_modul')),
        'alamat_url' => trim($this->input->post('alamat_url')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'modul';
      $where       = array(
        'modul.id_modul' => trim($this->input->post('id_modul')),
			);
      $this->Modul_model->update_data_modul($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'id_modul' => $this->input->post('id_modul'),
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_modul');
      $result = $this->db->get('modul');
      echo json_encode($result->result_array());
		}
   
   public function total_modul()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $this->db->from('modul');
      $where    = array(
        'status' => 1,
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_modul = $this->input->post('id_modul');
      $where = array(
        'id_modul' => $id_modul,
				'created_by' => $this->session->userdata('id_users'),
        );
      $this->db->from('modul');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'modul';
        $this->Modul_model->update_data_modul($data_update, $where, $table_name);
        echo 1;
        }
		}
    
  function option_domain()
	{
		$a = 0;
		echo $this->isi_option_domain($h="", $a);
	}
  private function isi_option_domain($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT *, data_skpd.skpd_website from skpd, data_skpd where skpd.status = 1 and data_skpd.id_skpd=skpd.id_skpd and data_skpd.status!=99 order by kode_skpd");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->skpd_website.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->nama_skpd.'';
			}
			else{
			$hasil .= ''.$h->nama_skpd.'';
			}
			$hasil .= '</option>';
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
	
  function load_the_option()
	{
		$a = 0;
		echo $this->option_modul(0,$h="", $a);
	}
  
  private function option_modul($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from modul where parent='".$parent."' and status = 1 order by id_modul");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<option value="'.$h->id_modul.'">';
			if( $a > 1 ){
			$hasil .= ''.str_repeat("--", $a).' '.$h->nama_modul.'';
			}
			else{
			$hasil .= ''.$h->nama_modul.'';
			}
			$hasil .= '</option>';
			$hasil = $this->option_modul($h->id_modul,$hasil, $a);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
 }