<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Charts extends CI_Controller
{
  
  function __construct()
  {
    parent::__construct();
    $this->load->library('fpdf');
		$this->load->library('Bcrypt');
		$this->load->library('Crud_model');
  }
  
  public function indexx()
  {
    $data['judul']         = 'Data Agama';
    $this->load->view('charts', $data);
  }
	
	 
	
	function getDatesFromRange($start, $end) {
    $interval = new DateInterval('P1D');
    $realEnd = new DateTime($end);
    $realEnd->add($interval);
    $period = new DatePeriod(
         new DateTime($start),
         $interval,
         $realEnd
    );
    foreach($period as $date) { 
        $array[] = $date->format('Y-m-d'); 
    }
    return $array;
	}
		
	public function index()
	{
		
		$dari_tanggal = trim($this->input->post('dari_tanggal'));
		$sampai_tanggal = trim($this->input->post('sampai_tanggal'));
		$periodik = trim($this->input->post('periodik'));
		$hari = date('Y-m-d');
		$data['hari'] = date('Y-m-d');
		if($periodik == 'periodik'){
			$data['periode'] = 'Tanggal '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' - '.$this->Crud_model->dateBahasaIndo($sampai_tanggal).' ';
			$data['dari_tanggal'] = ''.$dari_tanggal.'';
			$data['sampai_tanggal'] = ''.$sampai_tanggal.'';
			}
		else{
			$data['periode'] = 'Tanggal '.$this->Crud_model->dateBahasaIndo(date('Y-m-d')).'';
			$data['dari_tanggal'] = ''.date('Y-m-d').'';
			$data['sampai_tanggal'] = ''.date('Y-m-d').'';
			}
		
		
		if( empty($dari_tanggal) ){
			$where = array(
			'pus_pendaftaran.status !=' => 99,
			'pus_pendaftaran.tanggal_pendaftaran' => $hari
			);
		}
		else{
		$where = array(
			'pus_pendaftaran.status !=' => 99,
			'pus_pendaftaran.tanggal_pendaftaran >=' => $dari_tanggal,
			'pus_pendaftaran.tanggal_pendaftaran <=' => $sampai_tanggal
			);
		}		
		
		$this->db->select('puskesmas.nama_puskesmas, puskesmas.id_puskesmas, count(pus_pendaftaran.id_pus_pendaftaran) as total');
		$this->db->join('puskesmas', 'puskesmas.id_puskesmas=pus_pendaftaran.id_puskesmas');
		$this->db->where($where);
		$this->db->group_by('pus_pendaftaran.id_puskesmas'); 
		$this->db->order_by('total', 'desc'); 
		$data['test'] = $this->db->get('pus_pendaftaran');
		$data['main_view'] = 'charts/grafik_1a';
		$this->load->view('charts', $data);
	}
		
	public function grafik_1b()
	{
		$periodik = trim($this->input->post('periodik'));
		$id_puskesmas = trim($this->input->post('id_puskesmas'));
		
		$where_pus = array(
			'puskesmas.status !=' => 99,
			'puskesmas.jenis_puskesmas' => 'induk',
			'puskesmas.id_kecamatan !=' => '99'
			);
		$this->db->select('puskesmas.nama_puskesmas, puskesmas.id_puskesmas');
		$this->db->where($where_pus);
		$this->db->order_by('puskesmas.nama_puskesmas');
		$x = $this->db->get('puskesmas');
		$option = '';
		$option .= '<option value="semua">Semua Puskesmas</option>';
		foreach ($x->result() as $x1) 
			{
			if( $x1->id_puskesmas == $id_puskesmas ){
				$option .= '<option value="'.$x1->id_puskesmas.'" selected>'.$x1->nama_puskesmas.'</option>';
				}
			else{
				$option .= '<option value="'.$x1->id_puskesmas.'">'.$x1->nama_puskesmas.'</option>';
				}
			}
		$data['option'] = $option;	
		
		if($periodik == 'periodik'){
			$dari_tanggal = trim($this->input->post('dari_tanggal'));
			$sampai_tanggal = trim($this->input->post('sampai_tanggal'));
			$data['periode'] = 'Dari Tanggal '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' Sampai Tanggal '.$this->Crud_model->dateBahasaIndo($sampai_tanggal).' ';
			$data['dari_tanggal'] = ''.$dari_tanggal.'';
			$data['sampai_tanggal'] = ''.$sampai_tanggal.'';
			}
		else{
			$dari_tanggal = date("Y-m-d",strtotime("-1 month"));
			$sampai_tanggal = date('Y-m-d');
			$data['periode'] = ' Tanggal  : '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' sampai dengan '.$this->Crud_model->dateBahasaIndo(date('Y-m-d')).')';
			$data['dari_tanggal'] = $dari_tanggal;
			$data['sampai_tanggal'] = $sampai_tanggal;
			}
		$a = $this->getDatesFromRange($dari_tanggal, $sampai_tanggal);
		$s = 0;
		$data1 = '';
		$data2 = '';
		$labels = '';
		foreach ($a as $value) {
			$s = $s + 1;
			
			if($periodik == 'periodik'){
					if($id_puskesmas == 'semua'){
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						}
					else{
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						}
				}
			else{
					if($id_puskesmas == 'semua' || empty($id_puskesmas)){
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						}
					else{
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						}
				}
			
			$this->db->where($where1);
			$this->db->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
			$this->db->from('pus_pelayanan');
			$labels .= '"'.$this->Crud_model->dateBahasaIndo($value).'",';
			$data1 .= '"'.$this->db->count_all_results().'",'; 
			
			$this->db->where($where2);
			$this->db->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
			$this->db->from('pus_pelayanan');
			$data2 .= ''.$this->db->count_all_results().','; 
			
		}
		$data['labels'] = $labels;
		$data['data1'] = $data1;
		$data['data2'] = $data2;
		
		$data['main_view'] = 'charts/grafik_1b';
		$this->load->view('charts', $data);
	}
		
	public function grafik_1c()
	{
		$periodik = trim($this->input->post('periodik'));
		$id_puskesmas = trim($this->input->post('id_puskesmas'));
		
		$where_pus = array(
			'puskesmas.status !=' => 99,
			'puskesmas.jenis_puskesmas' => 'induk',
			'puskesmas.id_kecamatan !=' => '99'
			);
		$this->db->select('puskesmas.nama_puskesmas, puskesmas.id_puskesmas');
		$this->db->where($where_pus);
		$this->db->order_by('puskesmas.nama_puskesmas');
		$x = $this->db->get('puskesmas');
		$option = '';
		$option .= '<option value="semua">Semua Puskesmas</option>';
		foreach ($x->result() as $x1) 
			{
			if( $x1->id_puskesmas == $id_puskesmas ){
				$option .= '<option value="'.$x1->id_puskesmas.'" selected>'.$x1->nama_puskesmas.'</option>';
				}
			else{
				$option .= '<option value="'.$x1->id_puskesmas.'">'.$x1->nama_puskesmas.'</option>';
				}
			}
		$data['option'] = $option;	
		
		if($periodik == 'periodik'){
			$dari_tanggal = trim($this->input->post('dari_tanggal'));
			$sampai_tanggal = trim($this->input->post('sampai_tanggal'));
			$data['periode'] = 'Dari Tanggal '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' Sampai Tanggal '.$this->Crud_model->dateBahasaIndo($sampai_tanggal).' ';
			$data['dari_tanggal'] = ''.$dari_tanggal.'';
			$data['sampai_tanggal'] = ''.$sampai_tanggal.'';
			if( $id_puskesmas == 'semua' ){
					$where = array(
					'pus_pelayanan_obat.status' => 2,
					'pus_pendaftaran.tanggal_pendaftaran >=' => $dari_tanggal,
					'pus_pendaftaran.tanggal_pendaftaran <=' => $sampai_tanggal
					);
				}
			else{
					$where = array(
					'pus_pelayanan_obat.status' => 2,
					'pus_pendaftaran.id_puskesmas' => $id_puskesmas,
					'pus_pendaftaran.tanggal_pendaftaran >=' => $dari_tanggal,
					'pus_pendaftaran.tanggal_pendaftaran <=' => $sampai_tanggal
					);
				}
			}
		else{
			$dari_tanggal = date('Y-m-d');
			$sampai_tanggal = date('Y-m-d');
			$data['periode'] = ' Tanggal  : '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' ';
			$data['dari_tanggal'] = $dari_tanggal;
			$data['sampai_tanggal'] = $sampai_tanggal;
			
			$where = array(
			'pus_pelayanan_obat.status' => 2,
			'pus_pendaftaran.tanggal_pendaftaran' =>  date('Y-m-d')
			);
			
			}
		
		
		$this->db->select('obat.nama_obat, SUM(pus_pelayanan_obat.jumlah_obat) as total');
		$this->db->join('obat', 'obat.id_obat=pus_pelayanan_obat.id_obat');
		$this->db->join('pus_pelayanan', 'pus_pelayanan.id_pus_pelayanan=pus_pelayanan_obat.id_pus_pelayanan');
		$this->db->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
		$this->db->where($where);
		$this->db->group_by('pus_pelayanan_obat.id_obat'); 
		$this->db->order_by('total', 'desc'); 
		$this->db->limit(10); 
		$x = $this->db->get('pus_pelayanan_obat');
		
		$nmr = 0;
		$data1 = '';
		$tabel = '';
		foreach($x->result() as $r1) 
			{
			$nmr = $nmr + 1;
			$data1 .='
			{
					value: '.$r1->total.',
					label: "'.$r1->nama_obat.'"
				},
			';
			}
		$data['d'] = $data1;
		
		$this->db->select('obat.nama_obat, SUM(pus_pelayanan_obat.jumlah_obat) as total');
		$this->db->join('obat', 'obat.id_obat=pus_pelayanan_obat.id_obat');
		$this->db->join('pus_pelayanan', 'pus_pelayanan.id_pus_pelayanan=pus_pelayanan_obat.id_pus_pelayanan');
		$this->db->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
		$this->db->where($where);
		$this->db->group_by('pus_pelayanan_obat.id_obat'); 
		$this->db->order_by('total', 'desc'); 
		$y = $this->db->get('pus_pelayanan_obat');
		$tabel = '';
		foreach($y->result() as $r2) 
			{
			$tabel .= '<tr>';
			$tabel .= '<td>';
			$tabel .= ''.$r2->nama_obat.'';
			$tabel .= '</td>';
			$tabel .= '<td>';
			$tabel .= ''.$r2->total.'';
			$tabel .= '</td>';
			$tabel .= '</tr>';
			
			}
		$data['tabel'] = $tabel;
							
		$data['main_view'] = 'charts/grafik_1c';
		$this->load->view('charts', $data);
	}
		
	public function grafik_1d()
	{
		$periodik = trim($this->input->post('periodik'));
		$id_puskesmas = trim($this->input->post('id_puskesmas'));
		
		$where_pus = array(
			'puskesmas.status !=' => 99,
			'puskesmas.jenis_puskesmas' => 'induk',
			'puskesmas.id_kecamatan !=' => '99'
			);
		$this->db->select('puskesmas.nama_puskesmas, puskesmas.id_puskesmas');
		$this->db->where($where_pus);
		$this->db->order_by('puskesmas.nama_puskesmas');
		$x = $this->db->get('puskesmas');
		$option = '';
		$option .= '<option value="semua">Semua Puskesmas</option>';
		foreach ($x->result() as $x1) 
			{
			if( $x1->id_puskesmas == $id_puskesmas ){
				$option .= '<option value="'.$x1->id_puskesmas.'" selected>'.$x1->nama_puskesmas.'</option>';
				}
			else{
				$option .= '<option value="'.$x1->id_puskesmas.'">'.$x1->nama_puskesmas.'</option>';
				}
			}
		$data['option'] = $option;	
		
		if($periodik == 'periodik'){
			$dari_tanggal = trim($this->input->post('dari_tanggal'));
			$sampai_tanggal = trim($this->input->post('sampai_tanggal'));
			$data['periode'] = 'Dari Tanggal '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' Sampai Tanggal '.$this->Crud_model->dateBahasaIndo($sampai_tanggal).' ';
			$data['dari_tanggal'] = ''.$dari_tanggal.'';
			$data['sampai_tanggal'] = ''.$sampai_tanggal.'';
			if( $id_puskesmas == 'semua' ){
					$where = array(
					'pus_pelayanan_icd_x.status' => 1,
					'pus_pendaftaran.tanggal_pendaftaran >=' => $dari_tanggal,
					'pus_pendaftaran.tanggal_pendaftaran <=' => $sampai_tanggal
					);
				}
			else{
					$where = array(
					'pus_pelayanan_icd_x.status' => 1,
					'pus_pendaftaran.id_puskesmas' => $id_puskesmas,
					'pus_pendaftaran.tanggal_pendaftaran >=' => $dari_tanggal,
					'pus_pendaftaran.tanggal_pendaftaran <=' => $sampai_tanggal
					);
				}
			}
		else{
			$dari_tanggal = date('Y-m-d');
			$sampai_tanggal = date('Y-m-d');
			$data['periode'] = ' Tanggal  : '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' ';
			$data['dari_tanggal'] = $dari_tanggal;
			$data['sampai_tanggal'] = $sampai_tanggal;
			
			$where = array(
			'pus_pelayanan_icd_x.status' => 1,
			'pus_pendaftaran.tanggal_pendaftaran' =>  date('Y-m-d')
			);
			
			}
		
		
		$this->db->select('icd_x.nama_icd_x, COUNT(pus_pelayanan_icd_x.id_icd_x) as total');
		$this->db->join('icd_x', 'icd_x.id_icd_x=pus_pelayanan_icd_x.id_icd_x');
		$this->db->join('pus_pelayanan', 'pus_pelayanan.id_pus_pelayanan=pus_pelayanan_icd_x.id_pus_pelayanan');
		$this->db->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
		$this->db->where($where);
		$this->db->group_by('pus_pelayanan_icd_x.id_icd_x'); 
		$this->db->order_by('total', 'desc'); 
		$this->db->limit(10); 
		$x = $this->db->get('pus_pelayanan_icd_x');
		
		$nmr = 0;
		$data1 = '';
		$tabel = '';
		foreach($x->result() as $r1) 
			{
			$nmr = $nmr + 1;
			$data1 .='
			{
					value: '.$r1->total.',
					label: "'.$r1->nama_icd_x.'"
				},
			';
			}
		$data['d'] = $data1;
		
		$this->db->select('icd_x.nama_icd_x, COUNT(pus_pelayanan_icd_x.id_icd_x) as total');
		$this->db->join('icd_x', 'icd_x.id_icd_x=pus_pelayanan_icd_x.id_icd_x');
		$this->db->join('pus_pelayanan', 'pus_pelayanan.id_pus_pelayanan=pus_pelayanan_icd_x.id_pus_pelayanan');
		$this->db->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
		$this->db->where($where);
		$this->db->group_by('pus_pelayanan_icd_x.id_icd_x'); 
		$this->db->order_by('total', 'desc'); 
		$y = $this->db->get('pus_pelayanan_icd_x');
		$tabel = '';
		foreach($y->result() as $r2) 
			{
			$tabel .= '<tr>';
			$tabel .= '<td>';
			$tabel .= ''.$r2->nama_icd_x.'';
			$tabel .= '</td>';
			$tabel .= '<td>';
			$tabel .= ''.$r2->total.'';
			$tabel .= '</td>';
			$tabel .= '</tr>';
			
			}
		$data['tabel'] = $tabel;
							
		$data['main_view'] = 'charts/grafik_1d';
		$this->load->view('charts', $data);
	}
		
	public function grafik_1e()
	{
		$data['main_view'] = 'charts/grafik_1e';
		$this->load->view('charts', $data);
	}
  
  function user_login_per_day()
  {
    
    $where = array(
		'absensi.user_id !=' => 1,
		'users.hak' => 3,
		'absensi.hari_kerja' => $this->input->post('tanggal')
  	);

	$this->db->select("
	absensi.id_absensi,
	absensi.user_id,
	absensi.hari_kerja,
	absensi.jam_absen,
	absensi.login_dari,
	absensi.jam_pulang,
	absensi.logout_dari,
	users.user_id,
	pegawai.nama_pegawai,
	puskesmas.nama_puskesmas
	");
	$this->db->where($where);
	$this->db->join('users', 'users.user_id = absensi.user_id');
	$this->db->join('puskesmas', 'puskesmas.id_puskesmas = users.id_puskesmas');
	$this->db->join('pegawai', 'pegawai.nip_pegawai = users.user_name');
	
	$this->db->order_by('absensi.jam_absen');
	$result = $this->db->get('absensi');

	$data = '';
	$i = 1;
	foreach ($result->result() as $b1) 
		{			
			$where = array(
			'id_tabel' => $b1->user_id,
			'keterangan' => 'foto_profil'
			);
			$this->db->where($where);
			$this->db->from('attachment');
			$jml = $this->db->count_all_results(); // Produces an integer, like 17
			
			if( $jml > 0 ){
				$this->db->where($where);
				$this->db->limit(1);
				$query = $this->db->get('attachment');
				foreach ($query->result() as $row)
					{
					$file_kecil = 'k_'.$row->file_name.'';
					$file_sedang = 's_'.$row->file_name.'';
					}
				
			}
			else{
					$file_kecil = 'blank_user.png';
					$file_sedang = 'blank_user.png';
			}
						
			$data .= '<tr>';
			$data .= '<td valign="top">'.$i.' </td>';
			$data .= '<td valign="top"><img src="'.base_url().'media/upload/'.$file_sedang.'" class="img-circle" width="50" height="50"></td>';
			$data .= '<td valign="top">'.$b1->nama_pegawai.'</td>';
			$data .= '<td valign="top">'.$b1->nama_puskesmas.'</td>';
			$data .= '<td valign="top">'.$b1->jam_absen.'</td>';
			$data .= '<td valign="top">'.$b1->login_dari.'</td>';
			if( $b1->jam_pulang == '0000-00-00 00:00:00' ){
			$data .= '<td valign="top">-</td>';
			}
			else{
			$data .= '<td valign="top">- </td>';
			}
			$data .= '<td valign="top">- </td>';
			$data .= '</tr>';
			$i++;
		}
    echo $data;
  }
  
  public function grafik_x1()
	{
		$periodik = trim($this->input->post('periodik'));
		$id_puskesmas = trim($this->input->post('id_puskesmas'));
		
		$where_pus = array(
			'puskesmas.status !=' => 99,
			'puskesmas.jenis_puskesmas' => 'induk',
			'puskesmas.id_kecamatan !=' => '99'
			);
		$this->db->select('puskesmas.nama_puskesmas, puskesmas.id_puskesmas');
		$this->db->where($where_pus);
		$this->db->order_by('puskesmas.nama_puskesmas');
		$x = $this->db->get('puskesmas');
		$option = '';
		$option .= '<option value="semua">Semua Puskesmas</option>';
		foreach ($x->result() as $x1) 
			{
			if( $x1->id_puskesmas == $id_puskesmas ){
				$option .= '<option value="'.$x1->id_puskesmas.'" selected>'.$x1->nama_puskesmas.'</option>';
				}
			else{
				$option .= '<option value="'.$x1->id_puskesmas.'">'.$x1->nama_puskesmas.'</option>';
				}
			}
		$data['option'] = $option;	
		
		if($periodik == 'periodik'){
			$dari_tanggal = trim($this->input->post('dari_tanggal'));
			$sampai_tanggal = trim($this->input->post('sampai_tanggal'));
			$data['periode'] = 'Dari Tanggal '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' Sampai Tanggal '.$this->Crud_model->dateBahasaIndo($sampai_tanggal).' ';
			$data['dari_tanggal'] = ''.$dari_tanggal.'';
			$data['sampai_tanggal'] = ''.$sampai_tanggal.'';
			}
		else{
			$dari_tanggal = date("Y-m-d",strtotime("-1 month"));
			$sampai_tanggal = date('Y-m-d');
			$data['periode'] = ' Tanggal  : '.$this->Crud_model->dateBahasaIndo($dari_tanggal).' sampai dengan '.$this->Crud_model->dateBahasaIndo(date('Y-m-d')).')';
			$data['dari_tanggal'] = $dari_tanggal;
			$data['sampai_tanggal'] = $sampai_tanggal;
			}
		$a = $this->getDatesFromRange($dari_tanggal, $sampai_tanggal);
		$s = 0;
		$data1 = '';
		$data2 = '';
		$labels = '';
		foreach ($a as $value) {
			$s = $s + 1;
			
			if($periodik == 'periodik'){
					if($id_puskesmas == 'semua'){
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						}
					else{
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						}
				}
			else{
					if($id_puskesmas == 'semua' || empty($id_puskesmas)){
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value
							);
						}
					else{
						$where1 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						$where2 = array(
							'pus_pendaftaran.status !=' => 99,
							'pus_pelayanan.status' => 2,
							'pus_pendaftaran.tanggal_pendaftaran' => $value,
							'pus_pendaftaran.id_puskesmas' => $id_puskesmas
							);
						}
				}
			
			$this->db->where($where1);
			$this->db->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
			$this->db->from('pus_pelayanan');
			$labels .= '"'.$this->Crud_model->dateBahasaIndo($value).'",';
			$data1 .= '"'.$this->db->count_all_results().'",'; 
			
			$this->db->where($where2);
			$this->db->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran');
			$this->db->from('pus_pelayanan');
			$data2 .= ''.$this->db->count_all_results().','; 
			
		}
		$data['labels'] = $labels;
		$data['data1'] = $data1;
		$data['data2'] = $data2;
		
		$data['main_view'] = 'charts/grafik_1b';
		$this->load->view('charts', $data);
	}
    
	
} 