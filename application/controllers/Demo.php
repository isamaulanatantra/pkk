<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demo extends CI_Controller {

	function __construct()
		{
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Posting_model');
		}    
 
  private function menu_atas($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
        if($web == 'zb.wonosobokab.go.id'){
        $hasil .= '<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'website" class="fa-home ">HOME</a></li>'; 
        }
        else{
        $hasil .= '<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'" class="fa-home ">HOME</a></li>'; 
        }
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_","","","","","","","","","","","","","");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			if( $a > 1 ){
        $hasil .= '
          <li><a href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
        }
			else{ 
        if ($xx == 0){
          $hasil .= '
            <li> <a href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
        }
        else{
          $hasil .= '
            <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
        } 
      }
      
			$hasil = $this->menu_atas($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  
	public function index()
	{
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
      
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
      $a = 0;
      $data['judul'] = 'Selamat datang';
			$data['galery_wonosobo'] = $this->load_galery_wonosobo($h="", $a);
      $data['main_view'] = 'demo/home';
      $this->load->view('demo', $data); 
	}
  
  private function load_galery_wonosobo($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.domain = '".$web."'
		order by posting.created_time desc
    limit 9
		");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
				$hasil .= 
				'
											<div class="col-lg-4">
												<a class="portofolio-item" href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
													<img class="img-fluid" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="">
												</a>
											</div>
						
				';
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
}
