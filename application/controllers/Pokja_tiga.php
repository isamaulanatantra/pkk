<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pokja_tiga extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Pokja_tiga_model');
   }
   
  public function index(){
    $data['main_view'] = 'pokja_tiga/home';
    $this->load->view('tes', $data);
   }
  public function rekap(){
    $data['main_view'] = 'pokja_tiga/rekap';
    $this->load->view('tes', $data);
   }
  public function cetak_pokja_tiga(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_tiga/cetak_pokja_tiga';
    $this->load->view('print', $data);
   }
  public function cetak_semua_pokja_tiga(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_tiga/cetak_semua_pokja_tiga';
    $this->load->view('print', $data);
   }
  public function cetak_pokja_tiga_pekarangan(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_tiga/cetak_pokja_tiga_pekarangan';
    $this->load->view('print', $data);
   }
  public function cetak(){
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }
    $data['main_view'] = 'pokja_tiga/cetak';
    $this->load->view('print', $data);
   }
	public function json_all_pokja_tiga(){
    $web=$this->uut->namadomain(base_url());
		$table = 'pokja_tiga';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
    ( select (dusun.nama_dusun) from dusun where dusun.id_dusun=pokja_tiga.id_dusun limit 1) as nama_dusun,
    ( select (desa.nama_desa) from desa where desa.id_desa=pokja_tiga.id_desa limit 1) as nama_desa,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=pokja_tiga.id_kecamatan limit 1) as nama_kecamatan
    ";
    if($web=='demoopd.wonosobokab.go.id'){
      $where = array(
        'pokja_tiga.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where = array(
        'pokja_tiga.status !=' => 99
        );
    }else{
      $where = array(
        'pokja_tiga.status !=' => 99,
        'pokja_tiga.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        $query_desa = $this->db->query("SELECT desa.nama_desa FROM desa WHERE desa.id_desa = ".$row->id_desa."");
        echo '
        <tr id_pokja_tiga="'.$row->id_pokja_tiga.'" id="'.$row->id_pokja_tiga.'" name="id_pokja_tiga">
          <td>'.$urut.'</td>
          <td>'; 
            $id_desa = $row->id_desa; 
            if($id_desa=='9999'){
              echo 'TP PKK Kecamatan';
            } 
            else{
              foreach ($query_desa->result() as $row_desa){
                echo ''.$row_desa->nama_desa.'';
              }
            }
          echo'</td>
          <td>'.$row->jumlah_kader_pangan.'</td>
          <td>'.$row->jumlah_kader_sandang.'</td>
          <td>'.$row->jumlah_kader_tatalaksanart.'</td>
          <td>'.$row->jumlah_panganmakananpokok_beras.'</td>
          <td>'.$row->jumlah_panganmakananpokok_nonberas.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_peternakan.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_perikanan.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_warunghidup.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_lumbunghidup.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_toga.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_tanamankeras.'</td>
          <td>'.$row->jumlah_industrirumahtangga_pangan.'</td>
          <td>'.$row->jumlah_industrirumahtangga_sandang.'</td>
          <td>'.$row->jumlah_industrirumahtangga_jasa.'</td>
          <td>'.$row->jumlah_rumahsehat_layakhuni.'</td>
          <td>'.$row->jumlah_rumahtidaksehat_layakhuni.'</td>
          <td>'.$row->keterangan.'</td>
          <td>
            <a href="#tab_form_pokja_tiga" data-toggle="tab" class="update_id_pokja_tiga badge bg-success btn-sm"><i class="fa fa-pencil-square-o"></i></a>
            <a href="#" id="del_ajax_pokja_tiga" class="badge bg-danger btn-sm"><i class="fa fa-cut"></i></a>
          </td>
        </tr>
        ';
      }
			/*echo '
      <tr>
        <td valign="top" colspan="10" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'pokja_tiga/cetak_pokja_tiga/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Keluarga</a>
        </td>
      </tr>
      ';*/
          
	}
	public function json_all_rekap_pokja_tiga(){
    $web=$this->uut->namadomain(base_url());
    $query = $this->db->query("SELECT SUM(jumlah_kader_pangan)as jumlah_kader_pangan, 
     SUM(jumlah_kader_sandang)as jumlah_kader_sandang,
     SUM(jumlah_kader_tatalaksanart)as jumlah_kader_tatalaksanart,
     SUM(jumlah_panganmakananpokok_beras)as jumlah_panganmakananpokok_beras,
     SUM(jumlah_panganmakananpokok_nonberas)as jumlah_panganmakananpokok_nonberas,
     SUM(jumlah_panganhatinyapkk_peternakan)as jumlah_panganhatinyapkk_peternakan,
     SUM(jumlah_panganhatinyapkk_perikanan)as jumlah_panganhatinyapkk_perikanan,
     SUM(jumlah_panganhatinyapkk_warunghidup)as jumlah_panganhatinyapkk_warunghidup,
     SUM(jumlah_panganhatinyapkk_lumbunghidup)as jumlah_panganhatinyapkk_lumbunghidup,
     SUM(jumlah_panganhatinyapkk_toga)as jumlah_panganhatinyapkk_toga,
     SUM(jumlah_panganhatinyapkk_tanamankeras)as jumlah_panganhatinyapkk_tanamankeras,
     SUM(jumlah_industrirumahtangga_pangan)as jumlah_industrirumahtangga_pangan,
     SUM(jumlah_industrirumahtangga_sandang)as jumlah_industrirumahtangga_sandang,
     SUM(jumlah_industrirumahtangga_jasa)as jumlah_industrirumahtangga_jasa,
     SUM(jumlah_rumahsehat_layakhuni)as jumlah_rumahsehat_layakhuni,
     SUM(jumlah_rumahtidaksehat_layakhuni)as jumlah_rumahtidaksehat_layakhuni,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=pokja_tiga.id_kecamatan limit 1) as nama_kecamatan
    FROM pokja_tiga WHERE pokja_tiga.id_kabupaten=1 AND pokja_tiga.id_propinsi=1 GROUP BY pokja_tiga.id_kecamatan");
    $urut=0;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
        echo '
        <tr>
          <td>'.$urut.'</td>
          <td>'.$row->nama_kecamatan.'</td>
          <td>'.$row->jumlah_kader_pangan.'</td>
          <td>'.$row->jumlah_kader_sandang.'</td>
          <td>'.$row->jumlah_kader_tatalaksanart.'</td>
          <td>'.$row->jumlah_panganmakananpokok_beras.'</td>
          <td>'.$row->jumlah_panganmakananpokok_nonberas.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_peternakan.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_perikanan.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_warunghidup.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_lumbunghidup.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_toga.'</td>
          <td>'.$row->jumlah_panganhatinyapkk_tanamankeras.'</td>
          <td>'.$row->jumlah_industrirumahtangga_pangan.'</td>
          <td>'.$row->jumlah_industrirumahtangga_sandang.'</td>
          <td>'.$row->jumlah_industrirumahtangga_jasa.'</td>
          <td>'.$row->jumlah_rumahsehat_layakhuni.'</td>
          <td>'.$row->jumlah_rumahtidaksehat_layakhuni.'</td>
          <td></td>
        </tr>
        ';
      }
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='demoopd.wonosobokab.go.id'){
      $where0 = array(
        'pokja_tiga.status !=' => 99
        );
    }elseif($web=='pkk.wonosobokab.go.id'){
      $where0 = array(
        'pokja_tiga.status !=' => 99
        );
    }else{
      $where0 = array(
        'pokja_tiga.status !=' => 99,
        'pokja_tiga.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('pokja_tiga');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
  public function simpan_pokja_tiga()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('jumlah_kader_pangan', 'jumlah_kader_pangan', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => 1,
        'id_kabupaten' => 1,
        'id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'tahun' => trim($this->input->post('tahun')),
        'jumlah_kader_pangan' => trim($this->input->post('jumlah_kader_pangan')),
        'jumlah_kader_sandang' => trim($this->input->post('jumlah_kader_sandang')),
        'jumlah_kader_tatalaksanart' => trim($this->input->post('jumlah_kader_tatalaksanart')),
        'jumlah_panganmakananpokok_beras' => trim($this->input->post('jumlah_panganmakananpokok_beras')),
        'jumlah_panganmakananpokok_nonberas' => trim($this->input->post('jumlah_panganmakananpokok_nonberas')),
        'jumlah_panganhatinyapkk_peternakan' => trim($this->input->post('jumlah_panganhatinyapkk_peternakan')),
        'jumlah_panganhatinyapkk_perikanan' => trim($this->input->post('jumlah_panganhatinyapkk_perikanan')),
        'jumlah_panganhatinyapkk_warunghidup' => trim($this->input->post('jumlah_panganhatinyapkk_warunghidup')),
        'jumlah_panganhatinyapkk_lumbunghidup' => trim($this->input->post('jumlah_panganhatinyapkk_lumbunghidup')),
        'jumlah_panganhatinyapkk_toga' => trim($this->input->post('jumlah_panganhatinyapkk_toga')),
        'jumlah_panganhatinyapkk_tanamankeras' => trim($this->input->post('jumlah_panganhatinyapkk_tanamankeras')),
        'jumlah_industrirumahtangga_pangan' => trim($this->input->post('jumlah_industrirumahtangga_pangan')),
        'jumlah_industrirumahtangga_sandang' => trim($this->input->post('jumlah_industrirumahtangga_sandang')),
        'jumlah_industrirumahtangga_jasa' => trim($this->input->post('jumlah_industrirumahtangga_jasa')),
        'jumlah_rumahsehat_layakhuni' => trim($this->input->post('jumlah_rumahsehat_layakhuni')),
        'jumlah_rumahtidaksehat_layakhuni' => trim($this->input->post('jumlah_rumahtidaksehat_layakhuni')),
        'keterangan' => trim($this->input->post('keterangan')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'pokja_tiga';
      $this->Pokja_tiga_model->simpan_pokja_tiga($data_input, $table_name);
      /*echo $id;
			$table_name  = 'anggota_keluarga';
      $where       = array(
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_pokja_tiga' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);*/
     }
   }
  public function update_pokja_tiga()
   {
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
		$this->form_validation->set_rules('jumlah_kader_pangan', 'jumlah_kader_pangan', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
			
        'temp' => trim($this->input->post('temp')),
        'id_propinsi' => 1,
        'id_kabupaten' => 1,
        'id_kecamatan' => $this->session->userdata('id_kecamatan'),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_dusun' => trim($this->input->post('id_dusun')),
        'rt' => trim($this->input->post('rt')),
        'tahun' => trim($this->input->post('tahun')),
        'jumlah_kader_pangan' => trim($this->input->post('jumlah_kader_pangan')),
        'jumlah_kader_sandang' => trim($this->input->post('jumlah_kader_sandang')),
        'jumlah_kader_tatalaksanart' => trim($this->input->post('jumlah_kader_tatalaksanart')),
        'jumlah_panganmakananpokok_beras' => trim($this->input->post('jumlah_panganmakananpokok_beras')),
        'jumlah_panganmakananpokok_nonberas' => trim($this->input->post('jumlah_panganmakananpokok_nonberas')),
        'jumlah_panganhatinyapkk_peternakan' => trim($this->input->post('jumlah_panganhatinyapkk_peternakan')),
        'jumlah_panganhatinyapkk_perikanan' => trim($this->input->post('jumlah_panganhatinyapkk_perikanan')),
        'jumlah_panganhatinyapkk_warunghidup' => trim($this->input->post('jumlah_panganhatinyapkk_warunghidup')),
        'jumlah_panganhatinyapkk_lumbunghidup' => trim($this->input->post('jumlah_panganhatinyapkk_lumbunghidup')),
        'jumlah_panganhatinyapkk_toga' => trim($this->input->post('jumlah_panganhatinyapkk_toga')),
        'jumlah_panganhatinyapkk_tanamankeras' => trim($this->input->post('jumlah_panganhatinyapkk_tanamankeras')),
        'jumlah_industrirumahtangga_pangan' => trim($this->input->post('jumlah_industrirumahtangga_pangan')),
        'jumlah_industrirumahtangga_sandang' => trim($this->input->post('jumlah_industrirumahtangga_sandang')),
        'jumlah_industrirumahtangga_jasa' => trim($this->input->post('jumlah_industrirumahtangga_jasa')),
        'jumlah_rumahsehat_layakhuni' => trim($this->input->post('jumlah_rumahsehat_layakhuni')),
        'jumlah_rumahtidaksehat_layakhuni' => trim($this->input->post('jumlah_rumahtidaksehat_layakhuni')),
        'keterangan' => trim($this->input->post('keterangan')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
				);
      $table_name  = 'pokja_tiga';
      $where       = array(
        'pokja_tiga.id_pokja_tiga' => trim($this->input->post('id_pokja_tiga')),
        'pokja_tiga.domain' => $web
			);
      $this->Pokja_tiga_model->update_data_pokja_tiga($data_update, $where, $table_name);
      echo 1;
     }
   }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      if($web=='demoopd.wonosobokab.go.id'){
        $where = array(
        'pokja_tiga.id_pokja_tiga' => $this->input->post('id_pokja_tiga'),
          );
      }elseif($web=='pkk.wonosobokab.go.id'){
        $where = array(
        'pokja_tiga.id_pokja_tiga' => $this->input->post('id_pokja_tiga'),
          );
      }else{
        $where = array(
        'pokja_tiga.id_pokja_tiga' => $this->input->post('id_pokja_tiga'),
          'pokja_tiga.id_kecamatan' => $this->session->userdata('id_kecamatan')
          );
      }
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_pokja_tiga');
      $result = $this->db->get('pokja_tiga');
      echo json_encode($result->result_array());
		}
   
   public function total_pokja_tiga()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('pokja_tiga');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_pokja_tiga = $this->input->post('id_pokja_tiga');
      $where = array(
        'id_pokja_tiga' => $id_pokja_tiga,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('pokja_tiga');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'pokja_tiga';
        $this->Pokja_tiga_model->update_data_pokja_tiga($data_update, $where, $table_name);
        echo 1;
        }
		}
  function option_desa_by_id_kecamatan(){
		$id_kecamatan = $this->session->userdata('id_kecamatan');
		$w = $this->db->query("
		SELECT id_desa, nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function option_desa_by_id_desa(){
		$id_kecamatan = $this->input->post('id_kecamatan');
		$w = $this->db->query("
		SELECT id_desa, nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function option_propinsi(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_propinsi="";
		}
		else{
			//$where_id_propinsi="and propinsi.id_propinsi=".$this->session->userdata('id_propinsi')."";
			$where_id_propinsi="";
		}
		$w = $this->db->query("
		SELECT *
		from propinsi
		where propinsi.status = 1
    ".$where_id_propinsi."
		order by propinsi.id_propinsi
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_propinsi.'">'.$h->nama_propinsi.'</option>';
		}
	}
  function id_kabupaten_by_id_propinsi(){
		$id_propinsi = $this->input->post('id_propinsi');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kabupaten
		where kabupaten.status = 1
    and kabupaten.id_propinsi=".$id_propinsi."
		order by kabupaten.id_kabupaten
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kabupaten.'">'.$h->nama_kabupaten.'</option>';
		}
	}
  function id_kecamatan_by_id_kabupaten(){
		$id_kabupaten = $this->input->post('id_kabupaten');
    $web=$this->uut->namadomain(base_url());
		$w = $this->db->query("
		SELECT *
		from kecamatan
		where kecamatan.status = 1
		and kecamatan.id_kabupaten=".$id_kabupaten."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function id_desa_by_id_kecamatan(){
		$id_kecamatan = $this->session->userdata('id_kecamatan');
		$w = $this->db->query("
		SELECT nama_desa
		from desa
		where desa.status = 1
		and desa.id_kabupaten = 1
		and desa.id_kecamatan = ".$id_kecamatan."
		order by id_desa");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_desa.'">'.$h->nama_desa.'</option>';
		}
	}
  function id_dusun_by_id_desa(){
		$id_desa = $this->input->post('id_desa');
		$w = $this->db->query("
		SELECT *
		from dusun
		where dusun.status = 1
		and dusun.id_desa = ".$id_desa."
		order by id_dusun");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_dusun.'">'.$h->nama_dusun.'</option>';
		}
	}
  function option_kecamatan(){
    $web=$this->uut->namadomain(base_url());
		if($web=='demoopd.wonosobokab.go.id'){
			$where_id_kecamatan="";
		}
		else{
			$where_id_kecamatan="and data_kecamatan.id_kecamatan=".$this->session->userdata('id_kecamatan')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_kecamatan, kecamatan
		where data_kecamatan.status = 1
		and kecamatan.id_kecamatan=data_kecamatan.id_kecamatan
    ".$where_id_kecamatan."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function status_dalam_keluarga(){
		$w = $this->db->query("
		SELECT *
		from status_dalam_keluarga
		where status_dalam_keluarga.status = 1
		order by status_dalam_keluarga.id_status_dalam_keluarga
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_dalam_keluarga.'">'.$h->nama_status_dalam_keluarga.'</option>';
		}
	}
  function pekerjaan(){
		$w = $this->db->query("
		SELECT *
		from pekerjaan
		where pekerjaan.status = 1
		order by pekerjaan.id_pekerjaan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pekerjaan.'">'.$h->nama_pekerjaan.'</option>';
		}
	}
  function agama(){
		$w = $this->db->query("
		SELECT *
		from agama
		where agama.status = 1
		order by agama.id_agama
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_agama.'">'.$h->nama_agama.'</option>';
		}
	}
  function status_pernikahan(){
		$w = $this->db->query("
		SELECT *
		from status_pernikahan
		where status_pernikahan.status = 1
		order by status_pernikahan.id_status_pernikahan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_status_pernikahan.'">'.$h->nama_status_pernikahan.'</option>';
		}
	}
  function pendidikan(){
		$w = $this->db->query("
		SELECT *
		from pendidikan
		where pendidikan.status = 1
		order by pendidikan.id_pendidikan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_pendidikan.'">'.$h->nama_pendidikan.'</option>';
		}
	}
 }