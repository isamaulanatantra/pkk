<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website extends CI_Controller {

	function __construct()
		{
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Posting_model');
		}    
 
  private function menu_atas($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
        $hasil .= '<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'website">HOME</a></li>'; 
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
      $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
      $yummy   = array("_","","","","","","","","","","","","","");
      $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			if( $a > 1 ){
					if ($xx == 0){
						$hasil .= '
							<li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
					}
					else{
						$hasil .= '
							<li class="parent" > <span class="">'.$h->judul_posting.' </span>';
					} 
        }
			else{ 
        if ($xx == 0){
          $hasil .= '
            <li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
        }
        else{
          $hasil .= '
            <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
        } 
      }
      
			$hasil = $this->menu_atas($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  
  public function get_attachment_by_id_posting($id_posting)
   {
     $where = array(
						'id_tabel' => $id_posting
						);
    $d = $this->Posting_model->get_attachment_by_id_posting($where);
    if(!$d)
          {
						return 'blankgambar.jpg';
          }
        else
          {
						return $d['file_name'];
          }
   } 
  
  private function posting_berita($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where posting.status = 1 
		and posting.parent = '".$parent."'
		and posting.highlight = 1 
    and domain='".$web."'
    order by urut
    limit 6
		");
		$xx = $w->num_rows();
		if(($w->num_rows())>0){
			$hasils .= '
                    <div class="blog-tags">
                  ';
		}
		$nomor = 0;
		foreach($w->result() as $h){
      $nomor = $nomor + 1;
      $rs = $this->db->query("
      SELECT * from posting
      where parent='".$h->id_posting."'
      and domain='".$web."'
      order by urut
      limit 6
      ");
			$nomor = $nomor + 1;
      $xx = $rs->num_rows();
			if( $r > 1 ){
        $hasils .= '
              <ul class="posts-list margin-top-1">
                <li>
                  <div class="recent-post">
                      <a href="">
                          <img class="pull-left" src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="thumb1" style="width:54px">
                      </a>
                      <a href="#" class="posts-list-title">'.$h->judul_posting.'</a>
                      <br>
                      <span class="recent-post-date">
                          July 30, 2013
                      </span>
                  </div>
                  <div class="clearfix"></div>
                </li>
              </ul>
        ';
        }
			else{ 
        if ($xx == 0){
        $hasils .= '
          <ul class="blog-tags">
            <li> 
              <a class="blog-tag" href="">'.$h->judul_posting.'</a>
            </li> 
          </ul>
        ';
        }
        else{
        $hasils .= '
          <ul class="blog-tags">
            <li> 
              <a class="blog-tag" href="">'.$h->judul_posting.'</a>
            </li> 
          </ul>
        ';
        }
      }
      $hasils = $this->posting_berita($h->id_posting,$hasils, $r);
		}
		if(($w->num_rows)>0){
			$hasils .= '</div>';
		}
    else{
    }
		return $hasils;
	}
  private function posting_pengumuman($parents,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where parent='".$parents."' 
		and posting.status = 1 
		and posting.highlight = 1 
    and domain='".$web."'
    order by urut
    limit 6
		");
		$xx = $w->num_rows();
		if(($w->num_rows())>0){
			$hasils .= '
                    <div class="blog-tags">
                  ';
		}
		$nomor = 0;
		foreach($w->result() as $h){
      $nomor = $nomor + 1;
      $rs = $this->db->query("
      SELECT * from posting
      where parent='".$h->id_posting."'
      and domain='".$web."'
    order by urut
      limit 6
      ");
			$nomor = $nomor + 1;
      $xx = $rs->num_rows();
			if( $r > 1 ){
        $hasils .= '
              <ul class="posts-list margin-top-1">
                <li>
                  <div class="recent-post">
                      <a href="">
                          <img class="pull-left" src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="thumb1" style="width:54px">
                      </a>
                      <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML" class="posts-list-title">'.$h->judul_posting.'</a>
                      <br>
                      <span class="recent-post-date">'.$h->created_time.'</span>
                  </div>
                  <div class="clearfix"></div>
                </li>
              </ul>
        ';
        }
			else{ 
        if ($xx == 0){
        $hasils .= '
          <ul class="blog-tags">
            <li> 
              <a class="blog-tag" href="">'.$h->judul_posting.'</a>
            </li> 
          </ul>
        ';
        }
        else{
        $hasils .= '
          <ul class="blog-tags">
            <li> 
              <a class="blog-tag" href="">'.$h->judul_posting.'</a>
            </li> 
          </ul>
        ';
        }
      }
      $hasils = $this->posting_pengumuman($h->id_posting,$hasils, $r);
		}
		if(($w->num_rows)>0){
			$hasils .= '</div>';
		}
    else{
    }
		return $hasils;
	}
  
  private function posting_highlight($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where parent='".$parent."' 
		and posting.status = 1 
		and posting.highlight = 1 
    and domain='".$web."'
    order by urut
    limit 5
		");
		$x = $w->num_rows();
		if(($w->num_rows())>0){
			$hasils .= '
                    <div class="blog-tags">
                  ';
		}
		$nomor = 0;
		foreach($w->result() as $h){
      $nomor = $nomor + 1;
      $rs = $this->db->query("
      SELECT * from posting
      where parent='".$h->id_posting."'
      and domain='".$web."'
    order by urut
      limit 5
      ");
			$nomor = $nomor + 1;
      $xx = $rs->num_rows();
			if( $r > 1 ){
        $hasils .= '
              <ul class="posts-list margin-top-1">
                <li>
                  <div class="recent-post">
                      <a href="">
                          <img class="pull-left" src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="thumb1" style="width:54px">
                      </a>
                      <a href="#" class="posts-list-title">'.$h->judul_posting.'</a>
                      <br>
                      <span class="recent-post-date">
                          July 30, 2013
                      </span>
                  </div>
                  <div class="clearfix"></div>
                </li>
              </ul>
        ';
        }
			else{ 
        if ($xx == 0){
        $hasils .= '
      <ul class="blog-tags">
        <li> 
          <a class="blog-tag" href="">'.$h->judul_posting.'</a>
        </li> 
      </ul>
        ';
        }
        else{
        $hasils .= '
      <ul class="blog-tags">
        <li> 
          <a class="blog-tag" href="">'.$h->judul_posting.'</a>
        </li> 
      </ul>
        ';
        } 
      }
      $hasils = $this->posting_highlight($h->id_posting,$hasils, $r);
		}
		if(($w->num_rows)>0){
			$hasils .= '
                    </div>
      ';
		}
    else{
    }
		return $hasils;
	}
		
  private function posting_highlight1($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where parent='".$parent."' 
		and posting.status = 1 
    and domain='".$web."'
    order by urut desc
    limit 6
		");
		$x = $w->num_rows();
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    $nomor = $nomor + 1;
    $rs = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."'
    and domain='".$web."'
    limit 6
		");
		$xx = $rs->num_rows();
      if( $h->highlight == 1 ){
       
            $hasils .= 
            '
            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$h->judul_posting.'">
                    <figure class="animate fadeIn">
                        <img alt="'.$h->id_posting.'" src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'">
                        <figcaption>
                            <h3>'.$h->judul_posting.'</h3>
                            <span>'.substr(strip_tags($h->isi_posting), 0, 100).'</span>
                        </figcaption>
                    </figure>
                </a>
            </li>
            ';
          
      }        
			
      $hasils = $this->posting_highlight($h->id_posting,$hasils, $r);
		}
		return $hasils;
	}
		
  private function posting_daftar($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where parent='".$parent."' 
		and posisi = 'menu_atas'
		and status = 1 
    and domain='".$web."'
    order by posisi, urut
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<tr id_posting="'.$h->id_posting.'" id="'.$h->id_posting.'" >';
      if( $a == 1 ){
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->judul_posting.' </td>';
        }
			else if($a == 2){
        $hasil .= '<td style="padding: 2px 2px 2px '.($a * 20).'px ;"> <a target="_blank" href="'.base_url().'postings/categories/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a> </td>';
        }
			else{
        }
			$hasil .= '</tr>';
			$hasil = $this->posting_daftar($h->id_posting,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  private function posting_teksjalan($a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where parent!='0' 
		and tampil_menu_marquee	 = 1
		and status = 1 
    and domain='".$web."'
    order by created_time	DESC
    limit 1
		");
		foreach($w->result() as $h)
		{
      if( $a == 1 ){
        echo ' <a target="_blank" href="'.base_url().'postings/categories/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a> ';
        }
			else if($a == 2){
        echo ' <a target="_blank" href="'.base_url().'postings/categories/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a> ';
        }
			else{
        echo ' <a target="_blank" href="'.base_url().'postings/categories/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a> ';
        }
			// echo = $this->posting_teksjalan($a);
		}
		//return $hasil;
	}
  private function skpd($hasil, $a){
    // $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from skpd
									
		where status = 1
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
      if( $a > 1 ){
        $hasil .= '
          <div class="col-md-6">
          <a class="btn btn-danger margin-top-10" type="button" href="https://'.$h->domain.'" style="">
            <i class="fa '.$h->icon.'"></i> '.substr(strip_tags($h->judul_skpd), 0, 50).'
          </a>
          </div>
        ';
        }
			else{
        // $hasil .= '<td style="padding: 2px 2px 2px '.($a * 20).'px ;"> <a target="_blank" href="https://'.$h->domain.'">'.$h->judul_skpd.' </a> </td>';
        // $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->judul_skpd.' </td>';
        }
      // $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->telp.' </td>';
      // $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->domain.' </td>';
      // $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->keterangan.' </td>';
			$hasil = $this->skpd($h->id_skpd,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  private function website_desa(){
		$w = $this->db->query("
		SELECT * from website_desa
									
		where status = 1
    order by domain
		");
		foreach($w->result() as $h)
		{
        $hasil .= '
          <div class="col-md-6">
          <a class="btn btn-danger margin-top-10" type="button" href="https://'.$h->domain.'" style="">
            <i class="fa fa-home"></i> '.$h->keterangan.'
          </a>
          </div>
        ';
			$hasil = $this->website_desa($h->id_website_desa);
		}
		
		return $hasil;
	}
  private function skpd_opd($parent=0,$hasil, $a){
    // $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from skpd
									
		where parent='".$parent."' 
		and status = 1 
		and highlight = 1 
		and keterangan = 'websiteopd' 
    order by telp, domain
		");
		
		if(($w->num_rows())>0)
		{
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
      if( $a > 1 ){
        }
			else{
        $hasil .= '
          <div class="col-md-6">
          <a class="btn btn-danger margin-top-10" type="button" href="https://'.$h->domain.'" style="">
            <i class="fa '.$h->icon.'"></i> '.substr(strip_tags($h->judul_skpd), 0, 50).'
          </a>
          </div>
        ';
        }
			$hasil = $this->skpd_opd($h->id_skpd,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
		}
		
		return $hasil;
	}
  private function skpd_kecamatan($parent=0,$hasil, $a){
    // $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from skpd
									
		where parent='".$parent."' 
		and status = 1 
		and highlight = 1 
		and keterangan = 'websitekecamatan' 
    order by telp, domain
		");
		
		if(($w->num_rows())>0)
		{
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
      if( $a > 1 ){
        }
			else{
        $hasil .= '
          <div class="col-md-6">
          <a class="btn btn-danger margin-top-10" type="button" href="https://'.$h->domain.'" style="">
            <i class="fa '.$h->icon.'"></i> '.substr(strip_tags($h->judul_skpd), 0, 50).'
          </a>
          </div>
        ';
        }
			$hasil = $this->skpd_kecamatan($h->id_skpd,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
		}
		
		return $hasil;
	}
  private function skpd_desa($parent=0,$hasil, $a){
    // $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from skpd
									
		where parent='".$parent."' 
		and status = 1 
		and highlight = 1 
		and keterangan = 'websitedesa' 
    order by telp, domain
		");
		
		if(($w->num_rows())>0)
		{
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
      if( $a > 1 ){
        }
			else{
        $hasil .= '
          <div class="col-md-6">
          <a class="btn btn-danger margin-top-10" type="button" href="https://'.$h->domain.'" style="">
            <i class="fa '.$h->icon.'"></i> '.substr(strip_tags($h->judul_skpd), 0, 50).'
          </a>
          </div>
        ';
        }
			$hasil = $this->skpd_desa($h->id_skpd,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
		}
		
		return $hasil;
	}
  private function posting_egovernment($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where parent='".$parent."' 
		and status = 1 
		and posisi = 'egovernment' 
    and domain='".$web."'
    order by posisi, urut
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
      if( $a == 1 ){
        $hasil .= '
          <a class="btn btn-danger" type="button" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">
            <i class="fa '.$h->icon.'"></i> '.$h->judul_posting.'
          </a>
          ';
        }
			else if($a == 2){
        $hasil .= '
          <a style="padding: 2px 2px 2px '.($a * 20).'px ;" class="btn btn-danger" type="button" href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">
            <i class="fa '.$h->icon.'"></i> '.$h->judul_posting.'
          </a>
          ';
        }
			else{
        }
			$hasil .= '</tr>';
			$hasil = $this->posting_egovernment($h->id_posting,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  
  private function posting_marquee($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where parent='".$parent."' 
		and status = 1
		and tampil_menu_marquee = 1
    and domain='".$web."'
    order by created_time, domain
		");
		foreach($w->result() as $h)
		{
      if( $a > 1 ){
        // $hasil .= ''.substr(strip_tags($h->judul_posting), 0, 50).'';
        $hasil .= '|&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u><a target="_blank" href="'.base_url().'postings/categories/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a></u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
        }
			else{
        $hasil .= '
         '.substr(strip_tags($h->judul_posting), 0, 50).' 
        ';
      }
			$hasil = $this->posting_marquee($h->id_posting,$hasil, $a);
		}
		return $hasil;
	}
  private function posting_galery_berita($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where parent='".$parent."' 
		and posting.status = 1 
    and domain='".$web."'
    order by created_time desc
    
    
		");
		$x = $w->num_rows();
		$nomor = 0;
		foreach($w->result() as $h)
		{
    $nomor = $nomor + 1;
    $rs = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."'
    and domain='".$web."'
    order by created_time desc
    limit 3
		");
		$xx = $rs->num_rows();
      if( $h->highlight == 1 ){
            $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
            $yummy   = array("_","","","","","","","","","","","","","");
            $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
            $hasils .= 
            '
          <div class="col-lg-4">
            <a class="portofolio-item" href="'.base_url().'postings/details/'.$h->id_posting.'/'.$newphrase.'.HTML">
              <span class="caption">
                <span class="caption-content">
                  <h2>'.$h->judul_posting.'</h2>
                  <p class="mb-0">'.substr(strip_tags($h->isi_posting), 0, 100).'</p>
                </span>
              </span>
              <img class="img-fluid" src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="">
            </a>
          </div>
            ';
      }        
			
      $hasils = $this->posting_galery_berita($h->id_posting,$hasils, $r);
		}
		return $hasils;
	}
	public function index(){
    $web=$this->uut->namadomain(base_url());
      $where0 = array(
        'domain' => $web
        );
      $this->db->where($where0);
      $this->db->limit(1);
      $query0 = $this->db->get('dasar_website');
      foreach ($query0->result() as $row0)
        {
          $data['domain'] = $row0->domain;
          $data['alamat'] = $row0->alamat;
          $data['telpon'] = $row0->telpon;
          $data['email'] = $row0->email;
          $data['twitter'] = $row0->twitter;
          $data['facebook'] = $row0->facebook;
          $data['google'] = $row0->google;
          $data['instagram'] = $row0->instagram;
          $data['peta'] = $row0->peta;
          $data['keterangan'] = $row0->keterangan;
        }
        
      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->judul_komponen == 'Header' ){ //Header
            $data['Header'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
            $data['KolomKiriAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
            $data['KolomKananAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
            $data['KolomKiriBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
            $data['KolomPalingBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
            $data['KolomKananBawah'] = $row1->isi_komponen;
          }
          else{
          }
        }
      $r = 0;
      $p=0;
      $a = 0;
      $data['galery_berita'] = $this->option_posting_disparbud($h="", $a);
      $a = 0;
      $data['total'] = 10;
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['menu_mobile'] = $this->menu_mobile(0,$h="", $a);
        
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'welcome/welcome';
      $this->load->view('post_wonosobo', $data); 
	}
	public function dume(){
    $web=$this->uut->namadomain(base_url());
      $where0 = array(
        'domain' => $web
        );
      $this->db->where($where0);
      $this->db->limit(1);
      $query0 = $this->db->get('dasar_website');
      foreach ($query0->result() as $row0)
        {
          $data['domain'] = $row0->domain;
          $data['alamat'] = $row0->alamat;
          $data['telpon'] = $row0->telpon;
          $data['email'] = $row0->email;
          $data['twitter'] = $row0->twitter;
          $data['facebook'] = $row0->facebook;
          $data['google'] = $row0->google;
          $data['instagram'] = $row0->instagram;
          $data['peta'] = $row0->peta;
          $data['keterangan'] = $row0->keterangan;
        }
        
      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->judul_komponen == 'Header' ){ //Header
            $data['Header'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
            $data['KolomKiriAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
            $data['KolomKananAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
            $data['KolomKiriBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
            $data['KolomPalingBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
            $data['KolomKananBawah'] = $row1->isi_komponen;
          }
          else{
          }
          if( $row1->status == 1 ){
            $data['status_komponen'] = $row1->status;
          }
          else{ 
          }
        }
      $r = 0;
      $p=0;
      $a = 0;
      $data['galery_berita'] = $this->load_pilihan($h="", $a);
      // $data['load_slide'] = $this->load_slide();
      $a = 0;
      $data['total'] = 10;
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['menu_mobile'] = $this->menu_mobile(0,$h="", $a);
        
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'welcome/welcome_dume';
      $this->load->view('welcome_dume', $data); 
	}
  
  private function menu_mobile($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
					$hasil .= '<ul> <li><a href="'.base_url().'" role="button" aria-expanded="false">HOME</a></li>';
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
        $hasil .= '
          <li> <span class=""> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </span></a>';
        }
			else{
        if($h->judul_posting == 'Berita'){
            $hasil .= '
              <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
        }
        else{
          if ($xx == 0){
            $hasil .= '
              <li><a href="'.base_url().'post/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span></a>';
          }
          else{
            $hasil .= '
              <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
          } 
        }
      }
      
			$hasil = $this->menu_mobile($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  public function load_posting_pengumuman()
		{
			$pengumuman = $this->input->post('pengumuman');
			$web=$this->uut->namadomain(base_url());
			$this->db->select("*");
			$where_wisata = array(
				'status' => 1,
				'domain' => $web
				);
			$this->db->like('judul_posting', $pengumuman);
			$this->db->where($where_wisata);
			$this->db->order_by('posting.created_time', 'desc');
			$query_wisata = $this->db->get('posting');
			$a = 0;
			foreach ($query_wisata->result() as $row_wisata)
				{
																						echo'
																							<div class="slider-item">
																								<div class="row">
																								';
					$w = $this->db->query("
					SELECT *, (select file_name from attachment where attachment.id_tabel=posting.id_posting limit 1) as file_name_attachment from posting
					where parent='".$row_wisata->id_posting."'
					and status = 1 
					and domain='".$web."'
					order by created_time desc 
					limit 3
					");
					$x = $w->num_rows();
					$nomor = 0;
					foreach($w->result() as $row_posting)
					{
						$healthy = array(" ", "#", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
						$yummy   = array("_", "", "", "", "","", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
						$judul_posting = str_replace($healthy, $yummy, $row_posting->judul_posting);
						echo'
									<div class="slider-item">
										<a href="'.base_url().'post/detail/'.$row_posting->id_posting.'/'.$judul_posting.'.HTML" class="featured-href">
										<img src="'.base_url().'media/upload/'.$row_posting->file_name_attachment.'" alt=""/>
										<span>'.$row_posting->judul_posting.' | '.$row_posting->created_time.'</span>
										</a>
									</div>
							 ';
					}
																						echo'
																							</div>
																						</div>
																						';
				}
   }
	 
	 
  private function option_posting_disparbud($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 10
		");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			if( $nomor == 1 ){
				$hasil .= 
				'
          <article class="news-block">
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="overlay-link">
              <figure class="image-overlay">
                <img src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'" width="870" height="500" alt=""/>
              </figure>
            </a>
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="category">'.$h->judul_posting.'</a>
            <div class="news-details">
              <h2 class="news-title">
                <a href="#">
                </a>
              </h2>
              <p>
              <p style="text-align:justify"><span style="font-size:14px">'.substr(strip_tags($h->isi_posting), 0, 200).'</span></p>
              <p class="simple-share">
                by <a href="#"><b>Adm, </b></a> -  
                <span class="article-date"><i class="fa fa-clock-o"></i> '.$h->created_time.' wib</span>
              </p>
            </div>
          </article>
				';
			}
			else{
				$hasil .= 
				'
						<article class="simple-post clearfix">
							<div class="simple-thumb">
								<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
								<img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt=""/>
								</a>
							</div>
							<header>
								<h2>
									<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a>
								</h2>
								<p class="simple-share">
									<a href="#"></a> 
									by <a href="#">Admin</a> - 
									<span><i class="fa fa-clock-o"></i> '.$h->created_time.' wib</span>
								</p>
							</header>
						</article>
						
				';
			}
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  private function load_pilihan($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 8
		");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
				$hasil .= 
				'
      <div class="col-md-6">
        <article class="featured-small box-news">
          <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
          <img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="">
          </a>
          <header class="featured-header">
            <a class="category bgcolor2" href="#">Berita</a>
            <h2><a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></h2>
            <p class="simple-share">
              by <a href="#"><b>Admin</b></a> -  
              <span class="article-date">'.$h->created_time.' wib</span>
            </p>
          </header>
        </article>
      </div>
				';
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  private function load_slide(){
																		$web=$this->uut->namadomain(base_url());
																		$this->db->select("*");
																		$where_wisata = array(
																			'status' => 1,
																			'parent' => 0,
																			'domain' => $web
																			);
																		$this->db->where($where_wisata);
																		//$this->db->limit('7');
																		$this->db->order_by('slide_website.created_time', 'desc');
																		$query_wisata = $this->db->get('slide_website');
																		$a = 0;
																		foreach ($query_wisata->result() as $row_wisata)
																			{
																				$id_slide_website = $row_wisata->id_slide_website;
																				$keterangan = $row_wisata->keterangan;
																				$url_redirection = $row_wisata->url_redirection;
																				$a = $a+1;
																					if($a==1){
																						echo'
																							<div class="slider-item flex-active-slide">
																								<div class="row">
																								';
																										 echo
																										 '
																										<div class="col-md-8 omega">
																											<div class="featured-big">
																												<a href="'.$row_wisata->url_redirection.'" class="featured-href">
																													<img src="'.base_url().'media/upload/'.$row_wisata->file_name.'" alt="" draggable="false">
																													<div class="featured-header">
																														<h2>'.$row_wisata->keterangan.'</h2>
																													</div>
																												</a>
																											</div>
																										</div>
																										 ';
																									$aa=0;
																									$where1 = array(
																										'parent' => $row_wisata->id_slide_website
																										);
																									$this->db->where($where1);
																									$this->db->limit('2');
																									$this->db->order_by('slide_website.created_time', 'desc');
																									$query_attachment = $this->db->get('slide_website');
																									foreach ($query_attachment->result() as $row_attachment)
																										{
																											$url_redirection1=$row_attachment->url_redirection;
																											$aa = $aa+1;
																											if($aa==1){
																													echo
																													'
																														<div class="col-md-4 alpha">
																															<div class="featured-small featured-top">
																																<a href="'.$row_attachment->url_redirection.'" class="featured-href">
																																	<img src="'.base_url().'media/upload/'.$row_attachment->file_name.'" alt="" draggable="false">
																																	<div class="featured-header">
																																		<h2>'.$row_attachment->keterangan.'</h2>
																																	</div>
																																</a>
																															</div>
																														';
																											}
																											else{
																														echo'
																															<div class="featured-small">
																																<a href="'.$row_attachment->url_redirection.'" class="featured-href">
																																	<img src="'.base_url().'media/upload/'.$row_attachment->file_name.'" alt="" draggable="false">
																																	<div class="featured-header">
																																		<h2>'.$row_attachment->keterangan.'</h2>
																																	</div>
																																</a>
																															</div>
																														</div>
																												 ';
																											}
																										}
																						echo'
																							</div>
																						</div>
																						';
																					}
																					elseif($a==2){
																						echo'
																							<div class="slider-item">
																								<div class="row">
																								';
																										 echo
																										 '
																										<div class="col-md-8 omega">
																											<div class="featured-big">
																												<a href="'.$row_wisata->url_redirection.'" class="featured-href">
																													<img src="'.base_url().'media/upload/'.$row_wisata->file_name.'" alt="" draggable="false">
																													<div class="featured-header">
																														<h2>'.$row_wisata->keterangan.'</h2>
																													</div>
																												</a>
																											</div>
																										</div>
																										 ';
																									$aaa=0;
																									$where11 = array(
																										'parent' => $row_wisata->id_slide_website
																										);
																									$this->db->where($where11);
																									$this->db->limit('2');
																									$this->db->order_by('slide_website.created_time', 'desc');
																									$query_attachment1 = $this->db->get('slide_website');
																									foreach ($query_attachment1->result() as $row_attachment1)
																										{
																											$url_redirection2=$row_attachment1->url_redirection;
																											$aaa = $aaa+1;
																											if($aaa==1){
																													echo
																													'
																														<div class="col-md-4 alpha">
																															<div class="featured-small featured-top">
																																<a href="'.$row_attachment1->url_redirection.'" class="featured-href">
																																	<img src="'.base_url().'media/upload/'.$row_attachment1->file_name.'" alt="" draggable="false">
																																	<div class="featured-header">
																																		<h2>'.$row_attachment1->keterangan.'</h2>
																																	</div>
																																</a>
																															</div>
																														';
																											}
																											else{
																														echo'
																															<div class="featured-small">
																																<a href="'.$row_attachment1->url_redirection.'" class="featured-href">
																																	<img src="'.base_url().'media/upload/'.$row_attachment1->file_name.'" alt="" draggable="false">
																																	<div class="featured-header">
																																		<h2>'.$row_attachment1->keterangan.'</h2>
																																	</div>
																																</a>
																															</div>
																														</div>
																												 ';
																											}
																										}
																						echo'
																							</div>
																						</div>
																						';
																					}
																					else{
																						echo'
																							<div class="slider-item">
																								<div class="row">
																								';
																										 echo
																										 '
																										<div class="col-md-8 omega">
																											<div class="featured-big">
																												<a href="'.$row_wisata->url_redirection.'" class="featured-href">
																													<img src="'.base_url().'media/upload/'.$row_wisata->file_name.'" alt="" draggable="false">
																													<div class="featured-header">
																														<h2>'.$row_wisata->keterangan.'</h2>
																													</div>
																												</a>
																											</div>
																										</div>
																										 ';
																									$aaaa=0;
																									$where11 = array(
																										'parent' => $row_wisata->id_slide_website
																										);
																									$this->db->where($where11);
																									$this->db->limit('2');
																									$this->db->order_by('slide_website.created_time', 'desc');
																									$query_attachment2 = $this->db->get('slide_website');
																									foreach ($query_attachment2->result() as $row_attachment2)
																										{
																											$url_redirection3=$row_attachment2->url_redirection;
																											$aaaa = $aaaa+1;
																											if($aaaa==1){
																													echo
																													'
																														<div class="col-md-4 alpha">
																															<div class="featured-small featured-top">
																																<a href="'.$row_attachment2->url_redirection.'" class="featured-href">
																																	<img src="'.base_url().'media/upload/'.$row_attachment2->file_name.'" alt="" draggable="false">
																																	<div class="featured-header">
																																		<h2>'.$row_attachment2->keterangan.'</h2>
																																	</div>
																																</a>
																															</div>
																														';
																											}
																											else{
																														echo'
																															<div class="featured-small">
																																<a href="'.$row_attachment2->url_redirection.'" class="featured-href">
																																	<img src="'.base_url().'media/upload/'.$row_attachment2->file_name.'" alt="" draggable="false">
																																	<div class="featured-header">
																																		<h2>'.$row_attachment2->keterangan.'</h2>
																																	</div>
																																</a>
																															</div>
																														</div>
																												 ';
																											}
																										}
																						echo'
																							</div>
																						</div>
																						';
																					}
																				}
	}
	
  public function load_posting_kecamatan()
   {
			$web=$this->uut->namadomain(base_url());
			$this->db->select("
			posting.id_posting,
			posting.judul_posting,
			posting.created_time,
			posting.domain,
			data_kecamatan.kecamatan_nama,
			data_kecamatan.id_kecamatan,
			dasar_website.alamat,
			dasar_website.telpon,
			dasar_website.email,
			dasar_website.twitter,
			dasar_website.facebook,
			dasar_website.google,
			dasar_website.instagram,
			dasar_website.peta
			
			");
			$this->db->join('data_kecamatan', 'data_kecamatan.kecamatan_website = posting.domain');
			$this->db->join('dasar_website', 'dasar_website.domain = posting.domain');
			$where_wisata = array(
				'posting.status' => 1
				);
			$this->db->like('posting.judul_posting', 'Tentang Kami');
			$this->db->where($where_wisata);
			$this->db->limit('15');
			$this->db->order_by('data_kecamatan.id_kecamatan', 'asc');
			$query_wisata = $this->db->get('posting');
			$a = 0;
			foreach ($query_wisata->result() as $row_wisata)
				{
					$a = $a + 1;
					echo'<tr>';
					$healthy = array(" ", "#", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
					$yummy   = array("_", "", "", "", "","", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
					$judul_posting = str_replace($healthy, $yummy, $row_wisata->judul_posting);
					$profil_judul_posting = $row_wisata->judul_posting;
					echo'
						<td><h2><a target="_blank" href="https://'.$row_wisata->domain.'/postings/detail/'.$row_wisata->id_posting.'/'.$judul_posting.'.HTML" class="featured-href">'.$a.'. Profil Kecamatan '.$row_wisata->kecamatan_nama.'</a></h2></td>
					';
					echo'</tr>';
					echo'
						<tr>
							<td>
							alamat : '.$row_wisata->alamat.' <br />
							telpon : '.$row_wisata->telpon.' <br />
							email : '.$row_wisata->email.'<br />
							</td>
						</tr>
						';
						$w = $this->db->query("
						SELECT *, (select file_name from attachment where attachment.id_tabel=posting.id_posting limit 1) as file_name_attachment from posting
						where parent='".$row_wisata->id_posting."'
						and status = 1 
						order by created_time desc
						");
						$x = $w->num_rows();
						foreach($w->result() as $row_posting)
						{
					echo'<tr>';
							echo'
								<td>
								<img src="https://'.$row_wisata->domain.'/media/upload/'.$row_posting->file_name_attachment.'" alt=""/>
								<br />
								'.$row_posting->isi_posting.'
								</td>
								<td></td>
								 ';
					echo'</tr>';
						}
				}
   }
  public function load_organisasi_perangkat_daerah()
   {
			$web=$this->uut->namadomain(base_url());
			$this->db->select("
			*
			");
			$where_wisata = array(
				'skpd.status' => 1
				);
			$this->db->where($where_wisata);
			$this->db->order_by('skpd.kode_skpd', 'asc');
			$query_wisata = $this->db->get('skpd');
			$a = 0;
			foreach ($query_wisata->result() as $row_wisata)
				{
					$a = $a + 1;
					$healthy = array(" ", "#", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
					$yummy   = array("_", "", "", "", "","", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
					echo'
					<tr>
						<td>'.$a.'</td>
						<td><h3><a target="_blank" href="#" class="featured-href">'.$row_wisata->nama_skpd.'</a></h3></td>
						<td><h3>'.$row_wisata->keterangan.'</a></h3></td>
					</tr>';
				}
   }
	
  public function load_tentang_kami()
   {
			$web=$this->uut->namadomain(base_url());
			$this->db->select("
			posting.id_posting,
			posting.judul_posting,
			posting.isi_posting,
			posting.created_time,
			posting.domain
			
			");
			$where_wisata = array(
				'posting.status' => 1,
				'posting.domain' => $web
				);
			$this->db->like('posting.judul_posting', 'Tentang Kami');
			$this->db->where($where_wisata);
			$this->db->limit('15');
			$this->db->order_by('data_kecamatan.urut', 'asc');
			$query_wisata = $this->db->get('posting');
			$a = 0;
			foreach ($query_wisata->result() as $row_wisata)
				{
					$a = $a + 1;
					echo'<tr>';
					$healthy = array(" ", "#", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
					$yummy   = array("_", "", "", "", "","", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
					$judul_posting = str_replace($healthy, $yummy, $row_wisata->judul_posting);
					$profil_judul_posting = $row_wisata->judul_posting;
					echo'
						<td><h2><a target="_blank" href="https://'.$row_wisata->domain.'/postings/detail/'.$row_wisata->id_posting.'/'.$judul_posting.'.HTML" class="featured-href">'.$a.'. Profil Kecamatan '.$row_wisata->kecamatan_nama.'</a></h2></td>
					';
					echo'</tr>';
						$w = $this->db->query("
						SELECT *, (select file_name from attachment where attachment.id_tabel=posting.id_posting limit 1) as file_name_attachment from posting
						where parent='".$row_wisata->id_posting."'
						and status = 1 
						order by created_time desc
						");
						$x = $w->num_rows();
						foreach($w->result() as $row_posting)
						{
					echo'<tr>';
							echo'
								<td>
								<img src="https://'.$row_wisata->domain.'/media/upload/'.$row_posting->file_name_attachment.'" alt=""/>
								<br />
								'.$row_posting->isi_posting.'
								</td>
								<td></td>
								 ';
					echo'</tr>';
						}
				}
   }
	public function zb(){
    $web=$this->uut->namadomain(base_url());
      $where0 = array(
        'domain' => $web
        );
      $this->db->where($where0);
      $this->db->limit(1);
      $query0 = $this->db->get('dasar_website');
      foreach ($query0->result() as $row0)
        {
          $data['domain'] = $row0->domain;
          $data['alamat'] = $row0->alamat;
          $data['telpon'] = $row0->telpon;
          $data['email'] = $row0->email;
          $data['twitter'] = $row0->twitter;
          $data['facebook'] = $row0->facebook;
          $data['google'] = $row0->google;
          $data['instagram'] = $row0->instagram;
          $data['peta'] = $row0->peta;
          $data['keterangan'] = $row0->keterangan;
        }
        
      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->judul_komponen == 'Header' ){ //Header
            $data['Header'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
            $data['KolomKiriAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
            $data['KolomKananAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
            $data['KolomKiriBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
            $data['KolomPalingBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
            $data['KolomKananBawah'] = $row1->isi_komponen;
          }
          else{
          }
        }
      $r = 0;
      $p=0;
      $a = 0;
      $data['galery_berita'] = $this->option_posting_disparbud($h="", $a);
      $a = 0;
      $data['total'] = 10;
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['menu_mobile'] = $this->menu_mobile(0,$h="", $a);
        
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'welcome/welcome';
      $this->load->view('zb', $data); 
	}
}
