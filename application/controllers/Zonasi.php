<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Zonasi extends CI_Controller {

	function __construct(){
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Zonasi_model');
		} 
	public function index(){
    $web=$this->uut->namadomain(base_url());
      $where0 = array(
        'domain' => $web
        );
      $this->db->where($where0);
      $this->db->limit(1);
      $query0 = $this->db->get('dasar_website');
      foreach ($query0->result() as $row0)
        {
          $data['domain'] = $row0->domain;
          $data['alamat'] = $row0->alamat;
          $data['telpon'] = $row0->telpon;
          $data['email'] = $row0->email;
          $data['twitter'] = $row0->twitter;
          $data['facebook'] = $row0->facebook;
          $data['google'] = $row0->google;
          $data['instagram'] = $row0->instagram;
          $data['peta'] = $row0->peta;
          $data['keterangan'] = $row0->keterangan;
        }
        
        $where = array(
          'domain' => $web,
          'status' => 1
          );
        $d = $this->Zonasi_model->get_data_komponen($where);
        if(!$d){
          $data['judul_komponen'] = '';
          $data['isi_komponen'] = '';
          $data['status'] = '';
        }
        else{
          $data['judul_komponen'] = $d['judul_komponen'];
          $data['isi_komponen'] = $d['isi_komponen'];
          $data['status'] = $d['status'];
        }
      $data['judul'] = 'Selamat datang';
      // $data['main_view'] = 'welcome/welcome';
      $this->load->view('zonasi', $data); 
	}
}
