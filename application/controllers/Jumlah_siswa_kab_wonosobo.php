<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Jumlah_siswa_kab_wonosobo extends CI_Controller
 {
    
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    //$this->load->model('Jumlah_siswa_kab_wonosobo_model');
   }
  
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'jumlah_siswa_kab_wonosobo/home';
    $this->load->view('back_bone', $data);
   }
  public function smp()
   {
    $data['total'] = 10;
    $data['main_view'] = 'jumlah_siswa_kab_wonosobo/smp';
    $this->load->view('back_bone', $data);
   }
    public function smak()
   {
    $data['total'] = 10;
    $data['main_view'] = 'jumlah_siswa_kab_wonosobo/smak';
    $this->load->view('back_bone', $data);
   }
   public function pnf()
   {
    $data['total'] = 10;
    $data['main_view'] = 'jumlah_siswa_kab_wonosobo/pnf';
    $this->load->view('back_bone', $data);
   }
 }