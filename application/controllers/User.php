<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User extends CI_Controller
{
  function __construct(){
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('User_model');
    $this->load->model('Users_model');
    
  }
   
  public function index(){
    $data['main_view'] = 'users/users';
    $this->load->view('tes', $data);
  }
  
  function load_table_daftar_informasi_publik_pembantu()
	{
    $web=$this->uut->namadomain(base_url());
    $halaman    = $this->input->post('halaman');
    $limit    = $this->input->post('limit');
    $kata_kunci    = $this->input->post('kata_kunci');
    $id_kecamatan    = $this->input->post('option_kecamatan');
    $klasifikasi_informasi_publik_pembantu    = $this->input->post('klasifikasi_informasi_publik_pembantu');
		if($klasifikasi_informasi_publik_pembantu==''){
			$where_hak_akses="";
			$select_domain="";
		}
		elseif($klasifikasi_informasi_publik_pembantu=='web_desa'){
			$where_hak_akses="and users.hak_akses='".$this->input->post('klasifikasi_informasi_publik_pembantu')."'";
			$select_domain=",(select data_desa.desa_website from data_desa where data_desa.id_desa=users.id_desa) as data_website";
		}
		else{
			$where_hak_akses="and users.hak_akses='".$this->input->post('klasifikasi_informasi_publik_pembantu')."'";
			$select_domain=",(select data_skpd.skpd_website from data_skpd where data_skpd.id_skpd=users.id_skpd) as data_website";
		}
		if($id_kecamatan==''){
			$where_kecamatan="";
		}
		else{
			$where_kecamatan="and users.id_kecamatan='".$this->input->post('option_kecamatan')."'";
		}
		$w = $this->db->query("
		SELECT 
				*,
				(select skpd.nama_skpd from skpd where skpd.id_skpd=users.id_skpd) as nama_skpd,
				(select propinsi.nama_propinsi from propinsi where propinsi.id_propinsi=users.id_propinsi) as nama_propinsi,
				(select kabupaten.nama_kabupaten from kabupaten where kabupaten.id_kabupaten=users.id_kabupaten) as nama_kabupaten,
				(select kecamatan.nama_kecamatan from kecamatan where kecamatan.id_kecamatan=users.id_kecamatan) as nama_kecamatan,
				(select desa.nama_desa from desa where desa.id_desa=users.id_desa) as nama_desa
				".$select_domain."
		from users
		where users.status=1
    ".$where_hak_akses."
    ".$where_kecamatan."
		limit ".$limit."
		");
		
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$nama_skpd = $h->nama_skpd;
			$nama_kecamatan = $h->nama_kecamatan;
			$nama_desa = $h->nama_desa;
			echo '<tr style="padding: 2px;" id_users="'.$h->id_users.'" id="'.$h->id_users.'" >';
				echo '<td style="padding: 2px;">'.$nomor.'</td>';
        echo '<td style="padding: 2px;"><a target="_blank" href="https://'.$h->data_website.'" class="btn btn-flat">'.$h->data_website.'</td>';
				if($nama_skpd==null){
        echo '<td style="padding: 2px;">-</td>';
				}
				else{
        echo '<td style="padding: 2px;">'.$h->nama_skpd.'</td>';
				}
				if($nama_kecamatan==null){
        echo '<td style="padding: 2px;">-</td>';
				}
				else{
        echo '<td style="padding: 2px;">'.$h->nama_kecamatan.'</td>';
				}
				if($nama_desa==null){
        echo '<td style="padding: 2px;">-</td>';
				}
				else{
        echo '<td style="padding: 2px;">'.$h->nama_desa.'</td>';
				}
        echo '<td style="padding: 2px;"><a href="'.base_url().'ganti_password/by_user/'.$h->id_users.'" target="_blank">'.$h->user_name.'</a></td>';
        echo '<td style="padding: 2px;">'.$h->hak_akses.'</td>';
			echo '</tr>';
		}
	}
  function option_kecamatan(){
    $web=$this->uut->namadomain(base_url());
		if($web=='diskominfo.wonosobokab.go.id'){
			$where_id_kecamatan="";
		}
		else{
			$where_id_kecamatan="and data_kecamatan.id_kecamatan=".$this->session->userdata('id_kecamatan')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_kecamatan, kecamatan
		where data_kecamatan.status = 1
		and kecamatan.id_kecamatan=data_kecamatan.id_kecamatan
    ".$where_id_kecamatan."
		order by kecamatan.id_kecamatan
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_kecamatan.'">'.$h->nama_kecamatan.'</option>';
		}
	}
  function option_skpd(){
    $web=$this->uut->namadomain(base_url());
		if($web=='diskominfo.wonosobokab.go.id'){
			$where_id_skpd="";
		}
		else{
			$where_id_skpd="and data_skpd.id_skpd=".$this->session->userdata('id_skpd')."";
		}
		$w = $this->db->query("
		SELECT *
		from data_skpd, skpd
		where data_skpd.status != 99
		and skpd.id_skpd=data_skpd.id_skpd
    ".$where_id_skpd."
		order by skpd.id_skpd 
		");
		foreach($w->result() as $h)
		{
			echo '<option value="'.$h->id_skpd.'">'.$h->nama_skpd.'</option>';
		}
	}
  public function simpan_users()
   {
		$this->form_validation->set_rules('user_name', 'user_name', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		$this->form_validation->set_rules('hak_akses', 'hak_akses', 'required');
		$this->form_validation->set_rules('nip', 'nip', 'required');
    $this->form_validation->set_rules('nama', 'nama', 'required');
    $this->form_validation->set_rules('jabatan', 'jabatan', 'required');
    $this->form_validation->set_rules('golongan', 'golongan', 'required');
    $this->form_validation->set_rules('pangkat', 'pangkat', 'required');
		$this->form_validation->set_rules('id_skpd', 'id_skpd', 'required');
		$this->form_validation->set_rules('id_kecamatan', 'id_kecamatan', 'required');
		$this->form_validation->set_rules('id_desa', 'id_desa', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
			{
				$data_input = array(
				'user_name' => trim($this->input->post('user_name')),
				'password' => $this->bcrypt->hash_password($this->input->post('password')),
				'hak_akses' => trim($this->input->post('hak_akses')),
				'nip' => trim($this->input->post('nip')),
				'nama' => trim($this->input->post('nama')),
				'jabatan' => trim($this->input->post('jabatan')),
				'golongan' => trim($this->input->post('golongan')),
				'pangkat' => trim($this->input->post('pangkat')),
				'id_skpd' => trim($this->input->post('id_skpd')),
				'id_propinsi' => 1,
				'id_kabupaten' => 1,
				'id_kecamatan' => trim($this->input->post('id_kecamatan')),
				'id_desa' => trim($this->input->post('id_desa')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'last_login' => date('Y-m-d H:i:s'),
        'status' => 1

				);
      $table_name = 'users';
      $this->Users_model->simpan_users($data_input, $table_name);
     }
   }
   
	public function json_all_users(){
    $web=$this->uut->namadomain(base_url());
		$table = 'users';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
    ( select (desa.nama_desa) from desa where desa.id_desa=users.id_desa limit 1) as nama_desa,
    ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=users.id_kecamatan limit 1) as nama_kecamatan,
    ( select (skpd.nama_skpd) from skpd where skpd.id_skpd=users.id_skpd limit 1) as nama_skpd
    ";
    $where = array(
      'users.status !=' => 99
      );
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_users="'.$row->id_users.'" id="'.$row->id_users.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$this->Crud_model->dateBahasaIndo1($row->created_time).'</td>';
				echo '<td valign="top">'.$row->nama_skpd.'</td>';
				echo '<td valign="top">'.$row->nama_kecamatan.'</td>';
				echo '<td valign="top">'.$row->nama_desa.'</td>';
				echo '<td valign="top">'.$row->user_name.'</td>';
				// echo '<td valign="top">'.$row->password.'</td>';
				echo '<td valign="top">'.$row->hak_akses.'</td>';
				// echo '<td valign="top">'.$row->nama.'</td>';
				/*echo '<td style="padding: 2px;">';
						$wa = $this->db->query("
						SELECT *
						from lampiran_users
						where lampiran_users.id_tabel=".$row->id_users."
            and lampiran_users.table_name='users'
						");
						foreach($wa->result() as $ha){
							echo '<a href="'.base_url().'media/lampiran_users/'.$ha->file_name.'" class="blog-tag" target="_blank">'.$ha->keterangan.'</a><br />';
						}
        echo '</td>';*/
				echo '<td valign="top">
        <a href="'.base_url().'ganti_password/by_user/'.$row->id_users.'" class="badge bg-warning btn-sm" title data-original-title="Ganti Password"><i class="fa fa-key"></i> Ganti Password</a>
				<a href="#tab_form_users" data-toggle="tab" class="update_id_users badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax_users" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			/*echo '
      <tr>
        <td valign="top" colspan="7" style="text-align:right;">
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'users/cetak_users/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data KK</a>
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'users/cetak_semua_users/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Semua Warga</a>
          <a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'users/cetak_users_pekarangan/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-print"></i> Cetak Data Pemanfaatan Pekarangan Warga</a>
        </td>
      </tr>
      ';*/
          
	}
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($web=='diskominfo.wonosobokab.go.id'){
      $where0 = array(
        'users.status !=' => 99
        );
    }else{
      $where0 = array(
        'users.status !=' => 99,
        'users.id_kecamatan' => $this->session->userdata('id_kecamatan')
        );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('users');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function get_by_id(){
    $web=$this->uut->namadomain(base_url());
    $where    = array(
      'users.id_users' => $this->input->post('id_users')
      );
    $this->db->select("*");
    $this->db->where($where);
    $this->db->order_by('id_users');
    $result = $this->db->get('users');
    echo json_encode($result->result_array());
	}
  public function update_users(){
		$this->form_validation->set_rules('id_users', 'id_users', 'required');
		$this->form_validation->set_rules('nama_users', 'nama_users', 'required');
    if ($this->form_validation->run() == FALSE){
      echo 0;
		}
    else{
      $data_update = array(
			
        'nama_users' => trim($this->input->post('nama_users')),
        'jenis_kelamin' => trim($this->input->post('jenis_kelamin')),
        'tempat_lahir' => trim($this->input->post('tempat_lahir')),
        'tanggal_lahir' => trim($this->input->post('tanggal_lahir')),
        'berkebutuhan_khusus' => trim($this->input->post('berkebutuhan_khusus')),
        'berkebutuhan_khusus_fisik' => trim($this->input->post('berkebutuhan_khusus_fisik')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
								
			);
      $table_name  = 'users';
      $where       = array(
        'users.id_users' => trim($this->input->post('id_users'))
			);
      $this->User_model->update_users($data_update, $where, $table_name);
      echo 1;
		}
  }
  public function hapus(){
    $web=$this->uut->namadomain(base_url());
    $id_users = trim($this->input->post('id_users'));
    $where = array(
      'users.id_users' => $id_users
      );
    $this->db->from('users');
    $this->db->where($where);
    $a = $this->db->count_all_results();
    if($a == 0){
      echo 0;
    }
    else{
      $this->db->where($where);
      $this->db->delete('users');
      echo 1;
    }
  }
}