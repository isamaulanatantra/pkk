<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Users extends CI_Controller
{
  function __construct(){
  	parent::__construct();
		$this->load->library('Bcrypt');
		$this->load->library('Excel');
		$this->load->library('Uut');
		$this->load->library('Crud_model');
		$this->load->model('Users_model');
  }
  public function index(){
	  	$data['main_view'] = 'users/home';
	 	$this->load->view('back_bone', $data);
  }
  public function json_all_users()
  {
    $web=$this->uut->namadomain(base_url());
  $hak_akses  = trim($this->input->post('hak_akses'));
  $where   = array(
    'users.status !=' => 99,
    'data_skpd.skpd_website' => $web,
    );
  $a  = $this->Users_model->json_semua_users($where);
  $halaman  = $this->input->get('halaman');
  $limit   = 300;
  $start   = ($halaman - 1) * $limit;
  $fields  = "
    users.id_users,
    users.user_name,
    users.password,
    users.hak_akses,
    users.nip,
    users.nama,
    users.jabatan,
    users.golongan,
    users.pangkat,
    users.id_skpd,
    users.id_desa,
    users.id_kecamatan,
    users.id_kabupaten,
    users.id_propinsi,
    users.created_by,
    users.created_time,
    users.updated_by,
    users.updated_time,
    users.deleted_by,
    users.deleted_time,
    users.status,
    users.last_login,
    data_skpd.skpd_website,
    data_skpd.skpd_nama,
    
    ";
  $where   = array(
    'users.status !=' => 99,
    'data_skpd.skpd_website' => $web,
    );
  $order_by  = 'users.id_users';
  echo json_encode($this->Users_model->json_all_users($where, $limit, $start, $fields, $order_by));
  }
}