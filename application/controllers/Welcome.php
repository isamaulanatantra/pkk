<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
		{
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Uut');
    $this->load->library('Excel');
    $this->load->model('Crud_model');
    $this->load->model('Posting_model');
		}    
	
		public function index()
		{
		$web=$this->uut->namadomain(base_url());
		  $where0 = array(
			'domain' => $web
			);
		  $this->db->where($where0);
		  $this->db->limit(1);
		  $query0 = $this->db->get('dasar_website');
		  foreach ($query0->result() as $row0)
			{
			  $data['domain'] = $row0->domain;
			  $data['alamat'] = $row0->alamat;
			  $data['telpon'] = $row0->telpon;
			  $data['email'] = $row0->email;
			  $data['twitter'] = $row0->twitter;
			  $data['facebook'] = $row0->facebook;
			  $data['google'] = $row0->google;
			  $data['instagram'] = $row0->instagram;
			  $data['peta'] = $row0->peta;
			  $data['keterangan'] = $row0->keterangan;
			}
			
		  $where1 = array(
			'domain' => $web
			);
		  $this->db->where($where1);
		  $query1 = $this->db->get('komponen');
		  foreach ($query1->result() as $row1)
			{
			  if( $row1->judul_komponen == 'Header' ){ //Header
				$data['Header'] = $row1->isi_komponen;
			  }
			  else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
				$data['KolomKiriAtas'] = $row1->isi_komponen;
			  }
			  else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
				$data['KolomKananAtas'] = $row1->isi_komponen;
			  }
			  else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
				$data['KolomKiriBawah'] = $row1->isi_komponen;
			  }
			  else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
				$data['KolomPalingBawah'] = $row1->isi_komponen;
			  }
			  else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
				$data['KolomKananBawah'] = $row1->isi_komponen;
			  }
			  else{ 
			  }
			}
		  $r = 0;
		  $parents=6123;
		  $data['posting_pengumuman'] = $this->posting_pengumuman($parents,$q="", $r);
		  $a = 0;
		  $data['total'] = 10;
			
		if($web=='zb.wonosobokab.go.id'){
		  $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
		  $data['menu_mobile'] = $this->menu_mobile(0,$h="", $a);
				$data['galery_berita'] = $this->load_galery_wonosobo($h="", $a);
		  $data['judul'] = 'Selamat datang';
		  $data['main_view'] = 'demo/home';
		  $this->load->view('demo', $data);
		}
		elseif($web=='disparbud.wonosobokab.go.id'){
		  // $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
		  $data['menu_mobile'] = $this->menu_mobile(0,$h="", $a);
		  $data['galery_berita'] = $this->option_posting_disparbud($h="", $a);
		  $data['menu_atas'] = $this->menu_atas_disparbud(0,$h="", $a);
		  $data['judul'] = 'Selamat datang';
		  $data['main_view'] = 'welcome/welcome_disparbud';
		  $this->load->view('post', $data);
		}
		elseif($web=='pkk.wonosobokab.go.id'){
		  $data['menu_atas'] = $this->menu_atas_pkk(0,$h="", $a);
		  $data['menu_mobile'] = $this->menu_mobile_pkk(0,$h="", $a);
		  $data['galery_berita'] = $this->option_posting_terbarukan_pkk($h="", $a);
		  $data['judul'] = 'Selamat datang';
		  $data['main_view'] = 'welcome/welcome_pkk';
		  $this->load->view('pkk', $data);
		}
		else{
		  $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
		  $data['galery_berita'] = $this->option_posting_terbarukan($h="", $a);
		  $data['judul'] = 'Selamat datang';
		  if($web=='disdagkopukm.wonosobokab.go.id'){
			$data['main_view'] = 'welcome/welcome_disdag';
		  }
		  else{
			$data['main_view'] = 'welcome/welcome_page';
		  }
		  $this->load->view('welcome', $data); 
		}
		}
private function menu_atas($parent=NULL,$hasil, $a){
	$web=$this->uut->namadomain(base_url());
	$a = $a + 1;
	$w = $this->db->query("
	SELECT * from posting
	where posisi='menu_atas'
	and parent='".$parent."' 
	and status = 1 
	and tampil_menu_atas = 1 
	and domain='".$web."'
	order by urut
	");
	$x = $w->num_rows();
	if(($w->num_rows())>0)
	{
	if($parent == 0){
	if($web == 'zb.wonosobokab.go.id'){
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'website" class="fa-home ">HOME</a></li>
	'; 
	}
	elseif ($web == 'ppiddemo.wonosobokab.go.id'){
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'">HOME</a></li>
	<li class="parent">
	  <span class="">PPID </span>
	  <ul>
		<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid">PPID </a></span></li>
		<li> <span class=""> <a href="https://ppid.wonosobokab.go.id/ppid_pembantu">PPID Pembantu</a></span></li>
	  </ul>
	</li>
	';
	}
	else{
	$hasil .= '
	<ul id="hornavmenu" class="nav navbar-nav" >
	<li><a href="'.base_url().'">HOME</a></li>
	'; 
	}
	}
	else{
	$hasil .= '
	<ul>
	  ';
	  }
	  }
	  $nomor = 0;
	  foreach($w->result() as $h)
	  {
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
	  $r = $this->db->query("
	  SELECT * from posting
	  where parent='".$h->id_posting."' 
	  and status = 1 
	  and tampil_menu_atas = 1 
	  and domain='".$web."'
	  order by urut
	  ");
	  $xx = $r->num_rows();
	  $nomor = $nomor + 1;
	  if( $a > 1 ){
			if ($xx == 0){
				if($h->judul_posting == 'Data Rekap Kunjungan Pasien'){
				$hasil .= '
				<li><a href="'.base_url().'grafik/data_rekap_kunjungan_pasien"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Data Kunjungan Hari Ini'){
				$hasil .= '
				<li><a href="'.base_url().'grafik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Pengumuman'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Artikel'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'FAQ'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Sitemap'){
				$hasil .= '
				<li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Informasi Publik'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Data Penyakit'){
				$hasil .= '
				<li><a href="'.base_url().'grafik/data_penyakit"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}

				elseif($h->judul_posting == 'Data Pegawai'){
				$hasil .= '
				<li><a href="'.base_url().'grafik/data_pegawai"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}

				elseif($h->judul_posting == 'Pengaduan Masyarakat'){
				$hasil .= '
				<li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Permohonan Informasi Publik'){
				$hasil .= '
				<li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Permohonan Informasi'){
				$hasil .= '
				<li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'Berita'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				elseif($h->judul_posting == 'BERITA'){
				$hasil .= '
				<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
				else{
				$hasil .= '
				<li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
				}
			}
			else{
			$hasil .= '
			<li class="parent" > <span class="">'.$h->judul_posting.' </span>';
			} 
		}
		else{
			if($h->judul_posting == 'Berita'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'BERITA'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Artikel'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Pengumuman'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Sitemap'){
			$hasil .= '
			<li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Informasi Publik'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'FAQ'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Info'){
			$hasil .= '
			<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Pengaduan Masyarakat'){
				$hasil .= '
				<li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
			elseif($h->judul_posting == 'Permohonan Informasi Publik'){
				$hasil .= '
				<li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
			elseif($h->judul_posting == 'Permohonan Informasi'){
				$hasil .= '
				<li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
			else{
				if ($xx == 0){
					if($h->judul_posting == 'Berita'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'BERITA'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Data Rekap Kunjungan Pasien'){
					$hasil .= '
					<li><a href="http://180.250.150.76/simpus/charts/grafik_1b"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Data Penyakit'){
					$hasil .= '
					<li><a href="http://180.250.150.76/simpus/charts/grafik_1d"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Pengumuman'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Artikel'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Sitemap'){
					$hasil .= '
					<li><a href="'.base_url().'postings/sitemap/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'FAQ'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					elseif($h->judul_posting == 'Informasi Publik'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					else{
					$hasil .= '
					<li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span></a>';
					}
				}
				else{
					if($h->judul_posting == 'Pengumuman'){
					$hasil .= '
					<li><a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
					else{
					$hasil .= '
					<li class="parent" > <span class="">'.$h->judul_posting.' </span>';
					}
				}
			}
		}
		$hasil = $this->menu_atas($h->id_posting,$hasil, $a);
		$hasil .= '
	  </li>
	  ';
	  }
	  if(($w->num_rows)>0)
	  {
	  $hasil .= "
	</ul>
	";
	}
	else{
	}
	return $hasil;
}
private function menu_atas_pkk($parent=NULL,$hasil, $a){
	$web=$this->uut->namadomain(base_url());
	$a = $a + 1;
	$w = $this->db->query("
	SELECT * from posting
	where posisi='menu_atas'
	and parent='".$parent."' 
	and status = 1 
	and tampil_menu_atas = 1 
	and domain='".$web."'
	order by urut
	");
	$x = $w->num_rows();
	if(($w->num_rows())>0)
	{
	if($parent == 0){
      $hasil .= '
      <ul id="responsivemenu">
      <li class="active"><a href="'.base_url().'"><i class="icon-home homeicon"></i><span class="showmobile">Home</span></a></li>
      ';
	}
	else{
	$hasil .= '
	<ul style="display: none;">
	  ';
	  }
	  }
	  $nomor = 0;
	  foreach($w->result() as $h)
	  {
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
	  $r = $this->db->query("
	  SELECT * from posting
	  where parent='".$h->id_posting."' 
	  and status = 1 
	  and tampil_menu_atas = 1 
	  and domain='".$web."'
	  order by urut
	  ");
	  $xx = $r->num_rows();
	  $nomor = $nomor + 1;
	  if( $a > 1 ){
			if ($xx == 0){
        $hasil .= '
          <li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> '.$h->judul_posting.' </a>
        ';
			}
			else{
			$hasil .= '
        <li> <a href="">'.$h->judul_posting.' </a>
      ';
			} 
		}
		else{
      if ($xx == 0){
        if($h->judul_posting == 'SIM PKK'){
          $hasil .= '
            <li><a href="'.base_url().'login"> '.$h->judul_posting.' </a>
          ';
        }else{
          $hasil .= '
          <li><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"> '.$h->judul_posting.' </a>
          ';
        }
      }
      else{
        $hasil .= '
          <li> <a href="">'.$h->judul_posting.' </a>
        ';
      }
		}
		$hasil = $this->menu_atas_pkk($h->id_posting,$hasil, $a);
		$hasil .= '
	  </li>
	  ';
	  }
	  if(($w->num_rows)>0)
	  {
	  $hasil .= "
	</ul>
	";
	}
	else{
	}
	return $hasil;
}
  private function menu_mobile($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
					$hasil .= '<ul> <li><a href="'.base_url().'" role="button" aria-expanded="false">HOME</a></li>';
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
        $hasil .= '
          <li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
        }
			else{
				if($h->judul_posting == 'Berita'){
					$hasil .= '
					  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
				}
				elseif($h->judul_posting == 'Pengumuman'){
					$hasil .= '
					  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
				}
				elseif($h->judul_posting == 'Pengaduan Masyarakat'){
					$hasil .= '
				  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				elseif($h->judul_posting == 'Permohonan Informasi Publik'){
					$hasil .= '
				  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				elseif($h->judul_posting == 'Permohonan Informasi'){
					$hasil .= '
				  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				else{
				  if ($xx == 0){
					if($h->judul_posting == 'Berita'){
						$hasil .= '
						  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengumuman'){
						$hasil .= '
							<li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengaduan Masyarakat'){
						$hasil .= '
					  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi Publik'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
				  	else{
					$hasil .= '
					  <li><a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span></a>';
					  }
				  }
				  else{
					if($h->judul_posting == 'Berita'){
						$hasil .= '
						  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengumuman'){
						$hasil .= '
							<li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengaduan Masyarakat'){
						$hasil .= '
					  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi Publik'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
				  	else{
						$hasil .= '
						  <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
						} 
					  }
				}
      }
      
			$hasil = $this->menu_mobile($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  private function menu_mobile_pkk($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
					$hasil .= '<ul> <li><a href="'.base_url().'" role="button" aria-expanded="false">HOME</a></li>';
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			if( $a > 1 ){
        $hasil .= '
          <li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
        }
			else{
				if($h->judul_posting == 'Berita'){
					$hasil .= '
					  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
				}
				elseif($h->judul_posting == 'Pengumuman'){
					$hasil .= '
					  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
				}
				elseif($h->judul_posting == 'Pengaduan Masyarakat'){
					$hasil .= '
				  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				elseif($h->judul_posting == 'Permohonan Informasi Publik'){
					$hasil .= '
				  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				elseif($h->judul_posting == 'Permohonan Informasi'){
					$hasil .= '
				  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
					';
					}
				else{
				  if ($xx == 0){
					if($h->judul_posting == 'Berita'){
						$hasil .= '
						  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengumuman'){
						$hasil .= '
							<li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengaduan Masyarakat'){
						$hasil .= '
					  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi Publik'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
				  	else{
					$hasil .= '
					  <li><a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span></a>';
					  }
				  }
				  else{
					if($h->judul_posting == 'Berita'){
						$hasil .= '
						  <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengumuman'){
						$hasil .= '
							<li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML" role="button" aria-expanded="false"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
					}
					elseif($h->judul_posting == 'Pengaduan Masyarakat'){
						$hasil .= '
					  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi Publik'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
					elseif($h->judul_posting == 'Permohonan Informasi'){
						$hasil .= '
					  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
						';
						}
				  	else{
						$hasil .= '
						  <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
						} 
					  }
				}
      }
      
			$hasil = $this->menu_mobile_pkk($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
  private function menu_atasa2($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
    and tampil_menu = 1
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
        if($web == 'zb.wonosobokab.go.id'){
        $hasil .= '<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'website" class="fa-home ">HOME</a></li>'; 
        }
        else{
        $hasil .= '
        <ul>
        <li class="home"><a href="'.base_url().'">Home</a></li>
        '; 
        }
      }
      else{
			$hasil .= '
        <ul>
      ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
    $r = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
		and status = 1 
    and domain='".$web."'
    and tampil_menu_atas = 1
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
      if($h->judul_posting == 'Berita'){
          $hasil .= '
            <li> <a href="'.base_url().'postings/galeri/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a>
          ';
      }
      else{
        if ($xx == 0){
          $hasil .= '
            <li> <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a>
           ';
        }
        else{
          $hasil .= '
            <li> <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a>
           ';
        }
      }
      
			$hasil = $this->menu_atas($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0){
			$hasil .= '</ul>';
		}
    else{
      
    }
		
		return $hasil;
	}
  
  public function get_attachment_by_id_posting($id_posting)
   {
     $where = array(
						'id_tabel' => $id_posting
						);
    $d = $this->Posting_model->get_attachment_by_id_posting($where);
    if(!$d)
          {
						return 'blankgambar.jpg';
          }
        else
          {
						return $d['file_name'];
          }
   } 
  
  private function posting_berita($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where posting.status = 1 
		and posting.parent = '".$parent."'
		and posting.highlight = 1 
    and domain='".$web."'
    order by urut
    limit 6
		");
		$xx = $w->num_rows();
		if(($w->num_rows())>0){
			$hasils .= '
                    <div class="blog-tags">
                  ';
		}
		$nomor = 0;
		foreach($w->result() as $h){
      $nomor = $nomor + 1;
      $rs = $this->db->query("
      SELECT * from posting
      where parent='".$h->id_posting."'
      and domain='".$web."'
      order by urut
      limit 6
      ");
			$nomor = $nomor + 1;
      $xx = $rs->num_rows();
			if( $r > 1 ){
        $hasils .= '
              <ul class="posts-list margin-top-1">
                <li>
                  <div class="recent-post">
                      <a href="">
                          <img class="pull-left" src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="thumb1" style="width:54px">
                      </a>
                      <a href="#" class="posts-list-title">'.$h->judul_posting.'</a>
                      <br>
                      <span class="recent-post-date">
                          July 30, 2013
                      </span>
                  </div>
                  <div class="clearfix"></div>
                </li>
              </ul>
        ';
        }
			else{ 
        if ($xx == 0){
        $hasils .= '
          <ul class="blog-tags">
            <li> 
              <a class="blog-tag" href="">'.$h->judul_posting.'</a>
            </li> 
          </ul>
        ';
        }
        else{
        $hasils .= '
          <ul class="blog-tags">
            <li> 
              <a class="blog-tag" href="">'.$h->judul_posting.'</a>
            </li> 
          </ul>
        ';
        }
      }
      $hasils = $this->posting_berita($h->id_posting,$hasils, $r);
		}
		if(($w->num_rows)>0){
			$hasils .= '</div>';
		}
    else{
    }
		return $hasils;
	}
  private function posting_pengumuman($parents,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where parent='".$parents."' 
		and posting.status = 1 
		and posting.highlight = 1 
    and domain='".$web."'
    order by urut
    limit 6
		");
		$xx = $w->num_rows();
		if(($w->num_rows())>0){
			$hasils .= '
                    <div class="blog-tags">
                  ';
		}
		$nomor = 0;
		foreach($w->result() as $h){
      $nomor = $nomor + 1;
      $rs = $this->db->query("
      SELECT * from posting
      where parent='".$h->id_posting."'
      and domain='".$web."'
    order by urut
      limit 6
      ");
			$nomor = $nomor + 1;
      $xx = $rs->num_rows();
			if( $r > 1 ){
        $hasils .= '
              <ul class="posts-list margin-top-1">
                <li>
                  <div class="recent-post">
                      <a href="">
                          <img class="pull-left" src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="thumb1" style="width:54px">
                      </a>
                      <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML" class="posts-list-title">'.$h->judul_posting.'</a>
                      <br>
                      <span class="recent-post-date">'.$h->created_time.'</span>
                  </div>
                  <div class="clearfix"></div>
                </li>
              </ul>
        ';
        }
			else{ 
        if ($xx == 0){
        $hasils .= '
          <ul class="blog-tags">
            <li> 
              <a class="blog-tag" href="">'.$h->judul_posting.'</a>
            </li> 
          </ul>
        ';
        }
        else{
        $hasils .= '
          <ul class="blog-tags">
            <li> 
              <a class="blog-tag" href="">'.$h->judul_posting.'</a>
            </li> 
          </ul>
        ';
        }
      }
      $hasils = $this->posting_pengumuman($h->id_posting,$hasils, $r);
		}
		if(($w->num_rows)>0){
			$hasils .= '</div>';
		}
    else{
    }
		return $hasils;
	}
  
  private function posting_highlight($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where parent='".$parent."' 
		and posting.status = 1 
		and posting.highlight = 1 
    and domain='".$web."'
    order by urut
    limit 5
		");
		$x = $w->num_rows();
		if(($w->num_rows())>0){
			$hasils .= '
                    <div class="blog-tags">
                  ';
		}
		$nomor = 0;
		foreach($w->result() as $h){
      $nomor = $nomor + 1;
      $rs = $this->db->query("
      SELECT * from posting
      where parent='".$h->id_posting."'
      and domain='".$web."'
    order by urut
      limit 5
      ");
			$nomor = $nomor + 1;
      $xx = $rs->num_rows();
			if( $r > 1 ){
        $hasils .= '
              <ul class="posts-list margin-top-1">
                <li>
                  <div class="recent-post">
                      <a href="">
                          <img class="pull-left" src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="thumb1" style="width:54px">
                      </a>
                      <a href="#" class="posts-list-title">'.$h->judul_posting.'</a>
                      <br>
                      <span class="recent-post-date">
                          July 30, 2013
                      </span>
                  </div>
                  <div class="clearfix"></div>
                </li>
              </ul>
        ';
        }
			else{ 
        if ($xx == 0){
        $hasils .= '
      <ul class="blog-tags">
        <li> 
          <a class="blog-tag" href="">'.$h->judul_posting.'</a>
        </li> 
      </ul>
        ';
        }
        else{
        $hasils .= '
      <ul class="blog-tags">
        <li> 
          <a class="blog-tag" href="">'.$h->judul_posting.'</a>
        </li> 
      </ul>
        ';
        } 
      }
      $hasils = $this->posting_highlight($h->id_posting,$hasils, $r);
		}
		if(($w->num_rows)>0){
			$hasils .= '
                    </div>
      ';
		}
    else{
    }
		return $hasils;
	}
		
  private function posting_highlight1($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where parent='".$parent."' 
		and posting.status = 1 
    and domain='".$web."'
    order by urut desc
    limit 6
		");
		$x = $w->num_rows();
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    $nomor = $nomor + 1;
    $rs = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."'
    and domain='".$web."'
    limit 6
		");
		$xx = $rs->num_rows();
      if( $h->highlight == 1 ){
       
            $hasils .= 
            '
            <li class="portfolio-item col-sm-4 col-xs-6 margin-bottom-40">
                <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$h->judul_posting.'">
                    <figure class="animate fadeIn">
                        <img alt="'.$h->id_posting.'" src="'.base_url().'media/upload/'.$this->get_attachment_by_id_posting($h->id_posting).'">
                        <figcaption>
                            <h3>'.$h->judul_posting.'</h3>
                            <span>'.substr(strip_tags($h->isi_posting), 0, 100).'</span>
                        </figcaption>
                    </figure>
                </a>
            </li>
            ';
          
      }        
			
      $hasils = $this->posting_highlight($h->id_posting,$hasils, $r);
		}
		return $hasils;
	}
		
  private function posting_daftar($parent=0,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where parent='".$parent."' 
		and posisi = 'menu_atas'
		and status = 1 
    and domain='".$web."'
    order by posisi, urut
		");
		
		if(($w->num_rows())>0)
		{
			//$hasil .= "<tr>";
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$hasil .= '<tr id_posting="'.$h->id_posting.'" id="'.$h->id_posting.'" >';
      if( $a == 1 ){
        $hasil .= '<td style="padding: 2px 2px 2px 2px ;"> '.$h->judul_posting.' </td>';
        }
			else if($a == 2){
        $hasil .= '<td style="padding: 2px 2px 2px '.($a * 20).'px ;"> <a target="_blank" href="'.base_url().'postings/categories/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.' </a> </td>';
        }
			else{
        }
			$hasil .= '</tr>';
			$hasil = $this->posting_daftar($h->id_posting,$hasil, $a);
		}
		if(($w->num_rows)>0)
		{
			//$hasil .= "</tr>";
		}
		
		return $hasil;
	}
  private function posting_galery_berita($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where parent='".$parent."' 
		and posting.status = 1 
    and domain='".$web."'
    order by created_time desc
    limit 5
    
		");
		$x = $w->num_rows();
		$nomor = 0;
		foreach($w->result() as $h)
		{
    $nomor = $nomor + 1;
    $rs = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."'
    and domain='".$web."'
    order by created_time desc
    limit 5
		");
		$xx = $rs->num_rows();
      if( $h->highlight == 1 ){
            $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
            $yummy   = array("_","","","","","","","","","","","","","");
            $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
            $hasils .= 
            '
                <div class="col-md-4 col-sm-6 gallery-item">
                  <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" data-rel="fancybox-button" title="'.$h->judul_posting.'" class="fancybox-button">
                    <img alt="" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="'.$h->judul_posting.'" class="img-responsive">
                  </a> 
                  <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="posts-list-title">'.substr(strip_tags($h->judul_posting), 0, 100).'</a>
                                    <div class="clearfix"></div>
                </div>
            ';
      }        
			
      $hasils = $this->posting_galery_berita($h->id_posting,$hasils, $r);
		}
		return $hasils;
	}
  private function posting_highlight_daftar($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
		$r = $r + 1;
		$w = $this->db->query("
    
    SELECT * from posting
		where posting.parent='".$parent."' 
		and posting.status = 1 
    and posting.domain='".$web."'
    order by posting.urut 
    limit 5
    
		");
		$x = $w->num_rows();
		$nomor = 0;
		foreach($w->result() as $h)
		{
    $nomor = $nomor + 1;
      if( $h->highlight == 1 ){
            $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
            $yummy   = array("_","","","","","","","","","","","","","");
            $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
            $hasils .= 
            '
                  <div class="row">
                    <div class="col-md-4 col-sm-4">
                      <!-- BEGIN CAROUSEL -->            
                      <div class="front-carousel">
                        <div class="carousel slide" id="myCarousel">
                          <!-- Carousel items -->
                          <div class="carousel-inner">
                            <div class="item active">
                              <img alt="" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'">
                            </div>
                          </div>
                          <!-- Carousel nav -->
                          <a data-slide="prev" href="#myCarousel" class="carousel-control left">
                            <i class="fa fa-angle-left"></i>
                          </a>
                          <a data-slide="next" href="#myCarousel" class="carousel-control right">
                            <i class="fa fa-angle-right"></i>
                          </a>
                        </div>                
                      </div>
                      <!-- END CAROUSEL -->             
                    </div>
                    <div class="col-md-8 col-sm-8">
                      <h2><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></h2>
                      <ul class="blog-info">
                        <li><i class="fa fa-calendar"></i> '.$h->created_time.'</li>
                        <li><i class="fa fa-comments"></i> 0</li>
                        <li><i class="fa fa-tags"></i> '.$h->kata_kunci.'</li>
                      </ul>
                      <p>'.substr(strip_tags($h->isi_posting), 0, 200).'</p>
                      <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="more">Baca Selengkapnya <i class="icon-angle-right"></i></a>
                    </div>
                  </div>
                  <hr class="blog-post-sep">
            ';
      }
      $hasils = $this->posting_highlight_daftar($h->id_posting,$hasils, $r);
		}
		return $hasils;
	}
	
  function posting_terbarukan()
	{
		$a = 0;
		echo $this->option_posting_terbarukan($h="", $a);
	}
  private function option_posting_terbarukan($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 10
		");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
			$hasil .= 
			'
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<!-- BEGIN CAROUSEL -->            
								<div class="front-carousel">
									<div class="carousel slide" id="myCarousel">
										<!-- Carousel items -->
										<div class="carousel-inner">
											<div class="item active">
												<img alt="" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'">
											</div>
										</div>
										<!-- Carousel nav -->
										<a data-slide="prev" href="#myCarousel" class="carousel-control left">
											<i class="fa fa-angle-left"></i>
										</a>
										<a data-slide="next" href="#myCarousel" class="carousel-control right">
											<i class="fa fa-angle-right"></i>
										</a>
									</div>                
								</div>
								<!-- END CAROUSEL -->             
							</div>
							<div class="col-md-8 col-sm-8">
								<h2><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></h2>
								<ul class="blog-info">
									<li><i class="fa fa-calendar"></i> '.$created_time.'</li>
								</ul>
								<p>'.substr(strip_tags($h->isi_posting), 0, 200).'</p>
								<a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="more">Baca Selengkapnya <i class="icon-angle-right"></i></a>
							</div>
						</div>
						<hr class="blog-post-sep">
			';
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  private function option_posting_terbarukan_pkk($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 9
		");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
			$hasil .= 
			'
      
          <div class="c4" style="text-align:center;">
            <div class="featured-projects">
              <div class="featured-projects-image"><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><img class="imgOpa teamimage" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="Image"></a></div>
              <div class="featured-projects-content">
                <span class="hirefor"><a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a></span>
              </div>
            </div>
          </div>
			';
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
	public function dashboard()
	{
      $a = 0;
    $web=$this->uut->namadomain(base_url());
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
      $data['galery_berita'] = $this->option_posting_terbarukan($h="", $a);
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'welcome/dashboard_disdag';
      $this->load->view('dashboard', $data); 
  }
      
	public function vidio_intro()
	{
    $web=$this->uut->namadomain(base_url());
      $where0 = array(
        'domain' => $web
        );
      $this->db->where($where0);
      $this->db->limit(1);
      $query0 = $this->db->get('dasar_website');
      foreach ($query0->result() as $row0)
        {
          $data['domain'] = $row0->domain;
          $data['alamat'] = $row0->alamat;
          $data['telpon'] = $row0->telpon;
          $data['email'] = $row0->email;
          $data['twitter'] = $row0->twitter;
          $data['facebook'] = $row0->facebook;
          $data['google'] = $row0->google;
          $data['instagram'] = $row0->instagram;
          $data['peta'] = $row0->peta;
          $data['keterangan'] = $row0->keterangan;
        }
        
      $where1 = array(
        'domain' => $web
        );
      $this->db->where($where1);
      $query1 = $this->db->get('komponen');
      foreach ($query1->result() as $row1)
        {
          if( $row1->judul_komponen == 'Header' ){ //Header
            $data['Header'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
            $data['KolomKiriAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
            $data['KolomKananAtas'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
            $data['KolomKiriBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
            $data['KolomPalingBawah'] = $row1->isi_komponen;
          }
          else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
            $data['KolomKananBawah'] = $row1->isi_komponen;
          }
          else{ 
          }
        }
      $r = 0;
      //$parent=73;
      $parents=6123;
      //$data['posting_berita'] = $this->posting_berita($parent,$q="", $r);
      $data['posting_pengumuman'] = $this->posting_pengumuman($parents,$q="", $r);
      $a = 0;
      $data['posting_marquee'] = $this->posting_marquee(0,$h="", $a);
      $data['posting_daftar'] = $this->posting_daftar(0,$h="", $a);
      //$data['posting_teksjalan'] = $this->posting_teksjalan($a);
      $data['skpd'] = $this->skpd(0,$h="", $a);
      // $data['website_desa'] = $this->website_desa();
      $data['skpd_opd'] = $this->skpd_opd(0,$h="", $a);
      $data['skpd_kecamatan'] = $this->skpd_kecamatan(0,$h="", $a);
      $data['skpd_desa'] = $this->skpd_desa(0,$h="", $a);
      $data['total'] = 10;
      $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
        
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'movie/home';
      $this->load->view('movie', $data);
	}

  public function faqs()
   {
    $web=$this->uut->namadomain(base_url());
    $a = 0;
    $r = $this->uri->segment(5);
    $p=$this->uri->segment(3);
    $data['isi_halaman'] = $this->posting_faqs($p,$q="", $r);
    $data['menu_atas'] = $this->menu_atas(0,$h="", $a);
    $data['menu_atas_posting'] = $this->menu_atas_posting(0,$h="", $a);
    $data['menu_kiri'] = $this->posting(0,$h="", $a);
    $data['welcome1'] = $this->get_komponen('welcome1');
    $data['welcome2'] = $this->get_komponen('welcome2');
    $data['disclaimer'] = $this->get_komponen('disclaimer');
    $data['data_link_bawah'] = $this->get_komponen('data_link_bawah');
    $data['kontak_detail'] = $this->get_komponen('kontak_detail');
    $data['slide_show'] = $this->get_komponen('slide_show');
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan'] = $row0->keterangan;
      }

    $where = array(
						'id_posting' => $this->uri->segment(3),
            'status' => 1
						);
    $d = $this->Posting_model->get_data($where);
    if(!$d)
          {
						$data['judul_posting'] = '';
						$data['isi_posting'] = '';
						$data['kata_kunci'] = '';
						$data['gambar'] = 'blankgambar.jpg';
          }
        else
          {
						$data['judul_posting'] = $d['judul_posting'];
						$data['isi_posting'] = ''.$d['isi_posting'].'<br><em>('.$d['pembuat'].'/'.$d['created_time'].'/'.$d['pengedit'].'/'.$d['updated_time'].')</em>';
						$data['kata_kunci'] = $d['kata_kunci'];
            $data['gambar'] = $this->get_attachment_by_id_posting($d['id_posting']);
          }

    //PAGINATION
    $page =  $r;
    $noPage =  $page;
    $showPage = 0;
    $s = $this->db->query("SELECT COUNT(*) as jumlah_data from posting where parent='".$this->uri->segment(3)."' ");
    $jumPage = $s->row("jumlah_data")/10;

    $url = 'https://'.$web.'/postings/faqs/'.$this->uri->segment(3).'/'.$this->uri->segment(4).'';

    $pagination_top = '
    <div class="text-right">
      <ul class="pagination">
    ';
    $pagination_bottom = '
    <div class="text-center">
      <ul class="pagination">
    ';

    //menampilkan link halaman awal
    if ($page != 1 || empty($page)) { $pagination_bottom .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'">&laquo;&laquo;</a></li>'; }
 
    // menampilkan link previous
 
    if ($page > 1) { $pagination_bottom .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($page-1).'">&laquo;</a></li>'; }
 
    // memunculkan nomor halaman dan linknya
    for($page = 1; $page <= $jumPage; $page++)
    {
         if ((($page >= $noPage - 3) && ($page <= $noPage + 3)) || ($page == 1) || ($page == $jumPage)) 
         {   
            if (($showPage == 1) && ($page != 2))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top.= '<li><a href="#">...</a></li>'; }
            if (($showPage != ($jumPage - 1)) && ($page == $jumPage))  { $pagination_bottom .= '<li><a href="#">...</a></li>';$pagination_top .= '<li><a href="#">...</a></li>'; }
            if ($page == $r) { $pagination_bottom .= '<li class="active"><a href="#">'.$page.'</a></li>';$pagination_top.= '<li class="active"><a href="#">'.$page.'</a></li>'; }
            else { $pagination_bottom .= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>';$pagination_top.= '<li><a href="'.$url.'/'.$page.'">'.$page.'</a></li>'; }
            $showPage = $page;          
         }
    }
 
    // menampilkan link next
 
    if ($noPage < $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.($noPage+1).'">&gt;</a></li>'; }
 
    //menampilkan link halaman akhir
    if ($noPage != $jumPage) { $pagination_bottom .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>';$pagination_top .= '<li><a href="'.$url.'/'.$jumPage.'">&gt;&gt;</a></li>'; }
 
    $pagination_top .= '
      </ul>
    </div>
    ';
    $pagination_bottom .= '
      </ul>
    </div>
    ';
    $where1 = array(
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('komponen');
    foreach ($query1->result() as $row1)
      {
        if( $row1->judul_komponen == 'Header' ){ //Header
          $data['Header'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Atas' ){ //Kolom Kiri Atas
          $data['KolomKiriAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Atas' ){ //Kolom Kanan Atas
          $data['KolomKananAtas'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kiri Bawah' ){ //Kolom Kiri Bawah
          $data['KolomKiriBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Paling Bawah' ){ //Kolom Paling Bawah
          $data['KolomPalingBawah'] = $row1->isi_komponen;
        }
        else if( $row1->judul_komponen == 'Kolom Kanan Bawah' ){ //Kolom Kanan Bawah
          $data['KolomKananBawah'] = $row1->isi_komponen;
        }
        else{ 
        }
      }
    $data['pagination_top'] = $pagination_top;
    $data['pagination_bottom'] = $pagination_bottom;
    $data['main_view'] = 'postings/faqs';
    $this->load->view('halaman', $data);
    // $this->load->view('postings', $data);
    // $this->load->view('backbone_postings', $data);
   }
   
  private function posting_faqs($parent,$hasils, $r){
    $web=$this->uut->namadomain(base_url());
    if(empty($r) || $r == 1 || $r < 1)
    {
       $offset = 0;
    }elseif($r > 1){
      $offset = ($r*10);
    }

		$w = $this->db->query("
		SELECT * from posting
		where parent='".$parent."' 
    and posisi='menu_atas'
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",10
		");
		$x = $w->num_rows();
		$nomor = 0;

		foreach($w->result() as $h)
		{ 
    
    $rs = $this->db->query("
		SELECT * from posting
		where parent='".$h->id_posting."' 
    and posisi='menu_atas'
		and status = 1 
    and domain='".$web."'
    order by created_time desc
    limit ".$offset.",12
		");
		$xx = $rs->num_rows();

    $hasils = $this->posting_category($h->id_posting,$hasils, $r);
			if ($xx == 0){
                                $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
                                $yummy   = array("_","","","","","","","","","","","","","");
                                $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
            $hasils .= 
            '
                        <div class="media">                    
                          <a href="javascript:;" class="pull-left">
                          <img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="" class="media-object" alt="'.$h->judul_posting.'">
                          </a>
                          <div class="media-body">
                            <h4 class="media-heading">'.$h->judul_posting.' <span>Kemarin / <a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">Baca Selengkapnya</a></span></h4>
                            <p>'.substr(strip_tags($h->isi_posting), 0, 100).'</p>
                          </div>
                        </div>
            ';
            }
		}

		return $hasils;
	}
    
  private function option_posting_disparbud($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.parent != 0
		and posting.domain = '".$web."'
		order by created_time desc
    limit 10
		");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
				$hasil .= 
				'
						<article class="simple-post clearfix">
							<div class="simple-thumb">
								<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
								<img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt=""/>
								</a>
							</div>
							<header>
								<h2>
									<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a>
								</h2>
								<p class="simple-share">
									<span><i class="fa fa-clock-o"></i> '.$created_time.'</span>
								</p>
							</header>
						</article>
						
				';/*
			if( $nomor == 1 ){
				$hasil .= 
				'
          <article class="news-block">
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="overlay-link">
              <figure class="image-overlay">
                <img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" width="870" height="500" alt=""/>
              </figure>
            </a>
            <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" class="category">'.$h->judul_posting.'</a>
            <div class="news-details">
              <h2 class="news-title">
                <a href="#">
                </a>
              </h2>
              <p>
              <p style="text-align:justify"><span style="font-size:14px">'.substr(strip_tags($h->isi_posting), 0, 200).'</span></p>
              <p class="simple-share">
                <span class="article-date"><i class="fa fa-clock-o"></i> '.$created_time.'</span>
              </p>
            </div>
          </article>
				';
			}
			else{
				$hasil .= 
				'
						<article class="simple-post clearfix">
							<div class="simple-thumb">
								<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
								<img src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt=""/>
								</a>
							</div>
							<header>
								<h2>
									<a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">'.$h->judul_posting.'</a>
								</h2>
								<p class="simple-share">
									<span><i class="fa fa-clock-o"></i> '.$created_time.'</span>
								</p>
							</header>
						</article>
						
				';
			}*/
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
	
  private function load_galery_wonosobo($hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT *
		from posting
		where posting.status = 1 
		and posting.highlight = 1
		and posting.domain = '".$web."'
		order by posting.created_time desc
    limit 9
		");
		if(($w->num_rows())>0){
		} 
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			$yummy   = array("_","","","","","","","","","","","","","");
			$newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			$created_time = ''.$this->Crud_model->dateBahasaIndo1($h->created_time).'';
				$hasil .= 
				'
											<div class="col-lg-4">
												<a class="portofolio-item" href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML">
													<img class="img-fluid" src="'.base_url().'media/upload/s_'.$this->get_attachment_by_id_posting($h->id_posting).'" alt="">
												</a>
											</div>
						
				';
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  private function menu_atas_disparbud($parent=NULL,$hasil, $a){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("
		SELECT * from posting
									
		where posisi='menu_atas'
    and parent='".$parent."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$x = $w->num_rows();
    
		if(($w->num_rows())>0)
		{
      if($parent == 0){
				$hasil .= '<ul id="hornavmenu" class="nav navbar-nav" > <li><a href="'.base_url().'">HOME</a></li>'; 
      }
      else{
			$hasil .= '<ul> ';
      }
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{ 
    
    $r = $this->db->query("
		SELECT * from posting
									
		where parent='".$h->id_posting."' 
		and status = 1 
		and tampil_menu_atas = 1 
    and domain='".$web."'
    order by urut
		");
		$xx = $r->num_rows();
    
			$nomor = $nomor + 1;
			  $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
			  $yummy   = array("_","","","","","","","","","","","","","");
			  $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
			if( $a > 1 ){
					if ($xx == 0){
						if($h->judul_posting == 'Pengaduan Masyarakat'){
							$hasil .= '
						  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
							';
							}
			elseif($h->judul_posting == 'Pengumuman'){
			$hasil .= '
			<li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
			elseif($h->judul_posting == 'Permohonan Informasi Publik'){
				$hasil .= '
			  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
			elseif($h->judul_posting == 'Permohonan Informasi'){
				$hasil .= '
			  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
				';
				}
						else{
						$hasil .= '
							<li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
							}
					}
					else{
						$hasil .= '
							<li class="parent" > <span class="">'.$h->judul_posting.' </span>';
					} 
			}
			else{
        if($h->judul_posting == 'Berita'){
            $hasil .= '
              <li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
            ';
        }
			elseif($h->judul_posting == 'Pengumuman'){
			$hasil .= '
			<li><a href="'.base_url().'post/galeri/'.$h->id_posting.'/'.$newphrase.'.HTML"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
		elseif($h->judul_posting == 'Pengaduan Masyarakat'){
			$hasil .= '
		  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
		elseif($h->judul_posting == 'Permohonan Informasi Publik'){
			$hasil .= '
		  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
		elseif($h->judul_posting == 'Permohonan Informasi'){
			$hasil .= '
		  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
			';
			}
        else{
          if ($xx == 0){
						if($h->judul_posting == 'Pengaduan Masyarakat'){
							$hasil .= '
						  <li><a href="'.base_url().'pengaduan_masyarakat"> <span class=""> '.$h->judul_posting.' </span> </a>
							';
							}
						elseif($h->judul_posting == 'Permohonan Informasi Publik'){
							$hasil .= '
						  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
							';
							}
						elseif($h->judul_posting == 'Permohonan Informasi'){
							$hasil .= '
						  <li><a href="'.base_url().'permohonan_informasi_publik"> <span class=""> '.$h->judul_posting.' </span> </a>
							';
							}
						else{
						$hasil .= '
							<li> <a href="'.base_url().'post/detail/'.$h->id_posting.'/'.$newphrase.'.HTML"><span class=""> '.$h->judul_posting.' </span></a>';
							}
          }
          else{
            $hasil .= '
              <li class="parent" > <span class="">'.$h->judul_posting.' </span>';
          } 
        }
      }
      
			$hasil = $this->menu_atas_disparbud($h->id_posting,$hasil, $a);
      $hasil .= '</li>';
		}
		if(($w->num_rows)>0)
		{
			$hasil .= "
</ul>";
		}
    else{
      
    }
		
		return $hasil;
	}
}
