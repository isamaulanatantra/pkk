<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rekapdesa extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Input_rekap_data_umum_model');
   }
   
  public function index(){
    $data['main_view'] = 'rekap_catatan_data_kegiatan_warga/rekapdesa';
    $this->load->view('back_bone1', $data);
   }
  public function list_kecamatan()
  {
    $fields = 
      "
      kecamatan.id_kecamatan, 
      kecamatan.nama_kecamatan      
      ";
      
    $where = array(
      'kecamatan.status' => 1,
      'kecamatan.id_kabupaten' => 1,
      'kecamatan.id_propinsi' => 1
      );
      
    $this->db->select("$fields");
     
    $this->db->where($where);
    $this->db->order_by('id_kecamatan');
    $b = $this->db->get('kecamatan');
    $no=1;
    foreach ($b->result() as $b1) {
      echo '<tr>';
      echo '<td>'.$no.'<input type="hidden" id="'.$no.'" value="'.$b1->id_kecamatan.'"></td>';
      echo '<td>'.$b1->nama_kecamatan.'</td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_1"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_2"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_3"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_4"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_5"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_6"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_7"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_8"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_9"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_10"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_11"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_12"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_13"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_14"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_15"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_16"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_17"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_18"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_19"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_20"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_21"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_22"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_23"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_24"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_25"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_26"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_27"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_28"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_29"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_30"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_31"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_32"></div></td>';
      echo '<td><div id="'.$b1->id_kecamatan.'_33"></div></td>';
      echo '</tr>';
      $no++;
    }    
  }
  public function list_desa()
  {
    $fields = 
      "
      desa.id_desa, 
      desa.nama_desa      
      ";
      
    $where = array(
      'desa.status' => 1,
      'desa.id_kecamatan' => $this->session->userdata('id_kecamatan'),
      'desa.id_kabupaten' => 1,
      'desa.id_propinsi' => 1
      );
      
    $this->db->select("$fields");
     
    $this->db->where($where);
    $this->db->order_by('id_desa');
    $b = $this->db->get('desa');
    $no=1;
    foreach ($b->result() as $b1) {
      echo '<tr>';
      echo '<td>'.$no.'<input type="hidden" id="'.$no.'" value="'.$b1->id_desa.'"></td>';
      echo '<td>'.$b1->nama_desa.'</td>';
      echo '<td><div id="'.$b1->id_desa.'_1"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_2"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_3"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_4"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_5"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_6"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_7"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_8"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_9"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_10"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_11"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_12"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_13"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_14"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_15"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_16"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_17"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_18"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_19"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_20"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_21"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_22"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_23"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_24"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_25"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_26"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_27"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_28"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_29"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_30"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_31"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_32"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_33"></div></td>';
      echo '</tr>';
      $no++;
    }    
  }
  public function list_desa_welcome_pkk_kecamatan()
  {
    $fields = 
      "
      desa.id_desa, 
      desa.nama_desa      
      ";
      
    $where = array(
      'desa.status' => 1,
      'desa.id_kecamatan' => $this->session->userdata('id_kecamatan'),
      'desa.id_kabupaten' => 1,
      'desa.id_propinsi' => 1
      );
      
    $this->db->select("$fields");
     
    $this->db->where($where);
    $this->db->order_by('id_desa');
    $b = $this->db->get('desa');
    $no=1;
    foreach ($b->result() as $b1) {
      echo '<tr>';
      echo '<td>'.$no.'<input type="hidden" id="'.$no.'" value="'.$b1->id_desa.'"></td>';
      echo '<td>'.$b1->nama_desa.'</td>';
      echo '<td><div id="'.$b1->id_desa.'_1"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_2"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_3"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_4"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_5"></div></td>';
      echo '<td><div id="'.$b1->id_desa.'_6"></div></td>';
      echo '</tr>';
      $no++;
    }    
  }
  public function jumlah_kepala_keluarga()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    COUNT(id_data_keluarga)as jumlah_kepala_keluarga
    FROM data_keluarga 
    WHERE data_keluarga.id_desa=".$id_desa." 
    AND data_keluarga.id_kabupaten=1 
    AND data_keluarga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_kepala_keluarga.'';
      }
  }
  public function jumlah_input_anggota_keluarga()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    COUNT(id_anggota_keluarga)as jumlah_input_anggota_keluarga
    FROM anggota_keluarga 
    WHERE anggota_keluarga.id_desa=".$id_desa." 
    AND anggota_keluarga.id_kabupaten=1 
    AND anggota_keluarga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        $jumlah_input_anggota_keluarga = $row->jumlah_input_anggota_keluarga;
        if($jumlah_input_anggota_keluarga==0)
        {
          echo '<span class="text-danger">'.$jumlah_input_anggota_keluarga.'</span>';
        }
        else
        {
          echo '<span class="text-success">'.$jumlah_input_anggota_keluarga.'</span> <i class="fa fa-thumbs-o-up txt-success"></i>';
        }
      }
  }
  public function jumlah_anggota_keluarga()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_anggota_keluarga)as jumlah_anggota_keluarga
    FROM data_keluarga 
    WHERE data_keluarga.id_desa=".$id_desa." 
    AND data_keluarga.id_kabupaten=1 
    AND data_keluarga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_anggota_keluarga.'';
      }
  }
  public function jumlah_anggota_keluarga_laki_laki()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_anggota_keluarga_laki_laki)as jumlah_anggota_keluarga_laki_laki
    FROM data_keluarga 
    WHERE data_keluarga.id_desa=".$id_desa." 
    AND data_keluarga.id_kabupaten=1 
    AND data_keluarga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_anggota_keluarga_laki_laki.'';
      }
  }
  public function jumlah_anggota_keluarga_perempuan()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_anggota_keluarga_perempuan)as jumlah_anggota_keluarga_perempuan
    FROM data_keluarga 
    WHERE data_keluarga.id_desa=".$id_desa." 
    AND data_keluarga.id_kabupaten=1 
    AND data_keluarga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_anggota_keluarga_perempuan.'';
      }
  }
  public function jumlah_kartu_keluarga()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_kartu_keluarga)as jumlah_kartu_keluarga
    FROM data_keluarga 
    WHERE data_keluarga.id_desa=".$id_desa." 
    AND data_keluarga.id_kabupaten=1 
    AND data_keluarga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_kartu_keluarga.'';
      }
  }
  public function welcome_pkk_desa()
  {
    $fields = 
      "
      desa.id_desa, 
      desa.nama_desa      
      ";
      
    $where = array(
      'desa.status' => 1,
      'desa.id_kecamatan' => $this->session->userdata('id_kecamatan'),
      'desa.id_kabupaten' => 1,
      'desa.id_propinsi' => 1
      );
      
    $this->db->select("$fields");
     
    $this->db->where($where);
    $this->db->order_by('id_desa');
    $b = $this->db->get('desa');
    $no=1;
    foreach ($b->result() as $b1) {
      echo '<tr>';
        echo '<td>'.$no.'<input type="hidden" id="'.$no.'" value="'.$b1->id_desa.'"></td>';
        echo '<td>'.$b1->nama_desa.'</td>';
        echo '<td>';
         $query = $this->db->query("
          SELECT SUM(jumlah_anggota_keluarga)
          FROM data_keluarga 
          ");
          $jumlah_kepala_keluarga = $query->num_row();
          echo ''.$jumlah_kepala_keluarga.'';
        echo '</td>';
        echo '<td>';
        //$row->jumlah_kartu_keluarga
        echo '</td>';
        echo '<td>';
        foreach ($query->result() as $row)
          {
            $row->jumlah_anggota_keluarga;
              // $query_total_anggota_keluarga = $this->db->query("SELECT id_data_keluarga FROM anggota_keluarga WHERE id_data_keluarga = ".$row->id_data_keluarga." AND status!=99");
              // echo ''.$query_total_anggota_keluarga->num_rows().'';
          }
        echo'</td>';
      echo '</tr>';
      $no++;
    }    
  }
  public function sum_9()
  {echo '0';}
  public function jumlah_perencanaansehat_wus()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
     SUM(jumlah_perencanaansehat_wus)as jumlah_perencanaansehat_wus
    FROM pokja_empat 
    WHERE  pokja_empat.tahun='".$tahun."' 
    AND pokja_empat.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_empat.id_desa=".$id_desa."
    AND pokja_empat.id_kabupaten=1 
    AND pokja_empat.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_perencanaansehat_wus.'';
      }
  }
  public function jumlah_perencanaansehat_pus()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
     SUM(jumlah_perencanaansehat_pus)as jumlah_perencanaansehat_pus
    FROM pokja_empat 
    WHERE  pokja_empat.tahun='".$tahun."' 
    AND pokja_empat.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_empat.id_desa=".$id_desa."
    AND pokja_empat.id_kabupaten=1 
    AND pokja_empat.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_perencanaansehat_pus.'';
      }
  }
  public function kesling()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_krtmemiliki_lain)as jumlah_krtmemiliki_lain
    FROM pokja_empat 
    WHERE  pokja_empat.tahun='".$tahun."' 
    AND pokja_empat.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_empat.id_desa=".$id_desa."
    AND pokja_empat.id_kabupaten=1 
    AND pokja_empat.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_krtmemiliki_lain.'';
      }
  }
  public function jumlah_krtmemiliki_lain()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_krtmemiliki_lain)as jumlah_krtmemiliki_lain
    FROM pokja_empat 
    WHERE  pokja_empat.tahun='".$tahun."' 
    AND pokja_empat.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_empat.id_desa=".$id_desa."
    AND pokja_empat.id_kabupaten=1 
    AND pokja_empat.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_krtmemiliki_lain.'';
      }
  }
  public function jumlah_krtmemiliki_sumur()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_krtmemiliki_sumur)as jumlah_krtmemiliki_sumur
    FROM pokja_empat 
    WHERE  pokja_empat.tahun='".$tahun."' 
    AND pokja_empat.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_empat.id_desa=".$id_desa."
    AND pokja_empat.id_kabupaten=1 
    AND pokja_empat.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_krtmemiliki_sumur.'';
      }
  }
  public function jumlah_krtmemiliki_pdam()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_krtmemiliki_pdam)as jumlah_krtmemiliki_pdam
    FROM pokja_empat 
    WHERE  pokja_empat.tahun='".$tahun."' 
    AND pokja_empat.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_empat.id_desa=".$id_desa."
    AND pokja_empat.id_kabupaten=1 
    AND pokja_empat.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_krtmemiliki_pdam.'';
      }
  }
  public function jumlah_kelestarian_rumahberjamban()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_kelestarian_rumahberjamban)as jumlah_kelestarian_rumahberjamban
    FROM pokja_empat 
    WHERE  pokja_empat.tahun='".$tahun."' 
    AND pokja_empat.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_empat.id_desa=".$id_desa."
    AND pokja_empat.id_kabupaten=1 
    AND pokja_empat.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_kelestarian_rumahberjamban.'';
      }
  }
  public function jumlah_kelestarian_rumahberspal()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_kelestarian_rumahberspal)as jumlah_kelestarian_rumahberspal
    FROM pokja_empat 
    WHERE  pokja_empat.tahun='".$tahun."' 
    AND pokja_empat.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_empat.id_desa=".$id_desa."
    AND pokja_empat.id_kabupaten=1 
    AND pokja_empat.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_kelestarian_rumahberspal.'';
      }
  }
  public function jumlah_kelestarian_rumahtmpsampah()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_kelestarian_rumahtmpsampah)as jumlah_kelestarian_rumahtmpsampah
    FROM pokja_empat 
    WHERE  pokja_empat.tahun='".$tahun."' 
    AND pokja_empat.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_empat.id_desa=".$id_desa."
    AND pokja_empat.id_kabupaten=1 
    AND pokja_empat.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_kelestarian_rumahtmpsampah.'';
      }
  }
  public function industrirumahtangga()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
     SUM(jumlah_industrirumahtangga_pangan)as jumlah_industrirumahtangga_pangan,
     SUM(jumlah_industrirumahtangga_sandang)as jumlah_industrirumahtangga_sandang,
     SUM(jumlah_industrirumahtangga_jasa)as jumlah_industrirumahtangga_jasa
    FROM pokja_tiga 
    WHERE  pokja_tiga.tahun='".$tahun."' 
    AND pokja_tiga.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_tiga.id_desa=".$id_desa."
    AND pokja_tiga.id_kabupaten=1 
    AND pokja_tiga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        $dd=$row->jumlah_industrirumahtangga_pangan+$row->jumlah_industrirumahtangga_sandang+$row->jumlah_industrirumahtangga_jasa;
        echo ''.$dd.'';
      }
  }
  public function hatinyapkk()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
     SUM(jumlah_panganhatinyapkk_peternakan)as jumlah_panganhatinyapkk_peternakan,
     SUM(jumlah_panganhatinyapkk_perikanan)as jumlah_panganhatinyapkk_perikanan,
     SUM(jumlah_panganhatinyapkk_warunghidup)as jumlah_panganhatinyapkk_warunghidup,
     SUM(jumlah_panganhatinyapkk_lumbunghidup)as jumlah_panganhatinyapkk_lumbunghidup,
     SUM(jumlah_panganhatinyapkk_toga)as jumlah_panganhatinyapkk_toga,
     SUM(jumlah_panganhatinyapkk_tanamankeras)as jumlah_panganhatinyapkk_tanamankeras
    FROM pokja_tiga 
    WHERE  pokja_tiga.tahun='".$tahun."' 
    AND pokja_tiga.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_tiga.id_desa=".$id_desa."
    AND pokja_tiga.id_kabupaten=1 
    AND pokja_tiga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        $dd=$row->jumlah_panganhatinyapkk_peternakan+$row->jumlah_panganhatinyapkk_perikanan+$row->jumlah_panganhatinyapkk_warunghidup+$row->jumlah_panganhatinyapkk_lumbunghidup+$row->jumlah_panganhatinyapkk_toga+$row->jumlah_panganhatinyapkk_tanamankeras;
        echo ''.$dd.'';
      }
  }
  public function jumlah_panganmakananpokok_nonberas()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_panganmakananpokok_nonberas)as jumlah_panganmakananpokok_nonberas
    FROM pokja_tiga 
    WHERE  pokja_tiga.tahun='".$tahun."' 
    AND pokja_tiga.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_tiga.id_desa=".$id_desa."
    AND pokja_tiga.id_kabupaten=1 
    AND pokja_tiga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_panganmakananpokok_nonberas.'';
      }
  }
  public function jumlah_panganmakananpokok_beras()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_panganmakananpokok_beras)as jumlah_panganmakananpokok_beras
    FROM pokja_tiga 
    WHERE  pokja_tiga.tahun='".$tahun."' 
    AND pokja_tiga.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_tiga.id_kabupaten=1 
    AND pokja_tiga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_panganmakananpokok_beras.'';
      }
  }
  public function sum_rumah_tidaksehat()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_rumahtidaksehat_layakhuni)as jumlah_rumahtidaksehat_layakhuni
    FROM pokja_tiga 
    WHERE  pokja_tiga.tahun='".$tahun."' 
    AND pokja_tiga.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_tiga.id_desa=".$id_desa."
    AND pokja_tiga.id_kabupaten=1 
    AND pokja_tiga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_rumahtidaksehat_layakhuni.'';
      }
  }
  public function sum_rumah_sehat()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_rumahsehat_layakhuni)as jumlah_rumahsehat_layakhuni
    FROM pokja_tiga 
    WHERE  pokja_tiga.tahun='".$tahun."' 
    AND pokja_tiga.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND pokja_tiga.id_desa=".$id_desa."
    AND pokja_tiga.id_kabupaten=1 
    AND pokja_tiga.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_rumahsehat_layakhuni.'';
      }
  }
  public function jumlah_up2k()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(up2k_pemula_klp)as up2k_pemula_klp,
     SUM(up2k_pemula_peserta)as up2k_pemula_peserta,
     SUM(up2k_madya_klp)as up2k_madya_klp,
     SUM(up2k_madya_peserta)as up2k_madya_peserta,
     SUM(up2k_utama_klp)as up2k_utama_klp,
     SUM(up2k_utama_peserta)as up2k_utama_peserta,
     SUM(up2k_mandiri_klp)as up2k_mandiri_klp,
     SUM(up2k_mandiri_peserta)as up2k_mandiri_peserta,
     SUM(koperasi_berbadan_klp)as koperasi_berbadan_klp,
     SUM(koperasi_berbadan_anggota)as koperasi_berbadan_anggota
    FROM pokja_dua 
    WHERE  pokja_dua.tahun='".$tahun."' 
    AND pokja_dua.id_kecamatan=".$this->session->userdata('id_kecamatan')." 
    AND pokja_dua.id_desa=".$id_desa."
    AND pokja_dua.id_kabupaten=1 
    AND pokja_dua.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        $dd=$row->up2k_pemula_klp+$row->up2k_pemula_peserta+$row->up2k_madya_klp+$row->up2k_madya_peserta+$row->up2k_utama_klp+$row->up2k_utama_peserta+$row->up2k_mandiri_klp+$row->up2k_mandiri_peserta+$row->koperasi_berbadan_klp+$row->koperasi_berbadan_anggota;
        echo ''.$dd.'';
      }
  }
  public function sum_pokja_dua_3buta()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(warga_masih_3buta)as warga_masih_3buta
    FROM pokja_dua 
    WHERE  pokja_dua.tahun='".$tahun."' 
    AND pokja_dua.id_kecamatan=".$this->session->userdata('id_kecamatan')."  
    AND pokja_dua.id_desa=".$id_desa."
    AND pokja_dua.id_kabupaten=1 
    AND pokja_dua.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->warga_masih_3buta.'';
      }
  }
  public function sum_pkk_dusun()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_kelompok_pkk_dusun)as jumlah_kelompok_pkk_dusun
    FROM input_rekap_data_umum 
    WHERE  input_rekap_data_umum.tahun='".$tahun."' 
    AND input_rekap_data_umum.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND input_rekap_data_umum.id_desa=".$id_desa."
    AND input_rekap_data_umum.id_kabupaten=1 
    AND input_rekap_data_umum.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_kelompok_pkk_dusun.'';
      }
  }
  public function sum_pkk_rw()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_kelompok_pkk_rw)as jumlah_kelompok_pkk_rw
    FROM input_rekap_data_umum 
    WHERE  input_rekap_data_umum.tahun='".$tahun."' 
    AND input_rekap_data_umum.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND input_rekap_data_umum.id_desa=".$id_desa."
    AND input_rekap_data_umum.id_kabupaten=1 
    AND input_rekap_data_umum.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_kelompok_pkk_rw.'';
      }
  }
  public function sum_pkk_rt()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_kelompok_pkk_rt)as jumlah_kelompok_pkk_rt
    FROM input_rekap_data_umum 
    WHERE  input_rekap_data_umum.tahun='".$tahun."' 
    AND input_rekap_data_umum.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND input_rekap_data_umum.id_desa=".$id_desa."
    AND input_rekap_data_umum.id_kabupaten=1 
    AND input_rekap_data_umum.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_kelompok_pkk_rt.'';
      }
  }
  public function sum_pkk_dawis()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_kelompok_pkk_dawis)as jumlah_kelompok_pkk_dawis
    FROM input_rekap_data_umum 
    WHERE  input_rekap_data_umum.tahun='".$tahun."' 
    AND input_rekap_data_umum.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND input_rekap_data_umum.id_desa=".$id_desa."
    AND input_rekap_data_umum.id_kabupaten=1 
    AND input_rekap_data_umum.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_kelompok_pkk_dawis.'';
      }
  }
  public function sum_pkk_krt()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_krt)as jumlah_krt
    FROM input_rekap_data_umum 
    WHERE  input_rekap_data_umum.tahun='".$tahun."' 
    AND input_rekap_data_umum.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND input_rekap_data_umum.id_desa=".$id_desa."
    AND input_rekap_data_umum.id_kabupaten=1 
    AND input_rekap_data_umum.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_krt.'';
      }
  }
  public function sum_pkk_kk()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_kk)as jumlah_kk
    FROM input_rekap_data_umum 
    WHERE  input_rekap_data_umum.tahun='".$tahun."' 
    AND input_rekap_data_umum.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND input_rekap_data_umum.id_desa=".$id_desa."
    AND input_rekap_data_umum.id_kabupaten=1 
    AND input_rekap_data_umum.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_kk.'';
      }
  }
  public function sum_pkk_jiwal()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_jiwa_l)as jumlah_jiwa_l
    FROM input_rekap_data_umum 
    WHERE  input_rekap_data_umum.tahun='".$tahun."' 
    AND input_rekap_data_umum.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND input_rekap_data_umum.id_desa=".$id_desa."
    AND input_rekap_data_umum.id_kabupaten=1 
    AND input_rekap_data_umum.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_jiwa_l.'';
      }
  }
  public function sum_pkk_jiwap()
  {    
    $id_desa = $this->input->get('id_desa');
    $tahun = $this->input->get('tahun');
    $query = $this->db->query("SELECT 
    SUM(jumlah_jiwa_p)as jumlah_jiwa_p
    FROM input_rekap_data_umum 
    WHERE  input_rekap_data_umum.tahun='".$tahun."' 
    AND input_rekap_data_umum.id_kecamatan=".$this->session->userdata('id_kecamatan')."
    AND input_rekap_data_umum.id_desa=".$id_desa."
    AND input_rekap_data_umum.id_kabupaten=1 
    AND input_rekap_data_umum.id_propinsi=1 
    ");
    foreach ($query->result() as $row)
      {
        echo ''.$row->jumlah_jiwa_p.'';
      }
  }
}