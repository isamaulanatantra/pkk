<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Berita extends REST_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('Berita_model');
        $this->methods['berita_get']['limit'] = 5000; // 500 requests per hour per user/key
    }

    public function berita_get()
    {
        $web = $this->get('web');
        if($web === null){
            $berita = $this->Berita_model->getData();
        }else{
            $berita = $this->Berita_model->getData($web);
        }

        if($berita){            
            $this->response([
                'status' => true,
                'data' => $berita
            ], REST_Controller::HTTP_OK); 
        }else{            
            $this->response([
                'status' => false,
                'message' => 'data not found'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

}
