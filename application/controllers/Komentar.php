<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Komentar extends CI_Controller
 {
    
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Komentar_model');
   }
  
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'komentar/home';
    $this->load->view('tes', $data);
   }
  
  public function simpan_komentar()
   {
   		$web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('parent', 'parent', 'required');
		$this->form_validation->set_rules('nama', 'nama', 'required');
		$this->form_validation->set_rules('alamat', 'alamat', 'required');
		$this->form_validation->set_rules('nomor_telp', 'nomor_telp', 'required');
		$this->form_validation->set_rules('email', 'email', 'required');
		$this->form_validation->set_rules('isi_komentar', 'isi_komentar', 'required');
		$this->form_validation->set_rules('temp', 'temp', 'required');
		$this->form_validation->set_rules('id_posting', 'id_posting', 'required');
		
        $healthy = array("#", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
        $yummy   = array(" ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ", " ");
        $nama = str_replace($healthy, $yummy, $this->input->post('nama'));
        $alamat = str_replace($healthy, $yummy, $this->input->post('alamat'));
        $nomor_telp = str_replace($healthy, $yummy, $this->input->post('nomor_telp'));
        $email = str_replace($healthy, $yummy, $this->input->post('email'));
        $isi_komentar = str_replace($healthy, $yummy, $this->input->post('isi_komentar'));
				
		if ($this->form_validation->run() == FALSE)
		 {
		  echo 0;
		 }
		else
		 {
		  	$data_input = array(
        		'domain' => $web,
				'id_posting' => $this->input->post('id_posting'),
				'parent' => $this->input->post('parent'),
				'nama' => $nama,
				'alamat' => $alamat,
				'nomor_telp' => $nomor_telp,
				'email' => $email,
				'isi_komentar' => $isi_komentar,
        		'temp' => trim($this->input->post('temp')),
				'created_by' => 0,
				'created_time' => date('Y-m-d H:i:s'),
				'updated_by' => 0,
				'updated_time' => date('Y-m-d H:i:s'),
				'deleted_by' => 0,
				'deleted_time' => date('Y-m-d H:i:s'),
				'status' => 1

				);
     		$table_name = 'komentar';
      		$id = $this->Komentar_model->simpan_komentar($data_input, $table_name);
      		echo $id;
			$table_name  = 'attachment';
		  	$where       = array(
				'table_name' => 'komentar',
				'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_tabel' => $id
				);
      	$this->Crud_model->update_data($data_update, $where, $table_name);
     }
   }
   
  function load_tampil_kometar()
	{
    $web=$this->uut->namadomain(base_url());
    $id_posting = trim($this->input->post('id_posting'));
		$a = 0;
		echo $this->tampil_kometar(0,$h="", $a, $id_posting);
	}
  
  private function tampil_kometar($parent=0,$hasil, $a, $id_posting){
    $web=$this->uut->namadomain(base_url());
		$a = $a + 1;
		$w = $this->db->query("SELECT * from komentar 
                          where parent='".$parent."' 
                          and status = 1 
                          and id_posting='".$id_posting."'
                          and domain='".$web."'
                          ");
		if(($w->num_rows())>0){
		} 
		echo $jumlah_komentar = $w->num_rows();
		foreach($w->result() as $h)
		{
			if( $a > 1 ){
			$hasil =  '
				<tr>                    
					<td style="padding: 2px '.($a * 20).'px ;">
						<span><i class="fa fa-user"></i> <a href="#">'.$h->nama.' </a></span>
						<p>'.$h->isi_komentar.'</p>
					</td>
				</tr>
			';
			  }
			else{
				$hasil =  '
					<tr>                    
						<td>
						<span><i class="fa fa-user"></i> '.$h->nama.' </span>
							<p>'.$h->isi_komentar.'</p>
						</td>
					</tr>
				';
			}
			$hasil = $this->tampil_kometar($h->id_komentar,$hasil, $a, $id_posting);
		}
		if(($w->num_rows)>0){
		}
		return $hasil;
	}
  
  public function total_hit_komentar()
		{
    $web=$this->uut->namadomain(base_url());
      $id_posting = trim($this->input->post('id_posting'));
      $this->db->from('komentar');
      $where    = array(
        'domain' => $web,
        'status' => 1,
        'id_posting' => $id_posting
		);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo $a;
		}
    
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'id_komentar' => $this->input->post('id_komentar'),
        'komentar.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_komentar');
      $result = $this->db->get('komentar');
      echo json_encode($result->result_array());
		}
   
   public function total_komentar()
		{
      $web=$this->uut->namadomain(base_url());
      $limit = trim($this->input->get('limit'));
      $this->db->from('komentar');
      $where    = array(
        'status' => 1,
        'domain' => $web
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_komentar = $this->input->post('id_komentar');
      $where = array(
        'id_komentar' => $id_komentar,
        'domain' => $web
        );
      $this->db->from('komentar');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99,
					'updated_by' => $this->session->userdata('id_users'),
                  
          );
        $table_name  = 'komentar';
        $this->Komentar_model->update_data_komentar($data_update, $where, $table_name);
        echo 1;
        }
		}
   
  public function restore()
		{
      $web=$this->uut->namadomain(base_url());
			$id_komentar = $this->input->post('id_komentar');
      $where = array(
        'id_komentar' => $id_komentar,
        'domain' => $web
        );
      $this->db->from('komentar');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'komentar';
        $this->Komentar_model->update_data_komentar($data_update, $where, $table_name);
        echo 1;
        }
		}
    
  private function daftar_tampil_komen(){
    	$web=$this->uut->namadomain(base_url());
		$id_posting = $this->input->post('id_posting');
		$a = $a + 1;
		$w = $this->db->query("SELECT *
		from komentar
		where komentar.status = 1 
		and komentar.domain = '".$web."' 
		and komentar.id_posting = '".$id_posting."' 
		order by komentar.created_time desc");
	  	echo $jumlah_komentar=$w->num_rows();
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			echo '
				<div class="media">                    
					<a href="javascript:;" class="pull-left">
					<img src="'.base_url().'media/upload/user.png" alt="" class="media-object" alt="User">
					</a>
					<div class="media-body">
						<h4 class="media-heading"><a href="#">'.$h->nama.' </a></h4>
						<p>'.$h->isi_komentar.'</p>
					</div>
				</div>
				<hr class="blog-post-sep">
			';
		}
	}
	
  public function tampil_total_komen()
   {
    $web=$this->uut->namadomain(base_url());
		$id_posting = $this->input->post('posting');
		$b = $this->total_komen($id_posting);
		echo $b;
   }
       
  public function total_komen($id_posting)
		{
    $web=$this->uut->namadomain(base_url());
      $current_url = trim($id_posting);
      $this->db->from('komentar');
      $where    = array(
        'domain' => $web,
        'status' => 1,
        'id_posting' => $id_posting
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      return $a;
		}
	
  public function inaktif_komentar(){
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_komentar' => $this->input->post('id_komentar')
        );
      $this->db->from('komentar');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'komentar';
        if( $cek <> '' ){
          $this->Komentar_model->update_data_komentar($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktif_komentar(){
      $web=$this->uut->namadomain(base_url());
      $cek = $this->session->userdata('id_users');
      $where = array(
        'id_komentar' => $this->input->post('id_komentar')
        );
      $this->db->from('komentar');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'komentar';
        if( $cek <> '' ){
          $this->Komentar_model->update_data_komentar($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
 }