<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Permohonan_organisasi extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    // $this->load->library('fpdf');
    $this->load->library('Bcrypt');
    $this->load->library('Excel');
    $this->load->library('Uut');
    $this->load->model('Crud_model');
    $this->load->model('Permohonan_organisasi_model');
   }
   
  public function index()
   {
    $data['total'] = 10;
    $data['main_view'] = 'permohonan_organisasi/home';
    $this->load->view('tes', $data);
   }
   
  public function total_data()
  {
    $web=$this->uut->namadomain(base_url());
    $limit = trim($this->input->post('limit'));
    $keyword = trim($this->input->post('keyword'));
    $orderby    = $this->input->post('orderby');
    $perihal_surat    = $this->input->post('perihal_surat');
    $fields     = "*";
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    if($perihal_surat == '0'){
    $where0 = array(
      'permohonan_organisasi.status !=' => 99,
      'permohonan_organisasi.domain' => $web
      );
    }else{
    $where0 = array(
      'permohonan_organisasi.status !=' => 99,
      'permohonan_organisasi.domain' => $web,
      'permohonan_organisasi.perihal_permohonan_organisasi' => $perihal_surat
      );
    }
    $this->db->where($where0);
    $query0 = $this->db->get('permohonan_organisasi');
    $a= $query0->num_rows();
    echo trim(ceil($a / $limit));
  }
	public function json_all_permohonan_organisasi(){
    $web=$this->uut->namadomain(base_url());
		$table = 'permohonan_organisasi';
    $page    = $this->input->post('page');
    $limit    = $this->input->post('limit');
    $keyword    = $this->input->post('keyword');
    $order_by    = $this->input->post('orderby');
    $perihal_surat    = $this->input->post('perihal_surat');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *,
		(select perihal_permohonan_pendaftaran_badan_usaha.judul_perihal_permohonan_pendaftaran_badan_usaha from perihal_permohonan_pendaftaran_badan_usaha where perihal_permohonan_pendaftaran_badan_usaha.id_perihal_permohonan_pendaftaran_badan_usaha=permohonan_organisasi.perihal_permohonan_organisasi) as judul_perihal_permohonan_organisasi
    ";
    if($perihal_surat == '0'){
    $where = array(
      'permohonan_organisasi.status !=' => 99,
      'permohonan_organisasi.domain' => $web
      );
    }else{
    $where = array(
      'permohonan_organisasi.status !=' => 99,
      'permohonan_organisasi.domain' => $web,
      'permohonan_organisasi.perihal_permohonan_organisasi' => $perihal_surat
      );
    }
    $orderby   = ''.$order_by.'';
    $query = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $urut=$start;
    foreach ($query->result() as $row)
      {
				$urut=$urut+1;
				echo '<tr id_permohonan_organisasi="'.$row->id_permohonan_organisasi.'" id="'.$row->id_permohonan_organisasi.'" >';
				echo '<td valign="top">'.($urut).'</td>';
				echo '<td valign="top">'.$row->nomor_permohonan_organisasi.'</td>';
				echo '<td valign="top">'.$row->tanggal_permohonan_organisasi.'</td>';
				echo '<td valign="top">'.$row->nama_organisasi.'</td>';
				echo '<td valign="top">'.$row->kesenian.'</td>';
				echo '<td valign="top">'.$row->alamat_organisasi.'</td>';
				echo '<td valign="top">
				<a class="badge bg-warning btn-sm" href="'.base_url().'permohonan_organisasi/cetak/?id_permohonan_organisasi='.$row->id_permohonan_organisasi.'" ><i class="fa fa-print"></i> Cetak</a>
				<a href="#tab_form_permohonan_organisasi" data-toggle="tab" class="update_id badge bg-info btn-sm"><i class="fa fa-pencil-square-o"></i> Sunting</a>
				<a href="#" id="del_ajax" class="badge bg-danger btn-sm"><i class="fa fa-trash-o"></i> Hapus</a>
				</td>';
				echo '</tr>';
      }
			echo '<tr>';
				echo '<td valign="top" colspan="8" style="text-align:right;"><a class="btn btn-default btn-sm" target="_blank" href="'.base_url().'permohonan_organisasi/cetak_xls/?page='.$page.'&limit='.$limit.'&keyword='.$keyword.'&orderby='.$orderby.'" ><i class="fa fa-download"></i> Download File Excel</a></td>';
			echo '</tr>';
          
	}
  public function simpan_permohonan_organisasi(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_permohonan_organisasi', 'judul_permohonan_organisasi', 'required');
		$this->form_validation->set_rules('riwayat_sejarah', 'riwayat_sejarah', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_input = array(
        'domain' => $web,
				'judul_permohonan_organisasi' => trim($this->input->post('judul_permohonan_organisasi')),
        'temp' => trim($this->input->post('temp')),
        'icon' => trim($this->input->post('icon')),
        'keterangan' => trim($this->input->post('keterangan')),
        'nomor_permohonan_organisasi' => trim($this->input->post('nomor_permohonan_organisasi')),
        'perihal_permohonan_organisasi' => trim($this->input->post('perihal_permohonan_organisasi')),
        'yth_permohonan_organisasi' => trim($this->input->post('yth_permohonan_organisasi')),
        'cq_permohonan_organisasi' => trim($this->input->post('cq_permohonan_organisasi')),
        'di_permohonan_organisasi' => trim($this->input->post('di_permohonan_organisasi')),
        'tanggal_permohonan_organisasi' => trim($this->input->post('tanggal_permohonan_organisasi')),
        'sifat_permohonan_organisasi' => trim($this->input->post('sifat_permohonan_organisasi')),
        'lampiran_permohonan_organisasi' => trim($this->input->post('lampiran_permohonan_organisasi')),
        'nama_organisasi' => trim($this->input->post('nama_organisasi')),
        'kesenian' => trim($this->input->post('kesenian')),
        'jenis_kesenian' => trim($this->input->post('jenis_kesenian')),
        'fungsi_kesenian' => trim($this->input->post('fungsi_kesenian')),
        'nama_kesenian' => trim($this->input->post('nama_kesenian')),
        'pengalaman_pentas' => trim($this->input->post('pengalaman_pentas')),
        'penghargaan_yang_pernah_diterima' => trim($this->input->post('penghargaan_yang_pernah_diterima')),
        'fasilitas_peralatan_yang_dimiliki' => trim($this->input->post('fasilitas_peralatan_yang_dimiliki')),
        'hambatan_kendala' => trim($this->input->post('hambatan_kendala')),
        'riwayat_sejarah' => trim($this->input->post('riwayat_sejarah')),
        'alamat_organisasi' => trim($this->input->post('alamat_organisasi')),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_kecamatan' => trim($this->input->post('id_kecamatan')),
        'id_kabupaten' => trim($this->input->post('id_kabupaten')),
        'id_propinsi' => trim($this->input->post('id_propinsi')),
        'nama_pemohon' => trim($this->input->post('nama_pemohon')),
        'tempat_lahir_pemohon' => trim($this->input->post('tempat_lahir_pemohon')),
        'tanggal_lahir_pemohon' => trim($this->input->post('tanggal_lahir_pemohon')),
        'pekerjaan_pemohon' => trim($this->input->post('pekerjaan_pemohon')),
        'nomor_telp_pemohon' => trim($this->input->post('nomor_telp_pemohon')),
        'alamat_pemohon' => trim($this->input->post('alamat_pemohon')),
        'id_desa_pemohon' => trim($this->input->post('id_desa_pemohon')),
        'id_kecamatan_pemohon' => trim($this->input->post('id_kecamatan_pemohon')),
        'id_kabupaten_pemohon' => trim($this->input->post('id_kabupaten_pemohon')),
        'id_propinsi_pemohon' => trim($this->input->post('id_propinsi_pemohon')),
        'kepertluan' => trim($this->input->post('kepertluan')),
        'tanggal_pendirian' => trim($this->input->post('tanggal_pendirian')),
        'jumlah_anggota_pria' => trim($this->input->post('jumlah_anggota_pria')),
        'jumlah_anggota_wanita' => trim($this->input->post('jumlah_anggota_wanita')),
        'jumlah_anggota' => trim($this->input->post('jumlah_anggota')),
        'created_by' => $this->session->userdata('id_users'),
        'created_time' => date('Y-m-d H:i:s'),
        'updated_by' => 0,
        'updated_time' => date('Y-m-d H:i:s'),
        'deleted_by' => 0,
        'deleted_time' => date('Y-m-d H:i:s'),
        'status' => 1
				);
      $table_name = 'permohonan_organisasi';
      $id         = $this->Permohonan_organisasi_model->simpan_permohonan_organisasi($data_input, $table_name);
      echo $id;
			$table_name  = 'anggota_organisasi';
      $where       = array(
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_permohonan_organisasi' => $id
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
	}
  public function update_permohonan_organisasi(){
    $web=$this->uut->namadomain(base_url());
		$this->form_validation->set_rules('judul_permohonan_organisasi', 'judul_permohonan_organisasi', 'required');
		$this->form_validation->set_rules('nomor_permohonan_organisasi', 'nomor_permohonan_organisasi', 'required');
    if ($this->form_validation->run() == FALSE)
     {
      echo 0;
     }
    else
     {
      $data_update = array(
        'judul_permohonan_organisasi' => trim($this->input->post('judul_permohonan_organisasi')),
        'temp' => trim($this->input->post('temp')),
        'icon' => trim($this->input->post('icon')),
        'keterangan' => trim($this->input->post('keterangan')),
        'keterangan' => trim($this->input->post('keterangan')),
        'nomor_permohonan_organisasi' => trim($this->input->post('nomor_permohonan_organisasi')),
        'perihal_permohonan_organisasi' => trim($this->input->post('perihal_permohonan_organisasi')),
        'yth_permohonan_organisasi' => trim($this->input->post('yth_permohonan_organisasi')),
        'cq_permohonan_organisasi' => trim($this->input->post('cq_permohonan_organisasi')),
        'di_permohonan_organisasi' => trim($this->input->post('di_permohonan_organisasi')),
        'tanggal_permohonan_organisasi' => trim($this->input->post('tanggal_permohonan_organisasi')),
        'sifat_permohonan_organisasi' => trim($this->input->post('sifat_permohonan_organisasi')),
        'lampiran_permohonan_organisasi' => trim($this->input->post('lampiran_permohonan_organisasi')),
        'nama_organisasi' => trim($this->input->post('nama_organisasi')),
        'kesenian' => trim($this->input->post('kesenian')),
        'jenis_kesenian' => trim($this->input->post('jenis_kesenian')),
        'fungsi_kesenian' => trim($this->input->post('fungsi_kesenian')),
        'nama_kesenian' => trim($this->input->post('nama_kesenian')),
        'pengalaman_pentas' => trim($this->input->post('pengalaman_pentas')),
        'penghargaan_yang_pernah_diterima' => trim($this->input->post('penghargaan_yang_pernah_diterima')),
        'fasilitas_peralatan_yang_dimiliki' => trim($this->input->post('fasilitas_peralatan_yang_dimiliki')),
        'hambatan_kendala' => trim($this->input->post('hambatan_kendala')),
        'riwayat_sejarah' => trim($this->input->post('riwayat_sejarah')),
        'alamat_organisasi' => trim($this->input->post('alamat_organisasi')),
        'id_desa' => trim($this->input->post('id_desa')),
        'id_kecamatan' => trim($this->input->post('id_kecamatan')),
        'id_kabupaten' => trim($this->input->post('id_kabupaten')),
        'id_propinsi' => trim($this->input->post('id_propinsi')),
        'nama_pemohon' => trim($this->input->post('nama_pemohon')),
        'tempat_lahir_pemohon' => trim($this->input->post('tempat_lahir_pemohon')),
        'tanggal_lahir_pemohon' => trim($this->input->post('tanggal_lahir_pemohon')),
        'pekerjaan_pemohon' => trim($this->input->post('pekerjaan_pemohon')),
        'nomor_telp_pemohon' => trim($this->input->post('nomor_telp_pemohon')),
        'alamat_pemohon' => trim($this->input->post('alamat_pemohon')),
        'id_desa_pemohon	' => trim($this->input->post('id_desa_pemohon	')),
        'id_kecamatan_pemohon' => trim($this->input->post('id_kecamatan_pemohon')),
        'id_kabupaten_pemohon' => trim($this->input->post('id_kabupaten_pemohon')),
        'id_propinsi_pemohon' => trim($this->input->post('id_propinsi_pemohon')),
        'kepertluan' => trim($this->input->post('kepertluan')),
        'tanggal_pendirian' => trim($this->input->post('tanggal_pendirian')),
        'jumlah_anggota_pria' => trim($this->input->post('jumlah_anggota_pria')),
        'jumlah_anggota_wanita' => trim($this->input->post('jumlah_anggota_wanita')),
        'jumlah_anggota' => trim($this->input->post('jumlah_anggota')),
				'updated_by' => $this->session->userdata('id_users'),
        'updated_time' => date('Y-m-d H:i:s')
				);
      $table_name  = 'permohonan_organisasi';
      $where       = array(
        'permohonan_organisasi.id_permohonan_organisasi' => trim($this->input->post('id_permohonan_organisasi')),
        'permohonan_organisasi.domain' => $web
			);
      $id = $this->Permohonan_organisasi_model->update_data_permohonan_organisasi($data_update, $where, $table_name);
      echo $id;
			$table_name  = 'anggota_organisasi';
      $where       = array(
        'temp' => trim($this->input->post('temp'))
				);
			$data_update = array(
				'id_permohonan_organisasi' => trim($this->input->post('id_permohonan_organisasi'))
				);
      $this->Crud_model->update_data($data_update, $where, $table_name);
     }
  }
   
   public function get_by_id()
		{
      $web=$this->uut->namadomain(base_url());
      $where    = array(
        'permohonan_organisasi.id_permohonan_organisasi' => $this->input->post('id_permohonan_organisasi'),
        'permohonan_organisasi.domain' => $web
				);
      $this->db->select("*");
      $this->db->where($where);
      $this->db->order_by('id_permohonan_organisasi');
      $result = $this->db->get('permohonan_organisasi');
      echo json_encode($result->result_array());
		}
   
   public function total_permohonan_organisasi()
		{
      $limit = trim($this->input->get('limit'));
      $this->db->from('permohonan_organisasi');
      $where    = array(
        'status' => 1
				);
      $this->db->where($where);
      $a = $this->db->count_all_results(); 
      echo trim(ceil($a / $limit));
		}
   
  public function inaktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_permohonan_organisasi = $this->input->post('id_permohonan_organisasi');
      $where = array(
        'id_permohonan_organisasi' => $id_permohonan_organisasi
        );
      $this->db->from('permohonan_organisasi');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 0
                  
          );
        $table_name  = 'permohonan_organisasi';
        if( $cek <> '' ){
          $this->Permohonan_organisasi_model->update_data_permohonan_organisasi($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
   
  public function aktifkan()
		{
      $cek = $this->session->userdata('id_users');
			$id_permohonan_organisasi = $this->input->post('id_permohonan_organisasi');
      $where = array(
        'id_permohonan_organisasi' => $id_permohonan_organisasi
        );
      $this->db->from('permohonan_organisasi');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 1
                  
          );
        $table_name  = 'permohonan_organisasi';
        if( $cek <> '' ){
          $this->Permohonan_organisasi_model->update_data_permohonan_organisasi($data_update, $where, $table_name);
          echo 1;
          }
        else{
          echo 0;
          }
        }
		}
    
  public function pdf()
  {
  $data['IconAbout']='ok';
  
  $this->load->view('permohonan_organisasi/pdf');
  $html = $this->output->get_output();
  $this->load->library('dompdf_gen');
  $this->dompdf->load_html($html);
  $width =8.5 * 72;
  $height = 13.88 *72;
  $this->dompdf->set_paper(array(0,0,$width,$height));
  $this->dompdf->render();
  $this->dompdf->stream("cetak_permohonan_organisasi_" . date('d_m_y') . ".pdf");
  
  }
	public function cetak_xls(){
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
			->setCreator("Budi Utomo")
			->setLastModifiedBy("Budi Utomo")
			->setTitle("Laporan I")
			->setSubject("Office 2007 XLSX Document")
			->setDescription("Laporan Permohonan Pendaftaran Badan Usaha")
			->setKeywords("Laporan Halaman")
			->setCategory("Bentuk XLS");
		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A1', 'Download')
			->mergeCells('A1:G1')
			->setCellValue('A2', 'Permohonan Pendaftaran Badan Usaha')
			->mergeCells('A2:G2')
			->setCellValue('A3', 'Tanggal Download : '.date('d/m/Y').' ')
			->mergeCells('A3:G3')
			
			->setCellValue('A6', 'No')
			->setCellValue('B6', 'Nomor Permohonan Pendaftaran Badan Usaha')
			->setCellValue('C6', 'Tanggal Permohonan Pendaftaran Badan Usaha')
      ;
		
		$web=$this->uut->namadomain(base_url());
		
    $table    = 'permohonan_organisasi';
    $page    = $this->input->get('page');
    $limit    = $this->input->get('limit');
    $keyword    = $this->input->get('keyword');
    $order_by    = $this->input->get('orderby');
    $start      = ($page - 1) * $limit;
    $fields     = "
    *
    ";
    $where      = array(
      'permohonan_organisasi.status !=' => 99,
      'permohonan_organisasi.status !=' => 999
    );
    $orderby   = ''.$order_by.'';
    $query = $this->Permohonan_organisasi_model->html_all_permohonan_organisasi($table, $where, $limit, $start, $fields, $orderby, $keyword);
    $i = 7;
    $urut=$start;
    foreach ($query->result() as $row)
      {
        $urut=$urut+1;
        $objPHPExcel->setActiveSheetIndex(0)
        ->setCellValue('A'.$i, ''.$urut.'')
        ->setCellValue('B'.$i, ''.$row->nomor_permohonan_organisasi.'')
        ->setCellValueExplicit('C'.$i, ''.$this->Crud_model->dateBahasaIndo($row->tanggal_permohonan_organisasi).'', PHPExcel_Cell_DataType::TYPE_STRING)
        ;
        $objPHPExcel->getActiveSheet()
        ->getStyle('A'.$i.':C'.$i.'')
        ->getBorders()->getAllBorders()
        ->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
        $i++;
      }
		
		$objPHPExcel->getActiveSheet()->setTitle('Probable Clients');
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getFont()->setBold(true);
		
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID);
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getFill()->getStartColor()->setARGB('cccccc');
								
		$objPHPExcel->getActiveSheet()->getStyle('A6:C6')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
								
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		
		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);
		// Redirect output to a client’s web browser (Excel5)
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="permohonan_organisasi_'.date('ymdhis').'.xls"');
		header('Cache-Control: max-age=0');
		// If you're serving to IE 9, then the following may be needed
		header('Cache-Control: max-age=1');

		// If you're serving to IE over SSL, then the following may be needed
		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
		header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
		header ('Pragma: public'); // HTTP/1.0

		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$objWriter->save('php://output'); 
          
   }
	 
  public function cetak(){
    $id_permohonan_organisasi    = $this->input->get('id_permohonan_organisasi');
    //$id_permohonan_organisasi    = $this->uri->segment(3);
    $where = array(
		'id_permohonan_organisasi' => $id_permohonan_organisasi,
		'status !=' => 99
		);
    $d = $this->Permohonan_organisasi_model->get_data($where);
			if(!$d){
				$data['id_permohonan_organisasi'] = '';
				$data['judul_permohonan_organisasi'] = '';
				$data['nomor_permohonan_organisasi'] = '';
				$data['perihal_permohonan_organisasi'] = '';
				$data['yth_permohonan_organisasi'] = '';
				$data['cq_permohonan_organisasi'] = '';
				$data['di_permohonan_organisasi'] = '';
				$data['tanggal_permohonan_organisasi'] = '';
				$data['sifat_permohonan_organisasi'] = '';
				$data['lampiran_permohonan_organisasi'] = '';
				$data['nama_organisasi'] = '';
				$data['kesenian'] = '';
				$data['jenis_kesenian'] = '';
				$data['fungsi_kesenian'] = '';
				$data['nama_kesenian'] = '';
				$data['pengalaman_pentas'] = '';
				$data['penghargaan_yang_pernah_diterima'] = '';
				$data['fasilitas_peralatan_yang_dimiliki'] = '';
				$data['hambatan_kendala'] = '';
				$data['riwayat_sejarah'] = '';
				$data['alamat_organisasi'] = '';
				$data['nama_desa'] = '';
				$data['nama_kecamatan'] = '';
				$data['nama_kabupaten'] = '';
				$data['nama_propinsi'] = '';
				$data['nama_pemohon'] = '';
				$data['tempat_lahir_pemohon'] = '';
				$data['tanggal_lahir_pemohon'] = '';
				$data['pekerjaan_pemohon'] = '';
				$data['nomor_telp_pemohon'] = '';
				$data['alamat_pemohon'] = '';
				$data['nama_desa_pemohon'] = '';
				$data['nama_kecamatan_pemohon'] = '';
				$data['nama_kabupaten_pemohon'] = '';
				$data['nama_propinsi_pemohon'] = '';
				$data['kepertluan'] = '';
				$data['tanggal_pendirian'] = '';
				$data['jumlah_anggota_pria'] = '';
				$data['jumlah_anggota_wanita'] = '';
				$data['jumlah_anggota'] = '';
				$data['created_time'] = '';
				$data['pengedit'] = '';
				$data['updated_time'] = '';
			}
			else{
				$data['id_permohonan_organisasi'] = $d['id_permohonan_organisasi'];
				$data['judul_permohonan_organisasi'] = $d['judul_permohonan_organisasi'];
				$data['nomor_permohonan_organisasi'] = $d['nomor_permohonan_organisasi'];
				$data['perihal_permohonan_organisasi'] = ''.$d['perihal_permohonan_organisasi'].' ';
				$data['yth_permohonan_organisasi'] = ''.$d['yth_permohonan_organisasi'].' ';
				$data['cq_permohonan_organisasi'] = ''.$d['cq_permohonan_organisasi'].' ';
				$data['di_permohonan_organisasi'] = ''.$d['di_permohonan_organisasi'].' ';
				$data['tanggal_permohonan_organisasi'] = ''.$this->Crud_model->dateBahasaIndo($d['tanggal_permohonan_organisasi']).'';
				$data['tanggal_lahir_pemohon'] = ''.$this->Crud_model->dateBahasaIndo($d['tanggal_lahir_pemohon']).'';
				$data['sifat_permohonan_organisasi'] = $d['sifat_permohonan_organisasi'];
				$data['lampiran_permohonan_organisasi'] = $d['lampiran_permohonan_organisasi'];
				$data['nama_organisasi'] = $d['nama_organisasi'];
				$data['kesenian'] = $d['kesenian'];
				$data['jenis_kesenian'] = $d['jenis_kesenian'];
				$data['fungsi_kesenian'] = $d['fungsi_kesenian'];
				$data['nama_kesenian'] = $d['nama_kesenian'];
				$data['pengalaman_pentas'] = $d['pengalaman_pentas'];
				$data['penghargaan_yang_pernah_diterima'] = $d['penghargaan_yang_pernah_diterima'];
				$data['fasilitas_peralatan_yang_dimiliki'] = $d['fasilitas_peralatan_yang_dimiliki'];
				$data['hambatan_kendala'] = $d['hambatan_kendala'];
				$data['riwayat_sejarah'] = $d['riwayat_sejarah'];
				$data['alamat_organisasi'] = $d['alamat_organisasi'];
				$data['nama_desa'] = $d['nama_desa'];
				$data['nama_kecamatan'] = $d['nama_kecamatan'];
				$data['nama_kabupaten'] = $d['nama_kabupaten'];
				$data['nama_propinsi'] = $d['nama_propinsi'];
				$data['nama_pemohon'] = $d['nama_pemohon'];
				$data['tempat_lahir_pemohon'] = $d['tempat_lahir_pemohon'];
				$data['pekerjaan_pemohon'] = $d['pekerjaan_pemohon'];
				$data['nomor_telp_pemohon'] = $d['nomor_telp_pemohon'];
				$data['alamat_pemohon'] = $d['alamat_pemohon'];
				$data['nama_desa_pemohon'] = $d['nama_desa_pemohon'];
				$data['nama_kecamatan_pemohon'] = $d['nama_kecamatan_pemohon'];
				$data['nama_kabupaten_pemohon'] = $d['nama_kabupaten_pemohon'];
				$data['nama_propinsi_pemohon'] = $d['nama_propinsi_pemohon'];
				$data['kepertluan'] = $d['kepertluan'];
				$data['tanggal_pendirian'] = ''.$this->Crud_model->dateBahasaIndo($d['tanggal_pendirian']).'';
				$data['jumlah_anggota_pria'] = $d['jumlah_anggota_pria'];
				$data['jumlah_anggota_wanita'] = $d['jumlah_anggota_wanita'];
				$data['jumlah_anggota'] = $d['jumlah_anggota'];
				$data['created_time'] = $this->Crud_model->dateBahasaIndo1($d['created_time']);
				$data['pengedit'] = $d['pengedit'];
				$data['updated_time'] = $d['updated_time'];
			}
    $web=$this->uut->namadomain(base_url());
    $where0 = array(
      'domain' => $web
      );
    $this->db->where($where0);
    $this->db->limit(1);
    $query0 = $this->db->get('dasar_website');
    foreach ($query0->result() as $row0)
      {
        $data['domain'] = $row0->domain;
        $data['alamat'] = $row0->alamat;
        $data['telpon'] = $row0->telpon;
        $data['email'] = $row0->email;
        $data['twitter'] = $row0->twitter;
        $data['facebook'] = $row0->facebook;
        $data['google'] = $row0->google;
        $data['instagram'] = $row0->instagram;
        $data['peta'] = $row0->peta;
        $data['keterangan_'] = $row0->keterangan;
      }
      $data['judul'] = 'Selamat datang';
      $data['main_view'] = 'permohonan_organisasi/cetak';
      $this->load->view('print', $data);
  }
	
  public function hapus()
		{
      $web=$this->uut->namadomain(base_url());
			$id_permohonan_organisasi = $this->input->post('id_permohonan_organisasi');
      $where = array(
        'id_permohonan_organisasi' => $id_permohonan_organisasi,
				'created_by' => $this->session->userdata('id_users'),
        'domain' => $web
        );
      $this->db->from('permohonan_organisasi');
      $this->db->where($where);
      $a = $this->db->count_all_results();
      if($a == 0){
        echo 0;
        }
      else{
        $data_update = array(
        
          'status' => 99
                  
          );
        $table_name  = 'permohonan_organisasi';
        $this->Permohonan_organisasi_model->update_data_permohonan_organisasi($data_update, $where, $table_name);
        echo 1;
        }
		}
 }