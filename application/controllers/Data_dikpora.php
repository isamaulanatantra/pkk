<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Data_dikpora extends CI_Controller
 {
   
  function __construct()
   {
    parent::__construct();
    $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
   }
   
  public function index()
   {
    $data['main_view'] = 'data_dikpora/home';
    $this->load->view('tes', $data);
   }
	 
  public function jumlah_sekolah()
   {
    $data['main_view'] = 'data_dikpora/jumlah_sekolah';
    $this->load->view('tes', $data);
   }
	 	 
  public function jumlah_siswa()
   {
    $data['main_view'] = 'data_dikpora/jumlah_siswa';
    $this->load->view('tes', $data);
   }
             
  public function jumlah_guru()
   {
    $data['main_view'] = 'data_dikpora/jumlah_guru';
    $this->load->view('tes', $data);
   }
             
  public function data_siswa()
   {
    $data['main_view'] = 'data_dikpora/data_siswa';
    $this->load->view('tes', $data);
   }
	 
  public function load_table()
	{
    $limit    = $this->input->post('limit');
    $klasifikasi_informasi_publik_pembantu    = $this->input->post('klasifikasi_informasi_publik_pembantu');
		$a=0;
		$a = $a + 1;
		if($klasifikasi_informasi_publik_pembantu==''){
			$kategori_opd="";
		}
		else{
			$kategori_opd="where data_dikpora.thn_pelajaran='".$this->input->post('klasifikasi_informasi_publik_pembantu')."'";
		}
		$w = $this->db->query("
		SELECT *
		from data_dikpora where kec_
		 
    ".$kategori_opd."
    order by data_dikpora.nama_sp
		");
		
		if(($w->num_rows())>0){
		}
		$nomor = 0;
		foreach($w->result() as $h)
		{
			$nomor = $nomor + 1;
			$healthy = array(" ", "#", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
			$yummy   = array("_", "", "", "", "","", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
			/*echo '<tr style="padding: 2px;" id_data_dikpora="'.$h->id_data_dikpora.'" id="'.$h->id_data_dikpora.'" >';
				echo '<td style="padding: 2px;">'.$nomor.'</td>';
				echo '<td style="padding: 2px;">'.$h->kec.'</td>';
				echo '<td style="padding: 2px;">'.$h->npsn.'</td>';
				echo '<td style="padding: 2px;">'.$h->status_sekolah.'</td>';
				echo '<td style="padding: 2px;">'.$h->nama_kepsek.'</td>';
			echo '</tr>';*/
		}
	}
    public function upload(){
        $fileName = time().$_FILES['file']['name'];
         
        $config['upload_path'] = './media/kml/'; //buat folder dengan nama assets di root folder
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv';
        $config['max_size'] = 1000000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        $inputFileName = './media/kml/'.$media['file_name'];
         
        try {
                $inputFileType = IOFactory::identify($inputFileName);
                $objReader = IOFactory::createReader($inputFileType);
                $objPHPExcel = $objReader->load($inputFileName);
            } catch(Exception $e) {
                die('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
            }
 
            $sheet = $objPHPExcel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
             
            for ($row = 2; $row <= $highestRow; $row++){                  //  Read a row of data into an array                 
                $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
                                                NULL,
                                                TRUE,
                                                FALSE);
                                                 
                //Sesuaikan sama nama kolom tabel di database 
								$data = array(
                    "npsn"=> $rowData[0][0],
                    "nama_sp"=> $rowData[0][1],
                    "alamat_jalan"=> $rowData[0][2],
                    "desa_kelurahan"=> $rowData[0][3],
                    "kec"=> $rowData[0][4],
                    "jenjang"=> $rowData[0][5],
                    "status_sekolah"=> $rowData[0][6],
                    "email"=> $rowData[0][7],
                    "website"=> $rowData[0][8],
                    "nama_kepsek"=> $rowData[0][9],
                    "ptk_g_pns_l"=> $rowData[0][10],
                    "ptk_g_pns_p"=> $rowData[0][11],
                    "ptk_g_gty_l"=> $rowData[0][12],
                    "ptk_g_gty_p"=> $rowData[0][13],
                    "ptk_g_gtt_l"=> $rowData[0][14],
                    "ptk_g_gtt_p"=> $rowData[0][15],
                    "ptk_g_gb_l"=> $rowData[0][16],
                    "ptk_g_gb_p"=> $rowData[0][17],
                    "ptk_g_gh_l"=> $rowData[0][18],
                    "ptk_g_gh_p"=> $rowData[0][19],
                    "ptk_adm_pns_l"=> $rowData[0][20],
                    "ptk_adm_pns_p"=> $rowData[0][21],
                    "ptk_adm_gty_l"=> $rowData[0][22],
                    "ptk_adm_gty_p"=> $rowData[0][23],
                    "ptk_adm_gtt_l"=> $rowData[0][24],
                    "ptk_adm_gtt_p"=> $rowData[0][25],
                    "ptk_adm_gb_l"=> $rowData[0][26],
                    "ptk_adm_gb_p"=> $rowData[0][27],
                    "ptk_adm_gh_l"=> $rowData[0][28],
                    "ptk_adm_gh_p"=> $rowData[0][29],
                    "pd_tkt_1_l"=> $rowData[0][30],
                    "pd_tkt_1_p"=> $rowData[0][31],
                    "pd_tkt_2_l"=> $rowData[0][32],
                    "pd_tkt_2_p"=> $rowData[0][33],
                    "pd_tkt_3_l"=> $rowData[0][34],
                    "pd_tkt_3_p"=> $rowData[0][35],
                    "pd_tkt_4_l"=> $rowData[0][36],
                    "pd_tkt_4_p"=> $rowData[0][37],
                    "pd_tkt_5_l"=> $rowData[0][38],
                    "pd_tkt_5_p"=> $rowData[0][39],
                    "pd_tkt_6_l"=> $rowData[0][40],
                    "pd_tkt_6_p"=> $rowData[0][41],
                    "pd_tkt_7_l"=> $rowData[0][42],
                    "pd_tkt_7_p"=> $rowData[0][43],
                    "pd_tkt_8_l"=> $rowData[0][44],
                    "pd_tkt_8_p"=> $rowData[0][45],
                    "pd_tkt_9_l"=> $rowData[0][46],
                    "pd_tkt_9_p"=> $rowData[0][47],
                    "pd_tkt_10_l"=> $rowData[0][48],
                    "pd_tkt_10_p"=> $rowData[0][49],
                    "pd_tkt_11_l"=> $rowData[0][50],
                    "pd_tkt_11_p"=> $rowData[0][51],
                    "pd_tkt_12_l"=> $rowData[0][52],
                    "pd_tkt_12_p"=> $rowData[0][53],
                    "pd_usia_5_l"=> $rowData[0][54],
                    "pd_usia_5_p"=> $rowData[0][55],
                    "pd_usia_6_l"=> $rowData[0][56],
                    "pd_usia_6_p"=> $rowData[0][57],
                    "pd_usia_7_l"=> $rowData[0][58],
                    "pd_usia_7_p"=> $rowData[0][59],
                    "pd_usia_8_l"=> $rowData[0][60],
                    "pd_usia_8_p"=> $rowData[0][61],
                    "pd_usia_9_l"=> $rowData[0][62],
                    "pd_usia_9_p"=> $rowData[0][63],
                    "pd_usia_10_l"=> $rowData[0][64],
                    "pd_usia_10_p"=> $rowData[0][65],
                    "pd_usia_11_l"=> $rowData[0][66],
                    "pd_usia_11_p"=> $rowData[0][67],
                    "pd_usia_12_l"=> $rowData[0][68],
                    "pd_usia_12_p"=> $rowData[0][69],
                    "pd_usia_13_l"=> $rowData[0][70],
                    "pd_usia_13_p"=> $rowData[0][71],
                    "pd_usia_14_l"=> $rowData[0][72],
                    "pd_usia_14_p"=> $rowData[0][73],
                    "pd_usia_15_l"=> $rowData[0][74],
                    "pd_usia_15_p"=> $rowData[0][75],
                    "pd_usia_16_l"=> $rowData[0][76],
                    "pd_usia_16_p"=> $rowData[0][77],
                    "pd_usia_17_l"=> $rowData[0][78],
                    "pd_usia_17_p"=> $rowData[0][79],
                    "pd_usia_18_l"=> $rowData[0][80],
                    "pd_usia_18_p"=> $rowData[0][81],
                    "pd_usia_19_l"=> $rowData[0][82],
                    "pd_usia_19_p"=> $rowData[0][83],
                    "pd_mengulang_tkt_1_l"=> $rowData[0][84],
                    "pd_mengulang_tkt_1_p"=> $rowData[0][85],
                    "pd_mengulang_tkt_2_l"=> $rowData[0][86],
                    "pd_mengulang_tkt_2_p"=> $rowData[0][87],
                    "pd_mengulang_tkt_3_l"=> $rowData[0][88],
                    "pd_mengulang_tkt_3_p"=> $rowData[0][89],
                    "pd_mengulang_tkt_4_l"=> $rowData[0][90],
                    "pd_mengulang_tkt_4_p"=> $rowData[0][91],
                    "pd_mengulang_tkt_5_l"=> $rowData[0][92],
                    "pd_mengulang_tkt_5_p"=> $rowData[0][93],
                    "pd_mengulang_tkt_6_l"=> $rowData[0][94],
                    "pd_mengulang_tkt_6_p"=> $rowData[0][95],
                    "pd_mengulang_tkt_7_l"=> $rowData[0][96],
                    "pd_mengulang_tkt_7_p"=> $rowData[0][97],
                    "pd_mengulang_tkt_8_l"=> $rowData[0][98],
                    "pd_mengulang_tkt_8_p"=> $rowData[0][99],
                    "pd_mengulang_tkt_9_l"=> $rowData[0][100],
                    "pd_mengulang_tkt_9_p"=> $rowData[0][101],
                    "pd_mengulang_tkt_10_l"=> $rowData[0][102],
                    "pd_mengulang_tkt_10_p"=> $rowData[0][103],
                    "pd_mengulang_tkt_11_l"=> $rowData[0][104],
                    "pd_tmengulang_kt_11_p"=> $rowData[0][105],
                    "pd_mengulang_tkt_12_l"=> $rowData[0][106],
                    "pd_mengulang_tkt_12_p"=> $rowData[0][107],
                    "pd_putus_tkt_1_l"=> $rowData[0][108],
                    "pd_putus_tkt_1_p"=> $rowData[0][109],
                    "pd_putus_tkt_2_l"=> $rowData[0][110],
                    "pd_putus_tkt_2_p"=> $rowData[0][111],
                    "pd_putus_tkt_3_l"=> $rowData[0][112],
                    "pd_putus_tkt_3_p"=> $rowData[0][113],
                    "pd_putus_tkt_4_l"=> $rowData[0][114],
                    "pd_putus_tkt_4_p"=> $rowData[0][115],
                    "pd_putus_tkt_5_l"=> $rowData[0][116],
                    "pd_putus_tkt_5_p"=> $rowData[0][117],
                    "pd_putus_tkt_6_l"=> $rowData[0][118],
                    "pd_putus_tkt_6_p"=> $rowData[0][119],
                    "pd_putus_tkt_7_l"=> $rowData[0][120],
                    "pd_putus_tkt_7_p"=> $rowData[0][121],
                    "pd_putus_tkt_8_l"=> $rowData[0][122],
                    "pd_putus_tkt_8_p"=> $rowData[0][123],
                    "pd_putus_tkt_9_l"=> $rowData[0][124],
                    "pd_putus_tkt_9_p"=> $rowData[0][125],
                    "pd_putus_tkt_10_l"=> $rowData[0][126],
                    "pd_putus_tkt_10_p"=> $rowData[0][127],
                    "pd_putus_tkt_11_l"=> $rowData[0][128],
                    "pd_putus_tkt_11_p"=> $rowData[0][129],
                    "pd_putus_tkt_12_l"=> $rowData[0][130],
                    "pd_putus_tkt_12_p"=> $rowData[0][131],
                    "rombel_tkt_1"=> $rowData[0][132],
                    "rombel_tkt_2"=> $rowData[0][133],
                    "rombel_tkt_3"=> $rowData[0][134],
                    "rombel_tkt_4"=> $rowData[0][135],
                    "rombel_tkt_5"=> $rowData[0][136],
                    "rombel_tkt_6"=> $rowData[0][137],
                    "rombel_tkt_7"=> $rowData[0][138],
                    "rombel_tkt_8"=> $rowData[0][139],
                    "rombel_tkt_9"=> $rowData[0][140],
                    "rombel_tkt_10"=> $rowData[0][141],
                    "rombel_tkt_11"=> $rowData[0][142],
                    "rombel_tkt_12"=> $rowData[0][143],
                    "thn_pelajaran"=> $rowData[0][144]
								);
                 
                //sesuaikan nama dengan nama tabel
                $insert = $this->db->insert("data_dikpora",$data);
                delete_files($media['file_path']);
                     
            }
        redirect('data_dikpora/');
    }
		
 }