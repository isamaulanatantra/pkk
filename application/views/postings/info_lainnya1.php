                            <!--
                  <h3>Informasi Terkait</h3>
                  <div class="recent-news margin-bottom-10" id="informasi_lainnya">
                  </div>       
                  -->
                  <h3>Informasi Lainnya</h3>
                  <div class="recent-news margin-bottom-10" id="informasi_terkait">
                  </div>
<?php 
$urls =  $this->uri->segment(3);
?>
<script>
  function load_informasi_terkait() {
    $('#informasi_terkait').html('');
    $('#spinners_informasi_terkait').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        parent:'<?php echo $urls; ?>'
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>postings/informasi_terkait_by_id/',
      success: function(html) {
        $('#informasi_terkait').html(html);
				$('#spinners_informasi_terkait').fadeOut('slow');
      }
    });
  }
</script>
<script>
  function informasi_lainnya() {
    $('#informasi_lainnya').html('');
    $('#spinners_informasi_lainnya').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        parent:'<?php echo $urls; ?>'
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>postings/informasi_lainnya/',
      success: function(html) {
        $('#informasi_lainnya').html(html);
				$('#spinners_informasi_lainnya').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_informasi_terkait();
  informasi_lainnya();
});
</script>