<div id="carousel-example-1" class="carousel slide" data-ride="carousel">
	<!-- Indicators -->
	<ol class="carousel-indicators">
		<li data-target="#carousel-example-1" data-slide-to="0" class="active"></li>
		<li data-target="#carousel-example-1" data-slide-to="1"></li>
		<li data-target="#carousel-example-1" data-slide-to="2"></li>
	</ol>
	<!-- Wrapper for slides -->
	<div class="carousel-inner">
      <?php
      $where1 = array(
        'id_tabel' => $this->uri->segment(3),
        'table_name' => 'posting'
        );
      $this->db->where($where1);
      $query1 = $this->db->get('attachment');
      $a = 0;
      foreach ($query1->result() as $row1)
        {
          $a = $a+1;
          if( $a == 1 ){
            echo
            '
            <div class="item active">
                <img src="'.base_url().'media/upload/'.$row1->file_name.'">
            </div>
            ';
            }
          else{
            echo
            '
            <div class="item">
                <img src="'.base_url().'media/upload/'.$row1->file_name.'">
            </div>
            ';
            } 
        }
      ?>
	</div>
</div>

  <h2><a href="javascript:;"><?php if(!empty($judul_posting)){ echo $judul_posting; } ?></a></h2>
  <p>
  <?php if(!empty($isi_posting)){ echo $isi_posting; } ?>
  </p>
  <div>
										<?php if(!empty($isi_halaman)){ echo $isi_halaman; } ?>
  </div>
  
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
                            <input name="tabel" id="tabel" value="posting" type="hidden" value="">
                            <input name="page" id="page" value="1" type="hidden" value="">
														<input name="id_posting" class="form-control input-sm pull-right" placeholder="Search" type="text" id="id_posting" style="display:none;" value="<?php echo $this->uri->segment(3); ?>">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
                              <option value="posting.created_time">Tanggal Pembuatan</option>
															<option value="posting.judul_posting">Judul</option>
														</select>
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="10">10 Per-Halaman</option>
															<option value="20">20 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
															<option value="999999999">Semua</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_posting"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
                        <ul class="portfolio-group" id="tbl_data_posting">
                        </ul>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_posting" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                    
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var id_posting = $('#id_posting').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        id_posting:id_posting,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>postings/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" id_posting="' + id_posting + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var id_posting = $('#id_posting').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        id_posting:id_posting,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>postings/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    load_data(tabel);
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>