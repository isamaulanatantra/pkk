

                  <div class="blog-item-img">
                    <!-- BEGIN CAROUSEL -->            
                    <div class="front-carousel">
                      <div id="myCarousel" class="carousel slide">
                        <!-- Carousel items -->
                        <div class="carousel-inner">
                          <?php
                            if($gambar <> 'blankgambar.jpg'){
                              echo
                              '
                              <div class="item active">
                                <img src="'.base_url().'media/upload/'.$gambar.'" alt="">
                              </div>
                              ';
                            }
                          ?>
                        </div>
                        <!-- Carousel nav -->
                        <a class="carousel-control left" href="#myCarousel" data-slide="prev">
                          <i class="fa fa-angle-left"></i>
                        </a>
                        <a class="carousel-control right" href="#myCarousel" data-slide="next">
                          <i class="fa fa-angle-right"></i>
                        </a>
                      </div>                
                    </div>
                    <!-- END CAROUSEL -->             
                  </div>
                  <h2><a href="javascript:;"><?php if(!empty($judul_posting)){ echo $judul_posting; } ?></a></h2>
                  <p>
                  <?php if(!empty($isi_posting)){ echo $isi_posting; } ?>
                  </p>
                  <ul class="blog-info">
                    <li><i class="fa fa-user"></i> By admin</li>
                    <li><i class="fa fa-calendar"></i> <?php if(!empty($created_time)){ echo $created_time; } ?></li>
                    <li><i class="fa fa-comments"></i> </li>
                    <li><i class="fa fa-tags"></i> <?php if(!empty($domain)){ echo $domain; } ?></li>
                  </ul>
									
									<div class="">
										<div class="col-md-12">
											<?php
											$url =  $this->uri->segment(4);
											if($url == 'Permohonan_Informasi.HTML'){
												$this -> load -> view('postings/formulir_permohonan_informasi_publik.php');
											}
											elseif($url == 'Daftar_Informasi_Publik.HTML'){
												$this -> load -> view('postings/daftar_informasi_publik.php');
											}
											elseif($url == 'Pengaduan_Masyarakat.HTML'){
												$this -> load -> view('postings/formulir_pengaduan_masyarakat.php');
											}
											elseif($url == 'Informasi_Publik.HTML'){
												$this -> load -> view('postings/daftar_informasi_publik.php');
											}
											else{
											}
											?>
										</div>
									</div>
									<div class="">
                  <?php // echo $pagination_top; ?>
									<div class="comments">
										<?php echo $isi_halaman; ?>      
									</div>
									<?php echo $pagination_bottom; ?>
									
									</div>