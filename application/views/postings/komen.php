<section class="content" id="awal">
  <div class="row">
          <div class="col-md-6">
            <div class="box box-primary box-solid">
              <div class="box-body">
                <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=komentar" enctype="multipart/form-data">
                  <div class="box-body">
										<div class="form-group" style="display:none;">
											<label for="temp">temp</label>
											<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="mode">mode</label>
											<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_komentar">id_komentar</label>
											<input class="form-control" id="id_komentar" name="id" value="" placeholder="id_komentar" type="text">
										</div>
										<div class="form-group">
											<label for="nama_komentator">Nama Komentator</label>
											<input class="form-control" id="nama_komentator" name="nama_komentator" value="" placeholder="Nama Komentator" type="text">
										</div>
										<div class="form-group">
											<label for="email_komentator">Email Komentator</label>
											<input class="form-control" id="email_komentator" name="email_komentator" value="" placeholder="Email Komentator" type="text">
										</div>
										<div class="form-group">
											<label for="isi_komentar">Isi Komentar</label>
											<input class="form-control" id="isi_komentar" name="isi_komentar" value="" placeholder="Isi Komentar" type="text">
										</div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="simpan_komentar">SIMPAN</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_komentar').show();
    $('#update_komentar').hide();
    $('#tbl_attachment_komentar').html('');
    $('#id_komentar, #nama_komentator, #email_komentator, #isi_komentar').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_komentar').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'nama_komentator', 'email_komentator', 'isi_komentar' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["nama_komentator"] = $("#nama_komentator").val();
      parameter["email_komentator"] = $("#email_komentator").val();
      parameter["isi_komentar"] = $("#isi_komentar").val();
      parameter["temp"] = $("#temp").val();
      var url = '<?php echo base_url(); ?>komentar/simpan_komentar';
      
      var parameterRv = [ 'nama_komentator', 'email_komentator', 'isi_komentar' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedKomentar();
      }
    });
  });
</script>
