<h3>SITEMAP <?php if(!empty( $keterangan )){ echo $keterangan; } ?></h3><br />
<div class="table-responsive">
	<table class="table table-hover table-valign-middle">
		<thead>
			<tr>
				<th>Judul</th>
				<th>Tanggal</th>
			</tr>
		</thead>
		<tbody id="tbl_utama_sitemap">
		</tbody>
	</table>
	<div class="overlay" id="spinners_data" style="display:none;">
		<i class="fa fa-refresh fa-spin"></i>
	</div>
</div>
														
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
  load_data_sitemap();
});
</script>
 
<script>
  function load_data_sitemap(halaman, limit) {
    $('#tbl_utama_sitemap').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>postings/load_sitemap/',
      success: function(html) {
        $('#tbl_utama_sitemap').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 