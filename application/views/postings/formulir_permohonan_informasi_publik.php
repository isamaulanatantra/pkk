<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_1">
        
      </div>
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="col-md-8">
						<div class="box box-primary box-solid">
							<div class="box-header with-border">
								<h3 class="box-title" id="judul_formulir">FORMULIR PERMOHONAN INFORMASI PUBLIK</h3>
							</div>
							<div class="box-body">
								<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=permohonan_informasi_publik" enctype="multipart/form-data">
									<div class="box-body">
										<div class="form-group" style="display:none;">
											<label for="temp">temp</label>
											<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="mode">mode</label>
											<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_permohonan_informasi_publik">id_permohonan_informasi_publik</label>
											<input class="form-control" id="id_permohonan_informasi_publik" name="id" value="" placeholder="id_permohonan_informasi_publik" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_posting">id_posting</label>
											<input class="form-control" id="id_posting" name="id_posting" value="<?php echo $this->uri->segment(3); ?>" placeholder="id_posting" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="created_by">created_by</label>
											<input class="form-control" id="created_by" name="created_by" value="0" placeholder="created_by" type="text">
										</div>
										<div class="form-group">
											<label for="nama">Nama</label>
											<input class="form-control" id="nama" name="nama" value="" placeholder="nama" type="text">
										</div>
										<div class="form-group">
											<label for="alamat">Alamat</label>
											<input class="form-control" id="alamat" name="alamat" value="" placeholder="alamat" type="text">
										</div>
										<div class="form-group">
											<label for="pekerjaan">Pekerjaan</label>
											<input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="pekerjaan" type="text">
										</div>
										<div class="form-group">
											<label for="nomor_telp">Nomor telp</label>
											<input class="form-control" id="nomor_telp" name="nomor_telp" value="" placeholder="nomor_telp" type="text">
										</div>
										<div class="form-group">
											<label for="email">Email</label>
											<input class="form-control" id="email" name="email" value="" placeholder="email" type="text">
										</div>
										<div class="form-group">
											<label for="rincian_informasi_yang_diinginkan">Rincian informasi yang diinginkan</label>
											<textarea class="form-control" rows="3" id="rincian_informasi_yang_diinginkan" name="rincian_informasi_yang_diinginkan" value="" placeholder="" type="text">
											</textarea>
										</div>
										<div class="form-group">
											<label for="tujuan_penggunaan_informasi">Tujuan penggunaan informasi</label>
											<input class="form-control" id="tujuan_penggunaan_informasi" name="tujuan_penggunaan_informasi" value="" placeholder="Tujuan penggunaan informasi" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="kategori_permohonan_informasi_publik">kategori_permohonan_informasi_publik</label>
											<input class="form-control" id="kategori_permohonan_informasi_publik" name="kategori_permohonan_informasi_publik" value="Permohonan Inormasi" placeholder="Tujuan penggunaan informasi" type="text">
										</div>
										<div class="form-group">
											<label for="cara_melihat_membaca_mendengarkan_mencatat">cara memperoleh Informasi <br /> melihat/membaca/mendengarkan/mencatat</label>
											<select class="form-control" id="cara_melihat_membaca_mendengarkan_mencatat" name="cara_melihat_membaca_mendengarkan_mencatat" >
											<option value="1">Ya</option>
											<option value="0">Tidak</option>
											</select>
										</div>
										<div class="form-group">
											<label for="cara_hardcopy_softcopy">Cara Memperoleh Informasi <br /> hardcopy/softcopy</label>
											<select class="form-control" id="cara_hardcopy_softcopy" name="cara_hardcopy_softcopy" >
											<option value="1">Ya</option>
											<option value="0">Tidak</option>
											</select>
										</div>
										<div class="form-group">
											<label for="cara_mengambil_langsung">Cara Mendapatkan Informasi <br /> Mengambil langsung</label>
											<select class="form-control" id="cara_mengambil_langsung" name="cara_mengambil_langsung" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group">
											<label for="cara_kurir">Kurir</label>
											<select class="form-control" id="cara_kurir" name="cara_kurir" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group">
											<label for="cara_pos">POS</label>
											<select class="form-control" id="cara_pos" name="cara_pos" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group">
											<label for="cara_faksimili">Faksimili</label>
											<select class="form-control" id="cara_faksimili" name="cara_faksimili" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group">
											<label for="cara_email">Email</label>
											<select class="form-control" id="cara_email" name="cara_email" >
											<option value="1">Ya</option>
											<option value="0">Tidak</option>
											</select>
										</div>
									</div>
									<div class="box-footer">
										<div class="overlay" id="overlay_form_input" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i> Berhasil dikirim.
											<div class="alert alert-info alert-dismissable" id="pesan_terkirim">
												<i class="fa fa-info-circle"></i> Permohonan Informasi Publik yang Anda kirim akan segera diproses. Terima Kasih. <br />Kirim Email adalah cara terbaik untuk mendapatkan informasi!
											</div>
										</div>
										<button type="submit" class="btn btn-primary" id="simpan_permohonan_informasi_publik">KIRIM</button>
										<button type="submit" class="btn btn-primary" id="update_permohonan_informasi_publik" style="display:none;">UPDATE</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
      </div>
    </div>
  </div>
</section>

<script>
  function AfterSavedPermohonan_informasi_publik() {
    $('#id_permohonan_informasi_publik, #id_posting,  #nama, #alamat, #pekerjaan, #kategori_permohonan_informasi_publik, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_permohonan_informasi_publik').show();
    $('#update_permohonan_informasi_publik').hide();
    $('#tbl_attachment_permohonan_informasi_publik').html('');
    $('#id_permohonan_informasi_publik, #id_posting,  #nama, #alamat, #pekerjaan, #kategori_permohonan_informasi_publik, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR PERMOHONAN INFORMASI PUBLIK');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_permohonan_informasi_publik').on('click', function(e) {
      e.preventDefault();
      $('#simpan_permohonan_informasi_publik').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
      $('#pesan_terkirim').show();
      var parameter = [ 'id_posting', 'nama', 'alamat', 'pekerjaan', 'kategori_permohonan_informasi_publik',  'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan', 'tujuan_penggunaan_informasi', 'cara_melihat_membaca_mendengarkan_mencatat', 'cara_hardcopy_softcopy', 'cara_mengambil_langsung', 'cara_kurir', 'cara_pos', 'cara_faksimili', 'cara_email', 'created_by' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_posting"] = $("#id_posting").val();
      parameter["nama"] = $("#nama").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      parameter["tampil_menu_atas"] = $("#tampil_menu_atas").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["rincian_informasi_yang_diinginkan"] = $("#rincian_informasi_yang_diinginkan").val();
      parameter["tujuan_penggunaan_informasi"] = $("#tujuan_penggunaan_informasi").val();
      parameter["kategori_permohonan_informasi_publik"] = $("#kategori_permohonan_informasi_publik").val();
      parameter["cara_melihat_membaca_mendengarkan_mencatat"] = $("#cara_melihat_membaca_mendengarkan_mencatat").val();
      parameter["cara_hardcopy_softcopy"] = $("#cara_hardcopy_softcopy").val();
      parameter["cara_mengambil_langsung"] = $("#cara_mengambil_langsung").val();
      parameter["cara_kurir"] = $("#cara_kurir").val();
      parameter["cara_pos"] = $("#cara_pos").val();
      parameter["cara_faksimili"] = $("#cara_faksimili").val();
      parameter["cara_email"] = $("#cara_email").val();
      parameter["temp"] = $("#temp").val();
      parameter["created_by"] = $("#created_by").val();
      var url = '<?php echo base_url(); ?>permohonan_informasi_publik/simpan_permohonan_informasi_publik_no';
      
      var parameterRv = [ 'id_posting', 'nama', 'alamat', 'pekerjaan', 'kategori_permohonan_informasi_publik', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan', 'tujuan_penggunaan_informasi', 'cara_melihat_membaca_mendengarkan_mencatat', 'cara_hardcopy_softcopy', 'cara_mengambil_langsung', 'cara_kurir', 'cara_pos', 'cara_faksimili', 'cara_email', 'created_by' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
				$('#overlay_form_input').fadeOut();
				alert('gagal');
				$('html, body').animate({
					scrollTop: $('#awal').offset().top
				}, 1000);
      }
      else{
        SimpanData(parameter, url);
        // AfterSavedPermohonan_informasi_publik();
				// alertify.alert('Permohonan Informasi berhasil di kirim');
				alertify.alert('Data berhasil disimpan');
				$('#simpan_permohonan_informasi_publik').removeAttr('disabled', 'disabled');
				$('#overlay_form_input').fadeOut('slow');
				$('#pesan_terkirim').fadeOut('slow');
      }
    });
  });
</script>
