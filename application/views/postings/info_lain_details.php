                                   
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Informasi Lainnya</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="overlay" id="spinners_info_lain_details" style="display:none;">
                                      <i class="fa fa-refresh fa-spin"></i>
                                    </div>
                                    <ul class="posts-list margin-top-10" id="info_lain_details">
                                    </ul>
                                </div>
                            </div>
                            
<?php
$urls =  $this->uri->segment(3);
?>
<script>
  function load_info_lain_details() {
    $('#info_lain_details').html('');
    $('#spinners_info_lain_details').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        parent:'<?php echo $urls; ?>'
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>postings/info_lain_details_by_id/',
      success: function(html) {
        $('#info_lain_details').html(html);
				$('#spinners_info_lain_details').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  load_info_lain_details();
});
</script>