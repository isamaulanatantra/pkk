<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_1">
        
      </div>
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                DAFTAR INFORMASI PUBLIK
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="kata_kunci">
                  <select name="limit_data_daftar_informasi_publik_pembantu" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_daftar_informasi_publik_pembantu">
                    <option value="10">10 Per-Halaman</option>
                    <option value="20">20 Per-Halaman</option>
                    <option value="50">50 Per-Halaman</option>
                    <option value="999999999">Semua</option>
                  </select>
                  <select name="urut_data_daftar_informasi_publik_pembantu" class="form-control input-sm pull-right" id="urut_data_daftar_informasi_publik_pembantu" style="display:none;">
                    <option value="judul_posting">Berdasarkan Ringkasan Informasi</option>
                  </select>
                  <select name="klasifikasi_informasi_publik_pembantu" class="form-control input-sm pull-right" style="width: 180px;" id="klasifikasi_informasi_publik_pembantu">
                    <option value="pilih_kategori_informasi">Pilih Kategori Informasi</option>
                    <option value="informasi_berkala">Informasi Berkala</option>
                    <option value="informasi_serta_merta">Informasi Serta Merta</option>
                    <option value="informasi_setiap_saat">Informasi Setiap Saat</option>
                    <option value="informasi_dikecualikan">Informasi Dikecualikan</option>
                  </select>
                  <select name="opd_domain" class="form-control input-sm pull-right" style="display:none;" id="opd_domain">
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-body table-responsive no-padding">
                        <div id="tblExport">
                          <table class="table table-hover">
                            <thead>
                              <tr id="header_exel">
                                <th>NO</th>
                                <th>Ringkasan Informasi Publik</th>
                                <!--<th>Kategori</th>-->
                                <th>Update</th>
                                <th>Proses</th>
                              </tr>
                            </thead>
                            <tbody id="tbl_utama_daftar_informasi_publik_pembantu">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="row">
                        
                        
                        
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!----------------------->
<script>
  function load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain) {
    $('#tbl_utama_daftar_informasi_publik_pembantu').html('');
    $('#spinners_data').show();
    var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:halaman,
        limit:limit,
        kata_kunci:kata_kunci,
        urut_data_daftar_informasi_publik_pembantu:urut_data_daftar_informasi_publik_pembantu,
        klasifikasi_informasi_publik_pembantu:klasifikasi_informasi_publik_pembantu,
        opd_domain:opd_domain
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/json_all_daftar_informasi_publik_opd/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
          var url_judul_posting = ''+json[i].judul_posting+'';
					tr += '<tr id_daftar_informasi_publik_pembantu="' + json[i].id_daftar_informasi_publik_pembantu + '" id="' + json[i].id_daftar_informasi_publik_pembantu + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
          tr += '<td valign="top">';
          if( json[i].judul_posting == 'Tentang Kami' ){
            tr += 'Profil '+json[i].nama_skpd+'';
          }
          else{
            tr += ''+json[i].judul_posting+'';
            //tr += ''+json[i].judul_posting+' '+json[i].nama_skpd+'';
          }
          tr += '</td>';
          // tr += '<td valign="top">'+klasifikasi_informasi_publik_pembantu+'</td>';
          tr += '<td valign="top">';
          if( json[i].created_time == '0000-00-00 00:00:00' ){
            tr += '';
          }
          else{
            tr += ''+json[i].created_time+'';
          }
          tr += '</td>';
          tr += '<td valign="top"><a href="https://'+json[i].domain+'/postings/detail/'+json[i].id_posting+'" target="_blank"><i class="fa fa-eye"></i> Tampilkan</a></td>';
          tr += '</tr>';
        }
        $('#tbl_utama_daftar_informasi_publik_pembantu').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
  }
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#limit_data_daftar_informasi_publik_pembantu').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#urut_data_daftar_informasi_publik_pembantu').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#kata_kunci').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#opd_domain').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain);
  });
</script>
<!----------------------->
<script>
  function load_opd_domain() {
    $('#opd_domain').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/opd_domain/',
      success: function(html) {
        $('#opd_domain').html('<option value="">Semua OPD</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_opd_domain();
});
</script>

<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#klasifikasi_informasi_publik_pembantu').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain);
    });
  });
</script>