<section class="content" id="awal">
  <div class="">
    <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_1">
        
      </div>
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="col-md-12">
						<div class="box box-primary box-solid">
							<div class="box-header with-border">
								<h3 class="box-title" id="judul_formulir">KOMENTAR</h3>
							</div>
							<div class="box-body" id="tampil_komen">
							</div>
							<div class="box-footer">
								<div class="row">
									<div class="col-md-8" id="div_form_input">
										<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=permohonan_informasi_publik" enctype="multipart/form-data">
											<div class="box-body">
												<div class="form-group" style="display:none;">
													<label for="temp">temp</label>
													<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
												</div>
												<div class="form-group" style="display:none;">
													<label for="mode">mode</label>
													<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
												</div>
												<div class="form-group" style="display:none;">
													<label for="id_permohonan_informasi_publik">id_permohonan_informasi_publik</label>
													<input class="form-control" id="id_permohonan_informasi_publik" name="id" value="" placeholder="id_permohonan_informasi_publik" type="text">
												</div>
												<div class="form-group" style="display:none;">
													<label for="id_posting">id_posting</label>
													<input class="form-control" id="id_posting" name="id_posting" value="<?php echo $this->uri->segment(3); ?>" placeholder="id_posting" type="text">
												</div>
												<div class="form-group" style="display:none;">
													<label for="created_by">created_by</label>
													<input class="form-control" id="created_by" name="created_by" value="0" placeholder="created_by" type="text">
												</div>
												<div class="form-group">
													<label for="nama">Nama</label>
													<input class="form-control" id="nama" name="nama" value="" placeholder="nama" type="text">
												</div>
												<div class="form-group">
													<label for="alamat">Alamat</label>
													<input class="form-control" id="alamat" name="alamat" value="" placeholder="alamat" type="text">
												</div>
												<div class="form-group">
													<label for="pekerjaan">Pekerjaan</label>
													<input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="pekerjaan" type="text">
												</div>
												<div class="form-group">
													<label for="nomor_telp">Nomor telp</label>
													<input class="form-control" id="nomor_telp" name="nomor_telp" value="" placeholder="nomor_telp" type="text">
												</div>
												<div class="form-group">
													<label for="email">Email</label>
													<input class="form-control" id="email" name="email" value="" placeholder="email" type="text">
												</div>
												<div class="form-group">
													<label for="rincian_informasi_yang_diinginkan">Isi Komentar</label>
													<textarea class="form-control" rows="3" id="rincian_informasi_yang_diinginkan" name="rincian_informasi_yang_diinginkan" value="" placeholder="" type="text">
													</textarea>
												</div>
												<div class="form-group" style="display:none;">
													<label for="tujuan_penggunaan_informasi">Tujuan penggunaan informasi</label>
													<input class="form-control" id="tujuan_penggunaan_informasi" name="tujuan_penggunaan_informasi" value="Komentar" placeholder="Tujuan penggunaan informasi" type="text">
												</div>
												<div class="form-group" style="display:none;">
													<label for="kategori_permohonan_informasi_publik">kategori_permohonan_informasi_publik</label>
													<input class="form-control" id="kategori_permohonan_informasi_publik" name="kategori_permohonan_informasi_publik" value="Komentar" placeholder="Tujuan penggunaan informasi" type="text">
												</div>
												<div class="form-group" style="display:none;">
													<label for="cara_melihat_membaca_mendengarkan_mencatat">cara memperoleh Informasi <br /> melihat/membaca/mendengarkan/mencatat</label>
													<select class="form-control" id="cara_melihat_membaca_mendengarkan_mencatat" name="cara_melihat_membaca_mendengarkan_mencatat" >
													<option value="0">Tidak</option>
													<option value="1">Ya</option>
													</select>
												</div>
												<div class="form-group" style="display:none;">
													<label for="cara_hardcopy_softcopy">Cara Memperoleh Informasi <br /> hardcopy/softcopy</label>
													<select class="form-control" id="cara_hardcopy_softcopy" name="cara_hardcopy_softcopy" >
													<option value="0">Tidak</option>
													<option value="1">Ya</option>
													</select>
												</div>
												<div class="form-group" style="display:none;">
													<label for="cara_mengambil_langsung">Cara Mendapatkan Informasi <br /> Mengambil langsung</label>
													<select class="form-control" id="cara_mengambil_langsung" name="cara_mengambil_langsung" >
													<option value="0">Tidak</option>
													<option value="1">Ya</option>
													</select>
												</div>
												<div class="form-group" style="display:none;">
													<label for="cara_kurir">Kurir</label>
													<select class="form-control" id="cara_kurir" name="cara_kurir" >
													<option value="0">Tidak</option>
													<option value="1">Ya</option>
													</select>
												</div>
												<div class="form-group" style="display:none;">
													<label for="cara_pos">POS</label>
													<select class="form-control" id="cara_pos" name="cara_pos" >
													<option value="0">Tidak</option>
													<option value="1">Ya</option>
													</select>
												</div>
												<div class="form-group" style="display:none;">
													<label for="cara_faksimili">Faksimili</label>
													<select class="form-control" id="cara_faksimili" name="cara_faksimili" >
													<option value="0">Tidak</option>
													<option value="1">Ya</option>
													</select>
												</div>
												<div class="form-group" style="display:none;">
													<label for="cara_email">Email</label>
													<select class="form-control" id="cara_email" name="cara_email" >
													<option value="1">Ya</option>
													<option value="0">Tidak</option>
													</select>
												</div>
											</div>
											<div class="box-footer">
												<div class="overlay" id="overlay_form_input" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i> Berhasil dikirim.
													<div class="alert alert-info alert-dismissable" id="pesan_terkirim">
														<i class="fa fa-info-circle"></i> Permohonan Informasi Publik yang Anda kirim akan segera diproses. Terima Kasih. <br />Kirim Email adalah cara terbaik untuk mendapatkan informasi!
													</div>
												</div>
												<button type="submit" class="btn btn-primary" id="simpan_permohonan_informasi_publik">KIRIM KOMENTAR</button>
												<button type="submit" class="btn btn-primary" id="update_permohonan_informasi_publik" style="display:none;">UPDATE</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_permohonan_informasi_publik').on('click', function(e) {
      e.preventDefault();
      $('#simpan_permohonan_informasi_publik').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
			
			var id_permohonan_informasi_publik = $("#id_permohonan_informasi_publik").val();
			var id_posting = $("#id_posting").val();
			var nama = $("#nama").val();
			var alamat = $("#alamat").val();
			var pekerjaan = $("#pekerjaan").val();
			var nomor_telp = $("#nomor_telp").val();
			var kategori_permohonan_informasi_publik = $("#kategori_permohonan_informasi_publik").val();
			var email = $("#email").val();
			var rincian_informasi_yang_diinginkan = $("#rincian_informasi_yang_diinginkan").val();
			var tujuan_penggunaan_informasi = $("#tujuan_penggunaan_informasi").val();
			var cara_melihat_membaca_mendengarkan_mencatat = $("#cara_melihat_membaca_mendengarkan_mencatat").val();
			var cara_hardcopy_softcopy = $("#cara_hardcopy_softcopy").val();
			var cara_mengambil_langsung = $("#cara_mengambil_langsung").val();
			var cara_kurir = $("#cara_kurir").val();
			var cara_pos = $("#cara_pos").val();
			var cara_faksimili = $("#cara_faksimili").val();
			var cara_email = $("#cara_email").val();
			var created_by = $("#created_by").val();
			var temp = $("#temp").val();
						
			if (id_permohonan_informasi_publik == '') {
					$('#id_permohonan_informasi_publik').css('background-color', '#DFB5B4');
				} else {
					$('#id_permohonan_informasi_publik').removeAttr('style');
				}
			if (id_posting == '') {
					$('#id_posting').css('background-color', '#DFB5B4');
				} else {
					$('#id_posting').removeAttr('style');
				}
			if (cara_email == '') {
					$('#cara_email').css('background-color', '#DFB5B4');
				} else {
					$('#cara_email').removeAttr('style');
				}
			if (cara_faksimili == '') {
					$('#cara_faksimili').css('background-color', '#DFB5B4');
				} else {
					$('#cara_faksimili').removeAttr('style');
				}
			if (cara_pos == '') {
					$('#cara_pos').css('background-color', '#DFB5B4');
				} else {
					$('#cara_pos').removeAttr('style');
				}
			if (cara_kurir == '') {
					$('#cara_kurir').css('background-color', '#DFB5B4');
				} else {
					$('#cara_kurir').removeAttr('style');
				}
			if (cara_mengambil_langsung == '') {
					$('#cara_mengambil_langsung').css('background-color', '#DFB5B4');
				} else {
					$('#cara_mengambil_langsung').removeAttr('style');
				}
			if (cara_hardcopy_softcopy == '') {
					$('#cara_hardcopy_softcopy').css('background-color', '#DFB5B4');
				} else {
					$('#cara_hardcopy_softcopy').removeAttr('style');
				}
			if (cara_melihat_membaca_mendengarkan_mencatat == '') {
					$('#cara_melihat_membaca_mendengarkan_mencatat').css('background-color', '#DFB5B4');
				} else {
					$('#cara_melihat_membaca_mendengarkan_mencatat').removeAttr('style');
				}
			if (tujuan_penggunaan_informasi == '') {
					$('#tujuan_penggunaan_informasi').css('background-color', '#DFB5B4');
				} else {
					$('#tujuan_penggunaan_informasi').removeAttr('style');
				}
			if (rincian_informasi_yang_diinginkan == '') {
					$('#rincian_informasi_yang_diinginkan').css('background-color', '#DFB5B4');
				} else {
					$('#rincian_informasi_yang_diinginkan').removeAttr('style');
				}
			if (email == '') {
					$('#email').css('background-color', '#DFB5B4');
				} else {
					$('#email').removeAttr('style');
				}
			if (nama == '') {
					$('#nama').css('background-color', '#DFB5B4');
				} else {
					$('#nama').removeAttr('style');
				}
			if (alamat == '') {
					$('#alamat').css('background-color', '#DFB5B4');
				} else {
					$('#alamat').removeAttr('style');
				}
			if (pekerjaan == '') {
					$('#pekerjaan').css('background-color', '#DFB5B4');
				} else {
					$('#pekerjaan').removeAttr('style');
				}
			if (nomor_telp == '') {
					$('#nomor_telp').css('background-color', '#DFB5B4');
				} else {
					$('#nomor_telp').removeAttr('style');
				}
			if (kategori_permohonan_informasi_publik == '') {
					$('#kategori_permohonan_informasi_publik').css('background-color', '#DFB5B4');
				} else {
					$('#kategori_permohonan_informasi_publik').removeAttr('style');
				}
			if (created_by == '') {
					$('#created_by').css('background-color', '#DFB5B4');
				} else {
					$('#created_by').removeAttr('style');
				}
			if (temp == '') {
					$('#temp').css('background-color', '#DFB5B4');
				} else {
					$('#temp').removeAttr('style');
				}
						
			$.ajax({
        type: 'POST',
        async: true,
        data: {
					id_posting:id_posting,
					nama:nama,
					alamat:alamat,
					pekerjaan:pekerjaan,
					nomor_telp:nomor_telp,
					kategori_permohonan_informasi_publik:kategori_permohonan_informasi_publik,
					email:email,
					rincian_informasi_yang_diinginkan:rincian_informasi_yang_diinginkan,
					tujuan_penggunaan_informasi:tujuan_penggunaan_informasi,
					cara_melihat_membaca_mendengarkan_mencatat:cara_melihat_membaca_mendengarkan_mencatat,
					cara_hardcopy_softcopy:cara_hardcopy_softcopy,
					cara_mengambil_langsung:cara_mengambil_langsung,
					cara_kurir:cara_kurir,
					cara_pos:cara_pos,
					cara_faksimili:cara_faksimili,
					cara_email:cara_email,
					created_by:created_by,
					temp:temp,
										},
        dataType: 'text',
        url: '<?php echo base_url(); ?>permohonan_informasi_publik/simpan_permohonan_informasi_publik_no/',
        success: function(json) {
          if (json == '0') {
            $('#simpan_permohonan_informasi_publik').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut();
            alert('gagal');
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
          } else {
						alertify.alert('Data berhasil disimpan');
            var halaman = 1;
            load_tampil_komen(halaman);
            $('#div_form_input form').trigger('reset');
            $('#simpan_permohonan_informasi_publik').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut('slow');
          }
        }
      });
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<!----------------------->
<script>
  function load_tampil_komen() {
    $('#tampil_komen').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        posting: <?php echo $this->uri->segment(3); ?>
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_informasi_publik/tampil_komen/',
      success: function(html) {
        $('#tampil_komen').html(''+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_tampil_komen();
});
</script>

            <script>
              function Load_total_komen() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
										posting: <?php echo $this->uri->segment(3); ?>
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>permohonan_informasi_publik/tampil_total_komen/',
                  success: function(html) {
                    $('#total_komen').html(''+html+'');
                  }
                });
              }
            </script>

            <script type="text/javascript">
            $(document).ready(function() {
              Load_total_komen();
            });
            </script>
