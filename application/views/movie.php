<!DOCTYPE html>
<html lang="en-US" class="no-js">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
    <!-- Meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
    <meta name="author" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>,">
    <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>">
    <meta name="og:description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
    <meta name="og:url" content="<?php echo base_url(); ?>"/>
    <meta name="og:title" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>"/>
    <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
    <meta name="og:keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
    <!-- Favicon -->
    <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />
  
    <link href="<?php echo base_url('Template/zb/css/style.css'); ?>" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo base_url('Template/zb/css/reset.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('Template/zb/css/component.css'); ?>"/>
    <link rel="stylesheet" href="<?php echo base_url('Template/zb/css/font-awesome.min.css'); ?>">
    <link href="<?php echo base_url('Template/zb/css/bootstrap.css'); ?>" rel="stylesheet">
		<script src="<?php echo base_url('Template/zb/js/modernizr.js'); ?>"></script>
		<link rel='dns-prefetch' href='//s.w.org'/>
		<script type="text/javascript">window._wpemojiSettings={"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/2.3\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/wonosobokab.go.id\/Template\/zb\/js\/wp-emoji-release.min.js"}};!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55358,56794,8205,9794,65039],[55358,56794,8203,9794,65039]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);</script>
		<style type="text/css">img.wp-smiley,img.emoji{display:inline!important;border:none!important;box-shadow:none!important;height:1em!important;width:1em!important;margin:0 .07em!important;vertical-align:-.1em!important;background:none!important;padding:0!important}</style>
    
	</head>
	<body class="home blog">
	<header class="cd-header">
      <div id="cd-logo"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url('media/upload/logowsb.png'); ?>" alt="Logo"></a></div>
	</header>

	<section class="cd-hero">
		<ul class="cd-hero-slider">
			<li class="selected cd-bg-video">
        <div class="cd-bg-video-wrapper">
          <!-- video element will be loaded using jQuery -->
          <?php
          $web=$this->uut->namadomain(base_url());
          $where = array(
            'status' => 1,
            );
          $this->db->where($where);
          $this->db->limit(1);
          $query = $this->db->get('vidio_intro');
          foreach ($query->result() as $row)
            {
              $tema_keterangan = $row->keterangan;
              $file_name = $row->file_name;
            }
          echo '
          <video src="'.base_url().'media/upload/'.$file_name.'" autoplay="true" loop="true"></video> 	  
          ';
          ?>
        </div>
			</li>

		</ul> <!-- .cd-hero-slider -->

	</section> <!-- .cd-hero -->

	<main class="cd-main-content" style="background-color: #000;">
		<div class="main">
				<ul class="og-grid" style="padding:0px;">	
                          <?php
                            $this->db->select("
                            *, (select attachment.file_name from attachment where attachment.id_tabel=posting.id_posting and table_name='posting' limit 1) as attachment_berita_posting
                            ");
                            $where = array(
                              'status' => 1,
                              'parent' => 6093,
                              );
                            $this->db->where($where);
                            $this->db->order_by('created_time','Desc');
                            $this->db->limit(8);
                            $query = $this->db->get('posting');
                            $total=$query->num_rows();
                            //echo '<div class="col-md-12"><h3>Jumlah Website Desa '.$total.' </h3></div>';
                            foreach ($query->result() as $h)
                              {
                                $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
                                $yummy   = array("_","","","","","","","","","","","","","");
                                $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
                                echo '		
					<li>
						<a href="'.base_url().'postings/detail/'.$h->id_posting.'/'.$newphrase.'.HTML" data-description="'.$h->judul_posting.'">
						
							<img src="'.base_url().'media/upload/'.$h->attachment_berita_posting.'" style=" height: 200px; width: 300px;" alt="'.$h->judul_posting.'">
						</a>
					</li>
						
                                ';
                              }
                            ?>		
					
				</ul>
			</div>
      
			<div class="buttons">
				<div style="text-align: center;">
					<ul class="btn">
						<li><a class="demo" href="<?php echo base_url('website'); ?>" target="_blank">TAMPIL SEMUA GALERI</a></li>
					</ul>
				</div>
			</div>
      
		</div>
	</div><!-- /container  style="background-color: #e5e5ff;" -->
</main>	
	<main class="cd-main-content">
		<div class="main">
				<ul id="og-grid" class="og-grid">			
					<li>
						<a href="#" data-largesrc="<?php echo base_url(); ?>media/upload/bupatine wonosobo.jpg" data-title="Pengaduan Masyarakat" data-description="
						<li><a href='#' target='_blank'>Email pemkab@wonosobokab.go.id</a></li>
						<li><a href='https://www.facebook.com/laporbupatiwonosobo/' target='_blank'>Facebook Lapor Bupati</a></li>
						<li><a href='https://twitter.com/BupatiWsbTweet' target='_blank'>Twitter BupatiWsbTweet</a></li>
            ">
						
							<img src="<?php echo base_url(); ?>/media/png/pengaduan.png" alt="Pengaduan Masyarakat">
						</a>
					</li>
					
				
					<li>
						<a href="#" data-largesrc="<?php echo base_url(); ?>media/upload/bupatine wonosobo.jpg" data-title="Layanan Publik" data-description="
						<li><a href='https://dpmptsp.wonosobokab.go.id/' target='_blank'>Perizinan</a></li>
						<li><a href='https://disdukcapil.wonosobokab.go.id/' target='_blank'>Kependudukan</a></li>
						<li><a href='https://disdagkopukm.wonosobokab.go.id/' target='_blank'>Perdagangan Koperasi dan UMKM</a></li>
						<li><a href='https://arpusda.wonosobokab.go.id/' target='_blank'>Perpustakaan</a></li>
						<li><a href='https://disnakerintrans.wonosobokab.go.id/' target='_blank'>Ketenagakerjaan</a></li>
						<li><a href='https://dppkbpppa.wonosobokab.go.id/' target='_blank'>Keluarga Berencana</a></li>
						<li><a href='https://rsud.wonosobokab.go.id/' target='_blank'>RSUD Setjonegoro</a></li>
						<li><a href='https://ppid.wonosobokab.go.id/' target='_blank'>Layanan PPID</a></li>
						">
						<img src="<?php echo base_url(); ?>/media/png/layananpublik.png" alt="Layanan Publik">
						</a>
					</li>

					<li>
						<a href="#" data-largesrc="<?php echo base_url(); ?>media/upload/bupatine wonosobo.jpg" data-title="Informasi Publik" data-description="
						<li><a href='http://lpse.wonosobokab.go.id/' target='_blank'>Informasi Pengadaan (Lelang)</a></li>
						<li><a href='https://wonosobokab.bps.go.id/' target='_blank'>Pusat Statistik Wonosobo</a></li>
						<li><a href='https://disparbud.wonosobokab.go.id/' target='_blank'>Wisata Wonosobo</a></li>
						<li><a href='https://dikpora.wonosobokab.go.id/' target='_blank'>Pendidikan</a></li>
						">
							<img src="<?php echo base_url(); ?>/media/png/informasipublik.png" alt="Informasi Publik">
						</a>
					</li>
					<li>
						<a href="#" data-largesrc="<?php echo base_url(); ?>media/upload/bupatine wonosobo.jpg" data-title="Transparansi Anggaran" data-description="
						<li><a href='https://datadesa.wonosobokab.go.id/'>Transparansi Anggaran APBDes</a></li>">
							<img src="<?php echo base_url(); ?>/media/png/transparansi.png" alt="Transparansi Anggaran">
						</a>
					</li>
					
					<li>
						<a href="#" data-largesrc="<?php echo base_url(); ?>media/upload/bupatine wonosobo.jpg" data-title="Dokumen Publik " data-description="
						<li><a href='https://jdih.wonosobokab.go.id'>Produk Hukum</a></li>
						<li><a href='https://bkd.wonosobokab.go.id/'>Badan Kepegawaian Daerah Kab. Wonosobo</a></li>
            ">
						<img src="<?php echo base_url(); ?>/media/png/dokumen.png" alt="Dokumen Publik">
						</a>
					</li>
				</ul>
			</div>

			<div class="buttons">
				<div style="text-align: center;">
					<ul class="btn">
						<li><a class="demo" href="<?php echo base_url('website'); ?>" target="_blank">WEBSITE</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div><!-- /container -->
</main>	
		<script src="<?php echo base_url('Template/zb/js/jquery-2.1.1.js'); ?>"></script>
		<script src="<?php echo base_url('Template/zb/js/grid.js'); ?>"></script>
		<script>$(function(){Grid.init();});</script>
		<script src="<?php echo base_url('Template/zb/js/main.js'); ?>"></script> <!-- Resource jQuery -->

    <footer id="colophon" class="site-footer" role="contentinfo">
      <div class="site-info">
				<!-- end #content -->
				<div id="sidebar"></div>				
        <div style="clear: both;">&nbsp;</div>
			</div>
		</footer>
    
    <div id="footer-bottom-wrap">
      <p>Copyright &copy; <?php date('Y')?> <a href="<?php echo base_url(); ?>"><?php if(!empty( $keterangan )){ echo $keterangan; } ?></a></p>
    </div>

    <script type='text/javascript' src='<?php echo base_url('Template/zb/js/wp-embed.min.js'); ?>'></script>


</body>
</html>
 