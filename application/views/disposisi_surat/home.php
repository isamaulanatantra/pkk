
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Disposisi Surat</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#"></a></li>
              <li class="breadcrumb-item active">Disposisi Surat</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#tab_form_disposisi_surat" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_data_disposisi_surat" data-toggle="tab" id="klik_tab_data_disposisi_surat">Data</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>disposisi_surat/?id_surat=<?php echo $this->input->get('id_surat'); ?>"><i class="fa fa-refresh"></i></a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
									<div class="tab-pane active" id="tab_form_disposisi_surat">
										<input name="tabel" id="tabel" value="disposisi_surat" type="hidden" value="">
										<form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=disposisi_surat" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="">
                            <div class="card-body">
                              <input name="page" id="page" value="1" type="hidden" value="">
                              <div class="form-group" style="display:none;">
                                <label for="temp">temp</label>
                                <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="mode">mode</label>
                                <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="id_disposisi_surat">id_disposisi_surat</label>
                                <input class="form-control" id="id_disposisi_surat" name="id" value="" placeholder="id_disposisi_surat" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="id_surat">id_surat</label>
                                <input class="form-control" id="id_surat" name="id" value="<?php echo $this->input->get('id_surat'); ?>" placeholder="id_surat" type="text">
                              </div>
                              <div class="form-group">
                                <label for="tujuan_disposisi">Tujuan Disposisi</label>
                                <input class="form-control" id="tujuan_disposisi" name="tujuan_disposisi" value="" placeholder="Tujuan Disposisi" type="text">
                              </div>
                              <div class="form-group">
                                <label for="isi_disposisi">Isi Disposisi</label>
                                <textarea class="form-control" id="isi_disposisi" name="isi_disposisi" value="" placeholder="Isi Disposisi" type="text">
                                </textarea>
                              </div>
                              <div class="form-group">
                                <label for="sifat">Sifat</label>
                                <select class="form-control" id="sifat" name="sifat">
                                  <option value="">- Pilih -</option>
                                  <option value="Biasa">Biasa</option>
                                  <option value="Segera">Segera</option>
                                  <option value="Perlu Perhatian Khusus">Perlu Perhatian Khusus</option>
                                  <option value="Perhatian Batas Waktu">Perhatian Batas Waktu</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="batas_waktu">Batas Waktu</label>
                                <input class="form-control" id="batas_waktu" name="batas_waktu" value="" placeholder="Batas Waktu" type="text">
                              </div>
                              <div class="form-group">
                                <label for="catatan">Catatan</label>
                                <input class="form-control" id="catatan" name="catatan" value="" placeholder="Catatan" type="text">
                              </div>
                              <div class="form-group">
                                <button type="submit" class="btn btn-primary" id="simpan_disposisi_surat"><i class="fa fa-save"></i> SIMPAN</button>
                                <button type="submit" class="btn btn-warning" id="update_disposisi_surat" style="display:none;"><i class="fa fa-save"></i> UPDATE </button>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
										</form>

										<div class="overlay" id="overlay_form_input" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
									<div class="tab-pane table-responsive" id="tab_data_disposisi_surat">
										<div class="">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="id_surat" class="form-control input-sm pull-right" placeholder="Search" type="text" id="id_surat" style="display:none;" value="<?php echo $this->input->get('id_surat'); ?>">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="999999999">Semua</option>
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="disposisi_surat.tujuan_disposisi">Tujuan Disposisi</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_disposisi_surat"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<thead class="bg-gray">
                            <tr>
                              <th>No</th>
                              <th>Tujuan Disposisi</th>
                              <th>Isi Disposisi</th>
                              <th>Sifat, Batas Waktu</th>
                              <th>Proses</th>
                            </tr>
													</thead>
													<tbody id="tbl_data_disposisi_surat">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_disposisi_surat" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var id_surat = $('#id_surat').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        id_surat:id_surat,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>disposisi_surat/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" id_surat="' + id_surat + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var id_surat = $('#id_surat').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        id_surat:id_surat,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedDisposisi_surat() {
    $('#id_disposisi_surat, #tujuan_disposisi, #isi_disposisi, #sifat, #batas_waktu, #catatan').val('');
    $('#tbl_attachment_disposisi_surat').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_disposisi_surat').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'tujuan_disposisi', 'sifat' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_surat"] = $("#id_surat").val();
      parameter["tujuan_disposisi"] = $("#tujuan_disposisi").val();
      parameter["isi_disposisi"] = $("#isi_disposisi").val();
      parameter["sifat"] = $("#sifat").val();
      parameter["batas_waktu"] = $("#batas_waktu").val();
      parameter["catatan"] = $("#catatan").val();
      var url = '<?php echo base_url(); ?>disposisi_surat/simpan_disposisi_surat';
      
      var parameterRv = [ 'tujuan_disposisi', 'sifat' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedDisposisi_surat();
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_disposisi_surat').on('click', '.update_id_disposisi_surat', function() {
    $('#mode').val('edit');
    $('#simpan_disposisi_surat').hide();
    $('#update_disposisi_surat').show();
    $('#overlay_data_disposisi_surat').show();
    $('#tbl_data_disposisi_surat').html('');
    var id_disposisi_surat = $(this).closest('tr').attr('id_disposisi_surat');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_disposisi_surat');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_disposisi_surat').val(id_disposisi_surat);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_disposisi_surat:id_disposisi_surat
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>disposisi_surat/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_disposisi_surat').val(json[i].id_disposisi_surat);
            $('#id_surat').val(json[i].id_surat);
            $('#tujuan_disposisi').val(json[i].tujuan_disposisi);
            $('#isi_disposisi').val(json[i].isi_disposisi);
            $('#sifat').val(json[i].sifat);
            $('#batas_waktu').val(json[i].batas_waktu);
            $('#catatan').val(json[i].catatan);
          }
        }
      });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_disposisi_surat').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'tujuan_disposisi', 'sifat' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_disposisi_surat"] = $("#id_disposisi_surat").val();
      parameter["id_surat"] = $("#id_surat").val();
      parameter["temp"] = $("#temp").val();
      parameter["tujuan_disposisi"] = $("#tujuan_disposisi").val();
      parameter["isi_disposisi"] = $("#isi_disposisi").val();
      parameter["sifat"] = $("#sifat").val();
      parameter["batas_waktu"] = $("#batas_waktu").val();
      parameter["catatan"] = $("#catatan").val();
      var url = '<?php echo base_url(); ?>disposisi_surat/update_disposisi_surat';
      
      var parameterRv = [ 'id_disposisi_surat', 'tujuan_disposisi', 'temp', 'sifat' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_disposisi_surat').on('click', '#del_ajax_disposisi_surat', function() {
    var id_disposisi_surat = $(this).closest('tr').attr('id_disposisi_surat');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_disposisi_surat"] = id_disposisi_surat;
        var url = '<?php echo base_url(); ?>disposisi_surat/hapus/';
        HapusData(parameter, url);
        $('[id_disposisi_surat='+id_disposisi_surat+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
});
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#batas_waktu').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>