<?php 
  $web=$this->uut->namadomain(base_url()); 
  ?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Title -->
    <title><?php if(!empty( $title )){ echo $title; } ?></title>
    <!-- Meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" />
    <meta name="author" content="<?php if(!empty( $title )){ echo $title; } ?>" />
    <meta name="keywords" content="<?php if(!empty( $title )){ echo $title; } ?> <?php echo base_url(); ?>" />
    <meta name="og:description" content="<?php if(!empty( $title )){ echo $title; } ?>" />
    <meta name="og:url" content="<?php echo base_url(); ?> <?php if(!empty( $title )){ echo $title; } ?>" />
    <meta name="og:title" content="<?php if(!empty( $title )){ echo $title; } ?> <?php echo base_url(); ?>" />
    <meta name="og:keywords" content="<?php if(!empty( $title )){ echo $title; } ?> <?php echo base_url(); ?>" />
    <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
    
    <?php if(!empty( $template_head )){ echo $template_head; } ?>
    
    <!-- Default -->
    <script src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.min.js" type="text/javascript"></script>
    
    <script src="<?php echo base_url(); ?>Template/HTML/assets/js/bootstrap.min.js"></script>
  </head>

  <body>
  
    <?php if(!empty( $template_body )){ echo $template_body; } ?>
    
  </body>
</html>