
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="display:none;">Pokja_tiga</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>pokja_tiga">Home</a></li>
              <li class="breadcrumb-item active">Pokja_tiga</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
										<input name="tabel" id="tabel" value="pokja_tiga" type="hidden" value="">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title"><i class="fa fa-th"></i> DATA KEGIATAN POKJA III</h3>
												<div class="card-tools" style="display:none;">
													<div class="input-group input-group-sm">
                            <select class="form-control input-sm pull-right" id="keyword" name="keyword" style="width: 150px;">
                              <option value="2018">2018</option>
                              <option value="2019">2019</option>
                              <option value="2017">2017</option>
                              <option value="2016">2016</option>
                            </select>
														<select name="limit" class="form-control input-sm pull-right" style="display:none;width: 150px;" id="limit">
															<option value="999999999">Semua</option>
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="display:none; width: 150px;" id="orderby">
															<option value="pokja_tiga.tahun">tahun</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_pokja_tiga"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<thead>
                            
                            <tr>
                              <th rowspan="3">NO</th>
                              <th rowspan="3">NAMA WILAYAH</th>
                              <th colspan="3">JUMLAH KADER</th>
                              <th colspan="8">PANGAN</th>
                              <th colspan="3">JUMLAH INDUSTRI <br>RUMAH TANGGA</th>
                              <th colspan="2">JUMLAH <br>RUMAH</th>
                              <th rowspan="3">KETERAGAN</th>
                            </tr>
                            <tr>
                              <th rowspan="2">PANGAN</th>
                              <th rowspan="2">SANDANG</th>
                              <th rowspan="2">TATALAKSANA R.T</th>
                              <th colspan="2">MAKANAN POKOK</th>
                              <th colspan="6">HATINYA PKK</th>
                              <th rowspan="2">PANGAN</th>
                              <th rowspan="2">SANDANG</th>
                              <th rowspan="2">JASA</th>
                              <th rowspan="2">SEHAT &amp; <br>LAYAK HUNI</th>
                              <th rowspan="2">TIDAK SEHAT &amp; <br>TIDAK LAYAK HUNI</th>
                            </tr>
                            <tr>
                              <th>BERAS</th>
                              <th>NONBERAS</th>
                              <th>PETERNAKAN</th>
                              <th>PERIKANAN</th>
                              <th>WARUNG HIDUP</th>
                              <th>LUMBUNG HIDUP</th>
                              <th>TOGA</th>
                              <th>TANAMAN KERAS</th>
                            </tr>
                            
													</thead>
													<tbody id="tbl_data_pokja_tiga">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination" style="display:none;">
												</ul>
												<div class="overlay" id="spinners_tbl_data_pokja_tiga" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_rekap_'+tabel+'/',
      success: function(html) {
        // $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    load_data(tabel);
  });
</script>