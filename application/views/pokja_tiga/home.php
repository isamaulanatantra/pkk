<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Pokja_tiga</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>pokja_tiga">Home</a></li>
          <li class="breadcrumb-item active">Pokja_tiga</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">


  <div class="row" id="awal">
    <div class="col-12">
      <!-- Custom Tabs -->
      <div class="card">
        <div class="card-header p-2">
          <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link active" href="#tab_form_pokja_tiga" data-toggle="tab" id="klik_tab_input">Form</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_data_pokja_tiga" data-toggle="tab" id="klik_tab_data_pokja_tiga">Data</a></li>
            <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>pokja_tiga"><i class="fa fa-refresh"></i></a></li>
          </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="tab-content">
            <div class="tab-pane active" id="tab_form_pokja_tiga">
              <input name="tabel" id="tabel" value="pokja_tiga" type="hidden" value="">
              <form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=pokja_tiga" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-6">
                    <input name="page" id="page" value="1" type="hidden" value="">
                    <div class="form-group" style="display:none;">
                      <label for="temp">temp</label>
                      <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="mode">mode</label>
                      <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_pokja_tiga">id_pokja_tiga</label>
                      <input class="form-control" id="id_pokja_tiga" name="id" value="" placeholder="id_pokja_tiga" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_propinsi">Provinsi</label>
                      <select class="form-control" id="id_propinsi" name="id_propinsi">
                        <option value="1">Jawa Tengah</option>
                      </select>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_kabupaten">Kabupaten</label>
                      <select class="form-control" id="id_kabupaten" name="id_kabupaten">
                        <option value="1">Wonosobo</option>
                      </select>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_kecamatan">Kecamatan</label>
                      <div class="overlay" id="overlay_id_kecamatan" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                      <select class="form-control" id="id_kecamatan" name="id_kecamatan">
                      </select>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-5">
                          <label for="id_desa">Desa</label>
                        </div>
                        <div class="col-md-7">
                          <div class="overlay" id="overlay_id_desa" style="display:none;">
                            <i class="fa fa-refresh fa-spin"></i>
                          </div>
                          <select class="form-control" id="id_desa" name="id_desa">
                            <option value="0">Pilih Desa</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_dusun">Dusun</label>
                      <div class="overlay" id="overlay_id_dusun" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                      <select class="form-control" id="id_dusun" name="id_dusun">
                        <option value="0">Pilih Dusun</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="tahun">tahun</label>
                        </div>
                        <div class="col-md-5">
                          <select class="form-control" id="tahun" name="tahun">
                            <option value="0000">Pilih tahun</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_pangan">jumlah_kader_pangan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_pangan" name="jumlah_kader_pangan" value="" placeholder="jumlah_kader_pangan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_sandang">jumlah_kader_sandang</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_sandang" name="jumlah_kader_sandang" value="" placeholder="jumlah_kader_sandang" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_tatalaksanart">jumlah_kader_tatalaksanart</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_tatalaksanart" name="jumlah_kader_tatalaksanart" value="" placeholder="jumlah_kader_tatalaksanart" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_panganmakananpokok_beras">jumlah_panganmakananpokok_beras</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_panganmakananpokok_beras" name="jumlah_panganmakananpokok_beras" value="" placeholder="jumlah_panganmakananpokok_beras" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_panganmakananpokok_nonberas">jumlah_panganmakananpokok_nonberas</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_panganmakananpokok_nonberas" name="jumlah_panganmakananpokok_nonberas" value="" placeholder="jumlah_panganmakananpokok_nonberas" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_panganhatinyapkk_peternakan">jumlah_panganhatinyapkk_peternakan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_panganhatinyapkk_peternakan" name="jumlah_panganhatinyapkk_peternakan" value="" placeholder="jumlah_panganhatinyapkk_peternakan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_panganhatinyapkk_perikanan">jumlah_panganhatinyapkk_perikanan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_panganhatinyapkk_perikanan" name="jumlah_panganhatinyapkk_perikanan" value="" placeholder="jumlah_panganhatinyapkk_perikanan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_panganhatinyapkk_warunghidup">jumlah_panganhatinyapkk_warunghidup</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_panganhatinyapkk_warunghidup" name="jumlah_panganhatinyapkk_warunghidup" value="" placeholder="jumlah_panganhatinyapkk_warunghidup" type="text">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_panganhatinyapkk_lumbunghidup">jumlah_panganhatinyapkk_lumbunghidup</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_panganhatinyapkk_lumbunghidup" name="jumlah_panganhatinyapkk_lumbunghidup" value="" placeholder="jumlah_panganhatinyapkk_lumbunghidup" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_panganhatinyapkk_toga">jumlah_panganhatinyapkk_toga</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_panganhatinyapkk_toga" name="jumlah_panganhatinyapkk_toga" value="" placeholder="jumlah_panganhatinyapkk_toga" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_panganhatinyapkk_tanamankeras">jumlah_panganhatinyapkk_tanamankeras</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_panganhatinyapkk_tanamankeras" name="jumlah_panganhatinyapkk_tanamankeras" value="" placeholder="jumlah_panganhatinyapkk_tanamankeras" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_industrirumahtangga_pangan">jumlah_industrirumahtangga_pangan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_industrirumahtangga_pangan" name="jumlah_industrirumahtangga_pangan" value="" placeholder="jumlah_industrirumahtangga_pangan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_industrirumahtangga_sandang">jumlah_industrirumahtangga_sandang</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_industrirumahtangga_sandang" name="jumlah_industrirumahtangga_sandang" value="" placeholder="jumlah_industrirumahtangga_sandang" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_industrirumahtangga_jasa">jumlah_industrirumahtangga_jasa</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_industrirumahtangga_jasa" name="jumlah_industrirumahtangga_jasa" value="" placeholder="jumlah_industrirumahtangga_jasa" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_rumahsehat_layakhuni">jumlah_rumahsehat_layakhuni</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_rumahsehat_layakhuni" name="jumlah_rumahsehat_layakhuni" value="" placeholder="jumlah_rumahsehat_layakhuni" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_rumahtidaksehat_layakhuni">jumlah_rumahtidaksehat_layakhuni</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_rumahtidaksehat_layakhuni" name="jumlah_rumahtidaksehat_layakhuni" value="" placeholder="jumlah_rumahtidaksehat_layakhuni" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="keterangan">keterangan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="keterangan" name="keterangan" value="" placeholder="keterangan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary" id="simpan_pokja_tiga">SIMPAN DATA KELUARGA</button>
                      <button type="submit" class="btn btn-primary" id="update_pokja_tiga" style="display:none;">UPDATE DATA KELUARGA</button>
                      <a class="btn text-primary" href="<?php echo base_url(); ?>pokja_tiga"><i class="fa fa-refresh"></i></a>
                    </div>
                  </div>
                </div>
              </form>

              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
            <div class="tab-pane table-responsive" id="tab_data_pokja_tiga">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">&nbsp;</h3>
                  <div class="card-tools">
                    <div class="input-group input-group-sm">
                      <select class="form-control input-sm pull-right" id="keyword" name="keyword" style="width: 150px;">
                        <option value="2022">2022</option>
                        <option value="2021">2021</option>
                        <option value="2020">2020</option>
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                        <option value="2017">2017</option>
                        <option value="2016">2016</option>
                      </select>
                      <select name="limit" class="form-control input-sm pull-right" style="display:none;width: 150px;" id="limit">
                        <option value="999999999">Semua</option>
                        <option value="1">1 Per-Halaman</option>
                        <option value="10">10 Per-Halaman</option>
                        <option value="50">50 Per-Halaman</option>
                        <option value="100">100 Per-Halaman</option>
                      </select>
                      <select name="orderby" class="form-control input-sm pull-right" style="display:none; width: 150px;" id="orderby">
                        <option value="pokja_tiga.tahun">tahun</option>
                      </select>
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" id="tampilkan_data_pokja_tiga"><i class="fa fa-search"></i> Tampil</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-body table-responsive p-0">
                  <table class="table table-bordered table-hover">
                    <thead>

                      <tr>
                        <th rowspan="3">NO</th>
                        <th rowspan="3">NAMA WILAYAH</th>
                        <th colspan="3">JUMLAH KADER</th>
                        <th colspan="8">PANGAN</th>
                        <th colspan="3">JUMLAH INDUSTRI <br>RUMAH TANGGA</th>
                        <th colspan="2">JUMLAH <br>RUMAH</th>
                        <th rowspan="3">KETERAGAN</th>
                        <th rowspan="3"></th>
                      </tr>
                      <tr>
                        <th rowspan="2">PANGAN</th>
                        <th rowspan="2">SANDANG</th>
                        <th rowspan="2">TATALAKSANA R.T</th>
                        <th colspan="2">MAKANAN POKOK</th>
                        <th colspan="6">HATINYA PKK</th>
                        <th rowspan="2">PANGAN</th>
                        <th rowspan="2">SANDANG</th>
                        <th rowspan="2">JASA</th>
                        <th rowspan="2">SEHAT &amp; <br>LAYAK HUNI</th>
                        <th rowspan="2">TIDAK SEHAT &amp; <br>TIDAK LAYAK HUNI</th>
                      </tr>
                      <tr>
                        <th>BERAS</th>
                        <th>NONBERAS</th>
                        <th>PETERNAKAN</th>
                        <th>PERIKANAN</th>
                        <th>WARUNG HIDUP</th>
                        <th>LUMBUNG HIDUP</th>
                        <th>TOGA</th>
                        <th>TANAMAN KERAS</th>
                      </tr>

                    </thead>
                    <tbody id="tbl_data_pokja_tiga">
                    </tbody>
                  </table>
                </div>
                <div class="card-footer">
                  <ul class="pagination pagination-sm m-0 float-right" id="pagination" style="display:none;">
                  </ul>
                  <div class="overlay" id="spinners_tbl_data_pokja_tiga" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.tab-content -->
          </div><!-- /.card-body -->
        </div>
        <!-- ./card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->


</section>


<script type="text/javascript">
  $(document).ready(function() {
    load_option_desa();
  });
</script>
<script>
  function load_option_desa() {
    $('#id_desa').html('');
    $('#overlay_id_desa').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>pokja_tiga/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html('<option value="0">Pilih Desa</option><option value="9999">TP PKK Kecamatan</option>' + html + '');
        $('#overlay_id_desa').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $('#tabel').val();
  });
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit: limit,
        keyword: keyword,
        orderby: orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>pokja_tiga/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_' + tabel + '').html('');
    $('#spinners_tbl_data_' + tabel + '').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page: page,
        limit: limit,
        keyword: keyword,
        orderby: orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>' + tabel + '/json_all_' + tabel + '/',
      success: function(html) {
        // $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
        $('#tbl_data_' + tabel + '').html(html);
        $('#spinners_tbl_data_' + tabel + '').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page = parseInt(id) + 1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_' + tabel + '').on('click', function(e) {
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_' + tabel + '').on('click', function(e) {
      load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedPokja_tiga() {
    $('#id_pokja_tiga, #id_dusun, #rt, #tahun, #id_desa, #id_kecamatan, #id_kabupaten, #jumlah_kader_pangan, #jumlah_kader_sandang, #jumlah_kader_tatalaksanart, #jumlah_panganmakananpokok_beras, #jumlah_panganmakananpokok_nonberas, #jumlah_panganhatinyapkk_peternakan, #jumlah_panganhatinyapkk_perikanan, #jumlah_panganhatinyapkk_warunghidup, #jumlah_panganhatinyapkk_lumbunghidup, #jumlah_panganhatinyapkk_toga, #jumlah_panganhatinyapkk_tanamankeras, #jumlah_industrirumahtangga_pangan, #jumlah_industrirumahtangga_sandang, #jumlah_industrirumahtangga_jasa, #jumlah_rumahsehat_layakhuni, #jumlah_rumahtidaksehat_layakhuni, #keterangan').val('');
    $('#tbl_attachment_pokja_tiga').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#temp').val(Math.random());
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_pokja_tiga').on('click', function(e) {
      e.preventDefault();
      var parameter = ['id_desa', 'temp', 'jumlah_kader_pangan'];
      InputValid(parameter);

      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["tahun"] = $("#tahun").val();
      parameter["jumlah_kader_pangan"] = $("#jumlah_kader_pangan").val();
      parameter["jumlah_kader_sandang"] = $("#jumlah_kader_sandang").val();
      parameter["jumlah_kader_tatalaksanart"] = $("#jumlah_kader_tatalaksanart").val();
      parameter["jumlah_panganmakananpokok_beras"] = $("#jumlah_panganmakananpokok_beras").val();
      parameter["jumlah_panganmakananpokok_nonberas"] = $("#jumlah_panganmakananpokok_nonberas").val();
      parameter["jumlah_panganhatinyapkk_peternakan"] = $("#jumlah_panganhatinyapkk_peternakan").val();
      parameter["jumlah_panganhatinyapkk_perikanan"] = $("#jumlah_panganhatinyapkk_perikanan").val();
      parameter["jumlah_panganhatinyapkk_warunghidup"] = $("#jumlah_panganhatinyapkk_warunghidup").val();
      parameter["jumlah_panganhatinyapkk_lumbunghidup"] = $("#jumlah_panganhatinyapkk_lumbunghidup").val();
      parameter["jumlah_panganhatinyapkk_toga"] = $("#jumlah_panganhatinyapkk_toga").val();
      parameter["jumlah_panganhatinyapkk_tanamankeras"] = $("#jumlah_panganhatinyapkk_tanamankeras").val();
      parameter["jumlah_industrirumahtangga_pangan"] = $("#jumlah_industrirumahtangga_pangan").val();
      parameter["jumlah_industrirumahtangga_sandang"] = $("#jumlah_industrirumahtangga_sandang").val();
      parameter["jumlah_industrirumahtangga_jasa"] = $("#jumlah_industrirumahtangga_jasa").val();
      parameter["jumlah_rumahsehat_layakhuni"] = $("#jumlah_rumahsehat_layakhuni").val();
      parameter["jumlah_rumahtidaksehat_layakhuni"] = $("#jumlah_rumahtidaksehat_layakhuni").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>pokja_tiga/simpan_pokja_tiga';

      var parameterRv = ['id_desa', 'temp', 'jumlah_kader_pangan'];
      var Rv = RequiredValid(parameterRv);
      if (Rv == 0) {
        alertify.error('Mohon data diisi secara lengkap');
      } else {
        SimpanData(parameter, url);
        AfterSavedPokja_tiga();
      }
      $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_data_pokja_tiga').on('click', '.update_id_pokja_tiga', function() {
      $('#mode').val('edit');
      $('#simpan_pokja_tiga').hide();
      $('#update_pokja_tiga').show();
      $('#overlay_data_anggota_keluarga').show();
      $('#tbl_data_anggota_keluarga').html('');
      var id_pokja_tiga = $(this).closest('tr').attr('id_pokja_tiga');
      var mode = $('#mode').val();
      var value = $(this).closest('tr').attr('id_pokja_tiga');
      $('#form_baru').show();
      $('#judul_formulir').html('FORMULIR EDIT');
      $('#id_pokja_tiga').val(id_pokja_tiga);
      $.ajax({
        type: 'POST',
        async: true,
        data: {
          id_pokja_tiga: id_pokja_tiga
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>pokja_tiga/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_propinsi').val(json[i].id_propinsi);
            $('#id_kabupaten').val(json[i].id_kabupaten);
            $('#id_kecamatan').val(json[i].id_kecamatan);
            $('#id_desa').val(json[i].id_desa);
            $('#id_dusun').val(json[i].id_dusun);
            $('#rt').val(json[i].rt);
            $('#tahun').val(json[i].tahun);
            $('#jumlah_kader_pangan').val(json[i].jumlah_kader_pangan);
            $('#jumlah_kader_sandang').val(json[i].jumlah_kader_sandang);
            $('#jumlah_kader_tatalaksanart').val(json[i].jumlah_kader_tatalaksanart);
            $('#jumlah_panganmakananpokok_beras').val(json[i].jumlah_panganmakananpokok_beras);
            $('#jumlah_panganmakananpokok_nonberas').val(json[i].jumlah_panganmakananpokok_nonberas);
            $('#jumlah_panganhatinyapkk_peternakan').val(json[i].jumlah_panganhatinyapkk_peternakan);
            $('#jumlah_panganhatinyapkk_perikanan').val(json[i].jumlah_panganhatinyapkk_perikanan);
            $('#jumlah_panganhatinyapkk_warunghidup').val(json[i].jumlah_panganhatinyapkk_warunghidup);
            $('#jumlah_panganhatinyapkk_lumbunghidup').val(json[i].jumlah_panganhatinyapkk_lumbunghidup);
            $('#jumlah_panganhatinyapkk_toga').val(json[i].jumlah_panganhatinyapkk_toga);
            $('#jumlah_panganhatinyapkk_tanamankeras').val(json[i].jumlah_panganhatinyapkk_tanamankeras);
            $('#jumlah_industrirumahtangga_pangan').val(json[i].jumlah_industrirumahtangga_pangan);
            $('#jumlah_industrirumahtangga_sandang').val(json[i].jumlah_industrirumahtangga_sandang);
            $('#jumlah_industrirumahtangga_jasa').val(json[i].jumlah_industrirumahtangga_jasa);
            $('#jumlah_rumahsehat_layakhuni').val(json[i].jumlah_rumahsehat_layakhuni);
            $('#jumlah_rumahtidaksehat_layakhuni').val(json[i].jumlah_rumahtidaksehat_layakhuni);
            $('#keterangan').val(json[i].keterangan);
            load_option_desa();
          }
        }
      });
      //AttachmentByMode(mode, value);
      //AfterSavedAnggota_keluarga(mode, value);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_pokja_tiga').on('click', function(e) {
      e.preventDefault();
      var parameter = ['id_pokja_tiga', 'id_desa', 'temp', 'jumlah_kader_pkbn'];
      InputValid(parameter);

      var parameter = {}
      parameter["id_pokja_tiga"] = $("#id_pokja_tiga").val();
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["tahun"] = $("#tahun").val();
      parameter["jumlah_kader_pangan"] = $("#jumlah_kader_pangan").val();
      parameter["jumlah_kader_sandang"] = $("#jumlah_kader_sandang").val();
      parameter["jumlah_kader_tatalaksanart"] = $("#jumlah_kader_tatalaksanart").val();
      parameter["jumlah_panganmakananpokok_beras"] = $("#jumlah_panganmakananpokok_beras").val();
      parameter["jumlah_panganmakananpokok_nonberas"] = $("#jumlah_panganmakananpokok_nonberas").val();
      parameter["jumlah_panganhatinyapkk_peternakan"] = $("#jumlah_panganhatinyapkk_peternakan").val();
      parameter["jumlah_panganhatinyapkk_perikanan"] = $("#jumlah_panganhatinyapkk_perikanan").val();
      parameter["jumlah_panganhatinyapkk_warunghidup"] = $("#jumlah_panganhatinyapkk_warunghidup").val();
      parameter["jumlah_panganhatinyapkk_lumbunghidup"] = $("#jumlah_panganhatinyapkk_lumbunghidup").val();
      parameter["jumlah_panganhatinyapkk_toga"] = $("#jumlah_panganhatinyapkk_toga").val();
      parameter["jumlah_panganhatinyapkk_tanamankeras"] = $("#jumlah_panganhatinyapkk_tanamankeras").val();
      parameter["jumlah_industrirumahtangga_pangan"] = $("#jumlah_industrirumahtangga_pangan").val();
      parameter["jumlah_industrirumahtangga_sandang"] = $("#jumlah_industrirumahtangga_sandang").val();
      parameter["jumlah_industrirumahtangga_jasa"] = $("#jumlah_industrirumahtangga_jasa").val();
      parameter["jumlah_rumahsehat_layakhuni"] = $("#jumlah_rumahsehat_layakhuni").val();
      parameter["jumlah_rumahtidaksehat_layakhuni"] = $("#jumlah_rumahtidaksehat_layakhuni").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>pokja_tiga/update_pokja_tiga';

      var parameterRv = ['id_pokja_tiga', 'id_desa', 'temp', 'jumlah_kader_pkbn'];
      var Rv = RequiredValid(parameterRv);
      if (Rv == 0) {
        alertify.error('Mohon data diisi secara lengkap');
      } else {
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_data_pokja_tiga').on('click', '#del_ajax_pokja_tiga', function() {
      var id_pokja_tiga = $(this).closest('tr').attr('id_pokja_tiga');
      alertify.confirm('Anda yakin data akan dihapus?', function(e) {
        if (e) {
          var parameter = {}
          parameter["id_pokja_tiga"] = id_pokja_tiga;
          var url = '<?php echo base_url(); ?>pokja_tiga/hapus/';
          HapusData(parameter, url);
          $('[id_pokja_tiga=' + id_pokja_tiga + ']').remove();
        } else {
          alertify.error('Hapus data dibatalkan');
        }
        $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
        load_data(tabel);
      });
    });
  });
</script>