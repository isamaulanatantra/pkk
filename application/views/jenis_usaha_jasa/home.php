
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Jenis_usaha_jasa</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Jenis_usaha_jasa </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link" href="#tab_1" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link active" href="#tab_2" data-toggle="tab" id="klik_tab_tampil">Data</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  	<div class="tab-pane" id="tab_1">
					  <form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=jenis_usaha_jasa" enctype="multipart/form-data">
					  	<div class="form-group" style="display:none;">
							<label for="temp">temp</label>
							<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
						</div>
						<div class="form-group" style="display:none;">
							<label for="mode">mode</label>
							<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
						</div>
						<div class="form-group" style="display:none;">
							<label for="id_jenis_usaha_jasa">id_jenis_usaha_jasa</label>
							<input class="form-control" id="id_jenis_usaha_jasa" name="id" value="" placeholder="id_jenis_usaha_jasa" type="text">
						</div>
						<div class="form-group" style="display:none;">
							<label for="icon">Icon</label>
						  <select class="form-control" id="icon" name="icon" >
						  <option value="fa-home">fa-home</option>
						  <option value="fa-gears">fa-gears</option>
						  <option value="fa-th">fa-th</option>
						  <option value="fa-font">fa-font</option>
						  <option value="fa-comment">fa-comment</option>
						  <option value="fa-cogs">fa-cogs</option>
						  <option value="fa-cloud-download">fa-cloud-download</option>
						  <option value="fa-bar-char">fa-bar-char</option>
						  <option value="fa-phone">fa-phone</option>
						  <option value="fa-envelope">fa-envelope</option>
						  <option value="fa-link">fa-link</option>
						  <option value="fa-tasks">fa-tasks</option>
						  <option value="fa-users">fa-users</option>
						  <option value="fa-signal">fa-signal</option>
						  <option value="fa-coffee">fa-coffee</option>
						  </select>
						  <div id="iconselected"></div>
						</div>
						<div class="form-group">
							<label for="judul_jenis_usaha_jasa">Judul Jenis_usaha_jasa</label>
							<input class="form-control" id="judul_jenis_usaha_jasa" name="judul_jenis_usaha_jasa" value="" placeholder="Judul Jenis_usaha_jasa" type="text">
						</div>
						<div class="form-group" style="display:none;">
							<label for="isi_jenis_usaha_jasa">Isi Jenis_usaha_jasa</label>
							<input class="form-control" id="isi_jenis_usaha_jasa" name="isi_jenis_usaha_jasa" value="" placeholder="Isi Jenis_usaha_jasa" type="text">
						</div>
						<div class="row">
						<textarea id="editor_isi_jenis_usaha_jasa"></textarea>
						</div>
						<div class="form-group">
							<label for="keterangan">Keterangan Jenis_usaha_jasa</label>
							<input class="form-control" id="keterangan" name="keterangan" value="" placeholder="Keterangan" type="text">
						</div>
						<div class="form-group">
						<button type="submit" class="btn btn-primary" id="simpan_jenis_usaha_jasa">SIMPAN</button>
						<button type="submit" class="btn btn-primary" id="update_jenis_usaha_jasa" style="display:none;">UPDATE</button>
						</div>
					</form>

				 	<div class="overlay" id="overlay_form_input" style="display:none;">
						<i class="fa fa-refresh fa-spin"></i>
				  	</div>
                	</div>
                  	<div class="tab-pane active table-responsive" id="tab_2">
					  <table class="table table-bordered">
						<tr>
						  <th>NO</th>
						  <th>Judul Jenis_usaha_jasa</th>
						  <th>Isi Jenis_usaha_jasa</th>
						  <th>PROSES</th> 
						</tr>
						<tbody id="tbl_utama_jenis_usaha_jasa">
						</tbody>
					  </table>
					  <div class="overlay" id="spinners_data" style="display:none;">
						<i class="fa fa-refresh fa-spin"></i>
					  </div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_tampil').on('click', function(e) {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_data_jenis_usaha_jasa(halaman, limit);
  });
});
</script>
<script>
  function AfterSavedJenis_usaha_jasa() {
    $('#id_jenis_usaha_jasa, #judul_jenis_usaha_jasa, #isi_jenis_usaha_jasa, #icon, #keterangan').val('');
    $('#tbl_attachment_jenis_usaha_jasa').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#icon').on('change', function(e) {
      e.preventDefault();
      var xa = $('#icon').val();
      $("#iconselected").html('<span class="fa '+xa+' ">'+xa+'</span>');
    });
  });
</script>
 
<script>
  function load_data_jenis_usaha_jasa(halaman, limit) {
    $('#tbl_utama_jenis_usaha_jasa').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>jenis_usaha_jasa/json_all_jenis_usaha_jasa/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_jenis_usaha_jasa="' + json[i].id_jenis_usaha_jasa + '" id="id_jenis_usaha_jasa' + json[i].id_jenis_usaha_jasa + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
					tr += '<td valign="top">' + json[i].judul_jenis_usaha_jasa + '</td>';
					tr += '<td valign="top">' + json[i].isi_jenis_usaha_jasa + '</td>';
					tr += '<td valign="top">';
					tr += '<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i> Edit</a> <br />';
					var status_jenis_usaha_jasa=json[i].status;
					if(status_jenis_usaha_jasa==0){
						tr += '<a href="#" id="aktifkan" class="text-danger"><i class="fa fa-times-circle"></i> Tidak Aktif </a>';
					}else{
						tr += '<a href="#" id="inaktifkan" class="text-success"><i class="fa fa-check-circle"></i> Aktif</a>';
					}
					// tr += '<a href="#" id="del_ajax" ><i class="fa fa-cut"></i></a>';
					tr += '</td>';
          tr += '</tr>';
        }
        $('#tbl_utama_jenis_usaha_jasa').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	var halaman = 1;
	var limit = limit_per_page_custome(20000);
	load_data_jenis_usaha_jasa(halaman, limit);
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_jenis_usaha_jasa').on('click', function(e) {
      e.preventDefault();
      var editor_isi_jenis_usaha_jasa = CKEDITOR.instances.editor_isi_jenis_usaha_jasa.getData();
      $('#isi_jenis_usaha_jasa').val( editor_isi_jenis_usaha_jasa );
      var parameter = [ 'judul_jenis_usaha_jasa', 'isi_jenis_usaha_jasa' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["judul_jenis_usaha_jasa"] = $("#judul_jenis_usaha_jasa").val();
      parameter["isi_jenis_usaha_jasa"] = $("#isi_jenis_usaha_jasa").val();
      var url = '<?php echo base_url(); ?>jenis_usaha_jasa/simpan_jenis_usaha_jasa';
      
      var parameterRv = [ 'judul_jenis_usaha_jasa', 'isi_jenis_usaha_jasa' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedJenis_usaha_jasa();
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_jenis_usaha_jasa').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_jenis_usaha_jasa').hide();
    $('#update_jenis_usaha_jasa').show();
    var id_jenis_usaha_jasa = $(this).closest('tr').attr('id_jenis_usaha_jasa');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_jenis_usaha_jasa');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_jenis_usaha_jasa').val(id_jenis_usaha_jasa);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_jenis_usaha_jasa:id_jenis_usaha_jasa
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>jenis_usaha_jasa/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#judul_jenis_usaha_jasa').val(json[i].judul_jenis_usaha_jasa);
            $('#icon').val(json[i].icon);
            $('#keterangan').val(json[i].keterangan);
            CKEDITOR.instances.editor_isi_jenis_usaha_jasa.setData(json[i].isi_jenis_usaha_jasa);
          }
        }
      });
    //AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_jenis_usaha_jasa').on('click', function(e) {
      e.preventDefault();
      var editor_isi_jenis_usaha_jasa = CKEDITOR.instances.editor_isi_jenis_usaha_jasa.getData();
      $('#isi_jenis_usaha_jasa').val( editor_isi_jenis_usaha_jasa );
      var parameter = [ 'id_jenis_usaha_jasa', 'judul_jenis_usaha_jasa', 'isi_jenis_usaha_jasa', 'icon', 'keterangan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_jenis_usaha_jasa"] = $("#id_jenis_usaha_jasa").val();
      parameter["judul_jenis_usaha_jasa"] = $("#judul_jenis_usaha_jasa").val();
      parameter["isi_jenis_usaha_jasa"] = $("#isi_jenis_usaha_jasa").val();
      parameter["icon"] = $("#icon").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>jenis_usaha_jasa/update_jenis_usaha_jasa';
      
      var parameterRv = [ 'id_jenis_usaha_jasa', 'judul_jenis_usaha_jasa', 'isi_jenis_usaha_jasa', 'icon', 'keterangan' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<!----------------------->
<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('editor_isi_jenis_usaha_jasa');
	$(".textarea").wysihtml5();
});
</script>


<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_jenis_usaha_jasa').on('click', '#inaktifkan', function() {
    var id_jenis_usaha_jasa = $(this).closest('tr').attr('id_jenis_usaha_jasa');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_jenis_usaha_jasa"] = id_jenis_usaha_jasa;
        var url = '<?php echo base_url(); ?>jenis_usaha_jasa/inaktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_jenis_usaha_jasa').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
        $('[id_jenis_usaha_jasa='+id_jenis_usaha_jasa+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_jenis_usaha_jasa').on('click', '#aktifkan', function() {
    var id_jenis_usaha_jasa = $(this).closest('tr').attr('id_jenis_usaha_jasa');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_jenis_usaha_jasa"] = id_jenis_usaha_jasa;
        var url = '<?php echo base_url(); ?>jenis_usaha_jasa/aktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_jenis_usaha_jasa').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
        $('[id_jenis_usaha_jasa='+id_jenis_usaha_jasa+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>