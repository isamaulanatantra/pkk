<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>INTEGRATED WEBSITE | <?php echo base_url(); ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?php echo base_url(); ?>Template/AdminLTE-2.4.3/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="<?php echo base_url(); ?>Template/AdminLTE-2.4.3/bower_components/font-awesome/cssfont-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url(); ?>Template/AdminLTE-2.4.3/dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="<?php echo base_url(); ?>Template/AdminLTE-2.4.3/plugins/iCheck/squareblue.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="../../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="../../https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
		
		<!-- Alert Confirmation -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/Typeahead-BS3-css.css" id="toggleCSS" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>boots/dist/css/bootstrap-timepicker.min.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-combined.min.css" id="toggleCSS" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    <style>

html, body {
    min-height: 100%;
		}
		#body {
				background-repeat:no-repeat;
				background-position:top center;
				padding-top:100px;
				background-size: 100% auto;
        background-color: #b8cffc;
               
		}
		@media (min-width: 1100px), (min-height: 570px) {
				body { background-size: auto; }
		}
    #hed{
      background-color: #EEE8AA;
      border: 1px solid black;
      opacity: 0.4;
      font-weight:bold;
      color:blue; 
     font: bold 52px Helvetica, Arial, Sans-Serif;
     text-shadow: 1px 1px #fe4902, 
                  2px 2px #fe4902, 
                  3px 3px #fe4902;
                
      padding:40px 0 70px 0;
      text-align:center;
      filter: alpha(opacity=60); /* For IE8 and earlier */
    }
    
    #hed:hover {
     position: relative; 
     top: -3px; 
     left: -3px; 
     text-shadow: 1px 1px #fe4902, 
                  2px 2px #fe4902, 
                  3px 3px #fe4902, 
                  4px 4px #fe4902, 
                  5px 5px #fe4902, 
                  6px 6px #fe4902;
}

#footerss{
    height: 100px; 
    width:100%;
    position: absolute;
    left: 0;
    bottom: 100px;
    margin: 0 5px 0 5px;
}
		</style>
  </head>
  <body class="hold-transition register-page">
    <div class="register-box">
      <div class="register-logo">
        <a href="<?php echo base_url(); ?>"><b>Register</b> <span style="text-transform: uppercase;"><?php $web=$this->uut->namadomain(base_url()); $aa=explode(".",$web);echo $aa[0]; ?></span></a>
      </div>

      <div class="register-box-body">
        <p class="login-box-msg">Daftarkan keanggotaan baru</p>
        <form action="#" id="form_isian" method="post">
          <div class="form-group has-feedback">
            <input type="hidden" class="form-control" id="id_register" placeholder="id_register" style="display:none;">
            <input type="hidden" class="form-control" id="temp" placeholder="temp" style="display:none;">
            <input type="text" class="form-control" id="nama" placeholder="Nama Lengkap">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="nik" placeholder="NIK/No.KTP">
            <span class="glyphicon glyphicon-modal-window form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="nomot_telp" placeholder="No. HP">
            <span class="glyphicon glyphicon-phone form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="email" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="alamat" placeholder="Alamat">
            <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="keterangan" placeholder="Keperluan">
            <span class="glyphicon glyphicon-map-marker form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <select class="form-control" id="id_kecamatan"></select>
          </div>
          <div class="form-group has-feedback">
            <select class="form-control" id="id_desa"></select>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <!--<input type="checkbox"> I agree to the <a href="#">terms</a>-->
                </label>
              </div>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat" id="simpan_register">Daftar</button>
            </div><!-- /.col -->
          </div>
        </form>

        <div class="social-auth-links text-center">
          <p>- OR -</p><!--
          <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign up using Facebook</a>
          <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign up using Google+</a>-->
        </div>

        <a href="<?php echo base_url(); ?>login" class="text-center">Saya sudah memiliki keanggotaan</a>
        <p>Silahkan tunggu makimal 3x24 jam, login menggunakan username: <span class="text-danger">NIK</span> Password: <span class="text-danger">No.HP</span></p>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->
    
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script>
  function load_option_kecamatan() {
    $('#id_kecamatan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/option_kecamatan_by_id_kabupaten/',
      success: function(html) {
        $('#id_kecamatan').html('<option value="semua">Pilih Kecamatan</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_option_kecamatan();
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var id_kecamatan = $('#id_kecamatan').val();
      load_option_desa(id_kecamatan);
    });
  });
</script>
<script>
  function load_option_desa(id_kecamatan) {
    $('#id_desa').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html('<option value="semua">Pilih Desa</option>  '+html+'');
      }
    });
  }
</script>

<script>
  function AfterSavedRegister() {
    $('#nama, #nomot_telp, #email, #alamat, #nik, #id_kecamatan, #id_desa, #keterangan, #temp').val('');
  }
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_register').on('click', function(e) {
      e.preventDefault();
      $('#overlay_form_input').show();
      $('#pesan_terkirim').show();
      var parameter = [ 'nama', 'nomot_telp', 'email', 'alamat', 'nik', 'id_kecamatan', 'id_desa', 'keterangan', 'temp' ];
			InputValid(parameter);
      
      var parameter = {};
      parameter["nama"] = $("#nama").val();
      parameter["nomot_telp"] = $("#nomot_telp").val();
      parameter["email"] = $("#email").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["nik"] = $("#nik").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["keterangan"] = $("#keterangan").val();
      parameter["temp"] = $("#temp").val();
      var url = '<?php echo base_url(); ?>register/simpan_register';
      var parameterRv = [ 'nama', 'nomot_telp', 'email', 'alamat', 'nik', 'id_kecamatan', 'id_desa', 'keterangan', 'temp' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedRegister();
      }
    });
  });
</script>

    <script type="text/javascript">
      //----------------------------------------------------------------------------------------------
      function reset() {
        $('#toggleCSS').attr('href', '<?php echo base_url(); ?>css/alertify/alertify.default.css');
        alertify.set({
          labels: {
            ok: 'OK',
            cancel: 'Cancel'
          },
          delay: 5000,
          buttonReverse: false,
          buttonFocus: 'ok'
        });
      }
      //----------------------------------------------------------------------------------------------
    </script>
		<!-- jQuery 2.1.3 
    <script src="<?php echo base_url(); ?>Template/jquery.min.js"></script>-->
    <script src="<?php echo base_url(); ?>boots/plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url(); ?>boots/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url(); ?>boots/plugins/slimScroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- FastClick -->
    <script src='<?php echo base_url(); ?>boots/plugins/fastclick/fastclick.min.js'></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>boots/dist/js/app.min.js" type="text/javascript"></script>
    <!-- AdminLTE for demo purposes -->
		<!-- uut -->
		<script src="<?php echo base_url(); ?>js/uut.js"></script>
		<!-- alertify -->
		<script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
		<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
  </body>
</html>