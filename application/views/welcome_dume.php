<?php
  $web=$this->uut->namadomain(base_url());
  ?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
    <!-- Meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
    <meta name="author" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>,">
    <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>">
    <meta name="og:description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
    <meta name="og:url" content="<?php echo base_url(); ?>"/>
    <meta name="og:title" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>"/>
    <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
    <meta name="og:keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
    <!-- Favicon -->
    <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300italic,400,400italic,500,700,700italic,900" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:900" rel="stylesheet" type="text/css">
    <!-- Icon Font -->
    <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles1.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <!-- Alert Confirmation -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/Typeahead-BS3-css.css" id="toggleCSS" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>boots/dist/css/bootstrap-timepicker.min.css" />
		<?php
        $web=$this->uut->namadomain(base_url());
				if($status_komponen==1){
					$where0 = array(
						'status' => 1,
						'domain' => $web
						);
					$this->db->where($where0);
					$this->db->limit(1);
					$query0 = $this->db->get('header_website');
					$header_website = 'logo_header.png';
					foreach ($query0->result() as $row0)
						{
							$header_website = $row0->file_name;
						}
				}else{$header_website='logo_header.png';}
		?>
        <style>
          #headere {
            position: relative;
            height: 123px;
            top: 0;
            transition: all 0.2s ease 0s;
            width: 100%;
            background-image: url('<?php echo base_url(); ?>media/upload/<?php echo $header_website; ?>');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: 100% auto;
          }
        </style>
  </head>
  <body>
    <div id="main" class="header-style1">
		
	  <header class="header-wrapper clearfix">
		<div class="header" style="background:#fff;">
				<div id="headere">
				</div>
		</div>
		<!-- .header -->
		<div class="header" id="header">
		  <div class="container">
			<!-- Mobile Menu Button -->
			<a class="navbar-toggle collapsed" id="nav-button" href="#mobile-nav">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</a><!-- .navbar-toggle -->
			<!-- Main Nav Wrapper -->
			<nav class="navbar mega-menu">
			  <!-- .logo -->
			  <!-- Navigation Menu -->
			  <!-- .navbar-collapse -->              
			  <!-- End Navigation Menu -->
			  <div class="navbar-collapse collapse" id="hornav">
				<?php echo ''.$menu_atas.''; ?>
				<!-- .nav .navbar-nav -->
			  </div>
			</nav>
			<!-- .navbar --> 
		  </div>
		  <!-- .container -->
		</div>
		<!-- .header -->
	  </header>

      <!-- Begin Main Wrapper -->
		<div class="row" style="padding-top:130px;">
			<?php $this -> load -> view('modul/slide.php');  ?>
		</div>
		<div class="container main-wrapper">
			<div class="main-content mag-content">
				<div class="row">
					<div class="col-md-8">
						<div class="row">
							<section class="admag-block">
									<div class="row">
										<div class="col-md-12">
											<h3 class="block-title"><span>Pilihan</span></h3>
										</div>
									</div>
									<div class="col-md-12">
										<?php if(!empty($galery_berita)){ echo $galery_berita; } ?>
									</div>
							</section>
							<?php $this -> load -> view($main_view);  ?>
							<?php if(!empty($isi_posting)){ $this -> load -> view('modul/'.$nama_modul.''); } ?>
						</div>
					</div>
					<!-- End Left big column -->
					<div class="col-md-4" data-stickycolumn>
						<aside class="sidebar clearfix">
							<div class="widget">
								<h3 class="block-title"><span>Pengumuman</span></h3>
								<div class="row">
									<div class="col-md-12">
										<div class="flexslider">
											<div class="featured-slider">
												<?php $this -> load -> view('modul/pengumuman.php');  ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="widget">
								<h3 class="block-title"><span>Event </span></h3>
								<div class="row">
									<div class="col-md-12">
										<div class="flexslider">
											<div class="featured-slider">
												<?php $this -> load -> view('modul/event.php');  ?>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="widget">
								<h3 class="block-title"><span>Medsos </span></h3>
								<div class="row">
									<div class="col-md-12">
										<div class="flexslider">
											<div class="featured-slider">
                      <?php if(!empty( $KolomPalingBawah )){ echo $KolomPalingBawah; } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</aside>
					</div>
					<!-- End last column -->         
				</div>
			</div>
		</div>
      <!-- .main-wrapper -->
		
      <!-- Footer -->
      <footer class="footer source-org vcard copyright clearfix" id="footer" role="contentinfo">
        <div class="footer-main">
          <div class="fixed-main">
            <div class="container">
              <div class="mag-content">
                <div class="row">
                  <div class="col-md-4">
                    <div class="footer-block clearfix">
                      <p class="clearfix">
                        <a class="logo" href="index.php" title="" rel="home">
                        <span><?php if(!empty( $keterangan )){ echo $keterangan; } ?></span>
                        </a><!-- .logo -->
                      </p>
                      <p class="description">
												<address class="margin-bottom-40">
													<?php if(!empty( $alamat )){ echo $alamat; } ?><br>
													Phone: <?php if(!empty( $telpon )){ echo $telpon; } ?><br>
													Email: <a href="mailto:<?php if(!empty( $email )){ echo $email; } ?>"><?php if(!empty( $email )){ echo $email; } ?></a><br>
													Website: <a href="https://<?php echo $web; ?>">https://<?php echo $web; ?></a>
												</address>
                      </p>
                      <ul class="social-list clearfix">
                        <li class="social-facebook">
                          <a href="<?php if(!empty( $facebook )){ echo $facebook; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Facebook">
                          <i class="fa fa-facebook"></i>
                          </a>
                        </li>
                        <li class="social-twitter" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Twitter">
                          <a href="<?php if(!empty( $twitter )){ echo $twitter; } ?>">
                          <i class="fa fa-twitter"></i>
                          </a>
                        </li>
                        <li class="social-gplus">
                          <a href="<?php if(!empty( $google )){ echo $google; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Google+">
                          <i class="fa fa-google-plus"></i>
                          </a>
                        </li>
                        <li class="social-youtube">
                          <a href="<?php if(!empty( $youtube )){ echo $youtube; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Youtube">
                          <i class="fa fa-youtube"></i>
                          </a>
                        </li>
                        <li class="social-instagram">
                          <a href="<?php if(!empty( $instagram )){ echo $instagram; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Instagram">
                          <i class="fa fa-instagram"></i>
                          </a>
                        </li>
                        <li class="social-pinterest">
                          <a href="<?php if(!empty( $pinterest )){ echo $pinterest; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Pinterest">
                          <i class="fa fa-pinterest"></i>
                          </a>
                        </li>
                        <li class="social-rss">
                          <a href="<?php if(!empty( $rss )){ echo $rss; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="RSS">
                          <i class="fa fa-rss"></i>
                          </a>
                        </li>
                      </ul>
                      <!-- BEGIN: Powered by Supercounters.com -->
                      </center>
                      <!-- END: Powered by Supercounters.com -->
                    </div>
                    <!-- Footer Block -->
                  </div>
                  <div class="col-md-5">
                    <div class="footer-block clearfix">
                      <!--<h3 class="footer-title"><a >Other Link</a></h3>
                        <ul class="tags-widget"><li><a href="#"><img src="xxi/iklan/13_Logo_Branding_Jateng_Gayeng.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/22_Logo_Visit_Jawa_Tengah_cpy.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/34_Pesona_Indonesia.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/45_Wonderful_Indonesia.png" width="55"></a></li><li><a href="1"><img src="xxi/iklan/56_BPPD_Jateng.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/67_logo_asita.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/78_PHRI.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/89_Logo_AWAI.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/910_Logo_ASPPI.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1012_HPI.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1111_IMG-20161126-WA0004.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1213_PeBeMas.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1314_FK_Deswita.jpg" width="55"></a></li><li><a href="1"><img src="xxi/iklan/" width="55"></a></li></ul>-->
                      <?php if(!empty( $KolomKiriBawah )){ echo $KolomKiriBawah; } ?>
                    </div>
                    <div class="footer-block clearfix">
                      <p class="clearfix">
                        <span class="description">
												<address class="margin-bottom-40">
													<span id="visitor"></span><br>
													<span id="hit_counter"></span>
												</address>
                        </span><!-- .logo -->
                      </p>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="footer-block clearfix">
                    </div>
                    <!-- Footer Block -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-bottom clearfix">
          <div class="fixed-main">
            <div class="container">
              <div class="mag-content">
                <div class="row">
                  <div class="col-md-12">
                    <p>Copyright <a href="https://<?php echo $web; ?>"> <?php if(!empty( $keterangan )){ echo $keterangan; } ?></a> © 2016. All Rights Reserved</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!-- End Footer -->
    </div>
    <!-- End Main -->
    <!-- Mobile Menu -->
    <nav id="mobile-nav">
      <?php echo ''.$menu_mobile.''; ?>
    </nav>
    <!-- / Mobile Menu -->
    <div id="go-top-button" class="fa fa-angle-up" title="Scroll To Top"></div>
    <div class="mobile-overlay" id="mobile-overlay"></div>
    <script>
      function LoadVisitor() {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            table:'visitor'
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>visitor/simpan_visitor/',
          success: function(html) {
            $('#visitor').html('Total Pengunjung '+html+' ');
          }
        });
      }
    </script>
    <script>
      function LoadHitCounter() {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            current_url:'<?php echo current_url(); ?>'
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>visitor/hit_counter/',
          success: function(html) {
            $('#hit_counter').html('Total Pembaca '+html+' ');
            $('#hit_counter_posting').html(''+html+'');
          }
        });
      }
    </script>
    <script type="text/javascript">
      $(document).ready(function() {
        LoadVisitor();
        LoadHitCounter();
      });
    </script>
    <!-- Jquery js -->
    <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/jquery.min.js" type="text/javascript"></script>
    <!-- Modernizr -->
    <script src="<?php echo base_url(); ?>assets/portfolio/js/modernizr.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Plugins js -->
    <script src="<?php echo base_url(); ?>assets/portfolio/js/plugins.js"></script>
    <!-- Theme js -->
    <script src="<?php echo base_url(); ?>assets/portfolio/js/script.js"></script>
    <script src="<?php echo base_url(); ?>js/uut.js"></script>
    <!-- alertify -->
    <script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
  </body>
</html>