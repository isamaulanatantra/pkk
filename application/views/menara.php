<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $judul_halaman; ?> <?php echo base_url(); ?></title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
  <!-- BARU SAJA 3.0.5 -->
	
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.costum.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	
  <!-- END BARU SAJA 3.0.5 <script src="<?php echo base_url(); ?>js/jquery.min.js"></script> -->
	
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="../../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="../../https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
		
		<!-- Alert Confirmation -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/Typeahead-BS3-css.css" id="toggleCSS" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>boots/dist/css/bootstrap-timepicker.min.css" />
		<!-- Progress bar -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-combined.min.css" id="toggleCSS" />
		
<script type="text/javascript" src="https://cellplan.wonosobokab.go.id/bantuls/js/js_jquery-1.8.3.js"></script>
<script type="text/javascript" src="https://cellplan.wonosobokab.go.id/bantuls/js/jquery.validate.js"></script>
<script type="text/javascript" src="https://cellplan.wonosobokab.go.id/bantuls/js/zxml.js"></script>
<script type="text/javascript" src="https://cellplan.wonosobokab.go.id/bantuls/js/request.js"></script>
<script type="text/javascript" src="https://cellplan.wonosobokab.go.id/bantuls/js/ruler-rem.js"></script>
<script type="text/javascript" src="https://cellplan.wonosobokab.go.id/bantuls/js/labels.js"></script>
<script type="text/javascript" src="https://cellplan.wonosobokab.go.id/bantuls/js/candidate-marker.js"></script>
	
<script type="text/javascript" src="https://cellplan.wonosobokab.go.id/bantuls/js/towermarkerspg.js"></script>
<script type="text/javascript" src="https://cellplan.wonosobokab.go.id/bantuls/js/zonemarkerspg.js"></script>
<!-- ECharts --> 
<script src="https://cellplan.wonosobokab.go.id/media/echarts/dist/echarts.min.js"></script> 
<script src="https://cellplan.wonosobokab.go.id/media/echarts/map/js/world.js"></script>
	
<script language="javascript" type="text/javascript">
function clearText(field){

    if (field.defaultValue == field.value) field.value = '';
    else if (field.value == '') field.value = field.defaultValue;

}
//putMarker() 

$(document).ready(function(){


// disable OK button before input coordinates
    var $submit = $("#crdbtn"),
        $inputs = $('input[type=text]');

    function checkEmpty() {

        // filter over the empty inputs

        return $inputs.filter(function() {
            return !$.trim(this.value);
        }).length === 0;
    }

    $inputs.on('keyup blur', function() {
        $submit.prop("disabled", !checkEmpty());
    }).keyup(); // trigger an initial blur
	
// disabled other button before coordinates submitted
/*$("#coordinput").validate({


errorElement: "em",
errorContainer: "#errordiv",
errorLabelContainer: "#errordiv",
errorPlacement: function(error,element) {
	error.appendTo("#errordiv");
                       return true;
                    },

   rules: {
       lat: {
       required: true,

     },
     lng: {
       required: true
     },
	 height: {
       required: true
     }
	},
   messages: {
        lat: {
       required: "- Latitude harus diisi!</br>",

     },
     lng: {
       required: "- Longitude harus diisi!",
     },
	   height: {
       required: "- Ketinggian harus diisi!",
     },
   }
	

});


$.validator.addMethod(

	  "regex",
	       function(value, element, regexp) {
          var check = false;
          var re = new RegExp(regexp);
          return this.optional(element) || re.test(value);
      },""
);

$("#markerLat").rules("add",{
   regex:"^-[7-8]\.([0-9]{5,7})$",
 messages: {
 regex: "- Isikan format Latitude dengan benar</br>"
},

});

$("#markerLng").rules("add",{
   regex:"^110\.([0-9]{5,7})$",
 messages: {
 regex: "- Isikan format Longitude dengan benar..</br>"
}
});

$("#towerHeight").rules("add",{
   regex:"^([0-9]{1,3})$",
 messages: {
 regex: "- Isikan format ketinggian dengan benar..</br>"
}
});


$("#crdbtn").click(function() {
    var isvalid=($("#coordinput").valid());
	if (isvalid==false) {
	//alert("Data yang anda masukkan tidka valid. periksa lagi.");
	} else {
	putMarker();
	}

    return false;
});
   
   // valid

*/
  

 });
</script>

</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition">
    <!-- Site wrapper -->
  <div class="wrapper">
      
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
  	<div class="container">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link bg-primary elevation-1" data-widget="pushmenu" href="<?php echo base_url(); ?><?php echo $nama_halaman; ?>"><i class="fa fa-wechat"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a target="_blank" href="<?php echo base_url(); ?>" class="nav-link text-primary"><?php echo $keterangan ?></a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>
  	</div>

  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<div class="container">
		<span id="dispcoord" style="color:white; display:none"></span><span id="dispcoordclick" style="color:white; float:right"></span>

			<?php $this -> load -> view($main_view);  ?>
		
  	</div>
  </div>
  <!-- /.content-wrapper -->
	
  <footer class="main-footer">
  	<div class="container">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.6
    </div>
    <strong>Copyright &copy; 2018 <a href="https://diskominfo.wonosobokab.go.id/">Kominfo</a>.</strong> All rights
    reserved.
  	</div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- BARU -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>boots/plugins/jQuery/jQuery-2.1.3.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo base_url(); ?>boots/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>


<!-- END BARU -->

    <script type="text/javascript">
      //----------------------------------------------------------------------------------------------
      function reset() {
        $('#toggleCSS').attr('href', '<?php echo base_url(); ?>css/alertify/alertify.default.css');
        alertify.set({
          labels: {
            ok: 'OK',
            cancel: 'Cancel'
          },
          delay: 5000,
          buttonReverse: false,
          buttonFocus: 'ok'
        });
      }
      //----------------------------------------------------------------------------------------------
    </script>
		
            <script>
              function LoadVisitor() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    table:'visitor'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/simpan_visitor/',
                  success: function(html) {
                    $('#visitor').html('Total Pengunjung '+html+' ');
                  }
                });
              }
            </script>
            <script>
              function LoadHitCounter() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    current_url:'<?php echo current_url(); ?>'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/hit_counter/',
                  success: function(html) {
                    $('#hit_counter').html(''+html+' Kali ');
                  }
                });
              }
            </script>
            <script type="text/javascript">
            $(document).ready(function() {
              LoadVisitor();
              LoadHitCounter();
            });
            </script>
		<!-- uut -->
		<script src="<?php echo base_url(); ?>js/uut.js"></script>
		<!-- alertify -->
		<script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
    <!-- Morris.js charts -->
    <script src="<?php echo base_url(); ?>js/plugins/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>js/plugins/morris/morris.min.js" type="text/javascript"></script>
		<!--- -->
		<script src="<?php echo base_url(); ?>js/colorbox/jquery.colorbox.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap-typeahead.js"></script>
    <script src="<?php echo base_url(); ?>js/hogan-2.0.0.js"></script>
		<!-- daterangepicker -->
    <script src="<?php echo base_url(); ?>boots/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
		<script src="<?php echo base_url(); ?>boots/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>boots/dist/js/moment-with-locales.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>boots/dist/js/bootstrap-datetimepicker.js"></script>
    <!-- InputMask -->
    <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script>
      $(function () {
        $("[data-mask]").inputmask();
      });
    </script>
		<!-- Start editor-->
		<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script>
		
    
	
  </body>
</html>
