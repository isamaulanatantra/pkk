<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
      <li><a data-toggle="tab" href="#tab_2" id="klik_tab_tampil" >Tampil</a></li>
      <li><a data-toggle="tab" href="#tab_3" id="klik_tab_tampil_arsip" >Arsip</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title" id="judul_formulir">FORMULIR INPUT</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" id="form_baru"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=posting" enctype="multipart/form-data">
                  <div class="box-body">
										<div class="form-group" style="display:none;">
											<label for="temp">temp</label>
											<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="mode">mode</label>
											<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_posting">id_posting</label>
											<input class="form-control" id="id_posting" name="id" value="" placeholder="id_posting" type="text">
										</div>
										<div class="form-group">
											<label for="posisi">Posisi</label>
											<select class="form-control" id="posisi" name="posisi" >
                      <option value="menu_atas">Menu Atas</option>
                      <!--<option value="menu_kanan">Menu Kanan</option>-->
                      <option value="menu_kiri">Menu Kiri</option>
                      <option value="independen">Independen</option>
                      </select>
										</div>
										<div class="form-group">
											<label for="parent">Parent</label>
											<select class="form-control" id="parent" name="parent" >
                      </select>
										</div>
										<div class="form-group">
											<label for="urut">Urut</label>
											<input class="form-control" id="urut" name="urut" value="" placeholder="urut" type="text">
										</div>
										<div class="form-group">
											<label for="icon">Icon</label>
                      <select class="form-control" id="icon" name="icon" >
                      <option value="">Pilih Icon</option>
                      <option value="fa-home">fa-home</option>
                      <option value="fa-gears">fa-gears</option>
                      <option value="fa-th">fa-th</option>
                      <option value="fa-font">fa-font</option>
                      <option value="fa-comment">fa-comment</option>
                      <option value="fa-cogs">fa-cogs</option>
                      <option value="fa-cloud-download">fa-cloud-download</option>
                      <option value="fa-bar-char">fa-bar-char</option>
                      <option value="fa-phone">fa-phone</option>
                      <option value="fa-envelope">fa-envelope</option>
                      <option value="fa-link">fa-link</option>
                      <option value="fa-tasks">fa-tasks</option>
                      <option value="fa-users">fa-users</option>
                      <option value="fa-signal">fa-signal</option>
                      <option value="fa-coffee">fa-coffee</option>
                      </select>
                      <div id="iconselected"></div>
										</div>
										<div class="form-group">
											<label for="highlight">High Light</label>
											<select class="form-control" id="highlight" name="highlight" >
                      <option value="0">Tidak</option>
                      <option value="1">Ya</option>
                      </select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="tampil_menu">Tampil Menu</label>
											<select class="form-control" id="tampil_menu" name="tampil_menu" >
                      <option value="1">Ya</option>
                      <option value="0">Tidak</option>
                      </select>
										</div>
										<div class="form-group">
											<label for="tampil_menu_atas">Tampil Menu Atas</label>
											<select class="form-control" id="tampil_menu_atas" name="tampil_menu_atas" >
                      <option value="1">Ya</option>
                      <option value="0">Tidak</option>
                      </select>
										</div>
										<div class="form-group">
											<label for="judul_posting">Judul Halaman</label>
											<input class="form-control" id="judul_posting" name="judul_posting" value="" placeholder="Judul Halaman" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="isi_posting">Isi Halaman</label>
											<input class="form-control" id="isi_posting" name="isi_posting" value="" placeholder="Isi Halaman" type="text">
										</div>
                    <div class="row">
                    <textarea id="editor_isi_posting"></textarea>
                    </div>
										<div class="form-group">
											<label for="kata_kunci">Kata Kunci</label>
											<input class="form-control" id="kata_kunci" name="kata_kunci" value="" placeholder="kata_kunci" type="text">
										</div>
										<div class="form-group">
											<label for="keterangan">Keterangan Halaman</label>
											<input class="form-control" id="keterangan" name="keterangan" value="" placeholder="Keterangan" type="text">
										</div>
										<div class="alert alert-info alert-dismissable">
											<div class="form-group">
												<label for="remake">Keterangan Lampiran </label>
												<input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran " type="text">
											</div>
											<div class="form-group">
												<label for="myfile">File Lampiran </label>
												<input type="file" size="60" name="myfile" id="file_lampiran" >
											</div>
											<div id="ProgresUpload">
												<div id="BarProgresUpload"></div>
												<div id="PersenProgresUpload">0%</div >
											</div>
											<div id="PesanProgresUpload"></div>
										</div>
										<div class="alert alert-info alert-dismissable">
											<h3 class="box-title">Data Lampiran </h3>
											<table class="table table-bordered">
												<tr>
													<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
												</tr>
												<tbody id="tbl_attachment_posting">
												</tbody>
											</table>
										</div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="simpan_posting">SIMPAN</button>
                    <button type="submit" class="btn btn-primary" id="update_posting" style="display:none;">UPDATE</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                Data Posting
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="kata_kunci_filter" />
                  <select name="limit_data_posting" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_posting">
                    <option value="10">10 Per-Halaman</option>
                    <option value="20">20 Per-Halaman</option>
                    <option value="50">50 Per-Halaman</option>
                    <option value="999999999">Semua</option>
                  </select>
                  <select name="urut_data_posting" class="form-control input-sm pull-right" style="width: 150px;" id="urut_data_posting">
                    <option value="judul_posting">Judul Posting</option>
                  </select>
                  <select name="posisi_filter" class="form-control input-sm pull-right" style="width: 150px;" id="posisi_filter">
                    <option value="">Pilih Posisi</option>
                    <option value="menu_atas">Menu Atas</option>
                    <option value="menu_kiri">Menu Kiri</option>
                    <option value="independen">Independen</option>
                  </select>
                  <select name="parent_filter" class="form-control input-sm pull-right" style="width: 150px;" id="parent_filter">
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default" id="search_kata_kunci"><i class="fa fa-search"></i></button>
                    <div class="overlay" id="spinners_posting" style="display:none;">
                      <i class="fa fa-refresh fa-spin"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-body table-responsive no-padding">
                        <div id="tblExport">
                          <table class="table table-hover">
                            <tbody>
                              <tr id="header_exel">
                                <th>NO</th> 
                                <th>Tema</th>
                                <th>Menu</th>
                                <th>Highlight</th>
                                <th>Publish</th>
                                <th>By</th>
                                <th>Proses</th>
                              </tr>
                            </tbody>
                            <tbody id="tbl_utama_posting">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                        </ul>
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab_3">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                Arsip Posting
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="kata_kunci_filter" />
                  <select name="limit_data_posting" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_posting">
                    <option value="10">10 Per-Halaman</option>
                    <option value="20">20 Per-Halaman</option>
                    <option value="50">50 Per-Halaman</option>
                    <option value="999999999">Semua</option>
                  </select>
                  <select name="urut_data_posting" class="form-control input-sm pull-right" style="width: 150px;" id="urut_data_posting">
                    <option value="judul_posting">Judul Posting</option>
                  </select>
                  <select name="posisi_filter" class="form-control input-sm pull-right" style="width: 150px;" id="posisi_filter">
                    <option value="">Pilih Posisi</option>
                    <option value="menu_atas">Menu Atas</option>
                    <option value="menu_kiri">Menu Kiri</option>
                    <option value="independen">Independen</option>
                  </select>
                  <select name="parent_filter" class="form-control input-sm pull-right" style="width: 150px;" id="parent_filter">
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default" id="search_kata_kunci"><i class="fa fa-search"></i></button>
                    <div class="overlay" id="spinners_posting" style="display:none;">
                      <i class="fa fa-refresh fa-spin"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-body table-responsive no-padding">
                        <div id="tblExport">
                          <table class="table table-hover">
                            <tbody>
                              <tr id="header_exel">
                                <th>NO</th> 
                                <th>Tema</th>
                                <th>Menu</th>
                                <th>Highlight</th>
                                <th>Publish</th>
                                <th>By</th>
                                <th>Proses</th>
                              </tr>
                            </tbody>
                            <tbody id="tbl_utama_posting_arsip">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="box-footer clearfix">
                        <ul class="pagination pagination-sm no-margin pull-right" id="pagination_arsip">
                        </ul>
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
  function load_option_posting_by_posisi_filter(posisi) {
    $('#parent_filter').html('');
    $('#spinners_posting').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        posisi: posisi
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>halaman/load_the_option_by_posisi_filter/',
      success: function(html) {
        $('#parent_filter').html('<option value="0">Pilih Parent</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#posisi_filter').on('change', function(e) {
      e.preventDefault();
      var posisi = $('#posisi_filter').val();
      var halaman = 1;
      var limit_data_posting = $('#limit_data_posting').val();
      var limit = limit_per_page_custome(limit_data_posting);
      var kata_kunci = $('#kata_kunci_filter').val();
      var urut_data_posting = $('#urut_data_posting').val();
      var parent = $('#parent_filter').val();
      load_data_posting(halaman, limit, kata_kunci, urut_data_posting, parent);
      load_option_posting_by_posisi_filter(posisi);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#parent_filter').on('change', function(e) {
      e.preventDefault();
        var halaman = 1;
        var limit_data_posting = $('#limit_data_posting').val();
        var limit = limit_per_page_custome(limit_data_posting);
        var kata_kunci = $('#kata_kunci_filter').val();
        var urut_data_posting = $('#urut_data_posting').val();
        var parent = $('#parent_filter').val();
        load_data_posting(halaman, limit, kata_kunci, urut_data_posting, parent);
    });
  });
</script>

<script>
  function load_data_posting(halaman, limit, kata_kunci, urut_data_posting, parent) {
    $('#tbl_utama_posting').html('');
    $('#spinners_data').show();
    var limit_data_posting = $('#limit_data_posting').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:halaman,
        limit:limit,
        kata_kunci:kata_kunci,
        urut_data_posting:urut_data_posting,
        parent:parent
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>halaman/json_all_posting/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_posting="' + json[i].id_posting + '" id="' + json[i].id_posting + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
          // var str = json[i].judul_posting;
          tr += '<td valign="top"><a target="_blank" href="<?php echo base_url(); ?>postings/faqs/' + json[i].id_posting + '">'+ json[i].judul_posting+'</td>';
          if( json[i].tampil_menu_atas == 1 ){
            tr += '<td valign="top" id="td_2_'+i+'"><p class="text-success">Ya</p></td>';
          }
          else{
            tr += '<td valign="top" id="td_3_'+i+'"><p class="text-danger">Tidak</p></td>';
          }
          if( json[i].highlight == 1 ){
            tr += '<td valign="top" id="td_2_'+i+'"><p class="text-success">Ya</p></td>';
          }
          else{
            tr += '<td valign="top" id="td_3_'+i+'"><p class="text-danger">Tidak</p></td>';
          }
          if( json[i].status == 1 ){
            tr += '<td valign="top" id="td_2_'+i+'"><a href="#" id="inaktifkan" class="text-info"><i class="fa fa-check-circle"></i> Ya</a></td>';
          }
          else{
            tr += '<td valign="top" id="td_3_'+i+'"><a href="#" id="aktifkan" class="text-danger"><i class="fa fa-times-circle"></i> Tidak</a></td>';
          }
          tr += '<td valign="top">' + json[i].nama_pengguna + '</td>';
          tr += '<td valign="top"><a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i> </a> &nbsp;&nbsp;<a href="#" id="del_ajax"> <i class="fa fa-cut"></i></a></td>';
          
          tr += '</tr>';
        }
        $('#tbl_utama_posting').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
    load_pagination();
  }
</script>
<!----------------------->

<script>
  function load_data_posting_arsip(halaman, limit, kata_kunci, urut_data_posting, parent) {
    $('#tbl_utama_posting_arsip').html('');
    $('#spinners_data').show();
    var limit_data_posting = $('#limit_data_posting').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:halaman,
        limit:limit,
        kata_kunci:kata_kunci,
        urut_data_posting:urut_data_posting,
        parent:parent
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>halaman/json_all_posting_arsip/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_posting="' + json[i].id_posting + '" id="' + json[i].id_posting + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
          // var str = json[i].judul_posting;
          tr += '<td valign="top"><a target="_blank" href="<?php echo base_url(); ?>postings/faqs/' + json[i].id_posting + '">'+ json[i].judul_posting+'</td>';
          if( json[i].tampil_menu_atas == 1 ){
            tr += '<td valign="top" id="td_2_'+i+'"><p class="text-success">Ya</p></td>';
          }
          else{
            tr += '<td valign="top" id="td_3_'+i+'"><p class="text-danger">Tidak</p></td>';
          }
          if( json[i].highlight == 1 ){
            tr += '<td valign="top" id="td_2_'+i+'"><p class="text-success">Ya</p></td>';
          }
          else{
            tr += '<td valign="top" id="td_3_'+i+'"><p class="text-danger">Tidak</p></td>';
          }
          if( json[i].status == 1 ){
            tr += '<td valign="top" id="td_2_'+i+'"><a href="#" id="inaktifkan" class="text-info"><i class="fa fa-check-circle"></i> Ya</a></td>';
          }
          else{
            tr += '<td valign="top" id="td_3_'+i+'"><a href="#" id="aktifkan" class="text-danger"><i class="fa fa-times-circle"></i> Tidak</a></td>';
          }
          tr += '<td valign="top">' + json[i].nama_pengguna + '</td>';
          tr += '<td valign="top"><a href="#" id="restore_ajax"><i class="fa fa-mail-reply-all"></i> Restore</a></td>';
          
          tr += '</tr>';
        }
        $('#tbl_utama_posting_arsip').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
    load_pagination_arsip();
  }
</script>
<!----------------------->

<script type="text/javascript">
  /* $(document).ready(function() {
    // cek_tema();
        var halaman = 1;
        var limit_data_posting = $('#limit_data_posting').val();
        var limit = limit_per_page_custome(limit_data_posting);
        var kata_kunci = $('#kata_kunci').val();
        var urut_data_posting = $('#urut_data_posting').val();
        var parent = $('#parent').val();
        load_data_posting(halaman, limit, kata_kunci, urut_data_posting, parent);
        // load_table(halaman, limit, kata_kunci, urut_data_posting);
  }); */
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_posting').on('click', '#inaktifkan', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan dipubish?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>halaman/inaktifkan',
          success: function(html) {
            alertify.success('Data berhasil publish');
            var halaman = 1;
            var limit_data_posting = $('#limit_data_posting').val();
            var limit = limit_per_page_custome(limit_data_posting);
            var kata_kunci = $('#kata_kunci_filter').val();
            var urut_data_posting = $('#urut_data_posting').val();
            load_data_posting(halaman, limit, kata_kunci, urut_data_posting);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_posting').on('click', '#aktifkan', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan dipubish?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>halaman/aktifkan',
          success: function(html) {
            alertify.success('Data berhasil publish');
            var halaman = 1;
            var limit_data_posting = $('#limit_data_posting').val();
            var limit = limit_per_page_custome(limit_data_posting);
            var kata_kunci = $('#kata_kunci_filter').val();
            var urut_data_posting = $('#urut_data_posting').val();
            load_data_posting(halaman, limit, kata_kunci, urut_data_posting);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#limit_data_posting').on('change', function(e) {
      e.preventDefault();
        var halaman = 1;
        var limit_data_posting = $('#limit_data_posting').val();
        var limit = limit_per_page_custome(limit_data_posting);
        var kata_kunci = $('#kata_kunci_filter').val();
        var urut_data_posting = $('#urut_data_posting').val();
        var parent = $('#parent_filter').val();
        load_data_posting(halaman, limit, kata_kunci, urut_data_posting, parent);
      // load_table(halaman, limit, kata_kunci, urut_data_posting);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#kata_kunci_filter').on('change', function(e) {
      e.preventDefault();
        var halaman = 1;
        var limit_data_posting = $('#limit_data_posting').val();
        var limit = limit_per_page_custome(limit_data_posting);
        var kata_kunci = $('#kata_kunci_filter').val();
        var urut_data_posting = $('#urut_data_posting').val();
        var parent = $('#parent_filter').val();
        load_data_posting(halaman, limit, kata_kunci, urut_data_posting, parent);
      // load_table(halaman, limit, kata_kunci, urut_data_posting);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        posisi: 'ok'
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>posting/cek_default_halaman/',
      success: function(text) {
        //alert(text);
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#icon').on('change', function(e) {
      e.preventDefault();
      var xa = $('#icon').val();
      $("#iconselected").html('<span class="fa '+xa+' ">'+xa+'</span>');
    });
  });
</script>

<script>
  function load_option_posting_by_posisi(posisi) {
    $('#parent').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        posisi: posisi
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>posting/load_the_option_by_posisi/',
      success: function(html) {
        $('#parent').html('<option value="0">Utama</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#posisi').on('change', function(e) {
      e.preventDefault();
      var posisi = $('#posisi').val();
      load_option_posting_by_posisi(posisi);
    });
  });
</script>

<script>
  function load_option_posting() {
    $('#parent').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>posting/load_the_option/',
      success: function(html) {
        $('#parent').html('<option value="0">Utama</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_option_posting();
});
</script>

<script>
  function load_pagination() {
    var limit_data_posting = $('#limit_data_posting').val();
    var limit = limit_per_page_custome(limit_data_posting);
    var kata_kunci = $('#kata_kunci_filter').val();
    var urut_data_posting = $('#urut_data_posting').val();
    var parent = $('#parent_filter').val();
    var tr = '';
    var td = TotalData('<?php echo base_url(); ?>halaman/total_posting/?limit='+limit_per_page_custome(limit_data_posting)+'&kata_kunci='+kata_kunci+'&urut_data_posting='+urut_data_posting+'&parent='+parent+'');
    for (var i = 1; i <= td; i++) {
      tr += '<li page="'+i+'" id="'+i+'"><a class="update_id" href="#">'+i+'</a></li>';
    }
    $('#pagination').html(tr);
  }
</script>
 
<script>
  function load_pagination_arsip() {
    var limit_data_posting = $('#limit_data_posting').val();
    var limit = limit_per_page_custome(limit_data_posting);
    var kata_kunci = $('#kata_kunci_filter').val();
    var urut_data_posting = $('#urut_data_posting').val();
    var parent = $('#parent_filter').val();
    var tr = '';
    var td = TotalData('<?php echo base_url(); ?>halaman/total_posting/?limit='+limit_per_page_custome(limit_data_posting)+'&kata_kunci='+kata_kunci+'&urut_data_posting='+urut_data_posting+'&parent='+parent+'');
    for (var i = 1; i <= td; i++) {
      tr += '<li page="'+i+'" id="'+i+'"><a class="update_id" href="#">'+i+'</a></li>';
    }
    $('#pagination_arsip').html(tr);
  }
</script>
 
<script>
  function AfterSavedPosting() {
    $('#id_posting, #judul_posting, #isi_posting, #urut, #posisi,  #tampil_menu_atas, #icon, #keterangan').val('');
    $('#tbl_attachment_posting').html('');
    $('#PesanProgresUpload').html('');
    load_option_posting();
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
  // load_pagination();
});
</script>
 
<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_tampil').on('click', function(e) {
        var halaman = 1;
        var limit_data_posting = $('#limit_data_posting').val();
        var limit = limit_per_page_custome(limit_data_posting);
        var kata_kunci = $('#kata_kunci').val();
        var urut_data_posting = $('#urut_data_posting').val();
        var parent = $('#parent').val();
        load_data_posting(halaman, limit, kata_kunci, urut_data_posting, parent);
        // load_pagination();
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_tampil_arsip').on('click', function(e) {
        var halaman = 1;
        var limit_data_posting = $('#limit_data_posting').val();
        var limit = limit_per_page_custome(limit_data_posting);
        var kata_kunci = $('#kata_kunci_filter').val();
        var urut_data_posting = $('#urut_data_posting').val();
        var parent = $('#parent_filter').val();
        load_data_posting_arsip(halaman, limit, kata_kunci, urut_data_posting, parent);
        // load_pagination();
  });
});
</script>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_posting').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'posting',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_posting').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_posting').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_posting').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_posting').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_posting').on('click', '#del_ajax', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_posting"] = id_posting;
        var url = '<?php echo base_url(); ?>posting/hapus/';
        HapusData(parameter, url);
        $('[id_posting='+id_posting+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_posting6').on('click', '#restore_ajax', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan direstore?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_posting"] = id_posting;
        var url = '<?php echo base_url(); ?>posting/restore/';
        HapusData(parameter, url);
        $('[id_posting='+id_posting+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_posting').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_posting').hide();
    $('#update_posting').show();
    var id_posting = $(this).closest('tr').attr('id_posting');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_posting');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_posting').val(id_posting);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_posting:id_posting
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>posting/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#judul_posting').val(json[i].judul_posting);
            $('#parent').val(json[i].parent);
            $('#highlight').val(json[i].highlight);
            $('#tampil_menu').val(json[i].tampil_menu);
            $('#tampil_menu_atas').val(json[i].tampil_menu_atas);
            //$('#isi_posting').val(json[i].isi_posting);
            $('#urut').val(json[i].urut);
            $('#posisi').val(json[i].posisi);
            $('#icon').val(json[i].icon);
            $('#kata_kunci').val(json[i].kata_kunci);
            $('#keterangan').val(json[i].keterangan);
            CKEDITOR.instances.editor_isi_posting.setData(json[i].isi_posting);
          }
        }
      });
    AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_posting').show();
    $('#update_posting').hide();
    $('#tbl_attachment_posting').html('');
    $('#id_posting, #judul_posting, #highlight, #tampil_menu,  #tampil_menu_atas, #parent, #isi_posting, #urut, #posisi, #icon, #kata_kunci, #keterangan').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_posting').on('click', function(e) {
      e.preventDefault();
      var editor_isi_posting = CKEDITOR.instances.editor_isi_posting.getData();
      $('#isi_posting').val( editor_isi_posting );
      var parameter = [ 'judul_posting', 'highlight', 'tampil_menu', 'tampil_menu_atas', 'parent', 'parent', 'isi_posting', 'urut', 'posisi', 'icon', 'kata_kunci', 'keterangan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["judul_posting"] = $("#judul_posting").val();
      parameter["highlight"] = $("#highlight").val();
      parameter["tampil_menu"] = $("#tampil_menu").val();
      parameter["tampil_menu_atas"] = $("#tampil_menu_atas").val();
      parameter["parent"] = $("#parent").val();
      parameter["isi_posting"] = $("#isi_posting").val();
      parameter["urut"] = $("#urut").val();
      parameter["posisi"] = $("#posisi").val();
      parameter["icon"] = $("#icon").val();
      parameter["kata_kunci"] = $("#kata_kunci").val();
      parameter["keterangan"] = $("#keterangan").val();
      parameter["temp"] = $("#temp").val();
      var url = '<?php echo base_url(); ?>posting/simpan_posting';
      
      var parameterRv = [ 'judul_posting', 'highlight', 'tampil_menu', 'tampil_menu_atas', 'parent', 'isi_posting', 'urut', 'posisi', 'icon', 'temp', 'kata_kunci', 'keterangan' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedPosting();
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_posting').on('click', function(e) {
      e.preventDefault();
      var editor_isi_posting = CKEDITOR.instances.editor_isi_posting.getData();
      $('#isi_posting').val( editor_isi_posting );
      var parameter = [ 'judul_posting', 'highlight', 'tampil_menu', 'tampil_menu_atas', 'parent', 'isi_posting', 'urut', 'posisi', 'icon', 'kata_kunci', 'keterangan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["judul_posting"] = $("#judul_posting").val();
      parameter["highlight"] = $("#highlight").val();
      parameter["tampil_menu"] = $("#tampil_menu").val();
      parameter["tampil_menu_atas"] = $("#tampil_menu_atas").val();
      parameter["parent"] = $("#parent").val();
      parameter["isi_posting"] = $("#isi_posting").val();
      parameter["urut"] = $("#urut").val();
      parameter["posisi"] = $("#posisi").val();
      parameter["icon"] = $("#icon").val();
      parameter["kata_kunci"] = $("#kata_kunci").val();
      parameter["keterangan"] = $("#keterangan").val();
      parameter["temp"] = $("#temp").val();
      parameter["id_posting"] = $("#id_posting").val();
      var url = '<?php echo base_url(); ?>posting/update_posting';
      
      var parameterRv = [ 'judul_posting', 'highlight', 'tampil_menu', 'tampil_menu_atas', 'parent', 'isi_posting', 'urut', 'posisi', 'icon', 'id_posting', 'kata_kunci', 'keterangan' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#pagination').on('click', '.update_id', function(e) {
		e.preventDefault();
		var id = $(this).closest('li').attr('page');
		var halaman = id;
    var limit_data_posting = $('#limit_data_posting').val();
    var limit = limit_per_page_custome(limit_data_posting);
    var kata_kunci = $('#kata_kunci_filter').val();
    var urut_data_posting = $('#urut_data_posting').val();
    var parent = $('#parent_filter').val();
    // var limit = limit_per_page_custome(20000);
    // load_data_posting(halaman, limit);
    load_data_posting(halaman, limit, kata_kunci, urut_data_posting, parent);
    
	});
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#pagination_arsip').on('click', '.update_id', function(e) {
		e.preventDefault();
		var id = $(this).closest('li').attr('page');
		var halaman = id;
    var limit_data_posting = $('#limit_data_posting').val();
    var limit = limit_per_page_custome(limit_data_posting);
    var kata_kunci = $('#kata_kunci_filter').val();
    var urut_data_posting = $('#urut_data_posting').val();
    var parent = $('#parent_filter').val();
    // var limit = limit_per_page_custome(20000);
    // load_data_posting(halaman, limit);
    load_data_posting(halaman, limit, kata_kunci, urut_data_posting, parent);
    
	});
});
</script>
<!----------------------->
<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('editor_isi_posting');
	$(".textarea").wysihtml5();
});
</script>