
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Permohonan Informasi Publik</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Permohonan Informasi Publik</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link" href="#tab_1" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link active" href="#tab_2" data-toggle="tab" id="klik_tab_tampil">Data</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab" id="klik_tab_tampil_arsip">Arsip</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane" id="tab_1">
				
										<div class="row">
											<div class="col-sm-12 col-md-6">
											
													<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=permohonan_informasi_publik" enctype="multipart/form-data">
															<h3 id="judul_formulir">FORMULIR INPUT</h3>
															<div class="form-group" style="display:none;">
																<label for="temp">temp</label>
																<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
															</div>
															<div class="form-group" style="display:none;">
																<label for="mode">mode</label>
																<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
															</div>
															<div class="form-group" style="display:none;">
																<label for="id_permohonan_informasi_publik_get_by_id">id_permohonan_informasi_publik_get_by_id</label>
																<input class="form-control" id="id_permohonan_informasi_publik_get_by_id" name="id" value="" placeholder="id_permohonan_informasi_publik_get_by_id" type="text">
															</div>
															<div class="form-group" style="display:none;">
																<label for="id_posting_get_by_id">id_posting</label>
																<input class="form-control" id="id_posting_get_by_id" name="id_posting_get_by_id" value="" placeholder="id_posting_get_by_id" type="text">
															</div>
															<div class="form-group" style="display:none;">
																<label for="kategori_permohonan_informasi_publik_get_by_id">kategori_permohonan_informasi_publik</label>
																<input class="form-control" id="kategori_permohonan_informasi_publik_get_by_id" name="kategori_permohonan_informasi_publik_get_by_id" value="" placeholder="kategori_permohonan_informasi_publik_get_by_id" type="text">
															</div>
															<div class="form-group">
																<label for="nama">Nama</label>
																<input class="form-control" id="nama" name="nama" value="" placeholder="nama" type="text">
															</div>
															<div class="form-group">
																<label for="alamat">Alamat</label>
																<input class="form-control" id="alamat" name="alamat" value="" placeholder="alamat" type="text">
															</div>
															<div class="form-group">
																<label for="pekerjaan">Pekerjaan</label>
																<input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="pekerjaan" type="text">
															</div>
															<div class="form-group">
																<label for="nomor_telp">Nomor telp</label>
																<input class="form-control" id="nomor_telp" name="nomor_telp" value="" placeholder="nomor_telp" type="text">
															</div>
															<div class="form-group">
																<label for="email">Email</label>
																<input class="form-control" id="email" name="email" value="" placeholder="email" type="text">
															</div>
															<div class="form-group">
																<label for="rincian_informasi_yang_diinginkan">Rincian informasi yang diinginkan</label>
																<input class="form-control" id="rincian_informasi_yang_diinginkan" name="rincian_informasi_yang_diinginkan" value="" placeholder="Rincian informasi yang diinginkan" type="text">
															</div>
															<div class="form-group">
																<label for="tujuan_penggunaan_informasi">Tujuan penggunaan informasi</label>
																<input class="form-control" id="tujuan_penggunaan_informasi" name="tujuan_penggunaan_informasi" value="" placeholder="Tujuan penggunaan informasi" type="text">
															</div>
															<div class="form-group">
																<label for="cara_melihat_membaca_mendengarkan_mencatat">cara memperoleh Informasi <br /> melihat/membaca/mendengarkan/mencatat</label>
																<select class="form-control" id="cara_melihat_membaca_mendengarkan_mencatat" name="cara_melihat_membaca_mendengarkan_mencatat">
																<option value="0">Tidak</option>
																<option value="1">Ya</option>
																</select>
															</div>
															<div class="form-group">
																<label for="cara_hardcopy_softcopy">Cara Memperoleh Informasi <br /> hardcopy/softcopy</label>
																<select class="form-control" id="cara_hardcopy_softcopy" name="cara_hardcopy_softcopy">
																<option value="0">Tidak</option>
																<option value="1">Ya</option>
																</select>
															</div>
															<div class="form-group">
																<label for="cara_mengambil_langsung">Cara Mendapatkan Informasi <br /> Mengambil langsung</label>
																<select class="form-control" id="cara_mengambil_langsung" name="cara_mengambil_langsung">
																<option value="0">Tidak</option>
																<option value="1">Ya</option>
																</select>
															</div>
															<div class="form-group">
																<label for="cara_kurir">Kurir</label>
																<select class="form-control" id="cara_kurir" name="cara_kurir">
																<option value="0">Tidak</option>
																<option value="1">Ya</option>
																</select>
															</div>
															<div class="form-group">
																<label for="cara_pos">POS</label>
																<select class="form-control" id="cara_pos" name="cara_pos">
																<option value="0">Tidak</option>
																<option value="1">Ya</option>
																</select>
															</div>
															<div class="form-group">
																<label for="cara_faksimili">Faksimili</label>
																<select class="form-control" id="cara_faksimili" name="cara_faksimili">
																<option value="0">Tidak</option>
																<option value="1">Ya</option>
																</select>
															</div>
															<div class="form-group">
																<label for="cara_email">Email</label>
																<select class="form-control" id="cara_email" name="cara_email">
																<option value="1">Ya</option>
																<option value="0">Tidak</option>
																</select>
															</div>
															<div class="row" style="display:none;">
																<div class="alert alert-info alert-dismissable">
																	<div class="form-group">
																		<label for="remake">Keterangan Lampiran </label>
																		<input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran " type="text">
																	</div>
																	<div class="form-group">
																		<label for="myfile">File Lampiran </label>
																		<input type="file" size="60" name="myfile" id="file_lampiran">
																	</div>
																	<div id="ProgresUpload">
																		<div id="BarProgresUpload"></div>
																		<div id="PersenProgresUpload">0%</div >
																	</div>
																	<div id="PesanProgresUpload"></div>
																</div>
																<div class="alert alert-info alert-dismissable">
																	<h3 class="box-title">Data Lampiran </h3>
																	<table class="table table-bordered">
																		<tr>
																			<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
																		</tr>
																		<tbody id="tbl_attachment_permohonan_informasi_publik">
																		</tbody>
																	</table>
																</div>
															</div>
														
															<button type="submit" class="btn btn-primary" id="simpan_permohonan_informasi_publik">SIMPAN</button>
															<button type="submit" class="btn btn-primary" id="simpan_permohonan_informasi_publik_komentar_balas" style="display:none;">UPDATE</button>
														
													</form>
													<div class="overlay" id="overlay_form_input" style="display:none;">
														<i class="fa fa-refresh fa-spin"></i>
													</div>
											</div>
											<div class="col-sm-12 col-md-6">
											
											</div>
										</div>
										
										
									</div>
                  <div class="tab-pane active" id="tab_2">
				
										<div class="row">
											<div class="col-sm-12 col-md-6">
											
											</div>
											<div class="col-sm-12 col-md-6">
												<select name="limit_data_permohonan_informasi_publik" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_permohonan_informasi_publik">
													<option value="999999999">Semua</option>
													<option value="10">10 Per-Halaman</option>
													<option value="20">20 Per-Halaman</option>
													<option value="50">50 Per-Halaman</option>
												</select>
											</div>
										</div>
										
										<div class="">
												<table class="table table-bordered table-hover table-reponsive">
													<thead>
														<th>Pemohon</th>
														<th>Rincian informasi yang diinginkan</th>
														<th>Tujuan penggunaan informasi</th> 
														<th>Cara Memperoleh Informasi</th>
														<th>Cara Mendapatkan Informasi</th> 
														<th>Created time</th>
														<th>PROSES</th> 
													</thead>
													<tbody id="tbl_utama_permohonan_informasi_publik">
													</tbody>
												</table>
												<div class="overlay" id="spinners_data" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
												
										</div>
									</div>
                  <div class="tab-pane table-reponsive" id="tab_3">
				
										<div class="row">
											<div class="col-sm-12 col-md-6">
											
											</div>
											<div class="col-sm-12 col-md-6">
											
											</div>
										</div>
										
										<table class="table table-bordered table-hover">
											<tbody id="tbl_utama_permohonan_informasi_publik_arsip">
											</tbody>
										</table>
										<div class="box-footer clearfix">
											<ul class="pagination pagination-sm no-margin pull-right" id="pagination">
											</ul>
										</div>
										<div class="overlay" id="spinners_data6" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
										
									</div>
								</div>
							</div>
                <!-- /.tab-content -->
						</div><!-- /.card-body -->
            <!-- ./card -->
          </div>
							
        </div>
        <!-- /.row -->
				

    </section>
		

<script>
  function AfterSavedPermohonan_informasi_publik() {
    $('#id_permohonan_informasi_publik, #kategori_permohonan_informasi_publik_get_by_id, #id_permohonan_informasi_publik_get_by_id, #id_posting_get_by_id, #nama, #alamat, #kategori_permohonan_informasi_publik, #pekerjaan, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
    $('#tbl_attachment_permohonan_informasi_publik').html('');
    $('#PesanProgresUpload').html('');
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
 
<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1;
		var limit_data_permohonan_informasi_publik = $('#limit_data_permohonan_informasi_publik').val();
		var limit = limit_per_page_custome(limit_data_permohonan_informasi_publik);
		var kategori_permohonan_informasi_publik = $('#kategori_permohonan_informasi_publik').val();
    load_data_permohonan_informasi_publik(halaman, limit, kategori_permohonan_informasi_publik);
});
</script>

<script>
  function load_data_permohonan_informasi_publik(halaman, limit) {
    $('#tbl_utama_permohonan_informasi_publik').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_informasi_publik/load_table_permohonan_informasi_publik/',
      success: function(html) {
        $('#tbl_utama_permohonan_informasi_publik').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_tampil').on('click', function(e) {
    var halaman = 1;
		var limit_data_permohonan_informasi_publik = $('#limit_data_permohonan_informasi_publik').val();
		var limit = limit_per_page_custome(limit_data_permohonan_informasi_publik);
		var kategori_permohonan_informasi_publik = $('#kategori_permohonan_informasi_publik').val();
    load_data_permohonan_informasi_publik(halaman, limit, kategori_permohonan_informasi_publik);
  });
});
</script>

<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#kategori_permohonan_informasi_publik').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
			var limit_data_permohonan_informasi_publik = $('#limit_data_permohonan_informasi_publik').val();
			var limit = limit_per_page_custome(limit_data_permohonan_informasi_publik);
      var kategori_permohonan_informasi_publik = $('#kategori_permohonan_informasi_publik').val();
      load_data_permohonan_informasi_publik(halaman, limit, kategori_permohonan_informasi_publik);
    });
  });
</script>

<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#limit_data_permohonan_informasi_publik').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
			var limit_data_permohonan_informasi_publik = $('#limit_data_permohonan_informasi_publik').val();
			var limit = limit_per_page_custome(limit_data_permohonan_informasi_publik);
      var kategori_permohonan_informasi_publik = $('#kategori_permohonan_informasi_publik').val();
      load_data_permohonan_informasi_publik(halaman, limit, kategori_permohonan_informasi_publik);
    });
  });
</script>

<script>
  function load_data_permohonan_informasi_publik_arsip(halaman, limit) {
    $('#tbl_utama_permohonan_informasi_publik6').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_informasi_publik/load_table_arsip/',
      success: function(html) {
        $('#tbl_utama_permohonan_informasi_publik6').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_tampil_arsip').on('click', function(e) {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_table_permohonan_informasi_publik(halaman, limit);
  });
});
</script>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_permohonan_informasi_publik').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'permohonan_informasi_publik',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_permohonan_informasi_publik').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_permohonan_informasi_publik').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_permohonan_informasi_publik').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_permohonan_informasi_publik').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_permohonan_informasi_publik').on('click', '#del_ajax', function() {
    var id_permohonan_informasi_publik = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_informasi_publik"] = id_permohonan_informasi_publik;
        var url = '<?php echo base_url(); ?>permohonan_informasi_publik/hapus/';
        HapusData(parameter, url);
        $('[id_permohonan_informasi_publik='+id_permohonan_informasi_publik+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_permohonan_informasi_publik6').on('click', '#restore_ajax', function() {
    var id_permohonan_informasi_publik = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    alertify.confirm('Anda yakin data akan direstore?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_informasi_publik"] = id_permohonan_informasi_publik;
        var url = '<?php echo base_url(); ?>permohonan_informasi_publik/restore/';
        HapusData(parameter, url);
        $('[id_permohonan_informasi_publik='+id_permohonan_informasi_publik+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_permohonan_informasi_publik').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_permohonan_informasi_publik').hide();
    $('#simpan_permohonan_informasi_publik_komentar_balas').show();
    var id_permohonan_informasi_publik = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_permohonan_informasi_publik_get_by_id').val(id_permohonan_informasi_publik);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_permohonan_informasi_publik:id_permohonan_informasi_publik
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>permohonan_informasi_publik/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#id_permohonan_informasi_publik_get_by_id').val(json[i].id_permohonan_informasi_publik);
            $('#id_posting_get_by_id').val(json[i].id_posting);
            $('#nama_get_by_id').val(json[i].nama);
            $('#alamat_get_by_id').val(json[i].alamat);
            $('#pekerjaan_get_by_id').val(json[i].pekerjaan);
            $('#nomor_telp_get_by_id').val(json[i].nomor_telp);
            $('#email_get_by_id').val(json[i].email);
            $('#rincian_informasi_yang_diinginkan_get_by_id').val(json[i].rincian_informasi_yang_diinginkan);
            $('#tujuan_penggunaan_informasi_get_by_id').val(json[i].tujuan_penggunaan_informasi);
            $('#kategori_permohonan_informasi_publik_get_by_id').val(json[i].kategori_permohonan_informasi_publik);
            $('#cara_melihat_membaca_mendengarkan_mencatat_get_by_id').val(json[i].cara_melihat_membaca_mendengarkan_mencatat);
            $('#cara_hardcopy_softcopy_get_by_id').val(json[i].cara_hardcopy_softcopy);
            $('#cara_mengambil_langsung_get_by_id').val(json[i].cara_mengambil_langsung);
            $('#cara_kurir_get_by_id').val(json[i].cara_kurir);
            $('#cara_pos_get_by_id').val(json[i].cara_pos);
            $('#cara_faksimili_get_by_id').val(json[i].cara_faksimili);
            $('#cara_email_get_by_id').val(json[i].cara_email);
          }
        }
      });
    AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_permohonan_informasi_publik').show();
    $('#update_permohonan_informasi_publik').hide();
    $('#tbl_attachment_permohonan_informasi_publik').html('');
    $('#id_permohonan_informasi_publik, #id_permohonan_informasi_publik_get_by_id, #id_posting_get_by_id, #kategori_permohonan_informasi_publik_get_by_id, #nama, #alamat, #kategori_permohonan_informasi_publik, #pekerjaan, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_permohonan_informasi_publik').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'nama','nama', 'alamat', 'pekerjaan', 'kategori_permohonan_informasi_publik', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan', 'tujuan_penggunaan_informasi', 'cara_melihat_membaca_mendengarkan_mencatat', 'cara_hardcopy_softcopy', 'cara_mengambil_langsung', 'cara_kurir', 'cara_pos', 'cara_faksimili', 'cara_email', 'created_by' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["nama"] = $("#nama").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      parameter["tampil_menu_atas"] = $("#tampil_menu_atas").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["rincian_informasi_yang_diinginkan"] = $("#rincian_informasi_yang_diinginkan").val();
      parameter["tujuan_penggunaan_informasi"] = $("#tujuan_penggunaan_informasi").val();
      parameter["kategori_permohonan_informasi_publik"] = $("#kategori_permohonan_informasi_publik").val();
      parameter["cara_melihat_membaca_mendengarkan_mencatat"] = $("#cara_melihat_membaca_mendengarkan_mencatat").val();
      parameter["cara_hardcopy_softcopy"] = $("#cara_hardcopy_softcopy").val();
      parameter["cara_mengambil_langsung"] = $("#cara_mengambil_langsung").val();
      parameter["cara_kurir"] = $("#cara_kurir").val();
      parameter["cara_pos"] = $("#cara_pos").val();
      parameter["cara_faksimili"] = $("#cara_faksimili").val();
      parameter["cara_email"] = $("#cara_email").val();
      parameter["temp"] = $("#temp").val();
      parameter["created_by"] = $("#created_by").val();
      var url = '<?php echo base_url(); ?>permohonan_informasi_publik/simpan_permohonan_informasi_publik';
      
      var parameterRv = [ 'nama', 'alamat', 'pekerjaan', 'kategori_permohonan_informasi_publik', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan', 'tujuan_penggunaan_informasi', 'cara_melihat_membaca_mendengarkan_mencatat', 'cara_hardcopy_softcopy', 'cara_mengambil_langsung', 'cara_kurir', 'cara_pos', 'cara_faksimili', 'cara_email', 'created_by' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedPermohonan_informasi_publik();
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_permohonan_informasi_publik_komentar_balas').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'nama', 'kategori_permohonan_informasi_publik_get_by_id', 'id_permohonan_informasi_publik_get_by_id', 'id_posting_get_by_id', 'alamat', 'pekerjaan', 'kategori_permohonan_informasi_publik', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan', 'tujuan_penggunaan_informasi', 'cara_melihat_membaca_mendengarkan_mencatat', 'cara_hardcopy_softcopy', 'cara_mengambil_langsung', 'cara_kurir', 'cara_pos', 'cara_faksimili', 'cara_email', 'created_by' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["nama"] = $("#nama").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      parameter["tampil_menu_atas"] = $("#tampil_menu_atas").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["rincian_informasi_yang_diinginkan"] = $("#rincian_informasi_yang_diinginkan").val();
      parameter["tujuan_penggunaan_informasi"] = $("#tujuan_penggunaan_informasi").val();
      parameter["kategori_permohonan_informasi_publik_get_by_id"] = $("#kategori_permohonan_informasi_publik_get_by_id").val();
      parameter["cara_melihat_membaca_mendengarkan_mencatat"] = $("#cara_melihat_membaca_mendengarkan_mencatat").val();
      parameter["cara_hardcopy_softcopy"] = $("#cara_hardcopy_softcopy").val();
      parameter["cara_mengambil_langsung"] = $("#cara_mengambil_langsung").val();
      parameter["cara_kurir"] = $("#cara_kurir").val();
      parameter["cara_pos"] = $("#cara_pos").val();
      parameter["cara_faksimili"] = $("#cara_faksimili").val();
      parameter["cara_email"] = $("#cara_email").val();
      parameter["temp"] = $("#temp").val();
      parameter["created_by"] = $("#created_by").val();
      parameter["id_posting_get_by_id"] = $("#id_posting_get_by_id").val();
      parameter["id_permohonan_informasi_publik_get_by_id"] = $("#id_permohonan_informasi_publik_get_by_id").val();
      var url = '<?php echo base_url(); ?>permohonan_informasi_publik/simpan_permohonan_informasi_publik_komentar_balas';
      
      var parameterRv = [ 'nama', 'id_permohonan_informasi_publik_get_by_id', 'id_posting_get_by_id', 'kategori_permohonan_informasi_publik_get_by_id', 'alamat', 'kategori_permohonan_informasi_publik', 'pekerjaan', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan', 'tujuan_penggunaan_informasi', 'cara_melihat_membaca_mendengarkan_mencatat', 'cara_hardcopy_softcopy', 'cara_mengambil_langsung', 'cara_kurir', 'cara_pos', 'cara_faksimili', 'cara_email', 'created_by' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#pagination').on('click', '.update_id', function(e) {
		e.preventDefault();
		var id = $(this).closest('li').attr('page');
		var halaman = id;
    var limit = limit_per_page_custome(20000);
    load_data_permohonan_informasi_publik(halaman, limit);
    
	});
});
</script>

<!-----------DIP------------>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_permohonan_informasi_publik').on('click', '#inaktif_permohonan_informasi_publik', function() {
    var id_permohonan_informasi_publik = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    alertify.confirm('Anda yakin data tidak di aktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_permohonan_informasi_publik:id_permohonan_informasi_publik
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>permohonan_informasi_publik/inaktif_permohonan_informasi_publik',
          success: function(html) {
            alertify.success('Berhasil Inaktif');
						var halaman = 1;
						var limit = limit;
						load_data_permohonan_informasi_publik(halaman, limit);
						}
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_permohonan_informasi_publik').on('click', '#aktif_permohonan_informasi_publik', function() {
    var id_permohonan_informasi_publik = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    alertify.confirm('Anda yakin data akan diaktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_permohonan_informasi_publik:id_permohonan_informasi_publik
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>permohonan_informasi_publik/aktif_permohonan_informasi_publik',
          success: function(html) {
            alertify.success('Berhasil Aktif');
						var halaman = 1;
						var limit = limit;
						load_data_permohonan_informasi_publik(halaman, limit);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!-----------DIP------------>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_permohonan_informasi_publik').on('click', '#inaktif_permohonan_informasi_publik_komentar', function() {
    var id_permohonan_informasi_publik = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    alertify.confirm('<input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="pekerjaan" type="text" readonly>', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_permohonan_informasi_publik:id_permohonan_informasi_publik
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>permohonan_informasi_publik/inaktif_permohonan_informasi_publik_komentar',
          success: function(html) {
            alertify.success('Berhasil Inaktif');
						var halaman = 1;
						var limit = limit;
						load_data_permohonan_informasi_publik(halaman, limit);
						}
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_permohonan_informasi_publik').on('click', '#aktif_permohonan_informasi_publik', function() {
    var id_permohonan_informasi_publik = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    alertify.confirm('Anda yakin data akan diaktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_permohonan_informasi_publik:id_permohonan_informasi_publik
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>permohonan_informasi_publik/aktif_permohonan_informasi_publik',
          success: function(html) {
            alertify.success('Berhasil Aktif');
						var halaman = 1;
						var limit = limit;
						load_data_permohonan_informasi_publik(halaman, limit);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>