<?php
    $web=$this->uut->namadomain(base_url());
?>
      <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Permohonan Informasi Publik</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Buku Agenda</a></li>
              <li class="breadcrumb-item active">Permohonan Informasi Publik</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
      </section>
<section class="content" id="awal">
		<!-- Custom Tabs -->
		<div class="card">
      <div class="card-header p-2">
        <ul class="nav nav-pills">
          <li class="nav-item active"><a class="nav-link" href="#tab_data_permohonan_informasi_publik" data-toggle="tab" id="klik_tab_data_permohonan_informasi_publik">Data</a></li>
        </ul>
      </div>
			<div class="card-body">
        <div class="tab-content">
          <div class="tab-pane active" id="tab_data_permohonan_informasi_publik">
                <div class="row">
                  <div class="col-md-12">
                    <div class="card-body">
                      <table id="main_table" class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 1%" id="gt">
                                    #
                                </th>
                                <th style="width: 20%">
                                    Nama Pemohon
                                </th>
                                <th style="width: 10%">
                                    Domain
                                </th>
                                <th style="width: 10%">
                                    No HP
                                </th>
                                <th style="width: 10%">
                                    E-mail
                                </th>
                                <th style="width: 10%">
                                    Tgl Permohonan
                                </th>
                                <th>
                                    Rincian informasi yang diminta
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tabel_list_profil">
                        </tbody>
                      </table>
                      <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                      </ul>
                    </div>
                  </div>
                </div>
          </div>
          <!-- /.tab-pane -->
        </div>
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
</section>

<script type="text/javascript">
  $(document).ready(function() {
      load_data();
  });
</script>

<script type="text/javascript">
function load_data() { 
  var tabel = null;
  var no = 1;
  tabel = $('#main_table').DataTable({        
    dom: 'Blfrtip',
    buttons: [
      {
          text: 'Export',
          extend: 'collection',
          className: 'custom-html-collection',
          buttons: [
              'copy',
              'pdf',
              'csv',
              'excel',
              'print'
          ]
      }
    ],
    "retrieve": true,
    "destroy": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, // Set true agar bisa di sorting
    "order": [[ 0, 'asc' ]], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
    "ajax":
    {
        "url": "<?php echo base_url('permohonan_informasi_publik/view') ?>", // URL file untuk proses select datanya
        "type": "POST"
    },
    "deferRender": true,
    "aLengthMenu": [[10, 50, 100],[ ' 10',' 50',' 100']], // Combobox Limit          
    "columns": [
      {"data": 'id_permohonan_informasi_publik',"sortable": false, 
          render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
          }  
      },
      { "data": "nama" }, // Tampilkan nama
      { "render": function ( data, type, row ) { // Tampilkan nama_perpustakaan
          var html  = '<a href="https://'+row.domain+'/permohonan_informasi_publik/detail/'+row.id_permohonan_informasi_publik+'" target="_Blank">'+row.domain+'</a>'
          return html
        }
      },
      { "data": "nomor_telp" }, // Tampilkan nomor_telp
      { "data": "email" }, // Tampilkan email
      { "data": "created_time" }, // Tampilkan created_time
      { "data": "rincian_informasi_yang_diinginkan" }, // Tampilkan rincian_informasi_yang_diinginkan
    ],
  });
}
</script>

<script type="text/javascript">
  function paginations1(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby,
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>permohonan_informasi_publik/total_data1',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li id="' + a + '" page="' + a + '" keyword="' + keyword + '" id_kelembagaan="' + id_kelembagaan + '" jenis_permohonan_informasi_publik="' + jenis_permohonan_informasi_publik + '"><a id="next" href="#">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>

<script type="text/javascript">
  function load_data1() {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $('#page').val(page);
    $('#tbl_data_permohonan_informasi_publik').html('');
    $('#spinners_tbl_data_permohonan_informasi_publik').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_informasi_publik/json_laporan_permohonan_informasi_publik/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_permohonan_informasi_publik"]').tab('show');
        $('#tbl_data_permohonan_informasi_publik').html(html);
        $('#spinners_tbl_data_permohonan_informasi_publik').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilkan_data_permohonan_informasi_publik').on('click', function(e) {
      e.preventDefault();
      load_data();
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = 'permohonan_informasi_publik';
      load_data(tabel);
    });
  });
</script>