<?php
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
?>
		<section class="content">
          <div class="row" id="awal">
            <div class="col-md-12">
              <input name="tabel" id="tabel" value="permohonan_informasi_publik" type="hidden" value="">
              <input name="page" id="page" value="1" type="hidden" value="">
              <div class="card card-primary card-outline direct-chat-primary" id="div_form_input" style="display:none;">
                <div class="card-header">
                  <h3 class="card-title">Formulir Permohonan Informasi</h3>
                </div>
                <div class="card-body">
                  <form role="form" id="form_isian" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="form-group" style="display:none;">
                        <label for="temp">temp</label>
                        <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="mode">mode</label>
                        <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="id_permohonan_informasi_publik">id_permohonan_informasi_publik</label>
                        <input class="form-control" id="id_permohonan_informasi_publik" name="id" value="" placeholder="id_permohonan_informasi_publik" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="id_posting">id_posting</label>
                        <input class="form-control" id="id_posting" name="id_posting" value="0" placeholder="id_posting" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="created_by">created_by</label>
                        <input class="form-control" id="created_by" name="created_by" value="0" placeholder="created_by" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="nama">NIK</label>
                        <div class="row">
                          <div class="col-8">
                            <input class="form-control" id="nik" name="nik" value="0" placeholder="NIK" type="text">
                          </div>
                          <div class="col-4">
                            <div class="row">
                              <div class="col">
                                <button type="button" class="btn btn-primary" id="cek_nik">Cek NIK DISDUKCAPIL</button>
                              </div>
                              <div class="col">
                                <div id="spin_form" style="display:none;">
                                  <i class="fa fa-refresh fa-spin"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col"><br />
                            <div id="info_news" style="display:none;" class="callout callout-info">
                              <h5><i class="fa fa-info"></i> Info</h5>
                              <div class="row">
                                <div class="input-group mb-3 col-6">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">No. KK</span>
                                  </div>
                                  <input type="text" class="form-control" id="no_kk" name="no_kk" readonly />
                                </div>
                                <div class="input-group mb-3 col-6">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">Jenis Kelamin</span>
                                  </div>
                                  <input type="text" class="form-control" id="jenis_kelamin" name="jenis_kelamin" readonly />
                                </div>
                                <div class="input-group mb-3 col-6">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">Tempat Lahir</span>
                                  </div>
                                  <input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" readonly />
                                </div>
                                <div class="input-group mb-3 col-6">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">Tanggal Lahir</span>
                                  </div>
                                  <input type="text" class="form-control" id="tanggal_lahir" name="tanggal_lahir" readonly />
                                </div>
                                <div class="input-group mb-3 col-6">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">Gol Darah</span>
                                  </div>
                                  <input type="text" class="form-control" id="gol_darah" name="gol_darah" readonly />
                                </div>
                                <div class="input-group mb-3 col-6">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">Agama</span>
                                  </div>
                                  <input type="text" class="form-control" id="agama" name="agama" readonly />
                                </div>
                                <div class="input-group mb-3 col-6">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">Status Kawin</span>
                                  </div>
                                  <input type="text" class="form-control" id="status_perkawinan" name="status_perkawinan" readonly />
                                </div>
                                <div class="input-group mb-3 col-6">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text">Status Dalam Keluarga</span>
                                  </div>
                                  <input type="text" class="form-control" id="status_dalam_keluarga" name="status_dalam_keluarga" readonly />
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-6">
                            <div class="form-group">
                              <label for="nama">Nama</label>
                              <input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
                            </div>
                          </div>
                          <div class="col-6">
                            <label for="alamat">Alamat</label>
                            <input class="form-control" id="alamat" name="alamat" value="" placeholder="Alamat" type="text">
                          </div>
                          <div class="col">
                            <label for="dusun">dusun</label>
                            <input class="form-control" id="dusun" name="dusun" value="" placeholder="dusun" type="text">
                          </div>
                          <div class="col">
                            <label for="rt">rt</label>
                            <input class="form-control" id="rt" name="rt" value="" placeholder="rt" type="text">
                          </div>
                          <div class="col">
                            <label for="rw">rw</label>
                            <input class="form-control" id="rw" name="rw" value="" placeholder="rw" type="text">
                          </div>
                          <div class="col-6">
                            <label for="Desa">Desa</label>
                            <input class="form-control" id="nama_desa" name="nama_desa" value="" placeholder="Desa" type="text">
                          </div>
                          <div class="col-6">
                            <label for="Desa">Kecamatan</label>
                            <input class="form-control" id="nama_kecamatan" name="nama_kecamatan" value="" placeholder="Kecamatan" type="text">
                          </div>
                          <div class="col-6">
                            <label for="Desa">Kabupaten</label>
                            <input class="form-control" id="nama_kabupaten" name="nama_kabupaten" value="" placeholder="Kabupaten" type="text">
                          </div>
                          <div class="col-6">
                            <label for="Desa">Provinsi</label>
                            <input class="form-control" id="nama_provinsi" name="nama_provinsi" value="" placeholder="Provinsi" type="text">
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                              <label for="pekerjaan">Pekerjaan</label>
                              <input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="Pekerjaan" type="text">
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                              <label for="nomor_telp">Nomor telp</label>
                              <input class="form-control" id="nomor_telp" name="nomor_telp" value="" placeholder="contoh 081090909090" type="text">
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                              <label for="email">Email</label>
                              <input class="form-control" id="email" name="email" value="" placeholder="contoh email@mail.id" type="text">
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                              <label for="tujuan_penggunaan_informasi">Tujuan penggunaan informasi</label>
                              <input class="form-control" id="tujuan_penggunaan_informasi" name="tujuan_penggunaan_informasi" value="" placeholder="Tujuan penggunaan informasi" type="text">
                            </div>
                          </div>
                          <div class="col-12">
                            <div class="form-group">
                              <label for="rincian_informasi_yang_diinginkan">Rincian informasi yang diinginkan</label>
                              <textarea class="form-control" rows="3" id="rincian_informasi_yang_diinginkan" name="rincian_informasi_yang_diinginkan" value="" placeholder="" type="text">
                              </textarea>
                            </div>
                          </div>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="kategori_permohonan_informasi_publik">kategori_permohonan_informasi_publik</label>
                        <input class="form-control" id="kategori_permohonan_informasi_publik" name="kategori_permohonan_informasi_publik" value="Permohonan Informasi Publik" placeholder="Tujuan penggunaan informasi" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_melihat_membaca_mendengarkan_mencatat">cara memperoleh Informasi <br /> melihat/membaca/mendengarkan/mencatat</label>
                        <select class="form-control" id="cara_melihat_membaca_mendengarkan_mencatat" name="cara_melihat_membaca_mendengarkan_mencatat" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_hardcopy_softcopy">Cara Memperoleh Informasi <br /> hardcopy/softcopy</label>
                        <select class="form-control" id="cara_hardcopy_softcopy" name="cara_hardcopy_softcopy" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_mengambil_langsung">Cara Mendapatkan Informasi <br /> Mengambil langsung</label>
                        <select class="form-control" id="cara_mengambil_langsung" name="cara_mengambil_langsung" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_kurir">Kurir</label>
                        <select class="form-control" id="cara_kurir" name="cara_kurir" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_pos">POS</label>
                        <select class="form-control" id="cara_pos" name="cara_pos" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_faksimili">Faksimili</label>
                        <select class="form-control" id="cara_faksimili" name="cara_faksimili" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_email">Email</label>
                        <select class="form-control" id="cara_email" name="cara_email" >
                        <option value="1">Ya</option>
                        <option value="0">Tidak</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <ul class="nav nav-pills">
                          <li class="nav-item"><a class="nav-link" href="#tab_lampiran" data-toggle="tab"><i class="fa fa-paperclip"></i> Lampiran</a></li>
                        </ul>
                        <div class="tab-content">
                          <div class="tab-pane" id="tab_lampiran">
                            <div class="alert alert-info alert-dismissable">
                              <div class="form-group">
                                <h5 class="text-warning"><i class="icon fa fa-warning"></i>Isi keterangan lampiran terlebih dahulu.</h5>
                                <label for="">Lampiran dapat lebih dari satu, upload satu persatu.</label>
                                <label for="">Dan Mohon Tidak Upload Data yang berisi Identitas Pribadi, Seperti : foto NIK/KK/SIM dll .</label>
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="remake_permohonan_informasi_publik">Keterangan Lampiran </label>
                              <input class="form-control" id="remake_permohonan_informasi_publik" name="remake_permohonan_informasi_publik" placeholder="Keterangan Lampiran" type="text">
                            </div>
                            <div class="form-group">
                              <label for="myfile">File Lampiran </label>
                              <input type="file" size="60" name="myfile" id="file_lampiran_permohonan_informasi_publik" >
                            </div>
                            <div class="form-group">
                              <div id="ProgresUpload">
                                <div id="BarProgresUpload"></div>
                                <div id="PersenProgresUpload">0%</div >
                              </div>
                              <div id="PesanProgresUpload"></div>
                            </div>
                            <div class="alert alert-info alert-dismissable table-responsive">
                              <h3 class="card-title">Data Lampiran </h3>
                              <table class="table table-bordered">
                                <tr>
                                  <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                                </tr>
                                <tbody id="tbl_attachment_pengaduan_masyarakat">
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" id="simpan_permohonan_informasi_publik">KIRIM</button>
                      <button type="submit" class="btn btn-primary" id="update_permohonan_informasi_publik" style="display:none;">UPDATE</button>
                    </div>
                  </form>
                  <?php
                  if($ses) {
                  echo'
                  <form role="form" id="form_isian" method="post" action="'.base_url().'attachment/upload/?table_name=permohonan_informasi_publik" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="form-group" style="display:none;">
                        <label for="temp">temp</label>
                        <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="mode">mode</label>
                        <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="id_permohonan_informasi_publik">id_permohonan_informasi_publik</label>
                        <input class="form-control" id="id_permohonan_informasi_publik" name="id" value="" placeholder="id_permohonan_informasi_publik" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="id_posting">id_posting</label>
                        <input class="form-control" id="id_posting" name="id_posting" value="0" placeholder="id_posting" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="created_by">created_by</label>
                        <input class="form-control" id="created_by" name="created_by" value="0" placeholder="created_by" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="nama">Nama</label>
                        <input class="form-control" id="nama" name="nama" value="'.$this->session->userdata('nama').'" placeholder="Nama" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="alamat">Alamat</label>
                        <input class="form-control" id="alamat" name="alamat" value="-" placeholder="Alamat" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="pekerjaan">Pekerjaan</label>
                        <input class="form-control" id="pekerjaan" name="pekerjaan" value="-" placeholder="Pekerjaan" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="nomor_telp">Nomor telp</label>
                        <input class="form-control" id="nomor_telp" name="nomor_telp" value="-" placeholder="contoh 081090909090" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="email">Email</label>
                        <input class="form-control" id="email" name="email" value="-" placeholder="contoh email@mail.id" type="text">
                      </div>
                      <div class="form-group">
                        <label for="rincian_informasi_yang_diinginkan">Rincian informasi yang diinginkan</label>
                        <textarea class="form-control" rows="3" id="rincian_informasi_yang_diinginkan" name="rincian_informasi_yang_diinginkan" value="" placeholder="" type="text">
                        </textarea>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="tujuan_penggunaan_informasi">Tujuan penggunaan informasi</label>
                        <input class="form-control" id="tujuan_penggunaan_informasi" name="tujuan_penggunaan_informasi" value="-" placeholder="Tujuan penggunaan informasi" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="kategori_permohonan_informasi_publik">kategori_permohonan_informasi_publik</label>
                        <input class="form-control" id="kategori_permohonan_informasi_publik" name="kategori_permohonan_informasi_publik" value="Permohonan Informasi Publik" placeholder="Tujuan penggunaan informasi" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_melihat_membaca_mendengarkan_mencatat">cara memperoleh Informasi <br /> melihat/membaca/mendengarkan/mencatat</label>
                        <select class="form-control" id="cara_melihat_membaca_mendengarkan_mencatat" name="cara_melihat_membaca_mendengarkan_mencatat" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_hardcopy_softcopy">Cara Memperoleh Informasi <br /> hardcopy/softcopy</label>
                        <select class="form-control" id="cara_hardcopy_softcopy" name="cara_hardcopy_softcopy" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_mengambil_langsung">Cara Mendapatkan Informasi <br /> Mengambil langsung</label>
                        <select class="form-control" id="cara_mengambil_langsung" name="cara_mengambil_langsung" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_kurir">Kurir</label>
                        <select class="form-control" id="cara_kurir" name="cara_kurir" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_pos">POS</label>
                        <select class="form-control" id="cara_pos" name="cara_pos" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_faksimili">Faksimili</label>
                        <select class="form-control" id="cara_faksimili" name="cara_faksimili" >
                        <option value="0">Tidak</option>
                        <option value="1">Ya</option>
                        </select>
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="cara_email">Email</label>
                        <select class="form-control" id="cara_email" name="cara_email" >
                        <option value="1">Ya</option>
                        <option value="0">Tidak</option>
                        </select>
                      </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" id="simpan_permohonan_informasi_publik">KIRIM</button>
                      <button type="submit" class="btn btn-primary" id="update_permohonan_informasi_publik" style="display:none;">UPDATE</button>
                    </div>
                  </form>
                  ';
                  }
                  ?>
                </div>
              </div>
              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
						</div>
            <div class="col-md-12">
              <div class="card card-primary card-outline direct-chat-primary">
                  <div class="card-header">
                    <h3 class="card-title">Data Permohonan Informasi</h3>
                    <div class="card-tools">
                      <div class="input-group">
                        <input name="keyword" class="form-control input-sm" style="width: 150px; display:none;" placeholder="Search" type="text" id="keyword">
                        <select name="limit" class="form-control input-sm" style="width: 150px; display:none;" id="limit">
                          <option value="25">25 Per-Halaman</option>
                          <option value="50">50 Per-Halaman</option>
                          <option value="100">100 </option>
                          <option value="999999999">Semua</option>
                        </select>
                        <select name="orderby" class="form-control input-sm" style="width: 150px; display:none;" id="orderby">
                          <option value="permohonan_informasi_publik.id_permohonan_informasi_publik">ID</option>
                        </select>
                        <select class="form-control-sm" style="width: 150px;" id="tahun" name="tahun" >
                          <?php
                            for($i=date('Y'); $i>=date('Y')-10; $i-=1){
                            echo"<option value='$i'> $i </option>";
                            }
                          ?>
                        </select>
                        <div class="input-group-btn">
                          <button class="btn btn-sm btn-default" id="tampilkan_data_permohonan_informasi_publik"><i class="fa fa-search"></i> Tampil</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-body table-responsive">
                    <div class="row">
                      <div class="col-md-12" id="tbl_data_permohonan_informasi_publik">
                      </div>
                      <div class="col-md-12" id="tbl_utama_permohonan_informasi_publik_forward">
                      </div>
                      <div class="col-md-12">
                        <div class="paging_simple_numbers">
                          <ul class="pagination" id="pagination">
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="overlay" id="spinners_data" style="display:none;">
                      <i class="fa fa-refresh fa-spin"></i>
                    </div>
                  </div>
              </div>
            </div>
          </div>
		</section>
<script>
  function AfterSavedPermohonan_informasi_publik() {
    $('#id_permohonan_informasi_publik, #id_posting,  #nama, #kategori_permohonan_informasi_publik, #alamat, #pekerjaan, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
		$('#tbl_attachment_pengaduan_masyarakat').html('');
    $('#PesanProgresUpload').html('');
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#div_form_input').show();
  
  load_data_permohonan_informasi_publik_forward(1,10);
});
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_pengaduan_masyarakat').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'permohonan_informasi_publik',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>lampiran_pengaduan_masyarakat/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_lampiran_pengaduan_masyarakat="'+json[i].id_lampiran_pengaduan_masyarakat+'" id="'+json[i].id_lampiran_pengaduan_masyarakat+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/lampiran_pengaduan_masyarakat/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_attachment"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_pengaduan_masyarakat').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_permohonan_informasi_publik').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake_permohonan_informasi_publik').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran_permohonan_informasi_publik').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_pengaduan_masyarakat').on('click', '#del_attachment', function() {
    var id_lampiran_pengaduan_masyarakat = $(this).closest('tr').attr('id_lampiran_pengaduan_masyarakat');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_lampiran_pengaduan_masyarakat"] = id_lampiran_pengaduan_masyarakat;
        var url = '<?php echo base_url(); ?>lampiran_pengaduan_masyarakat/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_permohonan_informasi_publik').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_lampiran_pengaduan_masyarakat='+id_lampiran_pengaduan_masyarakat+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_permohonan_informasi_publik').on('click', function(e) {
      e.preventDefault();
      // $('#simpan_permohonan_informasi_publik').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
      $('#pesan_terkirim').show();
      var parameter = [ 'nama', 'alamat', 'pekerjaan', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan'  ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_posting"] = $("#id_posting").val();
      parameter["nama"] = $("#nama").val();
      parameter["alamat"] = ''+$("#alamat").val()+' RT: '+$("#rt").val()+' RW: '+$("#rw").val()+' Dusun: '+$("#dusun").val()+'Desa '+$("#nama_desa").val()+', Kecamatan '+$("#nama_kecamatan").val()+', Kabupaten '+$("#nama_kabupaten").val()+', Provinsi '+$("#nama_provinsi").val()+',';
      parameter["pekerjaan"] = $("#pekerjaan").val();
      parameter["tampil_menu_atas"] = $("#tampil_menu_atas").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["rincian_informasi_yang_diinginkan"] = $("#rincian_informasi_yang_diinginkan").val();
      parameter["tujuan_penggunaan_informasi"] = $("#tujuan_penggunaan_informasi").val();
      parameter["kategori_permohonan_informasi_publik"] = $("#kategori_permohonan_informasi_publik").val();
      parameter["cara_melihat_membaca_mendengarkan_mencatat"] = $("#cara_melihat_membaca_mendengarkan_mencatat").val();
      parameter["cara_hardcopy_softcopy"] = $("#cara_hardcopy_softcopy").val();
      parameter["cara_mengambil_langsung"] = $("#cara_mengambil_langsung").val();
      parameter["cara_kurir"] = $("#cara_kurir").val();
      parameter["cara_pos"] = $("#cara_pos").val();
      parameter["cara_faksimili"] = $("#cara_faksimili").val();
      parameter["cara_email"] = $("#cara_email").val();
      parameter["temp"] = $("#temp").val();
      parameter["created_by"] = $("#created_by").val();
      var url = '<?php echo base_url(); ?>permohonan_informasi_publik/simpan_permohonan_informasi_publik_no';
      
      var parameterRv = [ 'nama', 'alamat', 'pekerjaan', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
				$('#overlay_form_input').fadeOut();
				alert('gagal');
				$('html, body').animate({
					scrollTop: $('#awal').offset().top
				}, 1000);
      }
      else{
        SimpanData(parameter, url);
        AfterSavedPermohonan_informasi_publik();
				$('#overlay_form_input').fadeOut('slow');
        $('#div_form_input form').trigger('reset');
        var tabel = $("#tabel").val();
        load_data(tabel);
      }
    });
  });
</script>
<script>
  function load_data_permohonan_informasi_publik_forward(halaman, limit) {
    $('#tbl_utama_permohonan_informasi_publik_forward').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_informasi_publik/load_table_forward/',
      success: function(html) {
        $('#tbl_utama_permohonan_informasi_publik_forward').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_permohonan_informasi_publik').on('click', '#del_ajax', function() {
    var id_permohonan_informasi_publik = $(this).closest('div').attr('id_permohonan_informasi_publik');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_informasi_publik"] = id_permohonan_informasi_publik;
        var url = '<?php echo base_url(); ?>permohonan_informasi_publik/hapus/?id='+id_permohonan_informasi_publik+'';
        HapusData(parameter, url);
        $('[id_permohonan_informasi_publik='+id_permohonan_informasi_publik+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_permohonan_informasi_publik_forward').on('click', '#del_ajax1', function() {
    var id_permohonan_informasi_publik_forward = $(this).closest('tr').attr('id_permohonan_informasi_publik_forward');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_informasi_publik_forward"] = id_permohonan_informasi_publik_forward;
        var url = '<?php echo base_url(); ?>permohonan_informasi_publik/hapus_forward/?id='+id_permohonan_informasi_publik_forward+'';
        HapusData(parameter, url);
        $('[id_permohonan_informasi_publik_forward='+id_permohonan_informasi_publik_forward+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_tampil').on('click', function(e) {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_data_pengaduan_masyarakat(halaman, limit);
    load_data_pengaduan_masyarakat_forward(halaman, limit);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tahun = $('#tahun').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        tahun:tahun
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>permohonan_informasi_publik/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li id="' + a + '" page="' + a + '" keyword="' + keyword + '" tahun="' + tahun + '" class="paginate_button page-item "><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tahun = $('#tahun').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        tahun:tahun
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    load_data(tabel);
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $("#cek_nik").on("click", function(e) {
    e.preventDefault();
    $('#spin_form').show();
    $('#info_news').show();
    var nik = $("#nik").val();
    $.ajax({
      type: "POST",
      async: true,
      data: {
        nik: nik,
      },
      dataType: "json",
      url: '<?php echo base_url(); ?>api_capil/getNIK',
      success: function(json) {
          for (var i = 0; i < json.content.length; i++) {
              $("#nik").val(json.content[i].NIK);
              $("#no_kk").val(json.content[i].NO_KK);
              $("#nama").val(json.content[i].NAMA_LGKP);
              $("#tempat_lahir").val(json.content[i].TMPT_LHR);
              $("#tanggal_lahir").val(json.content[i].TGL_LHR);
              $("#gol_darah").val(json.content[i].GOL_DARAH);
              $("#agama").val(json.content[i].AGAMA);
              $("#status_perkawinan").val(json.content[i].STATUS_KAWIN);
              $("#status_dalam_keluarga").val(json.content[i].STAT_HBKEL);
              $("#jenis_kelamin").val(json.content[i].JENIS_KLMIN);
              $("#pendidikan").val(json.content[i].PDDK_AKH);
              $("#pekerjaan").val(json.content[i].JENIS_PKRJN);
              $("#kode_provinsi").val(json.content[i].NO_PROP);
              $("#nama_provinsi").val(json.content[i].PROP_NAME);
              $("#kode_kabupaten").val(json.content[i].NO_KAB);
              $("#nama_kabupaten").val(json.content[i].KAB_NAME);
              $("#kode_kecamatan").val(json.content[i].NO_KEC);
              $("#nama_kecamatan").val(json.content[i].KEC_NAME);
              $("#kode_desa").val(json.content[i].NO_KEL);
              $("#nama_desa").val(json.content[i].KEL_NAME);
              $("#alamat").val(json.content[i].ALAMAT);
              $("#rt").val(json.content[i].NO_RT);
              $("#rw").val(json.content[i].NO_RW);
              $("#dusun").val(json.content[i].DUSUN);
              $("#kode_pos").val(json.content[i].KODE_POS);
            }
        $('#spin_form').hide();
      }
    });
  });
});
</script>