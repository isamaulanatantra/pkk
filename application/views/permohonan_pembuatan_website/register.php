
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>PERMOHONAN WEBSITE DESA</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Permohonan Website Desa</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
<section class="content" id="awal">
  <div class="info-box">
    <ul class="nav nav-tabs">
			<li class="nav-item"><a class="nav-link active" href="#tab_form_permohonan_pembuatan_website" data-toggle="tab" id="klik_tab_input">Form</a></li>
			<li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab" id="tampilkan_data_permohonan_pembuatan_website">Data Permohonan Pembuatan Website Desa</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_form_permohonan_pembuatan_website">
        <div class="">
          <div class="box box-default">
            <div class="box-body">
              <input name="page" id="page" value="1" type="hidden" value="">
              <input name="tabel" id="tabel" value="permohonan_pembuatan_website" type="hidden" value="">
            <!--
              <h3 class="" id="judul_formulir">FORMULIR PERMOHONAN PEMBUATAN WEBSITE</h3>-->
              <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>lampiran_permohonan_pembuatan_website/upload/?table_name=permohonan_pembuatan_website" enctype="multipart/form-data">
                <div class="box-body">
                  <div class="form-group" style="display:none;">
                    <label for="temp">temp</label>
                    <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="mode">mode</label>
                    <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="id_permohonan_pembuatan_website">id_permohonan_pembuatan_website</label>
                    <input class="form-control" id="id_permohonan_pembuatan_website" name="id" value="" placeholder="id_permohonan_pembuatan_website" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="nomor_surat">nomor_surat</label>
                    <input class="form-control" id="nomor_surat" name="nomor_surat" value="-" placeholder="nomor_surat" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="perihal_surat">perihal_surat</label>
                    <input class="form-control" id="perihal_surat" name="perihal_surat" value="Ijin Penelitian" placeholder="perihal_surat" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="tanggal_surat">tanggal_surat</label>
                    <input class="form-control" id="tanggal_surat" name="tanggal_surat" value="" placeholder="tanggal_surat" type="text">
                  </div>
                  <div class="form-group">
                    <label for="nama">Nama Pemohon</label>
                    <input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
                  </div>
                  <div class="form-group">
                    <label for="nomot_telp">Nomot Telp Pemohon</label>
                    <input class="form-control" id="nomot_telp" name="nomot_telp" value="" placeholder="Nomot Telp" type="text">
                  </div>
                  <div class="form-group">
                    <label for="email">Email Pemohon</label>
                    <input class="form-control" id="email" name="email" value="" placeholder="email" type="text">
                  </div>
                  <div class="form-group">
                    <label for="alamat">Alamat Pemohon</label>
                    <input class="form-control" id="alamat" name="alamat" value="" placeholder="Alamat" type="text">
                  </div>
                  <div class="form-group">
                    <label for="jabatan">Jabatan Pemohon</label>
                    <input class="form-control" id="jabatan" name="jabatan" value="" placeholder="Jabatan" type="text">
                  </div>
                  <div class="form-group">
                    <label>Kecamatan</label>
                    <select class="form-control" id="id_kecamatan"></select>
                  </div>
                  <div class="form-group">
                    <label>Desa</label>
                    <select class="form-control" id="id_desa"></select>
                  </div>
                  <div class="form-group">
                    <div class="alert alert-info alert-dismissable">
                      <div class="form-group">
                        <label for="remake">Pilih satu per satu hingga ketiganya Anda Upload</label>
                        <select class="form-control" id="remake" name="remake" >
                        <option value="Scan Surat Permohonan">Scan Surat Permohonan</option>
                        <option value="Scan Surat Kuasa">Scan Surat Kuasa</option>
                        <option value="Scan KTP">Scan KTP</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="myfile">Setelah pilih persyaratan kemudian cari file disini</label>
                        <input type="file" size="60" name="myfile" id="file_lampiran" >
                      </div>
                      <div id="ProgresUpload">
                        <div id="BarProgresUpload"></div>
                        <div id="PersenProgresUpload">0%</div >
                      </div>
                      <div id="PesanProgresUpload"></div>
                    </div>
                    <div class="alert alert-info alert-dismissable">
                      <label class="">Data Lampiran </label>
                      <table class="table table-hover">
                        <thead>
                          <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                        </thead>
                        <tbody id="tbl_lampiran_permohonan_pembuatan_website">
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="box-footer">
                  <div class="overlay" id="overlay_form_input" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                  </div>
                  <button type="submit" class="btn btn-primary" id="simpan_permohonan_pembuatan_website"><i class="fa fa-sand"></i>KIRIM </button>
                  <button type="submit" class="btn btn-primary" id="update_permohonan_pembuatan_website" style="display:none;">UPDATE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab_3">
										<div class="box">
											<div class="box-header">
												<h3 class="box-title">&nbsp;</h3>
												<div class="box-tools">
													<div class="input-group input-group-sm">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="10">10 Per-Halaman</option>
															<option value="25">25 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
															<option value="999999999">Semua</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="permohonan_pembuatan_website.nama">Nama</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_permohonan_pembuatan_website"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="box-body table-responsive p-0 no-padding">
												<table class="table table-striped table-bordered">
													<tr>
														<th>NO</th>
														<th>Tanggal</th>
														<th>Kecamatan</th>
														<th>Desa</th>
														<!--<th>Domain</th>-->
														<th>Nama</th> 
														<th>Alamat</th> 
														<th>Jabatan</th> 
														<th>No.Telp</th> 
														<th>Email</th> 
														<th>Alamat</th> 
														<th>Proses</th> 
													</tr>
													<tbody id="tbl_data_permohonan_pembuatan_website">
													</tbody>
												</table>
											</div>
											<div class="box-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_permohonan_pembuatan_website" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_surat, #tanggal_surat_kesbangpol').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>

<script>
  function AfterSavedPermohonan_informasi_publik() {
    $('#nama, #nomot_telp, #email, #alamat, #jabatan, #id_kecamatan, #id_desa #temp').val('');
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_permohonan_pembuatan_website').show();
    $('#update_permohonan_pembuatan_website').hide();
    $('#tbl_lampiran_permohonan_pembuatan_website').html('');
    $('#nama, #nomot_telp, #email, #alamat, #jabatan, #id_kecamatan, #id_desa #temp').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORM');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_permohonan_pembuatan_website').on('click', function(e) {
      e.preventDefault();
      $('#simpan_permohonan_pembuatan_website').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
      $('#pesan_terkirim').show();
      var parameter = [ 'nama', 'nomot_telp', 'email', 'alamat', 'jabatan', 'id_kecamatan', 'id_desa', 'temp' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["nama"] = $("#nama").val();
      parameter["nomot_telp"] = $("#nomot_telp").val();
      parameter["email"] = $("#email").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["jabatan"] = $("#jabatan").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["temp"] = $("#temp").val();
      var url = '<?php echo base_url(); ?>permohonan_pembuatan_website/simpan_permohonan_pembuatan_website_register';
      var parameterRv = [ 'nama', 'nomot_telp', 'email', 'alamat', 'jabatan', 'id_kecamatan', 'id_desa', 'temp' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
				$('#overlay_form_input').fadeOut();
				alert('gagal');
				$('html, body').animate({
					scrollTop: $('#awal').offset().top
				}, 1000);
      }
      else{
        SimpanData(parameter, url);
				$('#simpan_permohonan_pembuatan_website').removeAttr('disabled', 'disabled');
				$('#overlay_form_input').fadeOut('slow');
				$('#pesan_terkirim').fadeOut('slow');
      }
    });
  });
</script>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_lampiran_permohonan_pembuatan_website').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'permohonan_pembuatan_website',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>lampiran_permohonan_pembuatan_website/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_lampiran_permohonan_pembuatan_website="'+json[i].id_lampiran_permohonan_pembuatan_website+'" id="'+json[i].id_lampiran_permohonan_pembuatan_website+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/lampiran_permohonan_pembuatan_website/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_lampiran_permohonan_pembuatan_website').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_permohonan_pembuatan_website').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_lampiran_permohonan_pembuatan_website').on('click', '#del_ajax', function() {
    var id_lampiran_permohonan_pembuatan_website = $(this).closest('tr').attr('id_lampiran_permohonan_pembuatan_website');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_lampiran_permohonan_pembuatan_website"] = id_lampiran_permohonan_pembuatan_website;
        var url = '<?php echo base_url(); ?>lampiran_permohonan_pembuatan_website/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_permohonan_pembuatan_website').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_lampiran_permohonan_pembuatan_website='+id_lampiran_permohonan_pembuatan_website+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script>
  function load_option_kecamatan() {
    $('#id_kecamatan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/option_kecamatan_by_id_kabupaten/',
      success: function(html) {
        $('#id_kecamatan').html('<option value="semua">Pilih Kecamatan</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_option_kecamatan();
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var id_kecamatan = $('#id_kecamatan').val();
      load_option_desa(id_kecamatan);
    });
  });
</script>
<script>
  function load_option_desa(id_kecamatan) {
    $('#id_desa').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html('<option value="semua">Pilih Desa</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/total_data_register',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'_register/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script>
  function load_wilayah_by_id_desa(id_desa) {
    $('#id_desa').html('');
    // $('#id_kecamatan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_desa:id_desa
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_wilayah_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_desa').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
        }
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_permohonan_pembuatan_website').on('click', '#del_ajax_permohonan_pembuatan_website', function() {
    var id_permohonan_pembuatan_website = $(this).closest('tr').attr('id_permohonan_pembuatan_website');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_pembuatan_website"] = id_permohonan_pembuatan_website;
        var url = '<?php echo base_url(); ?>permohonan_pembuatan_website/hapus/';
        HapusData(parameter, url);
        $('[id_permohonan_pembuatan_website='+id_permohonan_pembuatan_website+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_permohonan_pembuatan_website').on('click', '.update_id_permohonan_pembuatan_website', function() {
    $('#mode').val('edit');
    $('#simpan_permohonan_pembuatan_website').hide();
    $('#update_permohonan_pembuatan_website').show();
    var id_permohonan_pembuatan_website = $(this).closest('tr').attr('id_permohonan_pembuatan_website');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_permohonan_pembuatan_website');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_permohonan_pembuatan_website').val(id_permohonan_pembuatan_website);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_permohonan_pembuatan_website:id_permohonan_pembuatan_website
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>permohonan_pembuatan_website/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#tanggal_surat').val(json[i].tanggal_surat);
            $('#nama').val(json[i].nama);
            $('#nomot_telp').val(json[i].nomot_telp);
            $('#nik').val(json[i].nik);
            $('#email').val(json[i].email);
            $('#alamat').val(json[i].alamat);
            $('#jabatan').val(json[i].jabatan);
						load_wilayah_by_id_desa(json[i].id_desa);
						load_option_kecamatan();
          }
        }
      });
    AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_permohonan_pembuatan_website').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'nomot_telp', 'nama', 'temp', 'nik', 'email', 'jabatan', 'alamat', 'id_kecamatan', 'id_desa' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_permohonan_pembuatan_website"] = $("#id_permohonan_pembuatan_website").val();
      parameter["temp"] = $("#temp").val();
      parameter["nama"] = $("#nama").val();
      parameter["nomot_telp"] = $("#nomot_telp").val();
      parameter["nik"] = $("#nik").val();
      parameter["email"] = $("#email").val();
      parameter["jabatan"] = $("#jabatan").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      var url = '<?php echo base_url(); ?>permohonan_pembuatan_website/update_permohonan_pembuatan_website';
      
      var parameterRv = [ 'nomot_telp', 'nama', 'temp', 'nik', 'email', 'jabatan', 'alamat', 'id_kecamatan', 'id_desa' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>
