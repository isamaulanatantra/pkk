<!--
hak : 1 [root]
	<li><a href="<?php echo base_url(); ?>users">Users</a></li>
-->
<ul class="nav navbar-nav">
	<li><a href="<?php echo base_url(); ?>halaman">Halaman</a></li>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus-square"></i> Komponen<span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
      <li><a href="<?php echo base_url(); ?>tema_website"><i class="fa fa-plus-square"></i> Tema Website </a></li>
      <li><a href="<?php echo base_url(); ?>dasar_website"><i class="fa fa-plus-square"></i> Data Umum </a></li>
      <li><a href="<?php echo base_url(); ?>header_website"><i class="fa fa-plus-square"></i> Header Web </a></li>
      <li><a href="<?php echo base_url(); ?>slide_website"><i class="fa fa-plus-square"></i> Gambar Slide </a></li>
      <li><a href="<?php echo base_url(); ?>komponen"><i class="fa fa-plus-square"></i> Komponen </a></li>
    </ul>
  </li>
</ul>