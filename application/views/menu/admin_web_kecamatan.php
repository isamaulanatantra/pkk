
        <!-- sidebar menu: : style can be found in sidebar.less
        <li><a href="<?php echo base_url(); ?>users"><i class="fa fa-circle-o text-red"></i> <span>Users</span></a></li> -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="<?php echo base_url(); ?>konten"><i class="fa fa-circle-o text-red"></i> <span>Update Konten</span></a></li>
        <li><a href="<?php echo base_url(); ?>posting"><i class="fa fa-circle-o text-red"></i> <span>Posting</span></a></li>
				<li><a href="<?php echo base_url(); ?>permohonan_informasi_publik"><i class="fa fa-child text-red"></i> <span>Permohonan Informasi Publik</span></a></li>
				<!-- <li><a href="<?php echo base_url(); ?>daftar_informasi_publik"><i class="fa fa-building text-red"></i> <span>Pengolahan Daftar Informasi Publik</span></a></li>
        -->
        <li class="treeview">
          <a href="#">
          <i class="fa fa-dashboard"></i> <span>Buku Agenda</span> <i class="fa fa-book pull-right"></i>
          </a>
          <ul class="treeview-menu">
          <li><a href="<?php echo base_url(); ?>surat/?id_kelembagaan=3&jenis_surat=surat_masuk"><i class="fa fa-circle-o"></i> Surat Masuk</a></li>
          <li><a href="<?php echo base_url(); ?>surat/?id_kelembagaan=3&jenis_surat=surat_keluar"><i class="fa fa-circle-o"></i> Surat Keluar</a></li>
          </ul>
        </li>
		<li class="treeview">
			<a href="#">
			<i class="fa fa-dashboard"></i> <span>Komponen</span> <i class="fa fa-angle-left pull-right"></i>
			</a>
			<ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>tema_website"><i class="fa fa-circle-o"></i> Tema Website</a></li>
			<li><a href="<?php echo base_url(); ?>dasar_website"><i class="fa fa-circle-o"></i> Data Umum</a></li>
			<!--<li><a href="<?php echo base_url(); ?>data_skpd"><i class="fa fa-circle-o"></i> Data SKPD</a></li>-->
			<li><a href="<?php echo base_url(); ?>slide_website"><i class="fa fa-circle-o"></i> Gambar Slide</a></li>
			<li><a href="<?php echo base_url(); ?>komponen"><i class="fa fa-circle-o"></i> Komponen</a></li>
			<!--<li><a href="<?php echo base_url(); ?>peraturan"><i class="fa fa-circle-o"></i> Produk Hukum</a></li>-->
			</ul>
		</li>
      </ul>