
							<?php 
							$where = array(
							'id_tabel' => $this->session->userdata('id_users'),
							'table_name' => 'users'
							);
							$this->db->where($where);
							$this->db->limit(1);
							$this->db->from('attachment');
							$jml = $this->db->count_all_results(); // Produces an integer, like 17
							if( $jml > 0 ){
								$this->db->where($where);
								$this->db->limit(1);
								$query = $this->db->get('attachment');
								foreach ($query->result() as $row)
									{
									$file_kecil = 'k_'.$row->file_name.'';
									$file_sedang = 's_'.$row->file_name.'';
									}
								}
							else{
									$file_kecil = 'logo wonosobo.png';
									$file_sedang = 'logo wonosobo.png';
								}
								$where1 = array(
								'id_users' => $this->session->userdata('id_users'),
								);
								$this->db->where($where1);
								$this->db->limit(1);
								$query1 = $this->db->get('users');
								foreach ($query1->result() as $row1)
									{
									$nama_user = ''.$row1->nama.'';
									}
							?>
							
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url(); ?>media/upload/<?php echo $file_sedang; ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $nama_user; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu nav nav-pills nav-sidebar flex-column-->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
                <i class="right fa fa-angle-left text-warning"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>welcome_admin" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-warning"></i>
                  <p>Dashboard </p>
                </a>
              </li>
            </ul>
          </li>
			
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-sitemap"></i>
              <p>
                SIM PKK
                <i class="right fa fa-angle-left text-success"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item"><span class="text-success">DATA REKAPITULASI</li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>pokja_satu/rekap" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>POKJA I</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>pokja_dua/rekap" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>POKJA II</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>pokja_tiga/rekap" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>POKJA III</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>pokja_empat/rekap" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>POKJA IV</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>input_rekap_data_umum/rekap" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>REKAP DATA UMUM</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>REKAP CATATAN DATA KEGIATAN WARGA</p>
                </a>
              </li>
            <!--
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>data_keluarga" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>Data Keluarga</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>catatan_data_dan_kegiatan_warga" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>Catantan Data dan Kegiatan Warga</p>
                </a>
              </li>
              <li class="nav-item"><span class="text-success">MASTER</li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>dusun" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>Nama Dusun</p>
                </a>
              </li>-->
            </ul>
          </li>
			
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-copy"></i>
              <p>
                BUKU AGENDA
                <i class="right fa fa-angle-left text-success"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>surat/?id_kelembagaan=3&jenis_surat=surat_masuk" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>SURAT MASUK</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>surat/?id_kelembagaan=3&jenis_surat=surat_keluar" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>SURAT KELUAR</p>
                </a>
              </li>
            </ul>
          </li>
          
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>posting" class="nav-link">
              <i class="nav-icon fa fa-book text-primary"></i>
              <p>
                Posting
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-cubes"></i>
              <p>
                Komponen
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview"><!--
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>tema_website" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Tema Website</p>
                </a>
              </li>-->
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>dasar_website" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Data Umum</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>slide_website" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Gambar Slide</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>komponen" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Komponen beranda</p>
                </a>
              </li>
            </ul>
          </li>
			
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>dokumentasi" class="nav-link">
              <i class="nav-icon fa fa-file"></i>
              <p>Documentation</p>
                <span class="right badge badge-danger">New</span>
            </a>
          </li>
        </ul>
      </nav>