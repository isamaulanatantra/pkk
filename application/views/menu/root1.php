<!--
hak : 1 [root]
-->
<ul class="nav navbar-nav">
	<li><a href="<?php echo base_url(); ?>">Home</a></li>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus-square"></i> Master Kependudukan<span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
      <li><a href="<?php echo base_url(); ?>propinsi"><i class="fa fa-plus-square"></i> Provinsi </a></li>
      <li><a href="<?php echo base_url(); ?>kabupaten"><i class="fa fa-plus-square"></i> Kabupaten</a></li>
      <li><a href="<?php echo base_url(); ?>kecamatan"><i class="fa fa-plus-square"></i> Kecamatan</a></li>
      <li><a href="<?php echo base_url(); ?>desa"><i class="fa fa-plus-square"></i> Desa</a></li>
      <li><a href="<?php echo base_url(); ?>agama"><i class="fa fa-plus-square"></i> Agama</a></li>
      <li><a href="<?php echo base_url(); ?>golongan_darah"><i class="fa fa-plus-square"></i> Golongan Darah</a></li>
      <li><a href="<?php echo base_url(); ?>jenis_kelamin"><i class="fa fa-plus-square"></i> Jenis Kelamin</a></li>
      <li><a href="<?php echo base_url(); ?>kewarganegaraan"><i class="fa fa-plus-square"></i> Kewarganegaraan</a></li>
      <li><a href="<?php echo base_url(); ?>pendidikan"><i class="fa fa-plus-square"></i> Pendidikan</a></li>
      <li><a href="<?php echo base_url(); ?>shdk"><i class="fa fa-plus-square"></i> SHDK</a></li>
      <li><a href="<?php echo base_url(); ?>status_pernikahan"><i class="fa fa-plus-square"></i> Status Pernikahan</a></li>
      <li><a href="<?php echo base_url(); ?>pekerjaan"><i class="fa fa-plus-square"></i> Pekerjaan</a></li>
    </ul>
  </li>
  <li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-plus-square"></i> Master 2 <span class="caret"></span></a>
    <ul class="dropdown-menu" role="menu">
      <li><a href="<?php echo base_url(); ?>jenis_diklat">Jenis DIklat</a></li>
      <li><a href="<?php echo base_url(); ?>pangkat_pegawai">Pangkat Pegawai</a></li>
      <li><a href="<?php echo base_url(); ?>golongan_pegawai">Golongan Pegawai</a></li>
      <li><a href="<?php echo base_url(); ?>unit_kerja">Unit Kerja</a></li>
    </ul>
  </li>
  <li><a href="<?php echo base_url(); ?>pegawai">Data Umum Pegawai</a></li>
</ul>