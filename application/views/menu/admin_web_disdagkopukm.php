
        <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="<?php echo base_url(); ?>komoditi"><i class="fa fa-circle-o text-red"></i> <span>Komoditi</span></a></li>
        <li><a href="<?php echo base_url(); ?>kebutuhan_pokok"><i class="fa fa-circle-o text-red"></i> <span>Harga Kebutuhan Pokok</span></a></li>
        <li class="treeview">
          <a href="#">
          <i class="fa fa-book"></i> <span>Laporan</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
          <li><a href="<?php echo base_url(); ?>laporan_perkembangan"><i class="fa fa-circle-o"></i> Laporan Harian Per Pasar</a></li>
          <li><a href="<?php echo base_url(); ?>laporan_perkembangan_semua_pasar"><i class="fa fa-circle-o"></i> Laporan Harian Semua Pasar</a></li>
          <li><a href="<?php echo base_url(); ?>laporan_perkembangan_mingguan_per_pasar"><i class="fa fa-circle-o"></i> Laporan Mingguan Per Pasar</a></li>
          <li><a href="<?php echo base_url(); ?>laporan_perkembangan_bulanan_semua_pasar"><i class="fa fa-circle-o"></i> Laporan Bulanan Semua Pasar</a></li>
          </ul>
        </li>
        <li><a href="<?php echo base_url(); ?>posting"><i class="fa fa-circle-o text-red"></i> <span>Posting</span></a></li>
        <li class="treeview">
          <a href="#">
          <i class="fa fa-dashboard"></i> <span>Buku Agenda</span> <i class="fa fa-book pull-right"></i>
          </a>
          <ul class="treeview-menu">
          <li><a href="<?php echo base_url(); ?>surat/?id_kelembagaan=3&jenis_surat=surat_masuk"><i class="fa fa-circle-o"></i> Surat Masuk</a></li>
          <li><a href="<?php echo base_url(); ?>surat/?id_kelembagaan=3&jenis_surat=surat_keluar"><i class="fa fa-circle-o"></i> Surat Keluar</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
          <i class="fa fa-dashboard"></i> <span>Komponen</span> <i class="fa fa-angle-left pull-right"></i>
          </a>
          <ul class="treeview-menu">
          <li><a href="<?php echo base_url(); ?>tema_website"><i class="fa fa-circle-o"></i> Tema Website</a></li>
          <li><a href="<?php echo base_url(); ?>dasar_website"><i class="fa fa-circle-o"></i> Data Umum</a></li>
          <li><a href="<?php echo base_url(); ?>data_skpd"><i class="fa fa-circle-o"></i> Data SKPD</a></li>
          <li><a href="<?php echo base_url(); ?>slide_website"><i class="fa fa-circle-o"></i> Gambar Slide</a></li>
          <li><a href="<?php echo base_url(); ?>vidio_intro"><i class="fa fa-circle-o"></i> Vidio Intro</a></li>
          <li><a href="<?php echo base_url(); ?>komponen"><i class="fa fa-circle-o"></i> Komponen</a></li>
          <!--<li><a href="<?php echo base_url(); ?>peraturan"><i class="fa fa-circle-o"></i> Produk Hukum</a></li>-->
          </ul>
        </li>
      </ul>