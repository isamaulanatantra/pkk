
        <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li><a href="<?php echo base_url(); ?>skpd"><i class="fa fa-circle-o text-red"></i> <span>SKPD</span></a></li>
        <li><a href="<?php echo base_url(); ?>users"><i class="fa fa-circle-o text-red"></i> <span>Users</span></a></li>
        <li><a href="<?php echo base_url(); ?>halaman"><i class="fa fa-circle-o text-red"></i> <span>Halaman</span></a></li>
		<li class="treeview">
		  <a href="#">
			<i class="fa fa-dashboard"></i> <span>Komponen</span> <i class="fa fa-angle-left pull-right"></i>
		  </a>
		  <ul class="treeview-menu">
			<li><a href="<?php echo base_url(); ?>tema_website"><i class="fa fa-circle-o"></i> Tema Website</a></li>
			<li><a href="<?php echo base_url(); ?>dasar_website"><i class="fa fa-circle-o"></i> Data Umum</a></li>
			<li><a href="<?php echo base_url(); ?>data_skpd"><i class="fa fa-circle-o"></i> Data SKPD</a></li>
			<li><a href="<?php echo base_url(); ?>slide_website"><i class="fa fa-circle-o"></i> Gambar Slide</a></li>
			<li><a href="<?php echo base_url(); ?>vidio_intro"><i class="fa fa-circle-o"></i> Vidio Intro</a></li>
			<li><a href="<?php echo base_url(); ?>komponen"><i class="fa fa-circle-o"></i> Komponen</a></li>
		  </ul>
		</li>
      </ul>