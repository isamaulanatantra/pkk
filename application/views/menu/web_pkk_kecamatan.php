
							<?php 
							$where = array(
							'id_tabel' => $this->session->userdata('id_users'),
							'table_name' => 'users'
							);
							$this->db->where($where);
							$this->db->limit(1);
							$this->db->from('attachment');
							$jml = $this->db->count_all_results(); // Produces an integer, like 17
							if( $jml > 0 ){
								$this->db->where($where);
								$this->db->limit(1);
								$query = $this->db->get('attachment');
								foreach ($query->result() as $row)
									{
									$file_kecil = 'k_'.$row->file_name.'';
									$file_sedang = 's_'.$row->file_name.'';
									}
								}
							else{
									$file_kecil = 'logo wonosobo.png';
									$file_sedang = 'logo wonosobo.png';
								}
								$where1 = array(
								'id_users' => $this->session->userdata('id_users'),
								);
								$this->db->where($where1);
								$this->db->limit(1);
								$query1 = $this->db->get('users');
								foreach ($query1->result() as $row1)
									{
									$nama_user = ''.$row1->nama.'';
									}
							?>
							
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url(); ?>media/upload/<?php echo $file_sedang; ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $nama_user; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu nav nav-pills nav-sidebar flex-column-->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-dashboard text-warning"></i>
              <p>
                Dashboard
                <i class="right fa fa-angle-left text-warning"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>welcome_admin" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-warning"></i>
                  <p>Dashboard </p>
                </a>
              </li>
            </ul>
          </li>
			
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-sitemap text-success"></i>
              <p>
                SIM PKK
                <i class="right fa fa-angle-left text-success"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item"><span class="text-success">DATA KEGIATAN</li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>pokja_satu" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>INPUT DATA POKJA I</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>pokja_dua" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>INPUT DATA POKJA II</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>pokja_tiga" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>INPUT DATA POKJA III</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>pokja_empat" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>INPUT DATA POKJA IV</p>
                </a>
              </li>
              <li class="nav-item"><span class="text-success">DATA UMUM</li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>input_rekap_data_umum" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>INPUT REKAP DATA UMUM</p>
                </a>
              </li>
              <li class="nav-item"><span class="text-success">REKAP </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>rekapdesa" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>CATATAN DATA DAN KEGIATAN WARGA</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-sitemap text-success"></i>
              <p>
                BUKU AGENDA
                <i class="right fa fa-angle-left text-success"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>surat/?id_kelembagaan=3&jenis_surat=surat_masuk" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>SURAT MASUK</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>surat/?id_kelembagaan=3&jenis_surat=surat_keluar" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>SURAT KELUAR</p>
                </a>
              </li>
            </ul>
          </li>
			
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>posting" class="nav-link">
              <i class="nav-icon fa fa-newspaper-o text-primary"></i>
              <p>
                Posting
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>dokumentasi" class="nav-link">
              <i class="nav-icon fa fa-file"></i>
              <p>Documentation</p>
                <span class="right badge badge-danger">New</span>
            </a>
          </li>
        </ul>
      </nav>