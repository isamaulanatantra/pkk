
							<?php 
							$where = array(
							'id_tabel' => $this->session->userdata('id_users'),
							'table_name' => 'users'
							);
							$this->db->where($where);
							$this->db->limit(1);
							$this->db->from('attachment');
							$jml = $this->db->count_all_results(); // Produces an integer, like 17
							if( $jml > 0 ){
								$this->db->where($where);
								$this->db->limit(1);
								$query = $this->db->get('attachment');
								foreach ($query->result() as $row)
									{
									$file_kecil = 'k_'.$row->file_name.'';
									$file_sedang = 's_'.$row->file_name.'';
									}
								}
							else{
									$file_kecil = 'logo wonosobo.png';
									$file_sedang = 'logo wonosobo.png';
								}
								$where1 = array(
								'id_users' => $this->session->userdata('id_users'),
								);
								$this->db->where($where1);
								$this->db->limit(1);
								$query1 = $this->db->get('users');
								foreach ($query1->result() as $row1)
									{
									$nama_user = ''.$row1->nama.'';
									}
							?>
							
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php echo base_url(); ?>media/upload/<?php echo $file_sedang; ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $nama_user; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu nav nav-pills nav-sidebar flex-column-->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-dashboard"></i>
              <p>
                Dashboard
                <i class="right fa fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>welcome_admin" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Dashboard </p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Dashboard b</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-comments text-danger"></i>
              <p>
                PELAYANAN
                <i class="fa fa-angle-left right"></i>
              </p>
                <span class="right badge badge-danger">New</span>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>permohonan_pendaftaran_badan_usaha" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-success"></i>
                  <p>TDUP | Permohonan Pendaftaran Badan Usaha</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>permohonan_organisasi" class="nav-link">
                  <i class="fa fa-circle-o nav-icon text-danger"></i>
                  <p>Permohonan Surat Pengesahan</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>permohonan_informasi_publik" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Permohonan Informasi Publik</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>pengaduan_masyarakat" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Pengaduan Masyarakat</p>
                </a>
              </li>
            </ul>
          </li>
          <!--<li class="nav-item">
            <a href="<?php echo base_url(); ?>peraturan" class="nav-link">
              <i class="nav-icon fa fa-globe"></i>
              <p>Produk Hukum</p>
            </a>
          </li>-->
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-cubes"></i>
              <p>
                MASTER
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>jenis_kesenian" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Jenis Kesenian</p>
                </a>
              </li>
            </ul><!--
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>perihal_permohonan_pendaftaran_badan_usaha" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Perihal </p>
                </a>
              </li>
            </ul>-->
          </li>
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>posting" class="nav-link">
              <i class="nav-icon fa fa-newspaper-o"></i>
              <p>
                Posting
              </p>
            </a>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-book"></i>
              <p>
                Buku Agenda
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>surat/?id_kelembagaan=3&jenis_surat=surat_masuk" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Surat Masuk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>surat/?id_kelembagaan=3&jenis_surat=surat_keluar" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Surat Keluar</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-cubes"></i>
              <p>
                Komponen
                <i class="fa fa-angle-left right"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>tema_website" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Tema Website</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>dasar_website" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Data Umum</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>slide_website" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Gambar Slide</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="<?php echo base_url(); ?>komponen" class="nav-link">
                  <i class="fa fa-circle-o nav-icon"></i>
                  <p>Komponen beranda</p>
                </a>
              </li>
            </ul>
          </li>
			
          <li class="nav-item">
            <a href="<?php echo base_url(); ?>dokumentasi" class="nav-link">
              <i class="nav-icon fa fa-file"></i>
              <p>Documentation</p>
            </a>
          </li>
        </ul>
      </nav>