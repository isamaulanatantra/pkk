<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
$ses=$this->session->userdata('id_users');
if(!$ses) { return redirect(''.base_url().'login');  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Integrated Website <?php $web=$this->uut->namadomain(base_url()); $aa=explode(".",$web);echo $aa[0]; ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/Font-Awesome-master/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- SweetAlert2
  <link rel="stylesheet" href="../../plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
  -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sweetalert2/bootstrap-4.min.css">
  <!-- Toastr -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.css">
  <!-- Theme 
  <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
  style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte3.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>  
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.css">
  
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
  			<?php 
				$wherea = array(
				'id_users' => $this->session->userdata('id_users')
				);
				$this->db->where($wherea);
				$this->db->from('users');
				$jmla = $this->db->count_all_results();
				if( $jmla > 0 ){
					$this->db->where($wherea);
					$querya = $this->db->get('users');
					foreach ($querya->result() as $rowa)
						{
                  $this->load->view('menu/nav_'.$rowa->hak_akses.'');
						}
					}
				else{
						exit;
					}
			?>
  </nav>

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?php echo base_url(); ?>" class="brand-link">
      <img src="<?php echo base_url(); ?>media/upload/logo wonosobo.png"
           alt="<?php $web=$this->uut->namadomain(base_url()); $aa=explode(".",$web);echo $aa[0]; ?>"
           class="brand-image img-circle elevation-2"
           style="opacity: .8">
      <span class="brand-text font-weight-light"><b><?php $web=$this->uut->namadomain(base_url()); $aa=explode(".",$web);echo $aa[0]; ?></b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
  			<?php 
				$where = array(
				'id_users' => $this->session->userdata('id_users')
				);
				$this->db->where($where);
				$this->db->from('users');
				$jml = $this->db->count_all_results();
				if( $jml > 0 ){
					$this->db->where($where);
					$query = $this->db->get('users');
					foreach ($query->result() as $row)
						{
                  $this->load->view('menu/'.$row->hak_akses.'');
						}
					}
				else{
						exit;
					}
			?>
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
		<?php $this -> load -> view($main_view);  ?>
    <!-- Content Header (Page header) -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.0-rc.5
    </div>
    <strong>Copyright &copy; 2014-2019 <a href="http://adminlte.io">AdminLTE.io</a>.</strong> All rights
    reserved.
  </footer>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
 -->
<!-- Bootstrap 4
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
 -->
<script src="https://adminlte.io/themes/dev/AdminLTE/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="https://adminlte.io/themes/dev/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- SweetAlert2 -->
<script src="<?php echo base_url(); ?>assets/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte3.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

		<script src="<?php echo base_url(); ?>assets/js/uutv1.js"></script>
		<!-- alertify -->
		<script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
    <script src="<?php echo base_url(); ?>boots/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
</body>
</html>
