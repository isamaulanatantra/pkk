
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Daftar Informasi Publik</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Daftar Informasi Publik</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab" id="klik_tab_tampil">Data</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
				
										<div class="row">
											<div class="col-sm-12 col-md-6">
											</div>
											<div class="col-sm-12 col-md-6">
												<select name="limit_data_daftar_informasi_publik" class="form-control input-sm pull-right" style="width: 160px;" id="limit_data_daftar_informasi_publik">
													<option value="999999999">Semua</option>
													<option value="10">10 Per-Halaman</option>
													<option value="20">20 Per-Halaman</option>
													<option value="50">50 Per-Halaman</option>
												</select>
												<select name="urut_data_daftar_informasi_publik" class="form-control input-sm pull-right" style="display:none; width: 160px;" id="urut_data_daftar_informasi_publik">
													<option value="urut">Nomor Urut</option>
												</select>
												<select name="kategori_informasi_publik" class="form-control input-sm pull-right" style="width: 170px;" id="kategori_informasi_publik">
													<option value="pilih_kategori_informasi">Pilih Kategori Informasi</option>
													<option value="informasi_berkala">Informasi Berkala</option>
													<option value="informasi_serta_merta">Informasi Serta Merta</option>
													<option value="informasi_setiap_saat">Informasi Setiap Saat</option>
													<option value="informasi_dikecualikan">Informasi Dikecualikan</option>
												</select>
												<select name="opd_domain" class="form-control input-sm pull-right" style="display:none; width: 160px;" id="opd_domain">
												</select>
											</div>
										</div>
										<table class="table table-bordered table-hover">
											<tbody>
												<thead>
													<th>NO</th>
													<th>Judul Posting</th>
													<th>Kategori</th>
													<th>Informasi Berkala</th>
													<th>Informasi Serta Merta</th>
													<th>Informasi Setiap Saat</th>
													<th>Informasi Dikecualikan</th>
													<th>Proses</th>
												</thead>
											</tbody>
											<tbody id="tbl_utama_daftar_informasi_publik">
											</tbody>
										</table>
										<div class="overlay" id="spinners_data" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
								</div>
							</div>
                <!-- /.tab-content -->
						</div><!-- /.card-body -->
            <!-- ./card -->
          </div>
							
        </div>
        <!-- /.row -->
				

    </section>
		
<!----------------------->
<script>
  function load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain) {
    $('#tbl_utama_daftar_informasi_publik').html('');
    $('#spinners_data').show();
    var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:halaman,
        limit:limit,
        kata_kunci:kata_kunci,
        urut_data_daftar_informasi_publik:urut_data_daftar_informasi_publik,
        kategori_informasi_publik:kategori_informasi_publik,
        opd_domain:opd_domain
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>daftar_informasi_publik/json_all_data_informasi/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr style="padding: 2px;" id_posting="' + json[i].id_posting + '" id="' + json[i].id_posting + '" >';
					tr += '<td valign="top" style="padding: 2px;">' + (start) + '</td>';
          tr += '<td valign="top" style="padding: 2px;">';
					tr += ''+json[i].judul_posting+'';
          tr += '</td>';
          tr += '<td style="padding: 2px;"><smal>';
						if( json[i].parent == 1029172 ){
							tr += 'Profil';
						}
						else if( json[i].parent == 1029179 ){
							tr += 'Layanan Informasi';
						}
						else{
							tr += '-';
						}
          tr += '</smal></td>';
          if( json[i].informasi_berkala == 1 ){
						tr += '<td style="padding: 2px;" id="td_2_'+i+'"><a href="#" id="inaktif_informasi_berkala"> <i class="fa fa-check-square-o"></a></td>';
          }
          else{
						tr += '<td style="padding: 2px;" id="td_3_'+i+'"><a href="#" id="aktif_informasi_berkala"> <i class="fa fa-minus-square-o"></a></td>';
          }
          if( json[i].informasi_serta_merta == 1 ){
						tr += '<td style="padding: 2px;" id="td_2_'+i+'"><a href="#" id="inaktif_informasi_serta_merta"> <i class="fa fa-check-square-o"></a></td>';
          }
          else{
						tr += '<td style="padding: 2px;" id="td_3_'+i+'"><a href="#" id="aktif_informasi_serta_merta"> <i class="fa fa-minus-square-o"></a></td>';
          }
          if( json[i].informasi_setiap_saat == 1 ){
						tr += '<td style="padding: 2px;" id="td_2_'+i+'"><a href="#" id="inaktif_informasi_setiap_saat"> <i class="fa fa-check-square-o"></a></td>';
          }
          else{
						tr += '<td style="padding: 2px;" id="td_3_'+i+'"><a href="#" id="aktif_informasi_setiap_saat"> <i class="fa fa-minus-square-o"></a></td>';
          }
          if( json[i].informasi_dikecualikan == 1 ){
						tr += '<td style="padding: 2px;" id="td_2_'+i+'"><a href="#" id="inaktif_informasi_dikecualikan"> <i class="fa fa-check-square-o"></a></td>';
          }
          else{
						tr += '<td style="padding: 2px;" id="td_3_'+i+'"><a href="#" id="aktif_informasi_dikecualikan"> <i class="fa fa-minus-square-o"></a></td>';
          }
          tr += '<td style="padding: 2px;" valign="top"><a href="#" id=""><i class="fa fa-check-square-o text-green"></a></td>';
          tr += '</tr>';
        }
        $('#tbl_utama_daftar_informasi_publik').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
  }
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#limit_data_daftar_informasi_publik').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
      var kategori_informasi_publik = $('#kategori_informasi_publik').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#urut_data_daftar_informasi_publik').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
      var kategori_informasi_publik = $('#kategori_informasi_publik').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#kata_kunci').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
      var kategori_informasi_publik = $('#kategori_informasi_publik').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#opd_domain').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, opd_domain);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#kategori_informasi_publik').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
      var kategori_informasi_publik = $('#kategori_informasi_publik').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
      var halaman = 1;
      var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
      var kategori_informasi_publik = $('#kategori_informasi_publik').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
  });
</script>
<!----------------------->
<script>
  function load_opd_domain() {
    $('#opd_domain').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>daftar_informasi_publik/opd_domain/',
      success: function(html) {
        $('#opd_domain').html('<option value="">Semua OPD</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_opd_domain();
});
</script>



<!-----------DIP------------>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik').on('click', '#inaktif_informasi_berkala', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data tidak di aktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik/inaktif_informasi_berkala',
          success: function(html) {
            alertify.success('Berhasil Inaktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
						var kategori_informasi_publik = $('#kategori_informasi_publik').val();
						var opd_domain = $('#opd_domain').val();
						load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik').on('click', '#aktif_informasi_berkala', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan diaktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik/aktif_informasi_berkala',
          success: function(html) {
            alertify.success('Berhasil Aktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
						var kategori_informasi_publik = $('#kategori_informasi_publik').val();
						var opd_domain = $('#opd_domain').val();
						load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik').on('click', '#inaktif_informasi_serta_merta', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data tidak di aktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik/inaktif_informasi_serta_merta',
          success: function(html) {
            alertify.success('Berhasil Inaktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
						var kategori_informasi_publik = $('#kategori_informasi_publik').val();
						var opd_domain = $('#opd_domain').val();
						load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik').on('click', '#aktif_informasi_serta_merta', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan diaktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik/aktif_informasi_serta_merta',
          success: function(html) {
            alertify.success('Berhasil Aktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
						var kategori_informasi_publik = $('#kategori_informasi_publik').val();
						var opd_domain = $('#opd_domain').val();
						load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik').on('click', '#inaktif_informasi_setiap_saat', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data tidak di aktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik/inaktif_informasi_setiap_saat',
          success: function(html) {
            alertify.success('Berhasil Inaktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
						var kategori_informasi_publik = $('#kategori_informasi_publik').val();
						var opd_domain = $('#opd_domain').val();
						load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik').on('click', '#aktif_informasi_setiap_saat', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan diaktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik/aktif_informasi_setiap_saat',
          success: function(html) {
            alertify.success('Berhasil Aktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
						var kategori_informasi_publik = $('#kategori_informasi_publik').val();
						var opd_domain = $('#opd_domain').val();
						load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik').on('click', '#inaktif_informasi_dikecualikan', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data tidak di aktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik/inaktif_informasi_dikecualikan',
          success: function(html) {
            alertify.success('Berhasil Inaktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
						var kategori_informasi_publik = $('#kategori_informasi_publik').val();
						var opd_domain = $('#opd_domain').val();
						load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik').on('click', '#aktif_informasi_dikecualikan', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan diaktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik/aktif_informasi_dikecualikan',
          success: function(html) {
            alertify.success('Berhasil Aktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik = $('#limit_data_daftar_informasi_publik').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik = $('#urut_data_daftar_informasi_publik').val();
						var kategori_informasi_publik = $('#kategori_informasi_publik').val();
						var opd_domain = $('#opd_domain').val();
						load_data_daftar_informasi_publik(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik, kategori_informasi_publik, opd_domain);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>