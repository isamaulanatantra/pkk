<?php
  $web=$this->uut->namadomain(base_url());
  ?>
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
    <!-- Meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
    <meta name="author" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>,">
    <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>">
    <meta name="og:description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
    <meta name="og:url" content="<?php echo base_url(); ?>"/>
    <meta name="og:title" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>"/>
    <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
    <meta name="og:keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
    <!-- Favicon -->
    <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300italic,400,400italic,500,700,700italic,900" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:900" rel="stylesheet" type="text/css">
    <!-- Icon Font -->
    <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Bootstrap CSS -->
    <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles1.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <!-- Alert Confirmation -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/Typeahead-BS3-css.css" id="toggleCSS" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>boots/dist/css/bootstrap-timepicker.min.css" />
		<style>
          .well {
            min-height: 20px;
            padding: 2px;
            background-color: #292424;;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
          }
          .brandimg {
              float: left;
              padding-left: -100px;
          }
          .brandimga {
              float: left;
              padding-left: -100px;
              padding-top: 20px;
          }
          .brand {
              float: left;
              padding-left: 3px;
          }
          .brand_name {
              font-size: 20px;
              color: #000;
              font-weight: bold;
              padding-left: 5px;
              text-transform: uppercase;
          }
          .brand_namea {
              font-size: 22px;
              color: #000;
              font-weight: bold;
              padding-left: 5px;
              text-transform: uppercase;
          }
          .subjudul {
              font-size: 18px;
              color: #000;
              font-weight: bold;
              padding-left: 5px;
              text-transform: uppercase;
          }
          .featured-big a.featured-href, .featured-slider .featured-small a.featured-href {
              display: block;
              text-decoration: none;
              color: #fff;
              outline: 0;
              border: none;
          }
          .omega {
              padding-right: 0px !important;
          }
          a.featured-big a.featured-href, .featured-slider .featured-small a.featured-href {
              display: block;
              text-decoration: none;
              color: #fff;
              outline: 0;
              border: none;
          }
          .featured-big .featured-header {
              padding: 50px 50px 30px 30px;
          }
          .featured-header {
              position: absolute;
              left: 0;
              -webkit-backface-visibility: hidden;
              -webkit-perspective: 1000;
              -webkit-transform: scale(1);
              transform-style: flat;
              right: 0;
              bottom: 0;
              padding: 45px 30px 30px;
              z-index: 9;
              background: -webkit-linear-gradient(bottom, rgba(0, 0, 0, .7) 0, rgba(0, 0, 0, 0) 100%);
              background: linear-gradient(0deg, rgba(0, 0, 0, .7) 0, rgba(0, 0, 0, 0) 100%);
          }
          .btne{
          height: 30px;
          padding: 5px 10px;
          font-size: 12px;
          line-height: 1.5;
          border-radius: 3px;
          }
          .btne a, a:hover, a:active, a:focus {
              outline: 0;
              text-decoration: none;
          }
		</style>
  </head>
  <body>
    <div id="main" class="header-style1">
		
	  <header class="header-wrapper clearfix">
		<div class="header" style="background:#fff;">
		  <div class="container">
			<div class="row">
			  <!-- BEGIN TOP BAR LEFT PART -->
			  <div class="col-md-6 col-sm-6 additional-shop-info">
				<ul class="list-unstyled list-inline">
				  <li><i class="fa fa-phone"></i><span><?php if(!empty( $telpon )){ echo $telpon; } ?></span></li>
				  <li><i class="fa fa-envelope-o"></i><span><?php if(!empty( $email )){ echo $email; } ?></span></li>
				</ul>
			  </div>
			  <!-- END TOP BAR LEFT PART -->
			  <!-- BEGIN TOP BAR MENU -->
			  <div class="col-md-6 col-sm-6 additional-nav">
				<ul class="list-unstyled list-inline pull-right">
				  <li>
            <?php
              $ses=$this->session->userdata('id_users');
              if(!$ses) { echo'<a href="'.base_url('login').'">Log In</a>';  }else{
                $where = array(
                'id_users' => $this->session->userdata('id_users')
                );
                $this->db->where($where);
                $this->db->from('users');
                $jml = $this->db->count_all_results();
                if( $jml > 0 ){
                  $this->db->where($where);
                  $query = $this->db->get('users');
                  foreach ($query->result() as $row)
                    {
                    echo '<a href="'.base_url().'dashboard">Anggota</a>';
                    }
                  }
                else{
                    exit;
                  }
              }
            ?>
          </li>
				  <li><a href="https://diskominfo.wonosobokab.go.id/postings/detail/1892/FAQ.HTML">FAQ</a></li>
				</ul>
			  </div>
			  <!-- END TOP BAR MENU -->
			</div>
		  </div>
		  <!-- .container -->
		</div>
		<!-- .header -->
            <div class="" id="header">
                <div class="container no-padding">
                    <div class="row" style="margin-top: 3px;">
                        <div class="brandimg animate fadeInLeft animated"> <img src="<?php echo base_url(); ?>media/logo kabupaten wonosobo.png" height="80" width="60"></div>
                        <div class="brand animate fadeInUp animated"><span class="brand_name"><?php if(!empty( $keterangan )){ echo $keterangan; } ?></span><br /><span class="subjudul">Kabupaten Wonosobo</span></div>
                        <div class="animate fadeInLeft animated">
                        <form action="<?php echo base_url(); ?>post">
													<div class="input-group input-group-sm">
														<input name="keyword_pencarian" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword_pencarian">
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default"><i class="fa fa-search"></i> Pencarian</button>
														</div>
													</div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
		<div class="header" id="header">
		  <div class="container">
			<!-- Mobile Menu Button -->
			<a class="navbar-toggle collapsed" id="nav-button" href="#mobile-nav">
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</a><!-- .navbar-toggle -->
			<!-- Main Nav Wrapper -->
			<nav class="navbar mega-menu">
			  <!-- .logo -->
			  <!-- Navigation Menu -->
			  <!-- .navbar-collapse -->              
			  <!-- End Navigation Menu -->
			  <div class="navbar-collapse collapse" id="hornav">
				<?php echo ''.$menu_atas.''; ?>
				<!-- .nav .navbar-nav -->
			  </div>
			</nav>
			<!-- .navbar --> 
		  </div>
		  <!-- .container -->
		</div>
		<!-- .header -->
	  </header>

      <!-- Begin Main Wrapper -->
      <div class="container main-wrapper">
		<div class="main-content mag-content clearfix">
		  <div class="row main-body" data-stickyparent>
			<div class="col-md-8">

				  <?php $this -> load -> view($main_view);  ?>
				  <?php if(!empty($isi_posting)){ $this -> load -> view('modul/'.$nama_modul.''); } ?>

			</div>
            <!-- End Left big column -->
            <div class="col-md-4" data-stickycolumn>
              <aside class="sidebar clearfix">
                
              <?php
                $web=$this->uut->namadomain(base_url());
                $this->db->select("*");
                $where = array(
                  'status' => 1,
                  'domain' => $web
                  );
                $this->db->like('judul_posting', 'Covid-19');
                $this->db->where($where);
                $this->db->order_by('posting.created_time', 'desc');
                $query = $this->db->get('posting');
                $a = 0;
                foreach ($query->result() as $row)
                  {
                    $w = $this->db->query("
                    SELECT *, (select file_name from attachment where attachment.id_tabel=posting.id_posting limit 1) as file_name_attachment from posting
                    where parent='".$row->id_posting."'
                    and status != 99 
                    and domain='".$web."'
                    order by urut
                    limit 5
                    ");
                    $x = $w->num_rows();
                    $nomor = 0;
                    foreach($w->result() as $row_posting)
                    {
                      $healthy = array(" ", "#", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", ";", "[", "]", "<", ">", "+", "`", "^", "*", "'");
                      $yummy   = array("_", "", "", "", "","", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
                      $judul_posting = str_replace($healthy, $yummy, $row_posting->judul_posting);
                      if($row_posting->judul_posting == 'Informasi Umum'){
                        echo'
                          <a href="'.base_url().'post/detail_covid/'.$row_posting->id_posting.'/'.$judul_posting.'.HTML" class="featured-href">
                            <h3 class="block-title"><span>'.$row_posting->judul_posting.'</span></h3>
                          </a>
                        ';
                      }elseif($row_posting->judul_posting == 'Materi Edukasi'){
                        echo '
                        
                        <div class="widget">
                        <a href="'.base_url().'post/galeri_covid/'.$row_posting->id_posting.'/'.$judul_posting.'.HTML" class="featured-href">
                          <h3 class="block-title"><span>'.$row_posting->judul_posting.'</span></h3>
                        </a>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="flexslider">
                              <div class="featured-slider">
                        ';
                        $this->load->view('modul/custom_covid.php');
                        echo '
                              </div>
                            </div>
                          </div>
                        </div>
                        </div>
                        ';

                      }else{
                      echo '
                        <a href="'.base_url().'post/galeri_covid/'.$row_posting->id_posting.'/'.$judul_posting.'.HTML" class="featured-href">
                          <h3 class="block-title"><span>'.$row_posting->judul_posting.'</span></h3>
                        </a>
                      ';
                      }
                    }
                  }
                  
                ?>
              </aside>
            </div>
            <!-- End last column -->         
		  </div>
		</div>
      </div>
      <!-- .main-wrapper -->
		
      <!-- Footer -->
      <footer class="footer source-org vcard copyright clearfix" id="footer" role="contentinfo">
        <div class="footer-main">
          <div class="fixed-main">
            <div class="container">
              <div class="mag-content">
                <div class="row">
                  <div class="col-md-4">
                    <div class="footer-block clearfix">
                      <p class="clearfix">
                        <a class="logo" href="index.php" title="" rel="home">
                        <span><?php if(!empty( $keterangan )){ echo $keterangan; } ?></span>
                        </a><!-- .logo -->
                      </p>
                      <p class="description">
                      <address class="margin-bottom-40">
                        <?php if(!empty( $alamat )){ echo $alamat; } ?><br>
                        Phone: <?php if(!empty( $telpon )){ echo $telpon; } ?><br>
                        Email: <a href="mailto:<?php if(!empty( $email )){ echo $email; } ?>"><?php if(!empty( $email )){ echo $email; } ?></a><br>
                        Website: <a href="https://<?php echo $web; ?>">https://<?php echo $web; ?></a>
                      </address>
                      </p>
                      <ul class="social-list clearfix">
                        <li class="social-facebook">
                          <a href="<?php if(!empty( $facebook )){ echo $facebook; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Facebook">
                          <i class="fa fa-facebook"></i>
                          </a>
                        </li>
                        <li class="social-twitter" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Twitter">
                          <a href="<?php if(!empty( $twitter )){ echo $twitter; } ?>">
                          <i class="fa fa-twitter"></i>
                          </a>
                        </li>
                        <li class="social-gplus">
                          <a href="<?php if(!empty( $google )){ echo $google; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Google+">
                          <i class="fa fa-google-plus"></i>
                          </a>
                        </li>
                        <li class="social-youtube">
                          <a href="<?php if(!empty( $youtube )){ echo $youtube; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Youtube">
                          <i class="fa fa-youtube"></i>
                          </a>
                        </li>
                        <li class="social-instagram">
                          <a href="<?php if(!empty( $instagram )){ echo $instagram; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Instagram">
                          <i class="fa fa-instagram"></i>
                          </a>
                        </li>
                        <li class="social-pinterest">
                          <a href="<?php if(!empty( $pinterest )){ echo $pinterest; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Pinterest">
                          <i class="fa fa-pinterest"></i>
                          </a>
                        </li>
                        <li class="social-rss">
                          <a href="<?php if(!empty( $rss )){ echo $rss; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="RSS">
                          <i class="fa fa-rss"></i>
                          </a>
                        </li>
                      </ul>
                      <!-- BEGIN: Powered by Supercounters.com -->
                      </center>
                      <!-- END: Powered by Supercounters.com -->
                    </div>
                    <!-- Footer Block -->
                  </div>
                  <div class="col-md-5">
                    <div class="footer-block clearfix">
                      <!--<h3 class="footer-title"><a >Other Link</a></h3>
                        <ul class="tags-widget"><li><a href="#"><img src="xxi/iklan/13_Logo_Branding_Jateng_Gayeng.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/22_Logo_Visit_Jawa_Tengah_cpy.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/34_Pesona_Indonesia.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/45_Wonderful_Indonesia.png" width="55"></a></li><li><a href="1"><img src="xxi/iklan/56_BPPD_Jateng.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/67_logo_asita.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/78_PHRI.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/89_Logo_AWAI.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/910_Logo_ASPPI.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1012_HPI.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1111_IMG-20161126-WA0004.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1213_PeBeMas.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1314_FK_Deswita.jpg" width="55"></a></li><li><a href="1"><img src="xxi/iklan/" width="55"></a></li></ul>-->
                      <?php if(!empty( $KolomKiriBawah )){ echo $KolomKiriBawah; } ?>
                    </div>
                    <div class="footer-block clearfix">
                      <p class="clearfix">
                        <span class="description">
												<address class="margin-bottom-40">
													<span id="visitor"></span><br>
													<span id="hit_counter"></span>
												</address>
                        </span><!-- .logo -->
                      </p>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="footer-block clearfix">
                      <?php if(!empty( $KolomPalingBawah )){ echo $KolomPalingBawah; } ?>
                    </div>
                    <!-- Footer Block -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-bottom clearfix">
          <div class="fixed-main">
            <div class="container">
              <div class="mag-content">
                <div class="row">
                  <div class="col-md-12">
                    <p>Copyright <a href="https://<?php echo $web; ?>"> <?php if(!empty( $keterangan )){ echo $keterangan; } ?></a> © 2016. All Rights Reserved</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
      <!-- End Footer -->
    </div>
    <!-- End Main -->
    <!-- Mobile Menu -->
    <nav id="mobile-nav">
      <?php echo ''.$menu_mobile.''; ?>
    </nav>
    <!-- / Mobile Menu -->
    <div id="go-top-button" class="fa fa-angle-up" title="Scroll To Top"></div>
    <div class="mobile-overlay" id="mobile-overlay"></div>
    <script>
      function LoadVisitor() {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            table:'visitor'
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>visitor/simpan_visitor/',
          success: function(html) {
            $('#visitor').html('Total Pengunjung '+html+' ');
          }
        });
      }
    </script>
    <script>
      function LoadHitCounter() {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            current_url:'<?php echo current_url(); ?>'
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>visitor/hit_counter/',
          success: function(html) {
            $('#hit_counter').html(''+html+' Kali ');
            $('#hit_counter_posting').html(''+html+'');
          }
        });
      }
    </script>
    <script type="text/javascript">
      $(document).ready(function() {
        LoadVisitor();
        LoadHitCounter();
      });
    </script>
    <!-- Jquery js -->
    <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/jquery.min.js" type="text/javascript"></script>
    <!-- Modernizr -->
    <script src="<?php echo base_url(); ?>assets/portfolio/js/modernizr.min.js"></script>
    <!-- Bootstrap js -->
    <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- Plugins js -->
    <script src="<?php echo base_url(); ?>assets/portfolio/js/plugins.js"></script>
    <!-- Theme js -->
    <script src="<?php echo base_url(); ?>assets/portfolio/js/script.js"></script>
    <script src="<?php echo base_url(); ?>js/uut.js"></script>
    <!-- alertify -->
    <script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
  </body>
</html>