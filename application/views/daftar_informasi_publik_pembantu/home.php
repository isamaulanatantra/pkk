<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_1">
        
      </div>
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                Data Informasi
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="display:none; width: 150px;" placeholder="Search" type="text" id="kata_kunci">
                  <select name="limit_data_daftar_informasi_publik_pembantu" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_daftar_informasi_publik_pembantu">
                    <option value="10">10 Per-Halaman</option>
                    <option value="20">20 Per-Halaman</option>
                    <option value="50">50 Per-Halaman</option>
                    <option value="999999999">Semua</option>
                  </select>
                  <select name="urut_data_daftar_informasi_publik_pembantu" class="form-control input-sm pull-right" style="display:none; width: 150px;" id="urut_data_daftar_informasi_publik_pembantu">
                    <option value="urut">Nomor Urut</option>
                    <option value="judul_posting">Judul Posting</option>
                  </select>
                  <select name="klasifikasi_informasi_publik_pembantu" class="form-control input-sm pull-right" style="width: 170px;" id="klasifikasi_informasi_publik_pembantu">
                    <option value="pilih_kategori_informasi">Klasifikasi Informasi</option>
                    <option value="informasi_berkala">Informasi Berkala</option>
                    <option value="informasi_serta_merta">Informasi Serta Merta</option>
                    <option value="informasi_setiap_saat">Informasi Setiap Saat</option>
                    <option value="informasi_dikecualikan">Informasi Dikecualikan</option>
                  </select>
                  <select name="kategori_parent_posting" class="form-control input-sm pull-right" style="width: 150px;" id="kategori_parent_posting">
                  </select>
                  <select name="opd_domain" class="form-control input-sm pull-right" style="width: 150px;" id="opd_domain">
                  </select>
                  <div class="input-group-btn" style="display:none;">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="info-box">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box box-primary">
                      <div class="box-body table-responsive no-padding">
                        <div id="tblExport">
                          <table class="table table-hover">
                            <tbody>
                              <tr id="header_exel">
                                <th>NO</th>
                                <th>Judul Posting</th>
                                <th>Kategori</th>
                                <th>Informasi Berkala</th>
                                <th>Informasi Serta Merta</th>
                                <th>Informasi Setiap Saat</th>
                                <th>Informasi Dikecualikan</th>
                              </tr>
                            </tbody>
                            <tbody id="tbl_utama_daftar_informasi_publik_pembantu">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="row">
                        
                        
                        
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!----------------------->
<script>
  function load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting) {
    $('#tbl_utama_daftar_informasi_publik_pembantu').html('');
    $('#spinners_data').show();
    var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:halaman,
        limit:limit,
        kata_kunci:kata_kunci,
        urut_data_daftar_informasi_publik_pembantu:urut_data_daftar_informasi_publik_pembantu,
        klasifikasi_informasi_publik_pembantu:klasifikasi_informasi_publik_pembantu,
        opd_domain:opd_domain,
        kategori_parent_posting:kategori_parent_posting
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/json_all_data_informasi_pembantu/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr style="padding: 2px;" id_posting="' + json[i].id_posting + '" id="' + json[i].id_posting + '" >';
					tr += '<td valign="top" style="padding: 2px;">' + (start) + '</td>';
          tr += '<td valign="top" style="padding: 2px;">';
					tr += ''+json[i].judul_posting+'';
          tr += '</td>';
          tr += '<td style="padding: 2px;"><smal>';
						if( json[i].parent == 1029172 ){
							tr += 'Profil';
						}
						else if( json[i].parent == 1029179 ){
							tr += 'Layanan Informasi';
						}
						else{
							tr += '-';
						}
          tr += '</smal></td>';
          if( json[i].informasi_berkala == 1 ){
						tr += '<td style="padding: 2px;" id="td_2_'+i+'"><a href="#" id="inaktif_informasi_berkala"> <i class="fa fa-check-square-o"></a></td>';
          }
          else{
						tr += '<td style="padding: 2px;" id="td_3_'+i+'"><a href="#" id="aktif_informasi_berkala"> <i class="fa fa-minus-square-o"></a></td>';
          }
          if( json[i].informasi_serta_merta == 1 ){
						tr += '<td style="padding: 2px;" id="td_2_'+i+'"><a href="#" id="inaktif_informasi_serta_merta"> <i class="fa fa-check-square-o"></a></td>';
          }
          else{
						tr += '<td style="padding: 2px;" id="td_3_'+i+'"><a href="#" id="aktif_informasi_serta_merta"> <i class="fa fa-minus-square-o"></a></td>';
          }
          if( json[i].informasi_setiap_saat == 1 ){
						tr += '<td style="padding: 2px;" id="td_2_'+i+'"><a href="#" id="inaktif_informasi_setiap_saat"> <i class="fa fa-check-square-o"></a></td>';
          }
          else{
						tr += '<td style="padding: 2px;" id="td_3_'+i+'"><a href="#" id="aktif_informasi_setiap_saat"> <i class="fa fa-minus-square-o"></a></td>';
          }
          if( json[i].informasi_dikecualikan == 1 ){
						tr += '<td style="padding: 2px;" id="td_2_'+i+'"><a href="#" id="inaktif_informasi_dikecualikan"> <i class="fa fa-check-square-o"></a></td>';
          }
          else{
						tr += '<td style="padding: 2px;" id="td_3_'+i+'"><a href="#" id="aktif_informasi_dikecualikan"> <i class="fa fa-minus-square-o"></a></td>';
          }
          // tr += '<td style="padding: 2px;" valign="top"><a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i> </a> &nbsp;&nbsp;<a href="#" id="del_ajax"> <i class="fa fa-trash-o"></i></a></td>';
          tr += '</tr>';
        }
        $('#tbl_utama_daftar_informasi_publik_pembantu').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
  }
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#limit_data_daftar_informasi_publik_pembantu').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      var kategori_parent_posting = $('#kategori_parent_posting').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#urut_data_daftar_informasi_publik_pembantu').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      var kategori_parent_posting = $('#kategori_parent_posting').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#kata_kunci').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      var kategori_parent_posting = $('#kategori_parent_posting').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#opd_domain').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      var kategori_parent_posting = $('#kategori_parent_posting').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
      load_kategori_parent_posting();
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#kategori_parent_posting').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      var kategori_parent_posting = $('#kategori_parent_posting').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#klasifikasi_informasi_publik_pembantu').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      var kategori_parent_posting = $('#kategori_parent_posting').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var opd_domain = $('#opd_domain').val();
      var kategori_parent_posting = $('#kategori_parent_posting').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
  });
</script>
<!----------------------->
<script>
  function load_opd_domain() {
    $('#opd_domain').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/opd_domain/',
      success: function(html) {
        $('#opd_domain').html('<option value="">Semua OPD</option>  '+html+'');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  load_opd_domain();
});
</script>
<!----------------------->
<script>
  function load_kategori_parent_posting() {
    $('#kategori_parent_posting').html('');
    $('#spinners_data').show();
    var opd_domain = $('#opd_domain').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        opd_domain: opd_domain
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/kategori_parent_posting/',
      success: function(html) {
        $('#kategori_parent_posting').html('<option value="">Semua Kategori</option>  '+html+'');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  load_kategori_parent_posting();
});
</script>

<!-----------DIP------------>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik_pembantu').on('click', '#inaktif_informasi_berkala', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data tidak di aktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/inaktif_informasi_berkala',
          success: function(html) {
            alertify.success('Berhasil Inaktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
						var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
						var opd_domain = $('#opd_domain').val();
						var kategori_parent_posting = $('#kategori_parent_posting').val();
						load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik_pembantu').on('click', '#aktif_informasi_berkala', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan diaktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/aktif_informasi_berkala',
          success: function(html) {
            alertify.success('Berhasil Aktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
						var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
						var opd_domain = $('#opd_domain').val();
						var kategori_parent_posting = $('#kategori_parent_posting').val();
						load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik_pembantu').on('click', '#inaktif_informasi_serta_merta', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data tidak di aktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/inaktif_informasi_serta_merta',
          success: function(html) {
            alertify.success('Berhasil Inaktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
						var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
						var opd_domain = $('#opd_domain').val();
						var kategori_parent_posting = $('#kategori_parent_posting').val();
						load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik_pembantu').on('click', '#aktif_informasi_serta_merta', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan diaktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/aktif_informasi_serta_merta',
          success: function(html) {
            alertify.success('Berhasil Aktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
						var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
						var opd_domain = $('#opd_domain').val();
						var kategori_parent_posting = $('#kategori_parent_posting').val();
						load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik_pembantu').on('click', '#inaktif_informasi_setiap_saat', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data tidak di aktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/inaktif_informasi_setiap_saat',
          success: function(html) {
            alertify.success('Berhasil Inaktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
						var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
						var opd_domain = $('#opd_domain').val();
						var kategori_parent_posting = $('#kategori_parent_posting').val();
						load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik_pembantu').on('click', '#aktif_informasi_setiap_saat', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan diaktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/aktif_informasi_setiap_saat',
          success: function(html) {
            alertify.success('Berhasil Aktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
						var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
						var opd_domain = $('#opd_domain').val();
						var kategori_parent_posting = $('#kategori_parent_posting').val();
						load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik_pembantu').on('click', '#inaktif_informasi_dikecualikan', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data tidak di aktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/inaktif_informasi_dikecualikan',
          success: function(html) {
            alertify.success('Berhasil Inaktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
						var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
						var opd_domain = $('#opd_domain').val();
						var kategori_parent_posting = $('#kategori_parent_posting').val();
						load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_daftar_informasi_publik_pembantu').on('click', '#aktif_informasi_dikecualikan', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan diaktifkan?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_posting:id_posting
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/aktif_informasi_dikecualikan',
          success: function(html) {
            alertify.success('Berhasil Aktif');
						var halaman = 1;
						var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
						var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
						var kata_kunci = $('#kata_kunci').val();
						var urut_data_daftar_informasi_publik_pembantu = $('#urut_data_daftar_informasi_publik_pembantu').val();
						var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
						var opd_domain = $('#opd_domain').val();
						var kategori_parent_posting = $('#kategori_parent_posting').val();
						load_data_daftar_informasi_publik_pembantu(halaman, limit, kata_kunci, urut_data_daftar_informasi_publik_pembantu, klasifikasi_informasi_publik_pembantu, opd_domain, kategori_parent_posting);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>