
          <section class="content">

						<?php
						if (empty($_GET['thn_pelajaran'])) {
							$thn_pelajaran = "2017/2018";
						}else{
							$thn_pelajaran = $_GET['thn_pelajaran'];
						}
						?>
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Sekolah TP <?php echo $thn_pelajaran; ?> di Dapodik</h3>
              <div class="input-group pull-right">            
	              <select name="thn_pelajaran" class="form-control input-sm pull-right" style="width: 180px;" id="thn_pelajaran">
	                <option value="">Pilih Tahun Pelajaran</option>
	                <option value="2017/2018">2017/2018</option>
	                <option value="2018/2019">2018/2019</option>
	              </select>
	              <div class="input-group-btn">
	                <button class="btn btn-xs btn-primary" id="filter"><i class="fa fa-search"></i></button>
	              </div>
              </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body" width="200px">
			<table id="konten_posting" class="table table-bordered table-striped table-hover">
			  <tr>
				<th width="30px">No</th>
				<th>Jenjang</th>
				<th>Jumlah</th>
			  </tr>
			  <tr>
				<td>1</td>
				<td>SD Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  and status_sekolah='Negeri'
						  and thn_pelajaran = '$thn_pelajaran'
						  ");
						  $jml_SD_Negeri=$w2->num_rows();
							  echo''.$jml_SD_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>2</td>
				<td>SD Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  and status_sekolah='Swasta'
						  and thn_pelajaran = '$thn_pelajaran'
						  ");
						  $jml_SD_Swasta=$w2->num_rows();
							  echo''.$jml_SD_Swasta.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>3</td>
				<td>SMP Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  and status_sekolah='Negeri'
						  and thn_pelajaran = '$thn_pelajaran'
						  ");
						  $jml_SMP_Negeri=$w2->num_rows();
							  echo''.$jml_SMP_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>4</td>
				<td>SMP Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  and status_sekolah='Swasta'
						  and thn_pelajaran = '$thn_pelajaran'
						  ");
						  $jml_SMP_Swasta=$w2->num_rows();
							  echo''.$jml_SMP_Swasta.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>5</td>
				<td>SMA Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  and status_sekolah='Negeri'
						  and thn_pelajaran = '$thn_pelajaran'
						  ");
						  $jml_SMA_Negeri=$w2->num_rows();
							  echo''.$jml_SMA_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>6</td>
				<td>SMA Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  and status_sekolah='Swasta'
						  and thn_pelajaran = '$thn_pelajaran'
						  ");
						  $jml_SMA_Swasta=$w2->num_rows();
							  echo''.$jml_SMA_Swasta.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>7</td>
				<td>SMK Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  and status_sekolah='Negeri'
						  and thn_pelajaran = '$thn_pelajaran'
						  ");
						  $jml_SMA_Negeri=$w2->num_rows();
							  echo''.$jml_SMA_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>8</td>
				<td>SMK Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  and status_sekolah='Swasta'
						  and thn_pelajaran = '$thn_pelajaran'
						  ");
						  $jml_SMK_Swasta=$w2->num_rows();
							  echo''.$jml_SMK_Swasta.'';
						?>
				</td>
			  </tr>
			</table>
		  </div>
	  </div>
	  </section>
<!-- page script -->
<script type="text/javascript">
  $(function () {
    $('#konten_posting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

	$('#filter').on('click', function() {
	    var thn_pelajaran = $('#thn_pelajaran').val();
	    window.location.replace("https://dikpora.wonosobokab.go.id/postings/detail/1031232/Jumlah_Sekolah.HTML?thn_pelajaran="+thn_pelajaran+"");
	});

</script>
