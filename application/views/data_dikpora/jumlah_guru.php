
          <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Guru TP 2017/2018 di Dapodik</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body" width="200px">
			<table id="konten_posting" class="table table-bordered table-striped table-hover">
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">Jenjang</th>
				<th colspan="2">Jumlah Guru</th>
				<th rowspan="2">Total</th>
			  </tr>
			  <tr>
				<td>L</td>
				<td>P</td>
			  </tr>
			  <tr>
				<td>1</td>
				<td>SD Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  and status_sekolah='Negeri'
						  ");
						  $jml_guru_SD_Negeri_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SD_Negeri_l=($jml_guru_SD_Negeri_l*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SD_Negeri_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  and status_sekolah='Negeri'
						  ");
						  $jml_guru_SD_Negeri_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SD_Negeri_p=($jml_guru_SD_Negeri_p*1)+($h2->ptk_g_pns_p*1)+($h2->ptk_g_gty_p*1)+($h2->ptk_g_gtt_p*1)+($h2->ptk_g_gb_p*1)+($h2->ptk_g_gh_p*1)+($h2->ptk_adm_pns_p*1)+($h2->ptk_adm_gty_p*1)+($h2->ptk_adm_gtt_p*1)+($h2->ptk_adm_gb_p*1)+($h2->ptk_adm_gh_p*1);
						  }
							  echo''.$jml_guru_SD_Negeri_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SD_Negeri=$jml_guru_SD_Negeri_p+$jml_guru_SD_Negeri_l;
						  }
							  echo''.$jml_guru_SD_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>2</td>
				<td>SD Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  and status_sekolah='Swasta'
						  ");
						  $jml_guru_SD_Swasta_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SD_Swasta_l=($jml_guru_SD_Swasta_l*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SD_Swasta_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  and status_sekolah='Swasta'
						  ");
						  $jml_guru_SD_Swasta_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SD_Swasta_p=($jml_guru_SD_Swasta_p*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SD_Swasta_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SD_Swasta=$jml_guru_SD_Swasta_p+$jml_guru_SD_Swasta_l;
						  }
							  echo''.$jml_guru_SD_Swasta.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>3</td>
				<td>SMP Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  and status_sekolah='Negeri'
						  ");
						  $jml_guru_SMP_Negeri_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMP_Negeri_l=($jml_guru_SMP_Negeri_l*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SMP_Negeri_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  and status_sekolah='Negeri'
						  ");
						  $jml_guru_SMP_Negeri_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMP_Negeri_p=($jml_guru_SMP_Negeri_p*1)+($h2->ptk_g_pns_p*1)+($h2->ptk_g_gty_p*1)+($h2->ptk_g_gtt_p*1)+($h2->ptk_g_gb_p*1)+($h2->ptk_g_gh_p*1)+($h2->ptk_adm_pns_p*1)+($h2->ptk_adm_gty_p*1)+($h2->ptk_adm_gtt_p*1)+($h2->ptk_adm_gb_p*1)+($h2->ptk_adm_gh_p*1);
						  }
							  echo''.$jml_guru_SMP_Negeri_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMP_Negeri=$jml_guru_SMP_Negeri_p+$jml_guru_SMP_Negeri_l;
						  }
							  echo''.$jml_guru_SMP_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>4</td>
				<td>SMP Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  and status_sekolah='Swasta'
						  ");
						  $jml_guru_SMP_Swasta_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMP_Swasta_l=($jml_guru_SMP_Swasta_l*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SMP_Swasta_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  and status_sekolah='Swasta'
						  ");
						  $jml_guru_SMP_Swasta_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMP_Swasta_p=($jml_guru_SMP_Swasta_p*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SMP_Swasta_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMP_Swasta=$jml_guru_SMP_Swasta_p+$jml_guru_SMP_Swasta_l;
						  }
							  echo''.$jml_guru_SMP_Swasta.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>5</td>
				<td>SMA Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  and status_sekolah='Negeri'
						  ");
						  $jml_guru_SMA_Negeri_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMA_Negeri_l=($jml_guru_SMA_Negeri_l*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SMA_Negeri_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  and status_sekolah='Negeri'
						  ");
						  $jml_guru_SMA_Negeri_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMA_Negeri_p=($jml_guru_SMA_Negeri_p*1)+($h2->ptk_g_pns_p*1)+($h2->ptk_g_gty_p*1)+($h2->ptk_g_gtt_p*1)+($h2->ptk_g_gb_p*1)+($h2->ptk_g_gh_p*1)+($h2->ptk_adm_pns_p*1)+($h2->ptk_adm_gty_p*1)+($h2->ptk_adm_gtt_p*1)+($h2->ptk_adm_gb_p*1)+($h2->ptk_adm_gh_p*1);
						  }
							  echo''.$jml_guru_SMA_Negeri_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMA_Negeri=$jml_guru_SMA_Negeri_p+$jml_guru_SMA_Negeri_l;
						  }
							  echo''.$jml_guru_SMA_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>6</td>
				<td>SMA Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  and status_sekolah='Swasta'
						  ");
						  $jml_guru_SMA_Swasta_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMA_Swasta_l=($jml_guru_SMA_Swasta_l*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SMA_Swasta_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  and status_sekolah='Swasta'
						  ");
						  $jml_guru_SMA_Swasta_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMA_Swasta_p=($jml_guru_SMA_Swasta_p*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SMA_Swasta_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMA_Swasta=$jml_guru_SMA_Swasta_p+$jml_guru_SMA_Swasta_l;
						  }
							  echo''.$jml_guru_SMA_Swasta.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>7</td>
				<td>SMK Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  and status_sekolah='Negeri'
						  ");
						  $jml_guru_SMK_Negeri_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMK_Negeri_l=($jml_guru_SMK_Negeri_l*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SMK_Negeri_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  and status_sekolah='Negeri'
						  ");
						  $jml_guru_SMK_Negeri_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMK_Negeri_p=($jml_guru_SMK_Negeri_p*1)+($h2->ptk_g_pns_p*1)+($h2->ptk_g_gty_p*1)+($h2->ptk_g_gtt_p*1)+($h2->ptk_g_gb_p*1)+($h2->ptk_g_gh_p*1)+($h2->ptk_adm_pns_p*1)+($h2->ptk_adm_gty_p*1)+($h2->ptk_adm_gtt_p*1)+($h2->ptk_adm_gb_p*1)+($h2->ptk_adm_gh_p*1);
						  }
							  echo''.$jml_guru_SMK_Negeri_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMK_Negeri=$jml_guru_SMK_Negeri_p+$jml_guru_SMK_Negeri_l;
						  }
							  echo''.$jml_guru_SMK_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>8</td>
				<td>SMK Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  and status_sekolah='Swasta'
						  ");
						  $jml_guru_SMK_Swasta_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMK_Swasta_l=($jml_guru_SMK_Swasta_l*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SMK_Swasta_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  and status_sekolah='Swasta'
						  ");
						  $jml_guru_SMK_Swasta_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMK_Swasta_p=($jml_guru_SMK_Swasta_p*1)+($h2->ptk_g_pns_l*1)+($h2->ptk_g_gty_l*1)+($h2->ptk_g_gtt_l*1)+($h2->ptk_g_gb_l*1)+($h2->ptk_g_gh_l*1)+($h2->ptk_adm_pns_l*1)+($h2->ptk_adm_gty_l*1)+($h2->ptk_adm_gtt_l*1)+($h2->ptk_adm_gb_l*1)+($h2->ptk_adm_gh_l*1);
						  }
							  echo''.$jml_guru_SMK_Swasta_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_guru_SMK_Swasta=$jml_guru_SMK_Swasta_p+$jml_guru_SMK_Swasta_l;
						  }
							  echo''.$jml_guru_SMK_Swasta.'';
						?>
				</td>
			  </tr>
			</table>
		  </div>
	  </div>
	  </section>
<!-- page script -->
<script>
  $(function () {
    $('#konten_posting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
