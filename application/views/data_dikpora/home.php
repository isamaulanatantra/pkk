
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Posting Website OPD</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="konten_posting" class="table table-bordered table-striped table-hover">
                	<thead>
					  <tr>
						<th rowspan="3">No</th>
						<th rowspan="3">NPSN<br></th>
						<th rowspan="3">Nama Sekolah</th>
						<th rowspan="3">Alamat</th>
						<th rowspan="3">Desa/Kelurahan</th>
						<th rowspan="3">Kecamatan</th>
						<th rowspan="3">Jenjang</th>
						<th rowspan="3">Status</th>
						<th rowspan="3">Email Sekolah</th>
						<th rowspan="3">Website</th>
						<th rowspan="3">Nama Kepala Sekolah</th>
						<th colspan="12">Peserta Didik</th>
					  </tr>
					  <tr>
						<td colspan="2">Kelas 1</td>
						<td colspan="2">Kelas 2</td>
						<td colspan="2">Kelas 3</td>
						<td colspan="2">Kelas 4</td>
						<td colspan="2">Kelas 5</td>
						<td colspan="2">Kelas 6</td>
						<td colspan="2">Jumlah</td>
					  </tr>
					  <tr>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
					  </tr>
                	</thead>
                <tbody>
				<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  ");
						  $no=0;
						  foreach($w2->result() as $h2)
						  {
							  $no=$no+1;
							  echo'
								<tr>
									<td>'.$no.'</td>
									<td>'.$h2->npsn.'</td>
									<td>'.$h2->nama_sp.'</td>
									<td>'.$h2->alamat_jalan.'</td>
									<td>'.$h2->desa_kelurahan.'</td>
									<td>'.$h2->kec_.'</td>
									<td>'.$h2->jenjang.'</td>
									<td>'.$h2->status_sekolah.'</td>
									<td>'.$h2->email.'</td>
									<td>'.$h2->website.'</td>
									<td>'.$h2->nama_kepsek.'</td>
									<td>'.$h2->pd_tkt_1_l.'</td>
									<td>'.$h2->pd_tkt_1_p.'</td>
									<td>'.$h2->pd_tkt_2_l.'</td>
									<td>'.$h2->pd_tkt_2_p.'</td>
									<td>'.$h2->pd_tkt_3_l.'</td>
									<td>'.$h2->pd_tkt_3_p.'</td>
									<td>'.$h2->pd_tkt_4_l.'</td>
									<td>'.$h2->pd_tkt_4_p.'</td>
									<td>'.$h2->pd_tkt_5_l.'</td>
									<td>'.$h2->pd_tkt_5_p.'</td>
									<td>'.$h2->pd_tkt_6_l.'</td>
									<td>'.$h2->pd_tkt_6_p.'</td>
									<td>';echo ''.$jml_l=$h2->pd_tkt_1_l+$h2->pd_tkt_2_l+$h2->pd_tkt_3_l+$h2->pd_tkt_4_l+$h2->pd_tkt_5_l+$h2->pd_tkt_6_l.''; echo '</td>
									<td>';echo ''.$jml_p=$h2->pd_tkt_1_p+$h2->pd_tkt_2_p+$h2->pd_tkt_3_p+$h2->pd_tkt_4_p+$h2->pd_tkt_5_p+$h2->pd_tkt_6_p.''; echo '</td>
								  </tr>
							  ';
						  }
				?>
				
                </tbody>
			  </table>
		  </div>
	  </div>
<!-- page script -->
<script>
  $(function () {
    $('#konten_posting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
