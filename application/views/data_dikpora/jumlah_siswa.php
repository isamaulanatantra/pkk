
          <section class="content">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Siswa TP 2017/2018 di Dapodik</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body" width="200px">
			<table id="konten_posting" class="table table-bordered table-striped table-hover">
			<tr>
				<th rowspan="2">No</th>
				<th rowspan="2">Jenjang</th>
				<th colspan="2">Jumlah Siswa</th>
				<th rowspan="2">Total</th>
			  </tr>
			  <tr>
				<td>L</td>
				<td>P</td>
			  </tr>
			  <tr>
				<td>1</td>
				<td>SD Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  and status_sekolah='Negeri'
						  ");
						  $jml_siswa_SD_Negeri_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SD_Negeri_l=($jml_siswa_SD_Negeri_l*1)+($h2->pd_tkt_1_l*1)+($h2->pd_tkt_2_l*1)+($h2->pd_tkt_3_l*1)+($h2->pd_tkt_4_l*1)+($h2->pd_tkt_5_l*1)+($h2->pd_tkt_6_l*1);
						  }
							  echo''.$jml_siswa_SD_Negeri_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  and status_sekolah='Negeri'
						  ");
						  $jml_siswa_SD_Negeri_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SD_Negeri_p=($jml_siswa_SD_Negeri_p*1)+($h2->pd_tkt_1_p*1)+($h2->pd_tkt_2_p*1)+($h2->pd_tkt_3_p*1)+($h2->pd_tkt_4_p*1)+($h2->pd_tkt_5_p*1)+($h2->pd_tkt_6_p*1);
						  }
							  echo''.$jml_siswa_SD_Negeri_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SD_Negeri=$jml_siswa_SD_Negeri_p+$jml_siswa_SD_Negeri_l;
						  }
							  echo''.$jml_siswa_SD_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>2</td>
				<td>SD Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  and status_sekolah='Swasta'
						  ");
						  $jml_siswa_SD_Swasta_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SD_Swasta_l=($jml_siswa_SD_Swasta_l*1)+($h2->pd_tkt_1_l*1)+($h2->pd_tkt_2_l*1)+($h2->pd_tkt_3_l*1)+($h2->pd_tkt_4_l*1)+($h2->pd_tkt_5_l*1)+($h2->pd_tkt_6_l*1);
						  }
							  echo''.$jml_siswa_SD_Swasta_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  and status_sekolah='Swasta'
						  ");
						  $jml_siswa_SD_Swasta_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SD_Swasta_p=($jml_siswa_SD_Swasta_p*1)+($h2->pd_tkt_1_p*1)+($h2->pd_tkt_2_p*1)+($h2->pd_tkt_3_p*1)+($h2->pd_tkt_4_p*1)+($h2->pd_tkt_5_p*1)+($h2->pd_tkt_6_p*1);
						  }
							  echo''.$jml_siswa_SD_Swasta_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SD'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SD_Swasta=$jml_siswa_SD_Swasta_p+$jml_siswa_SD_Swasta_l;
						  }
							  echo''.$jml_siswa_SD_Swasta.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>3</td>
				<td>SMP Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  and status_sekolah='Negeri'
						  ");
						  $jml_siswa_SMP_Negeri_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMP_Negeri_l=($jml_siswa_SMP_Negeri_l*1)+($h2->pd_tkt_7_l*1)+($h2->pd_tkt_8_l*1)+($h2->pd_tkt_9_l*1);
						  }
							  echo''.$jml_siswa_SMP_Negeri_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  and status_sekolah='Negeri'
						  ");
						  $jml_siswa_SMP_Negeri_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMP_Negeri_p=($jml_siswa_SMP_Negeri_p*1)+($h2->pd_tkt_7_p*1)+($h2->pd_tkt_8_p*1)+($h2->pd_tkt_9_p*1);
						  }
							  echo''.$jml_siswa_SMP_Negeri_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMP_Negeri=$jml_siswa_SMP_Negeri_p+$jml_siswa_SMP_Negeri_l;
						  }
							  echo''.$jml_siswa_SMP_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>4</td>
				<td>SMP Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  and status_sekolah='Swasta'
						  ");
						  $jml_siswa_SMP_Swasta_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMP_Swasta_l=($jml_siswa_SMP_Swasta_l*1)+($h2->pd_tkt_7_l*1)+($h2->pd_tkt_8_l*1)+($h2->pd_tkt_9_l*1);
						  }
							  echo''.$jml_siswa_SMP_Swasta_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  and status_sekolah='Swasta'
						  ");
						  $jml_siswa_SMP_Swasta_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMP_Swasta_p=($jml_siswa_SMP_Swasta_p*1)+($h2->pd_tkt_7_p*1)+($h2->pd_tkt_8_p*1)+($h2->pd_tkt_9_p*1);
						  }
							  echo''.$jml_siswa_SMP_Swasta_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMP'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMP_Swasta=$jml_siswa_SMP_Swasta_p+$jml_siswa_SMP_Swasta_l;
						  }
							  echo''.$jml_siswa_SMP_Swasta.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>5</td>
				<td>SMA Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  and status_sekolah='Negeri'
						  ");
						  $jml_siswa_SMA_Negeri_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMA_Negeri_l=($jml_siswa_SMA_Negeri_l*1)+($h2->pd_tkt_10_l*1)+($h2->pd_tkt_11_l*1)+($h2->pd_tkt_12_l*1);
						  }
							  echo''.$jml_siswa_SMA_Negeri_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  and status_sekolah='Negeri'
						  ");
						  $jml_siswa_SMA_Negeri_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMA_Negeri_p=($jml_siswa_SMA_Negeri_p*1)+($h2->pd_tkt_10_p*1)+($h2->pd_tkt_11_p*1)+($h2->pd_tkt_12_p*1);
						  }
							  echo''.$jml_siswa_SMA_Negeri_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMA_Negeri=$jml_siswa_SMA_Negeri_p+$jml_siswa_SMA_Negeri_l;
						  }
							  echo''.$jml_siswa_SMA_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>6</td>
				<td>SMA Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  and status_sekolah='Swasta'
						  ");
						  $jml_siswa_SMA_Swasta_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMA_Swasta_l=($jml_siswa_SMA_Swasta_l*1)+($h2->pd_tkt_10_l*1)+($h2->pd_tkt_11_l*1)+($h2->pd_tkt_12_l*1);
						  }
							  echo''.$jml_siswa_SMA_Swasta_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  and status_sekolah='Swasta'
						  ");
						  $jml_siswa_SMA_Swasta_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMA_Swasta_p=($jml_siswa_SMA_Swasta_p*1)+($h2->pd_tkt_10_p*1)+($h2->pd_tkt_11_p*1)+($h2->pd_tkt_12_p*1);
						  }
							  echo''.$jml_siswa_SMA_Swasta_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMA'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMA_Swasta=$jml_siswa_SMA_Swasta_p+$jml_siswa_SMA_Swasta_l;
						  }
							  echo''.$jml_siswa_SMA_Swasta.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>7</td>
				<td>SMK Negeri</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  and status_sekolah='Negeri'
						  ");
						  $jml_siswa_SMK_Negeri_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMK_Negeri_l=($jml_siswa_SMK_Negeri_l*1)+($h2->pd_tkt_10_l*1)+($h2->pd_tkt_11_l*1)+($h2->pd_tkt_12_l*1);
						  }
							  echo''.$jml_siswa_SMK_Negeri_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  and status_sekolah='Negeri'
						  ");
						  $jml_siswa_SMK_Negeri_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMK_Negeri_p=($jml_siswa_SMK_Negeri_p*1)+($h2->pd_tkt_10_p*1)+($h2->pd_tkt_11_p*1)+($h2->pd_tkt_12_p*1);
						  }
							  echo''.$jml_siswa_SMK_Negeri_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMK_Negeri=$jml_siswa_SMK_Negeri_p+$jml_siswa_SMK_Negeri_l;
						  }
							  echo''.$jml_siswa_SMK_Negeri.'';
						?>
				</td>
			  </tr>
			  <tr>
				<td>8</td>
				<td>SMK Swasta</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  and status_sekolah='Swasta'
						  ");
						  $jml_siswa_SMK_Swasta_l='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMK_Swasta_l=($jml_siswa_SMK_Swasta_l*1)+($h2->pd_tkt_10_l*1)+($h2->pd_tkt_11_l*1)+($h2->pd_tkt_12_l*1);
						  }
							  echo''.$jml_siswa_SMK_Swasta_l.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  and status_sekolah='Swasta'
						  ");
						  $jml_siswa_SMK_Swasta_p='0';
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMK_Swasta_p=($jml_siswa_SMK_Swasta_p*1)+($h2->pd_tkt_10_p*1)+($h2->pd_tkt_11_p*1)+($h2->pd_tkt_12_p*1);
						  }
							  echo''.$jml_siswa_SMK_Swasta_p.'';
						?>
				</td>
				<td>
						<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from data_dikpora
						  where jenjang='SMK'
						  ");
						  foreach($w2->result() as $h2)
						  {
						  $jml_siswa_SMK_Swasta=$jml_siswa_SMK_Swasta_p+$jml_siswa_SMK_Swasta_l;
						  }
							  echo''.$jml_siswa_SMK_Swasta.'';
						?>
				</td>
			  </tr>
			</table>
		  </div>
	  </div>
	  </section>
<!-- page script -->
<script>
  $(function () {
    $('#konten_posting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
