
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Laporan Komdat</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Laporan Komdat</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                	<li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab" id="klik_tab_tampil">Data</a></li>

                	<li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab" id="klik_tab_form">Form Bulanan</a></li>
                  	

                  	<li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab" id="klik_tab_form_tahunan">Form Tahunan</a></li>

                  	<li class="nav-item"><a class="nav-link" href="#tab_4" data-toggle="tab" id="klik_tab_form_edit">Edit Data</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane" id="tab_2">
													
					<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>laporan_komdat/save">
						<h3 id="judul_formulir">FORMULIR INPUT</h3>
						<div class="row">
							<div class="col-sm-8 col-md-8">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
										  <label for="jenis_laporan">Jenis Laporan</label>						
											<select class="form-control" id="jenis_laporan" name="jenis_laporan" required >
												<option value="1">KESEHATAN IBU DAN ANAK</option>
												<option value="2">GIZI</option>
												<option value="3">IMUNISASI</option>
												<option value="4">PENYAKIT</option>
												<option value="5">DATA TRIWULANAN</option>
												<!-- <option value="6">DATA TAHUNAN</option> -->
											</select>
										</div>
									</div>
						            <div class="col-md-2">
						                <div class="form-group">
						                  <label for="tampilForm">&nbsp;</label>
						                  <button id="tampilForm" class="btn btn-primary" >Tampilkan</button>
						                </div>
						            </div>	



									<div class="col-md-2" id='thn' style="display:none;">
						                <div class="form-group">
											<label for="thn_komdat_form">Pilih Tahun</label>						
											<select class="form-control" id="thn_komdat_form" name="thn_komdat_form" required >
												<option value="2017">2017</option>
												<option value="2018">2018</option>
												<option value="2019">2019</option>
												<option value="2020">2020</option>
												<option value="2021">2021</option>
												<option value="2022">2022</option>
											</select>
						                </div>
						            </div>
						            <div class="col-md-2" id='bln' style="display:none;"">
						                <div class="form-group">
						                  <label for="bln_komdat_form">Pilih Bulan</label>						
											<select class="form-control" id="bln_komdat_form" name="bln_komdat_form" required >
												<option value="1">Januari</option>
												<option value="2">Februari</option>
												<option value="3">Maret</option>
												<option value="4">April</option>
												<option value="5">Mei</option>
												<option value="6">Juni</option>
												<option value="7">Juli</option>
												<option value="8">Agustus</option>
												<option value="9">September</option>
												<option value="10">Oktober</option>
												<option value="11">November</option>
												<option value="12">Desember</option>
											</select>
						                </div>
					             	</div>
					             </div>

					             <div class="row">
					             	<div class="col-md-12">
						             	<div id="tbl" style="display: none;">
						             		<table class="table table-bordered table-striped">
												<thead>
													<th>No</th>
													<th>Indikator Komdat</th>
													<th>Jumlah</th>
												</thead>
												<tbody id="tbl_form_komdat">
												</tbody>
											</table>
											<div class="row">
												<div class="col-md-4">
													<button type="submit" class="btn btn-primary" id="simpan_komdat">SIMPAN DATA</button>
													<button type="submit" class="btn btn-primary" id="update_komdat" style="display:none;">UPDATE DATA</button>
												</div>
											</div>	
						             	</div>					             	
						            </div>
					         	</div>
					         	 <div id="pesan"></div>
					             
							</div>
						</div>											
					</form>					
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane active table-responsive" id="tab_1">                 	

					<div class="row">
		              <div class="col-md-2">
		                <div class="form-group">
							<label for="thn_komdat">Pilih Tahun</label>						
							<select class="form-control" id="thn_komdat" name="thn_komdat" required >
								<option value="2017">2017</option>
								<option value="2018">2018</option>
								<option value="2019">2019</option>
								<option value="2020">2020</option>
								<option value="2021">2021</option>
								<option value="2022">2022</option>
							</select>
		                </div>
		              </div>
		              <div class="col-md-2" style="display: none;">
		                <div class="form-group">
		                  <label for="bln_komdat">Pilih Bulan</label>						
							<select class="form-control" id="bln_komdat" name="bln_komdat" required >
								<option value="1">Januari</option>
								<option value="2">Februari</option>
								<option value="3">Maret</option>
								<option value="4">April</option>
								<option value="5">Mei</option>
								<option value="6">Juni</option>
								<option value="7">Juli</option>
								<option value="8">Agustus</option>
								<option value="9">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
							</select>
		                </div>
		              </div>
		              <div class="col-md-1">
		                <div class="form-group">
		                  <label for="tampilkan">&nbsp;</label>
		                  <button id="tampilkan" class="btn btn-primary" >Tampilkan</button>
		                </div>
		              </div>		              		              
		            </div>
		            	<div class="row">
		            		<div class="col-md-6 col-md-offset-3">
								<div class="form-group">
								  <label for="btnExport">&nbsp;</label>
								  <button id="btnExport" class="btn btn-warning" >Export Excel</button>
								</div>
							</div>
		            	</div>
		            	<div class="row">
                     		<div id="tblExport">
								<table class="table table-bordered table-hover">
									<thead class="bg-success">
										<th>No</th>
										<th>Indikator Komdat</th>							
										<th>Jan</th>
										<th>Feb</th>
										<th>Mar</th>
										<th>Apr</th>
										<th>Mei</th>
										<th>Jun</th>
										<th>Jul</th>
										<th>Agst</th>
										<th>Sept</th>
										<th>Okt</th>
										<th>Nov</th>
										<th>Des</th>
									</thead>

									<tbody id="tbl_komdat_kia">
									</tbody>
									<tbody id="tbl_komdat_gizi">
									</tbody>
									<tbody id="tbl_komdat_imunisasi">
									</tbody>
									<tbody id="tbl_komdat_penyakit">
									</tbody>
									<tbody id="tbl_komdat_triwulan">
									</tbody>
								</table>
							</div>
						</div>
					<ul class="pagination pagination-sm no-margin pull-right" id="pagination">
					</ul>
					
					<div class="overlay" id="spinners_data" style="display:none;">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
											
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
													
					<form role="form" id="form_tahunan" method="post" action="<?php echo base_url(); ?>laporan_komdat/save_tahunan">
						<h3 id="judul_formulir">FORMULIR INPUT TAHUNAN</h3>
						<div class="row">
							<div class="col-sm-8 col-md-8">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
										  <label for="jenis_laporan_tahunan">Jenis Laporan Tahunan</label>						
											<select class="form-control" id="jenis_laporan_tahunan" name="jenis_laporan_tahunan" required >
												<option value="1">DATA UMUM</option>
												<option value="2">FARMASI</option>
												<option value="3">IMUNISASI</option>
												<option value="4">PENYAKIT</option>
												<option value="5">KESEHATAN LINGKUNGAN</option>
												<option value="6">SDM KESEHATAN</option>
												<option value="7">PROMOSI KESEHATAN</option>
												<option value="8">PELAYANAN RUMAH SAKIT</option>
												<option value="9">SARANA PRASARANA</option>
											</select>
										</div>
									</div>
						            <div class="col-md-2">
						                <div class="form-group">
						                  <label for="tampilFormTahunan">&nbsp;</label>
						                  <button id="tampilFormTahunan" class="btn btn-primary" >Tampilkan</button>
						                </div>
						            </div>	



									<div class="col-md-2" id='thn_tahunan' style="display:none;">
						                <div class="form-group">
											<label for="thn_komdat_form_tahunan">Pilih Tahun</label>						
											<select class="form-control" id="thn_komdat_form_tahunan" name="thn_komdat_form_tahunan" required >
												<option value="2017">2017</option>
												<option value="2018">2018</option>
												<option value="2019">2019</option>
												<option value="2020">2020</option>
												<option value="2021">2021</option>
												<option value="2022">2022</option>
											</select>
						                </div>
						            </div>						            
					             </div>

					             <div class="row">
					             	<div class="col-md-12">
						             	<div id="tbl_tahunan" style="display: none;">
						             		<table class="table table-bordered table-striped">
												<thead>
													<th>No</th>
													<th>Indikator Komdat</th>
													<th>Jumlah</th>
												</thead>
												<tbody id="tbl_form_komdat_tahunan">
												</tbody>
											</table>
											<div class="row">
												<div class="col-md-4">
													<button type="submit" class="btn btn-primary" id="simpan_komdat_tahunan">SIMPAN DATA</button>
													<button type="submit" class="btn btn-primary" id="update_komdat_tahunan" style="display:none;">UPDATE DATA</button>
												</div>
											</div>	
						             	</div>	
					             	</div>				             	
					             </div>
					             <div id="pesan_tahunan"></div>
					             
							</div>
						</div>											
					</form>					
                  </div>


                  <div class="tab-pane table-responsive" id="tab_4">
                  	<form role="form" id="form_edit" method="post" action="<?php echo base_url(); ?>laporan_komdat/update_edit_komdat">
						<h3 id="judul_formulir">UPDATE DATA</h3>
						<div class="row">
							<div class="col-md-3">
								<div class="form-group">
								  <label for="jenis_edit">Jenis Laporan</label>						
									<select class="form-control" id="jenis_edit" name="jenis_edit" required >
										<option value="1">KESEHATAN IBU DAN ANAK</option>
										<option value="2">GIZI</option>
										<option value="3">IMUNISASI</option>
										<option value="4">PENYAKIT</option>
										<option value="5">DATA TRIWULANAN</option>
										<option value="6">DATA TAHUNAN</option>
									</select>
								</div>
							</div>
							
			              <div class="col-md-2">
			                <div class="form-group">
								<label for="thn_edit">Pilih Tahun</label>						
								<select class="form-control" id="thn_edit" name="thn_edit" required >
									<option value="2017">2017</option>
									<option value="2018">2018</option>
									<option value="2019">2019</option>
									<option value="2020">2020</option>
									<option value="2021">2021</option>
									<option value="2022">2022</option>
								</select>
			                </div>
			              </div>
			              <div class="col-md-2">
			                <div class="form-group">
			                  <label for="bln_edit">Pilih Bulan</label>						
								<select class="form-control" id="bln_edit" name="bln_edit" required >
									<option value="1">Januari</option>
									<option value="2">Februari</option>
									<option value="3">Maret</option>
									<option value="4">April</option>
									<option value="5">Mei</option>
									<option value="6">Juni</option>
									<option value="7">Juli</option>
									<option value="8">Agustus</option>
									<option value="9">September</option>
									<option value="10">Oktober</option>
									<option value="11">November</option>
									<option value="12">Desember</option>
								</select>
			                </div>
			              </div>
			              <div class="col-md-1">
			                <div class="form-group">
			                  <label for="tampilkan">&nbsp;</label>
			                  <button id="tampilEdit" class="btn btn-primary" >Tampilkan</button>
			                </div>
			              </div>		              
			            </div>
	                    
						<table class="table table-bordered table-hover">
							<thead>
								<th>No</th>
								<th>Indikator Komdat</th>							
								<th>Jumlah</th>							
							</thead>

							<tbody id="tbl_edit_komdat">
							</tbody>
						</table>
						<div class="row">
							<div class="col-md-4">
								<button type="submit" class="btn btn-primary" id="update_edit_komdat" style="display:none;">UPDATE DATA</button>
							</div>
						</div>
						
						<div class="overlay" id="spinners_data" style="display:none;">
							<i class="fa fa-refresh fa-spin"></i>
						</div>

						<div id="pesan_edit"></div>
					</form>											
                  </div>

                  
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>

    <script src="<?php echo base_url(); ?>jquery.js"></script>
<script src="<?php echo base_url(); ?>js_exel/jquery.btechco.excelexport.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.base64.js"></script>
<script>
  (function($) {
    $(document).ready(function() { 
    $("#btnExport").click(function (e) {
        e.preventDefault();
          $("#tblExport").btechco_excelexport({
              containerid: "tblExport"
              , datatype: $datatype.Table
          });
      });
    });
  })(jQuery);
</script>


<script>
  function AfterSavedPerijinan_penelitian() {
    $('#id_perijinan_penelitian, #nama, #nomor_surat, #perihal_surat, #tanggal_surat, #nomot_telp, #kebangsaan, #alamat, #pekerjaan, #penanggung_jawab, #lokasi, #judul_penelitian, #asal_universitas, #nomor_surat_kesbangpol #tanggal_surat_kesbangpol, #surat_ditujukan_kepada, #tembusan_kepada_1, #tembusan_kepada_2, #tembusan_kepada_3, #tembusan_kepada_4, #tembusan_kepada_5, #tembusan_kepada_6, #temp, #header_ttd, #jabatan_ttd, #nama_ttd, #nip_ttd').val('');
  	$('#tbl_lampiran_perijinan_penelitian').html('');
    $('#PesanProgresUpload').html('');
    }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1;
    var limit = 10;
	//load_data_komdat(halaman, limit);
	//load_form_komdat();
	
});
</script>
<script>
  function load_data_komdat(halaman, limit) {
    $('#tbl_utama_komdat').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>laporan_komdat/load_table/',
      success: function(html) {
        $('#tbl_utama_komdat').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_perijinan_penelitian').show();
    $('#update_perijinan_penelitian').hide();
    $('#tbl_attachment_perijinan_penelitian').html('');
    $('#id_perijinan_penelitian, #nama, #nomor_surat, #perihal_surat, #tanggal_surat, #nomot_telp, #kebangsaan, #alamat, #pekerjaan, #penanggung_jawab, #lokasi, #judul_penelitian, #asal_universitas, #nomor_surat_kesbangpol #tanggal_surat_kesbangpol, #surat_ditujukan_kepada, #tembusan_kepada_1, #tembusan_kepada_2, #tembusan_kepada_3, #tembusan_kepada_4, #tembusan_kepada_5, #tembusan_kepada_6, #temp #header_ttd, #jabatan_ttd, #nama_ttd, #nip_ttd').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  function datakomdat(halaman, limit, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_utama_komdat').html('');
		$('#spinners_data').show();
		$.ajax({
		  type: 'POST',
		  async: true,
		  data: {
		    halaman: halaman,
		    limit: limit,
		    tahun: tahun,
		    bulan: bulan
		  },
		  dataType: 'html',
		  url: '<?php echo base_url(); ?>laporan_komdat/load_table/',
		  success: function(html) {
		    $('#tbl_utama_komdat').html(html);
		    $('#spinners_data').hide();
		  }
		});
  };
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilForm').on('click', function(e) {
      e.preventDefault();
      $('#thn, #bln, #tbl').show();
      $('#tbl_utama_komdat').html('');
      var halaman = 1;
	   	var limit = 10;
	   	var jenis_laporan = $('#jenis_laporan').val();
	   	
      
      load_form_komdat(halaman, limit, jenis_laporan);
    });
  });
</script>

<script>
  function load_form_komdat(halaman, limit, jenis_laporan) {
    $('#tbl_form_komdat').html('');
    $('#spinners_data').show();
    var halaman = 1;
	   	var limit = 10;
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit,
        jenis_laporan: jenis_laporan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>laporan_komdat/load_form_komdat/',
      success: function(html) {
        $('#tbl_form_komdat').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>

<!-- //////////////////// -->
<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilFormTahunan').on('click', function(e) {
      e.preventDefault();

      $('#thn_tahunan, #tbl_tahunan').show();
      $('#tbl_utama_komdat_tahunan').html('');
      var halaman = 1;
	   	var limit = 10;
	   	var jenis_laporan_tahunan = $('#jenis_laporan_tahunan').val();
	   	
      
      load_form_komdat_tahunan(halaman, limit, jenis_laporan_tahunan);
    });
  });
</script>

<script>
  function load_form_komdat_tahunan(halaman, limit, jenis_laporan_tahunan) {
    $('#tbl_form_komdat').html('');
    $('#spinners_data').show();
    var halaman = 1;
	   	var limit = 10;
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit,
        jenis_laporan_tahunan: jenis_laporan_tahunan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>laporan_komdat/load_form_komdat_tahunan/',
      success: function(html) {
        $('#tbl_form_komdat_tahunan').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilkan').on('click', function(e) {
      e.preventDefault();
      $('#tbl_utama_komdat').html('');
      	var halaman = 1;
	   	var limit = 10;
	   	var tahun = $('#thn_komdat').val();
	   	var bulan = $('#bln_komdat').val();	   	
      
		datakomdatkia(halaman, limit, tahun, bulan);
		datakomdatgizi(halaman, limit, tahun, bulan);
		datakomdatimunisasi(halaman, limit, tahun, bulan);
		datakomdatpenyakit(halaman, limit, tahun, bulan);
		datakomdattriwulan(halaman, limit, tahun, bulan);
    });
  });
</script>

<script type="text/javascript">
  function datakomdatkia(halaman, limit, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_komdat_kia').html('');
		$('#spinners_data').show();
		$.ajax({
		  type: 'POST',
		  async: true,
		  data: {
		    halaman: halaman,
		    limit: limit,
		    tahun: tahun,
		    bulan: bulan
		  },
		  dataType: 'html',
		  url: '<?php echo base_url(); ?>laporan_komdat/load_kia/',
		  success: function(html) {
		    $('#tbl_komdat_kia').html(html);
		    $('#spinners_data').hide();
		  }
		});
  };
</script>

<script type="text/javascript">
  function datakomdatgizi(halaman, limit, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_komdat_gizi').html('');
		$('#spinners_data').show();
		$.ajax({
		  type: 'POST',
		  async: true,
		  data: {
		    halaman: halaman,
		    limit: limit,
		    tahun: tahun,
		    bulan: bulan
		  },
		  dataType: 'html',
		  url: '<?php echo base_url(); ?>laporan_komdat/load_gizi/',
		  success: function(html) {
		    $('#tbl_komdat_gizi').html(html);
		    $('#spinners_data').hide();
		  }
		});
  };
</script>

<script type="text/javascript">
  function datakomdatimunisasi(halaman, limit, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_komdat_imunisasi').html('');
		$('#spinners_data').show();
		$.ajax({
		  type: 'POST',
		  async: true,
		  data: {
		    halaman: halaman,
		    limit: limit,
		    tahun: tahun,
		    bulan: bulan
		  },
		  dataType: 'html',
		  url: '<?php echo base_url(); ?>laporan_komdat/load_imunisasi/',
		  success: function(html) {
		    $('#tbl_komdat_imunisasi').html(html);
		    $('#spinners_data').hide();
		  }
		});
  };
</script>

<script type="text/javascript">
  function datakomdatpenyakit(halaman, limit, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_komdat_penyakit').html('');
		$('#spinners_data').show();
		$.ajax({
		  type: 'POST',
		  async: true,
		  data: {
		    halaman: halaman,
		    limit: limit,
		    tahun: tahun,
		    bulan: bulan
		  },
		  dataType: 'html',
		  url: '<?php echo base_url(); ?>laporan_komdat/load_penyakit/',
		  success: function(html) {
		    $('#tbl_komdat_penyakit').html(html);
		    $('#spinners_data').hide();
		  }
		});
  };
</script>

<script type="text/javascript">
  function datakomdattriwulan(halaman, limit, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_komdat_triwulan').html('');
		$('#spinners_data').show();
		$.ajax({
		  type: 'POST',
		  async: true,
		  data: {
		    halaman: halaman,
		    limit: limit,
		    tahun: tahun,
		    bulan: bulan
		  },
		  dataType: 'html',
		  url: '<?php echo base_url(); ?>laporan_komdat/load_triwulan/',
		  success: function(html) {
		    $('#tbl_komdat_triwulan').html(html);
		    $('#spinners_data').hide();
		  }
		});
  };
</script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>

 <script>
    $("#form_isian").submit(function(e) {
        e.preventDefault();
        
        var dataform = $("#form_isian").serialize();
        $.ajax({
            url: '<?php echo base_url(); ?>laporan_komdat/save/',
            type: "post",
            data: dataform,
            success: function(result) {
                var hasil = JSON.parse(result);
                if (hasil.hasil == "dobel") {   
                	$("#pesan").show();                 
                    $("#pesan").html("<div class=\"alert alert-danger\">Data sudah ada, silahkan cek tahun dan bulan</div>");
                    $('#pesan').animate({
						 opacity: 1,
						 right: "50px",
						 bottom: "10px",
						 height: "toggle"
						 }, 6000, function() {
					 }).css('position','fixed');
                    
                }else if (hasil.hasil == "gagal") {  
                	$("#pesan").show();
                	$("#pesan").html("<div class=\"alert alert-danger\">Data gagal disimpan !</div>");
                	$('#pesan').animate({
						 opacity: 1,
						 right: "50px",
						 bottom: "10px",
						 height: "toggle"
						 }, 6000, function() {
					 }).css('position','fixed');
                }else {
                	$("#pesan").show();
                    $("#pesan").html("<div class=\"alert alert-success\">Data berhasil disimpan !</div>");
                    // kosongkan lagi error form
                    $("#tbl_form_komdat").val('');
                    $("#tbl").hide();
                    $("#thn").hide();
                    $("#bln").hide();
                    //$("#alamat").val('');
                    $('#pesan').animate({
						 opacity: 1,
						 right: "50px",
						 bottom: "10px",
						 height: "toggle"
						 }, 6000, function() {
					 }).css('position','fixed');
                }
            }
        });
    });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilEdit').on('click', function(e) {
      e.preventDefault();
      $('#tbl_edit_komdat').html('');
      	var halaman = 1;
	   	var limit = 10;
	   	var jenis = $('#jenis_edit').val();
	   	var tahun = $('#thn_edit').val();
	   	var bulan = $('#bln_edit').val();
	   	// alert(jenis);
	   	// alert(tahun);
	   	// alert(bulan);
      
		datakomdatedit(halaman, limit, jenis, tahun, bulan);		
		$('#update_edit_komdat').show();
    });
  });
</script>

<script type="text/javascript">
  function datakomdatedit(halaman, limit, jenis, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_edit_komdat').html('');
		$('#spinners_data').show();
		$.ajax({
		  type: 'POST',
		  async: true,
		  data: {
		    halaman: halaman,
		    limit: limit,
		    jenis: jenis,
		    tahun: tahun,
		    bulan: bulan
		  },
		  dataType: 'html',
		  url: '<?php echo base_url(); ?>laporan_komdat/load_edit_komdat/',
		  success: function(html) {
		    $('#tbl_edit_komdat').html(html);
		    $('#spinners_data').hide();
		  }
		});
  };
</script>

<script type="text/javascript">
	$('.content').on('focus', 'input[type=number]', function (e) {
	  $(this).on('mousewheel.disableScroll', function (e) {
	    e.preventDefault()
	  })
	})
	$('.content').on('blur', 'input[type=number]', function (e) {
	  $(this).off('mousewheel.disableScroll')
	})
</script>

<script>
    $("#form_tahunan").submit(function(e) {
        e.preventDefault();
        
        var dataform = $("#form_tahunan").serialize();
        $.ajax({
            url: '<?php echo base_url(); ?>laporan_komdat/save_tahunan/',
            type: "post",
            data: dataform,
            success: function(result) {
                var hasil = JSON.parse(result);
                if (hasil.hasil == "dobel") {   
                	$("#pesan_tahunan").show();                 
                    $("#pesan_tahunan").html("<div class=\"alert alert-danger\">Data sudah ada, silahkan cek tahun dan bulan</div>");
                    $('#pesan_tahunan').animate({
						 opacity: 1,
						 right: "50px",
						 bottom: "10px",
						 height: "toggle"
						 }, 6000, function() {
					 }).css('position','fixed');
                    
                }else if (hasil.hasil == "gagal") {  
                	$("#pesan_tahunan").show();
                	$("#pesan_tahunan").html("<div class=\"alert alert-danger\">Data gagal disimpan !</div>");
                	$('#pesan_tahunan').animate({
						 opacity: 1,
						 right: "50px",
						 bottom: "10px",
						 height: "toggle"
						 }, 6000, function() {
					 }).css('position','fixed');
                }else {
                	$("#pesan_tahunan").show();
                    $("#pesan_tahunan").html("<div class=\"alert alert-success\">Data berhasil disimpan !</div>");
                    // kosongkan lagi error form
                    $("#tbl_tahunan").val('');
                    $("#tbl").hide();
                    $("#thn").hide();
                    $("#bln").hide();
                    //$("#alamat").val('');
                    $('#pesan_tahunan').animate({
						 opacity: 1,
						 right: "50px",
						 bottom: "10px",
						 height: "toggle"
						 }, 6000, function() {
					 }).css('position','fixed');
                }
            }
        });
    });
</script>

<script>
    $("#form_edit").submit(function(e) {
        e.preventDefault();
        
        var dataform = $("#form_edit").serialize();
        $.ajax({
            url: '<?php echo base_url(); ?>laporan_komdat/update_edit_komdat/',
            type: "post",
            data: dataform,
            success: function(result) {
                var hasil = JSON.parse(result);
                if (hasil.hasil == "dobel") {   
                	$("#pesan_edit").show();                 
                    $("#pesan_edit").html("<div class=\"alert alert-danger\">Data sudah ada, silahkan cek tahun dan bulan</div>");
                    $('#pesan_edit').animate({
						 opacity: 1,
						 right: "50px",
						 bottom: "10px",
						 height: "toggle"
						 }, 2000, function() {
					 }).css('position','fixed');
                    
                }else if (hasil.hasil == "gagal") {  
                	$("#pesan_edit").show();
                	$("#pesan_edit").html("<div class=\"alert alert-danger\">Tidak ada data yang di update !</div>");
                	$('#pesan_edit').animate({
						 opacity: 1,
						 right: "50px",
						 bottom: "10px",
						 height: "toggle"
						 }, 2000, function() {
					 }).css('position','fixed');
                }else {
                	$("#pesan_edit").show();
                    $("#pesan_edit").html("<div class=\"alert alert-success\">Data berhasil di update !</div>");
                    // kosongkan lagi error form
                    $("#tbl_edit_komdat").val('');
                    $("#tbl").hide();
                    $("#thn").hide();
                    $("#bln").hide();
                    //$("#alamat").val('');
                    $('#pesan_edit').animate({
						 opacity: 1,
						 right: "50px",
						 bottom: "10px",
						 height: "toggle"
						 }, 2000, function() {
					 }).css('position','fixed');
                }
            }
        });
    });
</script>