<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
      <li><a data-toggle="tab" href="#tab_2" id="klik_tab_tampil" >Tampil</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title" id="judul_formulir">FORMULIR INPUT</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" id="form_baru"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=posting" enctype="multipart/form-data">
                  <div class="box-body">
										<div class="form-group" style="display:none;">
											<label for="temp">temp</label>
											<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="mode">mode</label>
											<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_data_desa">id_data_desa</label>
											<input class="form-control" id="id_data_desa" name="id" value="" placeholder="id_data_desa" type="text">
										</div>
										<div class="form-group">
											<label for="jabatan_penandatangan">Jabatan Penandatangan</label>
											<input class="form-control" id="jabatan_penandatangan" name="jabatan_penandatangan" value="-" placeholder="jabatan_penandatangan" type="text">
										</div>
										<div class="form-group">
											<label for="nama_penandatangan">Nama Penandatangan</label>
											<input class="form-control" id="nama_penandatangan" name="nama_penandatangan" value="-" placeholder="nama_penandatangan" type="text">
										</div>
										<div class="form-group">
											<label for="pangkat_penandatangan">Pangkat Penandatangan</label>
											<input class="form-control" id="pangkat_penandatangan" name="pangkat_penandatangan" value="-" placeholder="pangkat_penandatangan" type="text">
										</div>
										<div class="form-group">
											<label for="nip_penandatangan">NIP Penandatangan</label>
											<input class="form-control" id="nip_penandatangan" name="nip_penandatangan" value="-" placeholder="nip_penandatangan" type="text">
										</div>
										<div class="form-group">
											<label for="desa_nama">desa Nama</label>
											<input class="form-control" id="desa_nama" name="desa_nama" value="" placeholder="desa_nama" type="text">
										</div>
										<div class="form-group">
											<label for="desa_telpn">desa telpn</label>
											<input class="form-control" id="desa_telpn" name="desa_telpn" value="-" placeholder="desa_telpn" type="text">
										</div>
										<div class="form-group">
											<label for="desa_alamat">desa alamat</label>
											<input class="form-control" id="desa_alamat" name="desa_alamat" value="-" placeholder="desa_alamat" type="text">
										</div>
										<div class="form-group">
											<label for="desa_fax">desa fax</label>
											<input class="form-control" id="desa_fax" name="desa_fax" value="-" placeholder="desa_fax" type="text">
										</div>
										<div class="form-group">
											<label for="desa_kode_pos">desa kode pos</label>
											<input class="form-control" id="desa_kode_pos" name="desa_kode_pos" value="-" placeholder="desa_kode_pos" type="text">
										</div>
										<div class="form-group">
											<label for="desa_nomor">desa_nomor</label>
											<input class="form-control" id="desa_nomor" name="desa_nomor" value="-" placeholder="desa_nomor" type="text">
										</div>
										<div class="form-group">
											<label for="desa_website">desa website</label>
											<input class="form-control" id="desa_website" name="desa_website" value="" placeholder="desa_website" type="text">
										</div>
										<div class="form-group">
											<label for="desa_email">desa_email</label>
											<input class="form-control" id="desa_email" name="desa_email" value="-" placeholder="desa_email" type="text">
										</div>
										<div class="form-group">
											<label for="jenis_pemerintahan">jenis_pemerintahan</label>
												<select class="form-control" id="jenis_pemerintahan" name="jenis_pemerintahan" >
													<option value="0">Desa</option>
													<option value="1">Kelurahan</option>
												</select>
											</div>
											<div class="form-group">
												<label for="id_kecamatan">id_kecamatan</label>
												<select class="form-control" id="id_kecamatan" name="id_kecamatan" >
												</select>
											</div>
											<div class="form-group">
												<label for="id_desa">id_desa</label>
												<select class="form-control" id="id_desa" name="id_desa" >
													<option value="0">Pilih Desa</option>
												</select>
											</div>
										<div class="alert alert-info alert-dismissable">
											<div class="form-group">
												<label for="remake">Keterangan Lampiran </label>
												<input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran " type="text">
											</div>
											<div class="form-group">
												<label for="myfile">File Lampiran </label>
												<input type="file" size="60" name="myfile" id="file_lampiran" >
											</div>
											<div id="ProgresUpload">
												<div id="BarProgresUpload"></div>
												<div id="PersenProgresUpload">0%</div >
											</div>
											<div id="PesanProgresUpload"></div>
										</div>
										<div class="alert alert-info alert-dismissable">
											<h3 class="box-title">Data Lampiran </h3>
											<table class="table table-bordered">
												<tr>
													<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
												</tr>
												<tbody id="tbl_attachment_data_desa">
												</tbody>
											</table>
										</div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="simpan_data_desa">SIMPAN</button>
                    <button type="submit" class="btn btn-primary" id="update_data_desa" style="display:none;">UPDATE</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <div class="tab-pane" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                Data Nama SKPD 
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="kata_kunci">
                  <select name="limit_data_data_desa" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_data_desa">
                    <option value="10">10 Per-Halaman</option>
                    <option value="20">20 Per-Halaman</option>
                    <option value="50">50 Per-Halaman</option>
                    <option value="999999999">Semua</option>
                  </select>
                  <select name="urut_data_data_desa" class="form-control input-sm pull-right" style="width: 150px;" id="urut_data_data_desa">
                    <option value="desa_nama">Nama Data SKPD</option>
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-body table-responsive no-padding">
                        <div id="tblExport">
                          <table class="table table-hover">
                            <tbody>
                              <tr id="header_exel">
                                <th>NO</th> 
                                <th>Nama SKPD</th>
                                <th>Website</th>
                                <th>Status</th>
                                <th>Proses</th>
                              </tr>
                            </tbody>
                            <tbody id="tbl_utama_data_desa">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="row">
                        
                        
                        
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!----------------------->
<script>
  function load_data_data_desa(halaman, limit, kata_kunci, urut_data_data_desa) {
    $('#tbl_utama_data_desa').html('');
    $('#spinners_data').show();
    var limit_data_data_desa = $('#limit_data_data_desa').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:halaman,
        limit:limit,
        kata_kunci:kata_kunci,
        urut_data_data_desa:urut_data_data_desa
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>data_desa/json_all_data_desa/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_data_desa="' + json[i].id_data_desa + '" id="' + json[i].id_data_desa + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
          tr += '<td valign="top">' + json[i].desa_nama + '</td>';          
          tr += '<td valign="top">' + json[i].desa_website + '</td>';         
          if( json[i].status == 1 ){
            tr += '<td valign="top" id="td_2_'+i+'"><a href="#" id="inaktifkan" ><i class="fa fa-cut"></i> Aktif</a></td>';
          }
          else{
            tr += '<td valign="top" id="td_3_'+i+'"><a href="#" id="aktifkan" ><i class="fa fa-cut"></i> Tidak Aktif</a></td>';
          }
          tr += '<td valign="top"><a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> <a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';          
          tr += '</tr>';
        }
        $('#tbl_utama_data_desa').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
  }
</script>
<!----------------------->
<script>
  function cek_desa() {
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:'a'
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>data_desa/cek_desa/',
      success: function(text) {
        
        var halaman = 1;
        var limit_data_data_desa = $('#limit_data_data_desa').val();
        var limit = limit_per_page_custome(limit_data_data_desa);
        var kata_kunci = $('#kata_kunci').val();
        var urut_data_data_desa = $('#urut_data_data_desa').val();
        load_data_data_desa(halaman, limit, kata_kunci, urut_data_data_desa);
      }
    });
  }
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    // cek_desa();
        var halaman = 1;
        var limit_data_data_desa = $('#limit_data_data_desa').val();
        var limit = limit_per_page_custome(limit_data_data_desa);
        var kata_kunci = $('#kata_kunci').val();
        var urut_data_data_desa = $('#urut_data_data_desa').val();
        load_data_data_desa(halaman, limit, kata_kunci, urut_data_data_desa);
  });
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_desa').on('click', '#inaktifkan', function() {
    var id_data_desa = $(this).closest('tr').attr('id_data_desa');
    alertify.confirm('Anda yakin data akan dipubish?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_data_desa:id_data_desa
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>data_desa/inaktifkan',
          success: function(html) {
            alertify.success('Data berhasil publish');
            var halaman = 1;
            var limit_data_data_desa = $('#limit_data_data_desa').val();
            var limit = limit_per_page_custome(limit_data_data_desa);
            var kata_kunci = $('#kata_kunci').val();
            var urut_data_data_desa = $('#urut_data_data_desa').val();
            load_data_data_desa(halaman, limit, kata_kunci, urut_data_data_desa);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_desa').on('click', '#aktifkan', function() {
    var id_data_desa = $(this).closest('tr').attr('id_data_desa');
    alertify.confirm('Anda yakin data akan dipubish?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_data_desa:id_data_desa
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>data_desa/aktifkan',
          success: function(html) {
            alertify.success('Data berhasil publish');
            var halaman = 1;
            var limit_data_data_desa = $('#limit_data_data_desa').val();
            var limit = limit_per_page_custome(limit_data_data_desa);
            var kata_kunci = $('#kata_kunci').val();
            var urut_data_data_desa = $('#urut_data_data_desa').val();
            load_data_data_desa(halaman, limit, kata_kunci, urut_data_data_desa);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#limit_data_data_desa').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_data_desa = $('#limit_data_data_desa').val();
      var limit = limit_per_page_custome(limit_data_data_desa);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_data_desa = $('#urut_data_data_desa').val();
      load_data_data_desa(halaman, limit, kata_kunci, urut_data_data_desa);
    });
  });
</script>

<script>
  function load_option_data_desa() {
    $('#id_desa').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_desa/load_the_option/',
      success: function(html) {
        $('#id_desa').html('<option value="0">Pilih SKPD</option>  '+html+'');
      }
    });
  }
</script>

<script>
  function AfterSavedData_desa() {
    $('#id_data_desa, #jabatan_penandatangan, #nama_penandatangan, #jenis_pemerintahan, #pangkat_penandatangan, #nip_penandatangan,  #desa_nama, #desa_telpn, #desa_alamat, #desa_fax, #desa_kode_pos, #desa_nomor, #desa_website, #desa_email, #id_desa').val('');
    $('#tbl_attachment_posting').html('');
    $('#PesanProgresUpload').html('');
    // load_option_data_desa();
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
 
<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_data_desa').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'data_desa',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_data_desa').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_posting').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_data_desa').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_data_desa').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_desa').on('click', '#del_ajax', function() {
    var id_data_desa = $(this).closest('tr').attr('id_data_desa');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_data_desa"] = id_posting;
        var url = '<?php echo base_url(); ?>data_desa/hapus/';
        HapusData(parameter, url);
        $('[id_data_desa='+id_data_desa+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_desa6').on('click', '#restore_ajax', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan direstore?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_data_desa"] = id_data_desa;
        var url = '<?php echo base_url(); ?>data_desa/restore/';
        HapusData(parameter, url);
        $('[id_data_desa='+id_data_desa+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_desa').on('click', '.update_id', function() {
    load_option_data_desa();
    $('#mode').val('edit');
    $('#simpan_data_desa').hide();
    $('#update_data_desa').show();
    var id_data_desa = $(this).closest('tr').attr('id_data_desa');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_data_desa');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_data_desa').val(id_data_desa);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_data_desa:id_data_desa
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>data_desa/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#jabatan_penandatangan').val(json[i].jabatan_penandatangan);
            $('#nama_penandatangan').val(json[i].nama_penandatangan);
            $('#pangkat_penandatangan').val(json[i].pangkat_penandatangan);
            $('#nip_penandatangan').val(json[i].nip_penandatangan);
            $('#desa_nama').val(json[i].desa_nama);
            $('#desa_telpn').val(json[i].desa_telpn);
            $('#desa_alamat').val(json[i].desa_alamat);
            $('#desa_fax').val(json[i].desa_fax);
            $('#desa_kode_pos').val(json[i].desa_kode_pos);
            $('#desa_nomor').val(json[i].desa_nomor);
            $('#desa_website').val(json[i].desa_website);
            $('#desa_email').val(json[i].desa_email);
            $('#id_desa').val(json[i].id_desa);
            $('#jenis_pemerintahan').val(json[i].jenis_pemerintahan);
          }
        }
      });
    AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_data_desa').show();
    $('#update_data_desa').hide();
    $('#tbl_attachment_data_desa').html('');
    $('#id_data_desa, #jabatan_penandatangan, #nama_penandatangan, #jenis_pemerintahan, #pangkat_penandatangan, #nip_penandatangan,  #desa_nama, #desa_telpn, #desa_alamat, #desa_fax, #desa_kode_pos, #desa_nomor, #desa_website, #desa_email, #id_desa').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_data_desa').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'jabatan_penandatangan', 'jenis_pemerintahan', 'nama_penandatangan', 'pangkat_penandatangan', 'nip_penandatangan', 'desa_nama', 'desa_telpn', 'desa_alamat', 'desa_fax', 'desa_kode_pos', 'desa_nomor', 'desa_website', 'desa_email', 'id_desa' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["jabatan_penandatangan"] = $("#jabatan_penandatangan").val();
      parameter["nama_penandatangan"] = $("#nama_penandatangan").val();
      parameter["pangkat_penandatangan"] = $("#pangkat_penandatangan").val();
      parameter["nip_penandatangan"] = $("#nip_penandatangan").val();
      parameter["desa_nama"] = $("#desa_nama").val();
      parameter["desa_telpn"] = $("#desa_telpn").val();
      parameter["desa_alamat"] = $("#desa_alamat").val();
      parameter["desa_fax"] = $("#desa_fax").val();
      parameter["desa_kode_pos"] = $("#desa_kode_pos").val();
      parameter["desa_nomor"] = $("#desa_nomor").val();
      parameter["desa_website"] = $("#desa_website").val();
      parameter["desa_email"] = $("#desa_email").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["jenis_pemerintahan"] = $("#jenis_pemerintahan").val();
      parameter["temp"] = $("#temp").val();
      var url = '<?php echo base_url(); ?>data_desa/simpan_data_desa';
      
      var parameterRv = [ 'jabatan_penandatangan', 'nama_penandatangan', 'jenis_pemerintahan', 'pangkat_penandatangan', 'nip_penandatangan', 'desa_nama', 'desa_telpn', 'desa_alamat', 'desa_fax', 'desa_kode_pos', 'desa_nomor', 'desa_website', 'desa_email', 'id_desa' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedData_desa();
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_data_desa').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'id_data_desa', 'jabatan_penandatangan', 'nama_penandatangan', 'jenis_pemerintahan', 'pangkat_penandatangan', 'nip_penandatangan', 'desa_nama', 'desa_telpn', 'desa_alamat', 'desa_fax', 'desa_kode_pos', 'desa_nomor', 'desa_website', 'desa_email', 'id_desa' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["jabatan_penandatangan"] = $("#jabatan_penandatangan").val();
      parameter["nama_penandatangan"] = $("#nama_penandatangan").val();
      parameter["pangkat_penandatangan"] = $("#pangkat_penandatangan").val();
      parameter["nip_penandatangan"] = $("#nip_penandatangan").val();
      parameter["desa_nama"] = $("#desa_nama").val();
      parameter["desa_telpn"] = $("#desa_telpn").val();
      parameter["desa_alamat"] = $("#desa_alamat").val();
      parameter["desa_fax"] = $("#desa_fax").val();
      parameter["desa_kode_pos"] = $("#desa_kode_pos").val();
      parameter["desa_nomor"] = $("#desa_nomor").val();
      parameter["desa_website"] = $("#desa_website").val();
      parameter["desa_email"] = $("#desa_email").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["jenis_pemerintahan"] = $("#jenis_pemerintahan").val();
      parameter["temp"] = $("#temp").val();
      parameter["id_data_desa"] = $("#id_data_desa").val();
      var url = '<?php echo base_url(); ?>data_desa/update_data_desa';
      
      var parameter = [ 'id_data_desa', 'jabatan_penandatangan', 'nama_penandatangan', 'jenis_pemerintahan', 'pangkat_penandatangan', 'nip_penandatangan', 'desa_nama', 'desa_telpn', 'desa_alamat', 'desa_fax', 'desa_kode_pos', 'desa_nomor', 'desa_website', 'desa_email', 'id_desa' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script>
  function load_opd_domain() {
    $('#id_kecamatan').html('');
    $('#option_kecamatan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>user/option_kecamatan/',
      success: function(html) {
        $('#option_kecamatan').html('<option value="0">Pilih Kecamatan</option>'+html+'');
        $('#id_kecamatan').html('<option value="0">Pilih Kecamatan</option>'+html+'');
      }
    });
  }
</script>

<script>
  function load_id_skpd() {
    $('#id_skpd').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>user/option_skpd/',
      success: function(html) {
        $('#id_skpd').html('<option value="0">Pilih SKPD</option>'+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var id_kecamatan = $('#id_kecamatan').val();
      load_option_desa(id_kecamatan);
    });
  });
</script>

<script>
  function load_option_desa(id_kecamatan) {
    $('#id_desa').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html(' '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  //load_id_skpd();
  load_opd_domain();
});
</script>
