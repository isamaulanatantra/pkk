
		<header class="post-header">

        <h3><?php if(!empty($judul_posting)){ echo $judul_posting; } ?></h3><!--  style="post-title" .post-title -->

        <p class="simple-share">
            <span><span class="article-date"><i class="fa fa-clock-o"></i> <?php if(!empty($created_time)){ echo $created_time; } ?></span></span>
        </p>

					<div class="page-slider">
						
										<?php
										$rul_id_posting = $this->uri->segment(3);
										$query1 = $this->db->query("SELECT file_name from attachment where id_tabel=".$rul_id_posting." and file_name LIKE '%.jpg' order by uploaded_time desc");
										if(($query1->num_rows())>0){
											echo'
						<div id="carousel-example-generic" class="carousel slide carousel-slider" data-ride="carousel">
							
								<ol class="carousel-indicators carousel-indicators-frontend">
									<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
									<li data-target="#carousel-example-generic" data-slide-to="1"></li>
									<li data-target="#carousel-example-generic" data-slide-to="2"></li>
								</ol>
								<div class="clearfix"></div>
							
								<div class="carousel-inner" role="listbox">
								';
										$a = 0;
										foreach ($query1->result() as $row1)
											{
												$a = $a+1;
												if( $a == 1 ){
													echo
													'
													<div class="item active">
															<img src="'.base_url().'media/upload/'.$row1->file_name.'">
															<div class="container">
																	<div class="carousel-position-six text-uppercase text-center">
																	</div>
															</div>
													</div>
													';
													}
												else{
													echo
													'
													<div class="item">
															<img src="'.base_url().'media/upload/'.$row1->file_name.'">
															<div class="container">
																	<div class="carousel-position-six text-uppercase text-center">
																	</div>
															</div>
													</div>
													';
													} 
											}
								echo '
								</div>
							
							<a class="left carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="prev">
									<i class="fa fa-angle-left" aria-hidden="true"></i>
							</a>
							<a class="right carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="next">
									<i class="fa fa-angle-right" aria-hidden="true"></i>
							</a>
						</div>
											';
										}
										?>
					</div>
    </header><!-- .post-header -->

    <div class="post-content clearfix">
			<?php if(!empty($isi_posting)){ echo $isi_posting; } ?>
		</div>
									<div class="box-footer blog-item">
										<?php
										$rul_id_posting = $this->uri->segment(3);
										$querypdf = $this->db->query("SELECT * from attachment where id_tabel=".$rul_id_posting." and file_name LIKE '%.pdf' order by uploaded_time asc");
										if(($querypdf->num_rows())>0){
										$a = 0;
										foreach ($querypdf->result() as $rowpdf)
											{
													echo
													'
														<p><a href="'.base_url().'media/upload/'.$rowpdf->file_name.'" class="mailbox-attachment-name" target="_blank"><i class="fa fa-file-pdf-o"></i> '.$rowpdf->keterangan.'</a></p>
														<hr />
													';
											}
										}
										$queryword = $this->db->query("SELECT * from attachment where id_tabel=".$rul_id_posting." and file_name LIKE '%.docx' order by uploaded_time asc");
										if(($queryword->num_rows())>0){
										$a = 0;
										foreach ($queryword->result() as $rowword)
											{
											echo
											'
													<p><a href="'.base_url().'media/upload/'.$rowword->file_name.'" class="mailbox-attachment-name" target="_blank"><i class="fa fa-file-word-o"></i> '.$rowword->keterangan.'</a></p>
													<hr />
											';
											}
										}
										$queryexcel = $this->db->query("SELECT * from attachment where id_tabel=".$rul_id_posting." and file_name LIKE '%.xlsx' order by uploaded_time asc");
										if(($queryexcel->num_rows())>0){
										$a = 0;
										foreach ($queryexcel->result() as $rowexcel)
											{
											echo
											'
														<p><a href="'.base_url().'media/upload/'.$rowexcel->file_name.'" class="mailbox-attachment-name" target="_blank"><i class="fa fa-file-excel-o"></i> '.$rowexcel->keterangan.'</a></p>
											';
											}
										}
										$queryexcelx = $this->db->query("SELECT * from attachment where id_tabel=".$rul_id_posting." and file_name LIKE '%.xls' order by uploaded_time asc");
										if(($queryexcelx->num_rows())>0){
										$a = 0;
										foreach ($queryexcelx->result() as $rowexcelx)
											{
											echo
											'
														<p><a href="'.base_url().'media/upload/'.$rowexcelx->file_name.'" class="mailbox-attachment-name" target="_blank"><i class="fa fa-file-excel-o"></i> '.$rowexcelx->keterangan.'</a></p>
											';
											}
										}
										?>
									</div>
                  <div class="post-comment">
										<?php if(!empty($isi_halaman)){ echo $isi_halaman; } ?>
  <!-- AddToAny BEGIN -->
  <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
  <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
  <a class="a2a_button_facebook"></a>
  <a class="a2a_button_twitter"></a>
  <a class="a2a_button_email"></a>
  </div>
  <script async src="https://static.addtoany.com/menu/page.js"></script>
  <!-- AddToAny END -->
                  </div>
                  <div class="post-comment">
										<?php //echo $pagination_bottom; ?>
                  </div>
                  <div class="post-comment">
									<?php
									$web=$this->uut->namadomain(base_url());
										if($this->uri->segment(4)=='DAFTAR_INFORMASI_PUBLIK.HTML'){
											
										}
										else{
											if($tampil_menu_atas==1){}
											else{
											$this -> load -> view('modul/formulir_komentar.php');
											}
										}
									?>
                  </div>
                  <footer class="post-meta">
  <!-- .<div class="tags-wrapper">
    <ul class="tags-widget clearfix">
      <li class="trending">TAGS:</li>
      <li><a href="#">Creative</a></li>
      <li><a href="#">Wood</a></li>
      <li><a href="#">Advice</a></li>
    </ul>
  </div>
  tags-wrapper -->
  <div class="share-wrapper clearfix">
    <div class="total-shares">
      <em id="hit_counter_posting"></em>
      <div class="caption">View</div>
    </div>
    <div class="share-buttons">
      <div class="addthis_toolbox addthis_default_style ">
        <a class="addthis_button_facebook_like at300b" fb:like:layout="button_count">
          <div fb-iframe-plugin-query="action=like&amp;app_id=172525162793917&amp;container_width=62&amp;font=arial&amp;height=25&amp;href=http%3A%2F%2Fvisitjawatengah.jatengprov.go.id%2Fdetailnews.php%3F8ef0327d29dfae100751f4ed0a042790-5211-Kunjungi%2520Tawangmangu%2C%2520Menhub%2520Budi%2520Karya%2520Cek%2520Rem%2520Bus%2520Pariwisata&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;send=false&amp;share=false&amp;show_faces=false&amp;width=90" fb-xfbml-state="rendered" data-send="false" data-href="http://visitjawatengah.jatengprov.go.id/detailnews.php?8ef0327d29dfae100751f4ed0a042790-5211-Kunjungi%20Tawangmangu,%20Menhub%20Budi%20Karya%20Cek%20Rem%20Bus%20Pariwisata" data-font="arial" data-height="25" data-width="90" data-action="like" data-share="false" data-show_faces="false" data-layout="button_count" style="height: 25px;" class="fb-like fb_iframe_widget"><span style="vertical-align: bottom; width: 62px; height: 20px;"><iframe class="" src="https://web.facebook.com/v2.6/plugins/like.php?action=like&amp;app_id=172525162793917&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2Fvy-MhgbfL4v.js%3Fversion%3D44%23cb%3Dfd05fbc524798%26domain%3Dvisitjawatengah.jatengprov.go.id%26origin%3Dhttp%253A%252F%252Fvisitjawatengah.jatengprov.go.id%252Ff37d4c58a39474a%26relation%3Dparent.parent&amp;container_width=62&amp;font=arial&amp;height=25&amp;href=http%3A%2F%2Fvisitjawatengah.jatengprov.go.id%2Fdetailnews.php%3F8ef0327d29dfae100751f4ed0a042790-5211-Kunjungi%2520Tawangmangu%2C%2520Menhub%2520Budi%2520Karya%2520Cek%2520Rem%2520Bus%2520Pariwisata&amp;layout=button_count&amp;locale=en_US&amp;sdk=joey&amp;send=false&amp;share=false&amp;show_faces=false&amp;width=90" style="border: medium none; visibility: visible; width: 62px; height: 20px;" title="fb:like Facebook Social Plugin" allow="encrypted-media" scrolling="no" allowfullscreen="true" allowtransparency="true" name="f91874b5e05c8" frameborder="0" height="25px" width="90px"></iframe></span></div>
        </a>
        <a class="addthis_button_tweet at300b">
          <div style="width: 62px; height: 25px;" class="tweet_iframe_widget"><span><iframe data-url="http://visitjawatengah.jatengprov.go.id/detailnews.php?8ef0327d29dfae100751f4ed0a042790-5211-Kunjungi%20Tawangmangu,%20Menhub%20Budi%20Karya%20Cek%20Rem%20Bus%20Pariwisata#.XHTmUHV5Tmw.twitter" src="https://platform.twitter.com/widgets/tweet_button.704fca4914c9b90d7a9d41abcaa19933.en.html#dnt=true&amp;id=twitter-widget-1&amp;lang=en&amp;original_referer=http%3A%2F%2Fvisitjawatengah.jatengprov.go.id%2Fdetailnews.php%3F8ef0327d29dfae100751f4ed0a042790-5211-Kunjungi%2520Tawangmangu%2C%2520Menhub%2520Budi%2520Karya%2520Cek%2520Rem%2520Bus%2520Pariwisata&amp;size=m&amp;text=Kunjungi%20Tawangmangu%2C%20Menhub%20Budi%20Karya%20Cek%20Rem%20Bus%20Pariwisata%20%7C%20DINAS%20PEMUDA%20OLAHRAGA%20DAN%20PARIWISATA%20PROVINSI%20JAWA%20TENGAH%3A&amp;time=1551165010251&amp;type=share&amp;url=http%3A%2F%2Fvisitjawatengah.jatengprov.go.id%2Fdetailnews.php%3F8ef0327d29dfae100751f4ed0a042790-5211-Kunjungi%2520Tawangmangu%2C%2520Menhub%2520Budi%2520Karya%2520Cek%2520Rem%2520Bus%2520Pariwisata%23.XHTmUHV5Tmw.twitter" title="Twitter Tweet Button" style="position: static; visibility: visible; width: 61px; height: 20px;" class="twitter-share-button twitter-share-button-rendered twitter-tweet-button" allowtransparency="true" scrolling="no" id="twitter-widget-1" frameborder="0"></iframe></span></div>
        </a>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="post-nav-wrapper clearfix">
      <!-- .<div class="row">
        <div class="col-md-6 omega">
          <div class="previous-post">
            <div class="post-nav-label">
              <i class="fa fa-angle-left"></i>
              Previous post
            </div>
            <a href="detailnews.php?e7d4c8d4fe04d9b4539a075d809c6d01-5210-Persaingan Industri Wisata Di Kebumen Kian Ketat " class="post-nav-title">Persaingan Industri Wisata Di Kebumen Kian Ketat </a>
          </div>
        </div>
        <div class="col-md-6 alpha">
          <div class="next-post">
            <div class="post-nav-label">
              Next post
              <i class="fa fa-angle-right"></i>
            </div>
            <a href="detailnews.php?7b670d553471ad0fd7491c75bad587ff-5209-Taman Bunga Ragil Kuning Desa Blagung, Simo Jadi Wisata Baru Boyolali" class="post-nav-title">Taman Bunga Ragil Kuning Desa Blagung, Simo Jadi Wisata Baru Boyolali</a>
          </div>
        </div>
      </div>
      post-nav-wrapper -->
    </div>
  </div>
</footer>