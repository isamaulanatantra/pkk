
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>KOMPONEN BERANDA</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Tabel_perijinan Beranda</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link" href="#tab_1" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link active" href="#tab_2" data-toggle="tab" id="klik_tab_tampil">Data</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  	<div class="tab-pane" id="tab_1">
					  <form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=tabel_perijinan" enctype="multipart/form-data">
					  	<div class="form-group" style="display:none;">
							<label for="temp">temp</label>
							<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
						</div>
						<div class="form-group" style="display:none;">
							<label for="mode">mode</label>
							<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
						</div>
						<div class="form-group" style="display:none;">
							<label for="id_tabel_perijinan">id_tabel_perijinan</label>
							<input class="form-control" id="id_tabel_perijinan" name="id" value="" placeholder="id_tabel_perijinan" type="text">
						</div>
						<div class="form-group" style="display:none;">
							<label for="icon">Icon</label>
						  <select class="form-control" id="icon" name="icon" >
						  <option value="fa-home">fa-home</option>
						  <option value="fa-gears">fa-gears</option>
						  <option value="fa-th">fa-th</option>
						  <option value="fa-font">fa-font</option>
						  <option value="fa-comment">fa-comment</option>
						  <option value="fa-cogs">fa-cogs</option>
						  <option value="fa-cloud-download">fa-cloud-download</option>
						  <option value="fa-bar-char">fa-bar-char</option>
						  <option value="fa-phone">fa-phone</option>
						  <option value="fa-envelope">fa-envelope</option>
						  <option value="fa-link">fa-link</option>
						  <option value="fa-tasks">fa-tasks</option>
						  <option value="fa-users">fa-users</option>
						  <option value="fa-signal">fa-signal</option>
						  <option value="fa-coffee">fa-coffee</option>
						  </select>
						  <div id="iconselected"></div>
						</div>
						<div class="form-group">
							<label for="judul_tabel_perijinan">Judul Tabel_perijinan</label>
							<input class="form-control" id="judul_tabel_perijinan" name="judul_tabel_perijinan" value="" placeholder="Judul Tabel_perijinan" type="text">
						</div>
						<div class="form-group" style="display:none;">
							<label for="isi_tabel_perijinan">Isi Tabel_perijinan</label>
							<input class="form-control" id="isi_tabel_perijinan" name="isi_tabel_perijinan" value="" placeholder="Isi Tabel_perijinan" type="text">
						</div>
						<div class="row">
						<textarea id="editor_isi_tabel_perijinan"></textarea>
						</div>
						<div class="form-group">
							<label for="keterangan">Keterangan Tabel_perijinan</label>
							<input class="form-control" id="keterangan" name="keterangan" value="" placeholder="Keterangan" type="text">
						</div>
						<div class="form-group">
						<button type="submit" class="btn btn-primary" id="simpan_tabel_perijinan">SIMPAN</button>
						<button type="submit" class="btn btn-primary" id="update_tabel_perijinan" style="display:none;">UPDATE</button>
						</div>
					</form>

				 	<div class="overlay" id="overlay_form_input" style="display:none;">
						<i class="fa fa-refresh fa-spin"></i>
				  	</div>
                	</div>
                  	<div class="tab-pane active table-responsive" id="tab_2">
					  <table class="table table-bordered">
						<tr>
						  <th>NO</th>
						  <th>Judul Tabel_perijinan</th>
						  <th>Isi Tabel_perijinan</th>
						  <th>PROSES</th> 
						</tr>
						<tbody id="tbl_utama_tabel_perijinan">
						</tbody>
					  </table>
					  <div class="overlay" id="spinners_data" style="display:none;">
						<i class="fa fa-refresh fa-spin"></i>
					  </div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_tampil').on('click', function(e) {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_data_tabel_perijinan(halaman, limit);
  });
});
</script>
<script>
  function AfterSavedTabel_perijinan() {
    $('#id_tabel_perijinan, #judul_tabel_perijinan, #isi_tabel_perijinan, #icon, #keterangan').val('');
    $('#tbl_attachment_tabel_perijinan').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#icon').on('change', function(e) {
      e.preventDefault();
      var xa = $('#icon').val();
      $("#iconselected").html('<span class="fa '+xa+' ">'+xa+'</span>');
    });
  });
</script>
 
<script>
  function load_data_tabel_perijinan(halaman, limit) {
    $('#tbl_utama_tabel_perijinan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>tabel_perijinan/json_all_tabel_perijinan/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_tabel_perijinan="' + json[i].id_tabel_perijinan + '" id="id_tabel_perijinan' + json[i].id_tabel_perijinan + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
					tr += '<td valign="top">' + json[i].judul_tabel_perijinan + '</td>';
					tr += '<td valign="top">' + json[i].isi_tabel_perijinan + '</td>';
					tr += '<td valign="top">';
					tr += '<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i> Edit</a> <br />';
					var status_tabel_perijinan=json[i].status;
					if(status_tabel_perijinan==0){
						tr += '<a href="#" id="aktifkan" class="text-danger"><i class="fa fa-times-circle"></i> Tidak Aktif </a>';
					}else{
						tr += '<a href="#" id="inaktifkan" class="text-success"><i class="fa fa-check-circle"></i> Aktif</a>';
					}
					// tr += '<a href="#" id="del_ajax" ><i class="fa fa-cut"></i></a>';
					tr += '</td>';
          tr += '</tr>';
        }
        $('#tbl_utama_tabel_perijinan').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	var halaman = 1;
	var limit = limit_per_page_custome(20000);
	load_data_tabel_perijinan(halaman, limit);
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_tabel_perijinan').on('click', function(e) {
      e.preventDefault();
      var editor_isi_tabel_perijinan = CKEDITOR.instances.editor_isi_tabel_perijinan.getData();
      $('#isi_tabel_perijinan').val( editor_isi_tabel_perijinan );
      var parameter = [ 'judul_tabel_perijinan', 'isi_tabel_perijinan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["judul_tabel_perijinan"] = $("#judul_tabel_perijinan").val();
      parameter["isi_tabel_perijinan"] = $("#isi_tabel_perijinan").val();
      var url = '<?php echo base_url(); ?>tabel_perijinan/simpan_tabel_perijinan';
      
      var parameterRv = [ 'judul_tabel_perijinan', 'isi_tabel_perijinan' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedTabel_perijinan();
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_tabel_perijinan').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_tabel_perijinan').hide();
    $('#update_tabel_perijinan').show();
    var id_tabel_perijinan = $(this).closest('tr').attr('id_tabel_perijinan');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_tabel_perijinan');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_tabel_perijinan').val(id_tabel_perijinan);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_tabel_perijinan:id_tabel_perijinan
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>tabel_perijinan/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#judul_tabel_perijinan').val(json[i].judul_tabel_perijinan);
            $('#icon').val(json[i].icon);
            $('#keterangan').val(json[i].keterangan);
            CKEDITOR.instances.editor_isi_tabel_perijinan.setData(json[i].isi_tabel_perijinan);
          }
        }
      });
    //AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_tabel_perijinan').on('click', function(e) {
      e.preventDefault();
      var editor_isi_tabel_perijinan = CKEDITOR.instances.editor_isi_tabel_perijinan.getData();
      $('#isi_tabel_perijinan').val( editor_isi_tabel_perijinan );
      var parameter = [ 'id_tabel_perijinan', 'judul_tabel_perijinan', 'isi_tabel_perijinan', 'icon', 'keterangan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_tabel_perijinan"] = $("#id_tabel_perijinan").val();
      parameter["judul_tabel_perijinan"] = $("#judul_tabel_perijinan").val();
      parameter["isi_tabel_perijinan"] = $("#isi_tabel_perijinan").val();
      parameter["icon"] = $("#icon").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>tabel_perijinan/update_tabel_perijinan';
      
      var parameterRv = [ 'id_tabel_perijinan', 'judul_tabel_perijinan', 'isi_tabel_perijinan', 'icon', 'keterangan' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<!----------------------->
<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('editor_isi_tabel_perijinan');
	$(".textarea").wysihtml5();
});
</script>


<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_tabel_perijinan').on('click', '#inaktifkan', function() {
    var id_tabel_perijinan = $(this).closest('tr').attr('id_tabel_perijinan');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_tabel_perijinan"] = id_tabel_perijinan;
        var url = '<?php echo base_url(); ?>tabel_perijinan/inaktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_tabel_perijinan').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
        $('[id_tabel_perijinan='+id_tabel_perijinan+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_tabel_perijinan').on('click', '#aktifkan', function() {
    var id_tabel_perijinan = $(this).closest('tr').attr('id_tabel_perijinan');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_tabel_perijinan"] = id_tabel_perijinan;
        var url = '<?php echo base_url(); ?>tabel_perijinan/aktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_tabel_perijinan').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
        $('[id_tabel_perijinan='+id_tabel_perijinan+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>