<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Pokja_empat</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>pokja_empat">Home</a></li>
          <li class="breadcrumb-item active">Pokja_empat</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">


  <div class="row" id="awal">
    <div class="col-12">
      <!-- Custom Tabs -->
      <div class="card">
        <div class="card-header p-2">
          <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link active" href="#tab_form_pokja_empat" data-toggle="tab" id="klik_tab_input">Form</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_data_pokja_empat" data-toggle="tab" id="klik_tab_data_pokja_empat">Data</a></li>
            <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>pokja_empat"><i class="fa fa-refresh"></i></a></li>
          </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="tab-content">
            <div class="tab-pane active" id="tab_form_pokja_empat">
              <input name="tabel" id="tabel" value="pokja_empat" type="hidden" value="">
              <form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=pokja_empat" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-6">
                    <input name="page" id="page" value="1" type="hidden" value="">
                    <div class="form-group" style="display:none;">
                      <label for="temp">temp</label>
                      <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="mode">mode</label>
                      <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_pokja_empat">id_pokja_empat</label>
                      <input class="form-control" id="id_pokja_empat" name="id" value="" placeholder="id_pokja_empat" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_propinsi">Provinsi</label>
                      <select class="form-control" id="id_propinsi" name="id_propinsi">
                        <option value="1">Jawa Tengah</option>
                      </select>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_kabupaten">Kabupaten</label>
                      <select class="form-control" id="id_kabupaten" name="id_kabupaten">
                        <option value="1">Wonosobo</option>
                      </select>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_kecamatan">Kecamatan</label>
                      <div class="overlay" id="overlay_id_kecamatan" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                      <select class="form-control" id="id_kecamatan" name="id_kecamatan">
                      </select>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-5">
                          <label for="id_desa">Desa</label>
                        </div>
                        <div class="col-md-7">
                          <div class="overlay" id="overlay_id_desa" style="display:none;">
                            <i class="fa fa-refresh fa-spin"></i>
                          </div>
                          <select class="form-control" id="id_desa" name="id_desa">
                            <option value="0">Pilih Desa</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_dusun">Dusun</label>
                      <div class="overlay" id="overlay_id_dusun" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                      <select class="form-control" id="id_dusun" name="id_dusun">
                        <option value="0">Pilih Dusun</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="tahun">tahun</label>
                        </div>
                        <div class="col-md-5">
                          <select class="form-control" id="tahun" name="tahun">
                            <option value="0000">Pilih tahun</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_posyandu">jumlah_kader_posyandu</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_posyandu" name="jumlah_kader_posyandu" value="" placeholder="jumlah_kader_posyandu" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_gizi">jumlah_kader_gizi</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_gizi" name="jumlah_kader_gizi" value="" placeholder="jumlah_kader_gizi" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_kesling">jumlah_kader_kesling</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_kesling" name="jumlah_kader_kesling" value="" placeholder="jumlah_kader_kesling" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_penyuluhannarkoba">jumlah_kader_penyuluhannarkoba</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_penyuluhannarkoba" name="jumlah_kader_penyuluhannarkoba" value="" placeholder="jumlah_kader_penyuluhannarkoba" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_phbs">jumlah_kader_phbs</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_phbs" name="jumlah_kader_phbs" value="" placeholder="jumlah_kader_phbs" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_kb">jumlah_kader_kb</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_kb" name="jumlah_kader_kb" value="" placeholder="jumlah_kader_kb" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kesehatan_posyandu_jumlah">jumlah_kesehatan_posyandu_jumlah</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kesehatan_posyandu_jumlah" name="jumlah_kesehatan_posyandu_jumlah" value="" placeholder="jumlah_kesehatan_posyandu_jumlah" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kesehatan_posyandu_terintegrasi">jumlah_kesehatan_posyandu_terintegrasi</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kesehatan_posyandu_terintegrasi" name="jumlah_kesehatan_posyandu_terintegrasi" value="" placeholder="jumlah_kesehatan_posyandu_terintegrasi" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kesehatan_lansia_kelompok">jumlah_kesehatan_lansia_kelompok</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kesehatan_lansia_kelompok" name="jumlah_kesehatan_lansia_kelompok" value="" placeholder="jumlah_kesehatan_lansia_kelompok" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kesehatan_lansia_anggota">jumlah_kesehatan_lansia_anggota</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kesehatan_lansia_anggota" name="jumlah_kesehatan_lansia_anggota" value="" placeholder="jumlah_kesehatan_lansia_anggota" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kesehatan_lansia_memilikikartu">jumlah_kesehatan_lansia_memilikikartu</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kesehatan_lansia_memilikikartu" name="jumlah_kesehatan_lansia_memilikikartu" value="" placeholder="jumlah_kesehatan_lansia_memilikikartu" type="text">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kelestarian_rumahberjamban">jumlah_kelestarian_rumahberjamban</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kelestarian_rumahberjamban" name="jumlah_kelestarian_rumahberjamban" value="" placeholder="jumlah_kelestarian_rumahberjamban" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kelestarian_rumahberspal">jumlah_kelestarian_rumahberspal</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kelestarian_rumahberspal" name="jumlah_kelestarian_rumahberspal" value="" placeholder="jumlah_kelestarian_rumahberspal" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kelestarian_rumahtmpsampah">jumlah_kelestarian_rumahtmpsampah</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kelestarian_rumahtmpsampah" name="jumlah_kelestarian_rumahtmpsampah" value="" placeholder="jumlah_kelestarian_rumahtmpsampah" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_mck">jumlah_mck</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_mck" name="jumlah_mck" value="" placeholder="jumlah_mck" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_krtmemiliki_pdam">jumlah_krtmemiliki_pdam</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_krtmemiliki_pdam" name="jumlah_krtmemiliki_pdam" value="" placeholder="jumlah_krtmemiliki_pdam" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_krtmemiliki_sumur">jumlah_krtmemiliki_sumur</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_krtmemiliki_sumur" name="jumlah_krtmemiliki_sumur" value="" placeholder="jumlah_krtmemiliki_sumur" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_krtmemiliki_lain">jumlah_krtmemiliki_lain</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_krtmemiliki_lain" name="jumlah_krtmemiliki_lain" value="" placeholder="jumlah_krtmemiliki_lain" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_perencanaansehat_pus">jumlah_perencanaansehat_pus</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_perencanaansehat_pus" name="jumlah_perencanaansehat_pus" value="" placeholder="jumlah_perencanaansehat_pus" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_perencanaansehat_wus">jumlah_perencanaansehat_wus</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_perencanaansehat_wus" name="jumlah_perencanaansehat_wus" value="" placeholder="jumlah_perencanaansehat_wus" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_perencanaansehat_asebtorkbl">jumlah_perencanaansehat_asebtorkbl</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_perencanaansehat_asebtorkbl" name="jumlah_perencanaansehat_asebtorkbl" value="" placeholder="jumlah_perencanaansehat_asebtorkbl" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_perencanaansehat_asebtorkbp">jumlah_perencanaansehat_asebtorkbp</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_perencanaansehat_asebtorkbp" name="jumlah_perencanaansehat_asebtorkbp" value="" placeholder="jumlah_perencanaansehat_asebtorkbp" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_perencanaansehat_kkmemilikitabungan">jumlah_perencanaansehat_kkmemilikitabungan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_perencanaansehat_kkmemilikitabungan" name="jumlah_perencanaansehat_kkmemilikitabungan" value="" placeholder="jumlah_perencanaansehat_kkmemilikitabungan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="keterangan">keterangan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="keterangan" name="keterangan" value="" placeholder="keterangan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary" id="simpan_pokja_empat">SIMPAN DATA KELUARGA</button>
                      <button type="submit" class="btn btn-primary" id="update_pokja_empat" style="display:none;">UPDATE DATA KELUARGA</button>
                      <a class="btn text-primary" href="<?php echo base_url(); ?>pokja_empat"><i class="fa fa-refresh"></i></a>
                    </div>
                  </div>
                </div>
              </form>

              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
            <div class="tab-pane table-responsive" id="tab_data_pokja_empat">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">&nbsp;</h3>
                  <div class="card-tools">
                    <div class="input-group input-group-sm">
                      <select class="form-control input-sm pull-right" id="keyword" name="keyword" style="width: 150px;">
                        <option value="2022">2022</option>
                        <option value="2021">2021</option>
                        <option value="2020">2020</option>
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                        <option value="2017">2017</option>
                        <option value="2016">2016</option>
                      </select>
                      <select name="limit" class="form-control input-sm pull-right" style="display:none;width: 150px;" id="limit">
                        <option value="999999999">Semua</option>
                        <option value="1">1 Per-Halaman</option>
                        <option value="10">10 Per-Halaman</option>
                        <option value="50">50 Per-Halaman</option>
                        <option value="100">100 Per-Halaman</option>
                      </select>
                      <select name="orderby" class="form-control input-sm pull-right" style="display:none; width: 150px;" id="orderby">
                        <option value="pokja_empat.tahun">tahun</option>
                      </select>
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" id="tampilkan_data_pokja_empat"><i class="fa fa-search"></i> Tampil</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-body table-responsive p-0">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th rowspan="4">NO</th>
                        <th rowspan="4">NAMA WILAYAH</th>
                        <th colspan="6">JUMLAH<br> KADER</th>
                        <th colspan="5">KESEHATAN</th>
                        <th colspan="7">KELESTARIAN LINGKUNGAN HIDUP</th>
                        <th colspan="5">PERENCANAAN<br> SEHAT</th>
                        <th rowspan="4">KET</th>
                        <th rowspan="4"></th>
                      </tr>
                      <tr>
                        <th rowspan="3">POSYANDU</th>
                        <th rowspan="3">GIZI</th>
                        <th rowspan="3">KESLING</th>
                        <th rowspan="3">PENYULUHAN NARKOBA</th>
                        <th rowspan="3">PHBS</th>
                        <th rowspan="3">KB</th>
                        <th colspan="5">POSYANDU</th>
                        <th colspan="3">JUMLAH RUMAH<br> YANG MEMILIKI</th>
                        <th rowspan="3">JUMLAH MCK</th>
                        <th colspan="3">JUMLAH KRT YANG MENGGUNAKAN AIR</th>
                        <th rowspan="3">JUMLAH PUS</th>
                        <th rowspan="3">JUMLAH WUS</th>
                        <th colspan="2">JUMLAH AKSEPTOR KB</th>
                        <th rowspan="3">JML KK YG MEMILIKI TABUNGAN KELUARGA</th>
                      </tr>
                      <tr>
                        <th rowspan="2">JUMLAH</th>
                        <th rowspan="2">TERINTEGRASI</th>
                        <th colspan="3">LANSIA</th>
                        <th rowspan="2">JAMBAN</th>
                        <th rowspan="2">SPAL</th>
                        <th rowspan="2">TEMPAT<br> PEMBUANGAN<br> SAMPAH</th>
                        <th rowspan="2">PDAM</th>
                        <th rowspan="2">SUMUR</th>
                        <th rowspan="2">LAIN-LAIN</th>
                        <th rowspan="2">L</th>
                        <th rowspan="2">P</th>
                      </tr>
                      <tr>
                        <th>JML KLP</th>
                        <th>JML<br>ANGGOTA</th>
                        <th>JML YG<br>MEMILIKI KARTU BEROBAT GRATIS</th>
                      </tr>
                    </thead>
                    <tbody id="tbl_data_pokja_empat">
                    </tbody>
                  </table>
                </div>
                <div class="card-footer">
                  <ul class="pagination pagination-sm m-0 float-right" id="pagination" style="display:none;">
                  </ul>
                  <div class="overlay" id="spinners_tbl_data_pokja_empat" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.tab-content -->
          </div><!-- /.card-body -->
        </div>
        <!-- ./card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->


</section>


<script type="text/javascript">
  $(document).ready(function() {
    load_option_desa();
  });
</script>
<script>
  function load_option_desa() {
    $('#id_desa').html('');
    $('#overlay_id_desa').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>pokja_empat/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html('<option value="0">Pilih Desa</option><option value="9999">TP PKK Kecamatan</option>' + html + '');
        $('#overlay_id_desa').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $('#tabel').val();
  });
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit: limit,
        keyword: keyword,
        orderby: orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>pokja_empat/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_' + tabel + '').html('');
    $('#spinners_tbl_data_' + tabel + '').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page: page,
        limit: limit,
        keyword: keyword,
        orderby: orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>' + tabel + '/json_all_' + tabel + '/',
      success: function(html) {
        // $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
        $('#tbl_data_' + tabel + '').html(html);
        $('#spinners_tbl_data_' + tabel + '').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page = parseInt(id) + 1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_' + tabel + '').on('click', function(e) {
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_' + tabel + '').on('click', function(e) {
      load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedPokja_empat() {
    $('#id_pokja_empat, #id_dusun, #rt, #tahun, #id_desa, #id_kecamatan, #id_kabupaten, #jumlah_kader_posyandu, #jumlah_kader_gizi, #jumlah_kader_kesling, #jumlah_kader_penyuluhannarkoba, #jumlah_kader_phbs, #jumlah_kader_kb, #jumlah_kesehatan_posyandu_jumlah, #jumlah_kesehatan_posyandu_terintegrasi, #jumlah_kesehatan_lansia_kelompok, #jumlah_kesehatan_lansia_anggota, #jumlah_kesehatan_lansia_memilikikartu, #jumlah_kelestarian_rumahberjamban, #jumlah_kelestarian_rumahberspal, #jumlah_kelestarian_rumahtmpsampah, #jumlah_mck, #jumlah_krtmemiliki_pdam, #jumlah_krtmemiliki_sumur, #jumlah_krtmemiliki_lain, #jumlah_perencanaansehat_pus, #jumlah_perencanaansehat_wus, #jumlah_perencanaansehat_asebtorkbl, #jumlah_perencanaansehat_asebtorkbp, #jumlah_perencanaansehat_kkmemilikitabungan, #keterangan').val('');
    $('#tbl_attachment_pokja_empat').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#temp').val(Math.random());
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_pokja_empat').on('click', function(e) {
      e.preventDefault();
      var parameter = ['id_desa', 'temp', 'jumlah_kader_posyandu'];
      InputValid(parameter);

      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["tahun"] = $("#tahun").val();
      parameter["jumlah_kader_posyandu"] = $("#jumlah_kader_posyandu").val();
      parameter["jumlah_kader_gizi"] = $("#jumlah_kader_gizi").val();
      parameter["jumlah_kader_kesling"] = $("#jumlah_kader_kesling").val();
      parameter["jumlah_kader_penyuluhannarkoba"] = $("#jumlah_kader_penyuluhannarkoba").val();
      parameter["jumlah_kader_phbs"] = $("#jumlah_kader_phbs").val();
      parameter["jumlah_kader_kb"] = $("#jumlah_kader_kb").val();
      parameter["jumlah_kesehatan_posyandu_jumlah"] = $("#jumlah_kesehatan_posyandu_jumlah").val();
      parameter["jumlah_kesehatan_posyandu_terintegrasi"] = $("#jumlah_kesehatan_posyandu_terintegrasi").val();
      parameter["jumlah_kesehatan_lansia_kelompok"] = $("#jumlah_kesehatan_lansia_kelompok").val();
      parameter["jumlah_kesehatan_lansia_anggota"] = $("#jumlah_kesehatan_lansia_anggota").val();
      parameter["jumlah_kesehatan_lansia_memilikikartu"] = $("#jumlah_kesehatan_lansia_memilikikartu").val();
      parameter["jumlah_kelestarian_rumahberjamban"] = $("#jumlah_kelestarian_rumahberjamban").val();
      parameter["jumlah_kelestarian_rumahberspal"] = $("#jumlah_kelestarian_rumahberspal").val();
      parameter["jumlah_kelestarian_rumahtmpsampah"] = $("#jumlah_kelestarian_rumahtmpsampah").val();
      parameter["jumlah_mck"] = $("#jumlah_mck").val();
      parameter["jumlah_krtmemiliki_pdam"] = $("#jumlah_krtmemiliki_pdam").val();
      parameter["jumlah_krtmemiliki_sumur"] = $("#jumlah_krtmemiliki_sumur").val();
      parameter["jumlah_krtmemiliki_lain"] = $("#jumlah_krtmemiliki_lain").val();
      parameter["jumlah_perencanaansehat_pus"] = $("#jumlah_perencanaansehat_pus").val();
      parameter["jumlah_perencanaansehat_wus"] = $("#jumlah_perencanaansehat_wus").val();
      parameter["jumlah_perencanaansehat_asebtorkbl"] = $("#jumlah_perencanaansehat_asebtorkbl").val();
      parameter["jumlah_perencanaansehat_asebtorkbp"] = $("#jumlah_perencanaansehat_asebtorkbp").val();
      parameter["jumlah_perencanaansehat_kkmemilikitabungan"] = $("#jumlah_perencanaansehat_kkmemilikitabungan").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>pokja_empat/simpan_pokja_empat';

      var parameterRv = ['id_desa', 'temp', 'jumlah_kader_posyandu'];
      var Rv = RequiredValid(parameterRv);
      if (Rv == 0) {
        alertify.error('Mohon data diisi secara lengkap');
      } else {
        SimpanData(parameter, url);
        AfterSavedPokja_empat();
      }
      $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_data_pokja_empat').on('click', '.update_id_pokja_empat', function() {
      $('#mode').val('edit');
      $('#simpan_pokja_empat').hide();
      $('#update_pokja_empat').show();
      $('#overlay_data_anggota_keluarga').show();
      $('#tbl_data_anggota_keluarga').html('');
      var id_pokja_empat = $(this).closest('tr').attr('id_pokja_empat');
      var mode = $('#mode').val();
      var value = $(this).closest('tr').attr('id_pokja_empat');
      $('#form_baru').show();
      $('#judul_formulir').html('FORMULIR EDIT');
      $('#id_pokja_empat').val(id_pokja_empat);
      $.ajax({
        type: 'POST',
        async: true,
        data: {
          id_pokja_empat: id_pokja_empat
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>pokja_empat/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_propinsi').val(json[i].id_propinsi);
            $('#id_kabupaten').val(json[i].id_kabupaten);
            $('#id_kecamatan').val(json[i].id_kecamatan);
            $('#id_desa').val(json[i].id_desa);
            $('#id_dusun').val(json[i].id_dusun);
            $('#rt').val(json[i].rt);
            $('#tahun').val(json[i].tahun);
            $('#jumlah_kader_posyandu').val(json[i].jumlah_kader_posyandu);
            $('#jumlah_kader_gizi').val(json[i].jumlah_kader_gizi);
            $('#jumlah_kader_kesling').val(json[i].jumlah_kader_kesling);
            $('#jumlah_kader_penyuluhannarkoba').val(json[i].jumlah_kader_penyuluhannarkoba);
            $('#jumlah_kader_phbs').val(json[i].jumlah_kader_phbs);
            $('#jumlah_kader_kb').val(json[i].jumlah_kader_kb);
            $('#jumlah_kesehatan_posyandu_jumlah').val(json[i].jumlah_kesehatan_posyandu_jumlah);
            $('#jumlah_kesehatan_posyandu_terintegrasi').val(json[i].jumlah_kesehatan_posyandu_terintegrasi);
            $('#jumlah_kesehatan_lansia_kelompok').val(json[i].jumlah_kesehatan_lansia_kelompok);
            $('#jumlah_kesehatan_lansia_anggota').val(json[i].jumlah_kesehatan_lansia_anggota);
            $('#jumlah_kesehatan_lansia_memilikikartu').val(json[i].jumlah_kesehatan_lansia_memilikikartu);
            $('#jumlah_kelestarian_rumahberjamban').val(json[i].jumlah_kelestarian_rumahberjamban);
            $('#jumlah_kelestarian_rumahberspal').val(json[i].jumlah_kelestarian_rumahberspal);
            $('#jumlah_kelestarian_rumahtmpsampah').val(json[i].jumlah_kelestarian_rumahtmpsampah);
            $('#jumlah_mck').val(json[i].jumlah_mck);
            $('#jumlah_krtmemiliki_pdam').val(json[i].jumlah_krtmemiliki_pdam);
            $('#jumlah_krtmemiliki_sumur').val(json[i].jumlah_krtmemiliki_sumur);
            $('#jumlah_krtmemiliki_lain').val(json[i].jumlah_krtmemiliki_lain);
            $('#jumlah_perencanaansehat_pus').val(json[i].jumlah_perencanaansehat_pus);
            $('#jumlah_perencanaansehat_wus').val(json[i].jumlah_perencanaansehat_wus);
            $('#jumlah_perencanaansehat_asebtorkbl').val(json[i].jumlah_perencanaansehat_asebtorkbl);
            $('#jumlah_perencanaansehat_asebtorkbp').val(json[i].jumlah_perencanaansehat_asebtorkbp);
            $('#jumlah_perencanaansehat_kkmemilikitabungan').val(json[i].jumlah_perencanaansehat_kkmemilikitabungan);
            $('#keterangan').val(json[i].keterangan);
            load_option_desa();
          }
        }
      });
      //AttachmentByMode(mode, value);
      //AfterSavedAnggota_keluarga(mode, value);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_pokja_empat').on('click', function(e) {
      e.preventDefault();
      var parameter = ['id_pokja_empat', 'id_desa', 'temp', 'jumlah_kader_pkbn'];
      InputValid(parameter);

      var parameter = {}
      parameter["id_pokja_empat"] = $("#id_pokja_empat").val();
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["tahun"] = $("#tahun").val();
      parameter["jumlah_kader_posyandu"] = $("#jumlah_kader_posyandu").val();
      parameter["jumlah_kader_gizi"] = $("#jumlah_kader_gizi").val();
      parameter["jumlah_kader_kesling"] = $("#jumlah_kader_kesling").val();
      parameter["jumlah_kader_penyuluhannarkoba"] = $("#jumlah_kader_penyuluhannarkoba").val();
      parameter["jumlah_kader_phbs"] = $("#jumlah_kader_phbs").val();
      parameter["jumlah_kader_kb"] = $("#jumlah_kader_kb").val();
      parameter["jumlah_kesehatan_posyandu_jumlah"] = $("#jumlah_kesehatan_posyandu_jumlah").val();
      parameter["jumlah_kesehatan_posyandu_terintegrasi"] = $("#jumlah_kesehatan_posyandu_terintegrasi").val();
      parameter["jumlah_kesehatan_lansia_kelompok"] = $("#jumlah_kesehatan_lansia_kelompok").val();
      parameter["jumlah_kesehatan_lansia_anggota"] = $("#jumlah_kesehatan_lansia_anggota").val();
      parameter["jumlah_kesehatan_lansia_memilikikartu"] = $("#jumlah_kesehatan_lansia_memilikikartu").val();
      parameter["jumlah_kelestarian_rumahberjamban"] = $("#jumlah_kelestarian_rumahberjamban").val();
      parameter["jumlah_kelestarian_rumahberspal"] = $("#jumlah_kelestarian_rumahberspal").val();
      parameter["jumlah_kelestarian_rumahtmpsampah"] = $("#jumlah_kelestarian_rumahtmpsampah").val();
      parameter["jumlah_mck"] = $("#jumlah_mck").val();
      parameter["jumlah_krtmemiliki_pdam"] = $("#jumlah_krtmemiliki_pdam").val();
      parameter["jumlah_krtmemiliki_sumur"] = $("#jumlah_krtmemiliki_sumur").val();
      parameter["jumlah_krtmemiliki_lain"] = $("#jumlah_krtmemiliki_lain").val();
      parameter["jumlah_perencanaansehat_pus"] = $("#jumlah_perencanaansehat_pus").val();
      parameter["jumlah_perencanaansehat_wus"] = $("#jumlah_perencanaansehat_wus").val();
      parameter["jumlah_perencanaansehat_asebtorkbl"] = $("#jumlah_perencanaansehat_asebtorkbl").val();
      parameter["jumlah_perencanaansehat_asebtorkbp"] = $("#jumlah_perencanaansehat_asebtorkbp").val();
      parameter["jumlah_perencanaansehat_kkmemilikitabungan"] = $("#jumlah_perencanaansehat_kkmemilikitabungan").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>pokja_empat/update_pokja_empat';

      var parameterRv = ['id_pokja_empat', 'id_desa', 'temp', 'jumlah_kader_pkbn'];
      var Rv = RequiredValid(parameterRv);
      if (Rv == 0) {
        alertify.error('Mohon data diisi secara lengkap');
      } else {
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_data_pokja_empat').on('click', '#del_ajax_pokja_empat', function() {
      var id_pokja_empat = $(this).closest('tr').attr('id_pokja_empat');
      alertify.confirm('Anda yakin data akan dihapus?', function(e) {
        if (e) {
          var parameter = {}
          parameter["id_pokja_empat"] = id_pokja_empat;
          var url = '<?php echo base_url(); ?>pokja_empat/hapus/';
          HapusData(parameter, url);
          $('[id_pokja_empat=' + id_pokja_empat + ']').remove();
        } else {
          alertify.error('Hapus data dibatalkan');
        }
        $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
        load_data(tabel);
      });
    });
  });
</script>