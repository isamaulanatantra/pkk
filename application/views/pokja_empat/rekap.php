
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="display:none;">Pokja_empat</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>pokja_empat">Home</a></li>
              <li class="breadcrumb-item active">Pokja_empat</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
										<input name="tabel" id="tabel" value="pokja_empat" type="hidden" value="">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title"><i class="fa fa-th"></i> DATA KEGIATAN POKJA IV</h3>
												<div class="card-tools" style="display:none;">
													<div class="input-group input-group-sm">
                            <select class="form-control input-sm pull-right" id="keyword" name="keyword" style="width: 150px;">
                              <option value="2018">2018</option>
                              <option value="2019">2019</option>
                              <option value="2017">2017</option>
                              <option value="2016">2016</option>
                            </select>
														<select name="limit" class="form-control input-sm pull-right" style="display:none;width: 150px;" id="limit">
															<option value="999999999">Semua</option>
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="display:none; width: 150px;" id="orderby">
															<option value="pokja_empat.tahun">tahun</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_pokja_empat"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<thead>
                            <tr>
                              <th rowspan="4">NO</th>
                              <th rowspan="4">NAMA WILAYAH</th>
                              <th colspan="6">JUMLAH<br>  KADER</th>
                              <th colspan="5">KESEHATAN</th>
                              <th colspan="7">KELESTARIAN LINGKUNGAN HIDUP</th>
                              <th colspan="5">PERENCANAAN<br>  SEHAT</th>
                              <th rowspan="4">KET</th>
                            </tr>
                            <tr>
                              <th rowspan="3">POSYANDU</th>
                              <th rowspan="3">GIZI</th>
                              <th rowspan="3">KESLING</th>
                              <th rowspan="3">PENYULUHAN NARKOBA</th>
                              <th rowspan="3">PHBS</th>
                              <th rowspan="3">KB</th>
                              <th colspan="5">POSYANDU</th>
                              <th colspan="3">JUMLAH RUMAH<br> YANG MEMILIKI</th>
                              <th rowspan="3">JUMLAH MCK</th>
                              <th colspan="3">JUMLAH KRT YANG MENGGUNAKAN AIR</th>
                              <th rowspan="3">JUMLAH PUS</th>
                              <th rowspan="3">JUMLAH WUS</th>
                              <th colspan="2">JUMLAH AKSEPTOR KB</th>
                              <th rowspan="3">JML KK YG MEMILIKI TABUNGAN KELUARGA</th>
                            </tr>
                            <tr>
                              <th rowspan="2">JUMLAH</th>
                              <th rowspan="2">TERINTEGRASI</th>
                              <th colspan="3">LANSIA</th>
                              <th rowspan="2">JAMBAN</th>
                              <th rowspan="2">SPAL</th>
                              <th rowspan="2">TEMPAT<br> PEMBUANGAN<br> SAMPAH</th>
                              <th rowspan="2">PDAM</th>
                              <th rowspan="2">SUMUR</th>
                              <th rowspan="2">LAIN-LAIN</th>
                              <th rowspan="2">L</th>
                              <th rowspan="2">P</th>
                            </tr>
                            <tr>
                              <th>JML KLP</th>
                              <th>JML<br>ANGGOTA</th>
                              <th>JML YG<br>MEMILIKI KARTU BEROBAT GRATIS</th>
                            </tr>
													</thead>
													<tbody id="tbl_data_pokja_empat">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination" style="display:none;">
												</ul>
												<div class="overlay" id="spinners_tbl_data_pokja_empat" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                	</div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_rekap_'+tabel+'/',
      success: function(html) {
        // $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
      }
    });
  }
</script>
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    load_data(tabel);
  });
</script>