
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Posting Website OPD</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="konten_posting" class="table table-bordered table-striped table-hover">
                	<thead>
					  <tr>
						<th rowspan="2">Nama OPD</th>
						<th colspan="4">Tentang Kami</th>
						<th rowspan="2">Berita</th>
						<th colspan="7">Transparansi Anggaran</th>
						<th rowspan="2">Pengumuman</th>
						<th colspan="3">PPID</th>
						<th colspan="2">Layanan</th>
					  </tr>
					  <tr>
						<td>Profil</td>
						<td>Visi Misi</td>
						<td>Tupoksi</td>
						<td>Struktur Organisasi</td>
						<td>DPA</td>
						<td>LRA</td>
						<td>CALK</td>
						<td>Rentra OPD</td>
						<td>Rekap Prog/ Keg</td>
						<td>Renja</td>
						<td>POBL</td>
						<td>DIP</td>
						<td>SK PPID</td>
						<td>Struktur PPID</td>
						<td>Pengaduan Masyarakat</td>
						<td>Permohonan Informasi Publik</td>
					  </tr>
                	</thead>
                <tbody>
				<?php
				$aw = $this->db->query("
				SELECT * FROM data_skpd WHERE status = 1
				");
				foreach($aw->result() as $ah){
				  $web=$ah->skpd_website;
				  $w = $this->db->query("
				  SELECT *, (select users.user_name from users where users.id_users=posting.created_by) as oleh 
				  from posting
				  where posting.status = 1 
				  and posting.domain='".$web."'
				  order by posting.created_time desc
				  limit 1
				  ");
				  foreach($w->result() as $h)
				  {
					$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
					$yummy   = array("_","","","","","","","","","","","","","");
					echo '
					  <tr>
						<td><a target="_blank" href="https://'.$web.'">'.$ah->skpd_nama.'</a></td>
						<td>';
						  $w1 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Profil'
						  limit 1
						  ");
						  foreach($w1->result() as $h1)
						  {
							  $url1 = str_replace($healthy, $yummy, $h1->judul_posting);
							  echo'<a target="_blank" href="https://'.$h1->domain.'/postings/detail/'.$h1->id_posting.'/'.$url1.'.HTML">';
							  $isi_posting = $h1->isi_posting;
							  if($isi_posting=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
						  }
					  	echo'</td>
						<td>';
						  $w2 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Visi Dan Misi'
						  limit 1
						  ");
						  foreach($w2->result() as $h2)
						  {
							  $url2 = str_replace($healthy, $yummy, $h2->judul_posting);
							  echo'<a target="_blank" href="https://'.$h2->domain.'/postings/detail/'.$h2->id_posting.'/'.$url2.'.HTML">';
							  $isi_posting2 = $h2->isi_posting;
							  if($isi_posting2=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
						  }
					  	echo'</td>
						<td>';
						  $w3 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Tupoksi'
						  limit 1
						  ");
						  foreach($w3->result() as $h3)
						  {
							  $url3 = str_replace($healthy, $yummy, $h3->judul_posting);
							  echo'<a target="_blank" href="https://'.$h3->domain.'/postings/detail/'.$h3->id_posting.'/'.$url3.'.HTML">';
							  $isi_posting3 = $h3->isi_posting;
							  if($isi_posting3=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting3=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting3==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting3=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting3=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting3=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting3=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
						  }
						  $w31 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Tugas Pokok dan Fungsi'
						  limit 1
						  ");
						  foreach($w31->result() as $h31)
						  {
							  $url31 = str_replace($healthy, $yummy, $h31->judul_posting);
							  echo'<a target="_blank" href="https://'.$h31->domain.'/postings/detail/'.$h31->id_posting.'/'.$url31.'.HTML">';
							  $isi_posting31 = $h31->isi_posting;
							  if($isi_posting31=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting31=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting31==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting31=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting31=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting31=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting31=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
						  }
					  	echo'</td>
						<td>';
						  $w4 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Struktur'
						  limit 1
						  ");
						  foreach($w4->result() as $h4)
						  {
							  $url4 = str_replace($healthy, $yummy, $h4->judul_posting);
							  echo'<a target="_blank" href="https://'.$h4->domain.'/postings/detail/'.$h4->id_posting.'/'.$url4.'.HTML">';
							  $isi_posting4 = $h4->isi_posting;
							  if($isi_posting4=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting4=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting4==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting4=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting4=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting4=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting4=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
						  }
					  	echo'</td>
						<td>';
						  $w5 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Berita'
						  order by posting.created_time desc
						  limit 1
						  ");
						  foreach($w5->result() as $h5)
						  {
							  $url5 = str_replace($healthy, $yummy, $h5->judul_posting);
							  echo'<a target="_blank" href="https://'.$h5->domain.'/postings/detail/'.$h5->id_posting.'/'.$url5.'.HTML">';
							  echo''.$h5->created_time.'<br />';
							  echo'</a>';
							  $w51 = $this->db->query("
							  SELECT *
							  from posting
							  where posting.parent='".$h5->id_posting."'
							  order by posting.created_time desc
							  ");
							  echo'Jumlah: '.$w51->num_rows().'';
						  }
					  	echo'</td>
						<td>';
						  $w5 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'DPA'
						  limit 1
						  ");
						  foreach($w5->result() as $h5)
						  {
							  $url5 = str_replace($healthy, $yummy, $h5->judul_posting);
							  echo'<a target="_blank" href="https://'.$h5->domain.'/postings/detail/'.$h5->id_posting.'/'.$url5.'.HTML">';
							  $isi_posting5 = $h5->isi_posting;
							  if($isi_posting5=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting5=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting5==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting5=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting5=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting5=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting5=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
							  $w51 = $this->db->query("
							  SELECT *
							  from posting
							  where posting.parent='".$h5->id_posting."'
							  ");
							  $no=0;
							  foreach($w51->result() as $h51)
							  {
								  $no=$no+1;
								  $url51 = str_replace($healthy, $yummy, $h51->judul_posting);
								  echo'<br /><a target="_blank" href="https://'.$h51->domain.'/postings/detail/'.$h51->id_posting.'/'.$url51.'.HTML">';
								  echo ''.$no.'. '.$h51->judul_posting.'';
								  echo'</a>';
							  }
						  }
					  	echo'</td>
						<td>';
						  $w6 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'LRA'
						  limit 1
						  ");
						  foreach($w6->result() as $h6)
						  {
							  $url6 = str_replace($healthy, $yummy, $h6->judul_posting);
							  echo'<a target="_blank" href="https://'.$h6->domain.'/postings/detail/'.$h5->id_posting.'/'.$url6.'.HTML">';
							  $isi_posting6 = $h6->isi_posting;
							  if($isi_posting6=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
							  $w61 = $this->db->query("
							  SELECT *
							  from posting
							  where posting.parent='".$h6->id_posting."'
							  ");
							  $no=0;
							  foreach($w61->result() as $h61)
							  {
								  $no=$no+1;
								  $url61 = str_replace($healthy, $yummy, $h61->judul_posting);
								  echo'<br /><a target="_blank" href="https://'.$h61->domain.'/postings/detail/'.$h61->id_posting.'/'.$url61.'.HTML">';
								  echo ''.$no.'. '.$h61->judul_posting.'';
								  echo'</a>';
							  }
						  }
					  	echo'</td>
						<td>';
						  $w7 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'CALK'
						  limit 1
						  ");
						  foreach($w7->result() as $h7)
						  {
							  $url7 = str_replace($healthy, $yummy, $h7->judul_posting);
							  echo'<a target="_blank" href="https://'.$h7->domain.'/postings/detail/'.$h5->id_posting.'/'.$url7.'.HTML">';
							  $isi_posting6 = $h7->isi_posting;
							  if($isi_posting6=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
							  $w71 = $this->db->query("
							  SELECT *
							  from posting
							  where posting.parent='".$h7->id_posting."'
							  ");
							  $no=0;
							  foreach($w71->result() as $h71)
							  {
								  $no=$no+1;
								  $url71 = str_replace($healthy, $yummy, $h71->judul_posting);
								  echo'<br /><a target="_blank" href="https://'.$h71->domain.'/postings/detail/'.$h71->id_posting.'/'.$url71.'.HTML">';
								  echo ''.$no.'. '.$h71->judul_posting.'';
								  echo'</a>';
							  }
						  }
					  	echo'</td>
						<td>';
						  $w8 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Renstra'
						  limit 1
						  ");
						  foreach($w8->result() as $h8)
						  {
							  $url7 = str_replace($healthy, $yummy, $h8->judul_posting);
							  echo'<a target="_blank" href="https://'.$h8->domain.'/postings/detail/'.$h5->id_posting.'/'.$url7.'.HTML">';
							  $isi_posting6 = $h8->isi_posting;
							  if($isi_posting6=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
							  $w81 = $this->db->query("
							  SELECT *
							  from posting
							  where posting.parent='".$h8->id_posting."'
							  ");
							  $no=0;
							  foreach($w81->result() as $h81)
							  {
								  $no=$no+1;
								  $url71 = str_replace($healthy, $yummy, $h81->judul_posting);
								  echo'<br /><a target="_blank" href="https://'.$h81->domain.'/postings/detail/'.$h81->id_posting.'/'.$url71.'.HTML">';
								  echo ''.$no.'. '.$h81->judul_posting.'';
								  echo'</a>';
							  }
						  }
					  	echo'</td>
						<td>';
						  $w9 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Rekap Program Kegiatan'
						  limit 1
						  ");
						  foreach($w9->result() as $h9)
						  {
							  $url7 = str_replace($healthy, $yummy, $h9->judul_posting);
							  echo'<a target="_blank" href="https://'.$h9->domain.'/postings/detail/'.$h5->id_posting.'/'.$url7.'.HTML">';
							  $isi_posting6 = $h9->isi_posting;
							  if($isi_posting6=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
							  $w91 = $this->db->query("
							  SELECT *
							  from posting
							  where posting.parent='".$h9->id_posting."'
							  ");
							  $no=0;
							  foreach($w91->result() as $h91)
							  {
								  $no=$no+1;
								  $url71 = str_replace($healthy, $yummy, $h91->judul_posting);
								  echo'<br /><a target="_blank" href="https://'.$h91->domain.'/postings/detail/'.$h91->id_posting.'/'.$url71.'.HTML">';
								  echo ''.$no.'. '.$h91->judul_posting.'';
								  echo'</a>';
							  }
						  }
					  	echo'</td>
						<td>';
						  $w10 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Renja'
						  limit 1
						  ");
						  foreach($w10->result() as $h10)
						  {
							  $url7 = str_replace($healthy, $yummy, $h10->judul_posting);
							  echo'<a target="_blank" href="https://'.$h10->domain.'/postings/detail/'.$h5->id_posting.'/'.$url7.'.HTML">';
							  $isi_posting6 = $h10->isi_posting;
							  if($isi_posting6=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
							  $w101 = $this->db->query("
							  SELECT *
							  from posting
							  where posting.parent='".$h10->id_posting."'
							  ");
							  $no=0;
							  foreach($w101->result() as $h101)
							  {
								  $no=$no+1;
								  $url71 = str_replace($healthy, $yummy, $h101->judul_posting);
								  echo'<br /><a target="_blank" href="https://'.$h101->domain.'/postings/detail/'.$h101->id_posting.'/'.$url71.'.HTML">';
								  echo ''.$no.'. '.$h101->judul_posting.'';
								  echo'</a>';
							  }
						  }
					  	echo'</td>
						<td>';
						  $w11 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'POBL'
						  limit 1
						  ");
						  foreach($w11->result() as $h11)
						  {
							  $url7 = str_replace($healthy, $yummy, $h11->judul_posting);
							  echo'<a target="_blank" href="https://'.$h11->domain.'/postings/detail/'.$h5->id_posting.'/'.$url7.'.HTML">';
							  $isi_posting6 = $h11->isi_posting;
							  if($isi_posting6=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting6=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
							  $w111 = $this->db->query("
							  SELECT *
							  from posting
							  where posting.parent='".$h11->id_posting."'
							  "); 
							  $no=0;
							  foreach($w111->result() as $h111)
							  {
								  $no=$no+1;
								  $url71 = str_replace($healthy, $yummy, $h111->judul_posting);
								  echo'<br /><a target="_blank" href="https://'.$h111->domain.'/postings/detail/'.$h111->id_posting.'/'.$url71.'.HTML">';
								  echo ''.$no.'. '.$h111->judul_posting.'';
								  echo'</a>';
							  }
						  }
					  	echo'</td>
						<td>';
						  $w12 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Pengumuman'
						  limit 1
						  ");
						  foreach($w12->result() as $h12)
						  {
							  $url7 = str_replace($healthy, $yummy, $h12->judul_posting);
							  $w121 = $this->db->query("
							  SELECT *
							  from posting
							  where posting.parent='".$h12->id_posting."'
							  "); 
							  $no=0;
							  echo'<i class="fa fa-check text-green"><br /> Jumlah: '.$w121->num_rows().'';
						  }
					  	echo'</td>
						<td>';
						  $w12 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Daftar Informasi Publik'
						  limit 1
						  ");
						  foreach($w12->result() as $h12)
						  {
							  $url7 = str_replace($healthy, $yummy, $h12->judul_posting);
							  echo'<a target="_blank" href="https://'.$h12->domain.'/postings/detail/'.$h5->id_posting.'/'.$url7.'.HTML">';
							  echo'<i class="fa fa-check text-green">';
							  echo'</a>';
						  }
					  	echo'</td>
						<td>';
						  $w2 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'SK PPID'
						  limit 1
						  ");
						  foreach($w2->result() as $h2)
						  {
							  $url2 = str_replace($healthy, $yummy, $h2->judul_posting);
							  echo'<a target="_blank" href="https://'.$h2->domain.'/postings/detail/'.$h2->id_posting.'/'.$url2.'.HTML">';
							  $isi_posting2 = $h2->isi_posting;
							  if($isi_posting2=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
						  }
					  	echo'</td>
						<td>';
						  $w2 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Struktur PPID'
						  limit 1
						  ");
						  foreach($w2->result() as $h2)
						  {
							  $url2 = str_replace($healthy, $yummy, $h2->judul_posting);
							  echo'<a target="_blank" href="https://'.$h2->domain.'/postings/detail/'.$h2->id_posting.'/'.$url2.'.HTML">';
							  $isi_posting2 = $h2->isi_posting;
							  if($isi_posting2=='Isikan deskripsi disini'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='-'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2==''){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>Isikan deskripsi disini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>-</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>Ditulis di sini</p>'){echo'<i class="fa fa-close text-red">';}
							  elseif($isi_posting2=='<p>Visi Misi</p>'){echo'<i class="fa fa-close text-red">';}
							  else{echo'<i class="fa fa-check text-green">';}
							  echo'</a>';
						  }
					  	echo'</td>
						<td>';
						  $w2 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Pengaduan Masyarakat'
						  limit 1
						  ");
						  foreach($w2->result() as $h2)
						  {
							  $url2 = str_replace($healthy, $yummy, $h2->judul_posting);
							  echo'<a target="_blank" href="https://'.$h2->domain.'/postings/detail/'.$h2->id_posting.'/'.$url2.'.HTML">';
							  echo'<a target="_blank" href="https://'.$h2->domain.'/postings/detail/'.$h2->id_posting.'/'.$url2.'.HTML">';
							  echo'<i class="fa fa-check text-green">';
							  echo'</a>';
						  }
					  	echo'</td>
						<td>';
						  $w2 = $this->db->query("
						  SELECT *
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  and judul_posting = 'Permohonan Informasi'
						  limit 1
						  ");
						  foreach($w2->result() as $h2)
						  {
							  $url2 = str_replace($healthy, $yummy, $h2->judul_posting);
							  echo'<a target="_blank" href="https://'.$h2->domain.'/postings/detail/'.$h2->id_posting.'/'.$url2.'.HTML">';
							  echo'<i class="fa fa-check text-green">';
							  echo'</a>';
						  }
					  	echo'</td>
					  </tr>
					';
				  }
				}
				?>
                </tbody>
			  </table>
		  </div>
	  </div>
<!-- page script -->
<script>
  $(function () {
    $('#konten_posting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
