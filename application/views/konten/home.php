
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Posting Website OPD</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="konten_posting" class="table table-bordered table-striped table-hover">
                <thead>
                <tr>
				  <th>Created Time</th>
				  <th>Domain</th>
				  <th>Judul Posting</th>
				  <th>Oleh</th>
                </tr>
                </thead>
                <tbody>
					<?php
						$aw = $this->db->query("
						SELECT * FROM dasar_website WHERE status = 1
						");
						foreach($aw->result() as $ah){
						  $web=$ah->domain;
						  $w = $this->db->query("
						  SELECT *, (select users.user_name from users where users.id_users=posting.created_by) as oleh 
						  from posting
						  where posting.status = 1 
						  and posting.domain='".$web."'
						  order by posting.created_time desc
						  limit 1
						  ");
						  foreach($w->result() as $h)
						  {
							echo '
								<tr>
								  <td>'.$h->created_time.'</td>
								  <td>'.$h->domain.'</td>
								  <td><a target="_blank" href="https://'.$h->domain.'/postings/detail/'.$h->id_posting.'/'.str_replace(' ', '_', $h->judul_posting ).'.HTML">'.$h->judul_posting.'</a></td>
								  <td>'.$h->oleh.'</td>
								</tr>
							';
						  }
						}
					?>
                </tbody>
			  </table>
		  </div>
	  </div>
<!-- page script -->
<script>
  $(function () {
    $('#konten_posting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
