<?php
    $web=$this->uut->namadomain(base_url());
?>
      <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Pengaduan Masyarakat</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Pengaduan Masyarakat</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
      </section>
<section class="content" id="awal">
		<!-- Custom Tabs -->
		<div class="card">
      <div class="card-header p-2">
        <ul class="nav nav-pills">
          <li class="nav-item active"><a class="nav-link" href="#tab_data_pengaduan_masyarakat" data-toggle="tab" id="klik_tab_data_pengaduan_masyarakat">Data</a></li>
        </ul>
      </div>
			<div class="card-body">
        <div class="tab-content">
          <div class="tab-pane active" id="tab_data_pengaduan_masyarakat">
                <div class="row">
                  <div class="col-md-12">
                    <div class="card-body">
                      <table id="main_table" class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 1%" id="gt">
                                    #
                                </th>
                                <th style="width: 20%">
                                    Nama
                                </th>
                                <th style="width: 10%">
                                    Domain
                                </th>
                                <th style="width: 10%">
                                    No HP
                                </th>
                                <th style="width: 10%">
                                    E-mail
                                </th>
                                <th style="width: 10%">
                                    Tgl Pengaduan
                                </th>
                                <th>
                                    Rincian informasi yang diminta
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tabel_list_profil">
                        </tbody>
                      </table>
                      <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                      </ul>
                    </div>
                  </div>
                </div>
          </div>
          <!-- /.tab-pane -->
        </div>
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
</section>

<script type="text/javascript">
  $(document).ready(function() {
      load_data();
  });
</script>

<script type="text/javascript">
function load_data() { 
  var tabel = null;
  var no = 1;
  tabel = $('#main_table').DataTable({        
    dom: 'Blfrtip',
    buttons: [
      {
          text: 'Export',
          extend: 'collection',
          className: 'custom-html-collection',
          buttons: [
              'copy',
              'pdf',
              'csv',
              'excel',
              'print'
          ]
      }
    ],
    "retrieve": true,
    "destroy": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, // Set true agar bisa di sorting
    "order": [[ 0, 'asc' ]], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
    "ajax":
    {
        "url": "<?php echo base_url('pengaduan_masyarakat/view') ?>", // URL file untuk proses select datanya
        "type": "POST"
    },
    "deferRender": true,
    "aLengthMenu": [[10, 50, 100],[ ' 10',' 50',' 100']], // Combobox Limit          
    "columns": [
      {"data": 'id_permohonan_informasi_publik',"sortable": false, 
          render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
          }  
      },
      { "data": "nama" }, // Tampilkan nama
      { "render": function ( data, type, row ) { // Tampilkan nama_perpustakaan
          var html  = '<a href="https://'+row.domain+'/pengaduan_masyarakat/detail/'+row.id_permohonan_informasi_publik+'" target="_Blank">'+row.domain+'</a>'
          return html
        }
      },
      { "data": "nomor_telp" }, // Tampilkan nomor_telp
      { "data": "email" }, // Tampilkan email
      { "data": "created_time" }, // Tampilkan created_time
      { "data": "rincian_informasi_yang_diinginkan" }, // Tampilkan rincian_informasi_yang_diinginkan
    ],
  });
}
</script>