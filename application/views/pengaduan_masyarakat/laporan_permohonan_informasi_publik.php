<?php
    $web=$this->uut->namadomain(base_url());
?>
      <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Permohonan Informasi Publik</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active">Permohonan Informasi Publik</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
      </section>
<section class="content" id="awal">
		<!-- Custom Tabs -->
		<div class="card">
      <div class="card-header p-2">
        <ul class="nav nav-pills">
          <li class="nav-item active"><a class="nav-link" href="#tab_data_permohonan_informasi_publik" data-toggle="tab" id="klik_tab_data_permohonan_informasi_publik">Data</a></li>
        </ul>
      </div>
			<div class="card-body">
        <div class="tab-content">
          <div class="tab-pane active" id="tab_data_permohonan_informasi_publik">
                <div class="row">
                  <div class="col-md-12">
                    <div class="card-body">
                      <table id="main_table" class="table table-striped projects">
                        <thead>
                            <tr>
                                <th style="width: 1%" id="gt">
                                    #
                                </th>
                                <th style="width: 20%">
                                    Nama Pemohon
                                </th>
                                <th style="width: 10%">
                                    Domain
                                </th>
                                <th style="width: 10%">
                                    No HP
                                </th>
                                <th style="width: 10%">
                                    Tgl Permohonan
                                </th>
                                <th>
                                    Rincian informasi yang diminta
                                </th>
                            </tr>
                        </thead>
                        <tbody id="tabel_list_profil">
                        </tbody>
                      </table>
                      <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                      </ul>
                    </div>
                      <!--<div class="card-tools">
                        <div class="input-group">
                          <input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
                          <select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
                            <option value="10">10 Per-Halaman</option>
                            <option value="50">50 Per-Halaman</option>
                            <option value="100">100 </option>
                            <option value="999999999">Tampilkan Semua</option>
                          </select>
                          <select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
                            <option value="permohonan_informasi_publik.created_time">Tanggal Permohonan</option>
                          </select>
                          <div class="input-group-btn">
                            <button class="btn btn-sm btn-default" id="tampilkan_data_permohonan_informasi_publik"><i class="fa fa-search"></i> Tampil</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer table-responsive no-padding">
                      <table class="table table-bordered table-hover">
                        <thead class="bg-gray">
                          <tr>
                            <th>NO/Kode</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>No HP</th>
                            <th>Rincian informasi yang diminta</th>
                          </tr>
                        </thead>
                        <tbody id="tbl_data_permohonan_informasi_publik">
                        </tbody>
                      </table>
                      <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                      </ul>
                      <div class="overlay" id="spinners_data" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                    </div>-->
                  </div>
                </div>
          </div>
          <!-- /.tab-pane -->
        </div>
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
</section>

<script type="text/javascript">
  $(document).ready(function() {
      load_data();
  });
</script>

<script type="text/javascript">
function load_data() { 
  var tabel = null;
  var no = 1;
  tabel = $('#main_table').DataTable({        
    dom: 'Blfrtip',
    buttons: [
      {
          text: 'Export',
          extend: 'collection',
          className: 'custom-html-collection',
          buttons: [
              'copy',
              'pdf',
              'csv',
              'excel',
              'print'
          ]
      }
    ],
    "retrieve": true,
    "destroy": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, // Set true agar bisa di sorting
    "order": [[ 0, 'asc' ]], // Default sortingnya berdasarkan kolom / field ke 0 (paling pertama)
    "ajax":
    {
        "url": "<?php echo base_url('permohonan_informasi_publik/view') ?>", // URL file untuk proses select datanya
        "type": "POST"
    },
    "deferRender": true,
    "aLengthMenu": [[10, 50, 100],[ ' 10',' 50',' 100']], // Combobox Limit          
    "columns": [
      {"data": 'id_permohonan_informasi_publik',"sortable": false, 
          render: function (data, type, row, meta) {
              return meta.row + meta.settings._iDisplayStart + 1;
          }  
      },
      { "data": "nama" }, // Tampilkan nama
      { "render": function ( data, type, row ) { // Tampilkan nama_perpustakaan
          var html  = '<a href="https://'+row.domain+'/permohonan_informasi_publik/detail/'+row.id_permohonan_informasi_publik+'" target="_Blank">'+row.domain+'</a>'
          return html
        }
      },
      { "data": "nomor_telp" }, // Tampilkan nomor_telp
      { "data": "created_time" }, // Tampilkan created_time
      { "data": "rincian_informasi_yang_diinginkan" }, // Tampilkan rincian_informasi_yang_diinginkan
    ],
  });
}
</script>