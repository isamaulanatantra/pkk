<?php
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
?>
		<section class="content" id="awal">
			<div class="nav nav-pills">
					<ul class="nav nav-tabs">
						<li class="nav-item"><a class="nav-link" href="#tab_1" data-toggle="tab" id="klik_tab_tampil">Data</a></li>
						<li class="nav-item"><a class="nav-link bg-success" href="#tab_2" data-toggle="tab" id="klik_tab_input">Formulir Baru</a></li>
					</ul>
			</div>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_1">
					<table>
						<tbody id="tbl_utama_pengaduan_masyarakat">
						</tbody>
					</table>
					<table>
						<tbody id="tbl_utama_pengaduan_masyarakat_forward">
						</tbody>
					</table>
				</div>
				<div class="tab-pane" id="tab_2">
            <div class="card">
              <div class="card-body">
								<form role="form" id="form_isian" method="post" action="<?php echo base_url();?>lampiran_pengaduan_masyarakat/upload/?table_name=permohonan_informasi_publik" enctype="multipart/form-data">
									<div class="box-body">
										<div class="form-group" style="display:none;">
											<label for="temp">temp</label>
											<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="mode">mode</label>
											<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_permohonan_informasi_publik">id_permohonan_informasi_publik</label>
											<input class="form-control" id="id_permohonan_informasi_publik" name="id" value="" placeholder="id_permohonan_informasi_publik" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_posting">id_posting</label>
											<input class="form-control" id="id_posting" name="id_posting" value="0" placeholder="id_posting" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="created_by">created_by</label>
											<input class="form-control" id="created_by" name="created_by" value="0" placeholder="created_by" type="text">
										</div>
								<?php
								if(!$ses) {
								echo'
										<div class="form-group">
											<label for="nama">Nama</label>
											<input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
										</div>
										<div class="form-group">
											<label for="alamat">Alamat</label>
											<input class="form-control" id="alamat" name="alamat" value="" placeholder="Alamat" type="text">
										</div>
										<div class="form-group">
											<label for="pekerjaan">Pekerjaan</label>
											<input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="Pekerjaan" type="text">
										</div>
										<div class="form-group">
											<label for="nomor_telp">Nomor telp</label>
											<input class="form-control" id="nomor_telp" name="nomor_telp" value="" placeholder="081090909090" type="text">
										</div>
										<div class="form-group">
											<label for="email">Email</label>
											<input class="form-control" id="email" name="email" value="" placeholder="email@mail.id" type="text">
										</div>
										<div class="form-group">
											<label for="rincian_informasi_yang_diinginkan">Rincian informasi yang diinginkan</label>
											<textarea class="form-control" rows="3" id="rincian_informasi_yang_diinginkan" name="rincian_informasi_yang_diinginkan" value="" placeholder="" type="text">
											</textarea>
										</div>
										<div class="form-group">
                      <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link" href="#tab_lampiran" data-toggle="tab"><i class="fa fa-paperclip"></i> Lampiran</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane" id="tab_lampiran">
                          <div class="alert alert-info alert-dismissable">
                            <div class="form-group">
                              <h5 class="text-warning"><i class="icon fa fa-warning"></i>Isi keterangan lampiran terlebih dahulu.</h5>
                              <label for="">Lampiran dapat lebih dari satu, upload satu persatu.</label>
                              <label for="">Dan Mohon Tidak Upload Data yang berisi Identitas Pribadi, Seperti : foto NIK/KK/SIM dll .</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="remake">Keterangan Lampiran </label>
                            <input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran" type="text">
                          </div>
                          <div class="form-group">
                            <label for="myfile">File Lampiran </label>
                            <input type="file" size="60" name="myfile" id="file_lampiran" >
                          </div>
                          <div class="form-group">
                            <div id="ProgresUpload">
                              <div id="BarProgresUpload"></div>
                              <div id="PersenProgresUpload">0%</div >
                            </div>
                            <div id="PesanProgresUpload"></div>
                          </div>
                          <div class="alert alert-info alert-dismissable table-responsive">
                            <h3 class="card-title">Data Lampiran </h3>
                            <table class="table table-bordered">
                              <tr>
                                <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                              </tr>
                              <tbody id="tbl_attachment_pengaduan_masyarakat">
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
										</div>
								';
								}else{
									echo'
										<div class="form-group" style="display:none;">
											<label for="nama">Nama</label>
											<input class="form-control" id="nama" name="nama" value="'.$this->session->userdata('nama').'" placeholder="Nama" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="alamat">Alamat</label>
											<input class="form-control" id="alamat" name="alamat" value="-" placeholder="Alamat" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="pekerjaan">Pekerjaan</label>
											<input class="form-control" id="pekerjaan" name="pekerjaan" value="-" placeholder="Pekerjaan" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="nomor_telp">Nomor telp</label>
											<input class="form-control" id="nomor_telp" name="nomor_telp" value="-" placeholder="081090909090" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="email">Email</label>
											<input class="form-control" id="email" name="email" value="-" placeholder="email@mail.id" type="text">
										</div>
										<div class="form-group">
											<label for="rincian_informasi_yang_diinginkan">Rincian informasi yang diinginkan</label>
											<textarea class="form-control" rows="3" id="rincian_informasi_yang_diinginkan" name="rincian_informasi_yang_diinginkan" value="" placeholder="" type="text">
											</textarea>
										</div>
										<div class="form-group">
                      <ul class="nav nav-pills">
                        <li class="nav-item"><a class="nav-link" href="#tab_lampiran" data-toggle="tab"><i class="fa fa-paperclip"></i> Lampiran</a></li>
                      </ul>
                      <div class="tab-content">
                        <div class="tab-pane" id="tab_lampiran">
                          <div class="alert alert-info alert-dismissable">
                            <div class="form-group">
                              <h5 class="text-warning"><i class="icon fa fa-warning"></i>Isi keterangan lampiran terlebih dahulu.</h5>
                              <label for="">Lampiran dapat lebih dari satu, upload satu persatu.</label>
                              <label for="">Dan Mohon Tidak Upload Data yang berisi Identitas Pribadi, Seperti : foto NIK/KK/SIM dll .</label>
                            </div>
                          </div>
                          <div class="form-group">
                            <label for="remake">Keterangan Lampiran </label>
                            <input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran" type="text">
                          </div>
                          <div class="form-group">
                            <label for="myfile">File Lampiran </label>
                            <input type="file" size="60" name="myfile" id="file_lampiran" >
                          </div>
                          <div class="form-group">
                            <div id="ProgresUpload">
                              <div id="BarProgresUpload"></div>
                              <div id="PersenProgresUpload">0%</div >
                            </div>
                            <div id="PesanProgresUpload"></div>
                          </div>
                          <div class="alert alert-info alert-dismissable table-responsive">
                            <h3 class="card-title">Data Lampiran </h3>
                            <table class="table table-bordered">
                              <tr>
                                <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                              </tr>
                              <tbody id="tbl_attachment_pengaduan_masyarakat">
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
										</div>
									';
								}
								?>
										<div class="form-group" style="display:none;">
											<label for="tujuan_penggunaan_informasi">Tujuan penggunaan informasi</label>
											<input class="form-control" id="tujuan_penggunaan_informasi" name="tujuan_penggunaan_informasi" value="Pengaduan Masyarakat" placeholder="Tujuan penggunaan informasi" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="kategori_permohonan_informasi_publik">kategori_permohonan_informasi_publik</label>
											<input class="form-control" id="kategori_permohonan_informasi_publik" name="kategori_permohonan_informasi_publik" value="Pengaduan Masyarakat" placeholder="Tujuan penggunaan informasi" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_melihat_membaca_mendengarkan_mencatat">cara memperoleh Informasi <br /> melihat/membaca/mendengarkan/mencatat</label>
											<select class="form-control" id="cara_melihat_membaca_mendengarkan_mencatat" name="cara_melihat_membaca_mendengarkan_mencatat" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_hardcopy_softcopy">Cara Memperoleh Informasi <br /> hardcopy/softcopy</label>
											<select class="form-control" id="cara_hardcopy_softcopy" name="cara_hardcopy_softcopy" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_mengambil_langsung">Cara Mendapatkan Informasi <br /> Mengambil langsung</label>
											<select class="form-control" id="cara_mengambil_langsung" name="cara_mengambil_langsung" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_kurir">Kurir</label>
											<select class="form-control" id="cara_kurir" name="cara_kurir" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_pos">POS</label>
											<select class="form-control" id="cara_pos" name="cara_pos" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_faksimili">Faksimili</label>
											<select class="form-control" id="cara_faksimili" name="cara_faksimili" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_email">Email</label>
											<select class="form-control" id="cara_email" name="cara_email" >
											<option value="1">Ya</option>
											<option value="0">Tidak</option>
											</select>
										</div>
									</div>
									<div class="box-footer">
										<button type="submit" class="btn btn-primary" id="simpan_permohonan_informasi_publik">KIRIM</button>
										<button type="submit" class="btn btn-primary" id="update_permohonan_informasi_publik" style="display:none;">UPDATE</button>
									</div>
								</form>
              </div>
						</div>
						<div class="overlay" id="overlay_form_input" style="display:none;">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
				</div>
			</div>
		</section>
<script>
  function AfterSavedPermohonan_informasi_publik() {
    $('#id_permohonan_informasi_publik, #id_posting,  #nama, #kategori_permohonan_informasi_publik, #alamat, #pekerjaan, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
		$('#tbl_attachment_pengaduan_masyarakat').html('');
    $('#PesanProgresUpload').html('');
	}
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_pengaduan_masyarakat').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'permohonan_informasi_publik',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>lampiran_pengaduan_masyarakat/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_lampiran_pengaduan_masyarakat="'+json[i].id_lampiran_pengaduan_masyarakat+'" id="'+json[i].id_lampiran_pengaduan_masyarakat+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/lampiran_pengaduan_masyarakat/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_attachment"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_pengaduan_masyarakat').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_posting').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_pengaduan_masyarakat').on('click', '#del_attachment', function() {
    var id_lampiran_pengaduan_masyarakat = $(this).closest('tr').attr('id_lampiran_pengaduan_masyarakat');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_lampiran_pengaduan_masyarakat"] = id_lampiran_pengaduan_masyarakat;
        var url = '<?php echo base_url(); ?>lampiran_pengaduan_masyarakat/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_permohonan_informasi_publik').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_lampiran_pengaduan_masyarakat='+id_lampiran_pengaduan_masyarakat+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_tampil').on('click', function(e) {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_data_pengaduan_masyarakat(halaman, limit);
    load_data_pengaduan_masyarakat_forward(halaman, limit);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_permohonan_informasi_publik').show();
    $('#update_permohonan_informasi_publik').hide();
    $('#tbl_attachment_pengaduan_masyarakat').html('');
    $('#id_permohonan_informasi_publik, #id_posting,  #nama, #kategori_permohonan_informasi_publik, #alamat, #pekerjaan, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_permohonan_informasi_publik').on('click', function(e) {
      e.preventDefault();
      $('#overlay_form_input').show();
      $('#pesan_terkirim').show();
      var parameter = [ 'nama', 'alamat', 'pekerjaan', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_posting"] = $("#id_posting").val();
      parameter["nama"] = $("#nama").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      parameter["tampil_menu_atas"] = $("#tampil_menu_atas").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["rincian_informasi_yang_diinginkan"] = $("#rincian_informasi_yang_diinginkan").val();
      parameter["tujuan_penggunaan_informasi"] = $("#tujuan_penggunaan_informasi").val();
      parameter["kategori_permohonan_informasi_publik"] = $("#kategori_permohonan_informasi_publik").val();
      parameter["cara_melihat_membaca_mendengarkan_mencatat"] = $("#cara_melihat_membaca_mendengarkan_mencatat").val();
      parameter["cara_hardcopy_softcopy"] = $("#cara_hardcopy_softcopy").val();
      parameter["cara_mengambil_langsung"] = $("#cara_mengambil_langsung").val();
      parameter["cara_kurir"] = $("#cara_kurir").val();
      parameter["cara_pos"] = $("#cara_pos").val();
      parameter["cara_faksimili"] = $("#cara_faksimili").val();
      parameter["cara_email"] = $("#cara_email").val();
      parameter["temp"] = $("#temp").val();
      parameter["created_by"] = $("#created_by").val();
      var url = '<?php echo base_url(); ?>pengaduan_masyarakat/simpan_pengaduan_masyarakat';
      
      var parameterRv = [ 'nama', 'alamat', 'pekerjaan', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan', ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
				$('#overlay_form_input').fadeOut();
				alert('gagal');
				$('html, body').animate({
					scrollTop: $('#awal').offset().top
				}, 1000);
      }
      else{
        SimpanData(parameter, url);
        AfterSavedPermohonan_informasi_publik();
				$('#overlay_form_input').fadeOut('slow');
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1;
	var limit = 10;
    load_data_pengaduan_masyarakat(halaman, limit);
    load_data_pengaduan_masyarakat_forward(halaman, limit);
});
</script>
<script>
  function load_data_pengaduan_masyarakat(halaman, limit) {
    $('#tbl_utama_pengaduan_masyarakat').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>pengaduan_masyarakat/load_table1/',
      success: function(html) {
        $('#tbl_utama_pengaduan_masyarakat').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
<script>
  function load_data_pengaduan_masyarakat_forward(halaman, limit) {
    $('#tbl_utama_pengaduan_masyarakat_forward').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>pengaduan_masyarakat/load_table_forward/',
      success: function(html) {
        $('#tbl_utama_pengaduan_masyarakat_forward').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_pengaduan_masyarakat').on('click', '#del_ajax', function() {
    var id_permohonan_informasi_publik = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_informasi_publik"] = id_permohonan_informasi_publik;
        var url = '<?php echo base_url(); ?>pengaduan_masyarakat/hapus/?id='+id_permohonan_informasi_publik+'';
        HapusData(parameter, url);
        $('[id_permohonan_informasi_publik='+id_permohonan_informasi_publik+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_pengaduan_masyarakat_forward').on('click', '#del_ajax1', function() {
    var id_permohonan_informasi_publik_forward = $(this).closest('tr').attr('id_permohonan_informasi_publik_forward');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_informasi_publik_forward"] = id_permohonan_informasi_publik_forward;
        var url = '<?php echo base_url(); ?>pengaduan_masyarakat/hapus_forward/?id='+id_permohonan_informasi_publik_forward+'';
        HapusData(parameter, url);
        $('[id_permohonan_informasi_publik_forward='+id_permohonan_informasi_publik_forward+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
