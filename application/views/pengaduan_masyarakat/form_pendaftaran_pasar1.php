<?php
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
?>
		<section class="content">
          <div class="row" id="awal">
            <div class="col-md-12">
              <input name="tabel" id="tabel" value="pendaftaran_pasar" type="hidden" value="">
              <input name="page" id="page" value="1" type="hidden" value="">
              <div class="card card-primary card-outline direct-chat-primary" id="div_form_input" style="display:none;">
                <div class="card-header">
                  <h3 class="card-title">Formulir Pendaftaran Pasar</h3>
                </div>
                <div class="card-body">
                  <form role="form" id="form_isian" method="post" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="form-group" style="display:none;">
                        <label for="temp">temp</label>
                        <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="mode">mode</label>
                        <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="id_pendaftaran_pasar">id_pendaftaran_pasar</label>
                        <input class="form-control" id="id_pendaftaran_pasar" name="id" value="" placeholder="id_pendaftaran_pasar" type="text">
                      </div>
                      <div class="form-group" style="display:none;">
                        <label for="created_by">created_by</label>
                        <input class="form-control" id="created_by" name="created_by" value="0" placeholder="created_by" type="text">
                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-6">
                            <div class="form-group">
                              <label for="tanggal_permohonan">Tanggal Permohonan</label>
                              <input class="form-control" id="tanggal_permohonan" name="tanggal_permohonan" value="" placeholder="Tanggal Permohonan" type="text">
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                              <label for="nama">Nama Lengkap</label>
                              <input class="form-control" id="nama" name="nama" value="" placeholder="Nama Lengkap" type="text">
                            </div>
                          </div>
                          <div class="col-12">
                            <div class="form-group">
                              <label for="alamat">Alamat Pedagang</label>
                              <textarea class="form-control" rows="3" id="alamat" name="alamat" value="" placeholder="" type="text"></textarea>
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                              <label for="nik">Nomor Induk Kependudukan (NIK)</label>
                              <input class="form-control" id="nik" name="nik" value="" placeholder="contoh 33071108099XXXX" type="text">
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                              <label for="no_kk">Nomor Kartu Keluarga (KK)</label>
                              <input class="form-control" id="no_kk" name="no_kk" value="" placeholder="contoh 33071108099XXXX" type="text">
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                              <label for="nomor_telp">Nomor Telp/HP</label>
                              <input class="form-control" id="nomor_telp" name="nomor_telp" value="" placeholder="contoh 081090909090" type="text">
                            </div>
                          </div>
                          <div class="col-6">
                            <div class="form-group">
                              <label for="email">Alamat Email/Surat Elektronik</label>
                              <input class="form-control" id="email" name="email" value="" placeholder="contoh email@mail.id" type="text">
                            </div>
                          </div>
                      </div>
                      <div class="form-group">
                        <div class="alert alert-info alert-dismissable">
                          <div class="form-group">
                            <h5 class="text-warning"><i class="icon fa fa-warning"></i>Lampiran Harus Lengkap.</h5>
                            <label for="">Lampiran di upload satu persatu, file tipe lampiran hanya dapat berupa file pdf/jpg/png dan ukuran maksimal 5 MB</label>
                          </div>
                        </div>
                        <div class="form-group">
                          <div id="ProgresUpload">
                            <div id="BarProgresUpload"></div>
                            <div id="PersenProgresUpload">0%</div >
                          </div>
                          <div id="PesanProgresUpload"></div>
                        </div>
                  </form>
                        <div class="alert alert-info alert-dismissable table-responsive">
                          <h3 class="card-title">Data Lampiran </h3>
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th><th>Keterangan</th><th>Dokumen</th><th>Hapus</th> 
                            </tr>
                            <tbody id="tbl_attachment_pendaftaran_pasar">
                              <tr>
                                <td>1</td><td>Surat pernyataan bahwa pemohon adalah benar merupakan Pedagang Eks Pasar Induk Wonosobo</td><td><form role="form" id="form_lampiran1" method="post" action="<?php echo base_url(); ?>lampiran_pendaftaran_pasar/upload1/?table_name=lampiran_pendaftaran_pasar&id=1" enctype="multipart/form-data"><div id="div_file1"><input class="form-control" id="remake1" name="remake1" value="Surat pernyataan bahwa pemohon adalah benar merupakan Pedagang Eks Pasar Induk Wonosobo" type="hidden"><input class="form-control" id="mode1" name="mode1" value="input" type="hidden"><input class="form-control" id="temp1" name="temp1" value="" type="hidden"><input type="file" size="60" name="myfile1" id="myfile1" ></div></td><td><a href="#" id="del_attachment1"><i class="fa fa-cut"></i></a></form></td>
                              </tr> 
                              <tr>
                                <td>2</td><td>Kartu Tanda Penduduk</td><td><form role="form" id="form_lampiran2" method="post" action="<?php echo base_url(); ?>lampiran_pendaftaran_pasar/upload2/?table_name=lampiran_pendaftaran_pasar&id=2" enctype="multipart/form-data"><div id="div_file2"><input class="form-control" id="remake2" name="remake2" value="Kartu Tanda Penduduk" type="hidden"><input class="form-control" id="mode2" name="mode2" value="input" type="hidden"><input class="form-control" id="temp2" name="temp2" value="" type="hidden"><input type="file" size="60" name="myfile2" id="myfile2" ></div></td><td><a href="#" id="del_attachment2"><i class="fa fa-cut"></i></a></form></td>
                              </tr> 
                              <tr>
                                <td>3</td><td>Kepemilikan hak pakai register lama dalam bentuk SIPTD</td><td><form role="form" id="form_lampiran3" method="post" action="<?php echo base_url(); ?>lampiran_pendaftaran_pasar/upload3/?table_name=lampiran_pendaftaran_pasar&id=3" enctype="multipart/form-data"><div id="div_file3"><input class="form-control" id="remake3" name="remake3" value="Kepemilikan hak pakai register lama dalam bentuk SIPTD" type="hidden"><input class="form-control" id="mode3" name="mode3" value="input" type="hidden"><input class="form-control" id="temp3" name="temp3" value="" type="hidden"><input type="file" size="60" name="myfile3" id="myfile3" ></div></td><td><a href="#" id="del_attachment3"><i class="fa fa-cut"></i></a></form></td>
                              </tr> 
                              <tr>
                                <td>4</td><td>Surat Relokasi</td><td><form role="form" id="form_lampiran4" method="post" action="<?php echo base_url(); ?>lampiran_pendaftaran_pasar/upload4/?table_name=lampiran_pendaftaran_pasar&id=4" enctype="multipart/form-data"><div id="div_file4"><input class="form-control" id="remake4" name="remake4" value="Surat Relokasi" type="hidden"><input class="form-control" id="mode4" name="mode4" value="input" type="hidden"><input class="form-control" id="temp4" name="temp4" value="" type="hidden"><input type="file" size="60" name="myfile4" id="myfile4" ></div></td><td><a href="#" id="del_attachment4"><i class="fa fa-cut"></i></a></form></td>
                              </tr> 
                              <tr>
                                <td>5</td><td>Foto pedagang Eks Pasar Induk Wonosobo</td><td><form role="form" id="form_lampiran5" method="post" action="<?php echo base_url(); ?>lampiran_pendaftaran_pasar/upload5/?table_name=lampiran_pendaftaran_pasar&id=5" enctype="multipart/form-data"><div id="div_file5"><input class="form-control" id="remake5" name="remake5" value="Foto pedagang Eks Pasar Induk Wonosobo" type="hidden"><input class="form-control" id="mode5" name="mode5" value="input" type="hidden"><input class="form-control" id="temp5" name="temp5" value="" type="hidden"><input type="file" size="60" name="myfile5" id="myfile5" ></div></td><td><a href="#" id="del_attachment5"><i class="fa fa-cut"></i></a></form></td>
                              </tr> 
                              <tr>
                                <td>6</td><td>Foto jenis dagangan atau komoditas</td><td><form role="form" id="form_lampiran6" method="post" action="<?php echo base_url(); ?>lampiran_pendaftaran_pasar/upload6/?table_name=lampiran_pendaftaran_pasar&id=6" enctype="multipart/form-data"><div id="div_file6"><input class="form-control" id="remake6" name="remake6" value="Foto jenis dagangan atau komoditas" type="hidden"><input class="form-control" id="mode6" name="mode6" value="input" type="hidden"><input class="form-control" id="temp6" name="temp6" value="" type="hidden"><input type="file" size="60" name="myfile6" id="myfile6" ></div></td><td><a href="#" id="del_attachment6"><i class="fa fa-cut"></i></a></form></td>
                              </tr> 
                            </tbody>
                          </table>
                        </div>
                        <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> Perhatian!</h4>
                        Sebelum disimpan, pastikan semua form sudah terisi dengan benar dan semua lampiran harus diupload.
                        </div>
                      </div>
                    </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" id="simpan_pendaftaran_pasar">KIRIM</button>
                      <button type="submit" class="btn btn-primary" id="update_pendaftaran_pasar" style="display:none;">UPDATE</button>
                    </div>
                </div>
              </div>
              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
						</div>
            <div class="col-md-12">
              <div class="card card-primary card-outline direct-chat-primary">
                  <div class="card-header">
                    <h3 class="card-title">Data Pendaftaran Pasar</h3>
                    <div class="card-tools">
                      <div class="input-group">
                        <input name="keyword" class="form-control input-sm" style="width: 150px; display:none;" placeholder="Search" type="text" id="keyword">
                        <select name="limit" class="form-control input-sm" style="width: 150px; display:none;" id="limit">
                          <option value="25">25 Per-Halaman</option>
                          <option value="50">50 Per-Halaman</option>
                          <option value="100">100 </option>
                          <option value="999999999">Semua</option>
                        </select>
                        <select name="orderby" class="form-control input-sm" style="width: 150px; display:none;" id="orderby">
                          <option value="pendaftaran_pasar.id_pendaftaran_pasar">ID</option>
                        </select>
                        <select class="form-control-sm" style="width: 150px;" id="tahun" name="tahun" >
                          <option value="2022">2022</option>
                          <option value="2021">2021</option>
                          <option value="2020">2020</option>
                          <option value="2019">2019</option>
                          <option value="2018">2018</option>
                          <option value="2017">2017</option>
                          <option value="2016">2016</option>
                          <option value="2015">2015</option>
                        </select>
                        <div class="input-group-btn">
                          <button class="btn btn-sm btn-default" id="tampilkan_data_pendaftaran_pasar"><i class="fa fa-search"></i> Tampil</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="card-body table-responsive">
                    <div class="row">
                      <div class="col-md-12" id="tbl_data_pendaftaran_pasar">
                      </div>
                      <div class="col-md-12" id="tbl_utama_pendaftaran_pasar_forward">
                      </div>
                      <div class="col-md-12">
                        <div class="paging_simple_numbers">
                          <ul class="pagination" id="pagination">
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="overlay" id="spinners_data" style="display:none;">
                      <i class="fa fa-refresh fa-spin"></i>
                    </div>
                  </div>
              </div>
            </div>
          </div>
		</section>
<script>
  function AfterSavedPendaftaran_pasar() {
    $('#id_pendaftaran_pasar, #id_posting, #tanggal_permohonana, #nama, #alamat, #nomor_telp,  #email,  #nik,  #no_kk,  #created_by').val('');
		$('#tbl_attachment_pendaftaran_pasar').html('');
    $('#PesanProgresUpload').html('');
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#div_form_input').show();
  
  load_data_pendaftaran_pasar_forward(1,10);
});
</script>
<script type="text/javascript">
$(document).ready(function() {
	var temp = Math.random();
	$('#temp').val(temp);
	$('#temp1').val(temp);
	$('#temp2').val(temp);
	$('#temp3').val(temp);
	$('#temp4').val(temp);
	$('#temp5').val(temp);
	$('#temp6').val(temp);
});
</script>

<script>
	function AttachmentByMode(value, id) {
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'lampiran_pendaftaran_pasar',
        value:value,
        id:id
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>lampiran_pendaftaran_pasar/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<a href="<?php echo base_url(); ?>media/lampiran_pendaftaran_pasar/'+json[i].file_name+'" target="_blank">Lihat Dokumen</a>';
				}
				$(['#div_file']+id).html(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        value = $('#temp1').val();
        AttachmentByMode(value, '1');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('myfile1').onchange = function() {
        $('#form_lampiran1').submit();
      };
    $('#form_lampiran1').ajaxForm(options);
  });
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        value = $('#temp2').val();
        AttachmentByMode(value, '2');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('myfile2').onchange = function() {
        $('#form_lampiran2').submit();
      };
    $('#form_lampiran2').ajaxForm(options);
  });
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        value = $('#temp3').val();
        AttachmentByMode(value, '3');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('myfile3').onchange = function() {
        $('#form_lampiran3').submit();
      };
    $('#form_lampiran3').ajaxForm(options);
  });
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        value = $('#temp4').val();
        AttachmentByMode(value, '4');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('myfile4').onchange = function() {
        $('#form_lampiran4').submit();
      };
    $('#form_lampiran4').ajaxForm(options);
  });
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        value = $('#temp5').val();
        AttachmentByMode(value, '5');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('myfile5').onchange = function() {
        $('#form_lampiran5').submit();
      };
    $('#form_lampiran5').ajaxForm(options);
  });
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        value = $('#temp6').val();
        AttachmentByMode(value, '6');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('myfile6').onchange = function() {
        $('#form_lampiran6').submit();
      };
    $('#form_lampiran6').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_pendaftaran_pasar').on('click', '#del_attachment', function() {
    var id_lampiran_pendaftaran_pasar = $(this).closest('tr').attr('id_lampiran_pendaftaran_pasar');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_lampiran_pendaftaran_pasar"] = id_lampiran_pendaftaran_pasar;
        var url = '<?php echo base_url(); ?>lampiran_pendaftaran_pasar/hapus/';
        HapusAttachment(parameter, url);
        var value = $('#temp').val();
        AttachmentByMode(value, '1');
        $('[id_lampiran_pendaftaran_pasar='+id_lampiran_pendaftaran_pasar+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_pendaftaran_pasar').on('click', '#del_attachment1', function() {
    var temp = $('#temp').val();
    var id = '1';
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id"] = id;
        parameter["temp"] = temp;
        var url = '<?php echo base_url(); ?>lampiran_pendaftaran_pasar/hapus/';
        HapusAttachment(parameter, url);
        
				$(['#div_file']+id).html('<input class="form-control" id="remake1" name="remake1" value="Surat pernyataan bahwa pemohon adalah benar merupakan Pedagang Eks Pasar Induk Wonosobo" type="hidden"><input class="form-control" id="mode1" name="mode1" value="input" type="hidden"><input class="form-control" id="temp1" name="temp1" value="'+temp+'" type="hidden"><input type="file" size="60" name="myfile1" id="myfile1">');
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_pendaftaran_pasar').on('click', '#del_attachment2', function() {
    var temp = $('#temp').val();
    var id = '2';
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id"] = id;
        parameter["temp"] = temp;
        var url = '<?php echo base_url(); ?>lampiran_pendaftaran_pasar/hapus/';
        HapusAttachment(parameter, url);
        
				$(['#div_file']+id).html('<input class="form-control" id="remake2" name="remake2" value="Kartu Tanda Penduduk" type="hidden"><input class="form-control" id="mode2" name="mode2" value="input" type="hidden"><input class="form-control" id="temp2" name="temp2" value="'+temp+'" type="hidden"><input type="file" size="60" name="myfile2" id="myfile2">');
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_pendaftaran_pasar').on('click', '#del_attachment3', function() {
    var temp = $('#temp').val();
    var id = '3';
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id"] = id;
        parameter["temp"] = temp;
        var url = '<?php echo base_url(); ?>lampiran_pendaftaran_pasar/hapus/';
        HapusAttachment(parameter, url);
        
				$(['#div_file']+id).html('<input class="form-control" id="remake3" name="remake3" value="Kepemilikan hak pakai register lama dalam bentuk SIPTD" type="hidden"><input class="form-control" id="mode3" name="mode3" value="input" type="hidden"><input class="form-control" id="temp3" name="temp3" value="'+temp+'" type="hidden"><input type="file" size="60" name="myfile3" id="myfile3">');
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_pendaftaran_pasar').on('click', '#del_attachment4', function() {
    var temp = $('#temp').val();
    var id = '4';
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id"] = id;
        parameter["temp"] = temp;
        var url = '<?php echo base_url(); ?>lampiran_pendaftaran_pasar/hapus/';
        HapusAttachment(parameter, url);
        
				$(['#div_file']+id).html('<input class="form-control" id="remake4" name="remake4" value="Surat Relokasi" type="hidden"><input class="form-control" id="mode4" name="mode4" value="input" type="hidden"><input class="form-control" id="temp4" name="temp4" value="'+temp+'" type="hidden"><input type="file" size="60" name="myfile4" id="myfile4">');
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_pendaftaran_pasar').on('click', '#del_attachment5', function() {
    var temp = $('#temp').val();
    var id = '5';
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id"] = id;
        parameter["temp"] = temp;
        var url = '<?php echo base_url(); ?>lampiran_pendaftaran_pasar/hapus/';
        HapusAttachment(parameter, url);
        
				$(['#div_file']+id).html('<input class="form-control" id="remake5" name="remake5" value="Foto pedagang Eks Pasar Induk Wonosobo" type="hidden"><input class="form-control" id="mode5" name="mode5" value="input" type="hidden"><input class="form-control" id="temp5" name="temp5" value="'+temp+'" type="hidden"><input type="file" size="60" name="myfile5" id="myfile5">');
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_pendaftaran_pasar').on('click', '#del_attachment6', function() {
    var temp = $('#temp').val();
    var id = '6';
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id"] = id;
        parameter["temp"] = temp;
        var url = '<?php echo base_url(); ?>lampiran_pendaftaran_pasar/hapus/';
        HapusAttachment(parameter, url);
        
				$(['#div_file']+id).html('<input class="form-control" id="remake6" name="remake6" value="Foto jenis dagangan atau komoditas" type="hidden"><input class="form-control" id="mode6" name="mode6" value="input" type="hidden"><input class="form-control" id="temp6" name="temp6" value="'+temp+'" type="hidden"><input type="file" size="60" name="myfile6" id="myfile6">');
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_pendaftaran_pasar').on('click', function(e) {
      e.preventDefault();
      // $('#simpan_pendaftaran_pasar').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
      $('#pesan_terkirim').show();
      var parameter = [ 'tanggal_permohonan', 'nama', 'alamat', 'nomor_telp', 'email', 'nik', 'no_kk'  ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_posting"] = $("#id_posting").val();
      parameter["tanggal_permohonan"] = $("#tanggal_permohonan").val();
      parameter["nama"] = $("#nama").val();
      parameter["nama"] = $("#nama").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["nik"] = $("#nik").val();
      parameter["no_kk"] = $("#no_kk").val();
      parameter["temp"] = $("#temp").val();
      parameter["created_by"] = $("#created_by").val();
      var url = '<?php echo base_url(); ?>pendaftaran_pasar/simpan_pendaftaran_pasar_no';
      
      var parameterRv = [ 'tanggal_permohonan', 'nama', 'alamat', 'nomor_telp', 'email', 'nik', 'no_kk' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
				$('#overlay_form_input').fadeOut();
				alert('gagal');
				$('html, body').animate({
					scrollTop: $('#awal').offset().top
				}, 1000);
      }
      else{
        SimpanData(parameter, url);
        AfterSavedPendaftaran_pasar();
				$('#overlay_form_input').fadeOut('slow');
        $('#div_form_input form').trigger('reset');
        var tabel = $("#tabel").val();
        load_data(tabel);
      }
    });
  });
</script>
<script>
  function load_data_pendaftaran_pasar_forward(halaman, limit) {
    $('#tbl_utama_pendaftaran_pasar_forward').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>pendaftaran_pasar/load_table_forward/',
      success: function(html) {
        $('#tbl_utama_pendaftaran_pasar_forward').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_pendaftaran_pasar').on('click', '#del_ajax', function() {
    var id_pendaftaran_pasar = $(this).closest('div').attr('id_pendaftaran_pasar');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_pendaftaran_pasar"] = id_pendaftaran_pasar;
        var url = '<?php echo base_url(); ?>pendaftaran_pasar/hapus/?id='+id_pendaftaran_pasar+'';
        HapusData(parameter, url);
        $('[id_pendaftaran_pasar='+id_pendaftaran_pasar+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_pendaftaran_pasar_forward').on('click', '#del_ajax1', function() {
    var id_pendaftaran_pasar_forward = $(this).closest('tr').attr('id_pendaftaran_pasar_forward');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_pendaftaran_pasar_forward"] = id_pendaftaran_pasar_forward;
        var url = '<?php echo base_url(); ?>pendaftaran_pasar/hapus_forward/?id='+id_pendaftaran_pasar_forward+'';
        HapusData(parameter, url);
        $('[id_pendaftaran_pasar_forward='+id_pendaftaran_pasar_forward+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_tampil').on('click', function(e) {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_data_pendaftaran_pasar(halaman, limit);
    load_data_pendaftaran_pasar_forward(halaman, limit);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tahun = $('#tahun').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        tahun:tahun
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>pendaftaran_pasar/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li id="' + a + '" page="' + a + '" keyword="' + keyword + '" tahun="' + tahun + '" class="paginate_button page-item "><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tahun = $('#tahun').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        tahun:tahun
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    load_data(tabel);
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_permohonan').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>    <!-- /.content -->