<?php
    $web=$this->uut->namadomain(base_url());
		$ses=$this->session->userdata('id_users');
?>
			<div class="card card-widget">
              <div class="card-header">
                <div class="user-block">
                  <img class="img-circle" src="<?php echo base_url(); ?>assets/images/blank_user.png" alt="User Image">
                  <span class="username"><a href="#"><?php if(!empty($nama)){ echo $nama; } ?></a></span>
                  <span class="description">
					  <?php if(!empty($alamat)){ echo $alamat; } ?> - <?php if(!empty($created_time)){ echo $created_time; } ?>
					  <?php if(!empty($pekerjaan)){ echo $pekerjaan; } ?>
					  <?php if(!empty($nomor_telp)){ echo $nomor_telp; } ?>
					  <?php if(!empty($email)){ echo $email; } ?>
					  
				  </span>
                </div>
                <!-- /.user-block -->
                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Mark as read">
                    <i class="fa fa-circle-o"></i></button>
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
                <!-- /.card-tools -->
              </div>
              <!-- /.card-header -->
              <div class="card-body">
				  
				<?php
				$rul_id_posting = $this->uri->segment(3);
				$where1 = array(
					'id_tabel' => $rul_id_posting,
					'table_name' => 'pengaduan_masyarakat'
					);
				$this->db->where($where1);
				$this->db->order_by('uploaded_time desc');
				$query1 = $this->db->get('attachment');
				if(($query1->num_rows())>0){
					echo'
			  	<div id="'.$nama_halaman.'" class="carousel slide" data-ride="carousel">
                  <ol class="carousel-indicators">
                    <li data-target="#'.$nama_halaman.'" data-slide-to="0" class="active"></li>
                    <li data-target="#'.$nama_halaman.'" data-slide-to="1" class=""></li>
                    <li data-target="#'.$nama_halaman.'" data-slide-to="2" class=""></li>
                  </ol>
                  <div class="carousel-inner">
					';
					$a = 0;
					foreach ($query1->result() as $row1)
						{
							$a = $a+1;
							if( $a == 1 ){
								echo
								'
                    <div class="carousel-item active">
                      <img class="d-block w-100" src="'.base_url().'media/upload/'.$row1->file_name.'" alt="First slide">
											<div class="carousel-caption">
												'.$row1->keterangan.'
											</div>
                    </div>
								';
								}
							else{
								echo
								'
                    <div class="carousel-item">
                      <img class="d-block w-100" src="'.base_url().'media/upload/'.$row1->file_name.'" alt="Second slide">
											<div class="carousel-caption">
												'.$row1->keterangan.'
											</div>
                    </div>
								';
								} 
						}
					echo '
                  </div>
                  <a class="carousel-control-prev" href="#'.$nama_halaman.'" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#'.$nama_halaman.'" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
					';
				}
				?>
				  
                <?php if(!empty($tujuan_penggunaan_informasi)){ echo $tujuan_penggunaan_informasi; } ?>
                <p><?php if(!empty($rincian_informasi_yang_diinginkan)){ echo $rincian_informasi_yang_diinginkan; } ?></p>
				  <?php
					if($web=='ppid.wonosobokab.go.id'){
						if(!$ses) {
						}else{
							echo'
									<a class="btn btn-default btn-sm" href="'.base_url().'permohonan_informasi_publik/forward/'.$rul_id_posting.'" target="_blank"><i class="fa fa-share"></i> Forward</a>
									';
						}
					}
					?>
                <button type="button" class="btn btn-default btn-sm"><i class="fa fa-thumbs-o-up"></i> Like</button>
                <span class="float-right text-muted">0 Suka - <?php echo $jumlah_komentar;?> komentar</span>
              </div>
              <!-- /.card-body -->
              <div class="card-footer card-comments" id="tbl_utama_permohonan_informasi_publik_balasan">
              </div>
              <!-- /.card-footer -->
              <div class="card-footer">
                <h3><u>Formulir Baru</u></h3>
				  <?php
					if(!$ses) { 
					echo'
					<form role="form" id="form_isian" method="post" enctype="multipart/form-data">
						<div class="box-body">
							<div class="form-group" style="display:none;">
								<label for="temp">temp</label>
								<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="mode">mode</label>
								<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="id_permohonan_informasi_publik">id_permohonan_informasi_publik</label>
								<input class="form-control" id="id_permohonan_informasi_publik" name="id" value="" placeholder="id_permohonan_informasi_publik" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="parent">parent</label>
								<input class="form-control" id="parent" name="id" value="'.$this->uri->segment(3).'" placeholder="parent" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="id_posting">id_posting</label>
								<input class="form-control" id="id_posting" name="id_posting" value="0" placeholder="id_posting" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="created_by">created_by</label>
								<input class="form-control" id="created_by" name="created_by" value="0" placeholder="created_by" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="kategori_permohonan_informasi_publik">kategori_permohonan_informasi_publik</label>
								<input class="form-control" id="kategori_permohonan_informasi_publik" name="kategori_permohonan_informasi_publik" value="Permohonan Informasi Publik" placeholder="" type="text">
							</div>
							<div class="form-group">
								<label for="nama">Nama</label>
								<input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
							</div>
							<div class="form-group">
								<label for="alamat">Alamat</label>
								<input class="form-control" id="alamat" name="alamat" value="" placeholder="Alamat" type="text">
							</div>
							<div class="form-group">
								<label for="pekerjaan">Pekerjaan</label>
								<input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="Pekerjaan" type="text">
							</div>
							<div class="form-group">
								<label for="nomor_telp">Nomor telp</label>
								<input class="form-control" id="nomor_telp" name="nomor_telp" value="" placeholder="081090909090" type="text">
							</div>
							<div class="form-group">
								<label for="email">Email</label>
								<input class="form-control" id="email" name="email" value="" placeholder="email@mail.id" type="text">
							</div>
							<div class="form-group">
								<label for="rincian_informasi_yang_diinginkan">Rincian informasi yang diinginkan</label>
								<textarea class="form-control" rows="3" id="rincian_informasi_yang_diinginkan" name="rincian_informasi_yang_diinginkan" value="" placeholder="" type="text">
								</textarea>
							</div>
							<div class="form-group">
								<label for="tujuan_penggunaan_informasi">Tujuan penggunaan informasi</label>
								<input class="form-control" id="tujuan_penggunaan_informasi" name="tujuan_penggunaan_informasi" value="" placeholder="Tujuan penggunaan informasi" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_melihat_membaca_mendengarkan_mencatat">cara memperoleh Informasi <br /> melihat/membaca/mendengarkan/mencatat</label>
								<select class="form-control" id="cara_melihat_membaca_mendengarkan_mencatat" name="cara_melihat_membaca_mendengarkan_mencatat" >
								<option value="1">Ya</option>
								<option value="0">Tidak</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_hardcopy_softcopy">Cara Memperoleh Informasi <br /> hardcopy/softcopy</label>
								<select class="form-control" id="cara_hardcopy_softcopy" name="cara_hardcopy_softcopy" >
								<option value="1">Ya</option>
								<option value="0">Tidak</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_mengambil_langsung">Cara Mendapatkan Informasi <br /> Mengambil langsung</label>
								<select class="form-control" id="cara_mengambil_langsung" name="cara_mengambil_langsung" >
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_kurir">Kurir</label>
								<select class="form-control" id="cara_kurir" name="cara_kurir" >
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_pos">POS</label>
								<select class="form-control" id="cara_pos" name="cara_pos" >
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_faksimili">Faksimili</label>
								<select class="form-control" id="cara_faksimili" name="cara_faksimili" >
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_email">Email</label>
								<select class="form-control" id="cara_email" name="cara_email" >
								<option value="1">Ya</option>
								<option value="0">Tidak</option>
								</select>
							</div>
						</div>
						<div class="box-footer">
							<div class="overlay" id="overlay_form_input" style="display:none;">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							<button type="submit" class="btn btn-primary" id="simpan_permohonan_informasi_publik">KIRIM</button>
							<button type="submit" class="btn btn-primary" id="update_permohonan_informasi_publik" style="display:none;">UPDATE</button>
						</div>
					</form>
					<div class="overlay" id="overlay_form_input" style="display:none;">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
					'; 
					}
					else{
						echo '
					<form role="form" id="form_isian" method="post" action="'.base_url().'attachment/upload/?table_name=permohonan_informasi_publik" enctype="multipart/form-data">
						<div class="box-body">
							<div class="form-group" style="display:none;">
								<label for="temp">temp</label>
								<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="mode">mode</label>
								<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="id_permohonan_informasi_publik">id_permohonan_informasi_publik</label>
								<input class="form-control" id="id_permohonan_informasi_publik" name="id" value="" placeholder="id_permohonan_informasi_publik" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="parent">parent</label>
								<input class="form-control" id="parent" name="id" value="'.$this->uri->segment(3).'" placeholder="parent" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="id_posting">id_posting</label>
								<input class="form-control" id="id_posting" name="id_posting" value="0" placeholder="id_posting" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="created_by">created_by</label>
								<input class="form-control" id="created_by" name="created_by" value="0" placeholder="created_by" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="kategori_permohonan_informasi_publik">kategori_permohonan_informasi_publik</label>
								<input class="form-control" id="kategori_permohonan_informasi_publik" name="kategori_permohonan_informasi_publik" value="Permohonan Informasi Publik" placeholder="" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="nama">Nama</label>
								<input class="form-control" id="nama" name="nama" value="'.$this->session->userdata('nama').'" placeholder="Nama" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="alamat">Alamat</label>
								<input class="form-control" id="alamat" name="alamat" value="-" placeholder="Alamat" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="pekerjaan">Pekerjaan</label>
								<input class="form-control" id="pekerjaan" name="pekerjaan" value="-" placeholder="Pekerjaan" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="nomor_telp">Nomor telp</label>
								<input class="form-control" id="nomor_telp" name="nomor_telp" value="-" placeholder="081090909090" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="email">Email</label>
								<input class="form-control" id="email" name="email" value="-" placeholder="email@mail.id" type="text">
							</div>
							<div class="form-group">
								<label for="rincian_informasi_yang_diinginkan">Rincian informasi yang diinginkan</label>
								<textarea class="form-control" rows="3" id="rincian_informasi_yang_diinginkan" name="rincian_informasi_yang_diinginkan" value="" placeholder="" type="text">
								</textarea>
							</div>
							<div class="form-group" style="display:none;">
								<label for="tujuan_penggunaan_informasi">Tujuan penggunaan informasi</label>
								<input class="form-control" id="tujuan_penggunaan_informasi" name="tujuan_penggunaan_informasi" value="Balas Permohonan Informasi Publik" placeholder="Tujuan penggunaan informasi" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_melihat_membaca_mendengarkan_mencatat">cara memperoleh Informasi <br /> melihat/membaca/mendengarkan/mencatat</label>
								<select class="form-control" id="cara_melihat_membaca_mendengarkan_mencatat" name="cara_melihat_membaca_mendengarkan_mencatat" >
								<option value="1">Ya</option>
								<option value="0">Tidak</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_hardcopy_softcopy">Cara Memperoleh Informasi <br /> hardcopy/softcopy</label>
								<select class="form-control" id="cara_hardcopy_softcopy" name="cara_hardcopy_softcopy" >
								<option value="1">Ya</option>
								<option value="0">Tidak</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_mengambil_langsung">Cara Mendapatkan Informasi <br /> Mengambil langsung</label>
								<select class="form-control" id="cara_mengambil_langsung" name="cara_mengambil_langsung" >
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_kurir">Kurir</label>
								<select class="form-control" id="cara_kurir" name="cara_kurir" >
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_pos">POS</label>
								<select class="form-control" id="cara_pos" name="cara_pos" >
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_faksimili">Faksimili</label>
								<select class="form-control" id="cara_faksimili" name="cara_faksimili" >
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
								</select>
							</div>
							<div class="form-group" style="display:none;">
								<label for="cara_email">Email</label>
								<select class="form-control" id="cara_email" name="cara_email" >
								<option value="1">Ya</option>
								<option value="0">Tidak</option>
								</select>
							</div>
              <div class="form-group">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link" href="#tab_lampiran" data-toggle="tab"><i class="fa fa-paperclip"></i> Lampiran</a></li>
                </ul>
                <div class="tab-content">
                  <div class="tab-pane" id="tab_lampiran">
                    <div class="alert alert-info alert-dismissable">
                      <div class="form-group">
                        <h5 class="text-warning"><i class="icon fa fa-warning"></i>Isi keterangan lampiran terlebih dahulu.</h5>
                        <label for="">Lampiran dapat lebih dari satu, upload satu persatu.</label>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="remake">Keterangan Lampiran </label>
                      <input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran" type="text">
                    </div>
                    <div class="form-group">
                      <label for="myfile">File Lampiran </label>
                      <input type="file" size="60" name="myfile" id="file_lampiran" >
                    </div>
                    <div class="form-group">
                      <div id="ProgresUpload">
                        <div id="BarProgresUpload"></div>
                        <div id="PersenProgresUpload">0%</div >
                      </div>
                      <div id="PesanProgresUpload"></div>
                    </div>
                    <div class="alert alert-info alert-dismissable table-responsive">
                      <h3 class="card-title">Data Lampiran </h3>
                      <table class="table table-bordered">
                        <tr>
                          <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                        </tr>
                        <tbody id="tbl_attachment_permohonan_informasi_publik">
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
						</div>
						<div class="box-footer">
							<div class="overlay" id="overlay_form_input" style="display:none;">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							<button type="submit" class="btn btn-primary" id="simpan_permohonan_informasi_publik">KIRIM</button>
							<button type="submit" class="btn btn-primary" id="update_permohonan_informasi_publik" style="display:none;">UPDATE</button>
						</div>
					</form>
					<div class="overlay" id="overlay_form_input" style="display:none;">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
						';
					} 
				  ?>
				  
              </div>
              <!-- /.card-footer -->
            </div>

<script>
  function AfterSavedPermohonan_informasi_publik() {
    $('#id_permohonan_informasi_publik, #id_posting, #parent, #nama, #kategori_permohonan_informasi_publik, #alamat, #pekerjaan, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_permohonan_informasi_publik').show();
    $('#update_permohonan_informasi_publik').hide();
    $('#tbl_attachment_permohonan_informasi_publik').html('');
    $('#id_permohonan_informasi_publik, #id_posting, #parent,  #nama, #kategori_permohonan_informasi_publik, #alamat, #pekerjaan, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR PERMOHONAN INFORMASI PUBLIK');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_permohonan_informasi_publik').on('click', function(e) {
      e.preventDefault();
      $('#simpan_permohonan_informasi_publik').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
      $('#pesan_terkirim').show();
      var parameter = [ 'id_posting', 'nama', 'parent', 'alamat', 'pekerjaan', 'kategori_permohonan_informasi_publik', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan', 'tujuan_penggunaan_informasi', 'cara_melihat_membaca_mendengarkan_mencatat', 'cara_hardcopy_softcopy', 'cara_mengambil_langsung', 'cara_kurir', 'cara_pos', 'cara_faksimili', 'cara_email', 'created_by' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_posting"] = $("#id_posting").val();
      parameter["parent"] = $("#parent").val();
      parameter["nama"] = $("#nama").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      parameter["tampil_menu_atas"] = $("#tampil_menu_atas").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["rincian_informasi_yang_diinginkan"] = $("#rincian_informasi_yang_diinginkan").val();
      parameter["tujuan_penggunaan_informasi"] = $("#tujuan_penggunaan_informasi").val();
      parameter["cara_melihat_membaca_mendengarkan_mencatat"] = $("#cara_melihat_membaca_mendengarkan_mencatat").val();
      parameter["kategori_permohonan_informasi_publik"] = $("#kategori_permohonan_informasi_publik").val();
      parameter["cara_hardcopy_softcopy"] = $("#cara_hardcopy_softcopy").val();
      parameter["cara_mengambil_langsung"] = $("#cara_mengambil_langsung").val();
      parameter["cara_kurir"] = $("#cara_kurir").val();
      parameter["cara_pos"] = $("#cara_pos").val();
      parameter["cara_faksimili"] = $("#cara_faksimili").val();
      parameter["cara_email"] = $("#cara_email").val();
      parameter["temp"] = $("#temp").val();
      parameter["created_by"] = $("#created_by").val();
      var url = '<?php echo base_url(); ?>permohonan_informasi_publik/simpan_permohonan_informasi_publik_no_balasan';
      
      var parameterRv = [ 'id_posting', 'parent', 'nama', 'alamat', 'kategori_permohonan_informasi_publik', 'pekerjaan', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan', 'tujuan_penggunaan_informasi', 'cara_melihat_membaca_mendengarkan_mencatat', 'cara_hardcopy_softcopy', 'cara_mengambil_langsung', 'cara_kurir', 'cara_pos', 'cara_faksimili', 'cara_email', 'created_by' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
				$('#overlay_form_input').fadeOut();
				alert('gagal');
				$('html, body').animate({
					scrollTop: $('#awal').offset().top
				}, 1000);
      }
      else{
        SimpanData(parameter, url);
        AfterSavedPermohonan_informasi_publik();
				// $('#simpan_permohonan_informasi_publik').removeAttr('disabled', 'disabled');
				$('#overlay_form_input').fadeOut('slow');
		  load_data_permohonan_informasi_publik_balasan();
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
    load_data_permohonan_informasi_publik_balasan();
});
</script>
<script>
  function load_data_permohonan_informasi_publik_balasan() {
    $('#tbl_utama_permohonan_informasi_publik_balasan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_permohonan_informasi_publik: <?php echo $this->uri->segment(3); ?>
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_informasi_publik/load_table_permohonan_informasi_publik_balasan/',
      success: function(html) {
        $('#tbl_utama_permohonan_informasi_publik_balasan').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_permohonan_informasi_publik_balasan').on('click', '#del_ajax', function() {
    var id_permohonan_informasi_publik = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_informasi_publik"] = id_permohonan_informasi_publik;
        var url = '<?php echo base_url(); ?>permohonan_informasi_publik/hapus/?id='+id_permohonan_informasi_publik+'';
        HapusData(parameter, url);
        $('[id_permohonan_informasi_publik='+id_permohonan_informasi_publik+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_permohonan_informasi_publik').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'permohonan_informasi_publik',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_permohonan_informasi_publik').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_permohonan_informasi_publik').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_permohonan_informasi_publik').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_permohonan_informasi_publik').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
