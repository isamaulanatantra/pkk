
			<div class="card card-widget">
				<div class="card-header">
					<h3 class="card-title">FORWARD <?php if(!empty($judul_halaman)){ echo $judul_halaman; } ?></h3>
				</div>
				<div class="card-body">
					<div class="alert alert-warning alert-dismissable" id="loading_input_ganti_password" style="display:none;">
						<h4><i class="fa fa-refresh fa-spin"></i> Mohon tunggu....</h4>
					</div>
					<div class="alert alert-danger alert-dismissable" id="error_input_ganti_password" style="display:none;">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<h4><i class="icon fa fa-ban"></i> Error !</h4>
						<p id="pesan_error"></p>
					</div>
					<form action="" id="formgantipassword" method="post">
						<div class="alert alert-info alert-dismissable">
						<?php
						$id_permohonan_informasi_publik = $this->uri->segment(3);
						$where1 = array(
							'id_permohonan_informasi_publik' => $id_permohonan_informasi_publik
							);
						$this->db->where($where1);
						$this->db->limit(1);
						$query1 = $this->db->get('permohonan_informasi_publik');
							foreach ($query1->result() as $row1)
								{
									echo'<h4><u><i class="fa fa-info"></i> '.$row1->tujuan_penggunaan_informasi.'</u></h4> <p>'.$row1->rincian_informasi_yang_diinginkan.'</p>';
								}
						?>
						</div>
						<div class="form-group">
							<label for="id_skpd">OPD</label>
							<select class="form-control" id="id_skpd" name="id_skpd" >
							</select>
						</div>
						<div class="form-group has-feedback" style="display:none;">
							<input class="form-control" id="id_permohonan_informasi_publik" placeholder="id_permohonan_informasi_publik" type="text" value="<?php echo $this->uri->segment(3); ?>">
							<span class="glyphicon glyphicon-lock form-control-feedback"></span>
						</div>
						
						<div class="row">
							<div class="col-xs-8">    
								<div class="checkbox icheck">
								</div>                        
							</div>
							<div class="col-xs-4">
								<button type="submit" id="simpan_ganti_password_baru" class="btn btn-primary btn-block btn-flat"> Forward</button>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer">
				
				</div>
			</div>

<script>
  function load_id_skpd() {
    $('#id_skpd').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_informasi_publik/option_skpd/',
      success: function(html) {
        $('#id_skpd').html('<option value="">Pilih OPD</option>'+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
  $(document).ready(function () {
		load_id_skpd();
	});
</script>

<script type="text/javascript">
  $(document).ready(function () {
		$("#simpan_ganti_password_baru").on("click", function(e){
		e.preventDefault();
		$('#loading_input_ganti_password').show();
		$('#error_input_ganti_password').hide();
		
		var id_skpd = $("#id_skpd").val();
		var id_permohonan_informasi_publik = $("#id_permohonan_informasi_publik").val();
			$.ajax({
			type: "POST",
			async: true, 
			data: {
					id_skpd:id_skpd,
					id_permohonan_informasi_publik:id_permohonan_informasi_publik
				}, 
			dataType: "json",
			url: '<?php echo base_url(); ?>permohonan_informasi_publik/simpan_forward',   
			success: function(json) {
				for (var i = 0; i < json.length; i++) {
					if(json[i].errors == 'form_kosong')
						{
							$('#loading_login').fadeOut( "slow" );
							$('#error_input_ganti_password').show();
							$('#pesan_error').html('Mohon isi data secara lengkap');
							//validasi input baru
							if(json[i].id_skpd == ''){ $('#id_skpd').css('background-color','#DFB5B4'); } else { $('#id_skpd').removeAttr( 'style' ); }
						}
					else if(json[i].errors == 'Yes')
						{
							$('#loading_login').fadeOut( "slow" );
							$('#error_input_ganti_password').show();
							$('#pesan_error').html('Password lama anda salah');
							//validasi input baru
							if(json[i].id_skpd == ''){ $('#id_skpd').css('background-color','#DFB5B4'); } else { $('#id_skpd').removeAttr( 'style' ); }
						}	
					else if(json[i].errors == 'No')
						{
							$('#formgantipassword').fadeOut( "slow" );
              $('#pesan_error').html('Password berhasil diganti');
            }
					else
						{
							$("#box_form form").trigger( "reset" );
							$("#box_form").fadeOut( "slow" );
							$('#pesan_berhasil').show();
						}
					}
					$('#loading_input_ganti_password').fadeOut("slow");
				}
			});
		});
  });
</script>