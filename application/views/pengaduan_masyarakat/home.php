
            <div class="card" id="awal">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab" id="klik_tab_tampil">Timeline</a></li>
                  <li class="nav-item"><a class="nav-link bg-success" href="#tab1" data-toggle="tab" id="klik_tab_input"><i class="fa fa-plus"></i> Formulir Baru</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
					
                  <div class="active tab-pane" id="timeline">
                    <!-- The timeline -->
                    <ul class="timeline timeline-inverse">
                      	<?php
						$web=$this->uut->namadomain(base_url());
						if($web=='wonosobokab.go.id'){$web_timeline = "";}
						else{$web_timeline = "AND domain='".$web."'";}
						/*for($x=1;$x<=5;$x++){
							echo $x;
						}*/
							$w1 = $this->db->query("
							SELECT *, 
							(SELECT user_name FROM users WHERE users.id_users=permohonan_informasi_publik.created_by) AS nama_pengguna, 
							(SELECT judul_posting FROM posting WHERE posting.id_posting=permohonan_informasi_publik.id_posting) AS judul_posting_permohonan
							FROM permohonan_informasi_publik
							WHERE created_time >= '2018-01-01 00:00:00'
							AND created_time <= '".$hari_ini."'
							AND status = 1
							AND parent = 0
							".$web_timeline."
							ORDER BY created_time desc
							");
							foreach($w1->result() as $h1){
								echo '
								  <li class="time-label">
									<span class="bg-danger">
									'.$h1->domain.'
									</span>
								  </li>
								';
								$tujuan = $h1->tujuan_penggunaan_informasi;
								echo '
								  <li>
								';
								if($tujuan=='Komentar'){
								$costum = 'warning';
								$fa = 'comments';
								}
								elseif($tujuan=='Pengaduan Masyarakat'){
								$costum = 'success';
								$fa = 'user';
								}
								else{
								$costum = 'primary';
								$fa = 'envelope';
								}
								echo '
									<i class="fa fa-'.$fa.' bg-'.$costum.'"></i>
									<div class="timeline-item card">
									  <div class="timeline-header">
									  <div class="user-block">
										<img class="img-circle img-bordered-sm text-'.$costum.'" src="'.base_url().'assets/images/blank_user.png" alt="user image">
										<span class="username">
										  <a href="#">'.$h1->nama.'</a>
										  <a href="#" class="float-right btn-tool"></a>
										</span>
										<span class="description">';
										$ses=$this->session->userdata('id_users');
										if(!$ses) { echo'...'; }
										else{echo ''.$h1->alamat.'';} 
										echo ' - '.$this->Crud_model->dateBahasaIndo1($h1->created_time).'
										</span>
									  </div>
									  </div>

									  <div class="timeline-body">';
										if(!$ses) { echo''; }
										else{
											echo '<p><u>Pekerjaan:</u> '.$h1->pekerjaan.'</p>';
											echo '<p><u>Nomor Telp.:</u> '.$h1->nomor_telp.'</p>';
											echo '<p><u>Email:</u> '.$h1->email.'</p>';
											echo '<p><u>Judul Posting:</u> '.$h1->judul_posting_permohonan.'</p>';
											echo '<p><u>Tujuan:</u> <span class="text-'.$costum.'">'.$h1->tujuan_penggunaan_informasi.'</span></p>';
											echo '<p><u>Website:</u> <a href="https://'.$h1->domain.'/pengaduan_masyarakat" class="text-muted">'.$h1->domain.'</a></p>';
											echo '<p><u>Rincian:</u></p>';
											} 
										echo '
										<p>'.$h1->rincian_informasi_yang_diinginkan.'</p>
									  </div>
									  <div class="timeline-footer card-comment">
										  <p>
											<a href="#" class="link-black text-sm"><i class="fa fa-thumbs-up mr-1 text-'.$costum.'"></i></a>
											<a href="#" class="link-black text-sm mr-2"><i class="fa fa-thumbs-down mr-1 text-'.$costum.'"></i></a>
											<span class="float-right">
											  <a href="#" class="link-black text-sm">
												<i class="fa fa-comments-o mr-1 text-'.$costum.'"></i> Comments';
													$w11 = $this->db->query("
													SELECT *
													FROM permohonan_informasi_publik
													WHERE parent = $h1->id_permohonan_informasi_publik
													");
													$jumlah_comments = $w11->num_rows(); 
												echo ' '.$jumlah_comments.'
											  </a>
											</span>
										  </p>
									  </div>
									  
									</div>
								  </li>
								';
							}
						?>
                      <li>
                        <i class="fa fa-clock-o bg-gray"></i>
                      </li>
                    </ul>
                  </div>
                  
                  <div class="tab-pane" id="tab1">
					  
								<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=permohonan_informasi_publik" enctype="multipart/form-data">
									<div class="box-body">
										<div class="form-group" style="display:none;">
											<label for="temp">temp</label>
											<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="mode">mode</label>
											<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_permohonan_informasi_publik">id_permohonan_informasi_publik</label>
											<input class="form-control" id="id_permohonan_informasi_publik" name="id" value="" placeholder="id_permohonan_informasi_publik" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_posting">id_posting</label>
											<input class="form-control" id="id_posting" name="id_posting" value="0" placeholder="id_posting" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="created_by">created_by</label>
											<input class="form-control" id="created_by" name="created_by" value="0" placeholder="created_by" type="text">
										</div>
										<div class="form-group">
											<label for="nama">Nama</label>
											<input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
										</div>
										<div class="form-group">
											<label for="alamat">Alamat</label>
											<input class="form-control" id="alamat" name="alamat" value="" placeholder="Alamat" type="text">
										</div>
										<div class="form-group">
											<label for="pekerjaan">Pekerjaan</label>
											<input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="Pekerjaan" type="text">
										</div>
										<div class="form-group">
											<label for="nomor_telp">Nomor telp</label>
											<input class="form-control" id="nomor_telp" name="nomor_telp" value="" placeholder="081090909090" type="text">
										</div>
										<div class="form-group">
											<label for="email">Email</label>
											<input class="form-control" id="email" name="email" value="" placeholder="email@mail.id" type="text">
										</div>
										<div class="form-group">
											<label for="rincian_informasi_yang_diinginkan">Rincian informasi yang diinginkan</label>
											<textarea class="form-control" rows="3" id="rincian_informasi_yang_diinginkan" name="rincian_informasi_yang_diinginkan" value="" placeholder="" type="text">
											</textarea>
										</div>
										<div class="form-group" style="display:none;">
											<label for="tujuan_penggunaan_informasi">Tujuan penggunaan informasi</label>
											<input class="form-control" id="tujuan_penggunaan_informasi" name="tujuan_penggunaan_informasi" value="Pengaduan Masyarakat" placeholder="Tujuan penggunaan informasi" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_melihat_membaca_mendengarkan_mencatat">cara memperoleh Informasi <br /> melihat/membaca/mendengarkan/mencatat</label>
											<select class="form-control" id="cara_melihat_membaca_mendengarkan_mencatat" name="cara_melihat_membaca_mendengarkan_mencatat" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_hardcopy_softcopy">Cara Memperoleh Informasi <br /> hardcopy/softcopy</label>
											<select class="form-control" id="cara_hardcopy_softcopy" name="cara_hardcopy_softcopy" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_mengambil_langsung">Cara Mendapatkan Informasi <br /> Mengambil langsung</label>
											<select class="form-control" id="cara_mengambil_langsung" name="cara_mengambil_langsung" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_kurir">Kurir</label>
											<select class="form-control" id="cara_kurir" name="cara_kurir" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_pos">POS</label>
											<select class="form-control" id="cara_pos" name="cara_pos" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_faksimili">Faksimili</label>
											<select class="form-control" id="cara_faksimili" name="cara_faksimili" >
											<option value="0">Tidak</option>
											<option value="1">Ya</option>
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="cara_email">Email</label>
											<select class="form-control" id="cara_email" name="cara_email" >
											<option value="1">Ya</option>
											<option value="0">Tidak</option>
											</select>
										</div>
									</div>
									<div class="box-footer">
										<button type="submit" class="btn btn-primary" id="simpan_permohonan_informasi_publik">KIRIM</button>
										<button type="submit" class="btn btn-primary" id="update_permohonan_informasi_publik" style="display:none;">UPDATE</button>
									</div>
								</form>
					  
					<div class="overlay" id="overlay_form_input" style="display:none;">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				  </div>
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
<script>
  function AfterSavedPermohonan_informasi_publik() {
    $('#id_permohonan_informasi_publik, #id_posting,  #nama, #alamat, #pekerjaan, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_permohonan_informasi_publik').show();
    $('#update_permohonan_informasi_publik').hide();
    $('#tbl_attachment_permohonan_informasi_publik').html('');
    $('#id_permohonan_informasi_publik, #id_posting,  #nama, #alamat, #pekerjaan, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR PERMOHONAN INFORMASI PUBLIK');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_permohonan_informasi_publik').on('click', function(e) {
      e.preventDefault();
      $('#simpan_permohonan_informasi_publik').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
      $('#pesan_terkirim').show();
      var parameter = [ 'id_posting', 'nama', 'alamat', 'pekerjaan', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan', 'tujuan_penggunaan_informasi', 'cara_melihat_membaca_mendengarkan_mencatat', 'cara_hardcopy_softcopy', 'cara_mengambil_langsung', 'cara_kurir', 'cara_pos', 'cara_faksimili', 'cara_email', 'created_by' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_posting"] = $("#id_posting").val();
      parameter["nama"] = $("#nama").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      parameter["tampil_menu_atas"] = $("#tampil_menu_atas").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["rincian_informasi_yang_diinginkan"] = $("#rincian_informasi_yang_diinginkan").val();
      parameter["tujuan_penggunaan_informasi"] = $("#tujuan_penggunaan_informasi").val();
      parameter["cara_melihat_membaca_mendengarkan_mencatat"] = $("#cara_melihat_membaca_mendengarkan_mencatat").val();
      parameter["cara_hardcopy_softcopy"] = $("#cara_hardcopy_softcopy").val();
      parameter["cara_mengambil_langsung"] = $("#cara_mengambil_langsung").val();
      parameter["cara_kurir"] = $("#cara_kurir").val();
      parameter["cara_pos"] = $("#cara_pos").val();
      parameter["cara_faksimili"] = $("#cara_faksimili").val();
      parameter["cara_email"] = $("#cara_email").val();
      parameter["temp"] = $("#temp").val();
      parameter["created_by"] = $("#created_by").val();
      var url = '<?php echo base_url(); ?>pengaduan_masyarakat/simpan_pengaduan_masyarakat';
      
      var parameterRv = [ 'id_posting', 'nama', 'alamat', 'pekerjaan', 'nomor_telp', 'email', 'rincian_informasi_yang_diinginkan', 'tujuan_penggunaan_informasi', 'cara_melihat_membaca_mendengarkan_mencatat', 'cara_hardcopy_softcopy', 'cara_mengambil_langsung', 'cara_kurir', 'cara_pos', 'cara_faksimili', 'cara_email', 'created_by' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
				$('#overlay_form_input').fadeOut();
				alert('gagal');
				$('html, body').animate({
					scrollTop: $('#awal').offset().top
				}, 1000);
      }
      else{
        SimpanData(parameter, url);
        AfterSavedPermohonan_informasi_publik();
				$('#simpan_permohonan_informasi_publik').removeAttr('disabled', 'disabled');
				$('#overlay_form_input').fadeOut('slow');
		  load_data_pengaduan_masyarakat();
      }
    });
  });
</script>

<script>
  function load_data_pengaduan_masyarakat(halaman, limit) {
    $('#tbl_utama_permohonan_informasi_publik').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>pengaduan_masyarakat/load_table/',
      success: function(html) {
        $('#tbl_utama_pengaduan_masyarakat').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 