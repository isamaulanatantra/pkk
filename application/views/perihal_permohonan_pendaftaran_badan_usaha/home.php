
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>KOMPONEN BERANDA</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Perihal_permohonan_pendaftaran_badan_usaha Beranda</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_form_perihal_permohonan_pendaftaran_badan_usaha" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_data_perihal_permohonan_pendaftaran_badan_usaha" data-toggle="tab" id="klik_tab_data_perihal_permohonan_pendaftaran_badan_usaha">Data</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
									<div class="tab-pane active" id="tab_form_perihal_permohonan_pendaftaran_badan_usaha">
										<input name="tabel" id="tabel" value="perihal_permohonan_pendaftaran_badan_usaha" type="hidden" value="">
										<form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=perihal_permohonan_pendaftaran_badan_usaha" enctype="multipart/form-data">
											<input name="page" id="page" value="1" type="hidden" value="">
											<div class="form-group" style="display:none;">
												<label for="temp">temp</label>
												<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="mode">mode</label>
												<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="id_perihal_permohonan_pendaftaran_badan_usaha">id_perihal_permohonan_pendaftaran_badan_usaha</label>
												<input class="form-control" id="id_perihal_permohonan_pendaftaran_badan_usaha" name="id" value="" placeholder="id_perihal_permohonan_pendaftaran_badan_usaha" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="icon">Icon</label>
												<select class="form-control" id="icon" name="icon" >
												<option value="fa-home">fa-home</option>
												<option value="fa-gears">fa-gears</option>
												<option value="fa-th">fa-th</option>
												<option value="fa-font">fa-font</option>
												<option value="fa-comment">fa-comment</option>
												<option value="fa-cogs">fa-cogs</option>
												<option value="fa-cloud-download">fa-cloud-download</option>
												<option value="fa-bar-char">fa-bar-char</option>
												<option value="fa-phone">fa-phone</option>
												<option value="fa-envelope">fa-envelope</option>
												<option value="fa-link">fa-link</option>
												<option value="fa-tasks">fa-tasks</option>
												<option value="fa-users">fa-users</option>
												<option value="fa-signal">fa-signal</option>
												<option value="fa-coffee">fa-coffee</option>
												</select>
												<div id="iconselected"></div>
											</div>
											<div class="form-group">
												<label for="judul_perihal_permohonan_pendaftaran_badan_usaha">Judul Perihal_permohonan_pendaftaran_badan_usaha</label>
												<input class="form-control" id="judul_perihal_permohonan_pendaftaran_badan_usaha" name="judul_perihal_permohonan_pendaftaran_badan_usaha" value="" placeholder="Judul Perihal_permohonan_pendaftaran_badan_usaha" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="isi_perihal_permohonan_pendaftaran_badan_usaha">Isi Perihal_permohonan_pendaftaran_badan_usaha</label>
												<input class="form-control" id="isi_perihal_permohonan_pendaftaran_badan_usaha" name="isi_perihal_permohonan_pendaftaran_badan_usaha" value="" placeholder="Isi Perihal_permohonan_pendaftaran_badan_usaha" type="text">
											</div>
											<div class="row">
											<textarea id="editor_isi_perihal_permohonan_pendaftaran_badan_usaha"></textarea>
											</div>
											<div class="form-group">
												<label for="keterangan">Keterangan Perihal_permohonan_pendaftaran_badan_usaha</label>
												<input class="form-control" id="keterangan" name="keterangan" value="" placeholder="Keterangan" type="text">
											</div>
											<div class="form-group">
											<button type="submit" class="btn btn-primary" id="simpan_perihal_permohonan_pendaftaran_badan_usaha">SIMPAN</button>
											<button type="submit" class="btn btn-primary" id="update_perihal_permohonan_pendaftaran_badan_usaha" style="display:none;">UPDATE</button>
											</div>
										</form>

										<div class="overlay" id="overlay_form_input" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
									<div class="tab-pane table-responsive" id="tab_data_perihal_permohonan_pendaftaran_badan_usaha">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
															<option value="999999999">Semua</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="perihal_permohonan_pendaftaran_badan_usaha.judul_perihal_permohonan_pendaftaran_badan_usaha">Judul</option>
															<option value="perihal_permohonan_pendaftaran_badan_usaha.keterangan">Keterangan</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_perihal_permohonan_pendaftaran_badan_usaha"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<tr>
														<th>NO</th>
														<th>Judul Perihal </th>
														<th>Keterangan</th>
														<th>PROSES</th> 
													</tr>
													<tbody id="tbl_data_perihal_permohonan_pendaftaran_badan_usaha">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_perihal_permohonan_pendaftaran_badan_usaha" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>perihal_permohonan_pendaftaran_badan_usaha/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedPerihal_permohonan_pendaftaran_badan_usaha() {
    $('#id_perihal_permohonan_pendaftaran_badan_usaha, #judul_perihal_permohonan_pendaftaran_badan_usaha, #isi_perihal_permohonan_pendaftaran_badan_usaha, #icon, #keterangan').val('');
    $('#tbl_attachment_perihal_permohonan_pendaftaran_badan_usaha').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#icon').on('change', function(e) {
      e.preventDefault();
      var xa = $('#icon').val();
      $("#iconselected").html('<span class="fa '+xa+' ">'+xa+'</span>');
    });
  });
</script>
 
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_perihal_permohonan_pendaftaran_badan_usaha').on('click', function(e) {
      e.preventDefault();
      var editor_isi_perihal_permohonan_pendaftaran_badan_usaha = CKEDITOR.instances.editor_isi_perihal_permohonan_pendaftaran_badan_usaha.getData();
      $('#isi_perihal_permohonan_pendaftaran_badan_usaha').val( editor_isi_perihal_permohonan_pendaftaran_badan_usaha );
      var parameter = [ 'judul_perihal_permohonan_pendaftaran_badan_usaha', 'isi_perihal_permohonan_pendaftaran_badan_usaha', 'keterangan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["judul_perihal_permohonan_pendaftaran_badan_usaha"] = $("#judul_perihal_permohonan_pendaftaran_badan_usaha").val();
      parameter["isi_perihal_permohonan_pendaftaran_badan_usaha"] = $("#isi_perihal_permohonan_pendaftaran_badan_usaha").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>perihal_permohonan_pendaftaran_badan_usaha/simpan_perihal_permohonan_pendaftaran_badan_usaha';
      
      var parameterRv = [ 'judul_perihal_permohonan_pendaftaran_badan_usaha', 'isi_perihal_permohonan_pendaftaran_badan_usaha', 'keterangan' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedPerihal_permohonan_pendaftaran_badan_usaha();
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perihal_permohonan_pendaftaran_badan_usaha').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_perihal_permohonan_pendaftaran_badan_usaha').hide();
    $('#update_perihal_permohonan_pendaftaran_badan_usaha').show();
    var id_perihal_permohonan_pendaftaran_badan_usaha = $(this).closest('tr').attr('id_perihal_permohonan_pendaftaran_badan_usaha');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_perihal_permohonan_pendaftaran_badan_usaha');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_perihal_permohonan_pendaftaran_badan_usaha').val(id_perihal_permohonan_pendaftaran_badan_usaha);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_perihal_permohonan_pendaftaran_badan_usaha:id_perihal_permohonan_pendaftaran_badan_usaha
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>perihal_permohonan_pendaftaran_badan_usaha/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#judul_perihal_permohonan_pendaftaran_badan_usaha').val(json[i].judul_perihal_permohonan_pendaftaran_badan_usaha);
            $('#icon').val(json[i].icon);
            $('#keterangan').val(json[i].keterangan);
            CKEDITOR.instances.editor_isi_perihal_permohonan_pendaftaran_badan_usaha.setData(json[i].isi_perihal_permohonan_pendaftaran_badan_usaha);
          }
        }
      });
    //AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_perihal_permohonan_pendaftaran_badan_usaha').on('click', function(e) {
      e.preventDefault();
      var editor_isi_perihal_permohonan_pendaftaran_badan_usaha = CKEDITOR.instances.editor_isi_perihal_permohonan_pendaftaran_badan_usaha.getData();
      $('#isi_perihal_permohonan_pendaftaran_badan_usaha').val( editor_isi_perihal_permohonan_pendaftaran_badan_usaha );
      var parameter = [ 'id_perihal_permohonan_pendaftaran_badan_usaha', 'judul_perihal_permohonan_pendaftaran_badan_usaha', 'isi_perihal_permohonan_pendaftaran_badan_usaha', 'icon', 'keterangan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_perihal_permohonan_pendaftaran_badan_usaha"] = $("#id_perihal_permohonan_pendaftaran_badan_usaha").val();
      parameter["judul_perihal_permohonan_pendaftaran_badan_usaha"] = $("#judul_perihal_permohonan_pendaftaran_badan_usaha").val();
      parameter["isi_perihal_permohonan_pendaftaran_badan_usaha"] = $("#isi_perihal_permohonan_pendaftaran_badan_usaha").val();
      parameter["icon"] = $("#icon").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>perihal_permohonan_pendaftaran_badan_usaha/update_perihal_permohonan_pendaftaran_badan_usaha';
      
      var parameterRv = [ 'id_perihal_permohonan_pendaftaran_badan_usaha', 'judul_perihal_permohonan_pendaftaran_badan_usaha', 'isi_perihal_permohonan_pendaftaran_badan_usaha', 'icon', 'keterangan' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<!----------------------->
<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('editor_isi_perihal_permohonan_pendaftaran_badan_usaha');
	$(".textarea").wysihtml5();
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_perihal_permohonan_pendaftaran_badan_usaha').on('click', '#del_ajax', function() {
    var id_perihal_permohonan_pendaftaran_badan_usaha = $(this).closest('tr').attr('id_perihal_permohonan_pendaftaran_badan_usaha');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_perihal_permohonan_pendaftaran_badan_usaha"] = id_perihal_permohonan_pendaftaran_badan_usaha;
        var url = '<?php echo base_url(); ?>perihal_permohonan_pendaftaran_badan_usaha/hapus/';
        HapusData(parameter, url);
        $('[id_perihal_permohonan_pendaftaran_badan_usaha='+id_perihal_permohonan_pendaftaran_badan_usaha+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perihal_permohonan_pendaftaran_badan_usaha').on('click', '#inaktifkan', function() {
    var id_perihal_permohonan_pendaftaran_badan_usaha = $(this).closest('tr').attr('id_perihal_permohonan_pendaftaran_badan_usaha');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_perihal_permohonan_pendaftaran_badan_usaha"] = id_perihal_permohonan_pendaftaran_badan_usaha;
        var url = '<?php echo base_url(); ?>perihal_permohonan_pendaftaran_badan_usaha/inaktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_perihal_permohonan_pendaftaran_badan_usaha').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
        $('[id_perihal_permohonan_pendaftaran_badan_usaha='+id_perihal_permohonan_pendaftaran_badan_usaha+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perihal_permohonan_pendaftaran_badan_usaha').on('click', '#aktifkan', function() {
    var id_perihal_permohonan_pendaftaran_badan_usaha = $(this).closest('tr').attr('id_perihal_permohonan_pendaftaran_badan_usaha');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_perihal_permohonan_pendaftaran_badan_usaha"] = id_perihal_permohonan_pendaftaran_badan_usaha;
        var url = '<?php echo base_url(); ?>perihal_permohonan_pendaftaran_badan_usaha/aktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_perihal_permohonan_pendaftaran_badan_usaha').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
        $('[id_perihal_permohonan_pendaftaran_badan_usaha='+id_perihal_permohonan_pendaftaran_badan_usaha+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>