<?php
  $web=$this->uut->namadomain(base_url());
  ?>
	<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="icon" type="image/png" href="https://indonesia.go.id/assets/img/logo2.png" />
    <!-- Bootstrap -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet"> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Muli:400,700" rel="stylesheet"> -->
    <!-- <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans" /> -->
    <!-- <link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">
    <link href="https://indonesia.go.id/assets/js/bootstrap-3.3.6-dist/css/bootstrap.css" rel="stylesheet" type="text/css" >
    <link href="https://indonesia.go.id/assets/css/style.css" rel="stylesheet" type="text/css" >
    <link href="https://indonesia.go.id/assets/css/video-js.css" rel="stylesheet" type="text/css" >
    <title>Indonesia.go.id</title>
    <meta name="description" content="<p>Indonesia.go.id menggambarkan Indonesia secara utuh. Menjadi pusat informasi data Indonesia</p>" />
    <meta name="keywords" content="indonesia, profile,sistem, parlemen, berita, agama, propinsi,  peta, presiden, wakil, konstitusi, lembaga, bendera, bahasa, peraturan, kementerian, suku, bangsa, daerah, negara" />
    <meta name="author" content="Redaksi Indonesia.go.id" />
    <meta name="robots" content="index,follow"/>
    <meta property="og:title" content="Indonesia.go.id"/>
    <meta property="og:site_name" content="Indonesia.go.id">
    <meta property="og:type" content="article"/>
    <meta property="og:image:type" content="image/jpeg">
    <meta property="og:image" content="https://indonesia.go.id/assets/img/profile/1507649180_INDONESIA_logo.png"/>
    <meta property="og:url" content="https://indonesia.go.id/"/>
    <meta name="twitter:card" content="summary_large_image"/>
    <meta name="twitter:site" content="@indonesiagoid"/>
    <meta name="twitter:site:id" content="@indonesiagoid"/>
    <meta name="twitter:creator" content="@indonesiagoid"/>
    <meta name="twitter:title" content="Indonesia.go.id"/>
    <meta name="twitter:url" content="https://indonesia.go.id/"/>
    <meta name="twitter:description" content="Indonesia.go.id menggambarkan Indonesia secara utuh. Menjadi pusat informasi data Indonesia"/>
    <meta name="twitter:image:src" content="https://indonesia.go.id/assets/img/profile/1507649180_INDONESIA_logo.png"/>
    <script type="text/javascript" src="https://indonesia.go.id/assets/js/jquery-1.11.0.min.js" ></script>    <script type="text/javascript" src="https://indonesia.go.id/assets/js/bootstrap-3.3.6-dist/js/bootstrap.min.js" ></script>    
    <link href="https://indonesia.go.id/assets/js/flexslider/flexslider.css" rel="stylesheet" type="text/css" >
    <script type="text/javascript" src="https://indonesia.go.id/assets/js/flexslider/jquery.flexslider-min.js" ></script>    <script type="text/javascript" src="https://indonesia.go.id/assets/js/parallax/parallax.js" ></script>    <script type="text/javascript" src="https://indonesia.go.id/assets/js/imgLiquid/imgLiquid-min.js" ></script>    <script type="text/javascript" src="https://indonesia.go.id/assets/js/video.min.js" ></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-69408233-2"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      
      gtag('config', 'UA-69408233-2');
    </script>
  </head>
  <body>
    <script type="text/javascript">
      $(document).ready(function(){
          $('.mobile-menu-button').stop().click(function(){
            $('.bdz-menu-mobile').fadeToggle();
          });
            $(".bdz-header-menu li").hover(function () {
                $(this).trigger('reset');
                
                $(this).children(".submenu ,.submenu-small").slideDown({
                    duration: 0,
                    easing: "swing",
                    complete: function () {
                        $(this).addClass("hover");
                    }
                });
            }, function () {
                var me = $(this);
                var timeout = setTimeout(function () {
                    me.find(".hover").slideUp({
                        duration: 50,
                        complete: function () {
                            $(this).removeClass("hover");
                        }
                    });
                }, 100);
                
                me.bind('reset', function () {
                    clearTimeout(timeout);
                });
            });
      
      
             $(".menu-item-sub").hover(function () {
                $(this).trigger('reset');
                
                $(this).children("ul").slideDown({
                    duration: 0,
                    easing: "swing",
                    complete: function () {
                        $(this).addClass("hover");
                    }
                });
            }, function () {
                var me = $(this);
                var timeout = setTimeout(function () {
                    me.find(".hover").slideUp({
                        duration: 0,
                        complete: function () {
                            $(this).removeClass("hover");
                        }
                    });
                }, 100);
                
                me.bind('reset', function () {
                    clearTimeout(timeout);
                });
            });
      
        //      $('.parallax-window').parallax({
        //         naturalWidth: 600,
        //       naturalHeight:340
        //       });
        // jQuery(window).trigger('resize').trigger('scroll');
      
        $('#slider').flexslider({
                    directionNav:false,
                    // controlNav: false,
                    reverse:true,
                    animationLoop: true,
                    slideshow: true
                  });
         $('#sliderx').flexslider({
                    directionNav:false,
                    controlNav: false,
                    reverse:true,
                    animationLoop: true,
                    slideshow: true
                  });
        // $("#languange_select").change(function(){
        //   val =  $(this).val();
          
        // });
        
      });
    </script>
    <div class='bdz-header-wrapper'>
      <div class='bdz-100 bdz-header-socmed'>
        <div class='bdz-header-socmed-container'>
          <div class='bdz-header-location'>
            <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
            Lokasi Kamu : <i>(Showing)</i> <b>Jakarta Kota</b>
          </div>
          <div class='bdz-header-socmed-search'>
            <form method="get"  action="https://indonesia.go.id/search">
              <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
              <input placeholder="search" name='key'>
            </form>
          </div>
          <div class='bdz-header-socmed-item'><a target="_blank" href="http://facebook.com/1ndonesiagoid/"><img src="https://indonesia.go.id/assets/img/banner/socmed-fb.png"></a></div>
          <div class='bdz-header-socmed-item'><a target="_blank" href="http://twitter.com/1ndonesiagoid"><img src="https://indonesia.go.id/assets/img/banner/socmed-twitter.png"></a></div>
          <div class='bdz-header-socmed-item'><a target="_blank" href="http://youtube.com/channel/UCrJnfV-myC-eihK1vvAdFBA"><img src="https://indonesia.go.id/assets/img/banner/socmed-youtube.png"></a></div>
          <div class='bdz-header-socmed-item'><a target="_blank" href="http://instagram.com/1ndonesiagoid/"><img src="https://indonesia.go.id/assets/img/banner/socmed-ig.png"></a></div>
          <div class='bdz-header-socmed-language'>
            <select class="form-control control-language" onchange="javascript:window.location.href='https://indonesia.go.id/multilanguageswitcher/switcher/'+this.value;">
              <option value="indonesia" selected="selected">Bahasa</option>
              <option value="english" >English</option>
            </select>
            <!--   <select id='languange_select'>
              <option value='indonesia' selected>Bahasa</option>
              <option value="eng">English</option>
              </select>  -->
          </div>
        </div>
      </div>
      <div class='bdz-header-container'>
        <div class='bdz-header-menu-container'>
          <a href='https://indonesia.go.id/' class='bdz-header-logo'>
          <img src="https://indonesia.go.id/assets/img/banner/logo.png">
          </a>
          <div class='bdz-header-menu'>
            <ul>
              <!-- profile -->

																		<?php
																		$this->db->select("*");
																		$where_wisata = array(
																			'status' => 1,
																			'parent' => 0,
																			'domain' => $web
																			);
																		$this->db->where($where_wisata);
																		//$this->db->limit('7');
																		$this->db->order_by('slide_website.created_time', 'desc');
																		$query_wisata = $this->db->get('slide_website');
																		$a = 0;
																		foreach ($query_wisata->result() as $row_wisata)
																			{
																				$id_slide_website = $row_wisata->id_slide_website;
																				$keterangan = $row_wisata->keterangan;
																				$url_redirection = $row_wisata->url_redirection;
																						echo'
																							<li>
																								<a href="https://indonesia.go.id/profil">
																								<img src="https://indonesia.go.id/assets/img/banner/header-menu-profil.png"> 
																								<span>
																								Profil 
																								</span> 
																								</a>
																								<div class="submenu-small submenu-lg">
																									<div class="bdz-100 bdz-header-submenu">
																										<div class="bdz-40 bdz-submenu-menu-container">
																											<div class="submenu-title">
																												Profil 
																											</div>
																											<ul class="no-padding">
																												<!-- <li><a href="https://indonesia.go.id/profil/profil-indonesia">Profil Indonesia</a></li> -->
																												<li><a href="https://indonesia.go.id/archipelago">Peta indonesia</a></li>
																												<li><a href="https://indonesia.go.id/profil/lambang-negara">Konstitusi, Lambang, Bendera & Bahasa</a></li>
																												<li><a href="https://indonesia.go.id/profil/sistem-pemerintahan">Sistem Pemerintahan</a></li>
																												<li><a href="https://indonesia.go.id/profil/presiden-dan-wakil-presiden">Presiden dan Wakil Presiden</a></li>
																												<li><a href="https://indonesia.go.id/kementerian-lembaga">Kementerian/Lembaga</a></li>
																												<li><a href="https://indonesia.go.id/profil/parlemen">Parlemen</a></li>
																												<li><a href="https://indonesia.go.id/province">Pemerintah Provinsi</a></li>
																												<li><a href="https://indonesia.go.id/profil/suku-bangsa">Suku Bangsa</a></li>
																												<li><a href="https://indonesia.go.id/profil/agama">Agama</a></li>
																												<li><a href="http://peraturan.go.id/" target="_blank">Peraturan</a></li>
																											</ul>
																											<div class="bdz-submenu-button">
																												Lihat Profil Indonesia.go.id <a href="https://indonesia.go.id/profil"> Selengkapnya</a>
																											</div>
																										</div>
																										<div class="bdz-60 bdz-submenu-image-container">                                                                                                                                                                                                                                                    
																											<img class="bdz-submenu-image" src="https://indonesia.go.id/assets/img/menu/1539767816_OK.png" >
																										</div>
																									</div>
																								</div>
																							</li>
																						';
																				}
																		?>
              <li>
                <a href="https://indonesia.go.id/profil">
                <img src="https://indonesia.go.id/assets/img/banner/header-menu-profil.png"> 
                <span>
                Profil 
                </span> 
                </a>
                <div class='submenu-small submenu-lg'>
                  <div class='bdz-100 bdz-header-submenu'>
                    <div class='bdz-40 bdz-submenu-menu-container'>
                      <div class='submenu-title'>
                        Profil 
                      </div>
                      <ul class="no-padding">
                        <!-- <li><a href="https://indonesia.go.id/profil/profil-indonesia">Profil Indonesia</a></li> -->
                        <li><a href="https://indonesia.go.id/archipelago">Peta indonesia</a></li>
                        <li><a href="https://indonesia.go.id/profil/lambang-negara">Konstitusi, Lambang, Bendera & Bahasa</a></li>
                        <li><a href="https://indonesia.go.id/profil/sistem-pemerintahan">Sistem Pemerintahan</a></li>
                        <li><a href="https://indonesia.go.id/profil/presiden-dan-wakil-presiden">Presiden dan Wakil Presiden</a></li>
                        <li><a href="https://indonesia.go.id/kementerian-lembaga">Kementerian/Lembaga</a></li>
                        <li><a href="https://indonesia.go.id/profil/parlemen">Parlemen</a></li>
                        <li><a href="https://indonesia.go.id/province">Pemerintah Provinsi</a></li>
                        <li><a href="https://indonesia.go.id/profil/suku-bangsa">Suku Bangsa</a></li>
                        <li><a href="https://indonesia.go.id/profil/agama">Agama</a></li>
                        <li><a href="http://peraturan.go.id/" target="_blank">Peraturan</a></li>
                      </ul>
                      <div class='bdz-submenu-button'>
                        Lihat Profil Indonesia.go.id <a href="https://indonesia.go.id/profil"> Selengkapnya</a>
                      </div>
                    </div>
                    <div class='bdz-60 bdz-submenu-image-container'>                                                                                                                                                                                                                                                    
                      <img class='bdz-submenu-image' src="https://indonesia.go.id/assets/img/menu/1539767816_OK.png" >
                    </div>
                  </div>
                </div>
              </li>
              <!-- ragam -->
              <li>
                <a href="https://indonesia.go.id/ragam"><img src="https://indonesia.go.id/assets/img/banner/header-menu-budaya.png"> 
                <span>Ragam</span> 
                </a>
                <div class='submenu-small submenu-lg'>
                  <div class='bdz-100 bdz-header-submenu'>
                    <div class='bdz-40 bdz-submenu-menu-container'>
                      <div class='submenu-title'>
                        Ragam                    
                      </div>
                      <ul class="no-padding">
                        <li><a href="https://indonesia.go.id/ragam/budaya">Budaya</a></li>
                        <li><a href="https://indonesia.go.id/ragam/pariwisata">Pariwisata</a></li>
                        <li><a href="https://indonesia.go.id/ragam/seni">Seni</a></li>
                        <li><a href="https://indonesia.go.id/ragam/komoditas">Komoditas</a></li>
                        <li><a href="https://indonesia.go.id/ragam/keanekaragaman-hayati">Keanekaragaman Hayati</a></li>
                      </ul>
                      <div class='bdz-submenu-button'>
                        Lihat Ragam Indonesia.go.id <a href="https://indonesia.go.id/ragam"> Selengkapnya</a>
                      </div>
                    </div>
                    <div class='bdz-60 bdz-submenu-image-container'>
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <a href="https://indonesia.go.id/ragam/budaya/kebudayaan/karakteristik-keris"  class='bdz-submenu-image-file'>
                        <img alt="Karakteristik Keris"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1543300417_Karakteristik_Keris.jpg&q=50&a=c&w=400&h=200">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Karakteristik Keris                                                          
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <a href="https://indonesia.go.id/ragam/budaya/kebudayaan/rujak-kudapan-dengan-cita-rasa-nusantara"  class='bdz-submenu-image-file'>
                        <img alt="Rujak, Kudapan dengan Cita Rasa Nusantara"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1543293155_Rujak,_Kudapan_dengan_Cita_Rasa_Nusantara_sportourismid.jpg&q=50&a=c&w=400&h=200">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Rujak, Kudapan dengan Cita Rasa Nusantara                                                          
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <a href="https://indonesia.go.id/ragam/budaya/kebudayaan/tenun-ntt-harta-keluarga-yang-bernilai-tinggi"  class='bdz-submenu-image-file'>
                        <img alt="Tenun NTT, Harta Keluarga yang Bernilai Tinggi "  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1543288737_Harta_Keluarga_yang_Bernilai_Tinggi.jpg&q=50&a=c&w=400&h=200">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Tenun NTT, Harta Keluarga yang Bernilai Tinggi                                                           
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <!-- layanan -->
              <li>
                <a href="http://infopublik.id/" target='_blank'><img src="https://indonesia.go.id/assets/img/banner/header-menu-layanan.png"> 
                <span>Layanan</span> 
                </a>
                <div class='submenu-small submenu-lg'>
                  <div class='bdz-100 bdz-header-submenu'>
                    <div class='bdz-40 bdz-submenu-menu-container'>
                      <div class='submenu-title'>
                        Layanan                  
                      </div>
                      <ul class="no-padding">
                        <li><a href="http://infopublik.id/kategori/pendidikan" target='_blank'>Pendidikan</a></li>
                        <li><a href="http://infopublik.id/kategori/kesehatan" target='_blank'>Kesehatan</a></li>
                        <li><a href="http://infopublik.id/kategori/keuangan" target='_blank'>Keuangan</a></li>
                        <li><a href="http://infopublik.id/kategori/kependudukan" target='_blank'>Kependudukan</a></li>
                        <li><a href="http://infopublik.id/kategori/keimigrasian" target='_blank'>Keimigrasian</a></li>
                        <li><a href="http://infopublik.id/kategori/perdagangan" target='_blank'>Perdagangan</a></li>
                        <li><a href="http://infopublik.id/kategori/investasi" target='_blank'>Investasi</a></li>
                        <!-- <li><a href="https://www.lapor.go.id/" target='_blank'>Lapor</a></li> -->
                        <li><a href="http://infopublik.id/kategori/lainnya" target='_blank'>Lainnya</a></li>
                        <!--  <li><a href="https://indonesia.go.id/layanan/pendidikan">Pendidikan</a></li>
                          <li><a href="https://indonesia.go.id/layanan/kesehatan">Kesehatan</a></li>
                          <li><a href="https://indonesia.go.id/layanan/keuangan">Keuangan</a></li>
                          <li><a href="https://indonesia.go.id/layanan/kependudukan">Kependudukan</a></li>
                          <li><a href="https://indonesia.go.id/layanan/keimigrasian">Keimigrasian</a></li>
                          <li><a href="https://indonesia.go.id/layanan/perdagangan">Perdagangan</a></li>
                          <li><a href="https://indonesia.go.id/layanan/investasi">Investasi</a></li>
                          <li><a href="https://www.lapor.go.id/">Lapor</a></li>
                          <li><a href="https://indonesia.go.id/layanan/lainnya">Lainnya</a></li> -->
                      </ul>
                      <div class='bdz-submenu-button'>
                        Lihat Layanan Indonesia.go.id <a href="http://infopublik.id/" target='_blank'> Selengkapnya</a>
                        <!-- Lihat Layanan Indonesia.go.id <a href="https://indonesia.go.id/layanan"> Selengkapnya</a> -->
                      </div>
                    </div>
                    <div class='bdz-60 bdz-submenu-image-container'>
                      <!-- kalo bahasa inggris di hide -->
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <a href="http://infopublik.id/kategori/lainnya/312195/cara-mengurus-penggabungan-tanah-perseorangan"  class='bdz-submenu-image-file'>
                        <img alt="Cara Mengurus Penggabungan Tanah Perseorangan"  src="http://infopublik.id/assets/upload/headline/20181120_214814_thumb.jpg">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Cara Mengurus Penggabungan Tanah Perseorangan                                            
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 20 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <a href="http://infopublik.id/kategori/lainnya/312194/cara-ganti-blangko-baru-sertifikat-tanah"  class='bdz-submenu-image-file'>
                        <img alt="Cara Ganti Blangko Baru Sertifikat Tanah"  src="http://infopublik.id/assets/upload/headline/images16_thumb.jpeg">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Cara Ganti Blangko Baru Sertifikat Tanah                                            
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 20 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <a href="http://infopublik.id/kategori/perdagangan/311549/cara-dan-syarat-pengajuan-kredit-usaha-rakyat-kur-untuk-umkmk"  class='bdz-submenu-image-file'>
                        <img alt="Cara dan Syarat Pengajuan Kredit Usaha Rakyat (KUR) untuk UMKMK"  src="http://infopublik.id/assets/upload/headline/Prosedur_KUR_thumb.jpeg">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Cara dan Syarat Pengajuan Kredit Usaha Rakyat (KUR) untuk UMKMK                                            
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 16 Nov 2018</small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <!-- narasi -->
              <li>
                <a href="https://indonesia.go.id/narasi"><img src="https://indonesia.go.id/assets/img/banner/header-menu-perspektif.png"> <span>Narasi</span> </a>
                <div class='submenu-small submenu-lg'>
                  <div class='bdz-100 bdz-header-submenu'>
                    <div class='bdz-100 bdz-submenu-image-container'>
                      <div class='bdz-20 bdz-submenu-image-item'>
                        <a href="https://indonesia.go.id/narasi/indonesia-dalam-angka/ekonomi/umi-bergairah-berkat-kredit-murah"  class='bdz-submenu-image-file'>
                        <img alt="UMi Bergairah Berkat Kredit Murah"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1543300254_UMi_Bergairah_Berkat_Kredit_Murah.jpg&q=50&a=c&w=400&h=200">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          UMi Bergairah Berkat Kredit Murah                                                      
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-20 bdz-submenu-image-item'>
                        <a href="https://indonesia.go.id/narasi/indonesia-dalam-angka/ekonomi/indonesia-siap-menuju-industri-4-0"  class='bdz-submenu-image-file'>
                        <img alt="Indonesia Siap Menuju Industri 4.0"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1543149184_industri.jpg&q=50&a=c&w=400&h=200">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Indonesia Siap Menuju Industri 4.0                                                      
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 25 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-20 bdz-submenu-image-item'>
                        <a href="https://indonesia.go.id/narasi/indonesia-dalam-angka/ekonomi/sungai-rapi-dulu-laut-bersih-kemudian"  class='bdz-submenu-image-file'>
                        <img alt="Sungai Rapi Dulu, Laut Bersih Kemudian"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1543046238_Sungai_Rapi_Dulu,_Laut_Bersih_Kemudian.jpg&q=50&a=c&w=400&h=200">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Sungai Rapi Dulu, Laut Bersih Kemudian                                                      
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 24 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-20 bdz-submenu-image-item'>
                        <a href="https://indonesia.go.id/narasi/indonesia-dalam-angka/ekonomi/menyongsong-era-4-0-guru-berubah-peran"  class='bdz-submenu-image-file'>
                        <img alt="Menyongsong Era 4.0, Guru Berubah Peran"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1542939273_RUBRIK_NARASI_HARI_GURU_NASIONAL_2018.jpeg&q=50&a=c&w=400&h=200">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Menyongsong Era 4.0, Guru Berubah Peran                                                      
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 23 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-20 bdz-submenu-image-item'>
                        <a href="https://indonesia.go.id/narasi/indonesia-dalam-angka/ekonomi/investasi-kian-deras-lapangan-kerja-terbuka-luas"  class='bdz-submenu-image-file'>
                        <img alt="Investasi Kian Deras, Lapangan Kerja Terbuka Luas"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1542868576_Investasi_Kian_Deras,_Lapangan_Kerja_Terbuka_Luas.jpg&q=50&a=c&w=400&h=200">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Investasi Kian Deras, Lapangan Kerja Terbuka Luas                                                      
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 22 Nov 2018</small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <!-- berita -->
              <li>
                <a href="https://jpp.go.id"><img src="https://indonesia.go.id/assets/img/banner/header-menu-berita.png"> 
                <span>Berita</span> 
                </a>
                <div class='submenu-small submenu-lg'>
                  <div class='bdz-100 bdz-header-submenu'>
                    <div class='bdz-40 bdz-submenu-menu-container'>
                      <div class='submenu-title'>
                        Berita                  
                      </div>
                      <ul class="no-padding">
                        <li><a href="https://jpp.go.id/nasional" target='_blank'>Nasional</a></li>
                        <li><a href="https://jpp.go.id/polkam" target='_blank'>POLKAM</a></li>
                        <li><a href="https://jpp.go.id/peristiwa" target='_blank'>Peristiwa</a></li>
                        <li><a href="https://jpp.go.id/ekonomi" target='_blank'>Ekonomi</a></li>
                        <li><a href="https://jpp.go.id/keuangan" target='_blank'>Keuangan</a></li>
                        <li><a href="https://jpp.go.id/ukm" target='_blank'>UKM</a></li>
                        <li><a href="https://jpp.go.id/humaniora" target='_blank'>Humaniora</a></li>
                        <li><a href="https://jpp.go.id/teknologi" target='_blank'>Teknologi</a></li>
                        <li><a href="https://jpp.go.id/olahraga" target='_blank'>Olah Raga</a></li>
                        <li><a href="https://jpp.go.id/cek-fakta" target='_blank'>Cek Fakta</a></li>
                      </ul>
                      <div class='bdz-submenu-button'>
                        Lihat Berita Indonesia.go.id <a href="https://jpp.go.id"> Selengkapnya</a>
                      </div>
                    </div>
                    <div class='bdz-60 bdz-submenu-image-container'>
                      <!-- kalo bahasa inggris di hide -->
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <div class='bdz-submenu-image-file'>
                          <img alt="Festival Olahraga Anak Usia Dini di Kubu Raya Digelar"  src="https://jpp.go.id/upload/berita/thumbs/images/327838/40ad94de3de88c50c52f3bbc41473124.jpeg">
                        </div>
                        <div class='bdz-submenu-image-title'>
                          Festival Olahraga Anak Usia Dini di Kubu Raya Digelar                                  
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <div class='bdz-submenu-image-file'>
                          <img alt="KLHK Buru Cukong Perambah Kawasan Hutan Bengkalis"  src="https://jpp.go.id/upload/berita/thumbs/images/327837/dfef5285bdffbdc76f094f6716182195.jpeg">
                        </div>
                        <div class='bdz-submenu-image-title'>
                          KLHK Buru Cukong Perambah Kawasan Hutan Bengkalis                                  
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <div class='bdz-submenu-image-file'>
                          <img alt="Pemerintah Usul: Penetapan BPIH 1440H/2019M dengan Dolar Amerika"  src="https://jpp.go.id/upload/berita/thumbs/images/327836/4f69faf15100cfe75079de559218add3.jpg">
                        </div>
                        <div class='bdz-submenu-image-title'>
                          Pemerintah Usul: Penetapan BPIH 1440H/2019M dengan Dolar Amerika                                  
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <!-- galeri -->
              <li>
                <a href="https://indonesia.go.id/gallery">
                <img src="https://indonesia.go.id/assets/img/banner/header-menu-galery.png"> 
                <span>Galeri</span> 
                </a>
                <div class='submenu-small submenu-lg'>
                  <div class='bdz-100 bdz-header-submenu'>
                    <div class='bdz-40 bdz-submenu-menu-container'>
                      <div class='submenu-title'>
                        Galeri                    
                      </div>
                      <ul class="no-padding">
                        <li><a href="https://indonesia.go.id/gallery?t=agenda">Agenda</a></li>
                        <li><a href="https://indonesia.go.id/gallery?t=foto">Foto</a></li>
                        <li><a href="https://indonesia.go.id/gallery?t=video">Videografis</a></li>
                        <li><a href="https://indonesia.go.id/gallery?t=infografis">Infografis</a></li>
                      </ul>
                      <div class='bdz-submenu-button'>
                        Lihat Galeri Indonesia.go.id <a href="https://indonesia.go.id/gallery"> Selengkapnya</a>
                      </div>
                    </div>
                    <div class='bdz-60 bdz-submenu-image-container'>
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <a href="https://indonesia.go.id/album/keragaman-indonesia"  class='bdz-submenu-image-file'>
                        <img alt="1543299564_merayakan_budaya_dan_kemanusiaan_lewat_batik_112104_1140.jpg"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/gallery/1543299564_merayakan_budaya_dan_kemanusiaan_lewat_batik_112104_1140.jpg&q=50&a=c&w=400&h=200">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Merayakan Budaya Lewat Batik                                                          
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <a href="https://indonesia.go.id/album/alam-indonesia"  class='bdz-submenu-image-file'>
                        <img alt="1543283151_Arosbaya_Madura__(1).jpg"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/gallery/1543283151_Arosbaya_Madura__(1).jpg&q=50&a=c&w=400&h=200">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Bukit Arosbaya, Tempat Eksotis di Madura                                                          
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                        </div>
                      </div>
                      <div class='bdz-33 bdz-submenu-image-item'>
                        <a href="https://indonesia.go.id/album/alam-indonesia"  class='bdz-submenu-image-file'>
                        <img alt="1543223756_pantai_nglambor_dijaga_dua_kura_kura_raksasa_175217_1140.jpg"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/gallery/1543223756_pantai_nglambor_dijaga_dua_kura_kura_raksasa_175217_1140.jpg&q=50&a=c&w=400&h=200">
                        </a>
                        <div class='bdz-submenu-image-title'>
                          Nglambor, Bawah Lautnya Begitu Cantik                                                          
                        </div>
                        <div class='bdz-submenu-image-date'>
                          <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 26 Nov 2018</small>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </li>
              <!-- galeri -->
              <li>
                <a href="https://www.lapor.go.id/" target="_blank">
                <img src="https://indonesia.go.id/assets/img/banner/header-menu-lapor.png"> 
                <span>Lapor</span> 
                </a>
              </li>
            </ul>
          </div>
        </div>
        <span class='mobile-menu-button'>
        <span class="glyphicon glyphicon-menu-hamburger" aria-hidden="true"></span>
        </span>
        <small class='bdz-mobile-language-switcher'>
          <div class='right'>
            <span onclick="javascript:window.location.href='https://indonesia.go.id/multilanguageswitcher/switcher/indonesia'" >Bahasa</span> 
            <span class='separator'>|</span>
            <span onclick="javascript:window.location.href='https://indonesia.go.id/multilanguageswitcher/switcher/english'" >English</span>
          </div>
          <div class='clearfix'></div>
          <hr>
          <div class='clearfix'></div>
        </small>
        <span class='mobile-logo-button'>
        <a href="https://indonesia.go.id/">
        <img src="https://indonesia.go.id/assets/img/banner/logo.png">
        </a>
        </span>
        <span class='bdz-mobile-search'>
        <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
        </span>
        <span class='bdz-mobile-remove-search'>
        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
        </span>
        <div class='clearfix'></div>
        <div class='bdz-menu-mobile'>
          <div class='mobile-header-menu'>
            <ul>
              <li><a href="https://indonesia.go.id/profil"> <img src="https://indonesia.go.id/assets/img/banner/header-menu-profil.png"> Profil</a></li>
              <li><a href="https://indonesia.go.id/ragam"> <img src="https://indonesia.go.id/assets/img/banner/header-menu-budaya.png"> Ragam</a></li>
              <li><a href="http://infopublik.id/"> <img src="https://indonesia.go.id/assets/img/banner/header-menu-layanan.png"> Layanan</a></li>
              <li><a href="https://indonesia.go.id/narasi"> <img src="https://indonesia.go.id/assets/img/banner/header-menu-perspektif.png"> Narasi</a></li>
              <li><a href="https://jpp.go.id"> <img src="https://indonesia.go.id/assets/img/banner/header-menu-berita.png"> Berita</a></li>
              <!-- <li><a href="https://indonesia.go.id/layanan"> <img src="https://indonesia.go.id/assets/img/banner/header-menu-layanan.png"> Layanan</a></li> -->
              <li><a href="https://indonesia.go.id/gallery"> <img src="https://indonesia.go.id/assets/img/banner/header-menu-galery.png"> Galeri</a></li>
              <li><a href="https://www.lapor.go.id/" target="_blank"> <img src="https://indonesia.go.id/assets/img/banner/header-menu-lapor.png"> Lapor</a></li>
              <li class='bdz-mobile-header-socmed'> 
                <a href="http://facebook.com/1ndonesiagoid/"><img alt="Facebook Indonesia" src="https://indonesia.go.id/assets/img/socmed/black/icfacebook32.png"></a>
                <a href="http://twitter.com/1ndonesiagoid"><img alt="Twitter Indonesia" src="https://indonesia.go.id/assets/img/socmed/black/ictwitter32.png"></a>
                <a href="http://youtube.com/channel/UCrJnfV-myC-eihK1vvAdFBA"><img alt="Youtube Indonesia" src="https://indonesia.go.id/assets/img/socmed/black/icyoutube32.png"></a>
                <a href="http://instagram.com/1ndonesiagoid/"><img alt="Instagram Indonesia" src="https://indonesia.go.id/assets/img/socmed/black/icinstagram32.png"></a>
              </li>
            </ul>
          </div>
        </div>
        <style type="text/css">
          .bdz-search-mobile-wrapper{display: none;width: 100%;height: 100%;position: fixed;top: 70px;left:0;background-color: #000000b5;z-index: 101}
          .bdz-search-mobile-container{width: 100%;float: left}
          .bdz-search-input-container input{width: 100% !important;font-size: 15;background-color: #fff;border-radius: 0px}
          .bdz-search-input-container form{padding: 10px;background-color: #fff}
          .bdz-mobile-remove-search{display: none}
          .bdz-mobile-header-socmed{text-align: right;}
          .top-slider-wrapper{display: block;left: 0;height: auto;padding-top: 0;width: 100%;}
          .bdz-main-wrapper{margin-top: 0}
          .b-slider-item-title a{color: #292929;text-decoration: none;font-size: 12px;}
          .b-slider-item-title a:hover{color: #292929;}
          .top-slider-wrapper{width:100%;float:right;}
          .top-slider-container{max-width: 1111px; margin: auto;}
          .right-slider-content{padding-left: 5px;}
          .b-label-container{ width: 100%;height:30px;}
          .b-label-item{ color: white; background-color: red; width: 100px; border-radius: 3px 3px 0px 0px; height: 100%; line-height: 28px;text-align: center;}
          .b-slider-wrapper{ position: relative;width:100%;float:left;overflow: hidden;}
          .b-slider-container ul{ position: relative;list-style: none; top: 0;left: 0;width: 11000px;margin-top: 0px;padding-top:5px;float: left; }
          .b-slider-container ul li{ float:left; top: 0;left: 0;margin-top: 0px;padding-top:0px}
          .b-slider-item{ float:left;width:375px;}
          .b-slider-item-left{float:left;width: 23%}
          .b-slider-item-right{float:left;width: 75%; margin-left: 5px;min-height: 40px;padding-bottom: 5px;}
          .b-slider-item-image{ float:left;width:100%;text-align: center}
          .b-slider-item-image img{ width: 70px; padding: 3px; height: 64px;}
          .b-slider-item-date{width: 100%;margin-top: 5px;font-size: 12px;color:#a40100;font-weight: bold}
          .b-slider-item-title{width: 100%;padding-right: 15px;}
          .bdz-news-text-single img{max-width: 100%;height: auto;}
          #languange_selec{color: #f00 }
          .control-language{height: 20px;font-size: 10px;padding: 0px;color: #f00}
          .bdz-submenu-image-item{padding: 20px;}
          .bdz-submenu-image-file{width: 100%;}
          .bdz-submenu-image-file img{width: 100% !important;max-height: 150px;}
          .bdz-submenu-image-title{float:left;text-align: left;margin-top: 20px;margin-bottom: 10px;}
          .bdz-submenu-image-date{float: left;width: 100%;}
          .bdz-submenu-image-date small{float: left;width: 100%;text-align: left;}
          .bdz-submenu-image-date span{float: left;display: inline-block !important;width: 20px !important;margin-right: 5px;}
        </style>
        <script type="text/javascript">
          $(document).ready(function(){
            $('.bdz-mobile-search').click(function(){
              $(".bdz-search-mobile-wrapper").stop().fadeToggle();
              $(this).fadeOut();
              $('.bdz-mobile-remove-search').fadeIn();
            });
            $('.bdz-mobile-remove-search').stop().click(function(){
              $(".bdz-search-mobile-wrapper").stop().fadeOut();
              $('.bdz-mobile-remove-search').fadeOut();
              $(".bdz-mobile-search").fadeIn();
            });
          
          
          });
        </script>
        <div class='bdz-search-mobile-wrapper'>
          <div class='bdz-search-mobile-container'>
            <div class='bdz-search-input-container'>
              <form method="get"  action="https://indonesia.go.id/search">
                <input type="text" name="key" class="form-control" placeholder="Cari apapun di website ini">
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class='bdz-slider-menu-wrapper'>
        <div class='bdz-slider-menu-container'>
          <ul>
            <li><a href="https://indonesia.go.id/"> <span class="glyphicon glyphicon-home" aria-hidden="true"></span></a></li>
            <li><a href="https://indonesia.go.id/profil"> Profil</a></li>
            <li><a href="https://indonesia.go.id/narasi"> Narasi</a></li>
            <li><a href="https://jpp.go.id" target="_blank">Berita</a></li>
            <!-- <li><a href="https://indonesia.go.id/layanan">Layanan</a></li> -->
            <li><a href="http://infopublik.id/" target='_blank'>Layanan</a></li>
            <li><a href="https://indonesia.go.id/gallery">Galeri</a></li>
            <li><a href="https://indonesia.go.id/ragam">  Ragam</a></li>
            <li><a href="https://www.lapor.go.id/" target="_blank"> Lapor</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class='bdz-main-wrapper'>
      <div class='bdz-main-container'>
        <div class='snap bdz-slider-wrapper'>
          <div  class='bdz-slider-container' >
            <script type="text/javascript">
              $(document).ready(function(){
                  
                  // $(this).scrollTop(0);
              
              	$(".bdz-slider-text").stop().hover(function(){
              		$(".bdz-slider-desc").slideToggle();
              	});
              
              	 $(window).scroll(function(){
                         var scroll = $(window).scrollTop();
                         if (scroll >= $(".bdz-home-text-middle").eq(0).offset().top - 170) {
                           // $(".top-slider-wrapper").stop().slideDown();
                           	$(".top-slider-wrapper").addClass('floating-marquee');
                           	$(".bdz-header-socmed").hide();
                           	$(".bdz-header-logo img").css({"width":"140px","margin-top":"10px"});
                           	$(".bdz-header-menu").css({"margin-top":"0px"});
                           	$(".bdz-header-menu ul li span").hide();
                             $('.submenu-small').css({"top":'57px'});
              
              
                         
              
                         }else{
                           	$(".top-slider-wrapper").removeClass('floating-marquee');
                           	$(".bdz-header-socmed").show();
                           	$(".bdz-header-logo img").css({"width":"225px","margin-top":"50px"});
                           	$(".bdz-header-menu").css({"margin-top":"43px"});
                           	$(".bdz-header-menu ul li span").show();
                             $('.submenu-small').css({"top":'115px'});
              
                           // $(".top-slider-wrapper").stop().slideUp();
                         };
                       });
                  $(".bdz-more-info").click(function(){
                    $('html, body').animate({
                         scrollTop: $(".bdz-home-item-container").offset().top - 130
                     }, 2000);
                  });
                  $('.bdz-download-slider').click(function(e){
                     e.preventDefault();  //stop the browser from following
                     var link = $(this).attr('data-url');
                     console.log(link)
                     window.location.href = link;
                     
                  });
              
              
                   
              
              });
            </script>
            <style type="text/css">
              .bdz-slider-name{font-size: 17px;padding-right:5px;line-height: 0px;}
              .bdz-slider-name span{font-size: 18px;line-height: 28px;}
              .bdz-slider-desc{font-size: 13px;padding-left:10px;line-height:20px;display: none;}
              .flex-control-nav{    z-index: 100!important;bottom: 35%;float: right;width: 10px;right: 21px;}
              .flex-control-nav li{margin: 0 6px;display: block;zoom: 1;margin-bottom: 10px;}
              /*.flex-control-nav{top: 50px;z-index: 1}*/
              .bdz-slider-text{z-index: 100}
              .bdz-slider-text-detail{font-size: 12px;background-color: #fff;padding: 14px;float: left;height: 85px;padding-bottom: 10px;width: 100%;}
              .bdz-slider-text{position: absolute;top: 125px;background-color: rgba(0, 0, 0, 0.61);width: 30%;left: 0px;font-size: 20px;color: #fff;padding: 15px;line-height: 20px;margin: 35px;border-radius: 5px;}
              .floating-marquee{position: fixed !important;top: 55px;bottom: auto !important}
              .bdz-more-info{text-align: center;
              bottom: 0px;
              font-size: 20px;
              color: #fff;
              text-transform: capitalize;
              cursor: pointer;
              border: solid 1px #eee;
              width: 230px;
              margin: auto;
              margin-top: 170px;
              border-radius: 5px;
              padding: 10px;}
              .bdz-slider-share-button{float: right;position: absolute;right: 20px;top: 150px;width: 300px;display: inline-block;}
              .bdz-slider-share-button .glyphicon{float: right;font-size: 43px;color: #fff;margin-right: 5px;cursor: pointer;}
              .addthis_inline_share_toolbox_xkvq{float: right;}
              .bdz-parallax-wrapper{
              bottom: 0;
              left: 0;
              overflow: hidden;
              position: relative;
              right: 0;
              top: 0;
              width: 100%;
              }
              .bdz-parallax-container{
              height: 100vh;
              max-height: 100%;
              overflow: hidden;}
              .bdz-parallax-image-item{
              position: fixed;
              height: auto;
              left: 50%;
              max-width: 1000%;
              min-height: 100%;
              min-width: 100%;
              min-width: 100vw;
              width: auto;
              top: 50%;
              padding-bottom: 1px;
              -ms-transform: translateX(-50%) translateY(-50%);
              -moz-transform: translateX(-50%) translateY(-50%);
              -webkit-transform: translateX(-50%) translateY(-50%);
              transform: translateX(-50%) translateY(-50%);
              height: 100%;
              left: 0;
              -o-object-fit: cover;
              object-fit: cover;
              top: 0;
              -ms-transform: none;
              -moz-transform: none;
              -webkit-transform: none;
              transform: none;
              width: 100%;
              }
              .slides img{
              position: fixed;
              height: auto;
              left: 50%;
              max-width: 1000%;
              min-height: 100%;
              min-width: 100%;
              min-width: 100vw;
              width: auto;
              top: 50%;
              padding-bottom: 1px;
              -ms-transform: translateX(-50%) translateY(-50%);
              -moz-transform: translateX(-50%) translateY(-50%);
              -webkit-transform: translateX(-50%) translateY(-50%);
              transform: translateX(-50%) translateY(-50%);
              height: 100%;
              left: 0;
              -o-object-fit: cover;
              object-fit: cover;
              top: 0;
              -ms-transform: none;
              -moz-transform: none;
              -webkit-transform: none;
              transform: none;
              width: 100%;
              }
            </style>
            <div class='bdz-parallax-wrapper'>
              <div class='bdz-parallax-container'>
                <div class='bdz-parallax-image-item'>
                  <div id="slider" class="flexslider">
                    <ul class="slides">
																		<?php
																		$this->db->select("*");
																		$where_wisata = array(
																			'status' => 1,
																			'domain' => $web
																			);
																		$this->db->where($where_wisata);
																		//$this->db->limit('7');
																		$this->db->order_by('slide_website.created_time', 'desc');
																		$query_wisata = $this->db->get('slide_website');
																		$a = 0;
																		foreach ($query_wisata->result() as $row_attachment)
																			{
																				echo'
																					<li >
																						<img alt="Merayakan Budaya Lewat Batik" src="'.base_url().'media/upload/'.$row_attachment->file_name.'"  />
																						<script type="text/javascript">
																							var addthis_config = {
																								 url: "'.base_url().'media/upload/'.$row_attachment->file_name.'" ,
																								 title: "",
																							}
																						</script>
																						<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5875fc331b3191b7"></script> 
																						<div class="bdz-slider-share-button">
																							<div class="addthis_inline_share_toolbox_xkvq"></div>
																							<span class="glyphicon glyphicon-download bdz-download-slider" data-url="'.base_url().'media/upload/'.$row_attachment->file_name.'" aria-hidden="true"></span>
																						</div>
																						<div class="bdz-slider-text">
																							<div class="bdz-slider-name">
																								<span class="glyphicon glyphicon-map-marker red" aria-hidden="true"></span>
																								Merayakan Budaya Lewat Batik                       
																								<!-- <span ><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></span> -->
																								<div class="bdz-slider-desc">
																									<p>'.$row_attachment->keterangan.'</p>
																									<p>(Sumber foto: Pesona Indonesia)</p>
																								</div>
																							</div>
																							<div class="cleafix"></div>
																						</div>
																					</li>
																											';
																				}
																		?>
                    </ul>
                  </div>
                  <div class='filter-paralax-wrapper'>
                    <div class='filter-paralax-container'>
                      <div class='bdz-100'>
                        <!--    <form method="get"  action="https://indonesia.go.id/search">
                          <div class="input-group ">
                          
                             <input type="text" name="key" class="form-control input-lg input-opa" placeholder="Cari apapun di website ini...">
                             <div class="input-group-btn">
                               <button type="submit" class="btn btn-danger btn-lg" aria-expanded="false"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                             </div>
                           </div>  
                          </form> -->
                        <div class='bdz-more-info'>
                          <span class="glyphicon glyphicon-menu-down" aria-hidden="true"></span> Ketahui Lebih Lanjut          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        <div class='snap bdz-home-wrapper bdz-home-item-container'>
          <div class='bdz-home-container'>
            <div class='bdz-home-text-middle'>
              <b>
                <div class='bdz-home-text-midle-title'>
                  <img src="https://indonesia.go.id/assets/img/banner/menu-gallery.png">  Ragam          
                </div>
                <span class='bdz-home-text-midle-menu'>
                  <ul>
                    <li><a href="https://indonesia.go.id/ragam/budaya">Budaya</a></li>
                    <li><a href="https://indonesia.go.id/ragam/seni">Seni</a></li>
                    <li><a href="https://indonesia.go.id/ragam/keanekaragaman-hayati">Keanekaragaman Hayati</a></li>
                    <li><a href="https://indonesia.go.id/ragam/pariwisata">Pariwisata</a></li>
                    <li><a href="https://indonesia.go.id/ragam/komoditas">Komoditas</a></li>
                  </ul>
                </span>
                <ul class='right'>
                  <li class='li-edit'>
                    <div>
                      Edit <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                      <ul class='ul-edit-submenu'>
                        <li data-action="hapus"><span class="glyphicon glyphicon-remove-sign" aria-hidden="true"></span> Hapus bagian</li>
                        <li data-action="atas"><span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span> Pindahkan ke bagian atas</li>
                        <li data-action="bawah"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span> Pindahkan ke bagian bawah</li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </b>
              <div class='clearfix'></div>
            </div>
            <div class='bdz-home-container-padding'>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
                <a href="https://indonesia.go.id/ragam/budaya/kebudayaan/satai-santapan-lezat-yang-diakui-dunia" class='bdz-100 bdz-sub-header-item'>
                  <div class='bdz-sub-header-item-container'>
                    <!-- <div class='bdz-sub-header-item-topik'>
                      Budaya                                              </div> -->
                    <img alt="Satai, Santapan Lezat yang Diakui Dunia"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1542939182_RUBRIK_RAGAM_KULINER_NONAME_EDT.jpeg&q=50&a=c&w=500&h=300">
                    <div class='bdz-sub-header-item-text-lg'>
                      Satai, Santapan Lezat yang Diakui Dunia                                                                                                  
                      <div class='clearfix'></div>
                      <!--   <div class='bdz-sub-header-item-text-lg-resume'>
                        ...
                        
                        </div> -->
                      <div class='clearfix'></div>
                      <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 23 Nov 2018</small>
                    </div>
                  </div>
                </a>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
                <a href="https://indonesia.go.id/ragam/budaya/kebudayaan/upacara-ritual-dan-kuasa"  class='bdz-100 bdz-sub-header-item-sm'>
                  <div class='bdz-sub-header-item-container'>
                    <!-- <div class='bdz-sub-header-item-topik'>
                      Budaya                                                </div> -->
                    <img class='bdz-30' alt="Upacara, Ritual, dan Kuasa"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1543209238_sistem_penanggalan_kalalakonblogspotcom.jpg&q=50&a=c&w=250&h=200">
                    <div class='bdz-70 bdz-sub-header-item-text-sm'>
                      Upacara, Ritual, dan Kuasa                                                                                                                                                        
                      <div class='clearfix'></div>
                      <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 26 Nov 2018</small>
                    </div>
                  </div>
                </a>
                <a href="https://indonesia.go.id/ragam/pariwisata/kebudayaan/dari-matahari-turun-ke-bulan"  class='bdz-100 bdz-sub-header-item-sm'>
                  <div class='bdz-sub-header-item-container'>
                    <!-- <div class='bdz-sub-header-item-topik'>
                      Pariwisata                                                </div> -->
                    <img class='bdz-30' alt="Dari Matahari Turun ke Bulan"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1542942914_KALENDER_TRIBUN.jpeg&q=50&a=c&w=250&h=200">
                    <div class='bdz-70 bdz-sub-header-item-text-sm'>
                      Dari Matahari Turun ke Bulan                                                                                                                                                        
                      <div class='clearfix'></div>
                      <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 23 Nov 2018</small>
                    </div>
                  </div>
                </a>
                <a href="https://indonesia.go.id/ragam/budaya/kebudayaan/keragaman-kalender-indonesia"  class='bdz-100 bdz-sub-header-item-sm'>
                  <div class='bdz-sub-header-item-container'>
                    <!-- <div class='bdz-sub-header-item-topik'>
                      Budaya                                                </div> -->
                    <img class='bdz-30' alt="Keragaman Kalender Indonesia"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1542942943_KALENDER_2.jpeg&q=50&a=c&w=250&h=200">
                    <div class='bdz-70 bdz-sub-header-item-text-sm'>
                      Keragaman Kalender Indonesia                                                                                                                                                        
                      <div class='clearfix'></div>
                      <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 23 Nov 2018</small>
                    </div>
                  </div>
                </a>
              </div>
              <div class='bdz-25 '>
                <a href="https://indonesia.go.id/ragam/budaya/kebudayaan/rujak-kudapan-dengan-cita-rasa-nusantara"  class='bdz-100 bdz-sub-header-item-sm'>
                  <div class='bdz-sub-header-item-container'>
                    <img class='bdz-30' alt="Rujak, Kudapan dengan Cita Rasa Nusantara"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1543293155_Rujak,_Kudapan_dengan_Cita_Rasa_Nusantara_sportourismid.jpg&q=50&a=c&w=250&h=200">
                    <div class='bdz-70 bdz-sub-header-item-text-sm'>
                      Rujak, Kudapan dengan Cita Rasa Nusantara                                                                                                  
                      <div class='clearfix'></div>
                      <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                    </div>
                  </div>
                </a>
                <a href="https://indonesia.go.id/ragam/budaya/kebudayaan/tenun-ntt-harta-keluarga-yang-bernilai-tinggi"  class='bdz-100 bdz-sub-header-item-sm'>
                  <div class='bdz-sub-header-item-container'>
                    <img class='bdz-30' alt="Tenun NTT, Harta Keluarga yang Bernilai Tinggi "  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1543288737_Harta_Keluarga_yang_Bernilai_Tinggi.jpg&q=50&a=c&w=250&h=200">
                    <div class='bdz-70 bdz-sub-header-item-text-sm'>
                      Tenun NTT, Harta Keluarga yang Bernilai Tinggi                                                                                                   
                      <div class='clearfix'></div>
                      <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                    </div>
                  </div>
                </a>
                <a href="https://indonesia.go.id/ragam/budaya/kebudayaan/keragaman-tradisi-borneo-yang-memikat"  class='bdz-100 bdz-sub-header-item-sm'>
                  <div class='bdz-sub-header-item-container'>
                    <img class='bdz-30' alt="Keragaman Tradisi Borneo yang Memikat "  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1543283442_Keragaman_Tradisi_Borneo_yang_Memikat.jpg&q=50&a=c&w=250&h=200">
                    <div class='bdz-70 bdz-sub-header-item-text-sm'>
                      Keragaman Tradisi Borneo yang Memikat                                                                                                   
                      <div class='clearfix'></div>
                      <small><span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                    </div>
                  </div>
                </a>
              </div>
              <div class='bdz-25 '>
                <a href="https://indonesia.go.id/ragam/budaya/kebudayaan/karakteristik-keris" class='bdz-100 bdz-sub-header-item'>
                  <div class='bdz-sub-header-item-container'>
                    <!-- <div class='bdz-sub-header-item-topik'>
                      Budaya                                              </div>  -->
                    <img alt="Karakteristik Keris"  src="https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img/content_image/1543300417_Karakteristik_Keris.jpg&q=50&a=c&w=500&h=300">
                    <div class='bdz-sub-header-item-text-lg'>
                      Karakteristik Keris                                                                                                  
                      <div class='clearfix'></div>
                      <!--  <div class='bdz-sub-header-item-text-lg-resume'>
                        ... 
                        </div> -->
                      <div class='clearfix'></div>
                      <small>  <span class="glyphicon glyphicon-time" aria-hidden="true"></span> 27 Nov 2018</small>
                    </div>
                  </div>
                </a>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='bdz-25 '>
              </div>
              <div class='clearfix'></div>
            </div>
          </div>
        </div>
        </div>
        <div class='clearfix'></div>
        <script type="text/javascript" src="https://indonesia.go.id/assets/js/scrolify/jquery.scrollify.js" ></script>
        <style type="text/css">
          .top-slider-wrapper{bottom: 0;}
          /*.bdz-home-wrapper{padding-top: 90px;}*/
        </style>
        <script type="text/javascript">
          $(document).ready(function() {
          
          
            // $.scrollify({
            //   section : ".snap",
            //   sectionName : false,
            //   interstitialSection : "",
            //   easing: "easeOutExpo",
            //   scrollSpeed:2000,
            //   offset : -134,
            //   scrollbars: true,
            //   standardScrollElements: "",
            //   setHeights: false,
            //   overflowScroll: true,
            //   updateHash: true,
            //   touchScroll:true,
            //   before:function() {
            //     // if ($.scrollify.currentIndex() > 0) {
            //     //       $(".bdz-footer-wrapper").css({'display':'none'});
            //     //       $(".bdz-footer-wrapper").css({'position':'fixed'});
            //     //       $(".bdz-footer-wrapper").fadeIn();
            //     // }else{
            //     //       $(".bdz-footer-wrapper").fadeOut();
            //     //       $(".bdz-footer-wrapper").css({'position':'relative'});
            //     //       $(".bdz-footer-wrapper").css({'display':'none'});
            //     // }
            //   },
            //   after:function() {
          
            //   },
            //   afterResize:function() {},
            //   afterRender:function() {}
            // });
          });
        </script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.css">
        <script src="https://cdn.jsdelivr.net/bxslider/4.2.12/jquery.bxslider.min.js"></script>
        <script>
          $(document).ready(function(){
            $('.bxslider').bxSlider({
              pager:false
            });
            $('.li-edit div').hover(function(){
                // $(".ul-edit-submenu").fadeOut();
          
                 $(this).trigger('reset');
                  $(this).children("ul").show({
                      duration: 100,
                      easing: "swing",
                      complete: function () {
                          $(this).addClass("hover");
                      }
                  });
              }, function () {
                  var me = $(this);
                  var timeout = setTimeout(function () {
                      me.find(".hover").hide({
                          duration: 0,
                          complete: function () {
                              $(this).removeClass("hover");
                          }
                      });
                  }, 100);
                  
                  me.bind('reset', function () {
                      clearTimeout(timeout);
                  });
          
                // $(this).children(".ul-edit-submenu").fadeToggle();            
          
            });
            $(".ul-edit-submenu li").click(function(){
              var item = $(this).attr("data-action");
              var index = $(".ul-edit-submenu li[data-action='"+item+"']").index(this);
              if (item == "hapus") { $('.bdz-home-item-container').eq(index).remove() };
              if (item == "atas") { $('.bdz-home-item-container').eq(index).insertAfter($('.bdz-home-item-container').eq(index-2))  };
              if (item == "bawah") { $('.bdz-home-item-container').eq(index).insertAfter($('.bdz-home-item-container').eq(index + 1)) };
            });
          
          });
        </script>
        <div class='clearfix '></div>
        <script type="text/javascript">
          $(document).ready(function(){
            $(".bdz-banner-small").error(function() {
               $(this).attr("src","https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img//sample/a.jpg&q=100&a=c&w=1100&h=200");
            });
             $(".bdz-banner-small-top").error(function() {
               $(this).attr("src","https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img//sample/a.jpg&q=100&a=c&w=400&h=400");
            });
             $(".bdz-featured-img").error(function() {
               $(this).attr("src","https://indonesia.go.id/assets//js/timthumb/timthumb.php?src=assets/img//sample/a.jpg&q=100&a=c&w=300&h=300");
            });
            $(".imgLiquidFill").imgLiquid();
          });
        </script>
      </div>
      <!-- BDZ main container -->
    </div>
    <!-- BDZ main wrapper	-->
    <div class='bdz-footer-wrapper'>
      <div class='bdz-footer-container'>
        <div class='bdz-100'>
          <img class='bdz-header-menu-logo' alt="Logo Indonesia.go.id" src="https://indonesia.go.id/assets/img/logo.png">
        </div>
        <!-- <div class='bdz-80'>
          <h3><a href="https://indonesia.go.id/" class='red'>WWW.INDONESIA.GO.ID</a></h3>
          
          </div> -->
        <div class='bdz-20'>
          <!-- 		    
            <span class='bdz-header-menu-item-search'>
                <div class="input-group ">
                    <input type="text" class="form-control input-sm input-opa" placeholder="Cari pada website ini">
                    <div class="input-group-btn">
                      <button type="button" class="btn btn-danger btn-sm" aria-expanded="false"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></button>
                    </div>
                </div>  
            </span> -->
        </div>
        <div class='clearfix'></div>
        <div class='bdz-70 mobile-hide'>
          <div class='clearfix'></div>
          <h4>
            <a href="https://indonesia.go.id/" class=''>Beranda</a> |
            <a href="https://indonesia.go.id/profil" class=''>Profil</a> |
            <a href="https://indonesia.go.id/narasi" class=''>Narasi</a> |
            <a href="https://jpp.go.id" class=''>Berita</a> | 
            <a href="https://indonesia.go.id/layanan" class=''>Layanan</a> |
            <a href="https://indonesia.go.id/gallery" class=''>Galeri</a>  |
            <a href="https://indonesia.go.id/ragam" class=''>Ragam</a> 
          </h4>
        </div>
        <div class='bdz-30 '>
          <h4 class='right mobile-center'>
            <a href="https://indonesia.go.id/tentang-indonesia" class=''>Tentang Kami</a> |
            <a href="https://indonesia.go.id/tentang-indonesia/redaksi" class=''>Redaksi</a> |
            <a href="https://indonesia.go.id/tentang-indonesia/kontak" class=''>Kontak Kami</a> 
          </h4>
        </div>
        <div class='clearfix'></div>
        <div class='bdz-50'>
          <h5>Ditjen Informasi dan Komunikasi Publik</h5>
          <div class='left'>Copyright &copy 2017 indonesia.go.id. All Rights Reserved</div>
        </div>
        <div class='bdz-50'>
          <div class='right bdz-footer-company-info-right'>
            Kementerian Komunikasi dan Informatika.
            Jalan Medan Merdeka Barat 9 
            Jakarta Pusat-Indonesia, 
            Jakarta - 16512  
            <br>
            Phone: (021) 7534428, Fax: (021) 7534428				
          </div>
        </div>
      </div>
    </div>
  </body>
</html>