
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Perijinan Penelitian</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Perijinan Penelitian</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link" href="#tab_1" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link active" href="#tab_2" data-toggle="tab" id="klik_tab_tampil">Data</a></li>
                  
                  <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab" id="klik_tab_tampil_arsip">Arsip</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane" id="tab_1">
													
					<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=perijinan_penelitian" enctype="multipart/form-data">
					<h3 id="judul_formulir">FORMULIR INPUT</h3>
					<div class="row">
						<div class="col-sm-12 col-md-6">

							<div class="form-group" style="display:none;">
								<label for="temp">temp</label>
								<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="mode">mode</label>
								<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="id_perijinan_penelitian">id_perijinan_penelitian</label>
								<input class="form-control" id="id_perijinan_penelitian" name="id" value="" placeholder="id_perijinan_penelitian" type="text">
							</div>
							<div class="form-group">
								<label for="nomor_surat">Nomor Surat</label>
								<input class="form-control" id="nomor_surat" name="nomor_surat" value="" placeholder="nomor_surat" type="text">
							</div>
							<div class="form-group">
								<label for="perihal_surat">Perihal Surat</label>
								<!-- <input class="form-control" id="perihal_surat" name="perihal_surat" value="" placeholder="perihal_surat" type="text"> -->
								<select class="form-control" id="perihal_surat" name="perihal_surat" required >
									<option value="Ijin Penelitian">Ijin Penelitian</option>
									<option value="Survey Pendahuluan">Survey Pendahuluan</option>
								</select>
							</div>
							<div class="form-group">
								<label for="tanggal_surat">Tanggal Surat</label>
								<input class="form-control" id="tanggal_surat" name="tanggal_surat" value="" placeholder="tanggal_surat" type="text">
							</div>
							<div class="form-group">
								<label for="nama">Nama</label>
								<input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
							</div>
							<div class="form-group">
								<label for="nomot_telp">Nomot Telp</label>
								<input class="form-control" id="nomot_telp" name="nomot_telp" value="" placeholder="Nomot Telp" type="text" >
							</div>
							<div class="form-group">
								<label for="kebangsaan">Kebangsaan</label>
								<input class="form-control" id="kebangsaan" name="kebangsaan" value="" placeholder="Kebangsaan" type="text" >
							</div>
							<div class="form-group">
								<label for="alamat">Alamat</label>
								<input class="form-control" id="alamat" name="alamat" value="" placeholder="Alamat" type="text" >
							</div>
							<div class="form-group">
								<label for="pekerjaan">Pekerjaan</label>
								<input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="Pekerjaan" type="text" >
							</div>
							<div class="form-group">
								<label for="penanggung_jawab">Penanggung Jawab</label>
								<input class="form-control" id="penanggung_jawab" name="penanggung_jawab" value="" placeholder="Penanggung Jawab" type="text" >
							</div>
							<div class="form-group">
								<label for="judul_penelitian">Judul Penelitian</label>
								<textarea class="form-control" rows="3" id="judul_penelitian" name="judul_penelitian" value="" placeholder="" type="text" >
								</textarea>
							</div>
							<div class="form-group">
								<label for="lokasi">Lokasi</label>
								<input class="form-control" id="lokasi" name="lokasi" value="" placeholder="Lokasi" type="text" >
							</div>
							<div class="form-group">
								<label for="asal_universitas">Asal Universitas/ Instansi</label>
								<input class="form-control" id="asal_universitas" name="asal_universitas" value="" placeholder="Asal Universitas/ Instansi" type="text" >
							</div>
							<div class="form-group">
								<label for="nomor_surat_kesbangpol">Nomor Surat Kesbangpol</label>
								<input class="form-control" id="nomor_surat_kesbangpol" name="nomor_surat_kesbangpol" value="" placeholder="Nomor Surat Kesbangpol" type="text" >
							</div>
							<div class="form-group">
								<label for="tanggal_surat_kesbangpol">Tanggal Surat Kesbangpol</label>
								<input class="form-control" id="tanggal_surat_kesbangpol" name="tanggal_surat_kesbangpol" value="" placeholder="Tanggal Surat Kesbangpol" type="text" >
							</div>
							<div class="form-group">
								<label for="surat_ditujukan_kepada">Surat Ditujukan Kepada</label>
								<input class="form-control" id="surat_ditujukan_kepada" name="surat_ditujukan_kepada" value="" placeholder="Surat Ditujukan Kepada" type="text" >
							</div>
							<div class="form-group">
								<div class="alert alert-info alert-dismissable">
									<div class="form-group">
										<label for="remake">Pilih satu per satu hingga keduannya Anda Upload</label>
										<select class="form-control" id="remake" name="remake" >
										<option value="scan_ijin_dari_kesbangpol">Scan Ijin dari Kesbangpol</option>
										<option value="scan_halaman_judul_proposal">Scan Halaman Judul Proposal</option>
										</select>
									</div>
									<div class="form-group">
										<label for="myfile">Setelah pilih cari file disini</label>
										<input type="file" size="60" name="myfile" id="file_lampiran" >
									</div>
									<div id="ProgresUpload">
										<div id="BarProgresUpload"></div>
										<div id="PersenProgresUpload">0%</div >
									</div>
									<div id="PesanProgresUpload"></div>
								</div>
								<div class="alert alert-info alert-dismissable">
									<h3 class="card-title">Data Scan </h3>
									<table class="table table-bordered">
										<tr>
											<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
										</tr>
										<tbody id="tbl_lampiran_perijinan_penelitian">
										</tbody>
									</table>
								</div>
							</div>
							<div class="form-group">
								<label for="tembusan_kepada_1">Tembusan Kepada</label>
								<input class="form-control" id="tembusan_kepada_1" name="tembusan_kepada_1" value="" placeholder="Tembusan Kepada" type="text">
							</div>
							<div class="form-group">
								<input class="form-control" id="tembusan_kepada_2" name="tembusan_kepada_2" value="" placeholder="Tembusan Kepada" type="text">
							</div>
							<div class="form-group">
								<input class="form-control" id="tembusan_kepada_3" name="tembusan_kepada_3" value="" placeholder="Tembusan Kepada" type="text">
							</div>
							<div class="form-group">
								<input class="form-control" id="tembusan_kepada_4" name="tembusan_kepada_4" value="" placeholder="Tembusan Kepada" type="text">
							</div>
							<div class="form-group">
								<input class="form-control" id="tembusan_kepada_5" name="tembusan_kepada_5" value="" placeholder="Tembusan Kepada" type="text">
							</div>
							<div class="form-group">
								<input class="form-control" id="tembusan_kepada_6" name="tembusan_kepada_6" value="" placeholder="Tembusan Kepada" type="text">
							</div>

							<div class="form-group">
								<label for="header_ttd">PENANDATANGANAN SURAT :</label>								
								<br>					
								<label for="header_ttd">Header</label>
								<input class="form-control" id="header_ttd" name="header_ttd" value="" placeholder="Header Penandatangan" type="text">
							</div>
							<div class="form-group">
								<label for="jabatan_ttd">Jabatan</label>
								<input class="form-control" id="jabatan_ttd" name="jabatan_ttd" value="" placeholder="Jabatan Penandatangan" type="text">
							</div>
							<div class="form-group">
								<label for="nama_ttd">Nama</label>
								<input class="form-control" id="nama_ttd" name="nama_ttd" value="" placeholder="Nama Penandatangan" type="text">
							</div>
							<div class="form-group">
								<label for="jabatan_ttd">Jabatan Bawah</label>
								<input class="form-control" id="jabatan2_ttd" name="jabatan2_ttd" value="" placeholder="Jabatan Penandatangan" type="text">
							</div>
							<div class="form-group">
								<label for="nip_ttd">NIP</label>
								<input class="form-control" id="nip_ttd" name="nip_ttd" value="" placeholder="NIP Penandatangan" type="text">
							</div>
																</div>
															</div>
															<button type="submit" class="btn btn-primary" id="simpan_perijinan_penelitian">SIMPAN</button>
															<button type="submit" class="btn btn-primary" id="update_perijinan_penelitian" style="display:none;">UPDATE</button>
													</form>
													<div class="overlay" id="overlay_form_input" style="display:none;">
														<i class="fa fa-refresh fa-spin"></i>
													</div>
											
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane active table-responsive" id="tab_2">
                    
														<table class="table table-bordered table-hover">
															<thead>
																<th>Proses</th>
																<th>Status</th>
																<th>Nomor Surat</th>
																<th>Perihal</th>
																<th>Tanggal</th>
																<th>Nama</th>
																<th>Kebangsaan</th>
																<th>Alamat</th>
																<th>Pekerjaan</th>
																<th>Penanggung Jawab</th>
																<th>Lokasi</th>
																<th>Judul Penelitian</th>
																<th>Asal Universitas</th>
																<th>Nomor Surat Kesbangpol</th>
																<th>Tanggal Surat Kesbangpol</th>
																<th>Surat Ditujukan Kepada</th>
															</thead>
															<tbody id="tbl_utama_perijinan_penelitian">
															</tbody>
														</table>
														<ul class="pagination pagination-sm no-margin pull-right" id="pagination">
														</ul>
														
														<div class="overlay" id="spinners_data" style="display:none;">
															<i class="fa fa-refresh fa-spin"></i>
														</div>
											
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                    
														<table class="table table-bordered table-hover">
															<thead>
																<th>No</th>
																<th>Nama</th>
																<th>Judul Halaman</th>
																<th>Tempat</th> 
																<th>Status</th> 
																<th>Proses</th> 
															</thead>
															<tbody id="tbl_utama_perijinan_penelitian6">
															</tbody>
														</table>
														<ul class="pagination pagination-sm no-margin pull-right" id="pagination">
														</ul>
														<div class="overlay" id="spinners_data6" style="display:none;">
															<i class="fa fa-refresh fa-spin"></i>
														</div>
										
                  </div>

                  <div class="tab-pane" id="tab_4">
                    
														<table class="table table-bordered table-hover">
															<thead>
																<th>No</th>
																<th>Nama</th>
																<th>Judul Halaman</th>
																<th>Tempat</th> 
																<th>Status</th> 
															</thead>
															<tbody id="tbl_utama_perijinan_penelitian7">
															</tbody>
														</table>
														<ul class="pagination pagination-sm no-margin pull-right" id="pagination">
														</ul>
														<div class="overlay" id="spinners_data6" style="display:none;">
															<i class="fa fa-refresh fa-spin"></i>
														</div>
										
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_surat, #tanggal_surat_kesbangpol').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perijinan_penelitian').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_perijinan_penelitian').hide();
    $('#update_perijinan_penelitian').show();
    var id_perijinan_penelitian = $(this).closest('tr').attr('id_perijinan_penelitian');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_perijinan_penelitian');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_perijinan_penelitian').val(id_perijinan_penelitian);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_perijinan_penelitian:id_perijinan_penelitian
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>perijinan_penelitian/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#nomor_surat').val(json[i].nomor_surat);
            $('#perihal_surat').val(json[i].perihal_surat);
            $('#tanggal_surat').val(json[i].tanggal_surat);
            $('#nama').val(json[i].nama);
            $('#nomot_telp').val(json[i].nomot_telp);
            $('#kebangsaan').val(json[i].kebangsaan);
            $('#alamat').val(json[i].alamat);
            $('#pekerjaan').val(json[i].pekerjaan);
            $('#penanggung_jawab').val(json[i].penanggung_jawab);
            $('#lokasi').val(json[i].lokasi);
            $('#judul_penelitian').val(json[i].judul_penelitian);
            $('#asal_universitas').val(json[i].asal_universitas);
            $('#nomor_surat_kesbangpol').val(json[i].nomor_surat_kesbangpol);
            $('#tanggal_surat_kesbangpol').val(json[i].tanggal_surat_kesbangpol);
            $('#surat_ditujukan_kepada').val(json[i].surat_ditujukan_kepada);
            $('#tembusan_kepada_1').val(json[i].tembusan_kepada_1);
            $('#tembusan_kepada_2').val(json[i].tembusan_kepada_2);
            $('#tembusan_kepada_3').val(json[i].tembusan_kepada_3);
            $('#tembusan_kepada_4').val(json[i].tembusan_kepada_4);
            $('#tembusan_kepada_5').val(json[i].tembusan_kepada_5);
            $('#tembusan_kepada_6').val(json[i].tembusan_kepada_6);
            $('#header_ttd').val(json[i].judul_ttd);
            $('#jabatan_ttd').val(json[i].jabatan_ttd);
            $('#nama_ttd').val(json[i].nama_ttd);
            $('#jabatan2_ttd').val(json[i].jabatan2_ttd);
            $('#nip_ttd').val(json[i].nip_ttd);
          }
        }
      });
    AttachmentByMode(mode, value);
  });
});
</script>

<script>
  function AfterSavedPerijinan_penelitian() {
    $('#id_perijinan_penelitian, #nama, #nomor_surat, #perihal_surat, #tanggal_surat, #nomot_telp, #kebangsaan, #alamat, #pekerjaan, #penanggung_jawab, #lokasi, #judul_penelitian, #asal_universitas, #nomor_surat_kesbangpol #tanggal_surat_kesbangpol, #surat_ditujukan_kepada, #tembusan_kepada_1, #tembusan_kepada_2, #tembusan_kepada_3, #tembusan_kepada_4, #tembusan_kepada_5, #tembusan_kepada_6, #temp, #header_ttd, #jabatan_ttd, #nama_ttd, #nip_ttd').val('');
  	$('#tbl_lampiran_perijinan_penelitian').html('');
    $('#PesanProgresUpload').html('');
    }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1;
    var limit = 10;
	load_data_perijinan_penelitian(halaman, limit);
	load_data_perijinan_penelitian6(halaman, limit);
	load_data_perijinan_penelitian7(halaman, limit);
});
</script>
<script>
  function load_data_perijinan_penelitian(halaman, limit) {
    $('#tbl_utama_perijinan_penelitian').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>perijinan_penelitian/load_table/',
      success: function(html) {
        $('#tbl_utama_perijinan_penelitian').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>

<script>
  function load_data_perijinan_penelitian6(halaman, limit) {
    $('#tbl_utama_perijinan_penelitian6').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>perijinan_penelitian/load_table_perijinan_penelitian6/',
      success: function(html) {
        $('#tbl_utama_perijinan_penelitian6').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>

<script>
  function load_data_perijinan_penelitian7(halaman, limit) {
    $('#tbl_utama_perijinan_penelitian7').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>perijinan_penelitian/load_table_perijinan_penelitian7/',
      success: function(html) {
        $('#tbl_utama_perijinan_penelitian7').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perijinan_penelitian').on('click', '#del_ajax', function() {
    var id_perijinan_penelitian = $(this).closest('tr').attr('id_perijinan_penelitian');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_perijinan_penelitian"] = id_perijinan_penelitian;
        var url = '<?php echo base_url(); ?>perijinan_penelitian/hapus/';
        HapusData(parameter, url);
        $('[id_perijinan_penelitian='+id_perijinan_penelitian+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_perijinan_penelitian').show();
    $('#update_perijinan_penelitian').hide();
    $('#tbl_attachment_perijinan_penelitian').html('');
    $('#id_perijinan_penelitian, #nama, #nomor_surat, #perihal_surat, #tanggal_surat, #nomot_telp, #kebangsaan, #alamat, #pekerjaan, #penanggung_jawab, #lokasi, #judul_penelitian, #asal_universitas, #nomor_surat_kesbangpol #tanggal_surat_kesbangpol, #surat_ditujukan_kepada, #tembusan_kepada_1, #tembusan_kepada_2, #tembusan_kepada_3, #tembusan_kepada_4, #tembusan_kepada_5, #tembusan_kepada_6, #temp #header_ttd, #jabatan_ttd, #nama_ttd, #nip_ttd').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_perijinan_penelitian').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'nama', 'nomor_surat', 'perihal_surat', 'tanggal_surat', 'nomot_telp', 'kebangsaan', 'alamat', 'pekerjaan', 'penanggung_jawab', 'lokasi', 'judul_penelitian', 'asal_universitas', 'nomor_surat_kesbangpol', 'tanggal_surat_kesbangpol', 'surat_ditujukan_kepada', 'tembusan_kepada_1',  'tembusan_kepada_2',  'tembusan_kepada_3',  'tembusan_kepada_4', 'tembusan_kepada_5',  'tembusan_kepada_6', 'temp', 'header_ttd', 'jabatan_ttd', 'nama_ttd', 'jabatan2_ttd', 'nip_ttd' ];
	  InputValid(parameter);
      
      var parameter = {}
      parameter["nama"] = $("#nama").val();
      parameter["nomor_surat"] = $("#nomor_surat").val();
      parameter["perihal_surat"] = $("#perihal_surat").val();
      parameter["tanggal_surat"] = $("#tanggal_surat").val();
      parameter["nomot_telp"] = $("#nomot_telp").val();
      parameter["kebangsaan"] = $("#kebangsaan").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      parameter["penanggung_jawab"] = $("#penanggung_jawab").val();
      parameter["lokasi"] = $("#lokasi").val();
      parameter["judul_penelitian"] = $("#judul_penelitian").val();
      parameter["asal_universitas"] = $("#asal_universitas").val();
      parameter["nomor_surat_kesbangpol"] = $("#nomor_surat_kesbangpol").val();
      parameter["tanggal_surat_kesbangpol"] = $("#tanggal_surat_kesbangpol").val();
      parameter["surat_ditujukan_kepada"] = $("#surat_ditujukan_kepada").val();
      parameter["tembusan_kepada_1"] = $("#tembusan_kepada_1").val();
      parameter["tembusan_kepada_2"] = $("#tembusan_kepada_2").val();
      parameter["tembusan_kepada_3"] = $("#tembusan_kepada_3").val();
      parameter["tembusan_kepada_4"] = $("#tembusan_kepada_4").val();
      parameter["tembusan_kepada_5"] = $("#tembusan_kepada_5").val();
      parameter["tembusan_kepada_6"] = $("#tembusan_kepada_6").val();      
      parameter["temp"] = $("#temp").val();
      parameter["header_ttd"] = $("#header_ttd").val();
      parameter["jabatan_ttd"] = $("#jabatan_ttd").val();
      parameter["nama_ttd"] = $("#nama_ttd").val();
      parameter["jabatan2_ttd"] = $("#jabatan2_ttd").val();
      parameter["nip_ttd"] = $("#nip_ttd").val();
      parameter["id_perijinan_penelitian"] = $("#id_perijinan_penelitian").val();
      var url = '<?php echo base_url(); ?>perijinan_penelitian/update_perijinan_penelitian';
      
      var parameterRv = [ 'nama', 'nomor_surat', 'perihal_surat', 'tanggal_surat', 'nomot_telp', 'kebangsaan', 'alamat', 'pekerjaan', 'penanggung_jawab', 'lokasi', 'judul_penelitian', 'asal_universitas', 'nomor_surat_kesbangpol', 'tanggal_surat_kesbangpol', 'surat_ditujukan_kepada', 'tembusan_kepada_1',  'tembusan_kepada_2',  'tembusan_kepada_3',  'tembusan_kepada_4', 'tembusan_kepada_5',  'tembusan_kepada_6',  'temp', 'header_ttd', 'jabatan_ttd', 'nama_ttd', 'jabatan2_ttd', 'nip_ttd' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_lampiran_perijinan_penelitian').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'perijinan_penelitian',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>lampiran_perijinan_penelitian/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_lampiran_perijinan_penelitian="'+json[i].id_lampiran_perijinan_penelitian+'" id="'+json[i].id_lampiran_perijinan_penelitian+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_lampiran_perijinan_penelitian').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_perijinan_penelitian').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_lampiran_perijinan_penelitian').on('click', '#del_ajax', function() {
    var id_lampiran_perijinan_penelitian = $(this).closest('tr').attr('id_lampiran_perijinan_penelitian');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_lampiran_perijinan_penelitian"] = id_lampiran_perijinan_penelitian;
        var url = '<?php echo base_url(); ?>lampiran_perijinan_penelitian/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_perijinan_penelitian').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_lampiran_perijinan_penelitian='+id_lampiran_perijinan_penelitian+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perijinan_penelitian').on('click', '#verif_ajax', function() {
    var id_perijinan_penelitian = $(this).closest('tr').attr('id_perijinan_penelitian');
    alertify.confirm('Anda yakin verifikasi data ini?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_perijinan_penelitian"] = id_perijinan_penelitian;
        var url = '<?php echo base_url(); ?>perijinan_penelitian/verifikasi/';
        HapusData(parameter, url);
        $('[id_perijinan_penelitian='+id_perijinan_penelitian+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perijinan_penelitian6').on('click', '#selesai_ajax', function() {
    var id_lampiran_perijinan_penelitian = $(this).closest('tr').attr('id_lampiran_perijinan_penelitian');
    alertify.confirm('Anda yakin sudah selesai penelitian?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_lampiran_perijinan_penelitian"] = id_lampiran_perijinan_penelitian;
        var url = '<?php echo base_url(); ?>perijinan_penelitian/selesai/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_perijinan_penelitian').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_lampiran_perijinan_penelitian='+id_lampiran_perijinan_penelitian+']').remove();
      } else {
        alertify.error('data dibatalkan');
      }
    });
  });
});
</script>