<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Document</title>
  <style type="text/css">

      h1 {
        font-family:arial;
        margin-bottom:2px;
        margin-left:50px;
        font-size: 35px;
        text-align: center;
        text-justify: inter-word;
        margin-right: 30px; 

      }

      h2 {
        font-family:arial;
        margin-bottom:2px;
        margin-left:10px;
        font-size: 20px;
        text-align: justify;
        text-justify: inter-word;
        margin-right: 30px; 
      }

       h3 {
        font-family:arial;
        margin-left:80px;
        font-size: 20px;
        margin-bottom:2px;
      }

      h4 {
        font-family:arial;
        margin-bottom:2px;
        margin-left:10px;
        font-size: 20px;
      }     

      h6 {
        font-family:arial;
        margin-left:80px;
        font-size: 20px;
        margin-bottom:2px;
      }
      h5 {
        font-family:arial;
        text-align: justify;
        text-justify: inter-word;
        margin-left:200px;
        font-size: 20px;
        margin-bottom:2px;        
      }

      p {
        font-family:arial;
        text-align: justify;
        text-justify: inter-word;
        margin-left:200px;
        font-size: 20px;
        margin-bottom:2px;
        margin-right: 30px; 
      }

      hr {          
          width: 100%;
          border: 0;
          border-top: 6px double #000000;
      }

      @media print {
        @page { margin: 0; }
        body { margin: 1.6cm; }
      }
  </style>
</head>
<body>

    <table>
        <tr>
               <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td>
                  <img src="<?php echo base_url(); ?>media/lampiran_perijinan_penelitian/logo-wonosobo-black.jpg" alt="Italian Trulli">
              </td>
            
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
            
            <td align="center" style="font-weight:bold">
                <h4>PEMERINTAH KABUPATEN WONOSOBO</h4>
                <h1>DINAS KESEHATAN</h1>
                <h4>Alamat : Jl. T.Jogonegoro 2-4 Telp./Faks. : <?php if(!empty($telpon)){ echo $telpon; } ?> / 321319<br>
                Email : <?php if(!empty($email)){ echo $email; } ?> <br>
                Wonosobo-56311 </h4>
            </td>
        </tr>        
    </table>
    <hr/>
    <table border="0">
        <tr>
          <td><h3></h3></td>
          <td><h4></h4></td>
          <td><h4></h4></td>
          <td style="opacity: 0">..............</td>
          <td style="opacity: 0">..............</td>
          <td colspan="3"><h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Wonosobo, <?php if(!empty($tanggal_surat)){ echo $tanggal_surat; } ?></h6></td>
          
        </tr>

        <tr>
          <td><h3></h3></td>
          <td><h4></h4></td>
          <td><h4></h4></td>
          <td></td>
          <td><h6>&nbsp;</h6></td>
          <td></td>
          <td style="opacity: 0">..............</td>
          <td style="opacity: 0">.......................</td>
        </tr>

        <tr>
          <td><h3>Nomor</h3></td>
          <td><h4>:</h4></td>
          <td><h4></h4></td>
          <td></td>
          <td style="opacity: 0">..............</td>
          <td colspan="3"><h6> </h6></td>
          
        </tr>

        <tr>
          <td valign="top"><h3>Lampiran</h3></td>
          <td><h4>:</h4></td>
          <td><h4>-</h4></td>
          <td style="opacity: 0">..............</td>
          <td valign="top"></td>
          <td valign="top" colspan="4"  width="250px"><h6>Kepada Yth. </h6></td>
          
        </tr>

        <tr>
          <td valign="top"><h3>Perihal</h3></td>
          <td valign="top"><h4>:</h4></td>
          <td valign="top" colspan="2"><h4>Keterangan Penelitian</h4></td>
          
          <td style="opacity: 0">..............</td>
          <td valign="top" colspan="3"  width="250px"><h6><?php if(!empty($surat_ditujukan_kepada)){ echo $surat_ditujukan_kepada; } ?></h6></td>
          
        </tr>        

        <tr>
          <td><h3></h3></td>
          <td><h4></h4></td>
          <td><h4></h4></td>
          <td></td>
          <td style="opacity: 0">..............</td>
          <td colspan="3"><h6> di <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WONOSOBO</h6></td>
          
        </tr>
        <tr>
          <td><h3></h3></td>
          <td><h4></h4></td>
          <td><h4></h4></td>
          <td></td>
          <td style="opacity: 0">..............</td>
          <td><h6>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6></td>
          <td style="opacity: 0">.............................................................</td>
        </tr>
    </table>
    
    <table>
        <tr>

            <td><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Menindaklanjuti dan memperhatikan surat dari <?php if(!empty($asal_universitas)){ echo $asal_universitas; } ?> Nomor : <?php if(!empty($nomor_surat_kesbangpol)){ echo $nomor_surat_kesbangpol; } ?> tanggal <?php if(!empty($tanggal_surat_kesbangpol)){ echo $tanggal_surat_kesbangpol; } ?> perihal Permohonan Ijin Penelitian yang akan dilaksanakan oleh :</p></td>
        </tr>
        <tr>

            <td>&nbsp;</td>
        </tr>
    </table>

    <table>
        <tr>
            <td valign="top"><h5>1.</h5></td>
            <td valign="top"><h4>Nama</h4></td>
            <td valign="top"><h4>:</h4></td>
            <td valign="top"><h2><?php if(!empty($nama)){ echo $nama; } ?></h2></td>            
        </tr>
        <tr>
            <td valign="top"><h5>2.</h5></td>
            <td valign="top"><h4>Kebangsaan</h4></td>
            <td valign="top"><h4>:</h4></td>
            <td valign="top"><h2><?php if(!empty($kebangsaan)){ echo $kebangsaan; } ?></h2></td>            
        </tr>
        <tr>
            <td valign="top"><h5>3.</h5></td>
            <td valign="top"><h4>Alamat</h4></td>
            <td valign="top"><h4>:</h4></td>
            <td valign="top"><h2><?php if(!empty($alamat1)){ echo $alamat1; } ?></h2></td>            
        </tr>
        <tr>
            <td valign="top"><h5>4.</h5></td>
            <td valign="top"><h4>Pekerjaan</h4></td>
            <td valign="top"><h4>:</h4></td>
            <td valign="top"><h2><?php if(!empty($pekerjaan)){ echo $pekerjaan; } ?></h2></td>            
        </tr>
        <tr>
            <td valign="top"><h5>5.</h5></td>
            <td valign="top" width="20%"><h4>Penanggung Jawab</h4></td>
            <td valign="top"><h4>:</h4></td>
            <td valign="top"><h2><?php if(!empty($penanggung_jawab)){ echo $penanggung_jawab; } ?></h2></td>            
        </tr>
        <tr>
            <td valign="top"><h5>6.</h5></td>
            <td valign="top"><h4>Judul Penelitian</h4></td>
            <td valign="top"><h4>:</h4></td>
            <td valign="top"><h2><?php if(!empty($judul_penelitian)){ echo $judul_penelitian; } ?></h2></td>            
        </tr>
        <tr>
            <td valign="top"><h5>7.</h5></td>
            <td valign="top"><h4>Lokasi</h4></td>
            <td valign="top"><h4>:</h4></td>
            <td valign="top"><h2><?php if(!empty($lokasi)){ echo $lokasi; } ?></h2></td>            
        </tr>
        <tr>
            <td valign="top"></td>
            <td valign="top"></td>
            <td valign="top"></td>
            <td valign="top">&nbsp;</td>            
        </tr>
    </table>

    <table>
        <tr>
            <td><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bersama ini kami sampaikan bahwa Permohonan Ijin Penelitian tersebut diijinkan dengan ketentuan yang bersangkutan wajib mentaati peraturan, tata tertib dan norma-norma yang berlaku di daerah setempat.</p></td>
        </tr>
        <tr>
            <td><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian untuk menjadikan perhatian, dan untuk dilaksanakan.</p></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>

    <table>
      <tr>
        <td align="center">
            <h6>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <?php if(!empty($judul_ttd)){ echo $judul_ttd; }else{ echo "kosong";} ?> 
            </h6>
        </td>
      </tr>
       <tr>
        <td align="center">
            <h6>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              KABUPATEN WONOSOBO 
            </h6>
        </td>
      </tr>
       <tr>
        <td align="center">
            <h6>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

              <?php if(!empty($jabatan_ttd)){ echo $jabatan_ttd; }else{ echo "kosong";} ?>
            </h6>
        </td>
      </tr>     

      <?php 
          for ($i=0; $i < 3 ; $i++) { 
            ?>
                <tr>
                <td align="center">
                    <h6>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      
                    </h6>
                </td>
              </tr>
            <?php 
          }
       ?>

       <tr>
        <td align="center">
            <h6>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <u><?php if(!empty($nama_ttd)){ echo $nama_ttd; }else{ echo "kosong";} ?></u> 
            </h6>
        </td>
      </tr>
       <tr>
        <td align="center">
            <h6>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <?php if(!empty($jabatan2_ttd)){ echo $jabatan2_ttd; }else{ echo "";} ?>
            </h6>
        </td>
      </tr>
       <tr>
        <td align="center">
            <h6>
               &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              NIP. <?php if(!empty($nip_ttd)){ echo $nip_ttd; }else{ echo "kosong";} ?>
            </h6>
        </td>
      </tr>

    </table>

    <table>
         <tr>
          <td><h3>Tembusan disampaikan kepada Yth. :</h3></td>                   
        </tr>
        <tr>
          <td><h3>
              <?php if(!empty($tembusan_kepada_1)){
              if ($tembusan_kepada_1 == "-") {
                    echo "1. -";
              }else{ 
                    echo "1. ".$tembusan_kepada_1; 
              }
            } ?>
          </h3></td>                   
        </tr>
        <tr>
          <td><h3>
              <?php if(!empty($tembusan_kepada_2)){
              if ($tembusan_kepada_2 == "-") {
                    echo "";
              }else{ 
                    echo "2. ".$tembusan_kepada_2; 
              }
            } ?>
          </h3></td>                   
        </tr>
        <tr>
          <td><h3>
              <?php if(!empty($tembusan_kepada_3)){
              if ($tembusan_kepada_3 == "-") {
                    echo "";
              }else{ 
                    echo "3. ".$tembusan_kepada_3; 
              }
            } ?>
          </h3></td>                   
        </tr>
        <tr>
          <td><h3>
              <?php if(!empty($tembusan_kepada_4)){
              if ($tembusan_kepada_4 == "-") {
                    echo "";
              }else{ 
                    echo "4. ".$tembusan_kepada_4; 
              }
            } ?>
          </h3></td>                   
        </tr>
        <tr>
          <td><h3>
              <?php if(!empty($tembusan_kepada_5)){
              if ($tembusan_kepada_5 == "-") {
                    echo "";
              }else{ 
                    echo "5. ".$tembusan_kepada_5; 
              }
            } ?>
          </h3></td>                   
        </tr>
        <tr>
          <td><h3>
              <?php if(!empty($tembusan_kepada_6)){
              if ($tembusan_kepada_6 == "-") {
                    echo "";
              }else{ 
                    echo "6. ".$tembusan_kepada_6; 
              }
            } ?>
          </h3></td>                   
        </tr>


    </table>
  
</body>
</html>