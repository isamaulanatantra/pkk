<table>
  <tr>
    <th colspan="0">
      <img src="<?php echo base_url(); ?>media/lampiran_perijinan_penelitian/logo-wonosobo-black.jpg" alt="Italian Trulli">
    </th>
    <th colspan="1" style="width:100%;height:200px;">
        <center>
        <h2>PEMERINTAH KABUPATEN WONOSOBO</h2>
        <h1>DINAS KESEHATAN</h1>
        Alamat : Jl. T.Jogonegoro 2-4 Telp./Faks. : <?php if(!empty($telpon)){ echo $telpon; } ?> / 321319<br>
        Email : <?php if(!empty($email)){ echo $email; } ?> <br>
        Wonosobo-56311 <br>
        </center>
    </th>
  </tr>
  <tr>
    <td colspan="5">
    <hr />
  </td>
  </tr>
  <tr>
    <td colspan="5">
      <table style="width:100%;height:700px;">
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td colspan="2"><center>Wonosobo, <?php if(!empty($tanggal_surat)){ echo $tanggal_surat; } ?></center><br></td>
        </tr>
        <tr>
          <td>&nbsp;&nbsp;&nbsp;Nomor</td>
          <td>: <?php if(!empty($nomor_surat)){ echo $nomor_surat; } ?></td>
          <td></td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Kepada Yth. :</td>
          <td></td>
        </tr>
        <tr>
          <td>&nbsp;&nbsp;&nbsp;Lampiran</td>
          <td>: -</td>
          <td></td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if(!empty($surat_ditujukan_kepada)){ echo $surat_ditujukan_kepada; } ?></td>
          <td></td>
        </tr>
        <tr>
          <td>&nbsp;&nbsp;&nbsp;Perihal</td>
          <td colspan="2">: <?php if(!empty($perihal_surat)){ echo $perihal_surat; } ?></td>
          <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Di</td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td></td>
          <td></td>
          <td><center>WONOSOBO</center></td>
          <td></td>
        </tr>        
        <tr>
          <td></td>


          <td colspan="5"><br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Menindaklanjuti dan memperhatikan Surat Kepala Kantor Kesatuan Bangsa dan Politik Nomor : <?php if(!empty($nomor_surat_kesbangpol)){ echo $nomor_surat_kesbangpol; } ?> tanggal <?php if(!empty($tanggal_surat_kesbangpol)){ echo $tanggal_surat_kesbangpol; } ?> perihal Permohonan Ijin Penelitian yang akan dilaksanakan oleh :</td>
        </tr>
        <tr>
          <td></td>
          <td style="width:250px;">1. Nama</td>
          <td>:</td>
          <td><?php if(!empty($nama)){ echo $nama; } ?></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>2. Kebangsaan</td>
          <td>:</td>
          <td><?php if(!empty($kebangsaan)){ echo $kebangsaan; } ?></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>3. Alamat</td>
          <td>:</td>
          <td><?php if(!empty($alamat)){ echo $alamat; } ?></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>4. Pekerjaan</td>
          <td>:</td>
          <td><?php if(!empty($pekerjaan)){ echo $pekerjaan; } ?></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>5. Penanggung Jawab</td>
          <td>:</td>
          <td><?php if(!empty($penanggung_jawab)){ echo $penanggung_jawab; } ?></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>6. Judul Penelitian</td>
          <td>:</td>
          <td><?php if(!empty($judul_penelitian)){ echo $judul_penelitian; } ?></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td>7. Lokasi</td>
          <td>:</td>
          <td><?php if(!empty($lokasi)){ echo $lokasi; } ?></td>
          <td></td>
        </tr>
        <tr>
          <td></td>
          <td colspan="4"><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Bersama ini kami sampaikan bahwa Permohonan Ijin Penelitian tersebut diijinkan dengan ketentuan yang bersangkutan wajib mentaati peraturan, tata tertib dan norma-norma yang berlaku di daerah setempat.
        </td>
        </tr>
        <tr>
          <td></td>
          <td colspan="4">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian untuk menjadikan periksa, dan atas kerjasamanya<br>
          kami sampaikan terimakasih.
        </td>
        </tr>
        <tr>
          <td colspan="3"></td>
          <td colspan="2"><br><center>An. KEPALA DINAS KESEHATAN<br>KABUPATEN WONOSOBO<br>
            Sekretaris<br><br><br><br>
          <u>dr. MOHAMMAD RIYATNO, M.Kes</u><br>

          Pembina <br>NIP. 19690405 200212 1 006</center></td>
        </tr>
        <tr>
          <td colspan="3">&nbsp;&nbsp;&nbsp;Tembusan disampaikan kepada Yth. :<br>
          &nbsp;&nbsp;&nbsp;
            <?php if(!empty($tembusan_kepada_1)){
              if ($tembusan_kepada_1 == "-") {
                    echo "1. -";
              }else{ 
                    echo "1. ".$tembusan_kepada_1; 
              }
            } ?>
            <br>
          &nbsp;&nbsp;&nbsp;
            <?php if(!empty($tembusan_kepada_2)){
              if ($tembusan_kepada_2 == "-") {
                    echo "";
              }else{ 
                    echo "2. ".$tembusan_kepada_2; 
              }
            } ?>
            <br>
          &nbsp;&nbsp;&nbsp;
            <?php if(!empty($tembusan_kepada_3)){
              if ($tembusan_kepada_3 == "-") {
                    echo "";
              }else{ 
                    echo "3. ".$tembusan_kepada_3; 
              }
            } ?>
            <br>
          &nbsp;&nbsp;&nbsp;
            <?php if(!empty($tembusan_kepada_4)){
              if ($tembusan_kepada_4 == "-") {
                    echo "";
              }else{ 
                    echo "4. ".$tembusan_kepada_4; 
              }
            } ?>
            <br>
          &nbsp;&nbsp;&nbsp;
            <?php if(!empty($tembusan_kepada_5)){
              if ($tembusan_kepada_5 == "-") {
                    echo "";
              }else{ 
                    echo "5. ".$tembusan_kepada_5; 
              }
            } ?>
            <br>
          &nbsp;&nbsp;&nbsp;
            <?php if(!empty($tembusan_kepada_6)){
              if ($tembusan_kepada_6 == "-") {
                    echo "";
              }else{ 
                    echo "6. ".$tembusan_kepada_6; 
              }
            } ?>
            <br>
          
          </td>
          <td colspan="2"></td>
        </tr>
      </table>
    </td>
  </tr>
</table>
