
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>LOKASI</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Lokasi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header d-flex p-0">
              </div><!-- /.card-header -->
              <div class="card-body">
					<div class="msg" style="display:none;"><?php // echo @$this->session->flashdata('msg'); ?></div>
					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
								<label>Zonasi</label>
								<select class="form-control tampil-zona">
									<option>Silahkan pilih</option>
									<option>Dalam Kota</option>
									<option>Luar Kota</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Batas Wilayah</label>
								<select class="form-control tampil-wilayah">
									<option>Silahkan pilih</option>
									<option>Kabupaten</option>
									<option>Kecamatan</option>
									<option>AOI Kota</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label>Menara</label>
								<select class="form-control tampil-menara">
									<option>Silahkan pilih</option>
									<option>MENARA ALL</option>
									<option>CENTRATAMA</option>
									<option>H3I</option>
									<option>IBS</option>
									<option>INDOSAT</option>
									<option>LAKSMANA SWASTI PRASIDA</option>
									<option>MITRATEL</option>
									<option>POLRI</option>
									<option>PROTELINDO</option>
									<option>PST</option>
									<option>STI</option>
									<option>STP</option>
									<option>TBG</option>
									<option>TELKOM</option>
									<option>TELKOMSEL</option>
									<option>TVRI</option>
									<option>XL</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<button class="btn btn-primary bersih-layar" data-id=""> Bersihkan Layar</button>
							</div>
						</div>
					</div>
              </div><!-- /.card-body -->
            </div><!-- ./card -->
			
            <div class="card">
              <div class="card-body p-0">
				<div id="map-canvas"></div>
              </div><!-- /.card-body -->
            </div><!-- ./card -->
				  
          </div><!-- /.col -->
        </div><!-- /.row -->
				
    </section>
