
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>MENARA</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Menara</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header d-flex p-0">
              </div><!-- /.card-header -->
              <div class="card-body">
				  
    <div id="canform" class="form-group">
      <form name="coordinput" id="coordinput">
        <div class="indiv">
          <label class="inlabel">Latitude:</label>
        </div>
        <div class="indivinput">
          <input type="text" name="lat" id="markerLat" size="8"/><span style="color:#555;font-size:11px; font-style:italic"> (misal: -7.3587905 )</span>
        </div>
        <div class="indiv">
          <label class="inlabel">Longitude:</label>
        </div>
        <div class="indivinput">
          <input type="text" name="lng" id="markerLng" size="8"/><span style="color:#555;font-size:11px; font-style:italic"> (misal: 109.9030928)</span>
        </div>
        <div class="indiv">
          <label class="inlabel">Tinggi(m):</label>
        </div>
        <div class="indivinput">
          <input type="text" name="height" id="towerHeight" size="4"/>
        </div>
        <input type="button" value="OK" name="submit" class="submit" id="crdbtn"/>
        <div id="errordiv" style="width:89%; border:1px solid #fc4e01; padding:2px 2px 2px 3px; color:#666; font-size:12px; display:none"></div>
      </form>
    </div>
    <div class="form-group">
      <input type = "button" class="button" id = "addruler" value = "Tampilkan Pengukur Jarak" onclick = "if(this.value=='Tampilkan Pengukur Jarak') { addruler()} else {addruler(true); this.value ='Tampilkan Pengukur Jarak'} "/>
    </div>
  <div class="form-group">
	<form method="post" action="<?php echo base_url(); ?>peta">
		<div class="form-group">
			<label class="inlabel">Kecamatan:</label>
		</div>
		<div class="form-group">
			<select id="id_kecamatan" name="id_kecamatan" >
			</select>
		</div>
		<div class="form-group">
			<button type="submit" id="tampil_custom" class="button"><i class="fa fa-search"></i>  Tampilkan !!</button>
			<!--<input type = "button" class="button" id = "addteritorial" value = "Tampilkan Teritorial" onclick = "if(this.value=='Tampilkan Teritorial') { addteritorial()} else {addteritorial(true); this.value ='Tampilkan Teritorial'} "/>-->
		</div>
    </form>
  </div>
              </div><!-- /.card-body -->
            </div><!-- ./card -->
			
            <div class="card">
              <div class="card-body p-0">
				  
				<div id="map_nya"></div>
              </div><!-- /.card-body -->
            </div><!-- ./card -->
				  
          </div><!-- /.col -->
        </div><!-- /.row -->
				
    </section>

<script type="text/javascript">
$(document).ready(function() {
	$('#id_kecamatan').html('');
	var id_kabupaten = 1;
	$.ajax({
		type: "POST",
		async: true,
		data: {
			id_kabupaten:id_kabupaten
		},
		dataType: "html",
		url: '<?php echo base_url(); ?>lokasi/option_kecamatan/',
		success: function(html) {
			$('#id_kecamatan').append(html);
		}
	});
});
</script>

<script>
	function load_peta_kabupaten() {
		$('#map_nya').html('');
		<?php
		$id_kecamatan=$this->input->post('id_kecamatan');
		// $id_kecamatan='semua';
		if( $id_kecamatan > 0 ){
		$inputan = 
			'
			id_kabupaten:1,
			id_kecamatan:'.$this->input->post('id_kecamatan').'
			';
		}
		else{
		echo " var id_kecamatan = 'semua';";
		$inputan = 
			'
			id_kabupaten:1
			';
		}
		?>
		$.ajax({
			type: "POST",
			async: true,
			data: {
				<?php echo $inputan; ?>
			},
			dataType: "html",
			url: '<?php echo base_url(); ?>lokasi/peta_kita/',
			success: function(html) {
				$('#map_nya').append(html);
			}
		});
	}
</script>

<script type="text/javascript">
$(document).ready(function() {
	load_peta_kabupaten();
});
</script>
