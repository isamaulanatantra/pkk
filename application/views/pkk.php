<!-- === BEGIN HEADER === -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class=" js no-touch cssanimations">
<!--<![endif]-->

<head>
	<!-- Title -->
	<title><?php if (!empty($keterangan)) {
				echo $keterangan;
			} ?></title>
	<!-- Meta -->
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="<?php if (!empty($keterangan)) {
											echo $keterangan;
										} ?>">
	<meta name="author" content="<?php if (!empty($keterangan)) {
										echo $keterangan;
									} ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	<meta name="keywords" content="<?php if (!empty($keterangan)) {
										echo $keterangan;
									} ?>, <?php if (!empty($domain)) {
												echo $domain;
											} ?>,">
	<meta name="description" content="<?php if (!empty($keterangan)) {
											echo $keterangan;
										} ?>, <?php if (!empty($domain)) {
													echo $domain;
												} ?>">
	<meta name="og:description" content="<?php if (!empty($keterangan)) {
												echo $keterangan;
											} ?>, <?php if (!empty($domain)) {
														echo $domain;
													} ?>" />
	<meta name="og:url" content="<?php echo base_url(); ?>" />
	<meta name="og:title" content="<?php if (!empty($keterangan)) {
										echo $keterangan;
									} ?>" />
	<meta name="og:image" content="<?php echo base_url(); ?>media/upload/Logo PKK.png" />
	<meta name="og:keywords" content="<?php if (!empty($keterangan)) {
											echo $keterangan;
										} ?>, <?php if (!empty($domain)) {
													echo $domain;
												} ?>" />
	<!-- Favicon -->
	<link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/upload/Logo PKK.png" type="image/png" />

	<?php
	$web = $this->uut->namadomain(base_url());
	?>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>salique-wide/css/style.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>salique-wide/css/icons.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>salique-wide/css/slider.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>salique-wide/css/skinblue.css" /><!-- change skin color -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>salique-wide/css/responsive.css" />
	<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>

</head>
<!-- Body BEGIN -->

<body>

	<div class="grid">
		<div class="row space-bot">
			<!--Logo-->
			<div class="c3">
				<a href="<?php echo base_url(); ?>">
					<img src="<?php echo base_url(); ?>media/upload/logo pkk wonosobo.png" class="logo" alt="" style="padding-top:5px;">
				</a>
			</div>
			<!--Menu-->
			<div class="c9">
				<nav id="topNav">
					<?php echo '' . $menu_atas . ''; ?>
				</nav>
			</div>
		</div>
	</div>

	<div class="undermenuarea">
		<div class="boxedshadow">
		</div>
		<!-- SLIDER AREA
	================================================== -->
		<div id="da-slider" class="da-slider">
			<?php
			$where1 = array(
				'status' => 1,
				'domain' => $web
			);
			$this->db->where($where1);
			$this->db->order_by('created_time desc');
			$query1 = $this->db->get('slide_website');
			$a = 0;
			foreach ($query1->result() as $row1) {
				echo '
                      <div class="da-slide">
                        <h2></h2>
                        <p style="font-size:24px;">' . $row1->keterangan . '</p>
                        <a href="' . $row1->url_redirection . '" class="da-link">';
				if (!empty($keterangan)) {
					echo $keterangan;
				}
				echo '</a>
                        <div class="da-img">
                          <div class="added-images"><img src="' . base_url() . 'media/upload/' . $row1->file_name . '" class="z-depth-1"></div>
                        </div>
                      </div>
                        ';
			}
			?>
			<nav class="da-arrows">
				<span class="da-arrows-prev"></span>
				<span class="da-arrows-next"></span>
			</nav>
		</div>
	</div>
	<!-- UNDER SLIDER - BLACK AREA
================================================== -->
	<div class="undersliderblack">
		<div class="grid">
			<div class="row space-bot">
				<div class="c12">
					<!--Box 1-->
					<div class="c3 introbox introboxfirst">
						<div class="introboxinner">
							<span class="homeicone">
								<i class="icon-bolt"></i>
							</span>
							<b><a href="<?php echo base_url(); ?>postings/detail/1033747/Anggota_PKK.HTML"><u style="font-size:24px;">Anggota PKK</u></a></b>
							<br />
							Pengurusan PKK.
						</div>
					</div>
					<!--Box 2-->
					<div class="c3 introbox introboxmiddle">
						<div class="introboxinner">
							<span class="homeicone">
								<i class="icon-cog"></i>
							</span>
							<b><a href="<?php echo base_url(); ?>postings/detail/1033748/Prestasi_PKK.HTML"><u style="font-size:24px;">Prestasi PKK</u></a></b>
							<br />
							Kabupaten Wonosobo.

						</div>
					</div>
					<!--Box 3-->
					<div class="c3 introbox introboxlast">
						<div class="introboxinner">
							<span class="homeicone">
								<i class="icon-lightbulb"></i>
							</span>
							<b><a href="<?php echo base_url(); ?>postings/detail/1033721/Agenda.HTML"><u style="font-size:24px;">Agenda PKK</u></a></b>
							<br />
							Agenda Kegiatan PKK
						</div>
					</div>
					<div class="c3 introbox introboxlast">
						<div class="introboxinner">
							<span class="homeicone">
								<i class="icon-sitemap"></i>
							</span>
							<b><a href="<?php echo base_url(); ?>login"><u style="font-size:24px;">SIM PKK</u></a></b>
							<br />
							Sistem Informasi Manajemen PKK
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="shadowunderslider">
	</div>

	<!-- START content area
================================================== -->
	<div class="grid">
		<div class="row space-bot">
			<!--INTRO-->
			<div class="c12">
				<div class="royalcontent">

					<?php if (!empty($KolomKiriAtas)) {
						echo $KolomKiriAtas;
					} ?>

				</div>
			</div>
		</div>
		<!--<div class="shadowundertop"></div>-->
		<div class="row">
			<div class="c12">
				<h1 class="maintitle ">
					<span>INFORMASI PILIHAN</span>
				</h1>
				<div class="row">
					<div class="c12">
						<?php if (!empty($galery_berita)) {
							echo $galery_berita;
						} ?>
					</div>
				</div>
				<h1>
				</h1>
				<div class="row">
					<div class="c12">
						<?php $this->load->view($main_view);  ?>
					</div>
				</div>
			</div><!--
			<div class="c4">
        <?php //if(!empty( $KolomPalingBawah )){ echo $KolomPalingBawah; } 
		?>
			</div>-->
		</div>

	</div><!-- end grid -->


	<!-- FOOTER
================================================== -->
	<div id="wrapfooter">
		<div class="grid">
			<div class="row" id="footer">
				<!-- to top button  -->
				<p class="back-top floatright">
					<a href="#top"><span></span></a>
				</p>
				<!-- 1st column -->
				<div class="c3">
					<img src="<?php echo base_url(); ?>media/Logo PKK.png" class="logo" alt="" style="padding-top: 50px; width:75%;">
					<?php if (!empty($KolomKiriBawah)) {
						echo $KolomKiriBawah;
					} ?>
				</div>
				<!-- 2nd column -->
				<div class="c3">
					<div>
						<i class="fa fa-bar-chart-o"></i> <span id="hit_counter"></span>
					</div>
					<div>
						<i class="fa fa-bar-chart-o"></i> <span id="visitor"></span>
					</div>
				</div>
				<!-- 3rd column -->
				<div class="c3">
					<h2 class="title"><i class="icon-envelope-alt"></i> Kontak Kami</h2>
					<hr class="footerstress">
					<dl>
						<dt><?php if (!empty($alamat)) {
								echo $alamat;
							} ?></dt>
						<dd><span>Telephone: </span><?php if (!empty($telpon)) {
														echo $telpon;
													} ?></dd>
						<dd>E-mail: <a href="mailto:<?php if (!empty($email)) {
														echo $email;
													} ?>"><?php if (!empty($email)) {
																echo $email;
															} ?></a></dd>
						<dd><span>Website: </span><a href="https://<?php echo $web; ?>">https://<?php echo $web; ?></a></dd>
					</dl>
					<ul class="social-links" style="margin-top:15px;">
						<li class="twitter-link smallrightmargin">
							<a href="<?php if (!empty($twitter)) {
											echo $twitter;
										} ?>" class="twitter has-tip" target="_blank" title="Follow Us on Twitter">Twitter</a>
						</li>
						<li class="facebook-link smallrightmargin">
							<a href="<?php if (!empty($facebook)) {
											echo $facebook;
										} ?>" class="facebook has-tip" target="_blank" title="Join us on Facebook">Facebook</a>
						</li>
						<li class="google-link smallrightmargin">
							<a href="<?php if (!empty($google)) {
											echo $google;
										} ?>" class="google has-tip" title="Google +" target="_blank">Google</a>
						</li>
						<li class="instagram-link smallrightmargin">
							<a href="<?php if (!empty($instagram)) {
											echo $instagram;
										} ?>" class="instagram has-tip" title="Instagram" target="_blank">Instagram</a>
						</li>
					</ul>
				</div>
				<!-- 4th column -->
				<div class="c3">
					<h2 class="title"><i class="icon-link"></i> Links Terkait</h2>
					<hr class="footerstress">
					<?php if (!empty($KolomKananBawah)) {
						echo $KolomKananBawah;
					} ?>
				</div>
				<!-- end 4th column -->
			</div>
		</div>
	</div>
	<!-- copyright area -->
	<div class="copyright">
		<div class="grid">
			<div class="row">
				<div class="c6">
					&copy; 2017. PKK Kabupaten Wonosobo.
				</div>
				<div class="c6">
					<span class="right">
						Website Resmi PKK Wonosobo by <a href="https://diskominfo.wonosobokab.go.id">Diskominfo Kabupaten Wonosobo</a></span>
				</div>
			</div>
		</div>
	</div>
	<!-- all -->
	<script src="<?php echo base_url(); ?>salique-wide/js/modernizr-latest.js"></script>

	<!-- menu & scroll to top -->
	<script src="<?php echo base_url(); ?>salique-wide/js/common.js"></script>

	<!-- slider -->
	<script src="<?php echo base_url(); ?>salique-wide/js/jquery.cslider.js"></script>

	<!-- cycle -->
	<script src="<?php echo base_url(); ?>salique-wide/js/jquery.cycle.js"></script>

	<!-- carousel items -->
	<script src="<?php echo base_url(); ?>salique-wide/js/jquery.carouFredSel-6.0.3-packed.js"></script>

	<!-- twitter
<script src="<?php echo base_url(); ?>salique-wide/js/jquery.tweet.js"></script>
 -->
	<!-- filtering -->
	<script src="<?php echo base_url(); ?>salique-wide/js/jquery.isotope.min.js"></script>
	<!-- hover effects -->
	<script type="text/javascript" src="<?php echo base_url(); ?>salique-wide/js/mosaic.1.0.1.min.js"></script>
	<!-- END PAGE LEVEL JAVASCRIPTS -->
	<script>
		function LoadVisitor() {
			$.ajax({
				type: 'POST',
				async: true,
				data: {
					table: 'visitor'
				},
				dataType: 'html',
				url: '<?php echo base_url(); ?>visitor/simpan_visitor/',
				success: function(html) {
					$('#visitor').html('Total Pengunjung : ' + html + ' ');
				}
			});
		}
	</script>
	<script>
		function LoadHitCounter() {
			$.ajax({
				type: 'POST',
				async: true,
				data: {
					current_url: '<?php echo base_url(); ?>index.php'
				},
				dataType: 'html',
				url: '<?php echo base_url(); ?>visitor/hit_counter/',
				success: function(html) {
					$('#hit_counter').html('Total Pembaca : ' + html + ' ');
				}
			});
		}
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			LoadVisitor();
			LoadHitCounter();
		});
	</script>
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s);
			js.id = id;
			js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.12&appId=351370971628122&autoLogAppEvents=1';
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>

	<!-- uut -->
	<script src="<?php echo base_url(); ?>js/uut.js"></script>
	<!-- alertify -->
	<script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
	<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>

</body>
<!-- END BODY -->

</html>