
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Posting Website OPD</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="konten_posting" class="table table-bordered table-striped table-hover">
                	<thead>
					  <tr>
						<th rowspan="3">No</th>
						<th rowspan="3">NPSN<br></th>
						<th rowspan="3">Nama Sekolah</th>
						<th rowspan="3">Alamat</th>
						<th rowspan="3">Desa/Kelurahan</th>
						<th rowspan="3">Kecamatan</th>
						<th rowspan="3">Jenjang</th>
						<th rowspan="3">Status</th>
						<th rowspan="3">Nama Kepala Sekolah</th>
					<!--	<th colspan="12">Peserta Didik</th>
					  </tr>
					  <tr>
						<td colspan="2">Kelas 10</td>
						<td colspan="2">Kelas 11</td>
						<td colspan="2">Kelas 12</td>
					  </tr>
					  <tr>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
					  </tr>-->
                	</thead>
                <tbody>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				  </tr>
				
                </tbody>
			  </table>
		  </div>
	  </div>
<!-- page script -->
<script>
  $(function () {
    $('#konten_posting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
