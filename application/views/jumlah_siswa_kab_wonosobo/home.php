
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Data Posting Website OPD</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive">
              <table id="konten_posting" class="table table-bordered table-striped table-hover">
                	<thead>
					  <tr>
						<th rowspan="3">No</th>
						<th rowspan="3">NPSN<br></th>
						<th rowspan="3">Nama Sekolah</th>
						<th rowspan="3">Alamat</th>
						<th rowspan="3">Desa/Kelurahan</th>
						<th rowspan="3">Kecamatan</th>
						<th rowspan="3">Jenjang</th>
						<th rowspan="3">Status</th>
						<th rowspan="3">Nama Kepala Sekolah</th>
						<th colspan="12">Peserta Didik</th>
					  </tr>
					  <tr>
						<td colspan="2">Kelas 1</td>
						<td colspan="2">Kelas 2</td>
						<td colspan="2">Kelas 3</td>
						<td colspan="2">Kelas 4</td>
						<td colspan="2">Kelas 5</td>
						<td colspan="2">Kelas 6</td>
						<td colspan="2">Jumlah</td>
					  </tr>
					  <tr>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
						<td>Laki-laki</td>
						<td>Perempuan</td>
					  </tr>
                	</thead>
                <tbody>
				<?php
						  $w2 = $this->db->query("
						  SELECT *
						  from jumlah_siswa_kab_wonosobo_sd
						  ");
						  $no=0;
						  foreach($w2->result() as $h2)
						  {
							  $no=$no+1;
							  echo'
								<tr>
									<td>'.$no.'</td>
									<td>'.$h2->npsn.'</td>
									<td>'.$h2->nama_sekolah.'</td>
									<td>'.$h2->alamat_sekolah.'</td>
									<td>'.$h2->nama_desa.'</td>
									<td>'.$h2->nama_kecamatan.'</td>
									<td>'.$h2->jenjang_pendidikan.'</td>
									<td>'.$h2->status_sekolah.'</td>
									<td>'.$h2->nama_kepala_sekolah.'</td>
									<td>'.$h2->kelas_satu_laki.'</td>
									<td>'.$h2->kelas_satu_perempuan.'</td>
									<td>'.$h2->kelas_dua_laki.'</td>
									<td>'.$h2->kelas_dua_perempuan.'</td>
									<td>'.$h2->kelas_tiga_laki.'</td>
									<td>'.$h2->kelas_tiga_perempuan.'</td>
									<td>'.$h2->kelas_empat_laki.'</td>
									<td>'.$h2->kelas_empat_perempuan.'</td>
									<td>'.$h2->kelas_lima_laki.'</td>
									<td>'.$h2->kelas_lima_perempuan.'</td>
									<td>'.$h2->kelas_enam_laki.'</td>
									<td>'.$h2->kelas_enam_perempuan.'</td>
									<td>';echo ''.$jml_l=$h2->kelas_satu_laki+$h2->kelas_dua_laki+$h2->kelas_tiga_laki+$h2->kelas_empat_laki+$h2->kelas_lima_laki+$h2->kelas_enam_laki.''; echo '</td>
									<td>';echo ''.$jml_l=$h2->kelas_satu_perempuan+$h2->kelas_dua_perempuan+$h2->kelas_tiga_perempuan+$h2->kelas_empat_perempuan+$h2->kelas_lima_perempuan+$h2->kelas_enam_perempuan.''; echo '</td>
								  </tr>
							  ';
						  }
				?>
				
                </tbody>
			  </table>
		  </div>
	  </div>
<!-- page script -->
<script>
  $(function () {
    $('#konten_posting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
