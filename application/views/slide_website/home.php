
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Gambar Slide</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Gambar Slide</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab" id="klik_tab_input">Form</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
										<h3 id="judul_formulir">FORMULIR INPUT</h3>
													
										<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>slide_website/upload/?table_name=slide_website" enctype="multipart/form-data">
											
												<div class="form-group" style="display:none;">
													<label for="mode_exel">mode exel</label>
													<input class="form-control" id="mode_exel" name="mode_exel" value="0" placeholder="mode_exel" type="text">
												</div>
												<div class="form-group" style="display:none;">
													<label for="halaman">halaman</label>
													<input class="form-control" id="halaman" name="halaman" value="" placeholder="halaman" type="text">
												</div>
												<div class="form-group" style="display:none;">
													<label for="temp">temp</label>
													<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
												</div>
												<div class="form-group" style="display:none;">
													<label for="mode">mode</label>
													<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
												</div>
												<div class="form-group" style="display:none;">
													<label for="id_slide_website">id_slide_website</label>
													<input class="form-control" id="id_slide_website" name="id" value="" placeholder="id_slide_website" type="text">
												</div>
												<div class="alert alert-primary alert-dismissable">
													<div class="form-group">
														<label for="parent">Parent</label>
														<select class="form-control" id="parent" name="parent" >
														</select>
													</div>
													<div class="form-group">
														<label for="remake">Keterangan</label>
														<input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran " type="text">
													</div>
													<div class="form-group">
														<label for="url_redirection">URL Redirection</label>
														<input class="form-control" id="url_redirection" name="url_redirection" placeholder="URL Redirection" type="text">
													</div>
													<div class="form-group">
														<label for="myfile">File</label>
														<input type="file" size="60" name="myfile" id="file_lampiran" >
													</div>
													<div id="ProgresUpload">
														<div id="BarProgresUpload"></div>
														<div id="PersenProgresUpload">0%</div >
													</div>
													<div id="PesanProgresUpload"></div>
												</div>
												<div class="alert alert-primary alert-dismissable">
													<h3 class="box-title">Data Header </h3>
													<table class="table table-bordered">
														<tr>
															<th>No</th><th>Keterangan</th><th>Status</th><th>Download</th><th>Hapus</th> 
														</tr>
														<tbody id="tbl_slide_website_slide_website">
														</tbody>
													</table>
													<div class="overlay" id="spinners_data" style="display:none;">
														<i class="fa fa-refresh fa-spin"></i>
													</div>
												</div>
												
												<button type="submit" class="btn btn-primary" id="simpan_slide_website" style="display:none;">SIMPAN</button>
												<button type="submit" class="btn btn-primary" id="update_slide_website" style="display:none;">UPDATE</button>
										</form>
										<div class="overlay" id="overlay_form_input" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
											
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
		
		

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_slide_website_slide_website').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'slide_website',
				mode:mode,
        value:value 
			},
      dataType: 'html',
      url: '<?php echo base_url(); ?>slide_website/load_table_slide_website/',
      success: function(html) {
        $('#tbl_slide_website_slide_website').html(html);
        $('#spinners_data').hide();
      }
			/* dataType: 'json',
			url: '<?php echo base_url(); ?>slide_website/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_slide_website="'+json[i].id_slide_website+'" id="'+json[i].id_slide_website+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
          if( i > 1 ){
            tr += '<td valign="top" style="padding: 2px '+(i * 1)+'px ;">'+json[i].keterangan+'</td>';
          }
          else{
            tr += '<td valign="top">'+json[i].keterangan+'</td>';
          }
          if( json[i].status == 1 ){
            tr += '<td valign="top"><a href="#" id="inaktifkan">Aktif</a> </td>';
          }
          else{
            tr += '<td valign="top"><a href="#" id="aktifkan">TIdak Aktif</a> </td>';
          }
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					
          tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_slide_website_slide_website').append(tr);
			} */
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_slide_website').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        $('#parent').val('');
        $('#url_redirection').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  var mode = $('#mode').val();
    if(mode == 'edit'){
      var value = $('#id_slide_website').val();
    }
    else{
      var value = $('#temp').val();
    }
  AttachmentByMode(mode, value);
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_slide_website_slide_website').on('click', '#del_ajax', function() {
    var id_slide_website = $(this).closest('tr').attr('id_slide_website');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_slide_website"] = id_slide_website;
        var url = '<?php echo base_url(); ?>slide_website/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_slide_website').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_slide_website='+id_slide_website+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_slide_website_slide_website').on('click', '#inaktifkan', function() {
    var id_slide_website = $(this).closest('tr').attr('id_slide_website');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_slide_website"] = id_slide_website;
        var url = '<?php echo base_url(); ?>slide_website/inaktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_slide_website').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_slide_website='+id_slide_website+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_slide_website_slide_website').on('click', '#aktifkan', function() {
    var id_slide_website = $(this).closest('tr').attr('id_slide_website');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_slide_website"] = id_slide_website;
        var url = '<?php echo base_url(); ?>slide_website/aktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_slide_website').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_slide_website='+id_slide_website+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>


<script>
  function load_option_slide_website() {
    $('#parent').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>slide_website/load_the_option/',
      success: function(html) {
        $('#parent').html('<option value="0">Utama</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_option_slide_website();
});
</script>
