
        <?php
        $web=$this->uut->namadomain(base_url());
        ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>USERS</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">USERS</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
<section class="content" id="awal">
  <div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header d-flex p-0">
					<ul class="nav nav-pills ml-auto p-2">
						<li class="nav-item"><a class="nav-link active" href="#tab_form_wisata" data-toggle="tab" id="klik_tab_input">Form</a></li>
						<li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab" id="klik_tab_data_wisata">Data</a></li>
					</ul>
				</div>
				<div class="card-body">
					<div class="tab-content">
						<div class="tab-pane active" id="tab_form_wisata">
					
							<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=wisata" enctype="multipart/form-data">
									<h3 id="judul_formulir">FORMULIR INPUT</h3>
									<div class="row">
										<div class="col-sm-12 col-md-6">

                      <input name="page" id="page" value="1" type="hidden" value="">
                      <input name="tabel" id="tabel" value="wisata" type="hidden" value="">										
											<div class="form-group" style="display:none;">
												<label for="temp">temp</label>
												<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="mode">mode</label>
												<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="id_wisata">id_wisata</label>
												<input class="form-control" id="id_wisata" name="id" value="" placeholder="id_wisata" type="text">
											</div>
											<div class="form-group">
												<label for="nama_wisata">Nama Wisata</label>
												<input class="form-control" id="nama_wisata" name="nama_wisata" value="" placeholder="nama" type="text">
											</div>
											<div class="form-group">
												<label for="status_wisata">Status Wisata</label>
												<input class="form-control" id="status_wisata" name="status_wisata" value="" placeholder="status_wisata" type="text">
											</div>
											<div class="form-group">
												<label for="id_jenis_wisata">id_jenis_wisata</label>
												<select class="form-control" id="id_jenis_wisata" name="id_jenis_wisata" >
												<option value="Desa Wisata Unggulan">Desa Wisata Unggulan</option>
												<option value="Desa Wisata Lestari">Desa Wisata Lestari</option>
												<option value="Desa Wisata Rintisan">Desa Wisata Rintisan</option>
												</select>
											</div>
											<div class="form-group">
												<label for="id_kecamatan">id_kecamatan</label>
												<select class="form-control" id="id_kecamatan" name="id_kecamatan" >
												</select>
											</div>
											<div class="form-group">
												<label for="id_desa">id_desa</label>
												<select class="form-control" id="id_desa" name="id_desa" >
													<option value="0">Pilih Desa</option>
												</select>
											</div>
											
										</div>
									</div>
									<button type="submit" class="btn btn-primary" id="simpan_wisata">SIMPAN</button>
									<button type="submit" class="btn btn-primary" id="update_wisata" style="display:none;">UPDATE</button>
							</form>
							<div class="overlay" id="overlay_form_input" style="display:none;">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							
						</div>
						<div class="tab-pane" id="tab_2">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
															<option value="999999999">Semua</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="wisata.created_time">Tanggal</option>
															<option value="wisata.nama">Nama</option>
															<option value="wisata.nama_wisata">User name</option>
															<option value="wisata.id_jenis_wisata">Hak Akses</option>
															<option value="wisata.id_kecamatan">Kecamatan</option>
															<option value="wisata.id_desa">Desa</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_wisata"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
                          <thead>
                            <tr>
                              <th>NO</th>
                              <th>Jenis</th>
                              <th>Nama</th>
                              <th>Status</th>
                              <th>Desa</th>
                              <th>Kecamatan</th>
                              <th>Proses</th>
                            </tr>
                          </thead>
													<tbody id="tbl_data_wisata">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_wisata" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!----------------------->
<script>
  function load_opd_domain() {
    $('#id_kecamatan').html('');
    $('#option_kecamatan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>wisata/option_kecamatan/',
      success: function(html) {
        $('#option_kecamatan').html('<option value="0">Pilih Kecamatan</option>'+html+'');
        $('#id_kecamatan').html('<option value="0">Pilih Kecamatan</option>'+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var id_kecamatan = $('#id_kecamatan').val();
      load_option_desa(id_kecamatan);
    });
  });
</script>

<script>
  function load_option_desa(id_kecamatan) {
    $('#id_desa').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html(' '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_opd_domain();
});
</script>

<script>
  function AfterSavedUsers() {
    $('#id_wisata, #nama_wisata, #status_wisata, #id_jenis_wisata, #id_desa, #id_kecamatan').val('');
    load_opd_domain();
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
 
<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_wisata').show();
    $('#update_wisata').hide();
    $('#id_wisata, #nama_wisata, #status_wisata, #id_jenis_wisata, #id_desa, #id_kecamatan').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_wisata').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'nama_wisata', 'status_wisata', 'id_jenis_wisata', 'id_desa', 'id_kecamatan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["nama_wisata"] = $("#nama_wisata").val();
      parameter["status_wisata"] = $("#status_wisata").val();
      parameter["id_jenis_wisata"] = $("#id_jenis_wisata").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      var url = '<?php echo base_url(); ?>wisata/simpan_wisata';
      var parameterRv = [ 'nama_wisata', 'status_wisata', 'id_jenis_wisata', 'id_desa', 'id_kecamatan' ];
			var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedUsers();
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>wisata/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>wisata/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_wisata').on('click', '.update_id_wisata', function() {
    $('#mode').val('edit');
    $('#simpan_wisata').hide();
    $('#update_wisata').show();
    $('#overlay_data_wisata').show();
    $('#tbl_data_wisata').html('');
    var id_wisata = $(this).closest('tr').attr('id_wisata');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_wisata');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_wisata').val(id_wisata);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_wisata:id_wisata
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>wisata/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_wisata').val(json[i].id_wisata);
            $('#id_jenis_wisata').val(json[i].id_jenis_wisata);
            $('#nama_wisata').val(json[i].nama_wisata);
            $('#status_wisata').val(json[i].status_wisata);
          }
        }
      });
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#update_wisata').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'id_wisata', 'temp', 'nama_wisata' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_wisata"] = $("#id_wisata").val();
      parameter["id_jenis_wisata"] = $("#id_jenis_wisata").val();
      parameter["temp"] = $("#temp").val();
      parameter["nama_wisata"] = $("#nama_wisata").val();
      parameter["status_wisata"] = $("#status_wisata").val();
      var url = '<?php echo base_url(); ?>wisata/update_wisata';
      
      var parameterRv = [ 'id_wisata', 'temp', 'nama_wisata' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_wisata').on('click', '#del_ajax_wisata', function() {
    var id_wisata = $(this).closest('tr').attr('id_wisata');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_wisata"] = id_wisata;
        var url = '<?php echo base_url(); ?>wisata/hapus/';
        HapusData(parameter, url);
        $('[id_wisata='+id_wisata+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
});
</script>