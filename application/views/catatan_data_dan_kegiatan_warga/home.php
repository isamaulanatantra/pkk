
                  <div class="col-md-12">
                    <div class="card">
											<div class="card-body table-responsive p-0">
                        <center>
                          <h1>REKAPITULASI</h1>
                          <h1>CATATAN DATA DAN KEGIATAN WARGA</h1>
                          <h1>KELOMPOK DESA WISMA</h1>
                        </center>
												<table class="table table-bordered">
                          <tr>
                            <th rowspan="3">NO</th>
                            <th rowspan="3">NAMA KEPALA RUMAH TANGGA</th>
                            <th rowspan="3">JML KK</th>
                            <th colspan="11">JUMLAH ANGGOTA KELUARGA</th>
                            <th colspan="6">KRITERIA RUMAH</th>
                            <th colspan="3">SUMBER AIR KELUARGA</th>
                            <th colspan="2">MAKANAN POKOK</th>
                            <th colspan="4">WARGA MENGIKUTI KEGIATAN</th>
                            <th rowspan="3">KET</th>
                          </tr>
                          <tr>
                            <td colspan="2">TOTAL</td>
                            <td colspan="2">BALITA</td>
                            <td rowspan="2">PUS</td>
                            <td rowspan="2">WUS</td>
                            <td rowspan="2">IBU HAMIL</td>
                            <td rowspan="2">IBU MENYUSUI</td>
                            <td rowspan="2">LANSIA</td>
                            <td rowspan="2">3 BUTA</td>
                            <td rowspan="2">BERKEBU TUHAN KHUSUS</td>
                            <td rowspan="2">SEHAT LAYAK HUNI</td>
                            <td rowspan="2">TIDAK SEHAT LAYAK HUNI</td>
                            <td rowspan="2">MEMILIKI SPAL</td>
                            <td rowspan="2">MEMILIKI TMPT PEMB SAMPAH</td>
                            <td rowspan="2">MEMILIKI JAMBAN KELUARGA</td>
                            <td rowspan="2">MENEMPEL STIKER P4K</td>
                            <td rowspan="2">PDAM</td>
                            <td rowspan="2">SUMUR</td>
                            <td rowspan="2">DLL</td>
                            <td rowspan="2">BERAS</td>
                            <td rowspan="2">NON BERAS</td>
                            <td rowspan="2">UP2K</td>
                            <td rowspan="2">PMFT TANAH PEKARANGAN</td>
                            <td rowspan="2">INDUSTRI RMH TANGGA</td>
                            <td rowspan="2">KERJA BAKTI</td>
                          </tr>
                          <tr>
                            <td>L</td>
                            <td>P</td>
                            <td>L</td>
                            <td>P</td>
                          </tr>
                          <tr>
                            <td>1</td>
                            <td>2</td>
                            <td>3</td>
                            <td>4</td>
                            <td>5</td>
                            <td>6</td>
                            <td>7</td>
                            <td>8</td>
                            <td>9</td>
                            <td>10</td>
                            <td>11</td>
                            <td>12</td>
                            <td>13</td>
                            <td>14</td>
                            <td>15</td>
                            <td>16</td>
                            <td>17</td>
                            <td>18</td>
                            <td>19</td>
                            <td>20</td>
                            <td>21</td>
                            <td>22</td>
                            <td>23</td>
                            <td>24</td>
                            <td>25</td>
                            <td>26</td>
                            <td>27</td>
                            <td>28</td>
                            <td>29</td>
                            <td>30</td>
                          </tr>
													<tbody>
                          <?php
                          $web=$this->uut->namadomain(base_url());
                          $table = 'data_keluarga';
                          $page    = $this->input->get('page');
                          $limit    = $this->input->get('limit');
                          $keyword    = $this->input->get('keyword');
                          $order_by    = $this->input->get('orderby');
                          $start      = ($page - 1) * $limit;
                          $fields     = "
                          *,
                          ( select (dusun.nama_dusun) from dusun where dusun.id_dusun=data_keluarga.id_dusun limit 1) as nama_dusun,
                          ( select (desa.nama_desa) from desa where desa.id_desa=data_keluarga.id_desa limit 1) as nama_desa,
                          ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=data_keluarga.id_kecamatan limit 1) as nama_kecamatan
                          ";
                          $hak_akses = $this->session->userdata('hak_akses');
                          if($hak_akses=='web_pkk'){
                            $where = array(
                            'data_keluarga.status' => 1
                            );
                          }elseif($hak_akses=='web_pkk_kecamatan'){
                            $where = array(
                              'data_keluarga.status' => 1,
                              'data_keluarga.id_kecamatan' => $this->session->userdata('id_kecamatan')
                              );
                          }elseif($hak_akses=='web_pkk_desa'){
                            $where = array(
                              'data_keluarga.status' => 1,
                              'data_keluarga.id_kecamatan' => $this->session->userdata('id_kecamatan'),
                              'data_keluarga.id_desa' => $this->session->userdata('id_desa')
                              );
                          }else{
                            $where = array(
                              'data_keluarga.status' => 1,
                              'data_keluarga.id_kecamatan' => $this->session->userdata('id_kecamatan'),
                              'data_keluarga.id_desa' => $this->session->userdata('id_desa')
                              );
                          }
                          $orderby   = ''.$order_by.'';
                          $data_data_keluarga = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
                          $urut=$start;
                          foreach ($data_data_keluarga->result() as $row){
                            $urut=$urut+1;
                            $query_total_anggota_keluarga_berkebutuhan_khusus = $this->db->query("SELECT * FROM anggota_keluarga WHERE id_data_keluarga = ".$row->id_data_keluarga." and berkebutuhan_khusus ='ya'");
                            $query_total_anggota_keluarga_wanita = $this->db->query("SELECT * FROM anggota_keluarga WHERE id_data_keluarga = ".$row->id_data_keluarga." and jenis_kelamin ='perempuan'");
                            
                            $total_anggota_keluarga_wanita = $query_total_anggota_keluarga_wanita->num_rows();
                            echo'
                            <tr>
                              <td>'.($urut).'</td>
                              <td>'.$row->nama_kepala_rumah_tangga.'</td>
                              <td>'.$row->jumlah_kartu_keluarga.'</td>
                              <td>'.$row->jumlah_anggota_keluarga_laki_laki	.'</td>
                              <td>'.$row->jumlah_anggota_keluarga_perempuan.'</td>
                              <td>'.$row->jumlah_balita.'</td>
                              <td>0</td>
                              <td>'.$row->jumlah_pus.'</td>
                              <td>'.$row->jumlah_wus.'</td>
                              <td>'.$row->jumlah_ibu_hamil.'</td>
                              <td>'.$row->jumlah_ibu_menyusui.'</td>
                              <td>'.$row->jumlah_lansia.'</td>
                              <td>'.$row->jumlah_buta.'</td>
                              <td>'.$query_total_anggota_keluarga_berkebutuhan_khusus->num_rows().'</td>
                              <td>';if($row->kriteria_rumah=='sehat'){echo'<i class="fa fa-check"></i>';}else{echo'-';}echo'</td>
                              <td>';if($row->kriteria_rumah=='kurang sehat'){echo'<i class="fa fa-check"></i>';}else{echo'-';}echo'</td>
                              <td>'.$row->memiliki_pembuangan_sampah.'</td>
                              <td>'.$row->mempunyai_saluran_pembuangan_air_limbah.'</td>
                              <td>'.$row->mempunyai_jamban_keluarga.'</td>
                              <td>'.$row->menempel_stiker_p4k.'</td>
                              ';if($row->sumber_air_keluarga=='pdam'){echo'<td><i class="fa fa-check"></i></td><td>-</td><td>-</td>';}elseif($row->sumber_air_keluarga=='sumur'){echo'<td>-</td><td><i class="fa fa-check"></i></td><td>-</td>';}else{echo'<td>-</td><td>-</td><td><i class="fa fa-check"></i></td>';}echo'
                              ';if($row->makanan_pokok=='beras'){echo'<td><i class="fa fa-check"></i></td><td>-</td>';}else{echo'<td><i class="fa fa-check"></i></td>';}echo'
                              <td>'.$row->kegiatan_up2k.'</td>
                              <td>-</td>
                              <td>-</td>
                              <td>-</td>
                              <td></td>
                            </tr>
                            ';
                          }
                          ?>
													</tbody>
												</table>
											</div>
                    </div>
                  </div>