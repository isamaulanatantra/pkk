<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Pokja_dua</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>pokja_dua">Home</a></li>
          <li class="breadcrumb-item active">Pokja_dua</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">


  <div class="row" id="awal">
    <div class="col-12">
      <!-- Custom Tabs -->
      <div class="card">
        <div class="card-header p-2">
          <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link active" href="#tab_form_pokja_dua" data-toggle="tab" id="klik_tab_input">Form</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_data_pokja_dua" data-toggle="tab" id="klik_tab_data_pokja_dua">Data</a></li>
            <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>pokja_dua"><i class="fa fa-refresh"></i></a></li>
          </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="tab-content">
            <div class="tab-pane active" id="tab_form_pokja_dua">
              <input name="tabel" id="tabel" value="pokja_dua" type="hidden" value="">
              <form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=pokja_dua" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-4">
                    <input name="page" id="page" value="1" type="hidden" value="">
                    <div class="form-group" style="display:none;">
                      <label for="temp">temp</label>
                      <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="mode">mode</label>
                      <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_pokja_dua">id_pokja_dua</label>
                      <input class="form-control" id="id_pokja_dua" name="id" value="" placeholder="id_pokja_dua" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_propinsi">Provinsi</label>
                      <select class="form-control" id="id_propinsi" name="id_propinsi">
                        <option value="1">Jawa Tengah</option>
                      </select>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_kabupaten">Kabupaten</label>
                      <select class="form-control" id="id_kabupaten" name="id_kabupaten">
                        <option value="1">Wonosobo</option>
                      </select>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_kecamatan">Kecamatan</label>
                      <div class="overlay" id="overlay_id_kecamatan" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                      <select class="form-control" id="id_kecamatan" name="id_kecamatan">
                      </select>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="id_desa">Desa</label>
                        </div>
                        <div class="col-md-5">
                          <div class="overlay" id="overlay_id_desa" style="display:none;">
                            <i class="fa fa-refresh fa-spin"></i>
                          </div>
                          <select class="form-control" id="id_desa" name="id_desa">
                            <option value="0">Pilih Desa</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_dusun">Dusun</label>
                      <div class="overlay" id="overlay_id_dusun" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                      <select class="form-control" id="id_dusun" name="id_dusun">
                        <option value="0">Pilih Dusun</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="tahun">tahun</label>
                        </div>
                        <div class="col-md-5">
                          <select class="form-control" id="tahun" name="tahun">
                            <option value="0000">Pilih tahun</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="warga_masih_3buta">warga_masih_3buta</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="warga_masih_3buta" name="warga_masih_3buta" value="" placeholder="warga_masih_3buta" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="paketa_kelbelajar">paketa_kelbelajar</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="paketa_kelbelajar" name="paketa_kelbelajar" value="" placeholder="paketa_kelbelajar" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="paketa_wargabelajar">paketa_wargabelajar</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="paketa_wargabelajar" name="paketa_wargabelajar" value="" placeholder="paketa_wargabelajar" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="paketb_kelbelajar">paketb_kelbelajar</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="paketb_kelbelajar" name="paketb_kelbelajar" value="" placeholder="paketb_kelbelajar" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="paketb_wargabelajar">paketb_wargabelajar</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="paketb_wargabelajar" name="paketb_wargabelajar" value="" placeholder="paketb_wargabelajar" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="paketc_kelbelajar">paketc_kelbelajar</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="paketc_kelbelajar" name="paketc_kelbelajar" value="" placeholder="paketc_kelbelajar" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="paketc_wargabelajar">paketc_wargabelajar</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="paketc_wargabelajar" name="paketc_wargabelajar" value="" placeholder="paketc_wargabelajar" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="kf_kelbelajar">kf_kelbelajar</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="kf_kelbelajar" name="kf_kelbelajar" value="" placeholder="kf_kelbelajar" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="kf_wargabelajar">kf_wargabelajar</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="kf_wargabelajar" name="kf_wargabelajar" value="" placeholder="kf_wargabelajar" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="paud_sejenis">paud_sejenis</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="paud_sejenis" name="paud_sejenis" value="" placeholder="paud_sejenis" type="text">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="tamanbacaan_perpustakaan">tamanbacaan_perpustakaan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="tamanbacaan_perpustakaan" name="tamanbacaan_perpustakaan" value="" placeholder="tamanbacaan_perpustakaan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="bkb_kel">bkb_kel</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="bkb_kel" name="bkb_kel" value="" placeholder="bkb_kel" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="bkb_ibu_peserta">bkb_ibu_peserta</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="bkb_ibu_peserta" name="bkb_ibu_peserta" value="" placeholder="bkb_ibu_peserta" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="bkb_ape">bkb_ape</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="bkb_ape" name="bkb_ape" value="" placeholder="bkb_ape" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="bkb_kecsimulasibkb">bkb_kecsimulasibkb</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="bkb_kecsimulasibkb" name="bkb_kecsimulasibkb" value="" placeholder="bkb_kecsimulasibkb" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="kaderkhusus_tutorkf">kaderkhusus_tutorkf</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="kaderkhusus_tutorkf" name="kaderkhusus_tutorkf" value="" placeholder="kaderkhusus_tutorkf" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="kaderkhusus_tutorpaud_sejenis">kaderkhusus_tutorpaud_sejenis</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="kaderkhusus_tutorpaud_sejenis" name="kaderkhusus_tutorpaud_sejenis" value="" placeholder="kaderkhusus_tutorpaud_sejenis" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="kaderkhusus_bkb">kaderkhusus_bkb</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="kaderkhusus_bkb" name="kaderkhusus_bkb" value="" placeholder="kaderkhusus_bkb" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="kaderkhusus_koperasi">kaderkhusus_koperasi</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="kaderkhusus_koperasi" name="kaderkhusus_koperasi" value="" placeholder="kaderkhusus_koperasi" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="kaderkhusus_keterampilan">kaderkhusus_keterampilan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="kaderkhusus_keterampilan" name="kaderkhusus_keterampilan" value="" placeholder="kaderkhusus_keterampilan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="kaderyangsudah_lp3pkk">kaderyangsudah_lp3pkk</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="kaderyangsudah_lp3pkk" name="kaderyangsudah_lp3pkk" value="" placeholder="kaderyangsudah_lp3pkk" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="kaderyangsudah_tpk3pkk">kaderyangsudah_tpk3pkk</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="kaderyangsudah_tpk3pkk" name="kaderyangsudah_tpk3pkk" value="" placeholder="kaderyangsudah_tpk3pkk" type="text">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="kaderyangsudah_damaspkk">kaderyangsudah_damaspkk</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="kaderyangsudah_damaspkk" name="kaderyangsudah_damaspkk" value="" placeholder="kaderyangsudah_damaspkk" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="up2k_pemula_klp">up2k_pemula_klp</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="up2k_pemula_klp" name="up2k_pemula_klp" value="" placeholder="up2k_pemula_klp" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="up2k_pemula_peserta">up2k_pemula_peserta</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="up2k_pemula_peserta" name="up2k_pemula_peserta" value="" placeholder="up2k_pemula_peserta" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="up2k_madya_klp">up2k_madya_klp</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="up2k_madya_klp" name="up2k_madya_klp" value="" placeholder="up2k_madya_klp" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="up2k_madya_peserta">up2k_madya_peserta</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="up2k_madya_peserta" name="up2k_madya_peserta" value="" placeholder="up2k_madya_peserta" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="up2k_utama_klp">up2k_utama_klp</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="up2k_utama_klp" name="up2k_utama_klp" value="" placeholder="up2k_utama_klp" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="up2k_utama_peserta">up2k_utama_peserta</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="up2k_utama_peserta" name="up2k_utama_peserta" value="" placeholder="up2k_utama_peserta" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="up2k_mandiri_klp">up2k_mandiri_klp</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="up2k_mandiri_klp" name="up2k_mandiri_klp" value="" placeholder="up2k_mandiri_klp" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="up2k_mandiri_peserta">up2k_mandiri_peserta</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="up2k_mandiri_peserta" name="up2k_mandiri_peserta" value="" placeholder="up2k_mandiri_peserta" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="koperasi_berbadan_klp">koperasi_berbadan_klp</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="koperasi_berbadan_klp" name="koperasi_berbadan_klp" value="" placeholder="koperasi_berbadan_klp" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="koperasi_berbadan_anggota">koperasi_berbadan_anggota</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="koperasi_berbadan_anggota" name="koperasi_berbadan_anggota" value="" placeholder="koperasi_berbadan_anggota" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="keterangan">keterangan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="keterangan" name="keterangan" value="" placeholder="keterangan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary" id="simpan_pokja_dua">SIMPAN DATA KELUARGA</button>
                      <button type="submit" class="btn btn-primary" id="update_pokja_dua" style="display:none;">UPDATE DATA KELUARGA</button>
                      <a class="btn text-primary" href="<?php echo base_url(); ?>pokja_dua"><i class="fa fa-refresh"></i></a>
                    </div>
                  </div>
                </div>
              </form>

              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
            <div class="tab-pane table-responsive" id="tab_data_pokja_dua">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">&nbsp;</h3>
                  <div class="card-tools">
                    <div class="input-group input-group-sm">
                      <select class="form-control input-sm pull-right" id="keyword" name="keyword" style="width: 150px;">
                        <option value="2022">2022</option>
                        <option value="2021">2021</option>
                        <option value="2020">2020</option>
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                        <option value="2017">2017</option>
                        <option value="2016">2016</option>
                      </select>
                      <select name="limit" class="form-control input-sm pull-right" style="display:none;width: 150px;" id="limit">
                        <option value="999999999">Semua</option>
                        <option value="1">1 Per-Halaman</option>
                        <option value="10">10 Per-Halaman</option>
                        <option value="50">50 Per-Halaman</option>
                        <option value="100">100 Per-Halaman</option>
                      </select>
                      <select name="orderby" class="form-control input-sm pull-right" style="display:none; width: 150px;" id="orderby">
                        <option value="pokja_dua.tahun">tahun</option>
                      </select>
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" id="tampilkan_data_pokja_dua"><i class="fa fa-search"></i> Tampil</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-body table-responsive p-0">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th rowspan="5">NO</th>
                        <th rowspan="5">NAMA WILAYAH</th>
                        <th colspan="23">PENDIDIKAN DAN KETERAMPILAN</th>
                        <th colspan="10">PENGEMBANGAN KEHIDUPAN BERKOPERASI</th>
                        <th rowspan="5">KET</th>
                      </tr>
                      <tr>
                        <th rowspan="4">WARGA MASIH 3 BUTA</th>
                        <th colspan="22">JUMLAH KELOMPOK BELAJAR</th>
                        <th colspan="8">PRA KOPERASI USAHA BERSAMA, UP2K</th>
                        <th colspan="2">KOPERASI BERBADAN</th>
                      </tr>
                      <tr>
                        <th colspan="2">PAKET A</th>
                        <th colspan="2">PAKET B</th>
                        <th colspan="2">PAKET C</th>
                        <th colspan="3">KF</th>
                        <th rowspan="3">PAUD<br>SEJENISNYA</th>
                        <th rowspan="2">TAMAN BACAAN<br>PERPUSTAKAAN</th>
                        <th colspan="4">BKB</th>
                        <th colspan="5">KADER KHUSUS</th>
                        <th colspan="3">KADER YANG SUDAH</th>
                        <th colspan="2">PEMULA</th>
                        <th colspan="2">MADYA</th>
                        <th colspan="2">UTAMA</th>
                        <th colspan="2">MANDIRI</th>
                        <th rowspan="3">JUMLAH KLP</th>
                        <th rowspan="3">JUMLAH ANGGOTA</th>
                      </tr>
                      <tr>
                        <th rowspan="2">KEL. BELAJAR</th>
                        <th rowspan="2">WARGA BELAJAR</th>
                        <th rowspan="2">KEL. BELAJAR</th>
                        <th rowspan="2">WARGA BELAJAR</th>
                        <th rowspan="2">KEL. BELAJAR</th>
                        <th rowspan="2">WARGA BELAJAR</th>
                        <th rowspan="2">KEL. BELAJAR</th>
                        <th rowspan="2">WARGA BELAJAR</th>
                        <th rowspan="2">JML.KEL</th>
                        <th rowspan="2">JML. IBU <br>PESERTA</th>
                        <th rowspan="2">JML. <br>APE</th>
                        <th rowspan="2">JML. KEC. <br>SIMULASI</th>
                        <th colspan="2">TUTOR</th>
                        <th rowspan="2">BKB</th>
                        <th rowspan="2">KOPERASI</th>
                        <th rowspan="2">KETERAMPILAN</th>
                        <th rowspan="2">LP.3PKK</th>
                        <th rowspan="2">TPK 3 PKK</th>
                        <th rowspan="2">DAMAS PKK</th>
                        <th rowspan="2">JML. KLP</th>
                        <th rowspan="2">JML. ANG</th>
                        <th rowspan="2">JML. KLP</th>
                        <th rowspan="2">JML. ANG</th>
                        <th rowspan="2">JML. KLP</th>
                        <th rowspan="2">JML. ANG</th>
                        <th rowspan="2">JML. KLP</th>
                        <th rowspan="2">JML. ANG</th>
                      </tr>
                      <tr>
                        <th>KF</th>
                        <th>PAUD<br>SEJENIS</th>
                      </tr>
                    </thead>
                    <tbody id="tbl_data_pokja_dua">
                    </tbody>
                  </table>
                </div>
                <div class="card-footer">
                  <ul class="pagination pagination-sm m-0 float-right" id="pagination" style="display:none;">
                  </ul>
                  <div class="overlay" id="spinners_tbl_data_pokja_dua" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.tab-content -->
          </div><!-- /.card-body -->
        </div>
        <!-- ./card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->


</section>


<script type="text/javascript">
  $(document).ready(function() {
    load_option_desa();
  });
</script>
<script>
  function load_option_desa() {
    $('#id_desa').html('');
    $('#overlay_id_desa').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>pokja_dua/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html('<option value="0">Pilih Desa</option><option value="9999">TP PKK Kecamatan</option>' + html + '');
        $('#overlay_id_desa').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $('#tabel').val();
  });
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit: limit,
        keyword: keyword,
        orderby: orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>pokja_dua/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_' + tabel + '').html('');
    $('#spinners_tbl_data_' + tabel + '').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page: page,
        limit: limit,
        keyword: keyword,
        orderby: orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>' + tabel + '/json_all_' + tabel + '/',
      success: function(html) {
        // $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
        $('#tbl_data_' + tabel + '').html(html);
        $('#spinners_tbl_data_' + tabel + '').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page = parseInt(id) + 1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_' + tabel + '').on('click', function(e) {
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_' + tabel + '').on('click', function(e) {
      load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedPokja_dua() {
    $('#id_pokja_dua, #id_dusun, #rt, #tahun, #id_desa, #id_kecamatan, #id_kabupaten, #warga_masih_3buta, #paketa_kelbelajar, #paketa_wargabelajar, #paketb_kelbelajar, #paketb_wargabelajar, #paketc_kelbelajar, #paketc_wargabelajar, #kf_kelbelajar, #kf_wargabelajar, #paud_sejenis, #tamanbacaan_perpustakaan, #bkb_kel, #bkb_ibu_peserta, #bkb_ape, #bkb_kecsimulasibkb, #kaderkhusus_tutorkf, #kaderkhusus_tutorpaud_sejenis, #kaderkhusus_bkb, #kaderkhusus_koperasi, #kaderkhusus_keterampilan, #kaderyangsudah_lp3pkk, #kaderyangsudah_tpk3pkk, #kaderyangsudah_damaspkk, #up2k_pemula_klp, #up2k_pemula_peserta, #up2k_madya_klp, #up2k_madya_peserta, #up2k_utama_klp, #up2k_utama_peserta, #up2k_mandiri_klp, #up2k_mandiri_peserta, #koperasi_berbadan_klp, #koperasi_berbadan_anggota, #keterangan').val('');
    $('#tbl_attachment_pokja_dua').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#temp').val(Math.random());
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_pokja_dua').on('click', function(e) {
      e.preventDefault();
      var parameter = ['id_desa', 'temp', 'warga_masih_3buta'];
      InputValid(parameter);

      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["tahun"] = $("#tahun").val();
      parameter["warga_masih_3buta"] = $("#warga_masih_3buta").val();
      parameter["paketa_kelbelajar"] = $("#paketa_kelbelajar").val();
      parameter["paketa_wargabelajar"] = $("#paketa_wargabelajar").val();
      parameter["paketb_kelbelajar"] = $("#paketb_kelbelajar").val();
      parameter["paketb_wargabelajar"] = $("#paketb_wargabelajar").val();
      parameter["paketc_kelbelajar"] = $("#paketc_kelbelajar").val();
      parameter["paketc_wargabelajar"] = $("#paketc_wargabelajar").val();
      parameter["kf_kelbelajar"] = $("#kf_kelbelajar").val();
      parameter["kf_wargabelajar"] = $("#kf_wargabelajar").val();
      parameter["paud_sejenis"] = $("#paud_sejenis").val();
      parameter["tamanbacaan_perpustakaan"] = $("#tamanbacaan_perpustakaan").val();
      parameter["bkb_kel"] = $("#bkb_kel").val();
      parameter["bkb_ibu_peserta"] = $("#bkb_ibu_peserta").val();
      parameter["bkb_ape"] = $("#bkb_ape").val();
      parameter["bkb_kecsimulasibkb"] = $("#bkb_kecsimulasibkb").val();
      parameter["kaderkhusus_tutorkf"] = $("#kaderkhusus_tutorkf").val();
      parameter["kaderkhusus_tutorpaud_sejenis"] = $("#kaderkhusus_tutorpaud_sejenis").val();
      parameter["kaderkhusus_bkb"] = $("#kaderkhusus_bkb").val();
      parameter["kaderkhusus_koperasi"] = $("#kaderkhusus_koperasi").val();
      parameter["kaderkhusus_keterampilan"] = $("#kaderkhusus_keterampilan").val();
      parameter["kaderyangsudah_lp3pkk"] = $("#kaderyangsudah_lp3pkk").val();
      parameter["kaderyangsudah_tpk3pkk"] = $("#kaderyangsudah_tpk3pkk").val();
      parameter["kaderyangsudah_damaspkk"] = $("#kaderyangsudah_damaspkk").val();
      parameter["up2k_pemula_klp"] = $("#up2k_pemula_klp").val();
      parameter["up2k_pemula_peserta"] = $("#up2k_pemula_peserta").val();
      parameter["up2k_madya_klp"] = $("#up2k_madya_klp").val();
      parameter["up2k_madya_peserta"] = $("#up2k_madya_peserta").val();
      parameter["up2k_utama_klp"] = $("#up2k_utama_klp").val();
      parameter["up2k_utama_peserta"] = $("#up2k_utama_peserta").val();
      parameter["up2k_mandiri_klp"] = $("#up2k_mandiri_klp").val();
      parameter["up2k_mandiri_peserta"] = $("#up2k_mandiri_peserta").val();
      parameter["koperasi_berbadan_klp"] = $("#koperasi_berbadan_klp").val();
      parameter["koperasi_berbadan_anggota"] = $("#koperasi_berbadan_anggota").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>pokja_dua/simpan_pokja_dua';

      var parameterRv = ['id_desa', 'temp', 'warga_masih_3buta'];
      var Rv = RequiredValid(parameterRv);
      if (Rv == 0) {
        alertify.error('Mohon data diisi secara lengkap');
      } else {
        SimpanData(parameter, url);
        AfterSavedPokja_dua();
      }
      $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_data_pokja_dua').on('click', '.update_id_pokja_dua', function() {
      $('#mode').val('edit');
      $('#simpan_pokja_dua').hide();
      $('#update_pokja_dua').show();
      $('#overlay_data_anggota_keluarga').show();
      $('#tbl_data_anggota_keluarga').html('');
      var id_pokja_dua = $(this).closest('tr').attr('id_pokja_dua');
      var mode = $('#mode').val();
      var value = $(this).closest('tr').attr('id_pokja_dua');
      $('#form_baru').show();
      $('#judul_formulir').html('FORMULIR EDIT');
      $('#id_pokja_dua').val(id_pokja_dua);
      $.ajax({
        type: 'POST',
        async: true,
        data: {
          id_pokja_dua: id_pokja_dua
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>pokja_dua/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_propinsi').val(json[i].id_propinsi);
            $('#id_kabupaten').val(json[i].id_kabupaten);
            $('#id_kecamatan').val(json[i].id_kecamatan);
            $('#id_desa').val(json[i].id_desa);
            $('#id_dusun').val(json[i].id_dusun);
            $('#rt').val(json[i].rt);
            $('#tahun').val(json[i].tahun);
            $('#warga_masih_3buta').val(json[i].warga_masih_3buta);
            $('#paketa_kelbelajar').val(json[i].paketa_kelbelajar);
            $('#paketa_wargabelajar').val(json[i].paketa_wargabelajar);
            $('#paketb_kelbelajar').val(json[i].paketb_kelbelajar);
            $('#paketb_wargabelajar').val(json[i].paketb_wargabelajar);
            $('#paketc_kelbelajar').val(json[i].paketc_kelbelajar);
            $('#paketc_wargabelajar').val(json[i].paketc_wargabelajar);
            $('#kf_kelbelajar').val(json[i].kf_kelbelajar);
            $('#kf_wargabelajar').val(json[i].kf_wargabelajar);
            $('#paud_sejenis').val(json[i].paud_sejenis);
            $('#tamanbacaan_perpustakaan').val(json[i].tamanbacaan_perpustakaan);
            $('#bkb_kel').val(json[i].bkb_kel);
            $('#bkb_ibu_peserta').val(json[i].bkb_ibu_peserta);
            $('#bkb_ape').val(json[i].bkb_ape);
            $('#bkb_kecsimulasibkb').val(json[i].bkb_kecsimulasibkb);
            $('#kaderkhusus_tutorkf').val(json[i].kaderkhusus_tutorkf);
            $('#kaderkhusus_tutorpaud_sejenis').val(json[i].kaderkhusus_tutorpaud_sejenis);
            $('#kaderkhusus_bkb').val(json[i].kaderkhusus_bkb);
            $('#kaderkhusus_koperasi').val(json[i].kaderkhusus_koperasi);
            $('#kaderkhusus_keterampilan').val(json[i].kaderkhusus_keterampilan);
            $('#kaderyangsudah_lp3pkk').val(json[i].kaderyangsudah_lp3pkk);
            $('#kaderyangsudah_tpk3pkk').val(json[i].kaderyangsudah_tpk3pkk);
            $('#kaderyangsudah_damaspkk').val(json[i].kaderyangsudah_damaspkk);
            $('#up2k_pemula_klp').val(json[i].up2k_pemula_klp);
            $('#up2k_pemula_peserta').val(json[i].up2k_pemula_peserta);
            $('#up2k_madya_klp').val(json[i].up2k_madya_klp);
            $('#up2k_madya_peserta').val(json[i].up2k_madya_peserta);
            $('#up2k_utama_klp').val(json[i].up2k_utama_klp);
            $('#up2k_utama_peserta').val(json[i].up2k_utama_peserta);
            $('#up2k_mandiri_klp').val(json[i].up2k_mandiri_klp);
            $('#up2k_mandiri_peserta').val(json[i].up2k_mandiri_peserta);
            $('#koperasi_berbadan_klp').val(json[i].koperasi_berbadan_klp);
            $('#koperasi_berbadan_anggota').val(json[i].koperasi_berbadan_anggota);
            $('#keterangan').val(json[i].keterangan);
            load_option_desa();
          }
        }
      });
      //AttachmentByMode(mode, value);
      //AfterSavedAnggota_keluarga(mode, value);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_pokja_dua').on('click', function(e) {
      e.preventDefault();
      var parameter = ['id_pokja_dua', 'id_desa', 'temp', 'warga_masih_3buta'];
      InputValid(parameter);

      var parameter = {}
      parameter["id_pokja_dua"] = $("#id_pokja_dua").val();
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["tahun"] = $("#tahun").val();
      parameter["warga_masih_3buta"] = $("#warga_masih_3buta").val();
      parameter["paketa_kelbelajar"] = $("#paketa_kelbelajar").val();
      parameter["paketa_wargabelajar"] = $("#paketa_wargabelajar").val();
      parameter["paketb_kelbelajar"] = $("#paketb_kelbelajar").val();
      parameter["paketb_wargabelajar"] = $("#paketb_wargabelajar").val();
      parameter["paketc_kelbelajar"] = $("#paketc_kelbelajar").val();
      parameter["paketc_wargabelajar"] = $("#paketc_wargabelajar").val();
      parameter["kf_kelbelajar"] = $("#kf_kelbelajar").val();
      parameter["kf_wargabelajar"] = $("#kf_wargabelajar").val();
      parameter["paud_sejenis"] = $("#paud_sejenis").val();
      parameter["tamanbacaan_perpustakaan"] = $("#tamanbacaan_perpustakaan").val();
      parameter["bkb_kel"] = $("#bkb_kel").val();
      parameter["bkb_ibu_peserta"] = $("#bkb_ibu_peserta").val();
      parameter["bkb_ape"] = $("#bkb_ape").val();
      parameter["bkb_kecsimulasibkb"] = $("#bkb_kecsimulasibkb").val();
      parameter["kaderkhusus_tutorkf"] = $("#kaderkhusus_tutorkf").val();
      parameter["kaderkhusus_tutorpaud_sejenis"] = $("#kaderkhusus_tutorpaud_sejenis").val();
      parameter["kaderkhusus_bkb"] = $("#kaderkhusus_bkb").val();
      parameter["kaderkhusus_koperasi"] = $("#kaderkhusus_koperasi").val();
      parameter["kaderkhusus_keterampilan"] = $("#kaderkhusus_keterampilan").val();
      parameter["kaderyangsudah_lp3pkk"] = $("#kaderyangsudah_lp3pkk").val();
      parameter["kaderyangsudah_tpk3pkk"] = $("#kaderyangsudah_tpk3pkk").val();
      parameter["kaderyangsudah_damaspkk"] = $("#kaderyangsudah_damaspkk").val();
      parameter["up2k_pemula_klp"] = $("#up2k_pemula_klp").val();
      parameter["up2k_pemula_peserta"] = $("#up2k_pemula_peserta").val();
      parameter["up2k_madya_klp"] = $("#up2k_madya_klp").val();
      parameter["up2k_madya_peserta"] = $("#up2k_madya_peserta").val();
      parameter["up2k_utama_klp"] = $("#up2k_utama_klp").val();
      parameter["up2k_utama_peserta"] = $("#up2k_utama_peserta").val();
      parameter["up2k_mandiri_klp"] = $("#up2k_mandiri_klp").val();
      parameter["up2k_mandiri_peserta"] = $("#up2k_mandiri_peserta").val();
      parameter["koperasi_berbadan_klp"] = $("#koperasi_berbadan_klp").val();
      parameter["koperasi_berbadan_anggota"] = $("#koperasi_berbadan_anggota").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>pokja_dua/update_pokja_dua';

      var parameterRv = ['id_pokja_dua', 'id_desa', 'temp', 'warga_masih_3buta'];
      var Rv = RequiredValid(parameterRv);
      if (Rv == 0) {
        alertify.error('Mohon data diisi secara lengkap');
      } else {
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_data_pokja_dua').on('click', '#del_ajax_pokja_dua', function() {
      var id_pokja_dua = $(this).closest('tr').attr('id_pokja_dua');
      alertify.confirm('Anda yakin data akan dihapus?', function(e) {
        if (e) {
          var parameter = {}
          parameter["id_pokja_dua"] = id_pokja_dua;
          var url = '<?php echo base_url(); ?>pokja_dua/hapus/';
          HapusData(parameter, url);
          $('[id_pokja_dua=' + id_pokja_dua + ']').remove();
        } else {
          alertify.error('Hapus data dibatalkan');
        }
        $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
        load_data(tabel);
      });
    });
  });
</script>