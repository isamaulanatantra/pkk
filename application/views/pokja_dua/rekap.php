
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 style="display:none;">Pokja_dua</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>pokja_dua">Home</a></li>
              <li class="breadcrumb-item active">Pokja_dua</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
          
										<div class="card">
											<div class="card-header">
												<h3 class="card-title"><i class="fa fa-th"></i> DATA KEGIATAN POKJA II</h3>
												<div class="card-tools" style="display:none;">
													<div class="input-group input-group-sm">
                          <input name="tabel" id="tabel" value="pokja_dua" type="hidden" value="">
                            <select class="form-control input-sm pull-right" id="keyword" name="keyword" style="width: 150px;">
                              <option value="2018">2018</option>
                              <option value="2019">2019</option>
                              <option value="2017">2017</option>
                              <option value="2016">2016</option>
                            </select>
														<select name="limit" class="form-control input-sm pull-right" style="display:none;width: 150px;" id="limit">
															<option value="999999999">Semua</option>
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="display:none; width: 150px;" id="orderby">
															<option value="pokja_dua.tahun">tahun</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_pokja_dua"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<thead>
                              <tr>
                                <th rowspan="5">NO</th>
                                <th rowspan="5">NAMA WILAYAH</th>
                                <th colspan="23">PENDIDIKAN DAN KETERAMPILAN</th>
                                <th colspan="10">PENGEMBANGAN KEHIDUPAN BERKOPERASI</th>
                                <th rowspan="5">KET</th>
                              </tr>
                              <tr>
                                <th rowspan="4">WARGA MASIH 3 BUTA</th>
                                <th colspan="22">JUMLAH KELOMPOK BELAJAR</th>
                                <th colspan="8">PRA KOPERASI USAHA BERSAMA, UP2K</th>
                                <th colspan="2">KOPERASI BERBADAN</th>
                              </tr>
                              <tr>
                                <th colspan="2">PAKET A</th>
                                <th colspan="2">PAKET B</th>
                                <th colspan="2">PAKET C</th>
                                <th colspan="3">KF</th>
                                <th rowspan="3">PAUD<br>SEJENISNYA</th>
                                <th rowspan="2">TAMAN BACAAN<br>PERPUSTAKAAN</th>
                                <th colspan="4">BKB</th>
                                <th colspan="5">KADER KHUSUS</th>
                                <th colspan="3">KADER YANG SUDAH</th>
                                <th colspan="2">PEMULA</th>
                                <th colspan="2">MADYA</th>
                                <th colspan="2">UTAMA</th>
                                <th colspan="2">MANDIRI</th>
                                <th rowspan="3">JUMLAH KLP</th>
                                <th rowspan="3">JUMLAH ANGGOTA</th>
                              </tr>
                              <tr>
                                <th rowspan="2">KEL. BELAJAR</th>
                                <th rowspan="2">WARGA BELAJAR</th>
                                <th rowspan="2">KEL. BELAJAR</th>
                                <th rowspan="2">WARGA BELAJAR</th>
                                <th rowspan="2">KEL. BELAJAR</th>
                                <th rowspan="2">WARGA BELAJAR</th>
                                <th rowspan="2">KEL. BELAJAR</th>
                                <th rowspan="2">WARGA BELAJAR</th>
                                <th rowspan="2">JML.KEL</th>
                                <th rowspan="2">JML. IBU <br>PESERTA</th>
                                <th rowspan="2">JML. <br>APE</th>
                                <th rowspan="2">JML. KEC. <br>SIMULASI</th>
                                <th colspan="2">TUTOR</th>
                                <th rowspan="2">BKB</th>
                                <th rowspan="2">KOPERASI</th>
                                <th rowspan="2">KETERAMPILAN</th>
                                <th rowspan="2">LP.3PKK</th>
                                <th rowspan="2">TPK 3 PKK</th>
                                <th rowspan="2">DAMAS PKK</th>
                                <th rowspan="2">JML. KLP</th>
                                <th rowspan="2">JML. ANG</th>
                                <th rowspan="2">JML. KLP</th>
                                <th rowspan="2">JML. ANG</th>
                                <th rowspan="2">JML. KLP</th>
                                <th rowspan="2">JML. ANG</th>
                                <th rowspan="2">JML. KLP</th>
                                <th rowspan="2">JML. ANG</th>
                              </tr>
                              <tr>
                                <th>KF</th>
                                <th>PAUD<br>SEJENIS</th>
                              </tr>
													</thead>
													<tbody id="tbl_data_pokja_dua">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination" style="display:none;">
												</ul>
												<div class="overlay" id="spinners_tbl_data_pokja_dua" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
          </div>
        </div>
        <!-- /.row -->
				

    </section>
				
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_rekap_'+tabel+'/',
      success: function(html) {
        // $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_pokja_dua').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    load_data(tabel);
});
</script>