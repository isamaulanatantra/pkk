<table>
  <tr>
    <th colspan="4">
			<center>
				<table style="width:70%;">
					<tr>
						<td>
							<center>
							<h2><?php if(!empty($judul_permohonan_pendaftaran_badan_usaha)){ echo strtoupper($judul_permohonan_pendaftaran_badan_usaha); } ?></h2>
							</center>
						</td>
					</tr>
				</table>
			</center>
		</th>
		<th>
		</th>
  </tr>
  <tr>
    <td colspan="5">
		<!--<hr />-->
		</td>
  </tr>
  <tr>
    <td colspan="5">
			<center>
				<table style="width:90%;">
					<tr>
						<td></td>
						<td style="width:290px;"></td>
						<td colspan="3" style="text-align:right;"><?php if(!empty($daerah_domisili_pemohon)){ echo $daerah_domisili_pemohon; } ?>, <?php if(!empty($tanggal_permohonan_pendaftaran_badan_usaha)){ echo $tanggal_permohonan_pendaftaran_badan_usaha; } ?></td>
					</tr>
					<tr>
						<td>Nomor</td>
						<td style="width:440px;">: <?php if(!empty($nomor_permohonan_pendaftaran_badan_usaha)){ echo $nomor_permohonan_pendaftaran_badan_usaha; } ?></td>
						<td></td>
						<td>Kepada :</td>
						<td></td>
					</tr>
					<tr>
						<td>Lampiran</td>
						<td>: satu (1) bendel</td>
						<td style="width:50px;">Yth. : </td>
						<td><?php if(!empty($yth_permohonan_pendaftaran_badan_usaha)){ echo $yth_permohonan_pendaftaran_badan_usaha; } ?></td>
						<td></td>
					</tr>
					<tr>
						<td>Perihal</td>
						<td>: <?php if(!empty($perihal_permohonan_pendaftaran_badan_usaha)){ echo $perihal_permohonan_pendaftaran_badan_usaha; } ?></td>
						<td>Cq.</td>
						<td><?php if(!empty($cq_permohonan_pendaftaran_badan_usaha)){ echo $cq_permohonan_pendaftaran_badan_usaha; } ?></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>Di_</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><?php if(!empty($di_permohonan_pendaftaran_badan_usaha)){ echo strtoupper($di_permohonan_pendaftaran_badan_usaha); } ?></td>
						<td></td>
					</tr>
				</table>
			</center>
			<center>
				<table style="width:80%;">
					<tr>
						<td colspan="5">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Yang bertanda tangan dibawah ini :</td>
					</tr>
					<tr>
						<td style="width:250px;">a. Nama Pengusaha</td>
						<td colspan="4">: <?php if(!empty($nama_pengusaha)){ $nama_pengusaha1=strtolower($nama_pengusaha); echo ucwords($nama_pengusaha1); } ?></td>
					</tr>
					<tr>
						<td>b. Tempat, Tanggal Lahir</td>
						<td colspan="4">: <?php if(!empty($tempat_lahir_pengusaha)){ echo $tempat_lahir_pengusaha; } ?>, <?php if(!empty($tanggal_lahir_pengusaha)){ echo $tanggal_lahir_pengusaha; } ?></td>
					</tr>
					<tr>
						<td>c. Pekerjaan</td>
						<td colspan="4">: <?php if(!empty($pekerjaan_pengusaha)){ echo $pekerjaan_pengusaha; } ?></td>
					</tr>
					<tr>
						<td>d. No Telp / Hp</td>
						<td colspan="4">: <?php if(!empty($nomor_telp_pengusaha)){ echo $nomor_telp_pengusaha; } ?></td>
					</tr>
					<tr>
						<td>e. Alamat Pengusaha</td>
						<td colspan="4">: <?php if(!empty($alamat_pengusaha)){ $alamat_pengusaha1=strtolower($alamat_pengusaha);echo ucwords($alamat_pengusaha1); } ?>, Desa <?php if(!empty($nama_desa_pengusaha)){ $nama_desa_pengusaha1=strtolower($nama_desa_pengusaha);echo ucwords($nama_desa_pengusaha1); } ?>, Kecamatan <?php if(!empty($nama_kecamatan_pengusaha)){ $nama_kecamatan_pengusaha1=strtolower($nama_kecamatan_pengusaha); echo ucwords($nama_kecamatan_pengusaha1); } ?>, Kabupaten <?php if(!empty($nama_kabupaten_pengusaha)){ $nama_kabupaten_pengusaha1=strtolower($nama_kabupaten_pengusaha); echo ucwords($nama_kabupaten_pengusaha1); } ?>, Provinsi <?php if(!empty($nama_propinsi_pengusaha)){ $nama_propinsi_pengusaha1=strtolower($nama_propinsi_pengusaha); echo ucwords($nama_propinsi_pengusaha1); } ?></td>
					</tr>
					<tr>
						<td colspan="5"><br />Mengajukan dengan hormat permohonan Pendaftaran Usaha Daya Tarik Wisata atas :</td>
					</tr>
					<tr>
						<td>a. Nama Perusahaan</td>
						<td colspan="4">: <?php if(!empty($nama_perusahaan)){ echo $nama_perusahaan; } ?></td>
					</tr>
					<tr>
						<td>b. Jenis Usaha Jasa</td>
						<td colspan="4">: <?php if(!empty($id_jenis_usaha_jasa_perusahaan)){ echo $id_jenis_usaha_jasa_perusahaan; } ?></td>
					</tr>
					<tr>
						<td>c. Alamat Perusahaan</td>
						<td colspan="4">: <?php if(!empty($alamat_perusahaan)){ $alamat_perusahaan1=strtolower($alamat_perusahaan);echo ucwords($alamat_perusahaan1); } ?>, Desa <?php if(!empty($nama_desa_perusahaan)){ $nama_desa_perusahaan1=strtolower($nama_desa_perusahaan);echo ucwords($nama_desa_perusahaan1); } ?>, Kecamatan <?php if(!empty($nama_kecamatan_perusahaan)){ $nama_kecamatan_perusahaan1=strtolower($nama_kecamatan_perusahaan); echo ucwords($nama_kecamatan_perusahaan1); } ?>, Kabupaten <?php if(!empty($nama_kabupaten_perusahaan)){ $nama_kabupaten_perusahaan1=strtolower($nama_kabupaten_perusahaan); echo ucwords($nama_kabupaten_perusahaan1); } ?>, Provinsi <?php if(!empty($nama_propinsi_perusahaan)){ $nama_propinsi_perusahaan1=strtolower($nama_propinsi_perusahaan); echo ucwords($nama_propinsi_perusahaan1); } ?></td>
					</tr>
					<tr>
						<td>d. No Telp / Hp</td>
						<td colspan="4">: <?php if(!empty($nomor_telp_perusahaan)){ echo $nomor_telp_perusahaan; } ?></td>
					</tr>
					<tr>
						<td colspan="5"><br />Sebagai kelengkapan pengajuan kami lampirkan : <br />
						1. Copy Kartu Tanda Penduduk Pengusaha;<br />
						2. Copy NPWP Pengusaha dan Perusahaan;<br />
						3. Copy Akta Pendirian Perusahaan dan perubahannya jika ada )3<br />
						4. Copy Sertifikat / Keterangan Hak Milik )4<br />
						5. Profil Perusahaan / Profil Pendirian Usaha)5<br />
						6. Copy Izin Teknis, yaitu :<br />
						&nbsp;&nbsp;&nbsp;&nbsp;a. Izin Tempat (untuk usaha yang menempati bangunan/lahan orang lain)<br />
						&nbsp;&nbsp;&nbsp;&nbsp;b. Perjanjian Sewa (untuk usaha yang menempati bangunan/lahan dengan sewa<br />
						&nbsp;&nbsp;&nbsp;&nbsp;c. Izin Operasional lainnya ( IMB, TDP, SIUP, dll)<br />
						7. Copy Izin Lingkungan Hidup, yaitu :<br />
						&nbsp;&nbsp;&nbsp;&nbsp;a. Amdal (Analisis Mengenai Dampak Lingkungan Hidup);<br />
						&nbsp;&nbsp;&nbsp;&nbsp;b. UKL – UPL (Upaya Pengelolaan Lingkungan – Upaya Pemantauan Lingkungan);<br />
						&nbsp;&nbsp;&nbsp;&nbsp;c. SPPL (Surat Pernyataan Pengelolaan Lingkungan).
						</td>
					</tr>
					<tr>
						<td colspan="5"><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian untuk menjadikan periksa dan terimakasih.</td>
					</tr>
					<tr>
						<td colspan="5">
							<table style="width:100%;">
								<tr>
									<td style="width:290px;"></td>
									<td style="width:290px;"></td>
									<td style="width:290px;"><center><strong>Pemohon</strong><br><br><br><br>
									<strong><u><?php if(!empty($nama_pengusaha)){ echo $nama_pengusaha; } ?></u></strong></center></td>
								</table>
							</tr>
						</td>
					</tr>
					<tr>
						<td colspan="5">Keterangan :<br />
				&nbsp;&nbsp;&nbsp;&nbsp;1) Diisi daerah domisili pemohon;<br />
				&nbsp;&nbsp;&nbsp;&nbsp;2) Berupa Pengelolaan Pemandian Air Panas Alami, Pengelolaan Goa, Pengelolaan Peninggalan Sejarah dan Purbakala (berupa Candi, Keraton, Prasasti, Pertilasan, dan Bangunan Kuno), Pengelolaan Museum, Pengelolaan Pemukiman dan/atau Lingkungan Adat, Pengelolaan Objek Ziarah, dan Wisata Agro;<br />
				&nbsp;&nbsp;&nbsp;&nbsp;3) Jika Berbentuk Badan Usaha Indonesia Berbadan Hukum;<br />
				&nbsp;&nbsp;&nbsp;&nbsp;4) Untuk usaha yang menempati tanah pribadi;<br />
				&nbsp;&nbsp;&nbsp;&nbsp;5) Diisi Lampiran:<br />
				&nbsp;&nbsp;&nbsp;&nbsp;a. Kekayaan Bersih (Tidak termasuk tanah dan bangunan) dan Hasil penjualan tahunan;<br />
				&nbsp;&nbsp;&nbsp;&nbsp;b. Fotocopy bukti hak pengelolaan dari pemilik daya tarik wisata;<br />
				&nbsp;&nbsp;&nbsp;&nbsp;6) Dilampiri surat pernyataan kebenaran dokumen yang dilampirkan.<br />
						</td>
					</tr>
				</table>
			</center>
	  </td>
  </tr>
</table>
