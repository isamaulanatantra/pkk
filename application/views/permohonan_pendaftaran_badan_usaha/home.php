
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Bidang Usaha Daya Tarik Wisata</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Bidang Usaha Daya Tarik Wisata</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_form_permohonan_pendaftaran_badan_usaha" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab" id="klik_tab_data_permohonan_pendaftaran_badan_usaha">Data</a></li>
                </ul>
								<div class="card-tools">
									<a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>permohonan_pendaftaran_badan_usaha"><i class="fa fa-refresh"></i></a>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
									<div class="tab-pane active" id="tab_form_permohonan_pendaftaran_badan_usaha">
										<input name="tabel" id="tabel" value="permohonan_pendaftaran_badan_usaha" type="hidden" value="">
										<form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=permohonan_pendaftaran_badan_usaha" enctype="multipart/form-data">
											<input name="page" id="page" value="1" type="hidden" value="">
											<div class="form-group" style="display:none;">
												<label for="temp">temp</label>
												<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="mode">mode</label>
												<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="id_permohonan_pendaftaran_badan_usaha">id_permohonan_pendaftaran_badan_usaha</label>
												<input class="form-control" id="id_permohonan_pendaftaran_badan_usaha" name="id" value="" placeholder="id_permohonan_pendaftaran_badan_usaha" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="icon">Icon</label>
												<select class="form-control" id="icon" name="icon" >
												<option value="fa-home">fa-home</option>
												<option value="fa-gears">fa-gears</option>
												<option value="fa-th">fa-th</option>
												<option value="fa-font">fa-font</option>
												<option value="fa-comment">fa-comment</option>
												<option value="fa-cogs">fa-cogs</option>
												<option value="fa-cloud-download">fa-cloud-download</option>
												<option value="fa-bar-char">fa-bar-char</option>
												<option value="fa-phone">fa-phone</option>
												<option value="fa-envelope">fa-envelope</option>
												<option value="fa-link">fa-link</option>
												<option value="fa-tasks">fa-tasks</option>
												<option value="fa-users">fa-users</option>
												<option value="fa-signal">fa-signal</option>
												<option value="fa-coffee">fa-coffee</option>
												</select>
												<div id="iconselected"></div>
											</div>
											<div class="form-group" style="display:none;">
												<label for="judul_permohonan_pendaftaran_badan_usaha">Judul Permohonan_pendaftaran_badan_usaha</label>
												<input class="form-control" id="judul_permohonan_pendaftaran_badan_usaha" name="judul_permohonan_pendaftaran_badan_usaha" value="Permohonan Pendaftaran Usaha Daya Tarik Wisata" placeholder="Judul Permohonan_pendaftaran_badan_usaha" type="text">
											</div>
											<div class="form-group">
												<label for="nomor_permohonan_pendaftaran_badan_usaha">Nomor</label>
												<input class="form-control" id="nomor_permohonan_pendaftaran_badan_usaha" name="nomor_permohonan_pendaftaran_badan_usaha" value="" placeholder="Nomor Permohonan_pendaftaran_badan_usaha" type="text">
											</div>
											<div class="form-group">
												<label for="perihal_permohonan_pendaftaran_badan_usaha">Perihal</label>
												<select class="form-control" id="perihal_permohonan_pendaftaran_badan_usaha" name="perihal_permohonan_pendaftaran_badan_usaha">
												</select>
											</div>
											<div class="form-group" style="display:none;">
												<label for="yth_permohonan_pendaftaran_badan_usaha">Yth.</label>
												<input class="form-control" id="yth_permohonan_pendaftaran_badan_usaha" name="yth_permohonan_pendaftaran_badan_usaha" value="Bupati Wonosobo" placeholder="Judul Permohonan_pendaftaran_badan_usaha" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="cq_permohonan_pendaftaran_badan_usaha">Cq.</label>
												<input class="form-control" id="cq_permohonan_pendaftaran_badan_usaha" name="cq_permohonan_pendaftaran_badan_usaha" value="Kepala Dinas Pariwisata dan Kebudayaan Kabupaten Wonosobo" placeholder="Judul Permohonan_pendaftaran_badan_usaha" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="di_permohonan_pendaftaran_badan_usaha">Di.</label>
												<input class="form-control" id="di_permohonan_pendaftaran_badan_usaha" name="di_permohonan_pendaftaran_badan_usaha" value="Wonosobo" placeholder="Judul Permohonan_pendaftaran_badan_usaha" type="text">
											</div>
											<div class="form-group">
												<label for="tanggal_permohonan_pendaftaran_badan_usaha">Tanggal</label>
												<input class="form-control" id="tanggal_permohonan_pendaftaran_badan_usaha" name="tanggal_permohonan_pendaftaran_badan_usaha" value="<?php echo date('Y-m-d'); ?>" placeholder="Judul Permohonan_pendaftaran_badan_usaha" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="isi_permohonan_pendaftaran_badan_usaha">Isi Permohonan_pendaftaran_badan_usaha</label>
												<input class="form-control" id="isi_permohonan_pendaftaran_badan_usaha" name="isi_permohonan_pendaftaran_badan_usaha" value="" placeholder="Isi Permohonan_pendaftaran_badan_usaha" type="text">
											</div>
											<div class="row" style="display:none;">
											<textarea id="editor_isi_permohonan_pendaftaran_badan_usaha">Permohonan_pendaftaran_badan_usaha</textarea>
											</div>
											<div class="row">
												<div class="col-12">
													<div class="form-group">
														<label for="nama_pengusaha">Nama Pengusaha</label>
														<input class="form-control" id="nama_pengusaha" name="nama_pengusaha" value="" placeholder="Nama Pengusaha" type="text">
													</div>
													<div class="form-group">
														<label for="tempat_lahir_pengusaha">Tempat Lahir</label>
														<input class="form-control" id="tempat_lahir_pengusaha" name="tempat_lahir_pengusaha" value="<?php echo date('Y-m-d'); ?>" placeholder="Tempat Lahir" type="text">
													</div>
													<div class="form-group">
														<label for="tanggal_lahir_pengusaha">Tanggal Lahir</label>
														<input class="form-control" id="tanggal_lahir_pengusaha" name="tanggal_lahir_pengusaha" value="" placeholder="Tanggal Lahir" type="text">
													</div>
													<div class="form-group">
														<label for="pekerjaan_pengusaha">Pekerjaan</label>
														<select class="form-control" id="pekerjaan_pengusaha" name="pekerjaan_pengusaha">
														</select>
													</div>
													<div class="form-group">
														<label for="nomor_telp_pengusaha">Nomor Telp/Hp</label>
														<input class="form-control" id="nomor_telp_pengusaha" name="nomor_telp_pengusaha" value="" placeholder="Nomor Telp/Hp" type="text">
													</div>
													<div class="form-group">
														<label for="alamat_pengusaha">Alamat</label>
														<input class="form-control" id="alamat_pengusaha" name="alamat_pengusaha" value="" placeholder="Alamat" type="text">
													</div>
													<div class="form-group">
														<label for="id_propinsi_pengusaha">Provinsi</label>
														<select class="form-control" id="id_propinsi_pengusaha" name="id_propinsi_pengusaha">
														</select>
													</div>
													<div class="form-group">
														<label for="id_kabupaten_pengusaha">Kabupaten</label>
														<select class="form-control" id="id_kabupaten_pengusaha" name="id_kabupaten_pengusaha">
														</select>
													</div>
													<div class="form-group">
														<label for="id_kecamatan_pengusaha">Kecamatan</label>
														<select class="form-control" id="id_kecamatan_pengusaha" name="id_kecamatan_pengusaha">
														</select>
													</div>
													<div class="form-group">
														<label for="id_desa_pengusaha">Desa</label>
														<select class="form-control" id="id_desa_pengusaha" name="id_desa_pengusaha">
														</select>
													</div>
													<div class="form-group">
														<label for="daerah_domisili_pemohon">Daerah Domisili</label>
														<input class="form-control" id="daerah_domisili_pemohon" name="daerah_domisili_pemohon" value="" placeholder="Daerah Domisili" type="text">
													</div>
												</div>
												<div class="col-12">
													<div class="form-group">
														<label for="nama_perusahaan">Nama Perusahaan</label>
														<input class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="" placeholder="Nama Pengusaha" type="text">
													</div>
													<div class="form-group">
														<label for="id_jenis_usaha_jasa_perusahaan">Jenis Usaha Jasa</label>
														<select class="form-control" id="id_jenis_usaha_jasa_perusahaan" name="id_jenis_usaha_jasa_perusahaan">
														</select>
													</div>
													<div class="form-group">
														<label for="alamat_perusahaan">Alamat</label>
														<input class="form-control" id="alamat_perusahaan" name="alamat_perusahaan" value="" placeholder="Alamat" type="text">
													</div>
													<div class="form-group">
														<label for="id_propinsi_perusahaan">Provinsi</label>
														<select class="form-control" id="id_propinsi_perusahaan" name="id_propinsi_perusahaan">
														</select>
													</div>
													<div class="form-group">
														<label for="id_kabupaten_perusahaan">Kabupaten</label>
														<select class="form-control" id="id_kabupaten_perusahaan" name="id_kabupaten_perusahaan">
														</select>
													</div>
													<div class="form-group">
														<label for="id_kecamatan_perusahaan">Kecamatan</label>
														<select class="form-control" id="id_kecamatan_perusahaan" name="id_kecamatan_perusahaan">
														</select>
													</div>
													<div class="form-group">
														<label for="id_desa_perusahaan">Desa</label>
														<select class="form-control" id="id_desa_perusahaan" name="id_desa_perusahaan">
														</select>
													</div>
													<div class="form-group">
														<label for="nomor_telp_perusahaan">Nomor Telp./Hp</label>
														<input class="form-control" id="nomor_telp_perusahaan" name="nomor_telp_perusahaan" value="" placeholder="Nomor Telp./Hp" type="text">
													</div>
													<div class="row">
														<div class="col-md-12">
															<div class="alert alert-info alert-dismissable">
																<div class="form-group">
																	<label for="remake"><i class="fa fa-warning"></i> Sebagai kelengkapan pengajuan kami lampirkan : </label>
																	<select class="form-control" id="remake" name="remake" >
																	<option value="1. Copy Kartu Tanda Penduduk Pengusaha">Pilih Lampiran satu per satu</option>
																	<option value="1. Copy Kartu Tanda Penduduk Pengusaha">1. Copy Kartu Tanda Penduduk Pengusaha;</option>
																	<option value="2. Copy NPWP Pengusaha dan Perusahaan">2. Copy NPWP Pengusaha dan Perusahaan;</option>
																	<option value="3. Copy Akta Pendirian Perusahaan dan perubahannya jika ada">3. Copy Akta Pendirian Perusahaan dan perubahannya jika ada (3)</option>
																	<option value="4. Copy Sertifikat / Keterangan Hak Milik">4. Copy Sertifikat / Keterangan Hak Milik (4)</option>
																	<option value="5. Profil Perusahaan / Profil Pendirian Usaha">5. Profil Perusahaan / Profil Pendirian Usaha (5)</option>
																	<option value="6. Copy Izin Teknis">6. Copy Izin Teknis, yaitu :</option>
																	<option value="a. Izin Tempat (untuk usaha yang menempati bangunan/lahan orang lain)">&nbsp;&nbsp;&nbsp;&nbsp;a. Izin Tempat (untuk usaha yang menempati bangunan/lahan orang lain)</option>
																	<option value="b. Perjanjian Sewa (untuk usaha yang menempati bangunan/lahan dengan sewa">&nbsp;&nbsp;&nbsp;&nbsp;b. Perjanjian Sewa (untuk usaha yang menempati bangunan/lahan dengan sewa</option>
																	<option value="c. Izin Operasional lainnya ( IMB, TDP, SIUP, dll)">&nbsp;&nbsp;&nbsp;&nbsp;c. Izin Operasional lainnya ( IMB, TDP, SIUP, dll)</option>
																	<option value="7. Copy Izin Lingkungan Hidup">7. Copy Izin Lingkungan Hidup, yaitu :</option>
																	<option value="a. Amdal (Analisis Mengenai Dampak Lingkungan Hidup)">&nbsp;&nbsp;&nbsp;&nbsp;a. Amdal (Analisis Mengenai Dampak Lingkungan Hidup);</option>
																	<option value="b. UKL – UPL (Upaya Pengelolaan Lingkungan – Upaya Pemantauan Lingkungan)">&nbsp;&nbsp;&nbsp;&nbsp;b. UKL – UPL (Upaya Pengelolaan Lingkungan – Upaya Pemantauan Lingkungan);</option>
																	<option value="c. SPPL (Surat Pernyataan Pengelolaan Lingkungan)">&nbsp;&nbsp;&nbsp;&nbsp;c. SPPL (Surat Pernyataan Pengelolaan Lingkungan).</option>
																	</select>
																</div>
																<div class="form-group">
																	<label for="myfile">File Lampiran </label>
																	<input type="file" size="60" name="myfile" id="file_lampiran" >
																</div>
																<div id="ProgresUpload">
																	<div id="BarProgresUpload"></div>
																	<div id="PersenProgresUpload">0%</div >
																</div>
																<div id="PesanProgresUpload"></div>
															</div>
															<div class="alert alert-info alert-dismissable table-responsive">
																<h3 class="card-title">Data Lampiran </h3>
																<table class="table table-bordered">
																	<tr>
																		<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
																	</tr>
																	<tbody id="tbl_attachment_permohonan_pendaftaran_badan_usaha">
																	</tbody>
																</table>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="form-group" style="display:none;">
												<label for="keterangan">Keterangan Permohonan_pendaftaran_badan_usaha</label>
												<input class="form-control" id="keterangan" name="keterangan" value="-" placeholder="Keterangan" type="text">
											</div>
											<div class="form-group">
											<button type="submit" class="btn btn-primary" id="simpan_permohonan_pendaftaran_badan_usaha">SIMPAN</button>
											<button type="submit" class="btn btn-primary" id="update_permohonan_pendaftaran_badan_usaha" style="display:none;">UPDATE</button>
											</div>
										</form>

										<div class="overlay" id="overlay_form_input" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
									<div class="tab-pane table-responsive" id="tab_2">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
															<option value="999999999">Semua</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="permohonan_pendaftaran_badan_usaha.nomor_permohonan_pendaftaran_badan_usaha">Nomor</option>
															<option value="permohonan_pendaftaran_badan_usaha.perihal_permohonan_pendaftaran_badan_usaha">Perihal</option>
															<option value="permohonan_pendaftaran_badan_usaha.tanggal_permohonan_pendaftaran_badan_usaha">Tanggal</option>
														</select>
														<select name="perihal_surat" class="form-control input-sm pull-right" style="width: 350px;" id="perihal_surat">
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_permohonan_pendaftaran_badan_usaha"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<tr>
														<th>No.</th>
														<th>Nomor</th>
														<th>Tanggal</th>
														<th>Perihal</th>
														<th>Pengusaha</th>
														<th>Perusahaan</th>
														<th>PROSES</th> 
													</tr>
													<tbody id="tbl_data_permohonan_pendaftaran_badan_usaha">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_permohonan_pendaftaran_badan_usaha" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var perihal_surat = $('#perihal_surat').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        perihal_surat:perihal_surat
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>permohonan_pendaftaran_badan_usaha/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '" perihal_surat="' + perihal_surat + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var perihal_surat = $('#perihal_surat').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        perihal_surat:perihal_surat
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<!-- perihal_surat -->
<script type="text/javascript">
$(document).ready(function() {
	load_perihal_surat();
});
</script>
<script type="text/javascript">
  function load_perihal_surat() {
    $('#perihal_surat').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>perihal_permohonan_pendaftaran_badan_usaha/perihal_surat/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Perihal</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_perihal_permohonan_pendaftaran_badan_usaha + '" >' + json[i].judul_perihal_permohonan_pendaftaran_badan_usaha + '</option>';
        }
        $('#perihal_surat').append(option);
      }
    });
  }
</script>
<!-- end perihal_surat -->
<!-- perihal_permohonan_pendaftaran_badan_usaha -->
<script type="text/javascript">
$(document).ready(function() {
	load_perihal_permohonan_pendaftaran_badan_usaha();
});
</script>
<script type="text/javascript">
  function load_perihal_permohonan_pendaftaran_badan_usaha() {
    $('#perihal_permohonan_pendaftaran_badan_usaha').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>perihal_permohonan_pendaftaran_badan_usaha/json_option/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Perihal</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_perihal_permohonan_pendaftaran_badan_usaha + '" >' + json[i].judul_perihal_permohonan_pendaftaran_badan_usaha + '</option>';
        }
        $('#perihal_permohonan_pendaftaran_badan_usaha').append(option);
      }
    });
  }
</script>
<!-- end perihal_permohonan_pendaftaran_badan_usaha -->
<!-- id_jenis_usaha_jasa_perusahaan -->
<script type="text/javascript">
$(document).ready(function() {
	load_perihal_id_jenis_usaha_jasa_perusahaan();
});
</script>
<script type="text/javascript">
  function load_perihal_id_jenis_usaha_jasa_perusahaan() {
    $('#id_jenis_usaha_jasa_perusahaan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>jenis_usaha_jasa/json_option/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Jenis Usaha Jasa</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_jenis_usaha_jasa + '" >' + json[i].judul_jenis_usaha_jasa + '</option>';
        }
        $('#id_jenis_usaha_jasa_perusahaan').append(option);
      }
    });
  }
</script>
<!-- end id_jenis_usaha_jasa_perusahaan -->
<!-- pekerjaan_pengusaha -->
<script type="text/javascript">
$(document).ready(function() {
	load_pekerjaan();
});
</script>
<script type="text/javascript">
  function load_pekerjaan() {
    $('#pekerjaan_pengusaha').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>pekerjaan/json_option_pekerjaan/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Pekerjaan</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_pekerjaan + '" >' + json[i].nama_pekerjaan + '</option>';
        }
        $('#pekerjaan_pengusaha').append(option);
      }
    });
  }
</script>
<!-- end pekerjaan_pengusaha -->
<!-- Wilayah -->
<script type="text/javascript">
$(document).ready(function() {
	load_propinsi();
});
</script>
<script type="text/javascript">
  function load_propinsi() {
    $('#id_propinsi_pengusaha').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_propinsi/',
      success: function(json) {
        var option = '';
        option += '<option value="" >Pilih Provinsi</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_propinsi + '" >' + json[i].nama_propinsi + '</option>';
        }
        $('#id_propinsi_pengusaha').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_propinsi_pengusaha').on('change', function(e) {
      e.preventDefault();
      var id_propinsi = $('#id_propinsi_pengusaha').val();
      load_kabupaten_by_id_propinsi(id_propinsi);
    });
});
</script>
<script type="text/javascript">
  function load_kabupaten_by_id_propinsi(id_propinsi) {
    $('#id_kabupaten_pengusaha').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_propinsi:id_propinsi
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_kabupaten_by_id_propinsi/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Kabupaten</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_kabupaten + '" >' + json[i].nama_kabupaten + '</option>';
        }
        $('#id_kabupaten_pengusaha').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_kabupaten_pengusaha').on('change', function(e) {
      e.preventDefault();
      var id_kabupaten = $('#id_kabupaten_pengusaha').val();
      load_kecamatan_by_id_kabupaten(id_kabupaten);
    });
});
</script>
<script type="text/javascript">
  function load_kecamatan_by_id_kabupaten(id_kabupaten) {
    $('#id_kecamatan_pengusaha').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kabupaten:id_kabupaten
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_kecamatan_by_id_kabupaten/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Kecamatan</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_kecamatan + '" >' + json[i].nama_kecamatan + '</option>';
        }
        $('#id_kecamatan_pengusaha').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_desa_by_id_kecamatan(id_kecamatan) {
    $('#id_desa_pengusaha').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kecamatan:id_kecamatan
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_desa_by_id_kecamatan/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Desa</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_desa + '" >' + json[i].nama_desa + '</option>';
        }
        $('#id_desa_pengusaha').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_kecamatan_pengusaha').on('change', function(e) {
      e.preventDefault();
      var id_kecamatan = $('#id_kecamatan_pengusaha').val();
      load_desa_by_id_kecamatan(id_kecamatan);
    });
});
</script>
<!-- end Wilayah -->
<!-- Wilayah Pemohon -->
<script type="text/javascript">
$(document).ready(function() {
	load_propinsi_id_propinsi_perusahaan();
});
</script>
<script type="text/javascript">
  function load_propinsi_id_propinsi_perusahaan() {
    $('#id_propinsi_perusahaan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_propinsi_id_propinsi_perusahaan/',
      success: function(json) {
        var option = '';
        option += '<option value="" >Pilih Provinsi</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_propinsi + '" >' + json[i].nama_propinsi + '</option>';
        }
        $('#id_propinsi_perusahaan').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_propinsi_perusahaan').on('change', function(e) {
      e.preventDefault();
      var id_propinsi = $('#id_propinsi_perusahaan').val();
      load_kabupaten_by_id_propinsi_id_propinsi_perusahaan(id_propinsi);
    });
});
</script>
<script type="text/javascript">
  function load_kabupaten_by_id_propinsi_id_propinsi_perusahaan(id_propinsi) {
    $('#id_kabupaten_perusahaan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_propinsi:id_propinsi
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_kabupaten_by_id_propinsi_id_kabupaten_perusahaan/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Kabupaten</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_kabupaten + '" >' + json[i].nama_kabupaten + '</option>';
        }
        $('#id_kabupaten_perusahaan').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_kabupaten_perusahaan').on('change', function(e) {
      e.preventDefault();
      var id_kabupaten = $('#id_kabupaten_perusahaan').val();
      load_kecamatan_by_id_kabupaten_id_kabupaten_perusahaan(id_kabupaten);
    });
});
</script>
<script type="text/javascript">
  function load_kecamatan_by_id_kabupaten_id_kabupaten_perusahaan(id_kabupaten) {
    $('#id_kecamatan_perusahaan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kabupaten:id_kabupaten
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_kecamatan_by_id_kabupaten_id_kecamatan_perusahaan/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Kecamatan</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_kecamatan + '" >' + json[i].nama_kecamatan + '</option>';
        }
        $('#id_kecamatan_perusahaan').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_desa_by_id_kecamatan_id_desa_perusahaan(id_kecamatan) {
    $('#id_desa_perusahaan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kecamatan:id_kecamatan
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_desa_by_id_kecamatan_id_desa_perusahaan/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Desa</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_desa + '" >' + json[i].nama_desa + '</option>';
        }
        $('#id_desa_perusahaan').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_kecamatan_perusahaan').on('change', function(e) {
      e.preventDefault();
      var id_kecamatan = $('#id_kecamatan_perusahaan').val();
      load_desa_by_id_kecamatan_id_desa_perusahaan(id_kecamatan);
    });
});
</script>
<!-- end Wilayah -->
<script>
  function AfterSavedPermohonan_pendaftaran_badan_usaha() {
    $('#id_permohonan_pendaftaran_badan_usaha, #judul_permohonan_pendaftaran_badan_usaha, #isi_permohonan_pendaftaran_badan_usaha, #icon, #keterangan, #perihal_permohonan_pendaftaran_badan_usaha, #yth_permohonan_pendaftaran_badan_usaha, #cq_permohonan_pendaftaran_badan_usaha, #di_permohonan_pendaftaran_badan_usaha, #tanggal_permohonan_pendaftaran_badan_usaha, #nama_pengusaha, #tempat_lahir_pengusaha, #tanggal_lahir_pengusaha, #pekerjaan_pengusaha, #nomor_telp_pengusaha, #alamat_pengusaha, #id_desa_pengusaha, #id_kecamatan_pengusaha, #id_kabupaten_pengusaha, #id_propinsi_pengusaha, #nama_perusahaan, #id_jenis_usaha_jasa_perusahaan, #alamat_perusahaan, #id_desa_perusahaan, #id_kecamatan_perusahaan, #id_kabupaten_perusahaan, #id_propinsi_perusahaan, #nomor_telp_perusahaan, #daerah_domisili_pemohon').val('');
    $('#tbl_attachment_permohonan_pendaftaran_badan_usaha').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#icon').on('change', function(e) {
      e.preventDefault();
      var xa = $('#icon').val();
      $("#iconselected").html('<span class="fa '+xa+' ">'+xa+'</span>');
    });
  });
</script>
 
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_permohonan_pendaftaran_badan_usaha').on('click', function(e) {
      e.preventDefault();
      var editor_isi_permohonan_pendaftaran_badan_usaha = CKEDITOR.instances.editor_isi_permohonan_pendaftaran_badan_usaha.getData();
      $('#isi_permohonan_pendaftaran_badan_usaha').val( editor_isi_permohonan_pendaftaran_badan_usaha );
      var parameter = [ 'judul_permohonan_pendaftaran_badan_usaha', 'isi_permohonan_pendaftaran_badan_usaha', 'nomor_permohonan_pendaftaran_badan_usaha', 'perihal_permohonan_pendaftaran_badan_usaha', 'yth_permohonan_pendaftaran_badan_usaha', 'cq_permohonan_pendaftaran_badan_usaha', 'di_permohonan_pendaftaran_badan_usaha', 'tanggal_permohonan_pendaftaran_badan_usaha', 'nama_pengusaha', 'tempat_lahir_pengusaha', 'tanggal_lahir_pengusaha', 'pekerjaan_pengusaha', 'nomor_telp_pengusaha', 'alamat_pengusaha', 'id_desa_pengusaha', 'id_kecamatan_pengusaha', 'id_kabupaten_pengusaha', 'id_propinsi_pengusaha', 'nama_perusahaan', 'id_jenis_usaha_jasa_perusahaan', 'alamat_perusahaan', 'id_desa_perusahaan', 'id_kecamatan_perusahaan', 'id_kabupaten_perusahaan', 'id_propinsi_perusahaan', 'nomor_telp_perusahaan', 'daerah_domisili_pemohon' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["judul_permohonan_pendaftaran_badan_usaha"] = $("#judul_permohonan_pendaftaran_badan_usaha").val();
      parameter["isi_permohonan_pendaftaran_badan_usaha"] = $("#isi_permohonan_pendaftaran_badan_usaha").val();
      parameter["nomor_permohonan_pendaftaran_badan_usaha"] = $("#nomor_permohonan_pendaftaran_badan_usaha").val();
      parameter["perihal_permohonan_pendaftaran_badan_usaha"] = $("#perihal_permohonan_pendaftaran_badan_usaha").val();
      parameter["yth_permohonan_pendaftaran_badan_usaha"] = $("#yth_permohonan_pendaftaran_badan_usaha").val();
      parameter["cq_permohonan_pendaftaran_badan_usaha"] = $("#cq_permohonan_pendaftaran_badan_usaha").val();
      parameter["di_permohonan_pendaftaran_badan_usaha"] = $("#di_permohonan_pendaftaran_badan_usaha").val();
      parameter["tanggal_permohonan_pendaftaran_badan_usaha"] = $("#tanggal_permohonan_pendaftaran_badan_usaha").val();
      parameter["nama_pengusaha"] = $("#nama_pengusaha").val();
      parameter["tempat_lahir_pengusaha"] = $("#tempat_lahir_pengusaha").val();
      parameter["tanggal_lahir_pengusaha"] = $("#tanggal_lahir_pengusaha").val();
      parameter["pekerjaan_pengusaha"] = $("#pekerjaan_pengusaha").val();
      parameter["nomor_telp_pengusaha"] = $("#nomor_telp_pengusaha").val();
      parameter["alamat_pengusaha"] = $("#alamat_pengusaha").val();
      parameter["id_desa_pengusaha"] = $("#id_desa_pengusaha").val();
      parameter["id_kecamatan_pengusaha"] = $("#id_kecamatan_pengusaha").val();
      parameter["id_kabupaten_pengusaha"] = $("#id_kabupaten_pengusaha").val();
      parameter["id_propinsi_pengusaha"] = $("#id_propinsi_pengusaha").val();
      parameter["nama_perusahaan"] = $("#nama_perusahaan").val();
      parameter["id_jenis_usaha_jasa_perusahaan"] = $("#id_jenis_usaha_jasa_perusahaan").val();
      parameter["alamat_perusahaan"] = $("#alamat_perusahaan").val();
      parameter["id_desa_perusahaan"] = $("#id_desa_perusahaan").val();
      parameter["id_kecamatan_perusahaan"] = $("#id_kecamatan_perusahaan").val();
      parameter["id_kabupaten_perusahaan"] = $("#id_kabupaten_perusahaan").val();
      parameter["id_propinsi_perusahaan"] = $("#id_propinsi_perusahaan").val();
      parameter["nomor_telp_perusahaan"] = $("#nomor_telp_perusahaan").val();
      parameter["daerah_domisili_pemohon"] = $("#daerah_domisili_pemohon").val();
      var url = '<?php echo base_url(); ?>permohonan_pendaftaran_badan_usaha/simpan_permohonan_pendaftaran_badan_usaha';
      
      var parameterRv = [ 'judul_permohonan_pendaftaran_badan_usaha', 'isi_permohonan_pendaftaran_badan_usaha', 'nomor_permohonan_pendaftaran_badan_usaha', 'perihal_permohonan_pendaftaran_badan_usaha', 'yth_permohonan_pendaftaran_badan_usaha', 'cq_permohonan_pendaftaran_badan_usaha', 'di_permohonan_pendaftaran_badan_usaha', 'tanggal_permohonan_pendaftaran_badan_usaha', 'nama_pengusaha', 'tempat_lahir_pengusaha', 'tanggal_lahir_pengusaha', 'pekerjaan_pengusaha', 'nomor_telp_pengusaha', 'alamat_pengusaha', 'id_desa_pengusaha', 'id_kecamatan_pengusaha', 'id_kabupaten_pengusaha', 'id_propinsi_pengusaha', 'nama_perusahaan', 'id_jenis_usaha_jasa_perusahaan', 'alamat_perusahaan', 'id_desa_perusahaan', 'id_kecamatan_perusahaan', 'id_kabupaten_perusahaan', 'id_propinsi_perusahaan', 'nomor_telp_perusahaan', 'daerah_domisili_pemohon' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedPermohonan_pendaftaran_badan_usaha();
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_permohonan_pendaftaran_badan_usaha').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_permohonan_pendaftaran_badan_usaha').hide();
    $('#update_permohonan_pendaftaran_badan_usaha').show();
    var id_permohonan_pendaftaran_badan_usaha = $(this).closest('tr').attr('id_permohonan_pendaftaran_badan_usaha');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_permohonan_pendaftaran_badan_usaha');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_permohonan_pendaftaran_badan_usaha').val(id_permohonan_pendaftaran_badan_usaha);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_permohonan_pendaftaran_badan_usaha:id_permohonan_pendaftaran_badan_usaha
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>permohonan_pendaftaran_badan_usaha/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#judul_permohonan_pendaftaran_badan_usaha').val(json[i].judul_permohonan_pendaftaran_badan_usaha);
            $('#icon').val(json[i].icon);
            $('#nomor_permohonan_pendaftaran_badan_usaha').val(json[i].nomor_permohonan_pendaftaran_badan_usaha);
            $('#perihal_permohonan_pendaftaran_badan_usaha').val(json[i].perihal_permohonan_pendaftaran_badan_usaha);
            $('#yth_permohonan_pendaftaran_badan_usaha').val(json[i].yth_permohonan_pendaftaran_badan_usaha);
            $('#cq_permohonan_pendaftaran_badan_usaha').val(json[i].cq_permohonan_pendaftaran_badan_usaha);
            $('#di_permohonan_pendaftaran_badan_usaha').val(json[i].di_permohonan_pendaftaran_badan_usaha);
            $('#tanggal_permohonan_pendaftaran_badan_usaha').val(json[i].tanggal_permohonan_pendaftaran_badan_usaha);
            $('#nama_pengusaha').val(json[i].nama_pengusaha);
            $('#tempat_lahir_pengusaha').val(json[i].tempat_lahir_pengusaha);
            $('#tanggal_lahir_pengusaha').val(json[i].tanggal_lahir_pengusaha);
            $('#pekerjaan_pengusaha').val(json[i].pekerjaan_pengusaha);
            $('#nomor_telp_pengusaha').val(json[i].nomor_telp_pengusaha);
            $('#alamat_pengusaha').val(json[i].alamat_pengusaha);
            //$('#id_desa_pengusaha').val(json[i].id_desa_pengusaha);
            //$('#id_kecamatan_pengusaha').val(json[i].id_kecamatan_pengusaha);
            //$('#id_kabupaten_pengusaha').val(json[i].id_kabupaten_pengusaha);
            //$('#id_propinsi_pengusaha').val(json[i].id_propinsi_pengusaha);
            $('#nama_perusahaan').val(json[i].nama_perusahaan);
            $('#id_jenis_usaha_jasa_perusahaan').val(json[i].id_jenis_usaha_jasa_perusahaan);
            $('#alamat_perusahaan').val(json[i].alamat_perusahaan);
            //$('#id_desa_perusahaan').val(json[i].id_desa_perusahaan);
            //$('#id_kecamatan_perusahaan').val(json[i].id_kecamatan_perusahaan);
            //$('#id_kabupaten_perusahaan').val(json[i].id_kabupaten_perusahaan);
            //$('#id_propinsi_perusahaan').val(json[i].id_propinsi_perusahaan);
            $('#nomor_telp_perusahaan').val(json[i].nomor_telp_perusahaan);
            $('#daerah_domisili_pemohon').val(json[i].daerah_domisili_pemohon);
            CKEDITOR.instances.editor_isi_permohonan_pendaftaran_badan_usaha.setData(json[i].isi_permohonan_pendaftaran_badan_usaha);
						load_wilayah_by_id_desa(json[i].id_desa_perusahaan);
						load_wilayah_by_id_desa_pengusaha(json[i].id_desa_pengusaha);
          }
        }
      });
    //AttachmentByMode(mode, value);
  });
});
</script>

<!-- edit desa-->
<script>
  function load_wilayah_by_id_desa(id_desa) {
    $('#id_desa_perusahaan').html('');
    $('#id_kecamatan_perusahaan').html('');
    $('#id_kabupaten_perusahaan').html('');
    $('#id_propinsi_perusahaan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_desa:id_desa
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_wilayah_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_desa_perusahaan').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan_perusahaan').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten_perusahaan').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
          $('#id_propinsi_perusahaan').html('<option value="' + json[i].id_propinsi + '">' + json[i].nama_propinsi + '</option>');
        }
      }
    });
  }
</script>
<script>
  function load_wilayah_by_id_desa_pengusaha(id_desa) {
    $('#id_desa_pengusaha').html('');
    $('#id_kecamatan_pengusaha').html('');
    $('#id_kabupaten_pengusaha').html('');
    $('#id_propinsi_pengusaha').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_desa:id_desa
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_wilayah_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_desa_pengusaha').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan_pengusaha').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten_pengusaha').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
          $('#id_propinsi_pengusaha').html('<option value="' + json[i].id_propinsi + '">' + json[i].nama_propinsi + '</option>');
        }
      }
    });
  }
</script>
<!-- end edit desa-->
<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_lahir_pengusaha, #tanggal_permohonan_pendaftaran_badan_usaha').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#update_permohonan_pendaftaran_badan_usaha').on('click', function(e) {
      e.preventDefault();
      var editor_isi_permohonan_pendaftaran_badan_usaha = CKEDITOR.instances.editor_isi_permohonan_pendaftaran_badan_usaha.getData();
      $('#isi_permohonan_pendaftaran_badan_usaha').val( editor_isi_permohonan_pendaftaran_badan_usaha );
      var parameter = [ 'id_permohonan_pendaftaran_badan_usaha', 'judul_permohonan_pendaftaran_badan_usaha', 'isi_permohonan_pendaftaran_badan_usaha', 'nomor_permohonan_pendaftaran_badan_usaha', 'perihal_permohonan_pendaftaran_badan_usaha', 'yth_permohonan_pendaftaran_badan_usaha', 'cq_permohonan_pendaftaran_badan_usaha', 'di_permohonan_pendaftaran_badan_usaha', 'tanggal_permohonan_pendaftaran_badan_usaha', 'nama_pengusaha', 'tempat_lahir_pengusaha', 'tanggal_lahir_pengusaha', 'pekerjaan_pengusaha', 'nomor_telp_pengusaha', 'alamat_pengusaha', 'id_desa_pengusaha', 'id_kecamatan_pengusaha', 'id_kabupaten_pengusaha', 'id_propinsi_pengusaha', 'nama_perusahaan', 'id_jenis_usaha_jasa_perusahaan', 'alamat_perusahaan', 'id_desa_perusahaan', 'id_kecamatan_perusahaan', 'id_kabupaten_perusahaan', 'id_propinsi_perusahaan', 'nomor_telp_perusahaan', 'daerah_domisili_pemohon' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_permohonan_pendaftaran_badan_usaha"] = $("#id_permohonan_pendaftaran_badan_usaha").val();
      parameter["judul_permohonan_pendaftaran_badan_usaha"] = $("#judul_permohonan_pendaftaran_badan_usaha").val();
      parameter["isi_permohonan_pendaftaran_badan_usaha"] = $("#isi_permohonan_pendaftaran_badan_usaha").val();
      parameter["nomor_permohonan_pendaftaran_badan_usaha"] = $("#nomor_permohonan_pendaftaran_badan_usaha").val();
      parameter["perihal_permohonan_pendaftaran_badan_usaha"] = $("#perihal_permohonan_pendaftaran_badan_usaha").val();
      parameter["yth_permohonan_pendaftaran_badan_usaha"] = $("#yth_permohonan_pendaftaran_badan_usaha").val();
      parameter["cq_permohonan_pendaftaran_badan_usaha"] = $("#cq_permohonan_pendaftaran_badan_usaha").val();
      parameter["di_permohonan_pendaftaran_badan_usaha"] = $("#di_permohonan_pendaftaran_badan_usaha").val();
      parameter["tanggal_permohonan_pendaftaran_badan_usaha"] = $("#tanggal_permohonan_pendaftaran_badan_usaha").val();
      parameter["nama_pengusaha"] = $("#nama_pengusaha").val();
      parameter["tempat_lahir_pengusaha"] = $("#tempat_lahir_pengusaha").val();
      parameter["tanggal_lahir_pengusaha"] = $("#tanggal_lahir_pengusaha").val();
      parameter["pekerjaan_pengusaha"] = $("#pekerjaan_pengusaha").val();
      parameter["nomor_telp_pengusaha"] = $("#nomor_telp_pengusaha").val();
      parameter["alamat_pengusaha"] = $("#alamat_pengusaha").val();
      parameter["id_desa_pengusaha"] = $("#id_desa_pengusaha").val();
      parameter["id_kecamatan_pengusaha"] = $("#id_kecamatan_pengusaha").val();
      parameter["id_kabupaten_pengusaha"] = $("#id_kabupaten_pengusaha").val();
      parameter["id_propinsi_pengusaha"] = $("#id_propinsi_pengusaha").val();
      parameter["nama_perusahaan"] = $("#nama_perusahaan").val();
      parameter["id_jenis_usaha_jasa_perusahaan"] = $("#id_jenis_usaha_jasa_perusahaan").val();
      parameter["alamat_perusahaan"] = $("#alamat_perusahaan").val();
      parameter["id_desa_perusahaan"] = $("#id_desa_perusahaan").val();
      parameter["id_kecamatan_perusahaan"] = $("#id_kecamatan_perusahaan").val();
      parameter["id_kabupaten_perusahaan"] = $("#id_kabupaten_perusahaan").val();
      parameter["id_propinsi_perusahaan"] = $("#id_propinsi_perusahaan").val();
      parameter["nomor_telp_perusahaan"] = $("#nomor_telp_perusahaan").val();
      parameter["daerah_domisili_pemohon"] = $("#daerah_domisili_pemohon").val();
      var url = '<?php echo base_url(); ?>permohonan_pendaftaran_badan_usaha/update_permohonan_pendaftaran_badan_usaha';
      
      var parameterRv = [ 'id_permohonan_pendaftaran_badan_usaha', 'judul_permohonan_pendaftaran_badan_usaha', 'isi_permohonan_pendaftaran_badan_usaha', 'nomor_permohonan_pendaftaran_badan_usaha', 'perihal_permohonan_pendaftaran_badan_usaha', 'yth_permohonan_pendaftaran_badan_usaha', 'cq_permohonan_pendaftaran_badan_usaha', 'di_permohonan_pendaftaran_badan_usaha', 'tanggal_permohonan_pendaftaran_badan_usaha', 'nama_pengusaha', 'tempat_lahir_pengusaha', 'tanggal_lahir_pengusaha', 'pekerjaan_pengusaha', 'nomor_telp_pengusaha', 'alamat_pengusaha', 'id_desa_pengusaha', 'id_kecamatan_pengusaha', 'id_kabupaten_pengusaha', 'id_propinsi_pengusaha', 'nama_perusahaan', 'id_jenis_usaha_jasa_perusahaan', 'alamat_perusahaan', 'id_desa_perusahaan', 'id_kecamatan_perusahaan', 'id_kabupaten_perusahaan', 'id_propinsi_perusahaan', 'nomor_telp_perusahaan', 'daerah_domisili_pemohon' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<!----------------------->
<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('editor_isi_permohonan_pendaftaran_badan_usaha');
	$(".textarea").wysihtml5();
});
</script>
<!-- attachment -->
<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_permohonan_pendaftaran_badan_usaha').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'permohonan_pendaftaran_badan_usaha',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_permohonan_pendaftaran_badan_usaha').append(tr);
			}
		});
	}
</script>
<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_permohonan_pendaftaran_badan_usaha').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_permohonan_pendaftaran_badan_usaha').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_permohonan_pendaftaran_badan_usaha').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
<!-- end attachment -->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_permohonan_pendaftaran_badan_usaha').on('click', '#del_ajax', function() {
    var id_permohonan_pendaftaran_badan_usaha = $(this).closest('tr').attr('id_permohonan_pendaftaran_badan_usaha');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_pendaftaran_badan_usaha"] = id_permohonan_pendaftaran_badan_usaha;
        var url = '<?php echo base_url(); ?>permohonan_pendaftaran_badan_usaha/hapus/';
        HapusData(parameter, url);
        $('[id_permohonan_pendaftaran_badan_usaha='+id_permohonan_pendaftaran_badan_usaha+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_permohonan_pendaftaran_badan_usaha').on('click', '#inaktifkan', function() {
    var id_permohonan_pendaftaran_badan_usaha = $(this).closest('tr').attr('id_permohonan_pendaftaran_badan_usaha');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_pendaftaran_badan_usaha"] = id_permohonan_pendaftaran_badan_usaha;
        var url = '<?php echo base_url(); ?>permohonan_pendaftaran_badan_usaha/inaktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_permohonan_pendaftaran_badan_usaha').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
        $('[id_permohonan_pendaftaran_badan_usaha='+id_permohonan_pendaftaran_badan_usaha+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_permohonan_pendaftaran_badan_usaha').on('click', '#aktifkan', function() {
    var id_permohonan_pendaftaran_badan_usaha = $(this).closest('tr').attr('id_permohonan_pendaftaran_badan_usaha');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_pendaftaran_badan_usaha"] = id_permohonan_pendaftaran_badan_usaha;
        var url = '<?php echo base_url(); ?>permohonan_pendaftaran_badan_usaha/aktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_permohonan_pendaftaran_badan_usaha').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
				var tabel = $('#tabel').val();
				load_data(tabel);
        $('[id_permohonan_pendaftaran_badan_usaha='+id_permohonan_pendaftaran_badan_usaha+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>