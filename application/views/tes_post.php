<?php
$web=$this->uut->namadomain(base_url());
?>

<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
        <!-- Meta -->
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
        <meta name="author" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>,">
        <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>">
        <meta name="og:description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
        <meta name="og:url" content="<?php echo base_url(); ?>"/>
        <meta name="og:title" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>"/>
        <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
        <meta name="og:keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
        <!-- Favicon -->
        <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />
        
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,300italic,400,400italic,500,700,700italic,900" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Noto+Serif:400,400italic,700,700italic" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:900" rel="stylesheet" type="text/css">

        <!-- Icon Font -->
  <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <!-- Bootstrap CSS -->
  <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Theme CSS -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/portfolio/css/styles.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/portfolio/css/biru.css">
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
				<!-- Alert Confirmation -->
				<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
				<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
				<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
				<link rel="stylesheet" href="<?php echo base_url(); ?>css/Typeahead-BS3-css.css" id="toggleCSS" />
				<link rel="stylesheet" href="<?php echo base_url(); ?>boots/dist/css/bootstrap-timepicker.min.css" />
    </head>
    <body>
			<div id="main" class="header-style1">

            <!-- Begin Main Wrapper -->
            <div class="container main-wrapper">

                <!-- End Main Banner -->
                <!-- End Main Banner -->
                <div class="main-header mag-header clearfix">
										<header class="header-wrapper clearfix">

											<div class="header" style="background:#fff;">
													<div class="container">
															<div class="row">
																	<!-- BEGIN TOP BAR LEFT PART -->
																	<div class="col-md-6 col-sm-6 additional-shop-info">
																			<ul class="list-unstyled list-inline">
																					<li><i class="fa fa-phone"></i><span><?php if(!empty( $telpon )){ echo $telpon; } ?></span></li>
																					<li><i class="fa fa-envelope-o"></i><span><?php if(!empty( $email )){ echo $email; } ?></span></li>
																			</ul>
																	</div>
																	<!-- END TOP BAR LEFT PART -->
																	<!-- BEGIN TOP BAR MENU -->
																	<div class="col-md-6 col-sm-6 additional-nav">
																			<ul class="list-unstyled list-inline pull-right">
																					<li><a href="<?php echo base_url('login'); ?>">Log In</a></li>
																					<li><a href="https://diskominfo.wonosobokab.go.id/postings/detail/1892/FAQ.HTML">FAQ</a></li>
																			</ul>
																	</div>
																	<!-- END TOP BAR MENU -->
															</div>
													</div><!-- .container -->
											</div><!-- .header -->
											<div class="header" id="header">
													<div class="container">
															<!-- Mobile Menu Button -->
															<a class="navbar-toggle collapsed" id="nav-button" href="#mobile-nav">
															<span class="icon-bar"></span>
															<span class="icon-bar"></span>
															<span class="icon-bar"></span>
															<span class="icon-bar"></span>
															</a><!-- .navbar-toggle -->
															<!-- Main Nav Wrapper -->
															<nav class="navbar mega-menu">
																<!-- .logo -->
																<!-- Navigation Menu -->
																<!-- .navbar-collapse -->              
																<!-- End Navigation Menu -->
																<div class="navbar-collapse collapse" id="hornav">
																		<?php // echo ''.$menu_atas_disparbud.''; ?>
																	<!-- .nav .navbar-nav -->
																</div>
															</nav>
																		<!-- .navbar --> 
													</div><!-- .container -->
											</div><!-- .header -->

										</header><!-- .header-wrapper -->
										
								</div><!-- .header -->
								<div class="main-content mag-content clearfix">

                    <div class="row">
											<div class="row">
												<div class="row featured-wrapper" style="padding-top:5px;">
													<div class="col-md-12">
														<?php $this -> load -> view('tes/slide1.php');  ?>
										
													</div>
														
												</div><!-- .main-content -->
											</div><!-- .main-content -->
										</div><!-- .main-content -->
										
											<!-- End last column -->      
										</div>
										<!-- .main-body -->

								</div><!-- .header -->
                <!-- End Main Banner -->
            </div><!-- .main-wrapper -->

            <!-- Footer -->
            <footer class="footer source-org vcard copyright clearfix" id="footer" role="contentinfo">
                <div class="footer-main">
                    <div class="fixed-main">
                        <div class="container">
                            <div class="mag-content">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="footer-block clearfix">
                                            <p class="clearfix">
                                                <a class="logo" href="index.php" title="" rel="home">
                                                     <span><?php if(!empty( $keterangan )){ echo $keterangan; } ?></span>
                                                </a><!-- .logo -->
                                            </p>
                                            <p class="description">
																							<address class="margin-bottom-40">
																								<?php if(!empty( $alamat )){ echo $alamat; } ?><br>
																								Phone: <?php if(!empty( $telpon )){ echo $telpon; } ?><br>
																								Email: <a href="mailto:<?php if(!empty( $email )){ echo $email; } ?>"><?php if(!empty( $email )){ echo $email; } ?></a><br>
																								Website: <a href="https://<?php echo $web; ?>">https://<?php echo $web; ?></a>
																							</address>
                                            </p>
                                            <ul class="social-list clearfix">
                                                <li class="social-facebook">
                                                    <a href="<?php if(!empty( $facebook )){ echo $facebook; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Facebook">
                                                        <i class="fa fa-facebook"></i>
                                                    </a>
                                                </li>
                                                <li class="social-twitter" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Twitter">
                                                    <a href="<?php if(!empty( $twitter )){ echo $twitter; } ?>">
                                                        <i class="fa fa-twitter"></i>
                                                    </a>
                                                </li>
                                                <li class="social-gplus">
                                                    <a href="<?php if(!empty( $google )){ echo $google; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Google+">
                                                        <i class="fa fa-google-plus"></i>
                                                    </a>
                                                </li>
                                                <li class="social-youtube">
                                                    <a href="<?php if(!empty( $youtube )){ echo $youtube; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Youtube">
                                                        <i class="fa fa-youtube"></i>
                                                    </a>
                                                </li>
                                                <li class="social-instagram">
                                                    <a href="<?php if(!empty( $instagram )){ echo $instagram; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Instagram">
                                                        <i class="fa fa-instagram"></i>
                                                    </a>
                                                </li>
                                                <li class="social-pinterest">
                                                    <a href="<?php if(!empty( $pinterest )){ echo $pinterest; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Pinterest">
                                                        <i class="fa fa-pinterest"></i>
                                                    </a>
                                                </li>
                                                <li class="social-rss">
                                                    <a href="<?php if(!empty( $rss )){ echo $rss; } ?>" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="RSS">
                                                        <i class="fa fa-rss"></i>
                                                    </a>
                                                </li>
                                            </ul>
											<!-- BEGIN: Powered by Supercounters.com -->
</center>
<!-- END: Powered by Supercounters.com -->

                                        </div><!-- Footer Block -->
                                    </div>
                                    
                                    <div class="col-md-5">
    <div class="footer-block clearfix">
        <!--<h3 class="footer-title"><a >Other Link</a></h3>
        <ul class="tags-widget"><li><a href="#"><img src="xxi/iklan/13_Logo_Branding_Jateng_Gayeng.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/22_Logo_Visit_Jawa_Tengah_cpy.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/34_Pesona_Indonesia.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/45_Wonderful_Indonesia.png" width="55"></a></li><li><a href="1"><img src="xxi/iklan/56_BPPD_Jateng.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/67_logo_asita.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/78_PHRI.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/89_Logo_AWAI.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/910_Logo_ASPPI.png" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1012_HPI.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1111_IMG-20161126-WA0004.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1213_PeBeMas.jpg" width="55"></a></li><li><a href="#"><img src="xxi/iklan/1314_FK_Deswita.jpg" width="55"></a></li><li><a href="1"><img src="xxi/iklan/" width="55"></a></li></ul>-->
				<?php if(!empty( $KolomKiriBawah )){ echo $KolomKiriBawah; } ?>
    </div>
</div>
                                    <div class="col-md-3">
                                        <div class="footer-block clearfix">
                                            <?php if(!empty( $KolomPalingBawah )){ echo $KolomPalingBawah; } ?>
                                        </div><!-- Footer Block -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="footer-bottom clearfix">
                    <div class="fixed-main">
                        <div class="container">
                            <div class="mag-content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>Copyright <a href="https://<?php echo $web; ?>"> <?php if(!empty( $keterangan )){ echo $keterangan; } ?></a> © 2016. All Rights Reserved</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- End Footer -->

        </div><!-- End Main -->

        <!-- Mobile Menu -->
        <nav id="mobile-nav">
    <div>
        <ul><li><a href="index.php?c4ca4238a0b923820dcc509a6f75849b-1-BERANDA" role="button" aria-expanded="false">BERANDA</a></li><li><a href="news.php?c81e728d9d4c2f636f067f89cc14862c-2-BERITA" role="button" aria-expanded="false">BERITA</a></li><li><a href="#">KABUPATEN/KOTA</a>
                    <ul><div class="mega-menu-5block"><li><a href="kabupaten.php?eccbc87e4b5ce2fe28308fd9f2a7baf3-3-Banjarnegara">Banjarnegara</a></li><li><a href="kabupaten.php?37693cfc748049e45d87b8c7d8b9aacd-23-Banyumas ">Banyumas </a></li><li><a href="kabupaten.php?a87ff679a2f3e71d9181a67b7542122c-4-Batang ">Batang </a></li><li><a href="kabupaten.php?c81e728d9d4c2f636f067f89cc14862c-2-Blora ">Blora </a></li><li><a href="kabupaten.php?e4da3b7fbbce2345d7772b0674a318d5-5-Boyolali ">Boyolali </a></li><li><a href="kabupaten.php?c4ca4238a0b923820dcc509a6f75849b-1-Brebes ">Brebes </a></li><li><a href="kabupaten.php?1679091c5a880faf6fb5e6087eb1b2dc-6-Cilacap ">Cilacap </a></li><li><a href="kabupaten.php?8f14e45fceea167a5a36dedd4bea2543-7-Demak ">Demak </a></li><li><a href="kabupaten.php?3c59dc048e8850243be8079a5c74d079-21-Grobogan ">Grobogan </a></li><li><a href="kabupaten.php?c9f0f895fb98ab9159f51fd0297e236d-8-Jepara ">Jepara </a></li><li><a href="kabupaten.php?9bf31c7ff062936a96d3c8bd1f8f2ff3-15-Kab. Magelang ">Kab. Magelang </a></li><li><a href="kabupaten.php?c20ad4d76fe97759aa27a0c99bff6710-12-Kab. Pekalongan ">Kab. Pekalongan </a></li><li><a href="kabupaten.php?182be0c5cdcd5072bb1864cdee4d3d6e-33-Kab. Semarang ">Kab. Semarang </a></li><li><a href="kabupaten.php?c16a5320fa475530d9583c34fd356ef5-31-kab. Tegal ">kab. Tegal </a></li><li><a href="kabupaten.php?aab3238922bcc25a6f606eb525ffdc56-14-Karanganyar ">Karanganyar </a></li><li><a href="kabupaten.php?45c48cce2e2d7fbdea1afc51c7c6ad26-9-Kebumen ">Kebumen </a></li><li><a href="kabupaten.php?d3d9446802a44259755d38e6d163e820-10-Kendal ">Kendal </a></li><li><a href="kabupaten.php?c51ce410c124a10e0db5e4b97fc2af39-13-Klaten ">Klaten </a></li><li><a href="kabupaten.php?c74d97b01eae257e44aa9d5bade97baf-16-Kota Magelang ">Kota Magelang </a></li><li><a href="kabupaten.php?6f4922f45568161a8cdf4ad2299f6d23-18-Kota Pekalongan ">Kota Pekalongan </a></li><li><a href="kabupaten.php?34173cb38f07f89ddbebc2ac9128303f-30-Kota Semarang ">Kota Semarang </a></li><li><a href="kabupaten.php?6ea9ab1baa0efb9e19094440c317e21b-29-Kota Tegal ">Kota Tegal </a></li><li><a href="kabupaten.php?6512bd43d9caa6e02c990b0a82652dca-11-Kudus ">Kudus </a></li><li><a href="kabupaten.php?98f13708210194c475687be6106a3b84-20-Pati ">Pati </a></li><li><a href="kabupaten.php?1f0e3dad99908345f7439f8ffabdffc4-19-Pemalang ">Pemalang </a></li><li><a href="kabupaten.php?70efdf2ec9b086079795c442636b55fb-17-Purbalingga ">Purbalingga </a></li><li><a href="kabupaten.php?b6d767d2f8ed5d21a44b0e5886680cb9-22-Purworejo ">Purworejo </a></li><li><a href="kabupaten.php?1ff1de774005f8da13f42943881c655f-24-Rembang ">Rembang </a></li><li><a href="kabupaten.php?33e75ff09dd601bbe69f351039152189-28-Salatiga ">Salatiga </a></li><li><a href="kabupaten.php?8e296a067a37563370ded05f5a3bf3ec-25-Sragen ">Sragen </a></li><li><a href="kabupaten.php?4e732ced3463d06de0ca9a15b6153677-26-Sukoharjo ">Sukoharjo </a></li><li><a href="kabupaten.php?02e74f10e0327ad868d138f2b4fdd6f0-27-Surakarta ">Surakarta </a></li><li><a href="kabupaten.php?6364d3f0f495b6ab9dcf8d3b5c6e0b01-32-Temanggung ">Temanggung </a></li><li><a href="kabupaten.php?e369853df766fa44e1ed0ff613f563bd-34-Wonogiri ">Wonogiri </a></li><li><a href="kabupaten.php?1c383cd30b7c298ab50293adfecb7b18-35-Wonosobo ">Wonosobo </a></li></ul></li><li>
                <a href="?d3d9446802a44259755d38e6d163e820-10-INFORMASI">INFORMASI</a>
                <ul><li><a href="video.php?6c8349cc7260ae62e3b1396831a8398f-45-Video"> Video</a></li><li><a href="news.php?a1d0c6e83f027327d8461063f4ac58a6-42-TIC"> TIC</a></li> </ul></li><li><a href="https://disporapar.jatengprov.go.id?eccbc87e4b5ce2fe28308fd9f2a7baf3-3-PPID" role="button" aria-expanded="false">PPID</a></li><li><a href="news.php?f457c545a9ded88f18ecee47145a72c0-49-SAIL KARIMUNJAWA" role="button" aria-expanded="false">SAIL KARIMUNJAWA</a></li><li><a href="news.php?642e92efb79421734881b53e1e1b18b6-48-BIF" role="button" aria-expanded="false">BIF</a></li><li><a href="foto?c0c7c76d30bd3dcaefc96f40275bdc0a-50-LOMBA FOTO" role="button" aria-expanded="false">LOMBA FOTO</a></li><li><a href="https://202.56.165.86/usahawisata/map.php?2838023a778dfaecdc212708f721b788-51-USAHA WISATA" role="button" aria-expanded="false">USAHA WISATA</a></li></ul>
    </div>
</nav>
        <!-- / Mobile Menu -->
        <div id="go-top-button" class="fa fa-angle-up" title="Scroll To Top"></div>
        <div class="mobile-overlay" id="mobile-overlay"></div>

            <script>
              function LoadVisitor() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    table:'visitor'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/simpan_visitor/',
                  success: function(html) {
                    $('#visitor').html('Total Pengunjung '+html+' ');
                  }
                });
              }
            </script>
            <script>
              function LoadHitCounter() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    current_url:'<?php echo current_url(); ?>'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/hit_counter/',
                  success: function(html) {
                    $('#hit_counter').html(''+html+' Kali ');
                    $('#hit_counter_posting').html(''+html+'');
                  }
                });
              }
            </script>
            <script type="text/javascript">
            $(document).ready(function() {
              LoadVisitor();
              LoadHitCounter();
            });
            </script>
        <!-- Jquery js -->
    <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/jquery.min.js" type="text/javascript"></script>

        <!-- Modernizr -->
<script src="<?php echo base_url(); ?>assets/portfolio/js/modernizr.min.js"></script>


        <!-- Bootstrap js -->
    <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <!-- Plugins js -->
        <script src="<?php echo base_url(); ?>assets/portfolio/js/plugins.js"></script>

        <!-- Theme js -->
        <script src="<?php echo base_url(); ?>assets/portfolio/js/script.js"></script>

		<script src="<?php echo base_url(); ?>js/uut.js"></script>
						<!-- alertify -->
						<script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
    </body>
</html>
