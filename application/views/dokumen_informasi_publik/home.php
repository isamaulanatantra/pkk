
<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab">GENERAL</a></li>
      <li><a href="#tab_2" data-toggle="tab">PENCARIAN</a></li>
			<li><a href="#tab_3" data-toggle="tab">AUTO SUGGEST</a></li>
			<li><a href="#tab_4" data-toggle="tab">DOWNLOAD</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="row" id="div_form_input">
          <div class="col-md-6">
              <div class="panel box box-primary box-solid">
                <div class="box-header with-border">
                  <h4 class="box-title">
                      <i class="ion ion-clipboard"></i> FORMULIR INPUT
                  </h4>
                </div>
                  <div class="box-body">
                  <!--formulir input-->
                    <form role="form" id="form_dokumen_informasi_publik" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=dokumen_informasi_publik" enctype="multipart/form-data">
                      <div class="box-body">
                      
                        <input id="id_dokumen_informasi_publik" name="id_dokumen_informasi_publik" type="hidden">
                        <div class="form-group">
                          <label for="kode_dokumen_informasi_publik">Kode Dokumen Informasi Pubik</label>
                          <input class="form-control" id="kode_dokumen_informasi_publik" name="kode_dokumen_informasi_publik" value="" placeholder="Kode Dokumen Informasi Pubik" type="text">
                        </div>
                        <div class="form-group">
                          <label for="nama_dokumen_informasi_publik">Nama Dokumen Informasi Pubik</label>
                          <input class="form-control" id="nama_dokumen_informasi_publik" name="nama_dokumen_informasi_publik" value="" placeholder="Nama Dokumen Informasi Pubik" type="text">
                        </div>
                        <input id="inserted_by" name="inserted_by" type="hidden">
                        <input id="inserted_time" name="inserted_time" type="hidden">
                        <input id="updated_by" name="updated_by" type="hidden">
                        <input id="updated_time" name="updated_time" type="hidden">
                        <input id="deleted_by" name="deleted_by" type="hidden">
                        <input id="deleted_time" name="deleted_time" type="hidden">
                        <input id="temp" name="temp" type="hidden">
                        <div class="form-group">
                          <label for="keterangan">Keterangan</label>
                          <input class="form-control" id="keterangan" name="keterangan" value="" placeholder="Keterangan" type="text">
                        </div>
                        <input id="status" name="status" type="hidden">
                                            
                        <div class="alert alert-info alert-dismissable">
                          <div class="form-group">
                            <label for="remake">Keterangan Lampiran Dokumen_informasi_publik</label>
                            <input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran Dokumen_informasi_publik" type="text">
                          </div>
                          <div class="form-group">
                            <label for="myfile">File Lampiran Dokumen_informasi_publik</label>
                            <input type="file" size="60" name="myfile" id="dokumen_informasi_publik_baru" >
                          </div>
                          <div id="progress_upload_lampiran_dokumen_informasi_publik">
                            <div id="bar_progress_upload_lampiran_dokumen_informasi_publik"></div>
                            <div id="percent_progress_upload_lampiran_dokumen_informasi_publik">0%</div >
                          </div>
                          <div id="message_progress_upload_lampiran_dokumen_informasi_publik"></div>
                        </div>
                        <div class="alert alert-info alert-dismissable">
                          <h3 class="box-title">Data Lampiran </h3>
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                            </tr>
                            <tbody id="tbl_lampiran_dokumen_informasi_publik">
                            </tbody>
                          </table>
                        </div>
                      </div>
                      <div class="box-footer">
                        <button type="submit" class="btn btn-primary" id="simpan_dokumen_informasi_publik">SIMPAN</button>
                      </div>
                    </form>
                    <div class="overlay" id="overlay_form_input" style="display:none;">
                      <i class="fa fa-refresh fa-spin"></i>
                    </div>
                  </div>
              </div>
          </div>
        </div>
        <div class="row" id="e_div_form_input" style="display:none;">
					<div class="col-md-6">
            <div class="box box-primary box-solid">
              <div class="box-header">
                <h3 class="box-title"><i class="ion ion-clipboard"></i> FORMULIR EDIT</h3>
              </div>
              <div class="box-body">
                <form role="form" id="e_form_dokumen_informasi_publik" method="post" action="<?php echo base_url(); ?>attachment/e_upload/?table_name=dokumen_informasi_publik" enctype="multipart/form-data">
                  <div class="box-body">
                    
										<input class="form-control" id="e_id_dokumen_informasi_publik" name="id" value="" type="hidden">
										<div class="form-group">
											<label for="e_kode_dokumen_informasi_publik">Kode Dokumen Informasi Pubik</label>
											<input class="form-control" id="e_kode_dokumen_informasi_publik" name="e_kode_dokumen_informasi_publik" value="" placeholder="Kode Dokumen Informasi Pubik" type="text">
										</div>
										<div class="form-group">
											<label for="e_nama_dokumen_informasi_publik">Nama Dokumen Informasi Pubik</label>
											<input class="form-control" id="e_nama_dokumen_informasi_publik" name="e_nama_dokumen_informasi_publik" value="" placeholder="Nama Dokumen Informasi Pubik" type="text">
										</div>
										<input class="form-control" id="e_inserted_by" name="e_inserted_by" value="" type="hidden">
										<input class="form-control" id="e_inserted_time" name="e_inserted_time" value="" type="hidden">
										<input class="form-control" id="e_updated_by" name="e_updated_by" value="" type="hidden">
										<input class="form-control" id="e_updated_time" name="e_updated_time" value="" type="hidden">
										<input class="form-control" id="e_deleted_by" name="e_deleted_by" value="" type="hidden">
										<input class="form-control" id="e_deleted_time" name="e_deleted_time" value="" type="hidden">
										<input class="form-control" id="e_temp" name="e_temp" value="" type="hidden">
										<div class="form-group">
											<label for="e_keterangan">Keterangan</label>
                        <input class="form-control" id="e_keterangan" name="e_keterangan" value="" placeholder="Keterangan" type="text">
										</div>
										<input class="form-control" id="e_status" name="e_status" value="" type="hidden">
																				
										<div class="alert alert-info alert-dismissable">
											<div class="form-group">
												<label for="remake">Keterangan Lampiran Dokumen_informasi_publik</label>
												<input class="form-control" id="e_remake" name="remake" placeholder="Keterangan Lampiran Dokumen_informasi_publik" type="text">
											</div>
											<div class="form-group">
												<label for="myfile">File Lampiran Dokumen_informasi_publik</label>
												<input type="file" size="60" name="myfile" id="e_dokumen_informasi_publik_baru" >
											</div>
											<div id="e_progress_upload_lampiran_dokumen_informasi_publik">
												<div id="e_bar_progress_upload_lampiran_dokumen_informasi_publik"></div>
												<div id="e_percent_progress_upload_lampiran_dokumen_informasi_publik">0%</div >
											</div>
											<div id="e_message_progress_upload_lampiran_dokumen_informasi_publik"></div>
										</div>
										<div class="alert alert-info alert-dismissable">
											<h3 class="box-title">Data Lampiran </h3>
											<table class="table table-bordered">
												<tr>
													<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
												</tr>
												<tbody id="e_tbl_lampiran_dokumen_informasi_publik">
												</tbody>
											</table>
										</div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="update_data_dokumen_informasi_publik">SIMPAN</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="e_overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
				<div class="row" id="div_data_default">
          <div class="col-md-12">
            <div class="box box-primary box-solid">
              <div class="box-header">
                <h3 class="box-title"><!--<a class="label label-info" id="klik_tab_input"><i class="fa fa-plus"></i> TAMBAH DATA</a>-->DATA</h3>
              </div>
              <div class="box-body">
                <table class="table table-bordered">
									<tr>
										<th>NO</th>
										<th>Kode Dokumen Informasi Pubik</th>
										<th>Nama Dokumen Informasi Pubik</th>
										<th>Keterangan</th>
										<th>PROSES</th> 
									</tr>
									<tbody id="tbl_utama_dokumen_informasi_publik">
									</tbody>
								</table>
								<div class="box-footer clearfix">
									<ul class="pagination pagination-sm no-margin pull-right" id="pagination">
									<?php
									for ($x = 1; $x <= $total; $x++) {
										echo '<li page="'.$x.'" id="'.$x.'"><a class="update_id" href="#">'.$x.'</a></li>';
									}
									?>
									</ul>
								</div>
              </div>
              <div class="overlay" id="overlay_data_default" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab_2">
        <div class="row" id="cari_div_form_input">
          <div class="col-md-6">
            <div class="box box-primary box-solid">
              <div class="box-header">
                <h3 class="box-title"><i class="fa fa-search"></i> FORMULIR CARI</h3>
              </div>
              <div class="box-body">
                <form role="form" id="cari_form_dokumen_informasi_publik" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=dokumen_informasi_publik" enctype="multipart/form-data">
                  <div class="box-body">
                    <input class="form-control" id="cari_temp" name="temp" value="" type="hidden">
                    <div class="form-group">
                      <label for="key_word">Kata Kunci</label>
                      <input class="form-control" id="key_word" placeholder="Kata Kunci" type="text">
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="cari_dokumen_informasi_publik">Tampil</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="cari_overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
						<div class="col-md-12" id="div_data_cari">
							<div class="box box-primary box-solid">
								<div class="box-header">
									<h3 class="box-title"><i class="fa fa-th"></i> DATA PENCARIAN</h3>
								</div>
								<div class="box-body">
									<table class="table table-bordered">
										<tr>
											<th>NO</th>
											<th>Kode Dokumen Informasi Pubik</th>
											<th>Nama Dokumen Informasi Pubik</th>
											<th>Keterangan</th>
											<th>PROSES</th> 
										</tr>
										<tbody id="tbl_search_dokumen_informasi_publik">
										</tbody>
									</table>
									<div class="box-footer clearfix">
										<div id="total_data_search"></div>
										<ul class="pagination pagination-sm no-margin pull-right" id="next_page_search">
										</ul>
									</div>
								</div>
								<div class="overlay" id="overlay_data_cari" style="display:none;">
									<i class="fa fa-refresh fa-spin"></i>
								</div>
							</div>
						</div>
        </div>
      </div>
			<div class="tab-pane" id="tab_3">
				<div class="row" id="a_div_form_input">
					<div class="col-md-6">
            <div class="box box-primary box-solid">
              <div class="box-header">
                <h3 class="box-title"><i class="ion ion-clipboard"></i> FORMULIR AUTO SUGGEST</h3>
              </div>
              <div class="box-body">
                <form role="form" id="a_form_dokumen_informasi_publik" method="post" action="<?php echo base_url(); ?>attachment/e_upload/?table_name=dokumen_informasi_publik" enctype="multipart/form-data">
                  <div class="box-body">
                    
										<input class="form-control" id="a_id_dokumen_informasi_publik" name="id" value="" type="hidden">
										<div class="form-group">
											<label for="a_kode_dokumen_informasi_publik">Kode Dokumen Informasi Pubik</label>
											<input class="form-control" id="a_kode_dokumen_informasi_publik" name="a_kode_dokumen_informasi_publik" value="" placeholder="Kode Dokumen Informasi Pubik" type="text">
										</div>
										<div class="form-group">
											<label for="a_nama_dokumen_informasi_publik">Nama Dokumen Informasi Pubik</label>
											<input class="form-control" id="a_nama_dokumen_informasi_publik" name="a_nama_dokumen_informasi_publik" value="" placeholder="Nama Dokumen Informasi Pubik" type="text">
										</div>
										<input class="form-control" id="a_inserted_by" name="a_inserted_by" value="" type="hidden">
										<input class="form-control" id="a_inserted_time" name="a_inserted_time" value="" type="hidden">
										<input class="form-control" id="a_updated_by" name="a_updated_by" value="" type="hidden">
										<input class="form-control" id="a_updated_time" name="a_updated_time" value="" type="hidden">
										<input class="form-control" id="a_deleted_by" name="a_deleted_by" value="" type="hidden">
										<input class="form-control" id="a_deleted_time" name="a_deleted_time" value="" type="hidden">
										<input class="form-control" id="a_temp" name="a_temp" value="" type="hidden">
										<div class="form-group">
											<label for="a_keterangan">Keterangan</label>
                      <input class="form-control" id="a_keterangan" name="a_keterangan" value="" placeholder="Keterangan" type="text">
										</div>
										<input class="form-control" id="a_status" name="a_status" value="" type="hidden">
																				
										<div class="alert alert-info alert-dismissable">
											<div class="form-group">
												<label for="a_remake">Keterangan Lampiran Dokumen_informasi_publik</label>
												<input class="form-control" id="a_remake" name="remake" placeholder="Keterangan Lampiran Dokumen_informasi_publik" type="text">
											</div>
											<div class="form-group">
												<label for="a_myfile">File Lampiran Dokumen_informasi_publik</label>
												<input type="file" size="60" name="myfile" id="a_dokumen_informasi_publik_baru" >
											</div>
											<div id="a_progress_upload_lampiran_dokumen_informasi_publik">
												<div id="a_bar_progress_upload_lampiran_dokumen_informasi_publik"></div>
												<div id="a_percent_progress_upload_lampiran_dokumen_informasi_publik">0%</div >
											</div>
											<div id="a_message_progress_upload_lampiran_dokumen_informasi_publik"></div>
										</div>
										<div class="alert alert-info alert-dismissable">
											<h3 class="box-title">Data Lampiran </h3>
											<table class="table table-bordered">
												<tr>
													<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
												</tr>
												<tbody id="a_tbl_lampiran_dokumen_informasi_publik">
												</tbody>
											</table>
										</div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="a_update_data_dokumen_informasi_publik">SIMPAN</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="a_overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
			</div>
			<div class="tab-pane" id="tab_4">
				<div class="row" id="download_div_form_input">
					<div class="col-md-6">
            <div class="box box-primary box-solid">
              <div class="box-header">
                <h3 class="box-title">Download</h3>
              </div>
              <div class="box-body">
                <a target="_blank" href="<?php echo base_url(); ?>dokumen_informasi_publik/download_xls">Download Xlsx</a><br />
                <a target="_blank" href="<?php echo base_url(); ?>dokumen_informasi_publik/cetak_pdf">Download Pdf</a>
              </div>
            </div>
          </div>
        </div>
			</div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_input').on('click', function(e) {
	$('#e_div_form_input').fadeOut('slow');
	$('#div_form_input').fadeIn('slow');
	//$('#div_form_input').show();
	});
	var aaa = Math.random();
  $('#temp').val(aaa);
});
</script>

<script>
  function load_default(halaman) {
    $('#overlay_data_default').show();
    $('#tbl_utama_dokumen_informasi_publik').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>dokumen_informasi_publik/json_all_dokumen_informasi_publik/?halaman='+halaman+'',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * <?php echo $per_page; ?>);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_dokumen_informasi_publik="' + json[i].id_dokumen_informasi_publik + '" id="' + json[i].id_dokumen_informasi_publik + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
					tr += '<td valign="top">' + json[i].kode_dokumen_informasi_publik + '</td>';
					tr += '<td valign="top">' + json[i].nama_dokumen_informasi_publik + '  <small class="badge bg-blue">0</small></td>';
					tr += '<td valign="top">' + json[i].keterangan + '</td>';
					tr += '<td valign="top">';
					tr += '<a href="#tab_1" data-toggle="tab" class="update_id label label-info" ><i class="fa fa-pencil-square-o"></i></a> ';
					tr += '<a class="label label-danger" href="#tab_1" data-toggle="tab" id="del_ajax" ><i class="fa fa-cut"></i></a> ';
					tr += '</td>';
          tr += '</tr>';
        }
        $('#tbl_utama_dokumen_informasi_publik').append(tr);
				$('#overlay_data_default').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#pagination').on('click', '.update_id', function(e) {
		e.preventDefault();
		var id = $(this).closest('li').attr('page');
		var halaman = id;
		load_default(halaman);
	});
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	var halaman = 1;
	load_default(halaman);
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#tbl_utama_dokumen_informasi_publik, #tbl_search_dokumen_informasi_publik').on('click', '.update_id', function(e) {
		e.preventDefault();
		$('#e_overlay_form_input').show();
		var id = $(this).closest('tr').attr('id_dokumen_informasi_publik');
		$.ajax({
			dataType: 'json',
			url: '<?php echo base_url(); ?>dokumen_informasi_publik/dokumen_informasi_publik_get_by_id/?id='+id+'',
			success: function(json) {
				if (json.length == 0) {
					alert('Tidak ada data');
				} else {
					for (var i = 0; i < json.length; i++) {
					$('#e_id_dokumen_informasi_publik').val(json[i].id_dokumen_informasi_publik);
					$('#e_kode_dokumen_informasi_publik').val(json[i].kode_dokumen_informasi_publik);
					$('#e_nama_dokumen_informasi_publik').val(json[i].nama_dokumen_informasi_publik);
					$('#e_inserted_by').val(json[i].inserted_by);
					$('#e_inserted_time').val(json[i].inserted_time);
					$('#e_updated_by').val(json[i].updated_by);
					$('#e_updated_time').val(json[i].updated_time);
					$('#e_deleted_by').val(json[i].deleted_by);
					$('#e_deleted_time').val(json[i].deleted_time);
					$('#e_temp').val(json[i].temp);
					$('#e_keterangan').val(json[i].keterangan);
					$('#e_status').val(json[i].status);
										}
					e_load_lampiran_dokumen_informasi_publik(id);
					$('#div_form_input').fadeOut('slow');
					$('#e_div_form_input').show();
					$('#e_overlay_form_input').fadeOut(2000);
					$('html, body').animate({
						scrollTop: $('#awal').offset().top
					}, 1000);
				}
			}
		});
	});
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_dokumen_informasi_publik').on('click', function(e) {
      e.preventDefault();
      $('#simpan_dokumen_informasi_publik').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
			
			var id_dokumen_informasi_publik = $("#id_dokumen_informasi_publik").val();
			var kode_dokumen_informasi_publik = $("#kode_dokumen_informasi_publik").val();
			var nama_dokumen_informasi_publik = $("#nama_dokumen_informasi_publik").val();
			var inserted_by = $("#inserted_by").val();
			var inserted_time = $("#inserted_time").val();
			var updated_by = $("#updated_by").val();
			var updated_time = $("#updated_time").val();
			var deleted_by = $("#deleted_by").val();
			var deleted_time = $("#deleted_time").val();
			var temp = $("#temp").val();
			var keterangan = $("#keterangan").val();
			var status = $("#status").val();
						
			if (kode_dokumen_informasi_publik == '') {
					$('#kode_dokumen_informasi_publik').css('background-color', '#DFB5B4');
				} else {
					$('#kode_dokumen_informasi_publik').removeAttr('style');
				}
			if (nama_dokumen_informasi_publik == '') {
					$('#nama_dokumen_informasi_publik').css('background-color', '#DFB5B4');
				} else {
					$('#nama_dokumen_informasi_publik').removeAttr('style');
				}
			if (inserted_by == '') {
					$('#inserted_by').css('background-color', '#DFB5B4');
				} else {
					$('#inserted_by').removeAttr('style');
				}
			if (temp == '') {
					$('#temp').css('background-color', '#DFB5B4');
				} else {
					$('#temp').removeAttr('style');
				}
						
			$.ajax({
        type: 'POST',
        async: true,
        data: {
					kode_dokumen_informasi_publik:kode_dokumen_informasi_publik,
					nama_dokumen_informasi_publik:nama_dokumen_informasi_publik,
					temp:temp,
					keterangan:keterangan,
					status:status,
										},
        dataType: 'text',
        url: '<?php echo base_url(); ?>dokumen_informasi_publik/simpan_dokumen_informasi_publik/',
        success: function(json) {
          if (json == '0') {
            $('#simpan_dokumen_informasi_publik').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut();
            alertify.set({ delay: 3000 });
            alertify.error("Gagal simpan");
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
          } else {
            alertify.set({ delay: 3000 });
            alertify.success("Berhasil simpan");
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
						$('#tbl_lampiran_dokumen_informasi_publik').html('');
						$('#message_progress_upload_lampiran_dokumen_informasi_publik').html('');
            var halaman = 1;
            load_default(halaman);
            var aaa = Math.random();
            $('#temp').val(aaa);
            $('#div_form_input form').trigger('reset');
            $('#simpan_dokumen_informasi_publik').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut('slow');
          }
        }
      });
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#update_data_dokumen_informasi_publik').on('click', function(e) {
		e.preventDefault();
		$('#update_data_dokumen_informasi_publik').attr('disabled', 'disabled');
		$('#e_overlay_form_input').show();
		var id_dokumen_informasi_publik = $('#e_id_dokumen_informasi_publik').val();
		var kode_dokumen_informasi_publik = $('#e_kode_dokumen_informasi_publik').val();
		var nama_dokumen_informasi_publik = $('#e_nama_dokumen_informasi_publik').val();
		var temp = $('#e_temp').val();
		var keterangan = $('#e_keterangan').val();
						
		if (id_dokumen_informasi_publik == '') {
				$('#e_id_dokumen_informasi_publik').css('background-color', '#DFB5B4');
			} else {
				$('#e_id_dokumen_informasi_publik').removeAttr('style');
			}
		if (kode_dokumen_informasi_publik == '') {
				$('#e_kode_dokumen_informasi_publik').css('background-color', '#DFB5B4');
			} else {
				$('#e_kode_dokumen_informasi_publik').removeAttr('style');
			}
		if (nama_dokumen_informasi_publik == '') {
				$('#e_nama_dokumen_informasi_publik').css('background-color', '#DFB5B4');
			} else {
				$('#e_nama_dokumen_informasi_publik').removeAttr('style');
			}
		if (temp == '') {
				$('#e_temp').css('background-color', '#DFB5B4');
			} else {
				$('#e_temp').removeAttr('style');
			}
		if (keterangan == '') {
				$('#e_keterangan').css('background-color', '#DFB5B4');
			} else {
				$('#e_keterangan').removeAttr('style');
			}
				
			$.ajax({
			type: 'POST',
			async: true,
			data: {
					
					id_dokumen_informasi_publik:id_dokumen_informasi_publik,
					kode_dokumen_informasi_publik:kode_dokumen_informasi_publik,
					nama_dokumen_informasi_publik:nama_dokumen_informasi_publik,
					temp:temp,
					keterangan:keterangan,
										
				},
			dataType: 'json',
			url: '<?php echo base_url(); ?>dokumen_informasi_publik/update_data_dokumen_informasi_publik/',
			success: function(json) {
				if (json.length == 0) {
          alertify.set({ delay: 3000 });
          alertify.error("Gagal simpan");
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
					$('#update_data_dokumen_informasi_publik').removeAttr('disabled', 'disabled');
					$('#e_overlay_form_input').fadeOut('slow');
					} 
				else {
          alertify.set({ delay: 3000 });
          alertify.success("Berhasil simpan");
					var halaman = 1;
					load_default(halaman);
					$('#e_div_form_input form').trigger('reset');
					$('#update_data_dokumen_informasi_publik').removeAttr('disabled', 'disabled');
					$('#e_overlay_form_input').fadeOut('slow');
					$('#e_div_form_input').fadeOut('slow');
					$('#tbl_lampiran_dokumen_informasi_publik').html('');
					$('#e_tbl_lampiran_dokumen_informasi_publik').html('');
					$('#a_tbl_lampiran_dokumen_informasi_publik').html('');
					}
			}
		});
	});
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#cari_dokumen_informasi_publik').on('click', function(e) {
      e.preventDefault();
      $('#overlay_data_cari').show();
      var key_word = $('#key_word').val();
      var start = 0;
      $('#tbl_search_dokumen_informasi_publik').html('');
      $('#total_data_search').html('');
      $.ajax({
        type: 'POST',
        async: true,
        data: {
          key_word: key_word,
          halaman: 1
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>dokumen_informasi_publik/search_dokumen_informasi_publik/',
        success: function(json) {
          if (json.length == 0) {
            if (key_word == '') {
              $('#key_word').css('background-color', '#DFB5B4');
            } else {
              $('#key_word').removeAttr('style');
            }
						$('#overlay_data_cari').fadeOut('slow');
          } else {
            $('#tbl_search_dokumen_informasi_publik').html('');
            var tr = '';
            for (var i = 0; i < json.length; i++) {
              start = start + 1;
              tr += '<tr id_dokumen_informasi_publik="' + json[i].id_dokumen_informasi_publik + '" id="id_dokumen_informasi_publik' + json[i].id_dokumen_informasi_publik + '" >';
							tr += '<td valign="top">' + (start) + '</td>';
							tr += '<td valign="top">' + json[i].kode_dokumen_informasi_publik + '</td>';
              tr += '<td valign="top"><a class="" href="<?php echo base_url(); ?>kecamatan/?id_dokumen_informasi_publik=' + json[i].id_dokumen_informasi_publik + '">' + json[i].nama_dokumen_informasi_publik + '</td>';
              tr += '<td valign="top">' + json[i].keterangan + '</td>';
              tr += '<td valign="top"><a class="" href="<?php echo base_url(); ?>user_dokumen_informasi_publik/?id_dokumen_informasi_publik=' + json[i].id_dokumen_informasi_publik + '">Tampilkan</a></td>';
              tr += '<td valign="top">';
							tr += '<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> ';
							tr += '<a href="#tab_1" data-toggle="tab" id="del_ajax" ><i class="fa fa-cut"></i></a>';
							tr += '</td>';
							tr += '</tr>';
            }
						$('#key_word').removeAttr('style');
            $('#tbl_search_dokumen_informasi_publik').append(tr);
						$('#overlay_data_cari').fadeOut('slow');
          }
        }
      });
      $('#total_data_search').html('');
      $.ajax({
        dataType: 'text',
        url: '<?php echo base_url(); ?>dokumen_informasi_publik/count_all_search_dokumen_informasi_publik/?key_word='+key_word+'',
        success: function(json) {
          var jumlah = json;
          $('#next_page_search').html('');
          var ajax_pagination = '';
          for (var a = 0; a < jumlah; a++) {
            ajax_pagination += '<li id="'+a+'" page="'+a+'" key_word="'+key_word+'" ><a id="next" href="#">'+(a + 1)+'</a></li>';
          }
          $('#next_page_search').append(ajax_pagination);
        }
      });
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#next_page_search').on('click', '#next', function(e) {
      e.preventDefault();
			$('#overlay_data_cari').show();
      var id = $(this).closest('li').attr('page');
      var key_word = $(this).closest('li').attr('key_word');
      var keyword_f = $(this).closest('li').attr('keyword_f');
			var halaman = parseInt(id) + 1;
      $('#tbl_search_dokumen_informasi_publik').html('');
      $.ajax({
				type: 'POST',
        async: true,
        data: {
          key_word: key_word,
					halaman:halaman
					},
        dataType: 'json',
        url: '<?php echo base_url(); ?>dokumen_informasi_publik/search_dokumen_informasi_publik/',
        success: function(json) {
          var tr = '';
					var start = (((parseInt(id) + 1) - 1) * <?php echo $per_page; ?>);
					for (var i = 0; i < json.length; i++) {
						var start = parseInt(start) + 1;
            tr += '<tr id_dokumen_informasi_publik="' + json[i].id_dokumen_informasi_publik + '" id="id_dokumen_informasi_publik' + json[i].id_dokumen_informasi_publik + '" >';
						tr += '<td valign="top">' + (start) + '</td>';
						tr += '<td valign="top">' + json[i].kode_dokumen_informasi_publik + '</td>';
              tr += '<td valign="top"><a class="" href="<?php echo base_url(); ?>kecamatan/?id_dokumen_informasi_publik=' + json[i].id_dokumen_informasi_publik + '">' + json[i].nama_dokumen_informasi_publik + '</td>';
              tr += '<td valign="top">' + json[i].keterangan + '</td>';
              tr += '<td valign="top"><a class="" href="<?php echo base_url(); ?>user_dokumen_informasi_publik/?id_dokumen_informasi_publik=' + json[i].id_dokumen_informasi_publik + '">Tampilkan</a></td>';
												tr += '<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> ';
						tr += '<a href="#tab_1" data-toggle="tab" id="del_ajax" ><i class="fa fa-cut"></i></a>';
						tr += '</td>';
						tr += '</tr>';
          }
          $('#tbl_search_dokumen_informasi_publik').append(tr);
          $('#overlay_data_cari').fadeOut('slow');
        }
      });
    });
  });
</script>

<script>
function reset() {
	$('#toggleCSS').attr('href', '<?php echo base_url(); ?>css/alertify/alertify.default.css');
	alertify.set({
		labels: {
			ok: 'OK',
			cancel: 'Cancel'
		},
		delay: 5000,
		buttonReverse: false,
		buttonFocus: 'ok'
	});
}
//===============HAPUS OBAT
$('#tbl_search_dokumen_informasi_publik, #tbl_utama_dokumen_informasi_publik').on('click', '#del_ajax', function() {
	reset();
	var id = $(this).closest('tr').attr('id_dokumen_informasi_publik');
	alertify.confirm('Anda yakin data akan dihapus?', function(e) {
		if (e) {
			$.ajax({
				dataType: 'json',
				url: '<?php echo base_url(); ?>dokumen_informasi_publik/hapus_dokumen_informasi_publik/?id='+id+'',
				success: function(response) {
					if (response.errors == 'Yes') {
						alertify.alert('Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya');
					} else {
						$('[id_dokumen_informasi_publik='+id+']').remove();
						alertify.alert('Data berhasil dihapus');
					}
				}
			});
		} else {
			alertify.alert('Hapus data dibatalkan');
		}
	});
});
//===============HAPUS Attachment
$('#tbl_lampiran_dokumen_informasi_publik, #e_tbl_lampiran_dokumen_informasi_publik, #a_tbl_lampiran_dokumen_informasi_publik').on('click', '#del_ajax', function() {
	reset();
	var id = $(this).closest('tr').attr('id_attachment');
	alertify.confirm('Anda yakin data akan dihapus?', function(e) {
		if (e) {
			$.ajax({
				dataType: 'json',
				url: '<?php echo base_url(); ?>attachment/hapus/?id='+id+'',
				success: function(response) {
					if (response.errors == 'Yes') {
						alertify.alert('Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya');
					} else {
						$('[id_attachment='+id+']').remove();
						$('#message_progress_upload_lampiran_dokumen_informasi_publik').remove();
						$('#e_message_progress_upload_lampiran_dokumen_informasi_publik').remove();
						$('#a_message_progress_upload_lampiran_dokumen_informasi_publik').remove();
						$('#percent_progress_upload_lampiran_dokumen_informasi_publik').html('');
						$('#e_percent_progress_upload_lampiran_dokumen_informasi_publik').html('');
						$('#a_percent_progress_upload_lampiran_dokumen_informasi_publik').html('');
						alertify.alert('Data berhasil dihapus');
					}
				}
			});
		} else {
			alertify.alert('Hapus data dibatalkan');
		}
	});
});

</script>

<script>
	function load_lampiran_dokumen_informasi_publik() {
		$('#tbl_lampiran_dokumen_informasi_publik').html('');
		var temp = $('#temp').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				temp:temp
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/?table=dokumen_informasi_publik',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_lampiran_dokumen_informasi_publik').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
			var options = { 
				beforeSend: function() {
					$('#progress_upload_lampiran_dokumen_informasi_publik').show();
					$('#bar_progress_upload_lampiran_dokumen_informasi_publik').width('0%');
					$('#message_progress_upload_lampiran_dokumen_informasi_publik').html('');
					$('#percent_progress_upload_lampiran_dokumen_informasi_publik').html('0%');
					},
				uploadProgress: function(event, position, total, percentComplete){
					$('#bar_progress_upload_lampiran_dokumen_informasi_publik').width(percentComplete+'%');
					$('#percent_progress_upload_lampiran_dokumen_informasi_publik').html(percentComplete+'%');
					},
				success: function(){
					$('#bar_progress_upload_lampiran_dokumen_informasi_publik').width('100%');
					$('#percent_progress_upload_lampiran_dokumen_informasi_publik').html('100%');
					},
				complete: function(response){
					$('#message_progress_upload_lampiran_dokumen_informasi_publik').html('<font color="green">'+response.responseText+'</font>');
					var temp = $('#temp').val();
					load_lampiran_dokumen_informasi_publik();
					},
				error: function(){
					$('#message_progress_upload_lampiran_dokumen_informasi_publik').html('<font color="red"> ERROR: unable to upload files</font>');
					}     
			};
			document.getElementById('dokumen_informasi_publik_baru').onchange = function() {
					$('#form_dokumen_informasi_publik').submit();
				};
			$('#form_dokumen_informasi_publik').ajaxForm(options);
		});
</script>

<script>
	function e_load_lampiran_dokumen_informasi_publik() {
		$('#e_tbl_lampiran_dokumen_informasi_publik').html('');
		var id = $('#e_id_dokumen_informasi_publik').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				id:id
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/e_load_lampiran/?table=dokumen_informasi_publik',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#e_tbl_lampiran_dokumen_informasi_publik').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
			var options = { 
				beforeSend: function() {
					$('#e_progress_upload_lampiran_dokumen_informasi_publik').show();
					$('#e_bar_progress_upload_lampiran_dokumen_informasi_publik').width('0%');
					$('#e_message_progress_upload_lampiran_dokumen_informasi_publik').html('');
					$('#e_percent_progress_upload_lampiran_dokumen_informasi_publik').html('0%');
					},
				uploadProgress: function(event, position, total, percentComplete){
					$('#e_bar_progress_upload_lampiran_dokumen_informasi_publik').width(percentComplete+'%');
					$('#e_percent_progress_upload_lampiran_dokumen_informasi_publik').html(percentComplete+'%');
					},
				success: function(){
					$('#e_bar_progress_upload_lampiran_dokumen_informasi_publik').width('100%');
					$('#e_percent_progress_upload_lampiran_dokumen_informasi_publik').html('100%');
					},
				complete: function(response){
					$('#e_message_progress_upload_lampiran_dokumen_informasi_publik').html('<font color="green">'+response.responseText+'</font>');
					e_load_lampiran_dokumen_informasi_publik();
					},
				error: function(){
					$('#e_message_progress_upload_lampiran_dokumen_informasi_publik').html('<font color="red"> ERROR: unable to upload files</font>');
					}     
			};
			document.getElementById('e_dokumen_informasi_publik_baru').onchange = function() {
					$('#e_form_dokumen_informasi_publik').submit();
				};
			$('#e_form_dokumen_informasi_publik').ajaxForm(options);
		});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#a_update_data_dokumen_informasi_publik').on('click', function(e) {
		e.preventDefault();
		$('#a_update_data_dokumen_informasi_publik').attr('disabled', 'disabled');
		$('#a_overlay_form_input').show();
		var id_dokumen_informasi_publik = $('#a_id_dokumen_informasi_publik').val();
		var kode_dokumen_informasi_publik = $('#a_kode_dokumen_informasi_publik').val();
		var nama_dokumen_informasi_publik = $('#a_nama_dokumen_informasi_publik').val();
		var temp = $('#a_temp').val();
		var keterangan = $('#a_keterangan').val();
						
		if (id_dokumen_informasi_publik == '') {
				$('#a_id_dokumen_informasi_publik').css('background-color', '#DFB5B4');
			} else {
				$('#a_id_dokumen_informasi_publik').removeAttr('style');
			}
		if (kode_dokumen_informasi_publik == '') {
				$('#a_kode_dokumen_informasi_publik').css('background-color', '#DFB5B4');
			} else {
				$('#a_kode_dokumen_informasi_publik').removeAttr('style');
			}
		if (nama_dokumen_informasi_publik == '') {
				$('#a_nama_dokumen_informasi_publik').css('background-color', '#DFB5B4');
			} else {
				$('#a_nama_dokumen_informasi_publik').removeAttr('style');
			}
		if (temp == '') {
				$('#a_temp').css('background-color', '#DFB5B4');
			} else {
				$('#a_temp').removeAttr('style');
			}
		if (keterangan == '') {
				$('#a_keterangan').css('background-color', '#DFB5B4');
			} else {
				$('#a_keterangan').removeAttr('style');
			}
				$.ajax({
			type: 'POST',
			async: true,
			data: {
					
					id_dokumen_informasi_publik:id_dokumen_informasi_publik,
					kode_dokumen_informasi_publik:kode_dokumen_informasi_publik,
					nama_dokumen_informasi_publik:nama_dokumen_informasi_publik,
					temp:temp,
					keterangan:keterangan,
																				
				},
			dataType: 'json',
			url: '<?php echo base_url(); ?>dokumen_informasi_publik/update_data_dokumen_informasi_publik/',
			success: function(json) {
				if (json.length == 0) {
          alertify.set({ delay: 3000 });
          alertify.error("Gagal simpan");
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
					$('#a_update_data_dokumen_informasi_publik').removeAttr('disabled', 'disabled');
					$('#a_overlay_form_input').fadeOut('slow');
					} 
				else {
          alertify.success("Berhasil simpan");
					var halaman = 1;
					load_default(halaman);
					$('#a_div_form_input form').trigger('reset');
					$('#a_update_data_dokumen_informasi_publik').removeAttr('disabled', 'disabled');
					$('#a_overlay_form_input').fadeOut('slow');
					$('#a_tbl_lampiran_dokumen_informasi_publik').html('');
					}
			}
		});
	});
});
</script>

<script>
	function a_load_lampiran_dokumen_informasi_publik() {
		$('#a_tbl_lampiran_dokumen_informasi_publik').html('');
		var id = $('#a_id_dokumen_informasi_publik').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				id:id
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/e_load_lampiran/?table=dokumen_informasi_publik',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#a_tbl_lampiran_dokumen_informasi_publik').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
			var options = { 
				beforeSend: function() {
					$('#a_progress_upload_lampiran_dokumen_informasi_publik').show();
					$('#a_bar_progress_upload_lampiran_dokumen_informasi_publik').width('0%');
					$('#a_message_progress_upload_lampiran_dokumen_informasi_publik').html('');
					$('#a_percent_progress_upload_lampiran_dokumen_informasi_publik').html('0%');
					},
				uploadProgress: function(event, position, total, percentComplete){
					$('#a_bar_progress_upload_lampiran_dokumen_informasi_publik').width(percentComplete+'%');
					$('#a_percent_progress_upload_lampiran_dokumen_informasi_publik').html(percentComplete+'%');
					},
				success: function(){
					$('#a_bar_progress_upload_lampiran_dokumen_informasi_publik').width('100%');
					$('#a_percent_progress_upload_lampiran_dokumen_informasi_publik').html('100%');
					},
				complete: function(response){
					$('#a_message_progress_upload_lampiran_dokumen_informasi_publik').html('<font color="green">'+response.responseText+'</font>');
					a_load_lampiran_dokumen_informasi_publik();
					},
				error: function(){
					$('#a_message_progress_upload_lampiran_dokumen_informasi_publik').html('<font color="red"> ERROR: unable to upload files</font>');
					}     
			};
			document.getElementById('a_dokumen_informasi_publik_baru').onchange = function() {
					$('#a_form_dokumen_informasi_publik').submit();
				};
			$('#a_form_dokumen_informasi_publik').ajaxForm(options);
		});
</script>

<script type="text/javascript">
	function reply_a_dokumen_informasi_publik(id_dokumen_informasi_publik, kode_dokumen_informasi_publik, nama_dokumen_informasi_publik )
	{
		$('#a_id_dokumen_informasi_publik').val( id_dokumen_informasi_publik );
		var id = id_dokumen_informasi_publik;
		$.ajax({
			dataType: 'json',
			url: '<?php echo base_url(); ?>dokumen_informasi_publik/dokumen_informasi_publik_get_by_id/?id='+id+'',
			success: function(json) {
				if (json.length == 0) {
					alert('Tidak ada data');
				} else {
					for (var i = 0; i < json.length; i++) {
					
					$('#a_id_dokumen_informasi_publik').val(json[i].id_dokumen_informasi_publik);
					$('#a_kode_dokumen_informasi_publik').val(json[i].kode_dokumen_informasi_publik);
					$('#a_nama_dokumen_informasi_publik').val(json[i].nama_dokumen_informasi_publik);
					$('#a_inserted_by').val(json[i].inserted_by);
					$('#a_inserted_time').val(json[i].inserted_time);
					$('#a_updated_by').val(json[i].updated_by);
					$('#a_updated_time').val(json[i].updated_time);
					$('#a_deleted_by').val(json[i].deleted_by);
					$('#a_deleted_time').val(json[i].deleted_time);
					$('#a_temp').val(json[i].temp);
					$('#a_keterangan').val(json[i].keterangan);
					$('#a_status').val(json[i].status);
										}
					a_load_lampiran_dokumen_informasi_publik(id);
				}
			}
		});
	}		
	$(document).ready(function(){
		$('#a_nama_dokumen_informasi_publik').typeahead({
			name: 'a_nama_dokumen_informasi_publik',
			template:['<div onclick="reply_a_dokumen_informasi_publik(this.getAttribute(\'id_dokumen_informasi_publik\'), this.getAttribute(\'kode_dokumen_informasi_publik\'), this.getAttribute(\'nama_dokumen_informasi_publik\') )" id_dokumen_informasi_publik="{{id_dokumen_informasi_publik}}" kode_dokumen_informasi_publik="{{kode_dokumen_informasi_publik}}"  nama_dokumen_informasi_publik="{{nama_dokumen_informasi_publik}}" ><div><p>{{nama_dokumen_informasi_publik}}<br><b>{{kode_dokumen_informasi_publik}}</b></div>',
								'</div>'
							 ].join(''),
			engine: Hogan,
			remote: '<?php echo base_url(); ?>dokumen_informasi_publik/auto_suggest/?q=%QUERY'
		});
		$('.form-control').siblings('input.tt-hint').remove();
	});
</script>