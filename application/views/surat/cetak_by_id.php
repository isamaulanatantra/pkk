<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php if(!empty( $title )){ echo $title; } ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="https://diskominfo.wonosobokab.go.id/assets/dist/css/adminlte.min.css">

  <link href="https://fonts.googleapis.com/css?family=Arial" rel="stylesheet">
  <!-- 
	-->
<style>
  	body {
		font-family: 'Roboto Mono', serif;
		font-size: 18px;
  	}
	hr {
		border-width: 4px;
		background-color: #000;
	} 
</style>
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
  	<div class="row">
      <div class="col-md-12">
        <div class="box">
          <div class="box-body table-responsive">
            <table class="table table-bordered table-hover">
              <tbody>
                <tr>
                  <td colspan="2">
                      <h1>Agenda <?php if(!empty( $nama_kelembagaan )){ echo $nama_kelembagaan; } ?> <?php if(!empty( $nama_desa )){ echo $nama_desa; } ?></h1>
                      <p>Alamat : <?php if(!empty( $alamat )){ echo $alamat; } ?></p>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <center>
                      <h1>LEMBAR DISPOSISI</h1>
                    </center>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Indeks Berkas : <?php if(!empty( $nama_kelembagaan )){ echo $nama_kelembagaan; } ?> Kode : <?php if(!empty( $nama_kelembagaan )){ echo $nama_kelembagaan; } ?>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Tanggal/Nomor : <?php if(!empty( $nama_kelembagaan )){ echo $nama_kelembagaan; } ?> / <?php if(!empty( $nama_kelembagaan )){ echo $nama_kelembagaan; } ?>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Asal Surat : <?php if(!empty( $nama_kelembagaan )){ echo $nama_kelembagaan; } ?>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Isi Ringkas : <?php if(!empty( $nama_kelembagaan )){ echo $nama_kelembagaan; } ?>
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Diterima Tanggal : <?php if(!empty( $nama_kelembagaan )){ echo $nama_kelembagaan; } ?> No. Agenda ( <?php if(!empty( $nama_kelembagaan )){ echo $nama_kelembagaan; } ?> )
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Tanggal Penyelesaian : <?php if(!empty( $nama_kelembagaan )){ echo $nama_kelembagaan; } ?>
                  </td>
                </tr>
                <tr>
                  <td>
                    Isi Disposisi : <?php if(!empty( $nama_kelembagaan )){ echo $nama_kelembagaan; } ?>
                  </td>
                  <td>
                    Diteruskan kepada :<br />
                    1. Kasubag Kelembagaan<br />
                    2. Kasubag Tata Laksana dan Pelayanan Publik<br />
                    3. Kasubag Pendayagunaan Aparatur<br />
                    4. Kepala TU<br />
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    Sesudah digunakan harap dikembalikan<br />
                    Kepada : ........................................................................................................................................<br />
                    Tanggal : ........................................................................................................................................<br />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
		</div>
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>