<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php $web = $this->uut->namadomain(base_url());
          $aa = explode(".", $web);
          echo $aa[0]; ?> Integrated Sosial Media Layanan Publik Kabupaten Wonosobo dengan sistem Integrasi artinya dapat posting di semua website se Kabupaten Wonosobo</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Meta -->
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" />
  <meta name="author" content="<?php if (!empty($title)) {
                                  echo $title;
                                } ?>" />
  <meta name="keywords" content="<?php if (!empty($title)) {
                                    echo $title;
                                  } ?> <?php echo base_url(); ?>" />
  <meta name="og:description" content="<?php if (!empty($title)) {
                                          echo $title;
                                        } ?>" />
  <meta name="og:url" content="<?php echo base_url(); ?> <?php if (!empty($title)) {
                                                            echo $title;
                                                          } ?>" />
  <meta name="og:title" content="<?php if (!empty($title)) {
                                    echo $title;
                                  } ?> <?php echo base_url(); ?>" />
  <meta name="og:keywords" content="<?php if (!empty($title)) {
                                      echo $title;
                                    } ?> <?php echo base_url(); ?>" />
																		<?php
																		if($web=='demoopd.wonosobokab.go.id' or $web=='jdih.wonosobokab.go.id'){
																		echo '
																			<meta name="og:image" content="'.base_url().'media/logo-jdihn.png" />
																			<link id="favicon" rel="shortcut icon" href="'.base_url().'media/logo-jdihn.png" type="image/png" />
																		';
																		}
																		else{
																			echo '
																			<meta name="og:image" content="'.base_url().'media/logo wonosobo.png" />
																			<link id="favicon" rel="shortcut icon" href="'.base_url().'media/logo wonosobo.png" type="image/png" />
																			';
																		}
																		?>
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/Font-Awesome-master/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- pace-progress -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>pace-progress/themes/red/pace-theme-flat-top.css">
  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/animate.css/animate.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/summernote/dist/summernote-lite.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/bootstrap-tagsinput/css/bootstrap-tagsinput.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/flatpickr/dist/flatpickr.min.css">

  <!-- CSS Front Template -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/theme.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.css">

  <script type="text/javascript">
    <?php

    foreach ($menu->result() as $b1) {

      $data['id_users'] = $b1->id_users;

      echo

        '

          function menu' . $b1->module_code . '() {

            getAjax("/' . strtolower($b1->module_code) . '", \'html\', function (response) {

                $("#tag_container").empty().html(response);

            });

          }

          ';
    }

    echo

      '

          function menuGanti_password() {

            getAjax("/ganti_password", \'html\', function (response) {

                $("#tag_container").empty().html(response);

            });

          }

          ';

    echo
      '
          function menuUser_profile() {
              getAjax("/user_profile", \'html\', function (response) {
                  $("#tag_container").empty().html(response);
              });
          }
          ';
    echo
      '
          function menuCompany_profile() {
            getAjax("/company_profile", \'html\', function (response) {
                $("#tag_container").empty().html(response);
            });
          }
          ';
    echo
      '
          function menuFoto_profil() {
            getAjax("/foto_profil", \'html\', function (response) {
                $("#tag_container").empty().html(response);
            });
          }
          ';
    echo
      '
          function menuSignup() {
            getAjax("/signup", \'html\', function (response) {
                $("#tag_container").empty().html(response);
            });
          }
          ';
    ?>
  </script>
  <script src="<?php echo base_url(); ?>Template/main.js"></script>

</head>

<body>

  <!-- ========== HEADER ========== -->
  <header id="header" class="u-header">
    <!-- Search -->
    <div id="searchPushTop" class="u-search-push-top">
      <div class="container position-relative">
        <div class="u-search-push-top__content">
          <!-- Close Button -->
          <button type="button" class="close u-search-push-top__close-btn" aria-haspopup="true" aria-expanded="false" aria-controls="searchPushTop" data-unfold-type="jquery-slide" data-unfold-target="#searchPushTop">
            <span aria-hidden="true">&times;</span>
          </button>
          <!-- End Close Button -->

          <!-- Input -->
          <form class="js-focus-state input-group">
            <input type="search" class="form-control" placeholder="Search Front" aria-label="Search Front">
            <div class="input-group-append">
              <button type="button" class="btn btn-primary">Search</button>
            </div>
          </form>
          <!-- End Input -->

          <!-- Content -->
          <div class="row d-none d-md-flex mt-7">
            <div class="col-sm-6">
              <strong class="d-block mb-2">Quick Links</strong>

              <div class="row">
                <!-- List Group -->
                <div class="col-6">
                  <div class="list-group list-group-transparent list-group-flush list-group-borderless">
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Search Results List
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Search Results Grid
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      About
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Services
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Invoice
                    </a>
                  </div>
                </div>
                <!-- End List Group -->

                <!-- List Group -->
                <div class="col-6">
                  <div class="list-group list-group-transparent list-group-flush list-group-borderless">
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Profile
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      User Contacts
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Reviews
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Settings
                    </a>
                  </div>
                </div>
                <!-- End List Group -->
              </div>
            </div>

            <div class="col-sm-6">
              <!-- Banner -->
              <div class="rounded u-search-push-top__banner">
                <div class="d-flex align-items-center">
                  <div class="u-search-push-top__banner-container">
                    <img class="img-fluid u-search-push-top__banner-img" src="<?php echo base_url(); ?>front/assets/img/mockups/img3.png" alt="Image Description">
                    <img class="img-fluid u-search-push-top__banner-img" src="<?php echo base_url(); ?>front/assets/img/mockups/img2.png" alt="Image Description">
                  </div>

                  <div>
                    <div class="mb-4">
                      <strong class="d-block mb-2">Featured Item</strong>
                      <p>Create astonishing web sites and pages.</p>
                    </div>
                    <a class="btn btn-xs btn-soft-success transition-3d-hover" href="index.html">Apply Now <span class="fas fa-angle-right ml-2"></span></a>
                  </div>
                </div>
              </div>
              <!-- End Banner -->
            </div>
          </div>
          <!-- End Content -->
        </div>
      </div>
    </div>
    <!-- End Search -->

    <div class="u-header__section">
      <!-- Topbar -->
      <div class="container u-header__hide-content pt-2">
        <div class="d-flex align-items-center">
          <!-- Language
        <div class="position-relative">
          <a id="languageDropdownInvoker" class="dropdown-nav-link dropdown-toggle d-flex align-items-center" href="javascript:;" role="button" aria-controls="languageDropdown" aria-haspopup="true" aria-expanded="false" data-unfold-event="hover" data-unfold-target="#languageDropdown" data-unfold-type="css-animation" data-unfold-duration="300" data-unfold-delay="300" data-unfold-hide-on-scroll="true" data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
            <img class="dropdown-item-icon" src="<?php // echo base_url(); 
                                                  ?>front/assets/vendor/flag-icon-css/flags/4x3/us.svg" alt="SVG">
            <span class="d-inline-block d-sm-none">US</span>
            <span class="d-none d-sm-inline-block">United States</span>
          </a>

          <div id="languageDropdown" class="dropdown-menu dropdown-unfold" aria-labelledby="languageDropdownInvoker">
            <a class="dropdown-item active" href="#">English</a>
            <a class="dropdown-item" href="#">Deutsch</a>
            <a class="dropdown-item" href="#">Español‎</a>
          </div>
        </div>
        End Language -->

          <div class="ml-auto">
            <!-- Jump To -->
            <div class="d-inline-block d-sm-none position-relative mr-2">
              <a id="jumpToDropdownInvoker" class="dropdown-nav-link dropdown-toggle d-flex align-items-center" href="javascript:;" role="button" aria-controls="jumpToDropdown" aria-haspopup="true" aria-expanded="false" data-unfold-event="hover" data-unfold-target="#jumpToDropdown" data-unfold-type="css-animation" data-unfold-duration="300" data-unfold-delay="300" data-unfold-hide-on-scroll="true" data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
                Jump to
              </a>

              <div id="jumpToDropdown" class="dropdown-menu dropdown-unfold" aria-labelledby="jumpToDropdownInvoker">
                <a class="dropdown-item" href="../pages/faq.html">Help</a>
                <a class="dropdown-item" href="../pages/contacts-agency.html">Contacts</a>
              </div>
            </div>
            <!-- End Jump To -->

            <!-- Links -->
            <div class="d-none d-sm-inline-block ml-sm-auto">
              <ul class="list-inline mb-0">
                <li class="list-inline-item mr-0">
                  <a class="u-header__navbar-link" href="../pages/faq.html">Help</a>
                </li>
                <li class="list-inline-item mr-0">
                  <a class="u-header__navbar-link" href="../pages/contacts-agency.html">Contacts</a>
                </li>
              </ul>
            </div>
            <!-- End Links -->
          </div>

          <ul class="list-inline ml-2 mb-0">
            <!-- Search -->
            <li class="list-inline-item">
              <a class="btn btn-xs btn-icon btn-text-secondary" href="javascript:;" role="button" aria-haspopup="true" aria-expanded="false" aria-controls="searchPushTop" data-unfold-type="jquery-slide" data-unfold-target="#searchPushTop">
                <span class="fas fa-search btn-icon__inner"></span>
              </a>
            </li>
            <!-- End Search -->

            <!-- Shopping Cart
          <li class="list-inline-item position-relative">
            <a id="shoppingCartDropdownInvoker" class="btn btn-xs btn-icon btn-text-secondary" href="javascript:;" role="button" aria-controls="shoppingCartDropdown" aria-haspopup="true" aria-expanded="false" data-unfold-event="hover" data-unfold-target="#shoppingCartDropdown" data-unfold-type="css-animation" data-unfold-duration="300" data-unfold-delay="300" data-unfold-hide-on-scroll="true" data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
              <span class="fas fa-shopping-cart btn-icon__inner"></span>
            </a>

            <div id="shoppingCartDropdown" class="dropdown-menu dropdown-unfold dropdown-menu-right text-center p-7" aria-labelledby="shoppingCartDropdownInvoker" style="min-width: 250px;">
              <span class="btn btn-icon btn-soft-primary rounded-circle mb-3">
                <span class="fas fa-shopping-basket btn-icon__inner"></span>
              </span>
              <span class="d-block">Your Cart is Empty</span>
            </div>
          </li>
          End Shopping Cart -->

            <!-- Account Login -->
            <li class="list-inline-item">
              <!-- Account Sidebar Toggle Button -->
              <a id="sidebarNavToggler" class="btn btn-xs btn-text-secondary u-sidebar--account__toggle-bg ml-1" href="javascript:;" role="button" aria-controls="sidebarContent" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent" data-unfold-type="css-animation" data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">

                <?php
                $ses = $this->session->userdata('id_users');
                if (!$ses) {
                  echo '
                <span class="position-relative">
                  <span class="u-sidebar--account__toggle-text">Login</span>
                  <img class="u-sidebar--account__toggle-img" src="https://web.wonosobokab.go.id/front/assets/svg/icons/icon-4.svg" alt="Image Login">
                </span>
                ';
                } else {
                  echo '
                <span class="position-relative">
                  <span class="u-sidebar--account__toggle-text">Natalie Curtis</span>
                  <img class="u-sidebar--account__toggle-img" src="' . base_url() . 'front/assets/img/100x100/img1.jpg" alt="Image Description">
                  <span class="badge badge-sm badge-success badge-pos rounded-circle">3</span>
                </span>
                ';
                }
                ?>

              </a>
              <!-- End Account Sidebar Toggle Button -->
            </li>
            <!-- End Account Login -->
          </ul>
        </div>
      </div>
      <!-- End Topbar -->

      <div id="logoAndNav" class="container">
        <!-- Nav -->
        <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">

          <!-- Responsive Toggle Button -->
          <button type="button" class="navbar-toggler btn u-hamburger" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
            <span id="hamburgerTrigger" class="u-hamburger__box">
              <span class="u-hamburger__inner"></span>
            </span>
          </button>
          <!-- End Responsive Toggle Button -->

          <!-- Navigation -->
          <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
            <?php echo '' . $menu_atas . ''; ?>
          </div>
          <!-- End Navigation -->
        </nav>
        <!-- End Nav -->
      </div>
    </div>
  </header>
  <!-- ========== END HEADER ========== -->

  <!-- ========== MAIN ========== -->
  <main id="content" role="main">
    <!-- Breadcrumb Section -->
    <div class="bg-primary">
      <div class="container space-1">
        <div class="d-sm-flex justify-content-sm-between align-items-sm-center">
          <div class="mb-3 mb-sm-0">
            <div class="media d-block d-sm-flex align-items-sm-center">
              <?php
              $ses = $this->session->userdata('id_users');
              if (!$ses) {
                echo '
              <div class="u-lg-avatar position-relative mb-3 mb-sm-0 mr-3">
                <img class="img-fluid rounded-circle" src="' . base_url() . 'front/assets/svg/icons/icon-4.svg" alt="Image Description">
                <span class="badge badge-md badge-outline-success badge-pos badge-pos--bottom-right rounded-circle">
                  <span class="fas fa-check"></span>
                </span>
              </div>
              <div class="media-body">
                <h1 class="h3 text-white font-weight-medium mb-1">
                  <a href="' . base_url() . '">
                  ';
                if (!empty($title)) {
                  echo $title;
                }
                echo '
                  </a>
                </h1>
                <span class="d-block text-white">
                ';
                if (!empty($email)) {
                  echo $email;
                }
                echo '
                </span>
              </div>
              ';
              } else {
                echo '
              <div class="u-lg-avatar position-relative mb-3 mb-sm-0 mr-3">
                <img class="img-fluid rounded-circle" src="';
                if (!empty($foto)) {
                  echo '' . $foto . '';
                } else {
                  echo '' . base_url() . 'front/assets/svg/icons/icon-4.svg';
                }
                echo '" alt="Image ';
                if (!empty($first_name)) {
                  echo '' . $first_name . ' ' . $last_name . ' ';
                }
                echo '">
                <span class="badge badge-md badge-outline-success badge-pos badge-pos--bottom-right rounded-circle">
                  <span class="fas fa-check"></span>
                </span>
              </div>
              <div class="media-body">
                <h1 class="h3 text-white font-weight-medium mb-1">
                  ';
                if (!empty($title)) {
                  echo $title;
                }
                echo '
                </h1>
                <span class="d-block text-white">
                ';
                if (!empty($email)) {
                  echo $email;
                }
                echo '
                </span>
              </div>
              ';
              }
              ?>
            </div>
            <!-- End Breadcrumb -->
          </div>

          <!-- Edit Profile
        <a class="btn btn-sm btn-soft-white transition-3d-hover" href="edit-profile.html">
          <span class="fas fa-user-cog small mr-2"></span>
          Edit Profile
        </a>
         End Edit Profile -->
        </div>

      </div>
    </div>
    <!-- End Breadcrumb Section -->

    <!-- Content Section -->
    <div class="bg-light">
      <div class="container space-2">
        <div class="row">
          <div class="col-lg-3 mb-7 mb-lg-0">
            <?php
            $ses = $this->session->userdata('id_users');
            if (!$ses) {
            } else {
            ?>
              <!-- Profile Card -->
              <div class="card p-1 mb-4">
                <div class="card-body">
                  <div class="mb-3 text-center">
                    <img class="u-lg-avatar rounded-circle" src="<?php if (!empty($foto)) {
                                                                    echo '' . $foto . '';
                                                                  } else {
                                                                    echo '' . base_url() . 'front/assets/img/160x160/img2.jpg';
                                                                  } ?>" alt="Image <?php if (!empty($first_name)) {
                                                                                        echo '' . $first_name . ' ' . $last_name . ' ';
                                                                                      } ?>">
                  </div>

                  <?php
                  if ($menu_root->num_rows() > 0) {
                    echo '
                    <div class="mb-3">
                      <h1 class="h6 font-weight-medium mb-0">';
                    if (!empty($first_name)) {
                      echo '' . $first_name . ' ' . $last_name . ' ';
                    }
                    echo '</h1>
                      <small class="d-block text-muted">ROOT</small>
                    </div>
                    ';
                    $a = 0;
                    foreach ($menu_root->result() as $b2) {
                      echo '
                        <div class="mb-2">
                          <a class="btn btn-sm btn-soft-primary transition-3d-hover mr-1" href="#" onclick="menu' . $b2->module_code . '()">
                            <span class="far fa-envelope mr-2"></span>
                            ' . ucwords(str_replace('_', ' ', $b2->module_name)) . '
                          </a>
                        </div>
                        ';
                    }
                  }
                  if ($menu_adminwebdesa->num_rows() > 0) {
                    echo '
                    <div class="mb-3">
                      <h1 class="h6 font-weight-medium mb-0">';
                    if (!empty($first_name)) {
                      echo '' . $first_name . ' ' . $last_name . ' ';
                    }
                    echo '</h1>
                      <small class="d-block text-muted">Admin Web Desa</small>
                    </div>
                    ';
                    $a = 0;
                    foreach ($menu_adminwebdesa->result() as $b2) {
                      echo '
                        <div class="mb-2">
                          <a class="btn btn-sm btn-soft-primary transition-3d-hover mr-1" href="#" onclick="menu' . $b2->module_code . '()">
                            <span class="far fa-envelope mr-2"></span>
                            ' . ucwords(str_replace('_', ' ', $b2->module_name)) . '
                          </a>
                        </div>
                        ';
                    }
                  }
                  if ($menu_adminwebskpd->num_rows() > 0) {
                    echo '
                    <div class="mb-3">
                      <h1 class="h6 font-weight-medium mb-0">';
                    if (!empty($first_name)) {
                      echo '' . $first_name . ' ' . $last_name . ' ';
                    }
                    echo '</h1>
                      <small class="d-block text-muted">Admin Web SKPD</small>
                    </div>
                    ';
                    $a = 0;
                    foreach ($menu_adminwebskpd->result() as $b2) {
                      echo '
                        <div class="mb-2">
                          <a class="btn btn-sm btn-soft-primary transition-3d-hover mr-1" href="#" onclick="menu' . $b2->module_code . '()">
                            <span class="far fa-envelope mr-2"></span>
                            ' . ucwords(str_replace('_', ' ', $b2->module_name)) . '
                          </a>
                        </div>
                        ';
                    }
                  }
                  if ($menu_adminjdih->num_rows() > 0) {
                    echo '
                    <div class="mb-3">
                      <h1 class="h6 font-weight-medium mb-0">';
                    if (!empty($first_name)) {
                      echo '' . $first_name . ' ' . $last_name . ' ';
                    }
                    echo '</h1>
                      <small class="d-block text-muted">Admin JDIH</small>
                    </div>
                    ';
                    $a = 0;
                    foreach ($menu_adminjdih->result() as $b2) {
                      echo '
                        <div class="mb-2">
                          <a class="btn btn-sm btn-soft-primary transition-3d-hover mr-1" href="#" onclick="menu' . $b2->module_code . '()">
                            <span class="far fa-envelope mr-2"></span>
                            ' . ucwords(str_replace('_', ' ', $b2->module_name)) . '
                          </a>
                        </div>
                        ';
                    }
                  }
                  ?>

                </div>
              </div>
              <!-- End Profile Card -->

            <?php
            }
            ?>

            <!-- Contacts  -->
            <div class="card mb-4">
              <div class="card-header pt-4 pb-3 px-0 mx-4">
                <h2 class="h6 mb-0">Contacts</h2>
              </div>

              <div class="card-body pt-3 pb-4 px-4">
                <!-- User -->
                <a class="d-flex align-items-start mb-4" href="#">
                  <div class="ml-3">
                    <span class="d-block text-dark">Alamat</span>
                    <small class="d-block text-secondary"><?php if (!empty($alamat)) {
                                                            echo $alamat;
                                                          } ?></small>
                  </div>
                </a>
                <!-- End User -->
                <!-- User -->
                <a class="d-flex align-items-start mb-4" href="#">
                  <div class="ml-3">
                    <span class="d-block text-dark">Telpon</span>
                    <small class="d-block text-secondary"><?php if (!empty($telpon)) {
                                                            echo $telpon;
                                                          } ?></small>
                  </div>
                </a>
                <!-- End User -->
                <!-- User -->
                <a class="d-flex align-items-start mb-4" href="#">
                  <div class="ml-3">
                    <span class="d-block text-dark">Email</span>
                    <small class="d-block text-secondary"><?php if (!empty($email)) {
                                                            echo $email;
                                                          } ?></small>
                  </div>
                </a>
                <!-- End User -->
              </div>
            </div>
            <!-- End Contacts  -->

            <!-- Social Profiles -->
            <div class="card mb-4">
              <div class="card-header pt-4 pb-3 px-0 mx-4">
                <h3 class="h6 mb-0">Social Profiles</h3>
              </div>

              <div class="card-body pt-3 pb-4 px-4">
                <!-- Social Profiles -->
                <a class="media mb-4" href="<?php if (!empty($instagram)) {
                                              echo $instagram;
                                            } ?>">
                  <div class="u-sm-avatar mr-3">
                    <img class="img-fluid" src="<?php echo base_url(); ?>front/assets/img/160x160/img11.jpg" alt="Image Description">
                  </div>
                  <div class="media-body">
                    <span class="d-block text-dark">Instagram</span>
                    <small class="d-block text-secondary">1.2k followers</small>
                  </div>
                </a>
                <!-- End Social Profiles -->

                <!-- Social Profiles -->
                <a class="media mb-4" href="<?php if (!empty($google)) {
                                              echo $google;
                                            } ?>">
                  <div class="u-sm-avatar mr-3">
                    <img class="img-fluid" src="<?php echo base_url(); ?>front/assets/img/160x160/img17.png" alt="Image Description">
                  </div>
                  <div class="media-body">
                    <span class="d-block text-dark">Google</span>
                    <small class="d-block text-secondary">4.5k followers</small>
                  </div>
                </a>
                <!-- End Social Profiles -->

                <!-- Social Profiles -->
                <a class="media mb-4" href="<?php if (!empty($facebook)) {
                                              echo $facebook;
                                            } ?>">
                  <div class="u-sm-avatar mr-3">
                    <img class="img-fluid" src="<?php echo base_url(); ?>front/assets/img/160x160/img19.png" alt="Image Description">
                  </div>
                  <div class="media-body">
                    <span class="d-block text-dark">Twitter</span>
                    <small class="d-block text-secondary">2.7k followers</small>
                  </div>
                </a>
                <!-- End Social Profiles -->

                <!-- Social Profiles -->
                <a class="media" href="<?php if (!empty($facebook)) {
                                          echo $facebook;
                                        } ?>">
                  <div class="u-sm-avatar mr-3">
                    <img class="img-fluid" src="<?php echo base_url(); ?>front/assets/img/160x160/img20.png" alt="Image Description">
                  </div>
                  <div class="media-body">
                    <span class="d-block text-dark">Facebook</span>
                    <small class="d-block text-secondary">3k followers</small>
                  </div>
                </a>
                <!-- End Social Profiles -->
              </div>
            </div>
            <!-- End Social Profiles -->
          </div>

          <div class="col-lg-9" id="tag_container">

            <?php $this->load->view($page);  ?>

          </div>
        </div>
      </div>
    </div>
    <!-- End Content Section -->
  </main>
  <!-- ========== END MAIN ========== -->

  <!-- ========== FOOTER ========== -->
  <footer>
    <!-- Lists -->
    <div class="border-bottom">
      <div class="container space-2">
        <div class="row justify-content-md-between">
          <div class="col-sm-4">

            <h3 class="h6 font-weight-semi-bold">Kami di sini untuk membantu</h3>
            <p class="font-weight-semi-bold">Temukan di <a class="text-warning font-weight-medium" href="<?php echo base_url(); ?>help">Pusat Bantuan kami.</a>
              <br />Email: <?php echo $email; ?><br />
              Telp: <?php echo $telpon; ?><br />
              Alamat: <?php echo $alamat; ?>
            </p>
          </div>

          <div class="col-md-6">
            <h4 class="h6 font-weight-semi-bold mb-4">We are driven to deliver results for all your businesses.</h4>

            <!-- Button -->
            <button type="button" class="btn btn-xs btn-dark btn-wide transition-3d-hover text-left mb-2 mr-1">
              <span class="media align-items-center">
                <span class="fab fa-apple fa-2x mr-3"></span>
                <span class="media-body">
                  <span class="d-block">Download on the</span>
                  <strong class="font-size-1">App Store</strong>
                </span>
              </span>
            </button>
            <!-- End Button -->

            <!-- Button -->
            <button type="button" class="btn btn-xs btn-dark btn-wide transition-3d-hover text-left mb-2">
              <span class="media align-items-center">
                <span class="fab fa-google-play fa-2x mr-3"></span>
                <span class="media-body">
                  <span class="d-block">Get it on</span>
                  <strong class="font-size-1">Google Play</strong>
                </span>
              </span>
            </button>
            <!-- End Button -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Lists -->

    <!-- Copyright -->
    <div class="container text-center space-1">
      <!-- Logo -->
      <a class="d-inline-flex align-items-center mb-2" href="../home/index.html" aria-label="Front">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="36px" height="36px" viewBox="0 0 46 46" xml:space="preserve" style="margin-bottom: 0;">
          <path fill="#3F7DE0" opacity=".65" d="M23,41L23,41c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18h11.3C38,5,41,8,41,11.7V23C41,32.9,32.9,41,23,41z" />
          <path class="fill-info" opacity=".5" d="M28,35.9L28,35.9c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18l11.3,0C43,0,46,3,46,6.6V18C46,27.9,38,35.9,28,35.9z" />
          <path class="fill-primary" opacity=".7" d="M18,46L18,46C8,46,0,38,0,28v0c0-9.9,8-18,18-18h11.3c3.7,0,6.6,3,6.6,6.6V28C35.9,38,27.9,46,18,46z" />
          <path class="fill-white" d="M17.4,34V18.3h10.2v2.9h-6.4v3.4h4.8v2.9h-4.8V34H17.4z" />
        </svg>
        <span class="brand brand-primary">Front</span>
      </a>
      <!-- End Logo -->
      <p class="small text-muted">&copy; Front. 2019 <a href="<?php echo base_url(); ?>"><?php echo base_url(); ?></a>. All rights reserved.</p>
    </div>
    <!-- End Copyright -->
  </footer>
  <!-- ========== END FOOTER ========== -->

  <!-- ========== SECONDARY CONTENTS ========== -->
  <!-- Account Sidebar Navigation -->
  <aside id="sidebarContent" class="u-sidebar" aria-labelledby="sidebarNavToggler">
    <div class="u-sidebar__scroller">
      <div class="u-sidebar__container">
        <?php
        if (!$ses) {
        ?>
          <div class="u-header-sidebar__footer-offset">
            <!-- Toggle Button -->
            <div class="d-flex align-items-center pt-4 px-7">
              <button type="button" class="close ml-auto" aria-controls="sidebarContent" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent" data-unfold-type="css-animation" data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <!-- End Toggle Button -->

            <!-- Content -->
            <div class="js-scrollbar u-sidebar__body">
              <div class="u-sidebar__content u-header-sidebar__content">
                <form class="js-validate" action="#" method="post">
                  <!-- Login -->
                  <div id="login" data-target-group="idForm">
                    <!-- Title -->
                    <header class="text-center mb-7">
                      <h2 class="h4 mb-0">Selamat Datang di <span style="text-transform: uppercase;"><?php $aa = explode(".", $web);
                                                                                                      echo $aa[0]; ?></span>!</h2>
                      <p>Integrated Sosial Media Layanan Publik Kabupaten Wonosobo dengan sistem Integrasi artinya dapat posting di semua website se Kabupaten Wonosobo </p>
                    </header>
                    <!-- End Title -->

                    <div class="alert alert-danger alert-dismissable" id="tunggu_redirect" style="display:none;">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Mohon Tunggu !</h4>
                      <div id="error2"></div>
                      Anda akan diarahkan ke dashboard
                    </div>
                    <div class="alert alert-warning alert-dismissable" id="loading_login" style="display:none;">
                      <h4><i class="fa fa-refresh fa-spin"></i> Mohon tunggu....</h4>
                    </div>
                    <div class="alert alert-danger alert-dismissable" id="login_error" style="display:none;">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <h4><i class="icon fa fa-ban"></i> Error !</h4>
                      <div id="pesan_error"></div>
                    </div>
                    <!-- Form Group -->
                    <div class="form-group">
                      <div class="js-form-message js-focus-state">
                        <label class="sr-only" for="signinEmail">Email</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="signinEmailLabel">
                              <span class="fas fa-user"></span>
                            </span>
                          </div>
                          <input type="email" class="form-control" name="user_name" id="user_name" placeholder="Email" aria-label="Email" aria-describedby="signinEmailLabel" required 
                               data-msg="Please enter a valid email address."
                               data-error-class="u-has-error"
                               data-success-class="u-has-success">
                        </div>
                      </div>
                    </div>
                    <!-- End Form Group -->

                    <!-- Form Group -->
                    <div class="form-group">
                      <div class="js-form-message js-focus-state">
                        <label class="sr-only" for="signinPassword">Password</label>
                        <div class="input-group">
                          <div class="input-group-prepend">
                            <span class="input-group-text" id="signinPasswordLabel">
                              <span class="fas fa-lock"></span>
                            </span>
                          </div>
                          <input type="password" class="form-control" name="password" id="password" placeholder="Password" aria-label="Password" aria-describedby="signinPasswordLabel" required 
                               data-msg="Your password is invalid. Please try again."
                               data-error-class="u-has-error"
                               data-success-class="u-has-success">
                        </div>
                      </div>
                    </div>
                    <!-- End Form Group -->

                    <div class="d-flex justify-content-end mb-4">
                      <a class="js-animation-link small link-muted" href="javascript:;" data-target="#forgotPassword" data-link-group="idForm" data-animation-in="slideInUp">Forgot Password?</a>
                    </div>

                    <div class="mb-2">
                      <button type="submit" class="btn btn-block btn-sm btn-primary transition-3d-hover" id="login_submit">Login</button>
                    </div>

                    <div class="text-center mb-4">
                      <span class="small text-muted">Do not have an account?</span>
                      <a class="js-animation-link small" href="<?php echo base_url(); ?>signup" data-target="#signup" data-link-group="idForm" data-animation-in="slideInUp">Signup
                      </a>
                    </div>

                    <div class="text-center">
                      <span class="u-divider u-divider--xs u-divider--text mb-4">OR</span>
                    </div>

                    <!-- Login Buttons -->
                    <div class="d-flex">
                      <a class="btn btn-block btn-sm btn-soft-facebook transition-3d-hover mr-1" href="#">
                        <span class="fab fa-facebook-square mr-1"></span>
                        Facebook
                      </a>
                      <a class="btn btn-block btn-sm btn-soft-google transition-3d-hover ml-1 mt-0" href="#">
                        <span class="fab fa-google mr-1"></span>
                        Google
                      </a>
                    </div>
                    <!-- End Login Buttons -->
                  </div>

									<!-- Signup -->
									<div id="signup" style="display: none; opacity: 0;" data-target-group="idForm">
										<!-- Title -->
										<header class="text-center mb-7">
											<h2 class="h4 mb-0">Welcome to Front.</h2>
											<p>Fill out the form to get started.</p>
										</header>
										<!-- End Title -->

										<!-- Form Group -->
										<div class="form-group">
											<div class="js-form-message js-focus-state">
												<label class="sr-only" for="signupEmail">Email</label>
												<div class="input-group">
													<div class="input-group-prepend">
														<span class="input-group-text" id="signupEmailLabel">
															<span class="fas fa-user"></span>
														</span>
													</div>
													<input type="email" class="form-control" name="email" id="signupEmail" placeholder="Email" aria-label="Email" aria-describedby="signupEmailLabel" required
																 data-msg="Please enter a valid email address."
																 data-error-class="u-has-error"
																 data-success-class="u-has-success">
												</div>
											</div>
										</div>
										<!-- End Input -->

										<!-- Form Group -->
										<div class="form-group">
											<div class="js-form-message js-focus-state">
												<label class="sr-only" for="signupPassword">Password</label>
												<div class="input-group">
													<div class="input-group-prepend">
														<span class="input-group-text" id="signupPasswordLabel">
															<span class="fas fa-lock"></span>
														</span>
													</div>
													<input type="password" class="form-control" name="password" id="signupPassword" placeholder="Password" aria-label="Password" aria-describedby="signupPasswordLabel" required
																 data-msg="Your password is invalid. Please try again."
																 data-error-class="u-has-error"
																 data-success-class="u-has-success">
												</div>
											</div>
										</div>
										<!-- End Input -->

										<!-- Form Group -->
										<div class="form-group">
											<div class="js-form-message js-focus-state">
												<label class="sr-only" for="signupConfirmPassword">Confirm Password</label>
												<div class="input-group">
													<div class="input-group-prepend">
														<span class="input-group-text" id="signupConfirmPasswordLabel">
															<span class="fas fa-key"></span>
														</span>
													</div>
													<input type="password" class="form-control" name="confirmPassword" id="signupConfirmPassword" placeholder="Confirm Password" aria-label="Confirm Password" aria-describedby="signupConfirmPasswordLabel" required
																 data-msg="Password does not match the confirm password."
																 data-error-class="u-has-error"
																 data-success-class="u-has-success">
												</div>
											</div>
										</div>
										<!-- End Input -->

										<div class="mb-2">
											<button type="submit" class="btn btn-block btn-sm btn-primary transition-3d-hover">Get Started</button>
										</div>

										<div class="text-center mb-4">
											<span class="small text-muted">Already have an account?</span>
											<a class="js-animation-link small" href="javascript:;"
												 data-target="#login"
												 data-link-group="idForm"
												 data-animation-in="slideInUp">Login
											</a>
										</div>

										<div class="text-center">
											<span class="u-divider u-divider--xs u-divider--text mb-4">OR</span>
										</div>

										<!-- Login Buttons -->
										<div class="d-flex">
											<a class="btn btn-block btn-sm btn-soft-facebook transition-3d-hover mr-1" href="#">
												<span class="fab fa-facebook-square mr-1"></span>
												Facebook
											</a>
											<a class="btn btn-block btn-sm btn-soft-google transition-3d-hover ml-1 mt-0" href="#">
												<span class="fab fa-google mr-1"></span>
												Google
											</a>
										</div>
										<!-- End Login Buttons -->
									</div>
									<!-- End Signup -->
                </form>
              </div>
            </div>
            <!-- End Content -->
          </div>

        <?php
        } else {
        ?>
          <div class="u-sidebar--account__footer-offset">
            <!-- Toggle Button -->
            <div class="d-flex justify-content-between align-items-center pt-4 px-7">
              <h3 class="h6 mb-0">My Account</h3>

              <button type="button" class="close ml-auto" aria-controls="sidebarContent" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent" data-unfold-type="css-animation" data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <!-- End Toggle Button -->

            <!-- Content -->
            <div class="js-scrollbar u-sidebar__body">
              <!-- Holder Info -->
              <header class="d-flex align-items-center u-sidebar--account__holder mt-3">
                <div class="position-relative">
                  <img class="u-sidebar--account__holder-img" src="<?php echo base_url(); ?>front/assets/img/100x100/img1.jpg" alt="Image Description">
                  <span class="badge badge-xs badge-outline-success badge-pos rounded-circle"></span>
                </div>
                <div class="ml-3">
                  <span class="font-weight-semi-bold">Natalie Curtis <span class="badge badge-success ml-1">Pro</span></span>
                  <span class="u-sidebar--account__holder-text">Lead Support Adviser</span>
                </div>

                <!-- Settings -->
                <div class="btn-group position-relative ml-auto mb-auto">
                  <a id="sidebar-account-settings-invoker" class="btn btn-xs btn-icon btn-text-secondary rounded" href="javascript:;" role="button" aria-controls="sidebar-account-settings" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#sidebar-account-settings" data-unfold-type="css-animation" data-unfold-duration="300" data-unfold-delay="300" data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
                    <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                  </a>

                  <div id="sidebar-account-settings" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="sidebar-account-settings-invoker">
                    <a class="dropdown-item" href="#">Settings</a>
                    <a class="dropdown-item" href="#">History</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="<?php echo base_url(); ?>sosegov/logout">Sign Out</a>
                  </div>
                </div>
                <!-- End Settings -->
              </header>
              <!-- End Holder Info -->

              <div class="u-sidebar__content--account">
                <!-- List Links -->
                <ul class="list-unstyled u-sidebar--account__list">
                  <li class="u-sidebar--account__list-item">
                    <a class="u-sidebar--account__list-link" href="dashboard.html">
                      <span class="fas fa-home u-sidebar--account__list-icon mr-2"></span>
                      Dashboard
                    </a>
                  </li>
                  <li class="u-sidebar--account__list-item">
                    <a class="u-sidebar--account__list-link" href="profile.html">
                      <span class="fas fa-user-circle u-sidebar--account__list-icon mr-2"></span>
                      Profile
                    </a>
                  </li>
                  <li class="u-sidebar--account__list-item">
                    <a class="u-sidebar--account__list-link" href="my-tasks.html">
                      <span class="fas fa-tasks u-sidebar--account__list-icon mr-2"></span>
                      My tasks
                    </a>
                  </li>
                  <li class="u-sidebar--account__list-item">
                    <a class="u-sidebar--account__list-link" href="projects.html">
                      <span class="fas fa-layer-group u-sidebar--account__list-icon mr-2"></span>
                      Projects <span class="badge badge-danger float-right mt-1">3</span>
                    </a>
                  </li>
                  <li class="u-sidebar--account__list-item">
                    <a class="u-sidebar--account__list-link" href="members.html">
                      <span class="fas fa-users u-sidebar--account__list-icon mr-2"></span>
                      Members
                    </a>
                  </li>
                  <li class="u-sidebar--account__list-item">
                    <a class="u-sidebar--account__list-link" href="activity.html">
                      <span class="fas fa-exchange-alt u-sidebar--account__list-icon mr-2"></span>
                      Activity
                    </a>
                  </li>
                  <li class="u-sidebar--account__list-item">
                    <a class="u-sidebar--account__list-link" href="payment-methods.html">
                      <span class="fas fa-wallet u-sidebar--account__list-icon mr-2"></span>
                      Payment methods
                    </a>
                  </li>
                  <li class="u-sidebar--account__list-item">
                    <a class="u-sidebar--account__list-link" href="plans.html">
                      <span class="fas fa-cubes u-sidebar--account__list-icon mr-2"></span>
                      Plans
                    </a>
                  </li>
                </ul>
                <!-- End List Links -->

                <div class="u-sidebar--account__list-divider"></div>

                <!-- List Links -->
                <ul class="list-unstyled u-sidebar--account__list">
                  <li class="u-sidebar--account__list-item">
                    <a class="u-sidebar--account__list-link" href="invite-friends.html">
                      <span class="fas fa-user-plus u-sidebar--account__list-icon mr-2"></span>
                      Invite friends
                    </a>
                  </li>
                  <li class="u-sidebar--account__list-item">
                    <a class="u-sidebar--account__list-link" href="api-token.html">
                      <span class="fas fa-key u-sidebar--account__list-icon mr-2"></span>
                      API Token
                    </a>
                  </li>
                </ul>
                <!-- End List Links -->
              </div>
            </div>
          </div>
        <?php
        }
        ?>
        <!-- Footer -->
        <footer id="SVGwaveWithDots" class="svg-preloader u-sidebar__footer u-sidebar__footer--account">
          <ul class="list-inline mb-0">
            <li class="list-inline-item pr-3">
              <a class="u-sidebar__footer--account__text" href="../pages/privacy.html">Privacy</a>
            </li>
            <li class="list-inline-item pr-3">
              <a class="u-sidebar__footer--account__text" href="../pages/terms.html">Terms</a>
            </li>
            <li class="list-inline-item">
              <a class="u-sidebar__footer--account__text" href="../pages/help.html">
                <i class="fas fa-info-circle"></i>
              </a>
            </li>
          </ul>

          <!-- SVG Background Shape -->
          <div class="position-absolute right-0 bottom-0 left-0">
            <img class="js-svg-injector" src="<?php echo base_url(); ?>front/assets/svg/components/wave-bottom-with-dots.svg" alt="Image Description" data-parent="#SVGwaveWithDots">
          </div>
          <!-- End SVG Background Shape -->
        </footer>
        <!-- End Footer -->
      </div>
    </div>
  </aside>
  <!-- End Account Sidebar Navigation -->
  <!-- ========== END SECONDARY CONTENTS ========== -->

  <div id="sidebar-overlay"></div>


  <script type="text/javascript">
    $(document).ready(function() {

      $(document).ajaxStart(function() {

        $("#loadinghalaman").show();

        Pace.restart();

      });

      $(document).ajaxSuccess(function() {

        Pace.stop();

      });

    });
  </script>


  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#" data-position='{"bottom": 15, "right": 15 }' data-type="fixed" data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->

  <!-- JS Global Compulsory -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/bootstrap/bootstrap.min.js"></script>

  <!-- JS Implementing Plugins -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/svg-injector/dist/svg-injector.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

  <!-- JS Front -->
  <script src="<?php echo base_url(); ?>front/assets/js/hs.core.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.header.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.unfold.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.malihu-scrollbar.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.focus-state.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.svg-injector.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.validation.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.summernote-editor.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.range-datepicker.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.go-to.js"></script>


  <!-- JS Plugins Init. -->
  <script>
    $(window).on('load', function() {
      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 767.98,
        hideTimeOut: 0
      });

      // initialization of HSMegaMenu component
      $('.js-breadcrumb-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 991.98,
        hideTimeOut: 0
      });

      // initialization of svg injector module
      $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function() {
      // initialization of header
      $.HSCore.components.HSHeader.init($('#header'));

      // initialization of unfold component
      $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
        afterOpen: function() {
          $(this).find('input[type="search"]').focus();
        }
      });

      // initialization of malihu scrollbar
      $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

      // initialization of forms
      $.HSCore.components.HSFocusState.init();

      // initialization of text editors
      $.HSCore.components.HSSummernoteEditor.init('.js-summernote-editor-hint', {
        toolbar: false,
      });

      $('#summernote').summernote('insertParagraph');

      // initialization of go to
      $.HSCore.components.HSGoTo.init('.js-go-to');
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function() {
			var limit = 3;
			var start = 0;
			var action = 'inactive';
			var judul = 'perundang_undangan';
			var keyword = $('#kata_kunci').val();
			var id = '<?php echo $id; ?>';

			function lazzy_loader_permohonan_informasi_publik(limit)
			{
				var output = '';
				for(var count=0; count<limit; count++)
				{
					output += '<div class="post_data">';
					output += '<p><span class="content-placeholder" style="width:100%; height: 30px;">&nbsp;</span></p>';
					output += '<p><span class="content-placeholder" style="width:100%; height: 100px;">&nbsp;</span></p>';
					output += '</div>';
				}
				$('#load_data_message_permohonan_informasi_publik').html(output);
			}

			lazzy_loader_permohonan_informasi_publik(limit);

			function load_data_permohonan_informasi_publik(limit, start, id, keyword)
			{
				$.ajax({
					url:"<?php echo base_url(); ?>sosegov/fetch_permohonan_informasi_publik",
					method:"POST",
					data:{limit:limit, start:start, id:id, keyword:keyword},
					cache: false,
					success:function(data)
					{
						if(data == '')
						{
							$('#load_data_message_permohonan_informasi_publik').html('<h3>No More Result Found</h3>');
							action = 'active';
						}
						else
						{
							$('#load_data_permohonan_informasi_publik').append(data);
							$('#load_data_message_permohonan_informasi_publik').html("");
							action = 'inactive';
						}
					}
				})
			}
			
			function load_data_kategori_permohonan_informasi_publik(limit, start, id, keyword)
			{
				$('#load_data_permohonan_informasi_publik').html("");
				$('#load_data_message_permohonan_informasi_publik').html("");
				$.ajax({
					url:"<?php echo base_url(); ?>sosegov/fetch_permohonan_informasi_publik",
					method:"POST",
					data:{limit:limit, start:start, id:id, keyword:keyword},
					cache: false,
					success:function(data)
					{
						if(data == '')
						{
							$('#load_data_message_permohonan_informasi_publik').html('<h3>No More Result Found</h3>');
							action = 'active';
						}
						else
						{
							$('#load_data_permohonan_informasi_publik').append(data);
							$('#load_data_message_permohonan_informasi_publik').html("");
							action = 'inactive';
						}
					}
				})
			}
			if(action == 'inactive')
			{
				action = 'active';
				var keyword = $('#kata_kunci').val();
				load_data_permohonan_informasi_publik(limit, start, id, keyword);
			}

			$(window).scroll(function(){
				var keyword = $('#kata_kunci').val();
				if($(window).scrollTop() + $(window).height() > $("#load_data_permohonan_informasi_publik").height() && action == 'inactive')
				{
					lazzy_loader_permohonan_informasi_publik(limit);
					action = 'active';
					start = start + limit;
					setTimeout(function(){
						load_data_permohonan_informasi_publik(limit, start, id, keyword);
					}, 1000);
				}
			});

			function debounce(fn, duration) {
				var timer;
				return function () {
					clearTimeout(timer);
					timer = setTimeout(fn, duration);
				}
			}
			$('#kata_kunci').on('keyup', debounce(function () {
				var keyword = $('#kata_kunci').val();
				load_data_kategori_permohonan_informasi_publik(limit, start, id, keyword);
			}, 1000));
			
		});
    $(document).ready(function() {
      $("#login_submit").on("click", function(e) {
        e.preventDefault();
        $('#login_error').hide();
        $('#loading_login').show();
        var user_name = $("#user_name").val();
        var password = $("#password").val();
        $.ajax({
          type: "POST",
          async: true,
          data: {
            user_name: user_name,
            password: password
          },
          dataType: "json",
          url: '<?php echo base_url(); ?>sosegov/proses_login',
          success: function(json) {
            var trHTML = '';
            for (var i = 0; i < json.length; i++) {
              if (json[i].errors == 'form_kosong') {
                $('#loading_login').fadeOut("slow");
                $('#login_error').show();
                $('#pesan_error').html('Mohon isi data secara lengkap');
                if (json[i].user_name == '') {
                  $('#user_name').css('background-color', '#DFB5B4');
                } else {
                  $('#user_name').removeAttr('style');
                }
                if (json[i].password == '') {
                  $('#password').css('background-color', '#DFB5B4');
                } else {
                  $('#password').removeAttr('style');
                }
              } else if (json[i].errors == 'user_tidak_ada') {
                $('#loading_login').fadeOut("slow");
                $('#login_error').show();
                $('#pesan_error').html('Data login anda salah');
                if (json[i].user_name == '') {
                  $('#user_name').css('background-color', '#DFB5B4');
                } else {
                  $('#user_name').removeAttr('style');
                }
                if (json[i].password == '') {
                  $('#password').css('background-color', '#DFB5B4');
                } else {
                  $('#password').removeAttr('style');
                }
              } else if (json[i].errors == 'miss_match') {
                $('#loading_login').fadeOut("slow");
                $('#login_error').show();
                $('#pesan_error').html('Data login anda salah');
                if (json[i].user_name == '') {
                  $('#user_name').css('background-color', '#DFB5B4');
                } else {
                  $('#user_name').removeAttr('style');
                }
                if (json[i].password == '') {
                  $('#password').css('background-color', '#DFB5B4');
                } else {
                  $('#password').removeAttr('style');
                }
              } else {
                $('#error2').html('Pesan : ' + json[i].errors + '');
                $('#box_login').hide();
                $('#tunggu_redirect').show();
                window.location = "<?php echo base_url(); ?>sosegov";
              }
            }
          }
        });
      });
    });
  </script>
  <script src="<?php echo base_url(); ?>assets/js/uutv1.js"></script>
  <!-- alertify -->
  <script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
  <script src="<?php echo base_url(); ?>boots/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.min.js"></script>

  <!-- Default-->
  <script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
  <script src="<?php echo base_url(); ?>js/alertify.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script>

  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->

  <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.js"></script>

  <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

  <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.extensions.js"></script>

  <script>
    $(function() {

      $("[data-mask]").inputmask();

    });
  </script>

  <!-- AdminLTE for demo purposes -->

  <!-- pace-progress -->
  <script src="<?php echo base_url(); ?>pace-progress/pace.min.js"></script>

  <script src="<?php echo base_url(); ?>js/uutv1.js"></script>

  <script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

  <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script>

</body>

</html>
<!-- end document-->