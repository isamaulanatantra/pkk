<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Pokja_satu</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>pokja_satu">Home</a></li>
          <li class="breadcrumb-item active">Pokja_satu</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">


  <div class="row" id="awal">
    <div class="col-12">
      <!-- Custom Tabs -->
      <div class="card">
        <div class="card-header p-2">
          <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link active" href="#tab_form_pokja_satu" data-toggle="tab" id="klik_tab_input">Form</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_data_pokja_satu" data-toggle="tab" id="klik_tab_data_pokja_satu">Data</a></li>
            <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>pokja_satu"><i class="fa fa-refresh"></i></a></li>
          </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="tab-content">
            <div class="tab-pane active" id="tab_form_pokja_satu">
              <input name="tabel" id="tabel" value="pokja_satu" type="hidden" value="">
              <form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=pokja_satu" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-6">
                    <input name="page" id="page" value="1" type="hidden" value="">
                    <div class="form-group" style="display:none;">
                      <label for="temp">temp</label>
                      <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="mode">mode</label>
                      <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_pokja_satu">id_pokja_satu</label>
                      <input class="form-control" id="id_pokja_satu" name="id" value="" placeholder="id_pokja_satu" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_propinsi">Provinsi</label>
                      <select class="form-control" id="id_propinsi" name="id_propinsi">
                        <option value="1">Jawa Tengah</option>
                      </select>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_kabupaten">Kabupaten</label>
                      <select class="form-control" id="id_kabupaten" name="id_kabupaten">
                        <option value="1">Wonosobo</option>
                      </select>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_kecamatan">Kecamatan</label>
                      <div class="overlay" id="overlay_id_kecamatan" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                      <select class="form-control" id="id_kecamatan" name="id_kecamatan">
                      </select>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-5">
                          <label for="id_desa">Desa</label>
                        </div>
                        <div class="col-md-7">
                          <div class="overlay" id="overlay_id_desa" style="display:none;">
                            <i class="fa fa-refresh fa-spin"></i>
                          </div>
                          <select class="form-control" id="id_desa" name="id_desa">
                            <option value="0">Pilih Desa</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_dusun">Dusun</label>
                      <div class="overlay" id="overlay_id_dusun" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                      <select class="form-control" id="id_dusun" name="id_dusun">
                        <option value="0">Pilih Dusun</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="tahun">tahun</label>
                        </div>
                        <div class="col-md-5">
                          <select class="form-control" id="tahun" name="tahun">
                            <option value="0000">Pilih tahun</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_pkbn">jumlah_kader_pkbn</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_pkbn" name="jumlah_kader_pkbn" value="" placeholder="jumlah_kader_pkbn" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_pkdrt">jumlah_kader_pkdrt</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_pkdrt" name="jumlah_kader_pkdrt" value="" placeholder="jumlah_kader_pkdrt" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_polaasuh">jumlah_kader_polaasuh</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_polaasuh" name="jumlah_kader_polaasuh" value="" placeholder="jumlah_kader_polaasuh" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="pkbn_klpsimulasi">pkbn_klpsimulasi</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="pkbn_klpsimulasi" name="pkbn_klpsimulasi" value="" placeholder="pkbn_klpsimulasi" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="pkbn_anggota">pkbn_anggota</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="pkbn_anggota" name="pkbn_anggota" value="" placeholder="pkbn_anggota" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="pkdrt_klp">pkdrt_klp</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="pkdrt_klp" name="pkdrt_klp" value="" placeholder="pkdrt_klp" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="pkdrt_anggota">pkdrt_anggota</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="pkdrt_anggota" name="pkdrt_anggota" value="" placeholder="pkdrt_anggota" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="polaasuh_klp">polaasuh_klp</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="polaasuh_klp" name="polaasuh_klp" value="" placeholder="polaasuh_klp" type="text">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="polaasuh_anggota">polaasuh_anggota</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="polaasuh_anggota" name="polaasuh_anggota" value="" placeholder="polaasuh_anggota" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="lansia_klp">lansia_klp</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="lansia_klp" name="lansia_klp" value="" placeholder="lansia_klp" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="lansia_anggota">lansia_anggota</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="lansia_anggota" name="lansia_anggota" value="" placeholder="lansia_anggota" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="kerjabakti">kerjabakti</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="kerjabakti" name="kerjabakti" value="" placeholder="kerjabakti" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="rukunkematian">rukunkematian</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="rukunkematian" name="rukunkematian" value="" placeholder="rukunkematian" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="keagamaan">keagamaan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="keagamaan" name="keagamaan" value="" placeholder="keagamaan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jimpitan">jimpitan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jimpitan" name="jimpitan" value="" placeholder="jimpitan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="arisan">arisan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="arisan" name="arisan" value="" placeholder="arisan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="keterangan">keterangan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="keterangan" name="keterangan" value="" placeholder="keterangan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary" id="simpan_pokja_satu">SIMPAN DATA KELUARGA</button>
                      <button type="submit" class="btn btn-primary" id="update_pokja_satu" style="display:none;">UPDATE DATA KELUARGA</button>
                      <a class="btn text-primary" href="<?php echo base_url(); ?>pokja_satu"><i class="fa fa-refresh"></i></a>
                    </div>
                  </div>
                </div>
              </form>

              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
            <div class="tab-pane table-responsive" id="tab_data_pokja_satu">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">&nbsp;</h3>
                  <div class="card-tools">
                    <div class="input-group input-group-sm">
                      <select class="form-control input-sm pull-right" id="keyword" name="keyword" style="width: 150px;">
                        <option value="2022">2022</option>
                        <option value="2021">2021</option>
                        <option value="2020">2020</option>
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                        <option value="2017">2017</option>
                        <option value="2016">2016</option>
                      </select>
                      <select name="limit" class="form-control input-sm pull-right" style="display:none;width: 150px;" id="limit">
                        <option value="999999999">Semua</option>
                        <option value="1">1 Per-Halaman</option>
                        <option value="10">10 Per-Halaman</option>
                        <option value="50">50 Per-Halaman</option>
                        <option value="100">100 Per-Halaman</option>
                      </select>
                      <select name="orderby" class="form-control input-sm pull-right" style="display:none; width: 150px;" id="orderby">
                        <option value="pokja_satu.tahun">tahun</option>
                      </select>
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" id="tampilkan_data_pokja_satu"><i class="fa fa-search"></i> Tampil</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-body table-responsive p-0">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th rowspan="3">NO</th>
                        <th rowspan="3">NAMA WILAYAH</th>
                        <th colspan="3">JUMLAH KADER</th>
                        <th colspan="8">PENGHAYATAN DAN PENGAMALAN PANCASILA</th>
                        <th colspan="5">GOTONG ROYONG</th>
                        <th rowspan="3">KETERANGAN</th>
                        <th rowspan="3"></th>
                      </tr>
                      <tr>
                        <th rowspan="2">PKBN</th>
                        <th rowspan="2">PKDRT</th>
                        <th rowspan="2">POLA ASUH</th>
                        <th colspan="2">PKBN</th>
                        <th colspan="2">PKDRT</th>
                        <th colspan="2">POLA ASUH</th>
                        <th colspan="2">LANSIA</th>
                        <th colspan="5">JUMLAH KELOMPOK</th>
                      </tr>
                      <tr>
                        <th>KLP</th>
                        <th>ANGGOTA</th>
                        <th>KLP</th>
                        <th>ANGGOTA</th>
                        <th>KLP</th>
                        <th>ANGGOTA</th>
                        <th>KLP</th>
                        <th>ANGGOTA</th>
                        <th>KERJA <br>BAKTI</th>
                        <th>RUKUN<br>KEMATIAN</th>
                        <th>KEAGAMAAN</th>
                        <th>JIMPITAN</th>
                        <th>ARISAN</th>
                      </tr>
                    </thead>
                    <tbody id="tbl_data_pokja_satu">
                    </tbody>
                  </table>
                </div>
                <div class="card-footer">
                  <ul class="pagination pagination-sm m-0 float-right" id="pagination" style="display:none;">
                  </ul>
                  <div class="overlay" id="spinners_tbl_data_pokja_satu" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.tab-content -->
          </div><!-- /.card-body -->
        </div>
        <!-- ./card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->


</section>


<script type="text/javascript">
  $(document).ready(function() {
    load_option_desa();
  });
</script>
<script>
  function load_option_desa() {
    $('#id_desa').html('');
    $('#overlay_id_desa').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>pokja_satu/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html('<option value="0">Pilih Desa</option><option value="9999">TP PKK Kecamatan</option>' + html + '');
        $('#overlay_id_desa').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $('#tabel').val();
  });
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit: limit,
        keyword: keyword,
        orderby: orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>pokja_satu/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_' + tabel + '').html('');
    $('#spinners_tbl_data_' + tabel + '').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page: page,
        limit: limit,
        keyword: keyword,
        orderby: orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>' + tabel + '/json_all_' + tabel + '/',
      success: function(html) {
        // $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
        $('#tbl_data_' + tabel + '').html(html);
        $('#spinners_tbl_data_' + tabel + '').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page = parseInt(id) + 1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_' + tabel + '').on('click', function(e) {
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_' + tabel + '').on('click', function(e) {
      load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedPokja_satu() {
    $('#id_pokja_satu, #id_dusun, #rt, #tahun, #id_desa, #id_kecamatan, #id_kabupaten, #jumlah_kader_pkbn, #jumlah_kader_pkdrt, #jumlah_kader_polaasuh, #pkbn_klpsimulasi, #pkbn_anggota, #pkdrt_klp, #pkdrt_anggota, #polaasuh_klp, #polaasuh_anggota, #lansia_klp, #lansia_anggota, #kerjabakti, #rukunkematian, #keagamaan, #jimpitan, #arisan, #keterangan').val('');
    $('#tbl_attachment_pokja_satu').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#temp').val(Math.random());
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_pokja_satu').on('click', function(e) {
      e.preventDefault();
      var parameter = ['id_desa', 'temp', 'jumlah_kader_pkbn'];
      InputValid(parameter);

      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["tahun"] = $("#tahun").val();
      parameter["jumlah_kader_pkbn"] = $("#jumlah_kader_pkbn").val();
      parameter["jumlah_kader_pkdrt"] = $("#jumlah_kader_pkdrt").val();
      parameter["jumlah_kader_polaasuh"] = $("#jumlah_kader_polaasuh").val();
      parameter["pkbn_klpsimulasi"] = $("#pkbn_klpsimulasi").val();
      parameter["pkbn_anggota"] = $("#pkbn_anggota").val();
      parameter["pkdrt_klp"] = $("#pkdrt_klp").val();
      parameter["pkdrt_anggota"] = $("#pkdrt_anggota").val();
      parameter["polaasuh_klp"] = $("#polaasuh_klp").val();
      parameter["polaasuh_anggota"] = $("#polaasuh_anggota").val();
      parameter["lansia_klp"] = $("#lansia_klp").val();
      parameter["lansia_anggota"] = $("#lansia_anggota").val();
      parameter["kerjabakti"] = $("#kerjabakti").val();
      parameter["rukunkematian"] = $("#rukunkematian").val();
      parameter["keagamaan"] = $("#keagamaan").val();
      parameter["jimpitan"] = $("#jimpitan").val();
      parameter["arisan"] = $("#arisan").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>pokja_satu/simpan_pokja_satu';

      var parameterRv = ['id_desa', 'temp', 'jumlah_kader_pkbn'];
      var Rv = RequiredValid(parameterRv);
      if (Rv == 0) {
        alertify.error('Mohon data diisi secara lengkap');
      } else {
        SimpanData(parameter, url);
        AfterSavedPokja_satu();
      }
      $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_data_pokja_satu').on('click', '.update_id_pokja_satu', function() {
      $('#mode').val('edit');
      $('#simpan_pokja_satu').hide();
      $('#update_pokja_satu').show();
      $('#overlay_data_anggota_keluarga').show();
      $('#tbl_data_anggota_keluarga').html('');
      var id_pokja_satu = $(this).closest('tr').attr('id_pokja_satu');
      var mode = $('#mode').val();
      var value = $(this).closest('tr').attr('id_pokja_satu');
      $('#form_baru').show();
      $('#judul_formulir').html('FORMULIR EDIT');
      $('#id_pokja_satu').val(id_pokja_satu);
      $.ajax({
        type: 'POST',
        async: true,
        data: {
          id_pokja_satu: id_pokja_satu
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>pokja_satu/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_propinsi').val(json[i].id_propinsi);
            $('#id_kabupaten').val(json[i].id_kabupaten);
            $('#id_kecamatan').val(json[i].id_kecamatan);
            $('#id_desa').val(json[i].id_desa);
            $('#id_dusun').val(json[i].id_dusun);
            $('#rt').val(json[i].rt);
            $('#tahun').val(json[i].tahun);
            $('#jumlah_kader_pkbn').val(json[i].jumlah_kader_pkbn);
            $('#jumlah_kader_pkdrt').val(json[i].jumlah_kader_pkdrt);
            $('#jumlah_kader_polaasuh').val(json[i].jumlah_kader_polaasuh);
            $('#pkbn_klpsimulasi').val(json[i].pkbn_klpsimulasi);
            $('#pkbn_anggota').val(json[i].pkbn_anggota);
            $('#pkdrt_klp').val(json[i].pkdrt_klp);
            $('#pkdrt_anggota').val(json[i].pkdrt_anggota);
            $('#polaasuh_klp').val(json[i].polaasuh_klp);
            $('#polaasuh_anggota').val(json[i].polaasuh_anggota);
            $('#lansia_klp').val(json[i].lansia_klp);
            $('#lansia_anggota').val(json[i].lansia_anggota);
            $('#kerjabakti').val(json[i].kerjabakti);
            $('#rukunkematian').val(json[i].rukunkematian);
            $('#keagamaan').val(json[i].keagamaan);
            $('#jimpitan').val(json[i].jimpitan);
            $('#arisan').val(json[i].arisan);
            $('#keterangan').val(json[i].keterangan);
            load_option_desa();
          }
        }
      });
      //AttachmentByMode(mode, value);
      //AfterSavedAnggota_keluarga(mode, value);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_pokja_satu').on('click', function(e) {
      e.preventDefault();
      var parameter = ['id_pokja_satu', 'id_desa', 'temp', 'jumlah_kader_pkbn'];
      InputValid(parameter);

      var parameter = {}
      parameter["id_pokja_satu"] = $("#id_pokja_satu").val();
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["tahun"] = $("#tahun").val();
      parameter["jumlah_kader_pkbn"] = $("#jumlah_kader_pkbn").val();
      parameter["jumlah_kader_pkdrt"] = $("#jumlah_kader_pkdrt").val();
      parameter["jumlah_kader_polaasuh"] = $("#jumlah_kader_polaasuh").val();
      parameter["pkbn_klpsimulasi"] = $("#pkbn_klpsimulasi").val();
      parameter["pkbn_anggota"] = $("#pkbn_anggota").val();
      parameter["pkdrt_klp"] = $("#pkdrt_klp").val();
      parameter["pkdrt_anggota"] = $("#pkdrt_anggota").val();
      parameter["polaasuh_klp"] = $("#polaasuh_klp").val();
      parameter["polaasuh_anggota"] = $("#polaasuh_anggota").val();
      parameter["lansia_klp"] = $("#lansia_klp").val();
      parameter["lansia_anggota"] = $("#lansia_anggota").val();
      parameter["kerjabakti"] = $("#kerjabakti").val();
      parameter["rukunkematian"] = $("#rukunkematian").val();
      parameter["keagamaan"] = $("#keagamaan").val();
      parameter["jimpitan"] = $("#jimpitan").val();
      parameter["arisan"] = $("#arisan").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>pokja_satu/update_pokja_satu';

      var parameterRv = ['id_pokja_satu', 'id_desa', 'temp', 'jumlah_kader_pkbn'];
      var Rv = RequiredValid(parameterRv);
      if (Rv == 0) {
        alertify.error('Mohon data diisi secara lengkap');
      } else {
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_data_pokja_satu').on('click', '#del_ajax_pokja_satu', function() {
      var id_pokja_satu = $(this).closest('tr').attr('id_pokja_satu');
      alertify.confirm('Anda yakin data akan dihapus?', function(e) {
        if (e) {
          var parameter = {}
          parameter["id_pokja_satu"] = id_pokja_satu;
          var url = '<?php echo base_url(); ?>pokja_satu/hapus/';
          HapusData(parameter, url);
          $('[id_pokja_satu=' + id_pokja_satu + ']').remove();
        } else {
          alertify.error('Hapus data dibatalkan');
        }
        $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
        load_data(tabel);
      });
    });
  });
</script>