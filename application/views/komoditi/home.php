<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
      <li><a data-toggle="tab" href="#tab_2" id="klik_tab_tampil" >Tampil</a></li>
      <li><a data-toggle="tab" href="#tab_6" id="klik_tab_tampil_arsip" >Arsip</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title" id="judul_formulir">FORMULIR INPUT</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" id="form_baru"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=komoditi" enctype="multipart/form-data">
                  <div class="box-body">
										<div class="form-group" style="display:none;">
											<label for="temp">temp</label>
											<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="mode">mode</label>
											<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_komoditi">id_komoditi</label>
											<input class="form-control" id="id_komoditi" name="id" value="" placeholder="id_komoditi" type="text">
										</div>
										<div class="form-group">
											<label for="parent">Parent</label>
											<select class="form-control" id="parent" name="parent" >
                      </select>
										</div>
										<div class="form-group">
											<label for="urut">Urut</label>
											<input class="form-control" id="urut" name="urut" value="" placeholder="urut" type="text">
										</div>
										<div class="form-group">
											<label for="satuan">Satuan</label>
											<input class="form-control" id="satuan" name="satuan" value="" placeholder="satuan" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="highlight">High Light</label>
											<select class="form-control" id="highlight" name="highlight" >
                      <option value="0">Tidak</option>
                      <option value="1">Ya</option>
                      </select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_pasar">Pasar</label>
											<select class="form-control" id="id_pasar" name="id_pasar" >
                      <option value="0">Pilih Pasar</option>
                      <option value="1">Pasar Induk</option>
                      <option value="2">Pasar Garung</option>
                      <option value="3">Pasar Kertek</option>
                      </select>
										</div>
										<div class="form-group">
											<label for="harga">Harga</label>
											<input class="form-control" id="harga" name="harga" value="" placeholder="Harga" type="text">
										</div>
										<div class="form-group">
											<label for="judul_komoditi">Judul Komoditi</label>
											<input class="form-control" id="judul_komoditi" name="judul_komoditi" value="" placeholder="Judul Komoditi" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="isi_komoditi">Isi Komoditi</label>
											<input class="form-control" id="isi_komoditi" name="isi_komoditi" value="" placeholder="Isi Komoditi" type="text">
										</div>
                    <div class="row">
                    <textarea id="editor_isi_komoditi"></textarea>
                    </div>
										<div class="form-group" style="display:none;">
											<label for="kata_kunci">Kata Kunci</label>
											<input class="form-control" id="kata_kunci" name="kata_kunci" value="-" placeholder="kata_kunci" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="keterangan">Keterangan Komoditi</label>
											<input class="form-control" id="keterangan" name="keterangan" value="-" placeholder="Keterangan" type="text">
										</div>
										<div class="alert alert-info alert-dismissable">
											<div class="form-group">
												<label for="remake">Keterangan Lampiran </label>
												<input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran " type="text">
											</div>
											<div class="form-group">
												<label for="myfile">File Lampiran </label>
												<input type="file" size="60" name="myfile" id="file_lampiran" >
											</div>
											<div id="ProgresUpload">
												<div id="BarProgresUpload"></div>
												<div id="PersenProgresUpload">0%</div >
											</div>
											<div id="PesanProgresUpload"></div>
										</div>
										<div class="alert alert-info alert-dismissable">
											<h3 class="box-title">Data Lampiran </h3>
											<table class="table table-bordered">
												<tr>
													<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
												</tr>
												<tbody id="tbl_attachment_komoditi">
												</tbody>
											</table>
										</div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="simpan_komoditi">SIMPAN</button>
                    <button type="submit" class="btn btn-primary" id="update_komoditi" style="display:none;">UPDATE</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">
                  Data
                </h3>
              </div>
              <div class="box-body">
                <div>
                  <table class="table table-striped table-bordered">
                    <tr>
                      <th>Urut</th>
                      <th>Judul Komoditi</th>
                      <th>Harga</th>
                      <th>Satuan</th>
                      <th>PROSES</th> 
                    </tr>
                    <tbody id="tbl_utama_komoditi">
                    </tbody>
                  </table>
                  <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                    </ul>
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab_6">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-header">
                <h3 class="box-title">
                  Arsip
                </h3>
              </div>
              <div class="box-body">
                <div>
                  <table class="table table-bordered">
                  <tbody id="tbl_utama_komoditi6">
                  </tbody>
                  </table>
                  <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                    </ul>
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data6" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script>
  $(document).ready(function() {
    $('#tanggal, #tanggal_sekarang').inputmask();
    $('#tanggal, #tanggal_sekarang').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
  
</script>

<script>
  function load_json_data_komoditi(halaman, limit, kata_kunci, urut_data_komoditi) {
    $('#tbl_utama_komoditi').html('');
    $('#spinners_data').show();
    var limit_data_tema_website = $('#limit_data_komoditi').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:halaman,
        limit:limit,
        kata_kunci:kata_kunci,
        urut_data_komoditi:urut_data_komoditi
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>komoditi/json_all_komoditi/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_komoditi="' + json[i].id_komoditi + '" id="' + json[i].id_komoditi + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
          tr += '<td valign="top">' + json[i].tanggal + '</td>';          
          tr += '<td valign="top">' + json[i].judul_komoditi + '</td>';          
          tr += '<td valign="top">' + json[i].urut + '</td>';
          tr += '<td valign="top">proses</td>';          
          
          tr += '</tr>';
        }
        $('#tbl_utama_komoditi').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  /* $(document).ready(function() {
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        tanggal: 'ok'
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>komoditi/cek_default_halaman/',
      success: function(text) {
        //alert(text);
      }
    });
  }); */
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#satuan').on('change', function(e) {
      e.preventDefault();
      var xa = $('#satuan').val();
      $("#satuanselected").html('<span class="fa '+xa+' ">'+xa+'</span>');
    });
  });
</script>

<script>
  function load_option_komoditi_by_tanggal(tanggal) {
    $('#parent').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        tanggal: tanggal
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>komoditi/load_the_option_by_tanggal/',
      success: function(html) {
        $('#parent').html('<option value="0">Utama</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tanggal').on('change', function(e) {
      e.preventDefault();
      var tanggal = $('#tanggal').val();
      load_option_komoditi_by_tanggal(tanggal);
    });
  });
</script>

<script>
  function load_option_komoditi() {
    $('#parent').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>komoditi/load_the_option/',
      success: function(html) {
        $('#parent').html('<option value="0">Utama</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_option_komoditi();
});
</script>

<script>
  function load_pagination() {
    var tr = '';
    var td = TotalData('<?php echo base_url(); ?>komoditi/total_komoditi/?limit='+limit_per_page_custome(20000)+'');
    for (var i = 1; i <= td; i++) {
      tr += '<li page="'+i+'" id="'+i+'"><a class="update_id" href="#">'+i+'</a></li>';
    }
    $('#pagination').html(tr);
  }
</script>
 
<script>
  function AfterSavedKomoditi() {
    $('#id_komoditi, #judul_komoditi, #isi_komoditi, #urut, #harga, #satuan, #keterangan').val('');
    $('#tbl_attachment_komoditi').html('');
    $('#PesanProgresUpload').html('');
    load_option_komoditi();
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
  load_pagination();
});
</script>
 
<script>
  function load_data_komoditi(halaman, limit) {
    $('#tbl_utama_komoditi').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>komoditi/load_table/',
      success: function(html) {
        $('#tbl_utama_komoditi').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script>
  function load_data_komoditi_arsip(halaman, limit) {
    $('#tbl_utama_komoditi6').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>komoditi/load_table_arsip/',
      success: function(html) {
        $('#tbl_utama_komoditi6').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script>
  function load_data_komoditi1(halaman, limit) {
    $('#tbl_utama_komoditi1').html('');
    $('#spinners_data1').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>komoditi/load_table1/',
      success: function(html) {
        $('#tbl_utama_komoditi1').html(html);
        $('#spinners_data1').hide();
      }
    });
  }
</script>
 
<script>
  function load_data_komoditi2(halaman, limit) {
    $('#tbl_utama_komoditi2').html('');
    $('#spinners_data2').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>komoditi/load_table2/',
      success: function(html) {
        $('#tbl_utama_komoditi2').html(html);
        $('#spinners_data2').hide();
      }
    });
  }
</script>
 
<script>
  function load_data_komoditi3(halaman, limit) {
    $('#tbl_utama_komoditi3').html('');
    $('#spinners_data3').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>komoditi/load_table3/',
      success: function(html) {
        $('#tbl_utama_komoditi3').html(html);
        $('#spinners_data3').hide();
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_tampil').on('click', function(e) {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_data_komoditi(halaman, limit);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_tampil_arsip').on('click', function(e) {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_data_komoditi_arsip(halaman, limit);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_harga').on('click', function(e) {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_data_komoditi1(halaman, limit);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_id_pasar_kanan').on('click', function(e) {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_data_komoditi2(halaman, limit);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_id_pasar_kiri').on('click', function(e) {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_data_komoditi3(halaman, limit);
  });
});
</script>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_komoditi').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'komoditi',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_komoditi').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_komoditi').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_komoditi').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_komoditi').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_komoditi').on('click', '#del_ajax', function() {
    var id_komoditi = $(this).closest('tr').attr('id_komoditi');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_komoditi"] = id_komoditi;
        var url = '<?php echo base_url(); ?>komoditi/hapus/';
        HapusData(parameter, url);
        $('[id_komoditi='+id_komoditi+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_komoditi6').on('click', '#restore_ajax', function() {
    var id_komoditi = $(this).closest('tr').attr('id_komoditi');
    alertify.confirm('Anda yakin data akan direstore?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_komoditi"] = id_komoditi;
        var url = '<?php echo base_url(); ?>komoditi/restore/';
        HapusData(parameter, url);
        $('[id_komoditi='+id_komoditi+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_komoditi').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_komoditi').hide();
    $('#update_komoditi').show();
    var id_komoditi = $(this).closest('tr').attr('id_komoditi');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_komoditi');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_komoditi').val(id_komoditi);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_komoditi:id_komoditi
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>komoditi/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#judul_komoditi').val(json[i].judul_komoditi);
            $('#parent').val(json[i].parent);
            // $('#highlight').val(json[i].highlight);
            $('#id_pasar').val(json[i].id_pasar);
            $('#harga').val(json[i].harga);
            // $('#isi_komoditi').val(json[i].isi_komoditi);
            $('#urut').val(json[i].urut);
            // $('#tanggal').val(json[i].tanggal);
            $('#satuan').val(json[i].satuan);
            // $('#kata_kunci').val(json[i].kata_kunci);
            // $('#keterangan').val(json[i].keterangan);
            CKEDITOR.instances.editor_isi_komoditi.setData(json[i].isi_komoditi);
          }
        }
      });
    AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_komoditi').show();
    $('#update_komoditi').hide();
    $('#tbl_attachment_komoditi').html('');
    $('#id_komoditi, #judul_komoditi, #highlight, #id_pasar,  #harga, #parent, #isi_komoditi, #urut, #satuan, #kata_kunci, #keterangan').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_komoditi').on('click', function(e) {
      e.preventDefault();
      var editor_isi_komoditi = CKEDITOR.instances.editor_isi_komoditi.getData();
      $('#isi_komoditi').val( editor_isi_komoditi );
      var parameter = [ 'judul_komoditi', 'highlight', 'id_pasar', 'harga', 'parent', 'parent', 'isi_komoditi', 'urut', 'satuan', 'kata_kunci', 'keterangan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["judul_komoditi"] = $("#judul_komoditi").val();
      parameter["highlight"] = $("#highlight").val();
      parameter["id_pasar"] = $("#id_pasar").val();
      parameter["harga"] = $("#harga").val();
      parameter["parent"] = $("#parent").val();
      parameter["isi_komoditi"] = $("#isi_komoditi").val();
      parameter["urut"] = $("#urut").val();
      // parameter["tanggal"] = $("#tanggal").val();
      parameter["satuan"] = $("#satuan").val();
      parameter["kata_kunci"] = $("#kata_kunci").val();
      parameter["keterangan"] = $("#keterangan").val();
      parameter["temp"] = $("#temp").val();
      var url = '<?php echo base_url(); ?>komoditi/simpan_komoditi';
      
      var parameterRv = [ 'judul_komoditi', 'highlight', 'id_pasar', 'harga', 'parent', 'isi_komoditi', 'urut', 'satuan', 'temp', 'kata_kunci', 'keterangan' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedKomoditi();
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_komoditi').on('click', function(e) {
      e.preventDefault();
      var editor_isi_komoditi = CKEDITOR.instances.editor_isi_komoditi.getData();
      $('#isi_komoditi').val( editor_isi_komoditi );
      var parameter = [ 'judul_komoditi', 'id_pasar', 'harga', 'parent', 'isi_komoditi', 'urut', 'satuan'  ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["judul_komoditi"] = $("#judul_komoditi").val();
      // parameter["highlight"] = $("#highlight").val();
      parameter["id_pasar"] = $("#id_pasar").val();
      parameter["harga"] = $("#harga").val();
      parameter["parent"] = $("#parent").val();
      parameter["isi_komoditi"] = $("#isi_komoditi").val();
      parameter["urut"] = $("#urut").val();
      // parameter["tanggal"] = $("#tanggal").val();
      parameter["satuan"] = $("#satuan").val();
      // parameter["kata_kunci"] = $("#kata_kunci").val();
      // parameter["keterangan"] = $("#keterangan").val();
      parameter["temp"] = $("#temp").val();
      parameter["id_komoditi"] = $("#id_komoditi").val();
      var url = '<?php echo base_url(); ?>komoditi/update_komoditi';
      
      var parameterRv = [ 'judul_komoditi', 'id_pasar', 'harga', 'parent', 'isi_komoditi', 'urut', 'satuan', 'id_komoditi' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#pagination').on('click', '.update_id', function(e) {
		e.preventDefault();
		var id = $(this).closest('li').attr('page');
		var halaman = id;
    var limit = limit_per_page_custome(20000);
    load_data_komoditi(halaman, limit);
    
	});
});
</script>
<!----------------------->
<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('editor_isi_komoditi');
	$(".textarea").wysihtml5();
});
</script