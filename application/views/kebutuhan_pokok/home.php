
                        <div class="row">
                          <div class="col-md-12">
                              <!-- general form elements -->
                              <div id="awal"></div>
                              <div class="box box-primary" id="box_form_baru">
                                <div class="box-header with-border">
                                  <h3 class="box-title">Formulir Input Harga Kebutuhan Pokok</h3>
                                </div>
                                <div class="box-body">
                                    <div class="error_message_find">
                                    </div>
                                  <!-- form start -->
                                  <form role="form" id="form" method="post" action="<?php echo base_url(); ?>kebutuhan_pokok/update_generate_data_baru" enctype="multipart/form-data">
                                    <div class="row">
                                      <div class="col-xs-6">
                                        <div class="form-group">
                                          <label>Tanggal</label>
                                          <input class="form-control" name="tanggal" id="tanggal" type="text" value="" onchange="load_data();">
                                        </div>
                                        <div class="form-group">
                                          <label>Pasar</label>
                                          <select class="form-control" name="id_pasar" id="id_pasar">
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="box-body table-responsive">
                                      <div class="overlay" id="overlay_data_default" style="display:none;"><i class="fa fa-refresh fa-spin"></i></div>
                                      <button type="submit" class="btn btn-primary" id="simpan_data" style="display:none;">Simpan</button>
                                    </div><!-- /.box-body -->
                                    <div class="box-footer">
                                      <table class="table table-bordered" id="form_table" style="display:none;">
                                          <thead>
                                          <tr>
                                              <th style="width: 10px">No</th>
                                              <th>Komoditi</th>
                                              <th>Tanggal</th>
                                              <th>Harga</th>
                                              <th>Satuan</th>
                                              <th>Keterangan</th>
                                          </tr>
                                          </thead>
                                          <input class="form-control" id="jumlah_data" name="jumlah_data" type="hidden" value="">  
                                          <tbody id="form_div">
                                          </tbody>
                                      </table>
                                    </div>
                                  </form>
                                </div><!-- /.box -->
                              </div><!--/.col () -->
                          </div>
                        </div>


<script>
  $(document).ready(function() {
    $('#tanggal, #tanggal_sekarang').inputmask();
    $('#tanggal, #tanggal_sekarang').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
  
</script>

<script>

  function load_data() {
    var tanggal = $('#tanggal').val();
    var id_pasar = $('#id_pasar').val();
          $('#overlay_data_default').show();
          $('#simpan_data').show();
					$('#overlay_form_input').fadeOut(2000);
					$('html, body').animate({
						scrollTop: $('#awal').offset().top
					}, 1000);
        $.ajax({ 
                type: "POST",
                async: true, 
                data: { 
                        tanggal:$("#tanggal").val(),
                        id_pasar:$("#id_pasar").val(),
                      }, 
                dataType: "json",
                url: '<?php echo base_url(); ?>kebutuhan_pokok/show_generate_data/',                    
                    success: function(json) 
                    {
                        var trHTML = '';  
                        $('#form_div').html('');    
                        var j = 1;
                        for (var i = 0; i < json.length; i++) 
                          {  
                            var id_start=    
                            trHTML += '<tr id_ajax_data_generate_id="' + j + '">';
                            trHTML += '<td valign="top">' + j +'<input class="form-control" id="id_harga_kebutuhan_pokok_' + j +'" name="id_harga_kebutuhan_pokok_' + j +'" type="hidden" value="' + json[i].id_harga_kebutuhan_pokok +'"></td>';
                            trHTML += '<td valign="top">' + json[i].judul_komoditi +'</td>';
                            var parent = json[i].parent;
                            trHTML += '<td valign="top">';
                            if(parent == 0)
                            {
                            trHTML += '<input class="form-control" id="tanggal_' + j +'" name="tanggal_' + j +'" type="hidden" value="' + tanggal +'" disabled="">';
                            }
                            else
                            {
                            trHTML += '<input class="form-control" id="tanggal_' + j +'" name="tanggal_' + j +'" type="text" value="' + tanggal +'" disabled="">';
                            }
                            trHTML += '</td>';
                            trHTML += '<td valign="top">';
                            if(parent == 0)
                            {
                            trHTML += '<input class="form-control" id="harga_' + j +'" name="harga_' + j +'" type="hidden" value="-">';
                            }
                            else
                            {
                            trHTML += '<input class="form-control" id="harga_' + j +'" name="harga_' + j +'" type="text" value="' + json[i].harga +'">';
                            }
                            trHTML += '</td>';
                            trHTML += '<td valign="top">';
                            if(parent == 0)
                            {
                            trHTML += '<input class="form-control" id="id_satuan_' + j +'" name="id_satuan_' + j +'" type="hidden" value="-">';
                            }
                            else
                            {
                            trHTML += '<input class="form-control" id="id_satuan_' + j +'" name="id_satuan_' + j +'" type="text" value="'+ json[i].id_satuan +'">';
                            // trHTML += '<input class="form-control" id="id_satuan_' + j +'" name="id_satuan_' + j +'" type="text" value="';if(j==25){trHTML += 'liter'}else if(j==28){trHTML += '400gr'}else if(j==29){trHTML += '400gr'}else if(j==31){trHTML += '385gr/klg'}else if(j==32){trHTML += '385gr/klg'}else if(j==35){trHTML += 'buah'}else if(j==37){trHTML += 'tabung'}else if(j==38){trHTML += 'telur'}else if(j==40){trHTML += 'buah'}else if(j==65){trHTML += 'buah'}else if(j==71){trHTML += 'buah'}else if(j==82){trHTML += 'Zak'}else if(j==83){trHTML += 'Zak'}else if(j==84){trHTML += 'Zak'}else if(j==86){trHTML += 'Batang'}else if(j==87){trHTML += 'Batang'}else if(j==88){trHTML += 'Batang'}else if(j==89){trHTML += 'Batang'}else if(j==91){trHTML += 'Lembar'}else if(j==92){trHTML += 'Lembar'}else if(j==93){trHTML += 'Lembar'}else if(j==96){trHTML += 'Batang'}else if(j==97){trHTML += 'Batang'}else if(j==105){trHTML += 'Zak'}else if(j==106){trHTML += 'Zak'}else if(j==107){trHTML += 'Zak'}else if(j==108){trHTML += 'Zak'}else{trHTML += 'kg'}trHTML += '">';
                            }
                            trHTML += '</td>';
                            trHTML += '<td valign="top">';
                            if(parent == 0)
                            {
                            trHTML += '<input class="form-control" id="keterangan_' + j +'" name="keterangan_' + j +'" type="hidden" value="-">';
                            }
                            else
                            {
                            trHTML += '<input class="form-control" id="keterangan_' + j +'" name="keterangan_' + j +'" type="text" value="' + json[i].keterangan +'">';
                            }
                            trHTML += '</td>';
                            trHTML += '</tr>';
                            j++;
                          }    
                        $('#form_div').append(trHTML);   
                        $('#form_table').show();
                        $('#jumlah_data').val(j-1);
                        $('#form').show();
                        $('#squaresWaveG').fadeOut( "slow" );
                        $('#overlay_data_default').hide();
                    }
                });
  }
</script>   

<script type="text/javascript">
  $(document).ready(function(){
    var options = { 
      complete: function() 
      {   
          //var json = JSON.parse(response.responseText);
          $('#squaresWaveG').show();
          $('#form_div').html('');   
          $('#form_table').hide();
          $('#jumlah_data').val('');
          $('#form').hide();
          $('#simpan_data').hide();
          $('#overlay_data_default').fadeOut('slow');
          load_data();
      },
    };

    document.getElementById('simpan_data').onClick = function() {
      $("#form").submit();
    };
    $("#form").ajaxForm(options);

  });
</script>

<script>
	function load_pasar() {
    $('#overlay_data_default').show();
		$('#id_pasar').html('');
		$.ajax({
			type: "POST",
			async: true,
			dataType: "html",
			url: '<?php echo base_url(); ?>kebutuhan_pokok/pilih_pasar/',
			success: function(html) {
				$('#id_pasar').append(html);
				//$('#overlay_data_default').fadeOut('slow');
        $('#overlay_data_default').hide();
			}
		});
	}
</script> 
<script type="text/javascript">
$(document).ready(function() {
	load_pasar();
	$('#id_pasar').on('change', function(e) {
	$('#overlay_data_default').fadeOut('slow');
    load_data();
	});
});
</script>
