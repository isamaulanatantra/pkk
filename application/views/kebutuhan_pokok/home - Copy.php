<!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Monitoring Operasional
            <small>Disiplin Pegawai</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="<?php echo base_url(); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Disiplin Pegawai</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
                        <div class="row">
                        <!-- left column -->
                        <div class="col-md-12">
                            <!-- general form elements -->
                        <div id="awal"></div>
                            <div class="box box-primary" id="box_form_baru"><br>
                                    <div class="row">
                                    <div class="col-md-9">      
                                    <div class="col-xs-6">  
                                    <div class="col-xs-4">                                    
                                    Pilih Tanggal
                                    </div>
                                    <div class="col-xs-7">
                                    <input class="form-control" id="tanggal" type="text" value="" onchange="load_data();">  
                                    </div>
                                    </div>
                                    <div class="col-xs-6">    
                                    <div class="row">
                                    <button class="btn btn-success" id="generate_form" data-target="awal">Generate <i class="fa fa-plus"></i></button> &nbsp;&nbsp;
                                    <button class="btn btn-warning" id="kurang_form">Batal <i class="fa fa-reply"></i></button>&nbsp;&nbsp;
                                    <button class="btn btn-default" id="refresh">Refresh <i class="fa fa-refresh"></i></button>
                                    </div>
                                    </div>
                                    </div>
                                    </div>
                                <div class="box-header">
                                    <h3 class="box-title">                                  
                                     <div class="error_message_find"></h3>
                                </div><!-- /.box-header -->
                                
                                <!-- form start -->
                                <form role="form" id="form" method="post" style="display:none;"  action="<?php echo base_url(); ?>mos/disiplin_pegawai/update_generate_data" enctype="multipart/form-data">
                                    <div class="box-body">
                                    <table class="table table-bordered" id="form_table" style="display:none;">
                                        <thead>
                                        <tr>
                                            <th style="width: 10px">No</th>
                                            <th>Nama Pegawai</th>
                                            <th>Tanggal</th>
                                            <th>Kehadiran</th>
                                            <th>Keterangan Tambahan</th>
                                        </tr>
                                        </thead>
                                        <input class="form-control" id="jumlah_data" name="jumlah_data" type="hidden" value="">  
                                        <tbody id="form_div">
                                        </tbody>
                                    </table>
                                    </div><!-- /.box-body -->

                                    <div class="box-footer">
                                        <button type="submit" class="btn btn-primary" id="simpan_data" style="display:none;">Simpan</button>
                                    </div>
                                </form>
                                
                            </div><!-- /.box -->
                        </div><!--/.col () -->
                        </div>
                                        
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Data Disiplin Pegawai 
                                        <span id="spinners_data" style="display: none;">
                                            <i class="fa fa-refresh fa-spin" ></i>
                                        </span>
                                    </h3>
                                </div><!-- /.box-header -->
                                <div class="box-body table-responsive" id="grid_utama_by_filter">
                                </div><!-- /.box-body -->
                            </div><!-- /.box -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->       
<script type="text/javascript">
  // When the document is ready
  $(document).ready(function () {
          var today = new Date();
          $('#tanggal').datepicker({
                  format: "yyyy-mm-dd"
          });   
          $('#tanggal').val($.datepicker.formatDate('yy-mm-dd', today));
  });
</script>   

<script>
  $(document).ready(function() {
      load_data();
  });
</script>

<script>
  function load_data() {
      $('#overlay_data_default').show();
      $('#grid_utama_by_filter').html('');
      var temp = $("#temp").val();
      var id_pasar_by_filter = $("#id_pasar_by_filter").val();
      var tanggal = $('#tanggal').val();
      if (id_pasar_by_filter == '') {
          $('#id_pasar_by_filter').css('background-color', '#DFB5B4');
        } else {
          $('#id_pasar_by_filter').removeAttr('style');
        }
      $.ajax({
        type: "POST",
        async: true,
        data: {
          temp:temp,
          id_pasar_by_filter:id_pasar_by_filter,
        },
        dataType: "html",
        url: '<?php echo base_url(); ?>kebutuhan_pokok/html_data/'+tanggal+'',
        success: function(html) {
          $('#div_filter_pasar').hide();
          $('#grid_utama_by_filter').append(html);
          $('#overlay_data_default').fadeOut('slow');
        }
      });
  }
</script>