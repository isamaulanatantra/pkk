<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
      <li><a data-toggle="tab" href="#tab_2" id="klik_tab_tampil" >Tampil</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title" id="judul_formulir">FORMULIR INPUT</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" id="form_baru"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=posting" enctype="multipart/form-data">
                  <div class="box-body">
										<div class="form-group" style="display:none;">
											<label for="temp">temp</label>
											<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="mode">mode</label>
											<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_data_skpd">id_data_skpd</label>
											<input class="form-control" id="id_data_skpd" name="id" value="" placeholder="id_data_skpd" type="text">
										</div>
										<div class="form-group">
											<label for="jabatan_penandatangan">Jabatan Penandatangan</label>
											<input class="form-control" id="jabatan_penandatangan" name="jabatan_penandatangan" value="" placeholder="jabatan_penandatangan" type="text">
										</div>
										<div class="form-group">
											<label for="nama_penandatangan">Nama Penandatangan</label>
											<input class="form-control" id="nama_penandatangan" name="nama_penandatangan" value="" placeholder="nama_penandatangan" type="text">
										</div>
										<div class="form-group">
											<label for="pangkat_penandatangan">Pangkat Penandatangan</label>
											<input class="form-control" id="pangkat_penandatangan" name="pangkat_penandatangan" value="" placeholder="pangkat_penandatangan" type="text">
										</div>
										<div class="form-group">
											<label for="nip_penandatangan">NIP Penandatangan</label>
											<input class="form-control" id="nip_penandatangan" name="nip_penandatangan" value="" placeholder="nip_penandatangan" type="text">
										</div>
										<div class="form-group">
											<label for="skpd_nama">Skpd Nama</label>
											<input class="form-control" id="skpd_nama" name="skpd_nama" value="" placeholder="skpd_nama" type="text">
										</div>
										<div class="form-group">
											<label for="skpd_telpn">skpd telpn</label>
											<input class="form-control" id="skpd_telpn" name="skpd_telpn" value="" placeholder="skpd_telpn" type="text">
										</div>
										<div class="form-group">
											<label for="skpd_alamat">skpd alamat</label>
											<input class="form-control" id="skpd_alamat" name="skpd_alamat" value="" placeholder="skpd_alamat" type="text">
										</div>
										<div class="form-group">
											<label for="skpd_fax">skpd fax</label>
											<input class="form-control" id="skpd_fax" name="skpd_fax" value="" placeholder="skpd_fax" type="text">
										</div>
										<div class="form-group">
											<label for="skpd_kode_pos">skpd kode pos</label>
											<input class="form-control" id="skpd_kode_pos" name="skpd_kode_pos" value="" placeholder="skpd_kode_pos" type="text">
										</div>
										<div class="form-group">
											<label for="skpd_nomor">skpd_nomor</label>
											<input class="form-control" id="skpd_nomor" name="skpd_nomor" value="" placeholder="skpd_nomor" type="text">
										</div>
										<div class="form-group">
											<label for="skpd_website">skpd website</label>
											<input class="form-control" id="skpd_website" name="skpd_website" value="" placeholder="skpd_website" type="text">
										</div>
										<div class="form-group">
											<label for="skpd_email">skpd_email</label>
											<input class="form-control" id="skpd_email" name="skpd_email" value="" placeholder="skpd_email" type="text">
										</div>
										<div class="form-group">
											<label for="id_skpd">id_skpd</label>
											<select class="form-control" id="id_skpd" name="id_skpd" >
                      </select>
										</div>
										<div class="alert alert-info alert-dismissable">
											<div class="form-group">
												<label for="remake">Keterangan Lampiran </label>
												<input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran " type="text">
											</div>
											<div class="form-group">
												<label for="myfile">File Lampiran </label>
												<input type="file" size="60" name="myfile" id="file_lampiran" >
											</div>
											<div id="ProgresUpload">
												<div id="BarProgresUpload"></div>
												<div id="PersenProgresUpload">0%</div >
											</div>
											<div id="PesanProgresUpload"></div>
										</div>
										<div class="alert alert-info alert-dismissable">
											<h3 class="box-title">Data Lampiran </h3>
											<table class="table table-bordered">
												<tr>
													<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
												</tr>
												<tbody id="tbl_attachment_data_skpd">
												</tbody>
											</table>
										</div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="simpan_data_skpd">SIMPAN</button>
                    <button type="submit" class="btn btn-primary" id="update_data_skpd" style="display:none;">UPDATE</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
        
      </div>
      <div class="tab-pane" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                Data Nama SKPD 
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="kata_kunci">
                  <select name="limit_data_data_skpd" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_data_skpd">
                    <option value="10">10 Per-Halaman</option>
                    <option value="20">20 Per-Halaman</option>
                    <option value="50">50 Per-Halaman</option>
                    <option value="999999999">Semua</option>
                  </select>
                  <select name="urut_data_data_skpd" class="form-control input-sm pull-right" style="width: 150px;" id="urut_data_data_skpd">
                    <option value="skpd_nama">Nama Data SKPD</option>
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-body table-responsive no-padding">
                        <div id="tblExport">
                          <table class="table table-hover">
                            <tbody>
                              <tr id="header_exel">
                                <th>NO</th> 
                                <th>Nama SKPD</th>
                                <th>Website</th>
                                <th>Status</th>
                                <th>Proses</th>
                              </tr>
                            </tbody>
                            <tbody id="tbl_utama_data_skpd">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="row">
                        
                        
                        
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!----------------------->
<script>
  function load_data_data_skpd(halaman, limit, kata_kunci, urut_data_data_skpd) {
    $('#tbl_utama_data_skpd').html('');
    $('#spinners_data').show();
    var limit_data_data_skpd = $('#limit_data_data_skpd').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:halaman,
        limit:limit,
        kata_kunci:kata_kunci,
        urut_data_data_skpd:urut_data_data_skpd
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>data_skpd/json_all_data_skpd/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_data_skpd="' + json[i].id_data_skpd + '" id="' + json[i].id_data_skpd + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
          tr += '<td valign="top">' + json[i].skpd_nama + '</td>';          
          tr += '<td valign="top">' + json[i].skpd_website + '</td>';         
          if( json[i].status == 1 ){
            tr += '<td valign="top" id="td_2_'+i+'"><a href="#" id="inaktifkan" ><i class="fa fa-cut"></i> Aktif</a></td>';
          }
          else{
            tr += '<td valign="top" id="td_3_'+i+'"><a href="#" id="aktifkan" ><i class="fa fa-cut"></i> Tidak Aktif</a></td>';
          }
          tr += '<td valign="top"><a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> <a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';          
          tr += '</tr>';
        }
        $('#tbl_utama_data_skpd').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
  }
</script>
<!----------------------->
<script>
  function cek_tema() {
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:'a'
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>data_skpd/cek_tema/',
      success: function(text) {
        
        var halaman = 1;
        var limit_data_data_skpd = $('#limit_data_data_skpd').val();
        var limit = limit_per_page_custome(limit_data_data_skpd);
        var kata_kunci = $('#kata_kunci').val();
        var urut_data_data_skpd = $('#urut_data_data_skpd').val();
        load_data_data_skpd(halaman, limit, kata_kunci, urut_data_data_skpd);
      }
    });
  }
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    cek_tema();
  });
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_skpd').on('click', '#inaktifkan', function() {
    var id_data_skpd = $(this).closest('tr').attr('id_data_skpd');
    alertify.confirm('Anda yakin data akan dipubish?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_data_skpd:id_data_skpd
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>data_skpd/inaktifkan',
          success: function(html) {
            alertify.success('Data berhasil publish');
            var halaman = 1;
            var limit_data_data_skpd = $('#limit_data_data_skpd').val();
            var limit = limit_per_page_custome(limit_data_data_skpd);
            var kata_kunci = $('#kata_kunci').val();
            var urut_data_data_skpd = $('#urut_data_data_skpd').val();
            load_data_data_skpd(halaman, limit, kata_kunci, urut_data_data_skpd);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_skpd').on('click', '#aktifkan', function() {
    var id_data_skpd = $(this).closest('tr').attr('id_data_skpd');
    alertify.confirm('Anda yakin data akan dipubish?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_data_skpd:id_data_skpd
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>data_skpd/aktifkan',
          success: function(html) {
            alertify.success('Data berhasil publish');
            var halaman = 1;
            var limit_data_data_skpd = $('#limit_data_data_skpd').val();
            var limit = limit_per_page_custome(limit_data_data_skpd);
            var kata_kunci = $('#kata_kunci').val();
            var urut_data_data_skpd = $('#urut_data_data_skpd').val();
            load_data_data_skpd(halaman, limit, kata_kunci, urut_data_data_skpd);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#limit_data_data_skpd').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_data_skpd = $('#limit_data_data_skpd').val();
      var limit = limit_per_page_custome(limit_data_data_skpd);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_data_skpd = $('#urut_data_data_skpd').val();
      load_data_data_skpd(halaman, limit, kata_kunci, urut_data_data_skpd);
    });
  });
</script>

<script>
  function load_option_data_skpd() {
    $('#id_skpd').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_skpd/load_the_option/',
      success: function(html) {
        $('#id_skpd').html('<option value="0">Pilih SKPD</option>  '+html+'');
      }
    });
  }
</script>

<script>
  function AfterSavedData_skpd() {
    $('#id_data_skpd, #jabatan_penandatangan, #nama_penandatangan, #pangkat_penandatangan, #nip_penandatangan,  #skpd_nama, #skpd_telpn, #skpd_alamat, #skpd_fax, #skpd_kode_pos, #skpd_nomor, #skpd_website, #skpd_email, #id_skpd').val('');
    $('#tbl_attachment_posting').html('');
    $('#PesanProgresUpload').html('');
    load_option_data_skpd();
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
$(document).ready(function() {
    load_option_data_skpd();
});
</script>
 
<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_data_skpd').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'data_skpd',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_data_skpd').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_posting').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_data_skpd').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_data_skpd').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_skpd').on('click', '#del_ajax', function() {
    var id_data_skpd = $(this).closest('tr').attr('id_data_skpd');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_data_skpd"] = id_posting;
        var url = '<?php echo base_url(); ?>data_skpd/hapus/';
        HapusData(parameter, url);
        $('[id_data_skpd='+id_data_skpd+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_skpd6').on('click', '#restore_ajax', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan direstore?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_data_skpd"] = id_data_skpd;
        var url = '<?php echo base_url(); ?>data_skpd/restore/';
        HapusData(parameter, url);
        $('[id_data_skpd='+id_data_skpd+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_skpd').on('click', '.update_id', function() {
    load_option_data_skpd();
    $('#mode').val('edit');
    $('#simpan_data_skpd').hide();
    $('#update_data_skpd').show();
    var id_data_skpd = $(this).closest('tr').attr('id_data_skpd');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_data_skpd');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_data_skpd').val(id_data_skpd);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_data_skpd:id_data_skpd
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>data_skpd/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#jabatan_penandatangan').val(json[i].jabatan_penandatangan);
            $('#nama_penandatangan').val(json[i].nama_penandatangan);
            $('#pangkat_penandatangan').val(json[i].pangkat_penandatangan);
            $('#nip_penandatangan').val(json[i].nip_penandatangan);
            $('#skpd_nama').val(json[i].skpd_nama);
            $('#skpd_telpn').val(json[i].skpd_telpn);
            $('#skpd_alamat').val(json[i].skpd_alamat);
            $('#skpd_fax').val(json[i].skpd_fax);
            $('#skpd_kode_pos').val(json[i].skpd_kode_pos);
            $('#skpd_nomor').val(json[i].skpd_nomor);
            $('#skpd_website').val(json[i].skpd_website);
            $('#skpd_email').val(json[i].skpd_email);
            $('#id_skpd').val(json[i].id_skpd);
          }
        }
      });
    AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_data_skpd').show();
    $('#update_data_skpd').hide();
    $('#tbl_attachment_data_skpd').html('');
    $('#id_data_skpd, #jabatan_penandatangan, #nama_penandatangan, #pangkat_penandatangan, #nip_penandatangan,  #skpd_nama, #skpd_telpn, #skpd_alamat, #skpd_fax, #skpd_kode_pos, #skpd_nomor, #skpd_website, #skpd_email, #id_skpd').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_data_skpd').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'skpd_nama', 'skpd_website', 'id_skpd' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["jabatan_penandatangan"] = $("#jabatan_penandatangan").val();
      parameter["nama_penandatangan"] = $("#nama_penandatangan").val();
      parameter["pangkat_penandatangan"] = $("#pangkat_penandatangan").val();
      parameter["nip_penandatangan"] = $("#nip_penandatangan").val();
      parameter["skpd_nama"] = $("#skpd_nama").val();
      parameter["skpd_telpn"] = $("#skpd_telpn").val();
      parameter["skpd_alamat"] = $("#skpd_alamat").val();
      parameter["skpd_fax"] = $("#skpd_fax").val();
      parameter["skpd_kode_pos"] = $("#skpd_kode_pos").val();
      parameter["skpd_nomor"] = $("#skpd_nomor").val();
      parameter["skpd_website"] = $("#skpd_website").val();
      parameter["skpd_email"] = $("#skpd_email").val();
      parameter["id_skpd"] = $("#id_skpd").val();
      parameter["temp"] = $("#temp").val();
      var url = '<?php echo base_url(); ?>data_skpd/simpan_data_skpd';
      
      var parameterRv = [ 'skpd_nama', 'skpd_website', 'id_skpd' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedData_skpd();
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_data_skpd').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'jabatan_penandatangan', 'nama_penandatangan', 'pangkat_penandatangan', 'nip_penandatangan', 'skpd_nama', 'skpd_telpn', 'skpd_alamat', 'skpd_fax', 'skpd_kode_pos', 'skpd_nomor', 'skpd_website', 'skpd_email', 'id_skpd' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["jabatan_penandatangan"] = $("#jabatan_penandatangan").val();
      parameter["nama_penandatangan"] = $("#nama_penandatangan").val();
      parameter["pangkat_penandatangan"] = $("#pangkat_penandatangan").val();
      parameter["nip_penandatangan"] = $("#nip_penandatangan").val();
      parameter["skpd_nama"] = $("#skpd_nama").val();
      parameter["skpd_telpn"] = $("#skpd_telpn").val();
      parameter["skpd_alamat"] = $("#skpd_alamat").val();
      parameter["skpd_fax"] = $("#skpd_fax").val();
      parameter["skpd_kode_pos"] = $("#skpd_kode_pos").val();
      parameter["skpd_nomor"] = $("#skpd_nomor").val();
      parameter["skpd_website"] = $("#skpd_website").val();
      parameter["skpd_email"] = $("#skpd_email").val();
      parameter["id_skpd"] = $("#id_skpd").val();
      parameter["temp"] = $("#temp").val();
      parameter["id_data_skpd"] = $("#id_data_skpd").val();
      var url = '<?php echo base_url(); ?>data_skpd/update_data_skpd';
      
      var parameter = [ 'jabatan_penandatangan', 'nama_penandatangan', 'pangkat_penandatangan', 'nip_penandatangan', 'skpd_nama', 'skpd_telpn', 'skpd_alamat', 'skpd_fax', 'skpd_kode_pos', 'skpd_nomor', 'skpd_website', 'skpd_email', 'id_skpd' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>
