<section class="content" id="awal">

						<?php
						if (empty($_GET['thn_pelajaran'])) {
							$thn_pelajaran = "2017/2018";
						}else{
							$thn_pelajaran = $_GET['thn_pelajaran'];
						}
						?>
  <div class="row">
    <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_1">
        
      </div>
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
								Data Sekolah Tahun Pelajaran <?php echo $thn_pelajaran; ?> di Dapodik
              </h3>
              <div class="box-tools">
                <div class="input-group">
				<select name="thn_pelajaran" class="form-control input-sm pull-right" style="width: 180px;" id="thn_pelajaran">
                    <option value="">Tahun Pelajaran</option>
                    <option value="2017/2018">2017/2018</option>
                    <option value="2018/2019">2018/2019</option>
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-xs btn-primary" id="filter"><i class="fa fa-search"></i> Tampil</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-body table-responsive no-padding">
                        <div id="tblExport">
                          <table class="table table-bordered table-hover">
                            <thead>
                              <tr id="header_exel">
								  <tr>
									<th rowspan="2">No</th>
									<th rowspan="2">Wilayah</th>
									<th colspan="3">SD</th>
									<th colspan="3">SMP</th>
									<th colspan="3">SMA</th>
									<th colspan="3">SMK</th>
								  </tr>
								  <tr>
									<td>Negeri</td>
									<td>Swasta</td>
									<td>Jml</td>
									<td>Negeri</td>
									<td>Swasta</td>
									<td>Jml</td>
									<td>Negeri</td>
									<td>Swasta</td>
									<td>Jml</td>
									<td>Negeri</td>
									<td>Swasta</td>
									<td>Jml</td>
								</tr>
									  <tr>
										<td>1</td>
										<td>Wadaslintang</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_01=$w2->num_rows();
												  echo''.$jml_SD_Negeri_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_01=$w2->num_rows();
												  echo''.$jml_SD_Swasta_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_01=$w2->num_rows();
												  echo''.$jml_SD_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_01=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_01=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_01=$w2->num_rows();
												  echo''.$jml_SMP_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_01=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_01=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_01=$w2->num_rows();
												  echo''.$jml_SMA_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_01=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_01=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wadaslintang'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_01=$w2->num_rows();
												  echo''.$jml_SMK_01.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>2</td>
										<td>Kepil</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_02=$w2->num_rows();
												  echo''.$jml_SD_Negeri_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_02=$w2->num_rows();
												  echo''.$jml_SD_Swasta_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_02=$w2->num_rows();
												  echo''.$jml_SD_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_02=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_02=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_02=$w2->num_rows();
												  echo''.$jml_SMP_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_02=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_02=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_02=$w2->num_rows();
												  echo''.$jml_SMA_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_02=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_02=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kepil'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_02=$w2->num_rows();
												  echo''.$jml_SMK_02.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>3</td>
										<td>Sapuran</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_03=$w2->num_rows();
												  echo''.$jml_SD_Negeri_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_03=$w2->num_rows();
												  echo''.$jml_SD_Swasta_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_03=$w2->num_rows();
												  echo''.$jml_SD_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_03=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_03=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_03=$w2->num_rows();
												  echo''.$jml_SMP_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_03=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_03=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_03=$w2->num_rows();
												  echo''.$jml_SMA_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_03=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_03=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sapuran'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_03=$w2->num_rows();
												  echo''.$jml_SMK_03.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>4</td>
										<td>Kaliwiro</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_04=$w2->num_rows();
												  echo''.$jml_SD_Negeri_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_04=$w2->num_rows();
												  echo''.$jml_SD_Swasta_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_04=$w2->num_rows();
												  echo''.$jml_SD_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_04=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_04=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_04=$w2->num_rows();
												  echo''.$jml_SMP_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_04=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_04=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_04=$w2->num_rows();
												  echo''.$jml_SMA_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_04=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_04=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kaliwiro'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_04=$w2->num_rows();
												  echo''.$jml_SMK_04.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>5</td>
										<td>Leksono</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_05=$w2->num_rows();
												  echo''.$jml_SD_Negeri_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_05=$w2->num_rows();
												  echo''.$jml_SD_Swasta_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_05=$w2->num_rows();
												  echo''.$jml_SD_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_05=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_05=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_05=$w2->num_rows();
												  echo''.$jml_SMP_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_05=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_05=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_05=$w2->num_rows();
												  echo''.$jml_SMA_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_05=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_05=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Leksono'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_05=$w2->num_rows();
												  echo''.$jml_SMK_05.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>6</td>
										<td>Selomerto</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_06=$w2->num_rows();
												  echo''.$jml_SD_Negeri_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_06=$w2->num_rows();
												  echo''.$jml_SD_Swasta_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_06=$w2->num_rows();
												  echo''.$jml_SD_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_06=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_06=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_06=$w2->num_rows();
												  echo''.$jml_SMP_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_06=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_06=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_06=$w2->num_rows();
												  echo''.$jml_SMA_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_06=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_06=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Selomerto'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_06=$w2->num_rows();
												  echo''.$jml_SMK_06.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>7</td>
										<td>Kalikajar</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_07=$w2->num_rows();
												  echo''.$jml_SD_Negeri_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_07=$w2->num_rows();
												  echo''.$jml_SD_Swasta_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_07=$w2->num_rows();
												  echo''.$jml_SD_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_07=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_07=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_07=$w2->num_rows();
												  echo''.$jml_SMP_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_07=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_07=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_07=$w2->num_rows();
												  echo''.$jml_SMA_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_07=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_07=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalikajar'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_07=$w2->num_rows();
												  echo''.$jml_SMK_07.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>8</td>
										<td>Kertek</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_08=$w2->num_rows();
												  echo''.$jml_SD_Negeri_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_08=$w2->num_rows();
												  echo''.$jml_SD_Swasta_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_08=$w2->num_rows();
												  echo''.$jml_SD_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_08=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_08=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_08=$w2->num_rows();
												  echo''.$jml_SMP_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_08=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_08=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_08=$w2->num_rows();
												  echo''.$jml_SMA_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_08=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_08=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kertek'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_08=$w2->num_rows();
												  echo''.$jml_SMK_08.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>9</td>
										<td>Wonosobo</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_09=$w2->num_rows();
												  echo''.$jml_SD_Negeri_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_09=$w2->num_rows();
												  echo''.$jml_SD_Swasta_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_09=$w2->num_rows();
												  echo''.$jml_SD_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_09=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_09=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_09=$w2->num_rows();
												  echo''.$jml_SMP_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_09=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_09=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_09=$w2->num_rows();
												  echo''.$jml_SMA_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_09=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_09=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Wonosobo'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_09=$w2->num_rows();
												  echo''.$jml_SMK_09.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>10</td>
										<td>Watumalang</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_10=$w2->num_rows();
												  echo''.$jml_SD_Negeri_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_10=$w2->num_rows();
												  echo''.$jml_SD_Swasta_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_10=$w2->num_rows();
												  echo''.$jml_SD_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_10=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_10=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_10=$w2->num_rows();
												  echo''.$jml_SMP_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_10=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_10=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_10=$w2->num_rows();
												  echo''.$jml_SMA_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_10=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_10=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Watumalang'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_10=$w2->num_rows();
												  echo''.$jml_SMK_10.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>11</td>
										<td>Mojotengah</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_11=$w2->num_rows();
												  echo''.$jml_SD_Negeri_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_11=$w2->num_rows();
												  echo''.$jml_SD_Swasta_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_11=$w2->num_rows();
												  echo''.$jml_SD_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_11=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_11=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_11=$w2->num_rows();
												  echo''.$jml_SMP_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_11=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_11=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_11=$w2->num_rows();
												  echo''.$jml_SMA_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_11=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_11=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Mojotengah'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_11=$w2->num_rows();
												  echo''.$jml_SMK_11.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>12</td>
										<td>Garung</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_12=$w2->num_rows();
												  echo''.$jml_SD_Negeri_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_12=$w2->num_rows();
												  echo''.$jml_SD_Swasta_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_12=$w2->num_rows();
												  echo''.$jml_SD_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_12=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_12=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_12=$w2->num_rows();
												  echo''.$jml_SMP_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_12=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_12=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_12=$w2->num_rows();
												  echo''.$jml_SMA_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_12=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_12=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Garung'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_12=$w2->num_rows();
												  echo''.$jml_SMK_12.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>13</td>
										<td>Kejajar</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_13=$w2->num_rows();
												  echo''.$jml_SD_Negeri_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_13=$w2->num_rows();
												  echo''.$jml_SD_Swasta_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_13=$w2->num_rows();
												  echo''.$jml_SD_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_13=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_13=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_13=$w2->num_rows();
												  echo''.$jml_SMP_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_13=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_13=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_13=$w2->num_rows();
												  echo''.$jml_SMA_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_13=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_13=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kejajar'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_13=$w2->num_rows();
												  echo''.$jml_SMK_13.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>14</td>
										<td>Sukoharjo</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_14=$w2->num_rows();
												  echo''.$jml_SD_Negeri_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_14=$w2->num_rows();
												  echo''.$jml_SD_Swasta_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_14=$w2->num_rows();
												  echo''.$jml_SD_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_14=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_14=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_14=$w2->num_rows();
												  echo''.$jml_SMP_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_14=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_14=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_14=$w2->num_rows();
												  echo''.$jml_SMA_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_14=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_14=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Sukoharjo'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_14=$w2->num_rows();
												  echo''.$jml_SMK_14.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>15</td>
										<td>Kalibawang</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SD'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Negeri_15=$w2->num_rows();
												  echo''.$jml_SD_Negeri_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SD'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_Swasta_15=$w2->num_rows();
												  echo''.$jml_SD_Swasta_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SD'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SD_15=$w2->num_rows();
												  echo''.$jml_SD_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SMP'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Negeri_15=$w2->num_rows();
												  echo''.$jml_SMP_Negeri_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SMP'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_Swasta_15=$w2->num_rows();
												  echo''.$jml_SMP_Swasta_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SMP'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMP_15=$w2->num_rows();
												  echo''.$jml_SMP_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SMA'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Negeri_15=$w2->num_rows();
												  echo''.$jml_SMA_Negeri_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SMA'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_Swasta_15=$w2->num_rows();
												  echo''.$jml_SMA_Swasta_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SMA'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMA_15=$w2->num_rows();
												  echo''.$jml_SMA_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SMK'
											  and status_sekolah='Negeri'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Negeri_15=$w2->num_rows();
												  echo''.$jml_SMK_Negeri_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SMK'
											  and status_sekolah='Swasta'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_Swasta_15=$w2->num_rows();
												  echo''.$jml_SMK_Swasta_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where kec_='Kec. Kalibawang'
											  and jenjang='SMK'
											  and thn_pelajaran = '$thn_pelajaran'
											  ");
											  $jml_SMK_15=$w2->num_rows();
												  echo''.$jml_SMK_15.'';
											?>
										</td>
									  </tr>
                            </thead>
                            <tbody id="tbl_utama_daftar_informasi_publik_pembantu">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="row">
                        
                        
                        
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1;
		var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
		var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
		var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
    load_data_daftar_informasi_publik_pembantu(halaman, limit, klasifikasi_informasi_publik_pembantu);
});
</script>

<script>
  function load_data_daftar_informasi_publik_pembantu(halaman, limit, klasifikasi_informasi_publik_pembantu) {
    $('#tbl_utama_daftar_informasi_publik_pembantu').html('');
    $('#spinners_data').show();
		var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        klasifikasi_informasi_publik_pembantu: klasifikasi_informasi_publik_pembantu,
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_dikpora/load_table/',
      success: function(html) {
        $('#tbl_utama_daftar_informasi_publik_pembantu').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilkan_daftar_informasi_publik').on('click', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, klasifikasi_informasi_publik_pembantu);
    });
  });
</script>

<script type="text/javascript">
  $(function () {
    $('#konten_posting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

	$('#filter').on('click', function() {
	    var thn_pelajaran = $('#thn_pelajaran').val();
	    window.location.replace("https://dikpora.wonosobokab.go.id/postings/detail/1031582/Data_Sekolah.HTML?thn_pelajaran="+thn_pelajaran+"");
	});

</script>
