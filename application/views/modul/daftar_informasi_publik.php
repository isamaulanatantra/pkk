<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_1">
        
      </div>
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                DAFTAR INFORMASI PUBLIK
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="display:none; width: 150px;" placeholder="Search" type="text" id="kata_kunci">
                  <select name="limit_data_daftar_informasi_publik_pembantu" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_daftar_informasi_publik_pembantu">
                    <option value="999999999">Semua</option>
                    <option value="10">10 Per-Halaman</option>
                    <option value="20">20 Per-Halaman</option>
                    <option value="50">50 Per-Halaman</option>
                  </select>
                  <select name="urut_data_daftar_informasi_publik_pembantu" class="form-control input-sm pull-right" id="urut_data_daftar_informasi_publik_pembantu" style="display:none;">
                    <option value="judul_posting">Berdasarkan Ringkasan Informasi</option>
                  </select>
                  <select name="klasifikasi_informasi_publik_pembantu" class="form-control input-sm pull-right" style="width: 180px;" id="klasifikasi_informasi_publik_pembantu">
                    <option value="informasi_setiap_saat">Informasi Setiap Saat</option>
                    <option value="informasi_serta_merta">Informasi Serta Merta</option>
                    <option value="informasi_berkala">Informasi Berkala</option>
                    <option value="informasi_dikecualikan">Informasi Dikecualikan</option>
                  </select>
                  <select name="opd_domain" class="form-control input-sm pull-right" style="display:none;" id="opd_domain">
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-info" id="tampilkan_daftar_informasi_publik"><i class="fa fa-search"></i> Tampilkan</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-body table-responsive no-padding">
                        <div>
                          <table class="table table-bordered table-hover">
                            <thead>
                              <tr id="header_exel">
                                <th>NO</th>
                                <th style="width: 400px;">Ringkasan Informasi Publik</th>
                                <!--<th>Kategori</th>-->
                                <th>Update</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody id="tbl_utama_daftar_informasi_publik_pembantu">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="row">
                        
                        
                        
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1; 
		var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
    load_data_daftar_informasi_publik_pembantu(halaman, klasifikasi_informasi_publik_pembantu);
});
</script>

<script>
  function load_data_daftar_informasi_publik_pembantu(halaman, klasifikasi_informasi_publik_pembantu) {
    $('#tbl_utama_daftar_informasi_publik_pembantu').html('');
    $('#spinners_data').show();
		var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        klasifikasi_informasi_publik_pembantu: klasifikasi_informasi_publik_pembantu,
        halaman: halaman
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/load_table_daftar_informasi_publik_pembantu/',
      success: function(html) {
        $('#tbl_utama_daftar_informasi_publik_pembantu').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilkan_daftar_informasi_publik').on('click', function(e) {
      e.preventDefault();
      var halaman = 1;
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      load_data_daftar_informasi_publik_pembantu(halaman, klasifikasi_informasi_publik_pembantu);
    });
  });
</script>
