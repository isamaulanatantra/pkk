<style>
.datepicker{z-index:9999 !important}
</style>
<style>
#myProgress {
  position: relative;
  width: 100%;
  height: 5px;
  background-color: #ddd;
}

#myBar {
  position: absolute;
  width: 1%;
  height: 100%;
  background-color: #8B008B;
}
</style>
<form role="form" id="form_pertanyaan" method="post" action="" enctype="multipart/form-data ">
  <div class="modal fade" id="FormReviewer" tabindex="-1" role="dialog" aria-labelledby="myReviewer" aria-hidden="true" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myReviewer">Silahkan tentukan periode yang ingin anda tampilkan</h4>
        </div>
        <div class="modal-body">
          
            <div class="box-body">
              <div class="form-group">
                <label for="dari_tanggal">Dari Tanggal</label>
                <input type="hidden" name="periodik" id="periodik" value="periodik">
                <input class="form-control" placeholder="Dari Tanggal" name="dari_tanggal" value="<?php echo $dari_tanggal; ?>" id="dari_tanggal" type="text">
              </div>
              <div class="form-group">
                <label for="sampai_tanggal">Sampai Tanggal</label>
                <input class="form-control" placeholder="Sampai Tanggal" name="sampai_tanggal" value="<?php echo $sampai_tanggal; ?>" id="sampai_tanggal" type="text">
              </div>
              <div class="form-group">
                <label for="id_puskesmas">Puskesmas</label>
                <select class="form-control" name="id_puskesmas" >
                <?php echo $option; ?>
                </select>
              </div>
            </div>
        </div>
        
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="tampilkan"><i id="overlay" style="display:none;" class="fa fa-refresh fa-spin"></i> Tampilkan</button>
        </div>
      </div>
    </div>
  </div>
</form>

<div class="row" id="atas">
	<div class="box box-success">
		<div class="col-xs-12">
      <div class="box-header with-border">
				<center>
        <h3 class="box-title">GRAFIK 10 BESAR PENYAKIT</h3> <br />
				<h5 class="box-title">Seluruh Puskesmas di Kabupaten Wonosobo</h5> <br />
				<!-- <h3 class="box-title"><?php echo $periode;  ?></h3> <br /> -->
        <a href="#" id="pop_reviewer" data-toggle="modal" data-target="#FormReviewer" class="btn btn-success" ><i class="fa fa-fw fa-calendar"></i> Pilih Periode Lain</a>
        </center>
			</div>
			<div class="box-body">
				<div class="col-xs-3">
					<div id="canvas-holder">
						<canvas id="chart-area" width="100" height="100"/>
					</div>
				</div>
				<div class="col-xs-9">
					<table class="table table-hover">
						<tbody>
							<tr>
							<th>Nama Penyakit</th>
							<th>Jumlah</th>
						</tr>
						<?php
						echo $tabel;
						?>
            </tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo base_url(); ?>assets/src/Chart.Core.js"></script>
<script src="<?php echo base_url(); ?>assets/src/Chart.Doughnut.js"></script>	

<script>
function move() {
  var elem = document.getElementById("myBar");   
  var width = 1;
  var id = setInterval(frame, 50);
  function frame() {
    if (width >= 50) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
    }
  }
}
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#dari_tanggal, #sampai_tanggal').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>

<script type="text/javascript">
	$(document).ready(function () {
		move()
		$('html, body').animate({
				scrollTop: $('#atas').offset().top
		}, 'slow');
});
</script>

<script>
  var doughnutData = [
      <?php echo $d; ?>
    ];

    window.onload = function(){
      var ctx = document.getElementById("chart-area").getContext("2d");
      window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});
    };
</script>