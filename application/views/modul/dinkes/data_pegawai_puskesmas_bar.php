<style>
.datepicker{z-index:9999 !important}
</style>
<style>
#myProgress {
  position: relative;
  width: 100%;
  height: 5px;
  background-color: #ddd;
}

#myBar {
  position: absolute;
  width: 1%;
  height: 100%;
  background-color: #8B008B;
}
</style>

<form role="form" id="form_pertanyaan" method="post" action="" enctype="multipart/form-data ">
  <div class="modal fade" id="FormReviewer" tabindex="-1" role="dialog" aria-labelledby="myReviewer" aria-hidden="true" >
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myReviewer">Silahkan pilih puskesmas</h4>
        </div>
        <div class="modal-body">
                     
              <div class="form-group">
                <label for="id_puskesmas">Puskesmas</label>
                <select class="form-control" name="id_puskesmas" >
                <?php echo $option; ?>
                </select>
              </div>
              <div class="form-group">
                   <center>
                    <button type="submit" class="btn btn-primary" id="tampilkan"><i id="overlay" style="display:none;" class="fa fa-refresh fa-spin"></i> Tampilkan Data</button>
                  </center>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</form>

<div class="pad" id="atas">
    <div class="row">
    <div class="col-xs-12">
        <center>
        <h3 class="box-title">GRAFIK DATA PEGAWAI PUSKESMAS</h3> <br />
        <div class="col-md-2 col-md-offset-4">
            <a href="#" id="pop_reviewer" data-toggle="modal" data-target="#FormReviewer" class="btn btn-primary" ><i class="glyphicon glyphicon-align-center"></i> Pilih Puskesmas</a>  
        </div>
        <div class="col-md-2">
            <a href="https://dinkes.wonosobokab.go.id/sdmk/" id="" class="btn btn-danger" target="blank" ><i class="glyphicon glyphicon-log-in"></i> Login Admin</a>  
        </div>
        
        <br>
        </center>
      <br>
        <div class="col-xs-4">
          <center>
            <button class="btn btn-success" id="btn_lihat">
              <i class="glyphicon glyphicon-list"></i> Lihat Data Pegawai
            </button>
          </center>
          <br>
          <table class="table table-bordered" >
            <tbody>
              <tr>
              <th class="text-center">No</th>
              <th class="text-center">Jenis Tenaga</th>
              <th class="text-center">Jumlah</th>
            </tr>
            <?php
            echo $tabel;
            ?>
            </tbody>
          </table>
        </div>
        <div class="col-xs-8">
          <div id="canvas-holder">
            <div id="graph"></div>
          </div>
        </div> 
    </div>
  </div>
</div>

<div id="tabel_pegawai" class="pad" style="display: none;">
    <div class="box-body">        
        <div class="col-xs-12">
          <div class="col-xs-3 col-xs-offset-9">
          <p class="text-rigth">            
              <a href="#" id="btnExport" class="btn btn-success" ><i class="glyphicon glyphicon-download-alt"></i> Export Excel</a>
              <a href="#" id="btn_close" class="btn btn-danger" ><i class="glyphicon glyphicon-remove"></i> Tutup</a>
          </p>
          </div>
          <table class="table table-bordered" id="tabelPegawaiExpost">
            <tbody>
              <tr>
              <th class="text-center">No</th>
              <th class="text-center">Nama Instansi</th>
              <th class="text-center">NIP/NI BULD</th>
              <th class="text-center">Nama Pegawai</th>
              <th class="text-center">Jenis Tenaga</th>
              <th class="text-center">Status</th>
            </tr>
            <?php
            echo $tabelPegawai;
            ?>
            </tbody>
          </table>
        </div>
      </div>
</div>

<script src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/raphael-min.js'?>"></script>
<script src="<?php echo base_url().'assets/js/morris.min.js'?>"></script>
<link rel="stylesheet" href="<?php echo base_url().'assets/css/morris.css'?>">

<script>
    Morris.Bar({
      element: 'graph',
      data: <?php echo $dataBar;?>,
      xkey: 'msPegawaiJabatan',
      ykeys: ['total'],
      labels: ['Jumlah '],
      barColors: function (row, series, type) {
        if (type === 'bar') {
          var red = Math.ceil(255 * row.y / this.ymax);
          return 'rgb(' + red + ',0,0)';
        }
        else {
          return '#000';
        }
      }
    });
</script>

<script>
  $('#btn_lihat').on('click', function(){     
      $("#atas").hide();
      $("#tabel_pegawai").show();
  });

  $('#btn_close').on('click', function(){
      $("#atas").show();
      $("#tabel_pegawai").hide();
  });
</script>

<script src="https://rawgit.com/RobinHerbots/Inputmask/4.x/dist/jquery.inputmask.bundle.js"></script>
<!----------------------->
<script src="<?php echo base_url(); ?>jquery.js"></script>
<script src="<?php echo base_url(); ?>js_exel/jquery.btechco.excelexport.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.base64.js"></script>
<script>
  (function($) {
    $(document).ready(function() { 
    $("#btnExport").click(function (e) {
        e.preventDefault();
          $("#tblExport").btechco_excelexport({
              containerid: "tabelPegawaiExpost"
              , datatype: $datatype.Table
          });
      });
    });
  })(jQuery);
</script>