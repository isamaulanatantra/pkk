<style>
.datepicker{z-index:9999 !important}
</style>
<style>
#myProgress {
  position: relative;
  width: 100%;
  height: 5px;
  background-color: #ddd;
}

#myBar {
  position: absolute;
  width: 1%;
  height: 100%;
  background-color: #8B008B;
}
</style>

<div class="row" id="atas">
  <div class="box box-success">
   <div class="row">
    <div class="col-xs-12">
      <div class="box-header with-border">
        <center>
        <h3 class="box-title">GRAFIK DATA PEGAWAI DINAS KESEHATAN PER BIDANG</h3> <br />
        <!-- <h5 class="box-title">Dan Seluruh Puskesmas di Kabupaten Wonosobo</h5> <br /> -->
        <!-- <h3 class="box-title"><?php echo $periode;  ?></h3> <br /> -->
        <!-- <a href="#" id="pop_reviewer" data-toggle="modal" data-target="#FormReviewer" class="btn btn-success" ><i class="fa fa-fw fa-calendar"></i> Pilih Periode Lain</a> -->
        </center>
      </div>
      <div class="box-body">
        <div class="col-xs-6 col-xs-offset-3">
          <table class="table table-bordered">
            <tbody>
              <tr>
              <th class="text-center">No</th>
              <th class="text-center">Nama Bidang</th>
              <th class="text-center">Jumlah</th>
            </tr>
            <?php
            echo $tabel;
            ?>
            </tbody>
          </table>
        </div>
        <div class="col-xs-12">
          <div id="canvas-holder">
            <!-- <canvas id="chart-area" width="100" height="100"/> -->
            <div id="graph"></div>
          </div>
        </div>
        
      </div>
    </div>
    </div> 

    <div class="row">
    <div class="col-xs-12">
      <div class="box-header with-border">
        <center>
        <h3 class="box-title"><br><br>GRAFIK DATA PEGAWAI DINAS KESEHATAN BERDASARKAN PENDIDIKAN</h3> <br />
        <!-- <h5 class="box-title">Dan Seluruh Puskesmas di Kabupaten Wonosobo</h5> <br /> -->
        <!-- <h3 class="box-title"><?php echo $periode;  ?></h3> <br /> -->
        <!-- <a href="#" id="pop_reviewer" data-toggle="modal" data-target="#FormReviewer" class="btn btn-success" ><i class="fa fa-fw fa-calendar"></i> Pilih Periode Lain</a> -->
        </center>
      </div>
      <div class="box-body">
        <div class="col-xs-6 col-xs-offset-3">
          <table class="table table-bordered">
            <tbody>
              <tr>
              <th class="text-center">No</th>
              <th class="text-center">Pendidikan</th>
              <th class="text-center">Jumlah</th>
            </tr>
            <?php
            echo $tabelPendidikan;
            ?>
            </tbody>
          </table>
        </div>
        <div class="col-xs-12">
          <div id="canvas-holder">
            <!-- <canvas id="chart-area" width="100" height="100"/> -->
            <div id="graph_pendidikan"></div>
          </div>
        </div>
        
      </div>
    </div>
    </div> 

  </div>
</div>

<script src="<?php echo base_url().'assets/js/jquery.min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/raphael-min.js'?>"></script>
    <script src="<?php echo base_url().'assets/js/morris.min.js'?>"></script>
    <link rel="stylesheet" href="<?php echo base_url().'assets/css/morris.css'?>">

<!-- <script>
function move() {
  var elem = document.getElementById("myBar");   
  var width = 1;
  var id = setInterval(frame, 50);
  function frame() {
    if (width >= 50) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = width + '%'; 
    }
  }
}
</script> -->

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#dari_tanggal, #sampai_tanggal').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>

<!-- <script type="text/javascript">
  $(document).ready(function () {
    move()
    $('html, body').animate({
        scrollTop: $('#atas').offset().top
    }, 'slow');
});
</script> -->

<script>
        Morris.Bar({
          element: 'graph',
          data: <?php echo $dataBar;?>,
          xkey: 'msPegawaiUnitKerja',
          ykeys: ['total'],
          labels: ['Jumlah '],
          barColors: function (row, series, type) {
            if (type === 'bar') {
              var red = Math.ceil(255 * row.y / this.ymax);
              return 'rgb(' + red + ',0,0)';
            }
            else {
              return '#000';
            }
          }
        });
    </script>

<script>
        Morris.Bar({
          element: 'graph_pendidikan',
          data: <?php echo $dataPendidikan;?>,
          xkey: 'msPegawaiPendidikan',
          ykeys: ['total'],
          labels: ['Jumlah '],
          barColors: function (row, series, type) {
            if (type === 'bar') {
              var red = Math.ceil(255 * row.y / this.ymax);
              return 'rgb(' + red + ',0,0)';
            }
            else {
              return '#000';
            }
          }
        });
    </script>