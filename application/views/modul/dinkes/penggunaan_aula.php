<div id="myModal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-backdrop="false">
  <div class="modal-dialog" style="overflow-y: scroll; max-height:85%;  margin-top: 50px; margin-bottom:50px; width:90%;">

    <!-- Modal content-->
    <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>perijinan_penggunaan_aula/simpan_booking_aula">
    <div class="modal-content">
        <div class="modal-header">
            <div class="row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4>Form Booking Aula</h4>

                    <div class="form-group">
                       <label for="exampleInputEmail1">Tanggal Penggunaan : *format sama (TTTT-BB-HH)</label>
                       <input type="text" name="tanggal_penggunaan" class="form-control" placeholder="Tanggal Penggunaan" value="<?php echo(date('Y-m-d')); ?>" required>
                    </div>
                     <div class="form-group">
                      <label for="tempat">Pilih Tempat</label>
                      <!-- <input class="form-control" id="perihal_surat" name="perihal_surat" value="" placeholder="perihal_surat" type="text"> -->
                      <select class="form-control" id="tempat" name="tempat" required >
                        <option value="1">Aula Utama</option>
                        <option value="2">Aula Rapat</option>
                      </select>
                    </div>
                    <div class="form-group">
                       <label for="exampleInputEmail1">Nama Kegiatan :</label>
                       <input type="text" name="nama_kegiatan" class="form-control" placeholder="Nama Kegiatan" required>
                    </div>
                    <!-- <div class="form-group">
                       <label for="exampleInputEmail1">Seksi Kegiatan :</label>
                       <input type="text" name="seksi_penggunaan" class="form-control" placeholder="Seksi Kegiatan" required>
                    </div> -->
                    <div class="form-group">
                      <label for="exampleInputEmail1">Seksi Kegiatan :</label>
                      <!-- <input class="form-control" id="perihal_surat" name="perihal_surat" value="" placeholder="perihal_surat" type="text"> -->
                      <select class="form-control" id="seksi_penggunaan" name="seksi_penggunaan" required >
                        <option value="1">Bidang P2P</option>
                        <option value="2">Bidang Yan SDK</option>
                        <option value="3">Bidang Kesmas</option>
                        <option value="4">Bidang Sekretariat</option>
                      </select>
                    </div>

                    <div class="form-group">
                       <label for="exampleInputEmail1">Penanggung Jawab :</label>
                       <input type="text" name="penanggung_jawab" class="form-control" placeholder="Penanggung Jawab" required>
                    </div>                     
                    <div class="form-group">
                       <label for="exampleInputEmail1">Jumlah Peserta :</label>
                       <input type="text" name="jumlah_peserta" class="form-control" placeholder="Jumlah Peserta" required>
                    </div>                     
                    <div class="form-group">
                       <label for="exampleInputEmail1">Sasaran :</label>
                       <input type="text" name="sasaran" class="form-control" placeholder="Sasaran" required>
                    </div>                     
                </div>
                 <div class="modal-footer">                                            
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" id="btnSimpan" name="btnSimpan" class="btn btn-success">Simpan</button>
                 </div>
                 <div id="pesan"></div>
            </div>
        </div>
      </div>
    </form>

      

  </div>
</div>

<section class="content" id="awal">
  <div class="pad">
          <!-- <button class="btn btn-success" data-toggle="modal" data-target="#myModal">Booking Aula</button> -->
          <button class="btn btn-success" id="tambah" name="tambah">Booking Aula</button>
          <button class="btn btn-warning" id="cetak" name="cetak">Cetak Jadwal</button>
          <table class="table table-hover" id="tbl_jadwal" name="tbl_jadwal">
            <thead>
              <tr>
                <th>NO</th>
                <th>TANGGAL</th>
                <th>TEMPAT KEGIATAN</th>
                <th>NAMA KEGIATAN</th>
                <th>SEKSI</th>
                <th>PENANGGUNG JAWAB</th>
                <th>JUMLAH PESERTA</th>
                <th>SASARAN</th>
                <th>KETERANGAN</th>
                <th>TANGGAL DAFTAR</th>
              </tr>
            </thead>
            <tbody id="tbl_utama_perijinan_penggunaan_aula">
            </tbody>
          </table>
  </div>

  <div class="pad" name="fomrtambah" id="fomrtambah" style="display: none;"> 
      <!-- <form role="form" id="formisian2" method="post" action="<?php echo base_url(); ?>perijinan_penggunaan_aula/simpan_booking_aula"> -->
        <form role="form" id="formisian2" method="post">
          <div class="form-group">
                       <label for="exampleInputEmail1">Tanggal Penggunaan : (Maksimal 30 hari ke depan)</label>
                       <input type="date" id="tanggal_penggunaan" name="tanggal_penggunaan" class="form-control" placeholder="Tanggal Penggunaan" value="<?php echo(date('Y-m-d')); ?>" required onchange="myFunction()">
                    </div>

                    <div class="form-group">
                       <label for="exampleInputEmail1">Tanggal Selisih :</label>
                       <input type="text" id="tanggal_selisih" name="tanggal_selisih" class="form-control" style="display: none;" placeholder="Tanggal Penggunaan" value="0" required>
                    </div>

                     <div class="form-group">
                      <label for="tempat">Pilih Tempat</label>
                      <!-- <input class="form-control" id="perihal_surat" name="perihal_surat" value="" placeholder="perihal_surat" type="text"> -->
                      <select class="form-control" id="tempat" name="tempat" required >
                        <option value="1">Aula Utama</option>
                        <option value="2">Aula Rapat</option>
                      </select>
                    </div>
                    <div class="form-group">
                       <label for="exampleInputEmail1">Nama Kegiatan :</label>
                       <input type="text" name="nama_kegiatan" class="form-control" placeholder="Nama Kegiatan" required>
                    </div>
                    <!-- <div class="form-group">
                       <label for="exampleInputEmail1">Seksi Kegiatan :</label>
                       <input type="text" name="seksi_penggunaan" class="form-control" placeholder="Seksi Kegiatan" required>
                    </div> -->
                    <div class="form-group">
                      <label for="exampleInputEmail1">Seksi Kegiatan :</label>
                      <!-- <input class="form-control" id="perihal_surat" name="perihal_surat" value="" placeholder="perihal_surat" type="text"> -->
                      <select class="form-control" id="seksi_penggunaan" name="seksi_penggunaan" required >
                        <option value="1">Bidang P2P</option>
                        <option value="2">Bidang Yan SDK</option>
                        <option value="3">Bidang Kesmas</option>
                        <option value="4">Bidang Sekretariat</option>
                      </select>
                    </div>
                    <div class="form-group">
                       <label for="exampleInputEmail1">Penanggung Jawab :</label>
                       <input type="text" name="penanggung_jawab" class="form-control" placeholder="Penanggung Jawab" required>
                    </div>                     
                    <div class="form-group">
                       <label for="exampleInputEmail1">Jumlah Peserta :</label>
                       <input type="text" name="jumlah_peserta" class="form-control" placeholder="Jumlah Peserta" required>
                    </div>                     
                    <div class="form-group">
                       <label for="exampleInputEmail1">Sasaran :</label>
                       <input type="text" name="sasaran" class="form-control" placeholder="Sasaran" required>
                    </div>                     
                
                 <div class="modal-footer">                                                                  
                      <button type="submit" id="btnSimpan2" name="btnSimpan2" class="btn btn-success">Simpan</button>
                      <button type="button" class="btn btn-danger" name="batal" id="batal" data-dismiss="modal">Batal</button>
                 </div>
      </form>
  </div>

  <div class="pad" name="fomrcetak" id="fomrcetak" style="display: none;">       
        <div class="form-group">
           <label for="exampleInputEmail1">Mulai Tanggal</label>
           <input type="date" id="tanggal_awal" name="tanggal_awal" class="form-control" placeholder="Tanggal Penggunaan" value="<?php echo(date('Y-m-d')); ?>" required >
        </div>

        <div class="form-group">
           <label for="exampleInputEmail1">Sampai Tanggal</label>
           <input type="date" id="tanggal_akhir" name="tanggal_akhir" class="form-control" placeholder="Tanggal Penggunaan" value="<?php echo(date('Y-m-d')); ?>" required >
        </div>                 
    
       <div class="modal-footer">                                      
          <button type="submit" id="btnCetak" name="btnCetak" class="btn btn-success">Cetak</button>
          <button type="button" class="btn btn-danger" name="batalCetak" id="batalCetak" data-dismiss="modal">Batal</button>
       </div>
  </div>

  <div id="pesan2"></div>
</section>

<!-- <script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_penggunaan').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script> -->


<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_table();
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#klik_tab_tampil').on('change', function(e) {
      e.preventDefault();
      load_table();
    });
  });
</script>

<script>
  function load_table() {
    $('#tbl_utama_perijinan_penggunaan_aula').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>perijinan_penggunaan_aula/load_table_perijinan_penggunaan_aula/',
      success: function(html) {
        $('#tbl_utama_perijinan_penggunaan_aula').html(''+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#form_baru').on('click', function(e) {
    $('#simpan_booking_aula').show();
    //$('#update_perijinan_penggunaan_aula').hide();
    //$('#tbl_attachment_perijinan_penggunaan_aula').html('');
    $('#tanggal_penggunaan, #nama_kegiatan, #seksi_penggunaan, #penanggung_jawab').val('');   
  });
});
</script>

<script>
    $("#form_isian").submit(function(e) {
        e.preventDefault();
        
        var dataform = $("#form_isian").serialize();
        $.ajax({
            url: '<?php echo base_url(); ?>perijinan_penggunaan_aula/simpan_booking_aula',
            type: "post",
            data: dataform,
            success: function(result) {
                var hasil = JSON.parse(result);
                if (hasil.hasil == "dobel") {   
                    $("#pesan").show();                 
                    $("#pesan").html("<div class=\"alert alert-danger\">Tanggal sudah di pakai</div>");
                    $('#pesan').animate({
                        opacity: 1,
                        right: "50px",
                        bottom: "10px",
                        height: "toggle"
                    }, 2000, function() {}).css('position','fixed');                    
                }else if (hasil.hasil == "sukses") {   
                    $("#pesan").show();                 
                    $("#pesan").html("<div class=\"alert alert-success\">Data berhasil di simpan</div>");
                    
                    $("#nama_kegiatan").val('');
                    $("#seksi_penggunaan").val('');
                    $("#penanggung_jawab").val('');
                    $('#pesan').animate({
                        opacity: 1,
                        right: "50px",
                        bottom: "10px",
                        height: "toggle"
                    }, 2000, function() {}).css('position','fixed');    
                }
            }
        });
    });
</script>

<script>
  $("#tambah").on("click", function(e){
      $("#tbl_jadwal").hide();
      $("#fomrtambah").show();

  });

  $("#batal").on("click", function(e){
      $("#tbl_jadwal").show();
      $("#fomrtambah").hide();

  });
</script>

<script>
  $("#cetak").on("click", function(e){
      $("#tbl_jadwal").hide();
      $("#fomrcetak").show();

  });

  $("#batalCetak").on("click", function(e){
      $("#tbl_jadwal").show();
      $("#fomrcetak").hide();

  });
</script>

<script>
  $('#btnCetak').on("click", function(e){
      var awal = document.getElementById('tanggal_awal').value;
      var akhir = document.getElementById('tanggal_akhir').value;

      window.open("https://dinkes.wonosobokab.go.id/Cetakjadwal?awal="+awal+"&akhir="+akhir,'_blank');
  });
</script>

<script>
    $("#formisian2").submit(function(e) {
        e.preventDefault();
        
        var dataform = $("#formisian2").serialize();
        $.ajax({
            url: '<?php echo base_url(); ?>perijinan_penggunaan_aula/simpan_booking_aula',
            type: "post",
            data: dataform,
            success: function(result) {
                var hasil = JSON.parse(result);
                if (hasil.hasil == "dobel") {   
                    $("#pesan2").show();                 
                    $("#pesan2").html("<div class=\"alert alert-danger\">Tanggal penggunaan sudah ada acara</div>");
                    $('#pesan2').animate({
                        opacity: 1,
                        right: "50px",
                        bottom: "10px",
                        height: "toggle"
                    }, 2000, function() {}).css('position','fixed');                    
                }else if (hasil.hasil == "maksimal") {   
                    $("#pesan2").show();                 
                    $("#pesan2").html("<div class=\"alert alert-danger\">Tanggal penggunaan melebihi 30 hari</div>");
                    $('#pesan2').animate({
                        opacity: 1,
                        right: "50px",
                        bottom: "10px",
                        height: "toggle"
                    }, 2000, function() {}).css('position','fixed');                    
                }else if (hasil.hasil == "sukses") {   
                    $("#pesan2").show();                 
                    $("#pesan2").html("<div class=\"alert alert-success\">Data berhasil di simpan</div>");
                    
                    $("#nama_kegiatan").val('');
                    $("#seksi_penggunaan").val('');
                    $("#penanggung_jawab").val('');
                    $('#pesan2').animate({
                        opacity: 1,
                        right: "50px",
                        bottom: "10px",
                        height: "toggle"
                    }, 2000, function() {}).css('position','fixed');    

                    window.location.assign("<?php echo base_url(); ?>postings/details/1033910/Jadwal_Penggunaan_Aula.HTML");
                }
            }
        });
    });
</script>

<!-- <script>
  $('#btnSimpan2').on('click',function(){
      alert('something wrong');
  });
</script> -->

<script>
function myFunction() {
  var date =document.getElementById("tanggal_penggunaan").value;

  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
  var yyyy = today.getFullYear();

  today = yyyy + '-' + mm + '-' + dd;

  var d1 = new Date(today); //"now"
  var d2 = new Date(date)  // some date
  var selisih = Math.abs(d1 - d2)/86400000;

  // alert(selisih);

  document.getElementById("tanggal_selisih").value = selisih;
  
}
</script>