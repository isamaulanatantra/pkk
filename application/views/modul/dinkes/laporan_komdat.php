

<section class="content" id="awal">
  <div class="pad">
          <div class="row">
              <div class="col-md-2">
                <div class="form-group">
                  <label for="thn_komdat">Pilih Tahun</label>           
                  <select class="form-control" id="thn_komdat" name="thn_komdat" required >
                    <!-- <option value="2017">2017</option> -->
                    <option value="2018">2018</option>
                    <option value="2019">2019</option>
                    <option value="2020">2020</option>
                    <option value="2021">2021</option>
                    <option value="2022">2022</option>
                  </select>
                </div>
              </div>
              <div class="col-md-2" style="display: none;">
                <div class="form-group">
                  <label for="bln_komdat">Pilih Bulan</label>           
                  <select class="form-control" id="bln_komdat" name="bln_komdat" required >
                    <option value="1">Januari</option>
                    <option value="2">Februari</option>
                    <option value="3">Maret</option>
                    <option value="4">April</option>
                    <option value="5">Mei</option>
                    <option value="6">Juni</option>
                    <option value="7">Juli</option>
                    <option value="8">Agustus</option>
                    <option value="9">September</option>
                    <option value="10">Oktober</option>
                    <option value="11">November</option>
                    <option value="12">Desember</option>
                  </select>
                </div>
              </div>
              <div class="col-md-1">
                <div class="form-group">
                  <label for="tampilkan">&nbsp;</label>
                  <button id="tampilkan" class="btn btn-primary" >Tampilkan</button>
                </div>                
              </div>
              <div class="col-md-1 col-md-offset-1">                
                <div class="form-group">
                  <label for="export">&nbsp;</label>
                  <button id="btnExport" class="btn btn-warning" >Export Excel</button>
                </div>
              </div>                                    
          </div>         
          
                  <div class="row">
                        <div id="tblExport">
                <table class="table table-bordered table-hover">
                  <thead class="bg-success">
                    <th>No</th>
                    <th>Indikator Komdat</th>             
                    <th>Jan</th>
                    <th>Feb</th>
                    <th>Mar</th>
                    <th>Apr</th>
                    <th>Mei</th>
                    <th>Jun</th>
                    <th>Jul</th>
                    <th>Agst</th>
                    <th>Sept</th>
                    <th>Okt</th>
                    <th>Nov</th>
                    <th>Des</th>
                  </thead>

                  <tbody id="tbl_komdat_kia">
                  </tbody>
                  <tbody id="tbl_komdat_gizi">
                  </tbody>
                  <tbody id="tbl_komdat_imunisasi">
                  </tbody>
                  <tbody id="tbl_komdat_penyakit">
                  </tbody>
                  <tbody id="tbl_komdat_triwulan">
                  </tbody>
                </table>
              </div>
            </div>
          <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
          </ul>
          
          <div class="overlay" id="spinners_data" style="display:none;">
            <i class="fa fa-refresh fa-spin"></i>
          </div>
  </div>
  <div id="pesan"></div>
</section>

<!-- <script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_penggunaan').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script> -->


<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>



<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilkan').on('click', function(e) {
      e.preventDefault();
      $('#tbl_utama_komdat').html('');
        var halaman = 1;
      var limit = 10;
      var tahun = $('#thn_komdat').val();
      var bulan = $('#bln_komdat').val();     
      
    datakomdatkia(halaman, limit, tahun, bulan);
    datakomdatgizi(halaman, limit, tahun, bulan);
    datakomdatimunisasi(halaman, limit, tahun, bulan);
    datakomdatpenyakit(halaman, limit, tahun, bulan);
    datakomdattriwulan(halaman, limit, tahun, bulan);
    });
  });
</script>

<script type="text/javascript">
  function datakomdatkia(halaman, limit, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_komdat_kia').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit,
        tahun: tahun,
        bulan: bulan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>laporan_komdat/load_kia/',
      success: function(html) {
        $('#tbl_komdat_kia').html(html);
        $('#spinners_data').hide();
      }
    });
  };
</script>

<script type="text/javascript">
  function datakomdatgizi(halaman, limit, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_komdat_gizi').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit,
        tahun: tahun,
        bulan: bulan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>laporan_komdat/load_gizi/',
      success: function(html) {
        $('#tbl_komdat_gizi').html(html);
        $('#spinners_data').hide();
      }
    });
  };
</script>

<script type="text/javascript">
  function datakomdatimunisasi(halaman, limit, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_komdat_imunisasi').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit,
        tahun: tahun,
        bulan: bulan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>laporan_komdat/load_imunisasi/',
      success: function(html) {
        $('#tbl_komdat_imunisasi').html(html);
        $('#spinners_data').hide();
      }
    });
  };
</script>

<script type="text/javascript">
  function datakomdatpenyakit(halaman, limit, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_komdat_penyakit').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit,
        tahun: tahun,
        bulan: bulan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>laporan_komdat/load_penyakit/',
      success: function(html) {
        $('#tbl_komdat_penyakit').html(html);
        $('#spinners_data').hide();
      }
    });
  };
</script>

<script type="text/javascript">
  function datakomdattriwulan(halaman, limit, tahun, bulan) {
    $('#spinners_data').show();    

    $('#tbl_komdat_triwulan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit,
        tahun: tahun,
        bulan: bulan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>laporan_komdat/load_triwulan/',
      success: function(html) {
        $('#tbl_komdat_triwulan').html(html);
        $('#spinners_data').hide();
      }
    });
  };
</script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>

     <script src="<?php echo base_url(); ?>jquery.js"></script>
<script src="<?php echo base_url(); ?>js_exel/jquery.btechco.excelexport.js"></script>
<script src="<?php echo base_url(); ?>js/jquery.base64.js"></script>
<script>
  (function($) {
    $(document).ready(function() { 
    $("#btnExport").click(function (e) {
        e.preventDefault();
          $("#tblExport").btechco_excelexport({
              containerid: "tblExport"
              , datatype: $datatype.Table
          });
      });
    });
  })(jQuery);
</script>

<script>
      $('#tbl_utama_komdat').html('');
        var halaman = 1;
      var limit = 10;
      var tahun = $('#thn_komdat').val();
      var bulan = $('#bln_komdat').val();     
      
    datakomdatkia(halaman, limit, tahun, bulan);
    datakomdatgizi(halaman, limit, tahun, bulan);
    datakomdatimunisasi(halaman, limit, tahun, bulan);
    datakomdatpenyakit(halaman, limit, tahun, bulan);
    datakomdattriwulan(halaman, limit, tahun, bulan);
</script>
