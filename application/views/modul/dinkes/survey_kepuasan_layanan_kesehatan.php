<?Php
include 'api/koneksi.php';

if($stmt = $koneksi->query("SELECT msDinkesNama,msDinkesLike,msDinkesDislike FROM dinkes")){

  
$php_data_array = Array(); // create PHP array
  
while ($row = $stmt->fetch_row()) {
  
   $php_data_array[] = $row; // Adding to array
   }
}else{
echo $koneksi->error;
}


//print_r( $php_data_array);
// You can display the json_encode output here. 
// echo json_encode($php_data_array); 

// Transfor PHP array to JavaScript two dimensional array 
echo "<script>
        var my_2d = ".json_encode($php_data_array)."
</script>";
?>

<form role="form" id="form_pertanyaan" method="post" action="" enctype="multipart/form-data ">
  <div class="modal fade" id="FormReviewer" tabindex="-1" role="dialog" aria-labelledby="myReviewer" aria-hidden="true" >
    <div class="modal-dialog">
      
      </div>
    </div>
  </div>
</form>

<div class="pad" id="atas">
    <div class="row">
    <div class="col-xs-12">
        <center>
        <h3 class="box-title">GRAFIK SURVEY KEPUASAN LAYANAN KESEHATAN</h3> <br />
        
        <div id="chart_div"></div>
        
        <br>
        </center>
      <br>

    </div>
  </div>
</div>

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

      // Load the Visualization API and the corechart package.
      google.charts.load('current', {packages: ['corechart', 'bar']});
      google.charts.setOnLoadCallback(drawChart);
    
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'NAMA PELAYANAN');
        data.addColumn('number', 'Puas');
    data.addColumn('number', 'Tidak Puas');
        for(i = 0; i < my_2d.length; i++)
    data.addRow([my_2d[i][0], parseInt(my_2d[i][1]),parseInt(my_2d[i][2])]);
       var options = {
        chart: {
          title: '',
          subtitle: ''
        },
        width: 1030,
        height: 400,
        axes: {
          x: {
            0: {side: 'bottom'}
          }
        },
        colors: ['green', 'red'],
        bars: 'horizontal'
      };

        var chart = new google.charts.Bar(document.getElementById('chart_div'));
        chart.draw(data, options);
       }
  ///////////////////////////////
////////////////////////////////////  
</script>

