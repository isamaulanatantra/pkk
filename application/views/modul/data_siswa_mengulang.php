<section class="content" id="awal">

						<?php
						if (empty($_GET['thn_pelajaran'])) {
							$thn_pelajaran = "2017/2018";
						}else{
							$thn_pelajaran = $_GET['thn_pelajaran'];
						}
						?>
  <div class="row">
    <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_1">
        
      </div>
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
								Data Siswa Mengulang Sekolah Tahun Pelajaran <?php echo $thn_pelajaran; ?> di Dapodik
              </h3>
              <div class="box-tools">
                <div class="input-group">
				<select name="thn_pelajaran" class="form-control input-sm pull-right" style="width: 180px;" id="thn_pelajaran">
                    <option value="">Tahun Pelajaran</option>
                    <option value="2017/2018">2017/2018</option>
                    <option value="2018/2019">2018/2019</option>
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-xs btn-primary" id="filter"><i class="fa fa-search"></i> Tampil</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-body table-responsive no-padding">
                        <div id="tblExport">
                          <table class="table table-bordered table-hover">
                            <thead>
                              <tr id="header_exel">
								  <tr>
									<th rowspan="2">No</th>
									<th rowspan="2">Wilayah</th>
									<th colspan="3">SD</th>
									<th colspan="3">SMP</th>
									<th colspan="3">SMA</th>
									<th colspan="3">SMK</th>
								  </tr>
								  <tr>
									<td>L</td>
									<td>P</td>
									<td>Jml</td>
									<td>L</td>
									<td>P</td>
									<td>Jml</td>
									<td>L</td>
									<td>P</td>
									<td>Jml</td>
									<td>L</td>
									<td>P</td>
									<td>Jml</td>
								</tr>
									  <tr>
										<td>1</td>
										<td>Wadaslintang</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Wadaslintang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_01='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_01=($jml_siswa_mengulang_SD_l_01*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Wadaslintang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_01='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_01=($jml_siswa_mengulang_SD_p_01*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_01=$jml_siswa_mengulang_SD_p_01+$jml_siswa_mengulang_SD_l_01;
											  }
												  echo''.$jml_siswa_mengulang_SD_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Wadaslintang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_01='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_01=($jml_siswa_mengulang_SMP_l_01*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Wadaslintang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_01='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_01=($jml_siswa_mengulang_SMP_p_01*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Wadaslintang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_01=$jml_siswa_mengulang_SMP_p_01+$jml_siswa_mengulang_SMP_l_01;
											  }
												  echo''.$jml_siswa_mengulang_SMP_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Wadaslintang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_01='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_01=($jml_siswa_mengulang_SMA_l_01*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Wadaslintang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_01='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_01=($jml_siswa_mengulang_SMA_p_01*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Wadaslintang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_01=$jml_siswa_mengulang_SMA_p_01+$jml_siswa_mengulang_SMA_l_01;
											  }
												  echo''.$jml_siswa_mengulang_SMA_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Wadaslintang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_01='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_01=($jml_siswa_mengulang_SMK_l_01*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Wadaslintang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_01='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_01=($jml_siswa_mengulang_SMK_p_01*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_01.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Wadaslintang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_01=$jml_siswa_mengulang_SMK_p_01+$jml_siswa_mengulang_SMK_l_01;
											  }
												  echo''.$jml_siswa_mengulang_SMK_01.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>2</td>
										<td>Kepil</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kepil'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_02='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_02=($jml_siswa_mengulang_SD_l_02*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kepil'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_02='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_02=($jml_siswa_mengulang_SD_p_02*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_02=$jml_siswa_mengulang_SD_p_02+$jml_siswa_mengulang_SD_l_02;
											  }
												  echo''.$jml_siswa_mengulang_SD_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kepil'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_02='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_02=($jml_siswa_mengulang_SMP_l_02*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kepil'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_02='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_02=($jml_siswa_mengulang_SMP_p_02*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kepil'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_02=$jml_siswa_mengulang_SMP_p_02+$jml_siswa_mengulang_SMP_l_02;
											  }
												  echo''.$jml_siswa_mengulang_SMP_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kepil'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_02='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_02=($jml_siswa_mengulang_SMA_l_02*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kepil'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_02='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_02=($jml_siswa_mengulang_SMA_p_02*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kepil'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_02=$jml_siswa_mengulang_SMA_p_02+$jml_siswa_mengulang_SMA_l_02;
											  }
												  echo''.$jml_siswa_mengulang_SMA_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kepil'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_02='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_02=($jml_siswa_mengulang_SMK_l_02*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kepil'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_02='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_02=($jml_siswa_mengulang_SMK_p_02*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_02.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kepil'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_02=$jml_siswa_mengulang_SMK_p_02+$jml_siswa_mengulang_SMK_l_02;
											  }
												  echo''.$jml_siswa_mengulang_SMK_02.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>3</td>
										<td>Sapuran</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Sapuran'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_03='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_03=($jml_siswa_mengulang_SD_l_03*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Sapuran'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_03='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_03=($jml_siswa_mengulang_SD_p_03*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_03=$jml_siswa_mengulang_SD_p_03+$jml_siswa_mengulang_SD_l_03;
											  }
												  echo''.$jml_siswa_mengulang_SD_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Sapuran'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_03='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_03=($jml_siswa_mengulang_SMP_l_03*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Sapuran'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_03='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_03=($jml_siswa_mengulang_SMP_p_03*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Sapuran'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_03=$jml_siswa_mengulang_SMP_p_03+$jml_siswa_mengulang_SMP_l_03;
											  }
												  echo''.$jml_siswa_mengulang_SMP_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Sapuran'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_03='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_03=($jml_siswa_mengulang_SMA_l_03*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Sapuran'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_03='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_03=($jml_siswa_mengulang_SMA_p_03*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Sapuran'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_03=$jml_siswa_mengulang_SMA_p_03+$jml_siswa_mengulang_SMA_l_03;
											  }
												  echo''.$jml_siswa_mengulang_SMA_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Sapuran'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_03='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_03=($jml_siswa_mengulang_SMK_l_03*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Sapuran'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_03='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_03=($jml_siswa_mengulang_SMK_p_03*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_03.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Sapuran'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_03=$jml_siswa_mengulang_SMK_p_03+$jml_siswa_mengulang_SMK_l_03;
											  }
												  echo''.$jml_siswa_mengulang_SMK_03.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>4</td>
										<td>Kaliwiro</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kaliwiro'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_04='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_04=($jml_siswa_mengulang_SD_l_04*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kaliwiro'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_04='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_04=($jml_siswa_mengulang_SD_p_04*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_04=$jml_siswa_mengulang_SD_p_04+$jml_siswa_mengulang_SD_l_04;
											  }
												  echo''.$jml_siswa_mengulang_SD_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kaliwiro'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_04='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_04=($jml_siswa_mengulang_SMP_l_04*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kaliwiro'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_04='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_04=($jml_siswa_mengulang_SMP_p_04*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kaliwiro'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_04=$jml_siswa_mengulang_SMP_p_04+$jml_siswa_mengulang_SMP_l_04;
											  }
												  echo''.$jml_siswa_mengulang_SMP_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kaliwiro'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_04='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_04=($jml_siswa_mengulang_SMA_l_04*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kaliwiro'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_04='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_04=($jml_siswa_mengulang_SMA_p_04*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kaliwiro'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_04=$jml_siswa_mengulang_SMA_p_04+$jml_siswa_mengulang_SMA_l_04;
											  }
												  echo''.$jml_siswa_mengulang_SMA_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kaliwiro'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_04='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_04=($jml_siswa_mengulang_SMK_l_04*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kaliwiro'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_04='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_04=($jml_siswa_mengulang_SMK_p_04*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_04.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kaliwiro'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_04=$jml_siswa_mengulang_SMK_p_04+$jml_siswa_mengulang_SMK_l_04;
											  }
												  echo''.$jml_siswa_mengulang_SMK_04.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>5</td>
										<td>Leksono</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Leksono'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_05='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_05=($jml_siswa_mengulang_SD_l_05*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Leksono'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_05='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_05=($jml_siswa_mengulang_SD_p_05*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_05=$jml_siswa_mengulang_SD_p_05+$jml_siswa_mengulang_SD_l_05;
											  }
												  echo''.$jml_siswa_mengulang_SD_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Leksono'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_05='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_05=($jml_siswa_mengulang_SMP_l_05*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Leksono'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_05='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_05=($jml_siswa_mengulang_SMP_p_05*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Leksono'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_05=$jml_siswa_mengulang_SMP_p_05+$jml_siswa_mengulang_SMP_l_05;
											  }
												  echo''.$jml_siswa_mengulang_SMP_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Leksono'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_05='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_05=($jml_siswa_mengulang_SMA_l_05*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Leksono'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_05='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_05=($jml_siswa_mengulang_SMA_p_05*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Leksono'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_05=$jml_siswa_mengulang_SMA_p_05+$jml_siswa_mengulang_SMA_l_05;
											  }
												  echo''.$jml_siswa_mengulang_SMA_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Leksono'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_05='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_05=($jml_siswa_mengulang_SMK_l_05*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Leksono'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_05='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_05=($jml_siswa_mengulang_SMK_p_05*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_05.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Leksono'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_05=$jml_siswa_mengulang_SMK_p_05+$jml_siswa_mengulang_SMK_l_05;
											  }
												  echo''.$jml_siswa_mengulang_SMK_05.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>6</td>
										<td>Selomerto</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Selomerto'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_06='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_06=($jml_siswa_mengulang_SD_l_06*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Selomerto'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_06='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_06=($jml_siswa_mengulang_SD_p_06*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_06=$jml_siswa_mengulang_SD_p_06+$jml_siswa_mengulang_SD_l_06;
											  }
												  echo''.$jml_siswa_mengulang_SD_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Selomerto'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_06='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_06=($jml_siswa_mengulang_SMP_l_06*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Selomerto'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_06='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_06=($jml_siswa_mengulang_SMP_p_06*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Selomerto'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_06=$jml_siswa_mengulang_SMP_p_06+$jml_siswa_mengulang_SMP_l_06;
											  }
												  echo''.$jml_siswa_mengulang_SMP_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Selomerto'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_06='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_06=($jml_siswa_mengulang_SMA_l_06*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Selomerto'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_06='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_06=($jml_siswa_mengulang_SMA_p_06*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Selomerto'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_06=$jml_siswa_mengulang_SMA_p_06+$jml_siswa_mengulang_SMA_l_06;
											  }
												  echo''.$jml_siswa_mengulang_SMA_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Selomerto'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_06='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_06=($jml_siswa_mengulang_SMK_l_06*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Selomerto'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_06='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_06=($jml_siswa_mengulang_SMK_p_06*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_06.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Selomerto'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_06=$jml_siswa_mengulang_SMK_p_06+$jml_siswa_mengulang_SMK_l_06;
											  }
												  echo''.$jml_siswa_mengulang_SMK_06.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>7</td>
										<td>Kalikajar</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kalikajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_07='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_07=($jml_siswa_mengulang_SD_l_07*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kalikajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_07='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_07=($jml_siswa_mengulang_SD_p_07*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_07=$jml_siswa_mengulang_SD_p_07+$jml_siswa_mengulang_SD_l_07;
											  }
												  echo''.$jml_siswa_mengulang_SD_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kalikajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_07='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_07=($jml_siswa_mengulang_SMP_l_07*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kalikajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_07='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_07=($jml_siswa_mengulang_SMP_p_07*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kalikajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_07=$jml_siswa_mengulang_SMP_p_07+$jml_siswa_mengulang_SMP_l_07;
											  }
												  echo''.$jml_siswa_mengulang_SMP_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kalikajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_07='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_07=($jml_siswa_mengulang_SMA_l_07*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kalikajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  if ($jml_siswa_mengulang_SMA_l_07=='0'){
												  echo'0';
											  }
											  else {
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_07=$jml_siswa_mengulang_SMA_p_07+$jml_siswa_mengulang_SMA_l_07;
											  }
												  echo''.$jml_siswa_mengulang_SMA_07.'';
											  }
											?>
										</td>
										<td>0</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kalikajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_07='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_07=($jml_siswa_mengulang_SMK_l_07*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kalikajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_07='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_07=($jml_siswa_mengulang_SMK_p_07*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_07.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kalikajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_07=$jml_siswa_mengulang_SMK_p_07+$jml_siswa_mengulang_SMK_l_07;
											  }
												  echo''.$jml_siswa_mengulang_SMK_07.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>8</td>
										<td>Kertek</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kertek'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_08='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_08=($jml_siswa_mengulang_SD_l_08*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kertek'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_08='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_08=($jml_siswa_mengulang_SD_p_08*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_08=$jml_siswa_mengulang_SD_p_08+$jml_siswa_mengulang_SD_l_08;
											  }
												  echo''.$jml_siswa_mengulang_SD_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kertek'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_08='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_08=($jml_siswa_mengulang_SMP_l_08*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kertek'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_08='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_08=($jml_siswa_mengulang_SMP_p_08*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kertek'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_08=$jml_siswa_mengulang_SMP_p_08+$jml_siswa_mengulang_SMP_l_08;
											  }
												  echo''.$jml_siswa_mengulang_SMP_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kertek'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_08='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_08=($jml_siswa_mengulang_SMA_l_08*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kertek'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_08='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_08=($jml_siswa_mengulang_SMA_p_08*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kertek'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_08=$jml_siswa_mengulang_SMA_p_08+$jml_siswa_mengulang_SMA_l_08;
											  }
												  echo''.$jml_siswa_mengulang_SMA_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kertek'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_08='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_08=($jml_siswa_mengulang_SMK_l_08*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kertek'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_08='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_08=($jml_siswa_mengulang_SMK_p_08*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_08.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kertek'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_08=$jml_siswa_mengulang_SMK_p_08+$jml_siswa_mengulang_SMK_l_08;
											  }
												  echo''.$jml_siswa_mengulang_SMK_08.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>9</td>
										<td>Wonosobo</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Wonosobo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_09='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_09=($jml_siswa_mengulang_SD_l_09*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Wonosobo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_09='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_09=($jml_siswa_mengulang_SD_p_09*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_09=$jml_siswa_mengulang_SD_p_09+$jml_siswa_mengulang_SD_l_09;
											  }
												  echo''.$jml_siswa_mengulang_SD_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Wonosobo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_09='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_09=($jml_siswa_mengulang_SMP_l_09*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Wonosobo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_09='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_09=($jml_siswa_mengulang_SMP_p_09*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Wonosobo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_09=$jml_siswa_mengulang_SMP_p_09+$jml_siswa_mengulang_SMP_l_09;
											  }
												  echo''.$jml_siswa_mengulang_SMP_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Wonosobo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_09='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_09=($jml_siswa_mengulang_SMA_l_09*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Wonosobo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_09='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_09=($jml_siswa_mengulang_SMA_p_09*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Wonosobo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_09=$jml_siswa_mengulang_SMA_p_09+$jml_siswa_mengulang_SMA_l_09;
											  }
												  echo''.$jml_siswa_mengulang_SMA_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Wonosobo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_09='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_09=($jml_siswa_mengulang_SMK_l_09*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Wonosobo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_09='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_09=($jml_siswa_mengulang_SMK_p_09*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_09.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Wonosobo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_09=$jml_siswa_mengulang_SMK_p_09+$jml_siswa_mengulang_SMK_l_09;
											  }
												  echo''.$jml_siswa_mengulang_SMK_09.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>10</td>
										<td>Mojotengah</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Mojotengah'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_10='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_10=($jml_siswa_mengulang_SD_l_10*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Mojotengah'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_10='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_10=($jml_siswa_mengulang_SD_p_10*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_10=$jml_siswa_mengulang_SD_p_10+$jml_siswa_mengulang_SD_l_10;
											  }
												  echo''.$jml_siswa_mengulang_SD_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Mojotengah'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_10='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_10=($jml_siswa_mengulang_SMP_l_10*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Mojotengah'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_10='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_10=($jml_siswa_mengulang_SMP_p_10*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Mojotengah'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_10=$jml_siswa_mengulang_SMP_p_10+$jml_siswa_mengulang_SMP_l_10;
											  }
												  echo''.$jml_siswa_mengulang_SMP_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Mojotengah'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_10='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_10=($jml_siswa_mengulang_SMA_l_10*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Mojotengah'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_10='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_10=($jml_siswa_mengulang_SMA_p_10*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Mojotengah'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_10=$jml_siswa_mengulang_SMA_p_10+$jml_siswa_mengulang_SMA_l_10;
											  }
												  echo''.$jml_siswa_mengulang_SMA_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Mojotengah'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_10='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_10=($jml_siswa_mengulang_SMK_l_10*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Mojotengah'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_10='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_10=($jml_siswa_mengulang_SMK_p_10*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_10.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Mojotengah'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_10=$jml_siswa_mengulang_SMK_p_10+$jml_siswa_mengulang_SMK_l_10;
											  }
												  echo''.$jml_siswa_mengulang_SMK_10.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>11</td>
										<td>Watumalang</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Watumalang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_11='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_11=($jml_siswa_mengulang_SD_l_11*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Watumalang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_11='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_11=($jml_siswa_mengulang_SD_p_11*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_11=$jml_siswa_mengulang_SD_p_11+$jml_siswa_mengulang_SD_l_11;
											  }
												  echo''.$jml_siswa_mengulang_SD_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Watumalang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_11='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_11=($jml_siswa_mengulang_SMP_l_11*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Watumalang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_11='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_11=($jml_siswa_mengulang_SMP_p_11*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Watumalang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_11=$jml_siswa_mengulang_SMP_p_11+$jml_siswa_mengulang_SMP_l_11;
											  }
												  echo''.$jml_siswa_mengulang_SMP_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Watumalang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_11='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_11=($jml_siswa_mengulang_SMA_l_11*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Watumalang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_11='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_11=($jml_siswa_mengulang_SMA_p_11*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Watumalang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_11=$jml_siswa_mengulang_SMA_p_11+$jml_siswa_mengulang_SMA_l_11;
											  }
												  echo''.$jml_siswa_mengulang_SMA_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Watumalang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_11='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_11=($jml_siswa_mengulang_SMK_l_11*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Watumalang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_11='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_11=($jml_siswa_mengulang_SMK_p_11*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_11.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Watumalang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_11=$jml_siswa_mengulang_SMK_p_11+$jml_siswa_mengulang_SMK_l_11;
											  }
												  echo''.$jml_siswa_mengulang_SMK_11.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>12</td>
										<td>Garung</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Garung'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_12='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_12=($jml_siswa_mengulang_SD_l_12*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Garung'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_12='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_12=($jml_siswa_mengulang_SD_p_12*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_12=$jml_siswa_mengulang_SD_p_12+$jml_siswa_mengulang_SD_l_12;
											  }
												  echo''.$jml_siswa_mengulang_SD_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Garung'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_12='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_12=($jml_siswa_mengulang_SMP_l_12*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Garung'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_12='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_12=($jml_siswa_mengulang_SMP_p_12*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Garung'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_12=$jml_siswa_mengulang_SMP_p_12+$jml_siswa_mengulang_SMP_l_12;
											  }
												  echo''.$jml_siswa_mengulang_SMP_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Garung'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_12='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_12=($jml_siswa_mengulang_SMA_l_12*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Garung'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_12='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_12=($jml_siswa_mengulang_SMA_p_12*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Garung'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  if ($jml_siswa_mengulang_SMA_l_12=='0'){
												  echo'0';
											  }
											  else {
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_12=$jml_siswa_mengulang_SMA_p_12+$jml_siswa_mengulang_SMA_l_12;
											  }
												  echo''.$jml_siswa_mengulang_SMA_12.'';
											  }
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Garung'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_12='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_12=($jml_siswa_mengulang_SMK_l_12*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Garung'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_12='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_12=($jml_siswa_mengulang_SMK_p_12*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_12.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Garung'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_12=$jml_siswa_mengulang_SMK_p_12+$jml_siswa_mengulang_SMK_l_12;
											  }
												  echo''.$jml_siswa_mengulang_SMK_12.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>13</td>
										<td>Kejajar</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kejajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_13='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_13=($jml_siswa_mengulang_SD_l_13*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kejajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_13='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_13=($jml_siswa_mengulang_SD_p_13*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_13=$jml_siswa_mengulang_SD_p_13+$jml_siswa_mengulang_SD_l_13;
											  }
												  echo''.$jml_siswa_mengulang_SD_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kejajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_13='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_13=($jml_siswa_mengulang_SMP_l_13*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kejajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_13='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_13=($jml_siswa_mengulang_SMP_p_13*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kejajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_13=$jml_siswa_mengulang_SMP_p_13+$jml_siswa_mengulang_SMP_l_13;
											  }
												  echo''.$jml_siswa_mengulang_SMP_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kejajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_13='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_13=($jml_siswa_mengulang_SMA_l_13*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kejajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_13='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_13=($jml_siswa_mengulang_SMA_p_13*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kejajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_13=$jml_siswa_mengulang_SMA_p_13+$jml_siswa_mengulang_SMA_l_13;
											  }
												  echo''.$jml_siswa_mengulang_SMA_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kejajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_13='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_13=($jml_siswa_mengulang_SMK_l_13*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kejajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_13='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_13=($jml_siswa_mengulang_SMK_p_13*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_13.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kejajar'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_13=$jml_siswa_mengulang_SMK_p_13+$jml_siswa_mengulang_SMK_l_13;
											  }
												  echo''.$jml_siswa_mengulang_SMK_13.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>14</td>
										<td>Sukoharjo</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Sukoharjo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_14='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_14=($jml_siswa_mengulang_SD_l_14*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Sukoharjo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_14='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_14=($jml_siswa_mengulang_SD_p_14*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_14=$jml_siswa_mengulang_SD_p_14+$jml_siswa_mengulang_SD_l_14;
											  }
												  echo''.$jml_siswa_mengulang_SD_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Sukoharjo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_14='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_14=($jml_siswa_mengulang_SMP_l_14*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Sukoharjo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_14='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_14=($jml_siswa_mengulang_SMP_p_14*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Sukoharjo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_14=$jml_siswa_mengulang_SMP_p_14+$jml_siswa_mengulang_SMP_l_14;
											  }
												  echo''.$jml_siswa_mengulang_SMP_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Sukoharjo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_14='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_14=($jml_siswa_mengulang_SMA_l_14*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Sukoharjo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_14='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_14=($jml_siswa_mengulang_SMA_p_14*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Sukoharjo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  if ($jml_siswa_mengulang_SMA_l_14=='0'){
												  echo'0';
											  }
											  else {
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_14=$jml_siswa_mengulang_SMA_p_14+$jml_siswa_mengulang_SMA_l_14;
											  }
												  echo''.$jml_siswa_mengulang_SMA_14.'';
											  }
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Sukoharjo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_14='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_14=($jml_siswa_mengulang_SMK_l_14*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Sukoharjo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_14='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_14=($jml_siswa_mengulang_SMK_p_14*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_14.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Sukoharjo'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_14=$jml_siswa_mengulang_SMK_p_14+$jml_siswa_mengulang_SMK_l_14;
											  }
												  echo''.$jml_siswa_mengulang_SMK_14.'';
											?>
										</td>
									  </tr>
									  <tr>
										<td>15</td>
										<td>Kalibawang</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kalibawang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_l_15='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_l_15=($jml_siswa_mengulang_SD_l_15*1)+($h2->pd_mengulang_tkt_1_l*1)+($h2->pd_mengulang_tkt_2_l*1)+($h2->pd_mengulang_tkt_3_l*1)+($h2->pd_mengulang_tkt_4_l*1)+($h2->pd_mengulang_tkt_5_l*1)+($h2->pd_mengulang_tkt_6_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_l_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  and kec_='Kec. Kalibawang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SD_p_15='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_p_15=($jml_siswa_mengulang_SD_p_15*1)+($h2->pd_mengulang_tkt_1_p*1)+($h2->pd_mengulang_tkt_2_p*1)+($h2->pd_mengulang_tkt_3_p*1)+($h2->pd_mengulang_tkt_4_p*1)+($h2->pd_mengulang_tkt_5_p*1)+($h2->pd_mengulang_tkt_6_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SD_p_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SD'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SD_15=$jml_siswa_mengulang_SD_p_15+$jml_siswa_mengulang_SD_l_15;
											  }
												  echo''.$jml_siswa_mengulang_SD_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kalibawang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_l_15='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_l_15=($jml_siswa_mengulang_SMP_l_15*1)+($h2->pd_mengulang_tkt_7_l*1)+($h2->pd_mengulang_tkt_8_l*1)+($h2->pd_mengulang_tkt_9_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_l_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kalibawang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMP_p_15='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_p_15=($jml_siswa_mengulang_SMP_p_15*1)+($h2->pd_mengulang_tkt_7_p*1)+($h2->pd_mengulang_tkt_8_p*1)+($h2->pd_mengulang_tkt_9_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMP_p_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMP'
											  and kec_='Kec. Kalibawang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMP_15=$jml_siswa_mengulang_SMP_p_15+$jml_siswa_mengulang_SMP_l_15;
											  }
												  echo''.$jml_siswa_mengulang_SMP_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kalibawang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_l_15='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_l_15=($jml_siswa_mengulang_SMA_l_15*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_l_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kalibawang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMA_p_15='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_p_15=($jml_siswa_mengulang_SMA_p_15*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMA_p_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMA'
											  and kec_='Kec. Kalibawang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  if ($jml_siswa_mengulang_SMA_l_15=='0'){
												  echo'0';
											  }
											  else {
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMA_15=$jml_siswa_mengulang_SMA_p_15+$jml_siswa_mengulang_SMA_l_15;
											  }
												  echo''.$jml_siswa_mengulang_SMA_15.'';
											  }
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kalibawang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_l_15='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_l_15=($jml_siswa_mengulang_SMK_l_15*1)+($h2->pd_mengulang_tkt_10_l*1)+($h2->pd_mengulang_tkt_11_l*1)+($h2->pd_mengulang_tkt_12_l*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_l_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kalibawang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  $jml_siswa_mengulang_SMK_p_15='0';
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_p_15=($jml_siswa_mengulang_SMK_p_15*1)+($h2->pd_mengulang_tkt_10_p*1)+($h2->pd_mengulang_tkt_11_p*1)+($h2->pd_mengulang_tkt_12_p*1);
											  }
												  echo''.$jml_siswa_mengulang_SMK_p_15.'';
											?>
										</td>
										<td>
											<?php
											  $w2 = $this->db->query("
											  SELECT *
											  from data_dikpora
											  where jenjang='SMK'
											  and kec_='Kec. Kalibawang'
											  and thn_pelajaran='$thn_pelajaran'
											  ");
											  foreach($w2->result() as $h2)
											  {
											  $jml_siswa_mengulang_SMK_15=$jml_siswa_mengulang_SMK_p_15+$jml_siswa_mengulang_SMK_l_15;
											  }
												  echo''.$jml_siswa_mengulang_SMK_15.'';
											?>
										</td>
									  </tr>
                            </thead>
                            <tbody id="tbl_utama_daftar_informasi_publik_pembantu">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="row">
                        
                        
                        
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1;
		var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
		var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
		var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
    load_data_daftar_informasi_publik_pembantu(halaman, limit, klasifikasi_informasi_publik_pembantu);
});
</script>

<script>
  function load_data_daftar_informasi_publik_pembantu(halaman, limit, klasifikasi_informasi_publik_pembantu) {
    $('#tbl_utama_daftar_informasi_publik_pembantu').html('');
    $('#spinners_data').show();
		var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        klasifikasi_informasi_publik_pembantu: klasifikasi_informasi_publik_pembantu,
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_dikpora/load_table/',
      success: function(html) {
        $('#tbl_utama_daftar_informasi_publik_pembantu').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilkan_daftar_informasi_publik').on('click', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, klasifikasi_informasi_publik_pembantu);
    });
  });
</script>

<script type="text/javascript">
  $(function () {
    $('#konten_posting').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })

	$('#filter').on('click', function() {
	    var thn_pelajaran = $('#thn_pelajaran').val();
	    window.location.replace("https://dikpora.wonosobokab.go.id/postings/detail/1031582/Data_Siswa_Mengulang.HTML?thn_pelajaran="+thn_pelajaran+"");
	});

</script>
