<!-- sementara -->

<div id="myModal" class="modal fade" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false" data-backdrop="false">
  <div class="modal-dialog" style="overflow-y: scroll; max-height:85%;  margin-top: 50px; margin-bottom:50px; width:90%;">

    
    <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>perijinan_penelitian/simpan_perijinan_penelitian_sementara" enctype="multipart/form-data">
    <div class="modal-content">
        <div class="modal-header">
            <div class="row">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4>FORMULIR PERIJINAN PENELITIAN</h4>

                    <div class="form-group" style="display:none;">
                      <label for="temp">temp</label>
                      <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="mode">mode</label>
                      <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_perijinan_penelitian">id_perijinan_penelitian</label>
                      <input class="form-control" id="id_perijinan_penelitian" name="id" value="" placeholder="id_perijinan_penelitian" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="nomor_surat">nomor_surat</label>
                      <input class="form-control" id="nomor_surat" name="nomor_surat" value="-" placeholder="nomor_surat" type="text">
                    </div>
                    <!-- <div class="form-group" style="display:none;">
                      <label for="perihal_surat">Perihal</label>
                      <input class="form-control" id="perihal_surat" name="perihal_surat" value="Ijin Penelitian" placeholder="perihal_surat" type="text">
                    </div> -->
                    <div class="form-group" style="">
                      <label for="perihal_surat">Perihal</label>
                      <!-- <input class="form-control" id="tanggal_surat" name="tanggal_surat" value="" placeholder="tanggal_surat" type="text"> -->
                      <select class="form-control" id="perihal_surat" name="perihal_surat" required >
                        <option value="Ijin Penelitian">Ijin Penelitian</option>
                        <option value="Survey Pendahuluan">Survey Pendahuluan</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
                    </div>
                    <div class="form-group">
                      <label for="nomot_telp">Nomot Telp</label>
                      <input class="form-control" id="nomot_telp" name="nomot_telp" value="" placeholder="Nomot Telp" type="text">
                    </div>
                    <div class="form-group">
                      <label for="kebangsaan">Kebangsaan</label>
                      <input class="form-control" id="kebangsaan" name="kebangsaan" value="" placeholder="Kebangsaan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <input class="form-control" id="alamat" name="alamat" value="" placeholder="Alamat" type="text">
                    </div>
                    <div class="form-group">
                      <label for="pekerjaan">Pekerjaan</label>
                      <input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="Pekerjaan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="penanggung_jawab">Penanggung Jawab</label>
                      <input class="form-control" id="penanggung_jawab" name="penanggung_jawab" value="" placeholder="Penanggung Jawab" type="text">
                    </div>
                    <div class="form-group">
                      <label for="judul_penelitian">Judul Penelitian</label>
                      <textarea class="form-control" rows="3" id="judul_penelitian" name="judul_penelitian" value="" placeholder="" type="text">
                      </textarea>
                    </div>
                    <div class="form-group">
                      <label for="lokasi">Lokasi</label>
                      <input class="form-control" id="lokasi" name="lokasi" value="" placeholder="Lokasi" type="text">
                    </div>
                    <div class="form-group">
                      <label for="asal_universitas">Asal Universitas/ Instansi</label>
                      <input class="form-control" id="asal_universitas" name="asal_universitas" value="" placeholder="Asal Universitas/ Instansi" type="text">
                    </div>
                    <div class="form-group">
                      <label for="nomor_surat_kesbangpol">Nomor Surat Kesbangpol</label>
                      <input class="form-control" id="nomor_surat_kesbangpol" name="nomor_surat_kesbangpol" value="" placeholder="Nomor Surat Kesbangpol" type="text">
                    </div>
                    <div class="form-group">
                      <label for="tanggal_surat_kesbangpol">Tanggal Surat Kesbangpol *format sama</label>
                      <input class="form-control" id="tanggal_surat_kesbangpol" name="tanggal_surat_kesbangpol"  placeholder="Tanggal Surat Kesbangpol" type="date" value="<?php echo(date('Y-m-d')); ?>">
                    </div>
                    <div class="form-group">
                      <label for="surat_ditujukan_kepada">Surat Ditujukan Kepada</label>
                      <input class="form-control" id="surat_ditujukan_kepada" name="surat_ditujukan_kepada" value="" placeholder="Surat Ditujukan Kepada" type="text">
                    </div>
                    <div class="form-group" style="display: none;">
                      <div class="alert alert-info alert-dismissable">
                        <div class="form-group">
                          <label for="remake">Pilih satu per satu hingga keduannya Anda Upload</label>
                          <select class="form-control" id="remake" name="remake" >
                          <option value="scan_ijin_dari_kesbangpol">Scan Ijin dari Kesbangpol</option>
                          <option value="scan_halaman_judul_proposal">Scan Halaman Judul Proposal</option>
                          </select>
                        </div>
                        <div class="form-group">
                          <label for="myfile">Setelah pilih cari file disini</label>
                          <input type="file" size="60" name="myfile" id="file_lampiran" >
                        </div>
                        <div id="ProgresUpload">
                          <div id="BarProgresUpload"></div>
                          <div id="PersenProgresUpload">0%</div >
                        </div>
                        <div id="PesanProgresUpload"></div>
                      </div>
                      <div class="alert alert-info alert-dismissable">
                        <h3 class="card-title">Data Scan </h3>
                        <table class="table table-bordered">
                          <tr>
                            <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                          </tr>
                          <tbody id="tbl_lampiran_perijinan_penelitian">
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="tembusan_kepada_1">Tembusan Kepada</label>
                      <input class="form-control" id="tembusan_kepada_1" name="tembusan_kepada_1" value="-" placeholder="Tembusan Kepada" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <input class="form-control" id="tembusan_kepada_2" name="tembusan_kepada_2" value="-" placeholder="Tembusan Kepada" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <input class="form-control" id="tembusan_kepada_3" name="tembusan_kepada_3" value="-" placeholder="Tembusan Kepada" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <input class="form-control" id="tembusan_kepada_4" name="tembusan_kepada_4" value="-" placeholder="Tembusan Kepada" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <input class="form-control" id="tembusan_kepada_5" name="tembusan_kepada_5" value="-" placeholder="Tembusan Kepada" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <input class="form-control" id="tembusan_kepada_6" name="tembusan_kepada_6" value="-" placeholder="Tembusan Kepada" type="text">
                    </div>                
                </div>
                 <div class="modal-footer">                                            
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      <button type="submit" id="btnSimpan" name="btnSimpan" class="btn btn-success">Simpan</button>
                 </div>
            </div>
        </div>
      </div>
    </form>     
  </div>
</div>

<section class="content" id="awal">
  <div class="pad">
        <button class="btn btn-success" data-toggle="modal" data-target="#myModal">Daftarkan Penelitian</button>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>NO</th>
							<th>Nama</th>
							<th>Judul Penelitian</th>
							<th>Lokasi</th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody id="tbl_utama_perijinan_penelitian7">
					</tbody>
				</table>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_table();
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#klik_tab_tampil').on('change', function(e) {
      e.preventDefault();
      load_table();
    });
  });
</script>

<script>
  function load_table() {
    $('#tbl_utama_perijinan_penelitian7').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>perijinan_penelitian/load_table_perijinan_penelitian8/',
      success: function(html) {
        $('#tbl_utama_perijinan_penelitian7').html(''+html+'');
      }
    });
  }
</script>
