
<style>
.dip{
  text-transform: uppercase;
}
</style>
  <center class="background-white row">
    <div class="form-group" style="">
    <h3>Data Informasi Serta Merta</h3>
    </div>
    <div class="form-group" style="">
      <input name="tabel" id="tabel" value="posting" type="hidden" value="">
      <input name="page" id="page" value="1" type="hidden" value="">
      <input name="keyword" class="form-control" style="width: 350px;" placeholder="Search" type="text" id="keyword">
    </div>
    <div class="form-group">
      <select name="orderby" class="form-control input-sm pull-right" style="display:none;" id="orderby">
        <option value="posting.judul_posting">Judul</option>
      </select>
    </div>
    <div class="form-group">
      <select name="klasifikasi_informasi_publik" class="form-control" style="width: 350px; display:none;" id="klasifikasi_informasi_publik">
        <option value="informasi_serta_merta">Informasi Serta Merta</option>
      </select>
    </div>
    <div class="form-group">
      <select name="limit" class="form-control input-sm pull-right" style="display:none;" id="limit">
        <option value="10">10 Per-Halaman</option>
        <option value="20">20 Per-Halaman</option>
        <option value="50">50 Per-Halaman</option>
        <option value="100">100 Per-Halaman</option>
        <option value="999999999">Semua</option>
      </select>
    </div>
    <div class="form-group">
      <div class="input-group-btn">
        <button class="btn btn-default" id="tampilkan_data_posting_get" style="width: 150px;"><i class="fa fa-search"></i> Tampil</button>
      </div>
    </div>
    <div class="form-group">
      <table class="table table-hover">
        <tbody id="tbl_data_posting_get">
        </tbody>
      </table>
    </div>
    <div class="form-group">
      <ul class="pagination pagination-sm m-0 float-right" id="pagination">
      </ul>
      <div class="overlay" id="spinners_tbl_data_posting_get" style="display:none;">
        <i class="fa fa-refresh fa-spin"></i>
      </div>
    </div>
  </center>
  
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var klasifikasi_informasi_publik = $('#klasifikasi_informasi_publik').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        klasifikasi_informasi_publik:klasifikasi_informasi_publik
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>daftar_informasi_publik/total_data_get',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '" klasifikasi_informasi_publik="' + klasifikasi_informasi_publik + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var klasifikasi_informasi_publik = $('#klasifikasi_informasi_publik').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_posting_get').html('');
    $('#spinners_tbl_data_posting_get').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        klasifikasi_informasi_publik:klasifikasi_informasi_publik
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>daftar_informasi_publik/json_all_posting_get/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_posting_get"]').tab('show');
        $('#tbl_data_posting_get').html(html);
        $('#spinners_tbl_data_posting_get').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    load_data(tabel);
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_posting_get').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>