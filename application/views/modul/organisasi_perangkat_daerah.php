          
                  <table class="table table-responsive table-hover">
										<tbody id="organisasi_perangkat_daerah">
										</tbody>
									</table>
<?php 
$urls =  $this->uri->segment(4);
?>
<script>
  function load_organisasi_perangkat_daerah() {
    $('#informasi_terkait').html('');
    $('#spinners_informasi_terkait').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        alamat_url:'<?php echo $urls; ?>'
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>website/load_organisasi_perangkat_daerah/',
      success: function(html) {
        $('#organisasi_perangkat_daerah').html(html);
				$('#spinners_load_posting_kecamatan').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  load_organisasi_perangkat_daerah();
});
</script>