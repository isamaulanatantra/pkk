<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_1">
        
      </div>
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="col-md-8">
			<div class="box box-primary box-solid">
				<div class="box-header with-border">
					<h3 class="box-title" id="judul_formulir">KOMENTAR</h3>
				</div>
				<div class="box-body">
					<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=komentar" enctype="multipart/form-data">
						<div class="box-body">
							<div class="form-group" style="display:none;">
								<label for="temp">temp</label>
								<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="mode">mode</label>
								<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="id_komentar">id_komentar</label>
								<input class="form-control" id="id_komentar" name="id" value="" placeholder="id_komentar" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="id_posting">id_posting</label>
								<input class="form-control" id="id_posting" name="id_posting" value="<?php echo $this->uri->segment(3); ?>" placeholder="id_posting" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="parent">parent</label>
								<select class="form-control" id="parent" name="parent" >
								<option value="0">Tidak</option>
								<option value="1">Ya</option>
								</select>
							</div>
							<div class="form-group">
								<label for="nama">Nama</label>
								<input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
							</div>
							<div class="form-group">
								<label for="alamat">Alamat</label>
								<input class="form-control" id="alamat" name="alamat" value="" placeholder="Alamat" type="text">
							</div>
							<div class="form-group">
								<label for="nomor_telp">Nomor telp</label>
								<input class="form-control" id="nomor_telp" name="nomor_telp" value="" placeholder="081090909090" type="text">
							</div>
							<div class="form-group">
								<label for="email">Email</label>
								<input class="form-control" id="email" name="email" value="" placeholder="email@mail.id" type="text">
							</div>
							<div class="form-group">
								<label for="isi_komentar">Isi Komentar</label>
								<textarea class="form-control" rows="3" id="isi_komentar" name="isi_komentar" value="" placeholder="" type="text">
								</textarea>
							</div>
						</div>
						<div class="box-footer">
							<div class="overlay" id="overlay_form_input" style="display:none;">
								<i class="fa fa-refresh fa-spin"></i> Berhasil dikirim.
							</div>
							<button type="submit" class="btn btn-primary" id="simpan_komentar"><i class="fa fa-sand"></i>Kirim Komentar</button>
							<button type="submit" class="btn btn-primary" id="update_komentar" style="display:none;">UPDATE</button>
						</div>
					</form>
				</div>
			</div>
			<h4><u>Daftar Komentar</u></h4>
			<u><span id="total_hit_komentar"></span></u>
			<table class="table table-bordered table-hover">
				<tbody id="tbl_tampil_komen">
				</tbody>
			</table>
			<div class="overlay" id="spinners_data" style="display:none;">
				<i class="fa fa-refresh fa-spin"></i>
			</div>
		  </div>
		</div>
      </div>
    </div>
  </div>
</section>

<script>
  function AfterSavedPermohonan_informasi_publik() {
    $('#id_komentar, #id_posting,  #nama, #alamat, #nomor_telp,  #email, #isi_komentar, #parent, #temp').val('');
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
	load_tampil_komen();
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_komentar').show();
    $('#update_komentar').hide();
    $('#tbl_attachment_komentar').html('');
    $('#id_komentar, #id_posting,  #nama, #alamat, #parent, #nomor_telp,  #email, #isi_komentar, #temp').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORM');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_komentar').on('click', function(e) {
      e.preventDefault();
      $('#simpan_komentar').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
      $('#pesan_terkirim').show();
      var parameter = [ 'id_posting', 'nama', 'alamat', 'isi_komentar', 'nomor_telp', 'email', 'parent', 'temp' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_posting"] = $("#id_posting").val();
      parameter["nama"] = $("#nama").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["isi_komentar"] = $("#isi_komentar").val();
      parameter["parent"] = $("#parent").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["temp"] = $("#temp").val();
      var url = '<?php echo base_url(); ?>komentar/simpan_komentar';
      
      var parameterRv = [ 'id_posting', 'nama', 'alamat', 'isi_komentar', 'nomor_telp', 'email', 'parent', 'temp' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
				$('#overlay_form_input').fadeOut();
				alert('gagal');
				$('html, body').animate({
					scrollTop: $('#awal').offset().top
				}, 1000);
      }
      else{
        SimpanData(parameter, url);
				$('#simpan_komentar').removeAttr('disabled', 'disabled');
				$('#overlay_form_input').fadeOut('slow');
				$('#pesan_terkirim').fadeOut('slow');
      }
    });
  });
</script>

<script>
  function load_tampil_komen(halaman, limit) {
    $('#tbl_tampil_komen').html('');
    $('#spinners_data').show();
    var id_posting = $('#id_posting').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_posting: id_posting,
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>komentar/load_tampil_kometar/',
      success: function(html) {
        $('#tbl_tampil_komen').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script>
	  function Load_total_hit_komentar() {
    	var id_posting = $('#id_posting').val();
		$.ajax({
		  type: 'POST',
		  async: true,
		  data: {
			id_posting:id_posting
		  },
		  dataType: 'html',
		  url: '<?php echo base_url(); ?>komentar/total_hit_komentar/',
		  success: function(html) {
			$('#total_hit_komentar').html('Total : '+html+' ');
		  }
		});
	  }
</script>
<script type="text/javascript">
	$(document).ready(function() {
	  Load_total_hit_komentar();
	});
</script>