<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_1">
        
      </div>
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                DAFTAR INFORMASI PUBLIK
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="display:none; width: 150px;" placeholder="Search" type="text" id="kata_kunci">
                  <select name="limit_data_daftar_informasi_publik_pembantu_domain" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_daftar_informasi_publik_pembantu_domain">
                    <option value="999999999">Semua</option>
                    <option value="10">10 Per-Halaman</option>
                    <option value="20">20 Per-Halaman</option>
                    <option value="50">50 Per-Halaman</option>
                  </select>
                  <select name="urut_data_daftar_informasi_publik_pembantu_domain" class="form-control input-sm pull-right" id="urut_data_daftar_informasi_publik_pembantu_domain" style="display:none;">
                    <option value="judul_posting">Berdasarkan Ringkasan Informasi</option>
                  </select>
                  <select name="klasifikasi_informasi_publik_pembantu_domain" class="form-control input-sm pull-right" style="width: 180px;" id="klasifikasi_informasi_publik_pembantu_domain">
                    <!-- <option value="">Semua Klasifikasi</option> -->
                    <option value="informasi_serta_merta">Informasi Serta Merta</option>
                    <option value="informasi_berkala">Informasi Berkala</option>
                    <option value="informasi_setiap_saat">Informasi Setiap Saat</option>
                    <option value="informasi_dikecualikan">Informasi Dikecualikan</option>
                  </select>
                  <select name="opd_domain" class="form-control input-sm pull-right" id="opd_domain" style="width: 280px;">
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default" id="tampilkan_daftar_informasi_publik_pembantu_domain"><i class="fa fa-search"></i> Tampil</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-body table-responsive no-padding">
                        <div id="tblExport">
                          <table class="table table-hover">
                            <thead>
                              <tr id="header_exel">
                                <th>NO</th>
                                <th>Ringkasan Informasi Publik</th>
                                <!--<th>Kategori</th>-->
                                <th>Update</th>
                                <th></th>
                              </tr>
                            </thead>
                            <tbody id="tbl_utama_daftar_informasi_publik_pembantu_domain">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="row">
                        
                        
                        
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1;
		var klasifikasi_informasi_publik_pembantu_domain = $('#klasifikasi_informasi_publik_pembantu_domain').val();
		var opd_domain = $('#opd_domain').val();
    load_data_daftar_informasi_publik_pembantu_domain(halaman, klasifikasi_informasi_publik_pembantu_domain);
});
</script>

<script>
  function load_data_daftar_informasi_publik_pembantu_domain(halaman, klasifikasi_informasi_publik_pembantu_domain, opd_domain) {
    $('#tbl_utama_daftar_informasi_publik_pembantu').html('');
    $('#spinners_data').show();
		var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu_domain').val();
		var opd_domain = $('#opd_domain').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        opd_domain: opd_domain,
        klasifikasi_informasi_publik_pembantu_domain: klasifikasi_informasi_publik_pembantu_domain,
        halaman: halaman
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/load_table_daftar_informasi_publik_pembantu_domain/',
      success: function(html) {
        $('#tbl_utama_daftar_informasi_publik_pembantu_domain').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<!----------------------->
<script>
  function load_opd_domain() {
    $('#opd_domain').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>daftar_informasi_publik_pembantu/opd_domain/',
      success: function(html) {
        $('#opd_domain').html('<option value="">Semua OPD</option>'+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_opd_domain();
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilkan_daftar_informasi_publik_pembantu_domain').on('click', function(e) {
      e.preventDefault();
      var halaman = 1;
      var klasifikasi_informasi_publik_pembantu_domain = $('#klasifikasi_informasi_publik_pembantu_domain').val();
      var opd_domain = $('#opd_domain').val();
      load_data_daftar_informasi_publik_pembantu_domain(halaman, klasifikasi_informasi_publik_pembantu_domain, opd_domain);
    });
  });
</script>
