<section class="content" id="awal">
  <div class="pad">
    <ul class="nav nav-tabs">
			<li class=""><a class="" href="#tab_2" data-toggle="tab" id="klik_tab_input">Persyaratan</a></li>
			<li class=""><a class="" href="#tab_3" data-toggle="tab" id="klik_tab_tampil">Data Permohonan Pembuatan Website</a></li>
			<li class="active"><a class="" href="#tab_4" data-toggle="tab" id="klik_tab_tampil_datadesa">Data Website Desa</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_2">
        <br />
					<h3 class="" id="judul_formulir">PERSYARATAN PERMOHONAN PEMBUATAN WEBSITE DESA</h3>
					<h3>Dapat di download dibawah ini:</h4>
          <p>
						1. <a href="https://diskominfo.wonosobokab.go.id/media/upload/20180321120807_Surat_Pengajuan.docx" target="_blank"><i class="fa fa-file-word-o text-blue"></i> Surat Permohonan Domain</a><br />
						2. <a href="https://diskominfo.wonosobokab.go.id/media/upload/20180321120825_Surat_Kuasa.docx" target="_blank"><i class="fa fa-file-word-o text-blue"></i> Surat Kuasa</a><br />
						3. Fotokopi KTP pengelola Website<br />
					</p>
					<p>Pemberitahuan telah posting tanggal 21 Maret 2018 <a href="https://diskominfo.wonosobokab.go.id/postings/detail/1971/Permohonan_Pembuatan_Website_Resmi_Desa.HTML" target="_blank">Permohonan Pembuatan Website Resmi Desa</a></p>
      </div>
      <div class="tab-pane" id="tab_3">
        <br />
				<h3 class="" id="judul_formulir">DATA PERMOHONAN PEMBUATAN WEBSITE</h3>
				<h4>Jumlah : 
				<?php
					$web=$this->uut->namadomain(base_url());
					$ww = $this->db->query("
						SELECT *, kecamatan.nama_kecamatan, desa.nama_desa 
						from permohonan_pembuatan_website, kecamatan, desa
						where permohonan_pembuatan_website.status = 1 
						and permohonan_pembuatan_website.domain='".$web."'
						and permohonan_pembuatan_website.id_desa=desa.id_desa
						and permohonan_pembuatan_website.id_kecamatan=kecamatan.id_kecamatan
						order by kecamatan.id_kecamatan
						");
					echo $ww->num_rows();
				?>
				</h4>
				<table class="table table-hover">
					<thead>
						<tr id="header_exel">
							<th>NO</th>
							<th>Kecamatan</th>
							<th>Desa</th>
							<th>Domain</th>
							<th>Persyaratan</th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody id="tbl_utama_permohonan_pembuatan_website">
					</tbody>
				</table>
      </div>
      <div class="tab-pane fade in  active" id="tab_4">
        <br />
				<h3 class="" id="judul_formulir">DATA WEBSITE DESA</h3>
				<h4>Jumlah : 
				<?php
					$w = $this->db->query("
						SELECT *, data_desa.desa_website, kecamatan.nama_kecamatan, desa.nama_desa 
						from dasar_website, data_desa, kecamatan, desa
						where dasar_website.status = 1 
						and dasar_website.domain=data_desa.desa_website
						and data_desa.id_desa=data_desa.id_desa
						and data_desa.id_desa=desa.id_desa
						and data_desa.jenis_pemerintahan=0
						and desa.id_kecamatan=kecamatan.id_kecamatan
						order by kecamatan.id_kecamatan
						");
					echo $w->num_rows();
				?>
				</h4>
				<table class="table table-hover table-bordered">
					<thead>
						<tr>
							<th>NO</th>
							<th>Kecamatan</th>
							<th>Desa</th>
							<th>Domain</th>
						</tr>
					</thead>
					<tbody id="tbl_utama_data_website_desa">
					</tbody>
				</table>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script>
  function load_option_kecamatan() {
    $('#id_kecamatan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/option_kecamatan_by_id_kabupaten/',
      success: function(html) {
        $('#id_kecamatan').html('<option value="semua">Pilih Kecamatan</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_option_kecamatan();
  load_table();
  load_table_website_desa();
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var id_kecamatan = $('#id_kecamatan').val();
      load_option_desa(id_kecamatan);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#klik_tab_tampil').on('change', function(e) {
      e.preventDefault();
      load_table();
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#klik_tab_tampil_datadesa').on('change', function(e) {
      e.preventDefault();
      load_table_website_desa();
    });
  });
</script>
<script>
  function load_option_desa(id_kecamatan) {
    $('#id_desa').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html('<option value="semua">Pilih Desa</option>  '+html+'');
      }
    });
  }
</script>

<script>
  function load_table() {
    $('#tbl_utama_permohonan_pembuatan_website').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/load_table/',
      success: function(html) {
        $('#tbl_utama_permohonan_pembuatan_website').html(''+html+'');
      }
    });
  }
</script>

<script>
  function load_table_website_desa() {
    $('#tbl_utama_data_website_desa').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/load_table_data_website_desa/',
      success: function(html) {
        $('#tbl_utama_data_website_desa').html(''+html+'');
      }
    });
  }
</script>
