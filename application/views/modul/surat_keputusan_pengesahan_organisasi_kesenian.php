
    <section class="content" style="display:none;">
			<div class="row" id="awal">
				<div class="col-12">
					<div class="card">
						<div class="card-header p-0">
							<ul class="nav nav-pills ml-auto p-2">
								<li class="nav-item"><a class="nav-link active" href="#tab_form_permohonan_organisasi" data-toggle="tab" id="klik_tab_input">Form</a></li>
							</ul>
						</div>
						<div class="card-body">
							<div class="tab-content">
								<div class="tab-pane active" id="tab_form_permohonan_organisasi">
									<input name="tabel" id="tabel" value="permohonan_organisasi" type="hidden" value="">
									<form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=permohonan_organisasi" enctype="multipart/form-data">
										<input name="page" id="page" value="1" type="hidden" value="">
										<div class="form-group" style="display:none;">
											<label for="temp">temp</label>
											<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="mode">mode</label>
											<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_permohonan_organisasi">id_permohonan_organisasi</label>
											<input class="form-control" id="id_permohonan_organisasi" name="id" value="" placeholder="id_permohonan_organisasi" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="icon">Icon</label>
											<select class="form-control" id="icon" name="icon" >
											<option value="fa-home">fa-home</option>
											<option value="fa-gears">fa-gears</option>
											<option value="fa-th">fa-th</option>
											<option value="fa-font">fa-font</option>
											<option value="fa-comment">fa-comment</option>
											<option value="fa-cogs">fa-cogs</option>
											<option value="fa-cloud-download">fa-cloud-download</option>
											<option value="fa-bar-char">fa-bar-char</option>
											<option value="fa-phone">fa-phone</option>
											<option value="fa-envelope">fa-envelope</option>
											<option value="fa-link">fa-link</option>
											<option value="fa-tasks">fa-tasks</option>
											<option value="fa-users">fa-users</option>
											<option value="fa-signal">fa-signal</option>
											<option value="fa-coffee">fa-coffee</option>
											</select>
											<div id="iconselected"></div>
										</div>
										<div class="form-group" style="display:none;">
											<label for="judul_permohonan_organisasi">Judul Permohonan_organisasi</label>
											<input class="form-control" id="judul_permohonan_organisasi" name="judul_permohonan_organisasi" value="Permohonan Surat Pengesahan" placeholder="Judul Permohonan_organisasi" type="text">
										</div>
										<div class="form-group">
											<label for="nomor_permohonan_organisasi">Nomor</label>
											<input class="form-control" id="nomor_permohonan_organisasi" name="nomor_permohonan_organisasi" value="" placeholder="Nomor Permohonan_organisasi" type="text">
										</div>
										<div class="form-group">
											<label for="perihal_permohonan_organisasi">Perihal</label>
											<select class="form-control" id="perihal_permohonan_organisasi" name="perihal_permohonan_organisasi">
											</select>
										</div>
										<div class="form-group" style="display:none;">
											<label for="yth_permohonan_organisasi">Yth.</label>
											<input class="form-control" id="yth_permohonan_organisasi" name="yth_permohonan_organisasi" value="Kepala Dinas Pariwisata dan Kebudayaan Kabupaten Wonosobo" placeholder="Judul Permohonan_organisasi" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="cq_permohonan_organisasi">Cq.</label>
											<input class="form-control" id="cq_permohonan_organisasi" name="cq_permohonan_organisasi" value="Kepala Dinas Pariwisata dan Kebudayaan Kabupaten Wonosobo" placeholder="Judul Permohonan_organisasi" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="di_permohonan_organisasi">Di.</label>
											<input class="form-control" id="di_permohonan_organisasi" name="di_permohonan_organisasi" value="Wonosobo" placeholder="Judul Permohonan_organisasi" type="text">
										</div>
										<div class="form-group">
											<label for="tanggal_permohonan_organisasi">Tanggal</label>
											<input class="form-control" id="tanggal_permohonan_organisasi" name="tanggal_permohonan_organisasi" value="<?php echo date('Y-m-d'); ?>" placeholder="Judul Permohonan_organisasi" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="isi_permohonan_organisasi">Isi Permohonan_organisasi</label>
											<input class="form-control" id="isi_permohonan_organisasi" name="isi_permohonan_organisasi" value="" placeholder="Isi Permohonan_organisasi" type="text">
										</div>
										<div class="row" style="display:none;">
										<textarea id="editor_isi_permohonan_organisasi">Permohonan_organisasi</textarea>
										</div>
										<div class="form-group" style="display:none;">
											<label for="sifat_permohonan_organisasi">Sifat</label>
											<input class="form-control" id="sifat_permohonan_organisasi" name="sifat_permohonan_organisasi" value="Penting" placeholder="Sifat" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="lampiran_permohonan_organisasi">Lampiran</label>
											<input class="form-control" id="lampiran_permohonan_organisasi" name="lampiran_permohonan_organisasi" value="1 (Satu) bendel" placeholder="Lampiran" type="text">
										</div>
										<div class="form-group">
											<label for="nama_organisasi">Nama Organisasi</label>
											<input class="form-control" id="nama_organisasi" name="nama_organisasi" value="" placeholder="Nama Organisasi" type="text">
										</div>
										<div class="form-group">
											<label for="kesenian">Kesenian</label>
											<input class="form-control" id="kesenian" name="kesenian" value="" placeholder="Kesenian" type="text">
										</div>
										<div class="form-group">
											<label for="jenis_kesenian">Jenis Kesenian</label>
											<select class="form-control" id="jenis_kesenian" name="jenis_kesenian">
											</select>
										</div>
										<div class="form-group">
											<label for="id_propinsi">Provinsi</label>
											<select class="form-control" id="id_propinsi" name="id_propinsi">
											</select>
										</div>
										<div class="form-group">
											<label for="id_kabupaten">Kabupaten</label>
											<select class="form-control" id="id_kabupaten" name="id_kabupaten">
											</select>
										</div>
										<div class="form-group">
											<label for="id_kecamatan">Kecamatan</label>
											<select class="form-control" id="id_kecamatan" name="id_kecamatan">
											</select>
										</div>
										<div class="form-group">
											<label for="id_desa">Desa</label>
											<select class="form-control" id="id_desa" name="id_desa">
											</select>
										</div>
										<div class="form-group">
											<label for="nama_pemohon">Nama pemohon</label>
											<input class="form-control" id="nama_pemohon" name="nama_pemohon" value="" placeholder="Nama pemohon" type="text">
										</div>
										<div class="form-group">
											<label for="tempat_lahir_pemohon">Tempat Lahir</label>
											<input class="form-control" id="tempat_lahir_pemohon" name="tempat_lahir_pemohon" value="" placeholder="Tempat Lahir" type="text">
										</div>
										<div class="form-group">
											<label for="tanggal_lahir_pemohon">Tanggal Lahir</label>
											<input class="form-control" id="tanggal_lahir_pemohon" name="tanggal_lahir_pemohon" value="" placeholder="Tanggal Lahir" type="text">
										</div>
										<div class="form-group">
											<label for="pekerjaan_pemohon">Pekerjaan</label>
											<select class="form-control" id="pekerjaan_pemohon" name="pekerjaan_pemohon">
											</select>
										</div>
										<div class="form-group">
											<label for="alamat_pemohon">Alamat</label>
											<input class="form-control" id="alamat_pemohon" name="alamat_pemohon" value="" placeholder="Alamat" type="text">
										</div>
										<div class="form-group">
											<label for="id_propinsi_pemohon">Provinsi</label>
											<select class="form-control" id="id_propinsi_pemohon" name="id_propinsi_pemohon">
											</select>
										</div>
										<div class="form-group">
											<label for="id_kabupaten_pemohon">Kabupaten</label>
											<select class="form-control" id="id_kabupaten_pemohon" name="id_kabupaten_pemohon">
											</select>
										</div>
										<div class="form-group">
											<label for="id_kecamatan_pemohon">Kecamatan</label>
											<select class="form-control" id="id_kecamatan_pemohon" name="id_kecamatan_pemohon">
											</select>
										</div>
										<div class="form-group">
											<label for="id_desa_pemohon">Desa</label>
											<select class="form-control" id="id_desa_pemohon" name="id_desa_pemohon">
											</select>
										</div>
										<div class="form-group">
											<label for="nomor_telp_pemohon">Nomor Telp./Hp</label>
											<input class="form-control" id="nomor_telp_pemohon" name="nomor_telp_pemohon" value="" placeholder="Nomor Telp./Hp" type="text">
										</div>
										<div class="form-group">
											<label for="tanggal_pendirian">Tanggal Pendirian</label>
											<input class="form-control" id="tanggal_pendirian" name="tanggal_pendirian" value="" placeholder="Tanggal Pendirian" type="text">
										</div>
										<div class="form-group">
											<label for="jumlah_anggota">Jumlah anggota</label>
											<input class="form-control" id="jumlah_anggota" name="jumlah_anggota" value="" placeholder="Jumlah anggota" type="text">
										</div>
										<div class="form-group">
											<label for="jumlah_anggota_pria">Jumlah anggota pria</label>
											<input class="form-control" id="jumlah_anggota_pria" name="jumlah_anggota_pria" value="" placeholder="Jumlah anggota pria" type="text">
										</div>
										<div class="form-group">
											<label for="jumlah_anggota_wanita">Jumlah anggota wanita</label>
											<input class="form-control" id="jumlah_anggota_wanita" name="jumlah_anggota_wanita" value="" placeholder="Jumlah anggota wanita" type="text">
										</div>
												<div class="card">
													<div class="card-header">
														<h5 class="card-title">Profil Organisasi</h5>
													</div>
													<div class="card-body">
														<div class="form-group">
															<label for="fungsi_kesenian">Fungsi Kesenian</label>
															<select class="form-control" id="fungsi_kesenian" name="fungsi_kesenian">
																<option value="Ritual">Ritual</option>
																<option value="Hiburan">Hiburan</option>
															</select>
														</div>
														<div class="form-group">
															<label for="nama_kesenian">Nama Kesenian</label>
															<input class="form-control" id="nama_kesenian" name="nama_kesenian" value="" placeholder="Nama Kesenian" type="text">
														</div>
														<div class="form-group">
															<label for="pengalaman_pentas">Pengalaman pentas</label>
															<input class="form-control" id="pengalaman_pentas" name="pengalaman_pentas" value="" placeholder="Pengalaman pentas" type="text">
														</div>
														<div class="form-group">
															<label for="penghargaan_yang_pernah_diterima">Penghargaan yang pernah diterima</label>
															<input class="form-control" id="penghargaan_yang_pernah_diterima" name="penghargaan_yang_pernah_diterima" value="" placeholder="Penghargaan yang pernah diterima" type="text">
														</div>
														<div class="form-group">
															<label for="fasilitas_peralatan_yang_dimiliki">Fasilitas peralatan yang dimiliki</label>
															<input class="form-control" id="fasilitas_peralatan_yang_dimiliki" name="fasilitas_peralatan_yang_dimiliki" value="" placeholder="Fasilitas peralatan yang dimiliki" type="text">
														</div>
														<div class="form-group">
															<label for="hambatan_kendala">Hambatan kendala</label>
															<input class="form-control" id="hambatan_kendala" name="hambatan_kendala" value="" placeholder="Hambatan kendala" type="text">
														</div>
														<div class="form-group">
															<label for="riwayat_sejarah">Riwayat / sejarah</label>
															<input class="form-control" id="riwayat_sejarah" name="riwayat_sejarah" value="" placeholder="Riwayat / sejarah" type="text">
														</div>
													</div>
												</div>
												<div class="card">
													<div class="card-header">
														<h5 class="card-title">Anggota</h5>
													</div>
													<div class="card-body">
														<div class="alert alert-info alert-dismissable" style="display:none;">
															<div class="form-group">
																<label for="remake"><i class="fa fa-warning"></i> Sebagai kelengkapan pengajuan kami lampirkan : </label>
																<input class="form-control" id="remake" name="remake" value="" placeholder="Riwayat / sejarah" type="text">
															</div>
															<div class="form-group">
																<label for="myfile">File Lampiran </label>
																<input type="file" size="60" name="myfile" id="file_lampiran" >
															</div>
															<div id="ProgresUpload">
																<div id="BarProgresUpload"></div>
																<div id="PersenProgresUpload">0%</div >
															</div>
															<div id="PesanProgresUpload"></div>
														</div>
														<div class="alert alert-info alert-dismissable table-responsive" style="display:none;">
															<h3 class="card-title">Data Lampiran </h3>
															<table class="table table-bordered">
																<tr>
																	<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
																</tr>
																<tbody id="tbl_attachment_permohonan_pendaftaran_badan_usaha">
																</tbody>
															</table>
														</div>
													</div>
												</div>
										<div class="form-group" style="display:none;">
											<label for="keterangan">Keterangan Permohonan_organisasi</label>
											<input class="form-control" id="keterangan" name="keterangan" value="-" placeholder="Keterangan" type="text">
										</div>
										<div class="form-group">
										<button type="submit" class="btn btn-primary" id="simpan_permohonan_organisasi">SIMPAN</button>
										<button type="submit" class="btn btn-primary" id="update_permohonan_organisasi" style="display:none;">UPDATE</button>
										</div>
									</form>

									<div class="overlay" id="overlay_form_input" style="display:none;">
										<i class="fa fa-refresh fa-spin"></i>
									</div>
								</div>
							</div>
            </div>
          </div>
        </div>
			</div>
    </section>