
				<div class="" style="display:none;">
					<div class="col-md-4 form-group">
						<label for="tahun"></label>
						<select class="form-control" id="tahun" name="tahun" >
						<option value="2018">2018</option>
						<option value="2017">2017</option>
						</select>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
						<tr>
							<th rowspan="2">No.</th>
							<th rowspan="2">JENIS INFORMASI</th>
							<th rowspan="2">RINGKASAN INFORMASI</th>
							<th rowspan="2">PENANGGUNG JAWAB</th>
							<th colspan="3">BENTUK INFORMASI</th> 
							<th colspan="4">KLASIFIKASI INFORMASI</th> 
							<th rowspan="2">RETENSI ARSIP</th>
						</tr>
						<tr>
							<th>ONLINE</th>
							<th>CETAK</th>
							<th>REKAM</th>
							<th>BERKALA</th>
							<th>SETIAP SAAT</th> 
							<th>SERTA MERTA</th> 
							<th>DIKECU ALIKAN</th>
						</tr>
						</thead>
						<tbody id="tbl_utama_dip">
						</tbody>
					</table>
					<div class="overlay" id="spinners_data" style="display:none;">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1;
    load_data_dip(halaman);
});
</script>
 
<script>
  function load_data_dip(halaman) {
    $('#tbl_utama_dip').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>dip/load_publik/',
      success: function(html) {
        $('#tbl_utama_dip').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>