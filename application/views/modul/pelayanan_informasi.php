
<div style="padding: 2px;" id="awal">
												<div class="card-tools">
													<div class="input-group input-group-sm">
                            <input name="tabel" id="tabel" value="permohonan_informasi_publik" type="hidden" value="">
                            <input name="page" id="page" value="1" type="hidden" value="">
                            <select class="form-control input-sm pull-right" style="width: 150px;" id="keyword" name="keyword">
                              <?php 
                              for ($i = 2014; $i <= (date('Y')); $i++) {
                                if (date('Y') == $i) {
                                  echo "<option value='$i' selected>$i</option>";
                                } else {
                                  echo "<option value='$i'>$i</option>";
                                }
                              }
                              ?>
                            </select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="permohonan_informasi_publik.created_time">Tahun</option>
															<option value="permohonan_informasi_publik.instansi">Instansi</option>
															<option value="permohonan_informasi_publik.rincian_informasi_yang_diinginkan">Keperluan</option>
														</select>
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px; display:none;" id="limit">
															<option value="20">20 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
															<option value="999999999">Semua</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_permohonan_informasi_publik"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
												<hr />
                        <ul class="portfolio-group" id="tbl_data_permohonan_informasi_publik">
                        </ul>
</div>

												<br />
												<hr />
                      <center>
												<br />
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_permohonan_informasi_publik" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i> Mohon tunggu...
												</div>
                      </center>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tahun = $('#tahun').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        tahun:tahun
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>pelayanan_informasi/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '" tahun="' + tahun + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tahun = $('#tahun').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        tahun:tahun
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>pelayanan_informasi/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    load_data(tabel);
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>