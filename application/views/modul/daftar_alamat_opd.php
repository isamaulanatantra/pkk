
				<div class="" style="display:none;">
					<div class="col-md-4 form-group">
						<label for="tahun"></label>
						<select class="form-control" id="tahun" name="tahun" >
						<option value="2018">2018</option>
						<option value="2017">2017</option>
						</select>
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-bordered table-striped">
						<thead>
							<th>No.</th>
							<th>Nama OPD</th>
							<th>Alamat</th>
							<th>No. Telp</th>
							<th>Email</th><!--
							<th>Domain</th>
							<th>Twitter</th>
							<th>Facebook</th>
							<th>Google+</th>
							<th>Instagram</th>-->
						</thead>
						<tbody id="tbl_utama_daftar_alamat_opd">
						</tbody>
					</table>
					<div class="overlay" id="spinners_data" style="display:none;">
						<i class="fa fa-refresh fa-spin"></i>
					</div>
				</div>
				
<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1;
    var limit = limit_per_page_custome(20000);
    load_data_dip(halaman, limit);
});
</script>
 
<script>
  function load_data_dip(halaman, limit) {
    $('#tbl_utama_daftar_alamat_opd').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>dip/load_daftar_alamat_opd/',
      success: function(html) {
        $('#tbl_utama_daftar_alamat_opd').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>