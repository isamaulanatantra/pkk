          
                  <div class="" id="load_tentang_kami">
                  </div>
<?php 
$urls =  $this->uri->segment(4);
?>
<script>
  function load_tentang_kami() {
    $('#informasi_terkait').html('');
    $('#spinners_informasi_terkait').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        parent:'<?php echo $urls; ?>'
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>website/load_tentang_kami/',
      success: function(html) {
        $('#load_tentang_kami').html(html);
				$('#spinners_load_tentang_kami').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  load_tentang_kami();
});
</script>