
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><b>INFORMASI STOK DARAH</b></h1><h1><b><font color="red">- UNDER CONSTRUCTION</font></b></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Informasi</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
<section class="content" id="awal">
  <div class="card">
    <ul class="nav nav-tabs">
			<li class="nav-item"><a class="nav-link active" href="#tab_form_informasi_stok_darah" data-toggle="tab" id="klik_tab_input">Form</a></li>
			<li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab" id="klik_tab_data_informasi_stok_darah">Data Informasi Stok Darah</a></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_form_informasi_stok_darah">
        <div class="pad">
          <div class="card card-primary">
            <div class="card-body">
              <input name="page" id="page" value="1" type="hidden" value="">
              <input name="tabel" id="tabel" value="informasi_stok_darah" type="hidden" value="">
            <!--
              <h3 class="" id="judul_formulir">FORMULIR PERMOHONAN PEMBUATAN WEBSITE</h3>-->
              <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>lampiran_informasi_stok_darah/upload/?table_name=informasi_stok_darah" enctype="multipart/form-data">
                <div class="card-body">
                  <div class="form-group" style="display:none;">
                    <label for="temp">temp</label>
                    <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="mode">mode</label>
                    <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="id_informasi_stok_darah">id_informasi_stok_darah</label>
                    <input class="form-control" id="id_informasi_stok_darah" name="id" value="" placeholder="id_informasi_stok_darah" type="text">
                  </div>
                  <div class="form-group">
                    <label for="gol_a">Stok Golongan Darah A</label>
                    <input class="form-control" id="gol_a" name="gol_a" value="" placeholder="Stok Golongan Darah A" type="text">
                  </div>
                  <div class="form-group">
                    <label for="gol_b">Stok Golongan Darah B</label>
                    <input class="form-control" id="gol_b" name="gol_b" value="" placeholder="Stok Golongan Darah B" type="text">
                  </div>
                  <div class="form-group">
                    <label for="gol_ab">Stok Golongan Darah AB</label>
                    <input class="form-control" id="gol_ab" name="gol_ab" value="" placeholder="Stok Golongan Darah AB" type="text">
                  </div>
                  <div class="form-group">
                    <label for="gol_o">Stok Golongan Darah O</label>
                    <input class="form-control" id="gol_o" name="gol_o" value="" placeholder="Stok Golongan Darah O" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="nik">NIK</label>
                    <input class="form-control" id="nik" name="nik" value="" placeholder="nik" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="nama">Nama Pemohon</label>
                    <input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="tanggal_pengambilan">Tanggal Pengambilan</label>
                    <input class="form-control" id="tanggal_pengambilan" name="tanggal_pengambilan" value="" placeholder="tanggal_pengambilan" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label>Golongan Darah</label>
                    <select class="form-control" id="id_golongan_darah"></select>
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="nomor_telp">Nomor Telp Pemohon</label>
                    <input class="form-control" id="nomor_telp" name="nomor_telp" value="" placeholder="Nomot Telp" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="email">Email Pemohon</label>
                    <input class="form-control" id="email" name="email" value="" placeholder="email" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="alamat">Alamat Pemohon</label>
                    <input class="form-control" id="alamat" name="alamat" value="" placeholder="Alamat" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label for="instansi">Instansi Pemohon</label>
                    <input class="form-control" id="instansi" name="instansi" value="" placeholder="instansi" type="text">
                  </div>
                  <div class="form-group" style="display:none;">
                    <label>Kecamatan</label>
                    <select class="form-control" id="id_kecamatan"></select>
                  </div>
                  <div class="form-group" style="display:none;">
                    <label>Desa</label>
                    <select class="form-control" id="id_desa"></select>
                  </div>
                  <div class="form-group">
                    <div class="alert alert-info alert-dismissable">
                      <div class="form-group">
                        <label for="remake">Pilih satu per satu hingga ketiganya Anda Upload</label>
                        <select class="form-control" id="remake" name="remake" >
                        <option value="Scan Surat Permohonan">Scan Surat Permohonan</option>
                        <option value="Scan Surat Kuasa">Scan Surat Kuasa</option>
                        <option value="Scan KTP">Scan KTP</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="myfile">Setelah pilih persyaratan kemudian cari file disini</label>
                        <input type="file" size="60" name="myfile" id="file_lampiran" >
                      </div>
                      <div id="ProgresUpload">
                        <div id="BarProgresUpload"></div>
                        <div id="PersenProgresUpload">0%</div >
                      </div>
                      <div id="PesanProgresUpload"></div>
                    </div>
                    <div class="alert alert-info alert-dismissable">
                      <label class="">Data Lampiran </label>
                      <table class="table table-hover">
                        <thead>
                          <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                        </thead>
                        <tbody id="tbl_lampiran_informasi_stok_darah">
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="overlay" id="overlay_form_input" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                  </div>
                  <button type="submit" class="btn btn-primary" id="simpan_informasi_stok_darah"><i class="fa fa-sand"></i>KIRIM </button>
                  <button type="submit" class="btn btn-primary" id="update_informasi_stok_darah" style="display:none;">UPDATE</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab_3">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="10">10 Per-Halaman</option>
															<option value="25">25 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
															<option value="999999999">Semua</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="informasi_stok_darah.created_time">Tanggal</option>
															<option value="informasi_stok_darah.nama">Nama</option>
															<option value="informasi_stok_darah.id_kecamatan">Kecamatan</option>
															<option value="informasi_stok_darah.id_desa">Desa</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_informasi_stok_darah"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
                          <tr>
                            <th rowspan="2">NO</th>
                            <th rowspan="2">Tanggal Stok</th>
                            <th colspan="4">Jumlah Kantung Darah</th>
                            <th rowspan="2">Tanggal Entry Data</th>
                            <!--<th>Domain</th>
                            <th>Desa</th>
                            <th>Nama</th> 
                            <th>Alamat</th> 
                            <th>instansi</th> 
                            <th>No.Telp</th> 
                            <th>Email</th> -->
                            <th rowspan="2">Lampiran</th> 
                            <th rowspan="2">Proses</th> 
                          </tr>
                          <tr>
                            <th>Golongan A</th>
                            <th>Golongan B</th>
                            <th>Golongan AB</th>
                            <th>Golongan O</th>
                          </tr>
													<tbody id="tbl_data_informasi_stok_darah">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_informasi_stok_darah" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_pengambilan').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>

<script>
  function AfterSavedPermohonan_informasi_publik() {
    $('#nama, #nomor_telp, #email, #alamat, #instansi, #id_kecamatan, #id_desa #temp').val('');
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_informasi_stok_darah').show();
    $('#update_informasi_stok_darah').hide();
    $('#tbl_lampiran_informasi_stok_darah').html('');
    $('#nama, #nomor_telp, #email, #alamat, #instansi, #id_kecamatan, #id_desa #temp').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORM');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_informasi_stok_darah').on('click', function(e) {
      e.preventDefault();
      $('#simpan_informasi_stok_darah').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
      $('#pesan_terkirim').show();
      var parameter = [ 'gol_a', 'gol_b', 'gol_ab', 'gol_o', /*'nama', 'nomor_telp', 'email', 'alamat', 'instansi', 'id_kecamatan', 'id_desa', 'temp'*/ ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["gol_a"] = $("#gol_a").val();
      parameter["gol_b"] = $("#gol_b").val();
      parameter["gol_ab"] = $("#gol_ab").val();
      parameter["gol_o"] = $("#gol_o").val();
      /*parameter["nama"] = $("#nama").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["instansi"] = $("#instansi").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["temp"] = $("#temp").val();*/
      var url = '<?php echo base_url(); ?>informasi_stok_darah/simpan_informasi_stok_darah';
      var parameterRv = [ 'gol_a', 'gol_b', 'gol_ab', 'gol_o', /*'nama', 'nomor_telp', 'email', 'alamat', 'instansi', 'id_kecamatan', 'id_desa', 'temp'*/ ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
				$('#overlay_form_input').fadeOut();
				alert('gagal');
				$('html, body').animate({
					scrollTop: $('#awal').offset().top
				}, 1000);
      }
      else{
        SimpanData(parameter, url);
				$('#simpan_informasi_stok_darah').removeAttr('disabled', 'disabled');
				$('#overlay_form_input').fadeOut('slow');
				$('#pesan_terkirim').fadeOut('slow');
      }
    });
  });
</script>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_lampiran_informasi_stok_darah').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'informasi_stok_darah',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>lampiran_informasi_stok_darah/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_lampiran_informasi_stok_darah="'+json[i].id_lampiran_informasi_stok_darah+'" id="'+json[i].id_lampiran_informasi_stok_darah+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/lampiran_informasi_stok_darah/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_lampiran_informasi_stok_darah').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_informasi_stok_darah').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_lampiran_informasi_stok_darah').on('click', '#del_ajax', function() {
    var id_lampiran_informasi_stok_darah = $(this).closest('tr').attr('id_lampiran_informasi_stok_darah');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_lampiran_informasi_stok_darah"] = id_lampiran_informasi_stok_darah;
        var url = '<?php echo base_url(); ?>lampiran_informasi_stok_darah/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_informasi_stok_darah').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_lampiran_informasi_stok_darah='+id_lampiran_informasi_stok_darah+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script>
  function load_option_kecamatan() {
    $('#id_kecamatan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>informasi_stok_darah/option_kecamatan_by_id_kabupaten/',
      success: function(html) {
        $('#id_kecamatan').html('<option value="semua">Pilih Kecamatan</option>  '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_option_kecamatan();
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var id_kecamatan = $('#id_kecamatan').val();
      load_option_desa(id_kecamatan);
    });
  });
</script>
<script>
  function load_option_desa(id_kecamatan) {
    $('#id_desa').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>informasi_stok_darah/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html('<option value="semua">Pilih Desa</option>  '+html+'');
      }
    });
  }
</script>

<script>
  function load_wilayah_by_id_desa(id_desa) {
    $('#id_desa').html('');
    $('#id_kecamatan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_desa:id_desa
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_wilayah_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_desa').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
        }
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>informasi_stok_darah/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_informasi_stok_darah').on('click', '#del_ajax_informasi_stok_darah', function() {
    var id_informasi_stok_darah = $(this).closest('tr').attr('id_informasi_stok_darah');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_informasi_stok_darah"] = id_informasi_stok_darah;
        var url = '<?php echo base_url(); ?>informasi_stok_darah/hapus/';
        HapusData(parameter, url);
        $('[id_informasi_stok_darah='+id_informasi_stok_darah+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_informasi_stok_darah').on('click', '.update_id_informasi_stok_darah', function() {
    $('#mode').val('edit');
    $('#simpan_informasi_stok_darah').hide();
    $('#update_informasi_stok_darah').show();
    var id_informasi_stok_darah = $(this).closest('tr').attr('id_informasi_stok_darah');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_informasi_stok_darah');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_informasi_stok_darah').val(id_informasi_stok_darah);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_informasi_stok_darah:id_informasi_stok_darah
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>informasi_stok_darah/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#gol_a').val(json[i].gol_a);
            $('#gol_b').val(json[i].gol_b);
            $('#gol_ab').val(json[i].gol_ab);
            $('#gol_o').val(json[i].gol_o);
            $('#temp').val(json[i].temp);
            $('#tanggal_pengambilan').val(json[i].tanggal_pengambilan);
            $('#nama').val(json[i].nama);
            $('#nomor_telp').val(json[i].nomor_telp);
            $('#nik').val(json[i].nik);
            $('#email').val(json[i].email);
            $('#alamat').val(json[i].alamat);
            $('#instansi').val(json[i].instansi);
						load_wilayah_by_id_desa(json[i].id_desa);
						load_option_kecamatan();
          }
        }
      });
    AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_informasi_stok_darah').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'gol_a', 'gol_b', 'gol_ab', 'gol_o', /*'nomor_telp', 'nama', 'temp', 'nik', 'email', 'instansi', 'alamat', 'id_kecamatan', 'id_desa'*/ ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_informasi_stok_darah"] = $("#id_informasi_stok_darah").val();
      parameter["gol_a"] = $("#gol_a").val();
      parameter["gol_b"] = $("#gol_b").val();
      parameter["gol_ab"] = $("#gol_ab").val();
      parameter["gol_o"] = $("#gol_o").val();
      /*parameter["temp"] = $("#temp").val();
      parameter["nama"] = $("#nama").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["nik"] = $("#nik").val();
      parameter["email"] = $("#email").val();
      parameter["instansi"] = $("#instansi").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();*/
      var url = '<?php echo base_url(); ?>informasi_stok_darah/update_informasi_stok_darah';
      
      var parameterRv = [ 'gol_a', 'gol_b', 'gol_ab', 'gol_o', /*'nomor_telp', 'nama', 'temp', 'nik', 'email', 'instansi', 'alamat', 'id_kecamatan', 'id_desa'*/ ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>
