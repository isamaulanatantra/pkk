
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>KOMPONEN BERANDA</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Jenis_kesenian Beranda</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_form_jenis_kesenian" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_data_jenis_kesenian" data-toggle="tab" id="klik_tab_data_jenis_kesenian">Data</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
									<div class="tab-pane active" id="tab_form_jenis_kesenian">
										<input name="tabel" id="tabel" value="jenis_kesenian" type="hidden" value="">
										<form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=jenis_kesenian" enctype="multipart/form-data">
											<input name="page" id="page" value="1" type="hidden" value="">
											<div class="form-group" style="display:none;">
												<label for="temp">temp</label>
												<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="mode">mode</label>
												<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="id_jenis_kesenian">id_jenis_kesenian</label>
												<input class="form-control" id="id_jenis_kesenian" name="id" value="" placeholder="id_jenis_kesenian" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="icon">Icon</label>
												<select class="form-control" id="icon" name="icon" >
												<option value="fa-home">fa-home</option>
												<option value="fa-gears">fa-gears</option>
												<option value="fa-th">fa-th</option>
												<option value="fa-font">fa-font</option>
												<option value="fa-comment">fa-comment</option>
												<option value="fa-cogs">fa-cogs</option>
												<option value="fa-cloud-download">fa-cloud-download</option>
												<option value="fa-bar-char">fa-bar-char</option>
												<option value="fa-phone">fa-phone</option>
												<option value="fa-envelope">fa-envelope</option>
												<option value="fa-link">fa-link</option>
												<option value="fa-tasks">fa-tasks</option>
												<option value="fa-users">fa-users</option>
												<option value="fa-signal">fa-signal</option>
												<option value="fa-coffee">fa-coffee</option>
												</select>
												<div id="iconselected"></div>
											</div>
											<div class="form-group">
												<label for="judul_jenis_kesenian">Judul Jenis_kesenian</label>
												<input class="form-control" id="judul_jenis_kesenian" name="judul_jenis_kesenian" value="" placeholder="Judul Jenis_kesenian" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="isi_jenis_kesenian">Isi Jenis_kesenian</label>
												<input class="form-control" id="isi_jenis_kesenian" name="isi_jenis_kesenian" value="" placeholder="Isi Jenis_kesenian" type="text">
											</div>
											<div class="row">
											<textarea id="editor_isi_jenis_kesenian"></textarea>
											</div>
											<div class="form-group">
												<label for="keterangan">Keterangan Jenis_kesenian</label>
												<input class="form-control" id="keterangan" name="keterangan" value="" placeholder="Keterangan" type="text">
											</div>
											<div class="form-group">
											<button type="submit" class="btn btn-primary" id="simpan_jenis_kesenian">SIMPAN</button>
											<button type="submit" class="btn btn-primary" id="update_jenis_kesenian" style="display:none;">UPDATE</button>
											</div>
										</form>

										<div class="overlay" id="overlay_form_input" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
									<div class="tab-pane table-responsive" id="tab_data_jenis_kesenian">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
															<option value="999999999">Semua</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="jenis_kesenian.judul_jenis_kesenian">Judul</option>
															<option value="jenis_kesenian.keterangan">Keterangan</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_jenis_kesenian"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<tr>
														<th>NO</th>
														<th>Judul Perihal </th>
														<th>Keterangan</th>
														<th>PROSES</th> 
													</tr>
													<tbody id="tbl_data_jenis_kesenian">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_jenis_kesenian" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>jenis_kesenian/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedJenis_kesenian() {
    $('#id_jenis_kesenian, #judul_jenis_kesenian, #isi_jenis_kesenian, #icon, #keterangan').val('');
    $('#tbl_attachment_jenis_kesenian').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#icon').on('change', function(e) {
      e.preventDefault();
      var xa = $('#icon').val();
      $("#iconselected").html('<span class="fa '+xa+' ">'+xa+'</span>');
    });
  });
</script>
 
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_jenis_kesenian').on('click', function(e) {
      e.preventDefault();
      var editor_isi_jenis_kesenian = CKEDITOR.instances.editor_isi_jenis_kesenian.getData();
      $('#isi_jenis_kesenian').val( editor_isi_jenis_kesenian );
      var parameter = [ 'judul_jenis_kesenian', 'isi_jenis_kesenian', 'keterangan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["judul_jenis_kesenian"] = $("#judul_jenis_kesenian").val();
      parameter["isi_jenis_kesenian"] = $("#isi_jenis_kesenian").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>jenis_kesenian/simpan_jenis_kesenian';
      
      var parameterRv = [ 'judul_jenis_kesenian', 'isi_jenis_kesenian', 'keterangan' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedJenis_kesenian();
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_jenis_kesenian').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_jenis_kesenian').hide();
    $('#update_jenis_kesenian').show();
    var id_jenis_kesenian = $(this).closest('tr').attr('id_jenis_kesenian');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_jenis_kesenian');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_jenis_kesenian').val(id_jenis_kesenian);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_jenis_kesenian:id_jenis_kesenian
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>jenis_kesenian/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#judul_jenis_kesenian').val(json[i].judul_jenis_kesenian);
            $('#icon').val(json[i].icon);
            $('#keterangan').val(json[i].keterangan);
            CKEDITOR.instances.editor_isi_jenis_kesenian.setData(json[i].isi_jenis_kesenian);
          }
        }
      });
    //AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_jenis_kesenian').on('click', function(e) {
      e.preventDefault();
      var editor_isi_jenis_kesenian = CKEDITOR.instances.editor_isi_jenis_kesenian.getData();
      $('#isi_jenis_kesenian').val( editor_isi_jenis_kesenian );
      var parameter = [ 'id_jenis_kesenian', 'judul_jenis_kesenian', 'isi_jenis_kesenian', 'icon', 'keterangan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_jenis_kesenian"] = $("#id_jenis_kesenian").val();
      parameter["judul_jenis_kesenian"] = $("#judul_jenis_kesenian").val();
      parameter["isi_jenis_kesenian"] = $("#isi_jenis_kesenian").val();
      parameter["icon"] = $("#icon").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>jenis_kesenian/update_jenis_kesenian';
      
      var parameterRv = [ 'id_jenis_kesenian', 'judul_jenis_kesenian', 'isi_jenis_kesenian', 'icon', 'keterangan' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<!----------------------->
<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('editor_isi_jenis_kesenian');
	$(".textarea").wysihtml5();
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_jenis_kesenian').on('click', '#del_ajax', function() {
    var id_jenis_kesenian = $(this).closest('tr').attr('id_jenis_kesenian');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_jenis_kesenian"] = id_jenis_kesenian;
        var url = '<?php echo base_url(); ?>jenis_kesenian/hapus/';
        HapusData(parameter, url);
        $('[id_jenis_kesenian='+id_jenis_kesenian+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_jenis_kesenian').on('click', '#inaktifkan', function() {
    var id_jenis_kesenian = $(this).closest('tr').attr('id_jenis_kesenian');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_jenis_kesenian"] = id_jenis_kesenian;
        var url = '<?php echo base_url(); ?>jenis_kesenian/inaktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_jenis_kesenian').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
        $('[id_jenis_kesenian='+id_jenis_kesenian+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_jenis_kesenian').on('click', '#aktifkan', function() {
    var id_jenis_kesenian = $(this).closest('tr').attr('id_jenis_kesenian');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_jenis_kesenian"] = id_jenis_kesenian;
        var url = '<?php echo base_url(); ?>jenis_kesenian/aktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_jenis_kesenian').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
        $('[id_jenis_kesenian='+id_jenis_kesenian+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>