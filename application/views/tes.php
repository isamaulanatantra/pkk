<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<?php
$ses = $this->session->userdata('id_users');
if (!$ses) {
  return redirect('' . base_url() . 'login');
}
?>
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>Integrated Website <?php $web = $this->uut->namadomain(base_url());
                            $aa = explode(".", $web);
                            echo $aa[0]; ?></title>
  <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

  <!-- BARU SAJA 3.0.5 -->

  <!-- Favicon -->
  <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />
  <!-- Font Awesome 
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">-->
  <!-- Font Awesome Icons -->
  <link href="<?php echo base_url(); ?>boots/font-awesome/font-awesome.min.css" rel="stylesheet" type="text/css" />
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables 
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.css">-->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.0.1/css/buttons.dataTables.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- Kalender -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/fullcalendar/main.css" />

  <!-- END BARU SAJA 3.0.5 -->

  <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
        <script src="../../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="../../https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

  <!-- Alert Confirmation -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/Typeahead-BS3-css.css" id="toggleCSS" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>boots/dist/css/bootstrap-timepicker.min.css" />
  <!-- Progress bar -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-combined.min.css" id="toggleCSS" />



</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->

<body class="hold-transition sidebar-mini sidebar-collapse">
  <!-- Site wrapper -->
  <div class="wrapper">

    <!-- Navbar -->
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
      <!-- Left navbar links -->
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a target="_blank" href="<?php echo base_url(); ?>" class="nav-link">Tampil Website</a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
          <a href="<?php echo base_url(); ?>login/logout" class="nav-link">Logout</a>
        </li>
      </ul>

      <!-- SEARCH FORM -->
      <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
          <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
              <i class="fa fa-search"></i>
            </button>
          </div>
        </div>
      </form>

      <!-- Right navbar links -->
      <ul class="navbar-nav ml-auto">
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fa fa-bell-o"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="<?php echo base_url(); ?>login/logout" class="dropdown-item">
              <i class="fa fa-envelope mr-2"></i> Logout
            </a>
            <div class="dropdown-divider"></div>
            <a href="<?php echo base_url(); ?>ganti_password" class="dropdown-item">
              <i class="fa fa-users mr-2"></i> Ganti Password
            </a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
            <i class="fa fa-th-large"></i>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.navbar -->

    <!-- Main Sidebar Container -->
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
      <!-- Brand Logo -->
      <!-- Sidebar -->
      <div class="sidebar">
        <?php
        $where = array(
          'id_users' => $this->session->userdata('id_users')
        );
        $this->db->where($where);
        $this->db->from('users');
        $jml = $this->db->count_all_results();
        if ($jml > 0) {
          $this->db->where($where);
          $query = $this->db->get('users');
          foreach ($query->result() as $row) {
            if ($row->hak_akses == 'admin_web_disdagkopukm') {
              $this->load->view('menu/admin_web_disdagkopukm');
            } elseif ($row->hak_akses == 'admin_web_dinkes') {
              $this->load->view('menu/admin_web_dinkes');
            } elseif ($row->hak_akses == 'admin_web_diskominfo') {
              $this->load->view('menu/admin_web_diskominfo');
            } elseif ($row->hak_akses == 'admin_web_disparbud') {
              $this->load->view('menu/admin_web_disparbud');
            } elseif ($row->hak_akses == 'admin_web_pmi') {
              $this->load->view('menu/admin_web_pmi');
            } elseif ($row->hak_akses == 'web_pkk') {
              $this->load->view('menu/web_pkk');
            } elseif ($row->hak_akses == 'web_pkk_kecamatan') {
              $this->load->view('menu/web_pkk_kecamatan');
            } elseif ($row->hak_akses == 'admin_ppid') {
              $this->load->view('menu/admin_web_ppid');
            } else {
              $this->load->view('menu/administrator_web_opd');
            }
          }
        } else {
          exit;
        }
        ?>
        <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <?php $this->load->view($main_view);  ?>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="float-right d-none d-sm-block">
        <b>Version</b> <a href="#">3.1.2<a />
      </div>
      <strong>Copyright &copy; 2014-2018 <a href="https://diskominfo.wonosobokab.go.id/">Kominfo</a>.</strong> All rights
      reserved.
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <!-- ./wrapper -->


  <!-- BARU -->

  <!-- jQuery -->
  <script src="<?php echo base_url(); ?>boots/plugins/jQuery/jQuery-2.1.3.min.js"></script>
  <!-- Bootstrap 3.3.2 JS -->
  <script src="<?php echo base_url(); ?>boots/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
  <!-- SlimScroll -->
  <script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

  <!--<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>-->
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
  <script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/dataTables.buttons.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/2.0.1/js/buttons.print.min.js"></script>


  <!-- END BARU -->

  <script type="text/javascript">
    //----------------------------------------------------------------------------------------------
    function reset() {
      $('#toggleCSS').attr('href', '<?php echo base_url(); ?>css/alertify/alertify.default.css');
      alertify.set({
        labels: {
          ok: 'OK',
          cancel: 'Cancel'
        },
        delay: 5000,
        buttonReverse: false,
        buttonFocus: 'ok'
      });
    }
    //----------------------------------------------------------------------------------------------
  </script>

  <!-- uut -->
  <script src="<?php echo base_url(); ?>js/uut.js"></script>
  <!-- alertify -->
  <script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
  <!-- Morris.js charts -->
  <script src="<?php echo base_url(); ?>js/plugins/raphael/2.1.0/raphael-min.js"></script>
  <script src="<?php echo base_url(); ?>js/plugins/morris/morris.min.js" type="text/javascript"></script>
  <!--- -->
  <script src="<?php echo base_url(); ?>js/colorbox/jquery.colorbox.js"></script>
  <script src="<?php echo base_url(); ?>js/bootstrap-typeahead.js"></script>
  <script src="<?php echo base_url(); ?>js/hogan-2.0.0.js"></script>
  <!-- daterangepicker -->
  <script src="<?php echo base_url(); ?>boots/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
  <script src="<?php echo base_url(); ?>boots/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>boots/dist/js/moment-with-locales.js"></script>
  <script type="text/javascript" src="<?php echo base_url(); ?>boots/dist/js/bootstrap-datetimepicker.js"></script>
  <!-- InputMask -->
  <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <script>
    $(function() {
      $("[data-mask]").inputmask();
    });
  </script>
  <!-- Start
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script> editor-->
  <script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

  <!-- Kalender Kegiatan -->
  <script src="<?php echo base_url(); ?>assets/plugins/fullcalendar/moment.min.js" type="text/javascript"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/fullcalendar/main.js" type="text/javascript"></script>

</body>

</html>