      <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Kalender Kegiatan</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Kalender Kegiatan</a></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
      </section>
    <section class="content" id="awal">
		<!-- Custom Tabs -->
		<div class="card">
      <div class="card-header p-2">
        <ul class="nav nav-pills">
          <li class="nav-item"><a class="nav-link active" href="#tab_form_kalender_kegiatan" data-toggle="tab" id="klik_tab_input">Form</a></li>
          <li class="nav-item"><a class="nav-link" href="#tab_data_kalender_kegiatan" data-toggle="tab" id="klik_tab_data_kalender_kegiatan">Data</a></li>
          <li class="nav-item"><a class="nav-link" href="#"><i class="fa fa-refresh"></i></a></li>
        </ul>
      </div>
			<div class="card-body">
        <div class="tab-content">
          <div class="tab-pane active" id="tab_form_kalender_kegiatan">
            <div class="row">
              <div class="col-md-3">
                <div class="sticky-top mb-3">
                  <div class="card">
                    <div class="card-header">
                      <h4 class="card-title">Draggable Events</h4>
                    </div>
                    <div class="card-body">
                      <!-- the events -->
                      <div id="external-events">
                        <div class="external-event bg-success">Lunch</div>
                        <div class="external-event bg-warning">Go home</div>
                        <div class="external-event bg-info">Do homework</div>
                        <div class="external-event bg-primary">Work on UI design</div>
                        <div class="external-event bg-danger">Sleep tight</div>
                        <div class="checkbox">
                          <label for="drop-remove">
                            <input type="checkbox" id="drop-remove">
                            remove after drop
                          </label>
                        </div>
                      </div>
                    </div>
                    <!-- /.card-body -->
                  </div>
                  <!-- /.card -->
                  <div class="card">
                    <div class="card-header">
                      <h3 class="card-title">Create Event</h3>
                    </div>
                    <div class="card-body">
                      <div class="btn-group" style="width: 100%; margin-bottom: 10px;">
                        <ul class="fc-color-picker" id="color-chooser">
                          <li><a class="text-primary" href="#"><i class="fa fa-square"></i></a></li>
                          <li><a class="text-warning" href="#"><i class="fa fa-square"></i></a></li>
                          <li><a class="text-success" href="#"><i class="fa fa-square"></i></a></li>
                          <li><a class="text-danger" href="#"><i class="fa fa-square"></i></a></li>
                          <li><a class="text-muted" href="#"><i class="fa fa-square"></i></a></li>
                        </ul>
                      </div>
                      <!-- /btn-group -->
                      <div class="input-group">
                        <input id="new-event" type="text" class="form-control" placeholder="Event Title">

                        <div class="input-group-append">
                          <button id="add-new-event" type="button" class="btn btn-primary">Add</button>
                        </div>
                        <!-- /btn-group -->
                      </div>
                      <!-- /input-group -->
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.col -->
              <div class="col-md-9">
                <div class="card card-primary">
                  <div class="card-body p-0">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.tab-pane -->
          <div class="tab-pane" id="tab_data_kalender_kegiatan">
                <div class="row">
                  <div class="col-md-12">
                    <div class="card-body">
                      <div class="card-tools">
                        <div class="input-group">
                          <input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
                          <select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
                            <option value="999999999">Semua</option>
                            <option value="10">10 Per-Halaman</option>
                            <option value="50">50 Per-Halaman</option>
                            <option value="100">100 </option>
                          </select>
                          <select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
                            <option value="surat.nomor_agenda">Nomor Agenda</option>
                            <option value="surat.tanggal_surat">Tanggal Surat</option>
                            <option value="surat.asal_surat">Asal Surat</option>
                          </select>
                          <div class="input-group-btn">
                            <button class="btn btn-sm btn-default" id="tampilkan_data_surat"><i class="fa fa-search"></i> Tampil</button>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="card-footer table-responsive no-padding">
                      <table class="table table-bordered table-hover">
                        <thead class="bg-gray">
                          <tr>
                            <th>NO/Kode</th>
                            <th>Isi Ringkasan, File</th>
                            <th>Asal Surat</th>
                            <th>Nomor, Tgl. Surat</th>
                            <th colspan="4">Proses</th>
                          </tr>
                        </thead>
                        <tbody id="tbl_data_surat">
                        </tbody>
                      </table>
                      <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                      </ul>
                      <div class="overlay" id="spinners_data" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                    </div>
                  </div>
                </div>
          </div>
          <!-- /.tab-pane -->
        </div>
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
</section>

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var jenis_surat = $('#jenis_surat').val();
    var id_kelembagaan = $('#id_kelembagaan').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        id_kelembagaan:id_kelembagaan,
        jenis_surat:jenis_surat
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>surat/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li id="' + a + '" page="' + a + '" keyword="' + keyword + '" id_kelembagaan="' + id_kelembagaan + '" jenis_surat="' + jenis_surat + '"><a id="next" href="#">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var id_kelembagaan = $('#id_kelembagaan').val();
    var jenis_surat = $('#jenis_surat').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        id_kelembagaan:id_kelembagaan,
        jenis_surat:jenis_surat
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript" >
  $(document).ready(function () {
    var tabel = $("#tabel").val();
    $("#update_"+tabel+"").on("click", function (e) {
      e.preventDefault();
      update(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedSurat() {
    $('#id_surat, #nomor_agenda, #asal_surat, #nomor_surat, #isi_ringkasan, #kode_klasifikasi, #tanggal_surat, #jenis_surat, #id_kelembagaan, #keterangan, #sifat_surat, #unit_pengolah_surat, #status_surat').val('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<!--lampiran -->

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_surat').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'surat',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_surat').append(tr);
			}
		});
	}
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_surat').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_surat').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_surat').show();
    $('#update_surat').hide();
    $('#id_surat, #nomor_agenda, #asal_surat, #nomor_surat, #isi_ringkasan, #kode_klasifikasi, #tanggal_surat, #jenis_surat, #id_kelembagaan, #keterangan, #sifat_surat, #unit_pengolah_surat, #status_surat').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_surat').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'nomor_agenda', 'tanggal_surat' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["nomor_agenda"] = $("#nomor_agenda").val();
      parameter["asal_surat"] = $("#asal_surat").val();
      parameter["nomor_surat"] = $("#nomor_surat").val();
      parameter["isi_ringkasan"] = $("#isi_ringkasan").val();
      parameter["kode_klasifikasi"] = $("#kode_klasifikasi").val();
      parameter["tanggal_surat"] = $("#tanggal_surat").val();
      parameter["jenis_surat"] = $("#jenis_surat").val();
      parameter["id_kelembagaan"] = $("#id_kelembagaan").val();
      parameter["keterangan"] = $("#keterangan").val();
      parameter["sifat_surat"] = $("#sifat_surat").val();
      parameter["unit_pengolah_surat"] = $("#unit_pengolah_surat").val();
      parameter["status_surat"] = $("#status_surat").val();
      parameter["temp"] = $("#temp").val();
      var url = '<?php echo base_url(); ?>surat/simpan_surat';
      
      var parameterRv = [ 'nomor_agenda', 'tanggal_surat' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedSurat();
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
  $('#klik_tab_input').on('click', function(e) {
    e.preventDefault();
    $('#tab_form_'+tabel+' form').trigger('reset');
    $('#judul_form').html('Form Input');
    $('#update_'+tabel+'').hide();
    $('#simpan_'+tabel+'').show();
    $('#temp').val(Math.random());
    $('#mode').val('input');
    $('#PesanProgresUpload').html('');
  });
});
</script>
<script type="text/javascript" >
  $(document).ready(function () {
    var tabel = $("#tabel").val();
    $("#update_"+tabel+"").on("click", function (e) {
      e.preventDefault();
      update(tabel);
    });
  });
</script>
<script type="text/javascript">
  function update(tabel) {
    $('#update_surat').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'id_surat', 'nomor_agenda' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_surat"] = $("#id_surat").val();
      parameter["nomor_agenda"] = $("#nomor_agenda").val();
      parameter["asal_surat"] = $("#asal_surat").val();
      parameter["nomor_surat"] = $("#nomor_surat").val();
      parameter["isi_ringkasan"] = $("#isi_ringkasan").val();
      parameter["kode_klasifikasi"] = $("#kode_klasifikasi").val();
      parameter["tanggal_surat"] = $("#tanggal_surat").val();
      parameter["jenis_surat"] = $("#jenis_surat").val();
      parameter["id_kelembagaan"] = $("#id_kelembagaan").val();
      parameter["keterangan"] = $("#keterangan").val();
      parameter["sifat_surat"] = $("#sifat_surat").val();
      parameter["unit_pengolah_surat"] = $("#unit_pengolah_surat").val();
      parameter["status_surat"] = $("#status_surat").val();
      var url = '<?php echo base_url(); ?>surat/update_surat';
      
      var parameterRv = [ 'id_surat', 'nomor_agenda' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_edit_by_id(id, tabel) {
    $('#judul_form').html('Form Edit');
		$.ajax({
      type: 'POST',
      async: true,
      data: {
        id_surat:id
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>'+tabel+'/get_by_id/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $("#id_surat").val(json[i].id_surat);
          $("#nomor_agenda").val(json[i].nomor_agenda);
          $("#asal_surat").val(json[i].asal_surat);
          $("#nomor_surat").val(json[i].nomor_surat);
          $("#isi_ringkasan").val(json[i].isi_ringkasan);
          $("#kode_klasifikasi").val(json[i].kode_klasifikasi);
          $("#tanggal_surat").val(json[i].tanggal_surat);
          $("#keterangan").val(json[i].keterangan);
          $("#sifat_surat").val(json[i].sifat_surat);
          $("#unit_pengolah_surat").val(json[i].unit_pengolah_surat);
          $("#status_surat").val(json[i].status_surat);
          $("#mode").val('edit');
        }
        $('.nav-tabs a[href="#tab_form_surat"]').tab('show');        
        $('#simpan_'+tabel+'').hide();
        $('#update_'+tabel+'').show();
        AttachmentByMode('edit', id, tabel);
        $('#PesanProgresUpload').html('');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $("#tabel").val();
  $('#tbl_data_'+tabel+'').on('click', '#update_id', function() {
    var id = $(this).closest('tr').attr('id_'+tabel+'');
    load_edit_by_id(id, tabel);
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $("#tabel").val();
  $('#tbl_data_'+tabel+'').on('click', '#del_ajax', function() {
    var id_surat = $(this).closest('tr').attr('id_surat');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_surat"] = id_surat;
        var url = '<?php echo base_url(); ?>'+tabel+'/hapus/';
        HapusData(parameter, url);
        $('[id_surat='+id_surat+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_surat').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>

<!--
<script type="text/javascript">
    $(document).ready(function(){
        var calendar = $('#calendar').fullCalendar({
            editable:true,
            header:{
                left:'prev,next today',
                center:'title',
                right:'month,agendaWeek,agendaDay'
            },
            events:"<?php echo base_url(); ?>kalender_kegiatan/load",
            selectable:true,
            selectHelper:true,
            select:function(start, end, allDay)
            {
                var title = prompt("Enter Event Title");
                if(title)
                {
                    var start = $.fullCalendar.formatDate(start, "Y-MM-DD HH:mm:ss");
                    var end = $.fullCalendar.formatDate(end, "Y-MM-DD HH:mm:ss");
                    $.ajax({
                        url:"<?php echo base_url(); ?>kalender_kegiatan/insert",
                        type:"POST",
                        data:{title:title, start:start, end:end},
                        success:function()
                        {
                            calendar.fullCalendar('refetchEvents');
                            alert("Added Successfully");
                        }
                    })
                }
            },
            editable:true,
            eventResize:function(event)
            {
                var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");

                var title = event.title;

                var id = event.id;

                $.ajax({
                    url:"<?php echo base_url(); ?>kalender_kegiatan/update",
                    type:"POST",
                    data:{title:title, start:start, end:end, id:id},
                    success:function()
                    {
                        calendar.fullCalendar('refetchEvents');
                        alert("Event Update");
                    }
                })
            },
            eventDrop:function(event)
            {
                var start = $.fullCalendar.formatDate(event.start, "Y-MM-DD HH:mm:ss");
                //alert(start);
                var end = $.fullCalendar.formatDate(event.end, "Y-MM-DD HH:mm:ss");
                //alert(end);
                var title = event.title;
                var id = event.id;
                $.ajax({
                    url:"<?php echo base_url(); ?>kalender_kegiatan/update",
                    type:"POST",
                    data:{title:title, start:start, end:end, id:id},
                    success:function()
                    {
                        calendar.fullCalendar('refetchEvents');
                        alert("Event Updated");
                    }
                })
            },
            eventClick:function(event)
            {
                if(confirm("Are you sure you want to remove it?"))
                {
                    var id = event.id;
                    $.ajax({
                        url:"<?php echo base_url(); ?>kalender_kegiatan/delete",
                        type:"POST",
                        data:{id:id},
                        success:function()
                        {
                            calendar.fullCalendar('refetchEvents');
                            alert('Event Removed');
                        }
                    })
                }
            }
        });
    });
             
</script>-->
<script>
  $(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function ini_events(ele) {
      ele.each(function () {

        // create an Event Object (https://fullcalendar.io/docs/event-object)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    ini_events($('#external-events div.external-event'))

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()

    var Calendar = FullCalendar.Calendar;
    var Draggable = FullCalendar.Draggable;

    var containerEl = document.getElementById('external-events');
    var checkbox = document.getElementById('drop-remove');
    var calendarEl = document.getElementById('calendar');

    // initialize the external events
    // -----------------------------------------------------------------

    new Draggable(containerEl, {
      itemSelector: '.external-event',
      eventData: function(eventEl) {
        return {
          title: eventEl.innerText,
          backgroundColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          borderColor: window.getComputedStyle( eventEl ,null).getPropertyValue('background-color'),
          textColor: window.getComputedStyle( eventEl ,null).getPropertyValue('color'),
        };
      }
    });

    var calendar = new Calendar(calendarEl, {
      headerToolbar: {
        left  : 'prev,next today',
        center: 'title',
        right : 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      themeSystem: 'bootstrap',
      //Random default events
      events: [
        {
          title          : 'All Day Event',
          start          : new Date(y, m, 1),
          backgroundColor: '#f56954', //red
          borderColor    : '#f56954', //red
          allDay         : true
        },
        {
          title          : 'Long Event',
          start          : new Date(y, m, d - 5),
          end            : new Date(y, m, d - 2),
          backgroundColor: '#f39c12', //yellow
          borderColor    : '#f39c12' //yellow
        },
        {
          title          : 'Meeting',
          start          : new Date(y, m, d, 10, 30),
          allDay         : false,
          backgroundColor: '#0073b7', //Blue
          borderColor    : '#0073b7' //Blue
        },
        {
          title          : 'Lunch',
          start          : new Date(y, m, d, 12, 0),
          end            : new Date(y, m, d, 14, 0),
          allDay         : false,
          backgroundColor: '#00c0ef', //Info (aqua)
          borderColor    : '#00c0ef' //Info (aqua)
        },
        {
          title          : 'Birthday Party',
          start          : new Date(y, m, d + 1, 19, 0),
          end            : new Date(y, m, d + 1, 22, 30),
          allDay         : false,
          backgroundColor: '#00a65a', //Success (green)
          borderColor    : '#00a65a' //Success (green)
        },
        {
          title          : 'Click for Google',
          start          : new Date(y, m, 28),
          end            : new Date(y, m, 29),
          url            : 'https://www.google.com/',
          backgroundColor: '#3c8dbc', //Primary (light-blue)
          borderColor    : '#3c8dbc' //Primary (light-blue)
        }
      ],
      editable  : true,
      droppable : true, // this allows things to be dropped onto the calendar !!!
      drop      : function(info) {
        // is the "remove after drop" checkbox checked?
        if (checkbox.checked) {
          // if so, remove the element from the "Draggable Events" list
          info.draggedEl.parentNode.removeChild(info.draggedEl);
        }
      }
    });

    calendar.render();
    // $('#calendar').fullCalendar()

    /* ADDING EVENTS */
    var currColor = '#3c8dbc' //Red by default
    // Color chooser button
    $('#color-chooser > li > a').click(function (e) {
      e.preventDefault()
      // Save color
      currColor = $(this).css('color')
      // Add color effect to button
      $('#add-new-event').css({
        'background-color': currColor,
        'border-color'    : currColor
      })
    })
    $('#add-new-event').click(function (e) {
      e.preventDefault()
      // Get value and make sure it is not null
      var val = $('#new-event').val()
      if (val.length == 0) {
        return
      }

      // Create events
      var event = $('<div />')
      event.css({
        'background-color': currColor,
        'border-color'    : currColor,
        'color'           : '#fff'
      }).addClass('external-event')
      event.text(val)
      $('#external-events').prepend(event)

      // Add draggable funtionality
      ini_events(event)

      // Remove event from text input
      $('#new-event').val('')
    })
  })
</script>