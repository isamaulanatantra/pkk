
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Perijinan Penggunaan Aula</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Perijinan Penggunaan Aula</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link" href="#tab_1" data-toggle="tab" id="klik_tab_input">TAMBAH DATA</a></li>
                  <li class="nav-item"><a class="nav-link active" href="#tab_2" data-toggle="tab" id="klik_tab_tampil">MENUNGGU</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_3" data-toggle="tab" id="klik_tab_tampil_arsip">LIST DATA</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane" id="tab_1">
													
					<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>perijinan_penggunaan_aula/simpan_perijinan_penggunaan_aula">
					<h3 id="judul_formulir">FORMULIR INPUT</h3>
					<div class="row">
						<div class="col-sm-12 col-md-6">

							<div class="form-group" style="display:none;">
								<label for="temp">temp</label>
								<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
							</div>
							<div class="form-group" style="display:none;">
								<label for="mode">mode</label>
								<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
							</div>

							<div class="form-group" style="display:none;">
								<label for="id_perijinan_penggunaan_aula">id_perijinan_penggunaan_aula</label>
								<input class="form-control" id="id_perijinan_penggunaan_aula" name="id" value="" placeholder="id_perijinan_penggunaan_aula" type="text">
							</div>
							<div class="form-group">
								<label for="tanggal_penggunaan">Tanggal Pengguanaan</label>
								<input class="form-control" id="tanggal_penggunaan" name="tanggal_penggunaan" value="" placeholder="Tanggal Penggunaan" type="text" required="">
							</div>							
							<div class="form-group">
								<label for="nama_kegiatan">Nama Kegiatan</label>
								<input class="form-control" id="nama_kegiatan" name="nama_kegiatan" value="" placeholder="Nama Kegiatan" type="text" required="">
							</div>							
							<div class="form-group">
								<label for="seksi_penggunaan">Seksi</label>
								<input class="form-control" id="seksi_penggunaan" name="seksi_penggunaan" value="" placeholder="Seksi Penggunaan" type="text" required="">
							</div>							
							<div class="form-group">
								<label for="penanggung_jawab">Penanggung Jawab</label>
								<input class="form-control" id="penanggung_jawab" name="penanggung_jawab" value="" placeholder="Penanggung Jawab" type="text" required="">
							</div>
						</div>
															</div>
															<button type="submit" class="btn btn-primary" id="simpan_perijinan_penggunaan_aula">SIMPAN</button>
															<button type="submit" class="btn btn-primary" id="update_perijinan_penggunaan_aula" style="display:none;">UPDATE</button>
													</form>
													<div class="overlay" id="overlay_form_input" style="display:none;">
														<i class="fa fa-refresh fa-spin"></i>
													</div>
											
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane active table-responsive" id="tab_2">
                    
														<table class="table table-bordered table-hover">
															<thead>																
																<th>NO</th>
																<th>TANGGAL PENGGUNAAN</th>
																<th>NAMA KEGIATAN</th>
																<th>SEKSI</th>
																<th>PENANGGUNG JAWAB</th>
                                <th>AKSI</th>
															</thead>
															<tbody id="tbl_utama_perijinan_penggunaan_aula">
															</tbody>
														</table>
														<ul class="pagination pagination-sm no-margin pull-right" id="pagination">
														</ul>
														
														<div class="overlay" id="spinners_data" style="display:none;">
															<i class="fa fa-refresh fa-spin"></i>
														</div>
											
                  </div>
                  <!-- /.tab-pane -->
                  <div class="tab-pane" id="tab_3">
                    
														<table class="table table-bordered table-hover">
                              <thead>                               
                                <th>NO</th>
                                <th>TANGGAL PENGGUNAAN</th>
                                <th>NAMA KEGIATAN</th>
                                <th>SEKSI</th>
                                <th>PENANGGUNG JAWAB</th>
                                <th>STATUS</th>
                                <th>PROSES</th>
                              </thead>
                              <tbody id="tbl_utama_perijinan_penggunaan_aula99">
                              </tbody>
                            </table>
														<ul class="pagination pagination-sm no-margin pull-right" id="pagination">
														</ul>
														<div class="overlay" id="spinners_data6" style="display:none;">
															<i class="fa fa-refresh fa-spin"></i>
														</div>
										
                  </div>
                  <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_penggunaan, #tanggal_surat_kesbangpol').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perijinan_penggunaan_aula').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_perijinan_penggunaan_aula').hide();
    $('#update_perijinan_penggunaan_aula').show();
    var id_perijinan_penggunaan_aula = $(this).closest('tr').attr('id_perijinan_penggunaan_aula');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_perijinan_penggunaan_aula');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_perijinan_penggunaan_aula').val(id_perijinan_penggunaan_aula);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_perijinan_penggunaan_aula:id_perijinan_penggunaan_aula
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>perijinan_penggunaan_aula/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#tanggal_penggunaan').val(json[i].tanggal_penggunaan);
            $('#nama_kegiatan').val(json[i].nama_kegiatan);
            $('#seksi_penggunaan').val(json[i].seksi_penggunaan);
            $('#penanggung_jawab').val(json[i].penanggung_jawab);            
          }
        }
      });
    AttachmentByMode(mode, value);
  });
});
</script>

<script>
  function AfterSavedperijinan_penggunaan_aula() {
    $('#id_perijinan_penggunaan_aula, #nama, #nomor_surat, #perihal_surat, #tanggal_surat, #nomot_telp, #kebangsaan, #alamat, #pekerjaan, #penanggung_jawab, #lokasi, #judul_penelitian, #asal_universitas, #nomor_surat_kesbangpol #tanggal_surat_kesbangpol, #surat_ditujukan_kepada, #tembusan_kepada_1, #tembusan_kepada_2, #tembusan_kepada_3, #tembusan_kepada_4, #tembusan_kepada_5, #tembusan_kepada_6, #temp').val('');
  	$('#tbl_lampiran_perijinan_penggunaan_aula').html('');
    $('#PesanProgresUpload').html('');
    }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1;
    var limit = 10;
	load_data_perijinan_penggunaan_aula(halaman, limit);
  load_data_perijinan_penggunaan_aula99(halaman, limit);
});
</script>
<script>
  function load_data_perijinan_penggunaan_aula(halaman, limit) {
    $('#tbl_utama_perijinan_penggunaan_aula').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>perijinan_penggunaan_aula/load_table/',
      success: function(html) {
        $('#tbl_utama_perijinan_penggunaan_aula').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>

<script>
  function load_data_perijinan_penggunaan_aula99(halaman, limit) {
    $('#tbl_utama_perijinan_penggunaan_aula99').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>perijinan_penggunaan_aula/load_table99/',
      success: function(html) {
        $('#tbl_utama_perijinan_penggunaan_aula99').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perijinan_penggunaan_aula').on('click', '#del_ajax', function() {
    var id_perijinan_penggunaan_aula = $(this).closest('tr').attr('id_perijinan_penggunaan_aula');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_perijinan_penggunaan_aula"] = id_perijinan_penggunaan_aula;
        var url = '<?php echo base_url(); ?>perijinan_penggunaan_aula/hapus/';
        HapusData(parameter, url);
        $('[id_perijinan_penggunaan_aula='+id_perijinan_penggunaan_aula+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_perijinan_penggunaan_aula').show();
    $('#update_perijinan_penggunaan_aula').hide();
    //$('#tbl_attachment_perijinan_penggunaan_aula').html('');
    $('#id_perijinan_penggunaan_aula, #tanggal_penggunaan, #nama_kegiatan, #seksi_penggunaan, #penanggung_jawab').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_perijinan_penggunaan_aula').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'nama', 'nomor_surat', 'perihal_surat', 'tanggal_surat', 'nomot_telp', 'kebangsaan', 'alamat', 'pekerjaan', 'penanggung_jawab', 'lokasi', 'judul_penelitian', 'asal_universitas', 'nomor_surat_kesbangpol', 'tanggal_surat_kesbangpol', 'surat_ditujukan_kepada', 'tembusan_kepada_1',  'tembusan_kepada_2',  'tembusan_kepada_3',  'tembusan_kepada_4', 'tembusan_kepada_5',  'tembusan_kepada_6', 'temp' ];
	  InputValid(parameter);
      
      var parameter = {}
      parameter["nama"] = $("#nama").val();
      parameter["nomor_surat"] = $("#nomor_surat").val();
      parameter["perihal_surat"] = $("#perihal_surat").val();
      parameter["tanggal_surat"] = $("#tanggal_surat").val();
      parameter["nomot_telp"] = $("#nomot_telp").val();
      parameter["kebangsaan"] = $("#kebangsaan").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      parameter["penanggung_jawab"] = $("#penanggung_jawab").val();
      parameter["lokasi"] = $("#lokasi").val();
      parameter["judul_penelitian"] = $("#judul_penelitian").val();
      parameter["asal_universitas"] = $("#asal_universitas").val();
      parameter["nomor_surat_kesbangpol"] = $("#nomor_surat_kesbangpol").val();
      parameter["tanggal_surat_kesbangpol"] = $("#tanggal_surat_kesbangpol").val();
      parameter["surat_ditujukan_kepada"] = $("#surat_ditujukan_kepada").val();
      parameter["tembusan_kepada_1"] = $("#tembusan_kepada_1").val();
      parameter["tembusan_kepada_2"] = $("#tembusan_kepada_2").val();
      parameter["tembusan_kepada_3"] = $("#tembusan_kepada_3").val();
      parameter["tembusan_kepada_4"] = $("#tembusan_kepada_4").val();
      parameter["tembusan_kepada_5"] = $("#tembusan_kepada_5").val();
      parameter["tembusan_kepada_6"] = $("#tembusan_kepada_6").val();
      parameter["temp"] = $("#temp").val();
      parameter["id_perijinan_penggunaan_aula"] = $("#id_perijinan_penggunaan_aula").val();
      var url = '<?php echo base_url(); ?>perijinan_penggunaan_aula/update_perijinan_penggunaan_aula';
      
      var parameterRv = [ 'nama', 'nomor_surat', 'perihal_surat', 'tanggal_surat', 'nomot_telp', 'kebangsaan', 'alamat', 'pekerjaan', 'penanggung_jawab', 'lokasi', 'judul_penelitian', 'asal_universitas', 'nomor_surat_kesbangpol', 'tanggal_surat_kesbangpol', 'surat_ditujukan_kepada', 'tembusan_kepada_1',  'tembusan_kepada_2',  'tembusan_kepada_3',  'tembusan_kepada_4', 'tembusan_kepada_5',  'tembusan_kepada_6',  'temp' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_lampiran_perijinan_penggunaan_aula').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'perijinan_penggunaan_aula',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>lampiran_perijinan_penggunaan_aula/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_lampiran_perijinan_penggunaan_aula="'+json[i].id_lampiran_perijinan_penggunaan_aula+'" id="'+json[i].id_lampiran_perijinan_penggunaan_aula+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_lampiran_perijinan_penggunaan_aula').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_perijinan_penggunaan_aula').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_lampiran_perijinan_penggunaan_aula').on('click', '#del_ajax', function() {
    var id_lampiran_perijinan_penggunaan_aula = $(this).closest('tr').attr('id_lampiran_perijinan_penggunaan_aula');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_lampiran_perijinan_penggunaan_aula"] = id_lampiran_perijinan_penggunaan_aula;
        var url = '<?php echo base_url(); ?>lampiran_perijinan_penggunaan_aula/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_perijinan_penggunaan_aula').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_lampiran_perijinan_penggunaan_aula='+id_lampiran_perijinan_penggunaan_aula+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>


<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perijinan_penggunaan_aula').on('click', '#verif_ajax', function() {
    var id_perijinan_penggunaan_aula = $(this).closest('tr').attr('id_perijinan_penggunaan_aula');
    alertify.confirm('Anda yakin konfirmasi data ini?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_perijinan_penggunaan_aula"] = id_perijinan_penggunaan_aula;
        var url = '<?php echo base_url(); ?>perijinan_penggunaan_aula/verifikasi/';
        HapusData(parameter, url);
        $('[id_perijinan_penggunaan_aula='+id_perijinan_penggunaan_aula+']').remove();
      } else {
        alertify.error('verifikasi dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perijinan_penggunaan_aula').on('click', '#noverif_ajax', function() {
    var id_perijinan_penggunaan_aula = $(this).closest('tr').attr('id_perijinan_penggunaan_aula');
    alertify.confirm('Anda yakin tidak konfirmasi data ini?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_perijinan_penggunaan_aula"] = id_perijinan_penggunaan_aula;
        var url = '<?php echo base_url(); ?>perijinan_penggunaan_aula/noverifikasi/';
        HapusData(parameter, url);
        $('[id_perijinan_penggunaan_aula='+id_perijinan_penggunaan_aula+']').remove();
      } else {
        alertify.error('verifikasi dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perijinan_penggunaan_aula').on('click', '#delete_ajax', function() {
    var id_perijinan_penggunaan_aula = $(this).closest('tr').attr('id_perijinan_penggunaan_aula');
    alertify.confirm('Anda yakin todak konfirmasi data ini?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_perijinan_penggunaan_aula"] = id_perijinan_penggunaan_aula;
        var url = '<?php echo base_url(); ?>perijinan_penggunaan_aula/deleteaula/';
        HapusData(parameter, url);
        $('[id_perijinan_penggunaan_aula='+id_perijinan_penggunaan_aula+']').remove();
      } else {
        alertify.error('verifikasi dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_perijinan_penggunaan_aula99').on('click', '#batalkan_ajax', function() {
    var id_perijinan_penggunaan_aula = $(this).closest('tr').attr('id_perijinan_penggunaan_aula');
    alertify.confirm('Anda yakin membatalkan acara ini?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_perijinan_penggunaan_aula"] = id_perijinan_penggunaan_aula;
        var url = '<?php echo base_url(); ?>perijinan_penggunaan_aula/deleteaula/';
        HapusData(parameter, url);
        $('[id_perijinan_penggunaan_aula='+id_perijinan_penggunaan_aula+']').remove();
      } else {
        alertify.error('verifikasi dibatalkan');
      }
    });
  });
});
</script>