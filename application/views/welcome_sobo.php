<!-- === BEGIN HEADER === -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <!-- Title -->
        <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
        <!-- Meta -->
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
        <meta name="author" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>,">
        <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>">
        <meta name="og:description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
        <meta name="og:url" content="<?php echo base_url(); ?>"/>
        <meta name="og:title" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>"/>
        <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
        <meta name="og:keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
        <!-- Favicon -->
        <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>Template/HTML/assets/css/bootstrap.css" rel="stylesheet">
        <!-- Template CSS
        <link rel="stylesheet" href="<?php echo base_url(); ?>Template/HTML/assets/css/animate.css" rel="stylesheet">
        -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>Template/HTML/assets/css/font-awesome.css" rel="stylesheet">
        <?php
        $web=$this->uut->namadomain(base_url());
        $where = array(
          'status' => 1,
          'domain' => $web
          );
        $this->db->where($where);
        $this->db->limit(1);
        $query = $this->db->get('tema_website');
        $tema = 'biru.css';
        foreach ($query->result() as $row)
          {
            $tema = $row->nama_tema_website;
          }
        echo '<link rel="stylesheet" href="'.base_url().'Template/HTML/assets/css/'.$tema.'" rel="stylesheet">';
        
        $where0 = array(
          'status' => 1,
          'domain' => $web
          );
        $this->db->where($where0);
        $this->db->limit(1);
        $query0 = $this->db->get('header_website');
        $header_website = 'logo.png';
        foreach ($query0->result() as $row0)
          {
            $header_website = $row0->file_name;
          }
        ?>
        <!-- Google Fonts-->
        <link href="//fonts.googleapis.com/css?family=Roboto+Condensed:400,300" rel="stylesheet" type="text/css">
        <style>
          #header {
            position: relative;
            height: 120px;
            top: 0;
            transition: all 0.2s ease 0s;
            width: 100%;
            background-image: url('<?php echo base_url(); ?>media/upload/<?php echo $header_website; ?>');
            background-repeat: no-repeat;
            background-position: top center;
            background-size: 100% auto;
          }
        </style>
        <!-- <script type="text/javascript"src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>-->
        
      <!-- Custom CSS -->
      <link href="<?php echo base_url(); ?>assets/portfolio/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">
      <link href="<?php echo base_url(); ?>assets/portfolio/css/stylish-portfolio.min.css" rel="stylesheet">
      
    </head>
    <body>
        <div id="body-bg">
            <!-- Phone/Email -->
            <div id="pre-header" class="background-gray-lighter">
                <div class="container no-padding">
                    <div class="row hidden-xs">
                        <div class="col-sm-6 padding-vert-5">
                            <strong>Phone:</strong>&nbsp;<?php if(!empty( $telpon )){ echo $telpon; } ?>
                        </div>
                        <div class="col-sm-6 text-right padding-vert-5">
                            <strong>Email:</strong>&nbsp;<?php if(!empty( $email )){ echo $email; } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div id="slideshow" class="bottom-border-shadow">
                <div class="container no-padding background-white bottom-border">
                    <div class="row">
                      <center>
                        <!-- Carousel Slideshow -->
                        <div id="carousel-example" class="carousel slide" data-ride="carousel">
                            <!-- Carousel Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#carousel-example" data-slide-to="0" class="active"></li>
                                <li data-target="#carousel-example" data-slide-to="1"></li>
                                <li data-target="#carousel-example" data-slide-to="2"></li>
                            </ol>
                            <div class="clearfix"></div>
                            <!-- End Carousel Indicators -->
                            <!-- Carousel Images -->
                            <div class="carousel-inner">
                                <?php
                                $where1 = array(
                                  'status' => 1,
                                  'domain' => $web
                                  );
                                $this->db->where($where1);
                                $query1 = $this->db->get('slide_website');
                                $a = 0;
                                foreach ($query1->result() as $row1)
                                  {
                                    $a = $a+1;
                                    if( $a == 1 ){
                                      echo
                                      '
                                      <div class="item active">
                                          <img src="'.base_url().'media/upload/'.$row1->file_name.'">
                                      </div>
                                      ';
                                      }
                                    else{
                                      echo
                                      '
                                      <div class="item">
                                          <img src="'.base_url().'media/upload/'.$row1->file_name.'">
                                      </div>
                                      ';
                                      } 
                                  }
                                ?>
                            </div>
                            <!-- End Carousel Images -->
                            <!-- Carousel Controls -->
                            <a class="left carousel-control" href="#carousel-example" data-slide="prev">
                                <span class="glyphicon glyphicon-chevron-left"></span>
                            </a>
                            <a class="right carousel-control" href="#carousel-example" data-slide="next">
                                <span class="glyphicon glyphicon-chevron-right"></span>
                            </a>
                            <!-- End Carousel Controls -->
                        </div>
                      </center>
                    </div>
                </div>
            </div>
            <div id="hornav" class="bottom-border-shadow">
                <div class="container no-padding border-bottom">
                    <div class="row">
                        <div class="col-md-12 no-padding">
                            <div class="visible-lg">
                                <?php echo $menu_atas; ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Top Menu -->
            <!-- === END HEADER === -->
            <!-- === BEGIN CONTENT === -->
            
            <div id="content" class="bottom-border-shadow">
                <div class="container background-white bottom-border">
                    <div class="row margin-vert-30">
                        <!-- Main Text -->
                        <div class="col-md-3">
                            <div class="row">
                              <?php if(!empty( $KolomPalingBawah )){ echo $KolomPalingBawah; } ?>
                            </div><!-- 
                            <div class="row">
                            
                              <div id="gpr-kominfo-widget-container">
                              </div>
                            
                            </div> -->
                            <div class="row">
                              <i class="fa fa-bar-chart-o"></i> <span id="visitor"></span>
                            </div>
                        </div>
                        <!-- End Main Text -->
                        <div class="col-md-9">
                            <div class="row">
                            <div class="col-md-6">
                            <?php if(!empty( $KolomKiriAtas )){ echo $KolomKiriAtas; } ?>
                            </div>
                            <div class="col-md-6">
                            <?php if(!empty( $KolomKananAtas )){ echo $KolomKananAtas; } ?>
                            </div>
                            </div>
                            <div class="row">
                              <h3 class="progress-label">Total Dibaca
                                  <span class="pull-right" id="hit_counter"></span>
                              </h3>
                              <div class="progress progress-sm">
                                  <div class="progress-bar progress-bar-orange" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                                  </div>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Portfolio
             -->
            <div id="portfolio" class="bottom-border-shadow">
                <div class="container bottom-border">
                    <div class="row margin-vert-30 padding-top-40">
                            <?php  if(!empty($galery_berita)){ echo $galery_berita; } ?>
                    </div>
                </div>
            </div>
            <!--
            <div id="portfolio" class="bottom-border-shadow">
                <div class="container bottom-border">
                    <div class="row margin-vert-30 padding-top-40">
          <div class="col-lg-4">
            <a class="portofolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Stationary</h2>
                  <p class="mb-0">A yellow pencil with envelopes on a clean, blue backdrop!</p>
                </span>
              </span>
              <img class="img-fluid" src="https://dinkes.wonosobokab.go.id/media/upload/20180327034819_JAWA_tengah_sebagai_provinsi_terbaik_dalam_pelaporan_dan_pencapaian_SPM_Bidang_kesehatan.jpg" alt="">
            </a>
          </div>
          <div class="col-lg-4">
            <a class="portofolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Ice Cream</h2>
                  <p class="mb-0">A dark blue background with a colored pencil, a clip, and a tiny ice cream cone!</p>
                </span>
              </span>
              <img class="img-fluid" src="https://dinkes.wonosobokab.go.id/media/upload/20180327033900_Pertemuan_Revisi_Folmularium.jpg" alt="">
            </a>
          </div>
          <div class="col-lg-4">
            <a class="portofolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Strawberries</h2>
                  <p class="mb-0">Strawberries are such a tasty snack, especially with a little sugar on top!</p>
                </span>
              </span>
              <img class="img-fluid" src="https://dinkes.wonosobokab.go.id/media/upload/20180321020814_Pertemuan_Pembinaan_Pengelolaan_Keuangan_BOK_dinas_kesehatan_wonosobo_2.jpg" alt="">
            </a>
          </div>
          <div class="col-lg-4">
            <a class="portofolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Workspace</h2>
                  <p class="mb-0">A yellow workspace with some scissors, pencils, and other objects.</p>
                </span>
              </span>
              <img class="img-fluid" src="https://dinkes.wonosobokab.go.id/media/upload/20180320033854_5.jpg" alt="">
            </a>
          </div>
          <div class="col-lg-4">
            <a class="portofolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Strawberries</h2>
                  <p class="mb-0">Strawberries are such a tasty snack, especially with a little sugar on top!</p>
                </span>
              </span>
              <img class="img-fluid" src="https://dinkes.wonosobokab.go.id/media/upload/20180314022240_Workshop_Program_Indonesia_Sehat_dengan_Pendekatan_Keluarga_Kabupaten_Wonosobo.jpeg" alt="">
            </a>
          </div>
          <div class="col-lg-4">
            <a class="portofolio-item" href="#">
              <span class="caption">
                <span class="caption-content">
                  <h2>Workspace</h2>
                  <p class="mb-0">A yellow workspace with some scissors, pencils, and other objects.</p>
                </span>
              </span>
              <img class="img-fluid" src="https://dinkes.wonosobokab.go.id/media/upload/20180314114438_Pencanangan_Kampanye_Imunisasi_Campak_Rubella.jpg" alt="">
            </a>
          </div>
                    </div>
                </div>
            </div>
             -->
            
    <!-- Portfolio -->
    <section class="content-section" id="portofolio">
      <div class="containers">
      </div>
    </section>
            <!-- End Portfolio -->
            <!-- === END CONTENT === -->
            <!-- === BEGIN FOOTER === -->
            <div id="base">
                <div class="container bottom-border padding-vert-30">
                    <div class="row">
                        <!-- Disclaimer -->
                        <div class="col-md-4">
                          <?php if(!empty( $KolomKiriBawah )){ echo $KolomKiriBawah; } ?>
                        </div>
                        <!-- End Disclaimer -->
                        <!-- Contact Details -->
                        <div class="col-md-4 margin-bottom-20">
                            <h3 class="margin-bottom-10">Kontak Kami</h3>
                            <p>
                                <span class="fa-phone">Telp:</span>&nbsp;<?php if(!empty( $telpon )){ echo $telpon; } ?>
                                <br>
                                <span class="fa-envelope">Email:</span>
                                <a href="mailto:<?php if(!empty( $email )){ echo $email; } ?>">&nbsp;<?php if(!empty( $email )){ echo $email; } ?></a>
                                <br>
                                <span class="fa-link">Website:</span>
                                <a href="https://<?php echo $web; ?>">https://<?php echo $web; ?></a>
                            </p>
                            <p><?php if(!empty( $alamat )){ echo $alamat; } ?></p>
                        </div>
                        <!-- End Contact Details -->
                        <!-- Sample Menu -->
                        <div class="col-md-4 margin-bottom-20">
                            <?php if(!empty( $KolomKananBawah )){ echo $KolomKananBawah; } ?>
                            <div class="clearfix"></div>
                        </div>
                        <!-- End Sample Menu -->
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <div id="footer" class="background-grey">
                <div class="container">
                    <div class="row">
                        <!-- Footer Menu -->
                        <div id="footermenu" class="col-md-8"><!--
                            <ul class="social-icons no-padding">
                                <li class="social-rss">
                                    <a href="#" target="_blank" title="RSS"></a>
                                </li>
                                <li class="social-twitter">
                                    <a href="#" target="_blank" title="Twitter"></a>
                                </li>
                                <li class="social-facebook">
                                    <a href="#" target="_blank" title="Facebook"></a>
                                </li>
                                <li class="social-googleplus">
                                    <a href="#" target="_blank" title="Google+"></a>
                                </li>
                            </ul>-->
                            
                        </div>
                        <!-- End Footer Menu -->
                        <!-- Copyright -->
                        <div id="copyright" class="col-md-4">
                            <p class="pull-right">&copy; <?php echo date('Y'); ?> <?php if(!empty( $domain )){ echo $domain; } ?></p>
                        </div>
                        <!-- End Copyright -->
                    </div>
                </div>
            </div>
            <!-- End Footer -->
            <!-- JS -->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.min.js" type="text/javascript"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/bootstrap.min.js" type="text/javascript"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/scripts.js"></script>
            <!-- Isotope - Portfolio Sorting -->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.isotope.js" type="text/javascript"></script>
            <!-- Mobile Menu - Slicknav -->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.slicknav.js" type="text/javascript"></script>
            <!-- Animate on Scroll-->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.visible.js" charset="utf-8"></script>
            <!-- Sticky Div -->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.sticky.js" charset="utf-8"></script>
            <!-- Slimbox2-->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/slimbox2.js" charset="utf-8"></script>
            <!-- Modernizr -->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/modernizr.custom.js" ></script>
            <!-- End JS -->
            <script>
              function LoadVisitor() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    table:'visitor'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/simpan_visitor/',
                  success: function(html) {
                    $('#visitor').html('Total Pengunjung '+html+' ');
                  }
                });
              }
            </script>
            <script>
              function LoadHitCounter() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    current_url:'<?php echo base_url(); ?>index.php'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/hit_counter/',
                  success: function(html) {
                    $('#hit_counter').html(''+html+' Kali ');
                  }
                });
              }
            </script>
            <script type="text/javascript">
            $(document).ready(function() {
              LoadVisitor();
              LoadHitCounter();
            });
            </script>
      <!-- Plugin JavaScript -->
      <script src="<?php echo base_url(); ?>assets/portfolio/jquery-easing/jquery.easing.min.js"></script>

      <!-- Custom scripts for this template -->
      <script src="<?php echo base_url(); ?>assets/portfolio/js/stylish-portfolio.min.js"></script>
      
    </body>
</html>
<!-- === END FOOTER === -->