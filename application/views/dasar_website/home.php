<section class="content" id="awal">
  <div class="row">
  <div class="col-md-12">
    <ul class="nav nav-pills ml-auto p-2">
      <li class="active"><a data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="row">
          <div class="col-md-6">
            <div class="card card-primary card-solid">
              <div class="card-header with-border">
                <h3 class="card-title" id="judul_formulir">UPDATE DATA DASAR</h3>
              </div>
              <div class="card-body">
                <form role="form" id="form_isian" method="post" action="https://patendemo.wonosobokab.go.id/attachment/upload/?table_name=dasar_website" enctype="multipart/form-data">
                  <div class="card-body">
                    <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <input class="form-control" name="alamat" id="alamat" placeholder="alamat" type="text">
                    </div>
                    <div class="form-group">
                      <label for="telpon">Telpon</label>
                      <input class="form-control" name="telpon" id="telpon" placeholder="telpon" type="text">
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input class="form-control" name="email" id="email" placeholder="email" type="text">
                    </div>
                    <div class="form-group">
                      <label for="twitter">Twitter</label>
                      <input class="form-control" name="twitter" id="twitter" placeholder="twitter" type="text">
                    </div>
                    <div class="form-group">
                      <label for="facebook">Facebook</label>
                      <input class="form-control" name="facebook" id="facebook" placeholder="facebook" type="text">
                    </div>
                    <div class="form-group">
                      <label for="google">Google</label>
                      <input class="form-control" name="google" id="google" placeholder="google" type="text">
                    </div>
                    <div class="form-group">
                      <label for="instagram">Instagram</label>
                      <input class="form-control" name="instagram" id="instagram" placeholder="instagram" type="text">
                    </div>
                    <div class="form-group">
                      <label for="peta">Peta</label>
                      <input class="form-control" name="peta" id="peta" placeholder="peta" type="text">
                    </div>
                    <div class="form-group">
                      <label for="keterangan">Keterangan</label>
                      <input class="form-control" name="keterangan" id="keterangan" placeholder="keterangan" type="text">
                    </div>
                  </div>
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary" id="update_dasar_website">UPDATE</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab_2">
        
      </div>
    </div>
  </div>
  </div>
</section>
<!----------------------->
<script>
  function load_data_dasar_website() {
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:'1'
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>dasar_website/json_all_dasar_website/',
      success: function(json) {
        var tr = '';
        for (var i = 0; i < json.length; i++) {
          $("#id_dasar_website").val(json[i].id_dasar_website);
          $("#domain").val(json[i].domain);
          $("#alamat").val(json[i].alamat);
          $("#telpon").val(json[i].telpon);
          $("#email").val(json[i].email);
          $("#twitter").val(json[i].twitter);
          $("#facebook").val(json[i].facebook);
          $("#google").val(json[i].google);
          $("#instagram").val(json[i].instagram);
          $("#peta").val(json[i].peta);
          $("#keterangan").val(json[i].keterangan);
        }
      }
    });
  }
</script>
<!----------------------->
<script>
  function cek_tema() {
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:'a'
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>dasar_website/cek_tema/',
      success: function(text) {
      load_data_dasar_website();
      }
    });
  }
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    cek_tema();
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#update_dasar_website').on('click', function(e) {
      e.preventDefault();
      
      var parameter = [
                       
                       'alamat',
                       'telpon',
                       'email',
                       'twitter',
                       'facebook',
                       'google',
                       'instagram',
                       'peta',
                       'keterangan',
                      ];
			InputValid(parameter);
      
      var parameter = {}
      
      parameter["alamat"] = $("#alamat").val();
      parameter["telpon"] = $("#telpon").val();
      parameter["email"] = $("#email").val();
      parameter["twitter"] = $("#twitter").val();
      parameter["facebook"] = $("#facebook").val();
      parameter["google"] = $("#google").val();
      parameter["instagram"] = $("#instagram").val();
      parameter["peta"] = $("#peta").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>dasar_website/update_dasar_website';
      var parameterRv = [
                       
                       'alamat',
                       'telpon',
                       'email',
                       'twitter',
                       'facebook',
                       'google',
                       'instagram',
                       'peta',
                       'keterangan',
                      ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        load_data_dasar_website();
      }
    });
  });
</script>