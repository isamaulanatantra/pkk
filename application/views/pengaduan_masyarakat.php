<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title><?php echo $judul_halaman; ?> <?php echo base_url(); ?></title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
		
  <!-- BARU SAJA 3.0.5 -->
	
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.costum.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
	
  <!-- END BARU SAJA 3.0.5 -->
	
	<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="../../https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="../../https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
		
		<!-- Alert Confirmation -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/Typeahead-BS3-css.css" id="toggleCSS" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>boots/dist/css/bootstrap-timepicker.min.css" />
		<!-- Progress bar -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrap-combined.min.css" id="toggleCSS" />
		
		
</head>
<!-- ADD THE CLASS layout-top-nav TO REMOVE THE SIDEBAR. -->
<body class="hold-transition">
    <!-- Site wrapper -->
  <div class="wrapper">
      
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
  	<div class="container">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link bg-primary elevation-1" data-widget="pushmenu" href="<?php echo base_url(); ?><?php echo $nama_halaman; ?>"><i class="fa fa-wechat"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a target="_blank" href="<?php echo base_url(); ?>" class="nav-link text-primary"><?php echo $keterangan ?></a>
      </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>
  	</div>

  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
  	<div class="container">
		
		
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1><?php echo $judul_halaman; ?></h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?><?php echo $nama_halaman; ?>">HOME</a></li>
              <li class="breadcrumb-item active"><?php echo $judul_halaman; ?></li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content" id="awal">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-9">
			<?php $this -> load -> view($main_view);  ?>
          </div>
          <div class="col-md-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Kontak Kami</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <strong><i class="fa fa-institution mr-1"></i> Pemerintah Daerah</strong>

                <p class="text-muted">
                  <?php echo $keterangan; ?>
                </p>

                <hr>

                <strong><i class="fa fa-map-marker mr-1"></i> Alamat</strong>

                <p class="text-muted"><?php echo $alamat; ?></p>

                <hr>

                <strong><i class="fa fa-envelope-o mr-1"></i> Email</strong>

                <p class="text-muted">
                  <?php echo $email; ?>
                </p>

                <hr>

                <strong><i class="fa fa-tty mr-1"></i> Telephone</strong>

                <p class="text-muted"><?php echo $telpon; ?></p>
              </div>
              <!-- /.card-body -->
            </div>
			<div class="info-box mb-3">
              <a href="<?php echo $google; ?>" class="info-box-icon bg-danger elevation-1"><i class="fa fa-google-plus"></i></a>
              <a href="<?php echo $facebook; ?>" class="info-box-icon bg-primary elevation-1"><i class="fa fa-facebook"></i></a>
              <a href="<?php echo $instagram; ?>" class="info-box-icon bg-danger-gradient elevation-1"><i class="fa fa-instagram"></i></a>
              <a href="<?php echo $twitter; ?>" class="info-box-icon bg-info elevation-1"><i class="fa fa-twitter"></i></a>
            </div>
            <div class="info-box bg-success-gradient">
              <span class="info-box-icon"><i class="fa fa-eye-slash"></i></span>

              <div class="info-box-content">
                <!-- <span class="info-box-text">Likes</span> -->
                <span class="info-box-number" id="hit_counter"></span>

                <div class="progress">
                  <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                  Pengunjung halaman ini
                </span>
              </div>
            </div>
						<div class="info-box bg-primary">
              <span class="info-box-icon"><i class="fa fa-files-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Permohonan Informasi</span>
                <span class="info-box-number">Total 
								<?php
									$w = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 1
									and tujuan_penggunaan_informasi!='Pengaduan Masyarakat'
									");
									echo $jumlah_komentar = $w->num_rows();
								?>
								</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                  <a href="https://ppid.wonosobokab.go.id/permohonan_informasi_publik" class="text-muted">30 OPD Kabupaten Wonosobo</a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
						<div class="info-box bg-danger">
              <span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Pengaduan Masyarakat</span>
                <span class="info-box-number">Total 
								<?php
									$w = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 1
									and tujuan_penggunaan_informasi='Pengaduan Masyarakat'
									");
									echo $jumlah_komentar = $w->num_rows();
								?>
								</span>

                <div class="progress">
                  <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
                  <a href="https://ppid.wonosobokab.go.id/pengaduan_masyarakat" class="text-muted">30 OPD Kabupaten Wonosobo</a>
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
			<div class="info-box bg-warning-gradient">
              <span class="info-box-icon"><i class="fa fa-street-view"></i></span>

              <div class="info-box-content">
                <!-- <span class="info-box-text">Events</span> -->
                <span class="info-box-number" id="visitor"></span>

                <div class="progress">
                  <div class="progress-bar" style="width: 70%"></div>
                </div>
                <span class="progress-description">
					Dalam  
					<?php
					function hitungHari($awal,$akhir){
					$tglAwal = strtotime($awal);
					$tglAkhir = strtotime($akhir);
					$jeda = abs($tglAkhir - $tglAwal);
					return floor($jeda/(60*60*24));
					}
					echo hitungHari('2018-01-01',date('Y-m-d'));
					?>
					hari di tahun 2018
                </span>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

  	</div>
  </div>
  <!-- /.content-wrapper -->
	
  <footer class="main-footer">
  	<div class="container">
    <div class="float-right d-none d-sm-block">
      <b>Version</b> 3.0.6
    </div>
    <strong>Copyright &copy; 2018 <a href="https://diskominfo.wonosobokab.go.id/">Kominfo</a>.</strong> All rights
    reserved.
  	</div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- BARU -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/js/bootstrap.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap4.js"></script>


<!-- END BARU -->

    <script type="text/javascript">
      //----------------------------------------------------------------------------------------------
      function reset() {
        $('#toggleCSS').attr('href', '<?php echo base_url(); ?>css/alertify/alertify.default.css');
        alertify.set({
          labels: {
            ok: 'OK',
            cancel: 'Cancel'
          },
          delay: 5000,
          buttonReverse: false,
          buttonFocus: 'ok'
        });
      }
      //----------------------------------------------------------------------------------------------
    </script>
		
            <script>
              function LoadVisitor() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    table:'visitor'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/simpan_visitor/',
                  success: function(html) {
                    $('#visitor').html('Total Pengunjung '+html+' ');
                  }
                });
              }
            </script>
            <script>
              function LoadHitCounter() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    current_url:'<?php echo current_url(); ?>'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/hit_counter/',
                  success: function(html) {
                    $('#hit_counter').html(''+html+' Kali ');
                  }
                });
              }
            </script>
            <script type="text/javascript">
            $(document).ready(function() {
              LoadVisitor();
              LoadHitCounter();
            });
            </script>
		<!-- uut -->
		<script src="<?php echo base_url(); ?>js/uut.js"></script>
		<!-- alertify -->
		<script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
    <!-- Morris.js charts -->
    <script src="<?php echo base_url(); ?>js/plugins/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo base_url(); ?>js/plugins/morris/morris.min.js" type="text/javascript"></script>
		<!--- -->
		<script src="<?php echo base_url(); ?>js/colorbox/jquery.colorbox.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrap-typeahead.js"></script>
    <script src="<?php echo base_url(); ?>js/hogan-2.0.0.js"></script>
		<!-- daterangepicker -->
    <script src="<?php echo base_url(); ?>boots/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
		<script src="<?php echo base_url(); ?>boots/plugins/timepicker/bootstrap-timepicker.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>boots/dist/js/moment-with-locales.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>boots/dist/js/bootstrap-datetimepicker.js"></script>
    <!-- InputMask -->
    <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <script>
      $(function () {
        $("[data-mask]").inputmask();
      });
    </script>
		<!-- Start editor-->
		<script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script>
		
  </body>
</html>
