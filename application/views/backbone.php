<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
    <meta name="og:description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>"/>
    <meta name="og:url" content="<?php echo base_url(); ?>"/>
    <meta name="og:title" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>"/>
    <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>    
		<meta name="og:keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $kata_kunci )){ echo $kata_kunci; } ?>"/>
		<link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />
    <!-- STYLES & JQUERY 
    ================================================== -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>salique-wide/css/style.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>salique-wide/css/icons.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>salique-wide/css/slider.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>salique-wide/css/skinblue.css"/><!-- change skin color -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>salique-wide/css/responsive.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>salique-wide/css/prettyPhoto.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>Template/HTML/assets/css/bootstrap.css" rel="stylesheet">
		<script src="<?php echo base_url(); ?>salique-wide/js/jquery.min.js"></script>
        <?php
        $web=$this->uut->namadomain(base_url());
        $where = array(
          'status' => 1,
          'domain' => $web
          );
        $this->db->where($where);
        $this->db->limit(1);
        $query = $this->db->get('tema_website');
        $tema = 'biru.css';
        foreach ($query->result() as $row)
          {
            $tema = $row->nama_tema_website;
          }
        // echo '<link rel="stylesheet" href="'.base_url().'Template/HTML/assets/css/'.$tema.'" rel="stylesheet">';
        
        $where0 = array(
          'status' => 1,
          'domain' => $web
          );
        $this->db->where($where0);
        $this->db->limit(1);
        $query0 = $this->db->get('header_website');
        $header_website = 'logo.png';
        foreach ($query0->result() as $row0)
          {
            $header_website = $row0->file_name;
          }
        ?>
  </head>
<body>
<!-- TOP LOGO & MENU
================================================== -->
<div class="grid">
	<div class="row space-bot">
		<!--Logo-->
		<div class="c4">
			<a href="<?php echo base_url(); ?>">
			<h1><?php if(!empty( $keterangan )){ echo $keterangan; } ?></h1>
			</a>
		</div>
		<!--Menu-->
		<div class="c8">
			<nav id="topNav">
        <?php echo $menu_atas; ?> 
			</nav>
		</div>
	</div>
</div>

<div class="undermenuarea">
	<div class="boxedshadow">
	</div>
	<!-- SLIDER AREA
	================================================== -->
	<div id="da-slider" class="da-slider">
    <?php
    $where1 = array(
      'status' => 1,
      'domain' => $web
      );
    $this->db->where($where1);
    $query1 = $this->db->get('slide_website');
    $a = 0;
    foreach ($query1->result() as $row1)
      {
        $a = $a+1;
        $keterangan_gambar_slide = $row1->keterangan;
        echo '
          <div class="da-slide">
            <h2></h2>
            <p>';
                  if (strlen($keterangan) >= 200){
                    echo ''.substr($keterangan, 0, 200).' ...';
                  }
                  else{
                    echo ''.$keterangan_gambar_slide.'';
                  }
            echo'
            </p>
            <div class="da-img">
              <div class="added-images"><img src="'.base_url().'media/upload/'.$row1->file_name.'" class="z-depth-1"></div>
            </div>
          </div>
        ';
      }
    ?>
		<nav class="da-arrows">
		<span class="da-arrows-prev"></span>
		<span class="da-arrows-next"></span>
		</nav>
	</div>
</div>
<!-- UNDER SLIDER - BLACK AREA
================================================== -->
<div class="undersliderblack">
	<div class="grid">
		<div class="row space-bot">
			<div class="c12">
				<!--Box 1-->
				<div class="c4 introbox introboxfirst">
					<div class="introboxinner">
						<span class="homeicone">
						<i class="icon-thumbs-up"></i>
						</span>
            <br />
            <?php if(!empty( $keterangan )){ echo $keterangan; } ?>
            <br />
            <span id="visitor"></span>
					</div>
				</div>
				<!--Box 2-->
				<div class="c4 introbox introboxmiddle">
					<div class="introboxinner">
						<span class="homeicone">
						<i class="icon-envelope"></i>
						</span>
            <br />
            <b>Email:</b>
            <br /><?php if(!empty( $email )){ echo $email; } ?>
					</div>
				</div>
				<!--Box 3-->
				<div class="c4 introbox introboxlast">
					<div class="introboxinner">
						<span class="homeicone">
						<i class="icon-phone"></i>
						</span> 
            <br />
            <b>Telpon:</b>
            <br />
            <?php if(!empty( $telpon )){ echo $telpon; } ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="shadowunderslider">
</div>
<!-- START content area
================================================== -->
<div class="grid">
	<div class="row space-bot">
    <div class="c6">
      <?php if(!empty( $KolomKiriAtas )){ echo $KolomKiriAtas; } ?>
      <i class="fa fa-bar-chart-o"></i> 
    </div>
    <div class="c6">
      <?php if(!empty( $KolomKananAtas )){ echo $KolomKananAtas; } ?>
      <h3 class="progress-label">Total Dibaca
          <span class="pull-right" id="hit_counter"></span>
      </h3>
      <div class="progress progress-sm">
          <div class="progress-bar progress-bar-orange" role="progressbar" aria-valuenow="78" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
          </div>
      </div>
    </div>
	</div>
		<!--<div class="shadowundertop"></div>-->
		<div class="row">
      <div class="shadowundertop"></div>
      <div class="row">
        <div class="c12">
          <h1 class="maintitle space-top">
          <span><?php if(!empty( $keterangan )){ echo $keterangan; } ?> Gallery</span>
          </h1>
        </div>
      </div>
		<!-- end filter -->		
		<div class="row space-top">
			<div id="content">
        <?php if(!empty($galery_berita)){ echo $galery_berita; } ?>
			</div>
		</div>
		</div>
    
</div><!-- end grid -->

  <!-- FOOTER
================================================== -->
<div id="wrapfooter">
	<div class="grid">
		<div class="row" id="footer">
			<!-- to top button  -->
			<p class="back-top floatright">
				<a href="#top"><span></span></a>
			</p>
			<!-- 1st column -->
			<div class="c3">
			<?php if(!empty( $KolomKiriBawah )){ echo $KolomKiriBawah; } ?>
			</div>
			<!-- 2nd column -->
			<div class="c3">
				<h2 class="title"><i class="icon-twitter"></i> Follow us</h2>
				<hr class="footerstress">
				<div id="ticker" class="query">
				</div>
			</div>
			<!-- 3rd column -->
			<div class="c3">
				<h2 class="title"><i class="icon-envelope-alt"></i> Kontak Kami</h2>
				<hr class="footerstress">
				<dl>
					<dt><?php if(!empty( $alamat )){ echo $alamat; } ?></dt>
					<dd><span>Telephone: </span><?php if(!empty( $telpon )){ echo $telpon; } ?></dd>
					<dd>E-mail: <a href="mailto:<?php if(!empty( $email )){ echo $email; } ?>">&nbsp;<?php if(!empty( $email )){ echo $email; } ?></a></dd>
					<dd><span>Website: </span><a href="https://<?php echo $web; ?>">https://<?php echo $web; ?></a></dd>
				</dl>
				<ul class="social-links" style="margin-top:15px;">
					<li class="twitter-link smallrightmargin">
					<a href="<?php if(!empty( $twitter )){ echo $twitter; } ?>" class="twitter has-tip" target="_blank" title="Follow Us on Twitter">Twitter</a>
					</li>
					<li class="facebook-link smallrightmargin">
					<a href="<?php if(!empty( $facebook )){ echo $facebook; } ?>" class="facebook has-tip" target="_blank" title="Join us on Facebook">Facebook</a>
					</li>
					<li class="google-link smallrightmargin">
					<a href="<?php if(!empty( $google )){ echo $google; } ?>" class="google has-tip" title="Google +" target="_blank">Google</a>
					</li>
          <!--
					<li class="linkedin-link smallrightmargin">
					<a href="<?php if(!empty( $telpon )){ echo $telpon; } ?>" class="linkedin has-tip" title="Linkedin" target="_blank">Linkedin</a>
					</li>
					<li class="pinterest-link smallrightmargin">
					<a href="<?php if(!empty( $telpon )){ echo $telpon; } ?>" class="pinterest has-tip" title="Pinterest" target="_blank">Pinterest</a>
					</li>
          -->
				</ul>
			</div>
			<!-- 4th column -->
			<div class="c3">
				<h2 class="title"><i class="icon-link"></i> Links</h2>
				<hr class="footerstress">
        <?php if(!empty( $KolomKananBawah )){ echo $KolomKananBawah; } ?>
			</div>
			<!-- end 4th column -->
		</div>
	</div>
</div>
<!-- copyright area -->
<div class="copyright">
	<div class="grid">
		<div class="row">
			<div class="c6">
				 &copy; <?php echo date('Y'); ?> . Website Resmi <?php if(!empty( $domain )){ echo $domain; } ?> <a href="https://diskominfo.wonosobokab.go.id">Diskominfo Kabupaten Wonosobo</a>
			</div>
			<div class="c6">
				<span class="right">
         Version 1.0.0
				</span>
			</div>
		</div>
	</div>
</div>
<!-- END CONTENT AREA -->
    <!-- JAVASCRIPTS
    ================================================== -->
    <!-- all -->
    <script src="<?php echo base_url(); ?>salique-wide/js/modernizr-latest.js"></script>

    <!-- menu & scroll to top -->
    <script src="<?php echo base_url(); ?>salique-wide/js/common.js"></script>

    <!-- slider -->
    <script src="<?php echo base_url(); ?>salique-wide/js/jquery.cslider.js"></script>

    <!-- cycle -->
    <script src="<?php echo base_url(); ?>salique-wide/js/jquery.cycle.js"></script>

    <!-- carousel items -->
    <script src="<?php echo base_url(); ?>salique-wide/js/jquery.carouFredSel-6.0.3-packed.js"></script>

    <!-- twitter
    <script src="<?php echo base_url(); ?>salique-wide/js/jquery.tweet.js"></script>
     -->
    <!-- filtering -->
    <script src="<?php echo base_url(); ?>salique-wide/js/jquery.isotope.min.js"></script>

    <!-- gallery -->
    <script src="<?php echo base_url(); ?>salique-wide/js/jquery.prettyPhoto.js"></script>
    <!-- hover effects -->
    <script type="text/javascript" src="<?php echo base_url(); ?>salique-wide/js/mosaic.1.0.1.min.js"></script>

    <script>
      function LoadVisitor() {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            table:'visitor'
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>visitor/simpan_visitor/',
          success: function(html) {
            $('#visitor').html('Total Pengunjung '+html+' ');
          }
        });
      }
    </script>
    <script>
      function LoadHitCounter() {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            current_url:'<?php echo base_url(); ?>index.php'
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>visitor/hit_counter/',
          success: function(html) {
            $('#hit_counter').html(''+html+' Kali ');
          }
        });
      }
    </script>
    <script type="text/javascript">
    $(document).ready(function() {
      LoadVisitor();
      LoadHitCounter();
    });
    </script>
</body>
</html>
