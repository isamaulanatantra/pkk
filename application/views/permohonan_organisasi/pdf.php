<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<?php
function int_to_words($x)
  	 {
  	     $nwords = array(    "zero", "one", "two", "three", "four", "five", "six", "seven",
  	                     "eight", "nine", "ten", "eleven", "twelve", "thirteen",
  	                     "fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
  	                     "nineteen", "twenty", 30 => "thirty", 40 => "forty",
  	                     50 => "fifty", 60 => "sixty", 70 => "seventy", 80 => "eighty",
  	                     90 => "ninety" );
  	     if(!is_numeric($x))
  	     {
  	         $w = '#';
  	     }else if(fmod($x, 1) != 0)
  	     {
  	         $w = '#';
  	     }else{
  	         if($x < 0)
  	         {
  	             $w = 'minus ';
  	             $x = -$x;
  	         }else{
  	             $w = '';
  	         }
  	         if($x < 21)
  	         {
  	             $w .= $nwords[$x];
  	         }else if($x < 100)
  	         {
  	             $w .= $nwords[10 * floor($x/10)];
  	             $r = fmod($x, 10);
  	             if($r > 0)
  	             {
  	                 $w .= '-'. $nwords[$r];
  	             }
  	         } else if($x < 1000)
  	         {
  	             $w .= $nwords[floor($x/100)] .' hundred';
  	             $r = fmod($x, 100);
  	             if($r > 0)
  	             {
  	                 $w .= ' and '. int_to_words($r);
  	             }
  	         } else if($x < 1000000)
  	         {
  	             $w .= int_to_words(floor($x/1000)) .' thousand';
  	             $r = fmod($x, 1000);
  	             if($r > 0)
  	             {
  	                 $w .= ' ';
  	                 if($r < 100)
  	                 {
  	                     $w .= 'and ';
  	                 }
  	                 $w .= int_to_words($r);
  	             }
  	         } else {
  	             $w .= int_to_words(floor($x/1000000)) .' million';
  	             $r = fmod($x, 1000000);
  	             if($r > 0)
 	             {
  	                 $w .= ' ';
  	                 if($r < 100)
  	                 {
  	                     $word .= 'and ';
  	                 }
  	                 $w .= int_to_words($r);
  	             }
  	         }
  	     }
  	     return $w;
  	 }

Class Terbilang {
	function terbilang() {
		$this->dasar = array(1=>'satu','dua','tiga','empat','lima','enam','tujuh','delapan','sembilan');
		$this->angka = array(1000000000,1000000,1000,100,10,1);
		$this->satuan = array('milyar','juta','ribu','ratus','puluh','');
	}
	function eja($n) {
		$str = '';
		$i=0;
		while($n!=0){
			$count = (int)($n/$this->angka[$i]);
			if($count>=10) $str .= $this->eja($count). " ".$this->satuan[$i]." ";
			else if($count > 0 && $count < 10)
			$str .= $this->dasar[$count] . " ".$this->satuan[$i]." ";
			$n -= $this->angka[$i] * $count;
			$i++;
		}
		$str = preg_replace("/satu puluh (\w+)/i","\\1 belas",$str);
		$str = preg_replace("/satu (ribu|ratus|puluh|belas)/i","se\\1",$str);
		return $str;
	}
}
?>

<?php

?>
  
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Cetak</title>
	<style type="text/css">
		html,
		body {
			background-color: #fff;
			margin:none;
			padding-top:20mm;
			padding-right:20mm;
			padding-bottom:15mm;
			padding-left:30mm;
			font-family: Arial Black,Arial Bold,Gadget,sans-serif;
			color: #000000;
			font-size:11px;
			text-align:justify;
      background-image: url('./Slide1.jpg');
      background-position: 50% 300px;
      background-repeat: no-repeat;      
		}
		
		.footer {
				bottom: 0.5cm;
		}
		.pagenum:before {
				content: counter(page);
		}
		p {
    text-indent: 50px;
		} 
		table{
		margin-bottom:1mm;
		}
    td{
      padding :none;
    }
	</style>
</head>
<body>
<?php
$fields     = "*";
$where      = array(
  'permohonan_organisasi.id_permohonan_organisasi' => $this->input->get('id_permohonan_organisasi')
);
$this->db->select("$fields");
$this->db->where($where);
$query=$this->db->get('permohonan_organisasi');
foreach ($query->result() as $row)
  {
    $nomor_permohonan_organisasi=$row->id_permohonan_organisasi;
    $nomor_permohonan_organisasi=$row->nomor_permohonan_organisasi;
    $tanggal_permohonan_organisasi=$row->tanggal_permohonan_organisasi;
  }
?>

<table style="border-bottom:solid 3px #000000; width:155mm;">
    <tr>
      <td valign="top" style="width:15mm;">
        <?php  
        $path1 = './logokiri.jpg';
        echo '<img src="'.$path1.'" />';
        ?>
      </td>
      <td valign="top" style="width:115mm; text-align:center;">
        <div style="font-weight:bold; text-align:center; font-size:16;">
          Header 1 Pdf
        </div>
        <div style="text-align:center; font-size:10;">
          Header 2 Pdf
        </div>
      </td>
      <td valign="top" style="width:20mm;">
        <?php  
        $path2 = './logokanan.jpg';
        echo '<img src="'.$path2.'" />';
        ?>
      </td>
    </tr>
</table>

<table style="margin-bottom:0mm; margin-top:4mm; width:155mm;">
	<tr>
    <td style= "text-align:center; font-weight:bold; font-size:12;">
      Judul Pdf
    </td>
	</tr>
</table>
				
<table style="margin-bottom:2mm; width:155mm; text-align:justify; font-size:10;">
	<tr>
		<td valign="top" colspan="3" >&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" colspan="3" >&nbsp;</td>
	</tr>
	<tr>
		<td valign="top" style="width:50mm;">Nomor Izin Eksperimen</td>
		<td valign="top" style="width:2mm;">:</td>
		<td valign="top" style="width:103mm; font-weight:bold;"><?php echo $nomor_permohonan_organisasi; ?></td>
	</tr>
	<tr>
		<td valign="top" style="text-align:left;">Tanggal Izin Eksperimen</td>
		<td valign="top">:</td>
		<td valign="top style="text-align:left;"">
    <?php echo $this->Crud_model->dateBahasaIndo($tanggal_permohonan_organisasi); ?>,
    </td>
	</tr>
</table>

<Table Style="Margin-Bottom:2mm; Width:155mm; Text-Align:center; Font-Size:10;">
	<tr>
		<td valign="top" style="text-align:center;">
    Kolom 1
    </td>
		<td valign="top" style="text-align:center;">
    Kolom 2
    </td>
		<td valign="top" style="text-align:center;">
    Kolom 3
		</td>
	</tr>
</table>

</body>
</html>