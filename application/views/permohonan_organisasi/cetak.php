
<?php
	$web=$this->uut->namadomain(base_url());
	$where0 = array(
		'status' => 1,
		'domain' => $web
		);
	$this->db->where($where0);
	$this->db->limit(1);
	$query0 = $this->db->get('bingkai_surat_pengesahan');
	$bingkai_surat_pengesahan = 'bingkai_surat_pengesahan.jpg';
	foreach ($query0->result() as $row0){
		$bingkai_surat_pengesahan = $row0->file_name;
	}
?>
<style>
	#bingkai_surat_pengesahan {
		position: relative;
		transition: all 0.2s ease 0s;
		width: 995px;
		height: 1415px;
		background-image: url('<?php echo base_url(); ?>media/upload/<?php echo $bingkai_surat_pengesahan; ?>');
		background-repeat: no-repeat;
		background-position: top center;
		background-size: 100% auto;
	}
</style>
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Charm">

<style>
.w3-surat_pengesahan {
  font-family: 'Charm', cursive;
  font-size: 48px;
  text-shadow: 4px 4px 4px #aaa;
}
</style>

<div class="col-12">
	<div id="bingkai_surat_pengesahan">
		<center>
			<table>
				<tr>
					<th colspan="4">
						<br />
						<br />
						<br />
						<center>
							<table style="width:60%;">
								<tr>
									<td>
										<center>
										<img style="width:20%;" src="<?php echo base_url(); ?>/media/logo wonosobo.png" />
										</center>
									</td>
								</tr>
								<tr>
									<td>
										<center>
										<h3><?php if(!empty($keterangan)){ echo strtoupper($keterangan); } ?></h3>
										</center>
									</td>
								</tr>
								<tr>
									<td>
										<hr />
									</td>
								</tr>
							</table>
						</center>
					</th>
					<th>
					</th>
				</tr>
				<tr>
					<th>
						<center>
							<table style="width:70%;">
								<tr>
									<td>
										<center>
										<div class="w3-surat_pengesahan">
										<u>Surat Pengesahan</u>
										</div>
										<h4>Nomor : <?php if(!empty($nomor_permohonan_organisasi)){ echo $nomor_permohonan_organisasi; } ?></h4>
										</center>
									</td>
								</tr>
							</table>
						</center>
					</th>
				</tr>
				<tr>
					<td colspan="5">
						<center>
							<table style="width:80%;">
								<tr>
									<td colspan="5"><center><br />Memperhatikan:</center></td>
								</tr>
								<tr>
									<td style="width:30px;">1. </td>
									<td colspan="4">Undang-Undang Nomor 10 Tahun 2009 Tentang Kepariwisataan;</td>
								</tr>
								<tr>
									<td valign="top">2. </td>
									<td colspan="4">Undang-Undang Nomor 23 Tahun 2014 Tentang Pemerintah Daerah, sebagaimana telah diubah beberapa kali terakhir dengan Undang-Undang Nomor 9 Tahun 2015 Tentang Perubahan Kedua Atas Undang-Undang Nomor 23 Tahun 2014 Tentang Pemerintah Daerah</td>
								</tr>
								<tr>
									<td>3. </td>
									<td colspan="4">Undang-Undang Nomor 5 Tahun 2017 Tentang Pemajuan Kebudayaan;</td>
								</tr>
								<tr>
									<td valign="top">4. </td>
									<td colspan="4">Peraturan Daerah Kabupaten Wonosobo Nomor 12 Tahun 2016 Tentang Pembentukan dan Susunan Perangkat Daerah Kabupaten Wonosobo;</td>
								</tr>
								<tr>
									<td valign="top">5. </td>
									<td colspan="5">Peraturan Bupati Wonosobo Nomor 51 Tahun 2016 Tentang Kedudukan, Susunan Organisasi, Tugas dan Fungsi Serta Tata Kerja Dinas Pariwisata Dan Kebudayaan Kabupaten Wonosobo.</td>
								</tr>
							</table>
							<table style="width:80%;">
								<tr>
									<td colspan="5"><center><br />Mengesahkan:</center></td>
								</tr>
								<tr>
									<td style="width:290px;"><b>NAMA ORGANISASI</b></td>
									<td colspan="4"><b>: <?php if(!empty($nama_organisasi)){ echo strtoupper($nama_organisasi); } ?></b></td>
								</tr>
								<tr>
									<td>JENIS KESENIAN</td>
									<td colspan="4">: <?php if(!empty($jenis_kesenian)){ echo $jenis_kesenian; } ?></td>
								</tr>
								<tr>
									<td>BERDIRI SEJAK TANGGAL</td>
									<td colspan="4">: <?php if(!empty($tanggal_pendirian)){ echo $tanggal_pendirian; } ?></td>
								</tr>
								<tr>
									<td valign="top">ALAMAT LENGKAP</td>
									<td colspan="4">:&nbsp;DUSUN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:<?php if(!empty($alamat_organisasi)){ echo $alamat_organisasi; } ?><br />&nbsp;&nbsp;DESA/KELURAHAN:&nbsp;<?php if(!empty($nama_desa)){ echo $nama_desa; } ?><br />&nbsp;&nbsp;KECAMATAN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php if(!empty($nama_kecamatan)){ echo $nama_kecamatan; } ?><br />&nbsp;&nbsp;KABUPATEN&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;<?php if(!empty($nama_kabupaten)){ echo $nama_kabupaten; } ?><br /></td>
								</tr>
								<tr>
									<td>KETUA</td>
									<td colspan="4">:&nbsp;<?php if(!empty($nama_pemohon)){ echo $nama_pemohon; } ?></td>
								</tr>
								<tr>
									<td>JUMLAH ANGGOTA</td>
									<td colspan="4">:&nbsp;<?php if(!empty($jumlah_anggota)){ echo $jumlah_anggota; } ?> ORANG</td>
								</tr>
								<tr>
									<td colspan="5">&nbsp;</td>
								</tr>
								<tr>
									<td colspan="5">
										<table style="width:100%;">
											<tr>
												<td style="width:290px;"></td>
												<td style="width:580px;"><center><strong>WONOSOBO, <?php if(!empty($tanggal_permohonan_organisasi)){ echo $tanggal_permohonan_organisasi; } ?><br />KEPALA DINAS PARIWISATA DAN KEBUDAYAAN<br />KABUPATEN WONOSOBO</strong><br /><br /><br />
												<strong><u>Drs. ONE ANDANG WARDOYO, M.Si</u></strong><br />Pembina Utama Muda<br />NIP. 19680925 198803 1 003</center></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</center>
					</td>
				</tr>
			</table>
		</center>
	</div>
</div>

<div class="col-12" style="margin-top:170px;">
	<table>
		<tr>
			<td colspan="5">
			<!--<hr />-->
			</td>
		</tr>
		<tr>
			<td colspan="5">
				<center>
					<table style="width:90%;">
						<tr>
							<td></td>
							<td style="width:290px;"></td>
							<td colspan="3" style="text-align:right;"></td>
						</tr>
						<tr>
							<td>Nama Organisasi</td>
							<td style="width:440px;">: <?php if(!empty($nama_organisasi)){ echo $nama_organisasi; } ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>Kesenian</td>
							<td>: <?php if(!empty($kesenian)){ echo $kesenian; } ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>Jenis Kegiatan</td>
							<td>: <?php if(!empty($jenis_kesenian)){ echo $jenis_kesenian; } ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>Desa/Kelurahan</td>
							<td>: <?php if(!empty($nama_desa)){ echo $nama_desa; } ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>Kecamatan</td>
							<td>: <?php if(!empty($nama_kecamatan)){ echo $nama_kecamatan; } ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td>&nbsp;&nbsp;<?php if(!empty($nama_kabupaten)){ echo $nama_kabupaten; } ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>Wonosobo, <?php if(!empty($tanggal_permohonan_organisasi)){ echo $tanggal_permohonan_organisasi; } ?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>Nomor</td>
							<td>: <?php if(!empty($nomor_permohonan_organisasi)){ echo $nomor_permohonan_organisasi; } ?></td>
							<td></td>
							<td>Kepada :</td>
							<td></td>
						</tr>
						<tr>
							<td valign="top">Sifat</td>
							<td valign="top">: <?php if(!empty($sifat_permohonan_organisasi)){ echo $sifat_permohonan_organisasi; } ?></td>
							<td></td>
							<td valign="top" rowspan="3">Yth. Bp. <br /><?php if(!empty($yth_permohonan_organisasi)){ echo $yth_permohonan_organisasi; } ?></td>
							<td></td>
						</tr>
						<tr>
							<td>Lampiran</td>
							<td>: <?php if(!empty($lampiran_permohonan_organisasi)){ echo $lampiran_permohonan_organisasi; } ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>Perihal</td>
							<td>: <?php if(!empty($perihal_permohonan_organisasi)){ echo $perihal_permohonan_organisasi; } ?></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td></td>
							<td>Di_</td>
							<td></td>
						</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php if(!empty($di_permohonan_organisasi)){ echo strtoupper($di_permohonan_organisasi); } ?></td>
						<td></td>
					</tr>
					</table>
				</center>
				<center>
					<table style="width:80%;">
						<tr>
							<td colspan="5">Yang bertandatangan dibawah ini: </td>
						</tr>
						<tr>
							<td style="width:250px;">1. Nama </td>
							<td colspan="4">: <?php if(!empty($nama_pemohon)){ $nama_pemohon1=strtolower($nama_pemohon); echo ucwords($nama_pemohon1); } ?></td>
						</tr>
						<tr>
							<td>2. Tempat, Tanggal Lahir</td>
							<td colspan="4">: <?php if(!empty($tempat_lahir_pemohon)){ echo $tempat_lahir_pemohon; } ?>, <?php if(!empty($tanggal_lahir_pemohon)){ echo $tanggal_lahir_pemohon; } ?></td>
						</tr>
						<tr>
							<td>3. Pekerjaan</td>
							<td colspan="4">: <?php if(!empty($pekerjaan_pemohon)){ echo $pekerjaan_pemohon; } ?></td>
						</tr>
						<tr>
							<td>4. Alamat </td>
							<td colspan="4">: <?php if(!empty($alamat_pemohon)){ $alamat_pemohon1=strtolower($alamat_pemohon);echo ucwords($alamat_pemohon1); } ?><br />&nbsp;&nbsp;&nbsp;Desa <?php if(!empty($nama_desa_pemohon)){ $nama_desa_pemohon1=strtolower($nama_desa_pemohon);echo ucwords($nama_desa_pemohon1); } ?><br />&nbsp;&nbsp;&nbsp;Kecamatan <?php if(!empty($nama_kecamatan_pemohon)){ $nama_kecamatan_pemohon1=strtolower($nama_kecamatan_pemohon); echo ucwords($nama_kecamatan_pemohon1); } ?><br />&nbsp;&nbsp;&nbsp;No. Hp <?php if(!empty($nomor_telp_pemohon)){ echo $nomor_telp_pemohon; } ?></td>
						</tr>
						<tr>
							<td>5. Keperluan </td>
							<td colspan="4">: <?php if(!empty($kepertluan)){ echo $kepertluan; } ?></td>
						</tr>
						<tr>
							<td>6. Tanggal Pendirian </td>
							<td colspan="4">: <?php if(!empty($tanggal_pendirian)){ echo $tanggal_pendirian; } ?></td>
						</tr>
						<tr>
							<td>7. Jumlah Anggota </td>
							<td colspan="4">: Laki-laki <?php if(!empty($jumlah_anggota_pria)){ echo $jumlah_anggota_pria; } ?> Perempuan <?php if(!empty($jumlah_anggota_wanita)){ echo $jumlah_anggota_wanita; } ?> Jumlah <?php if(!empty($jumlah_anggota)){ echo $jumlah_anggota; } ?></td>
						</tr>
						<tr>
							<td colspan="5">Demikian atas perkenannya disampaikan terima kasih.<br />Organisasi kesenian <?php if(!empty($nama_organisasi)){ echo $nama_organisasi; } ?><br /><?php if(!empty($nama_desa)){ echo $nama_desa; } ?></td>
						</tr>
						<tr>
							<td colspan="5"><br><br><center>MENGETAHUI</center><br><br></td>
						</tr>
						<tr>
							<td colspan="5">
								<table style="width:100%;">
									<tr>
										<td style="width:290px;"><center><strong>Kepala Desa/Kelurahan</strong><br><br><br><br><u>NAMA KADES</u><br><br><br>Camat<br><br><br><br><u>NAMA CAMAT</u></center></td>
										<td style="width:290px;"></td>
										<td style="width:290px;"><center><strong>Ketua</strong><br><br><br><br><strong><u><?php if(!empty($nama_pemohon)){ echo $nama_pemohon; } ?></u><br><br><br>Kapolsek<br><br><br><br><u>NAMA KAPOLSEK</u></strong></center></td>
									</table>
								</tr>
							</td>
						</tr>
					</table>
				</center>
			</td>
		</tr>
	</table>
</div>

<div class="col-12" style="margin-top:170px;">
	<table style="width:100%;">
		<tr>
			<td colspan="5" style="text-align:center;"><h2>PROFIL</h2></td>
		</tr>
		<tr>
			<td colspan="5">
				<table>
					<tr>
						<td>Organisasi Kesenian</td>
						<td>: </td>
						<td><?php if(!empty($nama_organisasi)){ echo $nama_organisasi; } ?></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Desa/Kelurahan</td>
						<td>: </td>
						<td><?php if(!empty($nama_desa)){ echo $nama_desa; } ?></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Kecamatan</td>
						<td>: </td>
						<td><?php if(!empty($nama_kecamatan)){ echo $nama_kecamatan; } ?></td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td>Kabupaten</td>
						<td>: </td>
						<td><?php if(!empty($nama_kabupaten)){ echo $nama_kabupaten; } ?></td>
						<td></td>
						<td></td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="5"><hr /></td>
		</tr>
		<tr>
				<center>
					<table>
						<tr>
							<td>1. </td>
							<td>Jenis Kesenian</td>
							<td>: <?php if(!empty($jenis_kesenian)){ echo $jenis_kesenian; } ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>2. </td>
							<td>fungsi Kesenian</td>
							<td>: <?php if(!empty($fungsi_kesenian)){ echo $fungsi_kesenian; } ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>3. </td>
							<td>Nama Kesenian</td>
							<td>: <?php if(!empty($nama_kesenian)){ echo $nama_kesenian; } ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>4.</td>
							<td>Tanggal Berdiri</td>
							<td>: <?php if(!empty($tanggal_pendirian)){ echo $tanggal_pendirian; } ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>5.</td>
							<td>Jumlah Anggota</td>
							<td>: <?php if(!empty($jumlah_anggota)){ echo $jumlah_anggota; } ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>6.</td>
							<td>Pengalaman Pentas</td>
							<td>: <?php if(!empty($pengalaman_pentas)){ echo $pengalaman_pentas; } ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>7.</td>
							<td>Penghargaan yang pernah diterima</td>
							<td>: <?php if(!empty($penghargaan_yang_pernah_diterima)){ echo $penghargaan_yang_pernah_diterima; } ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>8.</td>
							<td>Fasilitas/peralatan yang dimiliki</td>
							<td>: <?php if(!empty($fasilitas_peralatan_yang_dimiliki)){ echo $fasilitas_peralatan_yang_dimiliki; } ?></td>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td>9.</td>
							<td>Hambatan/kendala</td>
							<td>: <?php if(!empty($hambatan_kendala)){ echo $hambatan_kendala; } ?></td>
							<td></td>
							<td></td>
						</tr>
					</table>
				</center>
			</td>
		</tr>
	</table>
	<br />
	<br />
	<br />
	<table style="width:100%;">
		<tr>
			<td colspan="5" style="text-align:center;"><center><h2>RIWAYAT/SEJARAH SINGKAT ORGANISASI</h2></center></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align:left;"><?php if(!empty($riwayat_sejarah)){ echo $riwayat_sejarah; } ?></td>
		</tr>
	</table>
	<table style="width:100%;">
		<tr>
			<td colspan="5" style="text-align:left;"><h2>LAMPIRAN</h2></td>
		</tr>
		<tr>
			<td colspan="5" style="text-align:left;">
				<table style="width:100%;">
					<tr>
						<th>NO</th>
						<th>NAMA</th>
						<th>UMUR</th>
						<th>JABATAN</th>
						<th>KETERANGAN</th>
					</tr>
					<?php
						$web=$this->uut->namadomain(base_url());
						$where0 = array(
							'id_permohonan_organisasi' => $id_permohonan_organisasi
							);
						$this->db->where($where0);
						$query0 = $this->db->get('anggota_organisasi');
						$nomor=0;
						foreach ($query0->result() as $row0)
							{
								$nomor=$nomor+1;
								echo'
									<tr>
										<td>'.$nomor.'</td>
										<td>'.$row0->nama_anggota_organisasi.'</td>
										<td>'.$row0->umur_anggota_organisasi.'</td>
										<td>'.$row0->jabatan_anggota_organisasi.'</td>
										<td>'.$row0->keterangan_anggota_organisasi.'</td>
									</tr>
								';
							}
					?>
				</table>
			</td>
		</tr>
	</table>
</div>
