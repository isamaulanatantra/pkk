<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 style="display:none;">Rekap Catatan Data dan Kegiatan Warga</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga">Home</a></li>
          <li class="breadcrumb-item active">Rekap Catatan Data dan Kegiatan Warga</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">


  <div class="row" id="awal">
    <div class="col-12">
      <input name="tabel" id="tabel" value="input_rekap_data_umum" type="hidden" value="">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title"><i class="fa fa-th"></i> REKAP CATATAN DATA DAN KEGIATAN WARGA</h3>
          <div class="card-tools">
            <div class="input-group input-group-sm">
              <select class="form-control input-sm pull-right" id="tahun" name="tahun" style="width: 150px;">
                <option value="-">Pilih</option>
                <?php
                for ($i = date(Y); $i >= 2015; $i--) {
                  echo '<option value="' . $i . '">' . $i . '</option>';
                }
                ?>
              </select>
              <div class="input-group-btn">
                <button class="btn btn-sm btn-default" id="tampil"><i class="fa fa-search"></i> Tampil</button>
              </div>
            </div>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th rowspan="3">NO.</th>
                <th rowspan="3">NAMA DESA</th>
                <th colspan="4">JUMLAH KELOMPOK</th>
                <th colspan="2">JUMLAH</th>
                <th colspan="11">JUMLAH ANGGOTA KELUARGA</th>
                <th colspan="5">KRITERIA RUMAH</th>
                <th colspan="4">SUMBER AIR KELG</th>
                <th rowspan="3">JUML JAMBAN KELG</th>
                <th colspan="2">MAKANAN</th>
                <th colspan="4">WARGA MENGIKUTI KEGIATAN</th>
                <th rowspan="3">KET</th>
              </tr>
              <tr>
                <th rowspan="2">PKK DUSUN</th>
                <th rowspan="2">PKK RW</th>
                <th rowspan="2">PKK RT</th>
                <th rowspan="2">DAWIS</th>
                <th rowspan="2">KRT</th>
                <th rowspan="2">KK</th>
                <th colspan="2">TOTAL</th>
                <th colspan="2">BALITA</th>
                <th rowspan="2">PUS</th>
                <th rowspan="2">WUS</th>
                <th rowspan="2">BUMIL</th>
                <th rowspan="2">BUSUI</th>
                <th rowspan="2">LANSIA</th>
                <th colspan="2">3 BUTA</th>
                <th rowspan="2">SEHAT</th>
                <th rowspan="2">KRG SEHT</th>
                <th rowspan="2">MEML TPS</th>
                <th rowspan="2">MEML SPAL</th>
                <th rowspan="2">JAMBAN KEL</th>
                <th rowspan="2">PDAM</th>
                <th rowspan="2">SUMUR</th>
                <th rowspan="2">SUNGAI</th>
                <th rowspan="2">DLL</th>
                <th rowspan="2">BERAS</th>
                <th rowspan="2">NON BRS</th>
                <th rowspan="2">UP 2K</th>
                <th rowspan="2">PEMANF PEKRANGAN</th>
                <th rowspan="2">INDUSTRI RT</th>
                <th rowspan="2">KESLING</th>
              </tr>
              <tr>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
                <th>L</th>
                <th>P</th>
              </tr>
            </thead>
            <tbody id="tbl_utama">
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          <ul class="pagination pagination-sm m-0 float-right" id="pagination" style="display:none;">
          </ul>
          <div class="overlay" id="spinners_tbl_data_input_rekap_data_umum" style="display:none;">
            <i class="fa fa-refresh fa-spin"></i>
          </div>
        </div>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->


</section>

<script type="text/javascript">
  $(document).ready(function() {
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/list_kecamatan',
      success: function(html) {
        $('#tbl_utama').html(html);
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#tampil").on("click", function(e) {
      e.preventDefault();
      $('#btnExport').hide();
      var tahun = $("#tahun").val();
      var tanggal = 25;
      for (var i = 1; i <= 34; i++) {
        window.setTimeout(list_1(i, tahun), 1000);
        window.setTimeout(list_2(i, tahun), 1000);
        window.setTimeout(list_3(i, tahun), 1000);
        window.setTimeout(list_4(i, tahun), 1000);
        window.setTimeout(list_5(i, tahun), 1000);
        window.setTimeout(list_6(i, tahun), 1000);
        window.setTimeout(list_7(i, tahun), 1000);
        window.setTimeout(list_8(i, tahun), 1000);
        window.setTimeout(list_9(i, tahun), 1000);
        window.setTimeout(list_10(i, tahun), 1000);
        window.setTimeout(list_11(i, tahun), 1000);
        window.setTimeout(list_12(i, tahun), 1000);
        window.setTimeout(list_13(i, tahun), 1000);
        window.setTimeout(list_14(i, tahun), 1000);
        window.setTimeout(list_15(i, tahun), 1000);
        window.setTimeout(list_16(i, tahun), 1000);
        window.setTimeout(list_17(i, tahun), 1000);
        window.setTimeout(list_18(i, tahun), 1000);
        window.setTimeout(list_19(i, tahun), 1000);
        window.setTimeout(list_20(i, tahun), 1000);
        window.setTimeout(list_21(i, tahun), 1000);
        window.setTimeout(list_22(i, tahun), 1000);
        window.setTimeout(list_23(i, tahun), 1000);
        window.setTimeout(list_24(i, tahun), 1000);
        window.setTimeout(list_25(i, tahun), 1000);
        window.setTimeout(list_26(i, tahun), 1000);
        window.setTimeout(list_27(i, tahun), 1000);
        window.setTimeout(list_28(i, tahun), 1000);
        window.setTimeout(list_29(i, tahun), 1000);
        window.setTimeout(list_30(i, tahun), 1000);
        window.setTimeout(list_31(i, tahun), 1000);
        window.setTimeout(list_32(i, tahun), 1000);
        window.setTimeout(list_33(i, tahun), 1000);
      }
      $('#btnExport').show();
    });
  });
</script>
<script type="text/javascript">
  function list_33(i, tahun) {
    $('#' + i + '_33').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/kesling/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_33').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_32(i, tahun) {
    $('#' + i + '_32').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/industrirumahtangga/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_32').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_31(i, tahun) {
    $('#' + i + '_31').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/hatinyapkk/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_31').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_30(i, tahun) {
    $('#' + i + '_30').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/jumlah_up2k/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_30').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_29(i, tahun) {
    $('#' + i + '_29').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/jumlah_panganmakananpokok_nonberas/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_29').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_28(i, tahun) {
    $('#' + i + '_28').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/jumlah_panganmakananpokok_beras/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_28').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_27(i, tahun) {
    $('#' + i + '_27').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_9/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_27').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_26(i, tahun) {
    $('#' + i + '_26').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/jumlah_krtmemiliki_lain/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_26').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_25(i, tahun) {
    $('#' + i + '_25').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_9/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_25').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_24(i, tahun) {
    $('#' + i + '_24').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/jumlah_krtmemiliki_sumur/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_24').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_23(i, tahun) {
    $('#' + i + '_23').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/jumlah_krtmemiliki_pdam/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_23').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_22(i, tahun) {
    $('#' + i + '_22').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/jumlah_kelestarian_rumahberjamban/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_22').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_20(i, tahun) {
    $('#' + i + '_20').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/jumlah_kelestarian_rumahtmpsampah/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_20').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_21(i, tahun) {
    $('#' + i + '_21').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/jumlah_kelestarian_rumahberspal/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_21').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_19(i, tahun) {
    $('#' + i + '_19').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_rumah_tidaksehat/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_19').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_18(i, tahun) {
    $('#' + i + '_18').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_rumah_sehat/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_18').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_17(i, tahun) {
    $('#' + i + '_17').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_9/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_17').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_34(i, tahun) {
    $('#' + i + '_34').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_pokja_dua_3buta/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_34').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_16(i, tahun) {
    $('#' + i + '_16').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_9/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_16').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_15(i, tahun) {
    $('#' + i + '_15').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_9/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_15').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_14(i, tahun) {
    $('#' + i + '_14').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_9/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_14').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_13(i, tahun) {
    $('#' + i + '_13').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_9/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_13').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_12(i, tahun) {
    $('#' + i + '_12').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/jumlah_perencanaansehat_wus/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_12').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_11(i, tahun) {
    $('#' + i + '_11').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/jumlah_perencanaansehat_pus/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_11').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_10(i, tahun) {
    $('#' + i + '_10').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_9/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_10').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_9(i, tahun) {
    $('#' + i + '_9').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_9/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_9').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_1(i, tahun) {
    $('#' + i + '_1').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_pkk_dusun/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_1').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_2(i, tahun) {
    $('#' + i + '_2').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_pkk_rw/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_2').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_3(i, tahun) {
    $('#' + i + '_3').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_pkk_rt/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_3').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_4(i, tahun) {
    $('#' + i + '_4').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_pkk_dawis/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_4').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_5(i, tahun) {
    $('#' + i + '_5').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_pkk_krt/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_5').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_6(i, tahun) {
    $('#' + i + '_6').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_pkk_kk/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_6').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_7(i, tahun) {
    $('#' + i + '_7').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_pkk_jiwal/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_7').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  function list_8(i, tahun) {
    $('#' + i + '_8').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_kecamatan = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga/sum_pkk_jiwap/?id_kecamatan=' + id_kecamatan + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_kecamatan + '_8').html(html);
      }
    });
  };
</script>