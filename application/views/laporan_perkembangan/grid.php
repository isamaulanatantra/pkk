    <section class="content-header">
      <h1>
        Invoice
        <small>#007612</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Examples</a></li>
        <li class="active">Invoice</li>
      </ol>
    </section>
		<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <center>
            PERKEMBANGAN HARGA RATA-RATA KEBUTUHAN POKOK MASYARAKAT<br />
            3 (TIGA) PASAR TRADISIONAL DI KABUPATEN WONOSOBO<br />
            BULAN AGUSTUS 2017<br />
            </center>
        <!-- 
            <small class="pull-right">Date: <?php //echo date('d M Y'); ?></small> -->
          </h2>
        </div>
        <!-- /.col -->
      </div>
      
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive" id="grid_utama">
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-8">
          <p class="" style="margin-top: -10px; font-size:9px;">
            <i>
            Sumber Data: Pemantauan di Pasar Induk Wonosobo, Pasar Garung, dan Pasar Kertek di Kabupaten Wonosobo Dinas Perdagangan, Koperasi, UKM Kab. Wonosobo
            </i>
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <p class="lead"> </p>

          <div class="table-responsive">
            <table class="">
              <tbody>
                <tr>
                  <th><center>Wonosobo, 15 September 2017</center></th>
                </tr>
                <tr>
                  <td><center>KEPALA DINAS PERDAGANGAN, KOPERASI, UKM KABUPATEN WONOSOBO</center></td>
                </tr>
                <tr>
                  <td><center><br />ttd<br /></center></td>
                </tr>
                <tr>
                  <td><center><u>Drs. AGUS SURYATIN, MT</u><br />Pembina Utama Muda<br />NIP. 19601007 198612 1 001</center></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>
    <div class="clearfix"></div>
    
  <script>
    function load_grid() {
      $('#overlay_data_default').show();
      $('#grid_utama').html('');
      var tahun = $('#load_tahun_apbdesa').val();
      var id_kode_pendapatan = $('#load_pendapatan_desa_apbdesa').val();
      $.ajax({
        type: "POST",
        async: true,
        data: {
          tahun:tahun,
          id_kode_pendapatan:id_kode_pendapatan,
        },
        dataType: "html",
        url: '<?php echo base_url(); ?>harga_kebutuhan_pokok/html_harga_kebutuhan_pokok/',
        success: function(html) {
          $('#grid_utama').append(html);
          $('#overlay_data_default').fadeOut('slow');
        }
      });
    }
  </script>

  <script>
    $(document).ready(function() {
        load_grid();
    });
  </script>
