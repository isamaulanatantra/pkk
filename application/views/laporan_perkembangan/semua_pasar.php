
    <section class="invoice">
      <!-- title row -->
      <div class="row">
          <div class="col-xs-4">
            <div class="no-print">
              <label>Laporan Harian Semua Pasar</label>
              <div class="form-group">
                <label for="dari_tanggal_1">Tanggal</label>
                <input class="form-control" id="dari_tanggal_1" type="text">
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary" id="tampilkan_by_filter">Tampilkan</button>
              </div>
            </div>
          </div>
          <div class="col-xs-12">
            <div class="overlay" id="overlay_data_default" style="display:none;"><i class="fa fa-refresh fa-spin"></i></div>
            <div class="pad table-responsive" id="grid_utama_by_filter">
            </div>
          </div>
      </div>
    </section>
    
<script>
  $(document).ready(function() {
    $('#tampilkan_by_filter').on('click', function(e) {
      e.preventDefault();
      load_grid_utama_by_filter();
    });
  });
</script>

<script>
  function load_grid_utama_by_filter() {
      $('#overlay_data_default').show();
      $('#grid_utama_by_filter').html('');
      var temp = $("#temp").val();
      var dari_tanggal_1 = $("#dari_tanggal_1").val();
      if (dari_tanggal_1 == '') {
          $('#dari_tanggal_1').css('background-color', '#DFB5B4');
        } else {
          $('#dari_tanggal_1').removeAttr('style');
        }
      $.ajax({
        type: "POST",
        async: true,
        data: {
          temp:temp,
          dari_tanggal_1:dari_tanggal_1,
        },
        dataType: "html",
        url: '<?php echo base_url(); ?>laporan_perkembangan_semua_pasar/html_harga_kebutuhan_pokok/',
        success: function(html) {
          $('#div_filter_pasar').hide();
          $('#grid_utama_by_filter').append(html);
          $('#overlay_data_default').fadeOut('slow');
        }
      });
  }
</script>
<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#dari_tanggal_1, #sampai_tanggal_1').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>