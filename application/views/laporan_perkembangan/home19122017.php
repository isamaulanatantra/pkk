  <div class="" id="awal">
        <!-- title row -->
        <div class="row">
            <div class="card">
              <div class="card-body" id="div_filter_pasar">
                <h4 class="card-title">Laporan Harian Berdasarkan Pasar</h4>
                <label for="id_pasar_by_filter">Pasar</label>
                <div class="md-form">
                  <select class="browser-default" id="id_pasar_by_filter" name="id_pasar_by_filter">
                  </select>
                  <input type="hidden" name="hidden_nama_pasar" id="hidden_nama_pasar" value="" class="form-control" />
                </div>
                <div class="md-form">
                  <label for="dari_tanggal_1">Tanggal Pertama</label>
                  <input class="form-control" id="dari_tanggal_1" type="text">
                </div>
                <div class="md-form">
                  <label for="sampai_tanggal_1">Tanggal Kedua</label>
                  <input class="form-control" id="sampai_tanggal_1" type="text">
                </div>
                <div class="md-form">
                  <button type="submit" class="btn btn-primary btn-flat" id="tampilkan_by_filter">Tampilkan</button>
                </div>
              </div>
            </div>
            <div class="container preloader-wrapper big active" id="overlay_data_default" style="display:none;">
              <div class="preloader-wrapper big active">
                <div class="preloader-wrapper big active">
                  <div class="preloader-wrapper big active">
                    <div class="spinner-layer spinner-blue-only">
                      <div class="circle-clipper left">
                        <div class="circle"></div>
                      </div><div class="gap-patch">
                        <div class="circle"></div>
                      </div><div class="circle-clipper right">
                        <div class="circle"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="container table-responsive" id="grid_utama_by_filter">
            </div>
        </div>
        <!-- /.row -->
  </div>

<script>
  function load_id_pasar_by_filter() {
    $('#overlay_data_default').show();
    $('#id_pasar_by_filter').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp,
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>pasar/load_id_pasar_by_filter/',
      success: function(json) {
        var tr = '';
        tr += '<option value="0"> Pilih Pasar</option>';
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
          tr += '<option value="' + json[i].id_pasar + '"> ' + json[i].nama_pasar + ' </option>';
        }
        $('#id_pasar_by_filter').append(tr);
				$('#overlay_data_default').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
    load_id_pasar_by_filter();
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#id_berdasarkan').on('change', function(e) {
    $('#id_pasar_by_filter').html('');
    load_id_pasar_by_filter();
	});
});
</script>

<script>
  $(document).ready(function() {
    $('#tampilkan_by_filter').on('click', function(e) {
      e.preventDefault();
      load_grid_utama_by_filter();
    });
  });
</script>

<script>
  function load_grid_utama_by_filter() {
      $('#overlay_data_default').show();
      $('#grid_utama_by_filter').html('');
      var temp = $("#temp").val();
      var id_pasar_by_filter = $("#id_pasar_by_filter").val();
      var dari_tanggal_1 = $("#dari_tanggal_1").val();
      var sampai_tanggal_1 = $("#sampai_tanggal_1").val();
      if (id_pasar_by_filter == '') {
          $('#id_pasar_by_filter').css('background-color', '#DFB5B4');
        } else {
          $('#id_pasar_by_filter').removeAttr('style');
        }
      if (dari_tanggal_1 == '') {
          $('#dari_tanggal_1').css('background-color', '#DFB5B4');
        } else {
          $('#dari_tanggal_1').removeAttr('style');
        }
      if (sampai_tanggal_1 == '') {
          $('#sampai_tanggal_1').css('background-color', '#DFB5B4');
        } else {
          $('#sampai_tanggal_1').removeAttr('style');
        }
      $.ajax({
        type: "POST",
        async: true,
        data: {
          temp:temp,
          id_pasar_by_filter:id_pasar_by_filter,
          dari_tanggal_1:dari_tanggal_1,
          sampai_tanggal_1:sampai_tanggal_1,
        },
        dataType: "html",
        url: '<?php echo base_url(); ?>laporan_perkembangan/html_harga_kebutuhan_pokok/',
        success: function(html) {
          $('#div_filter_pasar').hide();
          $('#grid_utama_by_filter').append(html);
          $('#overlay_data_default').fadeOut('slow');
        }
      });
  }
</script>
<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#dari_tanggal_1, #sampai_tanggal_1').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
  
</script>