<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Invoice</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>boots/bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>boots/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>boots/dist/css/AdminLTE.min.css">

  <script src="<?php echo base_url(); ?>boots/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h4 class="page-header">
            <center>
            PERKEMBANGAN HARGA RATA-RATA KEBUTUHAN POKOK MASYARAKAT<br />
            3 (TIGA) PASAR TRADISIONAL DI KABUPATEN WONOSOBO<br />
            BULAN AGUSTUS 2017<br />
            </center>
        <!-- 
            <small class="pull-right">Date: <?php //echo date('d M Y'); ?></small> -->
          </h4>
        </div>
        <!-- /.col -->
      </div>
      
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive" id="grid_utama">
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-8">
          <p class="" style="margin-top: -10px; font-size:9px;">
            <i>
            Sumber Data: Pemantauan di Pasar Induk Wonosobo, Pasar Garung, dan Pasar Kertek di Kabupaten Wonosobo Dinas Perdagangan, Koperasi, UKM Kab. Wonosobo
            </i>
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <p class="lead"> </p>

          <div class="table-responsive">
            <table class="">
              <tbody>
                <tr>
                  <th><center>Wonosobo, 15 September 2017</center></th>
                </tr>
                <tr>
                  <td><center>KEPALA DINAS PERDAGANGAN, KOPERASI, UKM KABUPATEN WONOSOBO</center></td>
                </tr>
                <tr>
                  <td><center><br />ttd<br /></center></td>
                </tr>
                <tr>
                  <td><center><u>Drs. AGUS SURYATIN, MT</u><br />Pembina Utama Muda<br />NIP. 19601007 198612 1 001</center></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    
  <!-- /.content -->
</div>
<!-- ./wrapper -->

  <script>
    function load_grid() {
      $('#overlay_data_default').show();
      $('#grid_utama').html('');
      var tahun = $('#load_tahun_apbdesa').val();
      var id_kode_pendapatan = $('#load_pendapatan_desa_apbdesa').val();
      $.ajax({
        type: "POST",
        async: true,
        data: {
          tahun:tahun,
          id_kode_pendapatan:id_kode_pendapatan,
        },
        dataType: "html",
        url: '<?php echo base_url(); ?>harga_kebutuhan_pokok/html_harga_kebutuhan_pokok/',
        success: function(html) {
          $('#grid_utama').append(html);
          $('#overlay_data_default').fadeOut('slow');
        }
      });
    }
  </script>

  <script>
    $(document).ready(function() {
        load_grid();
    });
  </script>

</body>
</html>
