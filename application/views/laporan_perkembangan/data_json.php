      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
              <div class="box">
              <div class="box-body">
                <table class="table table-bordered">
									<tr>
										<th>NO</th>
										<th>KOMODITI </th>
										<th>SATUAN </th>
										<th>PASAR </th>
										<th>HARGA </th>
										<th>PROSES</th> 
									</tr>
									<tbody id="tbl_utama_harga_kebutuhan_pokok">
									</tbody>
								</table>
              </div>
              </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

<script>
  function load_default(halaman) {
    $('#overlay_data_default').show();
    $('#tbl_utama_harga_kebutuhan_pokok').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>harga_kebutuhan_pokok/json_all_harga_kebutuhan_pokok/?halaman='+halaman+'/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * <?php echo $per_page; ?>);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_harga_kebutuhan_pokok="' + json[i].id_harga_kebutuhan_pokok + '" id="' + json[i].id_harga_kebutuhan_pokok + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
					tr += '<td valign="top">' + json[i].nama_komoditi + '</td>';
					tr += '<td valign="top">' + json[i].nama_satuan + '</td>';
					tr += '<td valign="top">' + json[i].nama_pasar + '</td>';
					tr += '<td valign="top"><a class="" href="#">' + json[i].harga + ' </a></td>';
					tr += '<td valign="top">';
					tr += '<a href="#tab_1" data-toggle="tab" class="update_id label label-info" ><i class="fa fa-pencil-square-o"></i></a> ';
					tr += '<a class="label label-danger" href="#tab_1" data-toggle="tab" id="del_ajax" ><i class="fa fa-cut"></i></a>';
					tr += '</td>';
          tr += '</tr>';
        }
        $('#tbl_utama_harga_kebutuhan_pokok').append(tr);
				$('#overlay_data_default').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	var halaman = 1;
	load_default(halaman);
});
</script>
