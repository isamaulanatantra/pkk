
    <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-4">
          <div class="no-print">
            <div class="form-group">
              <select class="form-control" id="id_pasar_by_filter" name="id_pasar_by_filter">
              </select>
              <input type="hidden" name="hidden_nama_pasar" id="hidden_nama_pasar" value="" class="form-control" />
            </div>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12">
          <div class="overlay" id="overlay_data_default" style="display:none;"><i class="fa fa-refresh fa-spin"></i></div>
          <div class="pad" id="grid_utama_by_filter">
          </div>
        </div>
      </div>
        <!-- /.col -->
      <!-- /.row -->

    </section>
    
<script>
  function load_id_pasar_by_filter() {
    $('#overlay_data_default').show();
    $('#id_pasar_by_filter').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp,
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>pasar/load_id_pasar_by_filter/',
      success: function(json) {
        var tr = '';
        tr += '<option value="0"> Pilih Pasar</option>';
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
          tr += '<option value="' + json[i].id_pasar + '"> ' + json[i].nama_pasar + ' </option>';
        }
        $('#id_pasar_by_filter').append(tr);
				$('#overlay_data_default').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
    load_id_pasar_by_filter();
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#id_berdasarkan').on('change', function(e) {
    $('#id_pasar_by_filter').html('');
    load_id_pasar_by_filter();
	});
});
</script>

<script>
  $(document).ready(function() {
    $('#tampilkan_by_filter').on('click', function(e) {
      e.preventDefault();
      load_grid_utama_by_filter();
    });
  });
</script>

<script>
  function load_grid_utama_by_filter() {
      $('#overlay_data_default').show();
      $('#grid_utama_by_filter').html('');
      var temp = $("#temp").val();
      var id_pasar_by_filter = $("#id_pasar_by_filter").val();
      var dari_tanggal_1 = $("#dari_tanggal_1").val();
      var sampai_tanggal_1 = $("#sampai_tanggal_1").val();
      if (id_pasar_by_filter == '') {
          $('#id_pasar_by_filter').css('background-color', '#DFB5B4');
        } else {
          $('#id_pasar_by_filter').removeAttr('style');
        }
      if (dari_tanggal_1 == '') {
          $('#dari_tanggal_1').css('background-color', '#DFB5B4');
        } else {
          $('#dari_tanggal_1').removeAttr('style');
        }
      if (sampai_tanggal_1 == '') {
          $('#sampai_tanggal_1').css('background-color', '#DFB5B4');
        } else {
          $('#sampai_tanggal_1').removeAttr('style');
        }
      $.ajax({
        type: "POST",
        async: true,
        data: {
          temp:temp,
          id_pasar_by_filter:id_pasar_by_filter,
          dari_tanggal_1:dari_tanggal_1,
          sampai_tanggal_1:sampai_tanggal_1,
        },
        dataType: "html",
        url: '<?php echo base_url(); ?>laporan_perkembangan/html_harga_kebutuhan_pokok/',
        success: function(html) {
          $('#div_filter_pasar').hide();
          $('#grid_utama_by_filter').append(html);
          $('#overlay_data_default').fadeOut('slow');
        }
      });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#id_pasar_by_filter').on('change', function(e) {
    load_grid_utama_by_filter();
	});
});
</script>