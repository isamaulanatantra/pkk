<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Print Perijinan Penelitian</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Bootstrap 4 -->

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css">

  <link href="https://fonts.googleapis.com/css?family=Arial" rel="stylesheet">
  <!-- 
	-->
<style>
  	body {
		font-family: 'Roboto Mono', serif;
		font-size: 18px;
  	}
	hr {
		border-width: 4px;
		background-color: #000;
	} 
</style>
</head>
<body onload="window.print();">
<div class="wrapper">
  <!-- Main content -->
  <section class="invoice">
  	<div class="row">
			<?php $this -> load -> view($main_view);  ?>
		</div>
  </section>
  <!-- /.content -->
</div>
<!-- ./wrapper -->
</body>
</html>
