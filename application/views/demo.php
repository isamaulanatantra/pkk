<!DOCTYPE html>
<html lang="en">
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
    <!-- Meta -->
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
    <meta name="author" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>,">
    <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>">
    <meta name="og:description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
    <meta name="og:url" content="<?php echo base_url(); ?>"/>
    <meta name="og:title" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>"/>
    <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
    <meta name="og:keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
    <!-- Favicon -->
    <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />
    <meta name="twitter:image" content="<?php echo base_url(); ?>media/upload/sikunir wonosobo.jpg"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/component.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/default.css" />
    <!-- video css-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    <style type="text/css">
      body{
      background: 
      linear-gradient(
      rgba(0, 0, 0, 0.2), 
      rgba(0, 0, 0, 0.2)
      ),
      url(https://zb.wonosobokab.go.id/assets/images/sikunir_wonosobo.jpg); background-repeat: no-repeat; background-position: 50% 0%;
      background-attachment:fixed;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
      }
    </style>

    <script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/dist/css/adminlte.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>Template/AdminLTE-2.4.3/bower_components/font-awesome/css/font-awesome.min.css" />
  </head>
  <body>
    <div class="content">
    <div class="main">
      <div class="container">
        <header class="clearfix" style="margin-top:0px">
          <center>
            <a href="<?php echo base_url(); ?>website" target="_blank"><img src="<?php echo base_url(); ?>media/upload/logowsb.png" border="0"></a>
            <div style="margin-top:20px;">
              <form class="form-search" action="https://wonosobokab.go.id/pencarian.HTML" method='GET'>
                <div class="input" style="-moz-box-sizing: none;height:100%;width:100%">
									<input class="form-control form-control-navbar cari" type="search" placeholder="Cari tentang Wonosobo..." aria-label="Search" style="height:30px; width:300px;">
                  <button type="submit" class="btn"><i class="fa fa-search"></i></button>
                </div>
              </form>
            </div>
          </center>
        </header>
        <div class="main">
          <style type="text/css">
            .og-grid li {
            height: auto;
            }
            .og-grida li {
            height: auto;
            }
            @media (min-width: 1300px) {
            ul.og-grid {
            padding-right: 200px;
            padding-left: 200px;
            }
            input.cari {
            width: 300px;
            }
            .container > header {
            padding-bottom: 0px;
            }
            }
            body {
            padding-left: 0px;
            padding-right: 0px;
            }
            @media (max-width: 457px) {
            body {
            background:min-width:457px;
            }
            .container > header {
            padding-bottom: 0px;
            padding-top: 0px;
            }
            img.shortcut-icon {
            width: 140px;
            }
            }
          </style>
          <div class="container-fluid">
          <ul id="og-grid" class="row">
            <li class="">
              <a href="#" class="alink" data-title="" data-description='
									<div class="row">
										<div class="col-12 col-sm-6 col-md-4">
											<div class="info-box">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-bank"></i></span>
												<div class="info-box-content">
													<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/rencana/prioritas-pembangunan-daerah" target="_blank">Prioritas Pembangunan Daerah</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-registered"></i></span>
												<div class="info-box-content">
													<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/rencana/rpjmd" target="_blank">Rencana Pembangunan Jangka Menengah Daerah (RPJMD)</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-bar-chart-o"></i></span>
												<div class="info-box-content">
													<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/laporan/lkpj" target="_blank">Laporan Keterangan Pertanggungjawaban (LKPJ)</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-briefcase"></i></span>

												<div class="info-box-content">
													<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/laporan/lakip" target="_blank">Laporan Kinerja Instansi Pemerintah (LAKIP)</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-building-o"></i></span>

												<div class="info-box-content">
													<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/laporan/pk-2015" target="_blank">Perjanjian Kinerja (PK)</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-calendar"></i></span>

												<div class="info-box-content">
												<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/laporan/ilppd" target="_blank">Informasi atas Laporan Penyelenggaraan Pemerintah Daerah (ILPPD)</a>
												</div>
											</div>
										</div>
										<div class="col-12 col-sm-6 col-md-4">
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-child"></i></span>

												<div class="info-box-content">
													<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/laporan/lhkpn" target="_blank">Laporan Harta Kekayaan Penyelenggara Negara (LHKPN)</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-clone"></i></span>

												<div class="info-box-content">
												<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/laporan/lhp" target="_blank">Laporan Hasil Pemeriksaan (LHP)</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-dashboard"></i></span>

												<div class="info-box-content">
												<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/laporan/lra" target="_blank">Laporan Realisasi Anggaran (LRA)</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-cubes"></i></span>

												<div class="info-box-content">
												<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/anggaran-kegiatan/rka" target="_blank">Rencana Kerja Perubahan Anggaran (RKA)</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-envelope-o"></i></span>

												<div class="info-box-content">
												<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/anggaran-kegiatan/dpa" target="_blank">Dokumen Pelaksanaan Perubahan Anggaran (DPA)</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-edit"></i></span>

												<div class="info-box-content">
												<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/anggaran-kegiatan/calk" target="_blank">Catatan Atas Laporan Keuangan (CALK)</a>
												</div>
											</div>
										</div>
										<div class="col-12 col-sm-6 col-md-4">
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-flag"></i></span>

												<div class="info-box-content">
												<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/rencana/rup" target="_blank">Rencana Umum Pengadaan (RUP)</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-industry"></i></span>

												<div class="info-box-content">
												<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/laporan/neraca" target="_blank">Neraca Pemerintah Kabupaten Wonosobo</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-home"></i></span>

												<div class="info-box-content">
												<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/anggaran-kegiatan/apbd" target="_blank">Anggaran Pendapatan dan Belanja Daerah (APBD)</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-map"></i></span>

												<div class="info-box-content">
												<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/laporan/lelang-lpse" target="_blank">Lelang LPSE</a>
												</div>
											</div>
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-newspaper-o"></i></span>

												<div class="info-box-content">
												<a class="text-success" href="http://wonosobokab.go.id/website/index.php/2014-02-01-04-40-55/laporan/daftar-aset-inventaris" target="_blank">Daftar Aset dan Inventaris</a>
												</div>
											</div>
										</div>
										<div class="clearfix hidden-md-up"></div>
									</div>
                '>
              <img src="<?php echo base_url(); ?>assets/images/transparansi anggaran.png" alt="Transparansi Keuangan" class="img shortcut-icon img-responsive"/>
              </a>
            </li>
            <li class="">
              <a href="#" class="alink" data-title="" data-description='
								
                <div class="row">
												<div class="col-12 col-sm-6 col-md-4">
													<div class="info-box mb-3 bg-primary disabled color-palette">
														<ul>
															<li>
																<a class="text-danger" href="https://inspektorat.wonosobokab.go.id/" target="_blank">Inspektorat</a></li>
															<li>
																<a class="text-danger" href="https://dikpora.wonosobokab.go.id/" target="_blank">Dinas Pendidikan, Pemuda dan Olahraga</a></li>
															<li>
																<a class="text-danger" href="https://dinkes.wonosobokab.go.id/" target="_blank">Dinas Kesehatan</a></li>
															<li>
																<a class="text-danger" href="https://dpupr.wonosobokab.go.id/" target="_blank">Dinas Pekerjaan Umum dan Penataan Ruang</a></li>
															<li>
																<a class="text-danger" href="https://disperkimhub.wonosobokab.go.id/" target="_blank">Dinas Perumahan, Kawasan Pemukiman dan Perhubungan</a></li>
															<li>
																<a class="text-danger" href="https://dinsospmd.wonosobokab.go.id/" target="_blank">Dinas Sosial, Pemberdayaan Masyarakat dan Desa</a></li>
															<li>
																<a class="text-danger" href="https://dppkbpppa.wonosobokab.go.id/" target="_blank">Dinas Pengendalian Penduduk, Keluarga Berencana, Pemberdayaan Perempuan dan Perlindungan Anak</a></li>
															<li>
																<a class="text-danger" href="https://dispaperkan.wonosobokab.go.id/" target="_blank">Dinas Pangan, Pertanian dan Perikanan</a></li>
														
														<li>
															<a class="text-danger" href="https://dlh.wonosobokab.go.id/" target="_blank">Dinas Lingkungan Hidup</a></li>
														<li>
															<a class="text-danger" href="https://disdukcapil.wonosobokab.go.id/" target="_blank">Dinas Kependudukan dan Pencatatan Sipil</a></li>
														<li>
															<a class="text-danger" href="https://diskominfo.wonosobokab.go.id/" target="_blank">Dinas Komunikasi dan Informatika</a></li>
														<li>
															<a class="text-danger" href="https://disdagkopukm.wonosobokab.go.id/" target="_blank">Dinas Perdagangan, Koperasi, Usaha Kecil dan Menengah</a></li>
														<li>
															<a class="text-danger" href="https://dpmptsp.wonosobokab.go.id/" target="_blank">Dinas Penanaman Modal dan Pelayanan Terpadu Satu Pintu</a></li>
														<li>
															<a class="text-danger" href="https://disnakerintrans.wonosobokab.go.id/" target="_blank">Dinas Tenaga Kerja, Perindustrian dan Transmigrasi</a></li>
														<li>
															<a class="text-danger" href="https://arpusda.wonosobokab.go.id/" target="_blank">Dinas Kearsipan dan Perpustakaan Daerah</a></li>
														<li>
															<a class="text-danger" href="https://disparbud.wonosobokab.go.id/" target="_blank">Dinas Pariwisata dan Kebudayaan</a></li>
													</ul>
													</div>
												</div>
												<div class="col-12 col-sm-6 col-md-4">
													<div class="info-box mb-3 bg-success">
													<ul>
														<li>
															<a class="text-danger" href="https://satpolpp.wonosobokab.go.id/" target="_blank">Satuan Polisi Pamong Praja</a></li>
														<li>
															<a class="text-danger" href="https://bappeda.wonosobokab.go.id/" target="_blank">Badan Perencanaan  &amp; Pembangunan Daerah</a></li>
														<li>
															<a class="text-danger" href="https://bppkad.wonosobokab.go.id/" target="_blank">Badan Pengelolaan Pendapatan, Keuangan dan Aset Daerah</a></li>
														<li>
															<a class="text-danger" href="https://bkd.wonosobokab.go.id/" target="_blank">Badan Kepegawaian Daerah</a></li>
														<li>
															<a class="text-danger" href="https://bpbd.wonosobokab.go.id/" target="_blank">Badan Penanggulangan Bencana Daerah</a></li>
														<li>
															<a class="text-danger" href="https://kesbangpol.wonosobokab.go.id/" target="_blank">Kantor Kesatuan Bangsa dan Politik</a></li>
														<li>
															<a class="text-danger" href="https://kpud.wonosobokab.go.id/" target="_blank">Komisi Pemilihan Umum Daerah</a></li>
														<li>
															<a class="text-danger" href="https://rsud.wonosobokab.go.id/" target="_blank">Rumah Sakit Umum Daerah Setjonegoro</a></li>
														<li>
															<a class="text-danger" href="https://dwp.wonosobokab.go.id/" target="_blank">Dharma Wanita Persatuan</a></li>
														<li>
															<a class="text-danger" href="https://setwan.wonosobokab.go.id/" target="_blank">Sekretariat DPRD</a></li>
													
														<li>
															<a class="text-danger" href="https://bagianpemerintahan.wonosobokab.go.id/" target="_blank">Bagian Pemerintahan SETDA</a></li>
														<li>
															<a class="text-danger" href="https://bagianhukum.wonosobokab.go.id/" target="_blank">Bagian Hukum SETDA</a></li>
														<li>
															<a class="text-danger" href="https://bagianperekonomian.wonosobokab.go.id/" target="_blank">Bagian Perekonomian SETDA</a></li>
														<li>
															<a class="text-danger" href="https://bagiankesra.wonosobokab.go.id/" target="_blank">Bagian Kesejahteraan Rakyat SETDA</a></li>
														<li>
															<a class="text-danger" href="https://bagiandalbang.wonosobokab.go.id/" target="_blank">Bagian Pengendalian Pembangunan SETDA</a></li>
														<li>
															<a class="text-danger" href="https://bagianorganisasi.wonosobokab.go.id/" target="_blank">Bagian Organisasi SETDA</a></li>
														<li>
															<a class="text-danger" href="https://bagianhumasumum.wonosobokab.go.id/" target="_blank">Bagian Humas dan Umum SETDA</a></li>
														<li>
															<a class="text-danger" href="https://bagianpbj.wonosobokab.go.id/" target="_blank">Bagian Pengadaan Barang dan Jasa SETDA</a></li>
														<li>
															<a class="text-danger" href="https://kecamatanwonosobo.wonosobokab.go.id/" target="_blank">Kecamatan Wonosobo</a></li>
														<li>
															<a class="text-danger" href="https://kecamatankertek.wonosobokab.go.id/" target="_blank">Kecamatan Kertek</a></li>
													</ul>
													</div>
												</div>
												<div class="col-12 col-sm-6 col-md-4">
													<div class="info-box mb-3 bg-warning">
													<ul>
														<li>
															<a class="text-danger" href="https://kecamatankalikajar.wonosobokab.go.id/" target="_blank">Kecamatan Kalikajar</a></li>
														<li>
															<a class="text-danger" href="https://kecamatansapuran.wonosobokab.go.id/" target="_blank">Kecamatan Sapuran</a></li>
														<li>
															<a class="text-danger" href="https://kecamatankepil.wonosobokab.go.id/" target="_blank">Kecamatan Kepil</a></li>
														<li>
															<a class="text-danger" href="https://kecamatanwadaslintang.wonosobokab.go.id/" target="_blank">Kecamatan Wadaslintang</a></li>
														<li>
															<a class="text-danger" href="https://kecamatankalibawang.wonosobokab.go.id/" target="_blank">Kecamatan Kalibawang</a></li>
														<li>
															<a class="text-danger" href="https://kecamatankaliwiro.wonosobokab.go.id/" target="_blank">Kecamatan Kaliwiro</a></li>
														<li>
															<a class="text-danger" href="https://kecamatansukoharjo.wonosobokab.go.id/" target="_blank">Kecamatan Sukoharjo</a></li>
														<li>
															<a class="text-danger" href="https://kecamatanleksono.wonosobokab.go.id/" target="_blank">Kecamatan Leksono</a></li>
														<li>
															<a class="text-danger" href="https://kecamatanselomerto.wonosobokab.go.id/" target="_blank">Kecamatan Selomerto</a></li>
														<li>
															<a class="text-danger" href="https://kecamatanwatumalang.wonosobokab.go.id/" target="_blank">Kecamatan Watumalang</a></li>
														<li>
															<a class="text-danger" href="https://kecamatangarung.wonosobokab.go.id/" target="_blank">Kecamatan Garung</a></li>
														<li>
															<a class="text-danger" href="https://kecamatankejajar.wonosobokab.go.id/" target="_blank">Kecamatan Kejajar</a></li>
														<li>
															<a class="text-danger" href="https://kecamatanmojotengah.wonosobokab.go.id/" target="_blank">Kecamatan Mojotengah</a></li>
													</ul>
													</div>
												</div>
												<div class="col-12 col-sm-6 col-md-4">
													<div class="info-box mb-3 bg-danger">
													<ul>
														<li>
															<a class="text-danger" href="https://kelurahanandongsili.wonosobokab.go.id" target="_blank">Kelurahan Andongsili</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanbumireso.wonosobokab.go.id" target="_blank">Kelurahan Bumiroso</a></li>
														<li>
															<a class="text-danger" href="https://kelurahangarung.wonosobokab.go.id" target="_blank">Kelurahan Garung</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanjaraksari.wonosobokab.go.id" target="_blank">Kelurahan Jaraksari</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanjlamprang.wonosobokab.go.id" target="_blank">Kelurahan Jlamprang</a></li>
														<li>
															<a class="text-danger" href="https://kelurahankalianget.wonosobokab.go.id" target="_blank">Kelurahan Kalianget</a></li>
														<li>
															<a class="text-danger" href="https://kelurahankalibeber.wonosobokab.go.id" target="_blank">Kelurahan Kalibeber</a></li>
														<li>
															<a class="text-danger" href="https://kelurahankalikajar.wonosobokab.go.id" target="_blank">Kelurahan Kalikajar</a></li>
														<li>
															<a class="text-danger" href="https://kelurahankaliwiro.wonosobokab.go.id" target="_blank">Kelurahan Kaliwiro</a></li>
														<li>
															<a class="text-danger" href="https://kelurahankejajar.wonosobokab.go.id" target="_blank">Kelurahan Kejajar</a></li>
														<li>
															<a class="text-danger" href="https://kelurahankejiwan.wonosobokab.go.id" target="_blank">Kelurahan Kejiwan</a></li>
														<li>
															<a class="text-danger" href="https://kelurahankepil.wonosobokab.go.id" target="_blank">Kelurahan Kepil</a></li>
														<li>
															<a class="text-danger" href="https://kelurahankertek.wonosobokab.go.id" target="_blank">Kelurahan Kertek</a></li>
														<li>
															<a class="text-danger" href="https://kelurahankramatan.wonosobokab.go.id" target="_blank">Kelurahan Kramatan</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanleksono.wonosobokab.go.id" target="_blank">Kelurahan Leksono</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanmlipak.wonosobokab.go.id" target="_blank">Kelurahan Mlipak</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanmudal.wonosobokab.go.id" target="_blank">Kelurahan Mudal</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanpagerkukuh.wonosobokab.go.id" target="_blank">Kelurahan Pagerkukuh</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanrojoimo.wonosobokab.go.id" target="_blank">Kelurahan Rojoimo</a></li>
														<li>
															<a class="text-danger" href="https://kelurahansambek.wonosobokab.go.id" target="_blank">Kelurahan Sambek</a></li>
														<li>
															<a class="text-danger" href="https://kelurahansapuran.wonosobokab.go.id" target="_blank">Kelurahan Sapuran</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanselomerto.wonosobokab.go.id" target="_blank">Kelurahan Selomerto</a></li>
														<li>
															<a class="text-danger" href="https://kelurahantawangsari.wonosobokab.go.id" target="_blank">Kelurahan Tawangsari</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanwadaslintang.wonosobokab.go.id" target="_blank">Kelurahan Wadaslintang</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanwonorejo.wonosobokab.go.id" target="_blank">Kelurahan Wonorejo</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanwonoroto.wonosobokab.go.id" target="_blank">Kelurahan Wonoroto</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanwonosobobarat.wonosobokab.go.id" target="_blank">Kelurahan Wonosobo Barat</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanwonosobotimur.wonosobokab.go.id" target="_blank">Kelurahan Wonosobo Timur</a></li>
														<li>
															<a class="text-danger" href="https://kelurahanwringinanom.wonosobokab.go.id" target="_blank">Kelurahan Wringinanom</a></li>
													</ul>
													</div>
												</div>
												<div class="col-12 col-sm-6 col-md-4">
													<div class="info-box mb-3 bg-info">
													<ul>
														<li>
															<a class="text-danger" href="https://kumejing-wadaslintang.wonosobokab.go.id" target="_blank">Desa Kumejing</a></li>
														<li>
															<a class="text-danger" href="https://gumelar-wadaslintang.wonosobokab.go.id" target="_blank">Desa Gumelar</a></li>
														<li>
															<a class="text-danger" href="https://lancar-wadaslintang.wonosobokab.go.id" target="_blank">Desa Lancar</a></li>
														<li>
															<a class="text-danger" href="https://gadingrejo-kepil.wonosobokab.go.id" target="_blank">Desa Gadingrejo</a></li>
														<li>
															<a class="text-danger" href="https://tanjunganom-kepil.wonosobokab.go.id" target="_blank">Desa Tanjunganom</a></li>
														<li>
															<a class="text-danger" href="https://beran-kepil.wonosobokab.go.id" target="_blank">Desa Beran</a></li>
														<li>
															<a class="text-danger" href="https://kalipuru-kepil.wonosobokab.go.id" target="_blank">Desa Kalipuru</a></li>
														<li>
															<a class="text-danger" href="https://rejosari-kepil.wonosobokab.go.id" target="_blank">Desa Rejosari</a></li>
														<li>
															<a class="text-danger" href="https://kagungan-kepil.wonosobokab.go.id" target="_blank">Desa Kagungan</a></li>
														<li>
															<a class="text-danger" href="https://ropoh-kepil.wonosobokab.go.id" target="_blank">Desa Ropoh</a></li>
														<li>
															<a class="text-danger" href="https://bener-kepil.wonosobokab.go.id" target="_blank">Desa Bener</a></li>
														<li>
															<a class="text-danger" href="https://ngalian-kepil.wonosobokab.go.id" target="_blank">Desa Ngalian</a></li>
														<li>
															<a class="text-danger" href="https://kapulogo-kepil.wonosobokab.go.id" target="_blank">Desa Kapulogo</a></li>
														<li>
															<a class="text-danger" href="https://warangan-kepil.wonosobokab.go.id" target="_blank">Desa Warangan</a></li>
														<li>
															<a class="text-danger" href="https://pulosaren-kepil.wonosobokab.go.id" target="_blank">Desa Pulosaren</a></li>
														<li>
															<a class="text-danger" href="https://jangkrikan-kepil.wonosobokab.go.id" target="_blank">Desa Jangkrikan</a></li>
														<li>
															<a class="text-danger" href="https://gadingsukuh-kepil.wonosobokab.go.id" target="_blank">Desa Gadingsukuh</a></li>
														<li>
															<a class="text-danger" href="https://tegalgot-kepil.wonosobokab.go.id" target="_blank">Desa Tegalgot</a></li>
														<li>
															<a class="text-danger" href="https://talunombo-sapuran.wonosobokab.go.id" target="_blank">Desa Talunombo</a></li>
														<li>
															<a class="text-danger" href="https://batursari-sapuran.wonosobokab.go.id" target="_blank">Desa Batursari</a></li>
														<li>
															<a class="text-danger" href="https://desabogoran.wonosobokab.go.id" target="_blank">Desa Bogoran</a></li>
														<li>
															<a class="text-danger" href="https://ngadikerso-sapuran.wonosobokab.go.id" target="_blank">Desa Ngadikerso</a></li>
														<li>
															<a class="text-danger" href="https://rimpak-sapuran.wonosobokab.go.id" target="_blank">Desa Rimpak</a></li>
														<li>
															<a class="text-danger" href="https://winongsari-kaliwiro.wonosobokab.go.id" target="_blank">Desa Winongsari</a></li>
														<li>
															<a class="text-danger" href="https://kemiriombo-kaliwiro.wonosobokab.go.id" target="_blank">Desa Kemiriombo</a></li>
														<li>
															<a class="text-danger" href="https://tracap-kaliwiro.wonosobokab.go.id" target="_blank">Desa Tracap</a></li>
														<li>
															<a class="text-danger" href="https://grugu-kaliwiro.wonosobokab.go.id" target="_blank">Desa Grugu</a></li>
														<li>
															<a class="text-danger" href="https://kauman-kaliwiro.wonosobokab.go.id" target="_blank">Desa Kauman</a></li>
														<li>
															<a class="text-danger" href="https://medono-kaliwiro.wonosobokab.go.id" target="_blank">Desa Medono</a></li>
													</ul>
													</div>
												</div>
												<div class="col-12 col-sm-6 col-md-4">
													<div class="info-box mb-3 bg-warning">
													<ul>
														<li>
															<a class="text-danger" href="https://tracap-kaliwiro.wonosobokab.go.id" target="_blank">Desa Tracap</a></li>
														<li>
															<a class="text-danger" href="https://jonggolsari-leksono.wonosobokab.go.id" target="_blank">Desa Jonggolsari</a></li>
														<li>
															<a class="text-danger" href="https://selokromo-leksono.wonosobokab.go.id" target="_blank">Desa Selokromo</a></li>
														<li>
															<a class="text-danger" href="https://besani-leksono.wonosobokab.go.id" target="_blank">Desa Besani</a></li>
														<li>
															<a class="text-danger" href="https://manggis-leksono.wonosobokab.go.id" target="_blank">Desa Manggis</a></li>
														<li>
															<a class="text-danger" href="https://durensawit-leksono.wonosobokab.go.id" target="_blank">Desa Durensawit</a></li>
														<li>
															<a class="text-danger" href="https://lipursari-leksono.wonosobokab.go.id" target="_blank">Desa Lipursari</a></li>
														<li>
															<a class="text-danger" href="https://kalimendong-leksono.wonosobokab.go.id" target="_blank">Desa Kalimendong</a></li>
														<li>
															<a class="text-danger" href="https://kaliputih-selomerto.wonosobokab.go.id" target="_blank">Desa Kaliputih</a></li>
														<li>
															<a class="text-danger" href="https://kadipaten-selomerto.wonosobokab.go.id" target="_blank">Desa Kadipaten</a></li>
														<li>
															<a class="text-danger" href="https://simbarejo-selomerto.wonosobokab.go.id" target="_blank">Desa Simbarejo</a></li>
														<li>
															<a class="text-danger" href="https://sinduagung-selomerto.wonosobokab.go.id" target="_blank">Desa Sinduagung</a></li>
														<li>
															<a class="text-danger" href="https://wilayu-selomerto.wonosobokab.go.id" target="_blank">Desa Wilayu</a></li>
														<li>
															<a class="text-danger" href="https://candi-selomerto.wonosobokab.go.id" target="_blank">Desa Candi</a></li>
														<li>
															<a class="text-danger" href="https://balekambang-selomerto.wonosobokab.go.id" target="_blank">Desa Balekambang</a></li>
														<li>
															<a class="text-danger" href="https://gunungtawang-selomerto.wonosobokab.go.id" target="_blank">Desa Gunungtawang</a></li>
														<li>
															<a class="text-danger" href="https://pakuncen-selomerto.wonosobokab.go.id" target="_blank">Desa Pakuncen</a></li>
														<li>
															<a class="text-danger" href="https://tumenggungan-selomerto.wonosobokab.go.id" target="_blank">Desa Tumenggungan</a></li>
														<li>
															<a class="text-danger" href="https://semayu-selomerto.wonosobokab.go.id" target="_blank">Desa Semayu</a></li>
														<li>
															<a class="text-danger" href="https://kalierang-selomerto.wonosobokab.go.id" target="_blank">Desa Kalierang</a></li>
														<li>
															<a class="text-danger" href="https://wulungsari-selomerto.wonosobokab.go.id" target="_blank">Desa Wulungsari</a></li>
														<li>
															<a class="text-danger" href="https://kecis-selomerto.wonosobokab.go.id" target="_blank">Desa Kecis</a></li>
														<li>
															<a class="text-danger" href="https://sidorejo-selomerto.wonosobokab.go.id" target="_blank">Desa Sidorejo</a></li>
														<li>
															<a class="text-danger" href="https://simbang-kalikajar.wonosobokab.go.id" target="_blank">Desa Simbang</a></li>
														<li>
															<a class="text-danger" href="https://butuhkidul-kalikajar.wonosobokab.go.id" target="_blank">Desa Butuhkidul</a></li>
														<li>
															<a class="text-danger" href="https://butuh-kalikajar.wonosobokab.go.id" target="_blank">Desa Butuh</a></li>
														<li>
															<a class="text-danger" href="https://kedalon-kalikajar.wonosobokab.go.id" target="_blank">Desa Kedalon</a></li>
														<li>
															<a class="text-danger" href="https://perboto-kalikajar.wonosobokab.go.id" target="_blank">Desa Perboto</a></li>
														<li>
															<a class="text-danger" href="https://wonosari-kalikajar.wonosobokab.go.id" target="_blank">Desa Wonosari</a></li>
													</ul>
													</div>
												</div>
												<div class="col-12 col-sm-6 col-md-4">
													<div class="info-box mb-3 bg-success">
													<ul>
														<li>
															<a class="text-danger" href="https://maduretno-kalikajar.wonosobokab.go.id" target="_blank">Desa Maduretno</a></li>
														<li>
															<a class="text-danger" href="https://lamuk-kalikajar.wonosobokab.go.id" target="_blank">Desa Lamuk</a></li>
														<li>
															<a class="text-danger" href="https://rejosari-kalikajar.wonosobokab.go.id" target="_blank">Desa Rejosari</a></li>
														<li>
															<a class="text-danger" href="https://kembaran-kalikajar.wonosobokab.go.id" target="_blank">Desa Kembaran</a></li>
														<li>
															<a class="text-danger" href="https://tegalombo-kalikajar.wonosobokab.go.id" target="_blank">Desa Tegalombo</a></li>
														<li>
															<a class="text-danger" href="https://pagerejo-kertek.wonosobokab.go.id" target="_blank">Desa Pagerejo</a></li>
														<li>
															<a class="text-danger" href="https://ngadikusuman-kertek.wonosobokab.go.id" target="_blank">Desa Ngadikusuman</a></li>
														<li>
															<a class="text-danger" href="https://bojasari-kertek.wonosobokab.go.id" target="_blank">Desa Bojasari</a></li>
														<li>
															<a class="text-danger" href="https://candimulyo-kertek.wonosobokab.go.id" target="_blank">Desa Candimulyo</a></li>
														<li>
															<a class="text-danger" href="https://bejiarum-kertek.wonosobokab.go.id" target="_blank">Desa Bejiarum</a></li>
														<li>
															<a class="text-danger" href="https://kapencar-kertek.wonosobokab.go.id" target="_blank">Desa Kapencar</a></li>
														<li>
															<a class="text-danger" href="https://sindupaten-kertek.wonosobokab.go.id" target="_blank">Desa Sindupaten</a></li>
														<li>
															<a class="text-danger" href="https://sumberdalem-kertek.wonosobokab.go.id" target="_blank">Desa Sumberdalem</a></li>
														<li>
															<a class="text-danger" href="https://tlogomulyo-kertek.wonosobokab.go.id" target="_blank">Desa Tlogomulyo</a></li>
														<li>
															<a class="text-danger" href="https://banjar-kertek.wonosobokab.go.id" target="_blank">Desa Banjar</a></li>
														<li>
															<a class="text-danger" href="https://candiyasan-kertek.wonosobokab.go.id" target="_blank">Desa Candiyasan</a></li>
														<li>
															<a class="text-danger" href="https://purbosono-kertek.wonosobokab.go.id" target="_blank">Desa Purbosono</a></li>
														<li>
															<a class="text-danger" href="https://sudungdewo-kertek.wonosobokab.go.id" target="_blank">Desa Sudungdewo</a></li>
														<li>
															<a class="text-danger" href="https://bomerto-wonosobo.wonosobokab.go.id" target="_blank">Desa Bomerto</a></li>
														<li>
															<a class="text-danger" href="https://wonolelo-wonosobo.wonosobokab.go.id" target="_blank">Desa Wonolelo</a></li>
														<li>
															<a class="text-danger" href="https://tlogojati-wonosobo.wonosobokab.go.id" target="_blank">Desa Tlogojati</a></li>
														<li>
															<a class="text-danger" href="https://sariyoso-wonosobo.wonosobokab.go.id" target="_blank">Desa Sariyoso</a></li>
														<li>
															<a class="text-danger" href="https://jogoyitnan-wonosobo.wonosobokab.go.id" target="_blank">Desa Jogoyitnan</a></li>
														<li>
															<a class="text-danger" href="https://wonosari-wonosobo.wonosobokab.go.id" target="_blank">Desa Wonosari</a></li>
														<li>
															<a class="text-danger" href="https://pancurwening-wonosobo.wonosobokab.go.id" target="_blank">Desa Pancurwening</a></li>
														<li>
															<a class="text-danger" href="https://wonokampir-watumalang.wonosobokab.go.id" target="_blank">Desa Wonokampir</a></li>
														<li>
															<a class="text-danger" href="https://wonosroyo-watumalang.wonosobokab.go.id" target="_blank">Desa Wonosroyo</a></li>
														<li>
															<a class="text-danger" href="https://gondang-watumalang.wonosobokab.go.id" target="_blank">Desa Gondang</a></li>
														<li>
															<a class="text-danger" href="https://watumalang-watumalang.wonosobokab.go.id" target="_blank">Desa Watumalang</a></li>
													</ul>
													</div>
												</div>
												<div class="col-12 col-sm-6 col-md-4">
													<div class="info-box mb-3 bg-primary">
													<ul>
														<li>
															<a class="text-danger" href="https://kuripan-watumalang.wonosobokab.go.id" target="_blank">Desa Kuripan</a></li>
														<li>
															<a class="text-danger" href="https://krinjing-watumalang.wonosobokab.go.id" target="_blank">Desa Krinjing</a></li>
														<li>
															<a class="text-danger" href="https://kalidesel-watumalang.wonosobokab.go.id" target="_blank">Desa Kalidesel</a></li>
														<li>
															<a class="text-danger" href="https://limbangan-watumalang.wonosobokab.go.id" target="_blank">Desa Limbangan</a></li>
														<li>
															<a class="text-danger" href="https://wonokromo-mojotengah.wonosobokab.go.id" target="_blank">Desa Wonokromo</a></li>
														<li>
															<a class="text-danger" href="https://gemblengan-garung.wonosobokab.go.id" target="_blank">Desa Gemblengan</a></li>
														<li>
															<a class="text-danger" href="https://jengkol-garung.wonosobokab.go.id" target="_blank">Desa Jengkol</a></li>
														<li>
															<a class="text-danger" href="https://kayugiyang-garung.wonosobokab.go.id" target="_blank">Desa Kayugiyang</a></li>
														<li>
															<a class="text-danger" href="https://sendangsari-garung.wonosobokab.go.id" target="_blank">Desa Sendangsari</a></li>
														<li>
															<a class="text-danger" href="https://tlogo-garung.wonosobokab.go.id" target="_blank">Desa Tlogo</a></li>
														<li>
															<a class="text-danger" href="https://kuripan-garung.wonosobokab.go.id" target="_blank">Desa Kuripan</a></li>
														<li>
															<a class="text-danger" href="https://laranganlor-garung.wonosobokab.go.id" target="_blank">Desa Laranganlor</a></li>
														<li>
															<a class="text-danger" href="https://tegalsari-garung.wonosobokab.go.id" target="_blank">Desa Tegalsari</a></li>
														<li>
															<a class="text-danger" href="https://mlandi-garung.wonosobokab.go.id" target="_blank">Desa Mlandi</a></li>
														<li>
															<a class="text-danger" href="https://tambi-kejajar.wonosobokab.go.id" target="_blank">Desa Tambi</a></li>
														<li>
															<a class="text-danger" href="https://serang-kejajar.wonosobokab.go.id" target="_blank">Desa Serang</a></li>
														<li>
															<a class="text-danger" href="https://sembungan-kejajar.wonosobokab.go.id" target="_blank">Desa Sembungan</a></li>
														<li>
															<a class="text-danger" href="https://campursari-kejajar.wonosobokab.go.id" target="_blank">Desa Campursari</a></li>
														<li>
															<a class="text-danger" href="https://surengede-kejajar.wonosobokab.go.id" target="_blank">Desa Surengede</a></li>
														<li>
															<a class="text-danger" href="https://sikunang-kejajar.wonosobokab.go.id" target="_blank">Desa Sikunang</a></li>
														<li>
															<a class="text-danger" href="https://jojogan-kejajar.wonosobokab.go.id" target="_blank">Desa Jojogan</a></li>
														<li>
															<a class="text-danger" href="https://parikesit-kejajar.wonosobokab.go.id" target="_blank">Desa Parikesit</a></li>
														<li>
															<a class="text-danger" href="https://dieng-kejajar.wonosobokab.go.id" target="_blank">Desa Dieng</a></li>
														<li>
															<a class="text-danger" href="https://patakbanteng-kejajar.wonosobokab.go.id" target="_blank">Desa Patakbanteng</a></li>
														<li>
															<a class="text-danger" href="https://sigedang-kejajar.wonosobokab.go.id" target="_blank">Desa Sigedang</a></li>
														<li>
															<a class="text-danger" href="https://buntu-kejajar.wonosobokab.go.id" target="_blank">Desa Buntu</a></li>
														<li>
															<a class="text-danger" href="https://kreo-kejajar.wonosobokab.go.id" target="_blank">Desa Kreo</a></li>
														<li>
															<a class="text-danger" href="https://tieng-kejajar.wonosobokab.go.id" target="_blank">Desa Tieng</a></li>
														<li>
															<a class="text-danger" href="https://igirmranak-kejajar.wonosobokab.go.id" target="_blank">Desa Igirmranak</a></li>
													</ul>
													</div>
												</div>
												<div class="col-12 col-sm-6 col-md-4">
													<div class="info-box mb-3 bg-danger">
													<ul>
														<li>
															<a class="text-danger" href="https://plodongan-sukoharjo.wonosobokab.go.id" target="_blank">Desa Plodongan</a></li>
														<li>
															<a class="text-danger" href="https://tlogo-sukoharjo.wonosobokab.go.id" target="_blank">Desa Tlogo</a></li>
														<li>
															<a class="text-danger" href="https://pucungwetan-sukoharjo.wonosobokab.go.id" target="_blank">Desa Pucungwetan</a></li>
														<li>
															<a class="text-danger" href="https://jebengplampitan-sukoharjo.wonosobokab.go.id" target="_blank">Desa Jebengplampitan</a></li>
														<li>
															<a class="text-danger" href="https://gumiwang-sukoharjo.wonosobokab.go.id" target="_blank">Desa Gumiwang</a></li>
														<li>
															<a class="text-danger" href="https://gunungtugel-sukoharjo.wonosobokab.go.id" target="_blank">Desa Gunungtugel</a></li>
														<li>
															<a class="text-danger" href="https://depok-kalibawang.wonosobokab.go.id" target="_blank">Desa Depok</a></li>
														<li>
															<a class="text-danger" href="https://kalialang-kalibawang.wonosobokab.go.id" target="_blank">Desa Kalialang</a></li>
														<li>
															<a class="text-danger" href="https://mergolangu-kalibawang.wonosobokab.go.id" target="_blank">Desa Mergolangu</a></li>
														<li>
													</ul>
													</div>
												</div>
												
                </div>
                '>
              <img src="<?php echo base_url(); ?>assets/images/sub domain wsb.png" alt="Instansi" class="img shortcut-icon img-responsive"/>
              </a>
            </li>
            <li class="">
              <a href="#" class="alink" data-title="" data-description='
                
							<div class="row">
							
								<div class="col-md-4">
									<div class="small-box bg-primary">
										<div class="inner">
											<h3><a href="http://dashboard.wonosobokab.go.id/" target="_blank">Dashboard Eksekutif Bupati</a></h3>
										</div>
										<div class="icon">
											<i class="fa fa-dashboard"></i>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="small-box bg-success">
										<div class="inner">
											<h3><a href="http://callcenter-dinkes.wonosobokab.go.id/" target="_blank">Call Center Dinas Kesehatan</a></h3>
										</div>
										<div class="icon">
											<i class="fa fa-phone"></i>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="small-box bg-warning">
										<div class="inner">
											<h3><a href="http://bphtb.wonosobokab.go.id/" target="_blank">Bea Perolehan Hak Atas Tanah dan Bangunan (BPHTB)</a></h3>
										</div>
										<div class="icon">
											<i class="fa fa-balance-scale"></i>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="small-box bg-danger">
										<div class="inner">
											<h3><a href="http://apbdes.wonosobokab.go.id/" target="_blank">SI Monitoring Dana Transfer Desa (APBDES)</a></h3>
										</div>
										<div class="icon">
											<i class="fa fa-money"></i>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="small-box bg-primary">
										<div class="inner">
											<h3><a href="http://lpse.wonosobokab.go.id/" target="_blank">Layanan Pengadaan Secara Elektronik (LPSE)</a></h3>
										</div>
										<div class="icon">
											<i class="fa fa-legal"></i>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="small-box bg-success">
										<div class="inner">
											<h3><a href="http://retribusi.wonosobokab.go.id/" target="_blank">Retribusi Daerah</a></h3>
										</div>
										<div class="icon">
											<i class="fa fa-diamond"></i>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="small-box bg-warning">
										<div class="inner">
											<h3><a href="http://cellplan.wonosobokab.go.id/" target="_blank">SI Pengendalian Menara Telekomunikasi (CELLPLAN)</a></h3>
										</div>
										<div class="icon">
											<i class="fa fa-map-marker"></i>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="small-box bg-danger">
										<div class="inner">
											<h3><a href="https://sirup.lkpp.go.id/sirup" target="_blank">SI Rencana Umum Pengadaan (SIRUP)</a></h3>
										</div>
										<div class="icon">
											<i class="fa fa-area-chart"></i>
										</div>
									</div>
								</div>
								<div class="col-md-4">
									<div class="small-box bg-primary">
										<div class="inner">
											<h3><a href="http://pad.wonosobokab.go.id/" target="_blank">SI Pendapatan Asli Daerah (PAD)</a></h3>
										</div>
										<div class="icon">
											<i class="fa fa-pie-chart"></i>
										</div>
									</div>
								</div>
								
							</div>
								
                '>
								<img src="<?php echo base_url(); ?>assets/images/sistem informasi.png" alt="Sistem Informasi" class="img shortcut-icon img-responsive"/>
              </a>
            </li>
            <li class="">
              <a href="#" class="alink" data-title="" data-description='

							<div class="row">
								<div class="col-md-3">
									<div class="card-tools">
											<a class="btn btn-danger btn-sm daterange" href="http://sdnegeri5wonosobo.blogspot.co.id/" target="_blank"><i class="fa fa-institution"></i> SD Negeri 5 Wonosobo</a>
											<a class="btn btn-danger btn-sm daterange" href="http://www.sditwonosobo.sch.id/" target="_blank"><i class="fa fa-institution"></i> SD IT Insan Mulia Wonosobo</a>
											<a class="btn btn-danger btn-sm daterange" href="http://www.sdn2lancar.blogspot.co.id/" target="_blank"><i class="fa fa-institution"></i> SD Negeri 2 Lancar Kec Wadaslintang</a>
											<a class="btn btn-danger btn-sm daterange" href="https://sdn1besuki.wordpress.com/" target="_blank"><i class="fa fa-institution"></i> SD Negeri 1 Besuki Kec Wadaslintang</a>
											<a class="btn btn-danger btn-sm daterange" href="http://sdn2kaliguwo.blogspot.co.id/" target="_blank"><i class="fa fa-institution"></i> SD Negeri 2 Kaliguwo Kec Kaliwiro</a>
											<a class="btn btn-danger btn-sm daterange" href="http://www.sdkbendungan.blogspot.co.id/" target="_blank"><i class="fa fa-institution"></i> SD Kristen Bendungan Selomerto</a>
											<a class="btn btn-danger btn-sm daterange" href="http://www.sdncandirejo.blogspot.co.id/" target="_blank"><i class="fa fa-institution"></i> SD Negeri Candirejo Kec Mojotengah</a>
											<a class="btn btn-danger btn-sm daterange" href="http://sdn1buntukejajar.blogspot.co.id/" target="_blank"><i class="fa fa-institution"></i> SD Negeri 1 Buntu Kec Kejajar</a>
											<a class="btn btn-danger btn-sm daterange" href="http://www.sd3sukoharjo.blogspot.co.id/" target="_blank"><i class="fa fa-institution"></i> SD Negeri 3 Sukoharjo</a>
											<a class="btn btn-danger btn-sm daterange" href="http://20307181.siap-sekolah.com/" target="_blank"><i class="fa fa-institution"></i> SD Negeri 1 Watumalang</a>
											<a class="btn btn-danger btn-sm daterange" href="http://20306763.siap-sekolah.com/" target="_blank"><i class="fa fa-institution"></i> SD Negeri 2 Sindupaten Kec Kertek</a>
											<a class="btn btn-danger btn-sm daterange" href="http://www.sd1gemblengan.blogspot.co.id/" target="_blank"><i class="fa fa-institution"></i> SD Negeri 1 Gemblengan Kec Garung</a>
									</div>
								</div>
								<div class="col-md-3">
									<div class="card-tools">
											<a class="btn btn-info btn-sm daterange" href="http://smpn1wonosobo.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMP Negeri 1 Wonosobo</a>
											<a class="btn btn-info btn-sm daterange" href="http://smpn1kertek-wsb.sch-id.net/" target="_blank"><i class="fa fa-institution"></i> SMP Negeri 1 Kertek</a>
											<a class="btn btn-info btn-sm daterange" href="http://www.smpn1wadaslintang.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMP Negeri 1 Wadaslintang</a>
											<a class="btn btn-info btn-sm daterange" href="http://www.smppgri3kepil.blogspot.co.id/" target="_blank"><i class="fa fa-institution"></i> SMP PGRI 3 Kepil</a>
											<a class="btn btn-info btn-sm daterange" href="http://www.sataplimakalikajar.blogspot.co.id/" target="_blank"><i class="fa fa-institution"></i> SMP Negeri 5 Satu Atap Kec Kalikajar</a>
									</div>
								</div>
								<div class="col-md-3">
									<div class="card-tools">
											<a class="btn btn-success btn-sm daterange" href="http://www.sman1wonosobo.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMA Negeri 1 Wonosobo</a>
											<a class="btn btn-success btn-sm daterange" href="http://www.sma2wsb.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMA Negeri 2 Wonosobo</a>
											<a class="btn btn-success btn-sm daterange" href="http://www.smkn1-wnb.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMK Negeri 1 Wonosobo</a>
											<a class="btn btn-success btn-sm daterange" href="http://smkn2-wnb.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMK Negeri 2 Wonosobo</a>
											<a class="btn btn-success btn-sm daterange" href="http://www.smamuhwsb.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMA Muhammadiyah Wonosobo</a>
											<a class="btn btn-success btn-sm daterange" href="http://smkmuh1-wsb.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMK Muhammadiyah 1 Wonosobo</a>
											<a class="btn btn-success btn-sm daterange" href="http://sman1selomerto.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMA Negeri 1 Selomerto</a>
											<a class="btn btn-success btn-sm daterange" href="http://sman1kertek-wonosobo.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMA Negeri 1 Kertek</a>
											<a class="btn btn-success btn-sm daterange" href="http://sma1-mjt.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMA Negeri 1 Mojotengah</a>
											<a class="btn btn-success btn-sm daterange" href="http://sma1kaliwiro.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMA Negeri 1 Kaliwiro</a>
											<a class="btn btn-success btn-sm daterange" href="http://smatakhassus.sch.id/smataq/html/index.php" target="_blank"><i class="fa fa-institution"></i> SMA Takhasus Al Qur`an</a>
											<a class="btn btn-success btn-sm daterange" href="http://www.sma1sapuran.sch.id/" target="_blank"><i class="fa fa-institution"></i> SMA Negeri 1 Sapuran</a>
									</div>
								</div>
								<div class="col-md-3">
									<div class="card-tools">
										<a class="btn btn-primary btn-sm daterange" href="http://www.sma1sapuran.sch.id/" target="_blank">
											<i class="fa fa-institution"></i> SMK Negeri 1 Kalibawang
										</a>
										<a class="btn btn-primary btn-sm" data-widget="collapse" href="http://www.sman1wadaslintang.sch.id/" target="_blank">
											<i class="fa fa-institution"></i> SMA Negeri 1 Wadaslintang
										</a>
										<a class="btn btn-primary btn-sm" data-widget="collapse" href="http://smkn1wadaslintang.sch.id/" target="_blank">
											<i class="fa fa-institution"></i> SMK Negeri 1 Wadaslintang
										</a>
										<a class="btn btn-primary btn-sm" data-widget="collapse" href="http://www.smkinformatikawonosobo.sch.id/" target="_blank">
											<i class="fa fa-institution"></i> SMKS Informatika Wonosobo
										</a>
										<a class="btn btn-primary btn-sm" data-widget="collapse" href="http://smkgemanusantara.sch.id/" target="_blank">
											<i class="fa fa-institution"></i> SMKS Gema Nusantara
										</a>
										<a class="btn btn-primary btn-sm" data-widget="collapse" href="http://smkalmadani.p.ht/" target="_blank">
											<i class="fa fa-institution"></i> SMKS Al Madani
										</a>
										<a class="btn btn-primary btn-sm" data-widget="collapse" href="https://smknusantarawsb.wordpress.com/" target="_blank">
											<i class="fa fa-institution"></i> SMKS Nusantara
										</a>
										<a class="btn btn-primary btn-sm" data-widget="collapse" href="http://www.smkpaq.sch.id/" target="_blank">
											<i class="fa fa-institution"></i> SMKS Pelita Al-Qur`an
										</a>
										<a class="btn btn-primary btn-sm" data-widget="collapse" href="http://www.smkjalaluddin.sch.id/" target="_blank">
											<i class="fa fa-institution"></i> SMKS Jallaludin
										</a>
										<a class="btn btn-primary btn-sm" data-widget="collapse" href="http://smktarunanegara.blogspot.co.id/" target="_blank">
											<i class="fa fa-institution"></i> SMKS Taruna Negara
										</a>
										<a class="btn btn-primary btn-sm" data-widget="collapse" href="http://smkannuur-rpl.heck.in/" target="_blank">
											<i class="fa fa-institution"></i> SMKS An-Nuur Boarding School
										</a>
									</div>
								</div>
								
							</div>
                '>
              <img src="<?php echo base_url(); ?>assets/images/web sekolah.png" alt="Informasi Publik" class="img shortcut-icon img-responsive"/>
              </a>
            </li>
            <li class="">
		<script>(function(d, s, id) {  var js, fjs = d.getElementsByTagName(s)[0];  if (d.getElementById(id)) return;  js = d.createElement(s); js.id = id;  js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v3.0&appId=351370971628122&autoLogAppEvents=1';  fjs.parentNode.insertBefore(js, fjs);}(document, 'script', 'facebook-jssdk'));</script>
              <a href="#" class="alink" data-title="" data-description='
								
									<div class="row">
									<div class="col-md-4">

										<div class="card bg-primary-gradient">
											<div class="card-body">
												<p>Twitter : 
												<a href="https://twitter.com/BupatiWsbTweet" target="_blank">@BupatiWsbTweet</a></p>
												<a class="twitter-timeline" data-height="900" href="https://twitter.com/diskominfo_wsb?ref_src=twsrc%5Etfw">Tweets: Diskominfo Wonosobo</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="card bg-info-gradient">
											<div class="card-body">
												<p>Facebook : </p>
												<div id="fb-root"></div>
												<div class="fb-page" data-href="https://www.facebook.com/laporbupatiwonosobo/" data-tabs="timeline" data-height="900" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
													<blockquote cite="https://www.facebook.com/laporbupatiwonosobo/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/laporbupatiwonosobo/">Lapor Bupati Wonosobo</a></blockquote>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="card bg-primary-gradient">
											<div class="card-body">
												<p>Email : </p>
												<a href="http://wonosobokab.go.id/"> pemkab@wonosobokab.go.id</a>
												<a href="http://wonosobokab.go.id/">  diskominfo@wonosobokab.go.id</a>
											</div>
										</div>
										<div class="info-box bg-danger-gradient">
											<span class="info-box-icon"><i class="fa fa-info-circle"></i></span>

											<div class="info-box-content">
												<a class="info-box-text" href="https://wonosobokab.go.id/pengaduan_masyarakat">Permohonan Informasi Publik</a>
												<span class="info-box-number">41,410</span>

												<div class="progress">
													<div class="progress-bar" style="width: 70%"></div>
												</div>
												<span class="progress-description">
													70% Increase in 30 Days
												</span>
											</div>
										</div>
										<div class="info-box bg-success-gradient">
											<span class="info-box-icon"><i class="fa fa-comments-o"></i></span>

											<div class="info-box-content">
												<a class="info-box-text" href="https://wonosobokab.go.id/permohonan_informasi_publik">Pengaduan Masyarakat</a>
												<span class="info-box-number">41,410</span>

												<div class="progress">
													<div class="progress-bar" style="width: 70%"></div>
												</div>
												<span class="progress-description">
													70% Increase in 30 Days
												</span>
											</div>
										</div>
									</div>
									</div>
									
                '>
              <img src="<?php echo base_url(); ?>assets/images/pengduan masyarakat.png" alt="Layanan Publik" class="img shortcut-icon img-responsive"/>
              </a>
            </li>
            <li class="">
              <a href="#" class="alink" data-title="" data-description='
                
									<div class="row">
										<div class="col-md-4">
											<div class="card-tools">
												<a class="btn btn-primary btn-sm daterange" href="https://wonosobokab.bps.go.id/" target="_blank">
													<i class="fa fa-bar-chart"></i> BPS Wonosobo
												</a>
											</div>
										</div>
										<div class="col-md-4">
											<div class="card-tools">
												<a class="btn btn-primary btn-sm daterange" href="https://wonosobokab.go.id/" target="_blank">
													<i class="fa fa-bar-chart"></i> Perusahaan
												</a>
											</div>
										</div>
										<div class="col-md-4">
											<div class="card-tools">
												<a class="btn btn-primary btn-sm daterange" href="https://wonosobokab.go.id/" target="_blank">
													<i class="fa fa-bar-chart"></i> Organisasi Masyarakat
												</a>
											</div>
										</div>
										<div class="col-md-4">
											<div class="card-tools">
												<a class="btn btn-primary btn-sm daterange" href="https://wonosobokab.go.id/" target="_blank">
													<i class="fa fa-bar-chart"></i> Lembaga Pendidikan
												</a>
											</div>
										</div>
									</div>
                '>
              <img src="<?php echo base_url(); ?>assets/images/mitra pemkab.png" alt="CCTV" class="img shortcut-icon img-responsive"/>
              </a>
            </li>
            <li class="">
              <a href="#" class="alink" data-title="" data-description='
                
									<div class="row">
										<div class="col-md-4">
											<div class="small-box bg-primary">
												<div class="inner">
													<h3>PPID</h3>

													<p><a href="http://ppid.wonosobokab.go.id" target="_blank">PPID Sekretariat Daerah Wonosobo</a></p>
												</div>
												<div class="icon">
													<i class="fa fa-area-chart"></i>
												</div>
											</div>
											
										</div>
									</div>
									
                '>
              <img src="<?php echo base_url(); ?>assets/images/ppid.png" alt="Media Streaming" class="img shortcut-icon img-responsive"/>
              </a>
            </li>
            <li class="">
              <a href="#" class="alink" data-title="" data-description='
                
									<div class="row">
										<div class="col-md-4">
											<div class="info-box mb-3">
												<span class="info-box-icon bg-danger elevation-1"><i class="fa fa-microphone"></i></span>

												<div class="info-box-content">
													<span class="info-box-number">
													<a class="text-muted" href="http://pesonafmwonosobo.com/" target="_blank">Radio Pesona</a></span>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="info-box mb-3">
												<span class="info-box-icon bg-success elevation-1"><i class="fa fa-newspaper-o"></i></span>

												<div class="info-box-content">
													<span class="info-box-text"></span>
													<span class="info-box-number"><a class="text-muted" href="https://wonosoboekspres.wordpress.com/" target="_blank">Wonosobo Expres</a></span>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="info-box mb-3">
												<span class="info-box-icon bg-primary elevation-1"><i class="fa fa-object-group"></i></span>

												<div class="info-box-content">
													<span class="info-box-text"></span>
													<span class="info-box-number"><a class="text-muted" href="https://www.wonosobozone.com/" target="_blank">Wonosobo Zone</a></span>
												</div>
											</div>
										</div>
									</div>
									
                '>
              <img src="<?php echo base_url(); ?>assets/images/media informasi.png" alt="Smart City" class="img shortcut-icon img-responsive"/>
              </a>
            </li>
            <li class="">
              <a href="#" class="alink" data-title="" data-description='
                
									<div class="row">
										<div class="col-md-4">
											<div class="card card-widget widget-user">
												<div class="widget-user-header bg-info-active">
													<h3 class="widget-user-username"> </h3>
													<h5 class="widget-user-desc"> </h5>
												</div>
												<div class="widget-user-image">
													<img class="img-circle elevation-2" src="https://diskominfo.wonosobokab.go.id/media/upload/20180403041241_Wonosobo_Smart.png" alt="User Avatar">
												</div>
												<div class="card-footer">
													<div class="row">
														<div class="col-sm-12 border-right">
															<div class="description-block">
																<h5 class="description-header"> </h5>
																<span class="description-text"><a class="text-muted" href="https://play.google.com/store/apps/details?id=com.theksatrian.wonosobosmart" target="_blank">Wonosobo Smart</a></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="card card-widget widget-user">
												<div class="widget-user-header bg-info-active">
													<h3 class="widget-user-username"> </h3>
													<h5 class="widget-user-desc"> </h5>
												</div>
												<div class="widget-user-image">
													<img class="img-circle elevation-2" src="https://diskominfo.wonosobokab.go.id/media/upload/20180403040717_Sobo_News_-_Apps_on_Google_Play_-_Google_Chrome_2018-04-03_16.05" alt="User Avatar">
												</div>
												<div class="card-footer">
													<div class="row">
														<div class="col-sm-12 border-right">
															<div class="description-block">
																<h5 class="description-header"> </h5>
																<span class="description-text"><a class="text-muted" href="https://play.google.com/store/apps/details?id=com.theksatrian.sobonews" target="_blank">Wonosobo News</a></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="card card-widget widget-user">
												<div class="widget-user-header bg-info-active">
													<h3 class="widget-user-username"> </h3>
													<h5 class="widget-user-desc"> </h5>
												</div>
												<div class="widget-user-image">
													<img class="img-circle elevation-2" src="https://diskominfo.wonosobokab.go.id/media/upload/20180403041518_e_kinerja_wonosobo.png" alt="User Avatar">
												</div>
												<div class="card-footer">
													<div class="row">
														<div class="col-sm-12 border-right">
															<div class="description-block">
																<h5 class="description-header"> </h5>
																<span class="description-text"><a class="text-muted" href="https://play.google.com/store/apps/details?id=com.theksatrian.ekinerja" target="_blank">e Kinerja Wonosobo</a></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="card card-widget widget-user">
												<div class="widget-user-header bg-info-active">
													<h3 class="widget-user-username"> </h3>
													<h5 class="widget-user-desc"> </h5>
												</div>
												<div class="widget-user-image">
													<img class="img-circle elevation-2" src="https://diskominfo.wonosobokab.go.id/media/upload/20180404084846_One_Touch_Statistics_Wonosobo.png" alt="User Avatar">
												</div>
												<div class="card-footer">
													<div class="row">
														<div class="col-sm-12 border-right">
															<div class="description-block">
																<h5 class="description-header"> </h5>
																<span class="description-text"><a class="text-muted" href="https://play.google.com/store/apps/details?id=com.theksatrian.ekinerja" target="_blank">One Touch Statistics</a></span>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									
                '>
              <img src="<?php echo base_url(); ?>assets/images/smat siti.png" alt="Smart City" class="img shortcut-icon img-responsive"/>
              </a>
            </li>
          </ul>
          </div>
        </div>
      </div>
      <!-- /container -->
      <div style="position: auto;bottom: 10px;">
        <center>
          <a href="<?php echo base_url(); ?>website" target="_blank">
          <img src="<?php echo base_url(); ?>assets/images/tampilanpenuh.png" width="234" height="50" border="0" style="height:50px;margin-bottom:50px"></a>
        </center>
      </div>
      <div class="container">
          <div class="container-fluid">
          
            <div id="portfolio" class="bottom-border-shadow">
                <div class="container bottom-border">
                    <div class="row margin-vert-30 padding-top-40">
											<?php  if(!empty($galery_berita)){ echo $galery_berita; } ?>
                    </div>
										<a class="btn btn-danger" href="<?php echo base_url(); ?>galeri_wonosobo" target="_blank" style="margin:5px;">
											<h3> <i class="fa fa-file-photo-o"></i> Galeri Wonosobo</h3>
										</a>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div style="position: auto;bottom: 10px;">
			
			
			
      </div>
      <div id="footer" style="background:#000;color:#fff;font-size:12px;padding-bottom:10px;font-family:Verdana, Arial; padding-right: 15px;padding-left: 15px;">
        &nbsp;
        <center>Copyright © Pemerintah Kabupaten Wonosobo 2018 email: pemkab@wonosobokab.go.id</center>
      </div>
    </div>
    <video id="my-video" class="video" loop="true" muted autoplay>
      <?php
        $web=$this->uut->namadomain(base_url());
        $where = array(
          'status' => 1,
          );
        $this->db->where($where);
        $this->db->limit(1);
        $query = $this->db->get('vidio_intro');
        foreach ($query->result() as $row)
          {
            $tema_keterangan = $row->keterangan;
            $file_name = $row->file_name;
          }
        echo '
        <source src="'.base_url().'media/upload/'.$file_name.'" type="video/mp4">
        <source src="'.base_url().'media/upload/'.$file_name.'" type="video/ogg">
        <source src="'.base_url().'media/upload/'.$file_name.'" type="video/webm">  
        ';
        ?>
		<button id="unmuteButton"></button>

		<script>
			unmuteButton.addEventListener('click', function() {
				video.muted = false;
			});
		</script>

    </video>
		
    <!-- /video -->
    <script>
      (function() {
        /**
         * Video element
         * @type {HTMLElement}
         */
        var video = document.getElementById("my-video");
      
        /**
         * Check if video can play, and play it
         */
        video.addEventListener( "canplay", function() {
          video.play();
        });
      })();
    </script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/grid.js"></script>
    <script>
      $(function() {
        Grid.init();
      });
    </script> 
		
  </body>
</html>