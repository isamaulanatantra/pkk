
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Anggota_keluarga</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>anggota_keluarga">Home</a></li>
              <li class="breadcrumb-item active">Anggota_keluarga</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#tab_form_anggota_keluarga" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_data_anggota_keluarga" data-toggle="tab" id="klik_tab_data_anggota_keluarga">Data</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>anggota_keluarga/?id_data_keluarga=<?php echo $this->input->get('id_data_keluarga'); ?>"><i class="fa fa-refresh"></i></a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
									<div class="tab-pane active" id="tab_form_anggota_keluarga">
										<input name="tabel" id="tabel" value="anggota_keluarga" type="hidden" value="">
										<form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=anggota_keluarga" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="">
                            <div class="card-header">
                            <h4 class="card-title text-primary">FORMULIR INPUT ANGGOTA KELUARGA</h4>
                            </div>
                            <div class="card-body">
                              <input name="page" id="page" value="1" type="hidden" value="">
                              <div class="form-group" style="display:none;">
                                <label for="temp">temp</label>
                                <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="mode">mode</label>
                                <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="id_anggota_keluarga">id_anggota_keluarga</label>
                                <input class="form-control" id="id_anggota_keluarga" name="id" value="" placeholder="id_anggota_keluarga" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="id_data_keluarga">id_data_keluarga</label>
                                <input class="form-control" id="id_data_keluarga" name="id" value="<?php echo $this->input->get('id_data_keluarga'); ?>" placeholder="id_data_keluarga" type="text">
                              </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="nomor_registrasi">No. Registrasi</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="nomor_registrasi" name="nomor_registrasi" value="" placeholder="Nomor Registrasi" type="text">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="nik">No. KTP/NIK</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="nik" name="nik" value="" placeholder="NIK" type="text">
                                          <button type="submit" class="btn btn-primary" id="cek_nik">Cek NIK</button>
                                          <div class="overlay" id="spin_form" style="display:none;">
                                            <i class="fa fa-refresh fa-spin"></i>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="nama_anggota">Nama Anggota Keluarga</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="nama_anggota" name="nama_anggota" value="" placeholder="Nama Anggota Keluarga" type="text">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jabatan">Jabatan</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select class="form-control" id="jabatan" name="jabatan" >
                                              <option value="ya">Ya</option>
                                              <option value="tidak">Tidak</option>
                                            </select>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jenis_kelamin">Jenis Kelamin</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select class="form-control" id="jenis_kelamin" name="jenis_kelamin" >
                                              <option value="laki-laki">Laki-laki</option>
                                              <option value="perempuan">Perempuan</option>
                                            </select>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="tempat_lahir">Tempat Lahir</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="tempat_lahir" name="tempat_lahir" value="" placeholder="Tempat Lahir" type="text">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="tanggal_lahir">Tempat Lahir</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="" placeholder="Tanggal Lahir" type="text">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="status_perkawinan">Status Perkawinan</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="status_perkawinan" name="status_perkawinan" >
                                            <option value="menikah">Menikah</option>
                                            <option value="lajang">Lajang</option>
                                            <option value="janda">Janda</option>
                                            <option value="duda">Duda</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="status_dalam_keluarga">Status Dalam Keluarga</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="status_dalam_keluarga" name="status_dalam_keluarga" >
                                            <option value="Kepala Rumah Tangga">Kepala Rumah Tangga</option>
                                            <option value="KK atau Anggota Keluarga">KK atau Anggota Keluarga</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="agama">Agama</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="agama" name="agama" >
                                            <option value="Islam">Islam</option>
                                            <option value="Kristen">Kristen</option>
                                            <option value="Katholik">Katholik</option>
                                            <option value="Hindu">Hindu</option>
                                            <option value="Budha">Budha</option>
                                            <option value="Khonghuchu">Khonghuchu</option>
                                            <option value="Kepercayaan Lain">Kepercayaan Lain</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="alamat_anggota_keluarga">Alamat</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="alamat_anggota_keluarga" name="alamat_anggota_keluarga" value="" placeholder="Alamat" type="text">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="status_tinggal">Status Tinggal</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="status_tinggal" name="status_tinggal" value="" placeholder="Mukim/ Perantau asal ..../ Merantau ke ...." type="text">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label>Provinsi</label>
                                          <div class="overlay" id="overlay_id_propinsi_anggota_keluarga" style="display:none;">
                                            <i class="fa fa-refresh fa-spin"></i>
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="id_propinsi_anggota_keluarga" name="id_propinsi_anggota_keluarga" >
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="id_kabupaten_anggota_keluarga">Kabupaten</label>
                                          <div class="overlay" id="overlay_id_kabupaten_anggota_keluarga" style="display:none;">
                                            <i class="fa fa-refresh fa-spin"></i>
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="id_kabupaten_anggota_keluarga" name="id_kabupaten_anggota_keluarga" >
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="id_kecamatan_anggota_keluarga">Kecamatan</label>
                                          <div class="overlay" id="overlay_id_kecamatan_anggota_keluarga" style="display:none;">
                                            <i class="fa fa-refresh fa-spin"></i>
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="id_kecamatan_anggota_keluarga" name="id_kecamatan_anggota_keluarga" >
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="id_desa_anggota_keluarga">Desa</label>
                                          <div class="overlay" id="overlay_id_desa_anggota_keluarga" style="display:none;">
                                            <i class="fa fa-refresh fa-spin"></i>
                                          </div>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="id_desa_anggota_keluarga" name="id_desa_anggota_keluarga" >
                                            <option value="0">Pilih Desa</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="pendidikan">Pendidikan</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="pendidikan" name="pendidikan" >
                                            <option value="Tidak Tamat SD">Tidak Tamat SD</option>
                                            <option value="SD/MI Sederajat">SD/MI Sederajat</option>
                                            <option value="SMP/ Sederajat">SMP/ Sederajat</option>
                                            <option value="SMU/SMK/ Sederajat">SMU/SMK/ Sederajat</option>
                                            <option value="Diploma">Diploma</option>
                                            <option value="S1">S1</option>
                                            <option value="S2">S2</option>
                                            <option value="S3">S3</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="pekerjaan">Pekerjaan</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="pekerjaan" name="pekerjaan" >
                                            <option value="Petani">Petani</option>
                                            <option value="Pedagang">Pedagang</option>
                                            <option value="Swasta">Swasta</option>
                                            <option value="Wirausaha">Wirausaha</option>
                                            <option value="PNS">PNS</option>
                                            <option value="TNI/POLRI">TNI/POLRI</option>
                                            <option value="Lainnya">Lainnya</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="aseptor_kb">Akseptor KB</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="aseptor_kb" name="aseptor_kb" >
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jenis_aseptor_kb">Jenis Akseptor KB</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="jenis_aseptor_kb" name="jenis_aseptor_kb">
                                            <option value="">Pilih</option>
                                            <option value="mop">MOP</option>
                                            <option value="mow">MOW</option>
                                            <option value="uid">UID</option>
                                            <option value="implant">Implant</option>
                                            <option value="pil">Pil</option>
                                            <option value="suntik">Suntik</option>
                                            <option value="kondom">Kondom</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="aktif_dalam_kegiatan_posyandu">Aktif Dalam Kegiatan Posyandu</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="aktif_dalam_kegiatan_posyandu" name="aktif_dalam_kegiatan_posyandu">
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="frekuensi_volume_posyandu">Frekuensi/ Volume Aktif Dalam Kegiatan Posyandu</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="frekuensi_volume_posyandu" name="frekuensi_volume_posyandu" value="" placeholder="....kali" type="text">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="mengikuti_program_bina_keluarga_balita">Mengikuti Program Bina Keluarga Balita</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="mengikuti_program_bina_keluarga_balita" name="mengikuti_program_bina_keluarga_balita">
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="memiliki_tabungan">Memiliki Tabungan  <span class="text-warning">(diisi khusus KK)</span></label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="memiliki_tabungan" name="memiliki_tabungan">
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="mengikuti_kelompok_belajar">Mengikuti Kelompok Belajar </label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="mengikuti_kelompok_belajar" name="mengikuti_kelompok_belajar">
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jenis_kelompok_belajar">Jenis Kelompok Belajar </label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="jenis_kelompok_belajar" name="jenis_kelompok_belajar">
                                            <option value="">Pilih</option>
                                            <option value="Paket A">Paket A</option>
                                            <option value="Paket B">Paket B</option>
                                            <option value="Paket C">Paket C</option>
                                            <option value="KF">KF</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="mengikuti_paud_sejenis">Mengikuti PAUD/ Sejenis</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="mengikuti_paud_sejenis" name="mengikuti_paud_sejenis">
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="ikut_dalam_kegiatan_koperasi">Ikut Dalam Kegiatan Koperasi</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="ikut_dalam_kegiatan_koperasi" name="ikut_dalam_kegiatan_koperasi">
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jenis_koperasi">Jenis Koperasi</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="jenis_koperasi" name="jenis_koperasi" value="" placeholder="...." type="text">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="berkebutuhan_khusus">Berkebutuhan Khusus</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="berkebutuhan_khusus" name="berkebutuhan_khusus">
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                          <select class="form-control" id="berkebutuhan_khusus_fisik" name="berkebutuhan_khusus_fisik">
                                            <option value="">Pilih</option>
                                            <option value="fisik">Fisik</option>
                                            <option value="non fisik">Non Fisik</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <button type="submit" class="btn btn-primary" id="simpan_anggota_keluarga"><i class="fa fa-save"></i> SIMPAN ANGGOTA KELUARGA</button>
                                  <button type="submit" class="btn btn-warning" id="update_anggota_keluarga" style="display:none;"><i class="fa fa-save"></i> UPDATE ANGGOTA KELUARGA</button>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
										</form>

										<div class="overlay" id="overlay_form_input" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
									<div class="tab-pane table-responsive" id="tab_data_anggota_keluarga">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="id_data_keluarga" class="form-control input-sm pull-right" placeholder="Search" type="text" id="id_data_keluarga" style="display:none;" value="<?php echo $this->input->get('id_data_keluarga'); ?>">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="999999999">Semua</option>
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="anggota_keluarga.nama_anggota">Nama</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_anggota_keluarga"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<thead>
                            <tr>
                              <th>No</th>
                              <th>No. Reg</th>
                              <th>Nama Anggota Keluarga</th>
                              <th>Status Dalam Keluarga</th>
                              <th>Status Dalam Pernikahan</th>
                              <th>Jenis Kelamin</th> 
                              <th>Tgl.Lahir/ Umur</th> 
                              <th>Pendidikan</th>
                              <th>Pekerjaan</th>
                              <th>Proses</th>
                            </tr>
													</thead>
													<tbody id="tbl_data_anggota_keluarga">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_anggota_keluarga" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var id_data_keluarga = $('#id_data_keluarga').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        id_data_keluarga:id_data_keluarga,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>anggota_keluarga/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" id_data_keluarga="' + id_data_keluarga + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var id_data_keluarga = $('#id_data_keluarga').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        id_data_keluarga:id_data_keluarga,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedAnggota_keluarga() {
    $('#id_anggota_keluarga, #nomor_registrasi, #nik, #nama_anggota, #jabatan, #jenis_kelamin, #tempat_lahir, #tanggal_lahir, #status_perkawinan, #status_dalam_keluarga, #agama, #alamat_anggota_keluarga, #status_tinggal, #id_propinsi, #id_kabupaten, #id_kecamatan, #id_desa, #pendidikan, #aseptor_kb, #jenis_aseptor_kb, #aktif_dalam_kegiatan_posyandu, #frekuensi_volume_posyandu, #mengikuti_program_bina_keluarga_balita, #memiliki_tabungan, #mengikuti_kelompok_belajar, #jenis_kelompok_belajar, #mengikuti_paud_sejenis, #ikut_dalam_kegiatan_koperasi, #jenis_koperasi, #berkebutuhan_khusus, #berkebutuhan_khusus_fisik, #pekerjaan').val('');
    $('#tbl_attachment_anggota_keluarga').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_anggota_keluarga').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'nomor_registrasi', 'temp', 'nik', 'nama_anggota' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_data_keluarga"] = $("#id_data_keluarga").val();
      parameter["nomor_registrasi"] = $("#nomor_registrasi").val();
      parameter["nik"] = $("#nik").val();
      parameter["nama_anggota"] = $("#nama_anggota").val();
      parameter["jabatan"] = $("#jabatan").val();
      parameter["jenis_kelamin"] = $("#jenis_kelamin").val();
      parameter["tempat_lahir"] = $("#tempat_lahir").val();
      parameter["tanggal_lahir"] = $("#tanggal_lahir").val();
      parameter["status_perkawinan"] = $("#status_perkawinan").val();
      parameter["status_dalam_keluarga"] = $("#status_dalam_keluarga").val();
      parameter["agama"] = $("#agama").val();
      parameter["alamat_anggota_keluarga"] = $("#alamat_anggota_keluarga").val();
      parameter["status_tinggal"] = $("#status_tinggal").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["pendidikan"] = $("#pendidikan").val();
      parameter["aseptor_kb"] = $("#aseptor_kb").val();
      parameter["jenis_aseptor_kb"] = $("#jenis_aseptor_kb").val();
      parameter["aktif_dalam_kegiatan_posyandu"] = $("#aktif_dalam_kegiatan_posyandu").val();
      parameter["frekuensi_volume_posyandu"] = $("#frekuensi_volume_posyandu").val();
      parameter["mengikuti_program_bina_keluarga_balita"] = $("#mengikuti_program_bina_keluarga_balita").val();
      parameter["memiliki_tabungan"] = $("#memiliki_tabungan").val();
      parameter["mengikuti_kelompok_belajar"] = $("#mengikuti_kelompok_belajar").val();
      parameter["jenis_kelompok_belajar"] = $("#jenis_kelompok_belajar").val();
      parameter["mengikuti_paud_sejenis"] = $("#mengikuti_paud_sejenis").val();
      parameter["ikut_dalam_kegiatan_koperasi"] = $("#ikut_dalam_kegiatan_koperasi").val();
      parameter["jenis_koperasi"] = $("#jenis_koperasi").val();
      parameter["berkebutuhan_khusus"] = $("#berkebutuhan_khusus").val();
      parameter["berkebutuhan_khusus_fisik"] = $("#berkebutuhan_khusus_fisik").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      var url = '<?php echo base_url(); ?>anggota_keluarga/simpan_anggota_keluarga';
      
      var parameterRv = [ 'nomor_registrasi', 'temp', 'nik', 'nama_anggota' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedAnggota_keluarga();
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_anggota_keluarga').on('click', '.update_id_anggota_keluarga', function() {
    $('#mode').val('edit');
    $('#simpan_anggota_keluarga').hide();
    $('#update_anggota_keluarga').show();
    $('#overlay_data_anggota_keluarga').show();
    $('#tbl_data_anggota_keluarga').html('');
    var id_anggota_keluarga = $(this).closest('tr').attr('id_anggota_keluarga');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_anggota_keluarga');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_anggota_keluarga').val(id_anggota_keluarga);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_anggota_keluarga:id_anggota_keluarga
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>anggota_keluarga/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_anggota_keluarga').val(json[i].id_anggota_keluarga);
            $('#id_data_keluarga').val(json[i].id_data_keluarga);
            $('#nomor_registrasi').val(json[i].nomor_registrasi);
            $('#nik').val(json[i].nik);
            $('#nama_anggota').val(json[i].nama_anggota);
            $('#jabatan').val(json[i].jabatan);
            $('#jenis_kelamin').val(json[i].jenis_kelamin);
            $('#tempat_lahir').val(json[i].tempat_lahir);
            $('#tanggal_lahir').val(json[i].tanggal_lahir);
            $('#status_perkawinan').val(json[i].status_perkawinan);
            $('#status_dalam_keluarga').val(json[i].status_dalam_keluarga);
            $('#agama').val(json[i].agama);
            $('#alamat_anggota_keluarga').val(json[i].alamat_anggota_keluarga);
            $('#status_tinggal').val(json[i].status_tinggal);
            $('#id_propinsi_anggota_keluarga').val(json[i].id_propinsi_anggota_keluarga);
            $('#id_kabupaten_anggota_keluarga').val(json[i].id_kabupaten_anggota_keluarga);
            $('#id_kecamatan_anggota_keluarga').val(json[i].id_kecamatan_anggota_keluarga);
            $('#id_desa_anggota_keluarga').val(json[i].id_desa_anggota_keluarga);
            $('#pendidikan').val(json[i].pendidikan);
            $('#aseptor_kb').val(json[i].aseptor_kb);
            $('#jenis_aseptor_kb').val(json[i].jenis_aseptor_kb);
            $('#aktif_dalam_kegiatan_posyandu').val(json[i].aktif_dalam_kegiatan_posyandu);
            $('#frekuensi_volume_posyandu').val(json[i].frekuensi_volume_posyandu);
            $('#mengikuti_program_bina_keluarga_balita').val(json[i].mengikuti_program_bina_keluarga_balita);
            $('#memiliki_tabungan').val(json[i].memiliki_tabungan);
            $('#mengikuti_kelompok_belajar').val(json[i].mengikuti_kelompok_belajar);
            $('#jenis_kelompok_belajar').val(json[i].jenis_kelompok_belajar);
            $('#mengikuti_paud_sejenis').val(json[i].mengikuti_paud_sejenis);
            $('#ikut_dalam_kegiatan_koperasi').val(json[i].ikut_dalam_kegiatan_koperasi);
            $('#jenis_koperasi').val(json[i].jenis_koperasi);
            $('#berkebutuhan_khusus').val(json[i].berkebutuhan_khusus);
            $('#berkebutuhan_khusus_fisik').val(json[i].berkebutuhan_khusus_fisik);
            $('#pekerjaan').val(json[i].pekerjaan);
						load_wilayah_by_id_desa(json[i].id_desa);
          }
        }
      });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_anggota_keluarga').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'id_anggota_keluarga', 'dasa_wisma', 'temp', 'nama_kepala_rumah_tangga' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_anggota_keluarga"] = $("#id_anggota_keluarga").val();
      parameter["id_data_keluarga"] = $("#id_data_keluarga").val();
      parameter["temp"] = $("#temp").val();
      parameter["nomor_registrasi"] = $("#nomor_registrasi").val();
      parameter["nik"] = $("#nik").val();
      parameter["nama_anggota"] = $("#nama_anggota").val();
      parameter["jabatan"] = $("#jabatan").val();
      parameter["jenis_kelamin"] = $("#jenis_kelamin").val();
      parameter["tempat_lahir"] = $("#tempat_lahir").val();
      parameter["tanggal_lahir"] = $("#tanggal_lahir").val();
      parameter["status_perkawinan"] = $("#status_perkawinan").val();
      parameter["status_dalam_keluarga"] = $("#status_dalam_keluarga").val();
      parameter["agama"] = $("#agama").val();
      parameter["alamat_anggota_keluarga"] = $("#alamat_anggota_keluarga").val();
      parameter["status_tinggal"] = $("#status_tinggal").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["pendidikan"] = $("#pendidikan").val();
      parameter["aseptor_kb"] = $("#aseptor_kb").val();
      parameter["jenis_aseptor_kb"] = $("#jenis_aseptor_kb").val();
      parameter["aktif_dalam_kegiatan_posyandu"] = $("#aktif_dalam_kegiatan_posyandu").val();
      parameter["frekuensi_volume_posyandu"] = $("#frekuensi_volume_posyandu").val();
      parameter["mengikuti_program_bina_keluarga_balita"] = $("#mengikuti_program_bina_keluarga_balita").val();
      parameter["memiliki_tabungan"] = $("#memiliki_tabungan").val();
      parameter["mengikuti_kelompok_belajar"] = $("#mengikuti_kelompok_belajar").val();
      parameter["jenis_kelompok_belajar"] = $("#jenis_kelompok_belajar").val();
      parameter["mengikuti_paud_sejenis"] = $("#mengikuti_paud_sejenis").val();
      parameter["ikut_dalam_kegiatan_koperasi"] = $("#ikut_dalam_kegiatan_koperasi").val();
      parameter["jenis_koperasi"] = $("#jenis_koperasi").val();
      parameter["berkebutuhan_khusus"] = $("#berkebutuhan_khusus").val();
      parameter["berkebutuhan_khusus_fisik"] = $("#berkebutuhan_khusus_fisik").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      var url = '<?php echo base_url(); ?>anggota_keluarga/update_anggota_keluarga';
      
      var parameterRv = [ 'id_anggota_keluarga', 'dasa_wisma', 'temp', 'nama_kepala_rumah_tangga' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_anggota_keluarga').on('click', '#del_ajax_anggota_keluarga', function() {
    var id_anggota_keluarga = $(this).closest('tr').attr('id_anggota_keluarga');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_anggota_keluarga"] = id_anggota_keluarga;
        var url = '<?php echo base_url(); ?>anggota_keluarga/hapus/';
        HapusData(parameter, url);
        $('[id_anggota_keluarga='+id_anggota_keluarga+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
});
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_lahir, #tanggal_masuk_kader, #tanggal_pelatihan').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  load_id_propinsi_anggota_keluarga();
});
</script>
<script>
  function load_id_propinsi_anggota_keluarga() {
    $('#id_propinsi_anggota_keluarga').html('');
    $('#overlay_id_propinsi_anggota_keluarga').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_keluarga/option_propinsi/',
      success: function(html) {
        $('#id_propinsi_anggota_keluarga').html('<option value="0">Pilih Provinsi</option>'+html+'');
        $('#overlay_id_propinsi_anggota_keluarga').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_propinsi_anggota_keluarga').on('change', function(e) {
      e.preventDefault();
      var id_propinsi = $('#id_propinsi_anggota_keluarga').val();
      load_id_kabupaten_anggota_keluarga(id_propinsi);
    });
  });
</script>
<script>
  function load_id_kabupaten_anggota_keluarga(id_propinsi) {
    $('#id_kabupaten_anggota_keluarga').html('');
    $('#overlay_id_kabupaten_anggota_keluarga').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_propinsi: id_propinsi
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_keluarga/id_kabupaten_by_id_propinsi/',
      success: function(html) {
        $('#id_kabupaten_anggota_keluarga').html('<option value="0">Pilih Kabupaten</option>'+html+'');
        $('#overlay_id_kabupaten_anggota_keluarga').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kabupaten_anggota_keluarga').on('change', function(e) {
      e.preventDefault();
      var id_kabupaten = $('#id_kabupaten_anggota_keluarga').val();
      load_id_kecamatan_anggota_keluarga(id_kabupaten);
    });
  });
</script>
<script>
  function load_id_kecamatan_anggota_keluarga(id_kabupaten) {
    $('#id_kecamatan_anggota_keluarga').html('');
    $('#overlay_id_kecamatan_anggota_keluarga').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_kabupaten:id_kabupaten
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_keluarga/id_kecamatan_by_id_kabupaten/',
      success: function(html) {
        $('#id_kecamatan_anggota_keluarga').html('<option value="0">Pilih Kecamatan</option>'+html+'');
        $('#overlay_id_kecamatan_anggota_keluarga').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan_anggota_keluarga').on('change', function(e) {
      e.preventDefault();
      var id_kecamatan = $('#id_kecamatan_anggota_keluarga').val();
      load_id_desa_anggota_keluarga(id_kecamatan);
    });
  });
</script>
<script>
  function load_id_desa_anggota_keluarga(id_kecamatan) {
    $('#id_desa_anggota_keluarga').html('');
    $('#overlay_id_desa_anggota_keluarga').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_kecamatan:id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_keluarga/id_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa_anggota_keluarga').html('<option value="0">Pilih Desa</option>'+html+'');
        $('#overlay_id_desa_anggota_keluarga').fadeOut('slow');
      }
    });
  }
</script>
<script>
  function load_wilayah_by_id_desa(id_desa) {
    $('#id_desa_anggota_keluarga').html('');
    $('#id_kecamatan_anggota_keluarga').html('');
    $('#id_kabupaten_anggota_keluarga').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_desa:id_desa
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_wilayah_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_desa_anggota_keluarga').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan_anggota_keluarga').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten_anggota_keluarga').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
					$('#id_propinsi_anggota_keluarga').val(json[i].id_propinsi);
        }
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  $("#cek_nik").on("click", function(e) {
    e.preventDefault();
    $('#spin_form').show();
    var metode = $("#metode").val();
    var url = 'http://capilwonosob0.ddns.net:8181/ws_server/get_json/kominfo/callnik?USER_ID=KOMINFO&PASSWORD=kominfo12346&NIK=' + $("#nik").val()+'';
      success: function(json) {
        if(json.response == null){
          if(json.metaData.code == 500){
            alertify.alert('Maaf koneksi database webservice pCare pusat error, Proses tidak bisa kami lanjutkan');
          }
          else{
            alertify.alert('Maaf Terjadi Kesalahan');
          }
        }
        else{
            $("#nik").val(json.response.NIK);
            $("#no_kk").val(json.response.NO_KK);//Form Pendaftaran pCare
            $("#nama_anggota").val(json.response.NAMA_LGKP);//Form Pendaftaran SIK
            $("#tempat_lahir").val(json.response.TMPT_LHR);//Form Pendaftaran SIK
            $("#tanggal_lahir").val(json.response.TGL_LHR);//Form Pendaftaran pCare
            $("#gol_darah").val(json.response.GOL_DARAH);
            $("#agama").val(json.response.AGAMA);
            $("#status_perkawinan").val(json.response.STATUS_KAWIN);
            $("#status_dalam_keluarga").val(json.response.STAT_HBKEL);
            $("#pendidikan").val(json.response.PDDK_AKH);
            $("#pekerjaan").val(json.response.JENIS_PKRJN);
            $("#kode_provinsi").val(json.response.NO_PROP);
            $("#nama_provinsi").val(json.response.PROP_NAME);
            $("#kode_kabupaten").val(json.response.NO_KAB);
            $("#nama_kabupaten").val(json.response.KAB_NAME);
            $("#kode_kecamatan").val(json.response.NO_KEC);
            $("#nama_kecamatan").val(json.response.KEC_NAME);
            $("#kode_desa").val(json.response.NO_KEL);
            $("#nama_desa").val(json.response.KEL_NAME);
            $("#alamat").val(json.response.ALAMAT);
            $("#rt").val(json.response.NO_RT);
            $("#rw").val(json.response.NO_RW);
            $("#dusun").val(json.response.DUSUN);
            $("#kode_pos").val(json.response.KODE_POS);
            $("#jenis_kelamin").val(json.response.JENIS_KLMIN);
            if(json.response.JENIS_KLMIN == 'LAKI-LAKI'){
              $("#jenis_kelamin").val('laki-laki');//Form Pendaftaran SIK
            }
            else{
              $("#jenis_kelamin").val('perempuan');//Form Pendaftaran SIK
            }
            
        }
        
        $('#spin_form').hide();
      }
  });
});
</script>