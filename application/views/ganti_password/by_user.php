    <div class="register-box">
      <div class="register-box-body">
        <p class="login-box-msg">Ganti Password</p>
        <div class="alert alert-warning alert-dismissable" id="loading_input_ganti_password" style="display:none;">
          <h4><i class="fa fa-refresh fa-spin"></i> Mohon tunggu....</h4>
        </div>
        <div class="alert alert-danger alert-dismissable" id="error_input_ganti_password" style="display:none;">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">�</button>
          <h4><i class="icon fa fa-ban"></i> Error !</h4>
          <p id="pesan_error"></p>
        </div>
        <form action="" id="formgantipassword" method="post">
        
          <div class="form-group has-feedback">
            <input class="form-control" id="password_baru" placeholder="Password Baru" type="text">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback" style="display:none;">
            <input class="form-control" id="id_users" placeholder="id_users" type="text" value="<?php echo $this->uri->segment(3); ?>">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          
          <div class="row">
            <div class="col-xs-8">    
              <div class="checkbox icheck">
                
              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" id="simpan_ganti_password_baru" class="btn btn-primary btn-block btn-flat"> Update</button>
            </div><!-- /.col -->
          </div>
        </form>
      </div><!-- /.form-box -->
    </div>

<script type="text/javascript">
  $(document).ready(function () {
		$("#simpan_ganti_password_baru").on("click", function(e){
		e.preventDefault();
		$('#loading_input_ganti_password').show();
		$('#error_input_ganti_password').hide();
		
		var password_lama = $("#password_lama").val();
		var ulangi_password_lama = $("#ulangi_password_lama").val();
		var password_baru = $("#password_baru").val();
		var id_users = $("#id_users").val();
		if( password_lama != ulangi_password_lama ){
				$('#loading_login').fadeOut( "slow" );
				$('#error_input_ganti_password').show();
				$('#pesan_error').html('Password lama tidak sama');
				$('#loading_input_ganti_password').fadeOut("slow");
			}
		else{
			$.ajax({
			type: "POST",
			async: true, 
			data: {
					password_baru:password_baru,
					id_users:id_users
				}, 
			dataType: "json",
			url: '<?php echo base_url(); ?>ganti_password/proses_ganti_password_by_user',   
			success: function(json) {
				for (var i = 0; i < json.length; i++) {
					if(json[i].errors == 'form_kosong')
						{
							$('#loading_login').fadeOut( "slow" );
							$('#error_input_ganti_password').show();
							$('#pesan_error').html('Mohon isi data secara lengkap');
							//validasi input baru
							if(json[i].password_baru == ''){ $('#password_baru').css('background-color','#DFB5B4'); } else { $('#password_baru').removeAttr( 'style' ); }
						}
					else if(json[i].errors == 'Yes')
						{
							$('#loading_login').fadeOut( "slow" );
							$('#error_input_ganti_password').show();
							$('#pesan_error').html('Password lama anda salah');
							//validasi input baru
							if(json[i].password_baru == ''){ $('#password_baru').css('background-color','#DFB5B4'); } else { $('#password_baru').removeAttr( 'style' ); }
						}	
					else if(json[i].errors == 'No')
						{
							$('#formgantipassword').fadeOut( "slow" );
              $('#pesan_error').html('Password berhasil diganti');
            }
					else
						{
							$("#box_form form").trigger( "reset" );
							$("#box_form").fadeOut( "slow" );
							$('#pesan_berhasil').show();
						}
					}
					$('#loading_input_ganti_password').fadeOut("slow");
				}
			});
			}
		
		});
  });
</script>