
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Keluarga</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>data_keluarga">Home</a></li>
              <li class="breadcrumb-item active">Data Keluarga</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#tab_form_data_keluarga" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_data_data_keluarga" data-toggle="tab" id="klik_tab_data_data_keluarga">Data</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>data_keluarga"><i class="fa fa-refresh"></i></a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
									<div class="tab-pane active" id="tab_form_data_keluarga">
										<input name="tabel" id="tabel" value="data_keluarga" type="hidden" value="">
										<form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=data_keluarga" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="">
                            <div class="card-header">
                            <div class="alert alert-info alert-dismissible">
                            <h4 class="card-title text-primary">FORMULIR INPUT DATA KELUARGA</h4>
                            </div>
                            </div>
                            <div class="card-body">
                              <input name="page" id="page" value="1" type="hidden" value="">
                              <div class="form-group" style="display:none;">
                                <label for="temp">temp</label>
                                <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="mode">mode</label>
                                <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="id_data_keluarga">id_data_keluarga</label>
                                <input class="form-control" id="id_data_keluarga" name="id" value="" placeholder="id_data_keluarga" type="text">
                              </div>
                              <div class="row">
                                <div class="col-md-4">
                                  <div class="form-group">
                                    <div class="row">
                                      <div class="col-md-5">
                                        <label for="dasa_wisma">Dasa Wisma</label>
                                      </div>
                                      <div class="col-md-7">
                                        <input class="form-control" id="dasa_wisma" name="dasa_wisma" value="" placeholder="dasa_wisma" type="text">
                                      </div>
                                      <div class="col-md-5">
                                        <label for="id_propinsi">Provinsi</label>
                                      </div>
                                      <div class="col-md-7">
                                        <select class="form-control" id="id_propinsi" name="id_propinsi" >
                                          <option value="1">Jawa Tengah</option>
                                        </select>
                                      </div>
                                      <div class="col-md-5">
                                        <label for="id_kabupaten">Kabupaten</label>
                                      </div>
                                      <div class="col-md-7">
                                        <select class="form-control" id="id_kabupaten" name="id_kabupaten" >
                                          <option value="1">Wonosobo</option>
                                        </select>
                                      </div>
                                      <div class="col-md-5">
                                        <label for="id_kecamatan">Kecamatan</label>
                                        <div class="overlay" id="overlay_id_kecamatan" style="display:none;">
                                          <i class="fa fa-refresh fa-spin"></i>
                                        </div>
                                      </div>
                                      <div class="col-md-7">
                                        <select class="form-control" id="id_kecamatan" name="id_kecamatan" >
                                        </select>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-4">
                                  <div class="form-group">
                                    <div class="row">
                                      <div class="col-md-5">
                                        <label for="id_desa">Desa</label>
                                        <div class="overlay" id="overlay_id_desa" style="display:none;">
                                          <i class="fa fa-refresh fa-spin"></i>
                                        </div>
                                      </div>
                                      <div class="col-md-7">
                                        <select class="form-control" id="id_desa" name="id_desa" >
                                          <option value="0">Pilih Desa</option>
                                        </select>
                                      </div>
                                      <div class="col-md-5">
                                        <label for="id_dusun">Dusun</label>
                                        <div class="overlay" id="overlay_id_dusun" style="display:none;">
                                          <i class="fa fa-refresh fa-spin"></i>
                                        </div>
                                      </div>
                                      <div class="col-md-7">
                                        <select class="form-control" id="id_dusun" name="id_dusun" >
                                          <option value="0">Pilih Dusun</option>
                                        </select>
                                      </div>
                                      <div class="col-md-5">
                                        <label for="rw">RW</label>
                                      </div>
                                      <div class="col-md-7">
                                        <input class="form-control" id="rw" name="rw" value="" placeholder="RW" type="text">
                                      </div>
                                      <div class="col-md-5">
                                        <label for="rt">RT</label>
                                      </div>
                                      <div class="col-md-7">
                                        <input class="form-control" id="rt" name="rt" value="" placeholder="RT" type="text">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-md-8">
                                  <div class="form-group">
                                    <div class="row">
                                      <div class="col-md-5">
                                        <label for="nama_kepala_rumah_tangga">Nama Kepala Rumah Tangga</label>
                                      </div>
                                      <div class="col-md-7">
                                        <input class="form-control" id="nama_kepala_rumah_tangga" name="nama_kepala_rumah_tangga" value="" placeholder="Nama Kepala Rumah Tangga" type="text">
                                      </div>
                                      <div class="col-md-5">
                                        <label for="jumlah_anggota_keluarga">Jumlah Anggota Keluarga</label>
                                      </div>
                                      <div class="col-md-7">
                                        <div class="row">
                                          <div class="col-md-9">
                                            <input class="form-control" id="jumlah_anggota_keluarga" name="jumlah_anggota_keluarga" value="" placeholder="Jumlah Anggota Keluarga" type="text">
                                          </div>
                                          <div class="col-md-3">
                                            <span>Orang</span>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-5">
                                      </div>
                                      <div class="col-md-7">
                                        <div class="row">
                                          <div class="col-md-3">
                                            <label for="jumlah_anggota_keluarga_laki_laki">Laki-laki</label>
                                          </div>
                                          <div class="col-md-6">
                                            <input class="form-control" id="jumlah_anggota_keluarga_laki_laki" name="jumlah_anggota_keluarga_laki_laki" value="" placeholder="Jumlah Anggota Keluarga" type="text">
                                          </div>
                                          <div class="col-md-3">
                                            <span>Orang</span>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-5">
                                      </div>
                                      <div class="col-md-7">
                                        <div class="row">
                                          <div class="col-md-3">
                                            <label for="jumlah_anggota_keluarga_perempuan">Perempuan</label>
                                          </div>
                                          <div class="col-md-6">
                                            <input class="form-control" id="jumlah_anggota_keluarga_perempuan" name="jumlah_anggota_keluarga_perempuan" value="" placeholder="Jumlah Anggota Keluarga Perempuan" type="text">
                                          </div>
                                          <div class="col-md-3">
                                            <span>Orang</span>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jumlah_kartu_keluarga">Jumlah Kartu Keluarga</label>
                                        </div>
                                        <div class="col-md-3">
                                          <input class="form-control" id="jumlah_kartu_keluarga" name="jumlah_kartu_keluarga" value="" placeholder="Jumlah Kartu Keluarga" type="text">
                                        </div>
                                        <div class="col-md-3">
                                          <span></span>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jumlah_balita">Jumlah Balita</label>
                                        </div>
                                        <div class="col-md-3">
                                          <input class="form-control" id="jumlah_balita" name="jumlah_balita" value="" placeholder="Jumlah Balita" type="text">
                                        </div>
                                        <div class="col-md-3">
                                          <span>Anak</span>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jumlah_pus">Jumlah PUS</label>
                                        </div>
                                        <div class="col-md-3">
                                          <input class="form-control" id="jumlah_pus" name="jumlah_pus" value="" placeholder="Jumlah PUS" type="text">
                                        </div>
                                        <div class="col-md-3">
                                          <span>Pasang</span>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jumlah_wus">Jumlah WUS</label>
                                        </div>
                                        <div class="col-md-3">
                                          <input class="form-control" id="jumlah_wus" name="jumlah_wus" value="" placeholder="Jumlah WUS" type="text">
                                        </div>
                                        <div class="col-md-3">
                                          <span>Orang</span>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jumlah_buta">Jumlah Buta</label>
                                        </div>
                                        <div class="col-md-3">
                                          <input class="form-control" id="jumlah_buta" name="jumlah_buta" value="" placeholder="Jumlah Buta" type="text">
                                        </div>
                                        <div class="col-md-3">
                                          <span>Orang</span>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jumlah_ibu_hamil">Jumlah Ibu Hamil</label>
                                        </div>
                                        <div class="col-md-3">
                                          <input class="form-control" id="jumlah_ibu_hamil" name="jumlah_ibu_hamil" value="" placeholder="Jumlah Ibu Hamil" type="text">
                                        </div>
                                        <div class="col-md-3">
                                          <span>Orang</span>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jumlah_ibu_menyusui">Jumlah Ibu Menyusui</label>
                                        </div>
                                        <div class="col-md-3">
                                          <input class="form-control" id="jumlah_ibu_menyusui" name="jumlah_ibu_menyusui" value="" placeholder="Jumlah Menyusui" type="text">
                                        </div>
                                        <div class="col-md-3">
                                          <span>Orang</span>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jumlah_lansia">Jumlah Lansia</label>
                                        </div>
                                        <div class="col-md-3">
                                          <input class="form-control" id="jumlah_lansia" name="jumlah_lansia" value="" placeholder="Jumlah Lansia" type="text">
                                        </div>
                                        <div class="col-md-3">
                                          <span>Orang</span>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div><!-- end 8 -->
                                <div class="col-md-12">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="makanan_pokok">Makanan Pokok Sehari-hari</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="makanan_pokok" name="makanan_pokok" >
                                            <option value="beras">Beras</option>
                                            <option value="non beras">Non Beras</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jenis_makanan_pokok">Jenis Makanan Pokok</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="jenis_makanan_pokok" name="jenis_makanan_pokok" >
                                            <option value="">Pilih</option>
                                            <option value="Ubi">Ubi</option>
                                            <option value="Kentang">Kentang</option>
                                            <option value="Jagung">Jagung</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="mempunyai_jamban_keluarga">Mempunyai Jamban Keluarga</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="mempunyai_jamban_keluarga" name="mempunyai_jamban_keluarga" >
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jumlah_jamban_keluarga">Jumlah Jamban Keluarga</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="jumlah_jamban_keluarga" name="jumlah_jamban_keluarga" value="" placeholder="Jumlah Jamban" type="text">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="sumber_air_keluarga">Sumber Air Keluarga</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="sumber_air_keluarga" name="sumber_air_keluarga" >
                                            <option value="pdam">PDAM</option>
                                            <option value="sumur">Sumur</option>
                                            <option value="lainnya">Lainnya</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="memiliki_pembuangan_sampah">Memiliki Tempat Pembuangan Sampah</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="memiliki_pembuangan_sampah" name="memiliki_pembuangan_sampah" >
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="mempunyai_saluran_pembuangan_air_limbah">Mempunyai Saluran Pembuangan Air Limbah</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="mempunyai_saluran_pembuangan_air_limbah" name="mempunyai_saluran_pembuangan_air_limbah" >
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="menempel_stiker_p4k">Menempel Stiker P4K</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="menempel_stiker_p4k" name="menempel_stiker_p4k" >
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="kriteria_rumah">Kriteria Rumah</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="kriteria_rumah" name="kriteria_rumah" >
                                            <option value="sehat">Sehat</option>
                                            <option value="kurang sehat">Kurang Sehat</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="kegiatan_up2k">Aktifitas UP2K</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="kegiatan_up2k" name="kegiatan_up2k" >
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jenis_usaha">Jenis Usaha</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="jenis_usaha" name="jenis_usaha" >
                                            <option value="">Pilih</option>
                                            <option value="warung">Warung</option>
                                            <option value="kegiatan koperasi">Kegiatan Koperasi</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="aktifitas_kegiatan_usaha_kesehatan_lingkungan">Aktifitas Kegiatan Usaha Kesehatan Lingkungan</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="aktifitas_kegiatan_usaha_kesehatan_lingkungan" name="aktifitas_kegiatan_usaha_kesehatan_lingkungan" >
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
											<div class="form-group">
                        <button type="submit" class="btn btn-primary" id="simpan_data_keluarga">SIMPAN DATA KELUARGA</button>
                        <button type="submit" class="btn btn-primary" id="update_data_keluarga" style="display:none;">UPDATE DATA KELUARGA</button>
                        <a class="btn text-primary" href="<?php echo base_url(); ?>data_keluarga"><i class="fa fa-refresh"></i></a>
											</div>
										</form>

										<div class="overlay" id="overlay_form_input" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
									<div class="tab-pane table-responsive" id="tab_data_data_keluarga">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="999999999">Semua</option>
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="data_keluarga.nama_kepala_rumah_tangga">Nama</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_data_keluarga"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<thead>
                            <tr>
                              <th>No</th>
                              <th>Nama Kepala Rumah Tangga</th>
                              <th>Jumlah KK</th>
                              <th>Jumlah Anggota Keluarga</th>
                              <th>Kriteria Rumah</th>
                              <th>Sumber Air</th> 
                              <th>Makanan Pokok</th> 
                              <th>Warga Mengikuti Kegiatan</th> 
                              <th>Proses</th> 
                            </tr>
													</thead>
													<tbody id="tbl_data_data_keluarga">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_data_keluarga" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				

<script type="text/javascript">
$(document).ready(function() {
  load_id_kecamatan();
});
</script>
<script>
  function load_id_kecamatan() {
    $('#id_kecamatan').html('');
    $('#overlay_id_kecamatan').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_keluarga/option_kecamatan/',
      success: function(html) {
        $('#id_kecamatan').html('<option value="0">Pilih Kecamatan</option>'+html+'');
        $('#overlay_id_kecamatan').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan').on('change', function(e) {
      e.preventDefault();
      var id_kecamatan = $('#id_kecamatan').val();
      load_option_desa(id_kecamatan);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan_warga').on('change', function(e) {
      e.preventDefault();
      var id_kecamatan = $('#id_kecamatan_warga').val();
      load_option_desa_warga(id_kecamatan);
    });
  });
</script>
<script>
  function load_option_desa(id_kecamatan) {
    $('#id_desa').html('');
    $('#overlay_id_desa').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_keluarga/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html('<option value="0">Pilih Desa</option>'+html+'');
        $('#overlay_id_desa').fadeOut('slow');
      }
    });
  }
</script>
<script>
  function load_option_desa_warga(id_kecamatan) {
    $('#id_desa_warga').html('');
    $('#overlay_id_desa_warga').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_keluarga/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa_warga').html('<option value="0">Pilih Desa</option>'+html+'');
        $('#overlay_id_desa_warga').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_desa').on('change', function(e) {
      e.preventDefault();
      var id_desa = $('#id_desa').val();
      load_option_dusun(id_desa);
    });
  });
</script>
<script>
  function load_option_dusun(id_desa) {
    $('#id_dusun').html('');
    $('#overlay_id_dusun').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_desa: id_desa
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>data_keluarga/option_dusun_by_id_desa/',
      success: function(html) {
        $('#id_dusun').html('<option value="0">Pilih Dusun</option>'+html+'');
        $('#overlay_id_dusun').fadeOut('slow');
      }
    });
  }
</script>
<script>
  function load_dusun_by_id_desa(id_dusun) {
    $('#id_dusun').html('');
    $('#id_desa').html('');
    $('#id_kecamatan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_dusun:id_dusun
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_dusun_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_dusun').html('<option value="' + json[i].id_dusun + '">' + json[i].nama_dusun + '</option>');
          $('#id_desa').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
					$('#id_propinsi').val(json[i].id_propinsi);
        }
      }
    });
  }
</script>
<script>
  function load_dusun_by_id_desa_warga(id_dusun) {
    $('#id_dusun_warga').html('');
    $('#id_desa_warga').html('');
    $('#id_kecamatan_warga').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_dusun:id_dusun
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_dusun_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_dusun_warga').html('<option value="' + json[i].id_dusun + '">' + json[i].nama_dusun + '</option>');
          $('#id_desa_warga').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan_warga').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten_warga').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
					$('#id_propinsi_warga').val(json[i].id_propinsi);
        }
      }
    });
  }
</script>
<script>
  function load_dusun_by_id_desa_warga(id_dusun) {
    $('#id_dusun_warga').html('');
    $('#id_desa_warga').html('');
    $('#id_kecamatan_warga').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_dusun:id_dusun
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_dusun_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_dusun_warga').html('<option value="' + json[i].id_dusun + '">' + json[i].nama_dusun + '</option>');
          $('#id_desa_warga').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan_warga').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten_warga').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
					$('#id_propinsi_warga').val(json[i].id_propinsi);
        }
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>data_keluarga/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedData_keluarga() {
    $('#id_data_keluarga, #temp, #id_dusun, #rt, #rw, #id_desa, #id_kecamatan, #id_kabupaten, #dasa_wisma, #nama_kepala_rumah_tangga, #jumlah_anggota_keluarga, #jumlah_anggota_keluarga_laki_laki, #jumlah_anggota_keluarga_perempuan, #jumlah_kartu_keluarga, #jumlah_balita, #jumlah_pus, #jumlah_wus, #jumlah_buta, #jumlah_ibu_hamil, #jumlah_ibu_menyusui, #jumlah_lansia, #makanan_pokok, #jenis_makanan_pokok, #mempunyai_jamban_keluarga, #jumlah_jamban_keluarga, #sumber_air_keluarga, #memiliki_pembuangan_sampah, #mempunyai_saluran_pembuangan_air_limbah, #menempel_stiker_p4k, #kriteria_rumah, #kegiatan_up2k, #jenis_usahaaktifitas_kegiatan_usaha_kesehatan_lingkungan').val('');
    $('#tbl_attachment_data_keluarga').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_data_keluarga').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'dasa_wisma', 'temp', 'nama_kepala_rumah_tangga' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["rw"] = $("#rw").val();
      parameter["dasa_wisma"] = $("#dasa_wisma").val();
      parameter["nama_kepala_rumah_tangga"] = $("#nama_kepala_rumah_tangga").val();
      parameter["jumlah_anggota_keluarga"] = $("#jumlah_anggota_keluarga").val();
      parameter["jumlah_anggota_keluarga_laki_laki"] = $("#jumlah_anggota_keluarga_laki_laki").val();
      parameter["jumlah_anggota_keluarga_perempuan"] = $("#jumlah_anggota_keluarga_perempuan").val();
      parameter["jumlah_kartu_keluarga"] = $("#jumlah_kartu_keluarga").val();
      parameter["jumlah_balita"] = $("#jumlah_balita").val();
      parameter["jumlah_pus"] = $("#jumlah_pus").val();
      parameter["jumlah_wus"] = $("#jumlah_wus").val();
      parameter["jumlah_buta"] = $("#jumlah_buta").val();
      parameter["jumlah_ibu_hamil"] = $("#jumlah_ibu_hamil").val();
      parameter["jumlah_ibu_menyusui"] = $("#jumlah_ibu_menyusui").val();
      parameter["jumlah_lansia"] = $("#jumlah_lansia").val();
      parameter["makanan_pokok"] = $("#makanan_pokok").val();
      parameter["jenis_makanan_pokok"] = $("#jenis_makanan_pokok").val();
      parameter["mempunyai_jamban_keluarga"] = $("#mempunyai_jamban_keluarga").val();
      parameter["jumlah_jamban_keluarga"] = $("#jumlah_jamban_keluarga").val();
      parameter["sumber_air_keluarga"] = $("#sumber_air_keluarga").val();
      parameter["memiliki_pembuangan_sampah"] = $("#memiliki_pembuangan_sampah").val();
      parameter["mempunyai_saluran_pembuangan_air_limbah"] = $("#mempunyai_saluran_pembuangan_air_limbah").val();
      parameter["menempel_stiker_p4k"] = $("#menempel_stiker_p4k").val();
      parameter["kriteria_rumah"] = $("#kriteria_rumah").val();
      parameter["kegiatan_up2k"] = $("#kegiatan_up2k").val();
      parameter["jenis_usaha"] = $("#jenis_usaha").val();
      parameter["aktifitas_kegiatan_usaha_kesehatan_lingkungan"] = $("#aktifitas_kegiatan_usaha_kesehatan_lingkungan").val();
      var url = '<?php echo base_url(); ?>data_keluarga/simpan_data_keluarga';
      
      var parameterRv = [ 'dasa_wisma', 'temp', 'nama_kepala_rumah_tangga' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedData_keluarga();
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_data_keluarga').on('click', '.update_id_data_keluarga', function() {
    $('#mode').val('edit');
    $('#simpan_data_keluarga').hide();
    $('#update_data_keluarga').show();
    $('#overlay_data_anggota_keluarga').show();
    $('#tbl_data_anggota_keluarga').html('');
    var id_data_keluarga = $(this).closest('tr').attr('id_data_keluarga');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_data_keluarga');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_data_keluarga').val(id_data_keluarga);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_data_keluarga:id_data_keluarga
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>data_keluarga/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_propinsi').val(json[i].id_propinsi);
            $('#id_kabupaten').val(json[i].id_kabupaten);
            $('#id_kecamatan').val(json[i].id_kecamatan);
            $('#id_desa').val(json[i].id_desa);
            $('#id_dusun').val(json[i].id_dusun);
            $('#rt').val(json[i].rt);
            $('#rw').val(json[i].rw);
            $('#dasa_wisma').val(json[i].dasa_wisma);
            $('#nama_kepala_rumah_tangga').val(json[i].nama_kepala_rumah_tangga);
            $('#jumlah_anggota_keluarga').val(json[i].jumlah_anggota_keluarga);
            $('#jumlah_anggota_keluarga_laki_laki').val(json[i].jumlah_anggota_keluarga_laki_laki);
            $('#jumlah_anggota_keluarga_perempuan').val(json[i].jumlah_anggota_keluarga_perempuan);
            $('#jumlah_kartu_keluarga').val(json[i].jumlah_kartu_keluarga);
            $('#jumlah_balita').val(json[i].jumlah_balita);
            $('#jumlah_pus').val(json[i].jumlah_pus);
            $('#jumlah_wus').val(json[i].jumlah_wus);
            $('#jumlah_buta').val(json[i].jumlah_buta);
            $('#jumlah_ibu_hamil').val(json[i].jumlah_ibu_hamil);
            $('#jumlah_ibu_menyusui').val(json[i].jumlah_ibu_menyusui);
            $('#jumlah_lansia').val(json[i].jumlah_lansia);
            $('#makanan_pokok').val(json[i].makanan_pokok);
            $('#jenis_makanan_pokok').val(json[i].jenis_makanan_pokok);
            $('#mempunyai_jamban_keluarga').val(json[i].mempunyai_jamban_keluarga);
            $('#jumlah_jamban_keluarga').val(json[i].jumlah_jamban_keluarga);
            $('#sumber_air_keluarga').val(json[i].sumber_air_keluarga);
            $('#memiliki_pembuangan_sampah').val(json[i].memiliki_pembuangan_sampah);
            $('#mempunyai_saluran_pembuangan_air_limbah').val(json[i].mempunyai_saluran_pembuangan_air_limbah);
            $('#menempel_stiker_p4k').val(json[i].menempel_stiker_p4k);
            $('#kriteria_rumah').val(json[i].kriteria_rumah);
            $('#kegiatan_up2k').val(json[i].kegiatan_up2k);
            $('#jenis_usaha').val(json[i].jenis_usaha);
            $('#aktifitas_kegiatan_usaha_kesehatan_lingkungan').val(json[i].aktifitas_kegiatan_usaha_kesehatan_lingkungan);
						load_dusun_by_id_desa(json[i].id_dusun);
						// load_wilayah_by_id_desa(json[i].id_desa);
          }
        }
      });
    //AttachmentByMode(mode, value);
		//AfterSavedAnggota_keluarga(mode, value);
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_data_keluarga').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'id_data_keluarga', 'dasa_wisma', 'temp', 'nama_kepala_rumah_tangga' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_data_keluarga"] = $("#id_data_keluarga").val();
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["rw"] = $("#rw").val();
      parameter["dasa_wisma"] = $("#dasa_wisma").val();
      parameter["nama_kepala_rumah_tangga"] = $("#nama_kepala_rumah_tangga").val();
      parameter["jumlah_anggota_keluarga"] = $("#jumlah_anggota_keluarga").val();
      parameter["jumlah_anggota_keluarga_laki_laki"] = $("#jumlah_anggota_keluarga_laki_laki").val();
      parameter["jumlah_anggota_keluarga_perempuan"] = $("#jumlah_anggota_keluarga_perempuan").val();
      parameter["jumlah_kartu_keluarga"] = $("#jumlah_kartu_keluarga").val();
      parameter["jumlah_balita"] = $("#jumlah_balita").val();
      parameter["jumlah_pus"] = $("#jumlah_pus").val();
      parameter["jumlah_wus"] = $("#jumlah_wus").val();
      parameter["jumlah_buta"] = $("#jumlah_buta").val();
      parameter["jumlah_ibu_hamil"] = $("#jumlah_ibu_hamil").val();
      parameter["jumlah_ibu_menyusui"] = $("#jumlah_ibu_menyusui").val();
      parameter["jumlah_lansia"] = $("#jumlah_lansia").val();
      parameter["makanan_pokok"] = $("#makanan_pokok").val();
      parameter["jenis_makanan_pokok"] = $("#jenis_makanan_pokok").val();
      parameter["mempunyai_jamban_keluarga"] = $("#mempunyai_jamban_keluarga").val();
      parameter["jumlah_jamban_keluarga"] = $("#jumlah_jamban_keluarga").val();
      parameter["sumber_air_keluarga"] = $("#sumber_air_keluarga").val();
      parameter["memiliki_pembuangan_sampah"] = $("#memiliki_pembuangan_sampah").val();
      parameter["mempunyai_saluran_pembuangan_air_limbah"] = $("#mempunyai_saluran_pembuangan_air_limbah").val();
      parameter["menempel_stiker_p4k"] = $("#menempel_stiker_p4k").val();
      parameter["kriteria_rumah"] = $("#kriteria_rumah").val();
      parameter["kegiatan_up2k"] = $("#kegiatan_up2k").val();
      parameter["jenis_usaha"] = $("#jenis_usaha").val();
      parameter["aktifitas_kegiatan_usaha_kesehatan_lingkungan"] = $("#aktifitas_kegiatan_usaha_kesehatan_lingkungan").val();
      var url = '<?php echo base_url(); ?>data_keluarga/update_data_keluarga';
      
      var parameterRv = [ 'id_data_keluarga', 'dasa_wisma', 'temp', 'nama_kepala_rumah_tangga' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_data_keluarga').on('click', '#del_ajax_data_keluarga', function() {
    var id_data_keluarga = $(this).closest('tr').attr('id_data_keluarga');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_data_keluarga"] = id_data_keluarga;
        var url = '<?php echo base_url(); ?>data_keluarga/hapus/';
        HapusData(parameter, url);
        $('[id_data_keluarga='+id_data_keluarga+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
});
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_lahir, #tanggal_masuk_kader, #tanggal_pelatihan').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>