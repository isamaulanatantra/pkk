
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!--<h1>Balita</h1>-->
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>balita">Home</a></li>
              <li class="breadcrumb-item active">Balita</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header p-2">
                <ul class="nav nav-pills">
                  <li class="nav-item"><a class="nav-link active" href="#tab_form_balita" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_data_balita" data-toggle="tab" id="klik_tab_data_balita">Data</a></li>
                  <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>balita/?id_data_keluarga=<?php echo $this->input->get('id_data_keluarga'); ?>"><i class="fa fa-refresh"></i></a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
									<div class="tab-pane active" id="tab_form_balita">
										<input name="tabel" id="tabel" value="balita" type="hidden" value="">
										<form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=balita" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="">
                            <div class="card-header">
                            <h4 class="card-title text-primary">FORMULIR INPUT BALITA</h4>
                            </div>
                            <div class="card-body">
                              <input name="page" id="page" value="1" type="hidden" value="">
                              <div class="form-group" style="display:none;">
                                <label for="temp">temp</label>
                                <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="mode">mode</label>
                                <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="id_balita">id_balita</label>
                                <input class="form-control" id="id_balita" name="id" value="" placeholder="id_balita" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="id_data_keluarga">id_data_keluarga</label>
                                <input class="form-control" id="id_data_keluarga" name="id" value="<?php echo $this->input->get('id_data_keluarga'); ?>" placeholder="id_data_keluarga" type="text">
                              </div>
                                <div class="form-group">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="nik">NIK</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="nik" name="nik" value="" placeholder="NIK" type="text">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="nama_balita">Nama Balita</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="nama_balita" name="nama_balita" value="" placeholder="Nama Anggota Keluarga" type="text">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="jenis_kelamin">Jenis Kelamin</label>
                                        </div>
                                        <div class="col-md-6">
                                            <select class="form-control" id="jenis_kelamin" name="jenis_kelamin" >
                                              <option value="laki-laki">Laki-laki</option>
                                              <option value="perempuan">Perempuan</option>
                                            </select>
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="tempat_lahir">Tempat Lahir</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="tempat_lahir" name="tempat_lahir" value="" placeholder="Tempat Lahir" type="text">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="tanggal_lahir">Tempat Lahir</label>
                                        </div>
                                        <div class="col-md-6">
                                          <input class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="" placeholder="Tanggal Lahir" type="text">
                                        </div>
                                      </div>
                                      <div class="row">
                                        <div class="col-md-6">
                                          <label for="berkebutuhan_khusus">Berkebutuhan Khusus</label>
                                        </div>
                                        <div class="col-md-6">
                                          <select class="form-control" id="berkebutuhan_khusus" name="berkebutuhan_khusus">
                                            <option value="ya">Ya</option>
                                            <option value="tidak">Tidak</option>
                                          </select>
                                          <select class="form-control" id="berkebutuhan_khusus_fisik" name="berkebutuhan_khusus_fisik">
                                            <option value="">Pilih</option>
                                            <option value="fisik">Fisik</option>
                                            <option value="non fisik">Non Fisik</option>
                                          </select>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <button type="submit" class="btn btn-primary" id="simpan_balita"><i class="fa fa-save"></i> SIMPAN BALITA</button>
                                  <button type="submit" class="btn btn-warning" id="update_balita" style="display:none;"><i class="fa fa-save"></i> UPDATE BALITA</button>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
										</form>

										<div class="overlay" id="overlay_form_input" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
									<div class="tab-pane table-responsive" id="tab_data_balita">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="id_data_keluarga" class="form-control input-sm pull-right" placeholder="Search" type="text" id="id_data_keluarga" style="display:none;" value="<?php echo $this->input->get('id_data_keluarga'); ?>">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="999999999">Semua</option>
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="20">20 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="balita.nama_balita">Nama</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_balita"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<thead>
                            <tr>
                              <th>No</th>
                              <th>NIK</th>
                              <th>Nama Balita</th>
                              <th>Jenis Kelamin</th> 
                              <th>Tempat Lahir</th> 
                              <th>Tgl.Lahir/ Umur</th>
                              <th>Proses</th>
                            </tr>
													</thead>
													<tbody id="tbl_data_balita">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_balita" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var id_data_keluarga = $('#id_data_keluarga').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        id_data_keluarga:id_data_keluarga,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>balita/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" id_data_keluarga="' + id_data_keluarga + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var id_data_keluarga = $('#id_data_keluarga').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        id_data_keluarga:id_data_keluarga,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedBalita() {
    $('#id_balita, #nik, #nama_balita, #jenis_kelamin, #tempat_lahir, #tanggal_lahir, #berkebutuhan_khusus, #berkebutuhan_khusus_fisik ').val('');
    $('#tbl_attachment_balita').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_balita').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'temp', 'nik', 'nama_balita' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_data_keluarga"] = $("#id_data_keluarga").val();
      parameter["nik"] = $("#nik").val();
      parameter["nama_balita"] = $("#nama_balita").val();
      parameter["jenis_kelamin"] = $("#jenis_kelamin").val();
      parameter["tempat_lahir"] = $("#tempat_lahir").val();
      parameter["tanggal_lahir"] = $("#tanggal_lahir").val();
      parameter["berkebutuhan_khusus"] = $("#berkebutuhan_khusus").val();
      parameter["berkebutuhan_khusus_fisik"] = $("#berkebutuhan_khusus_fisik").val();
      var url = '<?php echo base_url(); ?>balita/simpan_balita';
      
      var parameterRv = [ 'temp', 'nik', 'nama_balita' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedBalita();
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_balita').on('click', '.update_id_balita', function() {
    $('#mode').val('edit');
    $('#simpan_balita').hide();
    $('#update_balita').show();
    $('#overlay_data_balita').show();
    $('#tbl_data_balita').html('');
    var id_balita = $(this).closest('tr').attr('id_balita');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_balita');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_balita').val(id_balita);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_balita:id_balita
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>balita/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_balita').val(json[i].id_balita);
            $('#id_data_keluarga').val(json[i].id_data_keluarga);
            $('#nik').val(json[i].nik);
            $('#nama_balita').val(json[i].nama_balita);
            $('#jenis_kelamin').val(json[i].jenis_kelamin);
            $('#tempat_lahir').val(json[i].tempat_lahir);
            $('#tanggal_lahir').val(json[i].tanggal_lahir);
            $('#berkebutuhan_khusus').val(json[i].berkebutuhan_khusus);
            $('#berkebutuhan_khusus_fisik').val(json[i].berkebutuhan_khusus_fisik);
          }
        }
      });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_balita').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'id_balita', 'temp', 'nama_balita' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_balita"] = $("#id_balita").val();
      parameter["id_data_keluarga"] = $("#id_data_keluarga").val();
      parameter["temp"] = $("#temp").val();
      parameter["nik"] = $("#nik").val();
      parameter["nama_balita"] = $("#nama_balita").val();
      parameter["jenis_kelamin"] = $("#jenis_kelamin").val();
      parameter["tempat_lahir"] = $("#tempat_lahir").val();
      parameter["tanggal_lahir"] = $("#tanggal_lahir").val();
      parameter["berkebutuhan_khusus"] = $("#berkebutuhan_khusus").val();
      parameter["berkebutuhan_khusus_fisik"] = $("#berkebutuhan_khusus_fisik").val();
      var url = '<?php echo base_url(); ?>balita/update_balita';
      
      var parameterRv = [ 'id_balita', 'temp', 'nama_balita' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_balita').on('click', '#del_ajax_balita', function() {
    var id_balita = $(this).closest('tr').attr('id_balita');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_balita"] = id_balita;
        var url = '<?php echo base_url(); ?>balita/hapus/';
        HapusData(parameter, url);
        $('[id_balita='+id_balita+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
});
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_lahir').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>