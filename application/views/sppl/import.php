<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="row">
          <div class="col-md-6">
            <div class="box box-primary box-solid">
              <h1>Excel import thousand row data in second's</h1>
              <?php
              echo form_open_multipart('sppl/import');
              echo form_upload('file');
              echo '<br/>';
              echo form_submit(null, 'Upload');
              echo form_close();
              ?>
              <h4>Total data : <?php echo $num_rows;?></h4>
              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
