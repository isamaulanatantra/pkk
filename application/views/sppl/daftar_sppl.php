
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                Daftar SPPL
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="kata_kunci_filter" />
                  <select name="limit_data_sppl" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_sppl">
                    <option value="10">10 Per-Halaman</option>
                    <option value="20">20 Per-Halaman</option>
                    <option value="50">50 Per-Halaman</option>
                    <option value="999999999">Semua</option>
                  </select>
                  <select name="urut_data_sppl" class="form-control input-sm pull-right" style="width: 150px;" id="urut_data_sppl">
                    <option value="nama_pemohon">Nama Pemohon</option>
                    <option value="nama_usaha">Nama Usaha</option>
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default" id="search_kata_kunci"><i class="fa fa-search"></i></button>
                    <div class="overlay" id="spinners_sppl" style="display:none;">
                      <i class="fa fa-refresh fa-spin"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body table-responsive">
                <div id="tblExport">
                  <table class="table table-bordered table-hover">
                    <tbody>
                      <tr id="header_exel">
                        <th>No</th>
                        <th>Nama Pemohon</th>
                        <th>Nama Pimpinan</th>
                        <th>Alamat Pimpinan</th>
                        <th>Jenis Usaha / Kegiatan</th>
                        <th>Kapasitas Produksi</th>
                        <th>Nama Usaha</th>
                        <th>Alamat Usaha</th>
                        <th>Nomor SPPL</th>
                      </tr>
                    </tbody>
                    <tbody id="tbl_utama_sppl">
                    </tbody>
                  </table>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                </ul>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
            
<script type="text/javascript">
  $(document).ready(function() {
        var halaman = 1;
        var limit_data_sppl = $('#limit_data_sppl').val();
        var limit = limit_per_page_custome(limit_data_sppl);
        var kata_kunci = $('#kata_kunci').val();
        var urut_data_sppl = $('#urut_data_sppl').val();
        load_data_sppl(halaman, limit, kata_kunci, urut_data_sppl);
  }); 
</script>
<script>
  function load_data_sppl(halaman, limit, kata_kunci, urut_data_sppl) {
    $('#tbl_utama_sppl').html('');
    $('#spinners_data').show();
    var limit_data_sppl = $('#limit_data_sppl').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:halaman,
        limit:limit,
        kata_kunci:kata_kunci,
        urut_data_sppl:urut_data_sppl
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>sppl/json_all_sppl/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_sppl="' + json[i].id_sppl + '" id="' + json[i].id_sppl + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
					tr += '<td valign="top">' + json[i].nama_pemohon + '</td>';
					tr += '<td valign="top">' + json[i].nama_pimpinan + '</td>';
					tr += '<td valign="top">' + json[i].alamat_pimpinan + '</td>';
					tr += '<td valign="top">' + json[i].jenis_usaha_kegiatan + '</td>';
					tr += '<td valign="top">' + json[i].kapasitas_produksi + '</td>';
					tr += '<td valign="top">' + json[i].nama_usaha + '</td>';
					tr += '<td valign="top">' + json[i].alamat_usaha + '</td>';
					tr += '<td valign="top"><a target="_blank" href="#">' + json[i].nomor_sppl + '</a></td>';
          tr += '</tr>';
        }
        $('#tbl_utama_sppl').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
    load_pagination();
  }
</script>
<script>
  function load_pagination() {
    var limit_data_sppl = $('#limit_data_sppl').val();
    var limit = limit_per_page_custome(limit_data_sppl);
    var kata_kunci = $('#kata_kunci_filter').val();
    var urut_data_sppl = $('#urut_data_sppl').val();
    var posisi = $('#posisi_filter').val();
    var parent = $('#parent_filter').val();
    var tr = '';
    var td = TotalData('<?php echo base_url(); ?>sppl/total_sppl/?limit='+limit_per_page_custome(limit_data_sppl)+'&kata_kunci='+kata_kunci+'&urut_data_sppl='+urut_data_sppl+'');
    for (var i = 1; i <= td; i++) {
      tr += '<li page="'+i+'" id="'+i+'"><a class="update_id" href="#">'+i+'</a></li>';
    }
    $('#pagination').html(tr);
  }
</script>
 
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#urut_data_sppl').on('change', function(e) {
      e.preventDefault();
        var halaman = 1;
        var limit_data_sppl = $('#limit_data_sppl').val();
        var limit = limit_per_page_custome(limit_data_sppl);
        var kata_kunci = $('#kata_kunci_filter').val();
        var urut_data_sppl = $('#urut_data_sppl').val();
        var posisi = $('#posisi_filter').val();
        var parent = $('#parent_filter').val();
        load_data_sppl(halaman, limit, kata_kunci, urut_data_sppl);
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#pagination').on('click', '.update_id', function(e) {
		e.preventDefault();
		var id = $(this).closest('li').attr('page');
		var halaman = id;
    var limit_data_sppl = $('#limit_data_sppl').val();
    var limit = limit_per_page_custome(limit_data_sppl);
    var kata_kunci = $('#kata_kunci_filter').val();
    var urut_data_sppl = $('#urut_data_sppl').val();
    load_data_sppl(halaman, limit, kata_kunci, urut_data_sppl);
    
	});
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#limit_data_sppl').on('change', function(e) {
      e.preventDefault();
        var halaman = 1;
        var limit_data_sppl = $('#limit_data_sppl').val();
        var limit = limit_per_page_custome(limit_data_sppl);
        var kata_kunci = $('#kata_kunci_filter').val();
        var urut_data_sppl = $('#urut_data_sppl').val();
        var posisi = $('#posisi_filter').val();
        var parent = $('#parent_filter').val();
        load_data_sppl(halaman, limit, kata_kunci, urut_data_sppl);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#kata_kunci_filter').on('change', function(e) {
      e.preventDefault();
        var halaman = 1;
        var limit_data_sppl = $('#limit_data_sppl').val();
        var limit = limit_per_page_custome(limit_data_sppl);
        var kata_kunci = $('#kata_kunci_filter').val();
        var urut_data_sppl = $('#urut_data_sppl').val();
        var posisi = $('#posisi_filter').val();
        var parent = $('#parent_filter').val();
        load_data_sppl(halaman, limit, kata_kunci, urut_data_sppl);
    });
  });
</script>
