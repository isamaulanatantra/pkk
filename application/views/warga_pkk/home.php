
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Warga PKK</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Warga_pkk</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_form_warga_pkk" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_data_warga_pkk" data-toggle="tab" id="klik_tab_data_warga_pkk">Data</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="">
                <div class="tab-content">
									<div class="tab-pane active" id="tab_form_warga_pkk">
										<input name="tabel" id="tabel" value="warga_pkk" type="hidden" value="">
										<form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=warga_pkk" enctype="multipart/form-data">
                      <div class="row">
                        <div class="col-sm-4">
                          <div class="card">
                            <div class="card-header">
                            <h4 class="card-title">FORMULIR INPUT</h4>
                            </div>
                            <div class="card-body">
                              <input name="page" id="page" value="1" type="hidden" value="">
                              <div class="form-group" style="display:none;">
                                <label for="temp">temp</label>
                                <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="mode">mode</label>
                                <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                              </div>
                              <div class="form-group" style="display:none;">
                                <label for="id_warga_pkk">id_warga_pkk</label>
                                <input class="form-control" id="id_warga_pkk" name="id" value="" placeholder="id_warga_pkk" type="text">
                              </div>
                              <div class="form-group">
                                <label for="tahun_pendaftaran">Tahun Pendaftaran <span class="text-danger">*</span></label>
                                <select name="tahun_pendaftaran" id="tahun_pendaftaran" class="form-control">
                                  <option value="">Pilih Tahun</option>
                                  <option value="2010">2010</option>
                                  <option value="2011">2011</option>
                                  <option value="2012">2012</option>
                                  <option value="2013">2013</option>
                                  <option value="2014">2014</option>
                                  <option value="2015">2015</option>
                                  <option value="2016">2016</option>
                                  <option value="2017">2017</option>
                                  <option value="2018">2018</option>
                                  <option value="2019">2019</option>
                                  <option value="2020">2020</option>
                                </select>
                              </div>
                              <div class="form-group">
                                <label for="id_kecamatan">Kecamatan</label>
                                <select class="form-control" id="id_kecamatan" name="id_kecamatan" >
                                </select>
                                <div class="overlay" id="overlay_id_kecamatan" style="display:none;">
                                  <i class="fa fa-refresh fa-spin"></i>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="id_desa">Desa</label>
                                <select class="form-control" id="id_desa" name="id_desa" >
                                  <option value="0">Pilih Desa</option>
                                </select>
                                <div class="overlay" id="overlay_id_desa" style="display:none;">
                                  <i class="fa fa-refresh fa-spin"></i>
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="id_dusun">Dusun</label>
                                <select class="form-control" id="id_dusun" name="id_dusun" >
                                  <option value="0">Pilih Dusun</option>
                                </select>
                                <div class="overlay" id="overlay_id_dusun" style="display:none;">
                                  <i class="fa fa-refresh fa-spin"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-8">
                          <div class="card">
                            <div class="card-header">
                              <h4 class="card-title">
                                Keterangan Umum
                              </h4>
                            </div>
                            <div class="card-body">
                            <div class="row">
                              <div class="col-lg-6">
                                <div class="form-group">
                                  <label for="rt">RT</label>
                                  <input class="form-control" id="rt" name="rt" value="" placeholder="RT" type="text">
                                </div>
                                <div class="form-group">
                                  <label for="rw">RW</label>
                                  <input class="form-control" id="rw" name="rw" value="" placeholder="RW" type="text">
                                </div>
                                <div class="form-group">
                                  <label for="dasawisma">Dasawisma</label>
                                  <input class="form-control" id="dasawisma" name="dasawisma" value="" placeholder="Dasawisma" type="text">
                                </div>
                                <div class="form-group">
                                  <label for="nik">NIK <span class="text-danger">*</span></label>
                                  <input class="form-control" id="nik" name="nik" value="" placeholder="NIK" type="text">
                                </div>
                              </div>
                              <div class="col-lg-6">
                                <div class="form-group">
                                  <label for="nama">Nama <span class="text-danger">*</span></label>
                                  <input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
                                </div>
                                <div class="form-group">
                                  <label for="nama_kepala_keluarga">Nama Kepala Keluarga</label>
                                  <input class="form-control" id="nama_kepala_keluarga" name="nama_kepala_keluarga" value="" placeholder="Nama Kepala Keluarga" type="text">
                                </div>
                                <div class="form-group">
                                  <label for="nomor_registrasi">No. Registrasi <span class="text-danger">*</span></label>
                                  <input class="form-control" id="nomor_registrasi" name="nomor_registrasi" value="" placeholder="No. Registrasi" type="text">
                                </div>
                                <div class="form-group">
                                  <label for="id_jabatan">Jabatan</label>
                                  <input class="form-control" id="id_jabatan" name="id_jabatan" value="" placeholder="Jabatan" type="text">
                                </div>
                              </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <div class="card">
                            <div class="card-header">
                              <div class="row">
                                <div class="form-group col-lg-6">
                                  <label for="kriteria_kader">Kriteria Kader <span class="text-danger">*</span></label>
                                  <select class="form-control" id="kriteria_kader" name="kriteria_kader" >
                                    <option value="">Pilih Kriteria</option>
                                    <option value="umum">Umum</option>
                                    <option value="khusus">Khusus</option>
                                  </select>
                                </div>
                                <div class="form-group col-lg-6">
                                  <label for="tanggal_masuk_kader">Tanggal Masuk Kader <span class="text-danger">*</span></label>
                                  <input class="form-control" id="tanggal_masuk_kader" name="tanggal_masuk_kader" value="" placeholder="Tanggal Masuk Kader" type="text">
                                </div>
                              </div>
                            </div>
                            <div class="card-body">
                              <div class="row">
                                <div class="col-3">
                                  <div class="form-group">
                                    <label for="kriteria_kader_pokja1">Pokja 1</label>
                                    <select class="form-control" id="kriteria_kader_pokja1" name="kriteria_kader_pokja1" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_pkbn">PKBN</label>
                                    <select class="form-control" id="kriteria_kader_pkbn" name="kriteria_kader_pkbn" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_kadarkum">Kadarkum</label>
                                    <select class="form-control" id="kriteria_kader_kadarkum" name="kriteria_kader_kadarkum" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_pola_asuh">Pola Asuh</label>
                                    <select class="form-control" id="kriteria_kader_pola_asuh" name="kriteria_kader_pola_asuh" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-3">
                                  <div class="form-group">
                                    <label for="kriteria_kader_pokja2">Pokja 2</label>
                                    <select class="form-control" id="kriteria_kader_pokja2" name="kriteria_kader_pokja2" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_tutor_kejar_paket">Tutor Kejar Pake A/B/C</label>
                                    <select class="form-control" id="kriteria_kader_tutor_kejar_paket" name="kriteria_kader_tutor_kejar_paket" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_tutor_kf">Tutor KF</label>
                                    <select class="form-control" id="kriteria_kader_tutor_kf" name="kriteria_kader_tutor_kf" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_tutor_paud">Tutor PAUD</label>
                                    <select class="form-control" id="kriteria_kader_tutor_paud" name="kriteria_kader_tutor_paud" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_bkb">BKB</label>
                                    <select class="form-control" id="kriteria_kader_bkb" name="kriteria_kader_bkb" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_koperasi">Koperasi</label>
                                    <select class="form-control" id="kriteria_kader_koperasi" name="kriteria_kader_koperasi" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_keterampilan">Keterampilan</label>
                                    <select class="form-control" id="kriteria_kader_keterampilan" name="kriteria_kader_keterampilan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_lp3_pkk">LP3 PKK</label>
                                    <select class="form-control" id="kriteria_kader_lp3_pkk" name="kriteria_kader_lp3_pkk" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_tp3_pkk">TP3 PKK</label>
                                    <select class="form-control" id="kriteria_kader_tp3_pkk" name="kriteria_kader_tp3_pkk" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_damas">Damas</label>
                                    <select class="form-control" id="kriteria_kader_damas" name="kriteria_kader_damas" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_mendongeng">Mendongeng</label>
                                    <select class="form-control" id="kriteria_kader_mendongeng" name="kriteria_kader_mendongeng" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-3">
                                  <div class="form-group">
                                    <label for="kriteria_kader_pokja3">Pokja 3</label>
                                    <select class="form-control" id="kriteria_kader_pokja3" name="kriteria_kader_pokja3" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_pangan">Pangan</label>
                                    <select class="form-control" id="kriteria_kader_pangan" name="kriteria_kader_pangan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_sandang">Sandang</label>
                                    <select class="form-control" id="kriteria_kader_sandang" name="kriteria_kader_sandang" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_tata_laksana_rt">Tata laksana RT</label>
                                    <select class="form-control" id="kriteria_kader_tata_laksana_rt" name="kriteria_kader_tata_laksana_rt" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-3">
                                  <div class="form-group">
                                    <label for="kriteria_kader_pokja4">Pokja 4</label>
                                    <select class="form-control" id="kriteria_kader_pokja4" name="kriteria_kader_pokja4" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_posyandu_balita">Posyandu Balita</label>
                                    <select class="form-control" id="kriteria_kader_posyandu_balita" name="kriteria_kader_posyandu_balita" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_posyandu_lansia">Posyandu Lansia</label>
                                    <select class="form-control" id="kriteria_kader_posyandu_lansia" name="kriteria_kader_posyandu_lansia" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_gizi">Gizi</label>
                                    <select class="form-control" id="kriteria_kader_gizi" name="kriteria_kader_gizi" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_kesling">Kesling</label>
                                    <select class="form-control" id="kriteria_kader_kesling" name="kriteria_kader_kesling" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_peyuluhan_narkoba">Peyuluhan Narkoba</label>
                                    <select class="form-control" id="kriteria_kader_peyuluhan_narkoba" name="kriteria_kader_peyuluhan_narkoba" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_kb">KB</label>
                                    <select class="form-control" id="kriteria_kader_kb" name="kriteria_kader_kb" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_phbs">PHBS</label>
                                    <select class="form-control" id="kriteria_kader_phbs" name="kriteria_kader_phbs" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kriteria_kader_tbc">TBC</label>
                                    <select class="form-control" id="kriteria_kader_tbc" name="kriteria_kader_tbc" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="card">
                            <div class="card-body">
                              <div class="row">
                                  <div class="form-group col-lg-4">
                                    <label for="jenis_kelamin">Jenis Kelamin</label>
                                    <select class="form-control" id="jenis_kelamin" name="jenis_kelamin" >
                                      <option value="">Pilih Jenis Kelamin</option>
                                      <option value="pria">Laki-laki</option>
                                      <option value="wanita">Perempuan</option>
                                    </select>
                                  </div>
                                  <div class="form-group col-lg-4">
                                    <label for="tempat_lahir">Tempat Lahir</label>
                                    <input class="form-control" id="tempat_lahir" name="tempat_lahir" value="" placeholder="Tempat Lahir" type="text">
                                  </div>
                                  <div class="form-group col-lg-4">
                                    <label for="tanggal_lahir">Tanggal Lahir</label>
                                    <input class="form-control" id="tanggal_lahir" name="tanggal_lahir" value="" placeholder="Tanggal Lahir" type="text">
                                  </div>
                                  <div class="form-group col-lg-4">
                                    <label for="pendidikan">Pendidikan</label>
                                    <select id="pendidikan" name="pendidikan" class="form-control">
                                      <option value="">Pilih Pendidikan</option>
                                    </select>
                                  </div>
                                  <div class="form-group col-lg-4">
                                    <label for="status_perkawinan">Status Perkawinan</label>
                                    <select id="status_perkawinan" name="status_perkawinan" class="form-control">
                                      <option value="">Pilih Status Perkawinan</option>
                                    </select>
                                  </div>
                                  <div class="form-group col-lg-4">
                                    <label for="status_dalam_keluarga">Status dalam Keluarga</label>
                                    <select name="status_dalam_keluarga" class="form-control">
                                      <option value="">Pilih Status dalam Keluarga</option>
                                    </select>
                                  </div>
                                  <div class="form-group col-lg-4">
                                    <label for="agama">Agama</label>
                                    <select id="agama" name="agama" class="form-control">
                                      <option value="">Pilih Agama</option>
                                    </select>
                                  </div>
                                  <div class="form-group col-lg-4">
                                    <label for="pekerjaan">Pekerjaan</label>
                                    <select id="pekerjaan" name="pekerjaan" class="form-control">
                                      <option value="">Pilih Pekerjaan</option>
                                    </select>
                                  </div>
                                  <div class="form-group col-lg-4">
                                    <label for="alamat_warga">Alamat</label>
                                    <input class="form-control" id="alamat_warga" name="alamat_warga" value="" placeholder="Alamat" type="text">
                                  </div>
                                  <div class="form-group col-lg-4" style="display:none;">
                                    <label for="id_kecamatan_warga">Kecamatan</label>
                                    <select class="form-control" id="id_kecamatan_warga" name="id_kecamatan_warga" >
                                    </select>
                                    <div class="overlay" id="overlay_id_kecamatan_warga" style="display:none;">
                                      <i class="fa fa-refresh fa-spin"></i>
                                    </div>
                                  </div>
                                  <div class="form-group col-lg-4" style="display:none;">
                                    <label for="id_desa_warga">Desa</label>
                                    <select class="form-control" id="id_desa_warga" name="id_desa_warga" >
                                      <option value="0">Pilih Desa</option>
                                    </select>
                                    <div class="overlay" id="overlay_id_desa_warga" style="display:none;">
                                      <i class="fa fa-refresh fa-spin"></i>
                                    </div>
                                  </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="card">
                            <div class="card-body">
                              <div class="row">
                                <div class="col-6">
                                  <div class="form-group">
                                    <label for="aseptor_kb">Aseptor KB</label>
                                    <select class="form-control" id="aseptor_kb" name="aseptor_kb" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="jenis_aseptor_kb">Jenis Aseptor KB</label>
                                    <select class="form-control" id="jenis_aseptor_kb" name="jenis_aseptor_kb" >
                                      <option value="">Pilih Jenis Aseptor KB</option>
                                      <option value="mop">MOP</option>
                                      <option value="mow">MOW</option>
                                      <option value="uid">UID</option>
                                      <option value="implant">Implant</option>
                                      <option value="pil">Pil</option>
                                      <option value="suntik">Suntik</option>
                                      <option value="kondom">Kondom</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="aktif_kegiatan_osyandu_balita">Aktif Kegiatan Posyandu Balita</label>
                                    <select class="form-control" id="aktif_kegiatan_osyandu_balita" name="aktif_kegiatan_osyandu_balita" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="memiliki_kartu_sehat">Memiliki Kartu Sehat</label>
                                    <select class="form-control" id="memiliki_kartu_sehat" name="memiliki_kartu_sehat" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="aktif_kegiatan_osyandu_lansia">Aktif Kegiatan Posyandu Lansia</label>
                                    <select class="form-control" id="aktif_kegiatan_osyandu_lansia" name="aktif_kegiatan_osyandu_lansia" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="mengikuti_program_bkb">Mengikuti Program BKB</label>
                                    <select class="form-control" id="mengikuti_program_bkb" name="mengikuti_program_bkb" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="memiliki_tabungan">Memiliki Tabungan</label>
                                    <select class="form-control" id="memiliki_tabungan" name="memiliki_tabungan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="jenis_tabungan">Jenis Tabungan</label>
                                    <select class="form-control" id="jenis_tabungan" name="jenis_tabungan" >
                                      <option value="">Pilih Jenis Tabungan</option>
                                      <option value="uang">Uang</option>
                                      <option value="tanaman">Tanaman</option>
                                      <option value="tanah">Tanah</option>
                                      <option value="hewan">Hewan</option>
                                      <option value="rumah">Rumah</option>
                                      <option value="Lainnya">Lainnya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="memiliki_kartu_berobat_gratis">Memiliki Kartu Berobat Gratis</label>
                                    <select class="form-control" id="memiliki_kartu_berobat_gratis" name="memiliki_kartu_berobat_gratis" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="memiliki_kelompok_belajar">Memiliki Kelompok Belajar</label>
                                    <select class="form-control" id="memiliki_kelompok_belajar" name="memiliki_kelompok_belajar" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="mengikuti_kegiatan_paud">Mengikuti Kegiatan PAUD</label>
                                    <select class="form-control" id="mengikuti_kegiatan_paud" name="mengikuti_kegiatan_paud" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="mengikuti_kegiatan_koperasi">Mengikuti Kegiatan Koperasi</label>
                                    <select class="form-control" id="mengikuti_kegiatan_koperasi" name="mengikuti_kegiatan_koperasi" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="aktifitas_up2k">Aktifitas UP2K</label>
                                    <select class="form-control" id="aktifitas_up2k" name="aktifitas_up2k" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="aktifitas_tpk_wanita">Aktifitas TPK Wanita</label>
                                    <select class="form-control" id="aktifitas_tpk_wanita" name="aktifitas_tpk_wanita" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="aktifitas_kegiatan_keshatan_lingkungan">Aktifitas Kegiatan Kesehatan Lingkungan</label>
                                    <select class="form-control" id="aktifitas_kegiatan_keshatan_lingkungan" name="aktifitas_kegiatan_keshatan_lingkungan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-6">
                                  <div class="form-group">
                                    <label for=""> Kegiatan Yang diikuti :</label>
                                  </div>
                                  <div class="form-group">
                                    <label for="penghayatan_dan_pengamalan_pancasila">Penghayatan dan Pengamalan Pancasila</label>
                                    <select class="form-control" id="penghayatan_dan_pengamalan_pancasila" name="penghayatan_dan_pengamalan_pancasila" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kegiatan_keagamaan">Kegiatan Keagamaan</label>
                                    <select class="form-control" id="kegiatan_keagamaan" name="kegiatan_keagamaan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="rukun_kematian">Rukun Kematian</label>
                                    <select class="form-control" id="rukun_kematian" name="rukun_kematian" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="arisan">Arisan</label>
                                    <select class="form-control" id="arisan" name="arisan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kerja_bakti">Kerja Bakti</label>
                                    <select class="form-control" id="kerja_bakti" name="kerja_bakti" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="jimpitan">Jimpitan</label>
                                    <select class="form-control" id="jimpitan" name="jimpitan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="jimpitan"> Mengikuti Penyuluhan :</label>
                                  </div>
                                  <div class="form-group">
                                    <label for="bidang_pangan">Bidang Pangan</label>
                                    <select class="form-control" id="bidang_pangan" name="bidang_pangan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="bidang_pangan">Bidang Sandang</label>
                                    <select class="form-control" id="bidang_pangan" name="bidang_pangan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="bidang_tata_laksana_rt">Bidang Tata Laksana RT</label>
                                    <select class="form-control" id="bidang_tata_laksana_rt" name="bidang_tata_laksana_rt" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="pkbn">PKBN</label>
                                    <select class="form-control" id="pkbn" name="pkbn" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kadarkum">Kadarkum</label>
                                    <select class="form-control" id="kadarkum" name="kadarkum" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="pola_asuh">Pola Asuh</label>
                                    <select class="form-control" id="pola_asuh" name="pola_asuh" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="mengikuti_pelatihan">Mengikuti Pelatihan</label>
                                    <select class="form-control" id="mengikuti_pelatihan" name="mengikuti_pelatihan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="nama_pelatihan">Nama Pelatihan</label>
                                    <select class="form-control" id="nama_pelatihan" name="nama_pelatihan" >
                                      <option value="">Pilih Nama Pelatihan</option>
                                      <option value="LP3PKK Bersertifikat">LP3PKK Bersertifikat</option>
                                      <option value="LP3PKK Tidak Bersertifikat">LP3PKK Tidak Bersertifikat</option>
                                      <option value="TPK3PKK Bersertifikat">TPK3PKK Bersertifikat</option>
                                      <option value="TP3KPKK Tidak Bersertifikat">TP3KPKK Tidak Bersertifikat</option>
                                      <option value="Damas Bersertifikat">Damas Bersertifikat</option>
                                      <option value="Damas Tidak Bersertifikat">Damas Tidak Bersertifikat</option>
                                      <option value="Mendongeng Bersertifikat">Mendongeng Bersertifikat</option>
                                      <option value="Mendongeng Tidak Bersertifikat">Mendongeng Tidak Bersertifikat</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="tanggal_pelatihan">Waktu Pelatihan</label>
                                    <input class="form-control" id="tanggal_pelatihan" name="tanggal_pelatihan" value="" placeholder="Tanggal Pelatihan" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="penyelenggara_pelatihan">Penyelenggara</label>
                                    <select class="form-control" id="penyelenggara_pelatihan" name="penyelenggara_pelatihan" >
                                      <option value="">Pilih Penyelenggara</option>
                                      <option value="PKK">PKK</option>
                                      <option value="Instansi Pemerintah">Instansi Pemerintah</option>
                                      <option value="Non Pemerintah">Non Pemerintah</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-12">
                          <div class="card">
                            <div class="card-header">
                              <label class="card-title">Kondisi Rumah Tinggal</label>
                            </div>
                            <div class="card-body">
                              <div class="row">
                                <div class="col-4">
                                  <div class="form-group">
                                    <label for="jumlah_kepala_keluarga">Jumlah Kepala Keluarga</label>
                                    <input class="form-control" id="jumlah_kepala_keluarga" name="jumlah_kepala_keluarga" value="" placeholder="Jumlah Kepala Keluarga" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="jumlah_anggota_keluarga">Jumlah Anggota Keluarga</label>
                                    <input class="form-control" id="jumlah_anggota_keluarga" name="jumlah_anggota_keluarga" value="" placeholder="Jumlah Kepala Keluarga" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="jumlah_pria">Jumlah Laki-laki</label>
                                    <input class="form-control" id="jumlah_pria" name="jumlah_pria" value="" placeholder="Jumlah Laki-laki" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="jumlah_wanita">Jumlah Perempuan</label>
                                    <input class="form-control" id="jumlah_wanita" name="jumlah_wanita" value="" placeholder="Jumlah Perempuan" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="jumlah_balita_pria">Jumlah Balita Laki Laki</label>
                                    <input class="form-control" id="jumlah_balita_pria" name="jumlah_balita_pria" value="" placeholder="Jumlah Balita Laki Laki" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="jumlah_balita_wanita">Jumlah Balita Perempuan</label>
                                    <input class="form-control" id="jumlah_balita_wanita" name="jumlah_balita_wanita" value="" placeholder="Jumlah  Balita Perempuan" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="jumlah_pus">Jumlah PUS</label>
                                    <input class="form-control" id="jumlah_pus" name="jumlah_pus" value="" placeholder="Jumlah PUS" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="jumlah_wus">Jumlah WUS</label>
                                    <input class="form-control" id="jumlah_wus" name="jumlah_wus" value="" placeholder="Jumlah WUS" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="tiga_buta_pria">3 Buta Laki Laki</label>
                                    <input class="form-control" id="tiga_buta_pria" name="tiga_buta_pria" value="" placeholder="3 Buta Laki Laki" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="tiga_buta_wanita">3 Buta Perempuan</label>
                                    <input class="form-control" id="tiga_buta_wanita" name="tiga_buta_wanita" value="" placeholder="3 Buta Perempuan" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="ibu_hamil">Ibu Hamil</label>
                                    <input class="form-control" id="ibu_hamil" name="ibu_hamil" value="" placeholder="Jumlah Ibu Hamil" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="ibu_menyusui">Ibu Menyusui</label>
                                    <input class="form-control" id="ibu_menyusui" name="ibu_menyusui" value="" placeholder="Jumlah Ibu Menyusui" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="jumlah_lansia">Jumlah Lansia</label>
                                    <input class="form-control" id="jumlah_lansia" name="jumlah_lansia" value="" placeholder="Jumlah Jumlah Lansia" type="text">
                                  </div>
                                </div>
                                <div class="col-4">
                                  <div class="form-group">
                                    <label for="makanan_pokok">Makanan Pokok</label>
                                    <select class="form-control" id="makanan_pokok" name="makanan_pokok" >
                                      <option value="">Pilih Makanan Pokok</option>
                                      <option value="Beras">Beras</option>
                                      <option value="Non Beras">Non Beras</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="memiliki_jamban">Memiliki Jamban</label>
                                    <select class="form-control" id="memiliki_jamban" name="memiliki_jamban" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="jumlah_jamban">Jumlah Jamban</label>
                                    <input class="form-control" id="jumlah_jamban" name="jumlah_jamban" value="" placeholder="Jumlah Jamban" type="text">
                                  </div>
                                  <div class="form-group">
                                    <label for="sumber_air">Sumber Air</label>
                                    <select class="form-control" id="sumber_air" name="sumber_air" >
                                      <option value="">Pilih Sumber Air</option>
                                      <option value="PDAM">PDAM</option>
                                      <option value="Sumur">Sumur</option>
                                      <option value="Mata Air">Mata Air</option>
                                      <option value="Sungai">Sungai</option>
                                      <option value="Lainnya">Lainnya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="memiliki_tempat_pembuangan_sampah">Memiliki Tmpt Pembuangan Sampah</label>
                                    <select class="form-control" id="memiliki_tempat_pembuangan_sampah" name="memiliki_tempat_pembuangan_sampah" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="memiliki_tempat_pembuangan_limbah">Memiliki Tempat Pembuangan Limbah</label>
                                    <select class="form-control" id="memiliki_tempat_pembuangan_limbah" name="memiliki_tempat_pembuangan_limbah" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="lantai_rumah">Memiliki Tempat Pembuangan Limbah</label>
                                    <select class="form-control" id="lantai_rumah" name="lantai_rumah" >
                                      <option value="">Pilih Memiliki Tempat Pembuangan Limbah</option>
                                      <option value="Tanah">Tanah</option>
                                      <option value="Plester">Plester</option>
                                      <option value="Keramik atau sejenisnya">Keramik/sejenisnya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="dinding_rumah">Dinding Rumah</label>
                                    <select class="form-control" id="dinding_rumah" name="dinding_rumah" >
                                      <option value="">Pilih Dinding Rumah</option>
                                      <option value="Kayu">Kayu</option>
                                      <option value="Semen/Batu">Semen/Batu</option>
                                      <option value="Semi Permanen">Semi Permanen</option>
                                      <option value="Gedhek">Gedhek</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="ventilasi_rumah">Ventilasi Rumah</label>
                                    <select class="form-control" id="ventilasi_rumah" name="ventilasi_rumah" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="memiliki_sekat_rumah">Memiliki Sekat Rumah</label>
                                    <select class="form-control" id="memiliki_sekat_rumah" name="memiliki_sekat_rumah" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="kandang_terpisah">Kandang Terpisah </label>
                                    <select class="form-control" id="kandang_terpisah" name="kandang_terpisah" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="menempel_stiker_p4k">Menempel Stiker P4K</label>
                                    <select class="form-control" id="menempel_stiker_p4k" name="menempel_stiker_p4k" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-4">
                                  <div class="form-group">
                                    <label for="kriteria_rumah_sehat">Kriteria Rumah Sehat</label>
                                    <select class="form-control" id="kriteria_rumah_sehat" name="kriteria_rumah_sehat" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for=""> Pemanfaatan Tanah Pekarangan :</label>
                                    <select class="form-control" id="pemanfaatan_tanah_pekarangan" name="pemanfaatan_tanah_pekarangan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="pemanfaatan_tanah_pekarangan_perikanan">Perikanan</label>
                                    <select class="form-control" id="pemanfaatan_tanah_pekarangan_perikanan" name="pemanfaatan_tanah_pekarangan_perikanan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="pemanfaatan_tanah_pekarangan_warung_hidup">Warung Hidup</label>
                                    <select class="form-control" id="pemanfaatan_tanah_pekarangan_warung_hidup" name="pemanfaatan_tanah_pekarangan_warung_hidup" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="pemanfaatan_tanah_pekarangan_tanaman_keras">Tanaman Keras</label>
                                    <select class="form-control" id="pemanfaatan_tanah_pekarangan_tanaman_keras" name="pemanfaatan_tanah_pekarangan_tanaman_keras" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="pemanfaatan_tanah_pekarangan_peternakan">Peternakan</label>
                                    <select class="form-control" id="pemanfaatan_tanah_pekarangan_peternakan" name="pemanfaatan_tanah_pekarangan_peternakan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="pemanfaatan_tanah_pekarangan_toga">Toga</label>
                                    <select class="form-control" id="pemanfaatan_tanah_pekarangan_toga" name="pemanfaatan_tanah_pekarangan_toga" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for=""> Industri Rumah Tangga:</label>
                                    <select class="form-control" id="industri_rumah_tangga" name="industri_rumah_tangga" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="industri_rumah_tangga_pangan">Pangan</label>
                                    <select class="form-control" id="industri_rumah_tangga_pangan" name="industri_rumah_tangga_pangan" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="industri_rumah_tangga_sandang">Sandang</label>
                                    <select class="form-control" id="industri_rumah_tangga_sandang" name="industri_rumah_tangga_sandang" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="industri_rumah_tangga_konveksi">Konveksi</label>
                                    <select class="form-control" id="industri_rumah_tangga_konveksi" name="industri_rumah_tangga_konveksi" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="industri_rumah_tangga_jasa">Jasa</label>
                                    <select class="form-control" id="industri_rumah_tangga_jasa" name="industri_rumah_tangga_jasa" >
                                      <option value="tidak">Tidak</option>
                                      <option value="ya">Ya</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
											<div class="form-group">
											<button type="submit" class="btn btn-primary" id="simpan_warga_pkk">SIMPAN</button>
											<button type="submit" class="btn btn-primary" id="update_warga_pkk" style="display:none;">UPDATE</button>
											</div>
										</form>

										<div class="overlay" id="overlay_form_input" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
									<div class="tab-pane table-responsive" id="tab_data_warga_pkk">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="999999999">Semua</option>
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="warga_pkk.nama">Nama</option>
															<option value="warga_pkk.tahun_pendaftaran">Tahun Pendaftaran</option>
															<option value="warga_pkk.nomor_registrasi">Nomor Registrasi</option>
															<option value="warga_pkk.tanggal_masuk_kader">Tanggal Masuk Kader</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_warga_pkk"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<tr>
														<th>NO</th>
														<th>Tahun</th>
														<th>Tanggal Masuk Kader</th>
														<th>Nama</th>
														<th>No. Registrasi</th>
														<th>Alamat</th> 
														<th>Proses</th> 
													</tr>
													<tbody id="tbl_data_warga_pkk">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_warga_pkk" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				

<script type="text/javascript">
$(document).ready(function() {
  load_status_dalam_keluarga();
});
</script>
<script>
  function load_status_dalam_keluarga() {
    $('#status_dalam_keluarga').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>warga_pkk/status_dalam_keluarga/',
      success: function(html) {
        $('#status_dalam_keluarga').html('<option value="">Pilih Status Dalam Keluarga</option>'+html+'');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  load_pekerjaan();
});
</script>
<script>
  function load_pekerjaan() {
    $('#pekerjaan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>warga_pkk/pekerjaan/',
      success: function(html) {
        $('#pekerjaan').html('<option value="">Pilih Agama</option>'+html+'');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  load_agama();
});
</script>
<script>
  function load_agama() {
    $('#agama').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>warga_pkk/agama/',
      success: function(html) {
        $('#agama').html('<option value="">Pilih Agama</option>'+html+'');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  load_status_pernikahan();
});
</script>
<script>
  function load_status_pernikahan() {
    $('#status_perkawinan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>warga_pkk/status_pernikahan/',
      success: function(html) {
        $('#status_perkawinan').html('<option value="">Pilih Status Perkawinan</option>'+html+'');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  load_pendidikan();
});
</script>
<script>
  function load_pendidikan() {
    $('#pendidikan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>warga_pkk/pendidikan/',
      success: function(html) {
        $('#pendidikan').html('<option value="">Pilih Pendidikan</option>'+html+'');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  load_opd_domain();
  load_opd_domain_warga();
});
</script>
<script>
  function load_opd_domain() {
    $('#id_kecamatan').html('');
    $('#overlay_id_kecamatan').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>warga_pkk/option_kecamatan/',
      success: function(html) {
        $('#id_kecamatan').html('<option value="0">Pilih Kecamatan</option>'+html+'');
        $('#overlay_id_kecamatan').fadeOut('slow');
      }
    });
  }
</script>
<script>
  function load_opd_domain_warga() {
    $('#id_kecamatan_warga').html('');
    $('#overlay_id_kecamatan_warga').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>warga_pkk/option_kecamatan/',
      success: function(html) {
        $('#id_kecamatan_warga').html('<option value="0">Pilih Kecamatan</option>'+html+'');
        $('#overlay_id_kecamatan_warga').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan').on('change', function(e) {
      e.preventDefault();
      var id_kecamatan = $('#id_kecamatan').val();
      load_option_desa(id_kecamatan);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan_warga').on('change', function(e) {
      e.preventDefault();
      var id_kecamatan = $('#id_kecamatan_warga').val();
      load_option_desa_warga(id_kecamatan);
    });
  });
</script>
<script>
  function load_option_desa(id_kecamatan) {
    $('#id_desa').html('');
    $('#overlay_id_desa').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>warga_pkk/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html('<option value="0">Pilih Desa</option>'+html+'');
        $('#overlay_id_desa').fadeOut('slow');
      }
    });
  }
</script>
<script>
  function load_option_desa_warga(id_kecamatan) {
    $('#id_desa_warga').html('');
    $('#overlay_id_desa_warga').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>warga_pkk/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa_warga').html('<option value="0">Pilih Desa</option>'+html+'');
        $('#overlay_id_desa_warga').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_desa').on('change', function(e) {
      e.preventDefault();
      var id_desa = $('#id_desa').val();
      load_option_dusun(id_desa);
    });
  });
</script>
<script>
  function load_option_dusun(id_desa) {
    $('#id_dusun').html('');
    $('#overlay_id_dusun').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_desa: id_desa
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>warga_pkk/option_dusun_by_id_desa/',
      success: function(html) {
        $('#id_dusun').html('<option value="0">Pilih Dusun</option>'+html+'');
        $('#overlay_id_dusun').fadeOut('slow');
      }
    });
  }
</script>
<script>
  function load_dusun_by_id_desa(id_dusun) {
    $('#id_dusun').html('');
    $('#id_desa').html('');
    $('#id_kecamatan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_dusun:id_dusun
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_dusun_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_dusun').html('<option value="' + json[i].id_dusun + '">' + json[i].nama_dusun + '</option>');
          $('#id_desa').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
					$('#id_propinsi').val(json[i].id_propinsi);
        }
      }
    });
  }
</script>
<script>
  function load_wilayah_by_id_desa(id_desa) {
    $('#id_desa_warga').html('');
    $('#id_kecamatan_warga').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_desa:id_desa
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_wilayah_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_desa_warga').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan_warga').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten_warga').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
					$('#id_propinsi_warga').val(json[i].id_propinsi);
        }
      }
    });
  }
</script>
<script>
  function load_dusun_by_id_desa_warga(id_dusun) {
    $('#id_dusun_warga').html('');
    $('#id_desa_warga').html('');
    $('#id_kecamatan_warga').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_dusun:id_dusun
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_dusun_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_dusun_warga').html('<option value="' + json[i].id_dusun + '">' + json[i].nama_dusun + '</option>');
          $('#id_desa_warga').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan_warga').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten_warga').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
					$('#id_propinsi_warga').val(json[i].id_propinsi);
        }
      }
    });
  }
</script>
<script>
  function load_dusun_by_id_desa_warga(id_dusun) {
    $('#id_dusun_warga').html('');
    $('#id_desa_warga').html('');
    $('#id_kecamatan_warga').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_dusun:id_dusun
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_dusun_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_dusun_warga').html('<option value="' + json[i].id_dusun + '">' + json[i].nama_dusun + '</option>');
          $('#id_desa_warga').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan_warga').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten_warga').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
					$('#id_propinsi_warga').val(json[i].id_propinsi);
        }
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>warga_pkk/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedWarga_pkk() {
    $('#id_warga_pkk, #temp, #id_dusun, #rt, #rw, #id_desa, #id_kecamatan, #id_kabupaten, #dasawisma, #id_jabatan, #tahun_pendaftaran, #kriteria_kader, #tanggal_masuk_kader, #kriteria_kader_pkbn, #kriteria_kader_kadarkum, #kriteria_kader_pola_asuh, #kriteria_kader_pokja2, #kriteria_kader_tutor_kejar_paket, #kriteria_kader_tutor_kf, #kriteria_kader_tutor_paud, #kriteria_kader_bkb, #kriteria_kader_koperasi, #kriteria_kader_keterampilan, #kriteria_kader_lp3_pkk, #kriteria_kader_tp3_pkk, #kriteria_kader_damas, #kriteria_kader_mendongeng, #kriteria_kader_pokja3, #kriteria_kader_pangan, #kriteria_kader_sandang, #kriteria_kader_tata_laksana_rt, #kriteria_kader_pokja4, #kriteria_kader_posyandu_balita, #kriteria_kader_posyandu_lansia, #kriteria_kader_gizi, #kriteria_kader_kesling, #kriteria_kader_peyuluhan_narkoba, #kriteria_kader_kb, #kriteria_kader_phbs, #kriteria_kader_tbc, #jenis_kelamin, #tempat_lahir, #tanggal_lahir, #status_perkawinan, #status_dalam_keluarga, #id_agama, #id_pendidikan, #id_pekerjaan, #alamat_warga, #id_desa_warga, #id_kecamatan_warga, #id_kabupaten_warga, #aseptor_kb, #jenis_aseptor_kb, #aktif_kegiatan_osyandu_balita, #memiliki_kartu_sehat, #aktif_kegiatan_osyandu_lansia, #mengikuti_program_bkb, #memiliki_tabungan, #jenis_tabungan, #memiliki_kartu_berobat_gratis, #memiliki_kelompok_belajar, #mengikuti_kegiatan_paud, #mengikuti_kegiatan_koperasi, #aktifitas_up2k, #aktifitas_tpk_wanita, #aktifitas_kegiatan_keshatan_lingkungan, #penghayatan_dan_pengamalan_pancasila, #kegiatan_keagamaan, #rukun_kematian, #arisan, #kerja_bakti, #jimpitan, #bidang_pangan, #bidang_sandang, #bidang_tata_laksana_rt, #pkbn, #kadarkum, #pola_asuh, #mengikuti_pelatihan, #nama_pelatihan, #tanggal_pelatihan, #penyelenggara_pelatihan, #jumlah_kepala_keluarga, #jumlah_anggota_keluarga, #jumlah_pria, #jumlah_wanita, #jumlah_balita_pria, #jumlah_balita_wanita, #jumlah_pus, #jumlah_wus, #tiga_buta_pria,  #tiga_buta_wanita, #ibu_hamil, #ibu_menyusui,  #jumlah_lansia, #makanan_pokok, #memiliki_jamban,  #jumlah_jamban, #sumber_air, #memiliki_tempat_pembuangan_sampah,  #memiliki_tempat_pembuangan_limbah, #lantai_rumah, #dinding_rumah,  #ventilasi_rumah, #memiliki_sekat_rumah, #kandang_terpisah,  #menempel_stiker_p4k, #kriteria_rumah_sehat, #pemanfaatan_tanah_pekarangan, #pemanfaatan_tanah_pekarangan_perikanan,  #pemanfaatan_tanah_pekarangan_warung_hidup, #pemanfaatan_tanah_pekarangan_tanaman_keras, #pemanfaatan_tanah_pekarangan_peternakan, #pemanfaatan_tanah_pekarangan_toga, #industri_rumah_tangga, #industri_rumah_tangga_pangan, #industri_rumah_tangga_sandang, #industri_rumah_tangga_konveksi, #industri_rumah_tangga_jasa, #nama, #nik, #nama_kepala_keluarga, #nomor_registrasi').val('');
    $('#tbl_attachment_warga_pkk').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_warga_pkk').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'tahun_pendaftaran', 'temp', 'nama', 'nik', 'dasawisma', 'nomor_registrasi' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["rw"] = $("#rw").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["dasawisma"] = $("#dasawisma").val();
      parameter["id_jabatan"] = $("#id_jabatan").val();
      parameter["nama_kepala_keluarga"] = $("#nama_kepala_keluarga").val();
      parameter["tahun_pendaftaran"] = $("#tahun_pendaftaran").val();
      parameter["kriteria_kader"] = $("#kriteria_kader").val();
      parameter["tanggal_masuk_kader"] = $("#tanggal_masuk_kader").val();
      parameter["kriteria_kader_pkbn"] = $("#kriteria_kader_pkbn").val();
      parameter["kriteria_kader_kadarkum"] = $("#kriteria_kader_kadarkum").val();
      parameter["kriteria_kader_pola_asuh"] = $("#kriteria_kader_pola_asuh").val();
      parameter["kriteria_kader_pokja2"] = $("#kriteria_kader_pokja2").val();
      parameter["kriteria_kader_tutor_kejar_paket"] = $("#kriteria_kader_tutor_kejar_paket").val();
      parameter["kriteria_kader_tutor_kf"] = $("#kriteria_kader_tutor_kf").val();
      parameter["kriteria_kader_tutor_paud"] = $("#kriteria_kader_tutor_paud").val();
      parameter["kriteria_kader_bkb"] = $("#kriteria_kader_bkb").val();
      parameter["kriteria_kader_koperasi"] = $("#kriteria_kader_koperasi").val();
      parameter["kriteria_kader_keterampilan"] = $("#kriteria_kader_keterampilan").val();
      parameter["kriteria_kader_lp3_pkk"] = $("#kriteria_kader_lp3_pkk").val();
      parameter["kriteria_kader_tp3_pkk"] = $("#kriteria_kader_tp3_pkk").val();
      parameter["kriteria_kader_damas"] = $("#kriteria_kader_damas").val();
      parameter["kriteria_kader_mendongeng"] = $("#kriteria_kader_mendongeng").val();
      parameter["kriteria_kader_pokja3"] = $("#kriteria_kader_pokja3").val();
      parameter["kriteria_kader_pangan"] = $("#kriteria_kader_pangan").val();
      parameter["kriteria_kader_sandang"] = $("#kriteria_kader_sandang").val();
      parameter["kriteria_kader_tata_laksana_rt"] = $("#kriteria_kader_tata_laksana_rt").val();
      parameter["kriteria_kader_pokja4"] = $("#kriteria_kader_pokja4").val();
      parameter["kriteria_kader_posyandu_balita"] = $("#kriteria_kader_posyandu_balita").val();
      parameter["kriteria_kader_posyandu_lansia"] = $("#kriteria_kader_posyandu_lansia").val();
      parameter["kriteria_kader_gizi"] = $("#kriteria_kader_gizi").val();
      parameter["kriteria_kader_kesling"] = $("#kriteria_kader_kesling").val();
      parameter["kriteria_kader_peyuluhan_narkoba"] = $("#kriteria_kader_peyuluhan_narkoba").val();
      parameter["kriteria_kader_kb"] = $("#kriteria_kader_kb").val();
      parameter["kriteria_kader_phbs"] = $("#kriteria_kader_phbs").val();
      parameter["kriteria_kader_tbc"] = $("#kriteria_kader_tbc").val();
      parameter["jenis_kelamin"] = $("#jenis_kelamin").val();
      parameter["tempat_lahir"] = $("#tempat_lahir").val();
      parameter["tanggal_lahir"] = $("#tanggal_lahir").val();
      parameter["status_perkawinan"] = $("#status_perkawinan").val();
      parameter["status_dalam_keluarga"] = $("#status_dalam_keluarga").val();
      parameter["id_agama"] = $("#id_agama").val();
      parameter["id_pendidikan"] = $("#id_pendidikan").val();
      parameter["id_pekerjaan"] = $("#id_pekerjaan").val();
      parameter["alamat_warga"] = $("#alamat_warga").val();
      parameter["id_desa_warga"] = $("#id_desa_warga").val();
      parameter["id_kecamatan_warga"] = $("#id_kecamatan_warga").val();
      parameter["id_kabupaten_warga"] = $("#id_kabupaten_warga").val();
      parameter["aseptor_kb"] = $("#aseptor_kb").val();
      parameter["jenis_aseptor_kb"] = $("#jenis_aseptor_kb").val();
      parameter["aktif_kegiatan_osyandu_balita"] = $("#aktif_kegiatan_osyandu_balita").val();
      parameter["memiliki_kartu_sehat"] = $("#memiliki_kartu_sehat").val();
      parameter["aktif_kegiatan_osyandu_lansia"] = $("#aktif_kegiatan_osyandu_lansia").val();
      parameter["mengikuti_program_bkb"] = $("#mengikuti_program_bkb").val();
      parameter["memiliki_tabungan"] = $("#memiliki_tabungan").val();
      parameter["jenis_tabungan"] = $("#jenis_tabungan").val();
      parameter["memiliki_kartu_berobat_gratis"] = $("#memiliki_kartu_berobat_gratis").val();
      parameter["memiliki_kelompok_belajar"] = $("#memiliki_kelompok_belajar").val();
      parameter["mengikuti_kegiatan_paud"] = $("#mengikuti_kegiatan_paud").val();
      parameter["mengikuti_kegiatan_koperasi"] = $("#mengikuti_kegiatan_koperasi").val();
      parameter["aktifitas_up2k"] = $("#aktifitas_up2k").val();
      parameter["aktifitas_tpk_wanita"] = $("#aktifitas_tpk_wanita").val();
      parameter["aktifitas_kegiatan_keshatan_lingkungan"] = $("#aktifitas_kegiatan_keshatan_lingkungan").val();
      parameter["penghayatan_dan_pengamalan_pancasila"] = $("#penghayatan_dan_pengamalan_pancasila").val();
      parameter["kegiatan_keagamaan"] = $("#kegiatan_keagamaan").val();
      parameter["rukun_kematian"] = $("#rukun_kematian").val();
      parameter["arisan"] = $("#arisan").val();
      parameter["kerja_bakti"] = $("#kerja_bakti").val();
      parameter["jimpitan"] = $("#jimpitan").val();
      parameter["bidang_pangan"] = $("#bidang_pangan").val();
      parameter["bidang_sandang"] = $("#bidang_sandang").val();
      parameter["bidang_tata_laksana_rt"] = $("#bidang_tata_laksana_rt").val();
      parameter["pkbn"] = $("#pkbn").val();
      parameter["kadarkum"] = $("#kadarkum").val();
      parameter["pola_asuh"] = $("#pola_asuh").val();
      parameter["mengikuti_pelatihan"] = $("#mengikuti_pelatihan").val();
      parameter["nama_pelatihan"] = $("#nama_pelatihan").val();
      parameter["tanggal_pelatihan"] = $("#tanggal_pelatihan").val();
      parameter["penyelenggara_pelatihan"] = $("#penyelenggara_pelatihan").val();
      parameter["jumlah_kepala_keluarga"] = $("#jumlah_kepala_keluarga").val();
      parameter["jumlah_anggota_keluarga"] = $("#jumlah_anggota_keluarga").val();
      parameter["jumlah_pria"] = $("#jumlah_pria").val();
      parameter["jumlah_wanita"] = $("#jumlah_wanita").val();
      parameter["jumlah_balita_pria"] = $("#jumlah_balita_pria").val();
      parameter["jumlah_balita_wanita"] = $("#jumlah_balita_wanita").val();
      parameter["jumlah_pus"] = $("#jumlah_pus").val();
      parameter["jumlah_wus"] = $("#jumlah_wus").val();
      parameter["tiga_buta_pria"] = $("#tiga_buta_pria").val();
      parameter["tiga_buta_wanita"] = $("#tiga_buta_wanita").val();
      parameter["ibu_hamil"] = $("#ibu_hamil").val();
      parameter["ibu_menyusui"] = $("#ibu_menyusui").val();
      parameter["jumlah_lansia"] = $("#jumlah_lansia").val();
      parameter["makanan_pokok"] = $("#makanan_pokok").val();
      parameter["memiliki_jamban"] = $("#memiliki_jamban").val();
      parameter["jumlah_jamban"] = $("#jumlah_jamban").val();
      parameter["sumber_air"] = $("#sumber_air").val();
      parameter["memiliki_tempat_pembuangan_sampah"] = $("#memiliki_tempat_pembuangan_sampah").val();
      parameter["memiliki_tempat_pembuangan_limbah"] = $("#memiliki_tempat_pembuangan_limbah").val();
      parameter["lantai_rumah"] = $("#lantai_rumah").val();
      parameter["dinding_rumah"] = $("#dinding_rumah").val();
      parameter["ventilasi_rumah"] = $("#ventilasi_rumah").val();
      parameter["memiliki_sekat_rumah"] = $("#memiliki_sekat_rumah").val();
      parameter["kandang_terpisah"] = $("#kandang_terpisah").val();
      parameter["menempel_stiker_p4k"] = $("#menempel_stiker_p4k").val();
      parameter["kriteria_rumah_sehat"] = $("#kriteria_rumah_sehat").val();
      parameter["pemanfaatan_tanah_pekarangan"] = $("#pemanfaatan_tanah_pekarangan").val();
      parameter["pemanfaatan_tanah_pekarangan_perikanan"] = $("#pemanfaatan_tanah_pekarangan_perikanan").val();
      parameter["pemanfaatan_tanah_pekarangan_warung_hidup"] = $("#pemanfaatan_tanah_pekarangan_warung_hidup").val();
      parameter["pemanfaatan_tanah_pekarangan_tanaman_keras"] = $("#pemanfaatan_tanah_pekarangan_tanaman_keras").val();
      parameter["pemanfaatan_tanah_pekarangan_peternakan"] = $("#pemanfaatan_tanah_pekarangan_peternakan").val();
      parameter["pemanfaatan_tanah_pekarangan_toga"] = $("#pemanfaatan_tanah_pekarangan_toga").val();
      parameter["industri_rumah_tangga"] = $("#industri_rumah_tangga").val();
      parameter["industri_rumah_tangga_pangan"] = $("#industri_rumah_tangga_pangan").val();
      parameter["industri_rumah_tangga_sandang"] = $("#industri_rumah_tangga_sandang").val();
      parameter["industri_rumah_tangga_konveksi"] = $("#industri_rumah_tangga_konveksi").val();
      parameter["industri_rumah_tangga_jasa"] = $("#industri_rumah_tangga_jasa").val();
      parameter["nama"] = $("#nama").val();
      parameter["nik"] = $("#nik").val();
      parameter["nomor_registrasi"] = $("#nomor_registrasi").val();
      var url = '<?php echo base_url(); ?>warga_pkk/simpan_warga_pkk';
      
      var parameterRv = [ 'tahun_pendaftaran', 'nama', 'temp', 'nik', 'dasawisma', 'nomor_registrasi' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedWarga_pkk();
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_warga_pkk').on('click', '.update_id_warga_pkk', function() {
    $('#mode').val('edit');
    $('#simpan_warga_pkk').hide();
    $('#update_warga_pkk').show();
    var id_warga_pkk = $(this).closest('tr').attr('id_warga_pkk');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_warga_pkk');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_warga_pkk').val(id_warga_pkk);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_warga_pkk:id_warga_pkk
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>warga_pkk/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#nama').val(json[i].nama);
            $('#nama_kepala_keluarga').val(json[i].nama_kepala_keluarga);
            $('#nomor_registrasi').val(json[i].nomor_registrasi);
            $('#id_dusun').val(json[i].id_dusun);
            $('#rt').val(json[i].rt);
            $('#rw').val(json[i].rw);
            $('#id_desa').val(json[i].id_desa);
            $('#id_kecamatan').val(json[i].id_kecamatan);
            $('#id_kabupaten').val(json[i].id_kabupaten);
            $('#dasawisma').val(json[i].dasawisma);
            $('#id_jabatan').val(json[i].id_jabatan);
            $('#tahun_pendaftaran').val(json[i].tahun_pendaftaran);
            $('#kriteria_kader').val(json[i].kriteria_kader);
            $('#tanggal_masuk_kader').val(json[i].tanggal_masuk_kader);
            $('#kriteria_kader_pkbn').val(json[i].kriteria_kader_pkbn);
            $('#kriteria_kader_kadarkum').val(json[i].kriteria_kader_kadarkum);
            $('#kriteria_kader_pola_asuh').val(json[i].kriteria_kader_pola_asuh);
            $('#kriteria_kader_pokja2').val(json[i].kriteria_kader_pokja2);
            $('#kriteria_kader_tutor_kejar_paket').val(json[i].kriteria_kader_tutor_kejar_paket);
            $('#kriteria_kader_tutor_kf').val(json[i].kriteria_kader_tutor_kf);
            $('#kriteria_kader_tutor_paud').val(json[i].kriteria_kader_tutor_paud);
            $('#kriteria_kader_bkb').val(json[i].kriteria_kader_bkb);
            $('#kriteria_kader_koperasi').val(json[i].kriteria_kader_koperasi);
            $('#kriteria_kader_keterampilan').val(json[i].kriteria_kader_keterampilan);
            $('#kriteria_kader_lp3_pkk').val(json[i].kriteria_kader_lp3_pkk);
            $('#kriteria_kader_tp3_pkk').val(json[i].kriteria_kader_tp3_pkk);
            $('#kriteria_kader_damas').val(json[i].kriteria_kader_damas);
            $('#kriteria_kader_mendongeng').val(json[i].kriteria_kader_mendongeng);
            $('#kriteria_kader_pokja3').val(json[i].kriteria_kader_pokja3);
            $('#kriteria_kader_pangan').val(json[i].kriteria_kader_pangan);
            $('#kriteria_kader_sandang').val(json[i].kriteria_kader_sandang);
            $('#kriteria_kader_tata_laksana_rt').val(json[i].kriteria_kader_tata_laksana_rt);
            $('#kriteria_kader_pokja4').val(json[i].kriteria_kader_pokja4);
            $('#kriteria_kader_posyandu_balita').val(json[i].kriteria_kader_posyandu_balita);
            $('#kriteria_kader_posyandu_lansia').val(json[i].kriteria_kader_posyandu_lansia);
            $('#kriteria_kader_gizi').val(json[i].kriteria_kader_gizi);
            $('#kriteria_kader_kesling').val(json[i].kriteria_kader_kesling);
            $('#kriteria_kader_peyuluhan_narkoba').val(json[i].kriteria_kader_peyuluhan_narkoba);
            $('#kriteria_kader_kb').val(json[i].kriteria_kader_kb);
            $('#kriteria_kader_phbs').val(json[i].kriteria_kader_phbs);
            $('#kriteria_kader_tbc').val(json[i].kriteria_kader_tbc);
            $('#jenis_kelamin').val(json[i].jenis_kelamin);
            $('#tempat_lahir').val(json[i].tempat_lahir);
            $('#tanggal_lahir').val(json[i].tanggal_lahir);
            $('#status_perkawinan').val(json[i].status_perkawinan);
            $('#status_dalam_keluarga').val(json[i].status_dalam_keluarga);
            $('#id_agama').val(json[i].id_agama);
            $('#id_pendidikan').val(json[i].id_pendidikan);
            $('#id_pekerjaan').val(json[i].id_pekerjaan);
            $('#alamat_warga').val(json[i].alamat_warga);
            $('#id_desa_warga').val(json[i].id_desa_warga);
            $('#id_kecamatan_warga').val(json[i].id_kecamatan_warga);
            $('#id_kabupaten_warga').val(json[i].id_kabupaten_warga);
            $('#aseptor_kb').val(json[i].aseptor_kb);
            $('#jenis_aseptor_kb').val(json[i].jenis_aseptor_kb);
            $('#aktif_kegiatan_osyandu_balita').val(json[i].aktif_kegiatan_osyandu_balita);
            $('#memiliki_kartu_sehat').val(json[i].memiliki_kartu_sehat);
            $('#aktif_kegiatan_osyandu_lansia').val(json[i].aktif_kegiatan_osyandu_lansia);
            $('#mengikuti_program_bkb').val(json[i].mengikuti_program_bkb);
            $('#memiliki_tabungan').val(json[i].memiliki_tabungan);
            $('#jenis_tabungan').val(json[i].jenis_tabungan);
            $('#memiliki_kartu_berobat_gratis').val(json[i].memiliki_kartu_berobat_gratis);
            $('#memiliki_kelompok_belajar').val(json[i].memiliki_kelompok_belajar);
            $('#mengikuti_kegiatan_paud').val(json[i].mengikuti_kegiatan_paud);
            $('#mengikuti_kegiatan_koperasi').val(json[i].mengikuti_kegiatan_koperasi);
            $('#aktifitas_up2k').val(json[i].aktifitas_up2k);
            $('#aktifitas_tpk_wanita').val(json[i].aktifitas_tpk_wanita);
            $('#aktifitas_kegiatan_keshatan_lingkungan').val(json[i].aktifitas_kegiatan_keshatan_lingkungan);
            $('#penghayatan_dan_pengamalan_pancasila').val(json[i].penghayatan_dan_pengamalan_pancasila);
            $('#kegiatan_keagamaan').val(json[i].kegiatan_keagamaan);
            $('#rukun_kematian').val(json[i].rukun_kematian);
            $('#arisan').val(json[i].arisan);
            $('#kerja_bakti').val(json[i].kerja_bakti);
            $('#jimpitan').val(json[i].jimpitan);
            $('#bidang_pangan').val(json[i].bidang_pangan);
            $('#bidang_sandang').val(json[i].bidang_sandang);
            $('#bidang_tata_laksana_rt').val(json[i].bidang_tata_laksana_rt);
            $('#pkbn').val(json[i].pkbn);
            $('#kadarkum').val(json[i].kadarkum);
            $('#pola_asuh').val(json[i].pola_asuh);
            $('#mengikuti_pelatihan').val(json[i].mengikuti_pelatihan);
            $('#nama_pelatihan').val(json[i].nama_pelatihan);
            $('#tanggal_pelatihan').val(json[i].tanggal_pelatihan);
            $('#penyelenggara_pelatihan').val(json[i].penyelenggara_pelatihan);
            $('#jumlah_kepala_keluarga').val(json[i].jumlah_kepala_keluarga);
            $('#jumlah_anggota_keluarga').val(json[i].jumlah_anggota_keluarga);
            $('#jumlah_pria').val(json[i].jumlah_pria);
            $('#jumlah_wanita').val(json[i].jumlah_wanita);
            $('#jumlah_balita_pria').val(json[i].jumlah_balita_pria);
            $('#jumlah_balita_wanita').val(json[i].jumlah_balita_wanita);
            $('#jumlah_pus').val(json[i].jumlah_pus);
            $('#jumlah_wus').val(json[i].jumlah_wus);
            $('#tiga_buta_pria').val(json[i].tiga_buta_pria);
            $('#tiga_buta_wanita').val(json[i].tiga_buta_wanita);
            $('#ibu_hamil').val(json[i].ibu_hamil);
            $('#ibu_menyusui').val(json[i].ibu_menyusui);
            $('#jumlah_lansia').val(json[i].jumlah_lansia);
            $('#makanan_pokok').val(json[i].makanan_pokok);
            $('#memiliki_jamban').val(json[i].memiliki_jamban);
            $('#jumlah_jamban').val(json[i].jumlah_jamban);
            $('#sumber_air').val(json[i].sumber_air);
            $('#memiliki_tempat_pembuangan_sampah').val(json[i].memiliki_tempat_pembuangan_sampah);
            $('#memiliki_tempat_pembuangan_limbah').val(json[i].memiliki_tempat_pembuangan_limbah);
            $('#lantai_rumah').val(json[i].lantai_rumah);
            $('#dinding_rumah').val(json[i].dinding_rumah);
            $('#ventilasi_rumah').val(json[i].ventilasi_rumah);
            $('#memiliki_sekat_rumah').val(json[i].memiliki_sekat_rumah);
            $('#kandang_terpisah').val(json[i].kandang_terpisah);
            $('#menempel_stiker_p4k').val(json[i].menempel_stiker_p4k);
            $('#kriteria_rumah_sehat').val(json[i].kriteria_rumah_sehat);
            $('#pemanfaatan_tanah_pekarangan').val(json[i].pemanfaatan_tanah_pekarangan);
            $('#pemanfaatan_tanah_pekarangan_perikanan').val(json[i].pemanfaatan_tanah_pekarangan_perikanan);
            $('#pemanfaatan_tanah_pekarangan_warung_hidup').val(json[i].pemanfaatan_tanah_pekarangan_warung_hidup);
            $('#pemanfaatan_tanah_pekarangan_tanaman_keras').val(json[i].pemanfaatan_tanah_pekarangan_tanaman_keras);
            $('#pemanfaatan_tanah_pekarangan_peternakan').val(json[i].pemanfaatan_tanah_pekarangan_peternakan);
            $('#pemanfaatan_tanah_pekarangan_toga').val(json[i].pemanfaatan_tanah_pekarangan_toga);
            $('#industri_rumah_tangga').val(json[i].industri_rumah_tangga);
            $('#industri_rumah_tangga_pangan').val(json[i].industri_rumah_tangga_pangan);
            $('#industri_rumah_tangga_sandang').val(json[i].industri_rumah_tangga_sandang);
            $('#industri_rumah_tangga_konveksi').val(json[i].industri_rumah_tangga_konveksi);
            $('#industri_rumah_tangga_jasa').val(json[i].industri_rumah_tangga_jasa);
            $('#nik').val(json[i].nik);
						load_dusun_by_id_desa(json[i].id_dusun);
						load_wilayah_by_id_desa(json[i].id_desa);
          }
        }
      });
    //AttachmentByMode(mode, value);
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_warga_pkk').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'tahun_pendaftaran', 'nama', 'temp', 'nik', 'dasawisma', 'nomor_registrasi' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_warga_pkk"] = $("#id_warga_pkk").val();
      parameter["temp"] = $("#temp").val();
      parameter["nama"] = $("#nama").val();
      parameter["nik"] = $("#nik").val();
      parameter["nama_kepala_keluarga"] = $("#nama_kepala_keluarga").val();
      parameter["nomor_registrasi"] = $("#nomor_registrasi").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["rw"] = $("#rw").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["dasawisma"] = $("#dasawisma").val();
      parameter["id_jabatan"] = $("#id_jabatan").val();
      parameter["tahun_pendaftaran"] = $("#tahun_pendaftaran").val();
      parameter["kriteria_kader"] = $("#kriteria_kader").val();
      parameter["tanggal_masuk_kader"] = $("#tanggal_masuk_kader").val();
      parameter["kriteria_kader_pkbn"] = $("#kriteria_kader_pkbn").val();
      parameter["kriteria_kader_kadarkum"] = $("#kriteria_kader_kadarkum").val();
      parameter["kriteria_kader_pola_asuh"] = $("#kriteria_kader_pola_asuh").val();
      parameter["kriteria_kader_pokja2"] = $("#kriteria_kader_pokja2").val();
      parameter["kriteria_kader_tutor_kejar_paket"] = $("#kriteria_kader_tutor_kejar_paket").val();
      parameter["kriteria_kader_tutor_kf"] = $("#kriteria_kader_tutor_kf").val();
      parameter["kriteria_kader_tutor_paud"] = $("#kriteria_kader_tutor_paud").val();
      parameter["kriteria_kader_bkb"] = $("#kriteria_kader_bkb").val();
      parameter["kriteria_kader_koperasi"] = $("#kriteria_kader_koperasi").val();
      parameter["kriteria_kader_keterampilan"] = $("#kriteria_kader_keterampilan").val();
      parameter["kriteria_kader_lp3_pkk"] = $("#kriteria_kader_lp3_pkk").val();
      parameter["kriteria_kader_tp3_pkk"] = $("#kriteria_kader_tp3_pkk").val();
      parameter["kriteria_kader_damas"] = $("#kriteria_kader_damas").val();
      parameter["kriteria_kader_mendongeng"] = $("#kriteria_kader_mendongeng").val();
      parameter["kriteria_kader_pokja3"] = $("#kriteria_kader_pokja3").val();
      parameter["kriteria_kader_pangan"] = $("#kriteria_kader_pangan").val();
      parameter["kriteria_kader_sandang"] = $("#kriteria_kader_sandang").val();
      parameter["kriteria_kader_tata_laksana_rt"] = $("#kriteria_kader_tata_laksana_rt").val();
      parameter["kriteria_kader_pokja4"] = $("#kriteria_kader_pokja4").val();
      parameter["kriteria_kader_posyandu_balita"] = $("#kriteria_kader_posyandu_balita").val();
      parameter["kriteria_kader_posyandu_lansia"] = $("#kriteria_kader_posyandu_lansia").val();
      parameter["kriteria_kader_gizi"] = $("#kriteria_kader_gizi").val();
      parameter["kriteria_kader_kesling"] = $("#kriteria_kader_kesling").val();
      parameter["kriteria_kader_peyuluhan_narkoba"] = $("#kriteria_kader_peyuluhan_narkoba").val();
      parameter["kriteria_kader_kb"] = $("#kriteria_kader_kb").val();
      parameter["kriteria_kader_phbs"] = $("#kriteria_kader_phbs").val();
      parameter["kriteria_kader_tbc"] = $("#kriteria_kader_tbc").val();
      parameter["jenis_kelamin"] = $("#jenis_kelamin").val();
      parameter["tempat_lahir"] = $("#tempat_lahir").val();
      parameter["tanggal_lahir"] = $("#tanggal_lahir").val();
      parameter["status_perkawinan"] = $("#status_perkawinan").val();
      parameter["status_dalam_keluarga"] = $("#status_dalam_keluarga").val();
      parameter["id_agama"] = $("#id_agama").val();
      parameter["id_pendidikan"] = $("#id_pendidikan").val();
      parameter["id_pekerjaan"] = $("#id_pekerjaan").val();
      parameter["alamat_warga"] = $("#alamat_warga").val();
      parameter["id_desa_warga"] = $("#id_desa_warga").val();
      parameter["id_kecamatan_warga"] = $("#id_kecamatan_warga").val();
      parameter["id_kabupaten_warga"] = $("#id_kabupaten_warga").val();
      parameter["aseptor_kb"] = $("#aseptor_kb").val();
      parameter["jenis_aseptor_kb"] = $("#jenis_aseptor_kb").val();
      parameter["aktif_kegiatan_osyandu_balita"] = $("#aktif_kegiatan_osyandu_balita").val();
      parameter["memiliki_kartu_sehat"] = $("#memiliki_kartu_sehat").val();
      parameter["aktif_kegiatan_osyandu_lansia"] = $("#aktif_kegiatan_osyandu_lansia").val();
      parameter["mengikuti_program_bkb"] = $("#mengikuti_program_bkb").val();
      parameter["memiliki_tabungan"] = $("#memiliki_tabungan").val();
      parameter["jenis_tabungan"] = $("#jenis_tabungan").val();
      parameter["memiliki_kartu_berobat_gratis"] = $("#memiliki_kartu_berobat_gratis").val();
      parameter["memiliki_kelompok_belajar"] = $("#memiliki_kelompok_belajar").val();
      parameter["mengikuti_kegiatan_paud"] = $("#mengikuti_kegiatan_paud").val();
      parameter["mengikuti_kegiatan_koperasi"] = $("#mengikuti_kegiatan_koperasi").val();
      parameter["aktifitas_up2k"] = $("#aktifitas_up2k").val();
      parameter["aktifitas_tpk_wanita"] = $("#aktifitas_tpk_wanita").val();
      parameter["aktifitas_kegiatan_keshatan_lingkungan"] = $("#aktifitas_kegiatan_keshatan_lingkungan").val();
      parameter["penghayatan_dan_pengamalan_pancasila"] = $("#penghayatan_dan_pengamalan_pancasila").val();
      parameter["kegiatan_keagamaan"] = $("#kegiatan_keagamaan").val();
      parameter["rukun_kematian"] = $("#rukun_kematian").val();
      parameter["arisan"] = $("#arisan").val();
      parameter["kerja_bakti"] = $("#kerja_bakti").val();
      parameter["jimpitan"] = $("#jimpitan").val();
      parameter["bidang_pangan"] = $("#bidang_pangan").val();
      parameter["bidang_sandang"] = $("#bidang_sandang").val();
      parameter["bidang_tata_laksana_rt"] = $("#bidang_tata_laksana_rt").val();
      parameter["pkbn"] = $("#pkbn").val();
      parameter["kadarkum"] = $("#kadarkum").val();
      parameter["pola_asuh"] = $("#pola_asuh").val();
      parameter["mengikuti_pelatihan"] = $("#mengikuti_pelatihan").val();
      parameter["nama_pelatihan"] = $("#nama_pelatihan").val();
      parameter["tanggal_pelatihan"] = $("#tanggal_pelatihan").val();
      parameter["penyelenggara_pelatihan"] = $("#penyelenggara_pelatihan").val();
      parameter["jumlah_kepala_keluarga"] = $("#jumlah_kepala_keluarga").val();
      parameter["jumlah_anggota_keluarga"] = $("#jumlah_anggota_keluarga").val();
      parameter["jumlah_pria"] = $("#jumlah_pria").val();
      parameter["jumlah_wanita"] = $("#jumlah_wanita").val();
      parameter["jumlah_balita_pria"] = $("#jumlah_balita_pria").val();
      parameter["jumlah_balita_wanita"] = $("#jumlah_balita_wanita").val();
      parameter["jumlah_pus"] = $("#jumlah_pus").val();
      parameter["jumlah_wus"] = $("#jumlah_wus").val();
      parameter["tiga_buta_pria"] = $("#tiga_buta_pria").val();
      parameter["tiga_buta_wanita"] = $("#tiga_buta_wanita").val();
      parameter["ibu_hamil"] = $("#ibu_hamil").val();
      parameter["ibu_menyusui"] = $("#ibu_menyusui").val();
      parameter["jumlah_lansia"] = $("#jumlah_lansia").val();
      parameter["makanan_pokok"] = $("#makanan_pokok").val();
      parameter["memiliki_jamban"] = $("#memiliki_jamban").val();
      parameter["jumlah_jamban"] = $("#jumlah_jamban").val();
      parameter["sumber_air"] = $("#sumber_air").val();
      parameter["memiliki_tempat_pembuangan_sampah"] = $("#memiliki_tempat_pembuangan_sampah").val();
      parameter["memiliki_tempat_pembuangan_limbah"] = $("#memiliki_tempat_pembuangan_limbah").val();
      parameter["lantai_rumah"] = $("#lantai_rumah").val();
      parameter["dinding_rumah"] = $("#dinding_rumah").val();
      parameter["ventilasi_rumah"] = $("#ventilasi_rumah").val();
      parameter["memiliki_sekat_rumah"] = $("#memiliki_sekat_rumah").val();
      parameter["kandang_terpisah"] = $("#kandang_terpisah").val();
      parameter["menempel_stiker_p4k"] = $("#menempel_stiker_p4k").val();
      parameter["kriteria_rumah_sehat"] = $("#kriteria_rumah_sehat").val();
      parameter["pemanfaatan_tanah_pekarangan"] = $("#pemanfaatan_tanah_pekarangan").val();
      parameter["pemanfaatan_tanah_pekarangan_perikanan"] = $("#pemanfaatan_tanah_pekarangan_perikanan").val();
      parameter["pemanfaatan_tanah_pekarangan_warung_hidup"] = $("#pemanfaatan_tanah_pekarangan_warung_hidup").val();
      parameter["pemanfaatan_tanah_pekarangan_tanaman_keras"] = $("#pemanfaatan_tanah_pekarangan_tanaman_keras").val();
      parameter["pemanfaatan_tanah_pekarangan_peternakan"] = $("#pemanfaatan_tanah_pekarangan_peternakan").val();
      parameter["pemanfaatan_tanah_pekarangan_toga"] = $("#pemanfaatan_tanah_pekarangan_toga").val();
      parameter["industri_rumah_tangga"] = $("#industri_rumah_tangga").val();
      parameter["industri_rumah_tangga_pangan"] = $("#industri_rumah_tangga_pangan").val();
      parameter["industri_rumah_tangga_sandang"] = $("#industri_rumah_tangga_sandang").val();
      parameter["industri_rumah_tangga_konveksi"] = $("#industri_rumah_tangga_konveksi").val();
      parameter["industri_rumah_tangga_jasa"] = $("#industri_rumah_tangga_jasa").val();
      var url = '<?php echo base_url(); ?>warga_pkk/update_warga_pkk';
      
      var parameterRv = [ 'tahun_pendaftaran', 'nama', 'nik', 'dasawisma', 'nomor_registrasi' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_warga_pkk').on('click', '#del_ajax_warga_pkk', function() {
    var id_warga_pkk = $(this).closest('tr').attr('id_warga_pkk');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_warga_pkk"] = id_warga_pkk;
        var url = '<?php echo base_url(); ?>warga_pkk/hapus/';
        HapusData(parameter, url);
        $('[id_warga_pkk='+id_warga_pkk+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
});
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_lahir, #tanggal_masuk_kader, #tanggal_pelatihan').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>