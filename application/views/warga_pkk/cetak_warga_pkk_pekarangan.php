
                  <div class="col-md-12">
                    <center>
                      <h1>Data Pemanfaatan Tanah Pekarangan Warga dan Industri Rumah Tangga <br />TP-<?php
                          $web=$this->uut->namadomain(base_url());
                          if($web=='demoopd.wonosobokab.go.id'){
                            if(!empty($keterangan)){ echo $keterangan; }
                          }elseif($web=='pkk.wonosobokab.go.id'){
                            if(!empty($keterangan)){ echo $keterangan; }
                          }else{
                            if(!empty($keterangan)){ echo 'PKK '.$keterangan.''; }
                          } ?>
                      </h1>
                    </center>
                    <div class="box">
											<div class="box-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<tr>
														<th>No.</th>
														<th>Dasa Wisma</th>
														<th>RT</th> 
														<th>RW</th> 
														<th>Dusun</th> 
														<th>Desa</th> 
														<th>Kecamatan</th> 
														<th>Tanggal Masuk Kader</th> 
													</tr>
													<tbody>
                          <?php
                          $table = 'warga_pkk';
                          $page    = $this->input->get('page');
                          $limit    = $this->input->get('limit');
                          $keyword    = $this->input->get('keyword');
                          $order_by    = $this->input->get('orderby');
                          $start      = ($page - 1) * $limit;
                          $fields     = "
                          *,
                          ( select (dusun.nama_dusun) from dusun where dusun.id_dusun=warga_pkk.id_dusun limit 1) as nama_dusun,
                          ( select (desa.nama_desa) from desa where desa.id_desa=warga_pkk.id_desa limit 1) as nama_desa,
                          ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=warga_pkk.id_kecamatan limit 1) as nama_kecamatan
                          ";
                          if($web=='demoopd.wonosobokab.go.id'){
                            $where = array(
                              'warga_pkk.status !=' => 99
                              );
                          }elseif($web=='pkk.wonosobokab.go.id'){
                            $where = array(
                              'warga_pkk.status !=' => 99
                              );
                          }else{
                            $where = array(
                              'warga_pkk.status !=' => 99,
                              'warga_pkk.id_kecamatan' => $this->session->userdata('id_kecamatan')
                              );
                          }
                          $orderby   = ''.$order_by.'';
                          $data_warga_pkk = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
                          $urut=$start;
                          foreach ($data_warga_pkk->result() as $row){
                            $urut=$urut+1;
                            if($row->pemanfaatan_tanah_pekarangan=='ya'){
                              echo'
                              <tr>
                                <td>'.($urut).'</td>
                                <td>'.$row->dasawisma.'</td>
                                <td>'.$row->rt.'</td>
                                <td>'.$row->rw.'</td>
                                <td>'.$row->nama_dusun.'</td>
                                <td>'.$row->nama_desa.'</td>
                                <td>'.$row->nama_kecamatan.'</td>
                                <td>'.$row->tanggal_masuk_kader.'</td>
                                <td>';
                                  if($row->pemanfaatan_tanah_pekarangan_perikanan=='ya'){echo 'Perikanan<br />';}
                                  if($row->pemanfaatan_tanah_pekarangan_warung_hidup=='ya'){echo 'Warung Hidup<br />';}
                                  if($row->pemanfaatan_tanah_pekarangan_tanaman_keras=='ya'){echo 'Tanaman Keras<br />';}
                                  if($row->pemanfaatan_tanah_pekarangan_peternakan=='ya'){echo 'Peternakan<br />';}
                                  if($row->pemanfaatan_tanah_pekarangan_toga=='ya'){echo 'Pekarangan Toga<br />';}
                                echo'</td>
                              </tr>
                              ';
                            }else{}
                            if($row->industri_rumah_tangga=='ya'){
                              echo'
                              <tr>
                                <td>'.($urut).'</td>
                                <td>'.$row->dasawisma.'</td>
                                <td>'.$row->rt.'</td>
                                <td>'.$row->rw.'</td>
                                <td>'.$row->nama_dusun.'</td>
                                <td>'.$row->nama_desa.'</td>
                                <td>'.$row->nama_kecamatan.'</td>
                                <td>'.$row->tanggal_masuk_kader.'</td>
                                <td>';
                                  if($row->industri_rumah_tangga_pangan=='ya'){echo 'Pangan<br />';}
                                  if($row->industri_rumah_tangga_sandang=='ya'){echo 'Sandang<br />';}
                                  if($row->industri_rumah_tangga_konveksi=='ya'){echo 'Konveksi<br />';}
                                  if($row->industri_rumah_tangga_jasa=='ya'){echo 'Jasa<br />';}
                                echo'</td>
                              </tr>
                              ';
                            }else{}
                          }
                          ?>
													</tbody>
												</table>
											</div>
                    </div>
                  </div>