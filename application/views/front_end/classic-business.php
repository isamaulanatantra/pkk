<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Title -->
  <title><?php if(!empty( $title )){ echo $title; } ?></title>
  <!-- Meta -->
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" />
  <meta name="author" content="<?php if(!empty( $title )){ echo $title; } ?>" />
  <meta name="keywords" content="<?php if(!empty( $title )){ echo $title; } ?> <?php echo base_url(); ?>" />
  <meta name="og:description" content="<?php if(!empty( $title )){ echo $title; } ?>" />
  <meta name="og:url" content="<?php echo base_url(); ?> <?php if(!empty( $title )){ echo $title; } ?>" />
  <meta name="og:title" content="<?php if(!empty( $title )){ echo $title; } ?> <?php echo base_url(); ?>" />
  <meta name="og:keywords" content="<?php if(!empty( $title )){ echo $title; } ?> <?php echo base_url(); ?>" />
  <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
  <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />

  <!-- Google Fonts -->
  <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/animate.css/animate.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/fancybox/jquery.fancybox.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/slick-carousel/slick/slick.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/cubeportfolio/css/cubeportfolio.min.css">

  <!-- CSS Front Template -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/theme.css">
</head>
<body>
  <!-- ========== HEADER ========== -->
    <?php $this->load->view('front_end/classic-business/header.php'); ?>
  <!-- ========== END HEADER ========== -->

  <!-- ========== MAIN ========== -->
  <main id="content" role="main">
    <!-- Hero v1 Section -->
    <div class="u-hero-v1">
      <!-- Hero Carousel Main -->
      <div id="heroNav" class="js-slick-carousel u-slick"
           data-autoplay="true"
           data-speed="10000"
           data-adaptive-height="true"
           data-arrows-classes="d-none d-lg-inline-block u-slick__arrow-classic u-slick__arrow-centered--y rounded-circle"
           data-arrow-left-classes="fas fa-arrow-left u-slick__arrow-classic-inner u-slick__arrow-classic-inner--left ml-lg-2 ml-xl-4"
           data-arrow-right-classes="fas fa-arrow-right u-slick__arrow-classic-inner u-slick__arrow-classic-inner--right mr-lg-2 mr-xl-4"
           data-numbered-pagination="#slickPaging"
           data-nav-for="#heroNavThumb">
          <?php
          $where = array(
            'status' => 1,
            'domain' => $web
            );
          $this->db->where($where);
          $this->db->limit(2);
          $this->db->order_by('created_time desc');
          $query1 = $this->db->get('slide_website');
          $a = 0;
          if($query1->num_rows()==0){
          }else{
            foreach ($query1->result() as $row1){
              echo
              '
        <div class="js-slide">
          <!-- Slide #1 -->
          <div class="d-lg-flex align-items-lg-center u-hero-v1__main" style="background-image: url('.base_url().'media/upload/'.$row1->file_name.');">
            <div class="container space-3 space-top-md-5 space-top-lg-3">
              <div class="row">
                <div class="col-md-8 col-lg-6 position-relative">
                  <span class="d-block h4 text-white font-weight-light mb-2"
                        data-scs-animation-in="fadeInUp"><a href="'.$row1->url_redirection.'">'.$row1->keterangan.'</a>
                  </span>
                  <h1 class="text-white display-4 font-size-md-down-5 mb-0"
                      data-scs-animation-in="fadeInUp"
                      data-scs-animation-delay="200">
                    <span class="font-weight-semi-bold"></span>
                  </h1>
                </div>
              </div>
            </div>
          </div>
          <!-- End Slide #1 -->
        </div>
              ';
            }
          }
          ?>
      </div>
      <!-- End Hero Carousel Main -->

      <!-- Slick Paging -->
      <div class="container position-relative">
        <div id="slickPaging" class="u-slick__paging"></div>
      </div>
      <!-- End Slick Paging -->

      <!-- Hero Carousel Secondary -->
      <div id="heroNavThumb" class="js-slick-carousel u-slick"
           data-autoplay="true"
           data-speed="10000"
           data-is-thumbs="true"
           data-nav-for="#heroNav">
        <div class="js-slide">
          <!-- Slide #1 -->
          <div class="d-flex align-items-center bg-white u-hero-v1__secondary">
      <div class="container space-2 space-top-md-5 space-bottom-md-3 space-top-lg-3">
        <div class="row">
          <div class="col-lg-8">
            <h3 class="h6">Join Front for free now!</h3>

            <!-- Form -->
            <form class="js-validate">
              <div class="form-row">
                <div class="col-6 col-sm-4 mb-2">
                  <div class="js-form-message">
                    <label class="sr-only" for="signupSrName">Your name</label>
                    <div class="input-group">
                      <input type="text" class="form-control" name="name" id="signupSrName" placeholder="Your name" aria-label="Your name" required
                             data-msg="Name must contain only letters."
                             data-error-class="u-has-error"
                             data-success-class="u-has-success">
                    </div>
                  </div>
                </div>

                <div class="col-6 col-sm-4 mb-2">
                  <div class="js-form-message">
                    <label class="sr-only" for="signupSrEmail">Your email</label>
                    <div class="input-group">
                      <input type="email" class="form-control" name="email" id="signupSrEmail" placeholder="Your email" aria-label="Your email" required
                             data-msg="Please enter a valid email address."
                             data-error-class="u-has-error"
                             data-success-class="u-has-success">
                    </div>
                  </div>
                </div>

                <div class="col-sm-4 mb-2">
                  <button type="submit" class="btn btn-primary btn-wide transition-3d-hover">Get Started</button>
                </div>
              </div>

              <!-- Checkbox -->
              <div class="js-form-message">
                <div class="custom-control custom-checkbox d-flex align-items-center text-muted">
                  <input type="checkbox" class="custom-control-input" id="termsCheckbox" name="termsCheckbox" required
                         data-msg="Please accept our Terms and Conditions.">
                  <label class="custom-control-label" for="termsCheckbox">
                    <small>
                      I agree to the
                      <a class="link-muted" href="../pages/terms.html">Terms and Conditions</a>
                    </small>
                  </label>
                </div>
              </div>
              <!-- End Checkbox -->
            </form>
            <!-- End Form -->
          </div>
        </div>
      </div>
          </div>
          <!-- End Slide #1 -->
        </div>

        <div class="js-slide">
          <!-- Slide #2 -->
          <div class="d-flex align-items-center bg-white u-hero-v1__secondary">
            <div class="container space-2">
              <div class="row">
                <div class="col-lg-4">
                  <h3 class="h5 text-muted">
                    <strong class="d-block">02.</strong>
                    <span class="d-block text-danger">Fully responsive</span>
                  </h3>
                  <p class="mb-0">Let visitors to view your content from their choice of device.</p>
                </div>
              </div>
            </div>

            <div class="w-100 h-100 d-none d-lg-inline-block bg-primary u-hero-v1__last">
              <div class="u-hero-v1__last-inner">
                <h3 class="h5 text-white">
                  <strong class="u-hero-v1__last-prev">Prev:</strong> Advanced editing
                </h3>
                <p class="text-white-70 mb-0">Modify any aspect of your website with Front</p>
              </div>
            </div>
          </div>
          <!-- End Slide #2 -->
        </div>
      </div>
      <!-- End Hero Carousel Secondary -->
    </div>
    <!-- End Hero v1 Section -->

    <hr class="my-0">
    <div id="SVGwave2Shape" class="position-relative" style="">
      <!-- Video Content -->
      <div class="bg-img-hero text-center space-4" style="background-image: url(<?php echo base_url(); ?>front/assets/img/1920x800/img4.jpg);">
        <!-- Fancybox -->
        <a class="js-fancybox u-media-player mb-4" href="javascript:;" data-src="//vimeo.com/167434033" data-speed="700" data-animate-in="zoomIn" data-animate-out="zoomOut" data-caption="Front - Responsive Website Template">
          <span class="u-media-player__icon u-media-player__icon--lg">
            <span class="fas fa-play u-media-player__icon-inner"></span>
          </span>
        </a>
        <!-- End Fancybox -->
        <br>
        <h4 class="d-inline-block text-white mb-0">Video WEB TV Kabupaten Wonosobo</h4>
      </div>
      <!-- End Video Content Section -->

      <!-- SVG Top Shape -->
      <figure class="position-absolute top-0 right-0 left-0">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" x="0px" y="0px" viewBox="0 0 1920 125.7" style="margin-top: -8px; enable-background:new 0 0 1920 125.7;" xml:space="preserve" class="injected-svg js-svg-injector" data-parent="#SVGwave2Shape">
        <style type="text/css">
          .wave-2-top-0{fill:#FFFFFF;}
        </style>
        <path class="wave-2-top-0 fill-white" d="M1920,0v44.2c0,0-451,63.8-960,6.3S0,125.7,0,125.7L0,0L1920,0z"></path>
        </svg>
      </figure>
      <!-- End SVG Top Shape -->

      <!-- SVG Bottom Shape -->
      <figure class="position-absolute right-0 bottom-0 left-0">
        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" x="0px" y="0px" viewBox="0 0 1920 125.7" style="margin-bottom: -8px; enable-background:new 0 0 1920 125.7;" xml:space="preserve" class="injected-svg js-svg-injector" data-parent="#SVGwave2Shape">
        <style type="text/css">
          .wave-2-bottom-0{fill:#FFFFFF;}
        </style>
        <path class="wave-2-bottom-0 fill-white" d="M0,125.7V81.5c0,0,451-63.8,960-6.3S1920,0,1920,0v125.7H0z"></path>
        </svg>
      </figure>
      <!-- End SVG Bottom Shape -->
    </div>
    <!-- Front in Frames Section -->
    <div class="overflow-hidden">
      <div class="container space-2 space-md-3">
        <div class="row justify-content-between align-items-center">
          <div class="col-lg-5 mb-7 mb-lg-0">
          
            <?php echo ''.$pengumuman.''; ?>

          </div>

          <div class="col-lg-6 position-relative">
            <!-- Image Gallery -->
            <div class="row mx-gutters-2">
                <?php echo ''.$pengumuman_berita1.''; ?>

            <!-- SVG Background Shape -->
            <div id="SVGbgShapeID1" class="svg-preloader w-100 content-centered-y z-index-n1">
              <figure class="ie-soft-triangle-shape">
                <img class="js-svg-injector" src="<?php echo base_url(); ?>front/assets/svg/components/soft-triangle-shape.svg" alt="Image Description"
                     data-parent="#SVGbgShapeID1">
              </figure>
            </div>
            <!-- End SVG Background Shape -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Front in Frames Section -->

    <!-- Devices v2 Section -->
    <div id="SVGSubscribe" class="svg-preloader bg-primary u-devices-v2">
      <div class="container space-2 space-md-3 position-relative z-index-2">
        <!-- Title -->
        <div class="w-md-80 w-lg-50 text-center mx-md-auto">
          <span class="btn btn-lg btn-icon btn-white rounded-circle mb-4">
            <span class="fas fa-paper-plane text-primary btn-icon__inner"></span>
          </span>
          <h2 class="h1 text-white">Device <span class="font-weight-semi-bold">friendly</span> features</h2>
          <p class="lead text-white-70 mb-0">Your website is fully responsive so visitors can view your content from their choice of device.</p>
        </div>
        <!-- End Title -->
      </div>

      <!-- Devices v2 -->
      <div class="d-none d-lg-block">
        <!-- SVG Tablet Mockup -->
        <div class="u-devices-v2__tablet">
          <div class="w-75 u-devices-v2__tablet-svg">
            <figure class="ie-devices-v2-tablet">
              <img class="js-svg-injector" src="<?php echo base_url(); ?>front/assets/svg/components/tablet.svg" alt="Image Description"
                   data-img-paths='[
                     {"targetId": "#SVGtabletImg1", "newPath": "<?php echo base_url(); ?>front/assets/img/533x711/img1.jpg"}
                   ]'
                   data-parent="#SVGSubscribe">
            </figure>
          </div>
        </div>
        <!-- End SVG Tablet Mockup -->

        <!-- SVG Phone Mockup -->
        <div class="u-devices-v2__phone">
          <div class="w-75 u-devices-v2__phone-svg">
            <figure class="ie-devices-v2-iphone">
              <img class="js-svg-injector" src="<?php echo base_url(); ?>front/assets/svg/components/iphone.svg" alt="Image Description"
                   data-img-paths='[
                     {"targetId": "#SVGiphoneImg1", "newPath": "<?php echo base_url(); ?>front/assets/img/282x500/img1.jpg"}
                   ]'
                   data-parent="#SVGSubscribe">
            </figure>
          </div>
        </div>
        <!-- End SVG Phone Mockup -->
      </div>
      <!-- End Devices v2 -->

      <!-- SVG Background Shape -->
      <figure class="w-100 content-centered-y position-absolute right-0 bottom-0">
        <img class="js-svg-injector" src="<?php echo base_url(); ?>front/assets/svg/components/process-pointer-1.svg" alt="Image Description"
             data-parent="#SVGSubscribe">
      </figure>
      <!-- End SVG Background Shape -->
    </div>
    <!-- End Devices v2 Section -->

    <!-- Team Section -->
    <div class="container space-2 space-md-3">
      <!-- Title -->
      <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
        <span class="btn btn-xs btn-soft-success btn-pill mb-2">Berita</span>
        <h2 class="text-primary">Berita Terbaru di <span class="font-weight-semi-bold"><?php if(!empty( $title )){ echo $title; } ?></span></h2>
      </div>
      <!-- End Title -->

      <!-- Slick Carousel -->
      <div class="js-slick-carousel u-slick u-slick--gutters-3"
           data-slides-show="2"
           data-slides-scroll="1"
           data-pagi-classes="text-center u-slick__pagination mt-7 mb-0"
           data-responsive='[{
             "breakpoint": 992,
             "settings": {
               "slidesToShow": 1
             }
           }, {
             "breakpoint": 768,
             "settings": {
               "slidesToShow": 1
             }
           }, {
             "breakpoint": 554,
             "settings": {
               "slidesToShow": 1
             }
           }]'>

           <?php echo ''.$highlight.''; ?>
           
      </div>
      <!-- End Slick Carousel -->
      <!-- Title -->
      <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9"><p></p>
        <a class="btn btn-sm btn-soft-primary btn-pill mb-2 transition-3d-hover" href="<?php echo base_url(); ?>sitemap_berita">Sitemap Berita <span class="fas fa-angle-right ml-2"></span></a>
      </div>
      <!-- End Title -->
    </div>
    <!-- End Team Section -->

    <!-- Blog Grid Section -->
    <div class="bg-light">
      <div class="container space-2 space-md-3">
        <!-- Title -->
        <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
          <span class="btn btn-xs btn-soft-success btn-pill mb-2">News</span>
          <h2 class="text-primary">Pelayanan Publik <span class="font-weight-semi-bold">Kabupaten Wonosobo</span></h2>
          <p>Pemohon Informasi Publik adalah warga negara dan/atau badan hukum Indonesia yang mengajukan permintaan informasi publik.</p>
        </div>
        <!-- End Title -->

        <!-- News Carousel -->
        <div class="js-slick-carousel u-slick u-slick--equal-height u-slick--gutters-2"
             data-slides-show="4"
             data-slides-scroll="1"
             data-pagi-classes="text-center u-slick__pagination mt-7 mb-0"
             data-responsive='[{
               "breakpoint": 1200,
               "settings": {
                 "slidesToShow": 3
               }
             }, {
               "breakpoint": 992,
               "settings": {
                 "slidesToShow": 2
               }
             }, {
               "breakpoint": 768,
               "settings": {
                 "slidesToShow": 2
               }
             }, {
               "breakpoint": 554,
               "settings": {
                 "slidesToShow": 1
               }
             }]'>
          <!-- Blog Grid -->
           <?php echo ''.$highlight_layanan.''; ?>
        </div>
        <!-- End News Carousel -->
        <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9"><p></p>
          <a class="btn btn-sm btn-soft-primary btn-pill mb-2 transition-3d-hover" href="<?php echo base_url(); ?>sitemap_berita">Layanan Publik<span class="fas fa-angle-right ml-2"></span></a>
        </div>
        <!-- End Title -->
      </div>
    </div>
    <!-- End Blog Grid Section -->
  </main>
  <!-- ========== END MAIN ========== -->

  <!-- ========== FOOTER ========== -->
  <footer class="container space-top-2 space-top-md-3">
    <div class="border-bottom">
      <div class="row mb-7">
        <div class="col-lg-3 mb-7 mb-lg-0">
          <div class="d-inline-flex align-self-start flex-column h-100">
            <!-- Logo -->
            <a class="d-flex align-items-center mb-3" href="index.html" aria-label="Front">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="36px" height="36px" viewBox="0 0 46 46" xml:space="preserve" style="margin-bottom: 0;">
                <path fill="#3F7DE0" opacity=".65" d="M23,41L23,41c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18h11.3C38,5,41,8,41,11.7V23C41,32.9,32.9,41,23,41z"/>
                <path class="fill-info" opacity=".5" d="M28,35.9L28,35.9c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18l11.3,0C43,0,46,3,46,6.6V18C46,27.9,38,35.9,28,35.9z"/>
                <path class="fill-primary" opacity=".7" d="M18,46L18,46C8,46,0,38,0,28v0c0-9.9,8-18,18-18h11.3c3.7,0,6.6,3,6.6,6.6V28C35.9,38,27.9,46,18,46z"/>
                <path class="fill-white" d="M17.4,34V18.3h10.2v2.9h-6.4v3.4h4.8v2.9h-4.8V34H17.4z"/>
              </svg>
              <span class="brand brand-primary">Front</span>
            </a>
            <!-- End Logo -->

            <!-- Country -->
            <div class="position-relative">
              <a id="footerCountryInvoker" class="dropdown-nav-link" href="javascript:;" role="button"
                 aria-controls="footer-country"
                 aria-haspopup="true"
                 aria-expanded="false"
                 data-toggle="dropdown"
                 data-unfold-event="click"
                 data-unfold-target="#footer-country"
                 data-unfold-type="css-animation"
                 data-unfold-duration="300"
                 data-unfold-delay="300"
                 data-unfold-hide-on-scroll="false"
                 data-unfold-animation-in="slideInUp"
                 data-unfold-animation-out="fadeOut">
                <img class="dropdown-item-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/us.svg" alt="United States Flag">
                <span>United States</span>
              </a>

              <div id="footer-country" class="dropdown-menu dropdown-unfold dropdown-card dropdown-menu-bottom" aria-labelledby="footerCountryInvoker">
                <div class="card">
                  <!-- Body -->
                  <div class="card-body list-group list-group-flush list-group-borderless p-5">
                    <h4 class="h6 font-weight-semi-bold">Front available in</h4>

                    <div class="row">
                      <div class="col-6">
                        <!-- List Group -->
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/au.svg" alt="Australia Flag">
                          Australia
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/at.svg" alt="Austria Flag">
                          Austria
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/be.svg" alt="Belgium Flag">
                          Belgium
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/ca.svg" alt="Canada Flag">
                          Canada
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/dk.svg" alt="Denmark Flag">
                          Denmark
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/fi.svg" alt="Finland Flag">
                          Finland
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/fr.svg" alt="France Flag">
                          France
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/de.svg" alt="Germany Flag">
                          Germany
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/nl.svg" alt="Netherlands Flag">
                          Netherlands
                        </a>
                        <!-- End List Group -->
                      </div>

                      <div class="col-6">
                        <!-- List Group -->
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/nz.svg" alt="New Zealand Flag">
                          New Zealand
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/no.svg" alt="Norway Flag">
                          Norway
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/pt.svg" alt="PortugalPREVIEW Flag">
                          Portugal
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/sg.svg" alt="Singapore Flag">
                          Singapore
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/es.svg" alt="Spain Flag">
                          Spain
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/se.svg" alt="Sweden Flag">
                          Sweden
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/ch.svg" alt="Switzerland Flag">
                          Switzerland
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/gb.svg" alt="United Kingdom Flag">
                          UK
                        </a>
                        <a class="list-group-item list-group-item-action active " href="#">
                          <img class="list-group-icon" src="<?php echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/us.svg" alt="United States Flag">
                          US
                        </a>
                        <!-- End List Group -->
                      </div>
                    </div>
                  </div>
                  <!-- End Body -->

                  <!-- Footer -->
                  <a class="card-footer card-bg-light p-5" href="#">
                    <span class="d-block text-muted mb-1">More countries coming soon.</span>
                    <small class="d-block">Signup to get notified <span class="fas fa-arrow-right small"></span></small>
                  </a>
                  <!-- End Footer -->
                </div>
              </div>
            </div>
            <!-- End Country -->
          </div>
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mb-4 mb-md-0">
          <h4 class="h6 font-weight-semi-bold">Account</h4>

          <!-- List Group -->
          <ul class="list-group list-group-flush list-group-borderless mb-0">
            <li><a class="list-group-item list-group-item-action" href="../account/dashboard.html">Account</a></li>
            <li><a class="list-group-item list-group-item-action" href="../account/my-tasks.html">My tasks</a></li>
            <li><a class="list-group-item list-group-item-action" href="../account/projects.html">Projects</a></li>
            <li><a class="list-group-item list-group-item-action" href="../account/invite-friends.html">Invite friends</a></li>
          </ul>
          <!-- End List Group -->
        </div>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 mb-4 mb-md-0">
          <h4 class="h6 font-weight-semi-bold">Company</h4>

          <!-- List Group -->
          <ul class="list-group list-group-flush list-group-borderless mb-0">
            <li><a class="list-group-item list-group-item-action" href="../pages/about-agency.html">About</a></li>
            <li><a class="list-group-item list-group-item-action" href="../pages/services-agency.html">Services</a></li>
            <li><a class="list-group-item list-group-item-action" href="../pages/careers.html">Careers</a></li>
            <li><a class="list-group-item list-group-item-action" href="../blog/grid-right-sidebar.html">Blog</a></li>
          </ul>
          <!-- End List Group -->
        </div>

        <div class="col-sm-4 col-md-3 col-lg-2 mb-4 mb-md-0">
          <h4 class="h6 font-weight-semi-bold">Resources</h4>

          <!-- List Group -->
          <ul class="list-group list-group-flush list-group-borderless mb-0">
            <li><a class="list-group-item list-group-item-action" href="../pages/contacts-agency.html">Contacts</a></li>
            <li><a class="list-group-item list-group-item-action" href="../pages/faq.html">Help</a></li>
            <li><a class="list-group-item list-group-item-action" href="../pages/terms.html">Terms</a></li>
            <li><a class="list-group-item list-group-item-action" href="../pages/privacy.html">Privacy</a></li>
          </ul>
          <!-- End List Group -->
        </div>

        <div class="col-md-3 col-lg-2">
          <h4 class="h6 font-weight-semi-bold">Contact</h4>

          <!-- Address -->
          <address>
            <ul class="list-group list-group-flush list-group-borderless mb-0">
              <li class="list-group-item">+1 (062) 109-9222</li>
              <li class="list-group-item">
                <a href="mailto:support@htmlstream.com">support@htmlstream.com</a>
              </li>
              <li class="list-group-item">153 Williamson Plaza, Maggieberg, MT 09514</li>
            </ul>
          </address>
          <!-- End Address -->
        </div>
      </div>
    </div>

    <div class="d-flex justify-content-between align-items-center py-7">
      <!-- Copyright -->
      <p class="small text-muted mb-0">&copy; Front. 2019 Htmlstream.</p>
      <!-- End Copyright -->

      <!-- Social Networks -->
      <ul class="list-inline mb-0">
        <li class="list-inline-item">
          <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" href="#">
            <span class="fab fa-facebook-f btn-icon__inner"></span>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" href="#">
            <span class="fab fa-google btn-icon__inner"></span>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" href="#">
            <span class="fab fa-twitter btn-icon__inner"></span>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent" href="#">
            <span class="fab fa-github btn-icon__inner"></span>
          </a>
        </li>
      </ul>
      <!-- End Social Networks -->
    </div>
  </footer>
  <!-- ========== END FOOTER ========== -->

  <?php $this->load->view('front_end/classic-business/sidebarakun.php'); ?>

  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#"
    data-position='{"bottom": 15, "right": 15 }'
    data-type="fixed"
    data-offset-top="400"
    data-compensation="#header"
    data-show-effect="slideInUp"
    data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->

  <!-- JS Global Compulsory -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/bootstrap/bootstrap.min.js"></script>

  <!-- JS Implementing Plugins -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/svg-injector/dist/svg-injector.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/fancybox/jquery.fancybox.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/slick-carousel/slick/slick.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>

  <!-- JS Front -->
  <script src="<?php echo base_url(); ?>front/assets/js/hs.core.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.header.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.unfold.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.fancybox.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.slick-carousel.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.validation.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.focus-state.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.cubeportfolio.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.svg-injector.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.go-to.js"></script>

  <!-- JS Plugins Init. -->
  <script>
    $(window).on('load', function () {
      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 767.98,
        hideTimeOut: 0
      });

      // initialization of svg injector module
      $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {
      // initialization of header
      $.HSCore.components.HSHeader.init($('#header'));

      // initialization of unfold component
      $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

      // initialization of fancybox
      $.HSCore.components.HSFancyBox.init('.js-fancybox');

      // initialization of slick carousel
      $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

      // initialization of form validation
      $.HSCore.components.HSValidation.init('.js-validate');

      // initialization of forms
      $.HSCore.components.HSFocusState.init();

      // initialization of cubeportfolio
      $.HSCore.components.HSCubeportfolio.init('.cbp');

      // initialization of go to
      $.HSCore.components.HSGoTo.init('.js-go-to');
    });
  </script>
</body>
</html>