<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title -->
  <title><?php if (!empty($title)) {echo $title;} ?></title>
  <!-- Meta -->
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" />
  <meta name="author" content="<?php if (!empty($title)) {
                                  echo $title;
                                } ?>" />
  <meta name="keywords" content="<?php if (!empty($title)) {
                                    echo $title;
                                  } ?> <?php echo base_url(); ?>" />
  <meta name="og:description" content="<?php if (!empty($title)) {
                                          echo $title;
                                        } ?>" />
  <meta name="og:url" content="<?php echo base_url(); ?> <?php if (!empty($title)) {
                                                            echo $title;
                                                          } ?>" />
  <meta name="og:title" content="<?php if (!empty($title)) {
                                    echo $title;
                                  } ?> <?php echo base_url(); ?>" />
  <meta name="og:keywords" content="<?php if (!empty($title)) {
                                      echo $title;
                                    } ?> <?php echo base_url(); ?>" />
																		<?php
																		if($web=='demoopd.wonosobokab.go.id' or $web=='jdih.wonosobokab.go.id'){
																		echo '
																			<meta name="og:image" content="'.base_url().'media/logo-jdihn.png" />
																			<link id="favicon" rel="shortcut icon" href="'.base_url().'media/logo-jdihn.png" type="image/png" />
																		';
																		}
																		else{
																			echo '
																			<meta name="og:image" content="'.base_url().'media/logo wonosobo.png" />
																			<link id="favicon" rel="shortcut icon" href="'.base_url().'media/logo wonosobo.png" type="image/png" />
																			';
																		}
																		?>
  <!-- Google Fonts -->
  <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/animate.css/animate.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/fancybox/jquery.fancybox.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/slick-carousel/slick/slick.css">

  <!-- CSS Front Template -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/theme.css">

</head>

<body>
  <!-- ========== HEADER ========== -->
  <header id="header" class="u-header">
    <div class="u-header__section">
      <div id="logoAndNav" class="container">
        <!-- Nav -->
        <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">
          <!-- Responsive Toggle Button -->
          <button type="button" class="navbar-toggler btn u-hamburger" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
            <span id="hamburgerTrigger" class="u-hamburger__box">
              <span class="u-hamburger__inner"></span>
            </span>
          </button>
          <!-- End Responsive Toggle Button -->
          <!-- Navigation -->
          <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
            <?php echo '' . $menu_atas . ''; ?>
          </div>
          <!-- End Navigation -->
        </nav>
        <!-- End Nav -->
      </div>
    </div>
  </header>
  <!-- ========== END HEADER ========== -->

  <!-- ========== MAIN CONTENT ========== -->
  <main id="content" role="main">
    <!-- Hero Section -->
    <div class="position-relative">
      <!-- Slick Carousel -->
      <div class="js-slick-carousel u-slick" data-fade="true" data-autoplay="true" data-speed="5000" data-infinite="true">
        <?php
        $where = array(
          'status' => 1,
          'domain' => $web
        );
        $this->db->where($where);
        $this->db->limit(3);
        $this->db->order_by('created_time desc');
        $query1 = $this->db->get('slide_website');
        $a = 0;
        if ($query1->num_rows() == 0) { } else {
          foreach ($query1->result() as $row1) {
            echo
              '
              <div class="js-slide">
                <div class="bg-img-hero min-height-620" style="background-image: url(' . base_url() . 'media/upload/' . $row1->file_name . ');"></div>
              </div>
              ';
            $url_redirection = $row1->url_redirection;
            $slide_website_keterangan = $row1->keterangan;
          }
        }
        ?>
      </div>
      <!-- End Slick Carousel -->

      <div class="container text-center mb-7 position-absolute right-0 bottom-0 left-0 space-top-5 space-bottom-2">
        <!-- Info Link -->
        <a class="d-sm-inline-flex align-items-center bg-primary text-white shadow rounded-pill p-2 pr-3 mb-3" href="<?php echo $url_redirection; ?>">
          <span class="btn btn-xs btn-soft-white btn-pill font-weight-semi-bold mr-3">Start Now</span>
          <span class="d-block d-sm-inline-block">
            <?php echo $slide_website_keterangan; ?>
          </span>
        </a>
        <!-- End Info Link -->

        <div class="card border-0">
          <div class="card-body p-7">
            <!-- Search Jobs Form -->
            <form action="<?php echo base_url(); ?>peraturan/">
              <div class="row">
                <div class="col-lg-5 mb-4 mb-lg-0">
                  <!-- Input -->
                  <label class="d-block">
                    <small class="d-block text-secondary">Pilih salah satu</small>
                  </label>
                  <div class="js-focus-state">
                    <div class="input-group">
                      <select name="orderby_perundang_undangan" class="form-control" id="orderby_perundang_undangan">
                        <option value="Peraturan Terbaru">Peraturan Terbaru</option>
                      </select>
                    </div>
                  </div>
                  <!-- End Input -->
                </div>

                <div class="col-lg-5 mb-4 mb-lg-0">
                  <!-- Input -->
                  <label class="d-block">
                    <small class="d-block text-secondary">Kata kunci masukkan disini...</small>
                  </label>
                  <div class="js-focus-state">
                    <div class="input-group">
                      <input type="text" id="kata_kunci" name="kata_kunci" class="form-control" placeholder="..." aria-label="City, state, or zip" aria-describedby="locationInputAddon">
                      <div class="input-group-append">
                        <span class="input-group-text">
                          <span class="fas fa-search" id="locationInputAddon"></span>
                        </span>
                      </div>
                    </div>
                  </div>
                  <!-- End Input -->
                </div>

                <div class="col-lg-2 align-self-lg-end">
                  <input type="submit" class="btn btn-block btn-primary transition-3d-hover" />
                </div>
              </div>
              <!-- End Checkbox -->
            </form>
            <!-- End Search Jobs Form -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Hero Section -->
    <div class="overflow-hidden">
      <div class="container space-1">
        <div class="row justify-content-lg-between align-items-center">
          <div class="col-md-6 col-lg-5 order-md-2 mb-1 mb-md-0">
            <div class="pl-md-4">
              <!-- Info -->
              <?php
              if ($web == 'demoopd.wonosobokab.go.id') {
                $where_web = "
              and perundang_undangan.domain = 'jdih.wonosobokab.go.id'
              ";
              } elseif ($web == 'jdih.wonosobokab.go.id') {
                $where_web = "";
              } else {
                $where_web = "
              and perundang_undangan.domain = '" . $web . "'
              ";
              }
              $wberita = $this->db->query("
            SELECT *, (select jenis_perundang_undangan.jenis_perundang_undangan_code from jenis_perundang_undangan where jenis_perundang_undangan.jenis_perundang_undangan_id=perundang_undangan.jenis_perundang_undangan_id) singkatan_peraturan
            from perundang_undangan
              where perundang_undangan.status = 1
              " . $where_web . "
              order by perundang_undangan.created_time desc
              limit 4
              ");
              $nomor = 0;
              foreach ($wberita->result() as $h) {
                echo '
                <div class="media mb-1">
                  <span class="btn btn-sm btn-icon btn-soft-success rounded-circle mr-3">
                    <span class="btn-icon__inner">' . $h->nomor_peraturan . '</span>
                  </span>
                  <div class="media-body">
                    <h3 class="h5 text-success"><b>'.ucwords(strtoupper(''.$h->singkatan_peraturan.'')).'</b> Tahun ' . $h->tahun_pengundangan . '</h3>
                    <p><a href="' . base_url() . 'peraturan/details/?id=' . $h->perundang_undangan_id . '">'.ucwords(strtolower(''.$h->perundang_undangan_name.'')).'</a></p>
                  </div>
                </div>
              ';
              }
              echo '<a class="btn btn-success btn-wide btn-pill transition-3d-hover" href="'.base_url().'peraturan">Tampilkan semua <span class="fas fa-angle-right ml-2"></span></a>';
              ?>
            </div>
          </div>

          <div id="SVGellipseMockupReverse" class="col-md-6 order-md-1" style="">
            <!-- SVG Mockup -->
            <figure class="ie-ellipse-mockup">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 614.8 747.7" style="enable-background:new 0 0 614.8 747.7;" xml:space="preserve" class="injected-svg js-svg-injector" data-img-paths="[
                   {&quot;targetId&quot;: &quot;#SVGellipseMockupReverseImg1&quot;, &quot;newPath&quot;: &quot;https://demoopd.wonosobokab.go.id/media/sekda bupati wonosobo.jpg&quot;}
                 ]" data-parent="#SVGellipseMockupReverse">
                <style type="text/css">
                  .ellipse-mockup-reverse-0 {
                    fill: #377DFF;
                  }

                  .ellipse-mockup-reverse-1 {
                    fill: #FF4E50;
                  }

                  .ellipse-mockup-reverse-2 {
                    fill: #00C9A7;
                  }

                  .ellipse-mockup-reverse-3 {
                    fill: #377DFF;
                  }

                  .ellipse-mockup-reverse-4 {
                    fill: #FFC107;
                  }
                </style>
                <path class="ellipse-mockup-reverse-0" opacity=".1" d="M428.5,138.9C489.6,144.5,545.6,176,581,226c54.3,76.8,67.1,204.7-193.8,371.7c-411.5,263.5-387.9,6-382.7-30.6  c0.5-3.6,0.9-7.2,1.3-10.7l22.1-241.9c10.6-116.2,113.5-201.8,229.8-191.1L428.5,138.9z"></path>
                <g>
                  <defs>
                    <path id="SVGellipseMockupReverseID1" d="M446.2,729.7L446.2,729.7C310.4,782.7,156,714.9,103,579.1L18,361.2C-34.9,225.4,32.8,71,168.6,18l0,0    c135.8-53,290.2,14.8,343.2,150.6l85,217.9C649.8,522.3,582,676.7,446.2,729.7z"></path>
                  </defs>
                  <clipPath id="SVGellipseMockupReverseID2">
                    <use xlink:href="#SVGellipseMockupReverseID1" style="overflow:visible;"></use>
                  </clipPath>
                  <g style="clip-path:url(#SVGellipseMockupReverseID2);">
                    <!-- Apply your (615px width to 750px height) image here -->
                    <image id="SVGellipseMockupReverseImg1" style="overflow:visible;" width="615" height="750" xlink:href="https://demoopd.wonosobokab.go.id/media/sekda bupati wonosobo.jpg" transform="matrix(1 0 0 1 -3.177470e-02 -0.9532)"></image>
                  </g>
                </g>
                <g>
                  <circle class="ellipse-mockup-reverse-1" cx="488.6" cy="693.8" r="16.3"></circle>
                  <circle class="ellipse-mockup-reverse-2" cx="482" cy="632.6" r="10.6"></circle>
                  <circle class="ellipse-mockup-reverse-3" cx="537.8" cy="655.9" r="21.6"></circle>
                  <circle class="ellipse-mockup-reverse-4" cx="576" cy="708.1" r="3.9"></circle>
                </g>
              </svg>
            </figure>
            <!-- End SVG Mockup -->
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <hr class="my-0">
    </div>
    <!-- Team Section -->
    <div class="container space-2 space-md-1">
      <!-- Title 
        <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
          <span class="btn btn-xs btn-soft-success btn-pill mb-2">Berita</span>
        </div>
         End Title -->

      <!-- Slick Carousel -->
      <div class="js-slick-carousel u-slick u-slick--gutters-3" data-slides-show="2" data-slides-scroll="1" data-pagi-classes="text-center u-slick__pagination mt-7 mb-0" data-responsive='[{
             "breakpoint": 992,
             "settings": {
               "slidesToShow": 1
             }
           }, {
             "breakpoint": 768,
             "settings": {
               "slidesToShow": 1
             }
           }, {
             "breakpoint": 554,
             "settings": {
               "slidesToShow": 1
             }
           }]'>

        <?php
        if ($web == 'web.wonosobokab.go.id') {
          $where_web = "";
        } elseif ($web == 'demoopd.wonosobokab.go.id') {
          $where_web = "
              and posting.domain = 'demoopd.wonosobokab.go.id'
              ";
        } else {
          $where_web = "
              and posting.domain = '" . $web . "'
              ";
        }
        $wberita = $this->db->query("
            SELECT posting.id_posting, posting.judul_posting
            from posting
            where posting.judul_posting like '%berita%'
            and posting.status = 1
            and posting.parent = 0
            " . $where_web . "
            order by created_time desc
            ");
        $xxberita = $wberita->num_rows();
        foreach ($wberita->result() as $hberita) {
          $w = $this->db->query("
              SELECT posting.id_posting, posting.judul_posting, posting.domain, posting.created_time, posting.isi_posting, attachment.file_name
              from posting, attachment
              where posting.status = 1 
              and posting.highlight = 1
              and posting.parent = $hberita->id_posting
              and attachment.id_tabel=posting.id_posting
              order by created_time desc
              limit 7
              ");
          $nomor = 0;
          foreach ($w->result() as $h) {
            $nomor = $nomor + 1;
            $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/", "'", "`");
            $yummy   = array("_", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "");
            $newphrase = str_replace($healthy, $yummy, $h->judul_posting);
            $created_time = '' . $this->Crud_model->dateBahasaIndotok($h->created_time) . '';
            echo '
                <div class="js-slide px-3">
                  <!-- Team -->
                  <div class="row">
                    <div class="col-sm-6 d-sm-flex align-content-sm-start flex-sm-column text-center text-sm-left mb-7 mb-sm-0">
                      <div class="w-100">
                        <h3 class="h5 mb-4">' . $h->judul_posting . '</h3>
                      </div>
                      <div class="d-inline-block">
                        <span class="badge badge-primary badge-pill badge-bigger mb-3">#' . $created_time . '</span>
                      </div>
                      <p class="font-size-1">' . substr(strip_tags($h->isi_posting), 0, 150) . '</p>
                      <ul class="list-inline mt-auto mb-0">
                        <li class="list-inline-item mx-0">
                          <a class="btn btn-sm btn-soft-secondary" href="https://' . $h->domain . '/front/details/' . $h->id_posting . '" target="_blank">
                            Selengkapnya
                          </a>
                        </li>
                      </ul>
                      <!-- End Social Networks -->
                    </div>
                    <div class="col-sm-6">
                      <img class="img-fluid rounded mx-auto" src="https://' . $h->domain . '/media/upload/s_' . $h->file_name . '" alt="Image ' . $h->judul_posting . '">
                    </div>
                  </div>
                  <!-- End Team -->
                </div>
              ';
          }
        }
        ?>

      </div>
      <!-- End Slick Carousel -->
      <!-- Title -->
      <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
        <p></p>
        <a class="btn btn-sm btn-soft-primary btn-pill mb-2 transition-3d-hover" href="<?php echo base_url(); ?>sitemap">Sitemap Berita <span class="fas fa-angle-right ml-2"></span></a>
      </div>
      <!-- End Title -->
    </div>
    <!-- End Team Section -->

    <div class="container">
      <hr class="my-0">
    </div>

    <!-- Testimonials -->
    <div class="bg-light">
      <div class="container space-2">
        <!-- Title -->
        <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
          <h2 class="font-weight-medium">Pelayanan Publik <span class="font-weight-semi-bold">Kabupaten Wonosobo</span></h2>
          <p>Pemohon Informasi Publik adalah warga negara dan/atau badan hukum Indonesia yang mengajukan permintaan informasi publik.</p>
        </div>
        <!-- End Title -->

        <!-- News Carousel -->
        <div class="js-slick-carousel u-slick u-slick--equal-height u-slick--gutters-2" data-slides-show="4" data-slides-scroll="1" data-pagi-classes="text-center u-slick__pagination mt-7 mb-0" data-responsive='[{
               "breakpoint": 1200,
               "settings": {
                 "slidesToShow": 3
               }
             }, {
               "breakpoint": 992,
               "settings": {
                 "slidesToShow": 2
               }
             }, {
               "breakpoint": 768,
               "settings": {
                 "slidesToShow": 2
               }
             }, {
               "breakpoint": 554,
               "settings": {
                 "slidesToShow": 1
               }
             }]'>
          <!-- Blog Grid -->
          <?php
          if ($web == 'web.wonosobokab.go.id') {
            $where_web = "";
          } elseif ($web == 'demoopd.wonosobokab.go.id') {
            $where_web = "
              and permohonan_informasi_publik.domain = 'diskominfo.wonosobokab.go.id'
              ";
          } else {
            $where_web = "
              and permohonan_informasi_publik.domain = '" . $web . "'
              ";
          }
          $w = $this->db->query("
            SELECT permohonan_informasi_publik.id_permohonan_informasi_publik, permohonan_informasi_publik.created_time, permohonan_informasi_publik.nama, permohonan_informasi_publik.instansi, permohonan_informasi_publik.rincian_informasi_yang_diinginkan, permohonan_informasi_publik.domain, permohonan_informasi_publik.tujuan_penggunaan_informasi
            from permohonan_informasi_publik
            where permohonan_informasi_publik.status = 1 
            and permohonan_informasi_publik.parent = 0
            " . $where_web . "
            order by permohonan_informasi_publik.created_time desc
            limit 7
            ");
          foreach ($w->result() as $h) {
            $created_time = '' . $this->Crud_model->dateBahasaIndotok($h->created_time) . '';
            echo '
              <div class="js-slide card border-0 mb-3">
                <div class="card-body p-5">
                  <small class="d-block text-muted mb-2">' . $created_time . '</small>
                  <small class="d-block text-muted mb-2">' . $h->domain . '</small>
                  <h3 class="h5">
                    <a href="https://' . $h->domain . '/postings/details/' . $h->id_permohonan_informasi_publik . '" tabindex="0">' . $h->tujuan_penggunaan_informasi . '</a>
                  </h3>
                  <p class="mb-0">' . substr(strip_tags($h->rincian_informasi_yang_diinginkan), 0, 150) . '</p>
                </div>
    
                <div class="card-footer pb-5 px-0 mx-5">
                  <div class="media align-items-center">
                    <div class="u-sm-avatar mr-3">
                      <img class="img-fluid rounded-circle" src="https://web.wonosobokab.go.id/front/assets/img/100x100/img4.jpg" alt="Image Description">
                    </div>
                    <div class="media-body">
                      <h4 class="small mb-0"><a href="https://' . $h->domain . '/sosegov/details/' . $h->id_permohonan_informasi_publik . '" tabindex="0">' . $h->nama . '</a> | ' . $h->instansi . '</h4>
                    </div>
                  </div>
                </div>
              </div>
              ';
          }
          ?>
        </div>
        <!-- End News Carousel -->
        <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
          <p></p>
          <a class="btn btn-sm btn-soft-primary btn-pill mb-2 transition-3d-hover" href="<?php echo base_url(); ?>sosegov">Layanan Publik<span class="fas fa-angle-right ml-2"></span></a>
        </div>
        <!-- End Title -->
      </div>
    </div>
    <!-- End Testimonials -->

    <!-- Testimonials -->

    <div class="container">
      <hr class="my-0">
    </div>
  </main>
  <!-- ========== END MAIN CONTENT ========== -->

  <!-- ========== FOOTER ========== -->
  <footer class="gradient-half-primary-v4">
    <div class="container">
      <!-- CTA -->
      <div class="row justify-content-lg-between align-items-md-center space-2">
        <div class="col-md-6 col-lg-5 mb-5 mb-md-0">
          <h3 class="text-white font-weight-medium mb-1">Kami di sini untuk membantu</h3>
          <p class="text-white">Temukan di <a class="text-warning font-weight-medium" href="<?php echo base_url(); ?>help">Pusat Bantuan kami.</a>
            <br />Email: <?php echo $email; ?><br />
            Telp: <?php echo $telpon; ?><br />
            Alamat: <?php echo $alamat; ?>
          </p>
          <ul class="list-inline mb-0">
            <li class="list-inline-item mb-2 mb-sm-0">
              <a class="btn btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="<?php echo $google; ?>">
                <span class="fab fa-google btn-icon__inner"></span>
              </a>
            </li>
            <li class="list-inline-item mb-2 mb-sm-0">
              <a class="btn btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="<?php echo $facebook; ?>">
                <span class="fab fa-facebook-f btn-icon__inner"></span>
              </a>
            </li>
            <li class="list-inline-item mb-2 mb-sm-0">
              <a class="btn btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                <span class="fab fa-medium-m btn-icon__inner"></span>
              </a>
            </li>
            <li class="list-inline-item mb-2 mb-sm-0">
              <a class="btn btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="#">
                <span class="fab fa-github btn-icon__inner"></span>
              </a>
            </li>
            <li class="list-inline-item mb-2 mb-sm-0">
              <a class="btn btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="<?php echo $twitter; ?>">
                <span class="fab fa-twitter btn-icon__inner"></span>
              </a>
            </li>
            <li class="list-inline-item mb-2 mb-sm-0">
              <a class="btn btn-icon btn-soft-dark btn-bg-transparent rounded-circle" href="<?php echo $instagram; ?>">
                <span class="fab fa-instagram btn-icon__inner"></span>
              </a>
            </li>
          </ul>
        </div>

        <div class="col-md-6 col-lg-5 text-md-center">
          <!-- Contact Form -->
          <form class="js-validate card border-0 shadow-soft p-5">
            <div class="mb-4">
              <h3 class="h5">Survey Kepuasan Masyarakat</h3>
            </div>

            <div class="row mx-gutters-2">
              <div class="col-md-12 mb-3">
                <div class="js-form-message">
                  <div class="input-group">
                    <select type="text" class="form-control custom-select text-muted" name="firstName" placeholder="First name" aria-label="First name" required data-msg="Please enter your first name." data-error-class="u-has-error" data-success-class="u-has-success">
											<option value="0">Pilih Persepsi</option>
											<option value="4">Sangat sesuai</option>
											<option value="3">Sesuai</option>
											<option value="2">Kurang sesuai</option>
											<option value="1">Tidak sesuai</option>
										</select>
                  </div>
                </div>
                <!-- End Input -->
              </div>

            </div>

          </form>
          <!-- End Contact Form -->
        </div>
      </div>
      <!-- End CTA -->

      <hr class="opacity-md my-0">
      <div class="container text-center space-1">
        <!-- End Logo -->
        <p class="small text-muted">© <?php echo $web; ?>. 2019 All rights reserved.</p>
      </div>
    </div>
  </footer>
  <!-- ========== END FOOTER ========== -->

  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#" data-position='{"bottom": 15, "right": 15 }' data-type="fixed" data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->

  <!-- JS Global Compulsory -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/bootstrap/bootstrap.min.js"></script>

  <!-- JS Implementing Plugins -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/svg-injector/dist/svg-injector.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/fancybox/jquery.fancybox.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/slick-carousel/slick/slick.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/cubeportfolio/js/jquery.cubeportfolio.min.js"></script>

  <!-- JS Front -->
  <script src="<?php echo base_url(); ?>front/assets/js/hs.core.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.header.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.unfold.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.focus-state.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.validation.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.fancybox.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.slick-carousel.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.show-animation.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.svg-injector.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.go-to.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.cubeportfolio.js"></script>

  <!-- JS Plugins Init. -->
  <script>
    $(window).on('load', function() {
      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 767.98,
        hideTimeOut: 0
      });

      // initialization of svg injector module
      $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function() {
			
      // initialization of header
      $.HSCore.components.HSHeader.init($('#header'));

      // initialization of unfold component
      $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));

      // initialization of forms
      // $.HSCore.components.HSFocusState.init();

      // initialization of form validation
      $.HSCore.components.HSValidation.init('.js-validate');

      // initialization of fancybox
      $.HSCore.components.HSFancyBox.init('.js-fancybox');

      // initialization of slick carousel
      $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

      // initialization of show animations
      $.HSCore.components.HSShowAnimation.init('.js-animation-link');

      // initialization of go to
      $.HSCore.components.HSGoTo.init('.js-go-to');
    });
  </script>
</body>

</html>