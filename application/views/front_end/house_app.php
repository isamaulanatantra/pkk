<?php
  defined('BASEPATH') or exit('No direct script access allowed');
  $ses=$this->session->userdata('id_users');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title -->
  <title><?php if (!empty($title)) {
            echo $title;
          } ?></title>
  <!-- Meta -->
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" />
  <meta name="author" content="<?php if (!empty($title)) {
                                  echo $title;
                                } ?>" />
  <meta name="keywords" content="<?php if (!empty($title)) {
                                    echo $title;
                                  } ?> <?php echo base_url(); ?>" />
  <meta name="og:description" content="<?php if (!empty($title)) {
                                          echo $title;
                                        } ?>" />
  <meta name="og:url" content="<?php echo base_url(); ?> <?php if (!empty($title)) {
                                                            echo $title;
                                                          } ?>" />
  <meta name="og:title" content="<?php if (!empty($title)) {
                                    echo $title;
                                  } ?> <?php echo base_url(); ?>" />
  <meta name="og:keywords" content="<?php if (!empty($title)) {
                                      echo $title;
                                    } ?> <?php echo base_url(); ?>" />
  <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png" />
  <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />

  <!-- Google Fonts -->
  <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/animate.css/animate.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/fancybox/jquery.fancybox.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/slick-carousel/slick/slick.css">

  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/flatpickr/dist/flatpickr.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css">

  <!-- CSS Front Template -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/theme.css">
  
</head>

<body>
  <!-- ========== HEADER ========== -->
  <header id="header" class="u-header u-header--floating-md">
    <!-- Search -->
    <div id="searchPushTop" class="u-search-push-top">
      <div class="container position-relative">
        <div class="u-search-push-top__content">
          <!-- Close Button -->
          <button type="button" class="close u-search-push-top__close-btn"
                  aria-haspopup="true"
                  aria-expanded="false"
                  aria-controls="searchPushTop"
                  data-unfold-type="jquery-slide"
                  data-unfold-target="#searchPushTop">
            <span aria-hidden="true">&times;</span>
          </button>
          <!-- End Close Button -->

          <!-- Input -->
          <form class="js-focus-state input-group">
            <input type="search" class="form-control" placeholder="Search Front" aria-label="Search Front">
            <div class="input-group-append">
              <button type="button" class="btn btn-primary">Search</button>
            </div>
          </form>
          <!-- End Input -->

          <!-- Content -->
          <div class="row d-none d-md-flex mt-7">
            <div class="col-sm-6">
              <strong class="d-block mb-2">Quick Links</strong>

              <div class="row">
                <!-- List Group -->
                <div class="col-6">
                  <div class="list-group list-group-transparent list-group-flush list-group-borderless">
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Search Results List
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Search Results Grid
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      About
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Services
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Invoice
                    </a>
                  </div>
                </div>
                <!-- End List Group -->

                <!-- List Group -->
                <div class="col-6">
                  <div class="list-group list-group-transparent list-group-flush list-group-borderless">
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Profile
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      User Contacts
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Reviews
                    </a>
                    <a class="list-group-item list-group-item-action" href="#">
                      <span class="fas fa-angle-right list-group-icon"></span>
                      Settings
                    </a>
                  </div>
                </div>
                <!-- End List Group -->
              </div>
            </div>

            <div class="col-sm-6">
              <!-- Banner -->
              <div class="rounded u-search-push-top__banner">
                <div class="d-flex align-items-center">
                  <div class="u-search-push-top__banner-container">
                    <img class="img-fluid u-search-push-top__banner-img" src="<?php echo base_url(); ?>front/assets/img/mockups/img3.png" alt="Image Description">
                    <img class="img-fluid u-search-push-top__banner-img" src="<?php echo base_url(); ?>front/assets/img/mockups/img2.png" alt="Image Description">
                  </div>

                  <div>
                    <div class="mb-4">
                      <strong class="d-block mb-2">Featured Item</strong>
                      <p>Create astonishing web sites and pages.</p>
                    </div>
                    <a class="btn btn-xs btn-soft-success transition-3d-hover" href="index.html">Apply Now <span class="fas fa-angle-right ml-2"></span></a>
                  </div>
                </div>
              </div>
              <!-- End Banner -->
            </div>
          </div>
          <!-- End Content -->
        </div>
      </div>
    </div>
    <!-- End Search -->

    <div class="container u-header__hide-content pt-2">
      <div class="d-flex align-items-center">
        <!-- Language
        <div class="position-relative">
          <a id="languageDropdownInvoker" class="dropdown-nav-link dropdown-toggle d-flex align-items-center" href="javascript:;" role="button" aria-controls="languageDropdown" aria-haspopup="true" aria-expanded="false" data-unfold-event="hover" data-unfold-target="#languageDropdown" data-unfold-type="css-animation" data-unfold-duration="300" data-unfold-delay="300" data-unfold-hide-on-scroll="true" data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
            <img class="dropdown-item-icon" src="<?php // echo base_url(); ?>front/assets/vendor/flag-icon-css/flags/4x3/us.svg" alt="SVG">
            <span class="d-inline-block d-sm-none">US</span>
            <span class="d-none d-sm-inline-block">United States</span>
          </a>

          <div id="languageDropdown" class="dropdown-menu dropdown-unfold" aria-labelledby="languageDropdownInvoker">
            <a class="dropdown-item active" href="#">English</a>
            <a class="dropdown-item" href="#">Deutsch</a>
            <a class="dropdown-item" href="#">Español‎</a>
          </div>
        </div>
        End Language -->

        <div class="ml-auto">
          <!-- Jump To -->
          <div class="d-inline-block d-sm-none position-relative mr-2">
            <a id="jumpToDropdownInvoker" class="dropdown-nav-link dropdown-toggle d-flex align-items-center" href="javascript:;" role="button" aria-controls="jumpToDropdown" aria-haspopup="true" aria-expanded="false" data-unfold-event="hover" data-unfold-target="#jumpToDropdown" data-unfold-type="css-animation" data-unfold-duration="300" data-unfold-delay="300" data-unfold-hide-on-scroll="true" data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
              Jump to
            </a>

            <div id="jumpToDropdown" class="dropdown-menu dropdown-unfold" aria-labelledby="jumpToDropdownInvoker">
              <a class="dropdown-item" href="<?php echo base_url(); ?>faq">Help</a>
              <a class="dropdown-item" href="#SVGFooter">Contacts</a>
            </div>
          </div>
          <!-- End Jump To -->

          <!-- Links -->
          <div class="d-none d-sm-inline-block ml-sm-auto">
            <ul class="list-inline mb-0">
              <li class="list-inline-item mr-0">
                <a class="u-header__navbar-link" href="<?php echo base_url(); ?>faq">Help</a>
              </li>
              <li class="list-inline-item mr-0">
                <a class="u-header__navbar-link" href="#SVGFooter">Contacts</a>
              </li>
            </ul>
          </div>
          <!-- End Links -->
        </div>

        <ul class="list-inline ml-2 mb-0">
          <!-- Search -->
          <li class="list-inline-item">
            <a class="btn btn-xs btn-icon btn-text-secondary" href="javascript:;" role="button" aria-haspopup="true" aria-expanded="false" aria-controls="searchPushTop" data-unfold-type="jquery-slide" data-unfold-target="#searchPushTop">
              <span class="fas fa-search btn-icon__inner"></span>
            </a>
          </li>
          <!-- End Search -->

          <!-- Shopping Cart
          <li class="list-inline-item position-relative">
            <a id="shoppingCartDropdownInvoker" class="btn btn-xs btn-icon btn-text-secondary" href="javascript:;" role="button" aria-controls="shoppingCartDropdown" aria-haspopup="true" aria-expanded="false" data-unfold-event="hover" data-unfold-target="#shoppingCartDropdown" data-unfold-type="css-animation" data-unfold-duration="300" data-unfold-delay="300" data-unfold-hide-on-scroll="true" data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
              <span class="fas fa-shopping-cart btn-icon__inner"></span>
            </a>

            <div id="shoppingCartDropdown" class="dropdown-menu dropdown-unfold dropdown-menu-right text-center p-7" aria-labelledby="shoppingCartDropdownInvoker" style="min-width: 250px;">
              <span class="btn btn-icon btn-soft-primary rounded-circle mb-3">
                <span class="fas fa-shopping-basket btn-icon__inner"></span>
              </span>
              <span class="d-block">Your Cart is Empty</span>
            </div>
          </li>
          End Shopping Cart -->

          <!-- Account Login -->
          <li class="list-inline-item">
            <!-- Account Sidebar Toggle Button -->
            <a id="sidebarNavToggler" class="btn btn-xs btn-text-secondary u-sidebar--account__toggle-bg ml-1" href="javascript:;" role="button" aria-controls="sidebarContent" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent" data-unfold-type="css-animation" data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">

              <?php
              if(!$ses) {
                echo'
                <span class="position-relative">
                  <span class="u-sidebar--account__toggle-text">Login</span>
                  <img class="u-sidebar--account__toggle-img" src="https://web.wonosobokab.go.id/front/assets/svg/icons/icon-4.svg" alt="Image Login">
                </span>
                ';
              }else{
                echo'
                <span class="position-relative">
                  <span class="u-sidebar--account__toggle-text">Natalie Curtis</span>
                  <img class="u-sidebar--account__toggle-img" src="'.base_url().'front/assets/img/100x100/img1.jpg" alt="Image Description">
                  <span class="badge badge-sm badge-success badge-pos rounded-circle">3</span>
                </span>
                ';
              }
              ?>

            </a>
            <!-- End Account Sidebar Toggle Button -->
          </li>
          <!-- End Account Login -->
        </ul>
      </div>
    </div>
    <!-- End Topbar -->

    <div id="logoAndNav" class="container">
      <div class="u-header__section u-header--floating__inner">
        <!-- Nav -->
        <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">
          <!-- Responsive Toggle Button -->
          <button type="button" class="navbar-toggler btn u-hamburger" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
            <span id="hamburgerTrigger" class="u-hamburger__box">
              <span class="u-hamburger__inner"></span>
            </span>
          </button>
          <!-- End Responsive Toggle Button -->

          <!-- Navigation -->
          <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
            <?php echo '' . $menu_atas . ''; ?>
          </div>
          <!-- End Navigation -->
        </nav>
        <!-- End Nav -->
      </div>
    </div>
  </header>
  <!-- ========== END HEADER ========== -->

  <!-- ========== MAIN CONTENT ========== -->
  <main id="content" role="main">
    <?php
    $where = array(
      'status' => 1,
      'domain' => $web
    );
    $this->db->where($where);
    $this->db->limit(1);
    $this->db->order_by('created_time desc');
    $query1 = $this->db->get('slide_website');
    $a = 0;
    if ($query1->num_rows() == 0) { } else {
      foreach ($query1->result() as $row1) {
        echo
          '
  <div id="SVGGraphicIllustration1" class="d-lg-flex bg-img-hero align-items-lg-center gradient-half-primary-v1 height-lg-100vh" style="background-image: url(' . base_url() . 'media/upload/' . $row1->file_name . ');">
      
      <div class="container space-top-3 space-bottom-2 space-top-md-4 space-bottom-md-3">
        <div class="row align-items-md-center">
          <div class="col-lg-6">
            <!-- Title -->
            <div class="mb-7">
              <h1 class="text-white font-weight-normal">
                We help<br>
                <span class="text-warning">
                  <span class="u-text-animation u-text-animation--typing">win more</span><span class="typed-cursor">|</span>
                </span>
              </h1>
              <p class="lead text-white-70">' . $row1->keterangan . '</p>
            </div>
            <!-- End Title -->

            <a class="btn btn-white btn-wide text-primary transition-3d-hover" href="' . $row1->url_redirection . '">Hire Us</a>
          </div>

          <div class="col-lg-6 d-none d-lg-inline-block">
            <!-- SVG Shapes -->
            <figure class="ie-graphic-illustration-1">
              <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 270 135.5" style="enable-background:new 0 0 270 135.5;" xml:space="preserve" class="injected-svg js-svg-injector" data-img-paths="[
                     {&quot;targetId&quot;: &quot;#SVGgraphicIllustration1Img1&quot;, &quot;newPath&quot;: &quot;'.base_url().'front/assets/img/100x100/img1.jpg&quot;},
                     {&quot;targetId&quot;: &quot;#SVGgraphicIllustration1Img2&quot;, &quot;newPath&quot;: &quot;'.base_url().'front/assets/img/100x100/img2.jpg&quot;},
                     {&quot;targetId&quot;: &quot;#SVGgraphicIllustration1Img3&quot;, &quot;newPath&quot;: &quot;'.base_url().'front/assets/img/160x160/img14.png&quot;},
                     {&quot;targetId&quot;: &quot;#SVGgraphicIllustration1Img4&quot;, &quot;newPath&quot;: &quot;'.base_url().'front/assets/img/160x160/img18.png&quot;},
                     {&quot;targetId&quot;: &quot;#SVGgraphicIllustration1Img5&quot;, &quot;newPath&quot;: &quot;'.base_url().'front/assets/img/160x160/img13.png&quot;}
                   ]" data-parent="#SVGGraphicIllustration1">
<style type="text/css">
	.graphic-illustration-0{fill:none;}
	.graphic-illustration-1{fill:#FFC107;}
	.graphic-illustration-2{fill:#FFFFFF;}
</style>
<linearGradient id="SVGgraphicIllustration1ID1" gradientUnits="userSpaceOnUse" x1="104.849" y1="61.8361" x2="265" y2="61.8361">
	<stop class="stop-color-white" offset="5.908129e-07" style="stop-color:#FFFFFF;stop-opacity:0"></stop>
	<stop class="stop-color-warning" offset="1" style="stop-color:#FFC107"></stop>
</linearGradient>
<path class="graphic-illustration-0 fill-none" stroke="url(#SVGgraphicIllustration1ID1)" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" opacity=".16" d="M263.5,38c0,0-30.9,1-47.9,26S173,91,150.7,77s-25.9-17.9-44.3-12"></path>
<linearGradient id="SVGgraphicIllustration1ID2" gradientUnits="userSpaceOnUse" x1="24.5" y1="66.5176" x2="270" y2="66.5176">
	<stop class="stop-color-white" offset="5.908129e-07" style="stop-color:#FFFFFF;stop-opacity:0"></stop>
	<stop class="stop-color-danger" offset="1" style="stop-color:#DE4437"></stop>
</linearGradient>
<path class="graphic-illustration-0 fill-none" stroke="url(#SVGgraphicIllustration1ID2)" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M26,128c10.1,2.1,20.6-1.1,30.5-3c8.7-1.7,17.6-2.5,26.5-3c11.6-0.6,23.2-0.1,34,4.5c0.3,0.1,0.6,0.3,0.9,0.4  c7.9,3.1,17.1,2.2,24.2-2.5c5.3-3.5,9.4-8.5,15.3-11.1c5.2-2.3,11-3,16.6-3.3c8.8-0.5,16.4,1.5,23,7.5c4.8,4.3,9,9.7,15.1,12.3  c5.7,2.4,12.2,2.2,17.9,0.2c3.9-1.4,7.2-4,9.9-7.2c10.8-12.9,12.7-33,13.8-49.1c0.7-9.9,1.3-19.9,0.3-29.8c-0.6-5.6-1-10.8,0-16.4  c1.8-10,7.1-19,14.4-26"></path>
<linearGradient id="SVGgraphicIllustration1ID7" gradientUnits="userSpaceOnUse" x1="0" y1="83.4264" x2="265.5" y2="83.4264">
	<stop class="stop-color-white" offset="3.954259e-07" style="stop-color:#FFFFFF;stop-opacity:0"></stop>
	<stop class="stop-color-warning" offset="1" style="stop-color:#FFC107"></stop>
</linearGradient>
<path id="SVGgraphicIllustration1ID8" class="graphic-illustration-0 fill-none" stroke="url(#SVGgraphicIllustration1ID7)" stroke-width="3" stroke-linecap="round" stroke-miterlimit="10" d="M1.5,134c21.8-0.7,44.8-4.5,63.9-18.3c10.7-7.8,18.2-18.2,24.9-30.9c6.5-12.4,15.5-23,28-24.6  c18.8-2.5,33.5,15.5,51.9,16.8c1.9,0.1,3.7,0.1,5.6-0.1c5.2-0.5,10.1-2.3,14.4-5.3c4.1-2.8,7.3-6.9,10.1-11  c6.3-9.1,14.6-16.9,24.5-21.8c7.4-3.6,15.5-5.8,23.7-6.1c5.2-0.2,10.4,0.6,15.5,1.1"></path>';
$whereshortcut = array(
  'shortcut.status' => 1,
  'attachment.table_name' => 'shortcut'
);
$this->db->select("
shortcut.shortcut_id,
shortcut.shortcut_code,
shortcut.information,
attachment.id_tabel,
attachment.file_name
");
$this->db->where($whereshortcut);
$this->db->join('attachment', 'attachment.id_tabel=shortcut.shortcut_id');
$this->db->order_by('shortcut.created_time desc');
$this->db->limit('5');
$queryshortcut = $this->db->get('shortcut');
if ($queryshortcut->num_rows() == 0) { } else {
  $a=0;
  foreach ($queryshortcut->result() as $rowshortcut) {
    if ($a=1) {
      echo '
      <g>
        <path class="graphic-illustration-1 fill-warning" opacity=".1" d="M236,83.8L236,83.8c-7.7,0-14-6.3-14-14v0c0-7.7,6.3-14,14-14h0c7.7,0,14,6.3,14,14v0   C250,77.5,243.7,83.8,236,83.8z"></path>
        <!-- Apply your (160px width to 160px height) image here -->
        <image id="SVGgraphicIllustration1Img3" style="overflow:visible;" width="160" height="160" xlink:href="' . base_url() . 'media/upload/' . $rowshortcut->file_name . '" transform="matrix(0.1187 0 0 0.1187 226.5 60.25)"></image>
      </g>
     ';
    }elseif ($a=2) {
      echo '
      <g>
        <path class="graphic-illustration-2 fill-white" d="M11,127.5L11,127.5c-4.7,0-8.5-3.8-8.5-8.5v0c0-4.7,3.8-8.5,8.5-8.5h0c4.7,0,8.5,3.8,8.5,8.5v0   C19.5,123.7,15.7,127.5,11,127.5z"></path>
        <!-- Apply your (160px width to 160px height) image here -->
        <image id="SVGgraphicIllustration1Img5" style="overflow:visible;" width="160" height="160" xlink:href="'.base_url().'media/upload/' . $rowshortcut->file_name . '" transform="matrix(5.625000e-02 0 0 5.625000e-02 6.5 114.5)"></image>
      </g>
     ';
    }elseif ($a=3) {
      echo'
      <g>
        <defs>
          <path id="SVGgraphicIllustration1ID3" d="M56.3,86.5L56.3,86.5c-9.5,0-17.3-7.7-17.3-17.2v0C39,59.7,46.7,52,56.2,52h0c9.5,0,17.2,7.7,17.2,17.2v0    C73.5,78.8,65.8,86.5,56.3,86.5z"></path>
        </defs>
        <clipPath id="SVGgraphicIllustration1ID4">
          <use xlink:href="#SVGgraphicIllustration1ID3" style="overflow:visible;"></use>
        </clipPath>
        <g transform="matrix(1 0 0 1 0 -3.814697e-06)" style="clip-path:url(#SVGgraphicIllustration1ID4);">
          <!-- Apply your (100px width to 100px height) image here -->
          <image id="SVGgraphicIllustration1Img1" style="overflow:visible;" width="100" height="100" xlink:href="'.base_url().'media/upload/' . $rowshortcut->file_name . '" transform="matrix(0.3666 0 0 0.3666 37.9219 50.5935)"></image>
        </g>
        <use xlink:href="#SVGgraphicIllustration1ID3" style="overflow:visible;fill:none;stroke:#FFFFFF;stroke-width:1.5;stroke-miterlimit:10;"></use>
      </g>
      ';
    }elseif ($a=4) {
      echo'
      <g>
          <path class="graphic-illustration-2 fill-white" d="M165,59.8L165,59.8c-8.3,0-15-6.7-15-15v0c0-8.3,6.7-15,15-15h0c8.3,0,15,6.7,15,15v0   C180,53,173.3,59.8,165,59.8z"></path>
          <!-- Apply your (160px width to 160px height) image here -->
          <image id="SVGgraphicIllustration1Img4" style="overflow:visible;" width="160" height="160" xlink:href="'.base_url().'media/upload/' . $rowshortcut->file_name . '" transform="matrix(0.1063 0 0 0.1063 156.5 36.25)"></image>
        </g>
      ';
    }else{
      echo'
      <g>
        <defs>
          <path id="SVGgraphicIllustration1ID5" d="M177.8,127.5L177.8,127.5c-8.7,0-15.8-7.1-15.8-15.7v0c0-8.7,7.1-15.7,15.7-15.7h0    c8.7,0,15.7,7.1,15.7,15.7v0C193.5,120.4,186.4,127.5,177.8,127.5z"></path>
        </defs>
        <clipPath id="SVGgraphicIllustration1ID6">
          <use xlink:href="#SVGgraphicIllustration1ID5" style="overflow:visible;"></use>
        </clipPath>
        <g style="clip-path:url(#SVGgraphicIllustration1ID6);">
          <!-- Apply your (100px width to 100px height) image here -->
          <image id="SVGgraphicIllustration1Img2" style="overflow:visible;" width="100" height="100" xlink:href="'.base_url().'media/upload/' . $rowshortcut->file_name . '" transform="matrix(0.3347 0 0 0.3347 161.0156 95.0156)"></image>
        </g>
      ';
    }
 }
}
echo '
	<use xlink:href="#SVGgraphicIllustration1ID5" style="overflow:visible;fill:none;stroke:#FFFFFF;stroke-width:1.5;stroke-miterlimit:10;"></use>
</g>
</svg>
            </figure>
            <!-- End SVG Shapes -->
          </div>
        </div>
      </div>
    </div>
      ';
      }
    }
    ?>
    </div>
    <!-- End Hero Section -->

    <!-- Divider -->
    <div class="container">
      <hr class="my-0">
    </div>
    <!-- End Divider -->

    <!-- Front in Frames Section -->
    <div class="overflow-hidden">
      <div class="container space-2 space-md-3">
        <div class="row justify-content-between align-items-center">
          <div class="col-lg-5 mb-7 mb-lg-0">

            <?php echo '' . $pengumuman . ''; ?>

          </div>

          <div class="col-lg-6 position-relative">
            <!-- Image Gallery -->
            <div class="row mx-gutters-2">
              <?php echo '' . $pengumuman_berita1 . ''; ?>

              <!-- SVG Background Shape -->
              <div id="SVGbgShapeID1" class="svg-preloader w-100 content-centered-y z-index-n1">
                <figure class="ie-soft-triangle-shape">
                  <img class="js-svg-injector" src="<?php echo base_url(); ?>front/assets/svg/components/soft-triangle-shape.svg" alt="Image Description" data-parent="#SVGbgShapeID1">
                </figure>
              </div>
              <!-- End SVG Background Shape -->
            </div>
          </div>
        </div>
      </div>
      <!-- End Front in Frames Section -->

      <!-- Team Section -->
      <div class="container space-2 space-md-3">
        <!-- Title -->
        <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
          <span class="btn btn-xs btn-soft-success btn-pill mb-2">Berita</span>
          <h2 class="text-primary">Berita Terbaru di <span class="font-weight-semi-bold"><?php if (!empty($title)) {
                                                                                            echo $title;
                                                                                          } ?></span></h2>
        </div>
        <!-- End Title -->

        <!-- Slick Carousel -->
        <div class="js-slick-carousel u-slick u-slick--gutters-3" data-slides-show="2" data-slides-scroll="1" data-pagi-classes="text-center u-slick__pagination mt-7 mb-0" data-responsive='[{
             "breakpoint": 992,
             "settings": {
               "slidesToShow": 1
             }
           }, {
             "breakpoint": 768,
             "settings": {
               "slidesToShow": 1
             }
           }, {
             "breakpoint": 554,
             "settings": {
               "slidesToShow": 1
             }
           }]'>

          <?php echo '' . $highlight . ''; ?>

        </div>
        <!-- End Slick Carousel -->
        <!-- Title -->
        <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
          <p></p>
          <a class="btn btn-sm btn-soft-primary btn-pill mb-2 transition-3d-hover" href="<?php echo base_url(); ?>sitemap_berita">Sitemap Berita <span class="fas fa-angle-right ml-2"></span></a>
        </div>
        <!-- End Title -->
      </div>
      <!-- End Team Section -->

      <!-- Houses Section -->
      <div class="bg-light text-center space-2 space-md-3">
        <div class="w-md-60 text-center mx-auto mb-6">
          <h2 class="font-weight-medium">Gabung yuk! <span class="text-primary font-weight-semi-bold">35,000</span> Video WEB TV Kabupaten Wonosobo</h2>
        </div>
        <!-- End Title -->

        <div class="mb-6">
          <a class="btn btn-primary btn-wide transition-3d-hover" href="https://www.youtube.com/c/OfficialWonosoboTV">Kunjungi Sekarang</a>
        </div>
      </div>
      <div class="container text-center space-2">
        <!-- End CTA -->
        <hr class="my-0">
        <div id="SVGwave2Shape" class="position-relative" style="">
          <!-- Video Content -->
          <div class="bg-img-hero text-center space-4" style="background-image: url(<?php echo base_url(); ?>front/assets/img/1920x800/img4.jpg);">
            <!-- Fancybox -->
            <a class="js-fancybox u-media-player mb-4" href="javascript:;" data-src="https://www.youtube.com/watch?v=d9mUBqtp6zk" data-speed="700" data-animate-in="zoomIn" data-animate-out="zoomOut" data-caption="Front - Responsive Website Template">
              <span class="u-media-player__icon u-media-player__icon--lg">
                <span class="fas fa-play u-media-player__icon-inner"></span>
              </span>
            </a>
            <!-- End Fancybox -->
            <br>
            <h4 class="d-inline-block text-white mb-0">Video WEB TV Kabupaten Wonosobo</h4>
          </div>
          <!-- End Video Content Section -->

          <!-- SVG Top Shape -->
          <figure class="position-absolute top-0 right-0 left-0">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" x="0px" y="0px" viewBox="0 0 1920 125.7" style="margin-top: -8px; enable-background:new 0 0 1920 125.7;" xml:space="preserve" class="injected-svg js-svg-injector" data-parent="#SVGwave2Shape">
              <style type="text/css">
                .wave-2-top-0 {
                  fill: #FFFFFF;
                }
              </style>
              <path class="wave-2-top-0 fill-white" d="M1920,0v44.2c0,0-451,63.8-960,6.3S0,125.7,0,125.7L0,0L1920,0z"></path>
            </svg>
          </figure>
          <!-- End SVG Top Shape -->

          <!-- SVG Bottom Shape -->
          <figure class="position-absolute right-0 bottom-0 left-0">
            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="none" x="0px" y="0px" viewBox="0 0 1920 125.7" style="margin-bottom: -8px; enable-background:new 0 0 1920 125.7;" xml:space="preserve" class="injected-svg js-svg-injector" data-parent="#SVGwave2Shape">
              <style type="text/css">
                .wave-2-bottom-0 {
                  fill: #FFFFFF;
                }
              </style>
              <path class="wave-2-bottom-0 fill-white" d="M0,125.7V81.5c0,0,451-63.8,960-6.3S1920,0,1920,0v125.7H0z"></path>
            </svg>
          </figure>
          <!-- End SVG Bottom Shape -->
        </div>

      </div>
      <!-- End Houses Section -->

      <!-- Testimonials -->
      <div class="bg-light">
        <div class="container space-2 space-md-3">
          <!-- Title -->
          <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
            <figure id="icon4" class="svg-preloader ie-height-72 max-width-10 mx-auto mb-3">
              <img class="js-svg-injector" src="<?php echo base_url(); ?>front/assets/svg/icons/icon-4.svg" alt="SVG" data-parent="#icon4">
            </figure>
            <h2 class="font-weight-medium">Pelayanan Publik <span class="font-weight-semi-bold">Kabupaten Wonosobo</span></h2>
            <p>Pemohon Informasi Publik adalah warga negara dan/atau badan hukum Indonesia yang mengajukan permintaan informasi publik.</p>
          </div>
          <!-- End Title -->

          <!-- News Carousel -->
          <div class="js-slick-carousel u-slick u-slick--equal-height u-slick--gutters-2" data-slides-show="4" data-slides-scroll="1" data-pagi-classes="text-center u-slick__pagination mt-7 mb-0" data-responsive='[{
               "breakpoint": 1200,
               "settings": {
                 "slidesToShow": 3
               }
             }, {
               "breakpoint": 992,
               "settings": {
                 "slidesToShow": 2
               }
             }, {
               "breakpoint": 768,
               "settings": {
                 "slidesToShow": 2
               }
             }, {
               "breakpoint": 554,
               "settings": {
                 "slidesToShow": 1
               }
             }]'>
            <!-- Blog Grid -->
            <?php echo '' . $highlight_layanan . ''; ?>
          </div>
          <!-- End News Carousel -->
          <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-9">
            <p></p>
            <a class="btn btn-sm btn-soft-primary btn-pill mb-2 transition-3d-hover" href="<?php echo base_url(); ?>sosegov">Layanan Publik<span class="fas fa-angle-right ml-2"></span></a>
          </div>
          <!-- End Title -->
        </div>
      </div>
      <!-- End Testimonials -->

      <!-- CTA -->
      <div class="container text-center space-2 space-md-3">
        <div class="container space-2 space-md-3">
          <!-- Slick Carousel -->
          <div class="js-slick-carousel u-slick u-slick-zoom u-slick--gutters-3 mb-7" data-slides-show="3" data-pagi-classes="text-center u-slick__pagination mt-7 mb-0" data-responsive='[{
               "breakpoint": 992,
               "settings": {
                 "slidesToShow": 2
               }
             }, {
               "breakpoint": 768,
               "settings": {
                 "slidesToShow": 1
               }
             }]'>
            <div class="js-slide card border-0 shadow-sm mb-3">
              <!-- House Items -->
              <div class="card-body p-0" id="gpr-kominfo-widget-container">
                <script type="text/javascript" src="https://widget.kominfo.go.id/gpr-widget-kominfo.min.js"></script>
                <div id="gpr-kominfo-widget-body">
                  <ul id="gpr-kominfo-widget-list">
                  </ul>
                </div>
              </div>
              <!-- End House Items -->
            </div>
            <div class="js-slide card border-0 shadow-sm mb-3">
              <div class="card-body p-1">

                <div id="fb-root"></div>
                <script async defer crossorigin="anonymous" src="https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v4.0"></script>

                <div class="fb-page" data-href="https://www.facebook.com/diskominfo.wsb/" data-tabs="timeline" data-height="636" data-width="500" data-small-header="false" data-adapt-container-width="true" data-show-facepile="true">
                  <blockquote cite="https://www.facebook.com/diskominfo.wsb/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/diskominfo.wsb/">Diskominfo Wonosobo</a></blockquote>
                </div>
              </div>

              <!-- End House Items -->
            </div>

            <div class="js-slide card border-0 shadow-sm mb-3">
              <!-- House Items -->

              <div class="card-body p-1">
                <a class="twitter-timeline" data-height="636" data-width="500" href="https://twitter.com/diskominfo_wsb?ref_src=twsrc%5Etfw">Tweets by diskominfo_wsb</a>
                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
              </div>
              <!-- End House Items -->
            </div>

            <div class="js-slide card border-0 shadow-sm mb-3">
              <!-- House Items -->
              <div class="position-relative">
                <img class="card-img-top" src="<?php echo base_url(); ?>front/assets/img/500x550/img17.jpg" alt="Image Description">
                <header class="position-absolute top-0 right-0 left-0 p-5">
                  <a class="media align-items-center text-white" href="#">
                    <div class="u-avatar mr-2">
                      <img class="img-fluid rounded-circle" src="<?php echo base_url(); ?>front/assets/img/100x100/img4.jpg" alt="Image Description">
                    </div>
                    <div class="media-body">
                      <span>Elon Fisher</span>
                    </div>
                  </a>
                </header>
                <div class="position-absolute right-0 bottom-0 left-0 p-5">
                  <span class="h4 text-white">$199,000</span>
                </div>
              </div>

              <div class="card-body p-5">
                <h4 class="h6">
                  <a href="#">Tintern Crescent</a>
                </h4>
                <span class="fas fa-map-marker-alt text-danger mr-2"></span>
                <a class="text-secondary" href="#">Toronto, Canada</a>
              </div>
              <!-- End House Items -->
            </div>

            <div class="js-slide card border-0 shadow-sm mb-3">
              <!-- House Items -->
              <div class="position-relative">
                <img class="card-img-top" src="<?php echo base_url(); ?>front/assets/img/500x550/img18.jpg" alt="Image Description">
                <header class="position-absolute top-0 right-0 left-0 p-5">
                  <a class="media align-items-center text-white" href="#">
                    <div class="u-avatar mr-2">
                      <img class="img-fluid rounded-circle" src="<?php echo base_url(); ?>front/assets/img/100x100/img1.jpg" alt="Image Description">
                    </div>
                    <div class="media-body">
                      <span>Maria Muszynska</span>
                    </div>
                  </a>
                </header>
                <div class="position-absolute right-0 bottom-0 left-0 p-5">
                  <span class="h4 text-white">$376,000</span>
                </div>
              </div>

              <div class="card-body p-5">
                <h4 class="h6">
                  <a href="#">Tiverton Avenue</a>
                </h4>
                <span class="fas fa-map-marker-alt text-danger mr-2"></span>
                <a class="text-secondary" href="#">Beijing, China</a>
              </div>
              <!-- End House Items -->
            </div>
          </div>
          <!-- End Slick Carousel -->

        </div>
      </div>

  </main>
  <!-- ========== END MAIN CONTENT ========== -->

  <!-- ========== FOOTER ========== -->
  <footer id="SVGFooter" class="svg-preloader gradient-half-primary-v4 position-relative">
    <div class="container space-bottom-1 position-relative z-index-2">
      <div class="row justify-content-lg-between mb-11">
        <div class="col-lg-5 space-top-2 space-top-lg-3 text-white mb-7 mb-lg-0">
          <!-- Title -->
          <div class="mb-7">
            <h2 class="h1 font-weight-medium">We're here to help</h2>
            <p class="text-white">Find the right solution and get tailored pricing options. Or, find fast answers in our <a class="text-warning font-weight-medium" href="../pages/help.html">Help Center.</a></p>
          </div>
          <!-- End Title -->

          <div class="row">
            <!-- Contacts Info -->
            <div class="col-sm-6 mb-5">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fas fa-envelope btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">General enquiries</h4>
              <a class="text-white-70 font-size-1" href="#"><?php echo $email; ?></a>
            </div>
            <!-- End Contacts Info -->

            <!-- Contacts Info -->
            <div class="col-sm-6 mb-5">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fas fa-phone btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">Phone Number</h4>
              <a class="text-white-70 font-size-1" href="#"><?php echo $telpon; ?></a>
            </div>
            <!-- End Contacts Info -->

            <!-- Contacts Info -->
            <div class="col-sm-6">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fas fa-map-marker-alt btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">Address</h4>
              <a class="text-white-70 font-size-1" href="#"><?php echo $alamat; ?></a>
            </div>
            <!-- End Contacts Info -->
            <!-- Contacts Info -->
            <div class="col-sm-6">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fab fa-apple btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">Media Sosial</h4>
            <ul class="list-inline">
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $facebook;?>">
                    <span class="fab fa-facebook-f btn-icon__inner"></span>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $google;?>">
                    <span class="fab fa-google btn-icon__inner"></span>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $twitter;?>">
                    <span class="fab fa-twitter btn-icon__inner"></span>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $instagram;?>">
                    <span class="fab fa-instagram btn-icon__inner"></span>
                  </a>
                </li>
              </ul>
          </div>
            <!-- End Contacts Info -->
          </div>
        </div>

        <div class="col-lg-6 mt-lg-n11">
          <!-- Contact Form -->
          <form class="js-validate card border-0 shadow-soft p-5">
            <div class="mb-4">
              <h3 class="h5">Drop us a message</h3>
            </div>

            <div class="row mx-gutters-2">
              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">First name</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <input type="text" class="form-control" name="firstName" placeholder="First name" aria-label="First name" required
                           data-msg="Please enter your first name."
                           data-error-class="u-has-error"
                           data-success-class="u-has-success">
                  </div>
                </div>
                <!-- End Input -->
              </div>

              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">Last name</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <input type="text" class="form-control" name="lasstName" placeholder="Last name" aria-label="Last name" required
                           data-msg="Please enter your last name."
                           data-error-class="u-has-error"
                           data-success-class="u-has-success">
                  </div>
                </div>
                <!-- End Input -->
              </div>

              <div class="w-100"></div>

              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">Country</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <select class="form-control custom-select text-muted" required
                            data-msg="Please select country."
                            data-error-class="u-has-error"
                            data-success-class="u-has-success">
                      <option value="">Select country</option>
                      <option value="AF">Afghanistan</option>
                      <option value="AX">Åland Islands</option>
                      <option value="AL">Albania</option>
                      <option value="DZ">Algeria</option>
                      <option value="AS">American Samoa</option>
                      <option value="AD">Andorra</option>
                      <option value="AO">Angola</option>
                      <option value="AI">Anguilla</option>
                      <option value="AQ">Antarctica</option>
                      <option value="AG">Antigua and Barbuda</option>
                      <option value="AR">Argentina</option>
                      <option value="AM">Armenia</option>
                      <option value="AW">Aruba</option>
                      <option value="AU">Australia</option>
                      <option value="AT">Austria</option>
                      <option value="AZ">Azerbaijan</option>
                      <option value="BS">Bahamas</option>
                      <option value="BH">Bahrain</option>
                      <option value="BD">Bangladesh</option>
                      <option value="BB">Barbados</option>
                      <option value="BY">Belarus</option>
                      <option value="BE">Belgium</option>
                      <option value="BZ">Belize</option>
                      <option value="BJ">Benin</option>
                      <option value="BM">Bermuda</option>
                      <option value="BT">Bhutan</option>
                      <option value="BO">Bolivia, Plurinational State of</option>
                      <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                      <option value="BA">Bosnia and Herzegovina</option>
                      <option value="BW">Botswana</option>
                      <option value="BV">Bouvet Island</option>
                      <option value="BR">Brazil</option>
                      <option value="IO">British Indian Ocean Territory</option>
                      <option value="BN">Brunei Darussalam</option>
                      <option value="BG">Bulgaria</option>
                      <option value="BF">Burkina Faso</option>
                      <option value="BI">Burundi</option>
                      <option value="KH">Cambodia</option>
                      <option value="CM">Cameroon</option>
                      <option value="CA">Canada</option>
                      <option value="CV">Cape Verde</option>
                      <option value="KY">Cayman Islands</option>
                      <option value="CF">Central African Republic</option>
                      <option value="TD">Chad</option>
                      <option value="CL">Chile</option>
                      <option value="CN">China</option>
                      <option value="CX">Christmas Island</option>
                      <option value="CC">Cocos (Keeling) Islands</option>
                      <option value="CO">Colombia</option>
                      <option value="KM">Comoros</option>
                      <option value="CG">Congo</option>
                      <option value="CD">Congo, the Democratic Republic of the</option>
                      <option value="CK">Cook Islands</option>
                      <option value="CR">Costa Rica</option>
                      <option value="CI">Côte d'Ivoire</option>
                      <option value="HR">Croatia</option>
                      <option value="CU">Cuba</option>
                      <option value="CW">Curaçao</option>
                      <option value="CY">Cyprus</option>
                      <option value="CZ">Czech Republic</option>
                      <option value="DK">Denmark</option>
                      <option value="DJ">Djibouti</option>
                      <option value="DM">Dominica</option>
                      <option value="DO">Dominican Republic</option>
                      <option value="EC">Ecuador</option>
                      <option value="EG">Egypt</option>
                      <option value="SV">El Salvador</option>
                      <option value="GQ">Equatorial Guinea</option>
                      <option value="ER">Eritrea</option>
                      <option value="EE">Estonia</option>
                      <option value="ET">Ethiopia</option>
                      <option value="FK">Falkland Islands (Malvinas)</option>
                      <option value="FO">Faroe Islands</option>
                      <option value="FJ">Fiji</option>
                      <option value="FI">Finland</option>
                      <option value="FR">France</option>
                      <option value="GF">French Guiana</option>
                      <option value="PF">French Polynesia</option>
                      <option value="TF">French Southern Territories</option>
                      <option value="GA">Gabon</option>
                      <option value="GM">Gambia</option>
                      <option value="GE">Georgia</option>
                      <option value="DE">Germany</option>
                      <option value="GH">Ghana</option>
                      <option value="GI">Gibraltar</option>
                      <option value="GR">Greece</option>
                      <option value="GL">Greenland</option>
                      <option value="GD">Grenada</option>
                      <option value="GP">Guadeloupe</option>
                      <option value="GU">Guam</option>
                      <option value="GT">Guatemala</option>
                      <option value="GG">Guernsey</option>
                      <option value="GN">Guinea</option>
                      <option value="GW">Guinea-Bissau</option>
                      <option value="GY">Guyana</option>
                      <option value="HT">Haiti</option>
                      <option value="HM">Heard Island and McDonald Islands</option>
                      <option value="VA">Holy See (Vatican City State)</option>
                      <option value="HN">Honduras</option>
                      <option value="HK">Hong Kong</option>
                      <option value="HU">Hungary</option>
                      <option value="IS">Iceland</option>
                      <option value="IN">India</option>
                      <option value="ID">Indonesia</option>
                      <option value="IR">Iran, Islamic Republic of</option>
                      <option value="IQ">Iraq</option>
                      <option value="IE">Ireland</option>
                      <option value="IM">Isle of Man</option>
                      <option value="IL">Israel</option>
                      <option value="IT">Italy</option>
                      <option value="JM">Jamaica</option>
                      <option value="JP">Japan</option>
                      <option value="JE">Jersey</option>
                      <option value="JO">Jordan</option>
                      <option value="KZ">Kazakhstan</option>
                      <option value="KE">Kenya</option>
                      <option value="KI">Kiribati</option>
                      <option value="KP">Korea, Democratic People's Republic of</option>
                      <option value="KR">Korea, Republic of</option>
                      <option value="KW">Kuwait</option>
                      <option value="KG">Kyrgyzstan</option>
                      <option value="LA">Lao People's Democratic Republic</option>
                      <option value="LV">Latvia</option>
                      <option value="LB">Lebanon</option>
                      <option value="LS">Lesotho</option>
                      <option value="LR">Liberia</option>
                      <option value="LY">Libya</option>
                      <option value="LI">Liechtenstein</option>
                      <option value="LT">Lithuania</option>
                      <option value="LU">Luxembourg</option>
                      <option value="MO">Macao</option>
                      <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                      <option value="MG">Madagascar</option>
                      <option value="MW">Malawi</option>
                      <option value="MY">Malaysia</option>
                      <option value="MV">Maldives</option>
                      <option value="ML">Mali</option>
                      <option value="MT">Malta</option>
                      <option value="MH">Marshall Islands</option>
                      <option value="MQ">Martinique</option>
                      <option value="MR">Mauritania</option>
                      <option value="MU">Mauritius</option>
                      <option value="YT">Mayotte</option>
                      <option value="MX">Mexico</option>
                      <option value="FM">Micronesia, Federated States of</option>
                      <option value="MD">Moldova, Republic of</option>
                      <option value="MC">Monaco</option>
                      <option value="MN">Mongolia</option>
                      <option value="ME">Montenegro</option>
                      <option value="MS">Montserrat</option>
                      <option value="MA">Morocco</option>
                      <option value="MZ">Mozambique</option>
                      <option value="MM">Myanmar</option>
                      <option value="NA">Namibia</option>
                      <option value="NR">Nauru</option>
                      <option value="NP">Nepal</option>
                      <option value="NL">Netherlands</option>
                      <option value="NC">New Caledonia</option>
                      <option value="NZ">New Zealand</option>
                      <option value="NI">Nicaragua</option>
                      <option value="NE">Niger</option>
                      <option value="NG">Nigeria</option>
                      <option value="NU">Niue</option>
                      <option value="NF">Norfolk Island</option>
                      <option value="MP">Northern Mariana Islands</option>
                      <option value="NO">Norway</option>
                      <option value="OM">Oman</option>
                      <option value="PK">Pakistan</option>
                      <option value="PW">Palau</option>
                      <option value="PS">Palestinian Territory, Occupied</option>
                      <option value="PA">Panama</option>
                      <option value="PG">Papua New Guinea</option>
                      <option value="PY">Paraguay</option>
                      <option value="PE">Peru</option>
                      <option value="PH">Philippines</option>
                      <option value="PN">Pitcairn</option>
                      <option value="PL">Poland</option>
                      <option value="PT">Portugal</option>
                      <option value="PR">Puerto Rico</option>
                      <option value="QA">Qatar</option>
                      <option value="RE">Réunion</option>
                      <option value="RO">Romania</option>
                      <option value="RU">Russian Federation</option>
                      <option value="RW">Rwanda</option>
                      <option value="BL">Saint Barthélemy</option>
                      <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                      <option value="KN">Saint Kitts and Nevis</option>
                      <option value="LC">Saint Lucia</option>
                      <option value="MF">Saint Martin (French part)</option>
                      <option value="PM">Saint Pierre and Miquelon</option>
                      <option value="VC">Saint Vincent and the Grenadines</option>
                      <option value="WS">Samoa</option>
                      <option value="SM">San Marino</option>
                      <option value="ST">Sao Tome and Principe</option>
                      <option value="SA">Saudi Arabia</option>
                      <option value="SN">Senegal</option>
                      <option value="RS">Serbia</option>
                      <option value="SC">Seychelles</option>
                      <option value="SL">Sierra Leone</option>
                      <option value="SG">Singapore</option>
                      <option value="SX">Sint Maarten (Dutch part)</option>
                      <option value="SK">Slovakia</option>
                      <option value="SI">Slovenia</option>
                      <option value="SB">Solomon Islands</option>
                      <option value="SO">Somalia</option>
                      <option value="ZA">South Africa</option>
                      <option value="GS">South Georgia and the South Sandwich Islands</option>
                      <option value="SS">South Sudan</option>
                      <option value="ES">Spain</option>
                      <option value="LK">Sri Lanka</option>
                      <option value="SD">Sudan</option>
                      <option value="SR">Suriname</option>
                      <option value="SJ">Svalbard and Jan Mayen</option>
                      <option value="SZ">Swaziland</option>
                      <option value="SE">Sweden</option>
                      <option value="CH">Switzerland</option>
                      <option value="SY">Syrian Arab Republic</option>
                      <option value="TW">Taiwan, Province of China</option>
                      <option value="TJ">Tajikistan</option>
                      <option value="TZ">Tanzania, United Republic of</option>
                      <option value="TH">Thailand</option>
                      <option value="TL">Timor-Leste</option>
                      <option value="TG">Togo</option>
                      <option value="TK">Tokelau</option>
                      <option value="TO">Tonga</option>
                      <option value="TT">Trinidad and Tobago</option>
                      <option value="TN">Tunisia</option>
                      <option value="TR">Turkey</option>
                      <option value="TM">Turkmenistan</option>
                      <option value="TC">Turks and Caicos Islands</option>
                      <option value="TV">Tuvalu</option>
                      <option value="UG">Uganda</option>
                      <option value="UA">Ukraine</option>
                      <option value="AE">United Arab Emirates</option>
                      <option value="GB">United Kingdom</option>
                      <option value="US">United States</option>
                      <option value="UM">United States Minor Outlying Islands</option>
                      <option value="UY">Uruguay</option>
                      <option value="UZ">Uzbekistan</option>
                      <option value="VU">Vanuatu</option>
                      <option value="VE">Venezuela, Bolivarian Republic of</option>
                      <option value="VN">Viet Nam</option>
                      <option value="VG">Virgin Islands, British</option>
                      <option value="VI">Virgin Islands, U.S.</option>
                      <option value="WF">Wallis and Futuna</option>
                      <option value="EH">Western Sahara</option>
                      <option value="YE">Yemen</option>
                      <option value="ZM">Zambia</option>
                      <option value="ZW">Zimbabwe</option>
                    </select>
                  </div>
                </div>
                <!-- End Input -->
              </div>

              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">Email address</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <input type="text" class="form-control" name="email" placeholder="Email address" aria-label="Email address" required
                           data-msg="Please enter a valid email address."
                           data-error-class="u-has-error"
                           data-success-class="u-has-success">
                  </div>
                </div>
                <!-- End Input -->
              </div>

              <div class="w-100"></div>

              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">Company</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <input type="text" class="form-control" name="company" placeholder="Company" aria-label="Company" required
                           data-msg="Please enter company name."
                           data-error-class="u-has-error"
                           data-success-class="u-has-success">
                  </div>
                </div>
                <!-- End Input -->
              </div>

              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">Job title</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <input type="text" class="form-control" name="jobTitle" placeholder="Job title" aria-label="Job title" required
                           data-msg="Please enter a job title."
                           data-error-class="u-has-error"
                           data-success-class="u-has-success">
                  </div>
                </div>
                <!-- End Input -->
              </div>
            </div>

            <!-- Input -->
            <div class="mb-5">
              <label class="sr-only">How can we help you?</label>

              <div class="js-form-message input-group">
                <textarea class="form-control" rows="4" name="description" placeholder="Hi there, I would like to ..." aria-label="Hi there, I would like to ..." required
                          data-msg="Please enter a reason."
                          data-error-class="u-has-error"
                          data-success-class="u-has-success"></textarea>
              </div>
            </div>
            <!-- End Input -->

            <!-- Checkbox -->
            <div class="js-form-message mb-3">
              <div class="custom-control custom-checkbox d-flex align-items-center text-muted">
                <input type="checkbox" class="custom-control-input" id="termsCheckbox" name="termsCheckbox" required
                       data-msg="Please accept our Terms and Conditions."
                       data-error-class="u-has-error"
                       data-success-class="u-has-success">
                <label class="custom-control-label" for="termsCheckbox">
                  <small>
                    I agree to the
                    <a class="link-muted" href="../pages/terms.html">Terms and Conditions</a>
                  </small>
                </label>
              </div>
            </div>
            <!-- End Checkbox -->

            <!-- Checkbox -->
            <div class="js-form-message mb-5">
              <div class="custom-control custom-checkbox d-flex align-items-center text-muted">
                <input type="checkbox" class="custom-control-input" id="newsletterCheckbox" name="newsletterCheckbox" required
                       data-msg="Please accept our Terms and Conditions."
                       data-error-class="u-has-error"
                       data-success-class="u-has-success">
                <label class="custom-control-label" for="newsletterCheckbox">
                  <small>I want to receive Front's Newsletters</small>
                </label>
              </div>
            </div>
            <!-- End Checkbox -->

            <button type="submit" class="btn btn-primary btn-pill btn-wide transition-3d-hover">Submit</button>
          </form>
          <!-- End Contact Form -->
        </div>
      </div>

      <div class="text-center">
        <!-- Logo -->
        <a class="d-inline-flex align-items-center mb-2" href="index.html" aria-label="Front">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="36px" height="36px" viewBox="0 0 46 46" xml:space="preserve" style="margin-bottom: 0;">
            <path fill="#FFFFFF" opacity=".65" d="M23,41L23,41c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18h11.3C38,5,41,8,41,11.7V23C41,32.9,32.9,41,23,41z"/>
            <path class="fill-white" opacity=".5" d="M28,35.9L28,35.9c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18l11.3,0C43,0,46,3,46,6.6V18C46,27.9,38,35.9,28,35.9z"/>
            <path class="fill-white" opacity=".7" d="M18,46L18,46C8,46,0,38,0,28v0c0-9.9,8-18,18-18h11.3c3.7,0,6.6,3,6.6,6.6V28C35.9,38,27.9,46,18,46z"/>
            <path class="fill-primary" d="M17.4,34V18.3h10.2v2.9h-6.4v3.4h4.8v2.9h-4.8V34H17.4z"/>
          </svg>
          <span class="brand brand-light">Front</span>
        </a>
        <!-- End Logo -->

        <p class="small text-white-70">&copy; <?php echo $web; ?>. All rights reserved.</p>
      </div>
    </div>

    <!-- SVG Background Shape -->
    <figure class="w-100 position-absolute bottom-0 left-0">
      <img class="js-svg-injector" src="<?php echo base_url(); ?>front/assets/svg/illustrations/isometric-squares.svg" alt="Image Description"
           data-parent="#SVGFooter">
    </figure>
    <!-- End SVG Background Shape -->
  </footer>
  <!-- ========== END FOOTER ========== -->

<!-- ========== SECONDARY CONTENTS ========== -->
<!-- Account Sidebar Navigation -->
<aside id="sidebarContent" class="u-sidebar" aria-labelledby="sidebarNavToggler">
  <div class="u-sidebar__scroller">
    <div class="u-sidebar__container">
      <?php
      if(!$ses) {
      ?>
      <div class="u-header-sidebar__footer-offset">
        <!-- Toggle Button -->
        <div class="d-flex align-items-center pt-4 px-7">
          <button type="button" class="close ml-auto"
                  aria-controls="sidebarContent"
                  aria-haspopup="true"
                  aria-expanded="false"
                  data-unfold-event="click"
                  data-unfold-hide-on-scroll="false"
                  data-unfold-target="#sidebarContent"
                  data-unfold-type="css-animation"
                  data-unfold-animation-in="fadeInRight"
                  data-unfold-animation-out="fadeOutRight"
                  data-unfold-duration="500">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <!-- End Toggle Button -->

        <!-- Content -->
        <div class="js-scrollbar u-sidebar__body">
          <div class="u-sidebar__content u-header-sidebar__content">
            <form class="js-validate" action="#" method="post">
              <!-- Login -->
              <div id="login" data-target-group="idForm">
                <!-- Title -->
                <header class="text-center mb-7">
                  <h2 class="h4 mb-0">Selamat Datang di <span style="text-transform: uppercase;"><?php $aa=explode(".",$web);echo $aa[0]; ?></span>!</h2>
                  <p>Integrated Sosial Media Layanan Publik Kabupaten Wonosobo dengan sistem Integrasi artinya dapat posting di semua website se Kabupaten Wonosobo </p>
                </header>
                <!-- End Title -->

                <div class="alert alert-danger alert-dismissable" id="tunggu_redirect" style="display:none;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-ban"></i> Mohon Tunggu !</h4>
                  <div id="error2"></div>
                  Anda akan diarahkan ke dashboard
                </div>
                <div class="alert alert-warning alert-dismissable" id="loading_login" style="display:none;">
                  <h4><i class="fa fa-refresh fa-spin"></i> Mohon tunggu....</h4>
                </div>
                <div class="alert alert-danger alert-dismissable" id="login_error" style="display:none;">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  <h4><i class="icon fa fa-ban"></i> Error !</h4>
                  <div id="pesan_error"></div>
                </div>
                <!-- Form Group -->
                <div class="form-group">
                  <div class="js-form-message js-focus-state">
                    <label class="sr-only" for="signinEmail">Email</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="signinEmailLabel">
                          <span class="fas fa-user"></span>
                        </span>
                      </div>
                      <input type="email" class="form-control" name="user_name" id="user_name" placeholder="Email" aria-label="Email" aria-describedby="signinEmailLabel" required
                             data-msg="Please enter a valid email address."
                             data-error-class="u-has-error"
                             data-success-class="u-has-success">
                    </div>
                  </div>
                </div>
                <!-- End Form Group -->

                <!-- Form Group -->
                <div class="form-group">
                  <div class="js-form-message js-focus-state">
                    <label class="sr-only" for="signinPassword">Password</label>
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text" id="signinPasswordLabel">
                          <span class="fas fa-lock"></span>
                        </span>
                      </div>
                      <input type="password" class="form-control" name="password" id="password" placeholder="Password" aria-label="Password" aria-describedby="signinPasswordLabel" required
                             data-msg="Your password is invalid. Please try again."
                             data-error-class="u-has-error"
                             data-success-class="u-has-success">
                    </div>
                  </div>
                </div>
                <!-- End Form Group -->

                <div class="d-flex justify-content-end mb-4">
                  <a class="js-animation-link small link-muted" href="javascript:;"
                     data-target="#forgotPassword"
                     data-link-group="idForm"
                     data-animation-in="slideInUp">Forgot Password?</a>
                </div>

                <div class="mb-2">
                  <button type="submit" class="btn btn-block btn-sm btn-primary transition-3d-hover" id="login_submit">Login</button>
                </div>

                <div class="text-center mb-4">
                  <span class="small text-muted">Do not have an account?</span>
                  <a class="js-animation-link small" href="javascript:;"
                     data-target="#signup"
                     data-link-group="idForm"
                     data-animation-in="slideInUp">Signup
                  </a>
                </div>

                <div class="text-center">
                  <span class="u-divider u-divider--xs u-divider--text mb-4">OR</span>
                </div>

                <!-- Login Buttons -->
                <div class="d-flex">
                  <a class="btn btn-block btn-sm btn-soft-facebook transition-3d-hover mr-1" href="#">
                    <span class="fab fa-facebook-square mr-1"></span>
                    Facebook
                  </a>
                  <a class="btn btn-block btn-sm btn-soft-google transition-3d-hover ml-1 mt-0" href="#">
                    <span class="fab fa-google mr-1"></span>
                    Google
                  </a>
                </div>
                <!-- End Login Buttons -->
              </div>

            </form>
          </div>
        </div>
        <!-- End Content -->
      </div>

      <?php
      }else{
        ?>
        <div class="u-sidebar--account__footer-offset">
          <!-- Toggle Button -->
          <div class="d-flex justify-content-between align-items-center pt-4 px-7">
            <h3 class="h6 mb-0">My Account</h3>
  
            <button type="button" class="close ml-auto" aria-controls="sidebarContent" aria-haspopup="true" aria-expanded="false" data-unfold-event="click" data-unfold-hide-on-scroll="false" data-unfold-target="#sidebarContent" data-unfold-type="css-animation" data-unfold-animation-in="fadeInRight" data-unfold-animation-out="fadeOutRight" data-unfold-duration="500">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <!-- End Toggle Button -->
  
          <!-- Content -->
          <div class="js-scrollbar u-sidebar__body">
            <!-- Holder Info -->
            <header class="d-flex align-items-center u-sidebar--account__holder mt-3">
              <div class="position-relative">
                <img class="u-sidebar--account__holder-img" src="<?php echo base_url(); ?>front/assets/img/100x100/img1.jpg" alt="Image Description">
                <span class="badge badge-xs badge-outline-success badge-pos rounded-circle"></span>
              </div>
              <div class="ml-3">
                <span class="font-weight-semi-bold">Natalie Curtis <span class="badge badge-success ml-1">Pro</span></span>
                <span class="u-sidebar--account__holder-text">Lead Support Adviser</span>
              </div>
  
              <!-- Settings -->
              <div class="btn-group position-relative ml-auto mb-auto">
                <a id="sidebar-account-settings-invoker" class="btn btn-xs btn-icon btn-text-secondary rounded" href="javascript:;" role="button" aria-controls="sidebar-account-settings" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown" data-unfold-event="click" data-unfold-target="#sidebar-account-settings" data-unfold-type="css-animation" data-unfold-duration="300" data-unfold-delay="300" data-unfold-animation-in="slideInUp" data-unfold-animation-out="fadeOut">
                  <span class="fas fa-ellipsis-v btn-icon__inner"></span>
                </a>
  
                <div id="sidebar-account-settings" class="dropdown-menu dropdown-unfold dropdown-menu-right" aria-labelledby="sidebar-account-settings-invoker">
                  <a class="dropdown-item" href="#">Settings</a>
                  <a class="dropdown-item" href="#">History</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?php echo base_url(); ?>sosegov/logout">Sign Out</a>
                </div>
              </div>
              <!-- End Settings -->
            </header>
            <!-- End Holder Info -->
  
            <div class="u-sidebar__content--account">
              <!-- List Links -->
              <ul class="list-unstyled u-sidebar--account__list">
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="dashboard.html">
                    <span class="fas fa-home u-sidebar--account__list-icon mr-2"></span>
                    Dashboard
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="profile.html">
                    <span class="fas fa-user-circle u-sidebar--account__list-icon mr-2"></span>
                    Profile
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="my-tasks.html">
                    <span class="fas fa-tasks u-sidebar--account__list-icon mr-2"></span>
                    My tasks
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="projects.html">
                    <span class="fas fa-layer-group u-sidebar--account__list-icon mr-2"></span>
                    Projects <span class="badge badge-danger float-right mt-1">3</span>
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="members.html">
                    <span class="fas fa-users u-sidebar--account__list-icon mr-2"></span>
                    Members
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="activity.html">
                    <span class="fas fa-exchange-alt u-sidebar--account__list-icon mr-2"></span>
                    Activity
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="payment-methods.html">
                    <span class="fas fa-wallet u-sidebar--account__list-icon mr-2"></span>
                    Payment methods
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="plans.html">
                    <span class="fas fa-cubes u-sidebar--account__list-icon mr-2"></span>
                    Plans
                  </a>
                </li>
              </ul>
              <!-- End List Links -->
  
              <div class="u-sidebar--account__list-divider"></div>
  
              <!-- List Links -->
              <ul class="list-unstyled u-sidebar--account__list">
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="invite-friends.html">
                    <span class="fas fa-user-plus u-sidebar--account__list-icon mr-2"></span>
                    Invite friends
                  </a>
                </li>
                <li class="u-sidebar--account__list-item">
                  <a class="u-sidebar--account__list-link" href="api-token.html">
                    <span class="fas fa-key u-sidebar--account__list-icon mr-2"></span>
                    API Token
                  </a>
                </li>
              </ul>
              <!-- End List Links -->
            </div>
          </div>
        </div>
        <?php
      }
      ?>
      <!-- Footer -->
      <footer id="SVGwaveWithDots" class="svg-preloader u-sidebar__footer u-sidebar__footer--account">
        <ul class="list-inline mb-0">
          <li class="list-inline-item pr-3">
            <a class="u-sidebar__footer--account__text" href="../pages/privacy.html">Privacy</a>
          </li>
          <li class="list-inline-item pr-3">
            <a class="u-sidebar__footer--account__text" href="../pages/terms.html">Terms</a>
          </li>
          <li class="list-inline-item">
            <a class="u-sidebar__footer--account__text" href="../pages/help.html">
              <i class="fas fa-info-circle"></i>
            </a>
          </li>
        </ul>

        <!-- SVG Background Shape -->
        <div class="position-absolute right-0 bottom-0 left-0">
          <img class="js-svg-injector" src="<?php echo base_url(); ?>front/assets/svg/components/wave-bottom-with-dots.svg" alt="Image Description" data-parent="#SVGwaveWithDots">
        </div>
        <!-- End SVG Background Shape -->
      </footer>
      <!-- End Footer -->
    </div>
  </div>
</aside>
<!-- End Account Sidebar Navigation -->
<!-- ========== END SECONDARY CONTENTS ========== -->

  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#" data-position='{"bottom": 15, "right": 15 }' data-type="fixed" data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->

  <!-- JS Global Compulsory -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/bootstrap/bootstrap.min.js"></script>

  <!-- JS Implementing Plugins -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/svg-injector/dist/svg-injector.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/fancybox/jquery.fancybox.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/slick-carousel/slick/slick.js"></script>

  <!-- JS Front -->
  <script src="<?php echo base_url(); ?>front/assets/js/hs.core.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.header.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.validation.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.fancybox.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.slick-carousel.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.svg-injector.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.go-to.js"></script>

  <!-- JS Implementing Plugins -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/flatpickr/dist/flatpickr.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js"></script>

  <!-- JS Front -->
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.unfold.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.malihu-scrollbar.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.focus-state.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.range-datepicker.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.selectpicker.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.datatables.js"></script>

  <!-- JS Plugins Init. -->
  <script>
    $(window).on('load', function() {
      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 767.98,
        hideTimeOut: 0
      });

    // initialization of HSMegaMenu component
    $('.js-breadcrumb-menu').HSMegaMenu({
      event: 'hover',
      pageContainer: $('.container'),
      breakpoint: 991.98,
      hideTimeOut: 0
    });

      // initialization of svg injector module
      $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function() {
      // initialization of header
      $.HSCore.components.HSHeader.init($('#header'));

    // initialization of unfold component
    $.HSCore.components.HSUnfold.init($('[data-unfold-target]'), {
      afterOpen: function() {
        $(this).find('input[type="search"]').focus();
      }
    });

      // initialization of form validation
      $.HSCore.components.HSValidation.init('.js-validate', {
        rules: {
          confirmPassword: {
            equalTo: '#signupPassword'
          }
        }
      });

      // initialization of fancybox
      $.HSCore.components.HSFancyBox.init('.js-fancybox');

      // initialization of slick carousel
      $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

    // initialization of malihu scrollbar
    $.HSCore.components.HSMalihuScrollBar.init($('.js-scrollbar'));

    // initialization of forms
    $.HSCore.components.HSFocusState.init();

      // initialization of go to
      $.HSCore.components.HSGoTo.init('.js-go-to');
    });
  </script>
  
<script type = "text/javascript" >
      $(document).ready(function () {
        $("#login_submit").on("click", function (e) {
          e.preventDefault();
          $('#login_error').hide();
          $('#loading_login').show();
          var user_name = $("#user_name").val();
          var password = $("#password").val();
          $.ajax({
            type: "POST",
            async: true,
            data: {
              user_name: user_name,
              password: password
            },
            dataType: "json",
            url: '<?php echo base_url(); ?>sosegov/proses_login',
            success: function (json) {
              var trHTML = '';
              for (var i = 0; i < json.length; i++) {
                if (json[i].errors == 'form_kosong') {
                  $('#loading_login').fadeOut("slow");
                  $('#login_error').show();
                  $('#pesan_error').html('Mohon isi data secara lengkap');
                  if (json[i].user_name == '') {
                    $('#user_name').css('background-color', '#DFB5B4');
                  } else {
                    $('#user_name').removeAttr('style');
                  }
                  if (json[i].password == '') {
                    $('#password').css('background-color', '#DFB5B4');
                  } else {
                    $('#password').removeAttr('style');
                  }
                } else if (json[i].errors == 'user_tidak_ada') {
                  $('#loading_login').fadeOut("slow");
                  $('#login_error').show();
                  $('#pesan_error').html('Data login anda salah');
                  if (json[i].user_name == '') {
                    $('#user_name').css('background-color', '#DFB5B4');
                  } else {
                    $('#user_name').removeAttr('style');
                  }
                  if (json[i].password == '') {
                    $('#password').css('background-color', '#DFB5B4');
                  } else {
                    $('#password').removeAttr('style');
                  }
                } else if (json[i].errors == 'miss_match') {
                  $('#loading_login').fadeOut("slow");
                  $('#login_error').show();
                  $('#pesan_error').html('Data login anda salah');
                  if (json[i].user_name == '') {
                    $('#user_name').css('background-color', '#DFB5B4');
                  } else {
                    $('#user_name').removeAttr('style');
                  }
                  if (json[i].password == '') {
                    $('#password').css('background-color', '#DFB5B4');
                  } else {
                    $('#password').removeAttr('style');
                  }
                } else {
                  $('#error2').html('Pesan : ' + json[i].errors + '');
                  $('#box_login').hide();
                  $('#tunggu_redirect').show();
                  window.location = "<?php echo base_url(); ?>";
                }
              }
            }
          });
        });
      });
    </script>
</body>

</html>