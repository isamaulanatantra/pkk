<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title -->
  <title><?php if (!empty($title)) {
            echo $title;
          } ?></title>
  <!-- Meta -->
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" />
  <meta name="author" content="<?php if (!empty($title)) {
                                  echo $title;
                                } ?>" />
  <meta name="keywords" content="<?php if (!empty($title)) {
                                    echo $title;
                                  } ?> <?php echo base_url(); ?>" />
  <meta name="og:description" content="<?php if (!empty($title)) {
                                          echo $title;
                                        } ?>" />
  <meta name="og:url" content="<?php echo base_url(); ?> <?php if (!empty($title)) {
                                                            echo $title;
                                                          } ?>" />
  <meta name="og:title" content="<?php if (!empty($title)) {
                                    echo $title;
                                  } ?> <?php echo base_url(); ?>" />
  <meta name="og:keywords" content="<?php if (!empty($title)) {
                                      echo $title;
                                    } ?> <?php echo base_url(); ?>" />
  <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png" />
  <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />

  <!-- Google Fonts -->
  <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/animate.css/animate.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/fancybox/jquery.fancybox.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/slick-carousel/slick/slick.css">

  <!-- CSS Front Template -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/theme.css">
</head>

<body>
  <!-- ========== HEADER ========== -->
  <header id="logoAndNav" class="">
    <div class="u-header__section u-header--floating__inner">
      <div id="logoAndNav" class="container">
        <!-- Nav -->
        <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">
          <!-- Responsive Toggle Button -->
          <button type="button" class="navbar-toggler btn u-hamburger" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
            <span id="hamburgerTrigger" class="u-hamburger__box">
              <span class="u-hamburger__inner"></span>
            </span>
          </button>
          <!-- End Responsive Toggle Button -->

          <!-- Navigation -->
          <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
            <?php echo '' . $menu_atas . ''; ?>
          </div>
          <!-- End Navigation -->
        </nav>
        <!-- End Nav -->
      </div>
    </div>
  </header>
  <!-- ========== END HEADER ========== -->

  <!-- ========== MAIN CONTENT ========== -->
  <main id="content" role="main">
    <div class="container space-2">
    <?php $this->load->view('front_end/house/top');  ?>
    </div>
    <div class="container space-2 space-bottom-lg-3">
    <div class="row">
        <div class="col-md-7 col-lg-8 mb-7 mb-md-0">
          <?php $this->load->view($main_view);  ?>
        </div>

        <div id="stickyBlockStartPoint" class="col-md-5 col-lg-4" style="">
          <div class="js-sticky-block pl-lg-4" data-parent="#stickyBlockStartPoint" data-sticky-view="md" data-start-point="#stickyBlockStartPoint" data-end-point="#stickyBlockEndPoint" data-offset-top="16" data-offset-bottom="16" style="">
            <h3 class="h6 font-weight-semi-bold mb-4">Useful links</h3>

            <!-- List Group -->
            <ul class="list-group list-group-flush list-group-borderless mb-5">
              <li>
                <a class="list-group-item list-group-item-action d-flex align-items-center" href="#">
                  All
                  <span class="badge bg-soft-secondary badge-pill ml-2">30+</span>
                  <small class="fas fa-angle-right ml-auto"></small>
                </a>
              </li>
              <li>
                <a class="list-group-item list-group-item-action d-flex align-items-center" href="#">
                  Top rated
                  <small class="fas fa-angle-right ml-auto"></small>
                </a>
              </li>
              <li>
                <a class="list-group-item list-group-item-action d-flex align-items-center" href="#">
                  Featured
                  <small class="fas fa-angle-right ml-auto"></small>
                </a>
              </li>
              <li>
                <a class="list-group-item list-group-item-action d-flex align-items-center" href="#">
                  Daily news
                  <span class="badge bg-soft-secondary badge-pill ml-2">18</span>
                  <small class="fas fa-angle-right ml-auto"></small>
                </a>
              </li>
              <li>
                <a class="list-group-item list-group-item-action d-flex align-items-center" href="#">
                  New
                  <small class="fas fa-angle-right ml-auto"></small>
                </a>
              </li>
            </ul>
            <!-- End List Group -->

            <div class="border-top pt-5 mt-5">
              <h3 class="h6 font-weight-semi-bold mb-4">Related stories</h3>

              <!-- Blog Card -->
              <article>
                <div class="row justify-content-between">
                  <div class="col-6">
                    <a class="d-inline-block text-muted font-weight-medium text-uppercase small" href="#">Product</a>
                    <h4 class="h6 font-weight-medium mb-0">
                      <a href="single-article.html">InVision design forward fund</a>
                    </h4>
                  </div>

                  <div class="col-5">
                    <img class="img-fluid" src="../../assets/img/500x280/img18.jpg" alt="Image Description">
                  </div>
                </div>
              </article>
              <!-- End Blog Card -->

              <!-- Blog Card -->
              <article class="border-top pt-5 mt-5">
                <div class="row justify-content-between">
                  <div class="col-6">
                    <a class="d-inline-block text-muted font-weight-medium text-uppercase small" href="#">Business</a>
                    <h4 class="h6 font-weight-medium mb-0">
                      <a href="single-article.html">What CFR really is about</a>
                    </h4>
                  </div>

                  <div class="col-5">
                    <img class="img-fluid" src="../../assets/img/500x280/img13.jpg" alt="Image Description">
                  </div>
                </div>
              </article>
              <!-- End Blog Card -->

              <!-- Blog Card -->
              <article class="border-top pt-5 mt-5">
                <div class="row justify-content-between">
                  <div class="col-6">
                    <a class="d-inline-block text-muted font-weight-medium text-uppercase small" href="#">Business</a>
                    <h4 class="h6 font-weight-medium mb-0">
                      <a href="single-article.html">Enjoy $1,000 worth of perks with Front for Business</a>
                    </h4>
                  </div>

                  <div class="col-5">
                    <img class="img-fluid" src="../../assets/img/500x280/img19.jpg" alt="Image Description">
                  </div>
                </div>
              </article>
              <!-- End Blog Card -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>

  <!-- ========== FOOTER ========== -->
  <footer id="SVGFooter" class="svg-preloader gradient-half-primary-v4 position-relative">
    <div class="container space-bottom-1 position-relative z-index-2">
      <div class="row justify-content-lg-between mb-11">
        <div class="col-lg-5 space-top-2 space-top-lg-3 text-white mb-7 mb-lg-0">
          <!-- Title -->
          <div class="mb-7">
            <h2 class="h1 font-weight-medium">Kami di sini untuk membantu</h2>
            <p class="text-white">Temukan solusi yang tepat dan dapatkan opsi harga yang disesuaikan. Atau, cari jawaban cepat di  <a class="text-warning font-weight-medium" href="<?php echo base_url(); ?>help">Pusat Bantuan kami.</a></p>
          </div>
          <!-- End Title -->

          <div class="row">
            <!-- Contacts Info -->
            <div class="col-sm-6 mb-5">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fas fa-envelope btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">General enquiries</h4>
              <a class="text-white-70 font-size-1" href="#"><?php echo $email; ?></a>
            </div>
            <!-- End Contacts Info -->

            <!-- Contacts Info -->
            <div class="col-sm-6 mb-5">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fas fa-phone btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">Phone Number</h4>
              <a class="text-white-70 font-size-1" href="#"><?php echo $telpon; ?></a>
            </div>
            <!-- End Contacts Info -->

            <!-- Contacts Info -->
            <div class="col-sm-6">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fas fa-map-marker-alt btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">Address</h4>
              <a class="text-white-70 font-size-1" href="#"><?php echo $alamat; ?></a>
            </div>
            <!-- End Contacts Info -->
            <!-- Contacts Info -->
            <div class="col-sm-6">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fab fa-apple btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">Media Sosial</h4>
              <ul class="list-inline">
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $facebook; ?>">
                    <span class="fab fa-facebook-f btn-icon__inner"></span>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $google; ?>">
                    <span class="fab fa-google btn-icon__inner"></span>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $twitter; ?>">
                    <span class="fab fa-twitter btn-icon__inner"></span>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $instagram; ?>">
                    <span class="fab fa-instagram btn-icon__inner"></span>
                  </a>
                </li>
              </ul>
            </div>
            <!-- End Contacts Info -->
          </div>
        </div>

        <div class="col-lg-6 mt-lg-n11">
          <!-- Contact Form -->
          <form class="js-validate card border-0 shadow-soft p-5">
            <div class="mb-4">
              <h3 class="h5">Drop us a message</h3>
            </div>

            <div class="row mx-gutters-2">
              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">First name</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <input type="text" class="form-control" name="firstName" placeholder="First name" aria-label="First name" required data-msg="Please enter your first name." data-error-class="u-has-error" data-success-class="u-has-success">
                  </div>
                </div>
                <!-- End Input -->
              </div>

              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">Last name</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <input type="text" class="form-control" name="lasstName" placeholder="Last name" aria-label="Last name" required data-msg="Please enter your last name." data-error-class="u-has-error" data-success-class="u-has-success">
                  </div>
                </div>
                <!-- End Input -->
              </div>

              <div class="w-100"></div>

              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">Country</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <select class="form-control custom-select text-muted" required data-msg="Please select country." data-error-class="u-has-error" data-success-class="u-has-success">
                      <option value="">Select country</option>
                      <option value="AF">Afghanistan</option>
                      <option value="AX">Åland Islands</option>
                      <option value="AL">Albania</option>
                      <option value="DZ">Algeria</option>
                      <option value="AS">American Samoa</option>
                      <option value="AD">Andorra</option>
                      <option value="AO">Angola</option>
                      <option value="AI">Anguilla</option>
                      <option value="AQ">Antarctica</option>
                      <option value="AG">Antigua and Barbuda</option>
                      <option value="AR">Argentina</option>
                      <option value="AM">Armenia</option>
                      <option value="AW">Aruba</option>
                      <option value="AU">Australia</option>
                      <option value="AT">Austria</option>
                      <option value="AZ">Azerbaijan</option>
                      <option value="BS">Bahamas</option>
                      <option value="BH">Bahrain</option>
                      <option value="BD">Bangladesh</option>
                      <option value="BB">Barbados</option>
                      <option value="BY">Belarus</option>
                      <option value="BE">Belgium</option>
                      <option value="BZ">Belize</option>
                      <option value="BJ">Benin</option>
                      <option value="BM">Bermuda</option>
                      <option value="BT">Bhutan</option>
                      <option value="BO">Bolivia, Plurinational State of</option>
                      <option value="BQ">Bonaire, Sint Eustatius and Saba</option>
                      <option value="BA">Bosnia and Herzegovina</option>
                      <option value="BW">Botswana</option>
                      <option value="BV">Bouvet Island</option>
                      <option value="BR">Brazil</option>
                      <option value="IO">British Indian Ocean Territory</option>
                      <option value="BN">Brunei Darussalam</option>
                      <option value="BG">Bulgaria</option>
                      <option value="BF">Burkina Faso</option>
                      <option value="BI">Burundi</option>
                      <option value="KH">Cambodia</option>
                      <option value="CM">Cameroon</option>
                      <option value="CA">Canada</option>
                      <option value="CV">Cape Verde</option>
                      <option value="KY">Cayman Islands</option>
                      <option value="CF">Central African Republic</option>
                      <option value="TD">Chad</option>
                      <option value="CL">Chile</option>
                      <option value="CN">China</option>
                      <option value="CX">Christmas Island</option>
                      <option value="CC">Cocos (Keeling) Islands</option>
                      <option value="CO">Colombia</option>
                      <option value="KM">Comoros</option>
                      <option value="CG">Congo</option>
                      <option value="CD">Congo, the Democratic Republic of the</option>
                      <option value="CK">Cook Islands</option>
                      <option value="CR">Costa Rica</option>
                      <option value="CI">Côte d'Ivoire</option>
                      <option value="HR">Croatia</option>
                      <option value="CU">Cuba</option>
                      <option value="CW">Curaçao</option>
                      <option value="CY">Cyprus</option>
                      <option value="CZ">Czech Republic</option>
                      <option value="DK">Denmark</option>
                      <option value="DJ">Djibouti</option>
                      <option value="DM">Dominica</option>
                      <option value="DO">Dominican Republic</option>
                      <option value="EC">Ecuador</option>
                      <option value="EG">Egypt</option>
                      <option value="SV">El Salvador</option>
                      <option value="GQ">Equatorial Guinea</option>
                      <option value="ER">Eritrea</option>
                      <option value="EE">Estonia</option>
                      <option value="ET">Ethiopia</option>
                      <option value="FK">Falkland Islands (Malvinas)</option>
                      <option value="FO">Faroe Islands</option>
                      <option value="FJ">Fiji</option>
                      <option value="FI">Finland</option>
                      <option value="FR">France</option>
                      <option value="GF">French Guiana</option>
                      <option value="PF">French Polynesia</option>
                      <option value="TF">French Southern Territories</option>
                      <option value="GA">Gabon</option>
                      <option value="GM">Gambia</option>
                      <option value="GE">Georgia</option>
                      <option value="DE">Germany</option>
                      <option value="GH">Ghana</option>
                      <option value="GI">Gibraltar</option>
                      <option value="GR">Greece</option>
                      <option value="GL">Greenland</option>
                      <option value="GD">Grenada</option>
                      <option value="GP">Guadeloupe</option>
                      <option value="GU">Guam</option>
                      <option value="GT">Guatemala</option>
                      <option value="GG">Guernsey</option>
                      <option value="GN">Guinea</option>
                      <option value="GW">Guinea-Bissau</option>
                      <option value="GY">Guyana</option>
                      <option value="HT">Haiti</option>
                      <option value="HM">Heard Island and McDonald Islands</option>
                      <option value="VA">Holy See (Vatican City State)</option>
                      <option value="HN">Honduras</option>
                      <option value="HK">Hong Kong</option>
                      <option value="HU">Hungary</option>
                      <option value="IS">Iceland</option>
                      <option value="IN">India</option>
                      <option value="ID">Indonesia</option>
                      <option value="IR">Iran, Islamic Republic of</option>
                      <option value="IQ">Iraq</option>
                      <option value="IE">Ireland</option>
                      <option value="IM">Isle of Man</option>
                      <option value="IL">Israel</option>
                      <option value="IT">Italy</option>
                      <option value="JM">Jamaica</option>
                      <option value="JP">Japan</option>
                      <option value="JE">Jersey</option>
                      <option value="JO">Jordan</option>
                      <option value="KZ">Kazakhstan</option>
                      <option value="KE">Kenya</option>
                      <option value="KI">Kiribati</option>
                      <option value="KP">Korea, Democratic People's Republic of</option>
                      <option value="KR">Korea, Republic of</option>
                      <option value="KW">Kuwait</option>
                      <option value="KG">Kyrgyzstan</option>
                      <option value="LA">Lao People's Democratic Republic</option>
                      <option value="LV">Latvia</option>
                      <option value="LB">Lebanon</option>
                      <option value="LS">Lesotho</option>
                      <option value="LR">Liberia</option>
                      <option value="LY">Libya</option>
                      <option value="LI">Liechtenstein</option>
                      <option value="LT">Lithuania</option>
                      <option value="LU">Luxembourg</option>
                      <option value="MO">Macao</option>
                      <option value="MK">Macedonia, the former Yugoslav Republic of</option>
                      <option value="MG">Madagascar</option>
                      <option value="MW">Malawi</option>
                      <option value="MY">Malaysia</option>
                      <option value="MV">Maldives</option>
                      <option value="ML">Mali</option>
                      <option value="MT">Malta</option>
                      <option value="MH">Marshall Islands</option>
                      <option value="MQ">Martinique</option>
                      <option value="MR">Mauritania</option>
                      <option value="MU">Mauritius</option>
                      <option value="YT">Mayotte</option>
                      <option value="MX">Mexico</option>
                      <option value="FM">Micronesia, Federated States of</option>
                      <option value="MD">Moldova, Republic of</option>
                      <option value="MC">Monaco</option>
                      <option value="MN">Mongolia</option>
                      <option value="ME">Montenegro</option>
                      <option value="MS">Montserrat</option>
                      <option value="MA">Morocco</option>
                      <option value="MZ">Mozambique</option>
                      <option value="MM">Myanmar</option>
                      <option value="NA">Namibia</option>
                      <option value="NR">Nauru</option>
                      <option value="NP">Nepal</option>
                      <option value="NL">Netherlands</option>
                      <option value="NC">New Caledonia</option>
                      <option value="NZ">New Zealand</option>
                      <option value="NI">Nicaragua</option>
                      <option value="NE">Niger</option>
                      <option value="NG">Nigeria</option>
                      <option value="NU">Niue</option>
                      <option value="NF">Norfolk Island</option>
                      <option value="MP">Northern Mariana Islands</option>
                      <option value="NO">Norway</option>
                      <option value="OM">Oman</option>
                      <option value="PK">Pakistan</option>
                      <option value="PW">Palau</option>
                      <option value="PS">Palestinian Territory, Occupied</option>
                      <option value="PA">Panama</option>
                      <option value="PG">Papua New Guinea</option>
                      <option value="PY">Paraguay</option>
                      <option value="PE">Peru</option>
                      <option value="PH">Philippines</option>
                      <option value="PN">Pitcairn</option>
                      <option value="PL">Poland</option>
                      <option value="PT">Portugal</option>
                      <option value="PR">Puerto Rico</option>
                      <option value="QA">Qatar</option>
                      <option value="RE">Réunion</option>
                      <option value="RO">Romania</option>
                      <option value="RU">Russian Federation</option>
                      <option value="RW">Rwanda</option>
                      <option value="BL">Saint Barthélemy</option>
                      <option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
                      <option value="KN">Saint Kitts and Nevis</option>
                      <option value="LC">Saint Lucia</option>
                      <option value="MF">Saint Martin (French part)</option>
                      <option value="PM">Saint Pierre and Miquelon</option>
                      <option value="VC">Saint Vincent and the Grenadines</option>
                      <option value="WS">Samoa</option>
                      <option value="SM">San Marino</option>
                      <option value="ST">Sao Tome and Principe</option>
                      <option value="SA">Saudi Arabia</option>
                      <option value="SN">Senegal</option>
                      <option value="RS">Serbia</option>
                      <option value="SC">Seychelles</option>
                      <option value="SL">Sierra Leone</option>
                      <option value="SG">Singapore</option>
                      <option value="SX">Sint Maarten (Dutch part)</option>
                      <option value="SK">Slovakia</option>
                      <option value="SI">Slovenia</option>
                      <option value="SB">Solomon Islands</option>
                      <option value="SO">Somalia</option>
                      <option value="ZA">South Africa</option>
                      <option value="GS">South Georgia and the South Sandwich Islands</option>
                      <option value="SS">South Sudan</option>
                      <option value="ES">Spain</option>
                      <option value="LK">Sri Lanka</option>
                      <option value="SD">Sudan</option>
                      <option value="SR">Suriname</option>
                      <option value="SJ">Svalbard and Jan Mayen</option>
                      <option value="SZ">Swaziland</option>
                      <option value="SE">Sweden</option>
                      <option value="CH">Switzerland</option>
                      <option value="SY">Syrian Arab Republic</option>
                      <option value="TW">Taiwan, Province of China</option>
                      <option value="TJ">Tajikistan</option>
                      <option value="TZ">Tanzania, United Republic of</option>
                      <option value="TH">Thailand</option>
                      <option value="TL">Timor-Leste</option>
                      <option value="TG">Togo</option>
                      <option value="TK">Tokelau</option>
                      <option value="TO">Tonga</option>
                      <option value="TT">Trinidad and Tobago</option>
                      <option value="TN">Tunisia</option>
                      <option value="TR">Turkey</option>
                      <option value="TM">Turkmenistan</option>
                      <option value="TC">Turks and Caicos Islands</option>
                      <option value="TV">Tuvalu</option>
                      <option value="UG">Uganda</option>
                      <option value="UA">Ukraine</option>
                      <option value="AE">United Arab Emirates</option>
                      <option value="GB">United Kingdom</option>
                      <option value="US">United States</option>
                      <option value="UM">United States Minor Outlying Islands</option>
                      <option value="UY">Uruguay</option>
                      <option value="UZ">Uzbekistan</option>
                      <option value="VU">Vanuatu</option>
                      <option value="VE">Venezuela, Bolivarian Republic of</option>
                      <option value="VN">Viet Nam</option>
                      <option value="VG">Virgin Islands, British</option>
                      <option value="VI">Virgin Islands, U.S.</option>
                      <option value="WF">Wallis and Futuna</option>
                      <option value="EH">Western Sahara</option>
                      <option value="YE">Yemen</option>
                      <option value="ZM">Zambia</option>
                      <option value="ZW">Zimbabwe</option>
                    </select>
                  </div>
                </div>
                <!-- End Input -->
              </div>

              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">Email address</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <input type="text" class="form-control" name="email" placeholder="Email address" aria-label="Email address" required data-msg="Please enter a valid email address." data-error-class="u-has-error" data-success-class="u-has-success">
                  </div>
                </div>
                <!-- End Input -->
              </div>

              <div class="w-100"></div>

              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">Company</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <input type="text" class="form-control" name="company" placeholder="Company" aria-label="Company" required data-msg="Please enter company name." data-error-class="u-has-error" data-success-class="u-has-success">
                  </div>
                </div>
                <!-- End Input -->
              </div>

              <div class="col-md-6 mb-3">
                <!-- Input -->
                <label class="sr-only">Job title</label>

                <div class="js-form-message">
                  <div class="input-group">
                    <input type="text" class="form-control" name="jobTitle" placeholder="Job title" aria-label="Job title" required data-msg="Please enter a job title." data-error-class="u-has-error" data-success-class="u-has-success">
                  </div>
                </div>
                <!-- End Input -->
              </div>
            </div>

            <!-- Input -->
            <div class="mb-5">
              <label class="sr-only">How can we help you?</label>

              <div class="js-form-message input-group">
                <textarea class="form-control" rows="4" name="description" placeholder="Hi there, I would like to ..." aria-label="Hi there, I would like to ..." required data-msg="Please enter a reason." data-error-class="u-has-error" data-success-class="u-has-success"></textarea>
              </div>
            </div>
            <!-- End Input -->

            <!-- Checkbox -->
            <div class="js-form-message mb-3">
              <div class="custom-control custom-checkbox d-flex align-items-center text-muted">
                <input type="checkbox" class="custom-control-input" id="termsCheckbox" name="termsCheckbox" required data-msg="Please accept our Terms and Conditions." data-error-class="u-has-error" data-success-class="u-has-success">
                <label class="custom-control-label" for="termsCheckbox">
                  <small>
                    I agree to the
                    <a class="link-muted" href="../pages/terms.html">Terms and Conditions</a>
                  </small>
                </label>
              </div>
            </div>
            <!-- End Checkbox -->

            <!-- Checkbox -->
            <div class="js-form-message mb-5">
              <div class="custom-control custom-checkbox d-flex align-items-center text-muted">
                <input type="checkbox" class="custom-control-input" id="newsletterCheckbox" name="newsletterCheckbox" required data-msg="Please accept our Terms and Conditions." data-error-class="u-has-error" data-success-class="u-has-success">
                <label class="custom-control-label" for="newsletterCheckbox">
                  <small>I want to receive Front's Newsletters</small>
                </label>
              </div>
            </div>
            <!-- End Checkbox -->

            <button type="submit" class="btn btn-primary btn-pill btn-wide transition-3d-hover">Submit</button>
          </form>
          <!-- End Contact Form -->
        </div>
      </div>

      <div class="text-center">
        <!-- Logo -->
        <a class="d-inline-flex align-items-center mb-2" href="index.html" aria-label="Front">
          <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="36px" height="36px" viewBox="0 0 46 46" xml:space="preserve" style="margin-bottom: 0;">
            <path fill="#FFFFFF" opacity=".65" d="M23,41L23,41c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18h11.3C38,5,41,8,41,11.7V23C41,32.9,32.9,41,23,41z" />
            <path class="fill-white" opacity=".5" d="M28,35.9L28,35.9c-9.9,0-18-8-18-18v0c0-9.9,8-18,18-18l11.3,0C43,0,46,3,46,6.6V18C46,27.9,38,35.9,28,35.9z" />
            <path class="fill-white" opacity=".7" d="M18,46L18,46C8,46,0,38,0,28v0c0-9.9,8-18,18-18h11.3c3.7,0,6.6,3,6.6,6.6V28C35.9,38,27.9,46,18,46z" />
            <path class="fill-primary" d="M17.4,34V18.3h10.2v2.9h-6.4v3.4h4.8v2.9h-4.8V34H17.4z" />
          </svg>
          <span class="brand brand-light">Front</span>
        </a>
        <!-- End Logo -->

        <p class="small text-white-70">&copy; <?php echo $web; ?>. All rights reserved.</p>
      </div>
    </div>

    <!-- SVG Background Shape -->
    <figure class="w-100 position-absolute bottom-0 left-0">
      <img class="js-svg-injector" src="<?php echo base_url(); ?>front/assets/svg/illustrations/isometric-squares.svg" alt="Image Description" data-parent="#SVGFooter">
    </figure>
    <!-- End SVG Background Shape -->
  </footer>
  <!-- ========== END FOOTER ========== -->


  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#" data-position='{"bottom": 15, "right": 15 }' data-type="fixed" data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->

  <!-- JS Global Compulsory -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/bootstrap/bootstrap.min.js"></script>

  <!-- JS Implementing Plugins -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/svg-injector/dist/svg-injector.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/fancybox/jquery.fancybox.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/slick-carousel/slick/slick.js"></script>

  <!-- JS Front -->
  <script src="<?php echo base_url(); ?>front/assets/js/hs.core.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.header.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.validation.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.fancybox.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.slick-carousel.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.sticky-block.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.g-map.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.svg-injector.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.go-to.js"></script>

  <!-- JS Plugins Init. -->
  <script>
    $(window).on('load', function () {
      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 767.98,
        hideTimeOut: 0
      });

      // initialization of svg injector module
      $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {
      // initialization of header
      $.HSCore.components.HSHeader.init($('#header'));

      // initialization of forms
      $.HSCore.components.HSFocusState.init();

      // initialization of form validation
      $.HSCore.components.HSValidation.init('.js-validate', {
        rules: {
          confirmPassword: {
            equalTo: '#signupPassword'
          }
        }
      });

      // initialization of fancybox
      $.HSCore.components.HSFancyBox.init('.js-fancybox');

      // initialization of slick carousel
      $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

      // initialization of sticky blocks
      $.HSCore.components.HSStickyBlock.init('.js-sticky-block');

      // initialization of go to
      $.HSCore.components.HSGoTo.init('.js-go-to');
    });

    // initialization of google map
    function initMap() {
      $.HSCore.components.HSGMap.init('.js-g-map');
    }
  </script>

</body>

</html>