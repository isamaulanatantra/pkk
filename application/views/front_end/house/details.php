
<article class="card shadow mb-5">
      <?php
      $rul_id_posting = $this->uri->segment(3);
      $query1 = $this->db->query("SELECT file_name from attachment where id_tabel=" . $rul_id_posting . " order by uploaded_time asc");

      $a = 0;
      foreach ($query1->result() as $row1) {
        $a++;
        if ($a == 1) {
          echo '
        <a class="js-fancybox u-media-viewer" href="javascript:;" data-src="' . base_url() . 'media/upload/' . $row1->file_name . '" data-fancybox="fancyboxGallery6" data-caption="Front in frames - image #01" data-speed="700" data-is-infinite="true">
        <img class="card-img-top" src="'.base_url().'media/upload/'.$gambar.'" alt="Image Description">
        </a>
      ';
        } else {
          echo '
        <a class="js-fancybox u-media-viewer" href="javascript:;" data-src="' . base_url() . 'media/upload/' . $row1->file_name . '" data-fancybox="fancyboxGallery6" data-caption="Front in frames - image #01" data-speed="700" data-is-infinite="true">
        </a>
      ';
        }
      }
      ?>

  <div class="card-body p-5">
    <h2 class="h5 font-weight-medium">
      <a href="single-article.html"><?php if (!empty($judul_posting)) {
                                          echo $judul_posting;
                                        } ?></a>
    </h2>
      <div class="row border-bottom">
        <div class="col-md-7 mb-5 mb-md-0">
          <div class="media align-items-center">
            <div class="u-avatar">
              <img class="img-fluid rounded-circle" src="<?php echo base_url(); ?>front/assets/img/100x100/img11.jpg" alt="Image Description">
            </div>
            <div class="media-body font-size-1 ml-3">
              <span class="h6 font-weight-medium"><a href="<?php echo base_url(); ?>profile"><?php if (!empty($pembuat)) {
                                                                                                echo $pembuat;
                                                                                              } ?></a> <button type="button" class="btn btn-xs btn-soft-primary font-weight-medium transition-3d-hover py-1 px-2 ml-1">Follow</button></span>
              <span class="d-block text-muted"><?php if (!empty($created_time)) {
                                                  echo $created_time;
                                                } ?></span>
            </div>
          </div>
        </div>
        <div class="col-md-5 mb-5 mb-md-0">
          <div class="d-md-flex justify-content-md-end align-items-md-center">
            <h2 class="text-secondary font-size-1 font-weight-medium text-uppercase mb-0">Share:</h2>

            <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent rounded-circle ml-2" href="#">
              <span class="fab fa-facebook-f btn-icon__inner"></span>
            </a>
            <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent rounded-circle ml-2" href="#">
              <span class="fab fa-twitter btn-icon__inner"></span>
            </a>
            <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent rounded-circle ml-2" href="#">
              <span class="fab fa-instagram btn-icon__inner"></span>
            </a>
            <a class="btn btn-sm btn-icon btn-soft-secondary btn-bg-transparent rounded-circle ml-2" href="#">
              <span class="fab fa-telegram btn-icon__inner"></span>
            </a>
          </div>
        </div>
      </div>
    <?php if (!empty($isi_posting)) {
        echo $isi_posting;
      } ?>
    <div class="media align-items-center pt-5">
      <div class="u-sm-avatar u-sm-avatar--bordered rounded-circle" data-toggle="tooltip" data-placement="top" title="" data-original-title="Nataly Gaga">
        <img class="img-fluid rounded-circle" src="<?php echo base_url(); ?>front/assets/img/100x100/img1.jpg" alt="Image Description">
      </div>
      <div class="u-sm-avatar u-sm-avatar--bordered rounded-circle ml-n2" data-toggle="tooltip" data-placement="top" title="" data-original-title="Emily Milda">
        <img class="img-fluid rounded-circle" src="<?php echo base_url(); ?>front/assets/img/100x100/img2.jpg" alt="Image Description">
      </div>
      <div class="media-body d-flex justify-content-end text-muted font-size-1 ml-2">
      <?php if (!empty($created_time)) {
                                                  echo $created_time;
                                                } ?>
      </div>
    </div>
  </div>
</article>
