
    <div class="container space-bottom-1 position-relative z-index-2">
      <div class="row justify-content-lg-between mb-11">
        <div class="col-lg-5 text-white mb-7 mb-lg-0">
          <!-- Title -->
          <div class="mb-7">
            <h2 class="h1 font-weight-medium">Kami di sini untuk membantu</h2>
            <p class="text-white">Temukan di  <a class="text-warning font-weight-medium" href="<?php echo base_url(); ?>help">Pusat Bantuan kami.</a></p>
          </div>
          <!-- End Title -->

        </div>

        <div class="col-lg-6 mt-lg-n11">
          <!-- Contact Form -->
          <form class="js-validate card border-0 shadow-soft p-5">
            <div class="mb-4">
              <h3 class="h5">Survey Kepuasan Masyarakat</h3>
            </div>

            <div class="row mx-gutters-2">
              <div class="col-md-12 mb-3">
                <div class="js-form-message">
                  <div class="input-group">
                    <select type="text" class="form-control custom-select text-muted" name="firstName" placeholder="First name" aria-label="First name" required data-msg="Please enter your first name." data-error-class="u-has-error" data-success-class="u-has-success">
											<option value="0">Pilih Persepsi</option>
											<option value="4">Sangat sesuai</option>
											<option value="3">Sesuai</option>
											<option value="2">Kurang sesuai</option>
											<option value="1">Tidak sesuai</option>
										</select>
                  </div>
                </div>
                <!-- End Input -->
              </div>

            </div>

          </form>
          <!-- End Contact Form -->
        </div>
          <div class="row text-white col-md-12">
            <!-- Contacts Info -->
            <div class="col-md-3 mb-5">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fas fa-envelope btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">General enquiries</h4>
              <a class="text-white-70 font-size-1" href="#"><?php echo $email; ?></a>
            </div>
            <!-- End Contacts Info -->

            <!-- Contacts Info -->
            <div class="col-md-3 mb-5">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fas fa-phone btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">Phone Number</h4>
              <a class="text-white-70 font-size-1" href="#"><?php echo $telpon; ?></a>
            </div>
            <!-- End Contacts Info -->

            <!-- Contacts Info -->
            <div class="col-md-3">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fas fa-map-marker-alt btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">Address</h4>
              <a class="text-white-70 font-size-1" href="#"><?php echo $alamat; ?></a>
            </div>
            <!-- End Contacts Info -->
            <!-- Contacts Info -->
            <div class="col-md-3">
              <span class="btn btn-icon btn-soft-white rounded-circle mb-3">
                <span class="fab fa-apple btn-icon__inner"></span>
              </span>
              <h4 class="h6 mb-0">Media Sosial</h4>
              <ul class="list-inline">
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $facebook; ?>">
                    <span class="fab fa-facebook-f btn-icon__inner"></span>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $google; ?>">
                    <span class="fab fa-google btn-icon__inner"></span>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $twitter; ?>">
                    <span class="fab fa-twitter btn-icon__inner"></span>
                  </a>
                </li>
                <li class="list-inline-item">
                  <a class="btn btn-sm btn-icon btn-soft-light btn-bg-transparent" href="<?php echo $instagram; ?>">
                    <span class="fab fa-instagram btn-icon__inner"></span>
                  </a>
                </li>
              </ul>
            </div>
            <!-- End Contacts Info -->
          </div>
      </div>

      <div class="text-center">
        <!-- Logo -->
        <a class="d-inline-flex align-items-center mb-2" href="<?php echo base_url(); ?>" aria-label="Front">
          <span class="brand brand-light"><?php $aa=explode(".",$web);echo $aa[0]; ?></span>
        </a>
        <!-- End Logo -->

        <p class="small text-white-70">&copy; <?php echo $web; ?>. All rights reserved.</p>
      </div>
    </div>

    <!-- SVG Background Shape -->
    <figure class="w-100 position-absolute bottom-0 left-0">
      <img class="js-svg-injector" src="<?php echo base_url(); ?>front/assets/svg/illustrations/isometric-squares.svg" alt="Image Description" data-parent="#SVGFooter">
    </figure>
    <!-- End SVG Background Shape -->