
    <!-- Login Form -->
    <div class="container space-2">
      <div class="col-md-6" id="formregistrasiberhasil" style="display:none;">
        <div class="row">
          <div data-form-alert-danger="" class="alert alert-danger col-12">
            Registrasi anda sudah berhasil, silahkan masuk ke email yang anda gunakan untuk konfirmasi pendaftaran.
          </div>
        </div>
      </div>
      <form class="js-validate w-md-75 w-lg-50 mx-md-auto" id="formregistrasi">
        <!-- Title -->
        <div class="mb-7">
          <h1 class="h3 text-primary font-weight-normal mb-0">Welcome to <span class="font-weight-semi-bold"><?php $aa=explode(".",$web);echo $aa[0]; ?></span></h1>
          <p><?php if (!empty($title)) {echo $title;} ?></p>
					<div style="display:none;" data-form-alert-danger="" id="pesan" class="alert alert-danger col-12">
					</div>
        </div>
        <!-- End Title -->

        <!-- Form Group -->
        <div class="js-form-message form-group">
          <label class="form-label" for="nama">Nama Lengkap</label>
          <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama Lengkap" aria-label="Email address" required
                 data-msg="Please enter a valid email address."
                 data-error-class="u-has-error"
                 data-success-class="u-has-success" value="">
        </div>
        <!-- End Form Group -->

        <!-- Form Group -->
        <div class="js-form-message form-group">
          <label class="form-label" for="nik">NIK</label>
          <input type="text" class="form-control" name="nik" id="nik" placeholder="NIK" aria-label="Email address" required
                 data-msg="Please enter a valid email address."
                 data-error-class="u-has-error"
                 data-success-class="u-has-success" value="">
        </div>
        <!-- End Form Group -->

        <!-- Form Group -->
        <div class="js-form-message form-group">
          <label class="form-label" for="nomot_telp">Nomot Telp</label>
          <input type="text" class="form-control" name="nomot_telp" id="nomot_telp" placeholder="Nomot Telp" aria-label="Email address" required
                 data-msg="Please enter a valid email address."
                 data-error-class="u-has-error"
                 data-success-class="u-has-success" value="">
        </div>
        <!-- End Form Group -->

        <!-- Form Group -->
        <div class="js-form-message form-group">
          <label class="form-label" for="tanggal_lahir">Tanggal Lahir</label>
          <input type="text" class="form-control" name="tanggal_lahir" id="tanggal_lahir" placeholder="Tanggal Lahir" aria-label="Email address" required
                 data-msg="Please enter a valid email address."
                 data-error-class="u-has-error"
                 data-success-class="u-has-success" value="">
        </div>
        <!-- End Form Group -->

        <!-- Form Group -->
        <div class="js-form-message form-group">
          <label class="form-label" for="jenis_kelamin">Jenis Kelamin</label>
          <select type="text" class="form-control" name="jenis_kelamin" id="jenis_kelamin" placeholder="Nama Lengkap" aria-label="Email address" required
                 data-msg="Please enter a valid email address."
                 data-error-class="u-has-error"
                 data-success-class="u-has-success">
									<option value="L">Laki-laki</option>
									<option value="P">Perempuan</option>
								</select>
        </div>
        <!-- End Form Group -->

        <!-- Form Group -->
        <div class="js-form-message form-group">
          <label class="form-label" for="email">Email address</label>
          <input type="email" class="form-control" name="email" id="email" placeholder="Email address" aria-label="Email address" required
                 data-msg="Please enter a valid email address."
                 data-error-class="u-has-error"
                 data-success-class="u-has-success" value="">
        </div>
        <!-- End Form Group -->

        <!-- Form Group -->
        <div class="js-form-message form-group">
          <label class="form-label" for="password">Password</label>
          <input type="password" class="form-control" name="password" id="password" placeholder="********" aria-label="********" required
                 data-msg="Your password is invalid. Please try again."
                 data-error-class="u-has-error"
                 data-success-class="u-has-success" value="">
        </div>
        <!-- End Form Group -->

        <!-- Form Group -->
        <div class="js-form-message form-group">
          <label class="form-label" for="password_confirm">Confirm password</label>
          <input type="password" class="form-control" name="password_confirm" id="password_confirm" placeholder="********" aria-label="********" required
                 data-msg="Password does not match the confirm password."
                 data-error-class="u-has-error"
                 data-success-class="u-has-success" value="">
        </div>
        <!-- End Form Group -->

        <!-- Checkbox -->
        <div class="js-form-message mb-5">
          <div class="custom-control custom-checkbox d-flex align-items-center text-muted">
            <input type="checkbox" class="custom-control-input" id="termsCheckbox" name="termsCheckbox" required
                   data-msg="Please accept our Terms and Conditions."
                   data-error-class="u-has-error"
                   data-success-class="u-has-success">
            <label class="custom-control-label" for="termsCheckbox">
              <small>
                I agree to the
                <a class="link-muted" href="<?php echo base_url(); ?>terms">Terms and Conditions</a>
              </small>
            </label>
          </div>
        </div>
        <!-- End Checkbox -->

        <!-- Button -->
        <div class="row align-items-center mb-5">
          <div class="col-5 col-sm-6">
            <span class="small text-muted">Already have an account?</span>
            <a class="small" href="login-simple.html">Login</a>
          </div>

          <div class="col-7 col-sm-6 text-right">
            <button type="submit" class="btn btn-primary transition-3d-hover" id="register">Get Started</button>
          </div>
        </div>
        <!-- End Button -->
      </form>
    </div>
    <!-- End Login Form -->
<script type="text/javascript">
  $(document).ready(function() {
    $('#tanggal_lahir').datepicker({
      format: 'dd/mm/yyyy',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function(){
  $("#tanggal_lahir").inputmask();
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $("#register").on("click", function(e) {
      e.preventDefault();
      $("#pesan").hide();
      var email = $("#email").val();
      var password = $("#password").val();
      var password_confirm = $("#password_confirm").val();
      var nama = $("#nama").val();
      var nomot_telp = $("#nomot_telp").val();
      var jenis_kelamin = $("#jenis_kelamin").val();
      var tanggal_lahir = $("#tanggal_lahir").val();
      var nik = $("#nik").val();
      if (email == '' || password == '' || password_confirm == '') {
        $("#pesan").html('Mohon Isi Data Secara Lengkap');
        $("#pesan").show();
      } else if (password != password_confirm) {
        $("#pesan").html('password Tidak Sama');
        $("#pesan").show();
      } else {
        $.ajax({
          type: "POST",
          async: true,
          data: {
            email: email,
            password: password,
            nama: nama,
            nomot_telp: nomot_telp,
            jenis_kelamin: jenis_kelamin,
            nik: nik,
            tanggal_lahir: tanggal_lahir
          },
          dataType: "text",
          url: '<?php echo base_url(); ?>signup/proses_register',
          success: function(text) {
            if (text == 'OK') {
              $("#formregistrasi").hide();
              $("#formregistrasiberhasil").show();
            } else {
              $("#pesan").html(''+text+'');
              $("#pesan").show();
            }
          }
        });
      }
      $('html, body').animate({
        scrollTop: $('#backtop').offset().top
      }, 1000);
    });
  });
</script>
