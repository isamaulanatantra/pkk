
<style>
	@-webkit-keyframes placeHolderShimmer {
		0% {
			background-position: -468px 0;
		}
		100% {
			background-position: 468px 0;
		}
	}

	@keyframes placeHolderShimmer {
		0% {
			background-position: -468px 0;
		}
		100% {
			background-position: 468px 0;
		}
	}

	.content-placeholder {
		display: inline-block;
		-webkit-animation-duration: 1s;
		animation-duration: 1s;
		-webkit-animation-fill-mode: forwards;
		animation-fill-mode: forwards;
		-webkit-animation-iteration-count: infinite;
		animation-iteration-count: infinite;
		-webkit-animation-name: placeHolderShimmer;
		animation-name: placeHolderShimmer;
		-webkit-animation-timing-function: linear;
		animation-timing-function: linear;
		background: #f6f7f8;
		background: -webkit-gradient(linear, left top, right top, color-stop(8%, #eeeeee), color-stop(18%, #dddddd), color-stop(33%, #eeeeee));
		background: -webkit-linear-gradient(left, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
		background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
		-webkit-background-size: 800px 104px;
		background-size: 800px 104px;
		height: inherit;
		position: relative;
	}

	.post_data
	{
		padding:0px;
		border:1px solid #f9f9f9;
		border-radius: 5px;
		margin-bottom: 24px;
		box-shadow: 10px 10px 5px #eeeeee;
	}
</style>

<!-- Property Description Section -->
<div class="container">
  <div class="row mx-gutters-2">
    <div class="col-lg mb-3 mb-lg-0">
      <!-- Search Property Input -->
      <div class="js-focus-state">
        <div class="input-group input-group-sm">
          <div class="input-group-prepend">
            <span class="input-group-text" id="searchProperty">
              <span class="fas fa-search"></span>
            </span>
          </div>
          <input type="text" class="form-control" name="kata_kunci" id="kata_kunci" placeholder="Search Perundang-undangan" aria-label="Search Perundang-undangan" aria-describedby="searchProperty" value="<?php echo $this->input->get('kata_kunci'); ?>">
        </div>
      </div>
      <!-- End Search Property Input -->
    </div>

    <div class="col-sm-auto ml-md-auto mb-3 mb-lg-0">
      <!-- Filter
      <div class="position-relative">
        <select id="jenis_perundang_undangan_id" name="jenis_perundang_undangan_id" class="btn btn-block btn-sm btn-soft-secondary dropdown-toggle" href="javascript:;" role="button">
          <span class="fas fa-home dropdown-item-icon"></span>
							<?php
								/*
								$where6      = array(
									'jenis_perundang_undangan.status' => 1
								);
								$this->db->select("
										jenis_perundang_undangan.jenis_perundang_undangan_id,
										jenis_perundang_undangan.jenis_perundang_undangan_code
									");    	
								$this->db->where($where6);
								$query6 = $this->db->get('jenis_perundang_undangan');
								$a6 = $query6->num_rows();
								if( $a6 == 0 ){
										echo '<option value="">Pilih Jenis</option>';
								}
								else{
										echo '<option value="">Pilih Jenis</option>';
									foreach ($query6->result() as $b6) {
										echo '<option value="'.$b6->jenis_perundang_undangan_id.'">'.$b6->jenis_perundang_undangan_code.'</option>';
									}
								}*/
							?>
        </select>
      </div>
      End Filter -->
    </div>
		<!-- 
    <div class="col-sm-auto">
      <button type="submit" class="btn btn-sm btn-primary" id="search_perundang_undangan">Search</button>
    </div>-->
  </div>
</div>
<!-- End Additional Functions -->

<div class="container space-top-1 space-bottom-2">
  <div class="row">
    <div class="col-lg-12">
		
			<div class="border-bottom pb-5 mb-5" id="load_data">
			</div>
			<div class="border-bottom pb-5 mb-5" id="load_data_message">
			</div>

    </div>
  </div>
</div>
