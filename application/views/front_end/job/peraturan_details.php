
	<?php
		// peraturan
		$where7      = array(
			'perundang_undangan.status' => 1,
			'perundang_undangan.perundang_undangan_id' => $this->input->get('id')
		);
		$this->db->select("
			*, 
			(
				select attachments.file_name from attachments
				where attachments.id_tabel=perundang_undangan.perundang_undangan_id
				and attachments.table_name='perundang_undangan'
				order by attachments.id_attachments DESC
				limit 1
			) as file,
			
			jenis_perundang_undangan.jenis_perundang_undangan_id,
			jenis_perundang_undangan.jenis_perundang_undangan_code
		");
		$this->db->where($where7);
		$this->db->join('jenis_perundang_undangan', 'jenis_perundang_undangan.jenis_perundang_undangan_id=perundang_undangan.jenis_perundang_undangan_id');
		$query7 = $this->db->get('perundang_undangan');
		$a7 = $query7->num_rows();
		if($a7==0){
			$perundang_undangan_id ='';
			$perundang_undangan_name ='';
			$tahun_pengundangan ='';
			$status_pengundangan ='';
			$nomor_peraturan ='';
			$file_foto ='';
			$jenis_perundang_undangan_id ='';
			$jenis_perundang_undangan_code ='';
		}
		foreach ($query7->result() as $b6) {
			$filenya = './media/peraturan/'.$b6->file.'';
			$file_info = new finfo(FILEINFO_MIME);  // object oriented approach!
			$mime_type = $file_info->buffer(file_get_contents($filenya));  // e.g. gives "image/jpeg"
			if (strpos($mime_type, 'image') !== false) {
				$file_foto='
        <a class="js-fancybox u-media-viewer" href="javascript:;" data-src="' . base_url() . 'media/peraturan/'.$b6->file.'" data-fancybox="fancyboxGallery6" data-caption="Front in frames - image #01" data-speed="700" data-is-infinite="true">
        <img class="card-img-top" src="'.base_url().'media/peraturan/'.$b6->file.'" alt="Image Description">
        </a>
				';
			}
			else{
				$file_foto='
				<embed src="'.base_url().'media/peraturan/'.$b6->file.'" type = "application / pdf" width = "100%" height = "700px" />
				';
			}
			$perundang_undangan_id = $b6->perundang_undangan_id;
			$perundang_undangan_name = ''.$b6->perundang_undangan_name;
			$tahun_pengundangan = $b6->tahun_pengundangan;
			$nomor_peraturan = $b6->nomor_peraturan;
			$status_pengundangan = $b6->status_pengundangan;
			$jenis_perundang_undangan_id = $b6->jenis_perundang_undangan_id;
			$jenis_perundang_undangan_code = $b6->jenis_perundang_undangan_code;
		}
	?>
<article class="card shadow mb-5">
  <div class="card-body p-5">
		<div class="border-bottom">
			<p class="font-size-1">
				<a href="<?php echo base_url(); ?>peraturan/kategori/?id=<?php if (!empty($jenis_perundang_undangan_id)) {echo $jenis_perundang_undangan_id;} ?>" style="text-transform: uppercase;"><?php if (!empty($jenis_perundang_undangan_code)) {echo $jenis_perundang_undangan_code;} ?></a>
			</p>
			<h2 class="h5 font-weight-medium">
				<a href="#" style="text-transform: capitalize;"><?php if (!empty($perundang_undangan_name)) {echo $perundang_undangan_name;} ?></a>
			</h2>
			<p class="font-size-1">
				<span class="fas fa-map-marker-alt mr-1"></span>
				Nomor: <?php if (!empty($nomor_peraturan)) {echo $nomor_peraturan;} ?>
			</p>
			<p class="font-size-1" href="#">
				<span class="fas fa-map-marker-alt mr-1"></span>
				Tahun: <?php if (!empty($tahun_pengundangan)) {echo $tahun_pengundangan;} ?>
			</p>
			<div class="d-flex align-items-center font-size-1">
				<a class="text-secondary mr-12" href="javascript:;">
					<span class="fas fa-star mr-1"></span>
					Status: <?php if (!empty($status_pengundangan)) {echo $status_pengundangan;} ?>
				</a>
			</div>
		</div>
    <div class="media align-items-center pt-5">
			<?php if (!empty($file_foto)) {echo $file_foto;} ?>
    </div>
  </div>
</article>
