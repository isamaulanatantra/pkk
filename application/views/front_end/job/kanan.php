
          <div class="js-sticky-block pl-lg-4" data-parent="#stickyBlockStartPoint" data-sticky-view="md" data-start-point="#stickyBlockStartPoint" data-end-point="#stickyBlockEndPoint" data-offset-top="16" data-offset-bottom="16" style="">
						<h3 class="h6 font-weight-semi-bold mb-4"> Jenis Perundang-undangan</h3>
            <!-- List Group -->
            <ul class="list-group list-group-flush list-group-borderless mb-5">
							<?php
								// jenis peraturan
								$where6      = array(
									'jenis_perundang_undangan.status' => 1
								);
								$this->db->select("
										jenis_perundang_undangan.jenis_perundang_undangan_id,
										jenis_perundang_undangan.jenis_perundang_undangan_code
									");    	
								$this->db->where($where6);
								$query6 = $this->db->get('jenis_perundang_undangan');
								$a6 = $query6->num_rows();
								if( $a6 == 0 ){
									echo '
									<li>
										<a class="list-group-item list-group-item-action d-flex align-items-center" href="#">
											All
											<span class="badge bg-soft-secondary badge-pill ml-2">0+</span>
											<small class="fas fa-angle-right ml-auto"></small>
										</a>
									</li>';
								}
								else{
									foreach ($query6->result() as $b6) {
										// peraturan
										$where7      = array(
											'perundang_undangan.status' => 1,
											'perundang_undangan.jenis_perundang_undangan_id' => $b6->jenis_perundang_undangan_id
										);
										$this->db->select("
												perundang_undangan.perundang_undangan_id
											");    	
										$this->db->where($where7);
										$query7 = $this->db->get('perundang_undangan');
										$a7 = $query7->num_rows();
										echo '
										<li>
											<a class="list-group-item list-group-item-action d-flex align-items-center" href="'.base_url().'peraturan/kategori/?id='.$b6->jenis_perundang_undangan_id.'">
												'.$b6->jenis_perundang_undangan_code.'
												<span class="badge bg-soft-secondary badge-pill ml-2">'.$a7.'</span>
												<small class="fas fa-angle-right ml-auto"></small>
											</a>
										</li>';
									}
								}
							?>
            </ul>
            <!-- End List Group -->

            <div class="border-top pt-5 mt-5">
              <h3 class="h6 font-weight-semi-bold mb-4">Top Informasi</h3>

							<?php
								// berita
								// if($web=='demoopd.wonosobokab.go.id'){$web='jdih.wonosobokab.go.id';}else{$web;}
								$query1 = $this->db->query("SELECT id_posting, judul_posting from posting where domain='".$web."' and judul_posting like '%berita%' order by created_time desc limit 1");
								foreach ($query1->result() as $row1) {
										$where9      = array(
											'posting.parent' => $row1->id_posting
										);
										$this->db->select("
												posting.id_posting,
												posting.judul_posting,
												(
													select attachment.file_name from attachment
													where attachment.id_tabel=posting.id_posting
													and attachment.table_name='posting'
													order by attachment.id_attachment DESC
													limit 1
												) as file
											");
										$this->db->where($where9);
										$this->db->limit(5);
										$query9 = $this->db->get('posting');
										$a9 = $query9->num_rows();
										if( $a9 == 0 ){
											echo '';
										}
										else{
											foreach ($query9->result() as $b9) {
												$filenya = './media/upload/'.$b9->file.'';
												$file_info = new finfo(FILEINFO_MIME);  // object oriented approach!
												$mime_type = $file_info->buffer(file_get_contents($filenya));  // e.g. gives "image/jpeg"
												if (strpos($mime_type, 'image') !== false) {
													$file_foto=''.base_url().'media/upload/'.$b9->file.'';
												}
												else{
													$file_foto=''.base_url().'media/imagekosong.png';
												}
												echo '
												<!-- Blog Card -->
												<article class="border-top pt-5 mt-5">
													<div class="row justify-content-between">
														<div class="col-8">
															<h4 class="h6 font-weight-medium mb-0">
																<a href="'.base_url().'front/details/'.$b9->id_posting.'">'.$b9->judul_posting.'</a>
															</h4>
														</div>

														<div class="col-4">
														<img class="img-fluid" src="'.$file_foto.'" alt="Image Description">
														</div>
													</div>
												</article>
												<!-- End Blog Card -->
												';
											}
										}
									}
							?>

            </div>
          </div>