<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title -->
  <title><?php if (!empty($title)) {
            echo $title;
          } ?></title>
  <!-- Meta -->
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" />
  <meta name="author" content="<?php if (!empty($title)) { echo $title; } ?>" />
  <meta name="keywords" content="<?php if (!empty($title)) { echo $title; } ?> <?php echo base_url(); ?>" />
  <meta name="og:description" content="<?php if (!empty($title)) { echo $title; } ?>" />
  <meta name="og:url" content="<?php echo base_url(); ?> <?php if (!empty($title)) { echo $title; } ?>" />
  <meta name="og:title" content="<?php if (!empty($title)) { echo $title; } ?> <?php echo base_url(); ?>" />
  <meta name="og:keywords" content="<?php if (!empty($title)) { echo $title;  } ?> <?php echo base_url(); ?>" />
  <?php
	if($web=='diskominfo.wonosobokab.go.id' or $web=='jdih.wonosobokab.go.id'){
	echo '
		<meta name="og:image" content="'.base_url().'media/logo-jdihn.png" />
		<link id="favicon" rel="shortcut icon" href="'.base_url().'media/logo-jdihn.png" type="image/png" />
	';
	}
	else{
		echo '
		<meta name="og:image" content="'.base_url().'media/logo wonosobo.png" />
		<link id="favicon" rel="shortcut icon" href="'.base_url().'media/logo wonosobo.png" type="image/png" />
		';
	}
  ?>
  <!-- Google Fonts -->
  <link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

  <!-- CSS Implementing Plugins -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/font-awesome/css/fontawesome-all.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/animate.css/animate.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/fancybox/jquery.fancybox.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/vendor/slick-carousel/slick/slick.css">

  <!-- CSS Front Template -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>front/assets/css/theme.css">
</head>

<body>
  <!-- ========== HEADER ========== -->
  <header id="logoAndNav" class="u-header u-header--abs-top-md u-header--show-hide-md">
    <div class="u-header__section u-header--floating__inner">
      <div id="logoAndNav" class="container">
        <!-- Nav -->
        <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">
          <!-- Responsive Toggle Button -->
          <button type="button" class="navbar-toggler btn u-hamburger" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
            <span id="hamburgerTrigger" class="u-hamburger__box">
              <span class="u-hamburger__inner"></span>
            </span>
          </button>
          <!-- End Responsive Toggle Button -->

          <!-- Navigation -->
          <div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
            <?php echo '' . $menu_atas . ''; ?>
          </div>
          <!-- End Navigation -->
        </nav>
        <!-- End Nav -->
      </div>
    </div>
  </header>
  <!-- ========== END HEADER ========== -->

  <!-- ========== MAIN CONTENT ========== -->
  <main id="content" role="main">
          <?php $this->load->view($main_top);  ?>
    <div class="container space-2 space-bottom-lg-3">
    <div class="row">
        <div class="col-md-7 col-lg-8 mb-7 mb-md-0">
          <?php $this->load->view($main_view);  ?>
        </div>

        <div id="stickyBlockStartPoint" class="col-md-5 col-lg-4" style="">
          <?php $this->load->view($main_kanan);  ?>
        </div>
      </div>
    </div>
  </main>

  <!-- ========== FOOTER ========== -->
  <footer id="SVGFooter" class="svg-preloader gradient-half-primary-v4 position-relative">
		<?php $this->load->view('front_end/job/footer.php');  ?>
  </footer>
  <!-- ========== END FOOTER ========== -->


  <!-- Go to Top -->
  <a class="js-go-to u-go-to" href="#" data-position='{"bottom": 15, "right": 15 }' data-type="fixed" data-offset-top="400" data-compensation="#header" data-show-effect="slideInUp" data-hide-effect="slideOutDown">
    <span class="fas fa-arrow-up u-go-to__inner"></span>
  </a>
  <!-- End Go to Top -->

  <!-- JS Global Compulsory -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery/dist/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/popper.js/dist/umd/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/bootstrap/bootstrap.min.js"></script>

  <!-- JS Implementing Plugins -->
  <script src="<?php echo base_url(); ?>front/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/svg-injector/dist/svg-injector.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/jquery-validation/dist/jquery.validate.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/fancybox/jquery.fancybox.min.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/vendor/slick-carousel/slick/slick.js"></script>

  <!-- JS Front -->
  <script src="<?php echo base_url(); ?>front/assets/js/hs.core.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.header.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.validation.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.fancybox.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.slick-carousel.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.sticky-block.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.g-map.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.svg-injector.js"></script>
  <script src="<?php echo base_url(); ?>front/assets/js/components/hs.go-to.js"></script>

  <!-- JS Plugins Init. -->
  <script>
    $(window).on('load', function () {
      // initialization of HSMegaMenu component
      $('.js-mega-menu').HSMegaMenu({
        event: 'hover',
        pageContainer: $('.container'),
        breakpoint: 767.98,
        hideTimeOut: 0
      });

      // initialization of svg injector module
      $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {
			
			var limit = 3;
			var start = 0;
			var action = 'inactive';
			var judul = 'perundang_undangan';
			var keyword = $('#kata_kunci').val();
			var id = '<?php echo $id; ?>';

			function lazzy_loader(limit)
			{
				var output = '';
				for(var count=0; count<limit; count++)
				{
					output += '<div class="post_data">';
					output += '<p><span class="content-placeholder" style="width:100%; height: 30px;">&nbsp;</span></p>';
					output += '<p><span class="content-placeholder" style="width:100%; height: 100px;">&nbsp;</span></p>';
					output += '</div>';
				}
				$('#load_data_message').html(output);
			}

			function lazzy_loader_posting(limit)
			{
				var output = '';
				for(var count=0; count<limit; count++)
				{
					output += '<div class="post_data">';
					output += '<p><span class="content-placeholder" style="width:100%; height: 30px;">&nbsp;</span></p>';
					output += '<p><span class="content-placeholder" style="width:100%; height: 100px;">&nbsp;</span></p>';
					output += '</div>';
				}
				$('#load_data_message_posting').html(output);
			}

			function lazzy_loader_kategori(limit)
			{
				var output = '';
				for(var count=0; count<limit; count++)
				{
					output += '<div class="post_data">';
					output += '<p><span class="content-placeholder" style="width:100%; height: 30px;">&nbsp;</span></p>';
					output += '<p><span class="content-placeholder" style="width:100%; height: 100px;">&nbsp;</span></p>';
					output += '</div>';
				}
				$('#load_data_message_kategori').html(output);
			}

			function lazzy_loader_struktur_organisasi(limit)
			{
				var output = '';
				for(var count=0; count<limit; count++)
				{
					output += '<div class="post_data">';
					output += '<p><span class="content-placeholder" style="width:100%; height: 30px;">&nbsp;</span></p>';
					output += '<p><span class="content-placeholder" style="width:100%; height: 100px;">&nbsp;</span></p>';
					output += '</div>';
				}
				$('#load_data_message_struktur_organisasi').html(output);
			}

			lazzy_loader(limit);
			lazzy_loader_kategori(limit);
			lazzy_loader_posting(limit);
			lazzy_loader_struktur_organisasi(limit);

			function load_data(limit, start, judul, keyword)
			{
				$.ajax({
					url:"<?php echo base_url(); ?>peraturan/fetch",
					method:"POST",
					data:{limit:limit, start:start, judul:judul, keyword:keyword},
					cache: false,
					success:function(data)
					{
						if(data == '')
						{
							$('#load_data_message').html('<h3>No More Result Found</h3>');
							action = 'active';
						}
						else
						{
							$('#load_data').append(data);
							$('#load_data_message').html("");
							action = 'inactive';
						}
					}
				})
			}
			
			function load_data_posting(limit, start, id, keyword)
			{
				$.ajax({
					url:"<?php echo base_url(); ?>front/fetch_posting",
					method:"POST",
					data:{limit:limit, start:start, id:id, keyword:keyword},
					cache: false,
					success:function(data)
					{
						if(data == '')
						{
							$('#load_data_message_posting').html('<h3>No More Result Found</h3>');
							action = 'active';
						}
						else
						{
							$('#load_data_posting').append(data);
							$('#load_data_message_posting').html("");
							action = 'inactive';
						}
					}
				})
			}
			
			function load_data_struktur_organisasi(limit, start, id, keyword)
			{
				$.ajax({
					url:"<?php echo base_url(); ?>front/fetch_struktur_organisasi",
					method:"POST",
					data:{limit:limit, start:start, id:id, keyword:keyword},
					cache: false,
					success:function(data)
					{
						if(data == '')
						{
							$('#load_data_message_posting').html('<h3>No More Result Found</h3>');
							action = 'active';
						}
						else
						{
							$('#load_data_struktur_organisasi').append(data);
							$('#load_data_message_struktur_organisasi').html("");
							action = 'inactive';
						}
					}
				})
			}
			
			function load_data_perundang_undangan(limit, start, judul, keyword)
			{
				$('#load_data').html("");
				$('#load_data_message').html("");
				$.ajax({
					url:"<?php echo base_url(); ?>peraturan/fetch",
					method:"POST",
					data:{limit:limit, start:start, judul:judul, keyword:keyword},
					cache: false,
					success:function(data)
					{
						if(data == '')
						{
							$('#load_data_message').html('<h3>No More Result Found</h3>');
							action = 'active';
						}
						else
						{
							$('#load_data').append(data);
							$('#load_data_message').html("");
							action = 'inactive';
						}
					}
				})
			}
			
			function load_data_kategori(limit, start, id, keyword)
			{
				$.ajax({
					url:"<?php echo base_url(); ?>peraturan/fetch_kategori",
					method:"POST",
					data:{limit:limit, start:start, id:id, keyword:keyword},
					cache: false,
					success:function(data)
					{
						if(data == '')
						{
							$('#load_data_message_kategori').html('<h3>No More Result Found</h3>');
							action = 'active';
						}
						else
						{
							$('#load_data_kategori').append(data);
							$('#load_data_message_kategori').html("");
							action = 'inactive';
						}
					}
				})
			}

			function load_data_kategori_perundang_undangan(limit, start, id, keyword)
			{
				$('#load_data_kategori').html("");
				$('#load_data_message_kategori').html("");
				$.ajax({
					url:"<?php echo base_url(); ?>peraturan/fetch_kategori",
					method:"POST",
					data:{limit:limit, start:start, id:id, keyword:keyword},
					cache: false,
					success:function(data)
					{
						if(data == '')
						{
							$('#load_data_message_kategori').html('<h3>No More Result Found</h3>');
							action = 'active';
						}
						else
						{
							$('#load_data_kategori').append(data);
							$('#load_data_message_kategori').html("");
							action = 'inactive';
						}
					}
				})
			}

			if(action == 'inactive')
			{
				action = 'active';
				var keyword = $('#kata_kunci').val();
				load_data(limit, start, judul, keyword);
				load_data_kategori(limit, start, id, keyword);
				load_data_posting(limit, start, id, keyword);
				load_data_struktur_organisasi(limit, start, id, keyword);
			}

			$(window).scroll(function(){
				var keyword = $('#kata_kunci').val();
				if($(window).scrollTop() + $(window).height() > $("#load_data").height() && action == 'inactive')
				{
					lazzy_loader(limit);
					action = 'active';
					start = start + limit;
					setTimeout(function(){
						load_data(limit, start, judul, keyword);
					}, 1000);
				}
			});

			$(window).scroll(function(){
				var keyword = $('#kata_kunci').val();
				if($(window).scrollTop() + $(window).height() > $("#load_data_kategori").height() && action == 'inactive')
				{
					lazzy_loader(limit);
					action = 'active';
					start = start + limit;
					setTimeout(function(){
						load_data_kategori(limit, start, id, keyword);
					}, 1000);
				}
			});

			$(window).scroll(function(){
				var keyword = $('#kata_kunci').val();
				if($(window).scrollTop() + $(window).height() > $("#load_data_posting").height() && action == 'inactive')
				{
					lazzy_loader_posting(limit);
					action = 'active';
					start = start + limit;
					setTimeout(function(){
						load_data_posting(limit, start, id, keyword);
					}, 1000);
				}
			});

			$(window).scroll(function(){
				var keyword = $('#kata_kunci').val();
				if($(window).scrollTop() + $(window).height() > $("#load_data_struktur_organisasi").height() && action == 'inactive')
				{
					lazzy_loader_posting(limit);
					action = 'active';
					start = start + limit;
					setTimeout(function(){
						load_data_struktur_organisasi(limit, start, id, keyword);
					}, 1000);
				}
			});

			function debounce(fn, duration) {
				var timer;
				return function () {
					clearTimeout(timer);
					timer = setTimeout(fn, duration);
				}
			}
			$('#kata_kunci').on('keyup', debounce(function () {
				var keyword = $('#kata_kunci').val();
				load_data_perundang_undangan(limit, start, judul, keyword);
				load_data_kategori_perundang_undangan(limit, start, id, keyword);
				load_data_posting(limit, start, id, keyword);
				load_data_struktur_organisasi(limit, start, id, keyword);
			}, 1000));
			
      // initialization of header
      $.HSCore.components.HSHeader.init($('#header'));

      // initialization of forms
      $.HSCore.components.HSFocusState.init();

      // initialization of form validation
      $.HSCore.components.HSValidation.init('.js-validate', {
        rules: {
          confirmPassword: {
            equalTo: '#signupPassword'
          }
        }
      });

      // initialization of fancybox
      $.HSCore.components.HSFancyBox.init('.js-fancybox');

      // initialization of slick carousel
      $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');

      // initialization of sticky blocks
      $.HSCore.components.HSStickyBlock.init('.js-sticky-block');

      // initialization of go to
      $.HSCore.components.HSGoTo.init('.js-go-to');
    });

    // initialization of google map
    function initMap() {
      $.HSCore.components.HSGMap.init('.js-g-map');
    }
  </script>

</body>

</html>