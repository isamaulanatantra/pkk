<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_1">
        
      </div>
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                Data Tema Website 
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="kata_kunci">
                  <select name="limit_data_data_website" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_data_website">
                    <option value="10">10 Per-Halaman</option>
                    <option value="20">20 Per-Halaman</option>
                    <option value="50">50 Per-Halaman</option>
                    <option value="999999999">Semua</option>
                  </select>
                  <select name="urut_data_data_website" class="form-control input-sm pull-right" style="width: 150px;" id="urut_data_data_website">
                    <option value="nama_data_website">Nama Data Website</option>
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-body table-responsive no-padding">
                        <div id="tblExport">
                          <table class="table table-hover">
                            <tbody>
                              <tr id="header_exel">
                                <th>NO</th> 
                                <th>Website</th>
                                <th>Status</th>
                              </tr>
                            </tbody>
                            <tbody id="tbl_utama_data_website">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="row">
                        
                        
                        
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!----------------------->
<script>
  function load_data_data_website(halaman, limit, kata_kunci, urut_data_data_website) {
    $('#tbl_utama_data_website').html('');
    $('#spinners_data').show();
    var limit_data_data_website = $('#limit_data_data_website').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:halaman,
        limit:limit,
        kata_kunci:kata_kunci,
        urut_data_data_website:urut_data_data_website
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>data_website/json_all_data_website/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_data_website="' + json[i].id_data_website + '" id="' + json[i].id_data_website + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
          tr += '<td valign="top">' + json[i].website + '</td>';          
          if( json[i].status == 1 ){
            tr += '<td valign="top" id="td_2_'+i+'"><a href="#" id="inaktifkan" ><i class="fa fa-cut"></i> Aktif</a></td>';
          }
          else{
            tr += '<td valign="top" id="td_3_'+i+'"><a href="#" id="aktifkan" ><i class="fa fa-cut"></i> Tidak Aktif</a></td>';
          }
          
          tr += '</tr>';
        }
        $('#tbl_utama_data_website').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
  }
</script>
<!----------------------->
<script>
  function cek_tema() {
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:'a'
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>data_website/cek_tema/',
      success: function(text) {
        
        var halaman = 1;
        var limit_data_data_website = $('#limit_data_data_website').val();
        var limit = limit_per_page_custome(limit_data_data_website);
        var kata_kunci = $('#kata_kunci').val();
        var urut_data_data_website = $('#urut_data_data_website').val();
        load_data_data_website(halaman, limit, kata_kunci, urut_data_data_website);
      }
    });
  }
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    cek_tema();
  });
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_website').on('click', '#inaktifkan', function() {
    var id_data_website = $(this).closest('tr').attr('id_data_website');
    alertify.confirm('Anda yakin data akan dipubish?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_data_website:id_data_website
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>data_website/inaktifkan',
          success: function(html) {
            alertify.success('Data berhasil publish');
            var halaman = 1;
            var limit_data_data_website = $('#limit_data_data_website').val();
            var limit = limit_per_page_custome(limit_data_data_website);
            var kata_kunci = $('#kata_kunci').val();
            var urut_data_data_website = $('#urut_data_data_website').val();
            load_data_data_website(halaman, limit, kata_kunci, urut_data_data_website);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_data_website').on('click', '#aktifkan', function() {
    var id_data_website = $(this).closest('tr').attr('id_data_website');
    alertify.confirm('Anda yakin data akan dipubish?', function(e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            aktif:1,
            id_data_website:id_data_website
          },
          dataType: 'html',
          url: '<?php echo base_url(); ?>data_website/aktifkan',
          success: function(html) {
            alertify.success('Data berhasil publish');
            var halaman = 1;
            var limit_data_data_website = $('#limit_data_data_website').val();
            var limit = limit_per_page_custome(limit_data_data_website);
            var kata_kunci = $('#kata_kunci').val();
            var urut_data_data_website = $('#urut_data_data_website').val();
            load_data_data_website(halaman, limit, kata_kunci, urut_data_data_website);
          }
        });
         
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#limit_data_data_website').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_data_website = $('#limit_data_data_website').val();
      var limit = limit_per_page_custome(limit_data_data_website);
      var kata_kunci = $('#kata_kunci').val();
      var urut_data_data_website = $('#urut_data_data_website').val();
      load_data_data_website(halaman, limit, kata_kunci, urut_data_data_website);
    });
  });
</script>