<section class="main-content" id="awal">
  <div class="section__content section__content--p30">    <div class="container-fluid">      <div class="row">							<div class="col">										<div class="default-tab">											<nav>												<div class="nav nav-tabs" id="nav-tab" role="tablist">													<a class="nav-item nav-link active" id="klik_tab_input" data-toggle="tab" href="#tab_1" role="tab" aria-controls="tab_1"													 aria-selected="true">Form Input</a>													<a class="nav-item nav-link" id="klick_tab_tampil" data-toggle="tab" href="#tab_2" role="tab" aria-controls="tab_2"													 aria-selected="false">Tampil</a>													<a class="nav-item nav-link" id="klick_tab_tampil_tabel" data-toggle="tab" href="#tab_3" role="tab" aria-controls="tab_3"													 aria-selected="false">Table View</a>												</div>											</nav>											<div class="tab-content pl-3 pt-2" id="nav-tabContent">												<div class="row tab-pane fade show active" id="tab_1" role="tabpanel" aria-labelledby="klik_tab_input">                                                  <form module="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=module" enctype="multipart/form-data">                            <div class="row">                                                        <div class="col">                                <div class="card card-primary">                                  <div class="card-header with-border">                                    <h3 class="card-title" id="judul_formulir">Formulir Input Module</h3>                                  </div>                                  <div class="card-body">                                    <div class="form-group" style="display:none;">                                      <label for="temp">temp</label>                                      <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">                                      <input class="form-control" id="page" name="page" value="1" placeholder="page" type="text">                                    </div>                                    <div class="form-group" style="display:none;">                                      <label for="mode">mode</label>                                      <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">                                    </div>                                    <div class="form-group" style="display:none;">                                      <label for="module_id">module_id</label>                                      <input class="form-control" id="module_id" name="id" value="" placeholder="module_id" type="text">                                    </div>                                    <div class="form-group">                                      <label for="module_code">Module Code</label>                                      <input class="form-control" id="module_code" name="module_code" value="" placeholder="Module Code" type="text">                                    </div>                                    <div class="form-group">                                      <label for="module_name">Module Name</label>                                      <input class="form-control" id="module_name" name="module_name" value="" placeholder="Module Name" type="text">                                    </div>                                    <div class="form-group">                                      <label for="information">Information</label>                                      <input class="form-control" id="information" name="information" value="" placeholder="Information" type="text">                                    </div>                                    <div class="form-group">                                      <label for="urutan_menu">Urutan Menu</label>                                      <input class="form-control" id="urutan_menu" name="urutan_menu" value="" placeholder="Urutan Menu" type="text">                                    </div>                                  </div>                                  <div class="card-body">                                    <div class="form-group">                                      <label for="remake">Attachment Information</label>                                      <input class="form-control" id="remake" name="remake" placeholder="Attachment Information" type="text">                                    </div>                                    <div class="form-group">                                      <label for="myfile">File Lampiran </label>                                      <input type="file" size="60" name="myfile" id="file_lampiran" >                                    </div>                                  </div>                                  <div class="card-body">                                    <h3 class="card-title">Attachment </h3>                                    <ul class="mailcard-attachments clearfix" id="tbl_attachment_module">                                    </ul>                                  </div>                                  <div class="overlay" id="overlay_input" style="display:none;">                                    <i class="fa fa-refresh fa-spin"></i>                                  </div>                                  <div class="card-footer">                                    <button type="submit" class="btn btn-primary" id="simpan_module">SIMPAN</button>                                    <button type="submit" class="btn btn-primary" id="update_module" style="display:none;">UPDATE</button>                                  </div>                                </div>                              </div>                            </div>                          </form>                          												</div>												<div class="row tab-pane fade" id="tab_2" role="tabpanel" aria-labelledby="klick_tab_tampil">                                                  <div class="row">                            <div class="col">                              <div class="card card-danger">                                <div class="card-header">                                  <h3 class="card-title">                                    Data Module                                    <button id="klikbukafilter" type="button" class="btn btn-card-tool" ><i class="fa fa-search"></i> Buka Filter</button>                                    <button id="kliktutupfilter" style="display: none;" type="button" class="btn btn-card-tool" ><i class="fa fa-times"></i> Tutup Filter</button>                                  </h3>                                </div>                                <div class="card-body" style="display: none;" id="filter">                                  <div class="row">                                    <div class="col-md-3">                                      <select name="limit_data_module" class="form-control input-sm" id="limit_data_module">                                        <option value="10">10 Per-Halaman</option>                                        <option value="20">20 Per-Halaman</option>                                        <option value="50">50 Per-Halaman</option>                                      </select>                                    </div>                                    <div class="col-md-3">                                      <select name="urut_data_module" class="form-control input-sm" id="urut_data_module">                                        <option value="module.module_name">Module Name</option>                                        <option value="module.module_code">Module Code</option>                                      </select>                                    </div>                                    <div class="col-md-2">                                      <select name="shorting" id="shorting" class="form-control input-sm" >                                        <option value="asc">A-Z</option>                                        <option value="desc">Z-A</option>                                      </select>                                    </div>                                    <div class="col-md-2">                                      <select name="status_module" class="form-control input-sm" id="status_module">                                        <option value="1">Aktif</option>                                        <option value="0">Tidak AKtif</option>                                        <option value="99">Arsip</option>                                      </select>                                    </div>                                    <div class="col-md-2">                                      <input name="kata_kunci" class="form-control input-sm" placeholder="Search" type="text" id="kata_kunci">                                    </div>                                  </div>                                </div>                                <!-- /.card-body -->                                <div class="card-body">                                  <ul id="tbl_utama_module" class="list-group list-group-flush">                                  </ul>                                </div>                                <div class="overlay" id="overlay_data" style="display:none;">                                  <i class="fa fa-refresh fa-spin"></i>                                </div>                                <div class="card-footer clearfix">                                  <ul id="pagination" class="pagination pagination-sm no-margin pull-right">                                  </ul>                                </div>                              </div>                            </div>                          </div>												</div>												<div class="row tab-pane fade" id="tab_3" role="tabpanel" aria-labelledby="klick_tab_tampil_tabel">                                                                    <div class="row">                            <div class="col">                              <div class="card card-danger">                                <div class="card-header">                                  <h3 class="card-title">                                    Data Module <a href="#" id="download_exel"> <i class="fa fa-fw fa-download"></i> Download File Exel</a>                                  </h3>                                </div>                                <div class="card-body table-responsive no-padding">                                  <table class="table table-hover">                                    <tr>                                      <th>No</th>                                      <th>Module Code</th>                                      <th>Module Name</th>                                      <th>Information</th>                                      <th>Urutan Menu</th>                                    </tr>                                    <tbody id="load_table_view">                                    </tbody>                                  </table>                                </div>                                <div class="overlay" id="overlay_data_tabel" style="display:none;">                                  <i class="fa fa-refresh fa-spin"></i>                                </div>                                <div class="card-footer clearfix">                                  <ul id="pagination_table_view" class="pagination pagination-sm no-margin pull-right">                                  </ul>                                </div>                              </div>                            </div>                          </div>												</div>											</div>										</div>              </div>                
      </div>    </div>  </div>
</section>
<script type="text/javascript">

function back_to_top() {
  $('html, body').animate({
    scrollTop: $('#awal').offset().top
  }, 1000);
}

function total(table, page, limit, order_by, keyword, order_field, status) {
  //$('#spinners_data').show();
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status
    },
    dataType: 'text',
    url: '<?php echo base_url(); ?>module/total/',
    success: function (text) {
      var tr = '';
      var total = parseInt(text);
      var page = parseInt(page);
      var limit = parseInt(limit);
      for (var i = 1; i <= total; i++) {
        if( i == page ){
          tr += '<li page="'+i+'" id="'+i+'"><a style="background: #cccccc;" id="nextPage" href="#">'+i+'</a></li>';
        }
        else{
          tr += '<li page="'+i+'" id="'+i+'"><a id="nextPage" href="#">'+i+'</a></li>';
        }
      }
      $('#pagination').html(tr);
      $('#pagination_table_view').html(tr);
    }
  });
}

function load_data_module(table, page, limit, order_by, keyword, order_field, status, shorting) {
  $('#overlay_data').show();
  total(table, page, limit, order_by, keyword, order_field, status);
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status,
      shorting: shorting
    },
    dataType: 'json',
    url: '<?php echo base_url(); ?>module/json/',
    success: function (json) {
      var tr = '';
      var tr2 = '';
      var start = ((page - 1) * limit);
      for (var i = 0; i < json.length; i++) {
        var start = start + 1;
        tr += '<li module_id="' + json[i].module_id + '" id="' + json[i].module_id + '" class="list-group-item">';        tr += '<div class="media">';
        if (json[i].file == null) {
          tr += '  <a href="#">';
          tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;"  src="https://q-nang.com/cooladmin/images/icon/avatar-01.jpg" alt="Product Image">';
          tr += '  </a>';
        } else {
          var http = new XMLHttpRequest();
          http.open('HEAD', '<?php echo base_url(); ?>media/upload/' + json[i].file + '', false);
          http.send();
          if (http.status != 404) {
            tr += '  <a href="#">';
            tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;"  src="<?php echo base_url(); ?>media/upload/' + json[i].file + '" alt="Product Image">';
            tr += '  </a>';
          } else {
            tr += '  <a href="#">';
            tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;"  src="https://q-nang.com/cooladmin/images/icon/avatar-01.jpg" alt="Product Image">';
            tr += '  </a>';
          }
        }
        tr += '  <div class="media-body">';
        tr += '    <a href="#" class="text-primary display-6">' + json[i].module_code + '</a>';
        tr += '    <span class="pull-right-container">';
        if( json[i].status == 99 ){
          tr += '  	   <span class="label pull-right"> <a href="#" id="restore_ajax" ><i class="fa fa-cut"></i> Restore</a> </span>';
        }
        else{
          tr += '  	   <span class="label pull-right"> <a href="#" id="del_ajax" class="btn btn-default btn-block btn-sm" ><i class="fa fa-cut"></i></a> </span>';
          tr += '  	   <span class="label pull-right"> <a href="#tab_1" data-toggle="tab" id="update_id" class="btn btn-default btn-block btn-sm" ><i class="fa fa-pencil-square-o"></i></a></span> ';
        }
        tr += '	 	 </span>';
        //tr += '      <span class="product-description">';
        var http = new XMLHttpRequest();
          http.open('HEAD', '<?php echo base_url(); ?>' + json[i].module_code + '', false);
          http.send();
          if (http.status != 404) {
            tr += '  <p>' + json[i].module_name + '</p>';
          } else {
            tr += '  <p> <span style="color:red">     ' + json[i].module_name + '</span> </p>';
          }
        //tr += '      </span>';
        tr += '  </div>';        tr += '  </div>';
        tr += '</li>';
      }
      $('#tbl_utama_module').html(tr);
      $('#overlay_data').hide();
    }
  });
}

function load_data_module_table(table, page, limit, order_by, keyword, order_field, status, shorting) {
  $('#overlay_data_tabel').show();
  total(table, page, limit, order_by, keyword, order_field, status);
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status,
      shorting: shorting
    },
    dataType: 'json',
    url: '<?php echo base_url(); ?>module/json/',
    success: function (json) {
      var tr = '';
      var start = ((page - 1) * limit);
      for (var i = 0; i < json.length; i++) {
        var start = start + 1;
        tr += '<tr module_id="' + json[i].module_id + '" id="' + json[i].module_id + '" >';
        tr += '<td valign="top">' + (start) + '</td>';
        tr += '<td valign="top">' + json[i].module_code + '</td>';
        tr += '<td valign="top">' + json[i].module_name + '</td>';
        tr += '<td valign="top">' + json[i].information + '</td>';
        tr += '<td valign="top">' + json[i].urutan_menu + '</td>';
        tr += '</tr>';
      }
      $('#load_table_view').html(tr);
      $('#overlay_data_tabel').hide();
    }
  });
}

function simpan(isinya) {
  $.ajax({
    type: 'POST',
    async: true,
    data: isinya,
    dataType: 'text',
    url: '<?php echo base_url(); ?>module/simpan/',
    success: function (text) {
      if (text == 0) {
        alertify.error('Data gagal disimpan');
      } else {
        alertify.success('Data berhasil disimpan');
      }
    }
  });
}

function update(isinya) {
  $.ajax({
    type: 'POST',
    async: true,
    data: isinya,
    dataType: 'text',
    url: '<?php echo base_url(); ?>module/update/',
    success: function (text) {
      if (text == 0) {
        alertify.error('Data gagal diupdate');
      } else {
        alertify.success('Data berhasil diaupdate');
      }
    }
  });
}

function AttachmentByMode(mode, value) {
  $('#tbl_attachment_module').html('');
  if (mode == 'edit') {
    var link = 'load_html_lampiran_by_id';
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table_name: 'module',
        mode: mode,
        id_tabel: value
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>attachment/' + link + '/',
      success: function (html) {
        $('#tbl_attachment_module').html(html);
      }
    });
  } else {
    var link = 'load_html_lampiran_by_temp';
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table_name: 'module',
        mode: mode,
        temp: value
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>attachment/' + link + '/',
      success: function (html) {
        $('#tbl_attachment_module').html(html);
      }
    });
  }
}

$(document).ready(function () {
  $('#temp').val(Math.random());
});

$('#tbl_attachment_module').on('click', '#del_ajax', function (e) {
  e.preventDefault();
  var id = $(this).closest('li').attr('id_attachment');
  alertify.confirm("Anda yakin data akan dihapus?", function (e) {
    if (e) {
      $.ajax({
        dataType: "json",
        url: '<?php echo base_url(); ?>attachment/hapus/?id=' + id + '',
        success: function (response) {
          if (response.errors == 'Yes') {
            alertify.alert("Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya");
          } else {
            $('[id_attachment=' + id + ']').remove();
            $('#img_upload').remove();
            alertify.alert("Data berhasil dihapus");
          }
        }
      });
    } else {
      alertify.alert("Hapus data dibatalkan");
    }
  });
});

$(document).ready(function () {
  $("#klick_tab_tampil").on("click", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module').val();
    var order_field = $('#urut_data_module').val();
    var status = $('#status_module').val();
    var shorting = $('#shorting').val();
    load_data_module('module', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#klick_tab_tampil_tabel").on("click", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module').val();
    var order_field = $('#urut_data_module').val();
    var status = $('#status_module').val();
    var shorting = $('#shorting').val();
    load_data_module_table('module', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#klik_tab_input").on("click", function (e) {
    e.preventDefault();
    $('#tbl_attachment_module').html('');
    $('#temp').val(Math.random());
    $('#judul_formulir').html('Formulir Input');
    $('#update_module').hide();
    $('#simpan_module').show();
    $('#module_id').val('');
    $('#module_code').val('');
    $('#module_name').val('');
    $("#information").val('');
    $("#urutan_menu").val('');
    $('#mode').val('input');
  });
});

$(document).ready(function () {
  $("#limit_data_module").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module').val();
    var order_field = $('#urut_data_module').val();
    var status = $('#status_module').val();
    var shorting = $('#shorting').val();
    load_data_module('module', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#urut_data_module").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module').val();
    var order_field = $('#urut_data_module').val();
    var status = $('#status_module').val();
    var shorting = $('#shorting').val();
    load_data_module('module', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#shorting").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module').val();
    var order_field = $('#urut_data_module').val();
    var status = $('#status_module').val();
    var shorting = $('#shorting').val();
    load_data_module('module', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#status_module").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module').val();
    var order_field = $('#urut_data_module').val();
    var status = $('#status_module').val();
    var shorting = $('#shorting').val();
    load_data_module('module', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $('#pagination').on('click', '#nextPage', function (e) {
    e.preventDefault();
    var page = $(this).closest('li').attr('page');
    var limit = $('#limit_data_module').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module').val();
    var order_field = $('#urut_data_module').val();
    var status = $('#status_module').val();
    var shorting = $('#shorting').val();
    load_data_module('module', page, limit, order_by, keyword, order_field, status, shorting);
    $('#page').val(page);
  });
});

$(document).ready(function () {
  $('#pagination_table_view').on('click', '#nextPage', function (e) {
    e.preventDefault();
    var page = $(this).closest('li').attr('page');
    var limit = $('#limit_data_module').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module').val();
    var order_field = $('#urut_data_module').val();
    var status = $('#status_module').val();
    var shorting = $('#shorting').val();
    $('#page').val(page);
    load_data_module_table('module', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  function debounce(fn, duration) {
    var timer;
    return function () {
      clearTimeout(timer);
      timer = setTimeout(fn, duration);
    }
  }
  $('#kata_kunci').on('keyup', debounce(function () {
    var str = $('#kata_kunci').val();
    var n = str.length;
    var page = '1';
    var limit = $('#limit_data_module').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module').val();
    var order_field = $('#urut_data_module').val();
    var status = $('#status_module').val();
    var shorting = $('#shorting').val();
    load_data_module('module', page, limit, order_by, keyword, order_field, status, shorting);
  }, 1000));
});

$(document).ready(function () {
  $('#klikbukafilter').on('click', function (e) {
    e.preventDefault();
    $('#filter').show();
    $('#klikbukafilter').hide();
    $('#kliktutupfilter').show();
  });
});

$(document).ready(function () {
  $('#kliktutupfilter').on('click', function (e) {
    e.preventDefault();
    $('#filter').hide();
    $('#klikbukafilter').show();
    $('#kliktutupfilter').hide();
  });
});

$(document).ready(function () {
  $('#simpan_module').on('click', function (e) {
    e.preventDefault();
    var isinya = {
      temp: $('#temp').val(),
      module_code: $('#module_code').val(),
      module_name: $('#module_name').val(),
      information: $('#information').val(),
      urutan_menu: $('#urutan_menu').val()
    };
    simpan(isinya);
    back_to_top();
  });
});

$(document).ready(function () {
  $('#update_module').on('click', function (e) {
    e.preventDefault();
    var isinya = {
      module_id: $('#module_id').val(),
      module_code: $('#module_code').val(),
      module_name: $('#module_name').val(),
      information: $('#information').val(),
      urutan_menu: $('#urutan_menu').val()
    };
    update(isinya);
  });
});

$(document).ready(function () {
  $('#tbl_utama_module').on('click', '#update_id', function () {
    $('#overlay_input').show();
    $('#mode').val('edit');
    var module_id = $(this).closest('li').attr('module_id');
    $('#judul_formulir').html('Formulir Edit');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        module_id: module_id
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>module/get_by_id/',
      success: function (json) {
        $('#simpan_module').hide();
        $('#update_module').show();
        for (var i = 0; i < json.length; i++) {
          $('#module_id').val(json[i].module_id);
          $('#module_code').val(json[i].module_code);
          $('#module_name').val(json[i].module_name);
          $("#information").val(json[i].information);
          $("#urutan_menu").val(json[i].urutan_menu);
        }
        $('#overlay_input').hide();
      }
    });
    AttachmentByMode($('#mode').val(), module_id);
  });
});

$(document).ready(function () {
  $('#tbl_utama_module').on('click', '#del_ajax', function () {
    var module_id = $(this).closest('li').attr('module_id');
    alertify.confirm('Anda yakin data akan dihapus?', function (e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            module_id: module_id
          },
          dataType: 'text',
          url: '<?php echo base_url(); ?>module/delete_by_id/',
          success: function (text) {
            if (text == 0) {
              alertify.error('Data gagal dihapus');
            } else {
              alertify.success('Data berhasil dihapus');
              var page = '1';
              var limit = $('#limit_data_module').val();
              var keyword = $('#kata_kunci').val();
              var order_by = $('#urut_data_module').val();
              var order_field = $('#urut_data_module').val();
              var status = $('#status_module').val();
              var shorting = $('#shorting').val();
              load_data_module('module', page, limit, order_by, keyword, order_field, status, shorting);
            }
          }
        });
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});

$(document).ready(function () {
  $('#tbl_utama_module').on('click', '#restore_ajax', function () {
    var module_id = $(this).closest('li').attr('module_id');
    alertify.confirm('Anda yakin data akan dikembalikan?', function (e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            module_id: module_id
          },
          dataType: 'text',
          url: '<?php echo base_url(); ?>module/restore_by_id/',
          success: function (text) {
            if (text == 0) {
              alertify.error('Data gagal dikembalikan');
            } else {
              alertify.success('Data berhasil dikembalikan');
              var page = '1';
              var limit = $('#limit_data_module').val();
              var keyword = $('#kata_kunci').val();
              var order_by = $('#urut_data_module').val();
              var order_field = $('#urut_data_module').val();
              var status = $('#status_module').val();
              var shorting = $('#shorting').val();
              load_data_module('module', page, limit, order_by, keyword, order_field, status, shorting);
            }
          }
        });
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});

$(document).ready(function () {
  var options = {
    beforeSend: function () {
      $('#ProgresUpload').show();
      $('#BarProgresUpload').width('0%');
      $('#PesanProgresUpload').html('');
      $('#PersenProgresUpload').html('0%');
    },
    uploadProgress: function (event, position, total, percentComplete) {
      $('#BarProgresUpload').width(percentComplete + '%');
      $('#PersenProgresUpload').html(percentComplete + '%');
    },
    success: function () {
      $('#BarProgresUpload').width('100%');
      $('#PersenProgresUpload').html('100%');
    },
    complete: function (response) {
      $('#PesanProgresUpload').html('<font color="green">' + response.responseText + '</font>');
      var mode = $('#mode').val();
      if (mode == 'edit') {
        var value = $('#module_id').val();
      } else {
        var value = $('#temp').val();
      }
      AttachmentByMode(mode, value);
      $('#remake').val('');
    },
    error: function () {
      $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
    }
  };
  document.getElementById('file_lampiran').onchange = function () {
    $('#form_isian').submit();
  };
  $('#form_isian').ajaxForm(options);
});


$(document).ready(function () {
  $("#download_exel").on("click", function (e) {
    e.preventDefault();
    var page = $('#page').val();
    var limit = $('#limit_data_module').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module').val();
    var order_field = $('#urut_data_module').val();
    var status = $('#status_module').val();
    var shorting = $('#shorting').val();
    window.location = "<?php echo base_url(); ?>module/xls/?page="+page+"&limit="+limit+"&keyword="+keyword+"&order_by="+order_by+"&order_field="+order_field+"&status="+status+"&shorting="+shorting+"";
  });
});

</script>