<section class="content" id="awal">
  <div class="row">
    <div class="col">
      <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="active"><a class="nav-link active" data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_2" id="klick_tab_tampil" >Tampil</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_3" id="klick_tab_tampil_tabel" >Table View</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div class="row">
            <div class="col">
              <form module="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachments/peraturan/?table_name=perundang_undangan" enctype="multipart/form-data">
                <div class="card card-primary">
                  <div class="card-header with-border">
                    <h3 class="card-title" id="judul_formulir">Formulir Input Pengundangan</h3>
                  </div>
                  <div class="card-body">
                    <div class="form-group" style="display:none;">
                      <label for="temp">temp</label>
                      <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                      <input class="form-control" id="page" name="page" value="1" placeholder="page" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="mode">mode</label>
                      <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="perundang_undangan_id">perundang_undangan_id</label>
                      <input class="form-control" id="perundang_undangan_id" name="id" value="" placeholder="perundang_undangan_id" type="text">
                    </div>
                    <div class="form-group">
                        <label for="jenis_perundang_undangan_id">Jenis Perundang-undangan</label>
                        <select class="form-control" id="jenis_perundang_undangan_id" name="jenis_perundang_undangan_id">
                            <option value="">Pilih Salah Satu</option>
                            <?php
                              $where1      = array(
                                'jenis_perundang_undangan.status' => 1
                              );
                              $this->db->where($where1);
                              $this->db->select("*");
                              foreach ($this->db->get('jenis_perundang_undangan')->result() as $b1) {
                                echo '<option value="'.$b1->jenis_perundang_undangan_id.'">'.$b1->jenis_perundang_undangan_name.'</option>';
                                }
            ?>
                                <select>
                    </div>
                    <div class="form-group">
                      <label for="nomor_peraturan">Nomor Peraturan</label>
                      <input class="form-control" id="nomor_peraturan" name="nomor_peraturan" value="" placeholder="Nomor Peraturan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="perundang_undangan_name">Judul</label>
                      <input class="form-control" id="perundang_undangan_name" name="perundang_undangan_name" value="" placeholder="Judul" type="text">
                    </div>
                    <div class="form-group">
                      <label for="status_pengundangan">Status Pengundangan</label>
                      <input class="form-control" id="status_pengundangan" name="status_pengundangan" value="" placeholder="Status Pengundangan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="tahun_pengundangan">Tahun Pengundangan</label>
                      <input class="form-control" id="tahun_pengundangan" name="tahun_pengundangan" value="" placeholder="Tahun Pengundangan" type="text">
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="form-group">
                      <label for="remake">Attachment Informasi</label>
                      <select class="form-control" id="remake" name="remake" placeholder="Attachment Informasi" type="text">
												<option value="">Pilih Informasi</option>
												<option value="Isi Peraturan">Isi Perundang-undangan</option>
												<option value="Naskah Akademik">Naskah Akademik</option>
											</select>
                    </div>
                    <div class="form-group">
                      <label for="myfile">File Lampiran </label>
                      <input type="file" size="60" name="myfile" id="file_lampiran" >
                    </div>
                  </div>
                  <div class="card-body">
                    <h3 class="card-title">Attachment </h3>
                    <ul class="mailcard-attachments clearfix" id="tbl_attachments_perundang_undangan">
                    </ul>
                  </div>
                  <div class="overlay" id="overlay_input" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                  </div>
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary" id="simpan_perundang_undangan">SIMPAN</button>
                    <button type="submit" class="btn btn-primary" id="update_perundang_undangan" style="display:none;">UPDATE</button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab_2">
          <div class="row">
            <div class="col-md-12">
              <div class="card card-danger">
                <div class="card-header">
                  <h3 class="card-title">
                    Data Perundang_undangan
                    <button id="klikbukafilter" type="button" class="btn btn-card-tool" ><i class="fa fa-search"></i> Buka Filter</button>
                    <button id="kliktutupfilter" style="display: none;" type="button" class="btn btn-card-tool" ><i class="fa fa-times"></i> Tutup Filter</button>
                  </h3>
                </div>
                <div class="card-body" style="display: none;" id="filter">
                  <div class="row">
                    <div class="col-md-3">
                      <select name="limit_data_perundang_undangan" class="form-control input-sm" id="limit_data_perundang_undangan">
                        <option value="10">10 Per-Halaman</option>
                        <option value="20">20 Per-Halaman</option>
                        <option value="50">50 Per-Halaman</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <select name="urut_data_perundang_undangan" class="form-control input-sm" id="urut_data_perundang_undangan">
                        <option value="perundang_undangan.perundang_undangan_name">Judul</option>
                        <option value="perundang_undangan.nomor_peraturan">Nomor Peraturan</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select name="shorting" id="shorting" class="form-control input-sm" >
                        <option value="asc">A-Z</option>
                        <option value="desc">Z-A</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select name="status_perundang_undangan" class="form-control input-sm" id="status_perundang_undangan">
                        <option value="1">Aktif</option>
                        <option value="0">Tidak AKtif</option>
                        <option value="99">Arsip</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <input name="kata_kunci" class="form-control input-sm" placeholder="Search" type="text" id="kata_kunci">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-body">
                  <ul id="tbl_utama_perundang_undangan" class="products-list product-list-in-card">
                  </ul>
                </div>
                <div class="overlay" id="overlay_data" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div class="card-footer clearfix">
                  <ul id="pagination" class="pagination pagination-sm no-margin pull-right">
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab_3">
          <div class="row">
            <div class="col-md-12">
              <div class="card card-danger">
                <div class="card-header">
                  <h3 class="card-title">
                    Data Perundang_undangan <a href="#" id="download_exel"> <i class="fa fa-fw fa-download"></i> Download File Exel</a>
                  </h3>
                </div>
                <div class="card-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>No</th>
                      <th>Nomor Peraturan</th>
                      <th>Judul</th>
                      <th>Status Pengundangan</th>
                      <th>Urutan Menu</th>
                    </tr>
                    <tbody id="load_table_view">
                    </tbody>
                  </table>
                </div>
                <div class="overlay" id="overlay_data_tabel" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div class="card-footer clearfix">
                  <ul id="pagination_table_view" class="pagination pagination-sm no-margin pull-right">
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">

function back_to_top() {
  $('html, body').animate({
    scrollTop: $('#awal').offset().top
  }, 1000);
}

function total(table, page, limit, order_by, keyword, order_field, status) {
  //$('#spinners_data').show();
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status
    },
    dataType: 'text',
    url: '<?php echo base_url(); ?>perundang_undangan/total/',
    success: function (text) {
      var tr = '';
      var total = parseInt(text);
      var page = parseInt(page);
      var limit = parseInt(limit);
      for (var i = 1; i <= total; i++) {
        if( i == page ){
          tr += '<li page="'+i+'" id="'+i+'"><a style="background: #cccccc;" id="nextPage" href="#">'+i+'</a></li>';
        }
        else{
          tr += '<li page="'+i+'" id="'+i+'"><a id="nextPage" href="#">'+i+'</a></li>';
        }
      }
      $('#pagination').html(tr);
      $('#pagination_table_view').html(tr);
    }
  });
}

function load_data_perundang_undangan(table, page, limit, order_by, keyword, order_field, status, shorting) {
  $('#overlay_data').show();
  total(table, page, limit, order_by, keyword, order_field, status);
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status,
      shorting: shorting
    },
    dataType: 'json',
    url: '<?php echo base_url(); ?>perundang_undangan/json/',
    success: function (json) {
      var tr = '';
      var tr2 = '';
      var start = ((page - 1) * limit);
      for (var i = 0; i < json.length; i++) {
        var start = start + 1;
        tr += '<li perundang_undangan_id="' + json[i].perundang_undangan_id + '" id="' + json[i].perundang_undangan_id + '" class="item">';
				tr += '  <div class="product-img">';
				tr += '    <img src="https://adminlte.io/themes/AdminLTE/dist/img/default-50x50.gif" alt="Product Image">';
				tr += '  </div>';
        tr += '  <div class="product-info">';
        if (json[i].file == null) {
        tr += '    <a href="#" class="product-title">' + json[i].perundang_undangan_name + '</a>';
        } else {
          var http = new XMLHttpRequest();
          http.open('HEAD', '<?php echo base_url(); ?>media/peraturan/' + json[i].file + '', false);
          http.send();
          if (http.status != 404) {
						tr += '    <a href="<?php echo base_url(); ?>media/peraturan/' + json[i].file + '" class="product-title">' + json[i].perundang_undangan_name + '</a>';
          } else {
            tr += '    <a href="#" class="product-title">' + json[i].perundang_undangan_name + '</a>';
          }
        }
        tr += '    <span class="pull-right-container">';
        if( json[i].status == 99 ){
          tr += '  	   <span class="label pull-right"> <a href="#" id="restore_ajax" ><i class="fa fa-cut"></i> Restore</a> </span>';
        }
        else{
          tr += '  	   <span class="label pull-right"> <a href="#" id="del_ajax" class="btn btn-danger float-right btn-sm" ><i class="fa fa-cut"></i></a> </span>';
          tr += '  	   <span class="label pull-right"> <a href="#tab_1" data-toggle="tab" id="update_id" class="btn btn-info float-right btn-sm" ><i class="fa fa-edit"></i></a></span> ';
        }
        tr += '	 	 </span>';
        tr += '      <span class="product-description">';
            tr += ' no. ' + json[i].nomor_peraturan + ' Tahun ' + json[i].tahun_pengundangan + '';
        tr += '      </span>';
        tr += '  </div>';
        tr += '</li>';
      }
      $('#tbl_utama_perundang_undangan').html(tr);
      $('#overlay_data').hide();
    }
  });
}

function load_data_perundang_undangan_table(table, page, limit, order_by, keyword, order_field, status, shorting) {
  $('#overlay_data_tabel').show();
  total(table, page, limit, order_by, keyword, order_field, status);
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status,
      shorting: shorting
    },
    dataType: 'json',
    url: '<?php echo base_url(); ?>perundang_undangan/json/',
    success: function (json) {
      var tr = '';
      var start = ((page - 1) * limit);
      for (var i = 0; i < json.length; i++) {
        var start = start + 1;
        tr += '<tr perundang_undangan_id="' + json[i].perundang_undangan_id + '" id="' + json[i].perundang_undangan_id + '" >';
        tr += '<td valign="top">' + (start) + '</td>';
        tr += '<td valign="top">' + json[i].nomor_peraturan + '</td>';
        tr += '<td valign="top">' + json[i].perundang_undangan_name + '</td>';
        tr += '<td valign="top">' + json[i].status_pengundangan + '</td>';
        tr += '<td valign="top">' + json[i].tahun_pengundangan + '</td>';
        tr += '</tr>';
      }
      $('#load_table_view').html(tr);
      $('#overlay_data_tabel').hide();
    }
  });
}

function simpan(isinya) {
  $.ajax({
    type: 'POST',
    async: true,
    data: isinya,
    dataType: 'text',
    url: '<?php echo base_url(); ?>perundang_undangan/simpan/',
    success: function (text) {
      if (text == 0) {
        alertify.error('Data gagal disimpan');
      } else {
        alertify.success('Data berhasil disimpan');
      }
    }
  });
}

function update(isinya) {
  $.ajax({
    type: 'POST',
    async: true,
    data: isinya,
    dataType: 'text',
    url: '<?php echo base_url(); ?>perundang_undangan/update/',
    success: function (text) {
      if (text == 0) {
        alertify.error('Data gagal diupdate');
      } else {
        alertify.success('Data berhasil diaupdate');
      }
    }
  });
}

function AttachmentByMode(mode, value) {
  $('#tbl_attachments_perundang_undangan').html('');
  if (mode == 'edit') {
    var link = 'load_html_lampiran_by_id';
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table_name: 'perundang_undangan',
        mode: mode,
        id_tabel: value
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>attachments/' + link + '/',
      success: function (html) {
        $('#tbl_attachments_perundang_undangan').html(html);
      }
    });
  } else {
    var link = 'load_html_lampiran_by_temp';
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table_name: 'perundang_undangan',
        mode: mode,
        temp: value
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>attachments/' + link + '/',
      success: function (html) {
        $('#tbl_attachments_perundang_undangan').html(html);
      }
    });
  }
}

$(document).ready(function () {
  $('#temp').val(Math.random());
});

$('#tbl_attachments_perundang_undangan').on('click', '#del_ajax', function (e) {
  e.preventDefault();
  var id = $(this).closest('li').attr('id_attachments');
  alertify.confirm("Anda yakin data akan dihapus?", function (e) {
    if (e) {
      $.ajax({
        dataType: "json",
        url: '<?php echo base_url(); ?>attachments/hapus/?id=' + id + '',
        success: function (response) {
          if (response.errors == 'Yes') {
            alertify.alert("Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya");
          } else {
            $('[id_attachments=' + id + ']').remove();
            $('#img_upload').remove();
            alertify.alert("Data berhasil dihapus");
          }
        }
      });
    } else {
      alertify.alert("Hapus data dibatalkan");
    }
  });
});

$(document).ready(function () {
  $("#klick_tab_tampil").on("click", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_perundang_undangan').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_perundang_undangan').val();
    var order_field = $('#urut_data_perundang_undangan').val();
    var status = $('#status_perundang_undangan').val();
    var shorting = $('#shorting').val();
    load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#klick_tab_tampil_tabel").on("click", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_perundang_undangan').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_perundang_undangan').val();
    var order_field = $('#urut_data_perundang_undangan').val();
    var status = $('#status_perundang_undangan').val();
    var shorting = $('#shorting').val();
    load_data_perundang_undangan_table('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#klik_tab_input").on("click", function (e) {
    e.preventDefault();
    $('#tbl_attachments_perundang_undangan').html('');
    $('#temp').val(Math.random());
    $('#judul_formulir').html('Formulir Input');
    $('#update_perundang_undangan').hide();
    $('#simpan_perundang_undangan').show();
    $('#perundang_undangan_id').val('');
    $('#jenis_perundang_undangan_id').val('');
    $('#nomor_peraturan').val('');
    $('#perundang_undangan_name').val('');
    $("#status_pengundangan").val('');
    $("#tahun_pengundangan").val('');
    $('#mode').val('input');
  });
});

$(document).ready(function () {
  $("#limit_data_perundang_undangan").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_perundang_undangan').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_perundang_undangan').val();
    var order_field = $('#urut_data_perundang_undangan').val();
    var status = $('#status_perundang_undangan').val();
    var shorting = $('#shorting').val();
    load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#urut_data_perundang_undangan").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_perundang_undangan').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_perundang_undangan').val();
    var order_field = $('#urut_data_perundang_undangan').val();
    var status = $('#status_perundang_undangan').val();
    var shorting = $('#shorting').val();
    load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#shorting").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_perundang_undangan').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_perundang_undangan').val();
    var order_field = $('#urut_data_perundang_undangan').val();
    var status = $('#status_perundang_undangan').val();
    var shorting = $('#shorting').val();
    load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#status_perundang_undangan").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_perundang_undangan').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_perundang_undangan').val();
    var order_field = $('#urut_data_perundang_undangan').val();
    var status = $('#status_perundang_undangan').val();
    var shorting = $('#shorting').val();
    load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $('#pagination').on('click', '#nextPage', function (e) {
    e.preventDefault();
    var page = $(this).closest('li').attr('page');
    var limit = $('#limit_data_perundang_undangan').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_perundang_undangan').val();
    var order_field = $('#urut_data_perundang_undangan').val();
    var status = $('#status_perundang_undangan').val();
    var shorting = $('#shorting').val();
    load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
    $('#page').val(page);
  });
});

$(document).ready(function () {
  $('#pagination_table_view').on('click', '#nextPage', function (e) {
    e.preventDefault();
    var page = $(this).closest('li').attr('page');
    var limit = $('#limit_data_perundang_undangan').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_perundang_undangan').val();
    var order_field = $('#urut_data_perundang_undangan').val();
    var status = $('#status_perundang_undangan').val();
    var shorting = $('#shorting').val();
    $('#page').val(page);
    load_data_perundang_undangan_table('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  function debounce(fn, duration) {
    var timer;
    return function () {
      clearTimeout(timer);
      timer = setTimeout(fn, duration);
    }
  }
  $('#kata_kunci').on('keyup', debounce(function () {
    var str = $('#kata_kunci').val();
    var n = str.length;
    var page = '1';
    var limit = $('#limit_data_perundang_undangan').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_perundang_undangan').val();
    var order_field = $('#urut_data_perundang_undangan').val();
    var status = $('#status_perundang_undangan').val();
    var shorting = $('#shorting').val();
    load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
  }, 1000));
});

$(document).ready(function () {
  $('#klikbukafilter').on('click', function (e) {
    e.preventDefault();
    $('#filter').show();
    $('#klikbukafilter').hide();
    $('#kliktutupfilter').show();
  });
});

$(document).ready(function () {
  $('#kliktutupfilter').on('click', function (e) {
    e.preventDefault();
    $('#filter').hide();
    $('#klikbukafilter').show();
    $('#kliktutupfilter').hide();
  });
});

$(document).ready(function () {
  $('#simpan_perundang_undangan').on('click', function (e) {
    e.preventDefault();
    var isinya = {
      temp: $('#temp').val(),
      jenis_perundang_undangan_id: $('#jenis_perundang_undangan_id').val(),
      nomor_peraturan: $('#nomor_peraturan').val(),
      perundang_undangan_name: $('#perundang_undangan_name').val(),
      status_pengundangan: $('#status_pengundangan').val(),
      tahun_pengundangan: $('#tahun_pengundangan').val()
    };
    simpan(isinya);
    back_to_top();
  });
});

$(document).ready(function () {
  $('#update_perundang_undangan').on('click', function (e) {
    e.preventDefault();
    var isinya = {
      perundang_undangan_id: $('#perundang_undangan_id').val(),
      jenis_perundang_undangan_id: $('#jenis_perundang_undangan_id').val(),
      nomor_peraturan: $('#nomor_peraturan').val(),
      perundang_undangan_name: $('#perundang_undangan_name').val(),
      status_pengundangan: $('#status_pengundangan').val(),
      tahun_pengundangan: $('#tahun_pengundangan').val()
    };
    update(isinya);
  });
});

$(document).ready(function () {
  $('#tbl_utama_perundang_undangan').on('click', '#update_id', function () {
    $('#overlay_input').show();
    $('#mode').val('edit');
    var perundang_undangan_id = $(this).closest('li').attr('perundang_undangan_id');
    $('#judul_formulir').html('Formulir Edit');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        perundang_undangan_id: perundang_undangan_id
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>perundang_undangan/get_by_id/',
      success: function (json) {
        $('#simpan_perundang_undangan').hide();
        $('#update_perundang_undangan').show();
        for (var i = 0; i < json.length; i++) {
          $('#perundang_undangan_id').val(json[i].perundang_undangan_id);
          $('#nomor_peraturan').val(json[i].nomor_peraturan);
          $('#perundang_undangan_name').val(json[i].perundang_undangan_name);
          $("#status_pengundangan").val(json[i].status_pengundangan);
          $("#tahun_pengundangan").val(json[i].tahun_pengundangan);
        }
        $('#overlay_input').hide();
      }
    });
    AttachmentByMode($('#mode').val(), perundang_undangan_id);
  });
});

$(document).ready(function () {
  $('#tbl_utama_perundang_undangan').on('click', '#del_ajax', function () {
    var perundang_undangan_id = $(this).closest('li').attr('perundang_undangan_id');
    alertify.confirm('Anda yakin data akan dihapus?', function (e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            perundang_undangan_id: perundang_undangan_id
          },
          dataType: 'text',
          url: '<?php echo base_url(); ?>perundang_undangan/delete_by_id/',
          success: function (text) {
            if (text == 0) {
              alertify.error('Data gagal dihapus');
            } else {
              alertify.success('Data berhasil dihapus');
              var page = '1';
              var limit = $('#limit_data_perundang_undangan').val();
              var keyword = $('#kata_kunci').val();
              var order_by = $('#urut_data_perundang_undangan').val();
              var order_field = $('#urut_data_perundang_undangan').val();
              var status = $('#status_perundang_undangan').val();
              var shorting = $('#shorting').val();
              load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
            }
          }
        });
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});

$(document).ready(function () {
  $('#tbl_utama_perundang_undangan').on('click', '#restore_ajax', function () {
    var perundang_undangan_id = $(this).closest('li').attr('perundang_undangan_id');
    alertify.confirm('Anda yakin data akan dikembalikan?', function (e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            perundang_undangan_id: perundang_undangan_id
          },
          dataType: 'text',
          url: '<?php echo base_url(); ?>perundang_undangan/restore_by_id/',
          success: function (text) {
            if (text == 0) {
              alertify.error('Data gagal dikembalikan');
            } else {
              alertify.success('Data berhasil dikembalikan');
              var page = '1';
              var limit = $('#limit_data_perundang_undangan').val();
              var keyword = $('#kata_kunci').val();
              var order_by = $('#urut_data_perundang_undangan').val();
              var order_field = $('#urut_data_perundang_undangan').val();
              var status = $('#status_perundang_undangan').val();
              var shorting = $('#shorting').val();
              load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
            }
          }
        });
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});

$(document).ready(function () {
  var options = {
    beforeSend: function () {
      $('#ProgresUpload').show();
      $('#BarProgresUpload').width('0%');
      $('#PesanProgresUpload').html('');
      $('#PersenProgresUpload').html('0%');
    },
    uploadProgress: function (event, position, total, percentComplete) {
      $('#BarProgresUpload').width(percentComplete + '%');
      $('#PersenProgresUpload').html(percentComplete + '%');
    },
    success: function () {
      $('#BarProgresUpload').width('100%');
      $('#PersenProgresUpload').html('100%');
    },
    complete: function (response) {
      $('#PesanProgresUpload').html('<font color="green">' + response.responseText + '</font>');
      var mode = $('#mode').val();
      if (mode == 'edit') {
        var value = $('#perundang_undangan_id').val();
      } else {
        var value = $('#temp').val();
      }
      AttachmentByMode(mode, value);
      $('#remake').val('');
    },
    error: function () {
      $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
    }
  };
  document.getElementById('file_lampiran').onchange = function () {
    $('#form_isian').submit();
  };
  $('#form_isian').ajaxForm(options);
});


$(document).ready(function () {
  $("#download_exel").on("click", function (e) {
    e.preventDefault();
    var page = $('#page').val();
    var limit = $('#limit_data_perundang_undangan').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_perundang_undangan').val();
    var order_field = $('#urut_data_perundang_undangan').val();
    var status = $('#status_perundang_undangan').val();
    var shorting = $('#shorting').val();
    window.location = "<?php echo base_url(); ?>perundang_undangan/xls/?page="+page+"&limit="+limit+"&keyword="+keyword+"&order_by="+order_by+"&order_field="+order_field+"&status="+status+"&shorting="+shorting+"";
  });
});

</script>