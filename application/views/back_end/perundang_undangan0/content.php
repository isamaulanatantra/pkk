<section class="content" id="awal">
  <div class="row">
    <div class="col">
      <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="active"><a class="nav-link active" data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_2" id="klick_tab_tampil">Tampil</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_3" id="klick_tab_tampil_tabel">Table View</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div class="row">
            <div class="col-md-12">
              <form perundang_undangan="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=perundang_undangan" enctype="multipart/form-data">
                <div class="card card-primary">
                  <div class="card-header with-border">
                    <h3 class="card-title" id="judul_formulir">Formulir Input Perundang-undangan</h3>
                  </div>
                  <div class="card-body">
                    <div class="form-group" style="display:none;">
                      <label for="temp">temp</label>
                      <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="mode">mode</label>
                      <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_perundang_undangan">id_perundang_undangan</label>
                      <input class="form-control" id="id_perundang_undangan" name="id" value="" placeholder="id_perundang_undangan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="jenis_perundang_undangan_id">Jenis Peraturan</label>
                      <select class="form-control" id="jenis_perundang_undangan_id" name="jenis_perundang_undangan_id">
                        <option value="">Pilih Salah Satu</option>
                        <?php
                        $where1      = array(
                          'jenis_perundang_undangan.status' => 1
                        );
                        $this->db->where($where1);
                        $this->db->order_by('jenis_perundang_undangan_name');
                        $this->db->select("*");
                        foreach ($this->db->get('jenis_perundang_undangan')->result() as $b1) {
                          echo '<option value="' . $b1->jenis_perundang_undangan_id . '">' . $b1->jenis_perundang_undangan_name . '</option>';
                        }
                        ?>
                        <select>
                    </div>
                    <div class="form-group">
                      <label for="nomor_peraturan">Nomor Peraturan</label>
                      <input class="form-control" id="nomor_peraturan" name="nomor_peraturan" value="" placeholder="nomor peraturan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="tahun_pengundangan">Tahun Peraturan</label>
                      <input class="form-control" id="nomor_peraturan" name="nomor_peraturan" value="" placeholder="tahun peraturan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="status_peraturan">Status Peraturan</label>
                      <input class="form-control" id="status_peraturan" name="status_peraturan" value="" placeholder="status peraturan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="nomor_panggil">nomor panggil</label>
                      <input class="form-control" id="nomor_panggil" name="nomor_panggil" value="" placeholder="nomor panggil" type="text">
                    </div>
                    <div class="form-group">
                      <label for="judul">judul</label>
                      <input class="form-control" id="judul" name="judul" value="" placeholder="judul" type="text">
                    </div>
                    <div class="form-group">
                      <label for="nama_pengarang">nama pengarang</label>
                      <input class="form-control" id="nama_pengarang" name="nama_pengarang" value="" placeholder="nama pengarang" type="text">
                    </div>
                    <div class="form-group">
                      <label for="tipe_pengarang">tipe pengarang</label>
                      <input class="form-control" id="tipe_pengarang" name="tipe_pengarang" value="" placeholder="tipe pengarang" type="text">
                    </div>
                    <div class="form-group">
                      <label for="jenis_pengarang">jenis pengarang</label>
                      <input class="form-control" id="jenis_pengarang" name="jenis_pengarang" value="" placeholder="jenis pengarang" type="text">
                    </div>
                    <div class="form-group">
                      <label for="jenis_peraturan">jenis peraturan</label>
                      <input class="form-control" id="jenis_peraturan" name="jenis_peraturan" value="" placeholder="jenis peraturan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="singkatan_jenis_peraturan">singkatan jenis peraturan</label>
                      <input class="form-control" id="singkatan_jenis_peraturan" name="singkatan_jenis_peraturan" value="" placeholder="singkatan jenis peraturan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="cetakan_edisi">Cetakan Edisi</label>
                      <input class="form-control" id="cetakan_edisi" name="cetakan_edisi" value="" placeholder="cetakan edisi" type="text">
                    </div>
                    <div class="form-group">
                      <label for="tempat_terbit">tempat terbit</label>
                      <input class="form-control" id="tempat_terbit" name="tempat_terbit" value="" placeholder="tempat terbit" type="text">
                    </div>
                    <div class="form-group">
                      <label for="penerbit">penerbit</label>
                      <input class="form-control" id="penerbit" name="penerbit" value="" placeholder="penerbit" type="text">
                    </div>
                    <div class="form-group">
                      <label for="tanggal_pengundangan">tanggal pengundangan</label>
                      <input class="form-control" id="tanggal_pengundangan" name="tanggal_pengundangan" value="0000-00-00" placeholder="tanggal pengundangan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="kolasi_deskripsi_fisik">kolasi deskripsi fisik</label>
                      <input class="form-control" id="kolasi_deskripsi_fisik" name="kolasi_deskripsi_fisik" value="" placeholder="kolasi deskripsi fisik" type="text">
                    </div>
                    <div class="form-group">
                      <label for="sumber">sumber</label>
                      <input class="form-control" id="sumber" name="sumber" value="" placeholder="sumber" type="text">
                    </div>
                    <div class="form-group">
                      <label for="subjek">subjek</label>
                      <input class="form-control" id="subjek" name="subjek" value="" placeholder="subjek" type="text">
                    </div>
                    <div class="form-group">
                      <label for="isbn">isbn</label>
                      <input class="form-control" id="isbn" name="isbn" value="" placeholder="isbn" type="text">
                    </div>
                    <div class="form-group">
                      <label for="bahasa">bahasa</label>
                      <input class="form-control" id="bahasa" name="bahasa" value="" placeholder="bahasa" type="text">
                    </div>
                    <div class="form-group">
                      <label for="bidang_hukum">bidang hukum</label>
                      <input class="form-control" id="bidang_hukum" name="bidang_hukum" value="" placeholder="bidang_hukum" type="text">
                    </div>
                    <div class="form-group">
                      <label for="nomor_induk_buku">nomor induk buku</label>
                      <input class="form-control" id="nomor_induk_buku" name="nomor_induk_buku" value="" placeholder="nomor induk buku" type="text">
                    </div>
                    <div class="form-group">
                      <label for="keterangan">Keterangan</label>
                      <input class="form-control" id="keterangan" name="keterangan" value="" placeholder="keterangan" type="text">
                    </div>
                  </div>
                  <div class="card-body">
                    <div class="form-group">
                      <label for="remake">Attachment Information</label>
                      <input class="form-control" id="remake" name="remake" placeholder="Attachment Information" type="text">
                    </div>
                    <div class="form-group">
                      <label for="myfile">File Lampiran </label>
                      <input type="file" size="60" name="myfile" id="file_lampiran">
                    </div>
                  </div>
                  <div class="card-body">
                    <h3 class="card-title">Attachment </h3>
                    <ul class="mailcard-attachments clearfix" id="tbl_attachment_perundang_undangan">
                    </ul>
                  </div>
                  <div class="overlay" id="overlay_input" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                  </div>
                  <div class="card-footer">
                    <button type="submit" class="btn btn-primary" id="simpan_perundang_undangan">SIMPAN</button>
                    <button type="submit" class="btn btn-primary" id="update_perundang_undangan" style="display:none;">UPDATE</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="col-md-6" style="display:none;">
              <div class="card card-danger">
                <div class="card-header">
                  <h3 class="card-title">
                    Modul By Role
                  </h3>
                </div>
                <div class="card-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>No</th>
                      <th>Role Name</th>
                      <th>Module Name</th>
                      <th>Controller</th>
                      <th>Information</th>
                    </tr>
                    <tbody id="perundang_undangan_by_jenis_perundang_undangan_id">
                    </tbody>
                  </table>
                </div>
                <div class="overlay" id="overlay_perundang_undangan_by_jenis_perundang_undangan" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab_2">
          <div class="row">
            <div class="col-md-12">
              <div class="card card-danger">
                <div class="card-header">
                  <h3 class="card-title">
                    Data Perundang-undangan
                    <button id="klikbukafilter" type="button" class="btn btn-card-tool"><i class="fa fa-search"></i> Buka Filter</button>
                    <button id="kliktutupfilter" style="display: none;" type="button" class="btn btn-card-tool"><i class="fa fa-times"></i> Tutup Filter</button>
                  </h3>
                </div>
                <div class="card-body" style="display: none;" id="filter">
                  <div class="row">
                    <div class="col-md-3">
                      <select name="limit_data_perundang_undangan" class="form-control input-sm" id="limit_data_perundang_undangan">
                        <option value="10">10 Per-Halaman</option>
                        <option value="20">20 Per-Halaman</option>
                        <option value="50">50 Per-Halaman</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <select name="urut_data_perundang_undangan" class="form-control input-sm" id="urut_data_perundang_undangan">
                        <option value="jenis_perundang_undangan.jenis_perundang_undangan_name">Jenis</option>
                        <option value="perundang_undangan.judul">Judul</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select name="shorting" id="shorting" class="form-control input-sm">
                        <option value="asc">A-z</option>
                        <option value="desc">Z-a</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select name="status_perundang_undangan" class="form-control input-sm" id="status_perundang_undangan">
                        <option value="1">Aktif</option>
                        <option value="0">Tidak AKtif</option>
                        <option value="99">Arsip</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <input name="kata_kunci" class="form-control input-sm" placeholder="Search" type="text" id="kata_kunci">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-body">
                  <ul id="tbl_utama_perundang_undangan" class="list-group list-group-flush">
                  </ul>
                </div>
                <div class="overlay" id="overlay_data" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div class="card-footer clearfix">
                  <ul id="pagination" class="pagination pagination-sm no-margin pull-right">
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab_3">
          <div class="row">
            <div class="col-md-12">
              <div class="card card-danger">
                <div class="card-header">
                  <h3 class="card-title">
                    Data Perundang-undangan
                  </h3>
                </div>
                <div class="card-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>No</th>
                      <th>Role Name</th>
                      <th>Module Name</th>
                      <th>Information</th>
                    </tr>
                    <tbody id="load_table_view">
                    </tbody>
                  </table>
                </div>
                <div class="overlay" id="overlay_data_tabel" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div class="card-footer clearfix">
                  <ul id="pagination_table_view" class="pagination pagination-sm no-margin pull-right">
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  function back_to_top() {
    $('html, body').animate({
      scrollTop: $('#awal').offset().top
    }, 1000);
  }

  function total(table, page, limit, order_by, keyword, order_field, status) {
    //$('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table: table,
        page: page,
        limit: limit,
        keyword: keyword,
        order_by: order_by,
        order_field: order_field,
        status: status
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>perundang_undangan/total/',
      success: function(text) {
        var tr = '';
        var total = parseInt(text);
        var page = parseInt(page);
        var limit = parseInt(limit);
        for (var i = 1; i <= total; i++) {
          if (i == page) {
            tr += '<li page="' + i + '" id="' + i + '"><a style="background: #cccccc;" id="nextPage" href="#">' + i + '</a></li>';
          } else {
            tr += '<li page="' + i + '" id="' + i + '"><a id="nextPage" href="#">' + i + '</a></li>';
          }
        }
        $('#pagination').html(tr);
        $('#pagination_table_view').html(tr);
      }
    });
  }

  function load_data_perundang_undangan(table, page, limit, order_by, keyword, order_field, status, shorting) {
    $('#overlay_data').show();
    total(table, page, limit, order_by, keyword, order_field, status);
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table: table,
        page: page,
        limit: limit,
        keyword: keyword,
        order_by: order_by,
        order_field: order_field,
        status: status,
        shorting: shorting
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>perundang_undangan/json/',
      success: function(json) {
        var tr = '';
        var tr2 = '';
        var start = ((page - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
          tr += '<li id_perundang_undangan="' + json[i].id_perundang_undangan + '" id="' + json[i].id_perundang_undangan + '" class="list-group-item">';
          tr += '<div class="media">';
          if (json[i].file == null) {
            tr += '  <a href="#">';
            tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;"  src="https://q-nang.com/cooladmin/images/icon/avatar-01.jpg" alt="Product Image">';
            tr += '  </a>';
          } else {
            var http = new XMLHttpRequest();
            http.open('HEAD', '<?php echo base_url(); ?>media/upload/' + json[i].file + '', false);
            http.send();
            if (http.status != 404) {
              tr += '  <a href="#">';
              tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;" src="<?php echo base_url(); ?>media/upload/' + json[i].file + '" alt="Product Image">';
              tr += '  </a>';
            } else {
              tr += '  <a href="#">';
              tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;"  src="https://q-nang.com/cooladmin/images/icon/avatar-01.jpg" alt="Product Image">';
              tr += '  </a>';
            }
          }
          tr += '  <div class="media-body">';
          tr += '    <a href="#" class="text-primary display-6">' + json[i].judul + '</a>';
          tr += '    <span class="pull-right-container">';
          if (json[i].status == 99) {
            tr += '  	   <span class="label pull-right"> <a href="#" id="restore_ajax" ><i class="fa fa-cut"></i> Restore</a> </span>';
          } else {
            tr += '  	   <span class="label pull-right"> <a href="#" id="del_ajax" class="btn btn-danger float-right btn-sm" ><i class="fa fa-cut"></i></a> </span>';
            tr += '  	   <span class="label pull-right"> <a href="#tab_1" data-toggle="tab" id="update_id" class="btn btn-info float-right btn-sm" ><i class="fa fa-edit"></i></a></span> ';
          }
          tr += '	 	 </span>';
          tr += '    <p>';
          tr += '          ' + json[i].jenis_perundang_undangan_name + '';
          tr += '    </p>';
          tr += '  </div>';
          tr += '  </div>';
          tr += '</li>';
        }
        $('#tbl_utama_perundang_undangan').html(tr);
        $('#overlay_data').hide();
      }
    });
  }

  function load_data_perundang_undangan_table(table, page, limit, order_by, keyword, order_field, status, shorting) {
    $('#overlay_data_tabel').show();
    total(table, page, limit, order_by, keyword, order_field, status);
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table: table,
        page: page,
        limit: limit,
        keyword: keyword,
        order_by: order_by,
        order_field: order_field,
        status: status,
        shorting: shorting
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>perundang_undangan/json/',
      success: function(json) {
        var tr = '';
        var start = ((page - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
          tr += '<tr id_perundang_undangan="' + json[i].id_perundang_undangan + '" id="' + json[i].id_perundang_undangan + '" >';
          tr += '<td valign="top">' + (start) + '</td>';
          tr += '<td valign="top">' + json[i].jenis_perundang_undangan_name + '</td>';
          tr += '<td valign="top">' + json[i].perundang_undangan_name + '</td>';
          tr += '<td valign="top">' + json[i].information + '</td>';
          tr += '</tr>';
        }
        $('#load_table_view').html(tr);
        $('#overlay_data_tabel').hide();
      }
    });
  }

  function simpan(isinya) {
    $.ajax({
      type: 'POST',
      async: true,
      data: isinya,
      dataType: 'text',
      url: '<?php echo base_url(); ?>perundang_undangan/simpan/',
      success: function(text) {
        if (text == 0) {
          alertify.error('Data gagal disimpan');
        } else {
          alertify.success('Data berhasil disimpan');
        }
      }
    });
  }

  function update(isinya) {
    $.ajax({
      type: 'POST',
      async: true,
      data: isinya,
      dataType: 'text',
      url: '<?php echo base_url(); ?>perundang_undangan/update/',
      success: function(text) {
        if (text == 0) {
          alertify.error('Data gagal diupdate');
        } else {
          alertify.success('Data berhasil diaupdate');
        }
      }
    });
  }

  function AttachmentByMode(mode, value) {
    $('#tbl_attachment_perundang_undangan').html('');
    if (mode == 'edit') {
      var link = 'load_html_lampiran_by_id';
      $.ajax({
        type: 'POST',
        async: true,
        data: {
          table_name: 'perundang_undangan',
          mode: mode,
          id_tabel: value
        },
        dataType: 'html',
        url: '<?php echo base_url(); ?>attachment/' + link + '/',
        success: function(html) {
          $('#tbl_attachment_perundang_undangan').html(html);
        }
      });
    } else {
      var link = 'load_html_lampiran_by_temp';
      $.ajax({
        type: 'POST',
        async: true,
        data: {
          table_name: 'perundang_undangan',
          mode: mode,
          temp: value
        },
        dataType: 'html',
        url: '<?php echo base_url(); ?>attachment/' + link + '/',
        success: function(html) {
          $('#tbl_attachment_perundang_undangan').html(html);
        }
      });
    }
  }

  $(document).ready(function() {
    $('#temp').val(Math.random());
  });

  $('#tbl_attachment_perundang_undangan').on('click', '#del_ajax', function(e) {
    e.preventDefault();
    var id = $(this).closest('li').attr('id_attachment');
    alertify.confirm("Anda yakin data akan dihapus?", function(e) {
      if (e) {
        $.ajax({
          dataType: "json",
          url: '<?php echo base_url(); ?>attachment/hapus/?id=' + id + '',
          success: function(response) {
            if (response.errors == 'Yes') {
              alertify.alert("Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya");
            } else {
              $('[id_attachment=' + id + ']').remove();
              $('#img_upload').remove();
              alertify.alert("Data berhasil dihapus");
            }
          }
        });
      } else {
        alertify.alert("Hapus data dibatalkan");
      }
    });
  });

  $(document).ready(function() {
    $("#klick_tab_tampil").on("click", function(e) {
      e.preventDefault();
      var page = '1';
      var limit = $('#limit_data_perundang_undangan').val();
      var keyword = $('#kata_kunci').val();
      var order_by = $('#urut_data_perundang_undangan').val();
      var order_field = $('#urut_data_perundang_undangan').val();
      var status = $('#status_perundang_undangan').val();
      var shorting = $('#shorting').val();
      load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
    });
  });

  $(document).ready(function() {
    $("#klick_tab_tampil_tabel").on("click", function(e) {
      e.preventDefault();
      var page = '1';
      var limit = $('#limit_data_perundang_undangan').val();
      var keyword = $('#kata_kunci').val();
      var order_by = $('#urut_data_perundang_undangan').val();
      var order_field = $('#urut_data_perundang_undangan').val();
      var status = $('#status_perundang_undangan').val();
      var shorting = $('#shorting').val();
      load_data_perundang_undangan_table('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
    });
  });

  $(document).ready(function() {
    $("#klik_tab_input").on("click", function(e) {
      e.preventDefault();
      $('#tbl_attachment_perundang_undangan').html('');
      $('#temp').val(Math.random());
      $('#judul_formulir').html('Formulir Input Perundang-undangan');
      $('#update_perundang_undangan').hide();
      $('#simpan_perundang_undangan').show();
      $('#id_perundang_undangan').val('');
      $('#jenis_perundang_undangan_id').val('');
      $("#nomor_peraturan").val('');
      $("#tahun_pengundangan").val('');
      $("#status_peraturan").val('');
      $("#nomor_panggil").val('');
      $("#judul").val('');
      $("#nama_pengarang").val('');
      $("#tipe_pengarang").val('');
      $("#jenis_pengarang").val('');
      $("#jenis_peraturan").val('');
      $("#singkatan_jenis_peraturan").val('');
      $("#cetakan_edisi").val('');
      $("#tempat_terbit").val('');
      $("#penerbit").val('');
      $("#tanggal_pengundangan").val('');
      $("#kolasi_deskripsi_fisik").val('');
      $("#sumber").val('');
      $("#subjek").val('');
      $("#isbn").val('');
      $("#bahasa").val('');
      $("#bidang_hukum").val('');
      $("#nomor_induk_buku").val('');
      $("#keterangan").val('');
      $('#mode').val('input');
    });
  });

  $(document).ready(function() {
    $("#limit_data_perundang_undangan").on("change", function(e) {
      e.preventDefault();
      var page = '1';
      var limit = $('#limit_data_perundang_undangan').val();
      var keyword = $('#kata_kunci').val();
      var order_by = $('#urut_data_perundang_undangan').val();
      var order_field = $('#urut_data_perundang_undangan').val();
      var status = $('#status_perundang_undangan').val();
      var shorting = $('#shorting').val();
      load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
    });
  });

  $(document).ready(function() {
    $("#urut_data_perundang_undangan").on("change", function(e) {
      e.preventDefault();
      var page = '1';
      var limit = $('#limit_data_perundang_undangan').val();
      var keyword = $('#kata_kunci').val();
      var order_by = $('#urut_data_perundang_undangan').val();
      var order_field = $('#urut_data_perundang_undangan').val();
      var status = $('#status_perundang_undangan').val();
      var shorting = $('#shorting').val();
      load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
    });
  });

  $(document).ready(function() {
    $("#shorting").on("change", function(e) {
      e.preventDefault();
      var page = '1';
      var limit = $('#limit_data_perundang_undangan').val();
      var keyword = $('#kata_kunci').val();
      var order_by = $('#urut_data_perundang_undangan').val();
      var order_field = $('#urut_data_perundang_undangan').val();
      var status = $('#status_perundang_undangan').val();
      var shorting = $('#shorting').val();
      load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
    });
  });

  $(document).ready(function() {
    $("#status_perundang_undangan").on("change", function(e) {
      e.preventDefault();
      var page = '1';
      var limit = $('#limit_data_perundang_undangan').val();
      var keyword = $('#kata_kunci').val();
      var order_by = $('#urut_data_perundang_undangan').val();
      var order_field = $('#urut_data_perundang_undangan').val();
      var status = $('#status_perundang_undangan').val();
      var shorting = $('#shorting').val();
      load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
    });
  });

  $(document).ready(function() {
    $('#pagination').on('click', '#nextPage', function(e) {
      e.preventDefault();
      var page = $(this).closest('li').attr('page');
      var limit = $('#limit_data_perundang_undangan').val();
      var keyword = $('#kata_kunci').val();
      var order_by = $('#urut_data_perundang_undangan').val();
      var order_field = $('#urut_data_perundang_undangan').val();
      var status = $('#status_perundang_undangan').val();
      var shorting = $('#shorting').val();
      load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
    });
  });

  $(document).ready(function() {
    $('#pagination_table_view').on('click', '#nextPage', function(e) {
      e.preventDefault();
      var page = $(this).closest('li').attr('page');
      var limit = $('#limit_data_perundang_undangan').val();
      var keyword = $('#kata_kunci').val();
      var order_by = $('#urut_data_perundang_undangan').val();
      var order_field = $('#urut_data_perundang_undangan').val();
      var status = $('#status_perundang_undangan').val();
      var shorting = $('#shorting').val();
      load_data_perundang_undangan_table('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
    });
  });

  $(document).ready(function() {
    function debounce(fn, duration) {
      var timer;
      return function() {
        clearTimeout(timer);
        timer = setTimeout(fn, duration);
      }
    }
    $('#kata_kunci').on('keyup', debounce(function() {
      var str = $('#kata_kunci').val();
      var n = str.length;
      var page = '1';
      var limit = $('#limit_data_perundang_undangan').val();
      var keyword = $('#kata_kunci').val();
      var order_by = $('#urut_data_perundang_undangan').val();
      var order_field = $('#urut_data_perundang_undangan').val();
      var status = $('#status_perundang_undangan').val();
      var shorting = $('#shorting').val();
      load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
    }, 1000));
  });

  $(document).ready(function() {
    $('#klikbukafilter').on('click', function(e) {
      e.preventDefault();
      $('#filter').show();
      $('#klikbukafilter').hide();
      $('#kliktutupfilter').show();
    });
  });

  $(document).ready(function() {
    $('#kliktutupfilter').on('click', function(e) {
      e.preventDefault();
      $('#filter').hide();
      $('#klikbukafilter').show();
      $('#kliktutupfilter').hide();
    });
  });

  $(document).ready(function() {
    $('#simpan_perundang_undangan').on('click', function(e) {
      e.preventDefault();
      var isinya = {
        temp: $('#temp').val(),
        jenis_perundang_undangan_id: $('#jenis_perundang_undangan_id').val(),
        nomor_peraturan: $('#nomor_peraturan').val(),
        tahun_pengundangan: $('#tahun_pengundangan').val(),
        status_peraturan: $('#status_peraturan').val(),
        nomor_panggil: $('#nomor_panggil').val(),
        judul: $('#judul').val(),
        nama_pengarang: $('#nama_pengarang').val(),
        tipe_pengarang: $('#tipe_pengarang').val(),
        jenis_pengarang: $('#jenis_pengarang').val(),
        jenis_peraturan: $('#jenis_peraturan').val(),
        singkatan_jenis_peraturan: $('#singkatan_jenis_peraturan').val(),
        cetakan_edisi: $('#cetakan_edisi').val(),
        tempat_terbit: $('#tempat_terbit').val(),
        penerbit: $('#penerbit').val(),
        tanggal_pengundangan: $('#tanggal_pengundangan').val(),
        kolasi_deskripsi_fisik: $('#kolasi_deskripsi_fisik').val(),
        sumber: $('#sumber').val(),
        subjek: $('#subjek').val(),
        isbn: $('#isbn').val(),
        bahasa: $('#bahasa').val(),
        bidang_hukum: $('#bidang_hukum').val(),
        nomor_induk_buku: $('#nomor_induk_buku').val(),
        keterangan: $('#keterangan').val()
      };
      simpan(isinya);
      back_to_top();
      perundang_undangan_by_jenis_perundang_undangan_id($('#jenis_perundang_undangan_id').val());
    });
  });

  $(document).ready(function() {
    $('#update_perundang_undangan').on('click', function(e) {
      e.preventDefault();
      var isinya = {
        jenis_perundang_undangan_id: $('#jenis_perundang_undangan_id').val(),
        nomor_peraturan: $('#nomor_peraturan').val(),
        tahun_pengundangan: $('#tahun_pengundangan').val(),
        status_peraturan: $('#status_peraturan').val(),
        nomor_panggil: $('#nomor_panggil').val(),
        judul: $('#judul').val(),
        nama_pengarang: $('#nama_pengarang').val(),
        tipe_pengarang: $('#tipe_pengarang').val(),
        jenis_pengarang: $('#jenis_pengarang').val(),
        jenis_peraturan: $('#jenis_peraturan').val(),
        singkatan_jenis_peraturan: $('#singkatan_jenis_peraturan').val(),
        cetakan_edisi: $('#cetakan_edisi').val(),
        tempat_terbit: $('#tempat_terbit').val(),
        penerbit: $('#penerbit').val(),
        tanggal_pengundangan: $('#tanggal_pengundangan').val(),
        kolasi_deskripsi_fisik: $('#kolasi_deskripsi_fisik').val(),
        sumber: $('#sumber').val(),
        subjek: $('#subjek').val(),
        isbn: $('#isbn').val(),
        bahasa: $('#bahasa').val(),
        bidang_hukum: $('#bidang_hukum').val(),
        nomor_induk_buku: $('#nomor_induk_buku').val(),
        keterangan: $('#keterangan').val()
      };
      update(isinya);
    });
  });

  $(document).ready(function() {
    $('#tbl_utama_perundang_undangan').on('click', '#update_id', function() {
      $('#overlay_input').show();
      $('#mode').val('edit');
      var id_perundang_undangan = $(this).closest('li').attr('id_perundang_undangan');
      $('#judul_formulir').html('Formulir Edit Perundang-undangan');
      $.ajax({
        type: 'POST',
        async: true,
        data: {
          id_perundang_undangan: id_perundang_undangan
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>perundang_undangan/get_by_id/',
        success: function(json) {
          $('#simpan_perundang_undangan').hide();
          $('#update_perundang_undangan').show();
          for (var i = 0; i < json.length; i++) {
            $('#id_perundang_undangan').val(json[i].id_perundang_undangan);
            $('#jenis_perundang_undangan_id').val(json[i].jenis_perundang_undangan_id);
            $('#nomor_peraturan').val(json[i].nomor_peraturan);
            $("#tahun_pengundangan").val(json[i].tahun_pengundangan);
            $("#status_peraturan").val(json[i].status_peraturan);
            $("#nomor_panggil").val(json[i].nomor_panggil);
            $("#judul").val(json[i].judul);
            $("#nama_pengarang").val(json[i].nama_pengarang);
            $("#tipe_pengarang").val(json[i].tipe_pengarang);
            $("#jenis_pengarang").val(json[i].jenis_pengarang);
            $("#jenis_peraturan").val(json[i].jenis_peraturan);
            $("#singkatan_jenis_peraturan").val(json[i].singkatan_jenis_peraturan);
            $("#cetakan_edisi").val(json[i].cetakan_edisi);
            $("#tempat_terbit").val(json[i].tempat_terbit);
            $("#penerbit").val(json[i].penerbit);
            $("#tanggal_pengundangan").val(json[i].tanggal_pengundangan);
            $("#kolasi_deskripsi_fisik").val(json[i].kolasi_deskripsi_fisik);
            $("#sumber").val(json[i].sumber);
            $("#subjek").val(json[i].subjek);
            $("#isbn").val(json[i].isbn);
            $("#bahasa").val(json[i].bahasa);
            $("#bidang_hukum").val(json[i].bidang_hukum);
            $("#nomor_induk_buku").val(json[i].nomor_induk_buku);
            $("#keterangan").val(json[i].keterangan);
          }
          $('#overlay_input').hide();
        }
      });
      AttachmentByMode($('#mode').val(), id_perundang_undangan);
    });
  });

  $(document).ready(function() {
    $('#tbl_utama_perundang_undangan').on('click', '#del_ajax', function() {
      var id_perundang_undangan = $(this).closest('li').attr('id_perundang_undangan');
      alertify.confirm('Anda yakin data akan dihapus?', function(e) {
        if (e) {
          $.ajax({
            type: 'POST',
            async: true,
            data: {
              id_perundang_undangan: id_perundang_undangan
            },
            dataType: 'text',
            url: '<?php echo base_url(); ?>perundang_undangan/delete_by_id/',
            success: function(text) {
              if (text == 0) {
                alertify.error('Data gagal dihapus');
              } else {
                alertify.success('Data berhasil dihapus');
                var page = '1';
                var limit = $('#limit_data_perundang_undangan').val();
                var keyword = $('#kata_kunci').val();
                var order_by = $('#urut_data_perundang_undangan').val();
                var order_field = $('#urut_data_perundang_undangan').val();
                var status = $('#status_perundang_undangan').val();
                var shorting = $('#shorting').val();
                load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
              }
            }
          });
        } else {
          alertify.error('Proses dibatalkan');
        }
      });
    });
  });

  $(document).ready(function() {
    $('#tbl_utama_perundang_undangan').on('click', '#restore_ajax', function() {
      var id_perundang_undangan = $(this).closest('li').attr('id_perundang_undangan');
      alertify.confirm('Anda yakin data akan dikembalikan?', function(e) {
        if (e) {
          $.ajax({
            type: 'POST',
            async: true,
            data: {
              id_perundang_undangan: id_perundang_undangan
            },
            dataType: 'text',
            url: '<?php echo base_url(); ?>perundang_undangan/restore_by_id/',
            success: function(text) {
              if (text == 0) {
                alertify.error('Data gagal dikembalikan');
              } else {
                alertify.success('Data berhasil dikembalikan');
                var page = '1';
                var limit = $('#limit_data_perundang_undangan').val();
                var keyword = $('#kata_kunci').val();
                var order_by = $('#urut_data_perundang_undangan').val();
                var order_field = $('#urut_data_perundang_undangan').val();
                var status = $('#status_perundang_undangan').val();
                var shorting = $('#shorting').val();
                load_data_perundang_undangan('perundang_undangan', page, limit, order_by, keyword, order_field, status, shorting);
              }
            }
          });
        } else {
          alertify.error('Proses dibatalkan');
        }
      });
    });
  });

  $(document).ready(function() {
    var options = {
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
      },
      uploadProgress: function(event, position, total, percentComplete) {
        $('#BarProgresUpload').width(percentComplete + '%');
        $('#PersenProgresUpload').html(percentComplete + '%');
      },
      success: function() {
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
      },
      complete: function(response) {
        $('#PesanProgresUpload').html('<font color="green">' + response.responseText + '</font>');
        var mode = $('#mode').val();
        if (mode == 'edit') {
          var value = $('#id_perundang_undangan').val();
        } else {
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
      },
      error: function() {
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
      }
    };
    document.getElementById('file_lampiran').onchange = function() {
      $('#form_isian').submit();
    };
    $('#form_isian').ajaxForm(options);
  });

  function perundang_undangan_by_jenis_perundang_undangan_id(jenis_perundang_undangan_id) {
    $('#overlay_perundang_undangan_by_jenis_perundang_undangan').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        jenis_perundang_undangan_id: jenis_perundang_undangan_id
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>perundang_undangan/perundang_undangan_by_jenis_perundang_undangan_id/',
      success: function(json) {
        var tr = '';
        var start = 1;
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
          tr += '<tr id_perundang_undangan="' + json[i].id_perundang_undangan + '" id="' + json[i].id_perundang_undangan + '" >';
          tr += '<td valign="top">' + (start) + '</td>';
          tr += '<td valign="top">' + json[i].jenis_perundang_undangan_name + '</td>';
          tr += '<td valign="top">' + json[i].perundang_undangan_name + '</td>';
          tr += '<td valign="top">' + json[i].perundang_undangan_code + '</td>';
          tr += '<td valign="top">' + json[i].information + '</td>';
          tr += '</tr>';
        }
        $('#perundang_undangan_by_jenis_perundang_undangan_id').html(tr);
        $('#overlay_perundang_undangan_by_jenis_perundang_undangan').hide();
      }
    });
  }

  $(document).ready(function() {
    $('#jenis_perundang_undangan_id').on('change', function(e) {
      e.preventDefault();
      perundang_undangan_by_jenis_perundang_undangan_id($('#jenis_perundang_undangan_id').val());
    });
  });
</script>