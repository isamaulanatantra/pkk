<section class="content" id="awal">
  <div class="row">
    <div class="col">
      <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="active"><a class="nav-link active" data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_2" id="klick_tab_tampil" >Tampil</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_3" id="klick_tab_tampil_tabel" >Table View</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
              <form module="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=template" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-9">
                    <div class="card card-danger">
                      <div class="card-body">
                        <div class="form-group">
                          <label for="template_head">Template Head</label>
                          <textarea class="form-control" rows="3" placeholder="Place some text here" id="template_head" name="template_head"></textarea>
                        </div>
                        <div class="form-group">
                          <label for="template_body">Template Body</label>
                          <textarea class="form-control" rows="13" placeholder="Place some text here" id="template_body" name="template_body"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-3">
                    <div class="card card-primary">
                      <div class="card-header with-border">
                        <h3 class="card-title" id="judul_formulir">Formulir Input Template</h3>
                      </div>
                      <div class="card-body">
                        <div class="form-group" style="display:none;">
                          <label for="temp">temp</label>
                          <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                          <input class="form-control" id="page" name="page" value="1" placeholder="page" type="text">
                        </div>
                        <div class="form-group" style="display:none;">
                          <label for="mode">mode</label>
                          <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                        </div>
                        <div class="form-group" style="display:none;">
                          <label for="template_id">template_id</label>
                          <input class="form-control" id="template_id" name="id" value="" placeholder="template_id" type="text">
                        </div>
                        <div class="form-group">
                          <label for="template_code">Template Code</label>
                          <input class="form-control" id="template_code" name="template_code" value="" placeholder="Template Code" type="text">
                        </div>
                        <div class="form-group">
                          <label for="template_name">Template Name</label>
                          <input class="form-control" id="template_name" name="template_name" value="" placeholder="Template Name" type="text">
                        </div>
                        <div class="form-group">
                          <label for="urutan_menu">Urutan Menu</label>
                          <input class="form-control" id="urutan_menu" name="urutan_menu" value="" placeholder="Urutan Menu" type="text">
                        </div>
                        <div class="form-group">
                          <label for="information">Information</label>
                          <input class="form-control" id="information" name="information" value="" placeholder="Information" type="text">
                        </div>
                      </div>
                      <div class="card-body">
                        <div class="form-group">
                          <label for="remake">Attachment Information</label>
                          <input class="form-control" id="remake" name="remake" placeholder="Attachment Information" type="text">
                        </div>
                        <div class="form-group">
                          <label for="myfile">File Lampiran </label>
                          <input type="file" size="60" name="myfile" id="file_lampiran" >
                        </div>
                      </div>
                      <div class="card-body">
                        <h3 class="card-title">Attachment </h3>
                        <ul class="mailcard-attachments clearfix" id="tbl_attachment_template">
                        </ul>
                      </div>
                      <div class="overlay" id="overlay_input" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                      <div class="card-footer">
                        <button type="submit" class="btn btn-primary" id="simpan_template">SIMPAN</button>
                        <button type="submit" class="btn btn-primary" id="update_template" style="display:none;">UPDATE</button>
                      </div>
                    </div>
                  </div>
                </div>
              </form>
        </div>
        <div class="tab-pane" id="tab_2">
          <div class="row">
            <div class="col-md-12">
              <div class="card card-danger">
                <div class="card-header">
                  <h3 class="card-title">
                    Data Template
                    <button id="klikbukafilter" type="button" class="btn btn-card-tool" ><i class="fa fa-search"></i> Buka Filter</button>
                    <button id="kliktutupfilter" style="display: none;" type="button" class="btn btn-card-tool" ><i class="fa fa-times"></i> Tutup Filter</button>
                  </h3>
                </div>
                <div class="card-body" style="display: none;" id="filter">
                  <div class="row">
                    <div class="col-md-3">
                      <select name="limit_data_template" class="form-control input-sm" id="limit_data_template">
                        <option value="10">10 Per-Halaman</option>
                        <option value="20">20 Per-Halaman</option>
                        <option value="50">50 Per-Halaman</option>
                      </select>
                    </div>
                    <div class="col-md-3">
                      <select name="urut_data_template" class="form-control input-sm" id="urut_data_template">
                        <option value="template.template_name">Template Name</option>
                        <option value="template.template_code">Template Code</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select name="shorting" id="shorting" class="form-control input-sm" >
                        <option value="asc">A-Z</option>
                        <option value="desc">Z-A</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <select name="status_template" class="form-control input-sm" id="status_template">
                        <option value="1">Aktif</option>
                        <option value="0">Tidak AKtif</option>
                        <option value="99">Arsip</option>
                      </select>
                    </div>
                    <div class="col-md-2">
                      <input name="kata_kunci" class="form-control input-sm" placeholder="Search" type="text" id="kata_kunci">
                    </div>
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-body">
                  <ul id="tbl_utama_template" class="products-list product-list-in-card">
                  </ul>
                </div>
                <div class="overlay" id="overlay_data" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div class="card-footer clearfix">
                  <ul id="pagination" class="pagination pagination-sm no-margin pull-right">
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab_3">
          <div class="row">
            <div class="col-md-12">
              <div class="card card-danger">
                <div class="card-header">
                  <h3 class="card-title">
                    Data Template <a href="#" id="download_exel"> <i class="fa fa-fw fa-download"></i> Download File Exel</a>
                  </h3>
                </div>
                <div class="card-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>No</th>
                      <th>Template Code</th>
                      <th>Template Name</th>
                      <th>Information</th>
                      <th>Urutan Menu</th>
                    </tr>
                    <tbody id="load_table_view">
                    </tbody>
                  </table>
                </div>
                <div class="overlay" id="overlay_data_tabel" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div class="card-footer clearfix">
                  <ul id="pagination_table_view" class="pagination pagination-sm no-margin pull-right">
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">

function back_to_top() {
  $('html, body').animate({
    scrollTop: $('#awal').offset().top
  }, 1000);
}

function total(table, page, limit, order_by, keyword, order_field, status) {
  //$('#spinners_data').show();
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status
    },
    dataType: 'text',
    url: '<?php echo base_url(); ?>template/total/',
    success: function (text) {
      var tr = '';
      var total = parseInt(text);
      var page = parseInt(page);
      var limit = parseInt(limit);
      for (var i = 1; i <= total; i++) {
        if( i == page ){
          tr += '<li page="'+i+'" id="'+i+'"><a style="background: #cccccc;" id="nextPage" href="#">'+i+'</a></li>';
        }
        else{
          tr += '<li page="'+i+'" id="'+i+'"><a id="nextPage" href="#">'+i+'</a></li>';
        }
      }
      $('#pagination').html(tr);
      $('#pagination_table_view').html(tr);
    }
  });
}

function load_data_template(table, page, limit, order_by, keyword, order_field, status, shorting) {
  $('#overlay_data').show();
  total(table, page, limit, order_by, keyword, order_field, status);
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status,
      shorting: shorting
    },
    dataType: 'json',
    url: '<?php echo base_url(); ?>template/json/',
    success: function (json) {
      var tr = '';
      var tr2 = '';
      var start = ((page - 1) * limit);
      for (var i = 0; i < json.length; i++) {
        var start = start + 1;
        tr += '<li template_id="' + json[i].template_id + '" id="' + json[i].template_id + '" class="item">';
        if (json[i].file == null) {
          tr += '  <div class="product-img">';
          tr += '    <img src="https://adminlte.io/themes/AdminLTE/dist/img/default-50x50.gif" alt="Product Image">';
          tr += '  </div>';
        } else {
          var http = new XMLHttpRequest();
          http.open('HEAD', '<?php echo base_url(); ?>media/upload/' + json[i].file + '', false);
          http.send();
          if (http.status != 404) {
            tr += '  <div class="product-img">';
            tr += '    <img src="<?php echo base_url(); ?>media/upload/' + json[i].file + '" alt="Product Image">';
            tr += '  </div>';
          } else {
            tr += '  <div class="product-img">';
            tr += '    <img src="https://adminlte.io/themes/AdminLTE/dist/img/default-50x50.gif" alt="Product Image">';
            tr += '  </div>';
          }
        }
        tr += '  <div class="product-info">';
        tr += '    <a href="#" class="product-title">' + json[i].template_code + '</a>';
        tr += '    <span class="pull-right-container">';
        if( json[i].status == 99 ){
          tr += '  	   <span class="label pull-right"> <a href="#" id="restore_ajax" ><i class="fa fa-cut"></i> Restore</a> </span>';
        }
        else{
          tr += '  	   <span class="label pull-right"> <a href="#" id="del_ajax" class="btn btn-danger float-right btn-sm" ><i class="fa fa-cut"></i></a> </span>';
          tr += '  	   <span class="label pull-right"> <a href="#tab_1" data-toggle="tab" id="update_id" class="btn btn-info float-right btn-sm" ><i class="fa fa-edit"></i></a></span> ';
        }
        tr += '	 	 </span>';
        tr += '      <span class="product-description">';
            tr += '          ' + json[i].template_name + '';
        /*var http = new XMLHttpRequest();
          http.open('HEAD', '<?php echo base_url(); ?>' + json[i].template_code + '', false);
          http.send();
          if (http.status != 404) {
            tr += '          ' + json[i].template_name + '';
          } else {
            tr += '     <span style="color:red">     ' + json[i].template_name + '</span>';
          }*/
        tr += '      </span>';
        tr += '  </div>';
        tr += '</li>';
      }
      $('#tbl_utama_template').html(tr);
      $('#overlay_data').hide();
    }
  });
}

function load_data_template_table(table, page, limit, order_by, keyword, order_field, status, shorting) {
  $('#overlay_data_tabel').show();
  total(table, page, limit, order_by, keyword, order_field, status);
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status,
      shorting: shorting
    },
    dataType: 'json',
    url: '<?php echo base_url(); ?>template/json/',
    success: function (json) {
      var tr = '';
      var start = ((page - 1) * limit);
      for (var i = 0; i < json.length; i++) {
        var start = start + 1;
        tr += '<tr template_id="' + json[i].template_id + '" id="' + json[i].template_id + '" >';
        tr += '<td valign="top">' + (start) + '</td>';
        tr += '<td valign="top">' + json[i].template_code + '</td>';
        tr += '<td valign="top">' + json[i].template_name + '</td>';
        tr += '<td valign="top">' + json[i].information + '</td>';
        tr += '<td valign="top">' + json[i].urutan_menu + '</td>';
        tr += '</tr>';
      }
      $('#load_table_view').html(tr);
      $('#overlay_data_tabel').hide();
    }
  });
}

function simpan(isinya) {
  $.ajax({
    type: 'POST',
    async: true,
    data: isinya,
    dataType: 'text',
    url: '<?php echo base_url(); ?>template/simpan/',
    success: function (text) {
      if (text == 0) {
        alertify.error('Data gagal disimpan');
      } else {
        alertify.success('Data berhasil disimpan');
      }
    }
  });
}

function update(isinya) {
  $.ajax({
    type: 'POST',
    async: true,
    data: isinya,
    dataType: 'text',
    url: '<?php echo base_url(); ?>template/update/',
    success: function (text) {
      if (text == 0) {
        alertify.error('Data gagal diupdate');
      } else {
        alertify.success('Data berhasil diaupdate');
      }
    }
  });
}

function AttachmentByMode(mode, value) {
  $('#tbl_attachment_template').html('');
  if (mode == 'edit') {
    var link = 'load_html_lampiran_by_id';
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table_name: 'template',
        mode: mode,
        id_tabel: value
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>attachment/' + link + '/',
      success: function (html) {
        $('#tbl_attachment_template').html(html);
      }
    });
  } else {
    var link = 'load_html_lampiran_by_temp';
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table_name: 'template',
        mode: mode,
        temp: value
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>attachment/' + link + '/',
      success: function (html) {
        $('#tbl_attachment_template').html(html);
      }
    });
  }
}

$(document).ready(function () {
  $('#temp').val(Math.random());
  $('.summernotehead').summernote({
    toolbar:[

      // This is a Custom Button in a new Toolbar Area
      ['custom', ['examplePlugin']],

      // You can also add Interaction to an existing Toolbar Area
      ['view', ['fullscreen', 'codeview', 'help']]
    ],
    placeholder: 'Klik gambar Source beralih ke Mode HTML khusus di dalam head contoh meta link script dll !',
    tabsize: 2,
    height: 100
  });
  $('.summernotebody').summernote({
    placeholder: 'Klik gambar Source beralih ke Mode HTML khusus di dalam body bisa dengan div dll !',
    tabsize: 2,
    height: 465
  });
});

$('#tbl_attachment_template').on('click', '#del_ajax', function (e) {
  e.preventDefault();
  var id = $(this).closest('li').attr('id_attachment');
  alertify.confirm("Anda yakin data akan dihapus?", function (e) {
    if (e) {
      $.ajax({
        dataType: "json",
        url: '<?php echo base_url(); ?>attachment/hapus/?id=' + id + '',
        success: function (response) {
          if (response.errors == 'Yes') {
            alertify.alert("Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya");
          } else {
            $('[id_attachment=' + id + ']').remove();
            $('#img_upload').remove();
            alertify.alert("Data berhasil dihapus");
          }
        }
      });
    } else {
      alertify.alert("Hapus data dibatalkan");
    }
  });
});

$(document).ready(function () {
  $("#klick_tab_tampil").on("click", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_template').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_template').val();
    var order_field = $('#urut_data_template').val();
    var status = $('#status_template').val();
    var shorting = $('#shorting').val();
    load_data_template('template', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#klick_tab_tampil_tabel").on("click", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_template').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_template').val();
    var order_field = $('#urut_data_template').val();
    var status = $('#status_template').val();
    var shorting = $('#shorting').val();
    load_data_template_table('template', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#klik_tab_input").on("click", function (e) {
    e.preventDefault();
    $('#tbl_attachment_template').html('');
    $('#temp').val(Math.random());
    $('#judul_formulir').html('Formulir Input Template');
    $('#update_template').hide();
    $('#simpan_template').show();
    $('#template_id').val('');
    $('#template_code').val('');
    $('#template_name').val('');
    $("#template_head").val('');
    $("#template_body").val('');
    $("#information").val('');
    $("#urutan_menu").val('');
    $('#mode').val('input');
  });
});

$(document).ready(function () {
  $("#limit_data_template").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_template').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_template').val();
    var order_field = $('#urut_data_template').val();
    var status = $('#status_template').val();
    var shorting = $('#shorting').val();
    load_data_template('template', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#urut_data_template").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_template').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_template').val();
    var order_field = $('#urut_data_template').val();
    var status = $('#status_template').val();
    var shorting = $('#shorting').val();
    load_data_template('template', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#shorting").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_template').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_template').val();
    var order_field = $('#urut_data_template').val();
    var status = $('#status_template').val();
    var shorting = $('#shorting').val();
    load_data_template('template', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#status_template").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_template').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_template').val();
    var order_field = $('#urut_data_template').val();
    var status = $('#status_template').val();
    var shorting = $('#shorting').val();
    load_data_template('template', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $('#pagination').on('click', '#nextPage', function (e) {
    e.preventDefault();
    var page = $(this).closest('li').attr('page');
    var limit = $('#limit_data_template').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_template').val();
    var order_field = $('#urut_data_template').val();
    var status = $('#status_template').val();
    var shorting = $('#shorting').val();
    load_data_template('template', page, limit, order_by, keyword, order_field, status, shorting);
    $('#page').val(page);
  });
});

$(document).ready(function () {
  $('#pagination_table_view').on('click', '#nextPage', function (e) {
    e.preventDefault();
    var page = $(this).closest('li').attr('page');
    var limit = $('#limit_data_template').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_template').val();
    var order_field = $('#urut_data_template').val();
    var status = $('#status_template').val();
    var shorting = $('#shorting').val();
    $('#page').val(page);
    load_data_template_table('template', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  function debounce(fn, duration) {
    var timer;
    return function () {
      clearTimeout(timer);
      timer = setTimeout(fn, duration);
    }
  }
  $('#kata_kunci').on('keyup', debounce(function () {
    var str = $('#kata_kunci').val();
    var n = str.length;
    var page = '1';
    var limit = $('#limit_data_template').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_template').val();
    var order_field = $('#urut_data_template').val();
    var status = $('#status_template').val();
    var shorting = $('#shorting').val();
    load_data_template('template', page, limit, order_by, keyword, order_field, status, shorting);
  }, 1000));
});

$(document).ready(function () {
  $('#klikbukafilter').on('click', function (e) {
    e.preventDefault();
    $('#filter').show();
    $('#klikbukafilter').hide();
    $('#kliktutupfilter').show();
  });
});

$(document).ready(function () {
  $('#kliktutupfilter').on('click', function (e) {
    e.preventDefault();
    $('#filter').hide();
    $('#klikbukafilter').show();
    $('#kliktutupfilter').hide();
  });
});

$(document).ready(function () {
  $('#simpan_template').on('click', function (e) {
    e.preventDefault();
    var isinya = {
      temp: $('#temp').val(),
      template_code: $('#template_code').val(),
      template_name: $('#template_name').val(),
      template_head: $('#template_head').val(),
      template_body: $('#template_body').val(),
      information: $('#information').val(),
      urutan_menu: $('#urutan_menu').val()
    };
    simpan(isinya);
    back_to_top();
  });
});

$(document).ready(function () {
  $('#update_template').on('click', function (e) {
    e.preventDefault();
    var isinya = {
      template_id: $('#template_id').val(),
      template_code: $('#template_code').val(),
      template_name: $('#template_name').val(),
      template_head: $('#template_head').val(),
      template_body: $('#template_body').val(),
      information: $('#information').val(),
      urutan_menu: $('#urutan_menu').val()
    };
    update(isinya);
  });
});

$(document).ready(function () {
  $('#tbl_utama_template').on('click', '#update_id', function () {
    $('#overlay_input').show();
    $('#mode').val('edit');
    var template_id = $(this).closest('li').attr('template_id');
    $('#judul_formulir').html('Formulir Edit Template');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        template_id: template_id
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>template/get_by_id/',
      success: function (json) {
        $('#simpan_template').hide();
        $('#update_template').show();
        for (var i = 0; i < json.length; i++) {
          $('#template_id').val(json[i].template_id);
          $('#template_code').val(json[i].template_code);
          $('#template_name').val(json[i].template_name);
          $("#template_head").val(json[i].template_head);
          $("#template_body").val(json[i].template_body);
          $("#information").val(json[i].information);
          $("#urutan_menu").val(json[i].urutan_menu);
        }
        $('#overlay_input').hide();
      }
    });
    AttachmentByMode($('#mode').val(), template_id);
  });
});

$(document).ready(function () {
  $('#tbl_utama_template').on('click', '#del_ajax', function () {
    var template_id = $(this).closest('li').attr('template_id');
    alertify.confirm('Anda yakin data akan dihapus?', function (e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            template_id: template_id
          },
          dataType: 'text',
          url: '<?php echo base_url(); ?>template/delete_by_id/',
          success: function (text) {
            if (text == 0) {
              alertify.error('Data gagal dihapus');
            } else {
              alertify.success('Data berhasil dihapus');
              var page = '1';
              var limit = $('#limit_data_template').val();
              var keyword = $('#kata_kunci').val();
              var order_by = $('#urut_data_template').val();
              var order_field = $('#urut_data_template').val();
              var status = $('#status_template').val();
              var shorting = $('#shorting').val();
              load_data_template('template', page, limit, order_by, keyword, order_field, status, shorting);
            }
          }
        });
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});

$(document).ready(function () {
  $('#tbl_utama_template').on('click', '#restore_ajax', function () {
    var template_id = $(this).closest('li').attr('template_id');
    alertify.confirm('Anda yakin data akan dikembalikan?', function (e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            template_id: template_id
          },
          dataType: 'text',
          url: '<?php echo base_url(); ?>template/restore_by_id/',
          success: function (text) {
            if (text == 0) {
              alertify.error('Data gagal dikembalikan');
            } else {
              alertify.success('Data berhasil dikembalikan');
              var page = '1';
              var limit = $('#limit_data_template').val();
              var keyword = $('#kata_kunci').val();
              var order_by = $('#urut_data_template').val();
              var order_field = $('#urut_data_template').val();
              var status = $('#status_template').val();
              var shorting = $('#shorting').val();
              load_data_template('template', page, limit, order_by, keyword, order_field, status, shorting);
            }
          }
        });
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});

$(document).ready(function () {
  var options = {
    beforeSend: function () {
      $('#ProgresUpload').show();
      $('#BarProgresUpload').width('0%');
      $('#PesanProgresUpload').html('');
      $('#PersenProgresUpload').html('0%');
    },
    uploadProgress: function (event, position, total, percentComplete) {
      $('#BarProgresUpload').width(percentComplete + '%');
      $('#PersenProgresUpload').html(percentComplete + '%');
    },
    success: function () {
      $('#BarProgresUpload').width('100%');
      $('#PersenProgresUpload').html('100%');
    },
    complete: function (response) {
      $('#PesanProgresUpload').html('<font color="green">' + response.responseText + '</font>');
      var mode = $('#mode').val();
      if (mode == 'edit') {
        var value = $('#template_id').val();
      } else {
        var value = $('#temp').val();
      }
      AttachmentByMode(mode, value);
      $('#remake').val('');
    },
    error: function () {
      $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
    }
  };
  document.getElementById('file_lampiran').onchange = function () {
    $('#form_isian').submit();
  };
  $('#form_isian').ajaxForm(options);
});


$(document).ready(function () {
  $("#download_exel").on("click", function (e) {
    e.preventDefault();
    var page = $('#page').val();
    var limit = $('#limit_data_template').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_template').val();
    var order_field = $('#urut_data_template').val();
    var status = $('#status_template').val();
    var shorting = $('#shorting').val();
    window.location = "<?php echo base_url(); ?>template/xls/?page="+page+"&limit="+limit+"&keyword="+keyword+"&order_by="+order_by+"&order_field="+order_field+"&status="+status+"&shorting="+shorting+"";
  });
});

</script>