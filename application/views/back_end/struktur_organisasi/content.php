<section class="content" id="awal">
  <div class="row">
    <div class="col">
      <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="active"><a class="nav-link active" data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_2" id="klick_tab_tampil" >Tampil</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_3" id="klick_tab_tampil_tabel" >Table View</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-6">
                                        <form periode_struktur_organisasi="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=struktur_organisasi" enctype="multipart/form-data">
                                            <div class="card card-primary">
                                                <div class="card-header with-border">
                                                    <h3 class="card-title" id="judul_formulir">Formulir Input Periode struktur organisasi</h3>
                                                </div>
                                                <div class="card-body">
                                                    <div class="form-group" style="display:none;">
                                                        <label for="temp">temp</label>
                                                        <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                                                    </div>
                                                    <div class="form-group" style="display:none;">
                                                        <label for="mode">mode</label>
                                                        <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                                                    </div>
                                                    <div class="form-group" style="display:none;">
                                                        <label for="struktur_organisasi_id">struktur_organisasi_id</label>
                                                        <input class="form-control" id="struktur_organisasi_id" name="id" value="" placeholder="struktur_organisasi_id" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="periode_struktur_organisasi_id">Periode Struktur Organisasi</label>
                                                        <select class="form-control" id="periode_struktur_organisasi_id" name="periode_struktur_organisasi_id">
                                                            <option value="">Pilih Salah Satu</option>
                                                            <?php
                                        $where2      = array(
                                          'periode_struktur_organisasi.status' => 1
                                        );
                                        $this->db->where($where2);
                                        $this->db->order_by('periode_struktur_organisasi_name');
                                        $this->db->select("*");
                                        foreach ($this->db->get('periode_struktur_organisasi')->result() as $b2) {
                                          echo '<option value="'.$b2->periode_struktur_organisasi_id.'">'.$b2->periode_struktur_organisasi_name.' ['.$b2->periode_struktur_organisasi_code.']</option>';
                                          }
                                        ?>
                                                                <select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nama">Nama</label>
                                                        <input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nip">NIP</label>
                                                        <input class="form-control" id="nip" name="nip" value="" placeholder="NIP" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="jabatan">Jabatan</label>
                                                        <input class="form-control" id="jabatan" name="jabatan" value="" placeholder="Jabatan" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="information">Information</label>
                                                        <input class="form-control" id="information" name="information" value="" placeholder="Information" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nomor_urut">Nomor Urut</label>
                                                        <input class="form-control" id="nomor_urut" name="nomor_urut" value="" placeholder="" type="text">
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="remake">Attachment Information</label>
                                                        <input class="form-control" id="remake" name="remake" placeholder="Attachment Information" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="myfile">File Lampiran </label>
                                                        <input type="file" size="60" name="myfile" id="file_lampiran">
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <h3 class="card-title">Attachment </h3>
                                                    <ul class="mailcard-attachments clearfix" id="tbl_attachment_struktur_organisasi">
                                                    </ul>
                                                </div>
                                                <div class="overlay" id="overlay_input" style="display:none;">
                                                    <i class="fa fa-refresh fa-spin"></i>
                                                </div>
                                                <div class="card-footer">
                                                    <button type="submit" class="btn btn-primary" id="simpan_struktur_organisasi">SIMPAN</button>
                                                    <button type="submit" class="btn btn-primary" id="update_struktur_organisasi" style="display:none;">UPDATE</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
        </div>
        <div class="tab-pane" id="tab_2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card card-danger">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                    Data Periode struktur organisasi
                                    <button id="klikbukafilter" type="button" class="btn btn-card-tool" ><i class="fa fa-search"></i> Buka Filter</button>
                                    <button id="kliktutupfilter" style="display: none;" type="button" class="btn btn-card-tool" ><i class="fa fa-times"></i> Tutup Filter</button>
                                  </h3>
                                            </div>
                                            <div class="card-body" style="display: none;" id="filter">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <select name="limit_data_struktur_organisasi" class="form-control input-sm" id="limit_data_struktur_organisasi">
                                                            <option value="10">10 Per-Halaman</option>
                                                            <option value="20">20 Per-Halaman</option>
                                                            <option value="50">50 Per-Halaman</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select name="urut_data_struktur_organisasi" class="form-control input-sm" id="urut_data_struktur_organisasi">
                                                            <option value="periode_struktur_organisasi.periode_struktur_organisasi_name">Module</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <select name="shorting" id="shorting" class="form-control input-sm">
                                                            <option value="asc">A-Z</option>
                                                            <option value="desc">Z-A</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <select name="status_struktur_organisasi" class="form-control input-sm" id="status_struktur_organisasi">
                                                            <option value="1">Aktif</option>
                                                            <option value="0">Tidak AKtif</option>
                                                            <option value="99">Arsip</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <input name="kata_kunci" class="form-control input-sm" placeholder="Search" type="text" id="kata_kunci">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-body">
                                                <ul id="tbl_utama_struktur_organisasi" class="list-group list-group-flush">
                                                </ul>
                                            </div>
                                            <div class="overlay" id="overlay_data" style="display:none;">
                                                <i class="fa fa-refresh fa-spin"></i>
                                            </div>
                                            <div class="card-footer clearfix">
                                                <ul id="pagination" class="pagination pagination-sm no-margin pull-right">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        </div>
        <div class="tab-pane" id="tab_3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card card-danger">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                    Data Periode struktur organisasi
                                  </h3>
                                            </div>
                                            <div class="card-body table-responsive no-padding">
                                                <table class="table table-hover">
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Role Name</th>
                                                        <th>Module Name</th>
                                                        <th>Information</th>
                                                    </tr>
                                                    <tbody id="load_table_view">
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="overlay" id="overlay_data_tabel" style="display:none;">
                                                <i class="fa fa-refresh fa-spin"></i>
                                            </div>
                                            <div class="card-footer clearfix">
                                                <ul id="pagination_table_view" class="pagination pagination-sm no-margin pull-right">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">

function back_to_top() {
  $('html, body').animate({
    scrollTop: $('#awal').offset().top
  }, 1000);
}

function total(table, page, limit, order_by, keyword, order_field, status) {
  //$('#spinners_data').show();
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status
    },
    dataType: 'text',
    url: '<?php echo base_url(); ?>struktur_organisasi/total/',
    success: function (text) {
      var tr = '';
      var total = parseInt(text);
      var page = parseInt(page);
      var limit = parseInt(limit);
      for (var i = 1; i <= total; i++) {
        if( i == page ){
          tr += '<li page="'+i+'" id="'+i+'"><a style="background: #cccccc;" id="nextPage" href="#">'+i+'</a></li>';
        }
        else{
          tr += '<li page="'+i+'" id="'+i+'"><a id="nextPage" href="#">'+i+'</a></li>';
        }
      }
      $('#pagination').html(tr);
      $('#pagination_table_view').html(tr);
    }
  });
}

function load_data_struktur_organisasi(table, page, limit, order_by, keyword, order_field, status, shorting) {
  $('#overlay_data').show();
  total(table, page, limit, order_by, keyword, order_field, status);
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status,
      shorting: shorting
    },
    dataType: 'json',
    url: '<?php echo base_url(); ?>struktur_organisasi/json/',
    success: function (json) {
      var tr = '';
      var tr2 = '';
      var start = ((page - 1) * limit);
      for (var i = 0; i < json.length; i++) {
        var start = start + 1;
        tr += '<li struktur_organisasi_id="' + json[i].struktur_organisasi_id + '" id="' + json[i].struktur_organisasi_id + '" class="list-group-item">';        tr += '<div class="media">';
        if (json[i].file == null) {          tr += '  <a href="#">';          tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;"  src="https://q-nang.com/cooladmin/images/icon/avatar-01.jpg" alt="Product Image">';
          tr += '  </a>';
        } else {
          var http = new XMLHttpRequest();
          http.open('HEAD', '<?php echo base_url(); ?>media/upload/' + json[i].file + '', false);
          http.send();
          if (http.status != 404) {          tr += '  <a href="#">';
            tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;" src="<?php echo base_url(); ?>media/upload/' + json[i].file + '" alt="Product Image">';
            tr += '  </a>';
          } else {          tr += '  <a href="#">';          tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;"  src="https://q-nang.com/cooladmin/images/icon/avatar-01.jpg" alt="Product Image">';
            tr += '  </a>';
          }
        }        tr += '  <div class="media-body">';
        tr += '    <a href="#" class="text-primary display-6">' + json[i].periode_struktur_organisasi_name + '</a>';
        tr += '    <span class="pull-right-container">';
        if( json[i].status == 99 ){
          tr += '  	   <span class="label pull-right"> <a href="#" id="restore_ajax" ><i class="fa fa-cut"></i> Restore</a> </span>';
        }
        else{
          tr += '  	   <span class="label pull-right"> <a href="#" id="del_ajax" class="btn btn-danger float-right btn-sm" ><i class="fa fa-cut"></i></a> </span>';
          tr += '  	   <span class="label pull-right"> <a href="#tab_1" data-toggle="tab" id="update_id" class="btn btn-info float-right btn-sm" ><i class="fa fa-edit"></i></a></span> ';
        }
        tr += '	 	 </span>';
        tr += '    <p>';
        tr += '          ' + json[i].nama + '';
        tr += '    </p>';
        tr += '    <p>';
        tr += '          ' + json[i].jabatan + '';
        tr += '    </p>';
        tr += '  </div>';        tr += '  </div>';
        tr += '</li>';
      }
      $('#tbl_utama_struktur_organisasi').html(tr);
      $('#overlay_data').hide();
    }
  });
}

function load_data_struktur_organisasi_table(table, page, limit, order_by, keyword, order_field, status, shorting) {
  $('#overlay_data_tabel').show();
  total(table, page, limit, order_by, keyword, order_field, status);
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status,
      shorting: shorting
    },
    dataType: 'json',
    url: '<?php echo base_url(); ?>struktur_organisasi/json/',
    success: function (json) {
      var tr = '';
      var start = ((page - 1) * limit);
      for (var i = 0; i < json.length; i++) {
        var start = start + 1;
        tr += '<tr struktur_organisasi_id="' + json[i].struktur_organisasi_id + '" id="' + json[i].struktur_organisasi_id + '" >';
        tr += '<td valign="top">' + (start) + '</td>';
        tr += '<td valign="top">' + json[i].nama + '</td>';
        tr += '<td valign="top">' + json[i].jabatan + '</td>';
        tr += '<td valign="top">' + json[i].periode_struktur_organisasi_name + '</td>';
        tr += '<td valign="top">' + json[i].information + '</td>';
        tr += '</tr>';
      }
      $('#load_table_view').html(tr);
      $('#overlay_data_tabel').hide();
    }
  });
}

function simpan(isinya) {
  $.ajax({
    type: 'POST',
    async: true,
    data: isinya,
    dataType: 'text',
    url: '<?php echo base_url(); ?>struktur_organisasi/simpan/',
    success: function (text) {
      if (text == 0) {
        alertify.error('Data gagal disimpan');
      } else {
        alertify.success('Data berhasil disimpan');
      }
    }
  });
}

function update(isinya) {
  $.ajax({
    type: 'POST',
    async: true,
    data: isinya,
    dataType: 'text',
    url: '<?php echo base_url(); ?>struktur_organisasi/update/',
    success: function (text) {
      if (text == 0) {
        alertify.error('Data gagal diupdate');
      } else {
        alertify.success('Data berhasil diaupdate');
      }
    }
  });
}

function AttachmentByMode(mode, value) {
  $('#tbl_attachment_struktur_organisasi').html('');
  if (mode == 'edit') {
    var link = 'load_html_lampiran_by_id';
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table_name: 'struktur_organisasi',
        mode: mode,
        id_tabel: value
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>attachment/' + link + '/',
      success: function (html) {
        $('#tbl_attachment_struktur_organisasi').html(html);
      }
    });
  } else {
    var link = 'load_html_lampiran_by_temp';
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table_name: 'struktur_organisasi',
        mode: mode,
        temp: value
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>attachment/' + link + '/',
      success: function (html) {
        $('#tbl_attachment_struktur_organisasi').html(html);
      }
    });
  }
}

$(document).ready(function () {
  $('#temp').val(Math.random());
});

$('#tbl_attachment_struktur_organisasi').on('click', '#del_ajax', function (e) {
  e.preventDefault();
  var id = $(this).closest('li').attr('id_attachment');
  alertify.confirm("Anda yakin data akan dihapus?", function (e) {
    if (e) {
      $.ajax({
        dataType: "json",
        url: '<?php echo base_url(); ?>attachment/hapus/?id=' + id + '',
        success: function (response) {
          if (response.errors == 'Yes') {
            alertify.alert("Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya");
          } else {
            $('[id_attachment=' + id + ']').remove();
            $('#img_upload').remove();
            alertify.alert("Data berhasil dihapus");
          }
        }
      });
    } else {
      alertify.alert("Hapus data dibatalkan");
    }
  });
});

$(document).ready(function () {
  $("#klick_tab_tampil").on("click", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_struktur_organisasi').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_struktur_organisasi').val();
    var order_field = $('#urut_data_struktur_organisasi').val();
    var status = $('#status_struktur_organisasi').val();
    var shorting = $('#shorting').val();
    load_data_struktur_organisasi('struktur_organisasi', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#klick_tab_tampil_tabel").on("click", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_struktur_organisasi').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_struktur_organisasi').val();
    var order_field = $('#urut_data_struktur_organisasi').val();
    var status = $('#status_struktur_organisasi').val();
    var shorting = $('#shorting').val();
    load_data_struktur_organisasi_table('struktur_organisasi', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#klik_tab_input").on("click", function (e) {
    e.preventDefault();
    $('#tbl_attachment_struktur_organisasi').html('');
    $('#temp').val(Math.random());
    $('#judul_formulir').html('Formulir Input Periode struktur organisasi');
    $('#update_struktur_organisasi').hide();
    $('#simpan_struktur_organisasi').show();
    $('#struktur_organisasi_id').val('');
    $('#periode_struktur_organisasi_id').val('');
    $("#nama").val('');
    $("#nip").val('');
    $("#jabatan").val('');
    $("#information").val('');
    $("#nomor_urut").val('');
    $('#mode').val('input');
  });
});

$(document).ready(function () {
  $("#limit_data_struktur_organisasi").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_struktur_organisasi').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_struktur_organisasi').val();
    var order_field = $('#urut_data_struktur_organisasi').val();
    var status = $('#status_struktur_organisasi').val();
    var shorting = $('#shorting').val();
    load_data_struktur_organisasi('struktur_organisasi', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#urut_data_struktur_organisasi").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_struktur_organisasi').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_struktur_organisasi').val();
    var order_field = $('#urut_data_struktur_organisasi').val();
    var status = $('#status_struktur_organisasi').val();
    var shorting = $('#shorting').val();
    load_data_struktur_organisasi('struktur_organisasi', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#shorting").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_struktur_organisasi').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_struktur_organisasi').val();
    var order_field = $('#urut_data_struktur_organisasi').val();
    var status = $('#status_struktur_organisasi').val();
    var shorting = $('#shorting').val();
    load_data_struktur_organisasi('struktur_organisasi', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#status_struktur_organisasi").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_struktur_organisasi').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_struktur_organisasi').val();
    var order_field = $('#urut_data_struktur_organisasi').val();
    var status = $('#status_struktur_organisasi').val();
    var shorting = $('#shorting').val();
    load_data_struktur_organisasi('struktur_organisasi', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $('#pagination').on('click', '#nextPage', function (e) {
    e.preventDefault();
    var page = $(this).closest('li').attr('page');
    var limit = $('#limit_data_struktur_organisasi').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_struktur_organisasi').val();
    var order_field = $('#urut_data_struktur_organisasi').val();
    var status = $('#status_struktur_organisasi').val();
    var shorting = $('#shorting').val();
    load_data_struktur_organisasi('struktur_organisasi', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $('#pagination_table_view').on('click', '#nextPage', function (e) {
    e.preventDefault();
    var page = $(this).closest('li').attr('page');
    var limit = $('#limit_data_struktur_organisasi').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_struktur_organisasi').val();
    var order_field = $('#urut_data_struktur_organisasi').val();
    var status = $('#status_struktur_organisasi').val();
    var shorting = $('#shorting').val();
    load_data_struktur_organisasi_table('struktur_organisasi', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  function debounce(fn, duration) {
    var timer;
    return function () {
      clearTimeout(timer);
      timer = setTimeout(fn, duration);
    }
  }
  $('#kata_kunci').on('keyup', debounce(function () {
    var str = $('#kata_kunci').val();
    var n = str.length;
    var page = '1';
    var limit = $('#limit_data_struktur_organisasi').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_struktur_organisasi').val();
    var order_field = $('#urut_data_struktur_organisasi').val();
    var status = $('#status_struktur_organisasi').val();
    var shorting = $('#shorting').val();
    load_data_struktur_organisasi('struktur_organisasi', page, limit, order_by, keyword, order_field, status, shorting);
  }, 1000));
});

$(document).ready(function () {
  $('#klikbukafilter').on('click', function (e) {
    e.preventDefault();
    $('#filter').show();
    $('#klikbukafilter').hide();
    $('#kliktutupfilter').show();
  });
});

$(document).ready(function () {
  $('#kliktutupfilter').on('click', function (e) {
    e.preventDefault();
    $('#filter').hide();
    $('#klikbukafilter').show();
    $('#kliktutupfilter').hide();
  });
});

$(document).ready(function () {
  $('#simpan_struktur_organisasi').on('click', function (e) {
    e.preventDefault();
    var isinya = {
      temp: $('#temp').val(),
      periode_struktur_organisasi_id: $('#periode_struktur_organisasi_id').val(),
      nama: $('#nama').val(),
      nip: $('#nip').val(),
      jabatan: $('#jabatan').val(),
      nomor_urut: $('#nomor_urut').val(),
      information: $('#information').val()
    };
    simpan(isinya);
    back_to_top();
  });
});

$(document).ready(function () {
  $('#update_struktur_organisasi').on('click', function (e) {
    e.preventDefault();
    var isinya = {
      struktur_organisasi_id: $('#struktur_organisasi_id').val(),
      periode_struktur_organisasi_id: $('#periode_struktur_organisasi_id').val(),
      nama: $('#nama').val(),
      nip: $('#nip').val(),
      jabatan: $('#jabatan').val(),
      nomor_urut: $('#nomor_urut').val(),
      information: $('#information').val()
    };
    update(isinya);
  });
});

$(document).ready(function () {
  $('#tbl_utama_struktur_organisasi').on('click', '#update_id', function () {
    $('#overlay_input').show();
    $('#mode').val('edit');
    var struktur_organisasi_id = $(this).closest('li').attr('struktur_organisasi_id');
    $('#judul_formulir').html('Formulir Edit Periode struktur organisasi');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        struktur_organisasi_id: struktur_organisasi_id
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>struktur_organisasi/get_by_id/',
      success: function (json) {
        $('#simpan_struktur_organisasi').hide();
        $('#update_struktur_organisasi').show();
        for (var i = 0; i < json.length; i++) {
          $('#struktur_organisasi_id').val(json[i].struktur_organisasi_id);
          $('#periode_struktur_organisasi_id').val(json[i].periode_struktur_organisasi_id);
          $("#information").val(json[i].information);
          $("#nomor_urut").val(json[i].nomor_urut);
          $("#nama").val(json[i].nama);
          $("#nip").val(json[i].nip);
          $("#jabatan").val(json[i].jabatan);
        }
        $('#overlay_input').hide();
      }
    });
    AttachmentByMode($('#mode').val(), struktur_organisasi_id);
  });
});

$(document).ready(function () {
  $('#tbl_utama_struktur_organisasi').on('click', '#del_ajax', function () {
    var struktur_organisasi_id = $(this).closest('li').attr('struktur_organisasi_id');
    alertify.confirm('Anda yakin data akan dihapus?', function (e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            struktur_organisasi_id: struktur_organisasi_id
          },
          dataType: 'text',
          url: '<?php echo base_url(); ?>struktur_organisasi/delete_by_id/',
          success: function (text) {
            if (text == 0) {
              alertify.error('Data gagal dihapus');
            } else {
              alertify.success('Data berhasil dihapus');
              var page = '1';
              var limit = $('#limit_data_struktur_organisasi').val();
              var keyword = $('#kata_kunci').val();
              var order_by = $('#urut_data_struktur_organisasi').val();
              var order_field = $('#urut_data_struktur_organisasi').val();
              var status = $('#status_struktur_organisasi').val();
              var shorting = $('#shorting').val();
              load_data_struktur_organisasi('struktur_organisasi', page, limit, order_by, keyword, order_field, status, shorting);
            }
          }
        });
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});

$(document).ready(function () {
  $('#tbl_utama_struktur_organisasi').on('click', '#restore_ajax', function () {
    var struktur_organisasi_id = $(this).closest('li').attr('struktur_organisasi_id');
    alertify.confirm('Anda yakin data akan dikembalikan?', function (e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            struktur_organisasi_id: struktur_organisasi_id
          },
          dataType: 'text',
          url: '<?php echo base_url(); ?>struktur_organisasi/restore_by_id/',
          success: function (text) {
            if (text == 0) {
              alertify.error('Data gagal dikembalikan');
            } else {
              alertify.success('Data berhasil dikembalikan');
              var page = '1';
              var limit = $('#limit_data_struktur_organisasi').val();
              var keyword = $('#kata_kunci').val();
              var order_by = $('#urut_data_struktur_organisasi').val();
              var order_field = $('#urut_data_struktur_organisasi').val();
              var status = $('#status_struktur_organisasi').val();
              var shorting = $('#shorting').val();
              load_data_struktur_organisasi('struktur_organisasi', page, limit, order_by, keyword, order_field, status, shorting);
            }
          }
        });
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});

$(document).ready(function () {
  var options = {
    beforeSend: function () {
      $('#ProgresUpload').show();
      $('#BarProgresUpload').width('0%');
      $('#PesanProgresUpload').html('');
      $('#PersenProgresUpload').html('0%');
    },
    uploadProgress: function (event, position, total, percentComplete) {
      $('#BarProgresUpload').width(percentComplete + '%');
      $('#PersenProgresUpload').html(percentComplete + '%');
    },
    success: function () {
      $('#BarProgresUpload').width('100%');
      $('#PersenProgresUpload').html('100%');
    },
    complete: function (response) {
      $('#PesanProgresUpload').html('<font color="green">' + response.responseText + '</font>');
      var mode = $('#mode').val();
      if (mode == 'edit') {
        var value = $('#struktur_organisasi_id').val();
      } else {
        var value = $('#temp').val();
      }
      AttachmentByMode(mode, value);
      $('#remake').val('');
    },
    error: function () {
      $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
    }
  };
  document.getElementById('file_lampiran').onchange = function () {
    $('#form_isian').submit();
  };
  $('#form_isian').ajaxForm(options);
});


</script>