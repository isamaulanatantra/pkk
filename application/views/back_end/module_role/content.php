<section class="content" id="awal">
  <div class="row">
    <div class="col">
      <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
        <li class="active"><a class="nav-link active" data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_2" id="klick_tab_tampil" >Tampil</a></li>
        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tab_3" id="klick_tab_tampil_tabel" >Table View</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-4">
                                        <form module_role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=module_role" enctype="multipart/form-data">
                                            <div class="card card-primary">
                                                <div class="card-header with-border">
                                                    <h3 class="card-title" id="judul_formulir">Formulir Input Module Role</h3>
                                                </div>
                                                <div class="card-body">
                                                    <div class="form-group" style="display:none;">
                                                        <label for="temp">temp</label>
                                                        <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                                                    </div>
                                                    <div class="form-group" style="display:none;">
                                                        <label for="mode">mode</label>
                                                        <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                                                    </div>
                                                    <div class="form-group" style="display:none;">
                                                        <label for="module_role_id">module_role_id</label>
                                                        <input class="form-control" id="module_role_id" name="id" value="" placeholder="module_role_id" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="role_id">Role Name</label>
                                                        <select class="form-control" id="role_id" name="role_id">
                                                            <option value="">Pilih Salah Satu</option>
                                                            <?php
                                        $where1      = array(
                                          'role.status' => 1
                                        );
                                        $this->db->where($where1);
                                        $this->db->order_by('role_name');
                                        $this->db->select("*");
                                        foreach ($this->db->get('role')->result() as $b1) {
                                          echo '<option value="'.$b1->role_id.'">'.$b1->role_name.'</option>';
                                          }
                                        ?>
                                                                <select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="module_id">Module Name</label>
                                                        <select class="form-control" id="module_id" name="module_id">
                                                            <option value="">Pilih Salah Satu</option>
                                                            <?php
                                        $where2      = array(
                                          'module.status' => 1
                                        );
                                        $this->db->where($where2);
                                        $this->db->order_by('module_name');
                                        $this->db->select("*");
                                        foreach ($this->db->get('module')->result() as $b2) {
                                          echo '<option value="'.$b2->module_id.'">'.$b2->module_name.' ['.$b2->module_code.']</option>';
                                          }
                                        ?>
                                                                <select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="information">Information</label>
                                                        <input class="form-control" id="information" name="information" value="" placeholder="Information" type="text">
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <div class="form-group">
                                                        <label for="remake">Attachment Information</label>
                                                        <input class="form-control" id="remake" name="remake" placeholder="Attachment Information" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="myfile">File Lampiran </label>
                                                        <input type="file" size="60" name="myfile" id="file_lampiran">
                                                    </div>
                                                </div>
                                                <div class="card-body">
                                                    <h3 class="card-title">Attachment </h3>
                                                    <ul class="mailcard-attachments clearfix" id="tbl_attachment_module_role">
                                                    </ul>
                                                </div>
                                                <div class="overlay" id="overlay_input" style="display:none;">
                                                    <i class="fa fa-refresh fa-spin"></i>
                                                </div>
                                                <div class="card-footer">
                                                    <button type="submit" class="btn btn-primary" id="simpan_module_role">SIMPAN</button>
                                                    <button type="submit" class="btn btn-primary" id="update_module_role" style="display:none;">UPDATE</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card card-danger">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                    Modul By Role
                                  </h3>
                                            </div>
                                            <div class="card-body table-responsive no-padding">
                                                <table class="table table-hover">
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Role Name</th>
                                                        <th>Module Name</th>
                                                        <th>Controller</th>
                                                        <th>Information</th>
                                                    </tr>
                                                    <tbody id="module_by_role_id">
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="overlay" id="overlay_module_by_role" style="display:none;">
                                                <i class="fa fa-refresh fa-spin"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        </div>
        <div class="tab-pane" id="tab_2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card card-danger">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                    Data Module Role
                                    <button id="klikbukafilter" type="button" class="btn btn-card-tool" ><i class="fa fa-search"></i> Buka Filter</button>
                                    <button id="kliktutupfilter" style="display: none;" type="button" class="btn btn-card-tool" ><i class="fa fa-times"></i> Tutup Filter</button>
                                  </h3>
                                            </div>
                                            <div class="card-body" style="display: none;" id="filter">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <select name="limit_data_module_role" class="form-control input-sm" id="limit_data_module_role">
                                                            <option value="10">10 Per-Halaman</option>
                                                            <option value="20">20 Per-Halaman</option>
                                                            <option value="50">50 Per-Halaman</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <select name="urut_data_module_role" class="form-control input-sm" id="urut_data_module_role">
                                                            <option value="role.role_name">Role</option>
                                                            <option value="module.module_name">Module</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <select name="shorting" id="shorting" class="form-control input-sm">
                                                            <option value="asc">A-Z</option>
                                                            <option value="desc">Z-A</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <select name="status_module_role" class="form-control input-sm" id="status_module_role">
                                                            <option value="1">Aktif</option>
                                                            <option value="0">Tidak AKtif</option>
                                                            <option value="99">Arsip</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <input name="kata_kunci" class="form-control input-sm" placeholder="Search" type="text" id="kata_kunci">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- /.card-body -->
                                            <div class="card-body">
                                                <ul id="tbl_utama_module_role" class="list-group list-group-flush">
                                                </ul>
                                            </div>
                                            <div class="overlay" id="overlay_data" style="display:none;">
                                                <i class="fa fa-refresh fa-spin"></i>
                                            </div>
                                            <div class="card-footer clearfix">
                                                <ul id="pagination" class="pagination pagination-sm no-margin pull-right">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        </div>
        <div class="tab-pane" id="tab_3">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card card-danger">
                                            <div class="card-header">
                                                <h3 class="card-title">
                                    Data Module Role
                                  </h3>
                                            </div>
                                            <div class="card-body table-responsive no-padding">
                                                <table class="table table-hover">
                                                    <tr>
                                                        <th>No</th>
                                                        <th>Role Name</th>
                                                        <th>Module Name</th>
                                                        <th>Information</th>
                                                    </tr>
                                                    <tbody id="load_table_view">
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="overlay" id="overlay_data_tabel" style="display:none;">
                                                <i class="fa fa-refresh fa-spin"></i>
                                            </div>
                                            <div class="card-footer clearfix">
                                                <ul id="pagination_table_view" class="pagination pagination-sm no-margin pull-right">
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">

function back_to_top() {
  $('html, body').animate({
    scrollTop: $('#awal').offset().top
  }, 1000);
}

function total(table, page, limit, order_by, keyword, order_field, status) {
  //$('#spinners_data').show();
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status
    },
    dataType: 'text',
    url: '<?php echo base_url(); ?>module_role/total/',
    success: function (text) {
      var tr = '';
      var total = parseInt(text);
      var page = parseInt(page);
      var limit = parseInt(limit);
      for (var i = 1; i <= total; i++) {
        if( i == page ){
          tr += '<li page="'+i+'" id="'+i+'"><a style="background: #cccccc;" id="nextPage" href="#">'+i+'</a></li>';
        }
        else{
          tr += '<li page="'+i+'" id="'+i+'"><a id="nextPage" href="#">'+i+'</a></li>';
        }
      }
      $('#pagination').html(tr);
      $('#pagination_table_view').html(tr);
    }
  });
}

function load_data_module_role(table, page, limit, order_by, keyword, order_field, status, shorting) {
  $('#overlay_data').show();
  total(table, page, limit, order_by, keyword, order_field, status);
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status,
      shorting: shorting
    },
    dataType: 'json',
    url: '<?php echo base_url(); ?>module_role/json/',
    success: function (json) {
      var tr = '';
      var tr2 = '';
      var start = ((page - 1) * limit);
      for (var i = 0; i < json.length; i++) {
        var start = start + 1;
        tr += '<li module_role_id="' + json[i].module_role_id + '" id="' + json[i].module_role_id + '" class="list-group-item">';        tr += '<div class="media">';
        if (json[i].file == null) {          tr += '  <a href="#">';          tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;"  src="https://q-nang.com/cooladmin/images/icon/avatar-01.jpg" alt="Product Image">';
          tr += '  </a>';
        } else {
          var http = new XMLHttpRequest();
          http.open('HEAD', '<?php echo base_url(); ?>media/upload/' + json[i].file + '', false);
          http.send();
          if (http.status != 404) {          tr += '  <a href="#">';
            tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;" src="<?php echo base_url(); ?>media/upload/' + json[i].file + '" alt="Product Image">';
            tr += '  </a>';
          } else {          tr += '  <a href="#">';          tr += '    <img class="align-self-center image mr-3" style="width:85px; height:85px;"  src="https://q-nang.com/cooladmin/images/icon/avatar-01.jpg" alt="Product Image">';
            tr += '  </a>';
          }
        }        tr += '  <div class="media-body">';
        tr += '    <a href="#" class="text-primary display-6">' + json[i].module_name + '</a>';
        tr += '    <span class="pull-right-container">';
        if( json[i].status == 99 ){
          tr += '  	   <span class="label pull-right"> <a href="#" id="restore_ajax" ><i class="fa fa-cut"></i> Restore</a> </span>';
        }
        else{
          tr += '  	   <span class="label pull-right"> <a href="#" id="del_ajax" class="btn btn-danger float-right btn-sm" ><i class="fa fa-cut"></i></a> </span>';
          tr += '  	   <span class="label pull-right"> <a href="#tab_1" data-toggle="tab" id="update_id" class="btn btn-info float-right btn-sm" ><i class="fa fa-edit"></i></a></span> ';
        }
        tr += '	 	 </span>';
        tr += '    <p>';
        tr += '          ' + json[i].role_name + '';
        tr += '    </p>';
        tr += '  </div>';        tr += '  </div>';
        tr += '</li>';
      }
      $('#tbl_utama_module_role').html(tr);
      $('#overlay_data').hide();
    }
  });
}

function load_data_module_role_table(table, page, limit, order_by, keyword, order_field, status, shorting) {
  $('#overlay_data_tabel').show();
  total(table, page, limit, order_by, keyword, order_field, status);
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      table: table,
      page: page,
      limit: limit,
      keyword: keyword,
      order_by: order_by,
      order_field: order_field,
      status: status,
      shorting: shorting
    },
    dataType: 'json',
    url: '<?php echo base_url(); ?>module_role/json/',
    success: function (json) {
      var tr = '';
      var start = ((page - 1) * limit);
      for (var i = 0; i < json.length; i++) {
        var start = start + 1;
        tr += '<tr module_role_id="' + json[i].module_role_id + '" id="' + json[i].module_role_id + '" >';
        tr += '<td valign="top">' + (start) + '</td>';
        tr += '<td valign="top">' + json[i].role_name + '</td>';
        tr += '<td valign="top">' + json[i].module_name + '</td>';
        tr += '<td valign="top">' + json[i].information + '</td>';
        tr += '</tr>';
      }
      $('#load_table_view').html(tr);
      $('#overlay_data_tabel').hide();
    }
  });
}

function simpan(isinya) {
  $.ajax({
    type: 'POST',
    async: true,
    data: isinya,
    dataType: 'text',
    url: '<?php echo base_url(); ?>module_role/simpan/',
    success: function (text) {
      if (text == 0) {
        alertify.error('Data gagal disimpan');
      } else {
        alertify.success('Data berhasil disimpan');
      }
    }
  });
}

function update(isinya) {
  $.ajax({
    type: 'POST',
    async: true,
    data: isinya,
    dataType: 'text',
    url: '<?php echo base_url(); ?>module_role/update/',
    success: function (text) {
      if (text == 0) {
        alertify.error('Data gagal diupdate');
      } else {
        alertify.success('Data berhasil diaupdate');
      }
    }
  });
}

function AttachmentByMode(mode, value) {
  $('#tbl_attachment_module_role').html('');
  if (mode == 'edit') {
    var link = 'load_html_lampiran_by_id';
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table_name: 'module_role',
        mode: mode,
        id_tabel: value
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>attachment/' + link + '/',
      success: function (html) {
        $('#tbl_attachment_module_role').html(html);
      }
    });
  } else {
    var link = 'load_html_lampiran_by_temp';
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        table_name: 'module_role',
        mode: mode,
        temp: value
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>attachment/' + link + '/',
      success: function (html) {
        $('#tbl_attachment_module_role').html(html);
      }
    });
  }
}

$(document).ready(function () {
  $('#temp').val(Math.random());
});

$('#tbl_attachment_module_role').on('click', '#del_ajax', function (e) {
  e.preventDefault();
  var id = $(this).closest('li').attr('id_attachment');
  alertify.confirm("Anda yakin data akan dihapus?", function (e) {
    if (e) {
      $.ajax({
        dataType: "json",
        url: '<?php echo base_url(); ?>attachment/hapus/?id=' + id + '',
        success: function (response) {
          if (response.errors == 'Yes') {
            alertify.alert("Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya");
          } else {
            $('[id_attachment=' + id + ']').remove();
            $('#img_upload').remove();
            alertify.alert("Data berhasil dihapus");
          }
        }
      });
    } else {
      alertify.alert("Hapus data dibatalkan");
    }
  });
});

$(document).ready(function () {
  $("#klick_tab_tampil").on("click", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module_role').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module_role').val();
    var order_field = $('#urut_data_module_role').val();
    var status = $('#status_module_role').val();
    var shorting = $('#shorting').val();
    load_data_module_role('module_role', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#klick_tab_tampil_tabel").on("click", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module_role').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module_role').val();
    var order_field = $('#urut_data_module_role').val();
    var status = $('#status_module_role').val();
    var shorting = $('#shorting').val();
    load_data_module_role_table('module_role', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#klik_tab_input").on("click", function (e) {
    e.preventDefault();
    $('#tbl_attachment_module_role').html('');
    $('#temp').val(Math.random());
    $('#judul_formulir').html('Formulir Input Module Role');
    $('#update_module_role').hide();
    $('#simpan_module_role').show();
    $('#module_role_id').val('');
    $('#role_id').val('');
    $('#module_id').val('');
    $("#information").val('');
    $('#mode').val('input');
  });
});

$(document).ready(function () {
  $("#limit_data_module_role").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module_role').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module_role').val();
    var order_field = $('#urut_data_module_role').val();
    var status = $('#status_module_role').val();
    var shorting = $('#shorting').val();
    load_data_module_role('module_role', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#urut_data_module_role").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module_role').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module_role').val();
    var order_field = $('#urut_data_module_role').val();
    var status = $('#status_module_role').val();
    var shorting = $('#shorting').val();
    load_data_module_role('module_role', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#shorting").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module_role').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module_role').val();
    var order_field = $('#urut_data_module_role').val();
    var status = $('#status_module_role').val();
    var shorting = $('#shorting').val();
    load_data_module_role('module_role', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $("#status_module_role").on("change", function (e) {
    e.preventDefault();
    var page = '1';
    var limit = $('#limit_data_module_role').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module_role').val();
    var order_field = $('#urut_data_module_role').val();
    var status = $('#status_module_role').val();
    var shorting = $('#shorting').val();
    load_data_module_role('module_role', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $('#pagination').on('click', '#nextPage', function (e) {
    e.preventDefault();
    var page = $(this).closest('li').attr('page');
    var limit = $('#limit_data_module_role').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module_role').val();
    var order_field = $('#urut_data_module_role').val();
    var status = $('#status_module_role').val();
    var shorting = $('#shorting').val();
    load_data_module_role('module_role', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  $('#pagination_table_view').on('click', '#nextPage', function (e) {
    e.preventDefault();
    var page = $(this).closest('li').attr('page');
    var limit = $('#limit_data_module_role').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module_role').val();
    var order_field = $('#urut_data_module_role').val();
    var status = $('#status_module_role').val();
    var shorting = $('#shorting').val();
    load_data_module_role_table('module_role', page, limit, order_by, keyword, order_field, status, shorting);
  });
});

$(document).ready(function () {
  function debounce(fn, duration) {
    var timer;
    return function () {
      clearTimeout(timer);
      timer = setTimeout(fn, duration);
    }
  }
  $('#kata_kunci').on('keyup', debounce(function () {
    var str = $('#kata_kunci').val();
    var n = str.length;
    var page = '1';
    var limit = $('#limit_data_module_role').val();
    var keyword = $('#kata_kunci').val();
    var order_by = $('#urut_data_module_role').val();
    var order_field = $('#urut_data_module_role').val();
    var status = $('#status_module_role').val();
    var shorting = $('#shorting').val();
    load_data_module_role('module_role', page, limit, order_by, keyword, order_field, status, shorting);
  }, 1000));
});

$(document).ready(function () {
  $('#klikbukafilter').on('click', function (e) {
    e.preventDefault();
    $('#filter').show();
    $('#klikbukafilter').hide();
    $('#kliktutupfilter').show();
  });
});

$(document).ready(function () {
  $('#kliktutupfilter').on('click', function (e) {
    e.preventDefault();
    $('#filter').hide();
    $('#klikbukafilter').show();
    $('#kliktutupfilter').hide();
  });
});

$(document).ready(function () {
  $('#simpan_module_role').on('click', function (e) {
    e.preventDefault();
    var isinya = {
      temp: $('#temp').val(),
      role_id: $('#role_id').val(),
      module_id: $('#module_id').val(),
      information: $('#information').val()
    };
    simpan(isinya);
    back_to_top();
    module_by_role_id($('#role_id').val());
  });
});

$(document).ready(function () {
  $('#update_module_role').on('click', function (e) {
    e.preventDefault();
    var isinya = {
      module_role_id: $('#module_role_id').val(),
      role_id: $('#role_id').val(),
      module_id: $('#module_id').val(),
      information: $('#information').val()
    };
    update(isinya);
  });
});

$(document).ready(function () {
  $('#tbl_utama_module_role').on('click', '#update_id', function () {
    $('#overlay_input').show();
    $('#mode').val('edit');
    var module_role_id = $(this).closest('li').attr('module_role_id');
    $('#judul_formulir').html('Formulir Edit Module Role');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        module_role_id: module_role_id
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>module_role/get_by_id/',
      success: function (json) {
        $('#simpan_module_role').hide();
        $('#update_module_role').show();
        for (var i = 0; i < json.length; i++) {
          $('#module_role_id').val(json[i].module_role_id);
          $('#role_id').val(json[i].role_id);
          $('#module_id').val(json[i].module_id);
          $("#information").val(json[i].information);
        }
        $('#overlay_input').hide();
      }
    });
    AttachmentByMode($('#mode').val(), module_role_id);
  });
});

$(document).ready(function () {
  $('#tbl_utama_module_role').on('click', '#del_ajax', function () {
    var module_role_id = $(this).closest('li').attr('module_role_id');
    alertify.confirm('Anda yakin data akan dihapus?', function (e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            module_role_id: module_role_id
          },
          dataType: 'text',
          url: '<?php echo base_url(); ?>module_role/delete_by_id/',
          success: function (text) {
            if (text == 0) {
              alertify.error('Data gagal dihapus');
            } else {
              alertify.success('Data berhasil dihapus');
              var page = '1';
              var limit = $('#limit_data_module_role').val();
              var keyword = $('#kata_kunci').val();
              var order_by = $('#urut_data_module_role').val();
              var order_field = $('#urut_data_module_role').val();
              var status = $('#status_module_role').val();
              var shorting = $('#shorting').val();
              load_data_module_role('module_role', page, limit, order_by, keyword, order_field, status, shorting);
            }
          }
        });
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});

$(document).ready(function () {
  $('#tbl_utama_module_role').on('click', '#restore_ajax', function () {
    var module_role_id = $(this).closest('li').attr('module_role_id');
    alertify.confirm('Anda yakin data akan dikembalikan?', function (e) {
      if (e) {
        $.ajax({
          type: 'POST',
          async: true,
          data: {
            module_role_id: module_role_id
          },
          dataType: 'text',
          url: '<?php echo base_url(); ?>module_role/restore_by_id/',
          success: function (text) {
            if (text == 0) {
              alertify.error('Data gagal dikembalikan');
            } else {
              alertify.success('Data berhasil dikembalikan');
              var page = '1';
              var limit = $('#limit_data_module_role').val();
              var keyword = $('#kata_kunci').val();
              var order_by = $('#urut_data_module_role').val();
              var order_field = $('#urut_data_module_role').val();
              var status = $('#status_module_role').val();
              var shorting = $('#shorting').val();
              load_data_module_role('module_role', page, limit, order_by, keyword, order_field, status, shorting);
            }
          }
        });
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});

$(document).ready(function () {
  var options = {
    beforeSend: function () {
      $('#ProgresUpload').show();
      $('#BarProgresUpload').width('0%');
      $('#PesanProgresUpload').html('');
      $('#PersenProgresUpload').html('0%');
    },
    uploadProgress: function (event, position, total, percentComplete) {
      $('#BarProgresUpload').width(percentComplete + '%');
      $('#PersenProgresUpload').html(percentComplete + '%');
    },
    success: function () {
      $('#BarProgresUpload').width('100%');
      $('#PersenProgresUpload').html('100%');
    },
    complete: function (response) {
      $('#PesanProgresUpload').html('<font color="green">' + response.responseText + '</font>');
      var mode = $('#mode').val();
      if (mode == 'edit') {
        var value = $('#module_role_id').val();
      } else {
        var value = $('#temp').val();
      }
      AttachmentByMode(mode, value);
      $('#remake').val('');
    },
    error: function () {
      $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
    }
  };
  document.getElementById('file_lampiran').onchange = function () {
    $('#form_isian').submit();
  };
  $('#form_isian').ajaxForm(options);
});

function module_by_role_id(role_id) {
  $('#overlay_module_by_role').show();
  $.ajax({
    type: 'POST',
    async: true,
    data: {
      role_id: role_id
    },
    dataType: 'json',
    url: '<?php echo base_url(); ?>module_role/module_by_role_id/',
    success: function (json) {
      var tr = '';
      var start = 1;
      for (var i = 0; i < json.length; i++) {
        var start = start + 1;
        tr += '<tr module_role_id="' + json[i].module_role_id + '" id="' + json[i].module_role_id + '" >';
        tr += '<td valign="top">' + (start) + '</td>';
        tr += '<td valign="top">' + json[i].role_name + '</td>';
        tr += '<td valign="top">' + json[i].module_name + '</td>';
        tr += '<td valign="top">' + json[i].module_code + '</td>';
        tr += '<td valign="top">' + json[i].information + '</td>';
        tr += '</tr>';
      }
      $('#module_by_role_id').html(tr);
      $('#overlay_module_by_role').hide();
    }
  });
}

$(document).ready(function () {
  $('#role_id').on('change', function (e) {
    e.preventDefault();
    module_by_role_id($('#role_id').val());
  });
});

</script>