<!-- === BEGIN HEADER === -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class=" js no-touch cssanimations">
    <!--<![endif]-->
    <head>
        <!-- Title -->
        <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
        <!-- Meta -->
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
        <meta name="author" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>,">
        <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>">
        <meta name="og:description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
        <meta name="og:url" content="<?php echo base_url(); ?>"/>
        <meta name="og:title" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>"/>
        <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
        <meta name="og:keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
        <!-- Favicon -->
        <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />
        
  <!-- Fonts START -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->          
  <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Global styles END --> 
   
  <!-- Page level plugin styles START -->
  <link href="<?php echo base_url(); ?>Template/theme/assets/pages/css/animate.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="<?php echo base_url(); ?>Template/theme/assets/pages/css/components.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>Template/theme/assets/pages/css/slider.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>Template/theme/assets/corporate/css/style.css" rel="stylesheet"> 
  <link href="<?php echo base_url(); ?>Template/theme/assets/corporate/css/style-responsive.css" rel="stylesheet">
        <?php
        $web=$this->uut->namadomain(base_url());
        ?>
  <link href="<?php echo base_url(); ?>Template/theme/assets/corporate/css/custom.css" rel="stylesheet">
  <!-- Theme styles END
  <link href="<?php echo base_url(); ?>Template/theme/assets/corporate/css/themes/red.css" rel="stylesheet" id="style-color">
  -->
    <!-- Bootstrap 3.3.2 -->
    <link href="<?php echo base_url(); ?>boots/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>Template/zb/js/slider.js?v=3.2.1" type="text/javascript" defer="defer"></script>
       
		<!-- Alert Confirmation -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/Typeahead-BS3-css.css" id="toggleCSS" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>boots/dist/css/bootstrap-timepicker.min.css" />
    <link href="<?php echo base_url(); ?>Template/theme/assets/corporate/css/temane.css" rel="stylesheet" id="style-color">
		<!-- DataTables -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/datatables.net-bs/css/dataTables.bootstrap.min.css">
	
        <?php
        $web=$this->uut->namadomain(base_url());
        $where = array(
          'status' => 1,
          'domain' => $web
          );
        $this->db->where($where);
        $this->db->limit(1);
        $query = $this->db->get('tema_website');
        $tema = 'biru.css';
        foreach ($query->result() as $row)
          {
            $tema = $row->nama_tema_website;
          }
        echo '<link rel="stylesheet" href="'.base_url().'assets/css/'.$tema.'" rel="stylesheet">';
        
        ?>
	<style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>
            <script src="<?php echo base_url(); ?>Template/HTML/assets/js/bootstrap.min.js"></script>
		
            <style type="text/css">
                a.gflag {
                    vertical-align: middle;
                    font-size: 15px;
                    padding: 0px;
                    background-repeat: no-repeat;
                    background-image: url(//gtranslate.net/flags/16.png);
                }

                a.gflag img {
                    border: 0;
                }

                a.gflag:hover {
                    background-image: url(//gtranslate.net/flags/16a.png);
                }

                #goog-gt-tt {
                    display: none !important;
                }

                .goog-te-banner-frame {
                    display: none !important;
                }

                .goog-te-menu-value:hover {
                    text-decoration: none !important;
                }

                body {
                    top: 0 !important;
                }

                #google_translate_element2 {
                    display: none !important;
                }
            </style>
</head>
<!-- Body BEGIN -->
<body class="corporate">

    <!-- BEGIN TOP BAR -->
    <div class="pre-header">
        <div class="container">
            <div class="row">
                <!-- BEGIN TOP BAR LEFT PART -->
                <div class="col-md-6 col-sm-6 additional-shop-info">
                    <ul class="list-unstyled list-inline">
                        <li><i class="fa fa-phone"></i><span><?php if(!empty( $telpon )){ echo $telpon; } ?></span></li>
                        <li><i class="fa fa-envelope-o"></i><span><?php if(!empty( $email )){ echo $email; } ?></span></li>
                    </ul>
                </div>
                <!-- END TOP BAR LEFT PART -->
                <!-- BEGIN TOP BAR MENU -->
                <div class="col-md-6 col-sm-6 additional-nav">
                    <ul class="list-unstyled list-inline pull-right">
                        <li><a href="<?php echo base_url('login'); ?>">Log In</a></li>
                        <li><a href="https://diskominfo.wonosobokab.go.id/postings/galeri/1892/FAQ.HTML">FAQ</a></li>
                            <!-- Modified by Novikov.ua -->
							<li><a href="#" onclick="doGTranslate('id|en');return false;" title="English" class="gflag nturl"
                            style="background-position:-0px -0px;"><img src="//gtranslate.net/flags/16.png" height="16" width="16" alt="English"/></a>
                            <a href="#" onclick="doGTranslate('en|id');return false;" title="Indonesia" class="gflag nturl"
                            style="background-position:-300px -300px;"><img src="//gtranslate.net/flags/16.png" height="16" width="16" alt="Indonesia"/></a>
							</li>


                            <div id="google_translate_element2"></div>

                            <script type="text/javascript">
                                function googleTranslateElementInit2() {
                                    new google.translate.TranslateElement({
                                        pageLanguage: 'id',
                                        autoDisplay: false
                                    }, 'google_translate_element2');
                                }
                            </script>
                            <script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>
                            <script type="text/javascript">
                                /* <![CDATA[ */
                                eval(function (p, a, c, k, e, r) {
                                    e = function (c) {
                                        return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36))
                                    };
                                    if (!''.replace(/^/, String)) {
                                        while (c--) r[e(c)] = k[c] || e(c);
                                        k = [function (e) {
                                            return r[e]
                                        }];
                                        e = function () {
                                            return '\\w+'
                                        };
                                        c = 1
                                    }
                                    while (c--) if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]);
                                    return p
                                }('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}', 43, 43, '||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'), 0, {}))
                                /* ]]> */
                            </script>
                    </ul>
                </div>
                <!-- END TOP BAR MENU -->
            </div>
        </div>        
    </div>
    <!-- END TOP BAR -->
    <!-- BEGIN HEADER -->
    <div class="header">
      <div class="container">
        <a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

        <!-- BEGIN NAVIGATION  class="header-navigation pull-right font-transform-inherit"-->
        <div class="header-navigation pull-right font-transform-inherit"  id="hornav">
							<?php echo ''.$menu_atas.''; ?>
        </div>
        <!-- END NAVIGATION -->
      </div>
    </div>
    <!-- Header END -->


    <div class="main">
      <div class="container">
				<div class="main-content mag-content clearfix" style="margin-top:-20px;">
					<!-- BEGIN SLIDER -->
					<div class="page-slider">
						<!-- Carousel Slideshow bottom-border -->
						<center>
						<div id="carousel-example-generic" class="carousel slide carousel-slider" data-ride="carousel">
								<!-- Carousel Indicators -->
								<ol class="carousel-indicators carousel-indicators-frontend">
									<li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
									<li data-target="#carousel-example-generic" data-slide-to="1"></li>
									<li data-target="#carousel-example-generic" data-slide-to="2"></li>
								</ol>
								<div class="clearfix"></div>
								<!-- End Carousel Indicators -->
								<!-- Carousel Images -->
								<div class="carousel-inner" role="listbox" style="height: 350px;">
										<?php
										$where1 = array(
											'status' => 1,
											'domain' => $web
											);
										$this->db->where($where1);
										$this->db->order_by('created_time desc');
										$query1 = $this->db->get('slide_website');
										$a = 0;
										foreach ($query1->result() as $row1)
											{
												$a = $a+1;
												if( $a == 1 ){
													/* echo
													'
													<div class="item active">
															<img src="'.base_url().'media/upload/'.$row1->file_name.'">
															<div class="container">
																	<div class="carousel-position-six text-uppercase text-center">
																			<p class="margin-bottom-20 animate-delay carousel-title-v5" data-animation="animated fadeInDown">
																					'.$row1->keterangan.'
																			</p>
																	</div>
															</div>
													</div>
													'; */
													echo
													'
													<div class="item active">
															<img src="'.base_url().'media/upload/'.$row1->file_name.'">
															<div class="container">
																	<div class="carousel-position-six text-uppercase text-center">
																	</div>
															</div>
													</div>
													';
													}
												else{
													echo
													'
													<div class="item">
															<img src="'.base_url().'media/upload/'.$row1->file_name.'">
															<div class="container">
																	<div class="carousel-position-six text-uppercase text-center">
																	</div>
															</div>
													</div>
													';
													} 
											}
										?>
								</div>
							<!-- Controls -->
							<a class="left carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="prev">
									<i class="fa fa-angle-left" aria-hidden="true"></i>
							</a>
							<a class="right carousel-control carousel-control-shop carousel-control-frontend" href="#carousel-example-generic" role="button" data-slide="next">
									<i class="fa fa-angle-right" aria-hidden="true"></i>
							</a>
						</div>
						</center>
					</div>
					<!-- END SLIDER -->
					<?php 
					if ($web=='disdagkopukm.wonosobokab.go.id'){
						$this -> load -> view('welcome/welcome_disdag.php');
					}
					/* elseif ($web=='disparbud.wonosobokab.go.id'){
						$this -> load -> view('welcome/welcome_disparbud.php');
					} */
					else{
						echo'   
					<div class="row service-box margin-bottom-40">
						<div class="col-md-6 col-sm-6">
							'; if(!empty( $KolomKiriAtas )){ echo $KolomKiriAtas; } echo'
						</div>
						<div class="col-md-6 col-sm-6">
							'; if(!empty( $KolomKananAtas )){ echo $KolomKananAtas; } echo'
						</div>
					</div>
					<div class="col-md-12">
						<div class="row quote-vo margin-bottom-30">
							<div class="col-md-12">
								<span>'; if(!empty( $Header )){ echo $Header; } echo'</span>
							</div>
						</div>
					</div>
						';
					}
					// echo $web; 
					?>

					<!-- BEGIN RECENT WORKS -->
					<div class="row recent-work margin-bottom-40">
						<div class="col-md-3">
							<?php if(!empty( $KolomKananBawah )){ echo $KolomKananBawah; } ?>
						</div>
						<div class="col-md-9 tab-style-1">
							<ul class="nav nav-tabs">
								<li class="active"><a href="#tab-1" data-toggle="tab">Informasi Terbaru</a></li>
							</ul>
							<div class="tab-content">
              <div class="tab-pane row fade in active" id="tab-1">
									<?php if(!empty($galery_berita)){ echo $galery_berita; } ?>
              </div>
            </div>
						</div>
					</div>   
					<!-- END RECENT WORKS -->

				</div>
      </div>
			
			
    </div>

    <!-- BEGIN FOOTER -->
    <div class="footer source-org vcard copyright clearfix" style="margin-top:2px;">
			<div class="footer-main">
				<div class="fixed-main">
					<!-- BEGIN PRE-FOOTER -->
						<div class="container">
							<div class="row">
								<!-- BEGIN BOTTOM ABOUT BLOCK -->
								<div class="col-md-4 col-sm-6 pre-footer-col">
									<?php if(!empty( $KolomKiriBawah )){ echo $KolomKiriBawah; } ?>
									<div>
										<i class="fa fa-bar-chart-o"></i> <span id="hit_counter"></span>
									</div>
									<div>
										<i class="fa fa-bar-chart-o"></i> <span id="visitor"></span>
									</div>
								</div>
								<!-- END BOTTOM ABOUT BLOCK -->

								<!-- BEGIN BOTTOM CONTACTS -->
								<div class="col-md-4 col-sm-6 pre-footer-col">
									<h2>Kontak Kami</h2>
									<address class="margin-bottom-40">
										<?php if(!empty( $alamat )){ echo $alamat; } ?><br>
										Phone: <?php if(!empty( $telpon )){ echo $telpon; } ?><br>
										Email: <a href="mailto:<?php if(!empty( $email )){ echo $email; } ?>"><?php if(!empty( $email )){ echo $email; } ?></a><br>
										Website: <a href="https://<?php echo $web; ?>">https://<?php echo $web; ?></a>
									</address>

								</div>
								<!-- END BOTTOM CONTACTS -->

								<!-- BEGIN TWITTER BLOCK --> 
								<div class="col-md-4 col-sm-6 pre-footer-col">
									<?php if(!empty( $KolomPalingBawah )){ echo $KolomPalingBawah; } ?>
								</div>
								<!-- END TWITTER BLOCK -->
							</div>
						</div>
					<div class="footer-bottom clearfix">
							<div class="fixed-main">
								<div class="container">
								<div class="mag-content">
									<div class="row">
										<!-- BEGIN COPYRIGHT -->
										<div class="col-md-4 col-sm-4 padding-top-10">
											<?php echo date('Y'); ?> © <?php echo $web; ?>. 
										</div>
										<!-- END COPYRIGHT -->
										<!-- BEGIN PAYMENTS -->
										<div class="col-md-4 col-sm-4">
											<ul class="social-footer list-unstyled list-inline pull-right">
												<li><a target="_blank" href="<?php if(!empty( $facebook )){ echo $facebook; } ?>"><i class="fa fa-facebook"></i></a></li>
												<li><a target="_blank" href="<?php if(!empty( $google )){ echo $google; } ?>"><i class="fa fa-google-plus"></i></a></li>
												<li><a target="_blank" href="<?php if(!empty( $twitter )){ echo $twitter; } ?>"><i class="fa fa-twitter"></i></a></li>
												<li><a target="_blank" href="<?php if(!empty( $instagram )){ echo $instagram; } ?>"><i class="fa fa-instagram"></i></a></li>
												<li><a target="_blank" href="javascript:;"><i class="fa fa-youtube"></i></a></li>
											</ul>  
										</div>
										<!-- END PAYMENTS -->
										<!-- BEGIN POWERED -->
										<div class="col-md-4 col-sm-4 text-right">
											<p class="powered">Powered by: <a href="https://<?php echo $web; ?>"><?php echo $web; ?></a></p>
										</div>
										<!-- END POWERED -->
									</div>
								</div>
							</div>
							</div>
					</div>
        </div>
      </div>
    </div>
    <!-- END FOOTER -->
    <!-- END PRE-FOOTER -->


    <!-- Load javascripts at bottom, this will reduce page load time -->
    <!-- BEGIN CORE PLUGINS (REQUIRED FOR ALL PAGES) -->
    <!--[if lt IE 9]>
    <script src="assets/plugins/respond.min.js"></script>
    <![endif]-->
    <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/jquery-migrate.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>      
    <script src="<?php echo base_url(); ?>Template/theme/assets/corporate/scripts/back-to-top.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->
    <!-- BEGIN PAGE LEVEL JAVASCRIPTS (REQUIRED ONLY FOR CURRENT PAGE) -->
    <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
    <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/owl.carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->

    <script src="<?php echo base_url(); ?>Template/theme/assets/corporate/scripts/layout.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>Template/theme/assets/pages/scripts/bs-carousel.js" type="text/javascript"></script>
    <script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();    
            Layout.initOWL();
            Layout.initTwitter();
            Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
            Layout.initNavScrolling();
        });
    </script>
    <!-- END PAGE LEVEL JAVASCRIPTS -->
            <script>
              function LoadVisitor() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    table:'visitor'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/simpan_visitor/',
                  success: function(html) {
                    $('#visitor').html('Total Pengunjung : '+html+' ');
                  }
                });
              }
            </script>
            <script>
              function LoadHitCounter() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    current_url:'<?php echo base_url(); ?>index.php'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/hit_counter/',
                  success: function(html) {
                    $('#hit_counter').html('Total Pembaca : '+html+' ');
                  }
                });
              }
            </script>
            <script type="text/javascript">
            $(document).ready(function() {
              LoadVisitor();
              LoadHitCounter();
            });
            </script>
            <div id="fb-root"></div>
            <script>
              (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.12&appId=351370971628122&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));
            </script>

		<!-- uut -->
		<script src="<?php echo base_url(); ?>js/uut.js"></script>
		<!-- alertify -->
		<script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
		<!-- datepicker -->
		<script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
<script src="<?php echo base_url(); ?>assets/portfolio/js/modernizr.min.js"></script>

</body>
<!-- END BODY -->
</html>