
          <div class="row" id="awal">
            <div class="col-md-12" id="div_form_input">
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Formulir Peraturan</h3>
                </div>
                <div class="box-body">
                  <form role="form" id="form_peraturan" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=peraturan" enctype="multipart/form-data">
                    <div class="box-body">
                    
                      <input id="id_peraturan" name="id_peraturan" type="hidden">
                      <input class="form-control" id="kode_peraturan" name="kode_peraturan" value="-" placeholder="Kode Peraturan" type="hidden">
                      <div class="form-group">
                        <label for="opsi_parent">Parent</label>
                        <select class="form-control" id="opsi_parent">
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="nama_peraturan">Nama Peraturan</label>
                        <input class="form-control" id="nama_peraturan" name="nama_peraturan" value="" placeholder="Nama Peraturan" type="text">
                      </div>
                      <input id="inserted_by" name="inserted_by" type="hidden">
                      <input id="inserted_time" name="inserted_time" type="hidden">
                      <input id="updated_by" name="updated_by" type="hidden">
                      <input id="updated_time" name="updated_time" type="hidden">
                      <input id="deleted_by" name="deleted_by" type="hidden">
                      <input id="deleted_time" name="deleted_time" type="hidden">
                      <input id="temp" name="temp" type="hidden">
                      <div class="form-group">
                        <label for="keterangan">Keterangan</label>
                        <textarea id="keterangan"></textarea>
                      <!--
                      <input class="form-control" id="keterangan" name="keterangan" value="" placeholder="Keterangan" type="text">
                      -->
                      </div>
                      <input id="status" name="status" type="hidden">
                      <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-paperclip"></i> Lampiran Pdf!</h4>
                        <div class="form-group">
                        <select class="form-control" id="remake" name="remake">
                          <option value="-">Pilih Lampiran</option>
                          <option value="Katalog">Katalog</option>
                          <option value="Abstrak">Abstrak</option>
                          <option value="Isi Peraturan">Isi Peraturan</option>
                        </select>
                        </div>
                        <div class="form-group">
                          <div class="btn btn-default btn-file">
                            <i class="fa fa-paperclip"></i> File Lampiran
                            <input type="file" name="myfile" id="peraturan_baru">
                          </div>
                        </div>
                        <div id="progress_upload_lampiran_peraturan">
                          <div id="bar_progress_upload_lampiran_peraturan"></div>
                          <div id="percent_progress_upload_lampiran_peraturan">0%</div >
                        </div>
                        <div id="message_progress_upload_lampiran_peraturan"></div>
                        <h3 class="box-title">Data Lampiran </h3>
                        <table class="table table-bordered">
                          <tr>
                            <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                          </tr>
                          <tbody id="tbl_lampiran_peraturan">
                          </tbody>
                        </table>
                      </div>
                    </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" id="simpan_peraturan">SIMPAN</button>
                    </div>
                  </form>
                </div>
                <div class="overlay" id="overlay_form_input" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row" id="e_div_form_input" style="display:none;">
            <div class="col-md-12">
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Formulir Edit Peraturan</h3>
                </div>
                <div class="box-body">
                  <form role="form" id="e_form_peraturan" method="post" action="<?php echo base_url(); ?>attachment/e_upload/?table_name=peraturan" enctype="multipart/form-data">
                    <div class="box-body">
                      
                      <input class="form-control" id="e_id_peraturan" name="id" value="" type="hidden">
                      <input class="form-control" id="e_kode_peraturan" name="e_kode_peraturan" value="-" placeholder="Kode Peraturan" type="hidden">
                      <div class="form-group">
                        <label for="e_opsi_parent">Parent</label>
                        <select class="w3-input" id="e_opsi_parent">
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="e_nama_peraturan">Nama Peraturan</label>
                        <input class="w3-input" id="e_nama_peraturan" name="e_nama_peraturan" value="" placeholder="Nama Peraturan" type="text">
                      </div>
                      <input class="form-control" id="e_inserted_by" name="e_inserted_by" value="" type="hidden">
                      <input class="form-control" id="e_inserted_time" name="e_inserted_time" value="" type="hidden">
                      <input class="form-control" id="e_updated_by" name="e_updated_by" value="" type="hidden">
                      <input class="form-control" id="e_updated_time" name="e_updated_time" value="" type="hidden">
                      <input class="form-control" id="e_deleted_by" name="e_deleted_by" value="" type="hidden">
                      <input class="form-control" id="e_deleted_time" name="e_deleted_time" value="" type="hidden">
                      <input class="form-control" id="e_temp" name="e_temp" value="" type="hidden">
                      <div class="form-group">
                        <label for="e_keterangan">Keterangan</label>
                        <textarea id="e_keterangan"></textarea>
                        <!--
                        <input class="form-control" id="e_keterangan" name="e_keterangan" value="" placeholder="Keterangan" type="text">
                        -->
                      </div>
                      <input class="form-control" id="e_status" name="e_status" value="" type="hidden">
                      
                      <div class="alert alert-info alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-paperclip"></i> Lampiran Pdf!</h4>
                        <div class="form-group">
                        <select class="form-control" id="e_remake" name="e_remake">
                          <option value="-">Pilih Lampiran</option>
                          <option value="Katalog">Katalog</option>
                          <option value="Abstrak">Abstrak</option>
                          <option value="Isi Peraturan">Isi Peraturan</option>
                        </select>
                        </div>
                            <div class="form-group">
                                <div class="btn btn-default btn-file">
                                  <i class="fa fa-paperclip"></i> File Lampiran
                                  <input type="file" name="myfile" id="e_peraturan_baru">
                                </div>
                            </div>
                            <div id="e_progress_upload_lampiran_peraturan">
                              <div id="e_bar_progress_upload_lampiran_peraturan"></div>
                              <div id="e_percent_progress_upload_lampiran_peraturan">0%</div >
                            </div>
                            <div id="e_message_progress_upload_lampiran_peraturan"></div>
                            <h3 class="box-title">Data Lampiran </h3>
                            <table class="table table-bordered">
                              <tr>
                                <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                              </tr>
                              <tbody id="e_tbl_lampiran_peraturan">
                              </tbody>
                            </table>
                      </div>
                      
                    </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" id="update_data_peraturan">SIMPAN</button>
                    </div>
                  </form>
                </div>
                <div class="overlay" id="e_overlay_form_input" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row" id="div_data_default">
            <div class="col-md-12">
              <div class="box box-info box-solid">
                <div class="box-header">
                  <h3 class="box-title">DATA</h3>
                </div>
                <div class="box-body">
                  <table class="table table-bordered">
                    <tr>
                      <th>NO</th>
                      <!--<th>Kode Peraturan</th>-->
                      <th>Nama Peraturan</th>
                      <!--<th>Keterangan</th>-->
                      <th>PROSES</th> 
                    </tr>
                    <tbody id="tbl_utama_peraturan">
                    </tbody>
                  </table>
                  <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                    <?php
                    for ($x = 1; $x <= $total; $x++) {
                      echo '<li page="'.$x.'" id="'.$x.'"><a class="update_id" href="#">'.$x.'</a></li>';
                    }
                    ?>
                    </ul>
                  </div>
                </div>
                <div class="overlay" id="overlay_data_default" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
              </div>
            </div>
          </div>
          
<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_input').on('click', function(e) {
	$('#e_div_form_input').fadeOut('slow');
	$('#div_form_input').show();
	});
	var aaa = Math.random();
  $('#temp').val(aaa);
});
</script>

<script>
  function load_default(halaman) {
    $('#overlay_data_default').show();
    $('#tbl_utama_peraturan').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>peraturan/json_all_peraturan/?halaman='+halaman+'/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * <?php echo $per_page; ?>);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_peraturan="' + json[i].id_peraturan + '" id="' + json[i].id_peraturan + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
					// tr += '<td valign="top">' + json[i].kode_peraturan + '</td>';
					tr += '<td valign="top">' + json[i].nama_peraturan + '</td>';
					// tr += '<td valign="top">' + json[i].keterangan + '</td>';
					// tr += '<td valign="top"><a href="<?php echo base_url(); ?>admin_peraturan/?id_peraturan=' + json[i].id_peraturan + '">Tampilkan</a></td>';
					tr += '<td valign="top">';
					tr += '<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> ';
					tr += '<a href="#tab_1" data-toggle="tab" id="del_ajax" ><i class="fa fa-cut"></i></a>';
					tr += '</td>';
          tr += '</tr>';
        }
        $('#tbl_utama_peraturan').append(tr);
				$('#overlay_data_default').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#pagination').on('click', '.update_id', function(e) {
		e.preventDefault();
		var id = $(this).closest('li').attr('page');
		var halaman = id;
		load_default(halaman);
	});
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	var halaman = 1;
	load_default(halaman);
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#tbl_utama_peraturan, #tbl_search_peraturan').on('click', '.update_id', function(e) {
		e.preventDefault();
		$('#e_overlay_form_input').show();
		var id = $(this).closest('tr').attr('id_peraturan');
		$.ajax({
			dataType: 'json',
			url: '<?php echo base_url(); ?>peraturan/peraturan_get_by_id/?id='+id+'',
			success: function(json) {
				if (json.length == 0) {
					alert('Tidak ada data');
				} else {
					for (var i = 0; i < json.length; i++) {
					$('#e_id_peraturan').val(json[i].id_peraturan);
					$('#e_id_parent').val(json[i].id_parent);
					$('#e_kode_peraturan').val(json[i].kode_peraturan);
					$('#e_nama_peraturan').val(json[i].nama_peraturan);
					$('#e_inserted_by').val(json[i].inserted_by);
					$('#e_inserted_time').val(json[i].inserted_time);
					$('#e_updated_by').val(json[i].updated_by);
					$('#e_updated_time').val(json[i].updated_time);
					$('#e_deleted_by').val(json[i].deleted_by);
					$('#e_deleted_time').val(json[i].deleted_time);
					$('#e_temp').val(json[i].temp);
					// $('#e_keterangan').val(json[i].keterangan);
          CKEDITOR.instances.e_keterangan.setData(json[i].keterangan);
					$('#e_status').val(json[i].status);
										}
					e_load_lampiran_peraturan(id);
					$('#div_form_input').fadeOut('slow');
					$('#e_div_form_input').show();
					$('#e_overlay_form_input').fadeOut(2000);
					$('html, body').animate({
						scrollTop: $('#awal').offset().top
					}, 1000);
				}
			}
		});
	});
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_peraturan').on('click', function(e) {
      e.preventDefault();
      $('#simpan_peraturan').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
			
			var id_peraturan = $("#id_peraturan").val();
			var id_parent = $("#id_parent").val();
			var kode_peraturan = $("#kode_peraturan").val();
			var nama_peraturan = $("#nama_peraturan").val();
			var inserted_by = $("#inserted_by").val();
			var inserted_time = $("#inserted_time").val();
			var updated_by = $("#updated_by").val();
			var updated_time = $("#updated_time").val();
			var deleted_by = $("#deleted_by").val();
			var deleted_time = $("#deleted_time").val();
			var temp = $("#temp").val();
			// var keterangan = $("#keterangan").val();
      var keterangan = CKEDITOR.instances.keterangan.getData();
			var status = $("#status").val();
						
			if (kode_peraturan == '') {
					$('#kode_peraturan').css('background-color', '#DFB5B4');
				} else {
					$('#kode_peraturan').removeAttr('style');
				}
			if (nama_peraturan == '') {
					$('#nama_peraturan').css('background-color', '#DFB5B4');
				} else {
					$('#nama_peraturan').removeAttr('style');
				}
			if (inserted_by == '') {
					$('#inserted_by').css('background-color', '#DFB5B4');
				} else {
					$('#inserted_by').removeAttr('style');
				}
			if (temp == '') {
					$('#temp').css('background-color', '#DFB5B4');
				} else {
					$('#temp').removeAttr('style');
				}
						
			$.ajax({
        type: 'POST',
        async: true,
        data: {
					id_parent:id_parent,
					kode_peraturan:kode_peraturan,
					nama_peraturan:nama_peraturan,
					temp:temp,
					keterangan:keterangan,
					status:status,
										},
        dataType: 'text',
        url: '<?php echo base_url(); ?>peraturan/simpan_peraturan/',
        success: function(json) {
          if (json == '0') {
            $('#simpan_peraturan').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut();
            alert('gagal');
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
          } else {
						alertify.alert('Data berhasil disimpan');
						$('#tbl_lampiran_peraturan').html('');
						$('#message_progress_upload_lampiran_peraturan').html('');
            var halaman = 1;
            load_default(halaman);
            CKEDITOR.instances.keterangan.setData('');
            var aaa = Math.random();
            $('#temp').val(aaa);
            $('#div_form_input form').trigger('reset');
            $('#simpan_peraturan').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut('slow');
          }
        }
      });
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#update_data_peraturan').on('click', function(e) {
		e.preventDefault();
		$('#update_data_peraturan').attr('disabled', 'disabled');
		$('#e_overlay_form_input').show();
		var id_peraturan = $('#e_id_peraturan').val();
		var id_parent = $('#e_id_parent').val();
		var kode_peraturan = $('#e_kode_peraturan').val();
		var nama_peraturan = $('#e_nama_peraturan').val();
		var temp = $('#e_temp').val();
		// var keterangan = $('#e_keterangan').val();
		var keterangan = CKEDITOR.instances.e_keterangan.getData();
						
		if (id_peraturan == '') {
				$('#e_id_peraturan').css('background-color', '#DFB5B4');
			} else {
				$('#e_id_peraturan').removeAttr('style');
			}
		if (kode_peraturan == '') {
				$('#e_kode_peraturan').css('background-color', '#DFB5B4');
			} else {
				$('#e_kode_peraturan').removeAttr('style');
			}
		if (nama_peraturan == '') {
				$('#e_nama_peraturan').css('background-color', '#DFB5B4');
			} else {
				$('#e_nama_peraturan').removeAttr('style');
			}
		if (temp == '') {
				$('#e_temp').css('background-color', '#DFB5B4');
			} else {
				$('#e_temp').removeAttr('style');
			}
		if (keterangan == '') {
				$('#e_keterangan').css('background-color', '#DFB5B4');
			} else {
				$('#e_keterangan').removeAttr('style');
			}
				
			$.ajax({
			type: 'POST',
			async: true,
			data: {
					
					id_peraturan:id_peraturan,
					id_parent:id_parent,
					kode_peraturan:kode_peraturan,
					nama_peraturan:nama_peraturan,
					temp:temp,
					keterangan:keterangan,
										
				},
			dataType: 'json',
			url: '<?php echo base_url(); ?>peraturan/update_data_peraturan/',
			success: function(json) {
				if (json.length == 0) {            
					alert('edit gagal');
					$('#update_data_peraturan').removeAttr('disabled', 'disabled');
					$('#e_overlay_form_input').fadeOut('slow');
					} 
				else {
					alertify.alert('Edit Data Berhasil');
					var halaman = 1;
					load_default(halaman);
					$('#e_div_form_input form').trigger('reset');
					$('#update_data_peraturan').removeAttr('disabled', 'disabled');
					$('#e_overlay_form_input').fadeOut('slow');
					$('#e_div_form_input').fadeOut('slow');
					$('#tbl_lampiran_peraturan').html('');
					$('#e_tbl_lampiran_peraturan').html('');
					$('#a_tbl_lampiran_peraturan').html('');
					}
			}
		});
	});
});
</script>

<script>
function reset() {
	$('#toggleCSS').attr('href', '<?php echo base_url(); ?>css/alertify/alertify.default.css');
	alertify.set({
		labels: {
			ok: 'OK',
			cancel: 'Cancel'
		},
		delay: 5000,
		buttonReverse: false,
		buttonFocus: 'ok'
	});
}
//===============HAPUS OBAT
$('#tbl_search_peraturan, #tbl_utama_peraturan').on('click', '#del_ajax', function() {
	reset();
	var id = $(this).closest('tr').attr('id_peraturan');
	alertify.confirm('Anda yakin data akan dihapus?', function(e) {
		if (e) {
			$.ajax({
				dataType: 'json',
				url: '<?php echo base_url(); ?>peraturan/hapus_peraturan/?id='+id+'',
				success: function(response) {
					if (response.errors == 'Yes') {
						alertify.alert('Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya');
					} else {
						$('[id_peraturan='+id+']').remove();
						alertify.alert('Data berhasil dihapus');
					}
				}
			});
		} else {
			alertify.alert('Hapus data dibatalkan');
		}
	});
});
//===============HAPUS Attachment
$('#tbl_lampiran_peraturan, #e_tbl_lampiran_peraturan, #a_tbl_lampiran_peraturan').on('click', '#del_ajax', function() {
	reset();
	var id = $(this).closest('tr').attr('id_attachment');
	alertify.confirm('Anda yakin data akan dihapus?', function(e) {
		if (e) {
			$.ajax({
				dataType: 'json',
				url: '<?php echo base_url(); ?>attachment/hapus/?id='+id+'',
				success: function(response) {
					if (response.errors == 'Yes') {
						alertify.alert('Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya');
					} else {
						$('[id_attachment='+id+']').remove();
						$('#message_progress_upload_lampiran_peraturan').remove();
						$('#e_message_progress_upload_lampiran_peraturan').remove();
						$('#a_message_progress_upload_lampiran_peraturan').remove();
						$('#percent_progress_upload_lampiran_peraturan').html('');
						$('#e_percent_progress_upload_lampiran_peraturan').html('');
						$('#a_percent_progress_upload_lampiran_peraturan').html('');
						alertify.alert('Data berhasil dihapus');
					}
				}
			});
		} else {
			alertify.alert('Hapus data dibatalkan');
		}
	});
});

</script>

<script>
	function load_lampiran_peraturan() {
		$('#tbl_lampiran_peraturan').html('');
		var temp = $('#temp').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				temp:temp
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/?table=peraturan',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_lampiran_peraturan').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
			var options = { 
				beforeSend: function() {
					$('#progress_upload_lampiran_peraturan').show();
					$('#bar_progress_upload_lampiran_peraturan').width('0%');
					$('#message_progress_upload_lampiran_peraturan').html('');
					$('#percent_progress_upload_lampiran_peraturan').html('0%');
					},
				uploadProgress: function(event, position, total, percentComplete){
					$('#bar_progress_upload_lampiran_peraturan').width(percentComplete+'%');
					$('#percent_progress_upload_lampiran_peraturan').html(percentComplete+'%');
					},
				success: function(){
					$('#bar_progress_upload_lampiran_peraturan').width('100%');
					$('#percent_progress_upload_lampiran_peraturan').html('100%');
					},
				complete: function(response){
					$('#message_progress_upload_lampiran_peraturan').html('<font color="green">'+response.responseText+'</font>');
					var temp = $('#temp').val();
					load_lampiran_peraturan();
					},
				error: function(){
					$('#message_progress_upload_lampiran_peraturan').html('<font color="red"> ERROR: unable to upload files</font>');
					}     
			};
			document.getElementById('peraturan_baru').onchange = function() {
					$('#form_peraturan').submit();
				};
			$('#form_peraturan').ajaxForm(options);
		});
</script>

<script>
	function e_load_lampiran_peraturan() {
		$('#e_tbl_lampiran_peraturan').html('');
		var id = $('#e_id_peraturan').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				id:id
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/e_load_lampiran/?table=peraturan',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#e_tbl_lampiran_peraturan').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
			var options = { 
				beforeSend: function() {
					$('#e_progress_upload_lampiran_peraturan').show();
					$('#e_bar_progress_upload_lampiran_peraturan').width('0%');
					$('#e_message_progress_upload_lampiran_peraturan').html('');
					$('#e_percent_progress_upload_lampiran_peraturan').html('0%');
					},
				uploadProgress: function(event, position, total, percentComplete){
					$('#e_bar_progress_upload_lampiran_peraturan').width(percentComplete+'%');
					$('#e_percent_progress_upload_lampiran_peraturan').html(percentComplete+'%');
					},
				success: function(){
					$('#e_bar_progress_upload_lampiran_peraturan').width('100%');
					$('#e_percent_progress_upload_lampiran_peraturan').html('100%');
					},
				complete: function(response){
					$('#e_message_progress_upload_lampiran_peraturan').html('<font color="green">'+response.responseText+'</font>');
					e_load_lampiran_peraturan();
					},
				error: function(){
					$('#e_message_progress_upload_lampiran_peraturan').html('<font color="red"> ERROR: unable to upload files</font>');
					}     
			};
			document.getElementById('e_peraturan_baru').onchange = function() {
					$('#e_form_peraturan').submit();
				};
			$('#e_form_peraturan').ajaxForm(options);
		});
</script>

<script type="text/javascript">
$(document).ready(function() {
			$.ajax({
				dataType: "json",
				url: '<?php echo base_url(); ?>peraturan/opsi_parent',
				success: function(json) {
					var opsi = '';
					opsi += '';
							opsi += '<option value="-" > Pilih Peraturan</option>';
					for (var i = 0; i < json.length; i++) {
							opsi += '<option value="' + json[i].id_peraturan + '" > ' + json[i].nama_peraturan + '  </option>';
						}
					opsi += '';
					$('#opsi_parent').append(opsi);  
				}
			});
			$.ajax({
				dataType: "json",
				url: '<?php echo base_url(); ?>peraturan/opsi_parent',
				success: function(json) {
					var opsi = '';
					opsi += '';
							opsi += '<option value="-" > Pilih Peraturan</option>';
					for (var i = 0; i < json.length; i++) {
							opsi += '<option value="' + json[i].id_peraturan + '" > ' + json[i].nama_peraturan + '  </option>';
						}
					opsi += '';
					$('#e_opsi_parent').append(opsi);  
				}
			});
});
</script>

<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('keterangan');
	CKEDITOR.replace('e_keterangan');
	//bootstrap WYSIHTML5 - text editor
	$(".textarea").wysihtml5();
});
</script>