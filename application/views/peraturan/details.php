
      <?php foreach ($details_peraturan->result() as $r_details_peraturan):?>
        

            <div class="col-md-12">
              <p><b>Judul</b> <br /><?php echo $r_details_peraturan->nama_peraturan;?></p>
              <div class="row">
                <div class="col-md-6">
                  <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                      <tbody>
                        <tr>
                          <td><b>Tipe Dokumen</b></td>
                          <td><b>Tahun Terbit</b></td>
                        </tr>
                        <tr>
                          <td>
                            <?php if($r_details_peraturan->id_parent=='7'){echo'Peraturan Daerah';}
                            elseif($r_details_peraturan->id_parent=='8'){echo'Peraturan Bupati';}
                            elseif($r_details_peraturan->id_parent=='592'){echo'Peraturan Desa';}
                            elseif($r_details_peraturan->id_parent=='6'){echo'Naskah Akademik';}
                            elseif($r_details_peraturan->id_parent=='321'){echo'Keputusan DPRD';}
                            else{}
                            ?>
                          </td>
                          <td>
                            <?php echo $r_details_peraturan->tahun_peraturan;?>
                          </td>
                        </tr>
                        <tr>
                          <td><b>Nomor Peraturan</b></td>
                          <td><b>Bidang Hukum</b></td>
                        </tr>
                        <tr>
                          <td>
                            <?php echo $r_details_peraturan->nomor_peraturan;?>
                          </td>
                          <td>
                            -
                          </td>
                        </tr>
                        <tr>
                          <td><b>Tempat Penetapan</b></td>
                          <td><b>Tanggal Penetapan / Pengundangan</b></td>
                        </tr>
                        <tr>
                          <td>
                            Wonosobo
                          </td>
                          <td>
                            -
                          </td>
                        </tr>
                        <tr>
                          <td><b>Singkatan Jenis / Bentuk Peraturan</b></td>
                          <td><b>Sumber</b></td>
                        </tr>
                        <tr>
                          <td>
                            <?php if($r_details_peraturan->id_parent=='7'){echo'Peraturan Daerah';}
                            elseif($r_details_peraturan->id_parent=='8'){echo'Peraturan Bupati';}
                            elseif($r_details_peraturan->id_parent=='592'){echo'Peraturan Desa';}
                            elseif($r_details_peraturan->id_parent=='6'){echo'Naskah Akademik';}
                            elseif($r_details_peraturan->id_parent=='321'){echo'Keputusan DPRD';}
                            else{}
                            ?>
                          </td>
                          <td>
                            -
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2"><b>Bahasa</b></td>
                        </tr>
                        <tr>
                          <td colspan="2">
                            Indonesia
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2">
                            T.E.U Orang / Pengarang
                            <table class="table table-bordered table-striped">
                              <tr>
                                <td>Nama Pengarang</td>
                                <td>Tipe Pengarang</td>
                                <td>Jenis Pengarang</td>
                              </tr>
                              <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2">
                            <b>Subjek</b>
                            <table class="table table-bordered table-striped">
                              <tr>
                                <td>Kata Kunci</td>
                                <td>Tipe Kata Kunci</td>
                                <td>Jenis Kata Kunci</td>
                              </tr>
                              <tr>
                                <td colspan="3">Data tidak tersedia </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2"><b>Log</b></td>
                        </tr>
                        <tr>
                          <td colspan="2">
                            <?php echo $r_details_peraturan->keterangan; ?>
                          </td>
                        </tr>
                        <tr>
                          <td colspan="2">
                            <?php if(!$r_details_peraturan->attachment_isi_peraturan){echo'';}else{echo 'Download <a href="'.base_url().'media/upload/'.$r_details_peraturan->attachment_isi_peraturan.'" style="color:#2556b9;"><i class="fa fa-download"></i> Dokumen Peraturan </a>';} ?>
                            <?php if(!$r_details_peraturan->attachment_katalog){echo'';}else{echo '<br />Download <a href="'.base_url().'media/upload/'.$r_details_peraturan->attachment_katalog.'" style="color:#2556b9;"><i class="fa fa-download"></i> Lampiran Katalog </a>';} ?>
                            <?php if(!$r_details_peraturan->attachment_abstrak){echo'';}else{echo '<br />Download <a href="'.base_url().'media/upload/'.$r_details_peraturan->attachment_abstrak.'" style="color:#2556b9;"><i class="fa fa-download"></i> Lampiran Abstrak </a>';} ?>
                            <?php if(!$r_details_peraturan->attachment_na){echo'';}else{echo '<br />Download <a href="'.base_url().'media/upload/'.$r_details_peraturan->attachment_na.'" style="color:#2556b9;"><i class="fa fa-download"></i> Lampiran Naskah Dinas </a>';} ?>
                            <?php if(!$r_details_peraturan->attachment_mou){echo'';}else{echo '<br />Download <a href="'.base_url().'media/upload/'.$r_details_peraturan->attachment_mou.'" style="color:#2556b9;"><i class="fa fa-download"></i> Lampiran MOU </a>';} ?>
                            <?php 
                            $sudah_forwardp = $this->db->query("
                              select attachment.file_name from attachment where attachment.id_tabel=".$r_details_peraturan->id_peraturan." and attachment.table_name='peraturan' and attachment.keterangan='Lampiran'
                            ");
                            $no_lampiran=0;
                            foreach ($sudah_forwardp->result() as $row1)
                              {
                                $no_lampiran=$no_lampiran+1;
                                echo '<br />Download <a href="'.base_url().'media/upload/'.$row1->file_name.'" style="color:#2556b9;"><i class="fa fa-download"></i> Lampiran '.$no_lampiran.'</a>';
                              }
                            ?>
                            
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div class="col-md-6">
                  <p><b>Views Dokumen Peraturan</b></p>
                  <blockquote class="no-padding no-margin">
                    <center>
                      <embed src="<?php echo ''.base_url().'media/upload/'.$r_details_peraturan->attachment_isi_peraturan.''; ?>" type="application/pdf" width="550" height="800">
                      <!--<a class="media" href="<?php echo ''.base_url().'media/upload/'.$r_details_peraturan->attachment_isi_peraturan.''; ?>"></a>
               
                      <script type="text/javascript">
                          $(function () {
                              $('.media').media({width: 450, height: 600});
                          });
                      </script>-->
                    </center>
                  </blockquote>
                </div>
              </div>
            </div>
            
      <?php endforeach; ?>
      