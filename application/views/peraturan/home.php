
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Produk Hukum</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Produk Hukum</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    
    
    <!-- Main content -->
    <section class="content">
        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
          
          <div class="row" id="awal">
            <div class="col-md-12" id="div_form_input" style="display:none;">
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Formulir Produk Hukum</h3>
                </div>
                <div class="box-body">
                  <form class="form-horizontal" role="form" id="form_peraturan" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=peraturan" enctype="multipart/form-data">
                    <div class="box-body">
                    
                      <input id="id_peraturan" name="id_peraturan" type="hidden">
                      <input class="form-control" id="kode_peraturan" name="kode_peraturan" value="-" placeholder="Kode Produk Hukum" type="hidden">
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="id_parent">Jenis Produk Hukum</label>
                        <div class="col-sm-10">
                          <?php echo $opsi_parent_jenis_peraturan;  ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="nomor_peraturan">Nomor Produk Hukum</label>
                        <div class="col-sm-10">
                          <input class="form-control" id="nomor_peraturan" name="nomor_peraturan" value="" placeholder="Nomor Produk Hukum" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="tahun_peraturan">Tahun Produk Hukum</label>
                        <div class="col-sm-10">
                          <input class="form-control" id="tahun_peraturan" name="tahun_peraturan" maxlength="4" value="" placeholder="0000" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="nama_peraturan">Tentang Produk Hukum</label>
                        <div class="col-sm-10">
                          <input class="form-control" id="nama_peraturan" name="nama_peraturan" value="" placeholder="Tentang Produk Hukum" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="status_peraturan">Status Produk Hukum</label>
                        <div class="col-sm-10">
                          <textarea id="status_peraturan"></textarea>
                        </div>
                      </div>
                      <input id="inserted_by" name="inserted_by" type="hidden">
                      <input id="inserted_time" name="inserted_time" type="hidden">
                      <input id="updated_by" name="updated_by" type="hidden">
                      <input id="updated_time" name="updated_time" type="hidden">
                      <input id="deleted_by" name="deleted_by" type="hidden">
                      <input id="deleted_time" name="deleted_time" type="hidden">
                      <input id="temp" name="temp" type="hidden">
                      <div class="form-group" style="display:none;">
                        <label class="col-sm-2 control-label" for="keterangan">Keterangan</label>
                        <div class="col-sm-10">
                          <textarea id="keterangan"></textarea>
                        <!--
                        <input class="form-control" id="keterangan" name="keterangan" value="" placeholder="Keterangan" type="text">
                        -->
                        </div>
                      </div>
                      <input id="status" name="status" type="hidden">
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Dokumen Produk Hukum</label>
                        <div class="col-sm-10">
                          <div class="alert alert-info alert-dismissible">
                            <div class="box-body">
                              <h4><i class="icon fa fa-paperclip"></i> Lampiran Pdf!</h4>
                              <div class="form-group">
                              <select class="form-control" id="remake" name="remake">
                                <option value="-">Pilih Lampiran</option>
                                <option value="Katalog">Katalog</option>
                                <option value="Abstrak">Abstrak</option>
                                <option value="Isi Peraturan">Isi Produk Hukum</option>
                                <option value="NA">Naskah Akademik</option>
                                <option value="MOU">MOU</option>
                                <option value="Lampiran">Lampiran Produk Hukum</option>
                              </select>
                              </div>
                              <div class="form-group">
                                <div class="btn btn-default btn-file">
                                  <i class="fa fa-paperclip"></i> File Lampiran
                                  <input type="file" name="myfile" id="peraturan_baru">
                                </div>
                              </div>
                              <div id="progress_upload_lampiran_peraturan">
                                <div id="bar_progress_upload_lampiran_peraturan"></div>
                                <div id="percent_progress_upload_lampiran_peraturan">0%</div >
                              </div>
                              <div id="message_progress_upload_lampiran_peraturan"></div>
                              <h3 class="box-title">Data Lampiran </h3>
                              <table class="table table-bordered">
                                <tr>
                                  <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                                </tr>
                                <tbody id="tbl_lampiran_peraturan">
                                </tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" id="simpan_peraturan">SIMPAN</button>
                    </div>
                  </form>
                </div>
                <div class="overlay" id="overlay_form_input" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row" id="e_div_form_input" style="display:none;">
            <div class="col-md-12">
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Formulir Edit Produk Hukum</h3>
                </div>
                <div class="box-body">
                  <form class="form-horizontal" role="form" id="e_form_peraturan" method="post" action="<?php echo base_url(); ?>attachment/e_upload/?table_name=peraturan" enctype="multipart/form-data">
                    <div class="box-body">
                      
                      <input class="form-control" id="e_id_peraturan" name="id" value="" type="hidden">
                      <input class="form-control" id="e_kode_peraturan" name="e_kode_peraturan" value="-" placeholder="Kode Produk Hukum" type="hidden">
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="e_id_parent">Jenis Produk Hukum</label>
                        <div class="col-sm-10">
                          <?php echo $e_opsi_parent_jenis_peraturan;  ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="e_nomor_peraturan">Nomor Produk Hukum</label>
                        <div class="col-sm-10">
                          <input class="form-control" id="e_nomor_peraturan" name="e_nomor_peraturan" value="" placeholder="Nomor Produk Hukum" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="e_tahun_peraturan">Tahun Produk Hukum</label>
                        <div class="col-sm-10">
                          <input class="form-control" id="e_tahun_peraturan" name="e_tahun_peraturan" value="" placeholder="0000" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="e_nama_peraturan">Tentang Produk Hukum</label>
                        <div class="col-sm-10">
                          <input class="form-control" id="e_nama_peraturan" name="e_nama_peraturan" value="" placeholder="Tentang Produk Hukum" type="text">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label" for="e_status_peraturan">Status Produk Hukum</label>
                        <div class="col-sm-10">
                          <textarea id="e_status_peraturan"></textarea>
                        </div>
                      </div>
                      <input class="form-control" id="e_inserted_by" name="e_inserted_by" value="" type="hidden">
                      <input class="form-control" id="e_inserted_time" name="e_inserted_time" value="" type="hidden">
                      <input class="form-control" id="e_updated_by" name="e_updated_by" value="" type="hidden">
                      <input class="form-control" id="e_updated_time" name="e_updated_time" value="" type="hidden">
                      <input class="form-control" id="e_deleted_by" name="e_deleted_by" value="" type="hidden">
                      <input class="form-control" id="e_deleted_time" name="e_deleted_time" value="" type="hidden">
                      <input class="form-control" id="e_temp" name="e_temp" value="" type="hidden">
                      <div class="form-group" style="display:none;">
                        <label class="col-sm-2 control-label" for="e_keterangan">Keterangan</label>
                        <div class="col-sm-10">
                          <textarea id="e_keterangan"></textarea>
                        </div>
                      </div>
                      <input class="form-control" id="e_status" name="e_status" value="" type="hidden">
                      
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Dokumen Produk Hukum</label>
                        <div class="col-sm-10">
                        <div class="alert alert-info alert-dismissible">
                          <div class="box-body">
                            <h4><i class="icon fa fa-paperclip"></i> Lampiran Pdf!</h4>
                            <div class="form-group">
                              <select class="form-control" id="e_remake" name="e_remake">
                                <option value="-">Pilih Lampiran</option>
                                <option value="Katalog">Katalog</option>
                                <option value="Abstrak">Abstrak</option>
                                <option value="Isi Peraturan">Isi Produk Hukum</option>
                                <option value="NA">Naskah Akademik</option>
                                <option value="MOU">MOU</option>
                                <option value="Lampiran">Lampiran Produk Hukum</option>
                              </select>
                            </div>
                            <div class="form-group">
                                <div class="btn btn-default btn-file">
                                  <i class="fa fa-paperclip"></i> File Lampiran
                                  <input type="file" name="myfile" id="e_peraturan_baru">
                                </div>
                            </div>
                            <div id="e_progress_upload_lampiran_peraturan">
                              <div id="e_bar_progress_upload_lampiran_peraturan"></div>
                              <div id="e_percent_progress_upload_lampiran_peraturan">0%</div >
                            </div>
                            <div id="e_message_progress_upload_lampiran_peraturan"></div>
                            <h3 class="box-title">Data Lampiran </h3>
                            <table class="table table-bordered">
                              <tr>
                                <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                              </tr>
                              <tbody id="e_tbl_lampiran_peraturan">
                              </tbody>
                            </table>
                          </div>
                        </div>
                        </div>
                      </div>
                      
                    </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" id="update_data_peraturan">SIMPAN</button>
                    </div>
                  </form>
                </div>
                <div class="overlay" id="e_overlay_form_input" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
              </div>
            </div>
          </div>

          <div class="row" id="div_ajukan" style="display:none;">
            <div class="col-md-8">
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Ajukan Ulang</h3>
                </div>
                <div class="box-body">
                  <form class="form-horizontal" role="form" id="form_ajukan" method="post" action="" enctype="multipart/form-data">
                      <input class="form-control" id="id_peraturan_ajukan" name="id_peraturan_ajukan" type="hidden">
                      <input class="form-control" id="status" name="status" type="hidden">
                      <div class="form-group">
                        <label class="control-label" for="keterangan_ajukan">Tambahkan Keterangan Pengajuan Kembali</label>
                        <textarea class="form-control" id="keterangan_ajukan"></textarea>
                      </div>
                  </form>
                </div>
                <div class="box-footer pull-right">
                  <button type="button" class="btn btn-secondary" id="close_ajukan">Close</button>
                  <button type="button" class="btn btn-primary" id="ajukan">Ajukan</button>
                </div>
                <div class="overlay" id="e_overlay_form_input" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
              </div>
            </div>
          </div>


          <div class="row" id="div_data_default">
            <div class="col-md-12">
              <div class="box box-info box-solid">
                <div class="box-header">
                  <div class="row">
                    <div class="col-md-6">
                      <a class="label label-primary" id="klik_tab_input"><i class="fa fa-plus"></i> TAMBAH DATA</a>
                    </div>
                    <div class="col-md-6">
                  <div class="box-tools pull-right" id="cari_div_form_input">
                      <form role="form" class="text-right search-form" id="cari_form_peraturan" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=peraturan" enctype="multipart/form-data">
                        
                        <div class="input-group">
                          <input class="form-control" id="cari_temp" name="temp" value="" type="hidden">
                          <input class="form-control input-sm" id="key_word" placeholder="Tentang" type="text">
                          <div class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary btn-flat" id="cari_peraturan"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                      </form>
                      <div class="overlay" id="cari_overlay_form_input" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                  </div>
                    </div>
                  </div>
                </div>
                <div class="box-body">
                  <table class="table table-bordered">
                    <tr>
                      <th>NO</th>
                      <th>Jenis</th>
                      <th>Nomor</th>
                      <th>Tahun</th>
                      <th>Tentang</th>
                      <th>Download</th>
                      <th>Status</th>
                      <th>PROSES</th> 
                    </tr>
                    <tbody id="tbl_utama_peraturan">
                    </tbody>
                    <tbody id="tbl_search_peraturan">
                    </tbody>
                  </table>
                  <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                    <?php
                    for ($x = 1; $x <= $total; $x++) {
                      echo '<li page="'.$x.'" id="'.$x.'"><a class="update_id" href="#">'.$x.'</a></li>';
                    }
                    ?>
                    </ul>
                    <div id="total_data_search"></div>
                    <ul class="pagination pagination-sm no-margin pull-right" id="next_page_search">
                    </ul>
                  </div>
                </div>
                <div class="overlay" id="overlay_data_default" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
                <div class="overlay" id="overlay_data_cari" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
              </div>
            </div>
          </div>
          
<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_input').on('click', function(e) {
	$('#e_div_form_input').fadeOut('slow');
	$('#div_form_input').show();
	});
	var aaa = Math.random();
  $('#temp').val(aaa);
});
</script>

<script>
  function load_default(halaman) {
    $('#overlay_data_default').show();
    $('#tbl_utama_peraturan').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>peraturan/json_all_peraturan/?halaman='+halaman+'/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * <?php echo $per_page; ?>);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_peraturan="' + json[i].id_peraturan + '" id="' + json[i].id_peraturan + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
          if(json[i].id_parent==321){tr += '<td valign="top">Kep. DPRD</td>';}
          if(json[i].id_parent==587){tr += '<td valign="top">Naskah Akademik</td>';}
          if(json[i].id_parent==7){tr += '<td valign="top">Peraturan Daerah</td>';}
          if(json[i].id_parent==8){tr += '<td valign="top">Perbup</td>';}
          if(json[i].id_parent==9){tr += '<td valign="top">Kep. Bupati</td>';}
          if(json[i].id_parent==10){tr += '<td valign="top">Intruksi Bupati</td>';}
          if(json[i].id_parent==11){tr += '<td valign="top">Raperda</td>';}
          if(json[i].id_parent==19){tr += '<td valign="top">E-book</td>';}
          if(json[i].id_parent==0){tr += '<td valign="top">Jenis</td>';}
          if(json[i].id_parent==1){tr += '<td valign="top">Peraturan Desa</td>';}
					tr += '<td valign="top">' + json[i].nomor_peraturan + '</td>';
					tr += '<td valign="top">' + json[i].tahun_peraturan + '</td>';
					tr += '<td valign="top">' + json[i].nama_peraturan + '</td>';
          tr += '<td valign="top">';
          var attachment_katalog = json[i].attachment_katalog;
          if(!attachment_katalog){ 
            tr += '';
          }
          else{
            tr += ' <a class="btn btn-success btn-xs" href="<?php echo base_url(); ?>media/upload/' + attachment_katalog + '" target="_blank"><i class="fa fa-download"></i> Katalog</a> | ';
          }
          var attachment_abstrak = json[i].attachment_abstrak;
          if(!attachment_abstrak){ 
            tr += '';
          }
          else{
            tr += ' <a class="btn btn-warning btn-xs" href="<?php echo base_url(); ?>media/upload/' + attachment_abstrak + '" target="_blank"><i class="fa fa-download"></i> Abstrak</a> |';
          }
          var attachment_isi_peraturan = json[i].attachment_isi_peraturan;
          if(!attachment_isi_peraturan){ 
            tr += '';
          }
          else{
            tr += ' <a class="btn btn-info btn-xs" href="<?php echo base_url(); ?>media/upload/' + attachment_isi_peraturan + '" target="_blank"><i class="fa fa-download"></i> Isi Produk Hukum</a> ';
          }
          var attachment_na = json[i].attachment_na;
          if(!attachment_na){ 
            tr += '';
          }
          else{
            tr += ' <a class="btn btn-info btn-xs" href="<?php echo base_url(); ?>media/upload/' + attachment_na + '" target="_blank"><i class="fa fa-download"></i> NA</a> ';
          }
          var attachment_mou = json[i].attachment_mou;
          if(!attachment_mou){ 
            tr += '';
          }
          else{
            tr += ' <a class="btn btn-info btn-xs" href="<?php echo base_url(); ?>media/upload/' + attachment_mou + '" target="_blank"><i class="fa fa-download"></i> MOU</a> ';
          }
          tr += '</td>';
					tr += '<td valign="top">';          
          if(json[i].status == '1'){
            tr += '<a class="btn btn-warning btn-xs"><i class="fa fa-refresh fa-spin"></i> Menunggu Evaluasi</a><br />';
          }else
          if(json[i].status == '2'){
            tr += '<a class="btn bg-yellow btn-xs"><i class="fa fa-refresh fa-spin"></i> Revisi Perbaikan</a><br />';
          }else
          if(json[i].status == '3'){
            tr += '<a class="btn btn-success btn-xs"><i class="fa fa-check"></i> Tervalidasi</a><br />';
          }
          var status_keterangan = '';
          var status_keterangan_raw = json[i].keterangan;
          var status_keterangan_raw = status_keterangan_raw.split("<br>");
          $.each(status_keterangan_raw, function( index, value ) {
            status_keterangan = value;
          })
          tr += ''+ status_keterangan + '';
          tr += '</td>';
					tr += '<td valign="top">';
					tr += '<a href="#tab_1" data-toggle="tab" class="update_id btn btn-default btn-xs"><i class="fa fa-pencil-square-o"></i></a> ';
					tr += '<a href="#tab_1" data-toggle="tab" id="del_ajax" class="btn btn-danger btn-xs"><i class="fa fa-cut"></i></a>';
          if(json[i].status == '2'){
            tr += '<br /><br /><a class="btn bg-primary btn-xs" onClick="ajukan(' + json[i].id_peraturan + ');" ><i class="fa fa-reply"></i> Ajukan Kembali</a>';
          }
					tr += '</td>';
          tr += '</tr>';
        }
        $('#tbl_utama_peraturan').append(tr);
				$('#overlay_data_default').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
function ajukan(id){
  $('#div_ajukan').show();
  $('#id_peraturan_ajukan').val(id);
  $('#status').val(1);
  $('#ajukan').removeAttr('disabled', 'disabled');
  CKEDITOR.instances.keterangan_ajukan.setData();
}
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#close_ajukan').on('click', function(e) {
      $('#div_ajukan').hide();
      CKEDITOR.instances.keterangan_ajukan.setData('');
      $('#form_ajukan form').trigger('reset');
      $('#ajukan').removeAttr('disabled', 'disabled');
      $('#overlay_form_input').fadeOut('slow');
    })
  });
  $(document).ready(function() {
    $('#ajukan').on('click', function(e) {
      e.preventDefault();
      $('#ajukan').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
			
			var id_peraturan = $("#id_peraturan_ajukan").val();
			var status = $("#status").val();
			//var keterangan_ajukan = $("#keterangan_ajukan").val();
      var keterangan = CKEDITOR.instances.keterangan_ajukan.getData();
						
			$.ajax({
        type: 'POST',
        async: true,
        data: {
					id_peraturan:id_peraturan,
					keterangan:keterangan,
					status:status,
										},
        dataType: 'text',
        url: '<?php echo base_url(); ?>peraturan/ajukan/',
        success: function(json) {
          if (json == '') {
            $('#ajukan').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut();
            alertify.set({ delay: 3000 });
            alertify.error("Error");
          } else {
            alertify.set({ delay: 3000 });
            alertify.success("Berhasil di Ajukan Ulang");
						$('#tbl_lampiran_peraturan').html('');
						$('#message_progress_upload_lampiran_peraturan').html('');
            var halaman = 1;
            load_default(halaman);
            $('#div_ajukan').hide();
            CKEDITOR.instances.keterangan_ajukan.setData('');
            $('#form_ajukan form').trigger('reset');
            $('#ajukan').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut('slow');
          }
        }
      });
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#pagination').on('click', '.update_id', function(e) {
		e.preventDefault();
		var id = $(this).closest('li').attr('page');
		var halaman = id;
		load_default(halaman);
	});
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	var halaman = 1;
	load_default(halaman);
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#tbl_utama_peraturan, #tbl_search_peraturan').on('click', '.update_id', function(e) {
		e.preventDefault();
		$('#e_overlay_form_input').show();
		var id = $(this).closest('tr').attr('id_peraturan');
		$.ajax({
			dataType: 'json',
			url: '<?php echo base_url(); ?>peraturan/peraturan_get_by_id/?id='+id+'',
			success: function(json) {
				if (json.length == 0) {
					alert('Tidak ada data');
				} else {
					for (var i = 0; i < json.length; i++) {
					$('#e_id_peraturan').val(json[i].id_peraturan);
					$('#e_id_parent').val(json[i].id_parent);
					$('#e_nomor_peraturan').val(json[i].nomor_peraturan);
					$('#e_tahun_peraturan').val(json[i].tahun_peraturan);
					// $('#e_status_peraturan').val(json[i].status_peraturan);
          CKEDITOR.instances.e_status_peraturan.setData(json[i].status_peraturan);
					$('#e_kode_peraturan').val(json[i].kode_peraturan);
					$('#e_nama_peraturan').val(json[i].nama_peraturan);
					$('#e_inserted_by').val(json[i].inserted_by);
					$('#e_inserted_time').val(json[i].inserted_time);
					$('#e_updated_by').val(json[i].updated_by);
					$('#e_updated_time').val(json[i].updated_time);
					$('#e_deleted_by').val(json[i].deleted_by);
					$('#e_deleted_time').val(json[i].deleted_time);
					$('#e_temp').val(json[i].temp);
					// $('#e_keterangan').val(json[i].keterangan);
          CKEDITOR.instances.e_keterangan.setData(json[i].keterangan);
					$('#e_status').val(json[i].status);
										}
					e_load_lampiran_peraturan(id);
					$('#div_form_input').fadeOut('slow');
					$('#e_div_form_input').show();
					$('#e_overlay_form_input').fadeOut(2000);
					$('html, body').animate({
						scrollTop: $('#awal').offset().top
					}, 1000);
				}
			}
		});
	});
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_peraturan').on('click', function(e) {
      e.preventDefault();
      $('#simpan_peraturan').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
			
			var id_peraturan = $("#id_peraturan").val();
			var id_parent = $("#id_parent").val();
			var jenis_peraturan = $("#jenis_peraturan").val();
			var nomor_peraturan = $("#nomor_peraturan").val();
			var tahun_peraturan = $("#tahun_peraturan").val();
			// var status_peraturan = $("#status_peraturan").val();
      var status_peraturan = CKEDITOR.instances.status_peraturan.getData();
			var kode_peraturan = $("#kode_peraturan").val();
			var nama_peraturan = $("#nama_peraturan").val();
			var inserted_by = $("#inserted_by").val();
			var inserted_time = $("#inserted_time").val();
			var updated_by = $("#updated_by").val();
			var updated_time = $("#updated_time").val();
			var deleted_by = $("#deleted_by").val();
			var deleted_time = $("#deleted_time").val();
			var temp = $("#temp").val();
			// var keterangan = $("#keterangan").val();
      var keterangan = CKEDITOR.instances.keterangan.getData();
			var status = $("#status").val();
						
			if (nomor_peraturan == '') {
					$('#nomor_peraturan').css('background-color', '#DFB5B4');
				} else {
					$('#nomor_peraturan').removeAttr('style');
				}
			if (tahun_peraturan == '') {
					$('#tahun_peraturan').css('background-color', '#DFB5B4');
				} else {
					$('#tahun_peraturan').removeAttr('style');
				}
			if (kode_peraturan == '') {
					$('#kode_peraturan').css('background-color', '#DFB5B4');
				} else {
					$('#kode_peraturan').removeAttr('style');
				}
			if (nama_peraturan == '') {
					$('#nama_peraturan').css('background-color', '#DFB5B4');
				} else {
					$('#nama_peraturan').removeAttr('style');
				}
			if (inserted_by == '') {
					$('#inserted_by').css('background-color', '#DFB5B4');
				} else {
					$('#inserted_by').removeAttr('style');
				}
			if (temp == '') {
					$('#temp').css('background-color', '#DFB5B4');
				} else {
					$('#temp').removeAttr('style');
				}
						
			$.ajax({
        type: 'POST',
        async: true,
        data: {
					id_parent:id_parent,
					nomor_peraturan:nomor_peraturan,
					tahun_peraturan:tahun_peraturan,
					status_peraturan:status_peraturan,
					kode_peraturan:kode_peraturan,
					nama_peraturan:nama_peraturan,
					temp:temp,
					keterangan:keterangan,
					status:status,
										},
        dataType: 'text',
        url: '<?php echo base_url(); ?>peraturan/simpan_peraturan/',
        success: function(json) {
          if (json == '0') {
            $('#simpan_peraturan').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut();
            alertify.set({ delay: 3000 });
            alertify.error("Gagal simpan");
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
          } else {
            alertify.set({ delay: 3000 });
            alertify.success("Berhasil simpan");
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
						$('#tbl_lampiran_peraturan').html('');
						$('#message_progress_upload_lampiran_peraturan').html('');
            var halaman = 1;
            load_default(halaman);
            CKEDITOR.instances.keterangan.setData('');
            CKEDITOR.instances.status_peraturan.setData('');
            var aaa = Math.random();
            $('#temp').val(aaa);
            $('#div_form_input form').trigger('reset');
            $('#simpan_peraturan').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut('slow');
          }
        }
      });
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#update_data_peraturan').on('click', function(e) {
		e.preventDefault();
		$('#update_data_peraturan').attr('disabled', 'disabled');
		$('#e_overlay_form_input').show();
		var id_peraturan = $('#e_id_peraturan').val();
		var id_parent = $('#e_id_parent').val();
		var nomor_peraturan = $('#e_nomor_peraturan').val();
		var tahun_peraturan = $('#e_tahun_peraturan').val();
		// var status_peraturan = $('#e_status_peraturan').val();
		var status_peraturan = CKEDITOR.instances.e_status_peraturan.getData();
		var kode_peraturan = $('#e_kode_peraturan').val();
		var nama_peraturan = $('#e_nama_peraturan').val();
		var temp = $('#e_temp').val();
		// var keterangan = $('#e_keterangan').val();
		var keterangan = CKEDITOR.instances.e_keterangan.getData();
						
		if (id_peraturan == '') {
				$('#e_id_peraturan').css('background-color', '#DFB5B4');
			} else {
				$('#e_id_peraturan').removeAttr('style');
			}
		if (nomor_peraturan == '') {
				$('#e_nomor_peraturan').css('background-color', '#DFB5B4');
			} else {
				$('#e_nomor_peraturan').removeAttr('style');
			}
		if (tahun_peraturan == '') {
				$('#e_tahun_peraturan').css('background-color', '#DFB5B4');
			} else {
				$('#e_tahun_peraturan').removeAttr('style');
			}
		if (status_peraturan == '') {
				$('#e_status_peraturan').css('background-color', '#DFB5B4');
			} else {
				$('#e_status_peraturan').removeAttr('style');
			}
		if (kode_peraturan == '') {
				$('#e_kode_peraturan').css('background-color', '#DFB5B4');
			} else {
				$('#e_kode_peraturan').removeAttr('style');
			}
		if (nama_peraturan == '') {
				$('#e_nama_peraturan').css('background-color', '#DFB5B4');
			} else {
				$('#e_nama_peraturan').removeAttr('style');
			}
		if (temp == '') {
				$('#e_temp').css('background-color', '#DFB5B4');
			} else {
				$('#e_temp').removeAttr('style');
			}
				
			$.ajax({
			type: 'POST',
			async: true,
			data: {
					
					id_peraturan:id_peraturan,
					id_parent:id_parent,
					nomor_peraturan:nomor_peraturan,
					tahun_peraturan:tahun_peraturan,
					status_peraturan:status_peraturan,
					kode_peraturan:kode_peraturan,
					nama_peraturan:nama_peraturan,
					temp:temp,
					keterangan:keterangan,
										
				},
			dataType: 'json',
			url: '<?php echo base_url(); ?>peraturan/update_data_peraturan/',
			success: function(json) {
				if (json.length == 0) {
          alertify.set({ delay: 3000 });
          alertify.error("Gagal simpan");
					$('#update_data_peraturan').removeAttr('disabled', 'disabled');
					$('#e_overlay_form_input').fadeOut('slow');
					} 
				else {
          alertify.set({ delay: 3000 });
          alertify.success("Berhasil simpan");
					var halaman = 1;
					load_default(halaman);
					$('#e_div_form_input form').trigger('reset');
					$('#update_data_peraturan').removeAttr('disabled', 'disabled');
					$('#e_overlay_form_input').fadeOut('slow');
					$('#e_div_form_input').fadeOut('slow');
					$('#tbl_lampiran_peraturan').html('');
					$('#e_tbl_lampiran_peraturan').html('');
					$('#a_tbl_lampiran_peraturan').html('');
					}
			}
		});
	});
});
</script>

<script>
function reset() {
	$('#toggleCSS').attr('href', '<?php echo base_url(); ?>css/alertify/alertify.default.css');
	alertify.set({
		labels: {
			ok: 'OK',
			cancel: 'Cancel'
		},
		delay: 5000,
		buttonReverse: false,
		buttonFocus: 'ok'
	});
}
//===============HAPUS OBAT
$('#tbl_search_peraturan, #tbl_utama_peraturan').on('click', '#del_ajax', function() {
	reset();
	var id = $(this).closest('tr').attr('id_peraturan');
	alertify.confirm('Anda yakin data akan dihapus?', function(e) {
		if (e) {
			$.ajax({
				dataType: 'json',
				url: '<?php echo base_url(); ?>peraturan/hapus_peraturan/?id='+id+'',
				success: function(response) {
					if (response.errors == 'Yes') {
						alertify.alert('Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya');
					} else {
						$('[id_peraturan='+id+']').remove();
						alertify.alert('Data berhasil dihapus');
					}
				}
			});
		} else {
			alertify.alert('Hapus data dibatalkan');
		}
	});
});
//===============HAPUS Attachment
$('#tbl_lampiran_peraturan, #e_tbl_lampiran_peraturan, #a_tbl_lampiran_peraturan').on('click', '#del_ajax', function() {
	reset();
	var id = $(this).closest('tr').attr('id_attachment');
	alertify.confirm('Anda yakin data akan dihapus?', function(e) {
		if (e) {
			$.ajax({
				dataType: 'json',
				url: '<?php echo base_url(); ?>attachment/hapus/?id='+id+'',
				success: function(response) {
					if (response.errors == 'Yes') {
						alertify.alert('Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya');
					} else {
						$('[id_attachment='+id+']').remove();
						$('#message_progress_upload_lampiran_peraturan').remove();
						$('#e_message_progress_upload_lampiran_peraturan').remove();
						$('#a_message_progress_upload_lampiran_peraturan').remove();
						$('#percent_progress_upload_lampiran_peraturan').html('');
						$('#e_percent_progress_upload_lampiran_peraturan').html('');
						$('#a_percent_progress_upload_lampiran_peraturan').html('');
						alertify.alert('Data berhasil dihapus');
					}
				}
			});
		} else {
			alertify.alert('Hapus data dibatalkan');
		}
	});
});

</script>

<script>
	function load_lampiran_peraturan() {
		$('#tbl_lampiran_peraturan').html('');
		var temp = $('#temp').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				temp:temp
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran_peraturan/?table=peraturan',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_lampiran_peraturan').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
			var options = { 
				beforeSend: function() {
					$('#progress_upload_lampiran_peraturan').show();
					$('#bar_progress_upload_lampiran_peraturan').width('0%');
					$('#message_progress_upload_lampiran_peraturan').html('');
					$('#percent_progress_upload_lampiran_peraturan').html('0%');
					},
				uploadProgress: function(event, position, total, percentComplete){
					$('#bar_progress_upload_lampiran_peraturan').width(percentComplete+'%');
					$('#percent_progress_upload_lampiran_peraturan').html(percentComplete+'%');
					},
				success: function(){
					$('#bar_progress_upload_lampiran_peraturan').width('100%');
					$('#percent_progress_upload_lampiran_peraturan').html('100%');
					},
				complete: function(response){
					$('#message_progress_upload_lampiran_peraturan').html('<font color="green">'+response.responseText+'</font>');
					var temp = $('#temp').val();
					load_lampiran_peraturan();
					},
				error: function(){
					$('#message_progress_upload_lampiran_peraturan').html('<font color="red"> ERROR: unable to upload files</font>');
					}     
			};
			document.getElementById('peraturan_baru').onchange = function() {
					$('#form_peraturan').submit();
				};
			$('#form_peraturan').ajaxForm(options);
		});
</script>

<script>
	function e_load_lampiran_peraturan() {
		$('#e_tbl_lampiran_peraturan').html('');
		var id = $('#e_id_peraturan').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				id:id
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/e_load_lampiran_peraturan/?table=peraturan',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#e_tbl_lampiran_peraturan').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
			var options = { 
				beforeSend: function() {
					$('#e_progress_upload_lampiran_peraturan').show();
					$('#e_bar_progress_upload_lampiran_peraturan').width('0%');
					$('#e_message_progress_upload_lampiran_peraturan').html('');
					$('#e_percent_progress_upload_lampiran_peraturan').html('0%');
					},
				uploadProgress: function(event, position, total, percentComplete){
					$('#e_bar_progress_upload_lampiran_peraturan').width(percentComplete+'%');
					$('#e_percent_progress_upload_lampiran_peraturan').html(percentComplete+'%');
					},
				success: function(){
					$('#e_bar_progress_upload_lampiran_peraturan').width('100%');
					$('#e_percent_progress_upload_lampiran_peraturan').html('100%');
					},
				complete: function(response){
					$('#e_message_progress_upload_lampiran_peraturan').html('<font color="green">'+response.responseText+'</font>');
					e_load_lampiran_peraturan();
					},
				error: function(){
					$('#e_message_progress_upload_lampiran_peraturan').html('<font color="red"> ERROR: unable to upload files</font>');
					}     
			};
			document.getElementById('e_peraturan_baru').onchange = function() {
					$('#e_form_peraturan').submit();
				};
			$('#e_form_peraturan').ajaxForm(options);
		});
</script>

<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('keterangan');
	CKEDITOR.replace('e_keterangan');
	CKEDITOR.replace('status_peraturan');
	CKEDITOR.replace('e_status_peraturan');
  CKEDITOR.replace('keterangan_ajukan');
	//bootstrap WYSIHTML5 - text editor
	$(".textarea").wysihtml5();
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#cari_peraturan').on('click', function(e) {
      e.preventDefault();
      $('#overlay_data_cari').show();
      var key_word = $('#key_word').val();
      var start = 0;
      $('#tbl_utama_peraturan').html('');
      $('#tbl_search_peraturan').html('');
      $('#total_data_search').html('');
      $.ajax({
        type: 'POST',
        async: true,
        data: {
          key_word: key_word,
          halaman: 1
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>peraturan/search_peraturan/',
        success: function(json) {
          if (json.length == 0) {
            if (key_word == '') {
              $('#key_word').css('background-color', '#DFB5B4');
            } else {
              $('#key_word').removeAttr('style');
            }
						$('#overlay_data_cari').fadeOut('slow');
          } else {
            $('#tbl_utama_peraturan').html('');
            $('#tbl_search_peraturan').html('');
            var tr = '';
            for (var i = 0; i < json.length; i++) {
              start = start + 1;
              tr += '<tr id_peraturan="' + json[i].id_peraturan + '" id="' + json[i].id_peraturan + '" >';
              tr += '<td valign="top">' + (start) + '</td>';
              if(json[i].id_parent==587){tr += '<td valign="top">Naskah Akademik</td>';}
              if(json[i].id_parent==321){tr += '<td valign="top">Kep. DPRD</td>';}
              if(json[i].id_parent==7){tr += '<td valign="top">Peraturan Daerah</td>';}
              if(json[i].id_parent==8){tr += '<td valign="top">Perbup</td>';}
              if(json[i].id_parent==9){tr += '<td valign="top">Kep. Bupati</td>';}
              if(json[i].id_parent==10){tr += '<td valign="top">Intruksi Bupati</td>';}
              if(json[i].id_parent==11){tr += '<td valign="top">Raperda</td>';}
              if(json[i].id_parent==19){tr += '<td valign="top">E-book</td>';}
              if(json[i].id_parent==0){tr += '<td valign="top">Jenis</td>';}
              tr += '<td valign="top">' + json[i].nomor_peraturan + '</td>';
              tr += '<td valign="top">' + json[i].tahun_peraturan + '</td>';
              tr += '<td valign="top">' + json[i].nama_peraturan + '</td>';
              tr += '<td valign="top">';
              
              var attachment_katalog = json[i].attachment_katalog;
              if(!attachment_katalog){ 
                tr += '-';
              }
              else{
                tr += '<a href="<?php echo base_url(); ?>media/upload/' + attachment_katalog + '" target="_blank"> Katalog</a>';
              }
              var attachment_abstrak = json[i].attachment_abstrak;
              if(!attachment_abstrak){ 
                tr += '-';
              }
              else{
                tr += '<a href="<?php echo base_url(); ?>media/upload/' + attachment_abstrak + '" target="_blank"> Abstrak</a>';
              }
              var attachment_isi_peraturan = json[i].attachment_isi_peraturan;
              if(!attachment_isi_peraturan){ 
                tr += '-';
              }
              else{
                tr += '<a href="<?php echo base_url(); ?>media/upload/' + attachment_isi_peraturan + '" target="_blank">Isi Produk Hukum</a>';
              }
							var attachment_na = json[i].attachment_na;
							if(!attachment_na){ 
								tr += '';
							}
							else{
								tr += ' <a class="btn btn-info btn-xs" href="<?php echo base_url(); ?>media/upload/' + attachment_na + '" target="_blank"><i class="fa fa-download"></i> NA</a> ';
							}
							var attachment_mou = json[i].attachment_mou;
							if(!attachment_mou){ 
								tr += '';
							}
							else{
								tr += ' <a class="btn btn-info btn-xs" href="<?php echo base_url(); ?>media/upload/' + attachment_mou + '" target="_blank"><i class="fa fa-download"></i> MOU</a> ';
							}
              tr += '</td>';
              tr += '<td valign="top">' + json[i].status_peraturan + '</td>';
              tr += '<td valign="top">';
              tr += '<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> ';
              tr += '<a href="#tab_1" data-toggle="tab" id="del_ajax" ><i class="fa fa-cut"></i></a>';
              tr += '</td>';
              tr += '</tr>';
            }
						$('#key_word').removeAttr('style');
            $('#tbl_utama_peraturan').hide();
            $('#tbl_search_peraturan').append(tr);
						$('#overlay_data_default').hide('slow');
						$('#overlay_data_cari').fadeOut('slow');
          }
        }
      });
      $('#total_data_search').html('');
      $.ajax({
        dataType: 'text',
        url: '<?php echo base_url(); ?>peraturan/count_all_search_peraturan/?key_word='+key_word+'',
        success: function(json) {
          var jumlah = json;
          $('#next_page_search').html('');
          var ajax_pagination = '';
          for (var a = 0; a < jumlah; a++) {
            ajax_pagination += '<li id="'+a+'" page="'+a+'" key_word="'+key_word+'" ><a id="next" href="#">'+(a + 1)+'</a></li>';
          }
          $('#next_page_search').append(ajax_pagination);
        }
      });
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#next_page_search').on('click', '#next', function(e) {
      e.preventDefault();
			$('#overlay_data_cari').show();
      var id = $(this).closest('li').attr('page');
      var key_word = $(this).closest('li').attr('key_word');
      var keyword_f = $(this).closest('li').attr('keyword_f');
			var halaman = parseInt(id) + 1;
      $('#tbl_search_peraturan').html('');
      $.ajax({
				type: 'POST',
        async: true,
        data: {
          key_word: key_word,
					halaman:halaman
					},
        dataType: 'json',
        url: '<?php echo base_url(); ?>peraturan/search_peraturan/',
        success: function(json) {
          var tr = '';
					var start = (((parseInt(id) + 1) - 1) * <?php echo $per_page; ?>);
					for (var i = 0; i < json.length; i++) {
						var start = parseInt(start) + 1;
            tr += '<tr id_peraturan="' + json[i].id_peraturan + '" id="' + json[i].id_peraturan + '" >';
            tr += '<td valign="top">' + (start) + '</td>';
            tr += '<td valign="top">' + json[i].nomor_peraturan + '</td>';
            tr += '<td valign="top">' + json[i].tahun_peraturan + '</td>';
            tr += '<td valign="top">' + json[i].nama_peraturan + '</td>';
            
            var attachment_katalog = json[i].attachment_katalog;
            if(!attachment_katalog){ 
              tr += '<td valign="top"> - </td>';
            }
            else{
              tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/' + attachment_katalog + '" target="_blank">Lihat Katalog</a></td>';
            }
            var attachment_abstrak = json[i].attachment_abstrak;
            if(!attachment_abstrak){ 
              tr += '<td valign="top"> - </td>';
            }
            else{
              tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/' + attachment_abstrak + '" target="_blank">Lihat Abstrak</a></td>';
            }
            var attachment_isi_peraturan = json[i].attachment_isi_peraturan;
            if(!attachment_isi_peraturan){ 
              tr += '<td valign="top"> - </td>';
            }
            else{
              tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/' + attachment_isi_peraturan + '" target="_blank">Isi Produk Hukum</a></td>';
            }
          var attachment_na = json[i].attachment_na;
          if(!attachment_na){ 
            tr += '';
          }
          else{
            tr += ' <a class="btn btn-info btn-xs" href="<?php echo base_url(); ?>media/upload/' + attachment_na + '" target="_blank"><i class="fa fa-download"></i> NA</a> ';
          }
          var attachment_mou = json[i].attachment_mou;
          if(!attachment_mou){ 
            tr += '';
          }
          else{
            tr += ' <a class="btn btn-info btn-xs" href="<?php echo base_url(); ?>media/upload/' + attachment_mou + '" target="_blank"><i class="fa fa-download"></i> MOU</a> ';
          }
            tr += '<td valign="top">' + json[i].status_peraturan + '</td>';
            tr += '<td valign="top">';
            tr += '<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> ';
            tr += '<a href="#tab_1" data-toggle="tab" id="del_ajax" ><i class="fa fa-cut"></i></a>';
            tr += '</td>';
            tr += '</tr>';
          }
          $('#tbl_search_peraturan').append(tr);
          $('#overlay_data_cari').fadeOut('slow');
        }
      });
    });
  });
</script>
