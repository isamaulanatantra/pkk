<?php 
  if($this->input->get('tentang')=='Peraturan_Daerah.html'){echo '<h1>Peraturan Daerah</h1>';}
  if($this->input->get('tentang')=='Peraturan_Bupati.html'){echo '<h1>Peraturan Bupati</h1>';}
  if($this->input->get('tentang')=='Peraturan_Desa.html'){echo '<h1>Peraturan Desa</h1>';}
  if($this->input->get('tentang')=='Naskah_Akademik.html'){echo '<h1>Naskah Akademik</h1>';}
  if($this->input->get('tentang')=='Keputusan_DPRD.html'){echo '<h1>Keputusan DPRD</h1>';}
  else{}
?>

            <table id="datatable-buttons" class="table table-bordered table-striped">
              <thead>
              <tr>
                <th>No. Urut</th>
                <th>Nomor Peraturan</th>
                <th>Tahun Peraturan</th>
                <th>Tentang Peraturan</th>
                <th>Download Peraturan</th>
                <th>Status Peraturan</th>
              </tr>
              </thead>
              <tbody>
      <?php $nomor=0; ?>
      <?php foreach ($daftar_peraturan->result() as $r_daftar_peraturan):?>
      <?php $nomor=$nomor+1; 
      
                                $healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
                                $yummy   = array("_","","","","","","","","","","","","","");
                                $newphrase = str_replace($healthy, $yummy, $r_daftar_peraturan->nama_peraturan);
      ?>
                <tr>
                  <th scope="row"><?php echo $nomor;?></th>
                  <td><?php echo $r_daftar_peraturan->nomor_peraturan;?></td>
                  <td><?php echo $r_daftar_peraturan->tahun_peraturan;?></td>
                  <td><a href="<?php echo base_url(); ?>peraturan/details/<?php echo $r_daftar_peraturan->id_peraturan;?>/<?php echo $newphrase;?>.HTML" style="color:#2556b9; text-transform: uppercase;"><?php echo $r_daftar_peraturan->nama_peraturan;?></a></td>
                  <td>
                  <?php 
                    $w = $this->db->query("
                    SELECT * from attachment
                    where attachment.id_tabel=".$r_daftar_peraturan->id_peraturan."
                    and attachment.table_name='peraturan'
                    ");
                    $x = $w->num_rows();
                    // $nomor = 0;
                    foreach($w->result() as $row_attachment){
                      $keterangan = $row_attachment->keterangan;
                      if($keterangan=='Katalog'){
                        echo'<a class="btn btn-info btn-xs" href="'.base_url().'media/upload/'.$row_attachment->file_name.'" target="_blank"><i class="fa fa-download"></i> Katalog</a><br />';
                      }
                      elseif($keterangan=='Abstrak'){
                        echo'<a class="btn btn-info btn-xs" href="'.base_url().'media/upload/'.$row_attachment->file_name.'" target="_blank"><i class="fa fa-download"></i> Abstrak</a><br />';
                      }
                      elseif($keterangan=='Isi Peraturan'){
                        echo'<a class="btn btn-info btn-xs" href="'.base_url().'media/upload/'.$row_attachment->file_name.'" target="_blank"><i class="fa fa-download"></i> Isi Peraturan</a><br />';
                      }
                      elseif($keterangan=='NA'){
                        echo'<a class="btn btn-info btn-xs" href="'.base_url().'media/upload/'.$row_attachment->file_name.'" target="_blank"><i class="fa fa-download"></i> Naskah Akademik</a><br />';
                      }
                      elseif($keterangan=='MOU'){
                        echo'<a class="btn btn-info btn-xs" href="'.base_url().'media/upload/'.$row_attachment->file_name.'" target="_blank"><i class="fa fa-download"></i> MOU</a><br />';
                      }
                      else{
                      }
                    }
                  ?>
                  </td>
                  <td>
                  <?php 
                    if($r_daftar_peraturan->status=='1'){
                      echo'<a class="btn btn-warning btn-xs"><i class="fa fa-refresh fa-spin"></i> Menunggu Evaluasi</a><br />';
                    }else
                    if($r_daftar_peraturan->status=='2'){
                      echo'<a class="btn btn-primary btn-xs"><i class="fa fa-refresh fa-spin"></i> Revisi Perbaikan</a><br />';
                    }else
                    if($r_daftar_peraturan->status=='3'){
                      echo'<a class="btn btn-success btn-xs"><i class="fa fa-check"></i> Tervalidasi</a><br />';
                    }
                  echo $r_daftar_peraturan->status_peraturan;
                  ?>
                  </td>
                </tr>
      <?php endforeach; ?>
              </tbody>
            </table>
        
    <!-- Datatables -->
    <script>
      $(document).ready(function() {
        var handleDataTableButtons = function() {
          if ($("#datatable-buttons").length) {
            $("#datatable-buttons").DataTable({
              dom: "Bfrtip",
              buttons: [
                {
                  extend: "copy",
                  className: "btn-sm"
                },
                {
                  extend: "csv",
                  className: "btn-sm"
                },
                {
                  extend: "excel",
                  className: "btn-sm"
                },
                {
                  extend: "pdfHtml5",
                  className: "btn-sm"
                },
                {
                  extend: "print",
                  className: "btn-sm"
                },
              ],
              responsive: true
            });
          }
        };

        TableManageButtons = function() {
          "use strict";
          return {
            init: function() {
              handleDataTableButtons();
            }
          };
        }();

        TableManageButtons.init();
      });
    </script>
    <!-- /Datatables -->
    