
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                Data Users
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="kata_kunci_filter" />
                  <select name="limit_data_users" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_users">
                    <option value="10">10 Per-Halaman</option>
                    <option value="20">20 Per-Halaman</option>
                    <option value="50">50 Per-Halaman</option>
                    <option value="999999999">Semua</option>
                  </select>
                  <select name="urut_data_users" class="form-control input-sm pull-right" style="width: 150px;" id="urut_data_users">
                    <option value="nama">Nama User</option>
                    <option value="user_name">User Name</option>
                    <option value="hak_akses">Hak Akses</option>
                  </select>
                  <select name="hak_akses_users" class="form-control input-sm pull-right" style="width: 150px;" id="hak_akses_users">
                    <option value="web_desa">Web Desa</option>
                    <option value="web_skpd">Web SKPD</option>
                    <option value="admin_web_kecamatan">Admin Kecamatan</option>
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default" id="search_kata_kunci"><i class="fa fa-search"></i></button>
                    <div class="overlay" id="spinners_users" style="display:none;">
                      <i class="fa fa-refresh fa-spin"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body table-responsive">
                <div id="tblExport">
                  <table class="table table-bordered table-hover">
                    <tbody>
                      <tr id="header_exel">
                        <th>No</th>
                        <th>Nama</th>
                        <th>UserName</th>
                        <th>Hak Akses</th>
                        <th>SKPD</th>
                        <!--<th>Provinsi</th>
                        <th>Kabupaten</th>-->
                        <th>Kecamatan</th>
                        <th>Desa</th>
                        <th>Domain</th>
                        <th>Proses</th>
                      </tr>
                    </tbody>
                    <tbody id="tbl_utama_users">
                    </tbody>
                  </table>
                </div>
              </div><!-- /.box-body -->
              <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                </ul>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
            
<script type="text/javascript">
  $(document).ready(function() {
        var halaman = 1;
        var limit_data_users = $('#limit_data_users').val();
        var limit = limit_per_page_custome(limit_data_users);
        var kata_kunci = $('#kata_kunci').val();
        var urut_data_users = $('#urut_data_users').val();
        var hak_akses = $('#hak_akses_users').val();
        load_data_users(halaman, limit, kata_kunci, urut_data_users, hak_akses);
  }); 
</script>
<script>
  function load_data_users(halaman, limit, kata_kunci, urut_data_users, hak_akses) {
    $('#tbl_utama_users').html('');
    $('#spinners_data').show();
    var limit_data_users = $('#limit_data_users').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:halaman,
        limit:limit,
        kata_kunci:kata_kunci,
        urut_data_users:urut_data_users,
        hak_akses:hak_akses
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>user/json_all_users/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * limit);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_users="' + json[i].id_users + '" id="' + json[i].id_users + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
					tr += '<td valign="top">' + json[i].nama + '</td>';
					tr += '<td valign="top">' + json[i].user_name + '</td>';
					tr += '<td valign="top">' + json[i].hak_akses + '</td>';
					tr += '<td valign="top">' + json[i].nama_skpd + '</td>';
					// tr += '<td valign="top">' + json[i].nama_propinsi + '</td>';
					// tr += '<td valign="top">' + json[i].nama_kabupaten + '</td>';
					tr += '<td valign="top">' + json[i].nama_kecamatan + '</td>';
					tr += '<td valign="top">' + json[i].nama_desa + '</td>';
					tr += '<td valign="top"><a target="_blank" href="https://' + json[i].data_website + '">' + json[i].data_website + '</td>';
					tr += '<td valign="top"><a target="_blank" href="#">' + json[i].status + '</a></td>';
          tr += '</tr>';
        }
        $('#tbl_utama_users').html(tr);
				$('#spinners_data').fadeOut('slow');
      }
    });
    load_pagination();
  }
</script>
<script>
  function load_pagination() {
    var limit_data_users = $('#limit_data_users').val();
    var limit = limit_per_page_custome(limit_data_users);
    var kata_kunci = $('#kata_kunci_filter').val();
    var hak_akses = $('#hak_akses_users').val();
    var tr = '';
    var td = TotalData('<?php echo base_url(); ?>user/total_users/?limit='+limit_per_page_custome(limit_data_users)+'&kata_kunci='+kata_kunci+'&hak_akses='+hak_akses+'');
    for (var i = 1; i <= td; i++) {
      tr += '<li page="'+i+'" id="'+i+'"><a class="update_id" href="#">'+i+'</a></li>';
    }
    $('#pagination').html(tr);
  }
</script>
 
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#urut_data_users').on('change', function(e) {
      e.preventDefault();
        var halaman = 1;
        var limit_data_users = $('#limit_data_users').val();
        var limit = limit_per_page_custome(limit_data_users);
        var kata_kunci = $('#kata_kunci_filter').val();
        var urut_data_users = $('#urut_data_users').val();
        var hak_akses = $('#hak_akses_users').val();
        load_data_users(halaman, limit, kata_kunci, urut_data_users, hak_akses);
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#pagination').on('click', '.update_id', function(e) {
		e.preventDefault();
		var id = $(this).closest('li').attr('page');
		var halaman = id;
    var limit_data_users = $('#limit_data_users').val();
    var limit = limit_per_page_custome(limit_data_users);
    var kata_kunci = $('#kata_kunci_filter').val();
    var urut_data_users = $('#urut_data_users').val();
    var hak_akses = $('#hak_akses_users').val();
    load_data_users(halaman, limit, kata_kunci, urut_data_users, hak_akses);
    
	});
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#limit_data_users').on('change', function(e) {
      e.preventDefault();
        var halaman = 1;
        var limit_data_users = $('#limit_data_users').val();
        var limit = limit_per_page_custome(limit_data_users);
        var kata_kunci = $('#kata_kunci_filter').val();
        var urut_data_users = $('#urut_data_users').val();
        var hak_akses = $('#hak_akses_users').val();
        load_data_users(halaman, limit, kata_kunci, urut_data_users, hak_akses);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#kata_kunci_filter').on('change', function(e) {
      e.preventDefault();
        var halaman = 1;
        var limit_data_users = $('#limit_data_users').val();
        var limit = limit_per_page_custome(limit_data_users);
        var kata_kunci = $('#kata_kunci_filter').val();
        var urut_data_users = $('#urut_data_users').val();
        var hak_akses = $('#hak_akses_users').val();
        load_data_users(halaman, limit, kata_kunci, urut_data_users, hak_akses);
    });
  });
</script>
