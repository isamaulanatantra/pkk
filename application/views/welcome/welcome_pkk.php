<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 style="display:none;">Rekap Catatan Data dan Kegiatan Warga</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>rekap_catatan_data_kegiatan_warga">Home</a></li>
          <li class="breadcrumb-item active">Rekap Catatan Data dan Kegiatan Warga</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">


  <div class="row" id="awal">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h3 class="card-title text-info">
            <i class="fa fa-th"></i> REKAP CATATAN DATA DAN KEGIATAN WARGA
            <div class="card-tools">
              <div class="input-group input-group-sm">
                <select class="form-control input-sm" id="tahun" name="tahun" style="width: 150px;">
                  <option value="">PILIH TAHUN</option>
                  <?php
                  for ($i = date(Y); $i >= 2015; $i--) {
                    echo '<option value="' . $i . '">' . $i . '</option>';
                  }
                  ?>
                </select>
              </div>
            </div>
          </h3>
        </div>
        <div class="card-body table-responsive p-0">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>NO.</th>
                <th>NAMA DESA</th>
                <th>JUMLAH KEPALA RUMAH TANGGA</th>
                <th>JUMLAH ANGGOTA KELUARGA</th>
                <th>JUMLAH ANGGOTA KELUARGA LAKI-LAKI</th>
                <th>JUMLAH ANGGOTA KELUARGA PEREMPUAN</th>
                <th>JUMLAH KARTU KELUARGA</th>
                <th><span class="text-success">JUMLAH INPUT ANGGOTA KELUARGA</span></th>
              </tr>
            </thead>
            <tbody id="tbl_utama">
            </tbody>
          </table>
        </div>
        <div class="card-footer">
          <ul class="pagination pagination-sm m-0 float-right" id="pagination" style="display:none;">
          </ul>
          <div class="overlay" id="spinners_tbl_data_input_rekap_data_umum" style="display:none;">
            <i class="fa fa-refresh fa-spin"></i>
          </div>
        </div>
      </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->


</section>

<script type="text/javascript">
  $(document).ready(function() {
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekapdesa/list_desa_welcome_pkk_kecamatan',
      success: function(html) {
        $('#tbl_utama').html(html);
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tahun').on('change', function(e) {
      e.preventDefault();
      $('#btnExport').hide();
      var tahun = $("#tahun").val();
      var tanggal = 25;
      for (var i = 1; i <= 34; i++) {
        window.setTimeout(list_1(i, tahun), 1000);
        window.setTimeout(list_2(i, tahun), 1000);
        window.setTimeout(list_3(i, tahun), 1000);
        window.setTimeout(list_4(i, tahun), 1000);
        window.setTimeout(list_5(i, tahun), 1000);
        window.setTimeout(list_6(i, tahun), 1000);
      }
      $('#btnExport').show();
    });
  });
</script>
<script type="text/javascript">
  function list_1(i, tahun) {
    $('#' + i + '_1').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_desa = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekapdesa/jumlah_kepala_keluarga/?id_desa=' + id_desa + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_desa + '_1').html(html);
      }
    });
  };

  function list_2(i, tahun) {
    $('#' + i + '_2').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_desa = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekapdesa/jumlah_anggota_keluarga/?id_desa=' + id_desa + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_desa + '_2').html(html);
      }
    });
  };

  function list_3(i, tahun) {
    $('#' + i + '_3').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_desa = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekapdesa/jumlah_anggota_keluarga_laki_laki/?id_desa=' + id_desa + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_desa + '_3').html(html);
      }
    });
  };

  function list_4(i, tahun) {
    $('#' + i + '_4').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_desa = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekapdesa/jumlah_anggota_keluarga_perempuan/?id_desa=' + id_desa + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_desa + '_4').html(html);
      }
    });
  };

  function list_5(i, tahun) {
    $('#' + i + '_5').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_desa = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekapdesa/jumlah_kartu_keluarga/?id_desa=' + id_desa + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_desa + '_5').html(html);
      }
    });
  };

  function list_6(i, tahun) {
    $('#' + i + '_6').html('<i class="fa fa-refresh fa-spin"></i>');
    var id_desa = $('#' + i).val();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>rekapdesa/jumlah_input_anggota_keluarga/?id_desa=' + id_desa + '&tahun=' + tahun + '',
      success: function(html) {
        $('#' + id_desa + '_6').html(html);
      }
    });
  };
</script>
<script type="text/javascript">
  /*$(document).ready(function() {
      $.ajax({
        dataType: "html",
        url: '<?php echo base_url(); ?>rekapdesa/welcome_pkk_desa',
        success: function(html) {
          $('#tbl_utama').html (html);
        }
      });
  });*/
</script>