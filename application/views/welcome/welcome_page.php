
        <!-- BEGIN SERVICE BOX -->   
        <div class="row service-box margin-bottom-40">
          <div class="col-md-6 col-sm-6">
            <?php if(!empty( $KolomKiriAtas )){ echo $KolomKiriAtas; } ?>
          </div>
          <div class="col-md-6 col-sm-6">
            <?php if(!empty( $KolomKananAtas )){ echo $KolomKananAtas; } ?>
          </div>
        </div>
        <!-- END SERVICE BOX -->

        <!-- BEGIN BLOCKQUOTE BLOCK -->   
        <div class="row quote-v1 margin-bottom-30">
          <div class="col-md-9">
            <span><?php if(!empty( $Header )){ echo $Header; } ?></span>
          </div>
          <div class="col-md-3 text-right">
            <a class="btn-transparent" href="https://<?php echo $web; ?>" target="_blank"><i class="fa fa-rocket margin-right-10"></i> </a>
          </div>
        </div>
        <!-- END BLOCKQUOTE BLOCK -->