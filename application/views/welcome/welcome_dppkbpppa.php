<style>

          .mb-4, .my-4 {
              margin-bottom: 1.5rem!important;
          }
          .border-left-primary {
              border-left: .25rem solid #4e73df!important;
          }

          .pb-2, .py-2 {
              padding-bottom: .5rem!important;
          }
          .pt-2, .py-2 {
              padding-top: .5rem!important;
          }
          .h-100 {
              height: 100%!important;
          }
          .shadow {
              -webkit-box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15)!important;
              box-shadow: 0 .15rem 1.75rem 0 rgba(58,59,69,.15)!important;
          }
          .card {
              position: relative;
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              -webkit-box-orient: vertical;
              -webkit-box-direction: normal;
              -ms-flex-direction: column;
              flex-direction: column;
              min-width: 0;
              word-wrap: break-word;
              background-color: #fff;
              background-clip: border-box;
              border: 1px solid #e3e6f0;
              border-radius: .35rem;
          }
          *, ::after, ::before {
              -webkit-box-sizing: border-box;
              box-sizing: border-box;
          }
          .card-body {
              -webkit-box-flex: 1;
              -ms-flex: 1 1 auto;
              flex: 1 1 auto;
              padding: 1.25rem;
          }

          *, ::after, ::before {
              -webkit-box-sizing: border-box;
              box-sizing: border-box;
          }
          .align-items-center {
              -webkit-box-align: center!important;
              -ms-flex-align: center!important;
              align-items: center!important;
          }

          .no-gutters {
              margin-right: 0;
              margin-left: 0;
          }
          .rows {
              display: -webkit-box;
              display: -ms-flexbox;
              display: flex;
              -ms-flex-wrap: wrap;
              flex-wrap: wrap;
              margin-right: -.75rem;
              margin-left: -.75rem;
          }
          *, ::after, ::before {
              -webkit-box-sizing: border-box;
              box-sizing: border-box;
          }
          .no-gutters>.col, .no-gutters>[class*=col-] {
              padding-right: 0;
              padding-left: 0;
          }

          .mr-2, .mx-2 {
              margin-right: .5rem!important;
          }
          .no-gutters>.col, .no-gutters>[class*=col-] {
              padding-right: 0;
              padding-left: 0;
          }

          .col-auto {
              -webkit-box-flex: 0;
              -ms-flex: 0 0 auto;
              flex: 0 0 auto;
              width: auto;
              max-width: 100%;
          }
          .text-xs {
              font-size: .7rem;
          }

          .text-primary {
              color: #4e73df!important;
          }
          .font-weight-bold {
              font-weight: 700!important;
          }
          .dropdown .dropdown-menu .dropdown-header, .sidebar .sidebar-heading, .text-uppercase {
              text-transform: uppercase!important;
          }
          .mb-1, .my-1 {
              margin-bottom: .25rem!important;
          }
          *, ::after, ::before {
              -webkit-box-sizing: border-box;
              box-sizing: border-box;
          }
          .text-gray-800 {
              color: #5a5c69!important;
          }

          .font-weight-bold {
              font-weight: 700!important;
              font-size: 12px;
          }
          .text-gray-300 {
              color: #dddfeb!important;
          }
          .fa-2x {
              font-size: 2em;
          }
          .border-left-success {
              border-left: .25rem solid #1cc88a!important;
          }
          .border-left-info {
              border-left: .25rem solid #36b9cc!important;
          }
</style>
    <div class="row">
    </div>
    <section class="admag-block">
      <div class="row">
        <div class="row">
          <div class="col-md-12">
            <h3 class="block-title"><span>Berita Terbaru</span></h3>
          </div>
        </div>
        <div class="col-md-12">
                  <?php if(!empty($galery_berita)){ echo $galery_berita; } ?>
        </div>
        <!-- End mid column -->
      </div>
    </section>
    <!-- BEGIN BLOCK 2 -->