
        <!-- BEGIN SERVICE BOX -->   
        <div class="row service-box margin-bottom-40">
          <div class="col-md-12">
							<div class="panel panel-info">
								<div class="panel panel-info">
									<div class="panel-heading">
										<h4 class="panel-title">Harga Kebutuhan Pokok</h4>
									</div>
								</div>
								<div class="form-group">
									<select class="form-control" id="id_pasar_home">
									</select>
								</div>
								<div class="form-group">
									<input class="form-control" id="tanggal_home" placeholder="Tanggal" type="text">
								</div>
								<div class="form-group">
									<button type="submit" class="btn purple-gradient btn-sm" id="tampilkan_by_filter">Tampilkan</button>
								</div>
								<div id="" style="margin-top:5px;">
                  <ul class="list-group" id="sembilan_harga_kebutuhan_pokok" style="height: 525px; display: inline-block; width: 100%; overflow: auto;">
                  </ul>
								</div>
								<div id="kebutuhan_publik">
								</div>
							</div>
          </div>
        </div>
        <!-- END SERVICE BOX -->

        
        <!-- END BLOCKQUOTE BLOCK -->
<script type="text/javascript">
  $(document).ready(function () {
  	$.ajax({
  		dataType: "json",
  		url: '<?php echo base_url('pasar/load_id_pasar_by_filter') ?>',
  		success: function(json) {
  			var trHTML = '';
  					trHTML += '<option value="0">Pilih Pasar</option>';
  			for (var i = 0; i < json.length; i++) {
  					trHTML += '<option value="'+json[i].id_pasar+'">'+json[i].nama_pasar+'</option>';
  				}
  			$('#id_pasar_home').append(trHTML); 
  		}
  	});
  });
</script>

<script>
  $(document).ready(function() {
    marquee_harga_kebutuhan_pokok();
  });
</script>

<script>
  function marquee_harga_kebutuhan_pokok() {
      $('#overlay_data_default').show();
      $('#marquee_harga_kebutuhan_pokok').html('');
      var temp = $("#temp").val();
      $.ajax({
        type: "POST",
        async: true,
        data: {
          temp:temp,
        },
        dataType: "html",
        url: '<?php echo base_url(); ?>pokok/marquee_kebutuhan_publik/',
        success: function(html) {
          $('#marquee_harga_kebutuhan_pokok').append(html);
          $('#overlay_data_default').fadeOut('slow');
        }
      });
  }
</script>

<script>
  $(document).ready(function() {
    load_sembilan_harga_kebutuhan_pokok();
  });
</script>

<script>
  function load_sembilan_harga_kebutuhan_pokok() {
      $('#overlay_data_default').show();
      $('#sembilan_harga_kebutuhan_pokok').html('');
      var temp = $("#temp").val();
      $.ajax({
        type: "POST",
        async: true,
        data: {
          temp:temp,
        },
        dataType: "html",
        url: '<?php echo base_url(); ?>pokok/kebutuhan_publik/',
        success: function(html) {
          $('#sembilan_harga_kebutuhan_pokok').append(html);
          $('#overlay_data_default').fadeOut('slow');
        }
      });
  }
</script>

<script>
  $(document).ready(function() {
    $('#tampilkan_by_filter').on('click', function(e) {
      e.preventDefault();
      load_kebutuhan_publik();
      load_mygrafik_bar();
    });
  });
</script>
<script>
  $(document).ready(function() {
      load_mygrafik_bar();
  });
</script>

<script>
  function load_kebutuhan_publik() {
      $('#overlay_data_default').show();
      $('#kebutuhan_publik').html('');
      var temp = $("#temp").val();
      var id_pasar = $("#id_pasar_home").val();
      var tanggal = $("#tanggal_home").val();
      if (id_pasar == '') {
          $('#id_pasar_home').css('background-color', '#DFB5B4');
        } else {
          $('#id_pasar_home').removeAttr('style');
        }
      if (tanggal == '') {
          $('#tanggal_home').css('background-color', '#DFB5B4');
        } else {
          $('#tanggal_home').removeAttr('style');
        }
      $.ajax({
        type: "POST",
        async: true,
        data: {
          temp:temp,
          id_pasar:id_pasar,
          tanggal:tanggal,
        },
        dataType: "html",
        url: '<?php echo base_url(); ?>pokok/kebutuhan_publik/',
        success: function(html) {
          $('#sembilan_harga_kebutuhan_pokok').hide();
          $('#mygrafik_bar1').hide();
          $('#kebutuhan_publik').append(html);
  				$('#mygrafik_bar').slow();
          $('#overlay_data_default').fadeOut('slow');
        }
      });
  }
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_home, #sampai_tanggal_1').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>

<script>
  function load_mygrafik_bar() {
    $('#overlay_data_default').show();
    var id_pasar_home = $("#id_pasar_home").val();
    var tanggal_home = $("#tanggal_home").val();
    if (id_pasar_home == '') {
        $('#id_pasar_home').css('background-color', '#DFB5B4');
      } else {
        $('#id_pasar_home').removeAttr('style');
      }
    if (tanggal_home == '') {
        $('#tanggal_home').css('background-color', '#DFB5B4');
      } else {
        $('#tanggal_home').removeAttr('style');
      }
  	$.ajax({
  		type: "POST",
  		async: true, 
  		data: {
  				id_pasar_home:id_pasar_home,
  				tanggal_home:tanggal_home,
  			  }, 
  		dataType: "html",
  		url: '<?php echo base_url(); ?>test/grafik_harga_kebutuhan_pokok',   
  			success: function(response) { 
  				$('#mygrafik_bar').append(response);
  				$('#overlay_data_default').fadeOut( "slow" ); 
  			}
  		});
  }
</script>