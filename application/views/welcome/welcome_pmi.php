
            <section class="section">
              <div class="container">
                <div class="columns">
                  <div class="column">
                    <a href="<?php echo base_url(); ?>" target="_blank" class="title-pmi-bg">
                      <h3 class="title is-spaced has-text-white">Donor <br> Darah</h3>
                      <div class="right-content"><img src="/assets/img/donordarah.png" alt="donor-darah-img" width="170" height="150"></div>
                    </a>
                    <div class="columns is-multiline is-gapless features">
                      <a href="<?php echo base_url(); ?>" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-Stok-darah"></i></div>
                            <div class="feature-right"><span>Stock Darah</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="<?php echo base_url(); ?>informasi_kebutuhan_darah" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-prosedur-permintaan-darah"></i></div>
                            <div class="feature-right"><span>Informasi Kebutuhan Darah</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="<?php echo base_url(); ?>" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-layanan-ambulance"></i></div>
                            <div class="feature-right"><span>Jadwal donor Darah/Mobil Unit Instansi</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="<?php echo base_url(); ?>" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-donasi-umum"></i></div>
                            <div class="feature-right"><span>Pendonor</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="<?php echo base_url(); ?>informasi_prosedur_permintaan_darah" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-prosedur-permintaan-darah"></i></div>
                            <div class="feature-right"><span>Prosedur Permintaan Darah</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="<?php echo base_url(); ?>informasi_prosedur_donor" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-Prosedur-donor-darah"></i></div>
                            <div class="feature-right"><span>Prosedur Donor Darah</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="column">
                    <a href="<?php echo base_url(); ?>donasi" target="_blank" class="title-pmi-bg">
                      <h3 class="title is-spaced has-text-white">Donasi</h3>
                      <div class="right-content"><img src="/assets/img/donasi.png" alt="donasi-img" width="150" height="150"></div>
                    </a>
                    <div class="columns is-multiline is-gapless features">
                      <a href="<?php echo base_url(); ?>layanan" class="column is-12 nuxt-link-exact-active nuxt-link-active">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-tanggap-bencana"></i></div>
                            <div class="feature-right"><span>Layanan Bencana/Konflik</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="<?php echo base_url(); ?>Bulan_dana_pmi" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-bulan-dana-pmi"></i></div>
                            <div class="feature-right"><span>Bulan Dana PMI</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="/" class="column is-12 nuxt-link-exact-active nuxt-link-active">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-donasi-khusus"></i></div>
                            <div class="feature-right"><span>Donasi  Khusus</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="<?php echo base_url(); ?>donasi" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-donasi-umum"></i></div>
                            <div class="feature-right"><span>Donasi Umum</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="<?php echo base_url(); ?>donasi" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-donasi-barang"></i></div>
                            <div class="feature-right"><span>Donasi Barang</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                  <div class="column">
                    <a href="<?php echo base_url(); ?>dashboard" target="_blank" class="title-pmi-bg">
                      <h3 class="title is-spaced has-text-white">Relawan</h3>
                      <div class="right-content"><img src="/assets/img/relawan.png" alt="relawan-img" width="150" height="150"></div>
                    </a>
                    <div class="columns is-multiline is-gapless features">
                      <a href="<?php echo base_url(); ?>" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-pmr-dan-relawan"></i></div>
                            <div class="feature-right"><span>Palang Merah Remaja</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="<?php echo base_url(); ?>" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-form-kebutuhan-darah"></i></div>
                            <div class="feature-right"><span>Tenaga Sukarela (TSR)</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="<?php echo base_url(); ?>" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-form-kebutuhan-darah"></i></div>
                            <div class="feature-right"><span>Korps Sukarela (KSR)</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                      <a href="<?php echo base_url(); ?>" class="column is-12">
                        <div class="feature-box">
                          <div class="feature-content">
                            <div class="feature-left"><i class="pmi icon-rekruitmen"></i></div>
                            <div class="feature-right"><span>Rekrutmen</span><span><i class="icon-chevron-right"></i></span></div>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="section">
              <div class="container">
                <div class="columns is-variable is-4">
                  <div class="column has-margin-b-1">
                    <div class="title-pmi has-margin-b-1">
                      <div class="decor">
                        <h3 class="title is-spaced">Pemberitaan <?php if(!empty( $keterangan )){ echo $keterangan; } ?></h3>
                        <div class="border-pmi"><span></span><span></span><span></span><span></span></div>
                      </div>
                    </div>
                    <div class="columns is-multiline is-fluid-mobile">
                      <?php if(!empty($highlight)){ echo $highlight; } ?>
                    </div>
                  </div>
                  <div class="column is-3 is-hidden-mobile">
                      <?php if(!empty($terbarukan)){ echo $terbarukan; } ?>
                  </div>
                </div>
              </div>
            </section>
            
            <section class="section">
              <div class="container">
                <div class="columns is-multiline is-mobile is-centered">
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="<?php echo base_url(); ?>" target="_blank" title="jaya raya"><img src="<?php echo base_url(); ?>media/logo_wonosobo.png" alt="Pemerintah Kabupaten Wonosobo"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="https://bnpb.go.id/" target="_blank" title="bnpb"><img src="<?php echo base_url(); ?>media/bnpb.png" alt="bnpb"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="https://bpbd.wonosobokab.go.id/" target="_blank" title="bpdb"><img src="<?php echo base_url(); ?>media/bpdb.png" alt="bpdb"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="https://www.bmkg.go.id/" target="_blank" title="bmkg"><img src="<?php echo base_url(); ?>media/bmkg.png" alt="bmkg"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="http://pmi.or.id/" target="_blank" title="pmi"><img src="<?php echo base_url(); ?>media/pmi_kecil.png" alt="pmi"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="https://www.ifrc.org/" target="_blank" title="ifrc"><img src="<?php echo base_url(); ?>media/ifrc.png" alt="ifrc"></a></div>
                  <div class="column has-text-centered is-one-quarter-mobile"><a href="https://www.icrc.org/" target="_blank" title="icrc"><img src="<?php echo base_url(); ?>media/icrc.png" alt="icrc"></a></div>
                </div>
              </div>
            </section>