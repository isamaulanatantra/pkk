
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!--<h1>Balita</h1>-->
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>register">Home</a></li>
              <li class="breadcrumb-item active">Register</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
										<div class="box">
											<div class="box-header">
												<h3 class="box-title">&nbsp;</h3>
												<div class="box-tools">
													<div class="input-group input-group-sm">
                            <input name="tabel" id="tabel" value="register" type="hidden" value="">
                            <input name="page" id="page" value="1" type="hidden" value="">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="999999999">Semua</option>
															<option value="1">1 Per-Halaman</option>
															<option value="10">10 Per-Halaman</option>
															<option value="20">20 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="register.created_time">Tanggal</option>
															<option value="register.nama">Nama</option>
															<option value="register.alamat">alamat</option>
															<option value="register.nik">NIK</option>
															<option value="register.nomot_telp">No. HP</option>
															<option value="register.keterangan">Keperluan</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_register"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="box-body table-responsive p-0 no-padding">
												<table class="table table-bordered table-hover">
													<thead>
                            <tr>
                              <th>No</th>
                              <th>NIK</th>
                              <th>Nama</th>
                              <!--<th>Email</th> -->
                              <th>No. HP</th> 
                              <th>Alamat</th>
                              <th>Keperluan</th>
                              <th>Proses</th>
                            </tr>
													</thead>
													<tbody id="tbl_data_register">
													</tbody>
												</table>
											</div>
											<div class="box-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_register" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>register/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    load_data(tabel);
  });
</script>
<script>
  function AfterSavedBalita() {
    $('#id_register, #nik, #nama_register, #jenis_kelamin, #tempat_lahir, #tanggal_lahir, #berkebutuhan_khusus, #berkebutuhan_khusus_fisik ').val('');
    $('#tbl_attachment_register').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_register').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'temp', 'nik', 'nama_register' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["nik"] = $("#nik").val();
      parameter["nama_register"] = $("#nama_register").val();
      parameter["jenis_kelamin"] = $("#jenis_kelamin").val();
      parameter["tempat_lahir"] = $("#tempat_lahir").val();
      parameter["tanggal_lahir"] = $("#tanggal_lahir").val();
      parameter["berkebutuhan_khusus"] = $("#berkebutuhan_khusus").val();
      parameter["berkebutuhan_khusus_fisik"] = $("#berkebutuhan_khusus_fisik").val();
      var url = '<?php echo base_url(); ?>register/simpan_register';
      
      var parameterRv = [ 'temp', 'nik', 'nama_register' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedBalita();
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_register').on('click', '.update_id_register', function() {
    $('#mode').val('edit');
    $('#simpan_register').hide();
    $('#update_register').show();
    $('#overlay_data_register').show();
    $('#tbl_data_register').html('');
    var id_register = $(this).closest('tr').attr('id_register');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_register');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_register').val(id_register);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_register:id_register
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>register/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_register').val(json[i].id_register);
            $('#nik').val(json[i].nik);
            $('#nama_register').val(json[i].nama_register);
            $('#jenis_kelamin').val(json[i].jenis_kelamin);
            $('#tempat_lahir').val(json[i].tempat_lahir);
            $('#tanggal_lahir').val(json[i].tanggal_lahir);
            $('#berkebutuhan_khusus').val(json[i].berkebutuhan_khusus);
            $('#berkebutuhan_khusus_fisik').val(json[i].berkebutuhan_khusus_fisik);
          }
        }
      });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_register').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'id_register', 'temp', 'nama_register' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_register"] = $("#id_register").val();
      parameter["temp"] = $("#temp").val();
      parameter["nik"] = $("#nik").val();
      parameter["nama_register"] = $("#nama_register").val();
      parameter["jenis_kelamin"] = $("#jenis_kelamin").val();
      parameter["tempat_lahir"] = $("#tempat_lahir").val();
      parameter["tanggal_lahir"] = $("#tanggal_lahir").val();
      parameter["berkebutuhan_khusus"] = $("#berkebutuhan_khusus").val();
      parameter["berkebutuhan_khusus_fisik"] = $("#berkebutuhan_khusus_fisik").val();
      var url = '<?php echo base_url(); ?>register/update_register';
      
      var parameterRv = [ 'id_register', 'temp', 'nama_register' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_register').on('click', '#del_ajax_register', function() {
    var id_register = $(this).closest('tr').attr('id_register');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_register"] = id_register;
        var url = '<?php echo base_url(); ?>register/hapus/';
        HapusData(parameter, url);
        $('[id_register='+id_register+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
});
</script>
