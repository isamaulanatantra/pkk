
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>DATA Register</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Data Register</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header d-flex p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_1" data-toggle="tab" id="klik_tab_input">Form</a></li>
                </ul>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
                  <div class="tab-pane active" id="tab_1">
										<h3 id="judul_formulir">UPDATE DATA DASAR</h3>
													
                <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=register" enctype="multipart/form-data">
                  
                    <div class="form-group" style="display:none;">
                      <label for="id_register">id_register</label>
                      <input class="form-control" name="id_register" id="id_register" placeholder="id_register" type="text">
                    </div>
                    <div class="form-group">
                      <label for="nik">NIK</label>
                      <input class="form-control" name="nik" id="nik" placeholder="NIK" type="text">
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input class="form-control" name="nama" id="nama" placeholder="Nama" type="text">
                    </div>
                    <div class="form-group">
                      <label for="email">Email</label>
                      <input class="form-control" name="email" id="email" placeholder="email" type="text">
                    </div>
                    <div class="form-group">
                      <label for="nomot_telp">No. Telp</label>
                      <input class="form-control" name="nomot_telp" id="nomot_telp" placeholder="No. Telp" type="text">
                    </div>
                    <div class="form-group">
                      <label for="alamat">Alamat</label>
                      <input class="form-control" name="alamat" id="alamat" placeholder="Alamat" type="text">
                    </div>
                    <div class="form-group">
                      <label for="keterangan">Keperluan</label>
                      <input class="form-control" name="keteragan" id="keterangan" placeholder="Keperluan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="domain">domain</label>
                      <input class="form-control" name="domain" id="domain" placeholder="domain" type="text">
                    </div>
                    <div class="form-group">
                      <label for="id_kecamatan">id_kecamatan</label>
                      <input class="form-control" name="id_kecamatan" id="id_kecamatan" placeholder="id_kecamatan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="id_desa">id_desa</label>
                      <input class="form-control" name="id_desa" id="id_desa" placeholder="id_desa" type="text">
                    </div>
                    <button type="submit" class="btn btn-primary" id="simpan_buat_user_register">BUAT USER REGISTER</button>
                </form>
										<!-- /.tab-pane -->
									</div>
                </div>
                <!-- /.tab-content -->
              </div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
		
<!----------------------->
<script>
  function load_data_buat_user_register() {
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman:'1',
        id_register:'<?php echo $this->uri->segment(3); ?>'
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>register/json_all_buat_user_register/',
      success: function(json) {
        var tr = '';
        for (var i = 0; i < json.length; i++) {
          $("#id_register").val(json[i].id_register);
          $("#domain").val(json[i].domain);
          $("#nik").val(json[i].nik);
          $("#nama").val(json[i].nama);
          $("#alamat").val(json[i].alamat);
          $("#nomot_telp").val(json[i].nomot_telp);
          $("#email").val(json[i].email);
          $("#keterangan").val(json[i].keterangan);
          $("#id_kecamatan").val(json[i].id_kecamatan);
          $("#id_desa").val(json[i].id_desa);
        }
      }
    });
  }
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    load_data_buat_user_register();
  });
</script>
<!----------------------->
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_buat_user_register').on('click', function(e) {
      e.preventDefault();
      
      var parameter = [
                       
                       'alamat',
                       'nomot_telp',
                       'email',
                       'nama',
                       'nik',
                       'keterangan',
                       'domain',
                       'id_kecamatan',
                       'id_desa',
                      ];
			InputValid(parameter);
      
      var parameter = {}
      
      parameter["alamat"] = $("#alamat").val();
      parameter["nomot_telp"] = $("#nomot_telp").val();
      parameter["email"] = $("#email").val();
      parameter["nik"] = $("#nik").val();
      parameter["nama"] = $("#nama").val();
      parameter["keterangan"] = $("#keterangan").val();
      parameter["domain"] = $("#domain").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      var url = '<?php echo base_url(); ?>register/simpan_buat_user_register';
      var parameterRv = [
                       
                       'alamat',
                       'nomot_telp',
                       'email',
                       'nik',
                       'nama',
                       'keterangan',
                       'domain',
                       'id_desa',
                       'id_kecamatan',
                      ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        // window.location = "<?php echo base_url(); ?>register/data";
      }
    });
  });
</script>