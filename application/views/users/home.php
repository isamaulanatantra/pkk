<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active">
        <a href="#tab_1" data-toggle="tab" id="klik_tab_input">GENERAL</a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="row" id="div_form_input">
          <div class="col-md-6">
            <div class="box box-danger box-solid">
              <div class="box-header">
                <h3 class="box-title">FORMULIR INPUT</h3>
              </div>
              <div class="box-body">
                <form role="form" id="form_users" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=users" enctype="multipart/form-data">
                  <div class="box-body">
                    <input id="id_users" name="id_users" type="hidden">
                    <input id="temp" name="temp" type="hidden">
                    <div class="form-group">
                      <label for="user_name">User Name</label>
                      <input class="form-control" id="user_name" name="user_name" value="" placeholder="User Name" type="text">
                    </div>
                    <div class="form-group">
                      <label for="password">Password</label>
                      <input class="form-control" id="password" name="password" value="" placeholder="Password" type="text">
                    </div>
                    <div class="form-group">
                      <label for="nama">Nama</label>
                      <input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
                    </div>
                    <div class="form-group">
                      <label for="nip">NIP</label>
                      <input class="form-control" id="nip" name="nip" value="" placeholder="NIP" type="text">
                    </div>
                    <div class="form-group">
                      <label for="jabatan">Jabatan</label>
                      <input class="form-control" id="jabatan" name="jabatan" value="" placeholder="Jabatan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="golongan">Golongan</label>
                      <input class="form-control" id="golongan" name="golongan" value="" placeholder="Golongan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="pangkat">Pangkat</label>
                      <input class="form-control" id="pangkat" name="pangkat" value="" placeholder="Pangkat" type="text">
                    </div>
                    <div class="form-group">
                      <label for="hak_akses">Hak Akses</label>
                      <select class="form-control" id="hak_akses" name="hak_akses">
                        <option value="web_skpd">User Website OPD </option>
                        <option value="admin_web_skpd">Admin Website OPD </option>
                      </select>
                    </div>
                    <div class="alert alert-info alert-dismissable">
                      <div class="form-group">
                        <label for="remake">Keterangan Lampiran Formulir 1 21</label>
                        <input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran Formulir 1 21" type="text">
                      </div>
                      <div class="form-group">
                        <label for="myfile">File Lampiran Users</label>
                        <input type="file" size="60" name="myfile" id="users_baru">
                      </div>
                      <div id="progress_upload_lampiran_users">
                        <div id="bar_progress_upload_lampiran_users"></div>
                        <div id="percent_progress_upload_lampiran_users">0%</div>
                      </div>
                      <div id="message_progress_upload_lampiran_users"></div>
                    </div>
                    <div class="alert alert-info alert-dismissable">
                      <h3 class="box-title">Data Lampiran </h3>
                      <table class="table table-bordered">
                        <tr>
                          <th>No</th>
                          <th>Keterangan</th>
                          <th>Download</th>
                          <th>Hapus</th>
                        </tr>
                        <tbody id="tbl_lampiran_users"></tbody>
                      </table>
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="simpan_users">SIMPAN</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="row" id="e_div_form_input" style="display:none;">
          <div class="col-md-6">
            <div class="box box-danger box-solid">
              <div class="box-header">
                <h3 class="box-title">FORMULIR EDIT</h3>
              </div>
              <div class="box-body">
                <form role="form" id="e_form_users" method="post" action="<?php echo base_url(); ?>attachment/e_upload/?table_name=users" enctype="multipart/form-data">
                  <div class="box-body">
                    <input id="e_id_users" name="id" type="hidden">
                    <div class="form-group">
                      <label for="e_user_name">User Name</label>
                      <input class="form-control" id="e_user_name" name="user_name" value="" placeholder="User Name" type="text">
                    </div>
                    <div class="form-group">
                      <label for="e_nama">Nama</label>
                      <input class="form-control" id="e_nama" name="nama" value="" placeholder="Nama" type="text">
                    </div>
                    <div class="form-group">
                      <label for="e_nip">NIP</label>
                      <input class="form-control" id="e_nip" name="nip" value="" placeholder="NIP" type="text">
                    </div>
                    <div class="form-group">
                      <label for="e_jabatan">Jabatan</label>
                      <input class="form-control" id="e_jabatan" name="jabatan" value="" placeholder="Jabatan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="e_golongan">Golongan</label>
                      <input class="form-control" id="e_golongan" name="golongan" value="" placeholder="Golongan" type="text">
                    </div>
                    <div class="form-group">
                      <label for="e_pangkat">Pangkat</label>
                      <input class="form-control" id="e_pangkat" name="pangkat" value="" placeholder="Pangkat" type="text">
                    </div>
                    <div class="form-group">
                      <label for="e_id_propinsi">Nama Propinsi</label>
                    </div>
                    <div class="input-group">
                      <select class="form-control" id="e_id_propinsi" name="id_propinsi" >
                      </select>
                      <span class="input-group-addon" id="e_ganti_propinsi" style="display:none;"><a href="" id="e_klik_ganti_provinsi"><i class="fa fa-check"></i> Ganti</a></span>
                    </div>
                    <div class="form-group">
                      <label for="e_id_kabupaten">Nama Kabupaten</label>
                      <select class="form-control" id="e_id_kabupaten" name="id_kabupaten" >
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="e_id_kecamatan">Nama Kecamatan</label>
                      <select class="form-control" id="e_id_kecamatan" name="id_kecamatan" >
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="e_id_desa">Nama Desa</label>
                      <select class="form-control" id="e_id_desa" name="id_desa" >
                      </select>
                    </div>
                    <div class="form-group">
                      <label for="e_hak_akses">Hak Akses</label>
                      <select class="form-control" id="e_hak_akses" name="hak_akses">
                        <option value="">Pilih Salah Satu</option>
                        <option value="user_fo">FO</option>
                        <option value="user_bo">BO</option>
                        <option value="user_tu">Tata Usaha</option>
                        <option value="user_kepala">Kepala SKPD</option>
                        <option value="user_kecamatan">User Kecamatan</option>
                        <option value="user_desa">User Desa</option>
                        <option value="root">Super User</option>
                      </select>
                    </div>
                    <div class="alert alert-info alert-dismissable">
                      <div class="form-group">
                        <label for="e_remake">Keterangan Lampiran Formulir 1 21</label>
                        <input class="form-control" id="e_remake" name="remake" placeholder="Keterangan Lampiran Formulir 1 21" type="text">
                      </div>
                      <div class="form-group">
                        <label for="myfile">File Lampiran Users</label>
                        <input type="file" size="60" name="myfile" id="e_users_baru">
                      </div>
                      <div id="e_progress_upload_lampiran_users">
                        <div id="e_bar_progress_upload_lampiran_users"></div>
                        <div id="e_percent_progress_upload_lampiran_users">0%</div>
                      </div>
                      <div id="e_message_progress_upload_lampiran_users"></div>
                    </div>
                    <div class="alert alert-info alert-dismissable">
                      <h3 class="box-title">Data Lampiran </h3>
                      <table class="table table-bordered">
                        <tr>
                          <th>No</th>
                          <th>Keterangan</th>
                          <th>Download</th>
                          <th>Hapus</th>
                        </tr>
                        <tbody id="e_tbl_lampiran_users"></tbody>
                      </table>
                    </div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="update_data_users">SIMPAN</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="e_overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="row" id="div_data_default">
          <div class="col-md-12">
            <div class="box box-danger box-solid">
              <div class="box-header">
                <h3 class="box-title">DATA</h3>
              </div>
              <div class="box-body">
                <table class="table table-bordered">
                  <tr>
                    <th>NO</th>        
                    <th>User Name</th>
                    <th>Nama</th>
                    <th>Hak</th>
                    <th>SKPD</th>
                    <th>PROSES</th>
                  </tr>
                  <tbody id="tbl_utama_users"></tbody>
                </table>
                <div class="box-footer clearfix">
                  </ul>
                </div>
              </div>
              <div class="overlay" id="overlay_data_default" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
  function load_default(halaman) {
    $('#overlay_data_default').show();
    $('#tbl_utama_users').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>users/json_all_users/?halaman=' + halaman + '/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * 10);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
          tr += '<tr id_users="' + json[i].id_users + '" id="' + json[i].id_users + '" >';
          tr += '<td valign="top">' + (start) + '</td>';
          tr += '<td valign="top">' + json[i].user_name + '</td>';
          tr += '<td valign="top">' + json[i].nama + '</td>';
          tr += '<td valign="top">' + json[i].hak_akses + '</td>';
          tr += '<td valign="top">' + json[i].skpd_nama + '</td>';
          tr += '<td valign="top">';
          // tr += '<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> ';
          tr += '</td>';
          tr += '</tr>';
        }
        $('#tbl_utama_users').append(tr);
        $('#overlay_data_default').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  function reset() {
    $('#toggleCSS').attr('href', '<?php echo base_url(); ?>css/alertify/alertify.default.css');
    alertify.set({
      labels: {
        ok: 'OK',
        cancel: 'Cancel'
      },
      delay: 5000,
      buttonReverse: false,
      buttonFocus: 'ok'
    });
  }
  //----------------------------------------------------------------------------------------------
  $('#tbl_search_users, #tbl_utama_users').on('click', '#del_ajax', function() {
    reset();
    var id = $(this).closest('tr').attr('id_users');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        $.ajax({
          dataType: 'json',
          url: '<?php echo base_url(); ?>users/hapus_users/?id=' + id + '',
          success: function(response) {
            if (response.errors == 'Yes') {
              alertify.alert('Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya');
            } else {
              $('[id_users=' + id + ']').remove();
              alertify.alert('Data berhasil dihapus');
            }
          }
        });
      } else {
        alertify.alert('Hapus data dibatalkan');
      }
    });
  });
</script>
<script type="text/javascript">
    $('#simpan_users').on('click', function(e) {
      e.preventDefault();
      $('#simpan_users').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
      //------------------------------
      var user_name = $("#user_name").val();
      var password = $("#password").val();
      var hak_akses = $("#hak_akses").val();
      var nip = $("#nip").val();
      var nama = $("#nama").val();
      var jabatan = $("#jabatan").val();
      var golongan = $("#golongan").val();
      var pangkat = $("#pangkat").val();
      var id_skpd = $("#id_skpd").val();
      var id_desa = $("#id_desa").val();
      var id_kecamatan = $("#id_kecamatan").val();
      var id_kabupaten = $("#id_kabupaten").val();
      var id_propinsi = $("#id_propinsi").val();
      var temp = $("#temp").val();
      			
      //------------------------------------
      if (user_name == '') {
        $('#user_name').css('background-color', '#DFB5B4');
      } else {
        $('#user_name').removeAttr('style');
      }
      if (password == '') {
        $('#password').css('background-color', '#DFB5B4');
      } else {
        $('#password').removeAttr('style');
      }
      if (hak_akses == '') {
        $('#hak_akses').css('background-color', '#DFB5B4');
      } else {
        $('#hak_akses').removeAttr('style');
      }
      if (nama == '') {
        $('#nama').css('background-color', '#DFB5B4');
      } else {
        $('#nama').removeAttr('style');
      }
      			
      $.ajax({
        type: 'POST',
        async: true,
        data: {
					
          user_name:user_name,
          password:password,
          hak_akses:hak_akses,
          nip:nip,
          nama:nama,
          jabatan:jabatan,
          golongan:golongan,
          pangkat:pangkat,
          temp:temp
          					
        },
        dataType: 'text',
        url: '<?php echo base_url(); ?>users/simpan_users/',
        success: function(json) {
          if (json == '0') {
            $('#simpan_users').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut();
            alert('gagal');
            $('html, body').animate({
              scrollTop: $('#awal').offset().top
            }, 1000);
          } else {
            alertify.alert('Data berhasil disimpan');
            $('#tbl_lampiran_users').html('');
            $('#message_progress_upload_lampiran_users').html('');
            var halaman = 1;
            load_default(halaman);
            var aaa = Math.random();
            $('#temp').val(aaa);
            $('#div_form_input form').trigger('reset');
            $('#simpan_users').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut('slow');
          }
        }
      });
    });
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    var halaman = 1;
    load_default(halaman);
  });
</script>