
        <?php
        $web=$this->uut->namadomain(base_url());
        ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>USERS</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">USERS</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
<section class="content" id="awal">
  <div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header d-flex p-0">
					<ul class="nav nav-pills ml-auto p-2">
						<li class="nav-item"><a class="nav-link active" href="#tab_form_users" data-toggle="tab" id="klik_tab_input">Form</a></li>
						<li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab" id="klik_tab_data_users">Data</a></li>
					</ul>
				</div>
				<div class="card-body">
					<div class="tab-content">
						<div class="tab-pane active" id="tab_form_users">
					
							<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=users" enctype="multipart/form-data">
									<h3 id="judul_formulir">FORMULIR INPUT</h3>
									<div class="row">
										<div class="col-sm-12 col-md-6">

                      <input name="page" id="page" value="1" type="hidden" value="">
                      <input name="tabel" id="tabel" value="users" type="hidden" value="">										
											<div class="form-group" style="display:none;">
												<label for="temp">temp</label>
												<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="mode">mode</label>
												<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="id_users">id_users</label>
												<input class="form-control" id="id_users" name="id" value="" placeholder="id_users" type="text">
											</div>
											<div class="form-group">
												<label for="user_name">User Name</label>
												<input class="form-control" id="user_name" name="user_name" value="" placeholder="user_name" type="text">
											</div>
											<div class="form-group">
												<label for="password">Password</label>
												<input class="form-control" id="password" name="password" value="" placeholder="password" type="text">
											</div>
											<div class="form-group">
												<label for="hak_akses">Hak Akses</label>
												<select class="form-control" id="hak_akses" name="hak_akses" >
                        <?php 
                        if($web=='diskominfo.wonosobokab.go.id'){
                          echo'
												<option value="web_skpd">web_skpd</option>
												<option value="admin_web_kecamatan">admin_web_kecamatan</option>
												<option value="web_pkk">web_pkk</option>
												<option value="web_desa">web_desa</option>
												<option value="web_pkk_kecamatan">web_pkk_kecamatan</option>
												<option value="web_pkk_desa">web_pkk_desa</option>
                          ';
                        }
                        elseif($web=='dinkes.wonosobokab.go.id'){
                          echo'
												<option value="admin_web_dinkes">admin_web_dinkes</option>
												<option value="web_skpd">web_skpd</option>
                          ';
                        }else{
                          echo'
												<option value="web_skpd">web_skpd</option>
                          ';
                        }
                        ?>
												</select>
											</div>
											<div class="form-group" style="display:none;">
												<label for="nip">nip</label>
												<input class="form-control" id="nip" name="nip" value="-" placeholder="nip" type="text">
											</div>
											<div class="form-group">
												<label for="nama">Nama</label>
												<input class="form-control" id="nama" name="nama" value="" placeholder="nama" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="jabatan">jabatan</label>
												<input class="form-control" id="jabatan" name="jabatan" value="-" placeholder="jabatan" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="golongan">golongan</label>
												<input class="form-control" id="golongan" name="golongan" value="-" placeholder="golongan" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="pangkat">pangkat</label>
												<input class="form-control" id="pangkat" name="pangkat" value="-" placeholder="pangkat" type="text">
											</div>
											<div class="form-group">
												<label for="id_skpd">id_skpd</label>
												<select class="form-control" id="id_skpd" name="id_skpd" >
												</select>
											</div>
											<div class="form-group">
												<label for="id_kecamatan">id_kecamatan</label>
												<select class="form-control" id="id_kecamatan" name="id_kecamatan" >
												</select>
											</div>
											<div class="form-group">
												<label for="id_desa">id_desa</label>
												<select class="form-control" id="id_desa" name="id_desa" >
													<option value="0">Pilih Desa</option>
												</select>
											</div>
											
										</div>
									</div>
									<button type="submit" class="btn btn-primary" id="simpan_users">SIMPAN</button>
									<button type="submit" class="btn btn-primary" id="update_users" style="display:none;">UPDATE</button>
							</form>
							<div class="overlay" id="overlay_form_input" style="display:none;">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
							
						</div>
						<div class="tab-pane" id="tab_2">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
															<option value="999999999">Semua</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="users.nama">Nama</option>
															<option value="users.user_name">User name</option>
															<option value="users.created_time">Tanggal</option>
															<option value="users.hak_akses">Hak Akses</option>
															<option value="users.id_kecamatan">Kecamatan</option>
															<option value="users.id_desa">Desa</option>
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_users"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
                          <thead>
                            <tr>
                              <th>NO</th>
                              <th>Tanggal</th>
                              <th>OPD</th>
                              <th>Kecamatan</th>
                              <th>Desa</th>
                              <th>Username</th>
                              <th>Hakakses</th>
                              <!--<th>Nama</th>-->
                              <th>Proses</th>
                            </tr>
                          </thead>
													<tbody id="tbl_data_users">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_users" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!----------------------->
<script>
  function load_opd_domain() {
    $('#id_kecamatan').html('');
    $('#option_kecamatan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>user/option_kecamatan/',
      success: function(html) {
        $('#option_kecamatan').html('<option value="0">Pilih Kecamatan</option>'+html+'');
        $('#id_kecamatan').html('<option value="0">Pilih Kecamatan</option>'+html+'');
      }
    });
  }
</script>

<script>
  function load_id_skpd() {
    $('#id_skpd').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>user/option_skpd/',
      success: function(html) {
        $('#id_skpd').html('<option value="0">Pilih SKPD</option>'+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var id_kecamatan = $('#id_kecamatan').val();
      load_option_desa(id_kecamatan);
    });
  });
</script>

<script>
  function load_option_desa(id_kecamatan) {
    $('#id_desa').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html(' '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_id_skpd();
  load_opd_domain();
});
</script>

<script>
  function AfterSavedUsers() {
    $('#id_users, #user_name, #password, #hak_akses, #nip, #nama, #jabatan, #golongan, #pangkat, #id_skpd, #id_desa, #id_kecamatan').val('');
    load_id_skpd();
    load_opd_domain();
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
 
<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_users').show();
    $('#update_users').hide();
    $('#id_users, #user_name, #password, #hak_akses, #nip, #nama, #jabatan, #golongan, #pangkat, #id_skpd, #id_desa, #id_kecamatan').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_users').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'user_name', 'password', 'hak_akses', 'nip', 'nama', 'jabatan', 'golongan', 'pangkat', 'id_skpd', 'id_desa', 'id_kecamatan' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["user_name"] = $("#user_name").val();
      parameter["password"] = $("#password").val();
      parameter["hak_akses"] = $("#hak_akses").val();
      parameter["nip"] = $("#nip").val();
      parameter["nama"] = $("#nama").val();
      parameter["jabatan"] = $("#jabatan").val();
      parameter["golongan"] = $("#golongan").val();
      parameter["pangkat"] = $("#pangkat").val();
      parameter["id_skpd"] = $("#id_skpd").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      var url = '<?php echo base_url(); ?>user/simpan_users';
      var parameterRv = [ 'user_name', 'password', 'hak_akses', 'nip', 'nama', 'jabatan', 'golongan', 'pangkat', 'id_skpd', 'id_desa', 'id_kecamatan' ];
			var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedUsers();
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>user/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>user/json_all_'+tabel+'/',
      success: function(html) {
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_users').on('click', '.update_id_users', function() {
    $('#mode').val('edit');
    $('#simpan_users').hide();
    $('#update_users').show();
    $('#overlay_data_users').show();
    $('#tbl_data_users').html('');
    var id_users = $(this).closest('tr').attr('id_users');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_users');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_users').val(id_users);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_users:id_users
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>user/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_users').val(json[i].id_users);
            $('#id_data_keluarga').val(json[i].id_data_keluarga);
            $('#nik').val(json[i].nik);
            $('#nama_users').val(json[i].nama_users);
            $('#jenis_kelamin').val(json[i].jenis_kelamin);
            $('#tempat_lahir').val(json[i].tempat_lahir);
            $('#tanggal_lahir').val(json[i].tanggal_lahir);
            $('#berkebutuhan_khusus').val(json[i].berkebutuhan_khusus);
            $('#berkebutuhan_khusus_fisik').val(json[i].berkebutuhan_khusus_fisik);
          }
        }
      });
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#update_users').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'id_users', 'temp', 'nama_users' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_users"] = $("#id_users").val();
      parameter["id_data_keluarga"] = $("#id_data_keluarga").val();
      parameter["temp"] = $("#temp").val();
      parameter["nik"] = $("#nik").val();
      parameter["nama_users"] = $("#nama_users").val();
      parameter["jenis_kelamin"] = $("#jenis_kelamin").val();
      parameter["tempat_lahir"] = $("#tempat_lahir").val();
      parameter["tanggal_lahir"] = $("#tanggal_lahir").val();
      parameter["berkebutuhan_khusus"] = $("#berkebutuhan_khusus").val();
      parameter["berkebutuhan_khusus_fisik"] = $("#berkebutuhan_khusus_fisik").val();
      var url = '<?php echo base_url(); ?>user/update_users';
      
      var parameterRv = [ 'id_users', 'temp', 'nama_users' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_users').on('click', '#del_ajax_users', function() {
    var id_users = $(this).closest('tr').attr('id_users');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_users"] = id_users;
        var url = '<?php echo base_url(); ?>user/hapus/';
        HapusData(parameter, url);
        $('[id_users='+id_users+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
});
</script>