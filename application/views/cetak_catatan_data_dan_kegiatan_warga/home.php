
                  <div class="col-md-12">
                    <center>
                      <h1>Data Warga TP-<?php
                        $web=$this->uut->namadomain(base_url());
                        if($web=='pkk.wonosobokab.go.id'){
                          if(!empty($keterangan)){ echo $keterangan; }
                        }else{
                          if(!empty($keterangan)){ echo 'PKK '.$keterangan.''; }
                        } ?>
                      </h1>
                    </center>
                    <div class="card">
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
                          <tr>
                            <th>No</th>
                            <th>No. Reg</th>
                            <th>Nama Anggota Keluarga</th>
                            <th>Status Dalam Keluarga</th>
                            <th>Status Dalam Pernikahan</th>
                            <th>Jenis Kelamin</th> 
                            <th>Tgl.Lahir/ Umur</th> 
                            <th>Pendidikan</th>
                            <th>Pekerjaan</th>
                          </tr>
													<tbody>
                          <?php
                          $table = 'anggota_keluarga';
                          $id_data_keluarga    = $this->input->get('id_data_keluarga');
                          $page    = $this->input->get('page');
                          $limit    = $this->input->get('limit');
                          $keyword    = $this->input->get('keyword');
                          $order_by    = $this->input->get('orderby');
                          $start      = ($page - 1) * $limit;
                          $fields     = "
                          *,
                          ( select (desa.nama_desa) from desa where desa.id_desa=anggota_keluarga.id_desa limit 1) as nama_desa,
                          ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=anggota_keluarga.id_kecamatan limit 1) as nama_kecamatan
                          ";
                          $where = array(
                            'anggota_keluarga.status !=' => 99,
                            'anggota_keluarga.id_data_keluarga' => $id_data_keluarga
                            );
                          $orderby   = ''.$order_by.'';
                          $data_anggota_keluarga = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
                          $urut=$start;
                          foreach ($data_anggota_keluarga->result() as $row){
                            $urut=$urut+1;
                            echo'
                            <tr>
                              <td>'.($urut).'</td>
                              <td>'.$row->nomor_registrasi.'</td>
                              <td>'.$row->nama_anggota.'</td>
                              <td>'.$row->status_dalam_keluarga.'</td>
                              <td>'.$row->status_perkawinan.'</td>
                              <td>'.$row->jenis_kelamin.'</td>
                              <td>'.$row->tanggal_lahir.'</td>
                              <td>'.$row->pendidikan.'</td>
                              <td>'.$row->pekerjaan.'</td>
                            </tr>
                            ';
                          }
                          ?>
													</tbody>
												</table>
											</div>
                    </div>
                  </div>