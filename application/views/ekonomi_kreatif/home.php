
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ekonomi Kreatif</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Ekonomi Kreatif</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_form_ekonomi_kreatif" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab" id="klik_tab_data_ekonomi_kreatif">Data</a></li>
                </ul>
								<div class="card-tools">
									<a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>ekonomi_kreatif"><i class="fa fa-refresh"></i></a>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
									<div class="tab-pane active" id="tab_form_ekonomi_kreatif">
										<input name="tabel" id="tabel" value="ekonomi_kreatif" type="hidden" value="">
										<form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=ekonomi_kreatif" enctype="multipart/form-data">
											<input name="page" id="page" value="1" type="hidden" value="">
											<div class="form-group" style="display:none;">
												<label for="temp">temp</label>
												<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="mode">mode</label>
												<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
											</div>
											<div class="form-group" style="display:none;">
												<label for="id_ekonomi_kreatif">id_ekonomi_kreatif</label>
												<input class="form-control" id="id_ekonomi_kreatif" name="id" value="" placeholder="id_ekonomi_kreatif" type="text">
											</div>
											<div class="form-group">
												<label for="nama">Nama</label>
												<input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
											</div>
											<div class="form-group">
											<button type="submit" class="btn btn-primary" id="simpan_ekonomi_kreatif">SIMPAN</button>
											<button type="submit" class="btn btn-primary" id="update_ekonomi_kreatif" style="display:none;">UPDATE</button>
											</div>
										</form>

										<div class="overlay" id="overlay_form_input" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
									<div class="tab-pane table-responsive" id="tab_2">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="10">10 Per-Halaman</option>
															<option value="25">25 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
															<option value="999999999">Semua</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="ekonomi_kreatif.nomor_ekonomi_kreatif">Nomor</option>
															<option value="ekonomi_kreatif.tanggal_ekonomi_kreatif">Tanggal</option>
															<option value="ekonomi_kreatif.nama_organisasi">Nama</option>
															<option value="ekonomi_kreatif.kesenian">Jenis</option>
														</select>
														<select name="perihal_surat" class="form-control input-sm pull-right" style="width: 350px; display:none;" id="perihal_surat">
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_ekonomi_kreatif"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<tr>
														<th>No.</th>
														<th>Nomor</th>
														<th>Tanggal</th>
														<th>Nama</th>
														<th>Jenis</th>
														<th>Alamat</th>
														<th>PROSES</th> 
													</tr>
													<tbody id="tbl_data_ekonomi_kreatif">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_ekonomi_kreatif" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				
<script>
  function AfterSavedAnggota_organisasi(mode, value) {
    $('#id_anggota_organisasi, #nama_anggota_organisasi, #umur_anggota_organisasi, #jabatan_anggota_organisasi, #keterangan_anggota_organisasi').val('');
    $('#overlay_data_anggota_organisasi').show();
    $('#tbl_data_anggota_organisasi').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
				mode:mode,
        value:value
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>anggota_organisasi/load_anggota_organisasi/',
      success: function(json) {
        var tr = '';
        var start = 0;
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_anggota_organisasi="' + json[i].id_anggota_organisasi + '" id="' + json[i].id_anggota_organisasi + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
					tr += '<td valign="top">' + json[i].nama_anggota_organisasi + '</td>';
					tr += '<td valign="top">' + json[i].umur_anggota_organisasi + '</td>';
					tr += '<td valign="top">' + json[i].jabatan_anggota_organisasi + '</td>';
					tr += '<td valign="top">' + json[i].keterangan_anggota_organisasi + '</td>';
					tr += '<td valign="top">';
					tr += '<a href="#tab_1" data-toggle="tab" class="update_id_anggota_organisasi badge bg-success btn-sm"><i class="fa fa-pencil-square-o"></i></a> ';
					tr += '<a href="#tab_1" data-toggle="tab" id="del_ajax_anggota_organisasi" class="badge bg-danger btn-sm"><i class="fa fa-cut"></i></a>';
					tr += '</td>';
          tr += '</tr>';
        }
        $('#tbl_data_anggota_organisasi').append(tr);
				$('#overlay_data_anggota_organisasi').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_anggota_organisasi').on('click', function(e) {
    // $('#temp').html('');
      e.preventDefault();
      var parameter = [ 'nama_anggota_organisasi', 'umur_anggota_organisasi', 'jabatan_anggota_organisasi', 'keterangan_anggota_organisasi', 'temp' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["nama_anggota_organisasi"] = $("#nama_anggota_organisasi").val();
      parameter["umur_anggota_organisasi"] = $("#umur_anggota_organisasi").val();
      parameter["jabatan_anggota_organisasi"] = $("#jabatan_anggota_organisasi").val();
      parameter["keterangan_anggota_organisasi"] = $("#keterangan_anggota_organisasi").val();
      var url = '<?php echo base_url(); ?>anggota_organisasi/simpan_anggota_organisasi';
      
      var parameterRv = [ 'nama_anggota_organisasi', 'umur_anggota_organisasi', 'jabatan_anggota_organisasi', 'keterangan_anggota_organisasi', 'temp' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_anggota_organisasi').val();
        }
        else{
          var value = $('#temp').val();
        }
        AfterSavedAnggota_organisasi(mode, value);
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_anggota_organisasi').on('click', '.update_id_anggota_organisasi', function() {
    $('#mode').val('edit_anggota_organisasi');
    $('#simpan_anggota_organisasi').hide();
    $('#update_anggota_organisasi').show();
    $('#overlay_data_anggota_organisasi').show();
    $('#temp').html('');
    var id_anggota_organisasi = $(this).closest('tr').attr('id_anggota_organisasi');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_anggota_organisasi');
    $('#form_baru_anggota_organisasi').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_anggota_organisasi').val(id_anggota_organisasi);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_anggota_organisasi:id_anggota_organisasi
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>anggota_organisasi/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#nama_anggota_organisasi').val(json[i].nama_anggota_organisasi);
            $('#umur_anggota_organisasi').val(json[i].umur_anggota_organisasi);
            $('#jabatan_anggota_organisasi').val(json[i].jabatan_anggota_organisasi);
            $('#keterangan_anggota_organisasi').val(json[i].keterangan_anggota_organisasi);
            $('#temp').val(json[i].temp);
          }
        }
      });
    //AttachmentByMode(mode, value);
		AfterSavedAnggota_organisasi(mode, value);
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_anggota_organisasi').on('click', '#del_ajax_anggota_organisasi', function() {
    var id_anggota_organisasi = $(this).closest('tr').attr('id_anggota_organisasi');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_anggota_organisasi"] = id_anggota_organisasi;
        var url = '<?php echo base_url(); ?>anggota_organisasi/hapus/';
        HapusData(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_anggota_organisasi').val();
          }
          else{
            var value = $('#temp').val();
          }
        AfterSavedAnggota_organisasi(mode, value);
        $('[id_anggota_organisasi='+id_anggota_organisasi+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#update_anggota_organisasi').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'id_anggota_organisasi', 'nama_anggota_organisasi', 'umur_anggota_organisasi', 'jabatan_anggota_organisasi', 'keterangan_anggota_organisasi' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_anggota_organisasi"] = $("#id_anggota_organisasi").val();
      parameter["nama_anggota_organisasi"] = $("#nama_anggota_organisasi").val();
      parameter["umur_anggota_organisasi"] = $("#umur_anggota_organisasi").val();
      parameter["jabatan_anggota_organisasi"] = $("#jabatan_anggota_organisasi").val();
      parameter["keterangan_anggota_organisasi"] = $("#keterangan_anggota_organisasi").val();
      var url = '<?php echo base_url(); ?>anggota_organisasi/update_anggota_organisasi';
      
      var parameterRv = [ 'id_anggota_organisasi', 'nama_anggota_organisasi', 'umur_anggota_organisasi', 'jabatan_anggota_organisasi', 'keterangan_anggota_organisasi' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var perihal_surat = $('#perihal_surat').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        perihal_surat:perihal_surat
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>ekonomi_kreatif/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '" perihal_surat="' + perihal_surat + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var perihal_surat = $('#perihal_surat').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#tab_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        perihal_surat:perihal_surat
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<!-- perihal_surat -->
<script type="text/javascript">
$(document).ready(function() {
	load_perihal_surat();
});
</script>
<script type="text/javascript">
  function load_perihal_surat() {
    $('#perihal_surat').html('');
    $('#perihal_ekonomi_kreatif').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>perihal_permohonan_pendaftaran_badan_usaha/perihal_surat_ekonomi_kreatif/',
      success: function(json) {
        var option = '';
        //option += '<option value="0">Pilih Perihal</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_perihal_permohonan_pendaftaran_badan_usaha + '" >' + json[i].judul_perihal_permohonan_pendaftaran_badan_usaha + '</option>';
        }
        $('#perihal_surat').append(option);
        $('#perihal_ekonomi_kreatif').append(option);
      }
    });
  }
</script>
<!-- end perihal_surat -->
<!-- nama_kesenian -->
<script type="text/javascript">
$(document).ready(function() {
	load_jenis_kesenian();
});
</script>
<script type="text/javascript">
  function load_jenis_kesenian() {
    $('#jenis_kesenian').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>jenis_kesenian/json_option/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Jenis Kesenian</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_jenis_kesenian + '" >' + json[i].judul_jenis_kesenian + '</option>';
        }
        $('#jenis_kesenian').append(option);
      }
    });
  }
</script>
<!-- end jenis_kesenian -->
<!-- pekerjaan_pemohon -->
<script type="text/javascript">
$(document).ready(function() {
	load_pekerjaan();
});
</script>
<script type="text/javascript">
  function load_pekerjaan() {
    $('#pekerjaan_pemohon').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>pekerjaan/json_option_pekerjaan/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Pekerjaan</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_pekerjaan + '" >' + json[i].nama_pekerjaan + '</option>';
        }
        $('#pekerjaan_pemohon').append(option);
      }
    });
  }
</script>
<!-- end pekerjaan_pemohon -->
<!-- Wilayah -->
<script type="text/javascript">
$(document).ready(function() {
	load_propinsi();
});
</script>
<script type="text/javascript">
  function load_propinsi() {
    $('#id_propinsi').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_propinsi/',
      success: function(json) {
        var option = '';
        option += '<option value="" >Pilih Provinsi</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_propinsi + '" >' + json[i].nama_propinsi + '</option>';
        }
        $('#id_propinsi').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_propinsi').on('change', function(e) {
      e.preventDefault();
      var id_propinsi = $('#id_propinsi').val();
      load_kabupaten_by_id_propinsi(id_propinsi);
    });
});
</script>
<script type="text/javascript">
  function load_kabupaten_by_id_propinsi(id_propinsi) {
    $('#id_kabupaten').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_propinsi:id_propinsi
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_kabupaten_by_id_propinsi/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Kabupaten</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_kabupaten + '" >' + json[i].nama_kabupaten + '</option>';
        }
        $('#id_kabupaten').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_kabupaten').on('change', function(e) {
      e.preventDefault();
      var id_kabupaten = $('#id_kabupaten').val();
      load_kecamatan_by_id_kabupaten(id_kabupaten);
    });
});
</script>
<script type="text/javascript">
  function load_kecamatan_by_id_kabupaten(id_kabupaten) {
    $('#id_kecamatan').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kabupaten:id_kabupaten
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_kecamatan_by_id_kabupaten/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Kecamatan</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_kecamatan + '" >' + json[i].nama_kecamatan + '</option>';
        }
        $('#id_kecamatan').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_desa_by_id_kecamatan(id_kecamatan) {
    $('#id_desa').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kecamatan:id_kecamatan
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_desa_by_id_kecamatan/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Desa</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_desa + '" >' + json[i].nama_desa + '</option>';
        }
        $('#id_desa').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_kecamatan').on('change', function(e) {
      e.preventDefault();
      var id_kecamatan = $('#id_kecamatan').val();
      load_desa_by_id_kecamatan(id_kecamatan);
    });
});
</script>
<!-- end Wilayah -->
<!-- Wilayah Pemohon -->
<script type="text/javascript">
$(document).ready(function() {
	load_propinsi_id_propinsi_pemohon();
});
</script>
<script type="text/javascript">
  function load_propinsi_id_propinsi_pemohon() {
    $('#id_propinsi_pemohon').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_propinsi_id_propinsi_perusahaan/',
      success: function(json) {
        var option = '';
        option += '<option value="" >Pilih Provinsi</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_propinsi + '" >' + json[i].nama_propinsi + '</option>';
        }
        $('#id_propinsi_pemohon').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_propinsi_pemohon').on('change', function(e) {
      e.preventDefault();
      var id_propinsi = $('#id_propinsi_pemohon').val();
      load_kabupaten_by_id_propinsi_id_propinsi_pemohon(id_propinsi);
    });
});
</script>
<script type="text/javascript">
  function load_kabupaten_by_id_propinsi_id_propinsi_pemohon(id_propinsi) {
    $('#id_kabupaten_pemohon').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_propinsi:id_propinsi
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_kabupaten_by_id_propinsi_id_kabupaten_perusahaan/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Kabupaten</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_kabupaten + '" >' + json[i].nama_kabupaten + '</option>';
        }
        $('#id_kabupaten_pemohon').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_kabupaten_pemohon').on('change', function(e) {
      e.preventDefault();
      var id_kabupaten = $('#id_kabupaten_pemohon').val();
      load_kecamatan_by_id_kabupaten_id_kabupaten_pemohon(id_kabupaten);
    });
});
</script>
<script type="text/javascript">
  function load_kecamatan_by_id_kabupaten_id_kabupaten_pemohon(id_kabupaten) {
    $('#id_kecamatan_pemohon').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kabupaten:id_kabupaten
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_kecamatan_by_id_kabupaten_id_kecamatan_perusahaan/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Kecamatan</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_kecamatan + '" >' + json[i].nama_kecamatan + '</option>';
        }
        $('#id_kecamatan_pemohon').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_desa_by_id_kecamatan_id_desa_pemohon(id_kecamatan) {
    $('#id_desa_pemohon').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kecamatan:id_kecamatan
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/json_all_desa_by_id_kecamatan_id_desa_perusahaan/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Desa</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_desa + '" >' + json[i].nama_desa + '</option>';
        }
        $('#id_desa_pemohon').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_kecamatan_pemohon').on('change', function(e) {
      e.preventDefault();
      var id_kecamatan = $('#id_kecamatan_pemohon').val();
      load_desa_by_id_kecamatan_id_desa_pemohon(id_kecamatan);
    });
});
</script>
<!-- end Wilayah -->
<script>
  function AfterSavedEkonomi_kreatif() {
    $('#id_ekonomi_kreatif, #judul_ekonomi_kreatif, #icon, #keterangan, #perihal_ekonomi_kreatif, #yth_ekonomi_kreatif, #cq_ekonomi_kreatif, #di_ekonomi_kreatif, #tanggal_ekonomi_kreatif, #sifat_ekonomi_kreatif, #lampiran_ekonomi_kreatif, #nama_organisasi, #pekerjaan_pemohon, #kesenian, #jenis_kesenian, #alamat_organisasi, #id_desa, #id_kecamatan, #id_kabupaten, #id_propinsi, #fungsi_kesenian, #nama_kesenian, #pengalaman_pentas, #id_desa_pemohon, #id_kecamatan_pemohon, #id_kabupaten_pemohon, #id_propinsi_pemohon, #penghargaan_yang_pernah_diterima, #fasilitas_peralatan_yang_dimiliki').val('');
    $('#tbl_attachment_ekonomi_kreatif').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#icon').on('change', function(e) {
      e.preventDefault();
      var xa = $('#icon').val();
      $("#iconselected").html('<span class="fa '+xa+' ">'+xa+'</span>');
    });
  });
</script>
 
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_ekonomi_kreatif').on('click', function(e) {
      e.preventDefault();
      var editor_riwayat_sejarah = CKEDITOR.instances.editor_riwayat_sejarah.getData();
      $('#riwayat_sejarah').val( editor_riwayat_sejarah );
      var parameter = [ 'judul_ekonomi_kreatif', 'nomor_ekonomi_kreatif', 'perihal_ekonomi_kreatif', 'yth_ekonomi_kreatif', 'cq_ekonomi_kreatif', 'di_ekonomi_kreatif', 'tanggal_ekonomi_kreatif', 'sifat_ekonomi_kreatif', 'lampiran_ekonomi_kreatif', 'nama_organisasi', 'pekerjaan_pemohon', 'kesenian', 'jenis_kesenian', 'alamat_organisasi', 'id_desa', 'id_kecamatan', 'id_kabupaten', 'id_propinsi', 'fungsi_kesenian', 'nama_kesenian', 'pengalaman_pentas', 'id_desa_pemohon', 'id_kecamatan_pemohon', 'id_kabupaten_pemohon', 'id_propinsi_pemohon', 'penghargaan_yang_pernah_diterima', 'fasilitas_peralatan_yang_dimiliki', 'hambatan_kendala', 'riwayat_sejarah', 'nama_pemohon', 'tempat_lahir_pemohon', 'tanggal_lahir_pemohon', 'pekerjaan_pemohon', 'nomor_telp_pemohon', 'alamat_pemohon', 'kepertluan', 'tanggal_pendirian', 'jumlah_anggota_pria', 'jumlah_anggota_wanita', 'jumlah_anggota' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["judul_ekonomi_kreatif"] = $("#judul_ekonomi_kreatif").val();
      parameter["nomor_ekonomi_kreatif"] = $("#nomor_ekonomi_kreatif").val();
      parameter["perihal_ekonomi_kreatif"] = $("#perihal_ekonomi_kreatif").val();
      parameter["yth_ekonomi_kreatif"] = $("#yth_ekonomi_kreatif").val();
      parameter["cq_ekonomi_kreatif"] = $("#cq_ekonomi_kreatif").val();
      parameter["di_ekonomi_kreatif"] = $("#di_ekonomi_kreatif").val();
      parameter["tanggal_ekonomi_kreatif"] = $("#tanggal_ekonomi_kreatif").val();
      parameter["sifat_ekonomi_kreatif"] = $("#sifat_ekonomi_kreatif").val();
      parameter["lampiran_ekonomi_kreatif"] = $("#lampiran_ekonomi_kreatif").val();
      parameter["nama_organisasi"] = $("#nama_organisasi").val();
      parameter["pekerjaan_pemohon"] = $("#pekerjaan_pemohon").val();
      parameter["kesenian"] = $("#kesenian").val();
      parameter["jenis_kesenian"] = $("#jenis_kesenian").val();
      parameter["alamat_organisasi"] = $("#alamat_organisasi").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["fungsi_kesenian"] = $("#fungsi_kesenian").val();
      parameter["nama_kesenian"] = $("#nama_kesenian").val();
      parameter["pengalaman_pentas"] = $("#pengalaman_pentas").val();
      parameter["id_desa_pemohon"] = $("#id_desa_pemohon").val();
      parameter["id_kecamatan_pemohon"] = $("#id_kecamatan_pemohon").val();
      parameter["id_kabupaten_pemohon"] = $("#id_kabupaten_pemohon").val();
      parameter["id_propinsi_pemohon"] = $("#id_propinsi_pemohon").val();
      parameter["penghargaan_yang_pernah_diterima"] = $("#penghargaan_yang_pernah_diterima").val();
      parameter["fasilitas_peralatan_yang_dimiliki"] = $("#fasilitas_peralatan_yang_dimiliki").val();
      parameter["hambatan_kendala"] = $("#hambatan_kendala").val();
      parameter["riwayat_sejarah"] = $("#riwayat_sejarah").val();
      parameter["nama_pemohon"] = $("#nama_pemohon").val();
      parameter["tempat_lahir_pemohon"] = $("#tempat_lahir_pemohon").val();
      parameter["tanggal_lahir_pemohon"] = $("#tanggal_lahir_pemohon").val();
      parameter["pekerjaan_pemohon"] = $("#pekerjaan_pemohon").val();
      parameter["nomor_telp_pemohon"] = $("#nomor_telp_pemohon").val();
      parameter["alamat_pemohon"] = $("#alamat_pemohon").val();
      parameter["kepertluan"] = $("#kepertluan").val();
      parameter["tanggal_pendirian"] = $("#tanggal_pendirian").val();
      parameter["jumlah_anggota_pria"] = $("#jumlah_anggota_pria").val();
      parameter["jumlah_anggota_wanita"] = $("#jumlah_anggota_wanita").val();
      parameter["jumlah_anggota"] = $("#jumlah_anggota").val();
      var url = '<?php echo base_url(); ?>ekonomi_kreatif/simpan_ekonomi_kreatif';
      
      var parameterRv = [ 'judul_ekonomi_kreatif', 'nomor_ekonomi_kreatif', 'perihal_ekonomi_kreatif', 'yth_ekonomi_kreatif', 'cq_ekonomi_kreatif', 'di_ekonomi_kreatif', 'tanggal_ekonomi_kreatif', 'sifat_ekonomi_kreatif', 'lampiran_ekonomi_kreatif', 'nama_organisasi', 'pekerjaan_pemohon', 'kesenian', 'jenis_kesenian', 'alamat_organisasi', 'id_desa', 'id_kecamatan', 'id_kabupaten', 'id_propinsi', 'fungsi_kesenian', 'nama_kesenian', 'pengalaman_pentas', 'id_desa_pemohon', 'id_kecamatan_pemohon', 'id_kabupaten_pemohon', 'id_propinsi_pemohon', 'penghargaan_yang_pernah_diterima', 'fasilitas_peralatan_yang_dimiliki', 'hambatan_kendala', 'riwayat_sejarah', 'nama_pemohon', 'tempat_lahir_pemohon', 'tanggal_lahir_pemohon', 'pekerjaan_pemohon', 'nomor_telp_pemohon', 'alamat_pemohon', 'kepertluan', 'tanggal_pendirian', 'jumlah_anggota_pria', 'jumlah_anggota_wanita', 'jumlah_anggota' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedEkonomi_kreatif();
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_ekonomi_kreatif').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_ekonomi_kreatif').hide();
    $('#update_ekonomi_kreatif').show();
    $('#overlay_data_anggota_organisasi').show();
    $('#tbl_data_anggota_organisasi').html('');
    $('#temp').html('');
    var id_ekonomi_kreatif = $(this).closest('tr').attr('id_ekonomi_kreatif');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_ekonomi_kreatif');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_ekonomi_kreatif').val(id_ekonomi_kreatif);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_ekonomi_kreatif:id_ekonomi_kreatif
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>ekonomi_kreatif/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#judul_ekonomi_kreatif').val(json[i].judul_ekonomi_kreatif);
            $('#icon').val(json[i].icon);
            $('#temp').val(json[i].temp);
            $('#nomor_ekonomi_kreatif').val(json[i].nomor_ekonomi_kreatif);
            $('#perihal_ekonomi_kreatif').val(json[i].perihal_ekonomi_kreatif);
            $('#yth_ekonomi_kreatif').val(json[i].yth_ekonomi_kreatif);
            $('#cq_ekonomi_kreatif').val(json[i].cq_ekonomi_kreatif);
            $('#di_ekonomi_kreatif').val(json[i].di_ekonomi_kreatif);
            $('#tanggal_ekonomi_kreatif').val(json[i].tanggal_ekonomi_kreatif);
            $('#sifat_ekonomi_kreatif').val(json[i].sifat_ekonomi_kreatif);
            $('#lampiran_ekonomi_kreatif').val(json[i].lampiran_ekonomi_kreatif);
            $('#nama_organisasi').val(json[i].nama_organisasi);
            $('#pekerjaan_pemohon').val(json[i].pekerjaan_pemohon);
            $('#kesenian').val(json[i].kesenian);
            $('#jenis_kesenian').val(json[i].jenis_kesenian);
            $('#alamat_organisasi').val(json[i].alamat_organisasi);
            //$('#id_desa').val(json[i].id_desa);
            //$('#id_kecamatan').val(json[i].id_kecamatan);
            //$('#id_kabupaten').val(json[i].id_kabupaten);
            //$('#id_propinsi').val(json[i].id_propinsi);
            $('#fungsi_kesenian').val(json[i].fungsi_kesenian);
            $('#nama_kesenian').val(json[i].nama_kesenian);
            $('#pengalaman_pentas').val(json[i].pengalaman_pentas);
            //$('#id_desa_pemohon').val(json[i].id_desa_pemohon);
            //$('#id_kecamatan_pemohon').val(json[i].id_kecamatan_pemohon);
            //$('#id_kabupaten_pemohon').val(json[i].id_kabupaten_pemohon);
            //$('#id_propinsi_pemohon').val(json[i].id_propinsi_pemohon);
            $('#penghargaan_yang_pernah_diterima').val(json[i].penghargaan_yang_pernah_diterima);
            $('#fasilitas_peralatan_yang_dimiliki').val(json[i].fasilitas_peralatan_yang_dimiliki);
            $('#hambatan_kendala').val(json[i].hambatan_kendala);
            $('#riwayat_sejarah').val(json[i].riwayat_sejarah);
            $('#nama_pemohon').val(json[i].nama_pemohon);
            $('#tempat_lahir_pemohon').val(json[i].tempat_lahir_pemohon);
            $('#tanggal_lahir_pemohon').val(json[i].tanggal_lahir_pemohon);
            $('#pekerjaan_pemohon').val(json[i].pekerjaan_pemohon);
            $('#nomor_telp_pemohon').val(json[i].nomor_telp_pemohon);
            $('#alamat_pemohon').val(json[i].alamat_pemohon);
            $('#kepertluan').val(json[i].kepertluan);
            $('#tanggal_pendirian').val(json[i].tanggal_pendirian);
            $('#jumlah_anggota_pria').val(json[i].jumlah_anggota_pria);
            $('#jumlah_anggota_wanita').val(json[i].jumlah_anggota_wanita);
            $('#jumlah_anggota').val(json[i].jumlah_anggota);
            CKEDITOR.instances.editor_riwayat_sejarah.setData(json[i].riwayat_sejarah);
						load_wilayah_by_id_desa_pemohon(json[i].id_desa_pemohon);
						load_wilayah_by_id_desa(json[i].id_desa);
          }
        }
      });
    //AttachmentByMode(mode, value);
		AfterSavedAnggota_organisasi(mode, value);
  });
});
</script>

<!-- edit desa-->
<script>
  function load_wilayah_by_id_desa_pemohon(id_desa) {
    $('#id_desa_pemohon').html('');
    $('#id_kecamatan_pemohon').html('');
    $('#id_kabupaten_pemohon').html('');
    // $('#id_propinsi_pemohon').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_desa:id_desa
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_wilayah_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_desa_pemohon').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan_pemohon').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten_pemohon').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
          // $('#id_propinsi_pemohon').html('<option value="' + json[i].id_propinsi + '">' + json[i].nama_propinsi + '</option>');
					$('#id_propinsi_pemohon').val(json[i].id_propinsi);
        }
      }
    });
  }
</script>
<script>
  function load_wilayah_by_id_desa(id_desa) {
    $('#id_desa').html('');
    $('#id_kecamatan').html('');
    $('#id_kabupaten').html('');
    // $('#id_propinsi').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_desa:id_desa
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>wilayah/load_wilayah_by_id_desa/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $('#id_desa').html('<option value="' + json[i].id_desa + '">' + json[i].nama_desa + '</option>');
          $('#id_kecamatan').html('<option value="' + json[i].id_kecamatan + '">' + json[i].nama_kecamatan + '</option>');
          $('#id_kabupaten').html('<option value="' + json[i].id_kabupaten + '">' + json[i].nama_kabupaten + '</option>');
          // $('#id_propinsi').html('<option value="' + json[i].id_propinsi + '">' + json[i].nama_propinsi + '</option>');
					$('#id_propinsi').val(json[i].id_propinsi);
        }
      }
    });
  }
</script>
<!-- end edit desa-->
<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_pendirian, #tanggal_lahir_pemohon, #tanggal_ekonomi_kreatif').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#update_ekonomi_kreatif').on('click', function(e) {
      e.preventDefault();
      var editor_riwayat_sejarah = CKEDITOR.instances.editor_riwayat_sejarah.getData();
      $('#riwayat_sejarah').val( editor_riwayat_sejarah );
      var parameter = [ 'id_ekonomi_kreatif', 'judul_ekonomi_kreatif', 'nomor_ekonomi_kreatif', 'perihal_ekonomi_kreatif', 'yth_ekonomi_kreatif', 'cq_ekonomi_kreatif', 'di_ekonomi_kreatif', 'tanggal_ekonomi_kreatif', 'sifat_ekonomi_kreatif', 'lampiran_ekonomi_kreatif', 'nama_organisasi', 'kesenian', 'jenis_kesenian', 'alamat_organisasi' ];
      InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_ekonomi_kreatif"] = $("#id_ekonomi_kreatif").val();
      parameter["judul_ekonomi_kreatif"] = $("#judul_ekonomi_kreatif").val();
      parameter["nomor_ekonomi_kreatif"] = $("#nomor_ekonomi_kreatif").val();
      parameter["perihal_ekonomi_kreatif"] = $("#perihal_ekonomi_kreatif").val();
      parameter["yth_ekonomi_kreatif"] = $("#yth_ekonomi_kreatif").val();
      parameter["cq_ekonomi_kreatif"] = $("#cq_ekonomi_kreatif").val();
      parameter["di_ekonomi_kreatif"] = $("#di_ekonomi_kreatif").val();
      parameter["tanggal_ekonomi_kreatif"] = $("#tanggal_ekonomi_kreatif").val();
      parameter["sifat_ekonomi_kreatif"] = $("#sifat_ekonomi_kreatif").val();
      parameter["lampiran_ekonomi_kreatif"] = $("#lampiran_ekonomi_kreatif").val();
      parameter["nama_organisasi"] = $("#nama_organisasi").val();
      parameter["pekerjaan_pemohon"] = $("#pekerjaan_pemohon").val();
      parameter["kesenian"] = $("#kesenian").val();
      parameter["jenis_kesenian"] = $("#jenis_kesenian").val();
      parameter["alamat_organisasi"] = $("#alamat_organisasi").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["fungsi_kesenian"] = $("#fungsi_kesenian").val();
      parameter["nama_kesenian"] = $("#nama_kesenian").val();
      parameter["pengalaman_pentas"] = $("#pengalaman_pentas").val();
      parameter["id_desa_pemohon"] = $("#id_desa_pemohon").val();
      parameter["id_kecamatan_pemohon"] = $("#id_kecamatan_pemohon").val();
      parameter["id_kabupaten_pemohon"] = $("#id_kabupaten_pemohon").val();
      parameter["id_propinsi_pemohon"] = $("#id_propinsi_pemohon").val();
      parameter["penghargaan_yang_pernah_diterima"] = $("#penghargaan_yang_pernah_diterima").val();
      parameter["fasilitas_peralatan_yang_dimiliki"] = $("#fasilitas_peralatan_yang_dimiliki").val();
      parameter["hambatan_kendala"] = $("#hambatan_kendala").val();
      parameter["riwayat_sejarah"] = $("#riwayat_sejarah").val();
      parameter["nama_pemohon"] = $("#nama_pemohon").val();
      parameter["tempat_lahir_pemohon"] = $("#tempat_lahir_pemohon").val();
      parameter["tanggal_lahir_pemohon"] = $("#tanggal_lahir_pemohon").val();
      parameter["pekerjaan_pemohon"] = $("#pekerjaan_pemohon").val();
      parameter["nomor_telp_pemohon"] = $("#nomor_telp_pemohon").val();
      parameter["alamat_pemohon"] = $("#alamat_pemohon").val();
      parameter["kepertluan"] = $("#kepertluan").val();
      parameter["tanggal_pendirian"] = $("#tanggal_pendirian").val();
      parameter["jumlah_anggota_pria"] = $("#jumlah_anggota_pria").val();
      parameter["jumlah_anggota_wanita"] = $("#jumlah_anggota_wanita").val();
      parameter["jumlah_anggota"] = $("#jumlah_anggota").val();
      var url = '<?php echo base_url(); ?>ekonomi_kreatif/update_ekonomi_kreatif';
      
      var parameterRv = [ 'id_ekonomi_kreatif', 'judul_ekonomi_kreatif', 'nomor_ekonomi_kreatif', 'perihal_ekonomi_kreatif', 'yth_ekonomi_kreatif', 'cq_ekonomi_kreatif', 'di_ekonomi_kreatif', 'tanggal_ekonomi_kreatif', 'sifat_ekonomi_kreatif', 'lampiran_ekonomi_kreatif', 'nama_organisasi', 'kesenian', 'jenis_kesenian', 'alamat_organisasi' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<!----------------------->
<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('editor_riwayat_sejarah');
	$(".textarea").wysihtml5();
});
</script>
<!-- attachment -->
<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_ekonomi_kreatif').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'ekonomi_kreatif',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_ekonomi_kreatif').append(tr);
			}
		});
	}
</script>
<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_ekonomi_kreatif').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_ekonomi_kreatif').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_ekonomi_kreatif').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
<!-- end attachment -->
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_ekonomi_kreatif').on('click', '#del_ajax', function() {
    var id_ekonomi_kreatif = $(this).closest('tr').attr('id_ekonomi_kreatif');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_ekonomi_kreatif"] = id_ekonomi_kreatif;
        var url = '<?php echo base_url(); ?>ekonomi_kreatif/hapus/';
        HapusData(parameter, url);
        $('[id_ekonomi_kreatif='+id_ekonomi_kreatif+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_ekonomi_kreatif').on('click', '#inaktifkan', function() {
    var id_ekonomi_kreatif = $(this).closest('tr').attr('id_ekonomi_kreatif');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_ekonomi_kreatif"] = id_ekonomi_kreatif;
        var url = '<?php echo base_url(); ?>ekonomi_kreatif/inaktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_ekonomi_kreatif').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
        $('[id_ekonomi_kreatif='+id_ekonomi_kreatif+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_ekonomi_kreatif').on('click', '#aktifkan', function() {
    var id_ekonomi_kreatif = $(this).closest('tr').attr('id_ekonomi_kreatif');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_ekonomi_kreatif"] = id_ekonomi_kreatif;
        var url = '<?php echo base_url(); ?>ekonomi_kreatif/aktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_ekonomi_kreatif').val();
          }
          else{
            var value = $('#temp').val();
          }
        //AttachmentByMode(mode, value);
				var tabel = $('#tabel').val();
				load_data(tabel);
        $('[id_ekonomi_kreatif='+id_ekonomi_kreatif+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>