<div style="padding:5px;">
  <div class="">
    <div class="col-md-4" id="list_opd">
    </div>
    <div class="col-md-4" id="list_kecamatan">
    </div>
    <div class="col-md-4" id="list_desakelurahan">
    </div>
    <div class="overlay" id="spinners_tbl_data" style="display:none;">
      <i class="fa fa-refresh fa-spin"></i> Silahkan tunggu...
    </div>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    $('#spinners_tbl_data').show();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>dashboard/list_opd',
      success: function(html) {
        $('#list_opd').html (html);
        $('#spinners_tbl_data').fadeOut('slow');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#spinners_tbl_data').show();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>dashboard/list_kecamatan',
      success: function(html) {
        $('#list_kecamatan').html (html);
        $('#spinners_tbl_data').fadeOut('slow');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#spinners_tbl_data').show();
    $.ajax({
      dataType: "html",
      url: '<?php echo base_url(); ?>dashboard/list_desakelurahan',
      success: function(html) {
        $('#list_desakelurahan').html (html);
        $('#spinners_tbl_data').fadeOut('slow');
      }
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $("#tampil_opd").on("click", function(e) {
      e.preventDefault();
      $('#btnExport').hide();
      var skpd_website = $("#skpd_website").val();
      var tanggal = 4;
      for (var i=1;i<=3;i++){
      window.setTimeout(list_1(i,skpd_website), 1000);
      }      
      $('#btnExport').show();
    });
  });
</script>
<script type="text/javascript">
  function list_1(i,skpd_website) {
      $('#'+i+'_1').html('<i class="fa fa-refresh fa-spin"></i>');
      var id_data_skpd = $('#'+i).val();
      $.ajax({
        dataType: "html",
        url: '<?php echo base_url(); ?>dashboard/total_posting_opd/?id_data_skpd=' + id_data_skpd + '&skpd_website=' + skpd_website + '',
        success: function(html) {
          $('#'+id_data_skpd+'_1').html(html);
        }
      });
  };
</script>