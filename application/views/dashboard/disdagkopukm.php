
												<center style="background-color: #d9edf7 !important; padding:10px;">
												<div style="col-md-6">
                          <div class="row">
                            <div class="col-md-4" style="display:none;">
                              <input name="tabel" id="tabel" value="harga_kebutuhan_pokok" type="hidden" value="">
                              <input name="page" id="page" value="1" type="hidden" value="">
                              <select name="limit" class="form-control" style="display:none;" id="limit">
                                <option value="999999999">Semua</option>
                                <option value="10">10 Per-Halaman</option>
                                <option value="20">20 Per-Halaman</option>
                                <option value="50">50 Per-Halaman</option>
                                <option value="100">100 Per-Halaman</option>
                              </select>
                              <select name="orderby" class="form-control" id="orderby" style="display:none;">
                                <option value="harga_kebutuhan_pokok.tanggal">Tanggal</option>
                                <option value="komoditi.judul_komoditi">Nama Komoditi</option>
                              </select>
                            </div>
                            <div class="col-md-4">
                              <?php
                              $w = $this->db->query("SELECT tanggal from harga_kebutuhan_pokok where status = 1 order by tanggal DESC limit 1");
                              foreach($w->result() as $h)
                              {
                                $tanggal = $h->tanggal;
                                $date = "".$tanggal."";
                                $date = strtotime($date);
                                $date = strtotime("-1 day", $date);
                                $tanggal_terakhir_input_tampil = date('Y-m-d', $date);
                              }
                              ?>
                              <input name="keyword" class="form-control" style="" placeholder="Search" type="text" id="keyword" value="<?php echo $tanggal; ?>">
                            </div>
                            <div class="col-md-4">
                              <select name="filter" class="form-control" style="display:none;" id="filter">
                                <option value="DESC">DESC</option>
                                <option value="ASC">ASC</option>
                              </select>
                              <select name="id_pasar" class="form-control" id="id_pasar">
                              </select>
                            </div>
                            <div class="col-md-4">
                              <div class="input-group-btn">
                                <button class="btn btn-block btn-primary btn-lg" id="tampilkan_data_harga_kebutuhan_pokok"><i class="fa fa-search"></i> Tampil</button>
                              </div>
                            </div>
                          </div>
                        </div>
												</center>
                      <div class="" style="background-color: #d9edf7 !important; padding:10px;">
                        <div class="alert alert-warning alert-dismissable">
                          <p><i class="icon fa fa-info"></i> Harga dibandingkan dengan harga pada hari sebelumnya <?php echo ''.$this->Crud_model->TanggalBahasaIndo($tanggal_terakhir_input_tampil).''; ?></p>
                        </div>
                      </div>
                      <div class="panel-body" style="background-color: #d9edf7 !important; padding:10px;">
                        <div class="" id="tbl_data_harga_kebutuhan_pokok">
                        </div>
                        <br />
                        <ul class="pagination pagination-sm m-0 float-right" id="pagination">
                        </ul>
                        <div class="overlay" id="spinners_tbl_data_harga_kebutuhan_pokok" style="display:none;">
                          <i class="fa fa-refresh fa-spin"></i>
                        </div>
                      </div>
<script>
  function load_option_pasar() {
    $('#id_pasar').html('');
    $('#spinners_id_pasar').show();
  	$.ajax({
  		dataType: "json",
  		url: '<?php echo base_url('pasar/load_id_pasar_by_filter') ?>',
  		success: function(json) {
  			var trHTML = '';
  					// trHTML += '<option value="0">Pilih Pasar</option>';
  			for (var i = 0; i < json.length; i++) {
  					trHTML += '<option value="'+json[i].id_pasar+'">'+json[i].nama_pasar+'</option>';
  				}
  			$('#id_pasar').append(trHTML); 
  		}
  	});
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_option_pasar();
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var id_pasar = $('#id_pasar').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        id_pasar:id_pasar,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>dashboard/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" id_pasar="' + id_pasar + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var id_pasar = $('#id_pasar').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var filter = $('#filter').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        id_pasar:id_pasar,
        keyword:keyword,
        orderby:orderby,
        filter:filter
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>dashboard/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    load_data(tabel);
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#keyword').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>