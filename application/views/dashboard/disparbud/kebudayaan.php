<!--
                      <div class="" style="background-color: #d9edf7 !important; padding:10px;">
                        <div class="alert alert-warning alert-dismissable">
                          <p><i class="icon fa fa-info"></i> Data dan Informasi</p>
                        </div>
                      </div>-->
                      <div class="" style="background-color: #d9edf7 !important;">
                        <div class="" id="tbl_data_disparbud">
                        </div>
                        <div class="overlay" id="spinners_tbl_data_disparbud" style="display:none;">
                          <i class="fa fa-refresh fa-spin"></i> Silahkan tunggu...
                        </div>
                      </div>
<script type="text/javascript">
$(document).ready(function() {
  load_data();
});
</script>
<script type="text/javascript">
  function load_data() {
    $('#tbl_data_disparbud').html('');
    $('#spinners_tbl_data_disparbud').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>dashboard/json_all_disparbud_kebudayaan_perkecamatan/',
      success: function(html) {
        $('#tbl_data_disparbud').html(html);
        $('#spinners_tbl_data_disparbud').fadeOut('slow');
      }
    });
  }
</script>