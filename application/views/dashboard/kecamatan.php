
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>KECAMATAN</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Kecamatan</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

		<div class="row" id="awal">
          	<div class="col-12">
           		<div class="card">
					<div class="card-body table-responsive">
						<table id="tablekecamatan" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>No</th>
								<th>Domain</th>
								<th>Nama Kecamatan</th>
								<th>Jumlah Posting</th>
								<th>Update</th>
								<th>Medsos</th>
							</tr>
							</thead>
							<tbody>

								<?php

									$w = $this->db->query("SELECT *, data_kecamatan.kecamatan_nama
										FROM dasar_website, data_kecamatan
										WHERE data_kecamatan.kecamatan_website = dasar_website.domain
										AND dasar_website.status = 1
									 	ORDER BY data_kecamatan.kecamatan_nama ASC
										");
									$no=0;
									foreach($w->result() as $h){
										$w1 = $this->db->query("SELECT *
											FROM posting
											WHERE domain='".$h->domain."'
											AND status != 99
											");
										$jumlah_posting_kecamatan = $w1->num_rows(); 
										$no=$no+1;
										echo '
													<tr>
														<td>
														'.$no.'
														</td>
														<td>
															<a href="https://'.$h->domain.'" class="text-muted" target="_blank">
															'.$h->domain.'
															</a>
														</td>
														<td>
														'.$h->kecamatan_nama.'
														</td>
														<td>
															<a href="'.base_url().'dashboard/posting/'.$h->domain.'" class="text-muted">
															<span class="text-danger">
															'.$jumlah_posting_kecamatan.'
															</span>
															</a>
														</td>
														<td>';
                            $w12 = $this->db->query("SELECT *
                              FROM posting
                              WHERE domain='".$h->domain."'
                              AND status != 99
                              ORDER BY created_time DESC
                              limit 1
                              ");
                            foreach($w12->result() as $h12){echo $h12->created_time;}
                            echo '
														</td>
														<td>
                              Twitter: <a href="'.$h->twitter.'" class="text-muted"><span class="text-danger">'.$h->twitter.'</span></a><br />
                              Facebook: <a href="'.$h->facebook.'" class="text-muted"><span class="text-danger">'.$h->facebook.'</span></a><br />
                              Google: <a href="'.$h->google.'" class="text-muted"><span class="text-danger">'.$h->google.'</span></a><br />
                              Instagram: <a href="'.$h->instagram.'" class="text-muted"><span class="text-danger">'.$h->instagram.'</span></a><br />
                              Email: <a href="" class="text-muted"><span class="text-danger">'.$h->email.'</span></a><br />
                              Telp.: <a href="" class="text-muted"><span class="text-danger">'.$h->telpon.'</span></a><br />
                              Alamat: <a href="" class="text-muted"><span class="text-danger">'.$h->alamat.'</span></a>
														</td>
													</tr>
										';
										}

								?>

							</tbody>
						</table>
					</div>
        		</div>
      		</div>
      	</div>
		
    </section>

<script>
  $(function () {
    $('#tablekecamatan').DataTable({
      "paging": false,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>