
                    <input name="id_komoditi" id="id_komoditi" value="<?php if(!empty($id_komoditi)){echo $id_komoditi; };?>" type="hidden" value="">
                    <div class="panel panel-primary" style="background-color: #d9edf7 !important;">
                      <div class="panel-heading">
                        <div class="panel-title" style="background-color: #d9edf7 !important; padding:10px;">
                          <?php
                          if(!empty($parent)){ $parent; } 
                            $w1 = $this->db->query("SELECT komoditi.judul_komoditi from komoditi where komoditi.id_komoditi = ".$parent." limit 1");
                            foreach($w1->result() as $h1){
                              $judul_komoditi1 = $h1->judul_komoditi;
                              }
                              echo $judul_komoditi1;
                          ?>
                          <?php if(!empty($judul_komoditi)){ echo ' '.$judul_komoditi.''; } ?>
                        </div>
                      </div>
                      <div class="panel-body">
												<center style="background-color: #d9edf7 !important; padding:10px;">
                          <div style="">
                            <div class="row">
                              <div class="col-md-3">
                                <select name="bulan" class="form-control" id="bulan">
                                  <option value="01">Januari</option>
                                  <option value="02">Februari</option>
                                  <option value="03">Maret</option>
                                  <option value="04">April</option>
                                  <option value="05">Mei</option>
                                  <option value="06">Juni</option>
                                  <option value="07">Juli</option>
                                  <option value="08">Agustus</option>
                                  <option value="09">September</option>
                                  <option value="10">Oktober</option>
                                  <option value="11">November</option>
                                  <option value="12">Desember</option>
                                </select>
                              </div>
                              <div class="col-md-3">
                                <select name="tahun" class="form-control" id="tahun">
                                  <?php
                                  $thn_skr = date('Y');
                                  for ($x = $thn_skr; $x >= 2015; $x--) {
                                  ?>
                                      <option value="<?php echo $x ?>"><?php echo $x ?></option>
                                  <?php
                                  }
                                  ?>
                                </select>
                              </div>
                              <div class="col-md-3">
                                <select name="id_pasar" class="form-control" id="id_pasar">
                                </select>
                              </div>
                              <div class="col-md-3">
                                <div class="input-group-btn">
                                  <button class="btn btn-block btn-primary btn-lg" id="tampilkan_data_harga_kebutuhan_pokok"><i class="fa fa-search"></i> Tampil</button>
                                </div>
                              </div>
                            </div>
                          </div>
												</center>
                        <div class="chart-responsive" id="lineChart">
                        </div>
                      </div>
                      <div class="panel-footer">
                        <div class="alert alert-warning alert-dismissable">
                          <p><i class="icon fa fa-info"></i> Harga <?php echo ''.$judul_komoditi1.' '; ?><?php if(!empty($judul_komoditi)){ echo ' '.$judul_komoditi.''; } ?> dalam sebulan</p>
                        </div>
                        <div class="table-responsive">
                          <table class="table table-striped table-bordered">
                            <thead>
                              <th>Nomor</th>
                              <!--<th>Nama Komoditi<th>-->
                              <th>Tanggal</th>
                              <th>Harga</th>
                            </thead>
                            <tbody id="tbl_data_harga_kebutuhan_pokok">
                            </tbody>
                          </table>
                          <div class="overlay" id="spinners_tbl_data_harga_kebutuhan_pokok" style="display:none;">
                            <i class="fa fa-refresh fa-spin"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                    
<script>
  function load_option_pasar() {
    $('#id_pasar').html('');
    $('#spinners_id_pasar').show();
  	$.ajax({
  		dataType: "json",
  		url: '<?php echo base_url('pasar/load_id_pasar_by_filter') ?>',
  		success: function(json) {
  			var trHTML = '';
  					// trHTML += '<option value="0">Pilih Pasar</option>';
  			for (var i = 0; i < json.length; i++) {
  					trHTML += '<option selected value="'+json[i].id_pasar+'">'+json[i].nama_pasar+'</option>';
  				}
  			$('#id_pasar').append(trHTML); 
  		}
  	});
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_option_pasar();
  load_data();
  load_lineChart();
});
</script>
<script type="text/javascript">
  function load_data() {
    var id_pasar = $('#id_pasar').val();
    var tahun = $('#tahun').val();
    var bulan = $('#bulan').val();
    var id_komoditi = $('#id_komoditi').val();
    $('#tbl_data_harga_kebutuhan_pokok').html('');
    $('#spinners_tbl_data_harga_kebutuhan_pokok').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_komoditi:id_komoditi,
        id_pasar:id_pasar,
        tahun:tahun,
        bulan:bulan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>dashboard/data_perbulan_harga_kebutuhan_pokok/',
      success: function(html) {
        $('#tbl_data_harga_kebutuhan_pokok').html(html);
        $('#spinners_tbl_data_harga_kebutuhan_pokok').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilkan_data_harga_kebutuhan_pokok').on('click', function(e) {
    load_data();
    load_lineChart();
    });
  });
</script>
<script>
  function load_lineChart() {
    $('#spinners_data').show();
    $('#lineChart').html('');
    var id_pasar = $('#id_pasar').val();
    var tahun = $('#tahun').val();
    var bulan = $('#bulan').val();
    var id_komoditi = $('#id_komoditi').val();
  	$.ajax({
  		type: "POST",
  		async: true, 
  		data: { 
        id_komoditi:id_komoditi,
        id_pasar:id_pasar,
        tahun:tahun,
        bulan:bulan
  			  }, 
  		dataType: "html",
  		url: '<?php echo base_url(); ?>dashboard/lineChart',   
  			success: function(response) {
  				$('#lineChart').append(response);
  				$('#spinners_data').fadeOut( "slow" );  
  			}
  		});
  }
</script>

