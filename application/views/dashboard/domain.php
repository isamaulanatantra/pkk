
          <!-- /.col-md-6 -->
          <div class="col-lg-6">
						<div class="card">
              <div class="card-header no-border">
                <h3 class="card-title">Integrated Data Website Kabupaten Wonosobo</h3>
                <div class="card-tools">
                  <a href="#" class="btn btn-tool btn-sm">
                    <i class="fa fa-download"></i>
                  </a>
                  <a href="#" class="btn btn-tool btn-sm">
                    <i class="fa fa-bars"></i>
                  </a>
                </div>
              </div>
              <div class="card-body p-0">
                <table class="table table-striped table-valign-middle">
                  <thead>
                  <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th>Jumlah</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>
                      
                      Website Kelurahan
                    </td>
                    <td>
										</td>
                    <td>
                    </td>
                    <td>
                      <a href="<?php echo base_url(); ?>dashboard/kelurahan" class="text-muted">
							<?php

								$w = $this->db->query("SELECT dasar_website.domain 
								from dasar_website, data_desa
								where data_desa.desa_website = dasar_website.domain
								and dasar_website.status = 1
								and data_desa.jenis_pemerintahan = 1
								");
								$jumlah_website_kelurahan = $w->num_rows(); 
						  		echo '
									<span class="text-danger">
									'.$jumlah_website_kelurahan.'
									</span>
								';
							?>
                      </a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      
                      Website Desa
                    </td>
                    <td></td>
                    <td>
                    </td>
                    <td>
                      <a href="<?php echo base_url(); ?>dashboard/desa" class="text-muted">
							<?php

								$w = $this->db->query("SELECT dasar_website.domain 
								from dasar_website, data_desa
								where data_desa.desa_website = dasar_website.domain
								and dasar_website.status = 1
								and data_desa.jenis_pemerintahan = 0
								");
								$jumlah_website_desa = $w->num_rows();
						  		echo '
									<span class="text-danger">
									'.$jumlah_website_desa.'
									</span>
								';
							?>
						</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      
                      Website Kecamatan
                    </td>
                    <td></td>
                    <td>
                    </td>
                    <td>
                      	<a href="<?php echo base_url(); ?>dashboard/kecamatan" class="text-muted">
							<?php

								$w = $this->db->query("SELECT dasar_website.domain 
								from dasar_website, data_kecamatan
								where data_kecamatan.kecamatan_website = dasar_website.domain
								and dasar_website.status = 1
								");
								$jumlah_website_kecamatan = $w->num_rows(); 
						  		echo '
									<span class="text-danger">
									'.$jumlah_website_kecamatan.'
									</span>
								';
							?>
						</a>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      
                      Website Organisasi Pemerintah Daerah
                    </td>
                    <td></td>
                    <td>
                    </td>
                    <td>
                      	<a href="<?php echo base_url(); ?>dashboard/opd" class="text-muted">
						<?php

							$w = $this->db->query("SELECT dasar_website.domain 
							from dasar_website, data_skpd
							where data_skpd.skpd_website = dasar_website.domain
							and data_skpd.status = 1
							and data_skpd.id_kecamatan = 0
							");
							$jumlah_website_opd = $w->num_rows(); 
						  		echo '
									<span class="text-danger">
									'.$jumlah_website_opd.'
									</span>
								';
						?>
						</a>
                    </td>
                  </tr>
                  </tbody>
                </table>
              </div>
            </div>
			  
          </div>
		<?php
			$this->load->view('dashboard/aduan.php');
		?>
