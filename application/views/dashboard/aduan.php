<?php $web=$this->uut->namadomain(base_url()); ?>

					<div class="col-md-12">
            <div class="card bg-success-gradient">
							<div class="card-header">
								<h3 class="card-title">Pengaduan Masyarakat</h3>

								<div class="card-tools">
									<button type="button" class="btn btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                  </button>
								</div>
							</div>
              <div class="card-body bg-white p-0">
								<table class="table table-hover table-valign-middle">
                  <tbody>
									<?php
									$w52 = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 1
									and tujuan_penggunaan_informasi ='Pengaduan Masyarakat'
									and domain ='".$web."'
									and parent = 0
									and balas = 0
									limit 5
									");
									$w51 = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 1
									and tujuan_penggunaan_informasi ='Pengaduan Masyarakat'
									and domain ='".$web."'
									and parent = 0
									and balas = 0
									");
									$w5balas = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 1
									and tujuan_penggunaan_informasi ='Pengaduan Masyarakat'
									and domain ='".$web."'
									and parent = 0
									and balas = 1
									");
									$total_forwardp = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 3
									and kategori_permohonan_informasi_publik ='Pengaduan Masyarakat'
									and forward ='".$web."'
									and parent = 0
									and balas = 0
									");
									$sudah_forwardp = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 3
									and kategori_permohonan_informasi_publik ='Pengaduan Masyarakat'
									and forward ='".$web."'
									and parent = 0
									and balas = 1
									");
									echo '
										<tr>
											<td>
												<span class="text-success">Total Sudah di balas <span>
											</td>
											<td colspan="2">
												<span class="text-success"><i class="fa fa-comments"></i> '.$w5balas->num_rows().'<span>
											</td>
										</tr>
									';
									echo '
										<tr>
											<td>
												<span class="text-danger">Total Belum di balas <span>
											</td>
											<td colspan="2">
												<span class="text-danger"><i class="fa fa-comments"></i> '.$w51->num_rows().'<span>
											</td>
										</tr>
									';
									foreach ($w52->result() as $row1)
										{
											echo '
												<tr>
													<td colspan="2">
														<a target="_blank" href="'.base_url().'pengaduan_masyarakat/detail/'.$row1->id_permohonan_informasi_publik.'" class="text-muted">
															'.$row1->rincian_informasi_yang_diinginkan.'
														</a>
													</td>
													<td>
														<a target="_blank" href="'.base_url().'pengaduan_masyarakat/detail/'.$row1->id_permohonan_informasi_publik.'" class="nav-link">
															<i class="fa fa-search"></i> Balas
														</a>
													</td>
												</tr>
											';
										}
										echo'
										<tr>
											<td>
												<span class="text-success">Total Forward Sudah di balas <span>
											</td>
											<td colspan="2">
												<span class="text-success"><i class="fa fa-comments"></i> '.$sudah_forwardp->num_rows().'<span>
											</td>
										</tr>
										<tr>
											<td>
												<span class="text-danger">Total Forward Belum di balas <span>
											</td>
											<td colspan="2">
												<span class="text-danger"><i class="fa fa-comments"></i> '.$total_forwardp->num_rows().'<span>
											</td>
										</tr>
										';
									foreach ($total_forwardp->result() as $rowtotal_forwardp)
										{
											echo '
												<tr>
													<td colspan="2">
														<a target="_blank" href="https://'.$rowtotal_forwardp->forward.'/pengaduan_masyarakat/detail/'.$rowtotal_forwardp->id_permohonan_informasi_publik.'" class="text-muted">
															'.$rowtotal_forwardp->rincian_informasi_yang_diinginkan.'
														</a>
													</td>
													<td>
														<a target="_blank" href="https://'.$rowtotal_forwardp->forward.'/pengaduan_masyarakat/detail/'.$rowtotal_forwardp->id_permohonan_informasi_publik.'" class="nav-link">
															<i class="fa fa-search"></i> Balas
														</a>
													</td>
												</tr>
											';
										}
									?>
                  </tbody>
								</table>
              </div>
              <div class="card-footer">
              </div>
            </div>
					</div>
					<div class="col-md-12">
            <div class="card bg-info-gradient">
							<div class="card-header">
								<h3 class="card-title">Permohonan Informasi Publik</h3>

								<div class="card-tools">
									<button type="button" class="btn btn-primary btn-sm" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                    <i class="fa fa-minus"></i>
                  </button>
								</div>
							</div>
              <div class="card-body bg-white p-0">
								<table class="table table-hover table-valign-middle">
                  <tbody>
									<?php
									$w52 = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 1
									and tujuan_penggunaan_informasi !='Pengaduan Masyarakat'
									and domain ='".$web."'
									and parent = 0
									and balas = 0
									limit 5
									");
									$w51 = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 1
									and tujuan_penggunaan_informasi !='Pengaduan Masyarakat'
									and domain ='".$web."'
									and parent = 0
									and balas = 0
									");
									$w5balas = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 1
									and tujuan_penggunaan_informasi !='Pengaduan Masyarakat'
									and domain ='".$web."'
									and parent = 0
									and balas = 1
									");
									$total_forward = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 3
									and tujuan_penggunaan_informasi !='Pengaduan Masyarakat'
									and forward ='".$web."'
									and parent = 0
									and balas = 0
									");
									$sudah_forward = $this->db->query("
									SELECT *
									from permohonan_informasi_publik
									where status = 3
									and tujuan_penggunaan_informasi !='Pengaduan Masyarakat'
									and forward ='".$web."'
									and parent = 0
									and balas = 1
									");
									echo '
										<tr>
											<td>
												<span class="text-success">Total Sudah di balas <span>
											</td>
											<td colspan="2">
												<span class="text-success"><i class="fa fa-comments"></i> '.$w5balas->num_rows().'<span>
											</td>
										</tr>
										<tr>
											<td>
												<span class="text-danger">Total Belum di balas <span>
											</td>
											<td colspan="2">
												<span class="text-danger"><i class="fa fa-comments"></i> '.$w51->num_rows().'<span>
											</td>
										</tr>
									';
									foreach ($w52->result() as $row1)
										{
											echo '
												<tr>
													<td colspan="2">
														<a target="_blank" href="'.base_url().'permohonan_informasi_publik/detail/'.$row1->id_permohonan_informasi_publik.'" class="text-muted">
															'.$row1->rincian_informasi_yang_diinginkan.'
														</a>
													</td>
													<td>
														<a target="_blank" href="'.base_url().'permohonan_informasi_publik/detail/'.$row1->id_permohonan_informasi_publik.'" class="nav-link">
															<i class="fa fa-search"></i> Balas
														</a>
													</td>
												</tr>
											';
										}
										echo'
										<tr>
											<td>
												<span class="text-success">Total Forward Sudah di balas <span>
											</td>
											<td colspan="2">
												<span class="text-success"><i class="fa fa-comments"></i> '.$sudah_forward->num_rows().'<span>
											</td>
										</tr>
										<tr>
											<td>
												<span class="text-danger">Total Forward Belum di balas <span>
											</td>
											<td colspan="2">
												<span class="text-danger"><i class="fa fa-comments"></i> '.$total_forward->num_rows().'<span>
											</td>
										</tr>
										';
									foreach ($total_forward->result() as $rowtotal_forward)
										{
											echo '
												<tr>
													<td colspan="2">
														<a target="_blank" href="https://'.$rowtotal_forward->forward.'/permohonan_informasi_publik/detail/'.$rowtotal_forward->id_permohonan_informasi_publik.'" class="text-muted">
															'.$rowtotal_forward->rincian_informasi_yang_diinginkan.'
														</a>
													</td>
													<td>
														<a target="_blank" href="https://'.$rowtotal_forward->forward.'/permohonan_informasi_publik/detail/'.$rowtotal_forward->id_permohonan_informasi_publik.'" class="nav-link">
															<i class="fa fa-search"></i> Balas
														</a>
													</td>
												</tr>
											';
										}
									?>
                  </tbody>
								</table>
              </div>
              <div class="card-footer">
              </div>
            </div>
					</div>
					<div class="col-md-12">
            <div class="card">
              <div class="card-header no-border bg-warning">
                <h3 class="card-title">Posting</h3>
                <div class="card-tools">
                  <a href="#" class="btn btn-sm btn-tool">
                    <i class="fa fa-download"></i>
                  </a>
                  <a href="#" class="btn btn-sm btn-tool">
                    <i class="fa fa-bars"></i>
                  </a>
                </div>
              </div>
              <div class="card-body no-padding no-margin">
								<table class="table table-hover table-valign-middle">
                  <tbody>
										<?php
										$query_posting = $this->db->query("
										SELECT *
										from posting
										where status = 1
										and domain ='".$web."'
										");
										echo'
												<tr>
													<td>
														Total Posting
													</td>
													<td colspan="2">'.$query_posting->num_rows().'</td>
												</tr>
										';
											$query_posting1 = $this->db->query("
											SELECT *
											from posting
											where status = 1
											and domain ='".$web."'
											order by created_time desc
											limit 1
											");
											foreach($query_posting1->result() as $row_posting1){
												echo '
												<tr>
													<td>
														Terakhir Posting
													</td>
													<td colspan="2">'.$this->Crud_model->dateBahasaIndo1($row_posting1->created_time).'</td>
												</tr>
											';
											}
									$qposting = $this->db->query("
										SELECT *
										from posting
										where status = 1
										and domain ='".$web."'
										order by created_time desc
										limit 1
									");
									foreach ($qposting->result() as $rposting)
										{
										$healthy = array(" ", ",", ".", "-", ":", "=", "!", "?", "(", ")", "@", "%", "&", "/");
										$yummy   = array("_","","","","","","","","","","","","","");
										$newphrase = str_replace($healthy, $yummy, $rposting->judul_posting);
											echo '
												<tr>
													<td>
														Posting Terbaru
													</td>
													<td>
														<a href="'.base_url().'postings/detail/'.$rposting->id_posting.'/'.$newphrase.'.HTML" class="nav-link">
															'.$rposting->judul_posting.'
														</a>
													</td>
													<td>
														<a href="'.base_url().'posting" class="nav-link">
															<i class="fa fa-plus"></i> Tambah Baru
														</a>
													</td>
												</tr>
											';
										}
										?>
                  </tbody>
                </table>
              </div>
            </div>
					</div>