
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>POSTING</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Posting</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

		<div class="row" id="awal">
          	<div class="col-12">
           		<div class="card">
					<div class="card-body">
						<table class="table table-striped table-valign-middle">
							<thead>
							<tr>
								<th>Judul</th>
								<th>Urut</th>
								<th>Tampil di Informasi Terkini</th>
							</tr>
							</thead>
							<tbody id="tbl_utama_posting">


							</tbody>
						</table>
						<div class="overlay" id="spinners_data" style="display:none;">
							<i class="fa fa-refresh fa-spin"></i>
						</div>
					</div>
        		</div>
      		</div>
      	</div>
		
    </section>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
  load_data_posting();
});
</script>
 
<script>
  function load_data_posting(halaman, limit) {
    $('#tbl_utama_posting').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        domain: '<?php echo $this->uri->segment(3); ?>',
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>dashboard/load_table_posting/',
      success: function(html) {
        $('#tbl_utama_posting').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 