 
<!DOCTYPE html>
<html lang="id-ID">
  <head>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139860546-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-139860546-2');
</script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Website Resmi Dinas Pendidikan Kota Semarang">
    <meta name="keyword" content="disdik semarang, disdik kota semarang, pendidikan semarang">
    <meta name="author" content="dinustek, Rudi Kurniawan">
    <!-- browser color -->
    <meta name="theme-color" content="#db241c">
    <meta name="msapplication-navbutton-color" content="#db241c">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<!--
    <link rel="shortcut icon" href="http://disdik.semarangkota.go.id/v15/assets/new/assets/images/favicon.png">
    <script type="text/javascript" src="<?php echo base_url(); ?>Template/zonasi/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>Template/zonasi/js/select2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>Template/zonasi/css/jquery.dataTables.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>Template/zonasi/css/select2.css" media="all">
    
    -->
    <title>Disdik Kota Semarang</title>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>Template/zonasi/css/main.css" media="all">

    <script type="text/javascript" src="<?php echo base_url(); ?>Template/zonasi/js/app.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <a href="#" class="btn btn-warning btn-sm text-center" style="position:fixed;z-index: 5; right: 15px;bottom: 45px;padding:5px 10px;">
      <i class="fa fa-arrow-up fa-2x"></i><br>
      <!-- ke atas<br>halaman -->
    </a>
    
    <header>
      <div id="menu-utama" class="navbar navbar-default navbar-static-top">
        <div class="container">
          <ul class="nav navbar-nav clearfix" style="float:none;position: absolute; right:0;">
            <li><a target="_blank" href="https://www.facebook.com/groups/127893867721144/"><img src="http://disdik.semarangkota.go.id/v15/assets/new/assets/images/icon-facebook.png" alt="" width="30"></a></li>
            <li><a target="_blank" href="https://twitter.com/disdik_kotasmg"><img src="http://disdik.semarangkota.go.id/v15/assets/new/assets/images/icon-twitter.png" alt="" width="30"></a></li>
            <li><a target="_blank" href="https://www.instagram.com/dinas_pendidikan_kota_semarang/"><img src="http://disdik.semarangkota.go.id/v15/assets/new/assets/images/icon-instagram.png" alt="" width="30"></a></li>
            <li><a target="_blank" href="https://www.youtube.com/channel/UChg0EqsfuPGWcruOTvPR-JA?view_as=subscriber"><img src="http://disdik.semarangkota.go.id/v15/assets/new/assets/images/icon-youtube.png" alt="" width="30"></a></li>
          </ul>
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="http://disdik.semarangkota.go.id/v15/">
              <h1 class="text-hide">Disdik Kota Semarang</h1>
              <img src="http://disdik.semarangkota.go.id/v15/assets/new/assets/images/disdik-logo.jpg" alt="Dinas Pendidikan Kota Semarang">
            </a>
          </div>
          <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right hidden-md">
              <li>
                <button class="btn btn-primary" data-toggle="modal" data-target="#modalCari">
                  <i class="glyphicon glyphicon-search"></i>
                </button>
              </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="http://disdik.semarangkota.go.id/v15/">Beranda</a></li>
                                                        <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PPID <span class="caret"></span></a>
                <ul class="dropdown-menu">
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/page/83/dasar-hukum" >Dasar Hukum</a></li>
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/page/96/layanan-informasi" >Layanan Informasi</a></li>
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/page/100/alur-pelayanan" >Alur Pelayanan</a></li>
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/page/77/profil-ppid" >Profil PPID</a></li>
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/page/71/layanan-aspirasi-dan-pengaduan-online" >Layanan Aspirasi dan Pengaduan Online</a></li>
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/page/101/daftar-website-smp-negeri-di-kota-semarang" >Daftar Website SMP Negeri di Kota Semarang</a></li>
                                </ul>
              </li>
                                                                    <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profil <span class="caret"></span></a>
                <ul class="dropdown-menu">
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/page/99/tugas-fungsi" >Tugas & Fungsi</a></li>
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/page/80/susunan-organisasi-dinas-pendidikan" >Susunan Organisasi Dinas Pendidikan</a></li>
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/page/38/struktur-organisasi" >Struktur Organisasi</a></li>
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/page/26/visi-dan-misi" >Visi dan Misi</a></li>
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/pegawai" target="_blank">Data Pegawai</a></li>
                                                <li><a href="http://disdik.semarangkota.go.id/v15/main/page/40/daftar-email-lingkungan-dinas-pendidikan" target="_blank">Daftar Email Lingkungan Dinas Pendidikan</a></li>
                                </ul>
              </li>
                                                                    <li><a href="http://disdik.semarangkota.go.id/v15/main/bukutamu" target="_blank">Buku Tamu</a></li>
                                                                    <li><a href="http://disdik.semarangkota.go.id/v15/main/page/6/kontak" >Kontak</a></li>
                                                                    <li><a href="http://disdik.semarangkota.go.id/v15/main/jurnal" target="_blank">E-Jurnal</a></li>
                                                                    <li><a href="http://disdik.semarangkota.go.id/v15/main/page/88/kursus-hebat" >Kursus Hebat</a></li>
                                                                    <li><a href="https://apbs.semarangkota.go.id/" target="_blank">APBS ONLINE</a></li>
                                      </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>      
    </header>

<!-- Modal -->
<div class="modal fade" id="modalCari" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Pencarian Berita</h4>
            </div>
            <div class="modal-body">
              <p>Ketikkan kata kunci untuk mencari berita</p>
              <!-- <form role="search"> -->
                <div class="row">
                  <div class="col-lg-12">
                    <div class="input-group input-group-lg">
                      <input id="param_cari" type="text" class="form-control" placeholder="Cari berita">
                      <span class="input-group-btn">
                        <button id="pencarian" class="btn btn-primary">Cari</button>
                      </span>
                    </div><!-- /input-group -->
                  </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
              <!-- </form> -->
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<div class="container">

<div id="headline">
      <div class="slick-carousel-headline">

              <figure>
          <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita190619021757-373.jpg" alt="" class="img-responsive">
          <figcaption>
            <div class="container">
              <h1><a href="http://disdik.semarangkota.go.id/v15/main/read/8/sekretariat/373/sosialisasi-saber-pungli">
              SOSIALISASI SABER PUNGLI</a></h1>
                            <p>19-06-2019 &nbsp; 14:39 WIB &nbsp; <a href="#">Sekretariat</a></p>
            </div>
          </figcaption>
        </figure>
              <figure>
          <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita180619122902-372.jpg" alt="" class="img-responsive">
          <figcaption>
            <div class="container">
              <h1><a href="http://disdik.semarangkota.go.id/v15/main/read/9/monbang/372/penyerahan-penghargaan-juara-o2sd-sd-kota-semarang-tahun-2019">
              Penyerahan Penghargaan Juara O2SD-SD Kota Semarang Tahun 2019</a></h1>
                            <p>18-06-2019 &nbsp; 13:49 WIB &nbsp; <a href="#">Sekretariat</a></p>
            </div>
          </figcaption>
        </figure>
              <figure>
          <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita220519033959-370.jpg" alt="" class="img-responsive">
          <figcaption>
            <div class="container">
              <h1><a href="http://disdik.semarangkota.go.id/v15/main/read/8/sekretariat/370/penyerahan-pentasyarufan-dana-zakat-infaq-dan-sedekah-zis">
              Penyerahan / Pentasyarufan Dana Zakat, Infaq dan Sedekah (ZIS)</a></h1>
                            <p>22-05-2019 &nbsp; 15:47 WIB &nbsp; <a href="#">Sekretariat</a></p>
            </div>
          </figcaption>
        </figure>
              <figure>
          <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita200519030328-370.jpg" alt="" class="img-responsive">
          <figcaption>
            <div class="container">
              <h1><a href="http://disdik.semarangkota.go.id/v15/main/read/8/sekretariat/369/upacara-peringatan-hari-kebangkitan-nasional-ke-111">
              Upacara Peringatan Hari Kebangkitan Nasional ke 111</a></h1>
                            <p>20-05-2019 &nbsp; 14:55 WIB &nbsp; <a href="#">Sekretariat</a></p>
            </div>
          </figcaption>
        </figure>
      
      </div>
    </div>

</div>    <!-- Running Text -->
  <div class="container">
    <section id="running-text">
      <div class="infoscroll">
        <span>
          <a href="#"><strong>Info </strong></a>
        </span>
        <!-- <a href="#" id="prev" class="prev" title="previous"><strong>&#9668;</strong></a> -->
        <!-- <a href="#" id="next" class="next" title="next"><strong>&#9658;</strong></a> -->
                <div>
          <ul>
                                    <ul id="infoscroll" class="clearfix list-unstyled slick-carousel-running-teks">
	<li>
		Kami Segenap Keluarga Besar Dinas Pendidikan Kota Semarang&nbsp;</li>
	<li>
		Mengucapkan SELAMAT HARI JADI KOTA SEMARANG Yang ke 472 dan HARI PENDIDIKAN NASIONAL</li>
	<li>
		Mari bergerak bersama ciptakan kualitas pendidikan di Kota Semarang yang semakin hebat!</li>
	<li>
		---------</li>
	<li>
		Informasi PPDB Kota Semarang bisa diakses di <em><strong><a href="https://ppd.semarangkota.go.id/">SINI</a></strong></em></li>
	<li>
		https://ppd.semarangkota.go.id/</li>
</ul>
                      </ul>
        </div>
      </div>
    </section>
  </div>
<div class="container">
  <div class="row">
  
    
    <div class="col-md-9 col-md-push-3">
      <div class="panel panel-section panel-default">
        <div class="panel-body">
          <div class="section-heading"><h1>Pengumuman</h1> <a href="http://disdik.semarangkota.go.id/v15/main/pengumuman" class="indeks pull-right">indeks pengumuman</a></div>
          <div class="row">
                    
            <div class="col-md-6">
                            <article>
                <h1><a href="http://disdik.semarangkota.go.id/v15/main/baca/6/1393/laporan-pns-yang-akan-menunaikan-ibadah-haji-1440-h-tahun-2019">
                Laporan PNS yang akan menunaikan Ibadah Haji 1440 H Tahun 2019                </a></h1>
                                <div class="artikel-waktu">19-06-2019 &nbsp; 13:54 WIB &nbsp; <a href="#">Admin</a></div>
                <p class="artikel-cuplikan" style="text-align:justify"><p>
	<span style="font-size: 16px;">Dengan hormat disampaikan edaran terkait dengan Laporan PNS yang akan menunaikan Ibadah Haji 1440 H Tahun 2019.</span></p>
<div>
	&nbsp;</div>
</p>
              </article>
                                <hr>
                                            <article>
                <h1><a href="http://disdik.semarangkota.go.id/v15/main/baca/5/1392/pelaksanaan-penerbitan-kartu-identitas-anak-kia-di-sekolah">
                Pelaksanaan Penerbitan Kartu Identitas Anak (KIA) di sekolah                </a></h1>
                                <div class="artikel-waktu">18-06-2019 &nbsp; 10:17 WIB &nbsp; <a href="#">Admin</a></div>
                <p class="artikel-cuplikan" style="text-align:justify"><p>
	<span style="font-size: 16px;">Disampaikan dengan hormat edaran terkait Pelaksanaan Penerbitan Kartu Identitas Anak (KIA) di sekolah.</span></p>
</p>
              </article>
                                          </div>
            <div class="col-md-6">
              <div class="scrollbox" style="height: 410px; overflow: hidden;">
                <ul class="list-group">
              
                              <li class="list-group-item">
                <h1 class="list-group-item-heading artikel-judul"><a href="http://disdik.semarangkota.go.id/v15/main/baca/5/1391/pelatihan-ecobriks-untuk-guru-sd-dan-smp-di-kota-semarang">
                Pelatihan Ecobriks untuk Guru SD dan SMP di Kota Semarang                </a></h1>
                                <div class="artikel-waktu">11-06-2019 &nbsp; 08:15 WIB &nbsp; <a href="#">Admin</a></div>
                </li>
                              <li class="list-group-item">
                <h1 class="list-group-item-heading artikel-judul"><a href="http://disdik.semarangkota.go.id/v15/main/baca/5/1390/larangan-pemberian-dan-penerimaan-gratifikasi-menjelang-hari-raya-idul-fitri-1440-h">
                Larangan Pemberian dan Penerimaan Gratifikasi Menjelang Hari Raya Idul Fitri 1440 H                </a></h1>
                                <div class="artikel-waktu">28-05-2019 &nbsp; 14:54 WIB &nbsp; <a href="#">Admin</a></div>
                </li>
                              <li class="list-group-item">
                <h1 class="list-group-item-heading artikel-judul"><a href="http://disdik.semarangkota.go.id/v15/main/baca/2/1389/pengumuman-hasil-seleksi-ppdb-jenjang-sd-tahun-20192020">
                PENGUMUMAN HASIL SELEKSI PPDB JENJANG SD TAHUN 2019/2020                </a></h1>
                                <div class="artikel-waktu">27-05-2019 &nbsp; 04:23 WIB &nbsp; <a href="#">Admin</a></div>
                </li>
                              <li class="list-group-item">
                <h1 class="list-group-item-heading artikel-judul"><a href="http://disdik.semarangkota.go.id/v15/main/baca/5/1388/undangan-pelatihan">
                Undangan pelatihan                </a></h1>
                                <div class="artikel-waktu">17-05-2019 &nbsp; 03:24 WIB &nbsp; <a href="#">Admin</a></div>
                </li>
                              <li class="list-group-item">
                <h1 class="list-group-item-heading artikel-judul"><a href="http://disdik.semarangkota.go.id/v15/main/baca/5/1387/pelaksanaan-upacara-hari-kebangkitan-nasional-tahun-2019">
                Pelaksanaan Upacara Hari Kebangkitan Nasional Tahun 2019                </a></h1>
                                <div class="artikel-waktu">16-05-2019 &nbsp; 19:46 WIB &nbsp; <a href="#">Admin</a></div>
                </li>
                              <li class="list-group-item">
                <h1 class="list-group-item-heading artikel-judul"><a href="http://disdik.semarangkota.go.id/v15/main/baca/5/1386/undangan-sholat-tarawih-bersama-keluarga-besar-dinas-pendidikan-kota-semarang">
                Undangan Sholat Tarawih bersama Keluarga Besar Dinas Pendidikan Kota Semarang.                </a></h1>
                                <div class="artikel-waktu">13-05-2019 &nbsp; 14:55 WIB &nbsp; <a href="#">Admin</a></div>
                </li>
                              <li class="list-group-item">
                <h1 class="list-group-item-heading artikel-judul"><a href="http://disdik.semarangkota.go.id/v15/main/baca/6/1385/edaran-kenaikan-pangkat-periode-1-oktober-2019">
                Edaran Kenaikan Pangkat Periode 1 Oktober 2019                </a></h1>
                                <div class="artikel-waktu">10-05-2019 &nbsp; 09:49 WIB &nbsp; <a href="#">Admin</a></div>
                </li>
                              <li class="list-group-item">
                <h1 class="list-group-item-heading artikel-judul"><a href="http://disdik.semarangkota.go.id/v15/main/baca/2/1384/undangan-bimbingan-teknis-ppdb-tahun-pelajaran-20192020">
                Undangan Bimbingan Teknis PPDB Tahun Pelajaran 2019/2020                </a></h1>
                                <div class="artikel-waktu">03-05-2019 &nbsp; 10:49 WIB &nbsp; <a href="#">Admin</a></div>
                </li>
              
                </ul>  
              </div>            
            </div>
          </div>
        </div>
      </div>

      <div class="panel panel-section panel-default">
        <div class="panel-body">
          <div class="section-heading"><h1>Berita</h1> <a href="http://disdik.semarangkota.go.id/v15/main/indexberita" class="indeks pull-right">indeks berita</a></div>

                    <!-- Berita terbaru -->
      <div class="row">
        <div class="col-md-6">
                    <figure class="panel-artikel-item" style="width:100%;">
            <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita190619021757-373.jpg" alt="" class="img-responsive gambar-artikel">
            <figcaption>
              <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/read/8/sekretariat/373/sosialisasi-saber-pungli">SOSIALISASI SABER PUNGLI</a></h1>
                            <div class="artikel-waktu">19-06-2019 &nbsp; 14:39 WIB &nbsp; <a href="#">Admin</a></div>
              <p class="artikel-cuplikan" style="text-align:justify">
	Selasa, 18 Juni 2019, Bertempat Di Aula Dinas Pendidikan Kota Semarang Tim Satgas Saber Pungli Kota Semarang memberikan sosialisasi terkait dengan &ldquo;PUNGLI&rdquo; kepada para Kepala Sekolah SD, Kepala Sekolah SMP dan para Koordinator Satuan Pendidikan Kota Semarang.&nbsp;
 ...</p>
            </figcaption>
          </figure>
                  </div>
        <div class="col-sm-6">
        <div class="scrollbox" style="height: 350px; overflow: hidden;">
          <ul class="list-unstyled">
          <!-- // Berita terbaru -->
                    <li class="panel-artikel-item highlight-1">
            <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita180619122902-372_thumb.jpg" alt="" class="img-responsive gambar-artikel">
            <figcaption>
              <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/read/9/monbang/372/penyerahan-penghargaan-juara-o2sd-sd-kota-semarang-tahun-2019">Penyerahan Penghargaan Juara O2SD-SD Kota Semarang Tahun 2019</a></h1>
                            <div class="artikel-waktu pull-left">18-06-2019 &nbsp; 13:49 WIB &nbsp; <br><a href="#">Admin</a></div>
            </figcaption>
          </li>
                    <li class="panel-artikel-item highlight-1">
            <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita010619075858-371_thumb.jpg" alt="" class="img-responsive gambar-artikel">
            <figcaption>
              <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/read/8/sekretariat/371/peringatan-hari-kelahiran-pancasila-1-juni-2019">Peringatan Hari Kelahiran Pancasila 1 Juni 2019</a></h1>
                            <div class="artikel-waktu pull-left">01-06-2019 &nbsp; 08:03 WIB &nbsp; <br><a href="#">Admin</a></div>
            </figcaption>
          </li>
                    <li class="panel-artikel-item highlight-1">
            <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita220519033959-370_thumb.jpg" alt="" class="img-responsive gambar-artikel">
            <figcaption>
              <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/read/8/sekretariat/370/penyerahan-pentasyarufan-dana-zakat-infaq-dan-sedekah-zis">Penyerahan / Pentasyarufan Dana Zakat, Infaq dan Sedekah (ZIS)</a></h1>
                            <div class="artikel-waktu pull-left">22-05-2019 &nbsp; 15:47 WIB &nbsp; <br><a href="#">Admin</a></div>
            </figcaption>
          </li>
                    <li class="panel-artikel-item highlight-1">
            <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita200519030328-370_thumb.jpg" alt="" class="img-responsive gambar-artikel">
            <figcaption>
              <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/read/8/sekretariat/369/upacara-peringatan-hari-kebangkitan-nasional-ke-111">Upacara Peringatan Hari Kebangkitan Nasional ke 111</a></h1>
                            <div class="artikel-waktu pull-left">20-05-2019 &nbsp; 14:55 WIB &nbsp; <br><a href="#">Admin</a></div>
            </figcaption>
          </li>
                    <li class="panel-artikel-item highlight-1">
            <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita200519023636-368_thumb.jpg" alt="" class="img-responsive gambar-artikel">
            <figcaption>
              <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/read/8/sekretariat/368/sholat-tarawih-bersama-keluarga-besar-disdik-kota-semarang">Sholat Tarawih bersama Keluarga Besar Disdik Kota Semarang</a></h1>
                            <div class="artikel-waktu pull-left">20-05-2019 &nbsp; 14:46 WIB &nbsp; <br><a href="#">Admin</a></div>
            </figcaption>
          </li>
                    <li class="panel-artikel-item highlight-1">
            <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita050519114631-367_thumb.jpg" alt="" class="img-responsive gambar-artikel">
            <figcaption>
              <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/read/8/sekretariat/367/lounching-website-ppdb-dinas-pendidikan-kota-semarang-tahun-pelajaran-20192020">Lounching Website PPDB Dinas Pendidikan Kota Semarang Tahun Pelajaran 2019/2020</a></h1>
                            <div class="artikel-waktu pull-left">05-05-2019 &nbsp; 12:21 WIB &nbsp; <br><a href="#">Admin</a></div>
            </figcaption>
          </li>
                    <li class="panel-artikel-item highlight-1">
            <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita300419070718-366_thumb.jpg" alt="" class="img-responsive gambar-artikel">
            <figcaption>
              <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/read/5/dikdasmen/366/porseni-dan-sains-smp-kota-semarang-tahun-2019">Porseni dan Sains SMP Kota Semarang Tahun 2019</a></h1>
                            <div class="artikel-waktu pull-left">30-04-2019 &nbsp; 07:29 WIB &nbsp; <br><a href="#">Admin</a></div>
            </figcaption>
          </li>
                    <li class="panel-artikel-item highlight-1">
            <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita220419013257-365_thumb.jpg" alt="" class="img-responsive gambar-artikel">
            <figcaption>
              <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/read/5/dikdasmen/365/hari-pertama-pelaksanaan-unbk-smp-kota-semarang">Hari Pertama Pelaksanaan UNBK SMP Kota Semarang</a></h1>
                            <div class="artikel-waktu pull-left">22-04-2019 &nbsp; 13:48 WIB &nbsp; <br><a href="#">Admin</a></div>
            </figcaption>
          </li>
                    <li class="panel-artikel-item highlight-1">
            <img src="http://disdik.semarangkota.go.id/v15/content/slides/berita150419040640-364_thumb.jpg" alt="" class="img-responsive gambar-artikel">
            <figcaption>
              <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/read/6/pnfi/364/dua-wbp-lapas-semarang-ikuti-unbk-kesetaraan-paket-c">Dua WBP Lapas Semarang Ikuti UNBK Kesetaraan Paket C</a></h1>
                            <div class="artikel-waktu pull-left">15-04-2019 &nbsp; 16:25 WIB &nbsp; <br><a href="#">Bidang Pembinaan PAUD dan PNF</a></div>
            </figcaption>
          </li>
                    </ul>
        </div>
        </div>
      </div>

        </div>
      </div>

      <!-- masih kosong !-->

      

      <div class="panel panel-section panel-default">
        <div class="panel-body">
          <div class="section-heading"><h1>Data Sekolah</h1> <a href="http://dapo.dikdasmen.kemdikbud.go.id/sp/2/036300" target="_blank" class="indeks pull-right">indeks</a></div>
          <!-- <iframe src="http://dapo.dikdasmen.kemdikbud.go.id/sp/2/036300" style="width:100%; height:500px; border:none"></iframe> -->
        </div>
      </div>
      <!-- masih kosong !-->


      <div id="galeri" class="panel panel-section panel-default">
        <div class="panel-body">
          <div class="section-heading"><h1>Galeri Foto</h1> <a href="http://disdik.semarangkota.go.id/v15/main/media/album" class="indeks pull-right">indeks</a></div>
          <div class="row">
                      <div class="col-md-9">
              <figure class="judul-tumpuk">
                <a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/42/pencanangan-sekolah-ramah-anak-di-smpn-33-semarang-4-april-2019">
                <img src="http://disdik.semarangkota.go.id/v15/content/galeri/08042019112704 1.jpg" alt="Pencanangan Sekolah Ramah Anak di SMPN 33 Semarang, 4 April 2019" class="img-responsive">
                  <figcaption>
                    <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/42/pencanangan-sekolah-ramah-anak-di-smpn-33-semarang-4-april-2019">
                    Pencanangan Sekolah Ramah Anak di SMPN 33 Semarang, 4 April 2019</a></h1>
                  </figcaption>
                </a>
              </figure>
            </div>
                        
            <div class="col-md-3">

            <div class="scrollbox" style="height: 400px; overflow: hidden;">
                <ul class="list-unstyled">
                          <li class="judul-tumpuk">
                <a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/41/upacara-hari-guru-2018-26-nop-2018">
                  <img src="http://disdik.semarangkota.go.id/v15/content/galeri/29112018114611 1.jpg" alt="Upacara Hari Guru 2018 - 26 NOP 2018" class="img-responsive">
                  <figcaption>
                    <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/41/upacara-hari-guru-2018-26-nop-2018">
                    Upacara Hari Guru 2018 - 26 NOP 2018</a></h1>
                  </figcaption>
                </a>
              </li>
                          <li class="judul-tumpuk">
                <a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/40/kunjungan-kerja-dewan-pendidikan-kabkota-jombang-6-november-2018">
                  <img src="http://disdik.semarangkota.go.id/v15/content/galeri/13112018091311 1.jpg" alt="Kunjungan Kerja Dewan Pendidikan Kab/Kota Jombang, 6 November 2018" class="img-responsive">
                  <figcaption>
                    <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/40/kunjungan-kerja-dewan-pendidikan-kabkota-jombang-6-november-2018">
                    Kunjungan Kerja Dewan Pendidikan Kab/Kota Jombang, 6 November 2018</a></h1>
                  </figcaption>
                </a>
              </li>
                          <li class="judul-tumpuk">
                <a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/39/kunjungan-walikota-semarang-ke-sdn-krapyak-5-november-2018">
                  <img src="http://disdik.semarangkota.go.id/v15/content/galeri/13112018083311 4.jpg" alt="Kunjungan Walikota Semarang ke SDN Krapyak , 5 November 2018" class="img-responsive">
                  <figcaption>
                    <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/39/kunjungan-walikota-semarang-ke-sdn-krapyak-5-november-2018">
                    Kunjungan Walikota Semarang ke SDN Krapyak , 5 November 2018</a></h1>
                  </figcaption>
                </a>
              </li>
                          <li class="judul-tumpuk">
                <a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/38/semarang-mengajar-mendidik-generasi-terampil-untuk-meningkatkan-kualitas-kerja-26-juli-2018">
                  <img src="http://disdik.semarangkota.go.id/v15/content/galeri/01082018121808 1.jpg" alt="Semarang Mengajar "Mendidik Generasi Terampil Untuk Meningkatkan Kualitas Kerja" 26 JULI 2018" class="img-responsive">
                  <figcaption>
                    <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/38/semarang-mengajar-mendidik-generasi-terampil-untuk-meningkatkan-kualitas-kerja-26-juli-2018">
                    Semarang Mengajar "Mendidik Generasi Terampil Untuk Meningkatkan Kualitas Kerja" 26 JULI 2018</a></h1>
                  </figcaption>
                </a>
              </li>
                          <li class="judul-tumpuk">
                <a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/37/pisah-sambut-ka-disdik-23-juli-2018">
                  <img src="http://disdik.semarangkota.go.id/v15/content/galeri/01082018115608 1.jpg" alt="Pisah Sambut Ka. Disdik- 23 JULI 2018" class="img-responsive">
                  <figcaption>
                    <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/37/pisah-sambut-ka-disdik-23-juli-2018">
                    Pisah Sambut Ka. Disdik- 23 JULI 2018</a></h1>
                  </figcaption>
                </a>
              </li>
                          <li class="judul-tumpuk">
                <a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/36/sosialisasi-pencegahan-korupsi-melalui-implementasi-tata-kelola-sekolah-berintegritas-oleh-kpk">
                  <img src="http://disdik.semarangkota.go.id/v15/content/galeri/05072018115007 1.jpg" alt="Sosialisasi Pencegahan Korupsi Melalui Implementasi Tata Kelola Sekolah Berintegritas oleh KPK" class="img-responsive">
                  <figcaption>
                    <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/36/sosialisasi-pencegahan-korupsi-melalui-implementasi-tata-kelola-sekolah-berintegritas-oleh-kpk">
                    Sosialisasi Pencegahan Korupsi Melalui Implementasi Tata Kelola Sekolah Berintegritas oleh KPK</a></h1>
                  </figcaption>
                </a>
              </li>
                          <li class="judul-tumpuk">
                <a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/35/upacara-peringatan-hari-kartini-di-kantor-dinas-pendidikan-23-april-2018">
                  <img src="http://disdik.semarangkota.go.id/v15/content/galeri/23042018020104 1 23042018020104.jpg" alt="Upacara Peringatan Hari Kartini di Kantor Dinas Pendidikan - 23 April 2018" class="img-responsive">
                  <figcaption>
                    <h1 class="judul-artikel"><a href="http://disdik.semarangkota.go.id/v15/main/media/galeri/35/upacara-peringatan-hari-kartini-di-kantor-dinas-pendidikan-23-april-2018">
                    Upacara Peringatan Hari Kartini di Kantor Dinas Pendidikan - 23 April 2018</a></h1>
                  </figcaption>
                </a>
              </li>
                            </ul>
            </div>

            </div>
          </div>
        </div>
      </div>



      <div class="panel panel-section panel-default">
        <div class="panel-body">
          <div class="section-heading"><h1>Galeri Video</h1> <a href="http://disdik.semarangkota.go.id/v15/main/media/video" class="indeks pull-right">indeks</a></div>
                    <figure class="panel-artikel-item tiga judul-tumpuk">
            <a href="http://www.youtube.com/watch?v=5VzYZGGpPL8" rel="prettyPhoto" title="Deskripsi video disini">
              <img src="http://disdik.semarangkota.go.id/v15/assets/new/assets/img/default-video-4x3.jpg" alt="" class="img-responsive gambar-artikel">
              <figcaption>
                <h1 class="judul-artikel"><a href="#">Potret Pendidikan Indonesi</a></h1>
              </figcaption>
            </a>
          </figure>
                    <figure class="panel-artikel-item tiga judul-tumpuk">
            <a href="http://www.youtube.com/watch?v=Shqjw4ZAeJE" rel="prettyPhoto" title="Deskripsi video disini">
              <img src="http://disdik.semarangkota.go.id/v15/assets/new/assets/img/default-video-4x3.jpg" alt="" class="img-responsive gambar-artikel">
              <figcaption>
                <h1 class="judul-artikel"><a href="#">LIPUTAN KHUSUS DIK KEL</a></h1>
              </figcaption>
            </a>
          </figure>
                    <figure class="panel-artikel-item tiga judul-tumpuk">
            <a href="http://www.youtube.com/watch?v=0gakPcu70v8" rel="prettyPhoto" title="Deskripsi video disini">
              <img src="http://disdik.semarangkota.go.id/v15/assets/new/assets/img/default-video-4x3.jpg" alt="" class="img-responsive gambar-artikel">
              <figcaption>
                <h1 class="judul-artikel"><a href="#">FILM KARTU INDONESIA PINTAR</a></h1>
              </figcaption>
            </a>
          </figure>
          
        </div>
      </div>
    </div>


<aside class="col-md-3 col-md-pull-9">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-12">
          <div class="panel panel-primary-1">
            <div class="panel-heading">Agenda </div>
            <div class="panel-body" style="height:400px">

<div class="row-fluid">
	<div id="calendar" class="span5"></div>
  <div class="text-center"><a href="#"><i>
  - indeks Agenda -</i></a></div>
</div>

            </div>
          </div>    
        </div>

            <div class="col-xs-12 col-sm-6 col-md-12">
          <div class="panel panel-default panel-primary-1">
            <div class="panel-heading">MENU PENDIDIKAN </div>
            <div class="list-group">

                          <a href="http://disdik.semarangkota.go.id/v15/main/page/10/profil-pendidikan" target="" class="list-group-item">Profil Pendidikan</a>
                          <a href="http://bos.kemdikbud.go.id/" target="_blank" class="list-group-item">BOS Online</a>
            
            </div>
          </div>    
        </div>
            <div class="col-xs-12 col-sm-6 col-md-12">
          <div class="panel panel-default panel-primary-1">
            <div class="panel-heading">LINK PENDIDIKAN </div>
            <div class="list-group">

                          <a href="http://tve.kemdikbud.go.id/" target="" class="list-group-item">Televisi Edukasi</a>
                          <a href="http://m-edukasi.kemdikbud.go.id/#awal" target="_blank" class="list-group-item">Mobile Edukasi (m-Edukasi)</a>
                          <a href="https://belajar.kemdikbud.go.id/Dashboard/" target="_blank" class="list-group-item">Rumah Belajar</a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/34/juknis-osn-sd-tahun-2018" target="_blank" class="list-group-item">JUKNIS OSN SD TAHUN 2018</a>
                          <a href="http://nisn.data.kemdikbud.go.id" target="_blank" class="list-group-item">NISN (Nomor Induk Siswa Nasional)</a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/84/kaldik-20172018" target="" class="list-group-item">KALDIK 2017/2018</a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/98/kaldik-20182019" target="" class="list-group-item">KALDIK 2018/2019</a>
            
            </div>
          </div>    
        </div>
            <div class="col-xs-12 col-sm-6 col-md-12">
          <div class="panel panel-default panel-primary-1">
            <div class="panel-heading">BIDANG LAYANAN </div>
            <div class="list-group">

                          <a href="http://disdik.semarangkota.go.id/v15/main/page/31/bidang-pembinaan-gtk" target="" class="list-group-item">BIDANG PEMBINAAN GTK </a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/30/bidang-pembinaan-sd" target="" class="list-group-item">Bidang Pembinaan SD</a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/36/sekretariat" target="" class="list-group-item">SEKRETARIAT</a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/85/uptd-pendidikan-kecamatan" target="" class="list-group-item">UPTD Pendidikan Kecamatan</a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/32/bidang-pembinaan-paud-dan-pnf" target="" class="list-group-item">Bidang Pembinaan PAUD dan PNF</a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/33/bidang-pembinaan-smp" target="" class="list-group-item">Bidang Pembinaan SMP</a>
            
            </div>
          </div>    
        </div>
            <div class="col-xs-12 col-sm-6 col-md-12">
          <div class="panel panel-default panel-primary-1">
            <div class="panel-heading">DATA SATUAN PENDIDIKAN </div>
            <div class="list-group">

                          <a href="http://disdik.semarangkota.go.id/v15/main/page/47/pendidikan-anak-usia-dini" target="" class="list-group-item">Pendidikan Anak Usia Dini</a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/43/sekolah-dasar-madrasah-ibtidaiyah" target="_blank" class="list-group-item">Sekolah Dasar & Madrasah Ibtidaiyah</a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/44/sekolah-menengah-pertama-mts" target="" class="list-group-item">Sekolah Menengah Pertama & Mts</a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/45/sekolah-menengah-atas-madrasah-aliyah" target="" class="list-group-item">Sekolah Menengah Atas & Madrasah Aliyah</a>
                          <a href="http://disdik.semarangkota.go.id/v15/main/page/48/sekolah-menengah-kejuruan-smk" target="" class="list-group-item">Sekolah Menengah Kejuruan (SMK)</a>
            
            </div>
          </div>    
        </div>
    
        <div class="col-xs-12 col-sm-6 col-md-12">
          <div class="panel panel-default panel-primary-1">
            <div class="panel-heading">Banner Tautan </div>
            <div class="list-group">

                          <a href="http://semarangkota.go.id/" class="list-group-item" target="_blank">
                <img src="http://disdik.semarangkota.go.id/v15/content/slides/banner2.jpg" alt="Portal Web Pemerintah Kota Semarang" width="100%" height="75px" class="img-responsive">
              </a>
                          <a href="http://www.jatengprov.go.id/" class="list-group-item" target="_blank">
                <img src="http://disdik.semarangkota.go.id/v15/content/slides/banner3.jpg" alt="Portal Resmi Web Pemerintah Provinsi Jateng" width="100%" height="75px" class="img-responsive">
              </a>
                          <a href="https://kemdikbud.go.id/" class="list-group-item" target="_blank">
                <img src="http://disdik.semarangkota.go.id/v15/content/slides/banner4.jpg" alt="Kementrian Pendidikan dan Kebudayaan" width="100%" height="75px" class="img-responsive">
              </a>
                          <a href="https://www.lapor.go.id/" class="list-group-item" target="_blank">
                <img src="http://disdik.semarangkota.go.id/v15/content/slides/banner5.jpg" alt="Layanan Aspirasi dan Pengaduan Online Rakyat" width="100%" height="75px" class="img-responsive">
              </a>
                          <a href="http://www.bkd.semarangkota.go.id/" class="list-group-item" target="_blank">
                <img src="http://disdik.semarangkota.go.id/v15/content/slides/banner6.jpg" alt="BKD Kota Semarang" width="100%" height="75px" class="img-responsive">
              </a>
                          <a href="http://nisn.data.kemdikbud.go.id/page/home" class="list-group-item" target="_blank">
                <img src="http://disdik.semarangkota.go.id/v15/content/slides/banner8.jpg" alt="NISN" width="100%" height="75px" class="img-responsive">
              </a>
            
            </div>
          </div>    
        </div>
      </div>
    </aside>  
  </div>
</div>

<div class="slick-carousel-tautan">
  <a href="http://dapodik.pdkjateng.go.id/" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan25.jpg" alt="Lumbung Data Pendidikan Jawa Tengah" class="img-responsive gambar-artikel">
  </a>
  <a href="http://simpatik.semarangkota.go.id/login" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan26.jpg" alt="Sistem Informasi Kepegawaian Berbasis Teknologi Kota Semarang" class="img-responsive gambar-artikel">
  </a>
  <a href="http://simajar.semarangkota.go.id/utama/" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan27.jpg" alt="SIM Pembelajar" class="img-responsive gambar-artikel">
  </a>
  <a href="http://dapo.dikdasmen.kemdikbud.go.id" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan28.jpg" alt="Dapodikdasmen" class="img-responsive gambar-artikel">
  </a>
  <a href="http://sangjuara.semarangkota.go.id" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan33.jpg" alt="SANG JUARA" class="img-responsive gambar-artikel">
  </a>
  <a href="http://ceksekolahku.or.id" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan34.jpg" alt="Cek Sekolahku" class="img-responsive gambar-artikel">
  </a>
  <a href="http://jendela.data.kemdikbud.go.id/banpnf/" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan36.jpg" alt="Data Akreditasi PAUD - PNF" class="img-responsive gambar-artikel">
  </a>
  <a href="http://referensi.data.kemdikbud.go.id/index.php" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan37.jpg" alt="Data Referensi Pendidikan" class="img-responsive gambar-artikel">
  </a>
  <a href="https://ppd.semarangkota.go.id/" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan40.jpg" alt="PPDB Tahun Pelajaran 2018/2019 Kota Semarang" class="img-responsive gambar-artikel">
  </a>
  <a href="http://pmp.dikdasmen.kemdikbud.go.id/" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan41.jpg" alt="Penjaminan Mutu Pendidikan" class="img-responsive gambar-artikel">
  </a>
  <a href="http://sibooky.semarangkota.go.id/" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan42.jpg" alt="Perpustakaan Digital Kota Semarang" class="img-responsive gambar-artikel">
  </a>
  <a href="https://perpustakaan.kemdikbud.go.id/" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan44.jpg" alt="PERPUSTAKAAN KEMDIKBUD" class="img-responsive gambar-artikel">
  </a>
  <a href="http://opendata.semarangkota.go.id/" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan45.jpg" alt="Open Data Pemerintah Kota Semarang" class="img-responsive gambar-artikel">
  </a>
  <a href="http://simponi.disdik.semarangkota.go.id/" target="_blank">
  <img src="http://disdik.semarangkota.go.id/v15/content/slides/tautan46.jpg" alt="Sistem Manajemen Peijinan Online PAUD dan PNF" class="img-responsive gambar-artikel">
  </a>
</div>
<br>

<footer>
<div id="footer-atas">
	<div class="container">
		<div class="statistik-pengunjung row">
			<div id="counter" class="col-md-12">
					<h4>Statistik Pengunjung</h4>
					<p><b>Online</b>:&nbsp;<span class="value">283</span></p>
					<p><b>Hari Ini</b>:&nbsp;<span class="value">294</span></p>
					<p><b>Kemarin</b>:&nbsp;<span class="value">389</span></p>
					<p><b>Minggu</b>:&nbsp;<span class="value">1039</span></p>
					<p><b>Bulan</b>:&nbsp;<span class="value">1452</span></p>
					<p><b>Tahun</b>:&nbsp;<span class="value">141665</span></p>
					<p><b>Total</b>:&nbsp;<span class="value">449787</span></p>
					<!--<div>
									Catatan: 8408(15.07.2017)
					</div>-->
			</div>
		</div>
	</div>
</div>
<div id="footer-bawah">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<div class="copyright text-left">
					&copy;2017 Dinas Pendidikan Kota Semarang. Dikembangkan oleh <a href="http://dinustek.co.id">dinustek</a>.
					<!-- <br class="visible-xs visible-sm">
					<br class="visible-xs visible-sm"> -->
				</div>
			</div>
			<div class="col-md-5 col-md-offset-1">
				<img src="http://disdik.semarangkota.go.id/v15/assets/new/assets/img/gambar-footer.png" alt="" class="img-responsive">
			</div>
		</div>
	</div>
</div>
</footer>
</body>
</html>