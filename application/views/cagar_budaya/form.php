
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Bidang Usaha Daya Tarik Wisata</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active">Bidang Usaha Daya Tarik Wisata</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
		
    <!-- Main content -->
    <section class="content">
		

        <div class="row" id="awal">
          <div class="col-12">
            <!-- Custom Tabs -->
            <div class="card">
              <div class="card-header p-0">
                <ul class="nav nav-pills ml-auto p-2">
                  <li class="nav-item"><a class="nav-link active" href="#tab_form_cagar_budaya" data-toggle="tab" id="klik_tab_input">Form</a></li>
                  <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab" id="klik_tab_data_cagar_budaya">Data</a></li>
                </ul>
								<div class="card-tools">
									<a class="btn btn-primary btn-sm" href="<?php echo base_url(); ?>cagar_budaya"><i class="fa fa-refresh"></i></a>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content">
									<div class="tab-pane active" id="tab_form_cagar_budaya">
                <input name="page" id="page" value="1" type="hidden" value="">
                <input name="tabel" id="tabel" value="cagar_budaya" type="hidden" value="">
                <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=cagar_budaya" enctype="multipart/form-data">
                  <div class="box-body">
										<div class="form-group" style="display:none;">
											<label for="temp">temp</label>
											<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="mode">mode</label>
											<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_cagar_budaya">id_cagar_budaya</label>
											<input class="form-control" id="id_cagar_budaya" name="id" value="" placeholder="id_cagar_budaya" type="text">
										</div>
										<div class="form-group">
											<label for="nama">Nama</label>
											<input class="form-control" id="nama" name="nama" value="-" placeholder="nama" type="text">
										</div>
										<div class="form-group">
											<label for="jenis">Jenis</label>
											<input class="form-control" id="jenis" name="jenis" value="-" placeholder="jenis" type="text">
										</div>
										<div class="form-group">
											<label for="lokasi">Lokasi</label>
											<input class="form-control" id="lokasi" name="lokasi" value="-" placeholder="lokasi" type="text">
										</div>
										<div class="form-group">
											<label for="kondisi">Kondisi</label>
											<input class="form-control" id="kondisi" name="kondisi" value="-" placeholder="kondisi" type="text">
										</div>
										<div class="form-group">
											<label for="keterangan">Keterangan</label>
											<input class="form-control" id="keterangan" name="keterangan" value="-" placeholder="keterangan" type="text">
										</div>
										<div class="form-group">
											<label for="saran">Saran</label>
											<input class="form-control" id="saran" name="saran" value="-" placeholder="saran" type="text">
										</div>
											<div class="form-group">
												<label for="id_kecamatan">id_kecamatan</label>
												<select class="form-control" id="id_kecamatan" name="id_kecamatan" >
												</select>
											</div>
											<div class="form-group">
												<label for="id_desa">id_desa</label>
												<select class="form-control" id="id_desa" name="id_desa" >
													<option value="0">Pilih Desa</option>
												</select>
											</div>
										<div class="alert alert-info alert-dismissable">
											<div class="form-group">
												<label for="remake">Keterangan Lampiran </label>
												<input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran " type="text">
											</div>
											<div class="form-group">
												<label for="myfile">File Lampiran </label>
												<input type="file" size="60" name="myfile" id="file_lampiran" >
											</div>
											<div id="ProgresUpload">
												<div id="BarProgresUpload"></div>
												<div id="PersenProgresUpload">0%</div >
											</div>
											<div id="PesanProgresUpload"></div>
										</div>
										<div class="alert alert-info alert-dismissable">
											<h3 class="box-title">Data Lampiran </h3>
											<table class="table table-bordered">
												<tr>
													<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
												</tr>
												<tbody id="tbl_attachment_cagar_budaya">
												</tbody>
											</table>
										</div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="simpan_cagar_budaya">SIMPAN</button>
                    <button type="submit" class="btn btn-primary" id="update_cagar_budaya" style="display:none;">UPDATE</button>
                  </div>
                </form>
									</div>
									<div class="tab-pane table-responsive" id="tab_2">
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">&nbsp;</h3>
												<div class="card-tools">
													<div class="input-group input-group-sm">
														<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
														<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
															<option value="999999999">Semua</option>
															<option value="10">10 Per-Halaman</option>
															<option value="25">25 Per-Halaman</option>
															<option value="50">50 Per-Halaman</option>
															<option value="100">100 Per-Halaman</option>
														</select>
														<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
															<option value="cagar_budaya.nama">nama</option>
															<option value="cagar_budaya.jenis">jenis</option>
														</select>
														<select name="perihal_surat" class="form-control input-sm pull-right" style="width: 350px; display:none;" id="perihal_surat">
														</select>
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default" id="tampilkan_data_cagar_budaya"><i class="fa fa-search"></i> Tampil</button>
														</div>
													</div>
												</div>
											</div>
											<div class="card-body table-responsive p-0">
												<table class="table table-bordered table-hover">
													<tr>
														<th>No.</th>
														<th>Nama</th>
														<th>Jenis</th>
														<th>Lokasi</th>
														<th>Kondisi</th>
														<th>Keterangan</th>
														<th>Saran</th> 
														<th>Desa, Kecamatan</th> 
													</tr>
													<tbody id="tbl_data_cagar_budaya">
													</tbody>
												</table>
											</div>
											<div class="card-footer">
												<ul class="pagination pagination-sm m-0 float-right" id="pagination">
												</ul>
												<div class="overlay" id="spinners_tbl_data_cagar_budaya" style="display:none;">
													<i class="fa fa-refresh fa-spin"></i>
												</div>
											</div>
										</div>
                	</div>
                <!-- /.tab-content -->
              	</div><!-- /.card-body -->
            </div>
            <!-- ./card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
				

    </section>
				
<script>
  function load_opd_domain() {
    $('#id_kecamatan').html('');
    $('#option_kecamatan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>cagar_budaya/option_kecamatan/',
      success: function(html) {
        $('#option_kecamatan').html('<option value="0">Pilih Kecamatan</option>'+html+'');
        $('#id_kecamatan').html('<option value="0">Pilih Kecamatan</option>'+html+'');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#id_kecamatan').on('change', function(e) {
      e.preventDefault();
      var halaman = 1;
      var id_kecamatan = $('#id_kecamatan').val();
      load_option_desa(id_kecamatan);
    });
  });
</script>

<script>
  function load_option_desa(id_kecamatan) {
    $('#id_desa').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        id_kecamatan: id_kecamatan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>permohonan_pembuatan_website/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html(' '+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  //load_id_skpd();
  load_opd_domain();
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>cagar_budaya/total_datas',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_alls_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedBalita() {
    $('#id_cagar_budaya, #nama, #jenis, #lokasi, #kondisi, #saran, #keterangan, #status, #temp, #id_desa, #id_kecamatan ').val('');
    $('#tbl_attachment_cagar_budaya').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_cagar_budaya').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'cagar_budaya',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_cagar_budaya').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_posting').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_cagar_budaya').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_cagar_budaya').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_cagar_budaya').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'temp', 'nama', 'jenis' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["nama"] = $("#nama").val();
      parameter["jenis"] = $("#jenis").val();
      parameter["lokasi"] = $("#lokasi").val();
      parameter["kondisi"] = $("#kondisi").val();
      parameter["saran"] = $("#saran").val();
      parameter["keterangan"] = $("#keterangan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      var url = '<?php echo base_url(); ?>cagar_budaya/simpan_cagar_budaya';
      
      var parameterRv = [ 'temp', 'nama', 'jenis' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedBalita();
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_cagar_budaya').on('click', '.update_id_cagar_budaya', function() {
    $('#mode').val('edit');
    $('#simpan_cagar_budaya').hide();
    $('#update_cagar_budaya').show();
    $('#overlay_data_cagar_budaya').show();
    $('#tbl_data_cagar_budaya').html('');
    var id_cagar_budaya = $(this).closest('tr').attr('id_cagar_budaya');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_cagar_budaya');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_cagar_budaya').val(id_cagar_budaya);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_cagar_budaya:id_cagar_budaya
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>cagar_budaya/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_cagar_budaya').val(json[i].id_cagar_budaya);
            $('#nama').val(json[i].nama);
            $('#jenis').val(json[i].jenis);
            $('#lokasi').val(json[i].lokasi);
            $('#kondisi').val(json[i].kondisi);
            $('#saran').val(json[i].saran);
            $('#keterangan').val(json[i].keterangan);
            $('#id_desa').val(json[i].id_desa);
            $('#id_kecamatan').val(json[i].id_kecamatan);
          }
        }
      });
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_cagar_budaya').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'id_cagar_budaya', 'nama', 'jenis' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_cagar_budaya"] = $("#id_cagar_budaya").val();
      parameter["nama"] = $("#nama").val();
      parameter["temp"] = $("#temp").val();
      parameter["jenis"] = $("#jenis").val();
      parameter["lokasi"] = $("#lokasi").val();
      parameter["kondisi"] = $("#kondisi").val();
      parameter["keterangan"] = $("#keterangan").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["saran"] = $("#saran").val();
      var url = '<?php echo base_url(); ?>cagar_budaya/update_cagar_budaya';
      
      var parameterRv = [ 'id_cagar_budaya', 'nama', 'jenis' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_data_cagar_budaya').on('click', '#del_ajax_cagar_budaya', function() {
    var id_cagar_budaya = $(this).closest('tr').attr('id_cagar_budaya');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_cagar_budaya"] = id_cagar_budaya;
        var url = '<?php echo base_url(); ?>cagar_budaya/hapus/';
        HapusData(parameter, url);
        $('[id_cagar_budaya='+id_cagar_budaya+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
      $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
      load_data(tabel);
    });
  });
});
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#tanggal_lahir').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>