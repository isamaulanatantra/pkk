<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
$ses=$this->session->userdata('id_users');
if(!$ses) { return redirect(''.base_url().'login');  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php $web=$this->uut->namadomain(base_url()); $aa=explode(".",$web);echo $aa[0]; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/Font-Awesome-master/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- pace-progress -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>pace-progress/themes/red/pace-theme-flat-top.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sweetalert2/bootstrap-4.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.css">
    <!-- Theme 
    <link rel="stylesheet" href="<?php echo base_url(); ?>Template/AdminLTE-2.4.3/dist/css/adminlte.min.css">
    style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte3.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>  
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
    <!-- summernote -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.css">
    
      <script type="text/javascript">
        <?php

        foreach ($menu->result() as $b1) {

          $data['id_users'] = $b1->id_users;

          echo

          '

          function menu'.$b1->module_code.'() {

            getAjax("/'.strtolower($b1->module_code).'", \'html\', function (response) {

                $("#tag_container").empty().html(response);

            });

          }

          ';

        }

          echo

          '

          function menuGanti_password() {

            getAjax("/ganti_password", \'html\', function (response) {

                $("#tag_container").empty().html(response);

            });

          }

          ';

          echo

          '

          function menuUser_profile() {

              getAjax("/user_profile", \'html\', function (response) {

                  $("#tag_container").empty().html(response);

              });

          }

          ';

          

          echo

          '

          function menuCompany_profile() {

            getAjax("/company_profile", \'html\', function (response) {

                $("#tag_container").empty().html(response);

            });

          }

          ';

          

          echo

          '

          function menuFoto_profil() {

            getAjax("/foto_profil", \'html\', function (response) {

                $("#tag_container").empty().html(response);

            });

          }

          ';

          

        ?>

      </script>
    <script src="<?php echo base_url(); ?>Template/main.js"></script>

  </head>


  <body class="hold-transition layout-top-nav">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
      <a href="index3.html" class="navbar-brand">
        <img src="<?php echo base_url(); ?>Template/AdminLTE-2.4.3/dist/img/avatar.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light"><?php $web=$this->uut->namadomain(base_url()); $aa=explode(".",$web);echo $aa[0]; ?></span>
      </a>
      
      <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse order-3" id="navbarCollapse">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a href="index3.html" class="nav-link">Home</a>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link">Contact</a>
          </li>
          <li class="nav-item dropdown">
            <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">Dropdown</a>
            <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
              <li><a href="#" class="dropdown-item">Some action </a></li>
              <li><a href="#" class="dropdown-item">Some other action</a></li>

              <li class="dropdown-divider"></li>

              <!-- Level two dropdown-->
              <li class="dropdown-submenu dropdown-hover">
                <a id="dropdownSubMenu2" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">Hover for action</a>
                <ul aria-labelledby="dropdownSubMenu2" class="dropdown-menu border-0 shadow">
                  <li>
                    <a tabindex="-1" href="#" class="dropdown-item">level 2</a>
                  </li>

                  <!-- Level three dropdown-->
                  <li class="dropdown-submenu">
                    <a id="dropdownSubMenu3" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-item dropdown-toggle">level 2</a>
                    <ul aria-labelledby="dropdownSubMenu3" class="dropdown-menu border-0 shadow">
                      <li><a href="#" class="dropdown-item">3rd level</a></li>
                      <li><a href="#" class="dropdown-item">3rd level</a></li>
                    </ul>
                  </li>
                  <!-- End Level three -->

                  <li><a href="#" class="dropdown-item">level 2</a></li>
                  <li><a href="#" class="dropdown-item">level 2</a></li>
                </ul>
              </li>
              <!-- End Level two -->
            </ul>
          </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-0 ml-md-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form>
      </div>

      <!-- Right navbar links -->
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fas fa-comments"></i>
            <span class="badge badge-danger navbar-badge">3</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?php echo base_url(); ?>Template/AdminLTE-2.4.3/dist/img/avatar.png" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Brad Diesel
                    <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">Call me whenever you can...</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?php echo base_url(); ?>Template/AdminLTE-2.4.3/dist/img/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    John Pierce
                    <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">I got your message bro</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <!-- Message Start -->
              <div class="media">
                <img src="<?php echo base_url(); ?>Template/AdminLTE-2.4.3/dist/img/user3-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                <div class="media-body">
                  <h3 class="dropdown-item-title">
                    Nora Silvester
                    <span class="float-right text-sm text-warning"><i class="fas fa-star"></i></span>
                  </h3>
                  <p class="text-sm">The subject goes here</p>
                  <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                </div>
              </div>
              <!-- Message End -->
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
          </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="far fa-bell"></i>
            <span class="badge badge-warning navbar-badge">15</span>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-header">15 Notifications</span>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i> 4 new messages
              <span class="float-right text-muted text-sm">3 mins</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-users mr-2"></i> 8 friend requests
              <span class="float-right text-muted text-sm">12 hours</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item">
              <i class="fas fa-file mr-2"></i> 3 new reports
              <span class="float-right text-muted text-sm">2 days</span>
            </a>
            <div class="dropdown-divider"></div>
            <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i
              class="fas fa-th-large"></i></a>
        </li>
      </ul>
    </div>
  </nav>
  <!-- /.navbar -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- awal profil -->
    
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container">
        <div class="row">
          <div class="col-md-3">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="<?php if( !empty($foto) ){echo ''.$foto.'';}else{echo''.base_url().'Template/AdminLTE-2.4.3/dist/img/user4-128x128.jpg';} ?>"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">
                <?php
                if( !empty($first_name) ){
                  echo ''.$first_name.' '.$last_name.' ';
                }
                ?>
                </h3>


                <?php
                if( $menu_root->num_rows() > 0 ){
                    echo '<p class="text-muted text-center">ROOT</p>';
                    echo '<ul class="list-group list-group-unbordered mb-3">';
                    foreach ($menu_root->result() as $b2) {
                        echo '
                        <li class="list-group-item">
                        <b><a href="#" onclick="menu'.$b2->module_code.'()">'.ucwords(str_replace('_', ' ', $b2->module_name)).'</a></b> <a class="float-right">'.$b2->urutan_menu.'</a>
                        </li>
                        ';
                    }
                    echo'</ul>';
                }
                if( $menu_adminwebdesa->num_rows() > 0 ){
                    echo '<p class="text-muted text-center">Admin Web DESA</p>';
                    echo '<ul class="list-group list-group-unbordered mb-3">';
                    foreach ($menu_adminwebdesa->result() as $b2) {
                        echo '
                        <li class="list-group-item">
                        <b><a href="#" onclick="menu'.$b2->module_code.'()">'.ucwords(str_replace('_', ' ', $b2->module_name)).'</a></b> <a class="float-right">'.$b2->urutan_menu.'</a>
                        </li>
                        ';
                    }
                    echo'</ul>';
                }
                if( $menu_adminwebskpd->num_rows() > 0 ){
                    echo '<p class="text-muted text-center">Admin SKPD</p>';
                    echo '<ul class="list-group list-group-unbordered mb-3">';
                    foreach ($menu_adminwebskpd->result() as $b2) {
                        echo '
                        <li class="list-group-item">
                        <b><a href="#" onclick="menu'.$b2->module_code.'()">'.ucwords(str_replace('_', ' ', $b2->module_name)).'</a></b> <a class="float-right">'.$b2->urutan_menu.'</a>
                        </li>
                        ';
                    }
                    echo'</ul>';
                }
              ?>

                <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9" id="tag_container">

            <?php $this -> load -> view($page);  ?>

          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
    <!-- akhir profil -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      Anything you want
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->



  <div id="sidebar-overlay"></div>
      
      
      <script type="text/javascript">

        $(document).ready(function() {

          $(document).ajaxStart(function() {

            $("#loadinghalaman").show();

            Pace.restart();

          });

          $(document).ajaxSuccess(function() {

            Pace.stop();

          });

        });

      </script>

  <script src="https://adminlte.io/themes/dev/AdminLTE/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="https://adminlte.io/themes/dev/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- SweetAlert2 -->
  <script src="<?php echo base_url(); ?>assets/plugins/sweetalert2/sweetalert2.min.js"></script>
  <!-- Toastr -->
  <script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?php echo base_url(); ?>assets/dist/js/adminlte3.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

      <script src="<?php echo base_url(); ?>assets/js/uutv1.js"></script>
      <!-- alertify -->
      <script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
  <script src="<?php echo base_url(); ?>boots/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js" type="text/javascript"></script>
      <script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
  
      <!-- Default-->
      <script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
      <script src="<?php echo base_url(); ?>js/alertify.min.js"></script>

      <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script>

      <!-- AdminLTE dashboard demo (This is only for demo purposes) -->    

      <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.js"></script>

      <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

      <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.extensions.js"></script>

      <script>

        $(function () {

          $("[data-mask]").inputmask();

        });

      </script>

      <!-- AdminLTE for demo purposes -->    

      <!-- pace-progress -->
      <script src="<?php echo base_url(); ?>pace-progress/pace.min.js"></script>

      <script src="<?php echo base_url(); ?>js/uutv1.js"></script>

      <script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

      <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script>

  </body>

</html>
<!-- end document-->