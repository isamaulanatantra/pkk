
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
						<select class="form-group" id="jenis_permohonan" name="jenis_permohonan" style="font-size:24px;">
						</select>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>">Home</a></li>
              <li class="breadcrumb-item active"><span id="jenis_permohonan_a"></span></li>
            </ol>
          </div>
        </div>
      </div>
    </section>
		<section class="content" id="awal">
			<ul class="nav nav-tabs">
			<li class="nav-item"><a class="nav-link" href="#tab_1" data-toggle="tab" id="klik_tab_input">Form</a></li>
			<li class="nav-item"><a class="nav-link active" href="#tab_2" data-toggle="tab" id="klik_tab_tampil">Data</a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane" id="tab_1">
					<div class="card">
						<div class="card-header">
							<h3 class="card-title" id="judul_formulir">FORMULIR BARU</h3>
						</div>
						<div class="card-body">
							<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=permohonan_informasi_publik" enctype="multipart/form-data">
								<div class="box-body">
									<div class="form-group" style="display:none;">
										<label for="temp">temp</label>
										<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
									</div>
									<div class="form-group" style="display:none;">
										<label for="mode">mode</label>
										<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
									</div>
									<div class="form-group" style="display:none;">
										<label for="id_permohonan_informasi_publik">id_permohonan_informasi_publik</label>
										<input class="form-control" id="id_permohonan_informasi_publik" name="id" value="" placeholder="id_permohonan_informasi_publik" type="text">
									</div>
									<div class="form-group" style="display:none;">
										<label for="id_posting">id_posting</label>
										<input class="form-control" id="id_posting" name="id_posting" value="0" placeholder="id_posting" type="text">
									</div>
									<div class="form-group" style="display:none;">
										<label for="created_by">created_by</label>
										<input class="form-control" id="created_by" name="created_by" value="0" placeholder="created_by" type="text">
									</div>
									<div class="form-group">
										<label for="created_time">Tanggal</label>
										<input class="form-control" id="created_time" type="text">
									</div>
									<div class="form-group">
										<label for="nama">Nama Pemohon</label>
										<input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text">
									</div>
									<div class="form-group">
										<label for="instansi">Instansi Pemohon</label>
										<input class="form-control" id="instansi" name="instansi" value="" placeholder="Instansi" type="text">
									</div>
									<div class="form-group">
										<label for="alamat">Alamat Pemohon</label>
										<input class="form-control" id="alamat" name="alamat" value="" placeholder="Alamat" type="text">
									</div>
									<div class="form-group">
										<label for="pekerjaan">Pekerjaan/Jabatan Pemohon</label>
										<input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="Pekerjaan" type="text">
									</div>
									<div class="form-group">
										<label for="nomor_telp">Nomor Telp Pemohon</label>
										<input class="form-control" id="nomor_telp" name="nomor_telp" value="" placeholder="081090909090" type="text">
									</div>
									<div class="form-group">
										<label for="email">Email Pemohon</label>
										<input class="form-control" id="email" name="email" value="" placeholder="email@mail.id" type="text">
									</div>
									<div class="form-group">
										<label for="pembimbing">Pembimbing</label>
										<input class="form-control" id="pembimbing" name="pembimbing" value="" placeholder="Pembimbing" type="text">
									</div>
                  <div class="form-group" style="display:none;">
                    <label for="rincian_informasi_yang_diinginkan">Isi Halaman</label>
                    <input class="form-control" id="rincian_informasi_yang_diinginkan" name="rincian_informasi_yang_diinginkan" value="" placeholder="Isi Halaman" type="text">
                  </div>
                  <div class="form-group">
                  <textarea id="editor_isi_posting"></textarea>
                  </div>
									<div class="form-group" style="display:none;">
										<label for="tujuan_penggunaan_informasi">Tujuan penggunaan informasi</label>
										<input class="form-control" id="tujuan_penggunaan_informasi" name="tujuan_penggunaan_informasi" value="Pelayanan Informasi" placeholder="Tujuan penggunaan informasi" type="text">
									</div>
									<div class="form-group" style="display:none;">
										<label for="cara_melihat_membaca_mendengarkan_mencatat">cara memperoleh Informasi <br /> melihat/membaca/mendengarkan/mencatat</label>
										<select class="form-control" id="cara_melihat_membaca_mendengarkan_mencatat" name="cara_melihat_membaca_mendengarkan_mencatat" >
										<option value="1">Ya</option>
										<option value="0">Tidak</option>
										</select>
									</div>
									<div class="form-group" style="display:none;">
										<label for="cara_hardcopy_softcopy">Cara Memperoleh Informasi <br /> hardcopy/softcopy</label>
										<select class="form-control" id="cara_hardcopy_softcopy" name="cara_hardcopy_softcopy" >
										<option value="0">Tidak</option>
										<option value="1">Ya</option>
										</select>
									</div>
									<div class="form-group" style="display:none;">
										<label for="cara_mengambil_langsung">Cara Mendapatkan Informasi <br /> Mengambil langsung</label>
										<select class="form-control" id="cara_mengambil_langsung" name="cara_mengambil_langsung" >
										<option value="0">Tidak</option>
										<option value="1">Ya</option>
										</select>
									</div>
									<div class="form-group" style="display:none;">
										<label for="cara_kurir">Kurir</label>
										<select class="form-control" id="cara_kurir" name="cara_kurir" >
										<option value="0">Tidak</option>
										<option value="1">Ya</option>
										</select>
									</div>
									<div class="form-group" style="display:none;">
										<label for="cara_pos">POS</label>
										<select class="form-control" id="cara_pos" name="cara_pos" >
										<option value="0">Tidak</option>
										<option value="1">Ya</option>
										</select>
									</div>
									<div class="form-group" style="display:none;">
										<label for="cara_faksimili">Faksimili</label>
										<select class="form-control" id="cara_faksimili" name="cara_faksimili" >
										<option value="0">Tidak</option>
										<option value="1">Ya</option>
										</select>
									</div>
									<div class="form-group" style="display:none;">
										<label for="cara_email">Email</label>
										<select class="form-control" id="cara_email" name="cara_email" >
										<option value="1">Ya</option>
										<option value="0">Tidak</option>
										</select>
									</div>
                  <div class="alert alert-info alert-dismissable">
                    <div class="form-group">
                      <label for="remake">Keterangan Lampiran </label>
                      <input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran " type="text">
                    </div>
                    <div class="form-group">
                      <label for="myfile">File Lampiran </label>
                      <input type="file" size="60" name="myfile" id="file_lampiran" >
                    </div>
                    <div id="ProgresUpload">
                      <div id="BarProgresUpload"></div>
                      <div id="PersenProgresUpload">0%</div >
                    </div>
                    <div id="PesanProgresUpload"></div>
                  </div>
                  <div class="alert alert-info alert-dismissable table-responsive">
                    <h3 class="card-title">Data Lampiran </h3>
                    <table class="table table-bordered">
                      <tr>
                        <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                      </tr>
                      <tbody id="tbl_attachment_permohonan_informasi_publik">
                      </tbody>
                    </table>
                  </div>
								</div>
								<div class="box-footer">
									<div class="overlay" id="overlay_form_input" style="display:none;">
										<i class="fa fa-refresh fa-spin"></i>
									</div>
									<button type="submit" class="btn btn-primary" id="simpan_permohonan_informasi_publik">KIRIM</button>
									<button type="submit" class="btn btn-primary" id="update_permohonan_informasi_publik" style="display:none;">UPDATE</button>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="tab-pane active" id="tab_2">
					<div class="card">
						<div class="card-header">
                <h5 class="card-title" id="jenis_permohonan_b"></h5>

                <div class="card-tools">
									<span>Tahun</span>
									<select class="button m-0" id="tahun" name="tahun">
										<option value="2019">2019</option>
										<option value="2018">2018</option>
										<option value="2017">2017</option>
										<option value="2016">2016</option>
										<option value="2015">2015</option>
									</select>
									<label id="total_pelayanan_informasi"></label>
                </div>
              </div>
						<div class="card-body">
							<div class="table-responsive">
								<table class="table table-striped table-hover table-bordered">
									<thead>
										<tr>
											<th>NO</th>
											<th>PEMOHON</th>
											<th>KEPERLUAN</th>
											<th>TUJUAN</th>
											<th>PROSES</th>
										</tr>
									</thead>
									<tbody id="tbl_utama_pelayanan_informasi">
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
</section>

<script type="text/javascript">
$(document).ready(function() {
  load_table();
  load_total_pelayanan_informasi();
  load_option_jenis_permohonan();
  load_jenis_permohonan();
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#klik_tab_tampil').on('change', function(e) {
      e.preventDefault();
      load_table();
      load_total_pelayanan_informasi();
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#tahun').on('change', function(e) {
      e.preventDefault();
      load_table();
      load_total_pelayanan_informasi();
    });
    $('#jenis_permohonan').on('change', function(e) {
      e.preventDefault();
      load_jenis_permohonan();
      load_table();
      load_total_pelayanan_informasi();
    });
  });
</script>
<script>
  function load_table() {
    $('#tbl_utama_pelayanan_informasi').html('');
    $('#spinners_data').show();
    var tahun = $('#tahun').val();
    var jenis_permohonan = $('#jenis_permohonan').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1,
        tahun: tahun,
        jenis_permohonan: jenis_permohonan
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>pelayanan_informasi/load_table_pelayanan_informasi/',
      success: function(html) {
        $('#tbl_utama_pelayanan_informasi').html(''+html+'');
      }
    });
  }
</script>
<script>
	function load_total_pelayanan_informasi() {
    var tahun = $('#tahun').val();
    var jenis_permohonan = $('#jenis_permohonan').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				tahun:tahun,
        jenis_permohonan: jenis_permohonan
			},
			dataType: 'html',
			url: '<?php echo base_url(); ?>pelayanan_informasi/total_pelayanan_informasi/',
			success: function(html) {
				$('#total_pelayanan_informasi').html('Total : '+html+' ');
			}
		});
	}
</script>
<script>
	function load_option_jenis_permohonan() {
    var tahun = $('#tahun').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				tahun:tahun
			},
			dataType: 'html',
			url: '<?php echo base_url(); ?>pelayanan_informasi/load_jenis_permohonan/',
			success: function(html) {
				$('#jenis_permohonan').html(''+html+'');
			}
		});
	}
</script>
<script>
	function load_jenis_permohonan() {
    var jenis_permohonan = $('#jenis_permohonan').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				jenis_permohonan:jenis_permohonan
			},
			dataType: 'html',
			url: '<?php echo base_url(); ?>pelayanan_informasi/jenis_permohonan/',
			success: function(html) {
				$('#jenis_permohonan_a').html(''+html+' ');
				$('#jenis_permohonan_b').html(''+html+' ');
			}
		});
	}
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_pelayanan_informasi').on('click', '#del_ajax', function() {
    var id_permohonan_informasi_publik = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_permohonan_informasi_publik"] = id_permohonan_informasi_publik;
        var url = '<?php echo base_url(); ?>pelayanan_informasi/hapus/';
        HapusData(parameter, url);
        $('[id_permohonan_informasi_publik='+id_permohonan_informasi_publik+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
<script>
  function AfterSavedPermohonan_informasi_publik() {
    $('#id_permohonan_informasi_publik, #id_posting, #created_time, #nama, #instansi, #pembimbing, #alamat, #pekerjaan, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #kategori_permohonan_informasi_publik, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #created_by').val('');
    $('#tbl_attachment_permohonan_informasi_publik').html('');
    $('#PesanProgresUpload').html('');
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_permohonan_informasi_publik').show();
    $('#update_permohonan_informasi_publik').hide();
    $('#tbl_attachment_permohonan_informasi_publik').html('');
    $('#id_permohonan_informasi_publik, #id_posting, #created_time, #nama, #instansi, #pembimbing, #alamat, #pekerjaan, #nomor_telp,  #email, #rincian_informasi_yang_diinginkan, #kategori_permohonan_informasi_publik, #tujuan_penggunaan_informasi, #cara_melihat_membaca_mendengarkan_mencatat, #cara_hardcopy_softcopy, #cara_mengambil_langsung, #cara_kurir, #cara_pos, #cara_faksimili, #cara_email, #temp, #created_by').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR PELAYANAN INFORMASI');
  });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_permohonan_informasi_publik').on('click', function(e) {
      e.preventDefault();
      var editor_isi_posting = CKEDITOR.instances.editor_isi_posting.getData();
      $('#rincian_informasi_yang_diinginkan').val( editor_isi_posting );
      $('#overlay_form_input').show();
      $('#pesan_terkirim').show();
      var parameter = [ 'nama', 'alamat', 'nomor_telp', 'temp' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_posting"] = $("#id_posting").val();
      parameter["created_time"] = $("#created_time").val();
      parameter["nama"] = $("#nama").val();
      parameter["instansi"] = $("#instansi").val();
      parameter["pembimbing"] = $("#pembimbing").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      parameter["tampil_menu_atas"] = $("#tampil_menu_atas").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["rincian_informasi_yang_diinginkan"] = $("#rincian_informasi_yang_diinginkan").val();
      parameter["kategori_permohonan_informasi_publik"] = $("#jenis_permohonan").val();
      parameter["tujuan_penggunaan_informasi"] = $("#tujuan_penggunaan_informasi").val();
      parameter["cara_melihat_membaca_mendengarkan_mencatat"] = $("#cara_melihat_membaca_mendengarkan_mencatat").val();
      parameter["cara_hardcopy_softcopy"] = $("#cara_hardcopy_softcopy").val();
      parameter["cara_mengambil_langsung"] = $("#cara_mengambil_langsung").val();
      parameter["cara_kurir"] = $("#cara_kurir").val();
      parameter["cara_pos"] = $("#cara_pos").val();
      parameter["cara_faksimili"] = $("#cara_faksimili").val();
      parameter["cara_email"] = $("#cara_email").val();
      parameter["temp"] = $("#temp").val();
      parameter["created_by"] = $("#created_by").val();
      var url = '<?php echo base_url(); ?>pelayanan_informasi/simpan_pelayanan_informasi';
      
      var parameterRv = [ 'nama', 'alamat', 'nomor_telp', 'temp' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap, Cek kembali formulir yang berwarna merah');
				$('#overlay_form_input').fadeOut();
				alert('gagal');
				$('html, body').animate({
					scrollTop: $('#awal').offset().top
				}, 1000);
      }
      else{
        SimpanData(parameter, url);
				$('#overlay_form_input').fadeOut('slow');
				$('#pesan_terkirim').fadeOut('slow');
      }
    });
  });
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#created_time').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>

<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('editor_isi_posting');
	$(".textarea").wysihtml5();
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_pelayanan_informasi').on('click', '.update_id', function() {
    $('#mode').val('edit');
    $('#simpan_permohonan_informasi_publik').hide();
    $('#update_permohonan_informasi_publik').show();
    $('#overlay_data_permohonan_informasi_publik').show();
    $('#tbl_utama_pelayanan_informasi').html('');
    var id_permohonan_informasi_publik = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    var mode = $('#mode').val();
    var value = $(this).closest('tr').attr('id_permohonan_informasi_publik');
    $('#form_baru').show();
    $('#judul_formulir').html('FORMULIR EDIT');
    $('#id_permohonan_informasi_publik').val(id_permohonan_informasi_publik);
		$.ajax({
        type: 'POST',
        async: true,
        data: {
          id_permohonan_informasi_publik:id_permohonan_informasi_publik
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>pelayanan_informasi/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_posting').val(json[i].id_posting);
            $('#created_time').val(json[i].created_time);
            $('#nama').val(json[i].nama);
            $('#instansi').val(json[i].instansi);
            $('#pembimbing').val(json[i].pembimbing);
            $('#alamat').val(json[i].alamat);
            $('#pekerjaan').val(json[i].pekerjaan);
            $('#nomor_telp').val(json[i].nomor_telp);
            $('#email').val(json[i].email);
            // $('#rincian_informasi_yang_diinginkan').val(json[i].rincian_informasi_yang_diinginkan);
            $('#kategori_permohonan_informasi_publik').val(json[i].kategori_permohonan_informasi_publik);
            $('#tujuan_penggunaan_informasi').val(json[i].tujuan_penggunaan_informasi);
            $('#cara_melihat_membaca_mendengarkan_mencatat').val(json[i].cara_melihat_membaca_mendengarkan_mencatat);
            $('#cara_hardcopy_softcopy').val(json[i].cara_hardcopy_softcopy);
            $('#cara_mengambil_langsung').val(json[i].cara_mengambil_langsung);
            $('#cara_kurir').val(json[i].cara_kurir);
            $('#cara_faksimili').val(json[i].cara_faksimili);
            $('#cara_email').val(json[i].cara_email);
            $('#created_by').val(json[i].created_by);
            CKEDITOR.instances.editor_isi_posting.setData(json[i].rincian_informasi_yang_diinginkan);
          }
        }
      });
    AttachmentByMode(mode, value);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#update_permohonan_informasi_publik').on('click', function(e) {
      e.preventDefault();
      var editor_isi_posting = CKEDITOR.instances.editor_isi_posting.getData();
      $('#rincian_informasi_yang_diinginkan').val( editor_isi_posting );
      var parameter = [ 'id_permohonan_informasi_publik', 'nama', 'alamat', 'nomor_telp' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["id_permohonan_informasi_publik"] = $("#id_permohonan_informasi_publik").val();
      parameter["id_posting"] = $("#id_posting").val();
      parameter["created_time"] = $("#created_time").val();
      parameter["nama"] = $("#nama").val();
      parameter["instansi"] = $("#instansi").val();
      parameter["pembimbing"] = $("#pembimbing").val();
      parameter["alamat"] = $("#alamat").val();
      parameter["pekerjaan"] = $("#pekerjaan").val();
      parameter["tampil_menu_atas"] = $("#tampil_menu_atas").val();
      parameter["nomor_telp"] = $("#nomor_telp").val();
      parameter["email"] = $("#email").val();
      parameter["rincian_informasi_yang_diinginkan"] = $("#rincian_informasi_yang_diinginkan").val();
      parameter["kategori_permohonan_informasi_publik"] = $("#jenis_permohonan").val();
      parameter["tujuan_penggunaan_informasi"] = $("#tujuan_penggunaan_informasi").val();
      parameter["cara_melihat_membaca_mendengarkan_mencatat"] = $("#cara_melihat_membaca_mendengarkan_mencatat").val();
      parameter["cara_hardcopy_softcopy"] = $("#cara_hardcopy_softcopy").val();
      parameter["cara_mengambil_langsung"] = $("#cara_mengambil_langsung").val();
      parameter["cara_kurir"] = $("#cara_kurir").val();
      parameter["cara_pos"] = $("#cara_pos").val();
      parameter["cara_faksimili"] = $("#cara_faksimili").val();
      parameter["cara_email"] = $("#cara_email").val();
      parameter["temp"] = $("#temp").val();
      var url = '<?php echo base_url(); ?>pelayanan_informasi/update_permohonan_informasi_publik';
      
      var parameterRv = [ 'id_permohonan_informasi_publik', 'nama', 'alamat', 'nomor_telp' ];
      var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
      }
    });
  });
</script>
<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_permohonan_informasi_publik').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'permohonan_informasi_publik',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_permohonan_informasi_publik').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_permohonan_informasi_publik').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_permohonan_informasi_publik').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_permohonan_informasi_publik').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
