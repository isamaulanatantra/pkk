<!-- === BEGIN HEADER === -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" class=" js no-touch cssanimations">
    <!--<![endif]-->
    <head>
        <!-- Title -->
        <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
        <!-- Meta -->
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
        <meta name="author" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>,">
        <meta name="description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>">
        <meta name="og:description" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
        <meta name="og:url" content="<?php echo base_url(); ?>"/>
        <meta name="og:title" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>"/>
        <meta name="og:image" content="<?php echo base_url(); ?>media/logo wonosobo.png"/>
        <meta name="og:keywords" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>, <?php if(!empty( $domain )){ echo $domain; } ?>"/>
        <!-- Favicon -->
        <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />
        
  <!-- Fonts START -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700|PT+Sans+Narrow|Source+Sans+Pro:200,300,400,600,700,900&amp;subset=all" rel="stylesheet" type="text/css">
  <!-- Fonts END -->

  <!-- Global styles START -->          
  <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Global styles END --> 
   
  <!-- Page level plugin styles START -->
  <link href="<?php echo base_url(); ?>Template/theme/assets/pages/css/animate.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/fancybox/source/jquery.fancybox.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/owl.carousel/assets/owl.carousel.css" rel="stylesheet">
  <!-- Page level plugin styles END -->

  <!-- Theme styles START -->
  <link href="<?php echo base_url(); ?>Template/theme/assets/pages/css/components.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>Template/theme/assets/pages/css/slider.css" rel="stylesheet">
  <link href="<?php echo base_url(); ?>Template/theme/assets/corporate/css/style.css" rel="stylesheet"> 
  <link href="<?php echo base_url(); ?>Template/theme/assets/corporate/css/style-responsive.css" rel="stylesheet">
        <?php
        $web=$this->uut->namadomain(base_url());
        ?>
  <link href="<?php echo base_url(); ?>Template/theme/assets/corporate/css/custom.css" rel="stylesheet">
  <!-- Theme styles END
  <link href="<?php echo base_url(); ?>Template/theme/assets/corporate/css/themes/red.css" rel="stylesheet" id="style-color">
  -->
    <!-- Bootstrap 3.3.2 -->
    <link href="<?php echo base_url(); ?>boots/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
       
		<!-- Alert Confirmation -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/datepicker.css" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>css/Typeahead-BS3-css.css" id="toggleCSS" />
		<link rel="stylesheet" href="<?php echo base_url(); ?>boots/dist/css/bootstrap-timepicker.min.css" />
    <link href="<?php echo base_url(); ?>Template/theme/assets/corporate/css/temane.css" rel="stylesheet" id="style-color">
	
    <!-- Theme CSS
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/portfolio/css/style.css"> -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/styles_biru.css">
	<style id="fit-vids-style">.fluid-width-video-wrapper{width:100%;position:relative;padding:0;}.fluid-width-video-wrapper iframe,.fluid-width-video-wrapper object,.fluid-width-video-wrapper embed {position:absolute;top:0;left:0;width:100%;height:100%;}</style>
</head>
    <body class="corporate">
		
		<!-- BEGIN TOP BAR -->
		<div class="pre-header">
			<div class="container">
				<div class="row">
					<!-- BEGIN TOP BAR LEFT PART -->
					<div class="col-md-6 col-sm-6 additional-shop-info">
						<ul class="list-unstyled list-inline">
							<li><i class="fa fa-phone"></i><span><?php if(!empty( $telpon )){ echo $telpon; } ?></span></li>
							<li><i class="fa fa-envelope-o"></i><span><?php if(!empty( $email )){ echo $email; } ?></span></li>
						</ul>
					</div>
					<!-- END TOP BAR LEFT PART -->
					<!-- BEGIN TOP BAR MENU -->
					<div class="col-md-6 col-sm-6 additional-nav">
						<ul class="list-unstyled list-inline pull-right">
							<li><a href="<?php echo base_url('login'); ?>">Log In</a></li>
							<li><a href="https://diskominfo.wonosobokab.go.id/postings/galeri/1892/FAQ.HTML">FAQ</a></li>
						</ul>
					</div>
					<!-- END TOP BAR MENU -->
				</div>
			</div>        
		</div>

		<!-- END TOP BAR -->
		<!-- BEGIN HEADER -->
		<div class="header">
		  <div class="container">
			<a href="javascript:void(0);" class="mobi-toggler"><i class="fa fa-bars"></i></a>

			<!-- BEGIN NAVIGATION  class="header-navigation pull-right font-transform-inherit"-->
			<div class="header-navigation pull-right font-transform-inherit"  id="hornav">
								<?php echo ''.$menu_atas.''; ?>
			</div>
			<!-- END NAVIGATION -->
		  </div>
		</div>
		<!-- Header END -->
		
		<div class="main">
		  <div class="container">
			<div class="main-content mag-content clearfix" style="margin-top:-20px;">
		  
					<!-- BEGIN RECENT WORKS -->
					<div class="row recent-work margin-bottom-40">
						<div class="col-md-12">
							<div class="content-page">
								<div class="row margin-bottom-40">
                                    <?php $this -> load -> view($main_view);  ?>
                                    <?php if(!empty($isi_posting)){ $this -> load -> view('modul/'.$nama_modul.''); } ?>
								</div>       
							</div>       
						</div>
					</div>
					<!-- END RECENT WORKS -->
			
			
			</div>
		  </div>
		</div>
		
		<!-- BEGIN FOOTER -->
		<div class="footer source-org vcard copyright clearfix" style="margin-top:2px;">
				<div class="footer-main">
					<div class="fixed-main">
						<!-- BEGIN PRE-FOOTER -->
							<div class="container">
								<div class="row">
									<!-- BEGIN BOTTOM ABOUT BLOCK -->
									<div class="col-md-4 col-sm-6 pre-footer-col">
										<?php if(!empty( $KolomKiriBawah )){ echo $KolomKiriBawah; } ?>
										<div>
											<i class="fa fa-bar-chart-o"></i> <span id="hit_counter"></span>
										</div>
										<div>
											<i class="fa fa-bar-chart-o"></i> <span id="visitor"></span>
										</div>
									</div>
									<!-- END BOTTOM ABOUT BLOCK -->

									<!-- BEGIN BOTTOM CONTACTS -->
									<div class="col-md-4 col-sm-6 pre-footer-col">
										<h2>Kontak Kami</h2>
										<address class="margin-bottom-40">
											<?php if(!empty( $alamat )){ echo $alamat; } ?><br>
											Phone: <?php if(!empty( $telpon )){ echo $telpon; } ?><br>
											Email: <a href="mailto:<?php if(!empty( $email )){ echo $email; } ?>"><?php if(!empty( $email )){ echo $email; } ?></a><br>
											Website: <a href="https://<?php echo $web; ?>">https://<?php echo $web; ?></a>
										</address>

									</div>
									<!-- END BOTTOM CONTACTS -->

									<!-- BEGIN TWITTER BLOCK --> 
									<div class="col-md-4 col-sm-6 pre-footer-col">
										<?php if(!empty( $KolomPalingBawah )){ echo $KolomPalingBawah; } ?>
									</div>
									<!-- END TWITTER BLOCK -->
								</div>
							</div>
						<div class="footer-bottom clearfix">
								<div class="fixed-main">
									<div class="container">
									<div class="mag-content">
										<div class="row">
											<!-- BEGIN COPYRIGHT -->
											<div class="col-md-4 col-sm-4 padding-top-10">
												<?php echo date('Y'); ?> © <?php echo $web; ?>. 
											</div>
											<!-- END COPYRIGHT -->
											<!-- BEGIN PAYMENTS -->
											<div class="col-md-4 col-sm-4">
												<ul class="social-footer list-unstyled list-inline pull-right">
													<li><a target="_blank" href="<?php if(!empty( $facebook )){ echo $facebook; } ?>"><i class="fa fa-facebook"></i></a></li>
													<li><a target="_blank" href="<?php if(!empty( $google )){ echo $google; } ?>"><i class="fa fa-google-plus"></i></a></li>
													<li><a target="_blank" href="<?php if(!empty( $twitter )){ echo $twitter; } ?>"><i class="fa fa-twitter"></i></a></li>
													<li><a target="_blank" href="<?php if(!empty( $instagram )){ echo $instagram; } ?>"><i class="fa fa-instagram"></i></a></li>
													<li><a target="_blank" href="javascript:;"><i class="fa fa-youtube"></i></a></li>
												</ul>  
											</div>
											<!-- END PAYMENTS -->
											<!-- BEGIN POWERED -->
											<div class="col-md-4 col-sm-4 text-right">
												<p class="powered">Powered by: <a href="https://<?php echo $web; ?>"><?php echo $web; ?></a></p>
											</div>
											<!-- END POWERED -->
										</div>
									</div>
								</div>
								</div>
						</div>
			</div>
		  </div>
		</div>
		<!-- END FOOTER -->
		<script src="<?php echo base_url(); ?>Template/theme/assets/plugins/jquery.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>Template/theme/assets/plugins/jquery-migrate.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>Template/theme/assets/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>      
		<script src="<?php echo base_url(); ?>Template/theme/assets/corporate/scripts/back-to-top.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>Template/theme/assets/plugins/fancybox/source/jquery.fancybox.pack.js" type="text/javascript"></script><!-- pop up -->
		<script src="<?php echo base_url(); ?>Template/theme/assets/plugins/owl.carousel/owl.carousel.min.js" type="text/javascript"></script><!-- slider for products -->
		<script src="<?php echo base_url(); ?>Template/theme/assets/corporate/scripts/layout.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>Template/theme/assets/pages/scripts/bs-carousel.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/uut.js"></script>
		<script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
		<script src="<?php echo base_url(); ?>assets/portfolio/js/modernizr.min.js"></script>

    <!-- Morris.js charts -->
    <script src="<?php echo base_url(); ?>assets/raphael/raphael.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/morris.js/morris.min.js" type="text/javascript"></script>
		<!---
		<script src="https://180.250.150.76/simpus/js/colorbox/jquery.colorbox.js"></script>
 -->
    <script src="<?php echo base_url(); ?>assets/js/bootstrap-typeahead.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/hogan-2.0.0.js"></script>
		<!-- daterangepicker -->
    <script src="<?php echo base_url(); ?>assets/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>assets/bootstrap-timepicker/js/bootstrap-timepicker.js" type="text/javascript"></script>
		
	<script type="text/javascript">
        jQuery(document).ready(function() {
            Layout.init();    
            Layout.initOWL();
            Layout.initTwitter();
            Layout.initFixHeaderWithPreHeader(); /* Switch On Header Fixing (only if you have pre-header) */
            Layout.initNavScrolling();
        });
    </script>
		
	<div id="fb-root"></div>
	<script>
	  (function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.12&appId=351370971628122&autoLogAppEvents=1';
		fjs.parentNode.insertBefore(js, fjs);
	  }(document, 'script', 'facebook-jssdk'));
	</script>
		
            <script>
              function LoadVisitor() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    table:'visitor'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/simpan_visitor/',
                  success: function(html) {
                    $('#visitor').html('Total Pengunjung '+html+' ');
                  }
                });
              }
            </script>
            <script>
              function LoadHitCounter() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    current_url:'<?php echo current_url(); ?>'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/hit_counter/',
                  success: function(html) {
                    $('#hit_counter').html(''+html+' Kali ');
                  }
                });
              }
            </script>
            <script type="text/javascript">
            $(document).ready(function() {
              LoadVisitor();
              LoadHitCounter();
            });
            </script>
            <!-- End Footer -->
		
    </body>
</html>
<!-- === END FOOTER === -->