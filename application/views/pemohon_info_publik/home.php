
  <div class="">
          <div class="row" id="awal">
            <div class="col-md-12" id="div_form_input" style="display:none;">
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Formulir Pemohon_info_publik</h3>
                </div>
                <div class="box-body">
                  <form role="form" id="form_pemohon_info_publik" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=pemohon_info_publik" enctype="multipart/form-data">
                    <div class="box-body">
                    
                      <input id="id_pemohon_info_publik" name="id_pemohon_info_publik" type="hidden">
                      <input id="kode_pemohon_info_publik" name="kode_pemohon_info_publik" type="hidden" value="">
                      <div class="form-group">
                        <label for="nama_pemohon_info_publik">Nama Pemohon</label>
                        <input class="form-control" id="nama_pemohon_info_publik" name="nama_pemohon_info_publik" value="" placeholder="Nama " type="text">
                      </div>
                      <div class="form-group">
                        <label for="jenis_kelamin">Jenis Kelamin</label>
                        <select class="form-control" id="jenis_kelamin" name="jenis_kelamin">
                          <option value="-">Pilih</option>
                          <option value="L">Laki-laki</option>
                          <option value="P">Perempuan</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input class="form-control" id="alamat" name="alamat" value="" placeholder="alamat " type="text">
                      </div>
                      <div class="form-group">
                        <label for="pekerjaan">Pekerjaan</label>
                        <input class="form-control" id="pekerjaan" name="pekerjaan" value="" placeholder="pekerjaan " type="text">
                      </div>
                      <div class="form-group">
                        <label for="nomor_telpon">Nomor Telpon</label>
                        <input class="form-control" id="nomor_telpon" name="nomor_telpon" value="" placeholder="085222111000 " type="text">
                      </div>
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input class="form-control" id="email" name="email" value="" placeholder="nama@mail.com " type="text">
                      </div>
                      <div class="form-group">
                        <label for="rincian_informasi_yang_dibutuhkan">Rincian</label>
                        <textarea id="rincian_informasi_yang_dibutuhkan"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="tujuan_penggunaan_informasi">Tujuan Penggunaan Informasi</label>
                        <input class="form-control" id="tujuan_penggunaan_informasi" name="tujuan_penggunaan_informasi" value="" placeholder="Tujuan Penggunaan Informasi " type="text">
                      </div>
                      <div class="form-group">
                        <label for="tanggal_pengambilan_informasi">Tanggal Pengambilan Informasi</label>
                        <input class="form-control" id="tanggal_pengambilan_informasi" name="tanggal_pengambilan_informasi" value="" placeholder="Tujuan Penggunaan Informasi " type="text">
                      </div>
                      <div class="form-group">
                        <label for="cara_memperoleh_informasi">Cara Memperoleh Informasi</label>
                        <select class="form-control" id="cara_memperoleh_informasi" name="cara_memperoleh_informasi">
                          <option value="-">Pilih</option>
                          <option value="MELIHAT">Melihat</option>
                          <option value="MEMBACA">Membaca</option>
                          <option value="MENDENGARKAN">Mendengarkan</option>
                          <option value="MENCATAT">Mencatat</option>
                          <option value="HARDCOPY">Hardcopy</option>
                          <option value="SOFTCOPY">Softcopy</option>
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="cara_mendapatkan_salinan_informasi">Cara Memperoleh Informasi</label>
                        <select class="form-control" id="cara_mendapatkan_salinan_informasi" name="cara_mendapatkan_salinan_informasi">
                          <option value="-">Pilih</option>
                          <option value="mengambil langsung">Mengambil Langsung</option>
                          <option value="kurir">Kurir</option>
                          <option value="pos">Pos</option>
                          <option value="faksimili">Faksimili</option>
                          <option value="email">Email</option>
                        </select>
                      </div>
                      <input id="inserted_by" name="inserted_by" type="hidden">
                      <input id="inserted_time" name="inserted_time" type="hidden">
                      <input id="updated_by" name="updated_by" type="hidden">
                      <input id="updated_time" name="updated_time" type="hidden">
                      <input id="deleted_by" name="deleted_by" type="hidden">
                      <input id="deleted_time" name="deleted_time" type="hidden">
                      <input id="temp" name="temp" type="hidden">
                        <input class="form-control" id="keterangan" name="keterangan" value="keterangan" placeholder="Keterangan" type="hidden">
                      <input id="status" name="status" type="hidden">
                                          
                      <div class="alert alert-info alert-dismissable">
                        <div class="form-group">
                          <label for="remake">Keterangan Lampiran </label>
                          <input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran " type="text">
                        </div>
                        <div class="form-group">
                          <div class="btn btn-default btn-file">
                            <i class="fa fa-paperclip"></i> File Lampiran 
                            <input type="file" name="myfile" id="file_lampiran">
                          </div>
                        </div>
                        <div id="ProgresUpload">
                          <div id="BarProgresUpload"></div>
                          <div id="PersenProgresUpload">0%</div >
                        </div>
                        <div id="PesanProgresUpload"></div>
                      </div>
										<div class="alert alert-info alert-dismissable">
											<h3 class="box-title">Data Lampiran </h3>
											<table class="table table-bordered">
												<tr>
													<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
												</tr>
												<tbody id="tbl_attachment_pemohon_info_publik">
												</tbody>
											</table>
										</div>
                    </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" id="simpan_pemohon_info_publik">SIMPAN</button>
                    </div>
                  </form>
                </div>
                <div class="overlay" id="overlay_form_input" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row" id="e_div_form_input" style="display:none;">
            <div class="col-md-6">
              <div class="box box-info box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Formulir Edit Pemohon_info_publik</h3>
                </div>
                <div class="box-body">
                  <form role="form" id="e_form_pemohon_info_publik" method="post" action="<?php echo base_url(); ?>attachment/e_upload/?table_name=pemohon_info_publik" enctype="multipart/form-data">
                    <div class="box-body">
                      
                      <input class="form-control" id="e_id_pemohon_info_publik" name="id" value="" type="hidden">
                      <div class="form-group">
                        <label for="e_option_id_pemohon_info_publik">Pemohon_info_publik</label>
                        <select class="form-control" id="e_option_id_pemohon_info_publik" name="e_option_id_pemohon_info_publik" value="" placeholder="Kode Pemohon_info_publik" type="text">
                        </select>
                      </div>
                      <div class="form-group">
                        <label for="e_nama_pemohon_info_publik">Isi Pemohon_info_publik</label>
                        <textarea id="e_nama_pemohon_info_publik"></textarea>
                      </div>
                      <input class="form-control" id="e_inserted_by" name="e_inserted_by" value="" type="hidden">
                      <input class="form-control" id="e_inserted_time" name="e_inserted_time" value="" type="hidden">
                      <input class="form-control" id="e_updated_by" name="e_updated_by" value="" type="hidden">
                      <input class="form-control" id="e_updated_time" name="e_updated_time" value="" type="hidden">
                      <input class="form-control" id="e_deleted_by" name="e_deleted_by" value="" type="hidden">
                      <input class="form-control" id="e_deleted_time" name="e_deleted_time" value="" type="hidden">
                      <input class="form-control" id="e_temp" name="e_temp" value="" type="hidden">
                        <input class="form-control" id="e_keterangan" name="e_keterangan" value="" placeholder="Keterangan" type="hidden">
                      <input class="form-control" id="e_status" name="e_status" value="" type="hidden">
                                          
                      <div class="nav-tabs-custom">
                        <!-- Tabs within a box -->
                        <ul class="nav nav-tabs">
                          <li><a data-toggle="tab" href="#e_lampiran"><i class="fa fa-paperclip"></i></a></li>
                        </ul>
                        <div class="tab-content no-padding">
                          <div id="e_lampiran" class="tab-pane">
                            <div class="form-group">
                              <label for="remake">Keterangan Lampiran Pemohon_info_publik</label>
                              <input class="form-control" id="e_remake" name="remake" placeholder="Keterangan Lampiran Pemohon_info_publik" type="text">
                            </div>
                            <div class="form-group">
                                <div class="btn btn-default btn-file">
                                  <i class="fa fa-paperclip"></i> File Lampiran Pemohon_info_publik
                                  <input type="file" name="myfile" id="e_pemohon_info_publik_baru">
                                </div>
                            </div>
                            <div id="e_progress_upload_lampiran_pemohon_info_publik">
                              <div id="e_bar_progress_upload_lampiran_pemohon_info_publik"></div>
                              <div id="e_percent_progress_upload_lampiran_pemohon_info_publik">0%</div >
                            </div>
                            <div id="e_message_progress_upload_lampiran_pemohon_info_publik"></div>
                            <h3 class="box-title">Data Lampiran </h3>
                            <table class="table table-bordered">
                              <tr>
                                <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                              </tr>
                              <tbody id="e_tbl_lampiran_pemohon_info_publik">
                              </tbody>
                            </table>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="box-footer">
                      <button type="submit" class="btn btn-primary" id="update_data_pemohon_info_publik">SIMPAN</button>
                    </div>
                  </form>
                </div>
                <div class="overlay" id="e_overlay_form_input" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row" id="div_data_default">
            <div class="col-md-4">
              <div class="external-event bg-aqua no-padding">
                <a class="btn btn-block btn-social btn-foursquare" id="klik_tab_input" href="#">
                  <i class="fa fa-foursquare"></i> Formulir Permohonan Informasi Publik
                </a>
              </div>
            </div>
            <div class="col-md-12">
              <div class="box box-info">
                <div class="box-header">
                  <h3 class="box-title"><b>DATA PERMOHONAN INFORMASI PUBLIK</b></h3>
                </div>
                <div class="box-body table-responsive no-padding">
                  <table class="table table-bordered">
                    <tr>
                      <th>NO</th>
                      <th>TANGGAL</th>
                      <th>NAMA</th>
                      <th>JENIS KELAMIN</th>
                      <th>ALAMAT</th>
                      <th>INFORMASI YANG DIMINTA</th>
                      <th>KETERANGAN</th>
                    </tr>
                    <tbody id="tbl_utama_pemohon_info_publik">
                    </tbody>
                  </table>
                  <div class="box-footer clearfix">
                    <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                    <?php
                    for ($x = 1; $x <= $total; $x++) {
                      echo '<li page="'.$x.'" id="'.$x.'"><a class="update_id" href="#">'.$x.'</a></li>';
                    }
                    ?>
                    </ul>
                  </div>
                </div>
                <div class="overlay" id="overlay_data_default" style="display:none;">
                  <i class="fa fa-refresh fa-spin"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_input').on('click', function(e) {
	$('#e_div_form_input').fadeOut('slow');
	$('#div_form_input').show();
	});
	var aaa = Math.random();
  $('#temp').val(aaa);
});
</script>

<script>
  function load_default(halaman) {
    $('#overlay_data_default').show();
    $('#tbl_utama_pemohon_info_publik').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>pemohon_info_publik/json_all_pemohon_info_publik/?halaman='+halaman+'/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * <?php echo $per_page; ?>);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_pemohon_info_publik="' + json[i].id_pemohon_info_publik + '" id="' + json[i].id_pemohon_info_publik + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
					tr += '<td valign="top">' + json[i].tanggal_pengambilan_informasi + '</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>pemohon_info_publik/views/?id_pemohon_info_publik=' + json[i].id_pemohon_info_publik + '">' + json[i].nama_pemohon_info_publik + '</a></td>';
					tr += '<td valign="top">' + json[i].jenis_kelamin + '</td>';
					tr += '<td valign="top">' + json[i].alamat + '</td>';
					tr += '<td valign="top">' + json[i].rincian_informasi_yang_dibutuhkan + '</td>';
					tr += '<td valign="top">' + json[i].cara_memperoleh_informasi + '</td>';
					// tr += '<td valign="top"><a href="<?php echo base_url(); ?>admin_pemohon_info_publik/?id_pemohon_info_publik=' + json[i].id_pemohon_info_publik + '">Tampilkan</a></td>';
          tr += '</tr>';
        }
        $('#tbl_utama_pemohon_info_publik').append(tr);
				$('#overlay_data_default').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#pagination').on('click', '.update_id', function(e) {
		e.preventDefault();
		var id = $(this).closest('li').attr('page');
		var halaman = id;
		load_default(halaman);
	});
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	var halaman = 1;
	load_default(halaman);
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#tbl_utama_pemohon_info_publik, #tbl_search_pemohon_info_publik').on('click', '.update_id', function(e) {
		e.preventDefault();
		$('#e_overlay_form_input').show();
		var id = $(this).closest('tr').attr('id_pemohon_info_publik');
		$.ajax({
			dataType: 'json',
			url: '<?php echo base_url(); ?>pemohon_info_publik/pemohon_info_publik_get_by_id/?id='+id+'',
			success: function(json) {
				if (json.length == 0) {
					alert('Tidak ada data');
				} else {
					for (var i = 0; i < json.length; i++) {
					$('#e_id_pemohon_info_publik').val(json[i].id_pemohon_info_publik);
					$('#e_kode_pemohon_info_publik').val(json[i].kode_pemohon_info_publik);
					$('#e_nama_pemohon_info_publik').val(json[i].nama_pemohon_info_publik);
					$('#e_inserted_by').val(json[i].inserted_by);
					$('#e_inserted_time').val(json[i].inserted_time);
					$('#e_updated_by').val(json[i].updated_by);
					$('#e_updated_time').val(json[i].updated_time);
					$('#e_deleted_by').val(json[i].deleted_by);
					$('#e_deleted_time').val(json[i].deleted_time);
					$('#e_temp').val(json[i].temp);
					$('#e_keterangan').val(json[i].keterangan);
					$('#e_status').val(json[i].status);
										}
					e_load_lampiran_pemohon_info_publik(id);
					$('#div_form_input').fadeOut('slow');
					$('#e_div_form_input').show();
					$('#e_overlay_form_input').fadeOut(2000);
					$('html, body').animate({
						scrollTop: $('#awal').offset().top
					}, 1000);
				}
			}
		});
	});
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_pemohon_info_publik').on('click', function(e) {
      e.preventDefault();
      $('#simpan_pemohon_info_publik').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
			
			var id_pemohon_info_publik = $("#id_pemohon_info_publik").val();
			var kode_pemohon_info_publik = $("#kode_pemohon_info_publik").val();
			var nama_pemohon_info_publik = $("#nama_pemohon_info_publik").val();
			var jenis_kelamin = $("#jenis_kelamin").val();
			var alamat = $("#alamat").val();
			var pekerjaan = $("#pekerjaan").val();
			var nomor_telpon = $("#nomor_telpon").val();
			var email = $("#email").val();
      var rincian_informasi_yang_dibutuhkan = CKEDITOR.instances.rincian_informasi_yang_dibutuhkan.getData();
			var tujuan_penggunaan_informasi = $("#tujuan_penggunaan_informasi").val();
			var cara_memperoleh_informasi = $("#cara_memperoleh_informasi").val();
			var cara_mendapatkan_salinan_informasi = $("#cara_mendapatkan_salinan_informasi").val();
			var tanggal_pengambilan_informasi = $("#tanggal_pengambilan_informasi").val();
			var inserted_by = $("#inserted_by").val();
			var inserted_time = $("#inserted_time").val();
			var updated_by = $("#updated_by").val();
			var updated_time = $("#updated_time").val();
			var deleted_by = $("#deleted_by").val();
			var deleted_time = $("#deleted_time").val();
			var temp = $("#temp").val();
			var keterangan = $("#keterangan").val();
			var status = $("#status").val();
						
			if (nama_pemohon_info_publik == '') {
					$('#nama_pemohon_info_publik').css('background-color', '#DFB5B4');
				} else {
					$('#nama_pemohon_info_publik').removeAttr('style');
				}
			if (jenis_kelamin == '') {
					$('#jenis_kelamin').css('background-color', '#DFB5B4');
				} else {
					$('#jenis_kelamin').removeAttr('style');
				}
			if (alamat == '') {
					$('#alamat').css('background-color', '#DFB5B4');
				} else {
					$('#alamat').removeAttr('style');
				}
			if (nomor_telpon == '') {
					$('#nomor_telpon').css('background-color', '#DFB5B4');
				} else {
					$('#nomor_telpon').removeAttr('style');
				}
			if (email == '') {
					$('#email').css('background-color', '#DFB5B4');
				} else {
					$('#email').removeAttr('style');
				}
			if (tujuan_penggunaan_informasi == '') {
					$('#tujuan_penggunaan_informasi').css('background-color', '#DFB5B4');
				} else {
					$('#tujuan_penggunaan_informasi').removeAttr('style');
				}
			if (rincian_informasi_yang_dibutuhkan == '') {
					$('#rincian_informasi_yang_dibutuhkan').css('background-color', '#DFB5B4');
				} else {
					$('#rincian_informasi_yang_dibutuhkan').removeAttr('style');
				}
			if (cara_memperoleh_informasi == '') {
					$('#cara_memperoleh_informasi').css('background-color', '#DFB5B4');
				} else {
					$('#cara_memperoleh_informasi').removeAttr('style');
				}
			if (cara_mendapatkan_salinan_informasi == '') {
					$('#cara_mendapatkan_salinan_informasi').css('background-color', '#DFB5B4');
				} else {
					$('#cara_mendapatkan_salinan_informasi').removeAttr('style');
				}
			if (tanggal_pengambilan_informasi == '') {
					$('#tanggal_pengambilan_informasi').css('background-color', '#DFB5B4');
				} else {
					$('#tanggal_pengambilan_informasi').removeAttr('style');
				}
			if (inserted_by == '') {
					$('#inserted_by').css('background-color', '#DFB5B4');
				} else {
					$('#inserted_by').removeAttr('style');
				}
			if (temp == '') {
					$('#temp').css('background-color', '#DFB5B4');
				} else {
					$('#temp').removeAttr('style');
				}
						
			$.ajax({
        type: 'POST',
        async: true,
        data: {
					kode_pemohon_info_publik:kode_pemohon_info_publik,
					nama_pemohon_info_publik:nama_pemohon_info_publik,
					jenis_kelamin:jenis_kelamin,
					alamat:alamat,
					pekerjaan:pekerjaan,
					nomor_telpon:nomor_telpon,
					email:email,
					rincian_informasi_yang_dibutuhkan:rincian_informasi_yang_dibutuhkan,
					tujuan_penggunaan_informasi:tujuan_penggunaan_informasi,
					cara_memperoleh_informasi:cara_memperoleh_informasi,
					cara_mendapatkan_salinan_informasi:cara_mendapatkan_salinan_informasi,
					tanggal_pengambilan_informasi:tanggal_pengambilan_informasi,
					temp:temp,
					keterangan:keterangan,
					status:status,
										},
        dataType: 'text',
        url: '<?php echo base_url(); ?>pemohon_info_publik/simpan_pemohon_info_publik/',
        success: function(json) {
          if (json == '0') {
            $('#simpan_pemohon_info_publik').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut();
            alert('gagal');
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
          } else {
						alertify.alert('Data berhasil disimpan');
						$('#tbl_lampiran_pemohon_info_publik').html('');
						$('#message_progress_upload_lampiran_pemohon_info_publik').html('');
            var halaman = 1;
            load_default(halaman);
            CKEDITOR.instances.rincian_informasi_yang_dibutuhkan.setData('');
            var aaa = Math.random();
            $('#temp').val(aaa);
            $('#div_form_input form').trigger('reset');
            $('#simpan_pemohon_info_publik').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut('slow');
          }
        }
      });
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#update_data_pemohon_info_publik').on('click', function(e) {
		e.preventDefault();
		$('#update_data_pemohon_info_publik').attr('disabled', 'disabled');
		$('#e_overlay_form_input').show();
		var id_pemohon_info_publik = $('#e_id_pemohon_info_publik').val();
		var kode_pemohon_info_publik = $('#e_option_id_pemohon_info_publik').val();
		var nama_pemohon_info_publik = CKEDITOR.instances.e_nama_pemohon_info_publik.getData();
		var temp = $('#e_temp').val();
		var keterangan = $('#e_keterangan').val();
						
		if (id_pemohon_info_publik == '') {
				$('#e_id_pemohon_info_publik').css('background-color', '#DFB5B4');
			} else {
				$('#e_id_pemohon_info_publik').removeAttr('style');
			}
		if (nama_pemohon_info_publik == '') {
				$('#e_nama_pemohon_info_publik').css('background-color', '#DFB5B4');
			} else {
				$('#e_nama_pemohon_info_publik').removeAttr('style');
			}
		if (temp == '') {
				$('#e_temp').css('background-color', '#DFB5B4');
			} else {
				$('#e_temp').removeAttr('style');
			}
		if (keterangan == '') {
				$('#e_keterangan').css('background-color', '#DFB5B4');
			} else {
				$('#e_keterangan').removeAttr('style');
			}
				
			$.ajax({
			type: 'POST',
			async: true,
			data: {
					
					id_pemohon_info_publik:id_pemohon_info_publik,
					kode_pemohon_info_publik:kode_pemohon_info_publik,
					nama_pemohon_info_publik:nama_pemohon_info_publik,
					temp:temp,
					keterangan:keterangan,
										
				},
			dataType: 'json',
			url: '<?php echo base_url(); ?>pemohon_info_publik/update_data_pemohon_info_publik/',
			success: function(json) {
				if (json.length == 0) {            
					alert('edit gagal');
					$('#update_data_pemohon_info_publik').removeAttr('disabled', 'disabled');
					$('#e_overlay_form_input').fadeOut('slow');
					} 
				else {
					alertify.alert('Edit Data Berhasil');
					var halaman = 1;
					load_default(halaman);
					$('#e_div_form_input form').trigger('reset');
					$('#update_data_pemohon_info_publik').removeAttr('disabled', 'disabled');
					$('#e_overlay_form_input').fadeOut('slow');
					$('#e_div_form_input').fadeOut('slow');
					$('#tbl_lampiran_pemohon_info_publik').html('');
					$('#e_tbl_lampiran_pemohon_info_publik').html('');
					$('#a_tbl_lampiran_pemohon_info_publik').html('');
					}
			}
		});
	});
});
</script>

<script>
function reset() {
	$('#toggleCSS').attr('href', '<?php echo base_url(); ?>css/alertify/alertify.default.css');
	alertify.set({
		labels: {
			ok: 'OK',
			cancel: 'Cancel'
		},
		delay: 5000,
		buttonReverse: false,
		buttonFocus: 'ok'
	});
}
//===============HAPUS OBAT
$('#tbl_search_pemohon_info_publik, #tbl_utama_pemohon_info_publik').on('click', '#del_ajax', function() {
	reset();
	var id = $(this).closest('tr').attr('id_pemohon_info_publik');
	alertify.confirm('Anda yakin data akan dihapus?', function(e) {
		if (e) {
			$.ajax({
				dataType: 'json',
				url: '<?php echo base_url(); ?>pemohon_info_publik/hapus_pemohon_info_publik/?id='+id+'',
				success: function(response) {
					if (response.errors == 'Yes') {
						alertify.alert('Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya');
					} else {
						$('[id_pemohon_info_publik='+id+']').remove();
						alertify.alert('Data berhasil dihapus');
					}
				}
			});
		} else {
			alertify.alert('Hapus data dibatalkan');
		}
	});
});
</script>

<script>
  function AfterSavedPemohon_info_publik() {
    $('#id_pemohon_info_publik, #keterangan').val('');
    $('#tbl_attachment_pemohon_info_publik').html('');
    $('#PesanProgresUpload').html('');
    load_default();
  }
</script>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_attachment_pemohon_info_publik').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'pemohon_info_publik',
				mode:mode,
        value:value
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_attachment_pemohon_info_publik').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_pemohon_info_publik').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_attachment_pemohon_info_publik').on('click', '#del_ajax', function() {
    var id_attachment = $(this).closest('tr').attr('id_attachment');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_attachment"] = id_attachment;
        var url = '<?php echo base_url(); ?>attachment/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_pemohon_info_publik').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_attachment='+id_attachment+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
<script>
  function load_id_pemohon_info_publik() {
    var temp = $('#temp').val();
    var halaman = 1;
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp,
        halaman: halaman
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>pemohon_info_publik/json_all_pemohon_info_publik/?halaman='+halaman+'/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * <?php echo $per_page; ?>);
        tr += '<option class="" value="">Pilih pemohon_info_publik</option>';
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<option class="" value="' + json[i].id_pemohon_info_publik + '" id="' + json[i].id_pemohon_info_publik + '" >' + json[i].nama_pemohon_info_publik + '</option>';
        }
        $('#option_id_pemohon_info_publik').html(tr);
      }
    });
  }
  function load_e_id_pemohon_info_publik() {
    var temp = $('#temp').val();
    var halaman = 1;
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp,
        halaman: halaman
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>pemohon_info_publik/json_all_pemohon_info_publik/?halaman='+halaman+'/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * <?php echo $per_page; ?>);
        tr += '<option class="" value="">Pilih pemohon_info_publik</option>';
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<option class="" value="' + json[i].id_pemohon_info_publik + '" id="' + json[i].id_pemohon_info_publik + '" >' + json[i].nama_pemohon_info_publik + '</option>';
        }
        $('#e_option_id_pemohon_info_publik').html(tr);
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	load_id_pemohon_info_publik();
	load_e_id_pemohon_info_publik();
});
</script>

<script type="text/javascript">
$(function() {
	// Replace the <textarea id="editor1"> with a CKEditor
	// instance, using default configuration.
	CKEDITOR.replace('rincian_informasi_yang_dibutuhkan');
	CKEDITOR.replace('e_rincian_informasi_yang_dibutuhkan');
	//bootstrap WYSIHTML5 - text editor
	$(".textarea").wysihtml5();
});
</script>