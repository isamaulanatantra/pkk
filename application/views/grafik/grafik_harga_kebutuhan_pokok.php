<style>
.datepicker{z-index:9999 !important}
</style>
<style>
#myProgress {
  position: relative;
  width: 100%;
  height: 5px;
  background-color: #ddd;
}

#myBar {
  position: absolute;
  width: 1%;
  height: 100%;
  background-color: #8B008B;
}
</style>
<form role="form" id="form_pertanyaan" method="post" action="" enctype="multipart/form-data">
  <div class="modal fade" id="FormReviewer" tabindex="-1" role="dialog" aria-labelledby="myReviewer" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myReviewer">Silahkan tentukan peride yang ingin anda tampilkan</h4>
        </div>
        <div class="modal-body">
            <div class="box-body">
              <div class="form-group">
                <label for="dari_tanggal">Dari Tanggal</label>
                <input type="hidden" name="periodik" id="periodik" value="periodik">
                <input class="form-control" placeholder="Dari Tanggal" name="dari_tanggal" value="<?php echo $dari_tanggal; ?>" id="dari_tanggal" type="text">
              </div>
              <div class="form-group">
                <label for="sampai_tanggal">Sampai Tanggal</label>
                <input class="form-control" placeholder="Sampai Tanggal" name="sampai_tanggal" value="<?php echo $sampai_tanggal; ?>" id="sampai_tanggal" type="text">
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="tampilkan"><i id="overlay" style="display:none;" class="fa fa-refresh fa-spin"></i> Tampilkan</button>
        </div>
      </div>
    </div>
  </div>
</form>

<script src="<?php echo base_url(); ?>simple_chart/Chart.js"></script>	

	<?php
	$labels = '';
	$data = '';
	$data2 = '';
	foreach ($test->result() as $b1) 
		{
			$labels .= '"'.$b1->nama_pasar.'",';
			$data .= '"'.$b1->total.'",';
			if( empty($dari_tanggal) ){
						$where3 = array(
						'harga_kebutuhan_pokok.id_pasar' => $b1->id_pasar,
						'harga_kebutuhan_pokok.tanggal' => $hari
						);
					}
					else{
					$where3 = array(
						'harga_kebutuhan_pokok.id_pasar' => $b1->id_pasar,
						'harga_kebutuhan_pokok.tanggal >=' => $dari_tanggal,
						'harga_kebutuhan_pokok.tanggal <=' => $sampai_tanggal
						);
					}
					$this->db->where($where3);
					$this->db->join('harga_kebutuhan_pokok', 'harga_kebutuhan_pokok.id_harga_kebutuhan_pokok=pasar.id_pasar');
					$this->db->from('pasar');

					$data2 .= '"'.$this->db->count_all_results().'",'; 
		}
	?>

<div class="row">
	<div id="myProgress">
    <div id="myBar"></div>
  </div>
  <div class="box box-success">
		<div class="box-header with-border">
			<center><h3 class="box-title">GRAFIK KUNJUNGAN PASIEN DI PUSKESMAS</h3> <br />
			<h5 class="box-title"><?php echo $periode;  ?> <a href="#" id="pop_reviewer" data-toggle="modal" data-target="#FormReviewer" class="btn btn-default" ><i class="fa fa-fw fa-calendar"></i> Pilih Periode Lain</a></h5> <br /></center>
			<img src="<?php echo base_url(); ?>media/grafik1.png" />
		</div>
		<div class="box-body">
			<div style="width: 100%">
				<canvas id="canvas" height="180" width="600"></canvas>
			</div>
		</div>
	</div>
</div>
	

<script>
function move() {
  var elem = document.getElementById("myBar");   
  var width = 1;
  var id = setInterval(frame, 50);
  function frame() {
    if (width >= 50) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = ''+width + '%'; 
    }
  }
}
</script>

<script type="text/JavaScript">
<!--setTimeout("location.href = '<?php echo base_url(); ?>test/grafik_1c';",30000);-->
</script>

<script type="text/javascript">
  // When the document is ready
  $(document).ready(function() {
    $('#dari_tanggal, #sampai_tanggal').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  move();
	$('#tampilkan').on('click', function() {
    $('#overlay').show();
	});
});
</script>

  
	<script>
	//var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

	var barChartData = {
		labels : [<?php echo $labels; ?>],
		datasets : [
			{
				fillColor : "rgba(0,23,255,0.5)",
				strokeColor : "rgba(0,23,255,0.8)",
				highlightFill: "rgba(0,23,255,0.75)",
				highlightStroke: "rgba(0,23,255,1)",
				data : [
					<?php echo $data; ?>
				]
			},
			{
				fillColor : "rgba(0,255,26,0.5)",
				strokeColor : "rgba(0,255,26,0.8)",
				highlightFill : "rgba(0,255,26,0.75)",
				highlightStroke : "rgba(0,255,26,1)",
				data : [
					<?php echo $data2; ?>
				]
			},
		]

	}
	window.onload = function(){
		var ctx = document.getElementById("canvas").getContext("2d");
		window.myBar = new Chart(ctx).Bar(barChartData, {
			responsive : true
		});
	}

	</script>