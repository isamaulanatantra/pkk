
    <!-- Main content -->
    <section class="content">
        <div class="row">
          <div class="col-12">
					
            <div class="card">
              <div class="card-body p-0">

								<div id="map-canvas"></div>
								
              </div><!-- /.card-body -->
            </div><!-- ./card -->
				  
          </div><!-- /.col -->
        </div><!-- /.row -->
    </section>
		
    <script>
      var customLabel = {
        restaurant: {
          label: 'R'
        },
        bar: {
          label: 'B'
        }
      };

        function initMap() {
        var map = new google.maps.Map(document.getElementById('map-canvas'), {
          center: new google.maps.LatLng(-7.3739382, 109.899983),
          zoom: 12
        });
        var infoWindow = new google.maps.InfoWindow;

          // Change this depending on the name of your PHP or XML file
          // downloadUrl('https://storage.googleapis.com/mapsdevsite/json/mapmarkers2.xml', function(data) {
          downloadUrl('https://diskominfo.wonosobokab.go.id/lokasi/mapmarkers', function(data) {
            var xml = data.responseXML;
            var markers = xml.documentElement.getElementsByTagName('marker');
            Array.prototype.forEach.call(markers, function(markerElem) {
              var id = markerElem.getAttribute('id');
              var name = markerElem.getAttribute('name');
              var address = markerElem.getAttribute('address');
              var type = markerElem.getAttribute('type');
              var point = new google.maps.LatLng(
                  parseFloat(markerElem.getAttribute('lat')),
                  parseFloat(markerElem.getAttribute('lng')));

              var infowincontent = document.createElement('div');
              var strong = document.createElement('strong');
              strong.textContent = name
              infowincontent.appendChild(strong);
              infowincontent.appendChild(document.createElement('br'));

              var text = document.createElement('text');
              text.textContent = address
              infowincontent.appendChild(text);
              var icon = customLabel[type] || {};
              // var icon = 'https://diskominfo.wonosobokab.go.id/media/logo_wonosobo_18_kb.png';
              var marker = new google.maps.Marker({
                map: map,
                position: point,
                label: icon.label
								// icon:'pin.png'
              });
              marker.addListener('click', function() {
                infoWindow.setContent(infowincontent);
                infoWindow.open(map, marker);
              });
            });
          });
        }



      function downloadUrl(url, callback) {
        var request = window.ActiveXObject ?
            new ActiveXObject('Microsoft.XMLHTTP') :
            new XMLHttpRequest;

        request.onreadystatechange = function() {
          if (request.readyState == 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
          }
        };

        request.open('GET', url, true);
        request.send(null);
      }

      function doNothing() {}
    </script>
		
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBQ3unX84NNlkJ2MeHPr-SBP6c7VbB8lnw&v=3.31"></script>
		