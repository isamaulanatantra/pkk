<!-- Content Header (Page header) -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1>Input_rekap_data_umum</h1>
      </div>
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?php echo base_url(); ?>input_rekap_data_umum">Home</a></li>
          <li class="breadcrumb-item active">Input_rekap_data_umum</li>
        </ol>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">


  <div class="row" id="awal">
    <div class="col-12">
      <!-- Custom Tabs -->
      <div class="card">
        <div class="card-header p-2">
          <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link active" href="#tab_form_input_rekap_data_umum" data-toggle="tab" id="klik_tab_input">Form</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_data_input_rekap_data_umum" data-toggle="tab" id="klik_tab_data_input_rekap_data_umum">Data</a></li>
            <li class="nav-item"><a class="nav-link" href="<?php echo base_url(); ?>input_rekap_data_umum"><i class="fa fa-refresh"></i></a></li>
          </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="tab-content">
            <div class="tab-pane active" id="tab_form_input_rekap_data_umum">
              <input name="tabel" id="tabel" value="input_rekap_data_umum" type="hidden" value="">
              <form role="form" id="form_input" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=input_rekap_data_umum" enctype="multipart/form-data">
                <div class="row">
                  <div class="col-sm-6">
                    <input name="page" id="page" value="1" type="hidden" value="">
                    <div class="form-group" style="display:none;">
                      <label for="temp">temp</label>
                      <input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="mode">mode</label>
                      <input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_input_rekap_data_umum">id_input_rekap_data_umum</label>
                      <input class="form-control" id="id_input_rekap_data_umum" name="id" value="" placeholder="id_input_rekap_data_umum" type="text">
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_propinsi">Provinsi</label>
                      <select class="form-control" id="id_propinsi" name="id_propinsi">
                        <option value="1">Jawa Tengah</option>
                      </select>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_kabupaten">Kabupaten</label>
                      <select class="form-control" id="id_kabupaten" name="id_kabupaten">
                        <option value="1">Wonosobo</option>
                      </select>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_kecamatan">Kecamatan</label>
                      <div class="overlay" id="overlay_id_kecamatan" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                      <select class="form-control" id="id_kecamatan" name="id_kecamatan">
                      </select>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-5">
                          <label for="id_desa">Desa</label>
                        </div>
                        <div class="col-md-7">
                          <div class="overlay" id="overlay_id_desa" style="display:none;">
                            <i class="fa fa-refresh fa-spin"></i>
                          </div>
                          <select class="form-control" id="id_desa" name="id_desa">
                            <option value="0">Pilih Desa</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group" style="display:none;">
                      <label for="id_dusun">Dusun</label>
                      <div class="overlay" id="overlay_id_dusun" style="display:none;">
                        <i class="fa fa-refresh fa-spin"></i>
                      </div>
                      <select class="form-control" id="id_dusun" name="id_dusun">
                        <option value="0">Pilih Dusun</option>
                      </select>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="tahun">tahun</label>
                        </div>
                        <div class="col-md-5">
                          <select class="form-control" id="tahun" name="tahun">
                            <option value="0000">Pilih tahun</option>
                            <option value="2016">2016</option>
                            <option value="2017">2017</option>
                            <option value="2018">2018</option>
                            <option value="2019">2019</option>
                            <option value="2020">2020</option>
                            <option value="2021">2021</option>
                            <option value="2022">2022</option>
                            <option value="2023">2023</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kelompok_pkk_dusun">jumlah_kelompok_pkk_dusun</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kelompok_pkk_dusun" name="jumlah_kelompok_pkk_dusun" value="" placeholder="jumlah_kelompok_pkk_dusun" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kelompok_pkk_rw">jumlah_kelompok_pkk_rw</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kelompok_pkk_rw" name="jumlah_kelompok_pkk_rw" value="" placeholder="jumlah_kelompok_pkk_rw" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kelompok_pkk_rt">jumlah_kelompok_pkk_rt</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kelompok_pkk_rt" name="jumlah_kelompok_pkk_rt" value="" placeholder="jumlah_kelompok_pkk_rt" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kelompok_pkk_dawis">jumlah_kelompok_pkk_dawis</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kelompok_pkk_dawis" name="jumlah_kelompok_pkk_dawis" value="" placeholder="jumlah_kelompok_pkk_dawis" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_krt">jumlah_krt</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_krt" name="jumlah_krt" value="" placeholder="jumlah_krt" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kk">jumlah_kk</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kk" name="jumlah_kk" value="" placeholder="jumlah_kk" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_jiwa_l">jumlah_jiwa_l</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_jiwa_l" name="jumlah_jiwa_l" value="" placeholder="jumlah_jiwa_l" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_jiwa_p">jumlah_jiwa_p</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_jiwa_p" name="jumlah_jiwa_p" value="" placeholder="jumlah_jiwa_p" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_pkk_anggota_pkk_l">jumlah_kader_pkk_anggota_pkk_l</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_pkk_anggota_pkk_l" name="jumlah_kader_pkk_anggota_pkk_l" value="" placeholder="jumlah_kader_pkk_anggota_pkk_l" type="text">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_pkk_anggota_pkk_p">jumlah_kader_pkk_anggota_pkk_p</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_pkk_anggota_pkk_p" name="jumlah_kader_pkk_anggota_pkk_p" value="" placeholder="jumlah_kader_pkk_anggota_pkk_p" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_pkk_umum_l">jumlah_kader_pkk_umum_l</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_pkk_umum_l" name="jumlah_kader_pkk_umum_l" value="" placeholder="jumlah_kader_pkk_umum_l" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_kader_pkk_umum_p">jumlah_kader_pkk_umum_p</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_kader_pkk_umum_p" name="jumlah_kader_pkk_umum_p" value="" placeholder="jumlah_kader_pkk_umum_p" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_khusus_l">jumlah_khusus_l</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_khusus_l" name="jumlah_khusus_l" value="" placeholder="jumlah_khusus_l" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_khusus_p">jumlah_khusus_p</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_khusus_p" name="jumlah_khusus_p" value="" placeholder="jumlah_khusus_p" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_tenaga_skr_honorer_l">jumlah_tenaga_skr_honorer_l</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_tenaga_skr_honorer_l" name="jumlah_tenaga_skr_honorer_l" value="" placeholder="jumlah_tenaga_skr_honorer_l" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_tenaga_skr_honorer_p">jumlah_tenaga_skr_honorer_p</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_tenaga_skr_honorer_p" name="jumlah_tenaga_skr_honorer_p" value="" placeholder="jumlah_tenaga_skr_honorer_p" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_tenaga_skr_bantuan_relawan_l">jumlah_tenaga_skr_bantuan_relawan_l</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_tenaga_skr_bantuan_relawan_l" name="jumlah_tenaga_skr_bantuan_relawan_l" value="" placeholder="jumlah_tenaga_skr_bantuan_relawan_l" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="jumlah_tenaga_skr_bantuan_relawan_p">jumlah_tenaga_skr_bantuan_relawan_p</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="jumlah_tenaga_skr_bantuan_relawan_p" name="jumlah_tenaga_skr_bantuan_relawan_p" value="" placeholder="jumlah_tenaga_skr_bantuan_relawan_p" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-7">
                          <label for="keterangan">keterangan</label>
                        </div>
                        <div class="col-md-5">
                          <input class="form-control" id="keterangan" name="keterangan" value="" placeholder="keterangan" type="text">
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-primary" id="simpan_input_rekap_data_umum">SIMPAN DATA KELUARGA</button>
                      <button type="submit" class="btn btn-primary" id="update_input_rekap_data_umum" style="display:none;">UPDATE DATA KELUARGA</button>
                      <a class="btn text-primary" href="<?php echo base_url(); ?>input_rekap_data_umum"><i class="fa fa-refresh"></i></a>
                    </div>
                  </div>
                </div>
              </form>

              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
            <div class="tab-pane table-responsive" id="tab_data_input_rekap_data_umum">
              <div class="card">
                <div class="card-header">
                  <h3 class="card-title">&nbsp;</h3>
                  <div class="card-tools">
                    <div class="input-group input-group-sm">
                      <select class="form-control input-sm pull-right" id="keyword" name="keyword" style="width: 150px;">
                        <option value="2022">2022</option>
                        <option value="2021">2021</option>
                        <option value="2020">2020</option>
                        <option value="2019">2019</option>
                        <option value="2018">2018</option>
                        <option value="2017">2017</option>
                        <option value="2016">2016</option>
                      </select>
                      <select name="limit" class="form-control input-sm pull-right" style="display:none; width: 150px;" id="limit">
                        <option value="999999999">Semua</option>
                        <option value="1">1 Per-Halaman</option>
                        <option value="10">10 Per-Halaman</option>
                        <option value="50">50 Per-Halaman</option>
                        <option value="100">100 Per-Halaman</option>
                      </select>
                      <select name="orderby" class="form-control input-sm pull-right" style="display:none; width: 150px;" id="orderby">
                        <option value="input_rekap_data_umum.tahun">Tahun</option>
                      </select>
                      <div class="input-group-btn">
                        <button class="btn btn-sm btn-default" id="tampilkan_data_input_rekap_data_umum"><i class="fa fa-search"></i> Tampil</button>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="card-body table-responsive p-0">
                  <table class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th rowspan="3">NO.</th>
                        <th rowspan="3">NAMA DESA</th>
                        <th colspan="4">JUMLAH KELOMPOK</th>
                        <th colspan="4">JUMLAH</th>
                        <th colspan="6">JUMLAH KADER PKK</th>
                        <th colspan="4">JUMLAH TENAGA SKR</th>
                        <th rowspan="3">KETERANGAN</th>
                        <th rowspan="3"></th>
                      </tr>
                      <tr>
                        <th rowspan="2">PKK DUSUN</th>
                        <th rowspan="2">PKK RW</th>
                        <th rowspan="2">PKK RT</th>
                        <th rowspan="2">DAWIS</th>
                        <th rowspan="2">KRT</th>
                        <th rowspan="2">KK</th>
                        <th colspan="2">JIWA</th>
                        <th colspan="2">ANGGOTA PKK</th>
                        <th colspan="2">UMUM</th>
                        <th colspan="2">KHUSUS</th>
                        <th colspan="2">HONORER</th>
                        <th colspan="2">BANTUAN/ RELAWAN</th>
                      </tr>
                      <tr>
                        <th>L</th>
                        <th>P</th>
                        <th>L</th>
                        <th>P</th>
                        <th>L</th>
                        <th>P</th>
                        <th>L</th>
                        <th>P</th>
                        <th>L</th>
                        <th>P</th>
                        <th>L</th>
                        <th>P</th>
                      </tr>
                    </thead>
                    <tbody id="tbl_data_input_rekap_data_umum">
                    </tbody>
                  </table>
                </div>
                <div class="card-footer">
                  <ul class="pagination pagination-sm m-0 float-right" id="pagination" style="display:none;">
                  </ul>
                  <div class="overlay" id="spinners_tbl_data_input_rekap_data_umum" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                  </div>
                </div>
              </div>
            </div>
            <!-- /.tab-content -->
          </div><!-- /.card-body -->
        </div>
        <!-- ./card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->


</section>


<script type="text/javascript">
  $(document).ready(function() {
    load_option_desa();
  });
</script>
<script>
  function load_option_desa() {
    $('#id_desa').html('');
    $('#overlay_id_desa').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>input_rekap_data_umum/option_desa_by_id_kecamatan/',
      success: function(html) {
        $('#id_desa').html('<option value="0">Pilih Desa</option><option value="9999">TP PKK Kecamatan</option>' + html + '');
        $('#overlay_id_desa').fadeOut('slow');
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $('#tabel').val();
  });
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit: limit,
        keyword: keyword,
        orderby: orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>input_rekap_data_umum/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li class="page-item" id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#" class="page-link">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_' + tabel + '').html('');
    $('#spinners_tbl_data_' + tabel + '').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page: page,
        limit: limit,
        keyword: keyword,
        orderby: orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>' + tabel + '/json_all_' + tabel + '/',
      success: function(html) {
        // $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
        $('#tbl_data_' + tabel + '').html(html);
        $('#spinners_tbl_data_' + tabel + '').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page = parseInt(id) + 1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_' + tabel + '').on('click', function(e) {
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_' + tabel + '').on('click', function(e) {
      load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedInput_rekap_data_umum() {
    $('#id_input_rekap_data_umum, #id_dusun, #rt, #tahun, #id_desa, #id_kecamatan, #id_kabupaten, #jumlah_kelompok_pkk_dusun, #jumlah_kelompok_pkk_rw, #jumlah_kelompok_pkk_rt, #jumlah_kelompok_pkk_dawis, #jumlah_krt, #jumlah_kk, #jumlah_jiwa_l, #jumlah_jiwa_p, #jumlah_kader_pkk_anggota_pkk_l, #jumlah_kader_pkk_anggota_pkk_p, #jumlah_kader_pkk_umum_l, #jumlah_kader_pkk_umum_p, #jumlah_khusus_l, #jumlah_khusus_p, #jumlah_tenaga_skr_honorer_l, #jumlah_tenaga_skr_honorer_p, #jumlah_tenaga_skr_bantuan_relawan_l, #jumlah_tenaga_skr_bantuan_relawan_p, #keterangan').val('');
    $('#tbl_attachment_input_rekap_data_umum').html('');
    $('#PesanProgresUpload').html('');
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#temp').val(Math.random());
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_input_rekap_data_umum').on('click', function(e) {
      e.preventDefault();
      var parameter = ['id_desa', 'temp', 'jumlah_kelompok_pkk_dusun'];
      InputValid(parameter);

      var parameter = {}
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["tahun"] = $("#tahun").val();
      parameter["jumlah_kelompok_pkk_dusun"] = $("#jumlah_kelompok_pkk_dusun").val();
      parameter["jumlah_kelompok_pkk_rw"] = $("#jumlah_kelompok_pkk_rw").val();
      parameter["jumlah_kelompok_pkk_rt"] = $("#jumlah_kelompok_pkk_rt").val();
      parameter["jumlah_kelompok_pkk_dawis"] = $("#jumlah_kelompok_pkk_dawis").val();
      parameter["jumlah_krt"] = $("#jumlah_krt").val();
      parameter["jumlah_kk"] = $("#jumlah_kk").val();
      parameter["jumlah_jiwa_l"] = $("#jumlah_jiwa_l").val();
      parameter["jumlah_jiwa_p"] = $("#jumlah_jiwa_p").val();
      parameter["jumlah_kader_pkk_anggota_pkk_l"] = $("#jumlah_kader_pkk_anggota_pkk_l").val();
      parameter["jumlah_kader_pkk_anggota_pkk_p"] = $("#jumlah_kader_pkk_anggota_pkk_p").val();
      parameter["jumlah_kader_pkk_umum_l"] = $("#jumlah_kader_pkk_umum_l").val();
      parameter["jumlah_kader_pkk_umum_p"] = $("#jumlah_kader_pkk_umum_p").val();
      parameter["jumlah_khusus_l"] = $("#jumlah_khusus_l").val();
      parameter["jumlah_khusus_p"] = $("#jumlah_khusus_p").val();
      parameter["jumlah_tenaga_skr_honorer_l"] = $("#jumlah_tenaga_skr_honorer_l").val();
      parameter["jumlah_tenaga_skr_honorer_p"] = $("#jumlah_tenaga_skr_honorer_p").val();
      parameter["jumlah_tenaga_skr_bantuan_relawan_l"] = $("#jumlah_tenaga_skr_bantuan_relawan_l").val();
      parameter["jumlah_tenaga_skr_bantuan_relawan_p"] = $("#jumlah_tenaga_skr_bantuan_relawan_p").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>input_rekap_data_umum/simpan_input_rekap_data_umum';

      var parameterRv = ['id_desa', 'temp', 'jumlah_kelompok_pkk_dusun'];
      var Rv = RequiredValid(parameterRv);
      if (Rv == 0) {
        alertify.error('Mohon data diisi secara lengkap');
      } else {
        SimpanData(parameter, url);
        AfterSavedInput_rekap_data_umum();
      }
      $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_data_input_rekap_data_umum').on('click', '.update_id_input_rekap_data_umum', function() {
      $('#mode').val('edit');
      $('#simpan_input_rekap_data_umum').hide();
      $('#update_input_rekap_data_umum').show();
      $('#overlay_data_anggota_keluarga').show();
      $('#tbl_data_anggota_keluarga').html('');
      var id_input_rekap_data_umum = $(this).closest('tr').attr('id_input_rekap_data_umum');
      var mode = $('#mode').val();
      var value = $(this).closest('tr').attr('id_input_rekap_data_umum');
      $('#form_baru').show();
      $('#judul_formulir').html('FORMULIR EDIT');
      $('#id_input_rekap_data_umum').val(id_input_rekap_data_umum);
      $.ajax({
        type: 'POST',
        async: true,
        data: {
          id_input_rekap_data_umum: id_input_rekap_data_umum
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>input_rekap_data_umum/get_by_id/',
        success: function(json) {
          for (var i = 0; i < json.length; i++) {
            $('#temp').val(json[i].temp);
            $('#id_propinsi').val(json[i].id_propinsi);
            $('#id_kabupaten').val(json[i].id_kabupaten);
            $('#id_kecamatan').val(json[i].id_kecamatan);
            $('#id_desa').val(json[i].id_desa);
            $('#id_dusun').val(json[i].id_dusun);
            $('#rt').val(json[i].rt);
            $('#tahun').val(json[i].tahun);
            $('#jumlah_kelompok_pkk_dusun').val(json[i].jumlah_kelompok_pkk_dusun);
            $('#jumlah_kelompok_pkk_rw').val(json[i].jumlah_kelompok_pkk_rw);
            $('#jumlah_kelompok_pkk_rt').val(json[i].jumlah_kelompok_pkk_rt);
            $('#jumlah_kelompok_pkk_dawis').val(json[i].jumlah_kelompok_pkk_dawis);
            $('#jumlah_krt').val(json[i].jumlah_krt);
            $('#jumlah_kk').val(json[i].jumlah_kk);
            $('#jumlah_jiwa_l').val(json[i].jumlah_jiwa_l);
            $('#jumlah_jiwa_p').val(json[i].jumlah_jiwa_p);
            $('#jumlah_kader_pkk_anggota_pkk_l').val(json[i].jumlah_kader_pkk_anggota_pkk_l);
            $('#jumlah_kader_pkk_anggota_pkk_p').val(json[i].jumlah_kader_pkk_anggota_pkk_p);
            $('#jumlah_kader_pkk_umum_l').val(json[i].jumlah_kader_pkk_umum_l);
            $('#jumlah_kader_pkk_umum_p').val(json[i].jumlah_kader_pkk_umum_p);
            $('#jumlah_khusus_l').val(json[i].jumlah_khusus_l);
            $('#jumlah_khusus_p').val(json[i].jumlah_khusus_p);
            $('#jumlah_tenaga_skr_honorer_l').val(json[i].jumlah_tenaga_skr_honorer_l);
            $('#jumlah_tenaga_skr_honorer_p').val(json[i].jumlah_tenaga_skr_honorer_p);
            $('#jumlah_tenaga_skr_bantuan_relawan_l').val(json[i].jumlah_tenaga_skr_bantuan_relawan_l);
            $('#jumlah_tenaga_skr_bantuan_relawan_p').val(json[i].jumlah_tenaga_skr_bantuan_relawan_p);
            $('#keterangan').val(json[i].keterangan);
            load_option_desa();
          }
        }
      });
      //AttachmentByMode(mode, value);
      //AfterSavedAnggota_keluarga(mode, value);
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#update_input_rekap_data_umum').on('click', function(e) {
      e.preventDefault();
      var parameter = ['id_input_rekap_data_umum', 'id_desa', 'temp', 'jumlah_kelompok_pkk_dusun'];
      InputValid(parameter);

      var parameter = {}
      parameter["id_input_rekap_data_umum"] = $("#id_input_rekap_data_umum").val();
      parameter["temp"] = $("#temp").val();
      parameter["id_propinsi"] = $("#id_propinsi").val();
      parameter["id_kabupaten"] = $("#id_kabupaten").val();
      parameter["id_kecamatan"] = $("#id_kecamatan").val();
      parameter["id_desa"] = $("#id_desa").val();
      parameter["id_dusun"] = $("#id_dusun").val();
      parameter["rt"] = $("#rt").val();
      parameter["tahun"] = $("#tahun").val();
      parameter["jumlah_kelompok_pkk_dusun"] = $("#jumlah_kelompok_pkk_dusun").val();
      parameter["jumlah_kelompok_pkk_rw"] = $("#jumlah_kelompok_pkk_rw").val();
      parameter["jumlah_kelompok_pkk_rt"] = $("#jumlah_kelompok_pkk_rt").val();
      parameter["jumlah_kelompok_pkk_dawis"] = $("#jumlah_kelompok_pkk_dawis").val();
      parameter["jumlah_krt"] = $("#jumlah_krt").val();
      parameter["jumlah_kk"] = $("#jumlah_kk").val();
      parameter["jumlah_jiwa_l"] = $("#jumlah_jiwa_l").val();
      parameter["jumlah_jiwa_p"] = $("#jumlah_jiwa_p").val();
      parameter["jumlah_kader_pkk_anggota_pkk_l"] = $("#jumlah_kader_pkk_anggota_pkk_l").val();
      parameter["jumlah_kader_pkk_anggota_pkk_p"] = $("#jumlah_kader_pkk_anggota_pkk_p").val();
      parameter["jumlah_kader_pkk_umum_l"] = $("#jumlah_kader_pkk_umum_l").val();
      parameter["jumlah_kader_pkk_umum_p"] = $("#jumlah_kader_pkk_umum_p").val();
      parameter["jumlah_khusus_l"] = $("#jumlah_khusus_l").val();
      parameter["jumlah_khusus_p"] = $("#jumlah_khusus_p").val();
      parameter["jumlah_tenaga_skr_honorer_l"] = $("#jumlah_tenaga_skr_honorer_l").val();
      parameter["jumlah_tenaga_skr_honorer_p"] = $("#jumlah_tenaga_skr_honorer_p").val();
      parameter["jumlah_tenaga_skr_bantuan_relawan_l"] = $("#jumlah_tenaga_skr_bantuan_relawan_l").val();
      parameter["jumlah_tenaga_skr_bantuan_relawan_p"] = $("#jumlah_tenaga_skr_bantuan_relawan_p").val();
      parameter["keterangan"] = $("#keterangan").val();
      var url = '<?php echo base_url(); ?>input_rekap_data_umum/update_input_rekap_data_umum';

      var parameterRv = ['id_input_rekap_data_umum', 'id_desa', 'temp', 'jumlah_kelompok_pkk_dusun'];
      var Rv = RequiredValid(parameterRv);
      if (Rv == 0) {
        alertify.error('Mohon data diisi secara lengkap');
      } else {
        SimpanData(parameter, url);
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#tbl_data_input_rekap_data_umum').on('click', '#del_ajax_input_rekap_data_umum', function() {
      var id_input_rekap_data_umum = $(this).closest('tr').attr('id_input_rekap_data_umum');
      alertify.confirm('Anda yakin data akan dihapus?', function(e) {
        if (e) {
          var parameter = {}
          parameter["id_input_rekap_data_umum"] = id_input_rekap_data_umum;
          var url = '<?php echo base_url(); ?>input_rekap_data_umum/hapus/';
          HapusData(parameter, url);
          $('[id_input_rekap_data_umum=' + id_input_rekap_data_umum + ']').remove();
        } else {
          alertify.error('Hapus data dibatalkan');
        }
        $('.nav-tabs a[href="#tab_data_' + tabel + '"]').tab('show');
        load_data(tabel);
      });
    });
  });
</script>