
                  <div class="col-md-12">
                    <center>
                      <h1>Data Keluarga TP-PKK Wonosobo
                      </h1>
                    </center>
                    <div class="box">
											<div class="box-body">
												<table class="table table-bordered table-hover">
                          <tr>
                            <th>No</th>
                            <th>Nama Kepala Rumah Tangga</th>
                            <th>Jumlah KK</th>
                            <th>Jumlah Anggota Keluarga</th>
                            <th>Kriteria Rumah</th>
                            <th>Sumber Air</th> 
                            <th>Makanan Pokok</th> 
                            <th>Warga Mengikuti Kegiatan</th> 
                          </tr>
													<tbody>
                          <?php
                          $table = 'data_keluarga';
                          $page    = $this->input->get('page');
                          $limit    = $this->input->get('limit');
                          $keyword    = $this->input->get('keyword');
                          $order_by    = $this->input->get('orderby');
                          $start      = ($page - 1) * $limit;
                          $fields     = "
                          *,
                          ( select (dusun.nama_dusun) from dusun where dusun.id_dusun=data_keluarga.id_dusun limit 1) as nama_dusun,
                          ( select (desa.nama_desa) from desa where desa.id_desa=data_keluarga.id_desa limit 1) as nama_desa,
                          ( select (kecamatan.nama_kecamatan) from kecamatan where kecamatan.id_kecamatan=data_keluarga.id_kecamatan limit 1) as nama_kecamatan
                          ";
                          $where = array(
                            'data_keluarga.status !=' => 99
                            );
                          $orderby   = ''.$order_by.'';
                          $data_data_keluarga = $this->Crud_model->html_all($table, $where, $limit, $start, $fields, $orderby, $keyword);
                          $urut=$start;
                          foreach ($data_data_keluarga->result() as $row){
                            $urut=$urut+1;
                            echo'
                            <tr>
                              <td>'.($urut).'</td>
                              <td>'.$row->nama_kepala_rumah_tangga.'</td>
                              <td>'.$row->jumlah_kartu_keluarga.'</td>
                              <td>'.$row->jumlah_anggota_keluarga.'</td>
                              <td>'.$row->kriteria_rumah.'</td>
                              <td>'.$row->sumber_air_keluarga.'</td>
                              <td>'.$row->makanan_pokok.'</td>
                              <td>'.$row->kegiatan_up2k.'</td>
                            </tr>
                            ';
                          }
                          ?>
													</tbody>
												</table>
											</div>
                    </div>
                  </div>