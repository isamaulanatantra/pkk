<section class="content" id="awal">
	<div class="col-md-12">
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#tab_data_isi_data" data-toggle="tab" id="klik_tab_data_isi_data">Data <?php echo $nama_kategori_data; ?></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_data_isi_data">
						<input name="page" id="page" value="1" type="hidden" value="">
						<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="hidden">
						<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="hidden">
						<input name="tabel" id="tabel" value="isi_data" type="hidden" value="">
						<div class="row">
						<div class="col-md-12">
							<div class="box-body">
								<div class="box-tools">
									<div class="input-group">
										<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
										<input name="id_kategori_data" class="form-control input-sm pull-right" style="width: 150px;" value="<?php echo $this->uri->segment(3); ?>" type="hidden" id="id_kategori_data">
										<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
											<option value="999999999">Semua</option>
											<option value="1">1 Per-Halaman</option>
											<option value="10">10 Per-Halaman</option>
											<option value="50">50 Per-Halaman</option>
											<option value="100">100 </option>
										</select>
										<select name="tahun" class="form-control input-sm pull-right" style="width: 150px;" id="tahun">
											<option value="semua">Semua Tahun</option>
											<option value="2018">2018</option>
											<option value="2017">2017</option>
											<option value="2016">2016</option>
											<option value="2015">2015</option>
											<option value="2014">2014</option>
											<option value="2013">2013</option>
										</select>
										<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
											<option value="isi_data.id_isi_data">Urut Input</option>
											<option value="isi_data.nama_isi_data">Nama Isi Data</option>
										</select>
										<div class="input-group-btn">
											<button class="btn btn-sm btn-default" id="tampilkan_data_isi_data"><i class="fa fa-search"></i> Tampil</button>
										</div>
									</div>
								</div>
							</div>
							<div class="box-footer table-responsive no-padding">
								<table class="table table-bordered table-hover">
									<thead>
										<tr>
											<th>NO</th>
											<th>Jenis Data</th>
											<th>Tahun</th>
											<th>Jumlah</th>
										</tr>
									</thead>
									<tbody id="tbl_data_isi_data">
									</tbody>
								</table>
								<ul class="pagination pagination-sm no-margin pull-right" id="pagination">
								</ul>
								<div class="overlay" id="spinners_data" style="display:none;">
									<i class="fa fa-refresh fa-spin"></i>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</section>

<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
	load_data(tabel);
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tahun = $('#tahun').val();
    var id_kategori_data = $('#id_kategori_data').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        tahun:tahun,
        id_kategori_data:id_kategori_data
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>isi_data/total_data_tahun',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tahun = $('#tahun').val();
    var id_kategori_data = $('#id_kategori_data').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby,
        tahun:tahun,
        id_kategori_data:id_kategori_data
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_views_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
