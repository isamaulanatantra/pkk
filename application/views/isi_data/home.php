<section class="content" id="awal">
	<div class="col-md-12">
		<!-- Custom Tabs -->
		<div class="nav-tabs-custom">
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#tab_form_isi_data" id="klik_tab_input">Form</a> <span id="demo"></span></li>
				<li><a href="#tab_data_isi_data" data-toggle="tab" id="klik_tab_data_isi_data">Data</a></li>
				<li class="dropdown" style="display:none;">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						Dropdown <span class="caret"></span>
					</a>
					<ul class="dropdown-menu">
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
						<li role="presentation" class="divider"></li>
						<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
					</ul>
				</li>
				<li class="pull-right" style="display:none;"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active" id="tab_form_isi_data">
					<input name="tabel" id="tabel" value="isi_data" type="hidden" value="">
					<div class="row">
						<div class="col-md-6">
							<form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=isi_data" enctype="multipart/form-data">
                  <input name="page" id="page" value="1" type="hidden" value="">
									<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="hidden">
									<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="hidden">
								<div class="form-group" style="display:none;">
									<label for="id_isi_data">id_isi_data</label>
									<input class="form-control" id="id_isi_data" name="id" value="" placeholder="id_isi_data" type="text">
								</div>
								<div class="form-group">
									<label for="id_skpd">id_skpd</label>
									<select class="form-control" id="id_skpd" name="id_skpd">
									</select>
								</div>
								<div class="form-group">
									<label for="id_kategori_data">id_kategori_data</label>
									<select class="form-control" id="id_kategori_data" name="id_kategori_data">
									</select>
								</div>
								<div class="form-group">
									<label for="nama_isi_data">Nama Isi Data</label>
									<input class="form-control" id="nama_isi_data" name="nama_isi_data" value="" placeholder="nama_isi_data" type="text">
								</div>
								<div class="form-group">
									<label for="tahun_isi_data">id_skpd</label>
									<select class="form-control" id="tahun_isi_data" name="tahun_isi_data">
										<option value="2018">2018</option>
										<option value="2017">2017</option>
										<option value="2016">2016</option>
										<option value="2015">2015</option>
										<option value="2014">2014</option>
										<option value="2013">2013</option>
									</select>
								</div>
								<div class="form-group">
									<label for="jumlah_isi_data">Jumlah Isi Data</label>
									<input class="form-control" id="jumlah_isi_data" name="jumlah_isi_data" value="" placeholder="jumlah_isi_data" type="text">
								</div>
								<button type="submit" class="btn btn-primary" id="simpan_isi_data">SIMPAN</button>
								<button type="submit" class="btn btn-primary" id="update_isi_data" style="display:none;">UPDATE</button>
							</form>
							<div class="overlay" id="overlay_form_input" style="display:none;">
								<i class="fa fa-refresh fa-spin"></i>
							</div>
						</div>
					</div>
				</div>
				<!-- /.tab-pane -->
				<div class="tab-pane" id="tab_data_isi_data">
							<div class="row">
								<div class="col-md-12">
									<div class="box-body">
										<div class="box-tools">
											<div class="input-group">
												<input name="keyword" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword">
												<select name="limit" class="form-control input-sm pull-right" style="width: 150px;" id="limit">
													<option value="1">1 Per-Halaman</option>
													<option value="10">10 Per-Halaman</option>
													<option value="50">50 Per-Halaman</option>
													<option value="100">100 </option>
													<option value="999999999">Semua</option>
												</select>
												<select name="orderby" class="form-control input-sm pull-right" style="width: 150px;" id="orderby">
													<option value="isi_data.id_isi_data">ID Isi Data</option>
													<option value="isi_data.nama_isi_data">Nama Isi Data</option>
												</select>
												<div class="input-group-btn">
													<button class="btn btn-sm btn-default" id="tampilkan_data_isi_data"><i class="fa fa-search"></i> Tampil</button>
												</div>
											</div>
										</div>
									</div>
									<div class="box-footer table-responsive no-padding">
										<table class="table table-bordered table-hover">
											<thead>
												<tr>
													<th>NO</th>
													<th>Kategori Data</th>
													<th>Nama Isi Data</th>
													<th>Tahun</th>
													<th>Jumlah</th>
													<th colspan="4">Proses</th>
												</tr>
											</thead>
											<tbody id="tbl_data_isi_data">
											</tbody>
										</table>
                    <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                    </ul>
										<div class="overlay" id="spinners_data" style="display:none;">
											<i class="fa fa-refresh fa-spin"></i>
										</div>
									</div>
								</div>
							</div>
				</div>
				<!-- /.tab-pane -->
			</div>
			<!-- /.tab-content -->
		</div>
		<!-- nav-tabs-custom -->
	</div>
	
</section>

<script type="text/javascript">
$(document).ready(function() {
	load_skpd();
});
</script>
<script type="text/javascript">
  function load_skpd() {
    $('#id_skpd').html('');
    $.ajax({
      type: 'POST',
      async: true,
      dataType: 'json',
      url: '<?php echo base_url(); ?>skpd/json_option_skpd/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih SKPD</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_skpd + '" >' + json[i].nama_skpd + '</option>';
        }
        $('#id_skpd').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#id_skpd').on('change', function(e) {
      e.preventDefault();
      var id_skpd = $('#id_skpd').val();
      load_pc_by_id_skpd(id_skpd);
    });
});
</script>
<script type="text/javascript">
  function load_pc_by_id_skpd(id_skpd) {
    $('#id_kategori_data').html('');
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_skpd:id_skpd
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>kategori_data/json_option_kategori_data/',
      success: function(json) {
        var option = '';
        option += '<option value="0">Pilih Kategori Data</option>';
        for (var i = 0; i < json.length; i++) {
          option += '<option value="' + json[i].id_kategori_data + '" >' + json[i].nama_kategori_data + '</option>';
        }
        $('#id_kategori_data').append(option);
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
});
</script>
<script type="text/javascript">
  function paginations(tabel) {
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'text',
      url: '<?php echo base_url(); ?>isi_data/total_data',
      success: function(text) {
        var jumlah = text;
        $('#pagination').html('');
        var pagination = '';
        for (var a = 0; a < jumlah; a++) {
          pagination += '<li id="' + a + '" page="' + a + '" keyword="' + keyword + '"><a id="next" href="#">' + (a + 1) + '</a></li>';
        }
        $('#pagination').append(pagination);
      }
    });
  }
</script>
<script type="text/javascript">
  function load_data(tabel) {
    var page = $('#page').val();
    var limit = $('#limit').val();
    var keyword = $('#keyword').val();
    var orderby = $('#orderby').val();
    var tabel = $('#tabel').val();
    $('#page').val(page);
    $('#tbl_data_'+tabel+'').html('');
    $('#spinners_tbl_data_'+tabel+'').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        page:page,
        limit:limit,
        keyword:keyword,
        orderby:orderby
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>'+tabel+'/json_all_'+tabel+'/',
      success: function(html) {
        $('.nav-tabs a[href="#tab_data_'+tabel+'"]').tab('show');
        $('#tbl_data_'+tabel+'').html(html);
        $('#spinners_tbl_data_'+tabel+'').fadeOut('slow');
        paginations(tabel);
      }
    });
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#pagination').on('click', '#next', function(e) {
      e.preventDefault();
      var id = $(this).closest('li').attr('page');
      var page= parseInt(id)+1;
      $('#page').val(page);
      var tabel = $("#tabel").val();
      load_data(tabel);
    });
  });
</script>
<script type="text/javascript" >
  $(document).ready(function () {
    var tabel = $("#tabel").val();
    $("#update_"+tabel+"").on("click", function (e) {
      e.preventDefault();
      update(tabel);
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#klik_tab_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    var tabel = $("#tabel").val();
    $('#tampilkan_data_'+tabel+'').on('click', function(e) {
    load_data(tabel);
    });
  });
</script>
<script>
  function AfterSavedUsers() {
    $('#id_isi_data, #nama_isi_data, #id_kategori_data').val('');
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#temp').val(Math.random());
});
</script>
<script type="text/javascript">
$(document).ready(function() {
	$('#form_baru').on('click', function(e) {
    $('#simpan_isi_data').show();
    $('#update_isi_data').hide();
    $('#id_isi_data, #nama_isi_data, #id_kategori_data, #tahun_isi_data, #jumlah_isi_data').val('');
    $('#form_baru').hide();
    $('#mode').val('input');
    $('#judul_formulir').html('FORMULIR INPUT');
  });
});
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_isi_data').on('click', function(e) {
      e.preventDefault();
      var parameter = [ 'nama_isi_data', 'id_kategori_data', 'tahun_isi_data', 'jumlah_isi_data' ];
			InputValid(parameter);
      
      var parameter = {}
      parameter["nama_isi_data"] = $("#nama_isi_data").val();
      parameter["id_kategori_data"] = $("#id_kategori_data").val();
      parameter["tahun_isi_data"] = $("#tahun_isi_data").val();
      parameter["jumlah_isi_data"] = $("#jumlah_isi_data").val();
      var url = '<?php echo base_url(); ?>isi_data/simpan_isi_data';
      var parameterRv = [ 'nama_isi_data', 'id_kategori_data' ];
			var Rv = RequiredValid(parameterRv);
      if(Rv == 0){
        alertify.error('Mohon data diisi secara lengkap');
      }
      else{
        SimpanData(parameter, url);
        AfterSavedUsers();
      }
    });
  });
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $('#tabel').val();
  $('#klik_tab_input').on('click', function(e) {
    e.preventDefault();
    $('#tab_form_'+tabel+' form').trigger('reset');
    $('#judul_form').html('Form Input');
    $('#update_'+tabel+'').hide();
    $('#simpan_'+tabel+'').show();
    $('#temp').val(Math.random());
    $('#mode').val('input');
    $('#PesanProgresUpload').html('');
  });
});
</script>
<script type="text/javascript" >
  $(document).ready(function () {
    var tabel = $("#tabel").val();
    $("#update_"+tabel+"").on("click", function (e) {
      e.preventDefault();
      update(tabel);
    });
  });
</script>
<script type="text/javascript">
  function load_edit_by_id(id, tabel) {
    $('#judul_form').html('Form Edit');
		$.ajax({
      type: 'POST',
      async: true,
      data: {
        id_isi_data:id
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>'+tabel+'/get_by_id/',
      success: function(json) {
        for (var i = 0; i < json.length; i++) {
          $("#id_isi_data").val(json[i].id_isi_data);
          $("#nama_isi_data").val(rupiah());
          $("#mode").val('edit');
        }
        $('.nav-tabs a[href="#tab_form_isi_data"]').tab('show');        
        $('#simpan_'+tabel+'').hide();
        $('#update_'+tabel+'').show();
        AttachmentByMode('edit', id, tabel);
        $('#PesanProgresUpload').html('');
      }
    });
  }
</script>
<script type="text/javascript">
$(document).ready(function() {
  var tabel = $("#tabel").val();
  $('#tbl_data_'+tabel+'').on('click', '#update_id', function() {
    var id = $(this).closest('tr').attr('id_'+tabel+'');
    load_edit_by_id(id, tabel);
  });
});
</script>
<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_utama_posting').on('click', '#del_ajax', function() {
    var id_posting = $(this).closest('tr').attr('id_posting');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_posting"] = id_posting;
        var url = '<?php echo base_url(); ?>posting/hapus/';
        HapusData(parameter, url);
        $('[id_posting='+id_posting+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>
