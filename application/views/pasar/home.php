
<div class="container" id="awal">
  <div class="tabs-wrapper">
    <ul class="nav nav-tabs nav-justified" role="tablist">
      <li class="nav-item"><a class="nav-link waves-light active" href="#tab_1" data-toggle="tab" id="klik_tab_input">GENERAL</a></li>
      <li class="nav-item"><a class="nav-link waves-light" href="#tab_2" data-toggle="tab">PENCARIAN</a></li>
			<li class="nav-item"><a class="nav-link waves-light" href="#tab_3" data-toggle="tab">AUTO SUGGEST</a></li>
			<li class="nav-item"><a class="nav-link waves-light" href="#tab_4" data-toggle="tab">DOWNLOAD</a></li>
    </ul>
  </div>
    <div class="tab-content">
      <div class="tab-pane fade in show active" id="tab_1" role="tabpanel">
        <div class="row">
          <div class="col-lg-6 col-xs-12" id="div_form_input">
              <div class="card">
                <div class="card-header success-color lighten-1 white-text">
                  <i class="fa fa-file-text-o" aria-hidden="true"></i> FORMULIR INPUT
                </div>
                  <div class="card-body">
                  <!--formulir input-->
                    <form role="form" id="form_pasar" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=pasar" enctype="multipart/form-data">
                        <input id="id_pasar" name="id_pasar" type="hidden">
                        <div class="md-form form-sm">
                          <label for="kode_pasar">Kode Pasar</label>
                          <input class="form-control" id="kode_pasar" name="kode_pasar" value="" type="text">
                        </div>
                        <div class="md-form form-sm">
                          <label for="nama_pasar">Nama Pasar</label>
                          <input class="form-control" id="nama_pasar" name="nama_pasar" value="" type="text">
                        </div>
                        <input id="inserted_by" name="inserted_by" type="hidden">
                        <input id="inserted_time" name="inserted_time" type="hidden">
                        <input id="updated_by" name="updated_by" type="hidden">
                        <input id="updated_time" name="updated_time" type="hidden">
                        <input id="deleted_by" name="deleted_by" type="hidden">
                        <input id="deleted_time" name="deleted_time" type="hidden">
                        <input id="temp" name="temp" type="hidden">
                        <div class="md-form form-sm">
                          <label for="keterangan">Keterangan</label>
                          <input class="form-control" id="keterangan" name="keterangan" type="text">
                        </div>
                        <input id="status" name="status" type="hidden">
                                            
                        <div class="alert alert-info alert-dismissable" style="display:none;">
                          <div class="md-form form-sm">
                            <label for="remake">Keterangan Lampiran Pasar</label>
                            <input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran Pasar" type="text">
                          </div>
                          <div class="md-form form-sm">
                            <label for="myfile">File Lampiran Pasar</label>
                            <input type="file" size="60" name="myfile" id="pasar_baru" >
                          </div>
                          <div id="progress_upload_lampiran_pasar">
                            <div id="bar_progress_upload_lampiran_pasar"></div>
                            <div id="percent_progress_upload_lampiran_pasar">0%</div >
                          </div>
                          <div id="message_progress_upload_lampiran_pasar"></div>
                        </div>
                        <div class="alert alert-info alert-dismissable" style="display:none;">
                          <h3 class="card-title">Data Lampiran </h3>
                          <table class="table table-bordered">
                            <tr>
                              <th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
                            </tr>
                            <tbody id="tbl_lampiran_pasar">
                            </tbody>
                          </table>
                        </div>
                        <button type="submit" class="btn btn-success" id="simpan_pasar">SIMPAN</button>
                    </form>
                    <div class="overlay" id="overlay_form_input" style="display:none;">
                      <i class="fa fa-refresh fa-spin"></i>
                    </div>
                  </div>
              </div>
          </div>
					<div class="col-lg-6 col-xs-12" id="e_div_form_input" style="display:none;">
            <div class="card">
              <div class="card-header deep-orange lighten-1 white-text">
              <i class="fa fa-file-text-o" aria-hidden="true"></i> FORMULIR EDIT
              </div>
              <div class="card-body">
                <form role="form" id="e_form_pasar" method="post" action="<?php echo base_url(); ?>attachment/e_upload/?table_name=pasar" enctype="multipart/form-data">
                  <div class="card-body">
                    
										<input class="form-control" id="e_id_pasar" name="id" value="" type="hidden">
										<div class="md-form form-sm">
											<label for="e_kode_pasar">Kode Pasar</label>
											<input class="form-control" id="e_kode_pasar" name="e_kode_pasar" value="" placeholder="Kode" type="text">
										</div>
										<div class="md-form form-sm">
											<label for="e_nama_pasar">Nama Pasar</label>
											<input class="form-control" id="e_nama_pasar" name="e_nama_pasar" value="" placeholder="Nama" type="text">
										</div>
										<input class="form-control" id="e_inserted_by" name="e_inserted_by" value="" type="hidden">
										<input class="form-control" id="e_inserted_time" name="e_inserted_time" value="" type="hidden">
										<input class="form-control" id="e_updated_by" name="e_updated_by" value="" type="hidden">
										<input class="form-control" id="e_updated_time" name="e_updated_time" value="" type="hidden">
										<input class="form-control" id="e_deleted_by" name="e_deleted_by" value="" type="hidden">
										<input class="form-control" id="e_deleted_time" name="e_deleted_time" value="" type="hidden">
										<input class="form-control" id="e_temp" name="e_temp" value="" type="hidden">
										<div class="md-form form-sm">
											<label for="e_keterangan">Keterangan</label>
                      <input class="form-control" id="e_keterangan" name="e_keterangan" placeholder="Keterangan" type="text">
										</div>
										<input class="form-control" id="e_status" name="e_status" value="" type="hidden">
																				
										<div class="alert alert-info alert-dismissable" style="display:none;">
											<div class="md-form form-sm">
												<label for="remake">Keterangan Lampiran Pasar</label>
												<input class="form-control" id="e_remake" name="remake" placeholder="Keterangan Lampiran Pasar" type="text">
											</div>
											<div class="md-form form-sm">
												<label for="myfile">File Lampiran Pasar</label>
												<input type="file" size="60" name="myfile" id="e_pasar_baru" >
											</div>
											<div id="e_progress_upload_lampiran_pasar">
												<div id="e_bar_progress_upload_lampiran_pasar"></div>
												<div id="e_percent_progress_upload_lampiran_pasar">0%</div >
											</div>
											<div id="e_message_progress_upload_lampiran_pasar"></div>
										</div>
										<div class="alert alert-info alert-dismissable" style="display:none;">
											<h3 class="card-title">Data Lampiran </h3>
											<table class="table table-bordered">
												<tr>
													<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
												</tr>
												<tbody id="e_tbl_lampiran_pasar">
												</tbody>
											</table>
										</div>
                  </div>
                    <button type="submit" class="btn btn-deep-orange" id="update_data_pasar">SIMPAN</button>
                </form>
              </div>
              <div class="overlay" id="e_overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-xs-12" id="div_data_default">
            <div class="card">
                <div class="table-wrapper-2">
                  <table class="table table-bordered table-hover table-responsive w-auto">
                    <thead class="mdb-color lighten-4">
                    <tr class="table-info">
                      <th>NO</th>
                      <th>Kode Pasar</th>
                      <th>Nama Pasar</th>
                      <th>Keterangan</th>
                      <th>PROSES</th> 
                    </tr>
                    </thead>
                    <tbody id="tbl_utama_pasar">
                    </tbody>
                  </table>
                  <nav>
                    <ul class="pagination" id="pagination">
                    <?php
                    for ($x = 1; $x <= $total; $x++) {
                      echo '<li class="page-item" page="'.$x.'" id="'.$x.'"><a class="update_id page-link" href="#">'.$x.'</a></li>';
                    }
                    ?>
                    </ul>
                  </nav>
                  <div class="preloader-wrapper big active" id="overlay_data_default" style="display:none;">
                    <div class="preloader-wrapper big active">
                      <div class="preloader-wrapper big active">
                        <div class="preloader-wrapper big active">
                          <div class="spinner-layer spinner-blue-only">
                            <div class="circle-clipper left">
                              <div class="circle"></div>
                            </div><div class="gap-patch">
                              <div class="circle"></div>
                            </div><div class="circle-clipper right">
                              <div class="circle"></div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div> 
                </div> 
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane fade in show" role="tabpanel" id="tab_2">
        <div class="row" id="cari_div_form_input">
          <div class="col-lg-6 col-xs-12">
            <div class="card">
              <div class="card-header success-color lighten-1 white-text">
                <i class="fa fa-search"></i> FORMULIR CARI
              </div>
              <div class="card-body">
                <form role="form" id="cari_form_pasar" method="post" action="<?php echo base_url(); ?>attachment/upload/?table_name=pasar" enctype="multipart/form-data">
                  <div class="card-body">
                    <input class="form-control" id="cari_temp" name="temp" value="" type="hidden">
                    <div class="md-form form-sm">
                      <label for="key_word">Kata Kunci</label>
                      <input class="form-control" id="key_word" type="text">
                    </div>
                    <button type="submit" class="btn btn-success" id="cari_pasar">Tampil</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="cari_overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
						<div class="col-lg-6 col-xs-12" id="div_data_cari">
							<div class="card">
								<div class="card-header">
                <i class="fa fa-th"></i> DATA PENCARIAN
								</div>
								<div class="card-body">
									<table class="table table-bordered">
										<tr>
											<th>NO</th>
											<th>Kode Pasar</th>
											<th>Nama Pasar</th>
											<th>Keterangan</th>
											<th>PROSES</th> 
										</tr>
										<tbody id="tbl_search_pasar">
										</tbody>
									</table>
									<div class="card-footer clearfix">
										<div id="total_data_search"></div>
										<ul class="pagination pagination-sm no-margin pull-right" id="next_page_search">
										</ul>
									</div>
								</div>
								<div class="overlay" id="overlay_data_cari" style="display:none;">
									<i class="fa fa-refresh fa-spin"></i>
								</div>
							</div>
						</div>
        </div>
      </div>
			<div class="tab-pane fade in show" role="tabpanel" id="tab_3">
				<div class="row" id="a_div_form_input">
					<div class="col-lg-6 col-xs-12">
            <div class="card">
              <div class="card-header success-color lighten-1 white-text">
              <i class="ion ion-clipboard"></i> FORMULIR AUTO SUGGEST
              </div>
              <div class="card-body">
                <form role="form" id="a_form_pasar" method="post" action="<?php echo base_url(); ?>attachment/e_upload/?table_name=pasar" enctype="multipart/form-data">
										<input class="form-control" id="a_id_pasar" name="id" value="" type="hidden">
										<div class="md-form form-sm">
											<label for="a_kode_pasar">Kode Pasar</label>
											<input class="form-control" id="a_kode_pasar" name="a_kode_pasar" value="" type="text">
										</div>
										<div class="md-form form-sm">
											<label for="a_nama_pasar">Nama Pasar</label>
											<input class="form-control" id="a_nama_pasar" name="a_nama_pasar" value="" type="text">
										</div>
										<input class="form-control" id="a_inserted_by" name="a_inserted_by" value="" type="hidden">
										<input class="form-control" id="a_inserted_time" name="a_inserted_time" value="" type="hidden">
										<input class="form-control" id="a_updated_by" name="a_updated_by" value="" type="hidden">
										<input class="form-control" id="a_updated_time" name="a_updated_time" value="" type="hidden">
										<input class="form-control" id="a_deleted_by" name="a_deleted_by" value="" type="hidden">
										<input class="form-control" id="a_deleted_time" name="a_deleted_time" value="" type="hidden">
										<input class="form-control" id="a_temp" name="a_temp" value="" type="hidden">
										<div class="md-form form-sm">
											<label for="a_keterangan">Keterangan</label>
                      <input class="form-control" id="a_keterangan" name="a_keterangan" type="text">
										</div>
										<input class="form-control" id="a_status" name="a_status" value="" type="hidden">
																				
										<div class="alert alert-info alert-dismissable">
											<div class="md-form form-sm">
												<label for="a_remake">Keterangan Lampiran Pasar</label>
												<input class="form-control" id="a_remake" name="remake" placeholder="Keterangan Lampiran Pasar" type="text">
											</div>
											<div class="md-form form-sm">
												<label for="a_myfile"></label>
												<input type="file" size="60" name="myfile" id="a_pasar_baru" >
											</div>
											<div id="a_progress_upload_lampiran_pasar">
												<div id="a_bar_progress_upload_lampiran_pasar"></div>
												<div id="a_percent_progress_upload_lampiran_pasar">0%</div >
											</div>
											<div id="a_message_progress_upload_lampiran_pasar"></div>
										</div>
										<div class="alert alert-info alert-dismissable">
											<h3 class="card-title">Data Lampiran </h3>
											<table class="table table-bordered">
												<tr>
													<th>No</th><th>Keterangan</th><th>Download</th><th>Hapus</th> 
												</tr>
												<tbody id="a_tbl_lampiran_pasar">
												</tbody>
											</table>
										</div>
                    <button type="submit" class="btn btn-success" id="a_update_data_pasar">SIMPAN</button>
                </form>
              </div>
              <div class="overlay" id="a_overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
			</div>
			<div class="tab-pane fade in show" role="tabpanel" id="tab_4">
				<div class="row" id="download_div_form_input">
					<div class="col-lg-6 col-xs-12">
            <div class="card">
              <div class="card-header primary-color lighten-1 white-text">
              Download
              </div>
              <div class="card-body">
                <a target="_blank" href="<?php echo base_url(); ?>pasar/download_xls">Download Xlsx</a><br />
                <a target="_blank" href="<?php echo base_url(); ?>pasar/cetak_pdf">Download Pdf</a>
              </div>
            </div>
          </div>
        </div>
			</div>
    </div>
  </div>

<script type="text/javascript">
$(document).ready(function() {
	$('#klik_tab_input').on('click', function(e) {
	$('#e_div_form_input').fadeOut('slow');
	$('#div_form_input').fadeIn('slow');
	// $('#div_form_input').show();
	});
	var aaa = Math.random();
  $('#temp').val(aaa);
});
</script>

<script>
  function load_default(halaman) {
    $('#overlay_data_default').show();
    $('#tbl_utama_pasar').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>pasar/json_all_pasar/?halaman='+halaman+'/',
      success: function(json) {
        var tr = '';
        var start = ((halaman - 1) * <?php echo $per_page; ?>);
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
					tr += '<tr id_pasar="' + json[i].id_pasar + '" id="' + json[i].id_pasar + '" >';
					tr += '<td valign="top">' + (start) + '</td>';
					tr += '<td valign="top">' + json[i].kode_pasar + '</td>';
					tr += '<td valign="top">' + json[i].nama_pasar + '</td>';
					tr += '<td valign="top">' + json[i].keterangan + '</td>';
					tr += '<td valign="top">';
					tr += '<a href="#tab_1" data-toggle="tab" class="update_id badge amber darken-2" ><i class="fa fa-pencil-square-o"></i></a> ';
					tr += '<a class="badge red" href="#tab_1" data-toggle="tab" id="del_ajax" ><i class="fa fa-cut"></i></a>';
					tr += '</td>';
          tr += '</tr>';
        }
        $('#tbl_utama_pasar').append(tr);
				$('#overlay_data_default').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#pagination').on('click', '.update_id', function(e) {
		e.preventDefault();
		var id = $(this).closest('li').attr('page');
		var halaman = id;
		load_default(halaman);
	});
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	var halaman = 1;
	load_default(halaman);
});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#tbl_utama_pasar, #tbl_search_pasar').on('click', '.update_id', function(e) {
		e.preventDefault();
		$('#e_overlay_form_input').show();
		var id = $(this).closest('tr').attr('id_pasar');
		$.ajax({
			dataType: 'json',
			url: '<?php echo base_url(); ?>pasar/pasar_get_by_id/?id='+id+'',
			success: function(json) {
				if (json.length == 0) {
					alert('Tidak ada data');
				} else {
					for (var i = 0; i < json.length; i++) {
					$('#e_id_pasar').val(json[i].id_pasar);
					$('#e_kode_pasar').val(json[i].kode_pasar);
					$('#e_nama_pasar').val(json[i].nama_pasar);
					$('#e_inserted_by').val(json[i].inserted_by);
					$('#e_inserted_time').val(json[i].inserted_time);
					$('#e_updated_by').val(json[i].updated_by);
					$('#e_updated_time').val(json[i].updated_time);
					$('#e_deleted_by').val(json[i].deleted_by);
					$('#e_deleted_time').val(json[i].deleted_time);
					$('#e_temp').val(json[i].temp);
					$('#e_keterangan').val(json[i].keterangan);
					$('#e_status').val(json[i].status);
          }
            var halaman = 1;
            load_default(halaman);
					e_load_lampiran_pasar(id);
					load_e_id_parent();
					$('#div_form_input').fadeOut('slow');
					$('#e_div_form_input').show();
					$('#e_overlay_form_input').fadeOut(2000);
					$('html, body').animate({
						scrollTop: $('#awal').offset().top
					}, 1000);
				}
			}
		});
	});
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#simpan_pasar').on('click', function(e) {
      e.preventDefault();
      $('#simpan_pasar').attr('disabled', 'disabled');
      $('#overlay_form_input').show();
			
			var id_pasar = $("#id_pasar").val();
			var kode_pasar = $("#kode_pasar").val();
			var nama_pasar = $("#nama_pasar").val();
			var inserted_by = $("#inserted_by").val();
			var inserted_time = $("#inserted_time").val();
			var updated_by = $("#updated_by").val();
			var updated_time = $("#updated_time").val();
			var deleted_by = $("#deleted_by").val();
			var deleted_time = $("#deleted_time").val();
			var temp = $("#temp").val();
			var keterangan = $("#keterangan").val();
			var status = $("#status").val();
						
			if (kode_pasar == '') {
					$('#kode_pasar').css('background-color', '#DFB5B4');
				} else {
					$('#kode_pasar').removeAttr('style');
				}
			if (nama_pasar == '') {
					$('#nama_pasar').css('background-color', '#DFB5B4');
				} else {
					$('#nama_pasar').removeAttr('style');
				}
			if (inserted_by == '') {
					$('#inserted_by').css('background-color', '#DFB5B4');
				} else {
					$('#inserted_by').removeAttr('style');
				}
			if (temp == '') {
					$('#temp').css('background-color', '#DFB5B4');
				} else {
					$('#temp').removeAttr('style');
				}
						
			$.ajax({
        type: 'POST',
        async: true,
        data: {
					kode_pasar:kode_pasar,
					nama_pasar:nama_pasar,
					temp:temp,
					keterangan:keterangan,
					status:status,
										},
        dataType: 'text',
        url: '<?php echo base_url(); ?>pasar/simpan_pasar/',
        success: function(json) {
          if (json == '0') {
            $('#simpan_pasar').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut();
            alertify.set({ delay: 3000 });
            alertify.error("Gagal simpan");
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
          } else {
            alertify.set({ delay: 3000 });
            alertify.success("Berhasil simpan");
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
						$('#tbl_lampiran_pasar').html('');
						$('#message_progress_upload_lampiran_pasar').html('');
            var halaman = 1;
            load_default(halaman);
            load_id_parent()
            var aaa = Math.random();
            $('#temp').val(aaa);
            $('#div_form_input form').trigger('reset');
            $('#simpan_pasar').removeAttr('disabled', 'disabled');
            $('#overlay_form_input').fadeOut('slow');
          }
        }
      });
    });
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#update_data_pasar').on('click', function(e) {
		e.preventDefault();
		$('#update_data_pasar').attr('disabled', 'disabled');
		$('#e_overlay_form_input').show();
		var id_pasar = $('#e_id_pasar').val();
		var kode_pasar = $('#e_kode_pasar').val();
		var nama_pasar = $('#e_nama_pasar').val();
		var temp = $('#e_temp').val();
		var keterangan = $('#e_keterangan').val();
						
		if (id_pasar == '') {
				$('#e_id_pasar').css('background-color', '#DFB5B4');
			} else {
				$('#e_id_pasar').removeAttr('style');
			}
		if (kode_pasar == '') {
				$('#e_kode_pasar').css('background-color', '#DFB5B4');
			} else {
				$('#e_kode_pasar').removeAttr('style');
			}
		if (nama_pasar == '') {
				$('#e_nama_pasar').css('background-color', '#DFB5B4');
			} else {
				$('#e_nama_pasar').removeAttr('style');
			}
		if (temp == '') {
				$('#e_temp').css('background-color', '#DFB5B4');
			} else {
				$('#e_temp').removeAttr('style');
			}
				
			$.ajax({
			type: 'POST',
			async: true,
			data: {
					
					id_pasar:id_pasar,
					kode_pasar:kode_pasar,
					nama_pasar:nama_pasar,
					temp:temp,
					keterangan:keterangan,
										
				},
			dataType: 'json',
			url: '<?php echo base_url(); ?>pasar/update_data_pasar/',
			success: function(json) {
				if (json.length == 0) {
          alertify.set({ delay: 3000 });
          alertify.error("Gagal simpan");
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
					$('#update_data_pasar').removeAttr('disabled', 'disabled');
					$('#e_overlay_form_input').fadeOut('slow');
					} 
				else {
          alertify.set({ delay: 3000 });
          alertify.success("Berhasil simpan");
					var halaman = 1;
					load_default(halaman);
					$('#e_div_form_input form').trigger('reset');
					$('#update_data_pasar').removeAttr('disabled', 'disabled');
					$('#e_overlay_form_input').fadeOut('slow');
					$('#e_div_form_input').fadeOut('slow');
					$('#tbl_lampiran_pasar').html('');
					$('#e_tbl_lampiran_pasar').html('');
					$('#a_tbl_lampiran_pasar').html('');
					}
			}
		});
	});
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('#cari_pasar').on('click', function(e) {
      e.preventDefault();
      $('#overlay_data_cari').show();
      var key_word = $('#key_word').val();
      var start = 0;
      $('#tbl_search_pasar').html('');
      $('#total_data_search').html('');
      $.ajax({
        type: 'POST',
        async: true,
        data: {
          key_word: key_word,
          halaman: 1
        },
        dataType: 'json',
        url: '<?php echo base_url(); ?>pasar/search_pasar/',
        success: function(json) {
          if (json.length == 0) {
            if (key_word == '') {
              $('#key_word').css('background-color', '#DFB5B4');
            } else {
              $('#key_word').removeAttr('style');
            }
						$('#overlay_data_cari').fadeOut('slow');
          } else {
            $('#tbl_search_pasar').html('');
            var tr = '';
            for (var i = 0; i < json.length; i++) {
              start = start + 1;
              tr += '<tr id_pasar="' + json[i].id_pasar + '" id="id_pasar' + json[i].id_pasar + '" >';
							tr += '<td valign="top">' + (start) + '</td>';
							tr += '<td valign="top">' + json[i].kode_pasar + '</td>';
              tr += '<td valign="top"><a class="" href="#">' + json[i].nama_pasar + '  <small class="badge bg-blue">0</small></a></td>';
              tr += '<td valign="top">' + json[i].keterangan + '</td>';
              tr += '<td valign="top">';
							tr += '<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> ';
							tr += '<a href="#tab_1" data-toggle="tab" id="del_ajax" ><i class="fa fa-cut"></i></a>';
							tr += '</td>';
							tr += '</tr>';
            }
						$('#key_word').removeAttr('style');
            $('#tbl_search_pasar').append(tr);
						$('#overlay_data_cari').fadeOut('slow');
          }
        }
      });
      $('#total_data_search').html('');
      $.ajax({
        dataType: 'text',
        url: '<?php echo base_url(); ?>pasar/count_all_search_pasar/?key_word='+key_word+'',
        success: function(json) {
          var jumlah = json;
          $('#next_page_search').html('');
          var ajax_pagination = '';
          for (var a = 0; a < jumlah; a++) {
            ajax_pagination += '<li id="'+a+'" page="'+a+'" key_word="'+key_word+'" ><a id="next" href="#">'+(a + 1)+'</a></li>';
          }
          $('#next_page_search').append(ajax_pagination);
        }
      });
    });
  });
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#next_page_search').on('click', '#next', function(e) {
      e.preventDefault();
			$('#overlay_data_cari').show();
      var id = $(this).closest('li').attr('page');
      var key_word = $(this).closest('li').attr('key_word');
      var keyword_f = $(this).closest('li').attr('keyword_f');
			var halaman = parseInt(id) + 1;
      $('#tbl_search_pasar').html('');
      $.ajax({
				type: 'POST',
        async: true,
        data: {
          key_word: key_word,
					halaman:halaman
					},
        dataType: 'json',
        url: '<?php echo base_url(); ?>pasar/search_pasar/',
        success: function(json) {
          var tr = '';
					var start = (((parseInt(id) + 1) - 1) * <?php echo $per_page; ?>);
					for (var i = 0; i < json.length; i++) {
						var start = parseInt(start) + 1;
            tr += '<tr id_pasar="' + json[i].id_pasar + '" id="id_pasar' + json[i].id_pasar + '" >';
						tr += '<td valign="top">' + (start) + '</td>';
						tr += '<td valign="top">' + json[i].kode_pasar + '</td>';
            tr += '<td valign="top"><a class="" href="<?php echo base_url(); ?>sub_pasar/?id_pasar=' + json[i].id_pasar + '">' + json[i].nama_pasar + '  <small class="badge bg-blue">0</small></a></td>';
            tr += '<td valign="top">' + json[i].keterangan + '</td>';
            tr += '<a href="#tab_1" data-toggle="tab" class="update_id" ><i class="fa fa-pencil-square-o"></i></a> ';
						tr += '<a href="#tab_1" data-toggle="tab" id="del_ajax" ><i class="fa fa-cut"></i></a>';
						tr += '</td>';
						tr += '</tr>';
          }
          $('#tbl_search_pasar').append(tr);
          $('#overlay_data_cari').fadeOut('slow');
        }
      });
    });
  });
</script>

<script>
function reset() {
	$('#toggleCSS').attr('href', '<?php echo base_url(); ?>boots/alertify/alertify.default.css');
	alertify.set({
		labels: {
			ok: 'OK',
			cancel: 'Cancel'
		},
		delay: 5000,
		buttonReverse: false,
		buttonFocus: 'ok'
	});
}
//===============HAPUS OBAT
$('#tbl_search_pasar, #tbl_utama_pasar').on('click', '#del_ajax', function() {
	reset();
	var id = $(this).closest('tr').attr('id_pasar');
	alertify.confirm('Anda yakin data akan dihapus?', function(e) {
		if (e) {
			$.ajax({
				dataType: 'json',
				url: '<?php echo base_url(); ?>pasar/hapus_pasar/?id='+id+'',
				success: function(response) {
					if (response.errors == 'Yes') {
						alertify.alert('Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya');
					} else {
						$('[id_pasar='+id+']').remove();
						alertify.alert('Data berhasil dihapus');
					}
				}
			});
		} else {
			alertify.alert('Hapus data dibatalkan');
		}
	});
});
//===============HAPUS Attachment
$('#tbl_lampiran_pasar, #e_tbl_lampiran_pasar, #a_tbl_lampiran_pasar').on('click', '#del_ajax', function() {
	reset();
	var id = $(this).closest('tr').attr('id_attachment');
	alertify.confirm('Anda yakin data akan dihapus?', function(e) {
		if (e) {
			$.ajax({
				dataType: 'json',
				url: '<?php echo base_url(); ?>attachment/hapus/?id='+id+'',
				success: function(response) {
					if (response.errors == 'Yes') {
						alertify.alert('Maaf data tidak bisa dihapus, Anda tidak berhak melakukannya');
					} else {
						$('[id_attachment='+id+']').remove();
						$('#message_progress_upload_lampiran_pasar').remove();
						$('#e_message_progress_upload_lampiran_pasar').remove();
						$('#a_message_progress_upload_lampiran_pasar').remove();
						$('#percent_progress_upload_lampiran_pasar').html('');
						$('#e_percent_progress_upload_lampiran_pasar').html('');
						$('#a_percent_progress_upload_lampiran_pasar').html('');
						alertify.alert('Data berhasil dihapus');
					}
				}
			});
		} else {
			alertify.alert('Hapus data dibatalkan');
		}
	});
});

</script>

<script>
	function load_lampiran_pasar() {
		$('#tbl_lampiran_pasar').html('');
		var temp = $('#temp').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				temp:temp
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/load_lampiran/?table=pasar',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_lampiran_pasar').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
			var options = { 
				beforeSend: function() {
					$('#progress_upload_lampiran_pasar').show();
					$('#bar_progress_upload_lampiran_pasar').width('0%');
					$('#message_progress_upload_lampiran_pasar').html('');
					$('#percent_progress_upload_lampiran_pasar').html('0%');
					},
				uploadProgress: function(event, position, total, percentComplete){
					$('#bar_progress_upload_lampiran_pasar').width(percentComplete+'%');
					$('#percent_progress_upload_lampiran_pasar').html(percentComplete+'%');
					},
				success: function(){
					$('#bar_progress_upload_lampiran_pasar').width('100%');
					$('#percent_progress_upload_lampiran_pasar').html('100%');
					},
				complete: function(response){
					$('#message_progress_upload_lampiran_pasar').html('<font color="green">'+response.responseText+'</font>');
					var temp = $('#temp').val();
					load_lampiran_pasar();
					},
				error: function(){
					$('#message_progress_upload_lampiran_pasar').html('<font color="red"> ERROR: unable to upload files</font>');
					}     
			};
			document.getElementById('pasar_baru').onchange = function() {
					$('#form_pasar').submit();
				};
			$('#form_pasar').ajaxForm(options);
		});
</script>

<script>
	function e_load_lampiran_pasar() {
		$('#e_tbl_lampiran_pasar').html('');
		var id = $('#e_id_pasar').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				id:id
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/e_load_lampiran/?table=pasar',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#e_tbl_lampiran_pasar').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
			var options = { 
				beforeSend: function() {
					$('#e_progress_upload_lampiran_pasar').show();
					$('#e_bar_progress_upload_lampiran_pasar').width('0%');
					$('#e_message_progress_upload_lampiran_pasar').html('');
					$('#e_percent_progress_upload_lampiran_pasar').html('0%');
					},
				uploadProgress: function(event, position, total, percentComplete){
					$('#e_bar_progress_upload_lampiran_pasar').width(percentComplete+'%');
					$('#e_percent_progress_upload_lampiran_pasar').html(percentComplete+'%');
					},
				success: function(){
					$('#e_bar_progress_upload_lampiran_pasar').width('100%');
					$('#e_percent_progress_upload_lampiran_pasar').html('100%');
					},
				complete: function(response){
					$('#e_message_progress_upload_lampiran_pasar').html('<font color="green">'+response.responseText+'</font>');
					e_load_lampiran_pasar();
					},
				error: function(){
					$('#e_message_progress_upload_lampiran_pasar').html('<font color="red"> ERROR: unable to upload files</font>');
					}     
			};
			document.getElementById('e_pasar_baru').onchange = function() {
					$('#e_form_pasar').submit();
				};
			$('#e_form_pasar').ajaxForm(options);
		});
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('#a_update_data_pasar').on('click', function(e) {
		e.preventDefault();
		$('#a_update_data_pasar').attr('disabled', 'disabled');
		$('#a_overlay_form_input').show();
		var id_pasar = $('#a_id_pasar').val();
		var kode_pasar = $('#a_kode_pasar').val();
		var nama_pasar = $('#a_nama_pasar').val();
		var temp = $('#a_temp').val();
		var keterangan = $('#a_keterangan').val();
						
		if (id_pasar == '') {
				$('#a_id_pasar').css('background-color', '#DFB5B4');
			} else {
				$('#a_id_pasar').removeAttr('style');
			}
		if (kode_pasar == '') {
				$('#a_kode_pasar').css('background-color', '#DFB5B4');
			} else {
				$('#a_kode_pasar').removeAttr('style');
			}
		if (nama_pasar == '') {
				$('#a_nama_pasar').css('background-color', '#DFB5B4');
			} else {
				$('#a_nama_pasar').removeAttr('style');
			}
		if (temp == '') {
				$('#a_temp').css('background-color', '#DFB5B4');
			} else {
				$('#a_temp').removeAttr('style');
			}
		if (keterangan == '') {
				$('#a_keterangan').css('background-color', '#DFB5B4');
			} else {
				$('#a_keterangan').removeAttr('style');
			}
				$.ajax({
			type: 'POST',
			async: true,
			data: {
					
					id_pasar:id_pasar,
					kode_pasar:kode_pasar,
					nama_pasar:nama_pasar,
					temp:temp,
					keterangan:keterangan,
																				
				},
			dataType: 'json',
			url: '<?php echo base_url(); ?>pasar/update_data_pasar/',
			success: function(json) {
				if (json.length == 0) {
          alertify.set({ delay: 3000 });
          alertify.error("Gagal simpan");
						$('html, body').animate({
							scrollTop: $('#awal').offset().top
						}, 1000);
					$('#a_update_data_pasar').removeAttr('disabled', 'disabled');
					$('#a_overlay_form_input').fadeOut('slow');
					} 
				else {
          alertify.success("Berhasil simpan");
					var halaman = 1;
					load_default(halaman);
					$('#a_div_form_input form').trigger('reset');
					$('#a_update_data_pasar').removeAttr('disabled', 'disabled');
					$('#a_overlay_form_input').fadeOut('slow');
					$('#a_tbl_lampiran_pasar').html('');
					}
			}
		});
	});
});
</script>

<script>
	function a_load_lampiran_pasar() {
		$('#a_tbl_lampiran_pasar').html('');
		var id = $('#a_id_pasar').val();
		$.ajax({
			type: 'POST',
			async: true,
			data: {
				id:id
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>attachment/e_load_lampiran/?table=pasar',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_attachment="'+json[i].id_attachment+'" id="'+json[i].id_attachment+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#a_tbl_lampiran_pasar').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
			var options = { 
				beforeSend: function() {
					$('#a_progress_upload_lampiran_pasar').show();
					$('#a_bar_progress_upload_lampiran_pasar').width('0%');
					$('#a_message_progress_upload_lampiran_pasar').html('');
					$('#a_percent_progress_upload_lampiran_pasar').html('0%');
					},
				uploadProgress: function(event, position, total, percentComplete){
					$('#a_bar_progress_upload_lampiran_pasar').width(percentComplete+'%');
					$('#a_percent_progress_upload_lampiran_pasar').html(percentComplete+'%');
					},
				success: function(){
					$('#a_bar_progress_upload_lampiran_pasar').width('100%');
					$('#a_percent_progress_upload_lampiran_pasar').html('100%');
					},
				complete: function(response){
					$('#a_message_progress_upload_lampiran_pasar').html('<font color="green">'+response.responseText+'</font>');
					a_load_lampiran_pasar();
					},
				error: function(){
					$('#a_message_progress_upload_lampiran_pasar').html('<font color="red"> ERROR: unable to upload files</font>');
					}     
			};
			document.getElementById('a_pasar_baru').onchange = function() {
					$('#a_form_pasar').submit();
				};
			$('#a_form_pasar').ajaxForm(options);
		});
</script>

<script type="text/javascript">
	function reply_a_pasar(id_pasar, kode_pasar, nama_pasar )
	{
		$('#a_id_pasar').val( id_pasar );
		var id = id_pasar;
		$.ajax({
			dataType: 'json',
			url: '<?php echo base_url(); ?>pasar/pasar_get_by_id/?id='+id+'',
			success: function(json) {
				if (json.length == 0) {
					alert('Tidak ada data');
				} else {
					for (var i = 0; i < json.length; i++) {
					
					$('#a_id_pasar').val(json[i].id_pasar);
					$('#a_kode_pasar').val(json[i].kode_pasar);
					$('#a_nama_pasar').val(json[i].nama_pasar);
					$('#a_inserted_by').val(json[i].inserted_by);
					$('#a_inserted_time').val(json[i].inserted_time);
					$('#a_updated_by').val(json[i].updated_by);
					$('#a_updated_time').val(json[i].updated_time);
					$('#a_deleted_by').val(json[i].deleted_by);
					$('#a_deleted_time').val(json[i].deleted_time);
					$('#a_temp').val(json[i].temp);
					$('#a_keterangan').val(json[i].keterangan);
					$('#a_status').val(json[i].status);
										}
					a_load_lampiran_pasar(id);
				}
			}
		});
	}		
	$(document).ready(function(){
		$('#a_nama_pasar').typeahead({
			name: 'a_nama_pasar',
			template:['<div onclick="reply_a_pasar(this.getAttribute(\'id_pasar\'), this.getAttribute(\'kode_pasar\'), this.getAttribute(\'nama_pasar\') )" id_pasar="{{id_pasar}}" kode_pasar="{{kode_pasar}}"  nama_pasar="{{nama_pasar}}" ><div><p>{{nama_pasar}}<br><b>{{kode_pasar}}</b></div>',
								'</div>'
							 ].join(''),
			engine: Hogan,
			remote: '<?php echo base_url(); ?>pasar/auto_suggest/?q=%QUERY'
		});
		$('.form-control').siblings('input.tt-hint').remove();
	});
</script>
<script>
  function load_id_parent() {
    $('#overlay_data_default').show();
    $('#id_parent').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>pasar/json_all_pasar_id_parent/',
      success: function(json) {
        var tr = '';
        tr += '<option value="0"> Pilih Satu</option>';
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
          tr += '<option value="' + json[i].id_pasar + '"> ' + json[i].nama_pasar + ' </option>';
        }
        $('#id_parent').append(tr);
				$('#overlay_data_default').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	load_id_parent();
});
</script>
<script>
  function load_e_id_parent() {
    $('#overlay_data_default').show();
    $('#e_id_parent').html('');
    var temp = $('#temp').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        temp: temp
      },
      dataType: 'json',
      url: '<?php echo base_url(); ?>pasar/json_all_pasar_id_parent/',
      success: function(json) {
        var tr = '';
        tr += '<option value="0"> Pilih Satu</option>';
        for (var i = 0; i < json.length; i++) {
          var start = start + 1;
          tr += '<option value="' + json[i].id_pasar + '"> ' + json[i].nama_pasar + ' </option>';
        }
        $('#e_id_parent').append(tr);
				$('#overlay_data_default').fadeOut('slow');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
	load_e_id_parent();
});
</script>
