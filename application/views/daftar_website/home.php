<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      
    </ul>
    <div class="tab-content">
      <div class="tab-pane" id="tab_1">
        
      </div>
      <div class="tab-pane active" id="tab_2">
        <div class="row">
          <div class="col-md-12">
            <div class="box-header">
              <h3 class="box-title">
                DAFTAR INFORMASI PUBLIK
              </h3>
              <div class="box-tools">
                <div class="input-group">
                  <input name="table_search" class="form-control input-sm pull-right" style="display:none; width: 150px;" placeholder="Search" type="text" id="kata_kunci">
                  <select name="limit_data_daftar_informasi_publik_pembantu" class="form-control input-sm pull-right" style="width: 150px;" id="limit_data_daftar_informasi_publik_pembantu">
                    <option value="10">10 User</option>
                    <option value="15">15 User</option>
                    <option value="20">20 User</option>
                    <option value="25">25 User</option>
                    <option value="50">50 User</option>
                    <option value="55">55 User</option>
                    <option value="100">100 User</option>
                    <option value="999999999">Semua</option>
                  </select>
                  <select name="urut_data_daftar_informasi_publik_pembantu" class="form-control input-sm pull-right" id="urut_data_daftar_informasi_publik_pembantu" style="display:none;">
                    <option value="judul_posting">Berdasarkan Ringkasan Informasi</option>
                  </select>
                  <select name="klasifikasi_informasi_publik_pembantu" class="form-control input-sm pull-right" style="width: 180px;" id="klasifikasi_informasi_publik_pembantu">
                    <option value="">Pilih Hak Akses</option>
                    <option value="web_desa">web_desa</option>
                    <option value="web_skpd">web_skpd</option>
                    <option value="admin_web_kecamatan">admin_web_kecamatan</option>
                    <option value="admin_ppid">admin_ppid</option>
                    <option value="admin_web_dinkes">admin_web_dinkes</option>
                    <option value="admin_web_disdagkopukm">admin_web_disdagkopukm</option>
                    <option value="admin_web_diskominfo">admin_web_diskominfo</option>
                  </select>
                  <select name="id_kecamatan" class="form-control input-sm pull-right" style="width: 180px;" id="id_kecamatan">
                  </select>
                  <div class="input-group-btn">
                    <button class="btn btn-sm btn-default" id="tampilkan_daftar_informasi_publik"><i class="fa fa-search"></i> Tampil</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-12">
            <div class="box box-primary">
              <div class="box-body">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="box">
                      <div class="box-body table-responsive no-padding">
                        <div id="tblExport">
                          <table class="table table-bordered table-hover">
                            <thead>
                              <tr id="header_exel">
                                <th>NO</th>
                                <th>Domain</th>
                                <th>Keterangan</th>
                                <th>Alamat</th>
                              </tr>
                            </thead>
                            <tbody id="tbl_utama_daftar_informasi_publik_pembantu">
                            </tbody>
                          </table>
                        </div>
                      </div><!-- /.box-body -->
                      <div class="row">
                        
                        
                        
                      </div>
                    </div><!-- /.box -->
                  </div>
                </div>
              </div>
              <div class="overlay" id="spinners_data" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
              <div class="box-footer">
                
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<script type="text/javascript">
$(document).ready(function() {
    var halaman = 1;
		var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
		var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
		var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
		var id_kecamatan = $('#id_kecamatan').val();
    load_data_daftar_informasi_publik_pembantu(halaman, limit, klasifikasi_informasi_publik_pembantu, id_kecamatan);
});
</script>

<script>
  function load_data_daftar_informasi_publik_pembantu(halaman, limit, klasifikasi_informasi_publik_pembantu, id_kecamatan) {
    $('#tbl_utama_daftar_informasi_publik_pembantu').html('');
    $('#spinners_data').show();
		var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
		var id_kecamatan = $('#id_kecamatan').val();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        id_kecamatan: id_kecamatan,
        klasifikasi_informasi_publik_pembantu: klasifikasi_informasi_publik_pembantu,
        halaman: halaman,
        limit: limit
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>daftar_website/load_table_daftar_informasi_publik_pembantu/',
      success: function(html) {
        $('#tbl_utama_daftar_informasi_publik_pembantu').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
 
<script type="text/javascript">
  $(document).ready(function() {
    $('#tampilkan_daftar_informasi_publik').on('click', function(e) {
      e.preventDefault();
      var halaman = 1;
      var limit_data_daftar_informasi_publik_pembantu = $('#limit_data_daftar_informasi_publik_pembantu').val();
      var limit = limit_per_page_custome(limit_data_daftar_informasi_publik_pembantu);
      var klasifikasi_informasi_publik_pembantu = $('#klasifikasi_informasi_publik_pembantu').val();
      var id_kecamatan = $('#id_kecamatan').val();
      load_data_daftar_informasi_publik_pembantu(halaman, limit, klasifikasi_informasi_publik_pembantu, id_kecamatan);
    });
  });
</script>

<!----------------------->
<script>
  function load_opd_domain() {
    $('#id_kecamatan').html('');
    $('#spinners_data').show();
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: 1
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>user/option_kecamatan/',
      success: function(html) {
        $('#id_kecamatan').html('<option value="">Pilih Kecamatan</option>'+html+'');
      }
    });
  }
</script>

<script type="text/javascript">
$(document).ready(function() {
  load_opd_domain();
});
</script>
