<!-- === BEGIN HEADER === -->
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <!-- Title -->
        <title><?php if(!empty( $keterangan )){ echo $keterangan; } ?></title>
        <!-- Meta -->
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <meta name="author" content="<?php if(!empty( $keterangan )){ echo $keterangan; } ?>" />
        <meta name="keywords" content="<?php if(!empty( $judul_posting )){ echo $judul_posting; } ?><?php if(!empty( $keterangan )){ echo $keterangan; } ?> <?php echo base_url(); ?>" />
        <meta name="og:description" content="<?php if(!empty( $judul_posting )){ echo $judul_posting; } ?>" />
        <meta name="og:url" content="<?php echo base_url(); ?>" />
        <meta name="og:title" content="<?php if(!empty( $judul_posting )){ echo $judul_posting; } ?>" />
        <meta name="og:keywords" content="<?php if(!empty( $judul_posting )){ echo $judul_posting; } ?><?php if(!empty( $keterangan )){ echo $keterangan; } ?> <?php echo base_url(); ?>" />
        <meta name="og:image" content="<?php echo base_url(); ?>media/upload/<?php if(!empty( $gambar )){ echo $gambar; } ?>"/>
        <link id="favicon" rel="shortcut icon" href="<?php echo base_url(); ?>media/logo wonosobo.png" type="image/png" />
        <style rel="stylesheet">
          .front-carousel .carousel-caption {
              padding: 10px 15px 0; background: rgba(0,0,0,0.5); left: 0; right: 0; bottom: 0; text-align: left;
          }
        </style>
        <!-- Bootstrap Core CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>Template/HTML/assets/css/bootstrap1.css" rel="stylesheet" />
        <!-- Template CSS -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>Template/HTML/assets/css/animate.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>Template/theme/assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>Template/HTML/assets/css/responsive.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>Template/HTML/assets/css/custom.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>Template/HTML/assets/alertify/alertify.core.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>Template/HTML/assets/css/datepicker.css" rel="stylesheet" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>Template/HTML/assets/alertify/alertify.default.css" rel="stylesheet" />
        
        <script src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>Template/theme/assets/plugins/bootstrap/js/bootstrap.js"></script>
            
		<?php
        $web=$this->uut->namadomain(base_url());
        $where0 = array(
          'status' => 1,
          'domain' => $web
          );
        $this->db->where($where0);
        $this->db->limit(1);
        $query0 = $this->db->get('header_website');
        $header_website = 'logo_header.png';
        foreach ($query0->result() as $row0)
          {
            $header_website = $row0->file_name;
          }
        $where = array(
          'status' => 1,
          'domain' => $web
          );
        $this->db->where($where);
        $this->db->limit(1);
        $query = $this->db->get('tema_website');
        $tema = 'biru.css';
        foreach ($query->result() as $row)
          {
            $tema = $row->nama_tema_website;
          }
        echo '<link rel="stylesheet" href="'.base_url().'Template/HTML/assets/css/'.$tema.'" rel="stylesheet" />';
		?>
		<style>
          .well {
            min-height: 20px;
            padding: 2px;
            background-color: #9F9393;;
            color: #fff;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
          }
          .brandimg {
              float: left;
              padding-left: -100px;
          }
          .brandimga {
              float: right;
              padding-right: -100px;
              padding-top: 20px;
          }
          .brand {
              float: left;
              padding-top: 13px;
              padding-left: 3px;
          }
          .brand_name {
              font-size: 20px;
              color: #000;
              font-weight: bold;
              padding-left: 5px;
              text-transform: uppercase;
          }
          .brand_namea {
              font-size: 22px;
              color: #000;
              font-weight: bold;
              padding-left: 5px;
              text-transform: uppercase;
          }
          .subjudul {
              font-size: 18px;
              color: #000;
              font-weight: bold;
              padding-left: 5px;
              text-transform: uppercase;
          }
          .featured-big a.featured-href, .featured-slider .featured-small a.featured-href {
              display: block;
              text-decoration: none;
              color: #fff;
              outline: 0;
              border: none;
          }
          .omega {
              padding-right: 0px !important;
          }
          a.featured-big a.featured-href, .featured-slider .featured-small a.featured-href {
              display: block;
              text-decoration: none;
              color: #fff;
              outline: 0;
              border: none;
          }
          .featured-big .featured-header {
              padding: 50px 50px 30px 30px;
          }
          .featured-header {
              position: absolute;
              left: 0;
              -webkit-backface-visibility: hidden;
              -webkit-perspective: 1000;
              -webkit-transform: scale(1);
              transform-style: flat;
              right: 0;
              bottom: 0;
              padding: 45px 30px 30px;
              z-index: 9;
              background: -webkit-linear-gradient(bottom, rgba(0, 0, 0, .7) 0, rgba(0, 0, 0, 0) 100%);
              background: linear-gradient(0deg, rgba(0, 0, 0, .7) 0, rgba(0, 0, 0, 0) 100%);
          }
          a.gflag {
              vertical-align: middle;
              font-size: 15px;
              padding: 0px;
              background-repeat: no-repeat;
              background-image: url(//gtranslate.net/flags/16.png);
          }

          a.gflag img {
              border: 0;
          }

          a.gflag:hover {
              background-image: url(//gtranslate.net/flags/16a.png);
          }

          #goog-gt-tt {
              display: none !important;
          }

          .goog-te-banner-frame {
              display: none !important;
          }

          .goog-te-menu-value:hover {
              text-decoration: none !important;
          }

          body {
              top: 0 !important;
          }

          #google_translate_element2 {
              display: none !important;
          }
		</style>
	</head>
    <body>
        <div id="body-bg">
            <div id="pre-header" class="background-gray-lighter">
                <div class="container no-padding">
                    <div class="row hidden-xs">
                        <div class="col-sm-6 padding-vert-5">
                            <strong>Phone:</strong>&nbsp;<?php if(!empty( $telpon )){ echo $telpon; } ?>
                            <strong>Email:</strong>&nbsp;<?php if(!empty( $email )){ echo $email; } ?>
                        </div>
                        <div class="col-sm-6 text-right padding-vert-5">
                          <?php
                            $ses=$this->session->userdata('id_users');
                            if(!$ses) { echo'<strong><a href="'.base_url('login').'">Log In</a></strong>';  }else{
                              $where = array(
                              'id_users' => $this->session->userdata('id_users')
                              );
                              $this->db->where($where);
                              $this->db->from('users');
                              $jml = $this->db->count_all_results();
                              if( $jml > 0 ){
                                $this->db->where($where);
                                $query = $this->db->get('users');
                                foreach ($query->result() as $row)
                                  {/*
                                  echo '
                                  <ul id="hornavmenu" class="nav navbar-nav">
                                    <li><a href="'.base_url().'dashboard">Anggota</a></li>
                                  </ul>';*/
                                    $hak_akses = $row->hak_akses;
                                    if($hak_akses=='register'){
                                      echo '<strong><a href="'.base_url().'dashboard">Anggota</a></strong>';
                                    }else{
                                    echo '<strong><a href="'.base_url().'welcome_admin">Administrator</a></strong>';
                                    }
                                  }
                                }
                              else{
                                  exit;
                                }
                            }
                          ?>
                            &nbsp;|&nbsp;
                            <strong><a href="https://diskominfo.wonosobokab.go.id/postings/galeri/1892/FAQ.HTML" style="color:#656565;">FAQ</a></strong>&nbsp;|&nbsp;
                            <!-- Modified by Novikov.ua -->
                            <a href="#" onclick="doGTranslate('id|en');return false;" title="English" class="gflag nturl"
                            style="background-position:-0px -0px;"><img src="//gtranslate.net/flags/16.png" height="16" width="16"
                                                                        alt="English"/></a>
                            <a href="#" onclick="doGTranslate('en|id');return false;" title="Indonesia" class="gflag nturl"
                            style="background-position:-300px -300px;"><img src="//gtranslate.net/flags/16.png" height="16" width="16"
                                                                            alt="Indonesia"/></a>


                            <div id="google_translate_element2"></div>

                            <script type="text/javascript">
                                function googleTranslateElementInit2() {
                                    new google.translate.TranslateElement({
                                        pageLanguage: 'id',
                                        autoDisplay: false
                                    }, 'google_translate_element2');
                                }
                            </script>
                            <script type="text/javascript" src="https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit2"></script>
                            <script type="text/javascript">
                                /* <![CDATA[ */
                                eval(function (p, a, c, k, e, r) {
                                    e = function (c) {
                                        return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36))
                                    };
                                    if (!''.replace(/^/, String)) {
                                        while (c--) r[e(c)] = k[c] || e(c);
                                        k = [function (e) {
                                            return r[e]
                                        }];
                                        e = function () {
                                            return '\\w+'
                                        };
                                        c = 1
                                    }
                                    while (c--) if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]);
                                    return p
                                }('6 7(a,b){n{4(2.9){3 c=2.9("o");c.p(b,f,f);a.q(c)}g{3 c=2.r();a.s(\'t\'+b,c)}}u(e){}}6 h(a){4(a.8)a=a.8;4(a==\'\')v;3 b=a.w(\'|\')[1];3 c;3 d=2.x(\'y\');z(3 i=0;i<d.5;i++)4(d[i].A==\'B-C-D\')c=d[i];4(2.j(\'k\')==E||2.j(\'k\').l.5==0||c.5==0||c.l.5==0){F(6(){h(a)},G)}g{c.8=b;7(c,\'m\');7(c,\'m\')}}', 43, 43, '||document|var|if|length|function|GTranslateFireEvent|value|createEvent||||||true|else|doGTranslate||getElementById|google_translate_element2|innerHTML|change|try|HTMLEvents|initEvent|dispatchEvent|createEventObject|fireEvent|on|catch|return|split|getElementsByTagName|select|for|className|goog|te|combo|null|setTimeout|500'.split('|'), 0, {}))
                                /* ]]> */
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="" id="header">
                <div class="container no-padding">
                    <div class="row" style="margin-top: 10px;">
                        <div class="brandimg animate fadeInLeft animated"> <img src="<?php echo base_url(); ?>media/logo kabupaten wonosobo.png" height="80" width="60"></div>
                        <div class="brand animate fadeInUp animated"><h3 class="brand_name"><span><?php if(!empty( $keterangan )){ echo $keterangan; } ?></span></h3><span class="subjudul">Kabupaten Wonosobo</span></div>
                        <div class="animate fadeInLeft animated">
                        <form action="<?php echo base_url(); ?>postings">
													<div class="input-group input-group-sm">
														<input name="keyword_pencarian" class="form-control input-sm pull-right" style="width: 150px;" placeholder="Search" type="text" id="keyword_pencarian">
														<div class="input-group-btn">
															<button class="btn btn-sm btn-default"><i class="fa fa-search"></i> Pencarian</button>
														</div>
													</div>
                        </form>
                        </div>
                    </div>
                </div>
            </div>
            
            <!-- Top Menu -->
            <div id="hornav" class="">
              <div class="container no-padding border-bottom">
                <div class="row visible-lg">
															<?php echo ''.$menu_atas.''; ?>
                </div>
              </div>
            </div>
            <div id="content" class="row">
              <div class="container background-white">
                <div class="row margin-vert-30">
                  <!-- Main Text -->
                  
                  <div class="col-md-9">
                    <div class="animate fadeInRightBig animated">
                      <?php $this -> load -> view($main_view);  ?>
                      <?php 
                      if($this->uri->segment(4)=='DAFTAR_INFORMASI_PUBLIK.HTML'){
                        
                      }
                      else{
                        if(!empty($isi_posting)){ $this -> load -> view('modul/'.$nama_modul.''); }
                      }
                      ?>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="animate fadeInRightBig animated">
                      <?php
                      $url1 =  $this->uri->segment(1);
                      $url2 =  $this->uri->segment(2);
                      if($url2 == 'detail'){
                        $this -> load -> view('postings/pengumuman.php');
                      }elseif($url2 == 'daftar_peraturan'){
                        $this -> load -> view('postings/banner_jdih.php');
                      }
                      elseif($url1 == 'peraturan' && $url2 == 'details'){
                      }
                      else{
                        $this -> load -> view('postings/pengumuman.php');
                      }
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
            <div id="base">
                <div class="container padding-vert-30">
                    <div class="row animate fadeInUp animated">
                        <!-- Disclaimer -->
                        <div class="col-md-4">
                            <h3 class="margin-bottom-10">Statistik Pengunjung</h3>
                          <!--<img src="<?php echo base_url(); ?>media/logo-jdihn.png" height="100" width="99">
                          <img src="<?php echo base_url(); ?>media/logo kabupaten wonosobo.png" height="100" width="130">-->
									<div>
										<i class="fa fa-bar-chart-o"></i> <span id="hit_counter"></span>
									</div>
									<div>
										<i class="fa fa-bar-chart-o"></i> <span id="visitor"></span>
									</div>
                  <?php if(!empty( $KolomKiriBawah )){ echo $KolomKiriBawah; } ?>
												</div>
                        <!-- End Disclaimer -->
                        <!-- Contact Details -->
                        <div class="col-md-4 margin-bottom-20">
                            <h3 class="margin-bottom-10">Kontak Kami</h3>
									<address class="margin-bottom-40">
										<?php if(!empty( $alamat )){ echo $alamat; } ?><br>
										Phone: <?php if(!empty( $telpon )){ echo $telpon; } ?><br>
										Email: <a style="color: white;"href="mailto:<?php if(!empty( $email )){ echo $email; } ?>"><?php if(!empty( $email )){ echo $email; } ?></a><br>
										Website: <a style="color: white;"href="https://<?php echo $web; ?>">https://<?php echo $web; ?></a>
									</address>
                        </div>
                        <!-- End Contact Details -->
                        <!-- Sample Menu -->
                        <div class="col-md-4 margin-bottom-20">
									<?php if(!empty( $KolomPalingBawah )){ echo $KolomPalingBawah; } ?>
                            <div class="clearfix"></div>
                        </div>
                        <!-- End Sample Menu -->
                    </div>
                </div>
            </div>
            <!-- Footer -->
            <div id="footer" class="background-grey">
                <div class="container">
                    <div class="row">
                        <!-- Footer Menu -->
                        <div id="footermenu" class="col-md-8">
                          <ul class="social-footer list-unstyled list-inline pull-right">
                            <li><a style="font-size: 20px;" target="_blank" href="<?php if(!empty( $facebook )){ echo $facebook; } ?>"><i class="fa fa-facebook"></i></a></li>
                            <li><a style="font-size: 20px;" target="_blank" href="<?php if(!empty( $google )){ echo $google; } ?>"><i class="fa fa-google-plus"></i></a></li>
                            <li><a style="font-size: 20px;" target="_blank" href="<?php if(!empty( $twitter )){ echo $twitter; } ?>"><i class="fa fa-twitter"></i></a></li>
                            <li><a style="font-size: 20px;" target="_blank" href="<?php if(!empty( $instagram )){ echo $instagram; } ?>"><i class="fa fa-instagram"></i></a></li>
                            <li><a style="font-size: 20px;" target="_blank" href="javascript:;"><i class="fa fa-youtube"></i></a></li>
                          </ul>
                        </div>
                        <!-- End Footer Menu -->
                        <!-- Copyright -->
                        <div id="copyright" class="col-md-4">
                            <p class="pull-right"> <?php echo date('Y'); ?> &copy; <?php echo $web; ?></p>
                        </div>
                        <!-- End Copyright -->
                    </div>
                </div>
            </div>
            <!-- End Footer -->
            <!-- JS -->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.min.js" type="text/javascript"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/bootstrap.min.js" type="text/javascript"></script>
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/scripts.js"></script>
            <!-- Isotope - Portfolio Sorting -->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.isotope.js" type="text/javascript"></script>
            <!-- Mobile Menu - Slicknav -->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.slicknav.js" type="text/javascript"></script>
            <!-- Animate on Scroll-->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.visible.js" charset="utf-8"></script>
            <!-- Sticky Div -->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/jquery.sticky.js" charset="utf-8"></script>
            <!-- Slimbox2-->
            <script type="text/javascript" src="<?php echo base_url(); ?>Template/HTML/assets/js/slimbox2.js" charset="utf-8"></script>
            <!-- Modernizr -->
            <script src="<?php echo base_url(); ?>Template/HTML/assets/js/modernizr.custom.js" type="text/javascript"></script>
            <!-- End
            <script src="<?php echo base_url(); ?>Template/dots.js" type="text/javascript"></script> JS -->
						
            <script>
              function LoadVisitor() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    table:'visitor'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/simpan_visitor/',
                  success: function(html) {
                    $('#visitor').html('Total Pengunjung : '+html+' ');
                  }
                });
              }
            </script>
            <script>
              function LoadHitCounter() {
                $.ajax({
                  type: 'POST',
                  async: true,
                  data: {
                    current_url:'<?php echo base_url(); ?>index.php'
                  },
                  dataType: 'html',
                  url: '<?php echo base_url(); ?>visitor/hit_counter/',
                  success: function(html) {
                    $('#hit_counter').html('Total Pembaca : '+html+' ');
                  }
                });
              }
            </script>
            <script type="text/javascript">
            $(document).ready(function() {
              LoadVisitor();
              LoadHitCounter();
            });
            </script>
            <div id="fb-root"></div>
            <script>
              (function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.12&appId=351370971628122&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
              }(document, 'script', 'facebook-jssdk'));
            </script>
<script>
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});
</script>

    </body>
</html>
