<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
$ses=$this->session->userdata('id_users');
if(!$ses) { return redirect(''.base_url().'login');  }
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php $web=$this->uut->namadomain(base_url()); $aa=explode(".",$web);echo $aa[0]; ?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/Font-Awesome-master/css/all.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- pace-progress -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>pace-progress/themes/red/pace-theme-flat-top.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/sweetalert2/bootstrap-4.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.css">
    <!-- Theme 
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
    style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/adminlte3.min.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery.min.js"></script>  
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.core.css" />
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/alertify/alertify.default.css" id="toggleCSS" />
    <!-- summernote -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.css">
    
      <script type="text/javascript">
        <?php

        foreach ($menu->result() as $b1) {

          $data['id_users'] = $b1->id_users;

          echo

          '

          function menu'.$b1->module_code.'() {

            getAjax("/'.strtolower($b1->module_code).'", \'html\', function (response) {

                $("#tag_container").empty().html(response);

            });

          }

          ';

        }

          echo

          '

          function menuGanti_password() {

            getAjax("/ganti_password", \'html\', function (response) {

                $("#tag_container").empty().html(response);

            });

          }

          ';

          echo

          '

          function menuUser_profile() {

              getAjax("/user_profile", \'html\', function (response) {

                  $("#tag_container").empty().html(response);

              });

          }

          ';

          

          echo

          '

          function menuCompany_profile() {

            getAjax("/company_profile", \'html\', function (response) {

                $("#tag_container").empty().html(response);

            });

          }

          ';

          

          echo

          '

          function menuFoto_profil() {

            getAjax("/foto_profil", \'html\', function (response) {

                $("#tag_container").empty().html(response);

            });

          }

          ';

          

        ?>

      </script>
    <script src="<?php echo base_url(); ?>Template/main.js"></script>

  </head>
  <body class="hold-transition sidebar-mini">
    <div class="wrapper">
      <!-- Navbar -->
      <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link">Home</a>
          </li>
          <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Contact</a>
          </li>
        </ul>

        <!-- SEARCH FORM -->
        <form class="form-inline ml-3">
          <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form>

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
          <!-- Messages Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-comments"></i>
              <span class="badge badge-danger navbar-badge">3</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <a href="#" class="dropdown-item">
                <!-- Message Start
                <div class="media">
                  <img src="dist/img/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                  <div class="media-body">
                    <h3 class="dropdown-item-title">
                      Brad Diesel
                      <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                    </h3>
                    <p class="text-sm">Call me whenever you can...</p>
                    <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                  </div>
                </div>
                <!-- Message End -->
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item dropdown-footer">See All Messages</a>
            </div>
          </li>
          <!-- Notifications Dropdown Menu -->
          <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
              <i class="far fa-bell"></i>
              <span class="badge badge-warning navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <span class="dropdown-header">15 Notifications</span>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item">
                <i class="fas fa-envelope mr-2"></i> 4 new messages
                <span class="float-right text-muted text-sm">3 mins</span>
              </a>
              <div class="dropdown-divider"></div>
              <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
            </div>
          </li>
          <li class="nav-item dropdown user-menu">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
              <img src="<?php if( !empty($foto) ){echo ''.$foto.'';} ?>" class="user-image img-circle elevation-2" alt="User Image">
              <span class="d-none d-md-inline">
                <?php
                if( !empty($first_name) ){
                  echo ''.$first_name.' '.$last_name.' ';
                }
                ?>
              </span>
            </a>
            <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
              <!-- User image -->
              <li class="user-header bg-primary">
                <img src="<?php if( !empty($foto) ){echo ''.$foto.'';} ?>" class="img-circle elevation-2" alt="User Image">

                <p>
                <?php
                if( !empty($first_name) ){
                  echo ''.$first_name.' '.$last_name.' ';
                }
                ?>
                  <small>Member sejak <?php if ( !empty($membersince)){ echo $membersince; } ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-4 text-center">
                    <a href="#" onclick="menuGanti_password()">Change Password</a>
                  </div>
                  <div class="col-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <a href="#" class="btn btn-default btn-flat" onclick="menuUser_profile()">Profile</a>
                <a href="<?php echo base_url(); ?>login/logout" class="btn btn-default btn-flat float-right">Sign out</a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i class="fas fa-th-large"></i></a>
          </li>
        </ul>
      </nav>
      <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="<?php echo base_url(); ?>" class="brand-link">
          <img src="<?php if( !empty($foto) ){echo ''.$foto.'';} ?>" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
          <span class="brand-text font-weight-light"><?php $web=$this->uut->namadomain(base_url()); $aa=explode(".",$web);echo $aa[0]; ?></span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
          <!-- Sidebar user panel (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
              <img src="<?php if( !empty($foto) ){echo ''.$foto.'';} ?>" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
              <a href="#" class="d-block">
                <?php
                if( !empty($first_name) ){
                  echo ''.$first_name.' '.$last_name.' ';
                }
                ?>
              </a>
            </div>
          </div>

          <!-- Sidebar Menu -->
          <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
              <?php
                if( $menu_root->num_rows() > 0 ){
                  echo '<li class="nav-item has-treeview menu-open">';
                    echo 
                      '
                    <a href="#" class="nav-link active">
                      <i class="nav-icon fas fa-tachometer-alt"></i>
                      <p>
                        Starter ROOT
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    ';
                    echo '<ul class="nav nav-treeview">';
                      foreach ($menu_root->result() as $b2) {
                        if($b2->urutan_menu==1){
                          echo '
                          <li class="nav-item">
                            <a href="#" class="nav-link active" onclick="menu'.$b2->module_code.'()">
                              <i class="far fa-circle nav-icon"></i>
                              <p>'.ucwords(str_replace('_', ' ', $b2->module_name)).'<span class="badge badge-default right">'.$b2->urutan_menu.'</span></p>
                            </a>
                          </li>
                          ';
                        }else{
                          echo '
                          <li class="nav-item">
                            <a href="#" class="nav-link" onclick="menu'.$b2->module_code.'()">
                              <i class="far fa-circle nav-icon"></i>
                              <p>'.ucwords(str_replace('_', ' ', $b2->module_name)).'<span class="badge badge-default right">'.$b2->urutan_menu.'</span></p>
                            </a>
                          </li>
                          ';
                        }
                      }
                    echo '</ul>';
                  echo '</li>';
                }
                if( $menu_adminwebdesa->num_rows() > 0 ){
                  echo '<li class="nav-item has-treeview menu-open">';
                    echo 
                      '
                    <a href="#" class="nav-link active">
                      <i class="nav-icon fas fa-tachometer-alt"></i>
                      <p>
                        Starter Admin Web SKPD
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    ';
                    echo '<ul class="nav nav-treeview">';
                      foreach ($menu_adminwebdesa->result() as $b2) {
                        if($b2->urutan_menu==1){
                          echo '
                          <li class="nav-item">
                            <a href="#" class="nav-link active" onclick="menu'.$b2->module_code.'()">
                              <i class="far fa-circle nav-icon"></i>
                              <p>'.ucwords(str_replace('_', ' ', $b2->module_name)).'<span class="badge badge-default right">'.$b2->urutan_menu.'</span></p>
                            </a>
                          </li>
                          ';
                        }else{
                          echo '
                          <li class="nav-item">
                            <a href="#" class="nav-link" onclick="menu'.$b2->module_code.'()">
                              <i class="far fa-circle nav-icon"></i>
                              <p>'.ucwords(str_replace('_', ' ', $b2->module_name)).'<span class="badge badge-default right">'.$b2->urutan_menu.'</span></p>
                            </a>
                          </li>
                          ';
                        }
                      }
                    echo '</ul>';
                  echo '</li>';
                }
                if( $menu_adminwebskpd->num_rows() > 0 ){
                  echo '<li class="nav-item has-treeview menu-open">';
                    echo 
                      '
                    <a href="#" class="nav-link active">
                      <i class="nav-icon fas fa-tachometer-alt"></i>
                      <p>
                        Starter Admin Web Desa
                        <i class="right fas fa-angle-left"></i>
                      </p>
                    </a>
                    ';
                    echo '<ul class="nav nav-treeview">';
                      foreach ($menu_adminwebskpd->result() as $b2) {
                        if($b2->urutan_menu==1){
                          echo '
                          <li class="nav-item">
                            <a href="#" class="nav-link active" onclick="menu'.$b2->module_code.'()">
                              <i class="far fa-circle nav-icon"></i>
                              <p>'.ucwords(str_replace('_', ' ', $b2->module_name)).'<span class="badge badge-default right">'.$b2->urutan_menu.'</span></p>
                            </a>
                          </li>
                          ';
                        }else{
                          echo '
                          <li class="nav-item">
                            <a href="#" class="nav-link" onclick="menu'.$b2->module_code.'()">
                              <i class="far fa-circle nav-icon"></i>
                              <p>'.ucwords(str_replace('_', ' ', $b2->module_name)).'<span class="badge badge-default right">'.$b2->urutan_menu.'</span></p>
                            </a>
                          </li>
                          ';
                        }
                      }
                    echo '</ul>';
                  echo '</li>';
                }
              ?>
            </ul>
          </nav>
          <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
      </aside>
      <div class="content-wrapper" style="min-height: 315px;">
        <!-- Content Header (Page header) -->
        <div class="content-header">
          <div class="container-fluid">
            <div class="row mb-2">
              <div class="col-sm-6">
                <h1 class="m-0 text-dark">Starter Page</h1>
              </div><!-- /.col -->
              <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active">Starter Page</li>
                </ol>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <div class="content">
          <div class="container-fluid" id="tag_container">
            <div class="row">
              <div class="col-lg-6">
                <div class="card">
                  <div class="card-body">
                    <h5 class="card-title">Card title</h5>

                    <p class="card-text">
                      Some quick example text to build on the card title and make up the bulk of the card's
                      content.
                    </p>

                    <a href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                  </div>
                </div>

                <div class="card card-primary card-outline">
                  <div class="card-body">
                    <h5 class="card-title">Card title</h5>

                    <p class="card-text">
                      Some quick example text to build on the card title and make up the bulk of the card's
                      content.
                    </p>
                    <a href="#" class="card-link">Card link</a>
                    <a href="#" class="card-link">Another link</a>
                  </div>
                </div><!-- /.card -->
              </div>
              <!-- /.col-md-6 -->
              <div class="col-lg-6">
                <div class="card">
                  <div class="card-header">
                    <h5 class="m-0">Featured</h5>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">Special title treatment</h6>

                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                  </div>
                </div>

                <div class="card card-primary card-outline">
                  <div class="card-header">
                    <h5 class="m-0">Featured</h5>
                  </div>
                  <div class="card-body">
                    <h6 class="card-title">Special title treatment</h6>

                    <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                  </div>
                </div>
              </div>
              <!-- /.col-md-6 -->
            </div>
            <!-- /.row -->
          </div><!-- /.container-fluid -->
        </div>
        <!-- /.content -->
      </div>
      
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
        <div class="p-3">
          <h5>Title</h5>
          <p>Sidebar content</p>
        </div>
      </aside>
      
      <footer class="main-footer">
        <!-- To the right -->
        <div class="float-right d-none d-sm-inline">
          Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright © 2014-2019 <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.
      </footer>
      
      <div id="sidebar-overlay"></div>
      
      
        <script type="text/javascript">

          $(document).ready(function() {

            $(document).ajaxStart(function() {

              $("#loadinghalaman").show();

              Pace.restart();

            });

            $(document).ajaxSuccess(function() {

              Pace.stop();

            });

          });

        </script>

    <script src="https://adminlte.io/themes/dev/AdminLTE/plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap 4 -->
    <script src="https://adminlte.io/themes/dev/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- SweetAlert2 -->
    <script src="<?php echo base_url(); ?>assets/plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="<?php echo base_url(); ?>assets/plugins/toastr/toastr.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dist/js/adminlte3.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>

		<script src="<?php echo base_url(); ?>assets/js/uutv1.js"></script>
		<!-- alertify -->
		<script src="<?php echo base_url(); ?>js/alertify.min.js"></script>
    <script src="<?php echo base_url(); ?>boots/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/bootstrap-datepicker.js" type="text/javascript"></script>
		<script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/summernote/summernote-bs4.min.js"></script>
    
        <!-- Default-->
        <script src="<?php echo base_url(); ?>js/jquery.form.js"></script>
        <script src="<?php echo base_url(); ?>js/alertify.min.js"></script>

        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script>

        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->    

        <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.js"></script>

        <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>

        <script src="<?php echo base_url(); ?>js/plugins/input-mask/jquery.inputmask.extensions.js"></script>

        <script>

          $(function () {

            $("[data-mask]").inputmask();

          });

        </script>

        <!-- AdminLTE for demo purposes -->    

        <!-- pace-progress -->
        <script src="<?php echo base_url(); ?>pace-progress/pace.min.js"></script>

        <script src="<?php echo base_url(); ?>js/uutv1.js"></script>

        <script src="<?php echo base_url(); ?>assets/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>

        <script src="<?php echo base_url(); ?>assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.js"></script>

    </body>

</html>
<!-- end document-->