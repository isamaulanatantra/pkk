<section class="content" id="awal">
  <div class="row">
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tab_1" id="klik_tab_input">Form</a> <span id="demo"></span></li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <div class="row">
          <div class="col-md-12">
            <div class="box box-primary box-solid">
              <div class="box-header with-border">
                <h3 class="box-title" id="judul_formulir">FORMULIR INPUT</h3>
                <div class="box-tools pull-right">
                  <button class="btn btn-box-tool" id="form_baru"><i class="fa fa-plus"></i></button>
                </div>
              </div>
              <div class="box-body">
                <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>header_website/upload/?table_name=header_website" enctype="multipart/form-data">
                  <div class="box-body">
										<div class="form-group" style="display:none;">
											<label for="mode_exel">mode exel</label>
											<input class="form-control" id="mode_exel" name="mode_exel" value="0" placeholder="mode_exel" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="halaman">halaman</label>
											<input class="form-control" id="halaman" name="halaman" value="" placeholder="halaman" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="temp">temp</label>
											<input class="form-control" id="temp" name="temp" value="" placeholder="temp" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="mode">mode</label>
											<input class="form-control" id="mode" name="mode" value="input" placeholder="mode" type="text">
										</div>
										<div class="form-group" style="display:none;">
											<label for="id_header_website">id_header_website</label>
											<input class="form-control" id="id_header_website" name="id" value="" placeholder="id_header_website" type="text">
										</div>
                    <p>&nbsp;</p>
                    <p>Ukuran Logo Yang proporsional adalah : 1366PX x 120PX</p>
										<div class="alert alert-info alert-dismissable">
											<div class="form-group">
												<label for="remake">Keterangan</label>
												<input class="form-control" id="remake" name="remake" placeholder="Keterangan Lampiran " type="text">
											</div>
											<div class="form-group">
												<label for="myfile">File</label>
												<input type="file" size="60" name="myfile" id="file_lampiran" >
											</div>
											<div id="ProgresUpload">
												<div id="BarProgresUpload"></div>
												<div id="PersenProgresUpload">0%</div >
											</div>
											<div id="PesanProgresUpload"></div>
										</div>
										<div class="alert alert-info alert-dismissable">
											<h3 class="box-title">Data Header </h3>
											<table class="table table-bordered">
												<tr>
													<th>No</th><th>Keterangan</th><th>Status</th><th>Download</th><th>Hapus</th> 
												</tr>
												<tbody id="tbl_header_website_header_website">
												</tbody>
											</table>
										</div>
                  </div>
                  <div class="box-footer">
                    <button type="submit" class="btn btn-primary" id="simpan_header_website">SIMPAN</button>
                    <button type="submit" class="btn btn-primary" id="update_header_website" style="display:none;">UPDATE</button>
                  </div>
                </form>
              </div>
              <div class="overlay" id="overlay_form_input" style="display:none;">
                <i class="fa fa-refresh fa-spin"></i>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="tab-pane" id="tab_2">
        
      </div>
    </div>
  </div>
</section>

<script>
	function AttachmentByMode(mode, value) {
		$('#tbl_header_website_header_website').html('');
		$.ajax({
			type: 'POST',
			async: true,
			data: {
        table:'header_website',
				mode:mode,
        value:value 
			},
			dataType: 'json',
			url: '<?php echo base_url(); ?>header_website/load_lampiran/',
			success: function(json) {
				var tr = '';
				for (var i = 0; i < json.length; i++) {
					tr += '<tr id_header_website="'+json[i].id_header_website+'" id="'+json[i].id_header_website+'" >';
					tr += '<td valign="top">'+(i + 1)+'</td>';
					tr += '<td valign="top">'+json[i].keterangan+'</td>';
          if( json[i].status == 1 ){
            tr += '<td valign="top"><a href="#" id="inaktifkan">Aktif</a> </td>';
          }
          else{
            tr += '<td valign="top"><a href="#" id="aktifkan">TIdak Aktif</a> </td>';
          }
					tr += '<td valign="top"><a href="<?php echo base_url(); ?>media/upload/'+json[i].file_name+'" target="_blank">Download</a> </td>';
					
          tr += '<td valign="top"><a href="#" id="del_ajax"><i class="fa fa-cut"></i></a> </td>';
					tr += '</tr>';
				}
				$('#tbl_header_website_header_website').append(tr);
			}
		});
	}
</script>

<script>
	$(document).ready(function(){
    var options = { 
      beforeSend: function() {
        $('#ProgresUpload').show();
        $('#BarProgresUpload').width('0%');
        $('#PesanProgresUpload').html('');
        $('#PersenProgresUpload').html('0%');
        },
      uploadProgress: function(event, position, total, percentComplete){
        $('#BarProgresUpload').width(percentComplete+'%');
        $('#PersenProgresUpload').html(percentComplete+'%');
        },
      success: function(){
        $('#BarProgresUpload').width('100%');
        $('#PersenProgresUpload').html('100%');
        },
      complete: function(response){
        $('#PesanProgresUpload').html('<font color="green">'+response.responseText+'</font>');
        var mode = $('#mode').val();
        if(mode == 'edit'){
          var value = $('#id_header_website').val();
        }
        else{
          var value = $('#temp').val();
        }
        AttachmentByMode(mode, value);
        $('#remake').val('');
        },
      error: function(){
        $('#PesanProgresUpload').html('<font color="red"> ERROR: unable to upload files</font>');
        }     
    };
    document.getElementById('file_lampiran').onchange = function() {
        $('#form_isian').submit();
      };
    $('#form_isian').ajaxForm(options);
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  var mode = $('#mode').val();
    if(mode == 'edit'){
      var value = $('#id_header_website').val();
    }
    else{
      var value = $('#temp').val();
    }
  AttachmentByMode(mode, value);
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_header_website_header_website').on('click', '#del_ajax', function() {
    var id_header_website = $(this).closest('tr').attr('id_header_website');
    alertify.confirm('Anda yakin data akan dihapus?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_header_website"] = id_header_website;
        var url = '<?php echo base_url(); ?>header_website/hapus/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_header_website').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_header_website='+id_header_website+']').remove();
      } else {
        alertify.error('Hapus data dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_header_website_header_website').on('click', '#inaktifkan', function() {
    var id_header_website = $(this).closest('tr').attr('id_header_website');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_header_website"] = id_header_website;
        var url = '<?php echo base_url(); ?>header_website/inaktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_header_website').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_header_website='+id_header_website+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>

<script type="text/javascript">
$(document).ready(function() {
  $('#tbl_header_website_header_website').on('click', '#aktifkan', function() {
    var id_header_website = $(this).closest('tr').attr('id_header_website');
    alertify.confirm('Anda yakin data akan diinaktifkan?', function(e) {
      if (e) {
        var parameter = {}
        parameter["id_header_website"] = id_header_website;
        var url = '<?php echo base_url(); ?>header_website/aktifkan/';
        HapusAttachment(parameter, url);
        var mode = $('#mode').val();
          if(mode == 'edit'){
            var value = $('#id_header_website').val();
          }
          else{
            var value = $('#temp').val();
          }
        AttachmentByMode(mode, value);
        $('[id_header_website='+id_header_website+']').remove();
      } else {
        alertify.error('Proses dibatalkan');
      }
    });
  });
});
</script>