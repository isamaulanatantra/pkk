<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Peraturan_model extends CI_Model
{

	function opsi($where, $table_name, $fields, $order_by){
		$this->db->select("$fields");
		$this->db->where($where);
		$this->db->order_by($order_by);
		return $this->db->get($table_name);
	}
  function details_peraturan($where_id,$table_name)
	{
    $this->db->select("
      *,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Isi Peraturan') as attachment_isi_peraturan,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Katalog') as attachment_katalog,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='Abstrak') as attachment_abstrak,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='NA') as attachment_na,
        (select attachment.file_name from attachment where attachment.id_tabel=peraturan.id_peraturan and attachment.table_name='peraturan' and attachment.keterangan='MOU') as attachment_mou
			");
		$this->db->where($where_id);
    return $this->db->get($table_name);
	}
  function daftar_peraturan1($where_id1,$table_name1,$fields1,$order_by1)
	{
		$this->db->select("
		$fields1
		");
		$this->db->where($where_id1);
		$this->db->order_by($order_by1, 'DESC');
		$this->db->order_by('peraturan.nomor_peraturan', 'ASC');
    return $this->db->get($table_name1);
	}
  function daftar_peraturan($where_id,$table_name,$fields,$order_by)
	{
		$this->db->select("
		$fields
		");
		$this->db->where($where_id);
		$this->db->order_by($order_by, 'DESC');
		$this->db->order_by('peraturan.nomor_peraturan', 'ASC');
    return $this->db->get($table_name);
	}
  function detail_peraturan($where_id,$fields,$table_name)
	{
		$this->db->select("
		$fields
		");
		$this->db->where($where_id);
    return $this->db->get($table_name);
	}
	function opsi_parent($table_name, $fields, $where, $order_by){
		$this->db->select("
		$fields
		");
		$this->db->where($where);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
	}
	public function json_all_peraturan($where, $limit, $start, $fields, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		// $this->db->order_by('peraturan.tahun_peraturan DESC', 'peraturan.nomor_peraturan ASC');
		$this->db->order_by('peraturan.tahun_peraturan', 'DESC');
		$this->db->order_by('peraturan.nomor_peraturan', 'ASC');
		$this->db->limit($limit, $start);
		$result = $this->db->get('peraturan');
		return $result->result_array();
    }

	public function json_join($where, $limit, $start, $table_name, $fields, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function get_by_id($table_name, $where, $fields, $order_by) {
		$this->db->select("$fields");
    $this->db->where($where);
		$this->db->order_by($order_by, 'DESC');
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function seperti($table_name, $where, $fields, $order_by, $like_data) {
		$this->db->select("$fields");
    $this->db->where($where);
		$this->db->like($like_data);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function get_by_id_join($table_name, $where, $fields, $order_by) {
		$this->db->select("$fields");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
  function check_before_save($table_name, $where)
	{
		$this->db->select('*');
    	$this->db->where($where);
		$result = $this->db->get($table_name);
		return $result->result_array();
	}
		
  function simpan_peraturan($data_input, $table_name)
	{
		$this->db->insert( $table_name, $data_input );
		return $this->db->insert_id();
	}
		
  function update_data_peraturan($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
	
	function json_semua_peraturan($where) {	
		$this->db->from('peraturan');
		$this->db->where($where);
		return $this->db->count_all_results(); 
	}	
	
	function like_data($where, $table_name, $like_data) {	
		$this->db->from($table_name);
		$this->db->where($where);
		$this->db->like($like_data);
		return $this->db->count_all_results(); 
	}	
	
	//function count_all_search_peraturan($where, $key_word, $table_name, $field, $key_word, $nomor_peraturan, $tahun_peraturan) {	
	public function count_all_search_peraturan($fields, $where, $limit, $start, $id_parent) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		// $this->db->like('nama_peraturan', $key_word);
		$this->db->like('peraturan.id_parent', $id_parent);
		// $this->db->like('peraturan.nomor_peraturan', $nomor_peraturan);
		// $this->db->like('peraturan.tahun_peraturan', $tahun_peraturan);
		$this->db->limit($limit, $start);
		$result = $this->db->get('peraturan');
		return $this->db->count_all_results(); 
	}
		
	public function search_peraturan($fields, $where, $limit, $start, $key_word, $order_by, $id_parent, $nomor_peraturan, $tahun_peraturan) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->like('nama_peraturan', $key_word);
		$this->db->like('peraturan.id_parent', $id_parent);
		$this->db->like('peraturan.nomor_peraturan', $nomor_peraturan);
		$this->db->like('peraturan.tahun_peraturan', $tahun_peraturan);
		// $this->db->order_by($order_by, 'DESC');
		$this->db->order_by('peraturan.tahun_peraturan', 'DESC');
		$this->db->order_by('peraturan.nomor_peraturan', 'ASC');
		$this->db->limit($limit, $start);
		$result = $this->db->get('peraturan');
		return $result->result_array();
    }	
		
	public function search_join($table_name, $fields, $where, $limit, $start, $field_like, $key_word, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->like($field_like, $key_word);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
	
	function auto_suggest($q, $where, $fields) {
		$this->db->select("$fields");
		$this->db->where($where);
		$this->db->like('peraturan.nama_peraturan', $q);
		$this->db->or_like('peraturan.kode_peraturan', $q);
		$this->db->limit('20');
		$result = $this->db->get('peraturan');
		return $result->result_array();
	}
	public function getData($id = null)
	{
	  if($id === null){
		$table_name = 'peraturan';
		$where_id = array(
		  'peraturan.status !=' => 99,
		  'peraturan.tipe_peraturan' => 'opd',
		  'peraturan.id_parent !=' => 0
		);
		$fields     = "
					peraturan.id_peraturan,
					peraturan.domain,
					peraturan.nomor_peraturan,
					peraturan.tahun_peraturan,
					peraturan.status_peraturan,
					peraturan.kode_peraturan,
					peraturan.nama_peraturan,
					peraturan.keterangan,
					peraturan.status
									";
		$order_by   = 'peraturan.tahun_peraturan';
		
		$this->db->select("
		$fields
		");
		$this->db->where($where_id);
		$this->db->order_by($order_by, 'DESC');
		$this->db->order_by('peraturan.nomor_peraturan', 'ASC');
		$result =  $this->db->get($table_name)->result();	
		//return $result;
		foreach ($result as $row)
		{
			$w = $this->db->query("
			SELECT * from attachment
			where attachment.id_tabel=".$row->id_peraturan."
			and attachment.table_name='peraturan'
			");
			$lampiran = '';
			$no = 0;
			foreach($w->result() as $row_attachment){
				if($no == 0){
					$lampiran .= $row_attachment->file_name.'|'.$row_attachment->keterangan;
				}else{
					$lampiran .= ','.$row_attachment->file_name.'|'.$row_attachment->keterangan;
				}
				$no++;
			}
			$data[] = array(
				'id_peraturan' => $row->id_peraturan,
				'domain' => $row->domain,
				'nomor_peraturan' => $row->nomor_peraturan,
				'tahun_peraturan' => $row->tahun_peraturan,
				'status_peraturan' => $row->status_peraturan,
				'kode_peraturan' => $row->kode_peraturan,
				'nama_peraturan' => $row->nama_peraturan,
				'keterangan' => $row->keterangan,
				'lampiran' => $lampiran,
				'status' => $row->status
			);
		}
		return $data;
	  }else{
		return $this->db->get_where('peraturan', ['id_peraturan' => $id])->result_array();
	  }    
	}
  
	public function createData($data)
	{
	  $this->db->insert('peraturan', $data);
	  return $this->db->affected_rows();
	}
  
	public function updateData($data,$id)
	{
	  $this->db->update('peraturan', $data, ['id_peraturan' => $id]);
	  return $this->db->affected_rows();
	}
  
	public function deleteData($id)
	{
	  $this->db->delete('peraturan', ['id_peraturan' => $id]);
	  return $this->db->affected_rows();
	}
	
}