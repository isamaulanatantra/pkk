<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permohonan_sikpg_sippg_model extends CI_Model
{
  
  function get_modul($wheres = array())
	{
		$this->db->select('*');
    $this->db->from('modul');
    $this->db->where($wheres);
		return $this->db->get()->row_array();
	}
  
  function get_data($where = array())
	{
		$this->db->select('
    permohonan_sikpg_sippg.id_permohonan_sikpg_sippg,
    permohonan_sikpg_sippg.parent,
    permohonan_sikpg_sippg.urut,
    permohonan_sikpg_sippg.posisi,
    permohonan_sikpg_sippg.judul_permohonan_sikpg_sippg,
    permohonan_sikpg_sippg.isi_permohonan_sikpg_sippg,
    permohonan_sikpg_sippg.kata_kunci,
    permohonan_sikpg_sippg.temp,
    permohonan_sikpg_sippg.created_by,
    permohonan_sikpg_sippg.created_time,
    permohonan_sikpg_sippg.updated_by,
    permohonan_sikpg_sippg.updated_time,
    permohonan_sikpg_sippg.deleted_by,
    permohonan_sikpg_sippg.deleted_time,
    permohonan_sikpg_sippg.status,
    permohonan_sikpg_sippg.keterangan,
    permohonan_sikpg_sippg.icon,
    permohonan_sikpg_sippg.highlight,
    permohonan_sikpg_sippg.tampil_menu	,
    permohonan_sikpg_sippg.tampil_menu_atas,
    
    ( select (users.nama) from users where users.id_users=permohonan_sikpg_sippg.created_by limit 1) as pembuat,
    ( select (users.nama) from users where users.id_users=permohonan_sikpg_sippg.updated_by limit 1) as pengedit
    
    ');
    $this->db->from('permohonan_sikpg_sippg');
    $this->db->where($where);
		return $this->db->get()->row_array();
	}
  
  function get_attachment_by_id_permohonan_sikpg_sippg($where = array())
	{
		$this->db->select('*');
    $this->db->from('attachment');
    $this->db->where($where);
		return $this->db->get()->row_array();
	}
  
	function opsi($where, $table_name, $fields, $order_by){
		$this->db->select("$fields");
		$this->db->where($where);
		$this->db->order_by($order_by);
		return $this->db->get($table_name);
	}

	public function json_all_permohonan_sikpg_sippg($where, $limit, $start, $fields, $order_by, $kata_kunci) {
		$this->db->select("
		$fields
		");
    if( $kata_kunci <> '' ){
      $this->db->like('judul_permohonan_sikpg_sippg', $kata_kunci);
      $this->db->or_like('domain', $kata_kunci);
    }
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('permohonan_sikpg_sippg');
		return $result->result_array();
    }
	public function json_all_permohonan_sikpg_sippgas($where, $limit, $start, $fields, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('permohonan_sikpg_sippg');
		return $result->result_array();
    }

	public function json_join($where, $limit, $start, $table_name, $fields, $order_by) {
		$this->db->select("
		$fields
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function get_by_id($table_name, $where, $fields, $order_by) {
		$this->db->select("$fields");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function seperti($table_name, $where, $fields, $order_by, $like_data) {
		$this->db->select("$fields");
    $this->db->where($where);
		$this->db->like($like_data);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function get_by_id_join($table_name, $where, $fields, $order_by) {
		$this->db->select("$fields");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
  function check_before_save($table_name, $where)
	{
		$this->db->select('*');
    	$this->db->where($where);
		$result = $this->db->get($table_name);
		return $result->result_array();
	}
	
  function simpan_permohonan_sikpg_sippg($data_input, $table_name)
	{
		$this->db->insert( $table_name, $data_input );
		return $this->db->insert_id();
	}
		
  function update_data_permohonan_sikpg_sippg($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
	
	function json_semua_permohonan_sikpg_sippg($where) {	
		$this->db->from('permohonan_sikpg_sippg');
		$this->db->where($where);
		return $this->db->count_all_results(); 
	}	
	
	function like_data($where, $table_name, $like_data) {	
		$this->db->from($table_name);
		$this->db->where($where);
		$this->db->like($like_data);
		return $this->db->count_all_results(); 
	}	
	
	function count_all_search_permohonan_sikpg_sippg($where, $key_word, $table_name, $field) {	
		$this->db->from($table_name);
		$this->db->where($where);
		$this->db->like($field, $key_word);
		return $this->db->count_all_results(); 
	}
		
	public function search_permohonan_sikpg_sippg($fields, $where, $limit, $start, $key_word, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->like('urut_permohonan_sikpg_sippg', $key_word);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('permohonan_sikpg_sippg');
		return $result->result_array();
    }	
		
	public function search_join($table_name, $fields, $where, $limit, $start, $field_like, $key_word, $order_by) {
		$this->db->select("
		$fields
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->like($field_like, $key_word);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }

	public function json_buku_kategori_buku($where) {
		$this->db->select("
		kategori_buku.kategori_buku_urut,
		buku_kategori_buku.id_buku_kategori_buku
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$result = $this->db->get('buku_kategori_buku');
		return $result->result_array();
    }

	public function json_laborat_perbagian($where) {
		$this->db->select("
		pus_pendaftaran.tanggal_pendaftaran,
		pus_pendaftaran.nomor_pendaftaran,
		bagian_puskesmas.urut_bagian_puskesmas,
		pus_pelayanan.id_pus_pelayanan,
		pasien.urut_penduduk,
		(select COUNT(*) FROM pus_pelayanan_pemeriksaan_laborat where pus_pelayanan_pemeriksaan_laborat.id_pus_pelayanan = pus_pelayanan.id_pus_pelayanan ) AS jumlah,
		(select COUNT(*) FROM pus_pelayanan_pemeriksaan_laborat where (pus_pelayanan_pemeriksaan_laborat.id_pus_pelayanan = pus_pelayanan.id_pus_pelayanan and pus_pelayanan_pemeriksaan_laborat.status = 1 ) ) AS status_pemeriksaan_lab
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by('pus_pendaftaran.nomor_pendaftaran');
		$result = $this->db->get('pus_pendaftaran');
		return $result->result_array();
    }

	public function json_obat_perbagian($where) {
		$this->db->select("
		pus_pendaftaran.tanggal_pendaftaran,
		pus_pendaftaran.nomor_pendaftaran,
		bagian_puskesmas.urut_bagian_puskesmas,
		pus_pelayanan.id_pus_pelayanan,
		pasien.urut_penduduk,
		(select COUNT(*) FROM pus_pelayanan_obat where pus_pelayanan_obat.id_pus_pelayanan = pus_pelayanan.id_pus_pelayanan and pus_pelayanan_obat.status = 1 ) AS jumlah,
		(select COUNT(*) FROM pus_pelayanan_obat where (pus_pelayanan_obat.id_pus_pelayanan = pus_pelayanan.id_pus_pelayanan and pus_pelayanan_obat.status = 1 ) ) AS status_pemberian_obat
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by('pus_pendaftaran.nomor_pendaftaran');
		$result = $this->db->get('pus_pendaftaran');
		return $result->result_array();
    }

	public function json_detail_obat_per_layanan($where) {
		$this->db->select("
		pus_pelayanan_obat.id_pus_pelayanan_obat,
		pus_pelayanan_obat.price,
		pus_pelayanan_obat.dosis_obat,
		pus_pelayanan_obat.jumlah_obat,
		pus_pelayanan_obat.status,
		obat.urut_obat
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by('pus_pelayanan_obat.id_pus_pelayanan_obat');
		$result = $this->db->get('pus_pelayanan_obat');
		return $result->result_array();
    }

	public function show_json_absensi_hari_ini($where, $table_name) {
		$this->db->select("
		absensi.id_absensi,
		absensi.id_permohonan_sikpg_sippg,
		absensi.hari_kerja,
		absensi.jam_absen,
		absensi.login_dari,
		absensi.jam_pulang,
		absensi.logout_dari,
		pegawai.urut_pegawai,
		puskesmas.urut_puskesmas
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by('absensi.jam_absen');
		$result = $this->db->get('absensi');
		return $result->result_array();
    }

	public function show_json_absensi_hari_ini_dinas($where, $table_name) {
		$this->db->select("
		absensi.id_absensi,
		absensi.id_permohonan_sikpg_sippg,
		absensi.hari_kerja,
		absensi.jam_absen,
		absensi.login_dari,
		absensi.jam_pulang,
		absensi.logout_dari,
		pegawai.urut_pegawai
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by('absensi.jam_absen');
		$result = $this->db->get('absensi');
		return $result->result_array();
    }
	function auto_suggest($q, $where, $fields) {
		$this->db->select("$fields");
		$this->db->where($where);
		$this->db->like('permohonan_sikpg_sippg.urut_permohonan_sikpg_sippg', $q);
		$this->db->or_like('permohonan_sikpg_sippg.judul_permohonan_sikpg_sippg', $q);
		$this->db->limit('20');
		$result = $this->db->get('permohonan_sikpg_sippg');
		return $result->result_array();
	}
	
}