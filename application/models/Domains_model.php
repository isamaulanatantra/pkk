<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Domains_model extends CI_Model
{
  
  public function getDomainDesa($id = null)
  {
    if($id === null){
      // return $this->db->get('data_desa')->result_array();
      $this->db->select('data_desa.desa_website, desa.kode_desa, desa.kode_kecamatan, desa.kode_kabupaten, desa.kode_propinsi, data_desa.jenis_pemerintahan');
      $this->db->join('dasar_website', 'dasar_website.domain=data_desa.desa_website');
      $this->db->join('desa', 'desa.id_desa=data_desa.id_desa');
      $where      = array(
        'data_desa.status' => 1,
        'dasar_website.status' => 1,
        'desa.id_kabupaten' => 1,
        'desa.id_propinsi' => 1
      );
      $this->db->where($where);
      $result = $this->db->get('data_desa');
      return $result->result_array();
    }else{
      return $this->db->get_where('data_desa', ['id_data_desa' => $id])->result_array();
      
    }
  }
  
  public function getDomainKecamatan($id = null)
  {
    if($id === null){
      // return $this->db->get('data_kecamatan')->result_array();
      $this->db->select('data_kecamatan.kecamatan_website, kecamatan.kode_kecamatan, kecamatan.kode_kecamatan, kecamatan.kode_kabupaten, kecamatan.kode_propinsi');
      $this->db->join('dasar_website', 'dasar_website.domain=data_kecamatan.kecamatan_website');
      $this->db->join('kecamatan', 'kecamatan.id_kecamatan=data_kecamatan.id_kecamatan');
      $where      = array(
        'data_kecamatan.status' => 1,
        'dasar_website.status' => 1,
        'kecamatan.id_kabupaten' => 1,
        'kecamatan.id_propinsi' => 1
      );
      $this->db->where($where);
      $result = $this->db->get('data_kecamatan');
      return $result->result_array();
    }else{
      return $this->db->get_where('data_kecamatan', ['id_data_kecamatan' => $id])->result_array();
      
    }
  }
  public function getDomainSkpd($id = null)
  {
    if($id === null){
      // return $this->db->get('data_skpd')->result_array();
      $this->db->select('data_skpd.skpd_website, skpd.kode_skpd, skpd.kode_skpd');
      $this->db->join('dasar_website', 'dasar_website.domain=data_skpd.skpd_website');
      $this->db->join('skpd', 'skpd.id_skpd=data_skpd.id_skpd');
      $where      = array(
        'data_skpd.status' => 1,
        'dasar_website.status' => 1
      );
      $this->db->where($where);
      $result = $this->db->get('data_skpd');
      return $result->result_array();
    }else{
      return $this->db->get_where('data_skpd', ['id_data_skpd' => $id])->result_array();
      
    }
  }
}