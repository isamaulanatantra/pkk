<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
	
	public function html_harga_kebutuhan_pokok($table, $where, $limit, $start, $fields, $orderby, $keyword, $filter){
    $this->db->select("$fields");
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
		$this->db->join('komoditi', 'komoditi.id_komoditi=harga_kebutuhan_pokok.id_komoditi');
    $this->db->where($where);
    $this->db->order_by($orderby, ''.$filter.'');
    $this->db->limit($limit, $start);
    return $this->db->get($table);
	}
}