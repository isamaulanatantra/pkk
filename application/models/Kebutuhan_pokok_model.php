<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kebutuhan_pokok_model extends CI_Model
{
  public function show_data($where,$table_name) {
		$this->db->select("
			*
			");
		$this->db->where($where);
		//$this->db->order_by('parent ASC', 'urut ASC');
		$this->db->order_by('parent', 'urut');
		$result = $this->db->get($table_name);
		return $result->result_array();
  }
  public function show_data_harga($where) {
		$this->db->select("*");
    $this->db->where($where);
		$this->db->order_by('tanggal', 'DESC');
		$this->db->limit(1);
		$result = $this->db->get('harga_kebutuhan_pokok');
		return $result->result_array();
  }
  public function show_data_generate_baru($where) {
		$this->db->select("
			harga_kebutuhan_pokok.id_harga_kebutuhan_pokok,
			harga_kebutuhan_pokok.harga,
			harga_kebutuhan_pokok.tanggal,
			harga_kebutuhan_pokok.id_satuan,
			harga_kebutuhan_pokok.keterangan,
			komoditi.judul_komoditi,
			komoditi.parent,
			komoditi.urut,
			");
    	$this->db->where($where);
		//$this->db->order_by('parent ASC', 'urut ASC');
		$this->db->order_by('urut');
		$this->db->join('komoditi', 'komoditi.id_komoditi = harga_kebutuhan_pokok.id_komoditi', 'left');
		$result = $this->db->get('harga_kebutuhan_pokok');
		return $result->result_array();
    }
  public function show_data_generate($where) {
		$this->db->select("
			harga_kebutuhan_pokok.harga,
			harga_kebutuhan_pokok.tanggal,
			harga_kebutuhan_pokok.id_satuan,
			komoditi.judul_komoditi,
      komoditi.parent,
			");
    	$this->db->where($where);
		$this->db->join('komoditi', 'komoditi.id_komoditi = harga_kebutuhan_pokok.id_komoditi', 'left');
		// $this->db->join('bagian', 'bagian.id_bagian = harga_kebutuhan_pokok.id_bagian', 'left');
		$result = $this->db->get('harga_kebutuhan_pokok');
		return $result->result_array();
    }
  function simpan_data_baru($data_input)
	{
		$this->db->insert( 'harga_kebutuhan_pokok', $data_input );
		return $this->db->insert_id();
	}
  function simpan_data($data_input)
	{
		$this->db->insert( 'harga_kebutuhan_pokok', $data_input );
		return $this->db->insert_id();
	}
  function update_data_baru($data_update, $where)
	{
    $this->db->where($where);
		$this->db->update('harga_kebutuhan_pokok', $data_update);
	}
  function update_data_baru_publik($data_update, $where)
	{
    $this->db->where($where);
		$this->db->update('harga_kebutuhan_pokok_publik', $data_update);
	}
  function update_data($data_updatea, $wherea)
	{
    	$this->db->where($wherea);
		$this->db->update('harga_kebutuhan_pokok', $data_updatea);
	}
  public function show_json_all($where) {
		$this->db->select("
			*
			");
    	$this->db->where($where);
		$this->db->join('komoditi', 'komoditi.id_komoditi = harga_kebutuhan_pokok.id_komoditi', 'left');
		$result = $this->db->get('harga_kebutuhan_pokok');
		return $result->result_array();
    }
}