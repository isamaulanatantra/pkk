<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pendaftaran_pasar_model extends CI_Model
{

	public function html_all_pendaftaran_pasar($table, $where, $limit, $start, $fields, $orderby, $keyword, $tahun){
    $this->db->select("$fields");
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $this->db->like('pendaftaran_pasar.created_time', $tahun);
    $this->db->where($where);
    $this->db->order_by($orderby, 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get($table);
	}
	public function html_all($table, $where, $limit, $start, $fields, $orderby, $keyword){
    $this->db->select("$fields");
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $this->db->where($where);
    $this->db->order_by($orderby, 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get($table);
	}  
  function get_modul($wheres = array()){
		$this->db->select('*');
    $this->db->from('modul');
    $this->db->where($wheres);
		return $this->db->get()->row_array();
	}
  
  function update_pendaftaran_pasar($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
  function get_data($where = array()){
	$this->db->select('
    pendaftaran_pasar.id_pendaftaran_pasar,
    pendaftaran_pasar.domain,
    pendaftaran_pasar.parent,
    pendaftaran_pasar.id_posting,
    pendaftaran_pasar.nama,
    pendaftaran_pasar.tanggal_permohonan,
    pendaftaran_pasar.alamat,
    pendaftaran_pasar.nomor_telp,
    pendaftaran_pasar.email,
    pendaftaran_pasar.nik,
    pendaftaran_pasar.no_kk,
    pendaftaran_pasar.temp,
    pendaftaran_pasar.created_by,
    pendaftaran_pasar.created_time,
    pendaftaran_pasar.updated_by,
    pendaftaran_pasar.updated_time,
    pendaftaran_pasar.deleted_by,
    pendaftaran_pasar.deleted_time,
    pendaftaran_pasar.status,
    pendaftaran_pasar.balas,
    
    ( select (users.nama) from users where users.id_users=pendaftaran_pasar.created_by limit 1) as pembuat,
    ( select (users.nama) from users where users.id_users=pendaftaran_pasar.updated_by limit 1) as pengedit
    
    ');
    $this->db->from('pendaftaran_pasar');
    $this->db->where($where);
		return $this->db->get()->row_array();
	}
  
  function get_attachment_by_id_pendaftaran_pasar($where = array())
	{
		$this->db->select('*');
		$this->db->limit('1');
    $this->db->from('attachment');
    $this->db->where($where);
		return $this->db->get()->row_array();
	}
  
	function opsi($where, $table_name, $fields, $order_by){
		$this->db->select("$fields");
		$this->db->where($where);
		$this->db->order_by($order_by);
		return $this->db->get($table_name);
	}

	public function json_all_pendaftaran_pasar($where, $limit, $start, $fields, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->order_by($order_by, 'DESC');
		$this->db->limit($limit, $start);
		$result = $this->db->get('pendaftaran_pasar');
		return $result->result_array();
    }

	public function json_join($where, $limit, $start, $table_name, $fields, $order_by) {
		$this->db->select("
		$fields
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function get_by_id($table_name, $where, $fields, $order_by) {
		$this->db->select("$fields");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function seperti($table_name, $where, $fields, $order_by, $like_data) {
		$this->db->select("$fields");
    $this->db->where($where);
		$this->db->like($like_data);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function get_by_id_join($table_name, $where, $fields, $order_by) {
		$this->db->select("$fields");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
  function check_before_save($table_name, $where)
	{
		$this->db->select('*');
    	$this->db->where($where);
		$result = $this->db->get($table_name);
		return $result->result_array();
	}
	
  function simpan_pendaftaran_pasar($data_input, $table_name)
	{
		$this->db->insert( $table_name, $data_input );
		return $this->db->insert_id();
	}
		
  function update_data_attachment($data_update_attachment, $where_attachment, $table_name_attachment)
	{
		$this->db->where($where_attachment);
		$this->db->update($table_name_attachment, $data_update_attachment);
	}
  function update_data_pendaftaran_pasar($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
	
	function json_semua_pendaftaran_pasar($where) {	
		$this->db->from('pendaftaran_pasar');
		$this->db->where($where);
		return $this->db->count_all_results(); 
	}	
	
	function like_data($where, $table_name, $like_data) {	
		$this->db->from($table_name);
		$this->db->where($where);
		$this->db->like($like_data);
		return $this->db->count_all_results(); 
	}	
	
	function count_all_search_pendaftaran_pasar($where, $key_word, $table_name, $field) {	
		$this->db->from($table_name);
		$this->db->where($where);
		$this->db->like($field, $key_word);
		return $this->db->count_all_results(); 
	}
		
	public function search_pendaftaran_pasar($fields, $where, $limit, $start, $key_word, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->like('urut_pendaftaran_pasar', $key_word);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('pendaftaran_pasar');
		return $result->result_array();
    }	
	function auto_suggest($q, $where, $fields) {
		$this->db->select("$fields");
		$this->db->where($where);
		$this->db->like('pendaftaran_pasar.urut_pendaftaran_pasar', $q);
		$this->db->or_like('pendaftaran_pasar.judul_pendaftaran_pasar', $q);
		$this->db->limit('20');
		$result = $this->db->get('pendaftaran_pasar');
		return $result->result_array();
	}
	
}