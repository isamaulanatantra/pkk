<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perijinan_penelitian_model extends CI_Model
{
  
	public function json_all_perijinan_penelitian($where, $limit, $start, $fields, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('perijinan_penelitian');
		return $result->result_array();
    }

	public function get_by_id($table_name, $where, $fields, $order_by) {
		$this->db->select("$fields");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
  function simpan_perijinan_penelitian($data_input, $table_name)
	{
		$this->db->insert( $table_name, $data_input );
		return $this->db->insert_id();
	}
		
  function update_data_perijinan_penelitian($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
	
	function count_all_search_perijinan_penelitian($where, $key_word, $table_name, $field) {	
		$this->db->from($table_name);
		$this->db->where($where);
		$this->db->like($field, $key_word);
		return $this->db->count_all_results(); 
	}
		
 	function get_data($where = array()){
		$this->db->select('
			*,
			( select (users.nama) from users where users.id_users=perijinan_penelitian.created_by limit 1) as pembuat,
			( select (users.nama) from users where users.id_users=perijinan_penelitian.updated_by limit 1) as pengedit

			');
		$this->db->from('perijinan_penelitian');
		$this->db->where($where);
		return $this->db->get()->row_array();
	}
}