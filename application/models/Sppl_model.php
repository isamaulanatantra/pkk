<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sppl_model extends CI_Model
{
 
	public function json_all_sppl($where, $limit, $start, $fields, $order_by, $kata_kunci) {
		$this->db->select("
		$fields
		");
    if( $kata_kunci <> '' ){
      $this->db->like('nama_pemohon', $kata_kunci);
      $this->db->or_like('nama_usaha', $kata_kunci);
    }
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('sppl');
		return $result->result_array();
    }

}