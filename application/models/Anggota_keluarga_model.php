<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anggota_keluarga_model extends CI_Model
{
	function json_semua_anggota_keluarga($where) {	
		$this->db->from('anggota_keluarga');
		$this->db->where($where);
		return $this->db->count_all_results(); 
	}
	public function json_all_anggota_keluarga($where, $limit, $start, $fields, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('anggota_keluarga');
		return $result->result_array();
	}
  function update_anggota_keluarga($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
}