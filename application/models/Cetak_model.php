<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cetak_model extends CI_Model
{
	
	function cetak_data_pasien($table, $fields, $where, $order_by){
		$this->db->select("$fields");
		$this->db->join('pasien', 'pasien.id_pasien=lokal_rekam_medis.id_pasien');
		$this->db->join('jenis_kelamin', 'jenis_kelamin.id_jenis_kelamin=pasien.id_jenis_kelamin');
		$this->db->join('pendidikan', 'pendidikan.id_pendidikan=pasien.id_pendidikan');
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=pasien.id_pekerjaan');
		$this->db->join('golongan_darah', 'golongan_darah.id_golongan_darah=pasien.id_golongan_darah');
    $this->db->where($where);
    //$this->db->limit(100);
		$this->db->order_by($order_by);
		return $this->db->get($table);
	}
	
	function cetak_single_table($table, $fields, $where, $order_by){
		$this->db->select("$fields");
    	$this->db->where($where);
		$this->db->order_by($order_by);
		return $this->db->get($table);
	}
		
	function cetak_puskesmas(){
		$this->db->select("*");
		$this->db->limit(2);
		return $this->db->get('puskesmas');
	}
		
	function cetak_pengiriman_obat_dari_dinkes(){
		$this->db->select("*");
		$this->db->limit(2);
		return $this->db->get('pengiriman_obat_dari_dinkes');
	}
		
	function cetak_rujukan_exteral($fields, $where){
		$this->db->select("$fields");
		$this->db->join('pus_pelayanan', 'pus_pelayanan.id_pus_pelayanan=rujukan_external.id_pus_pelayanan', 'left');
		$this->db->join('pus_pendaftaran', 'pus_pendaftaran.id_pus_pendaftaran=pus_pelayanan.id_pus_pendaftaran', 'left');
		$this->db->join('puskesmas', 'puskesmas.id_puskesmas=pus_pendaftaran.id_puskesmas', 'left');
		$this->db->join('kecamatan', 'kecamatan.id_kecamatan=puskesmas.id_kecamatan', 'left');
    $this->db->where($where);
		return $this->db->get('rujukan_external');
	}
		
	function contoh($where, $id_puskesmas_pendaftar){
		$this->db->select("
		pus_pendaftaran.tanggal_pendaftaran,
		pus_pendaftaran.nomor_pendaftaran,
		pasien.nama_penduduk,
		(SELECT lokal_rekam_medis.no_rekam_medis FROM lokal_rekam_medis WHERE 
		lokal_rekam_medis.id_pasien=pus_pendaftaran.id_pasien and lokal_rekam_medis.id_puskesmas='".$id_puskesmas_pendaftar."') as no_rekam_medis,
		pegawai.nama_pegawai,
		(SELECT bagian_puskesmas.nama_bagian_puskesmas FROM bagian_puskesmas, pus_pelayanan WHERE 
		bagian_puskesmas.id_bagian_puskesmas=pus_pelayanan.id_bagian_puskesmas 
		and pus_pelayanan.id_pus_pendaftaran=pus_pendaftaran.id_pus_pendaftaran
		and pus_pelayanan.rujukan_dari_bagian='0'
		) as nama_bagian_puskesmas
		
		");
    $this->db->where($where);
		$this->db->join('pasien', 'pasien.id_pasien=pus_pendaftaran.id_pasien');
		$this->db->join('users', 'users.user_id=pus_pendaftaran.created_by');
		$this->db->join('pegawai', 'pegawai.nip_pegawai=users.user_name');
		
		//$this->db->join('lokal_rekam_medis', 'lokal_rekam_medis.id_puskesmas=pus_pendaftaran.id_puskesmas');
		$this->db->order_by('pus_pendaftaran.tanggal_pendaftaran');
		return $this->db->get('pus_pendaftaran');
	}
	
}