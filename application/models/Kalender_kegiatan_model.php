<?php

class Kalender_kegiatan_model extends CI_Model
{
 function fetch_all_event(){
  $this->db->order_by('id');
  return $this->db->get('kalender_kegiatan');
 }

 function insert_event($data)
 {
  $this->db->insert('kalender_kegiatan', $data);
 }

 function update_event($data, $id)
 {
  $this->db->where('id', $id);
  $this->db->update('kalender_kegiatan', $data);
 }

 function delete_event($id)
 {
  $this->db->where('id', $id);
  $this->db->delete('kalender_kegiatan');
 }
}

?>