<?php
class Pegawai_dinkes_model extends CI_Model{

  //get data from database
  function get_data($where){
       $this->db->select('msPegawaiUnitKerja,COUNT(msPegawaiUnitKerja) as total');
       $this->db->where($where);
       $this->db->group_by('msPegawaiUnitKerja'); 
 		// $this->db->order_by('total', 'desc'); 
      $result = $this->db->get('tabel_dinkes_pegawai_instansi');
      return $result;
  }


  function get_data_pendidikan($where){
       $this->db->select('msPegawaiPendidikan,COUNT(msPegawaiPendidikan) as total');
       $this->db->where($where);
       $this->db->group_by('msPegawaiPendidikan'); 
 		// $this->db->order_by('total', 'desc'); 
      $result = $this->db->get('tabel_dinkes_pegawai_instansi');
      return $result;
  }

  function get_data_pegawai_puskesmas($where){
      $this->db->select('msPegawaiJabatan,COUNT(msPegawaiJabatan) as total');
      $this->db->where($where);
      $this->db->group_by('msPegawaiJabatan'); 
      $this->db->order_by('total DESC');
      $this->db->limit(10); 
      $result = $this->db->get('tabel_dinkes_pegawai_instansi');
      return $result;
  }

}
