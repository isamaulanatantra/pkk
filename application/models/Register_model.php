<?php

class Register_model extends CI_Model {
    
	function __construct() {
        parent::__construct();
    }
  
  function simpan_register($data_input, $table_name)
	{
		$this->db->insert( $table_name, $data_input );
		return $this->db->insert_id();
	}
  function update_register($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
  
  function simpan_buat_user_register($data_input, $table_name)
  {
  $this->db->insert( $table_name, $data_input );
  return $this->db->insert_id();
  }
} 