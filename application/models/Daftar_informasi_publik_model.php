<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Daftar_informasi_publik_model extends CI_Model
{
 
	public function json_all_daftar_informasi_publik($where, $limit, $start, $fields, $order_by, $kata_kunci) {
		$this->db->select("
		$fields
		");
		if( $kata_kunci <> '' ){
		  $this->db->like('judul_posting', $kata_kunci);
		  $this->db->or_like('domain', $kata_kunci);
		}
		$this->db->join('data_skpd', 'data_skpd.skpd_website=posting.domain');
		$this->db->join('skpd', 'skpd.id_skpd=data_skpd.id_skpd');
		$this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('posting');
		return $result->result_array();
    }

  function update_data_posting($data_update, $where, $table_name){
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
	
}