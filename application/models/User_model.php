<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{
 
	public function json_all_users($where, $limit, $start, $fields, $order_by, $kata_kunci) {
		$this->db->select("
		$fields
		");
    if( $kata_kunci <> '' ){
      $this->db->like('nama', $kata_kunci);
      $this->db->or_like('user_name', $kata_kunci);
    }
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('users');
		return $result->result_array();
  }

  function total_users($where, $limit, $start, $kata_kunci) {  
    $this->db->from('users');
      if( $kata_kunci <> '' ){
        $this->db->like('nama', $kata_kunci);
        $this->db->or_like('user_name', $kata_kunci);
      }
    $this->db->where($where);
    return $this->db->count_all_results(); 
  }
  
}