<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Berita_model extends CI_Model
{
  
  function getData($web = null)
  {
    if($web === null){
      $query = $this->db->query("SELECT `posting`.`id_posting` FROM `posting` WHERE `posting`.`status` != 99 AND `posting`.`judul_posting` = 'berita'");
      foreach ($query->result() as $h)
        {
            $query1 = $this->db->query("SELECT `posting`.`id_posting` as id_posting, `posting`.`domain`, `posting`.`judul_posting`, `posting`.`isi_posting`, (SELECT file_name FROM attachment WHERE attachment.id_tabel=posting.id_posting limit 1) as nama_file FROM `posting` WHERE `posting`.`status` != 99 AND `posting`.`parent` = '$h->id_posting' AND `posting`.`domain` = '$web' ORDER BY `posting`.`created_time` DESC");
            foreach ($query1->result() as $h1)
            {
                $query2 = $this->db->query("SELECT `attachment`.`file_name` FROM `attachment` WHERE `attachment`.`id_tabel` = '$h1->id_posting'");
                $attachment = '';
                foreach ($query2->result() as $h2)
                {   
                    if($attachment == "")
                    {
                        $attachment = base_url().'media/upload/s_'.$h2->file_name;
                    }else{
                        $attachment = $attachment.', '.base_url().'media/upload/s_'.$h2->file_name;
                    }
                }
                  return $query1->result_array();
            }
        }
    }else{
      // return $this->db->get_where('posting', ['domain' => $web])->result_array();
      $query = $this->db->query("SELECT `posting`.`id_posting` FROM `posting` WHERE `posting`.`status` != 99 AND `posting`.`judul_posting` = 'berita' AND `posting`.`domain` = '$web'");
      foreach ($query->result() as $h)
        {
            $query1 = $this->db->query("SELECT `posting`.`id_posting` as id_posting, `posting`.`domain`, `posting`.`judul_posting`, `posting`.`isi_posting`, (SELECT file_name FROM attachment WHERE attachment.id_tabel=posting.id_posting limit 1) as nama_file FROM `posting` WHERE `posting`.`status` != 99 AND `posting`.`parent` = '$h->id_posting' AND `posting`.`domain` = '$web' ORDER BY `posting`.`created_time` DESC");
            foreach ($query1->result() as $h1)
            {
                $query2 = $this->db->query("SELECT `attachment`.`file_name` FROM `attachment` WHERE `attachment`.`id_tabel` = '$h1->id_posting'");
                $attachment = '';
                foreach ($query2->result() as $h2)
                {   
                    if($attachment == "")
                    {
                        $attachment = base_url().'media/upload/s_'.$h2->file_name;
                    }else{
                        $attachment = $attachment.', '.base_url().'media/upload/s_'.$h2->file_name;
                    }
                }
                  return $query1->result_array();
                // print_r($data);
                //echo json_encode($data);
                //echo json_encode($query1->result_array());
            }
        }
    }
  }
}