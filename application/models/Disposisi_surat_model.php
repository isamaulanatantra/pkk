<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Disposisi_surat_model extends CI_Model
{
	function json_semua_disposisi_surat($where) {	
		$this->db->from('disposisi_surat');
		$this->db->where($where);
		return $this->db->count_all_results(); 
	}
	public function json_all_disposisi_surat($where, $limit, $start, $fields, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('disposisi_surat');
		return $result->result_array();
	}
  function update_disposisi_surat($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
}