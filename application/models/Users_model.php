<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model
{
  
function opsi($where, $table_name, $fields, $order_by)
  {
  $this->db->select("$fields");
  $this->db->where($where);
  $this->db->order_by($order_by);
  return $this->db->get($table_name);
  }
  public function json_all_users($where, $limit, $start, $fields, $order_by)
  {
  $this->db->select("
  $fields
  ");
  $this->db->join('data_skpd', 'data_skpd.id_skpd=users.id_skpd');
  $this->db->where($where);
  $this->db->order_by($order_by, 'desc');
  $this->db->limit($limit, $start);
  $result = $this->db->get('users');
  return $result->result_array();
  }
  public function json_join($where, $limit, $start, $table_name, $fields, $order_by) 
  {
  $this->db->select("
  $fields
  ");
  $this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
  $this->db->where($where);
  $this->db->order_by($order_by);
  $this->db->limit($limit, $start);
  $result = $this->db->get($table_name);
  return $result->result_array();
  }
  public function get_by_id($table_name, $where, $fields, $order_by) 
  {
  $this->db->select("$fields");
  $this->db->where($where);
  $this->db->order_by($order_by);
  $result = $this->db->get($table_name);
  return $result->result_array();
  }
  public function seperti($table_name, $where, $fields, $order_by, $like_data) 
  {
  $this->db->select("$fields");
  $this->db->where($where);
  $this->db->like($like_data);
  $this->db->order_by($order_by);
  $result = $this->db->get($table_name);
  return $result->result_array();
  }
  function check_before_save($table_name, $where)
  {
  $this->db->select('*');
  $this->db->where($where);
  $result = $this->db->get($table_name);
  return $result->result_array();
  }
  function simpan_users($data_input, $table_name)
  {
  $this->db->insert( $table_name, $data_input );
  return $this->db->insert_id();
  }
  function update_data_users($data_update, $where, $table_name)
  {
  $this->db->where($where);
  $this->db->update($table_name, $data_update);
  }
  function json_semua_users($where) 
  {  
  $this->db->from('users');
  $this->db->where($where);
  $this->db->join('data_skpd', 'data_skpd.id_skpd=users.id_skpd');
  return $this->db->count_all_results(); 
  }  
  function like_data($where, $table_name, $like_data) 
  {  
  $this->db->from($table_name);
  $this->db->where($where);
  $this->db->like($like_data);
  return $this->db->count_all_results(); 
  }  
  function count_all_search_users($where, $key_word, $table_name, $field) {  
  $this->db->from($table_name);
  $this->db->where($where);
  $this->db->like($field, $key_word);
  return $this->db->count_all_results(); 
  }
  public function search_users($fields, $where, $limit, $start, $key_word, $order_by) 
  {
  $this->db->select("
  $fields
  ");
  $this->db->where($where);
  $this->db->like('nama', $key_word);
  $this->db->order_by($order_by);
  $this->db->limit($limit, $start);
  $result = $this->db->get('users');
  return $result->result_array();
  }  
  function auto_suggest($q, $where, $fields) 
  {
  $this->db->select("$fields");
  //$this->db->where($where);
  $this->db->like('users.nama', $q);
  $this->db->limit('20');
  $result = $this->db->get('users');
  return $result->result_array();
  }
}