<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model
{
  
  public function getDatamenu($website)
  {
      $parent = 0;
      $result = $this->db->query("
      SELECT posting.judul_posting, posting.id_posting from posting
                    
      where posisi='menu_atas'
      and parent='".$parent."' 
      and status = 1 
      and domain='".$website."'
      order by urut
      ");
      $x = $result->num_rows();
      $a = 1;
      $data = "[";
      $data .= '
        {
            "nama_menu": "Home",
            "url": "'.base_url().'"
        },';
      foreach($result->result() as $h)
      {
        $row1 = $this->db->query("
        SELECT posting.judul_posting, posting.id_posting from posting
        where parent='".$h->id_posting."' 
        and status = 1 
        and tampil_menu = 1 
        and domain='".$website."'
        order by urut
        ");
        $xx = $row1->num_rows();
        if ($xx == 0){
          $data .= '
            {
                "nama_menu": "'.$h->judul_posting.'",
                "url": "'.$this->geturl($h->id_posting,$h->judul_posting,$xx).'"
            }';
          if($a < $x){
            $data .= ',';
          }
        }else{
          $data .= '
          {
              "nama_menu": "'.$h->judul_posting.'",
              "sub_menu": 
          [';
          $b = 1;
          foreach($row1->result() as $i)
          {
            $row2 = $this->db->query("
            SELECT posting.judul_posting, posting.id_posting from posting
            where parent='".$i->id_posting."' 
            and status = 1 
            and tampil_menu = 1 
            and domain='".$website."'
            order by urut
            ");
            $xxx = $row2->num_rows();
            if ($xxx == 0){
              $data .= '
              {
                  "sub_menu": "'.$i->judul_posting.'",
                  "url": "'.$this->geturl($i->id_posting,$i->judul_posting,1).'"
              }';
              if($b < $xx){
                $data .= ',';
              }
            }else{
              $data .= '
              {
                  "nama_menu": "'.$i->judul_posting.'",
                  "sub_sub_menu": 
              [';
              $c = 1;
              foreach($row2->result() as $j)
              {
                $data .= '
                {
                    "sub_sub_menu": "'.$j->judul_posting.'",
                    "url": "'.$this->geturl($j->id_posting,$j->judul_posting,1).'"
                }';
                if($c < $xxx){
                  $data .= ',';
                }
                $c++;
              }
              $data .= ']}';
              if($b < $xx){
                $data .= ',';
              }
            }
            $b++;
          }
          $data .= ']}';
          if($a < $x){
            $data .= ',';
          }
        }
        $a++;
      }
      $data .= ']';
      $data = json_decode($data);
      return $data;
  }

  public function geturl($id_posting, $judul_posting,$num_rows){
    if($num_rows == 0){
      $url = ''.base_url().'postings/categoris/'.$id_posting.'/'.str_replace(' ', '_', $judul_posting ).'.HTML';
    }else{
      if($judul_posting == 'Pengaduan Masyarakat'){
        $url = ''.base_url().'pengaduan_masyarakat';
      }
      elseif($judul_posting == 'Permohonan Informasi'){
        $url = ''.base_url().'permohonan_informasi_publik';
      }
      elseif($judul_posting == 'Pengajuan Keberatan'){
        $url = ''.base_url().'pengajuan_keberatan';
      }
      else{
        $url = ''.base_url().'postings/details/'.$id_posting.'/'.str_replace(' ', '_', $judul_posting ).'.HTML';
      }
    }
    return $url;
  }

  public function getData($id = null)
  {
    if($id === null){
      // return $this->db->get('data_desa')->result_array();
      $this->db->select('data_desa.desa_website, desa.kode_desa, desa.kode_kecamatan, desa.kode_kabupaten, desa.kode_propinsi, data_desa.jenis_pemerintahan');
      $this->db->join('dasar_website', 'dasar_website.domain=data_desa.desa_website');
      $this->db->join('desa', 'desa.id_desa=data_desa.id_desa');
      $where      = array(
        'data_desa.status' => 1,
        'dasar_website.status' => 1,
        'desa.id_kabupaten' => 1,
        'desa.id_propinsi' => 1
      );
      $this->db->where($where);
      $result = $this->db->get('data_desa');
      return $result->result_array();
    }else{
      return $this->db->get_where('data_desa', ['id_data_desa' => $id])->result_array();
      
    }    
  }

  public function createData($data)
  {
    $this->db->insert('data_desa', $data);
    return $this->db->affected_rows();
  }

  public function updateData($data,$id)
  {
    $this->db->insert('data_desa', $data, ['id' => $id]);
    return $this->db->affected_rows();
  }

  public function deleteData($id)
  {
    $this->db->delete('data_desa', ['id' => $id]);
    return $this->db->affected_rows();
  }

}