<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Anggota_organisasi_model extends CI_Model
{
	function json_semua_anggota_organisasi($where) {	
		$this->db->from('anggota_organisasi');
		$this->db->where($where);
		return $this->db->count_all_results(); 
	}
	public function json_all_anggota_organisasi($where, $limit, $start, $fields, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('anggota_organisasi');
		return $result->result_array();
	}
  function update_data_anggota_organisasi($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
}