<?php

class Login_model extends CI_Model {
    
	function __construct() {
        parent::__construct();
    }
  
  function update_last_login($data_update, $where)
	{
		$this->db->where($where);
		$this->db->update('users', $data_update);
	}
	
  function get_login($where = array())
	{
		$this->db->select('
    users.id_users,
    users.user_name,
    users.password,
    users.hak_akses,
    users.nip,
    users.nama,
    users.jabatan,
    users.golongan,
    users.pangkat,
    users.id_skpd,
    users.id_desa,
    users.id_kecamatan,
    users.id_kabupaten,
    users.id_propinsi,
    users.created_by,
    users.created_time,
    users.updated_by,
    users.updated_time,
    users.deleted_by,
    users.deleted_time,
    users.status,
    users.last_login,
    
    data_skpd.id_data_skpd,
    data_skpd.jabatan_penandatangan,
    data_skpd.nama_penandatangan,
    data_skpd.pangkat_penandatangan,
    data_skpd.nip_penandatangan,
    data_skpd.skpd_nama,
    data_skpd.skpd_telpn,
    data_skpd.skpd_alamat,
    data_skpd.skpd_fax,
    data_skpd.skpd_kode_pos,
    data_skpd.skpd_nomor,
    data_skpd.skpd_website,
    data_skpd.skpd_email,
    data_skpd.id_skpd,
    data_skpd.status,
    ');
    $this->db->from('users');
    $this->db->where($where);
    $this->db->join('data_skpd', 'data_skpd.id_skpd = users.id_skpd');
		return $this->db->get()->row_array();
	}
	
  function get_login_users($where = array())
	{
		$this->db->select('
			users.id_users,
			users.user_name,
			users.email,
			users.nama,
			users.hak_akses,
			users.id_puskesmas,
			users.id_lokasi_pemeriksaan,
			users.id_puskesmas_bagian,
			users.status,
			attachment.nama_file as nama_foto,
			attachment.id_attachment as id_foto,
			');
		$this->db->join('attachment', 'attachment.id_attachment = users.id_attachment', 'left');
    $this->db->from('users');
    $this->db->where($where);
		return $this->db->get()->row_array();
	}
  
} 