<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Skpda_model extends CI_Model{
	function opsi($table_name, $where, $fields, $order_by){
		$this->db->select($fields);
		$this->db->where($where);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
	}
	public function json($where, $limit, $start, $table_name, $fields, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
	public function html_all($table, $where, $limit, $start, $fields, $orderby, $keyword){
    $this->db->select("$fields");
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $this->db->where($where);
    $this->db->order_by($orderby);
    $this->db->limit($limit, $start);
    return $this->db->get($table);
    }
	public function get_by_id($table_name, $where, $fields, $order_by) {
		$this->db->select("$fields");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
  function save_data($data_input, $table_name){
		$this->db->insert( $table_name, $data_input );
		return $this->db->insert_id();
	}
  function update_data($data_update, $where, $table_name){
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
}