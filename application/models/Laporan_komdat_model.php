<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan_komdat_model extends CI_Model
{ 
		
 	function get_data($where = array()){
		$this->db->select('*');
		$this->db->from('tr_komdat');
		$this->db->leftjoin('ms_jenis_komdat','msJenisKomdatId = trKomdatMsJenisId');
		$this->db->leftjoin('ms_komdat','msKomdatId = trKomdatMsKomdatId');
		$this->db->where($where);
		return $this->db->get()->row_array();
	}

	public function save_data($data){
	    return $this->db->insert_batch('tr_komdat', $data);
	}
	
}