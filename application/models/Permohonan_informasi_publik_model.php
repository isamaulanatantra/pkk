<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Permohonan_informasi_publik_model extends CI_Model
{

	public function html_all_permohonan_informasi_publik($table, $where, $limit, $start, $fields, $orderby, $keyword, $tahun){
    $this->db->select("$fields");
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $this->db->like('permohonan_informasi_publik.created_time', $tahun);
    $this->db->where($where);
    $this->db->order_by($orderby, 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get($table);
	}
	public function html_all($table, $where, $limit, $start, $fields, $orderby, $keyword){
    $this->db->select("$fields");
    if($keyword <> ''){
      $this->db->like($orderby, $keyword);
    }
    $this->db->where($where);
    $this->db->order_by($orderby, 'DESC');
    $this->db->limit($limit, $start);
    return $this->db->get($table);
	}  
  function get_modul($wheres = array()){
		$this->db->select('*');
    $this->db->from('modul');
    $this->db->where($wheres);
		return $this->db->get()->row_array();
	}
  
  function update_permohonan_informasi_publik($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
  function get_data($where = array()){
	$this->db->select('
    permohonan_informasi_publik.id_permohonan_informasi_publik,
    permohonan_informasi_publik.domain,
    permohonan_informasi_publik.parent,
    permohonan_informasi_publik.id_posting,
    permohonan_informasi_publik.nama,
    permohonan_informasi_publik.alamat,
    permohonan_informasi_publik.pekerjaan,
    permohonan_informasi_publik.nomor_telp,
    permohonan_informasi_publik.email,
    permohonan_informasi_publik.rincian_informasi_yang_diinginkan,
    permohonan_informasi_publik.tujuan_penggunaan_informasi,
    permohonan_informasi_publik.cara_melihat_membaca_mendengarkan_mencatat,
    permohonan_informasi_publik.cara_hardcopy_softcopy,
    permohonan_informasi_publik.cara_mengambil_langsung,
    permohonan_informasi_publik.cara_kurir,
    permohonan_informasi_publik.cara_pos,
    permohonan_informasi_publik.cara_faksimili,
    permohonan_informasi_publik.cara_email,
    permohonan_informasi_publik.kategori_permohonan_informasi_publik,
    permohonan_informasi_publik.temp,
    permohonan_informasi_publik.created_by,
    permohonan_informasi_publik.created_time,
    permohonan_informasi_publik.updated_by,
    permohonan_informasi_publik.updated_time,
    permohonan_informasi_publik.deleted_by,
    permohonan_informasi_publik.deleted_time,
    permohonan_informasi_publik.status,
    permohonan_informasi_publik.balas,
    
    ( select (users.nama) from users where users.id_users=permohonan_informasi_publik.created_by limit 1) as pembuat,
    ( select (users.nama) from users where users.id_users=permohonan_informasi_publik.updated_by limit 1) as pengedit
    
    ');
    $this->db->from('permohonan_informasi_publik');
    $this->db->where($where);
		return $this->db->get()->row_array();
	}
  
  function get_attachment_by_id_permohonan_informasi_publik($where = array())
	{
		$this->db->select('*');
		$this->db->limit('1');
    $this->db->from('attachment');
    $this->db->where($where);
		return $this->db->get()->row_array();
	}
  
	function opsi($where, $table_name, $fields, $order_by){
		$this->db->select("$fields");
		$this->db->where($where);
		$this->db->order_by($order_by);
		return $this->db->get($table_name);
	}

	public function json_all_permohonan_informasi_publik($where, $limit, $start, $fields, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->order_by($order_by, 'DESC');
		$this->db->limit($limit, $start);
		$result = $this->db->get('permohonan_informasi_publik');
		return $result->result_array();
    }

	public function json_join($where, $limit, $start, $table_name, $fields, $order_by) {
		$this->db->select("
		$fields
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function get_by_id($table_name, $where, $fields, $order_by) {
		$this->db->select("$fields");
    $this->db->where($where);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function seperti($table_name, $where, $fields, $order_by, $like_data) {
		$this->db->select("$fields");
    $this->db->where($where);
		$this->db->like($like_data);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
	public function get_by_id_join($table_name, $where, $fields, $order_by) {
		$this->db->select("$fields");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by($order_by);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }
		
  function check_before_save($table_name, $where)
	{
		$this->db->select('*');
    	$this->db->where($where);
		$result = $this->db->get($table_name);
		return $result->result_array();
	}
	
  function simpan_permohonan_informasi_publik($data_input, $table_name)
	{
		$this->db->insert( $table_name, $data_input );
		return $this->db->insert_id();
	}
		
  function update_data_attachment($data_update_attachment, $where_attachment, $table_name_attachment)
	{
		$this->db->where($where_attachment);
		$this->db->update($table_name_attachment, $data_update_attachment);
	}
  function update_data_permohonan_informasi_publik($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
	
	function json_semua_permohonan_informasi_publik($where) {	
		$this->db->from('permohonan_informasi_publik');
		$this->db->where($where);
		return $this->db->count_all_results(); 
	}	
	
	function like_data($where, $table_name, $like_data) {	
		$this->db->from($table_name);
		$this->db->where($where);
		$this->db->like($like_data);
		return $this->db->count_all_results(); 
	}	
	
	function count_all_search_permohonan_informasi_publik($where, $key_word, $table_name, $field) {	
		$this->db->from($table_name);
		$this->db->where($where);
		$this->db->like($field, $key_word);
		return $this->db->count_all_results(); 
	}
		
	public function search_permohonan_informasi_publik($fields, $where, $limit, $start, $key_word, $order_by) {
		$this->db->select("
		$fields
		");
    $this->db->where($where);
		$this->db->like('urut_permohonan_informasi_publik', $key_word);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get('permohonan_informasi_publik');
		return $result->result_array();
    }	
		
	public function search_join($table_name, $fields, $where, $limit, $start, $field_like, $key_word, $order_by) {
		$this->db->select("
		$fields
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->like($field_like, $key_word);
		$this->db->order_by($order_by);
		$this->db->limit($limit, $start);
		$result = $this->db->get($table_name);
		return $result->result_array();
    }

	public function json_buku_kategori_buku($where) {
		$this->db->select("
		kategori_buku.kategori_buku_urut,
		buku_kategori_buku.id_buku_kategori_buku
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$result = $this->db->get('buku_kategori_buku');
		return $result->result_array();
    }

	public function json_laborat_perbagian($where) {
		$this->db->select("
		pus_pendaftaran.tanggal_pendaftaran,
		pus_pendaftaran.nomor_pendaftaran,
		bagian_puskesmas.urut_bagian_puskesmas,
		pus_pelayanan.id_pus_pelayanan,
		pasien.urut_penduduk,
		(select COUNT(*) FROM pus_pelayanan_pemeriksaan_laborat where pus_pelayanan_pemeriksaan_laborat.id_pus_pelayanan = pus_pelayanan.id_pus_pelayanan ) AS jumlah,
		(select COUNT(*) FROM pus_pelayanan_pemeriksaan_laborat where (pus_pelayanan_pemeriksaan_laborat.id_pus_pelayanan = pus_pelayanan.id_pus_pelayanan and pus_pelayanan_pemeriksaan_laborat.status = 1 ) ) AS status_pemeriksaan_lab
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by('pus_pendaftaran.nomor_pendaftaran');
		$result = $this->db->get('pus_pendaftaran');
		return $result->result_array();
    }

	public function json_obat_perbagian($where) {
		$this->db->select("
		pus_pendaftaran.tanggal_pendaftaran,
		pus_pendaftaran.nomor_pendaftaran,
		bagian_puskesmas.urut_bagian_puskesmas,
		pus_pelayanan.id_pus_pelayanan,
		pasien.urut_penduduk,
		(select COUNT(*) FROM pus_pelayanan_obat where pus_pelayanan_obat.id_pus_pelayanan = pus_pelayanan.id_pus_pelayanan and pus_pelayanan_obat.status = 1 ) AS jumlah,
		(select COUNT(*) FROM pus_pelayanan_obat where (pus_pelayanan_obat.id_pus_pelayanan = pus_pelayanan.id_pus_pelayanan and pus_pelayanan_obat.status = 1 ) ) AS status_pemberian_obat
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by('pus_pendaftaran.nomor_pendaftaran');
		$result = $this->db->get('pus_pendaftaran');
		return $result->result_array();
    }

	public function json_detail_obat_per_layanan($where) {
		$this->db->select("
		pus_pelayanan_obat.id_pus_pelayanan_obat,
		pus_pelayanan_obat.price,
		pus_pelayanan_obat.dosis_obat,
		pus_pelayanan_obat.jumlah_obat,
		pus_pelayanan_obat.status,
		obat.urut_obat
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by('pus_pelayanan_obat.id_pus_pelayanan_obat');
		$result = $this->db->get('pus_pelayanan_obat');
		return $result->result_array();
    }

	public function show_json_absensi_hari_ini($where, $table_name) {
		$this->db->select("
		absensi.id_absensi,
		absensi.id_permohonan_informasi_publik,
		absensi.hari_kerja,
		absensi.jam_absen,
		absensi.login_dari,
		absensi.jam_pulang,
		absensi.logout_dari,
		pegawai.urut_pegawai,
		puskesmas.urut_puskesmas
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by('absensi.jam_absen');
		$result = $this->db->get('absensi');
		return $result->result_array();
    }

	public function show_json_absensi_hari_ini_dinas($where, $table_name) {
		$this->db->select("
		absensi.id_absensi,
		absensi.id_permohonan_informasi_publik,
		absensi.hari_kerja,
		absensi.jam_absen,
		absensi.login_dari,
		absensi.jam_pulang,
		absensi.logout_dari,
		pegawai.urut_pegawai
		");
		$this->db->join('pekerjaan', 'pekerjaan.id_pekerjaan=anggota_perpus.id_pekerjaan');
    $this->db->where($where);
		$this->db->order_by('absensi.jam_absen');
		$result = $this->db->get('absensi');
		return $result->result_array();
    }
	function auto_suggest($q, $where, $fields) {
		$this->db->select("$fields");
		$this->db->where($where);
		$this->db->like('permohonan_informasi_publik.urut_permohonan_informasi_publik', $q);
		$this->db->or_like('permohonan_informasi_publik.judul_permohonan_informasi_publik', $q);
		$this->db->limit('20');
		$result = $this->db->get('permohonan_informasi_publik');
		return $result->result_array();
	}
	function TanggalBahasaIndo($date){
		$bulan=array(
				'00'=>'',
				'01'=>'Jan',
				'02'=>'Feb',
				'03'=>'Mar',
				'04'=>'Apr',
				'05'=>'Mei',
				'06'=>'Jun',
				'07'=>'Jul',
				'08'=>'Agu',
				'09'=>'Sep',
				'10'=>'Okt',
				'11'=>'Nov',
				'12'=>'Des',
			);
			$pecah=explode('-',$date);
			$sisa1=$pecah[2];
			$bln=$pecah[1];
			$thn=$pecah[0];

			$pecah=explode(' ',$sisa1);
			$tanggal=$pecah[0];

			return $tanggal.' '.$bulan[$bln].' '.$thn;
	}
	function TanggalLengkapBahasaIndo($date){
		$bulan=array(
				'00'=>'',
				'01'=>'Januari',
				'02'=>'Februari',
				'03'=>'Maret',
				'04'=>'April',
				'05'=>'Mei',
				'06'=>'Juni',
				'07'=>'Juli',
				'08'=>'Agustus',
				'09'=>'September',
				'10'=>'Oktober',
				'11'=>'November',
				'12'=>'Desember',
			);
			$pecah=explode('-',$date);
			$sisa1=$pecah[2];
			$bln=$pecah[1];
			$thn=$pecah[0];

			$pecah=explode(' ',$sisa1);
			$tanggal=$pecah[0];

			return $tanggal.' '.$bulan[$bln].' '.$thn;
	}

	public function filter($search, $limit, $start, $order_field, $order_ascdesc){
    $fields     = "*";
    $where = array(
        'permohonan_informasi_publik.status !=' => 99,
		'permohonan_informasi_publik.parent' => 0,
        'permohonan_informasi_publik.kategori_permohonan_informasi_publik' => 'Permohonan Informasi Publik'
    );    
    $this->db->select("$fields");
    $this->db->where($where);
	$this->db->where("(`permohonan_informasi_publik`.`nama` LIKE '%$search%' ESCAPE '!' OR `permohonan_informasi_publik`.`domain` LIKE '%$search%' ESCAPE '!' OR `permohonan_informasi_publik`.`nomor_telp` LIKE '%$search%' ESCAPE '!' OR `permohonan_informasi_publik`.`created_time` LIKE '%$search%' ESCAPE '!' OR `permohonan_informasi_publik`.`rincian_informasi_yang_diinginkan` LIKE '%$search%' ESCAPE '!')"); // Untuk menambahkan query where LIKE
	$this->db->order_by($order_field, $order_ascdesc); // Untuk menambahkan query ORDER BY
	$this->db->limit($limit, $start); // Untuk menambahkan query LIMIT

	return $this->db->get('permohonan_informasi_publik')->result_array(); // Eksekusi query sql sesuai kondisi diatas
	}

	public function count_all(){
    $where = array(
      'permohonan_informasi_publik.status !=' => 99,
      'permohonan_informasi_publik.parent' => 0,
	  'permohonan_informasi_publik.kategori_permohonan_informasi_publik' => 'Permohonan Informasi Publik'
      );
    $this->db->where($where);
		return $this->db->count_all('permohonan_informasi_publik'); // Untuk menghitung semua data siswa
	}

	public function count_filter($search){
    $fields     = "*";
    $where = array(
        'permohonan_informasi_publik.status !=' => 99,
		'permohonan_informasi_publik.parent' => 0,
        'permohonan_informasi_publik.kategori_permohonan_informasi_publik' => 'Permohonan Informasi Publik'
    );    
    $this->db->select("$fields");
    $this->db->where($where);
	$this->db->where("(`permohonan_informasi_publik`.`nama` LIKE '%$search%' ESCAPE '!' OR `permohonan_informasi_publik`.`domain` LIKE '%$search%' ESCAPE '!' OR `permohonan_informasi_publik`.`nomor_telp` LIKE '%$search%' ESCAPE '!' OR `permohonan_informasi_publik`.`created_time` LIKE '%$search%' ESCAPE '!' OR `permohonan_informasi_publik`.`rincian_informasi_yang_diinginkan` LIKE '%$search%' ESCAPE '!')"); // Untuk menambahkan query where LIKE

	return $this->db->get('permohonan_informasi_publik')->num_rows(); // Untuk menghitung jumlah data sesuai dengan filter pada textbox pencarian
	}
	
}