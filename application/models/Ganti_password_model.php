<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ganti_password_model extends CI_Model
{

  function get_login($where = array())
	{
		$this->db->select('*');
    $this->db->from('users');
    $this->db->where($where);
		return $this->db->get()->row_array();
	}
	
  function ganti_password($change_passsword_edit, $where_id_users)
	{
		$this->db->where($where_id_users);
		$this->db->update('users', $change_passsword_edit);
	}
	
	
}