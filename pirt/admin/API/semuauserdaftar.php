<?php
	require_once '../API/koneksi.php';
	//require_once '../include/DB_Functions.php';

	// if (isset($_POST['kode']) && isset($_POST['lat']) && isset($_POST['lng'])) {
	if (isset($_POST['id_puskesmas']) && isset($_POST['status']) ) {
			$id_puskesmas = $_POST['id_puskesmas'];
			$status = $_POST['status'];
			
			$query = $db->query("SELECT * FROM msuser WHERE status = '$status' AND msUserIdPuskesmas = '$id_puskesmas' ORDER BY created_by ASC");	
			$query->execute();
			$ray = array();			
			
			if ($query->rowCount() == 0) {
		        $responseJson["error"] = true;
		        $responseJson["message"] = "Belum ada data";
		    } else {
		        $responseJson["error"] = false;
		        $responseJson["message"] = "Berhasil mendapatkan data";
		        while($data = $query->fetch(PDO::FETCH_LAZY)){		        	
		        $responseJson['semuauserdaftar'][] = array(
		            'msUserId' => $data['msUserId'],
                    'msUserNik' => $data['msUserNik'],
                    'msUserNoHp' => $data['msUserNoHp'],
                    'msUserNama' => $data['msUserNama'],
                    'msUserFile' => $data['msUserFile'],
		            );
		        }
		    }

		    echo json_encode($responseJson, JSON_PRETTY_PRINT);

	}else{
		$responseJson["error"] = true;
		$responseJson["message"] = "Tidak ada data";
		echo json_encode($responseJson, JSON_PRETTY_PRINT);
	}	
		
?>