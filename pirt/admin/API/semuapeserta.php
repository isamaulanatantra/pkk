<?php
	require_once '../API/koneksi.php';
	//require_once '../include/DB_Functions.php';

	// if (isset($_POST['kode']) && isset($_POST['lat']) && isset($_POST['lng'])) {
	if (isset($_POST['id_puskesmas']) && isset($_POST['status']) ) {
			$id_puskesmas = $_POST['id_puskesmas'];
			$status = $_POST['status'];
			
			$query = $db->query("SELECT * FROM mspeserta WHERE status = '$status' AND msPesertaIdPuskesmas = '$id_puskesmas' ORDER BY created_by ASC");	
			$query->execute();
			$ray = array();			
			
			if ($query->rowCount() == 0) {
		        $responseJson["error"] = true;
		        $responseJson["message"] = "Belum ada data";
		    } else {
		        $responseJson["error"] = false;
		        $responseJson["message"] = "Berhasil mendapatkan data";
		        while($data = $query->fetch(PDO::FETCH_LAZY)){		        	
		        $responseJson['semuaupeserta'][] = array(
		            'msPesertaId' => $data['msPesertaId'],
                    'msPesertaNik' => $data['msPesertaNik'],
                    'msPesertaNoHp' => $data['msPesertaNoHp'],
                    'msPesertaNamaPerusahaan' => $data['msPesertaNamaPerusahaan'],
                    'msPesertaPemilik' => $data['msPesertaPemilik'],
                    'msPesertaPenanggungJawab' => $data['msPesertaPenanggungJawab'],
                    'msPesertaAlamat' => $data['msPesertaAlamat'],
                    'msPesertaKtp' => $data['msPesertaKtp'],
		            );
		        }
		    }

		    echo json_encode($responseJson, JSON_PRETTY_PRINT);

	}else{
		$responseJson["error"] = true;
		$responseJson["message"] = "Tidak ada data";
		echo json_encode($responseJson, JSON_PRETTY_PRINT);
	}	
		
?>