<?php
//require_once '../API/include/DB_Functions.php';
require_once 'DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['username']) && isset($_POST['sandi'])) {
 
    // menerima parameter POST ( email dan password )
    $username = $_POST['username'];
    $sandi = $_POST['sandi'];
 
    // get the user by email and password
    // get user berdasarkan email dan password
    $user = $db->getAdmin($username, $sandi);
 
    if ($user != false) {
        // user ditemukan
        $response["error"] = FALSE;
        $response["admin"]["id"] = $user["msAdminId"];        
        $response["admin"]["nama"] = $user["msAdminNama"];
        $response["admin"]["username"] = $user["msAdminUsername"];
        $response["admin"]["sandi2"] = $user["msAdminSandiDua"];
        $response["admin"]["foto"] = $user["msAdminFoto"];
        $response["admin"]["id_puskesmas"] = $user["msAdminIdPuskesmas"];
        echo json_encode($response, JSON_PRETTY_PRINT);
    } else {
        // user tidak ditemukan password/email salah
        $response["error"] = TRUE;
        $response["error_msg"] = "Login gagal. Username atau password salah sob";
        echo json_encode($response,  JSON_PRETTY_PRINT);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Data tidak di temukan sob";
    echo json_encode($response,  JSON_PRETTY_PRINT);
}
?>