<?php
 
class DB_Functions {
 
    private $conn;
 
    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // koneksi ke database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }
 
    // destructor
    function __destruct() {
         
    } 
    
    public function getPasienByNikAndNoHp($nik, $no_hp) {
 
       $stmt = $this->conn->prepare("SELECT * FROM msuser WHERE msUserNik = '$nik' AND msUserNoHp='$no_hp' AND status ='2' ");
 
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            
            return $user;
            
        } else {
            return NULL;
        }
    }

    public function getAdmin($username, $sandi) {
 
       $stmt = $this->conn->prepare("SELECT * FROM msadmin WHERE msAdminUsername = '$username' AND msAdminSandiDua='$sandi'");
 
        if ($stmt->execute()) {
            $admin = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            
            return $admin;
            
        } else {
            return NULL;
        }
    }

    
}
 
?>