<?php
Class Cetakdaftarhadirpenyuluhan extends AUTH_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->library('ciqrcode');   
        $this->load->library('session');
        $this->load->model('Penyuluhan_model');   

    }
    
    function index(){
        $id_jadwal=!empty($_GET['id_jadwal'])?$_GET['id_jadwal']:0;

        $fpdf = new FPDF('P','cm','legal');  
        $fpdf->SetMargins(13.175, 3.175, 3.175, 3.175);
        $fpdf->SetAutoPageBreak(false);      
        $fpdf->AddPage();        

        $fpdf -> Image(base_url().'assets/img/logowsb.jpg',1.3,1.5,2.0,2.2);
        // $fpdf -> Image(base_url().'assets/img/kemenkes.jpg',17.2,0.2,2.2,3.0);
        
        $fpdf -> Sety(1.5);
        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','B',12);
        $fpdf -> Cell(20,0.5,'PEMERINTAH KABUPATEN WONOSOBO','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','B',14);
        $fpdf -> Cell(20,0.5,'DINAS KESEHATAN','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Alamat : Jl. T.Jogonegoro 2-4 Telp./Faks. : (0286) 321033 / 321319','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Email : dinkes@wonosobokab.go.id','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.8);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'','B',0,'C');
        $fpdf -> Setx(0.8);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.52,'','B',0,'C');
        $fpdf -> Setx(0.8);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.53,'','B',0,'C');
        $fpdf -> Setx(0.8);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.54,'','B',0,'C');
        $fpdf -> Setx(0.8);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.55,'','B',0,'C');
        $fpdf -> Setx(0.8);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.56,'','B',0,'C');
        $fpdf -> Setx(0.8);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.65,'','B',0,'C');
        $fpdf -> ln();
        $fpdf -> ln();

        $jadwal = $this->Penyuluhan_model->select_jadwal_by_id($id_jadwal);
        foreach ($jadwal as $data) {
            $tanggal = $data->msJadwalPenyuluhanTgl;
        }

        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','B',12);
        $fpdf -> Cell(20,0.5,'DAFTAR HADIR PENYULUHAN KEAMANAN PANGAN','',0,'C');
        $fpdf -> ln();
        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','B',12);
        $fpdf -> Cell(20,0.5,'Tanggal '.$this->reversdate($tanggal),'',0,'C');
        $fpdf -> ln();

        

        $fpdf -> ln();
        $fpdf -> Setx(1.1);        
        $fpdf -> SetFont('Arial','B',12);
        $fpdf -> Cell(1,1.2,'No','LRBT',0,'C');
        // $fpdf -> Cell(4,1.2,'NIK','LBT',0,'C');
        $fpdf -> Cell(5.7,1.2,'Nama Perusahaan','LBT',0,'C');
        $fpdf -> Cell(5.7,1.2,'Nama Pemilik','LBT',0,'C');
        $fpdf -> Cell(7,1.2,'Tanda Tangan','LRBT',0,'C');
        $fpdf -> ln();

        $no = 1;
        $peserta = $this->Penyuluhan_model->select_peserta($id_jadwal);
        foreach ($peserta as $data) {
            $nik = $data->msPesertaNik;
            $nama_perusahaan = $data->msPesertaNamaPerusahaan;
            $nama_pemilik = $data->msPesertaPemilik;

            if($no % 2 == 0){
                $kiri = '';
                $kanan = $no.'.';
            }else{
                $kiri = $no.'.';
                $kanan = '';
            }

            $fpdf -> Setx(1.1);        
            $fpdf -> SetFont('Arial','',12);
            $fpdf -> Cell(1,0.8,$no,'LRB',0,'C');
            // $fpdf -> Cell(4,0.8,$nik,'LB',0,'');
            $fpdf -> Cell(5.7,0.8,$nama_perusahaan,'LB',0,'');
            $fpdf -> Cell(5.7,0.8,$nama_pemilik,'LB',0,'');
            $fpdf -> Cell(3.5,0.8,$kiri,'LB',0,'');
            $fpdf -> Cell(3.5,0.8,$kanan,'LRB',0,'');
            $fpdf -> ln();

            $no++;
        }
        


        $fpdf -> ln();
        $fpdf -> ln();
        $fpdf -> SetFont('Arial','',12);        
        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'Wonosobo, '.$this->reversdate($tanggal),'',0,'C');
        $fpdf -> ln();
        $fpdf -> ln(0.3);
        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'KEPALA DINAS KESEHATAN','',0,'C');
        $fpdf -> ln();
        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'KABUPATEN WONOSOBO','',0,'C');
        $fpdf -> ln();
        $fpdf -> ln();
        $fpdf -> ln();
        $fpdf -> ln();
        
        $fpdf -> Setx(11.0);
        $fpdf -> SetFont('Arial','U',12);
        $fpdf -> Cell(9.2,0.7,'JUNAEDI ,SKM, M.Kes','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(11.0);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(9.2,0.7,'Pembina Utama Muda','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'NIP. 19640601198803 1 012','',0,'C');
        $fpdf -> ln();           
        
        $fpdf->Output();
    }

    public function hari_ini($hari){

        switch($hari){
            case '0':
                $hari_ini = "Minggu";
            break;

            case '1':           
                $hari_ini = "Senin";
            break;

            case '2':
                $hari_ini = "Selasa";
            break;

            case '3':
                $hari_ini = "Rabu";
            break;

            case '4':
                $hari_ini = "Kamis";
            break;

            case '5':
                $hari_ini = "Jumat";
            break;

            case '6':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";     
            break;
        }

        return $hari_ini;

    }

    public function reversdate($tanggal){
        $a=explode('-',$tanggal);
        $d=$a[2];
        $m=$a[1];
        $y=$a[0];
        $a[0]=$m+0;
        $a[1]=$d;
        $a[2]=$y;

        $nmbulan='';

        switch ( $a[0] ) {
            case 1 : $nmbulan   = 'Januari';    break;
            case 2 : $nmbulan   = 'Februari';   break;
            case 3 : $nmbulan   = 'Maret';      break;
            case 4 : $nmbulan   = 'April';      break;
            case 5 : $nmbulan   = 'Mei';        break;
            case 6 : $nmbulan   = 'Juni';       break;
            case 7 : $nmbulan   = 'Juli';       break;
            case 8 : $nmbulan   = 'Agustus';    break;
            case 9 : $nmbulan   = 'September';  break;
            case 10: $nmbulan   = 'Oktober';    break;
            case 11: $nmbulan   = 'November';   break;
            case 12: $nmbulan   = 'Desember';   break;
        }

        $res=$a[1] . ' ' . $nmbulan . ' ' . $a['2'];
        return $res;
    }
}