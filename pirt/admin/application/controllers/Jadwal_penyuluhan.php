<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_penyuluhan extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Jadwal_model');
		$this->load->library('session');
		$this->load->helper('url');
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		// $data['dataGrup'] 	= $this->Grup_model->select_all();

		$data['page'] 		= "jadwal penyuluhan";
		$data['judul'] 		= "Data Jadwal Penyuluhan";
		$data['deskripsi'] 	= "Manage Data Jadwal Penyuluhan";
		
		$this->template->views('jadwal/home', $data);		
	}

	public function tampil() {
		$data['dataJadwal'] = $this->Jadwal_model->select_all();
		$this->load->view('jadwal/list_data', $data);
	}

	public function insert() {
		$this->form_validation->set_rules('waktu_jadwal', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('tempat_jadwal', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('ket_jadwal', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('jml_peserta', 'Keterangan ', 'trim|required');		

		$tgl_jadwal = $this->input->post('tgl_jadwal');
		$waktu_jadwal = $this->input->post('waktu_jadwal');
		$ket_jadwal = $this->input->post('ket_jadwal');
		$tempat_jadwal = $this->input->post('tempat_jadwal');
		$jml_peserta = $this->input->post('jml_peserta');
		
		$user = $this->userdata->msAdminId;

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$data_input = array(
	    		'msJadwalPenyuluhanTgl' => $tgl_jadwal,
	    		'msJadwalPenyuluhanWaktu'=> $waktu_jadwal,
	    		'msJadwalPenyuluhanKet' => $ket_jadwal,
	    		'msJadwalPenyuluhanTempat' => $tempat_jadwal,
	    		'msJadwalPesertaMaks' => $jml_peserta,
				'created_by' => $user,
				'created_time' => date('Y-m-d H:i:s'),
				'status' => 1
			);

			$table_name  = 'msjadwalpenyuluhan';
			$result = $this->Jadwal_model->insert($data_input, $table_name);
			if ($result > 0) {
				echo 2;
			} else {
				echo 1;
			}
		}	
	}

	public function update() {
		$this->form_validation->set_rules('id_jadwal', 'Id ', 'trim|required');
		$this->form_validation->set_rules('waktu_jadwal', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('tgl_jadwal', 'Tanggal ', 'trim|required');
		$this->form_validation->set_rules('tempat_jadwal', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('ket_jadwal', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('jml_peserta', 'Keterangan ', 'trim|required');
				
		$id = $this->input->post('id_jadwal');
		$tgl_jadwal = $this->input->post('tgl_jadwal');
		$waktu_jadwal = $this->input->post('waktu_jadwal');
		$ket_jadwal = $this->input->post('ket_jadwal');
		$tempat_jadwal = $this->input->post('tempat_jadwal');
		$jml_peserta = $this->input->post('jml_peserta');
		
		$user = $this->userdata->msAdminId;

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{

			$data_update = array(
	    		'msJadwalPenyuluhanTgl' => $tgl_jadwal,
	    		'msJadwalPenyuluhanWaktu'=> $waktu_jadwal,
	    		'msJadwalPenyuluhanTempat' => $tempat_jadwal,
	    		'msJadwalPenyuluhanKet' => $ket_jadwal,
	    		'msJadwalPesertaMaks' => $jml_peserta,
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				'status' => 1
			);
			

			$where = array(
	    		'msJadwalPenyuluhanId' => $id
			);

			$table_name  = 'msjadwalpenyuluhan';
			$result = $this->Jadwal_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		}	
	}

	public function hapus() {
		$this->form_validation->set_rules('id_jadwal', 'Id ', 'trim|required');
				
		$id = $this->input->post('id_jadwal');

		$user = $this->userdata->msAdminId;
		
		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{			
			$data_update = array(
	    		'deleted_by' => $user,
				'deleted_time' => date('Y-m-d H:i:s'),					
				'status' => 99
			);		
			
			$where = array(
	    		'msJadwalPenyuluhanId' => $id
			);

			$table_name  = 'msjadwalpenyuluhan';
			$result = $this->Jadwal_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		}	
	}

	// //mulai peserta
	// public function tampilPeserta() {
	// 	$id_jadwal = $this->input->get('id_jadwal');
	// 	$data['dataPeserta'] = $this->Pelatihan_model->select_peserta($id_jadwal);
	// 	$this->load->view('pelatihan/list_peserta', $data);
	// }

	// public function insertPeserta() {		
	// 	$this->form_validation->set_rules('nik', 'Keterangan ', 'trim|required');
	// 	$this->form_validation->set_rules('id_jadwal_peserta', 'Keterangan ', 'trim|required');
	// 	$this->form_validation->set_rules('nama_perusahaan', 'Keterangan ', 'trim|required');
	// 	$this->form_validation->set_rules('nama_pemilik', 'Keterangan ', 'trim|required');
	// 	$this->form_validation->set_rules('nama_penanggung_jawab', 'Keterangan ', 'trim|required');
	// 	$this->form_validation->set_rules('alamat_penanggung_jawab', 'Keterangan ', 'trim|required');		
	// 	$this->form_validation->set_rules('no_telp', 'Keterangan ', 'trim|required');		

	// 	$nik = $this->input->post('nik');
	// 	$id_jadwal_peserta = $this->input->post('id_jadwal_peserta');
	// 	$nama_perusahaan = $this->input->post('nama_perusahaan');
	// 	$nama_pemilik = $this->input->post('nama_pemilik');
	// 	$nama_penanggung_jawab = $this->input->post('nama_penanggung_jawab');
	// 	$alamat_penanggung_jawab = $this->input->post('alamat_penanggung_jawab');
	// 	$no_telp = $this->input->post('no_telp');

	// 	$now = date('Y-m-d');
	// 	$a = explode('-', $now);
	// 	$yy = $a[0];


	// 	$geturut = $this->Pelatihan_model->no_urut($yy);	
	// 	$no_urut = $geturut+1;
		
	// 	$user = $this->userdata->msAdminId;

	// 	if ($this->form_validation->run() == FALSE){
	// 		echo 0;		
	// 	}else{
	// 		$data_input = array(

	//     		'msPesertaIdJadwal' => $id_jadwal_peserta,
	//     		'msPesertaNik' => $nik,
	//     		'msPesertaNamaPerusahaan' => $nama_perusahaan,
	//     		'msPesertaPemilik' => $nama_pemilik,
	//     		'msPesertaPenanggungJawab' => $nama_penanggung_jawab,
	//     		'msPesertaAlamat' => $alamat_penanggung_jawab,
	//     		'msPesertaNoHp' => $no_telp,
	//     		'msPesertaTahun' => $yy,
	//     		'msPesertaNoUrut' => $no_urut,
	// 			'created_by' => $user,
	// 			'created_time' => date('Y-m-d H:i:s'),
	// 			'status' => 1
	// 		);

	// 		$table_name  = 'mspeserta';
	// 		$result = $this->Pelatihan_model->insert($data_input, $table_name);
	// 		if ($result > 0) {
	// 			echo 2;
	// 		} else {
	// 			echo 1;
	// 		}
	// 	}	
	// }

	// public function verifikasi() {
	// 	$this->form_validation->set_rules('id_peserta', 'Keterangan ', 'trim|required');
	// 	$this->form_validation->set_rules('nik', 'Keterangan ', 'trim|required');
	// 	$this->form_validation->set_rules('id_jadwal_peserta', 'Keterangan ', 'trim|required');
	// 	$this->form_validation->set_rules('nama_perusahaan', 'Keterangan ', 'trim|required');
	// 	$this->form_validation->set_rules('nama_pemilik', 'Keterangan ', 'trim|required');
	// 	$this->form_validation->set_rules('nama_penanggung_jawab', 'Keterangan ', 'trim|required');
	// 	$this->form_validation->set_rules('alamat_penanggung_jawab', 'Keterangan ', 'trim|required');		
	// 	$this->form_validation->set_rules('no_telp', 'Keterangan ', 'trim|required');
				
	// 	$id_peserta = $this->input->post('id_peserta');
	// 	$nik = $this->input->post('nik');
	// 	$id_jadwal_peserta = $this->input->post('id_jadwal_peserta');
	// 	$nama_perusahaan = $this->input->post('nama_perusahaan');
	// 	$nama_pemilik = $this->input->post('nama_pemilik');
	// 	$nama_penanggung_jawab = $this->input->post('nama_penanggung_jawab');
	// 	$alamat_penanggung_jawab = $this->input->post('alamat_penanggung_jawab');
	// 	$no_telp = $this->input->post('no_telp');
		
	// 	$user = $this->userdata->msAdminId;

	// 	if ($this->form_validation->run() == FALSE){
	// 		echo 0;		
	// 	}else{

	// 		$data_update = array(
	//     		'msPesertaIdJadwal' => $id_jadwal_peserta,
	//     		'msPesertaNik' => $nik,
	//     		'msPesertaNamaPerusahaan' => $nama_perusahaan,
	//     		'msPesertaPemilik' => $nama_pemilik,
	//     		'msPesertaPenanggungJawab' => $nama_penanggung_jawab,
	//     		'msPesertaAlamat' => $alamat_penanggung_jawab,
	//     		'msPesertaNoHp' => $no_telp,
	// 			'updated_by' => $user,
	// 			'updated_time' => date('Y-m-d H:i:s'),
	// 			'status' => 1
	// 		);
			

	// 		$where = array(
	//     		'msPesertaId' => $id_peserta
	// 		);

	// 		$table_name  = 'mspeserta';
	// 		$result = $this->Pelatihan_model->update($data_update, $where, $table_name);
	// 		if ($result > 0) {
	// 			echo 1;
	// 		} else {
	// 			echo 2;
	// 		}
	// 	}	
	// }

	// public function hapusPeserta() {
	// 	$this->form_validation->set_rules('id_peserta', 'Id ', 'trim|required');
				
	// 	$id_peserta = $this->input->post('id_peserta');

	// 	$user = $this->userdata->msAdminId;
		
	// 	if ($this->form_validation->run() == FALSE){
	// 		echo 0;		
	// 	}else{			
	// 		$data_update = array(
	//     		'deleted_by' => $user,
	// 			'deleted_time' => date('Y-m-d H:i:s'),					
	// 			'status' => 99
	// 		);		
			
	// 		$where = array(
	//     		'msPesertaId' => $id_peserta
	// 		);

	// 		$table_name  = 'mspeserta';
	// 		$result = $this->Pelatihan_model->update($data_update, $where, $table_name);
	// 		if ($result > 0) {
	// 			echo 1;
	// 		} else {
	// 			echo 2;
	// 		}
	// 	}	
	// }
}
