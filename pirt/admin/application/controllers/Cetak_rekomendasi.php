<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cetak_rekomendasi extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Ijin_model');
		$this->load->library('session');
		$this->load->helper('url');
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$data['dataKemasan'] 	= $this->Ijin_model->select_all_kemasan();
		$data['dataPangan'] 	= $this->Ijin_model->select_all_pangan();
		// $data['dataPuskesmas'] 	= $this->Ijin_model->select_all_puskesmas();

		$data['page'] 		= "pendaftaran SPP-IRT";
		$data['judul'] 		= "Data Pendaftaran SPP-IRT";
		$data['deskripsi'] 	= "Manage Data Pendaftaran SPP-IRT";
		
		$this->template->views('rekomendasi/home', $data);		
	}

	public function tampilPendaftaran() {
		$data['dataPendaftaran'] = $this->Ijin_model->select_sppirt_sudah_lengkap();
		$this->load->view('rekomendasi/list_data', $data);
	}

	
}