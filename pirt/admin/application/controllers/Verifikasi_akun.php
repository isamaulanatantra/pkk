<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi_akun extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Akun_model');
		$this->load->library('session');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Jakarta'); 
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		// $data['dataGrup'] 	= $this->Grup_model->select_all();

		$data['page'] 		= "menunggu verivikasi";
		$data['judul'] 		= "Menunggu Verifikasi";
		$data['deskripsi'] 	= "Manage Data Menunggu Verifikasi";		
		$this->template->views('verifikasi_akun/home', $data);
	}
	public function tampilPeserta() {
		$id_puskesmas = $this->userdata->msAdminIdPuskesmas;	
		$data['dataAkun'] = $this->Akun_model->select_belum_verifikasi($id_puskesmas);		
		$this->load->view('verifikasi_akun/list_data', $data);
	}

	public function verifikasi() {
		$this->form_validation->set_rules('id_peserta', 'Keterangan ', 'trim|required');		

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$id_peserta = $this->input->post('id_peserta');
			$user = $this->userdata->msAdminId;		
			
			$data_update = array(	    		
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				'status' => 2
			);
			

			$where = array(
	    		'msUserId' => $id_peserta
			);

			$table_name  = 'msuser';
			$result = $this->Akun_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}				
		}
	}

	public function tidakVerifikasi() {
		$this->form_validation->set_rules('id_peserta', 'Keterangan ', 'trim|required');		

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$id_peserta = $this->input->post('id_peserta');
			$user = $this->userdata->msAdminId;		
			
			$data_update = array(	    		
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				'status' => 10
			);
			

			$where = array(
	    		'msUserId' => $id_peserta
			);

			$table_name  = 'msuser';
			$result = $this->Akun_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}				
		}
	}

	
}
