<?php
Class Cetaksertifikat extends AUTH_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->library('ciqrcode');   
        $this->load->library('session');
        date_default_timezone_set('Asia/Jakarta'); 
    }
    
    function index(){
        $id_peserta=!empty($_GET['id_peserta'])?$_GET['id_peserta']:0;

        $fpdf = new FPDF('P','cm','legal');  
        $fpdf->SetMargins(13.175, 3.175, 3.175, 3.175);
        $fpdf->SetAutoPageBreak(false);      
        $fpdf->AddPage();        

        $fpdf -> Image(base_url().'assets/img/logowsb.jpg',1.3,1.5,2.0,2.2);
        // $fpdf -> Image(base_url().'assets/img/kemenkes.jpg',17.2,0.2,2.2,3.0);
        
        $fpdf -> Sety(1.5);
        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','B',12);
        $fpdf -> Cell(20,0.5,'PEMERINTAH KABUPATEN WONOSOBO','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','B',14);
        $fpdf -> Cell(20,0.5,'DINAS KESEHATAN','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Alamat : Jl. T.Jogonegoro 2-4 Telp./Faks. : (0286) 321033 / 321319','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Email : dinkes@wonosobokab.go.id','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Wonosobo-56311','',0,'C');        
        $fpdf -> ln();
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.0);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.01);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.02);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.03);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.04);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.05);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.06);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.07);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.1);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');

        $fpdf -> Sety(4.17);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> ln();

        $fpdf -> ln();
        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','B',18);
        $fpdf -> Cell(20,0.5,'SERTIFIKAT','',0,'C');
        $fpdf -> ln();
        $fpdf -> ln();

        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','BU',14);
        $fpdf -> Cell(20,0.5,'PENYULUHAN KEAMANAN PANGAN','',0,'C');
        $fpdf -> ln();
        $fpdf -> ln(0.3);        

        


        $w = $this->db->query("SELECT msPesertaNamaPerusahaan, msPesertaPemilik,    
                                    msPesertaPenanggungJawab,msPesertaAlamat, msJadwalPenyuluhanTgl,  
                                    msJadwalPenyuluhanWaktu, msJadwalPenyuluhanTempat, msPesertaNoUrut,  
                                    msPesertaTahun
                                from mspeserta 
                                LEFT JOIN msjadwalpenyuluhan ON msJadwalPenyuluhanId = msPesertaIdJadwal
                                where mspeserta.status != 99 
                                and msPesertaId ='".$id_peserta."'
                            ");
            
        foreach ($w->result() as $row){
            $namaPerusahaan =strtoupper($row->msPesertaNamaPerusahaan); 
            $namaPemilik =strtoupper($row->msPesertaPemilik); 
            $namaPenanggung =strtoupper($row->msPesertaPenanggungJawab); 
            $alamat=$row->msPesertaAlamat;
            $tgl=strtoupper($row->msJadwalPenyuluhanTgl);
            $waktu=strtoupper($row->msJadwalPenyuluhanWaktu);
            $tempat=strtoupper($row->msJadwalPenyuluhanTempat);
            $urut = $row->msPesertaNoUrut;
            $tahun = $row->msPesertaTahun;  

            $unixTimestamp = strtotime($tgl);
            $hari = $this->hari_ini(date("w", $unixTimestamp));
            $tanggal = $this->reversdate($tgl);

            $alamat1 = substr($alamat, 0,50);
            $alamat2 = substr($alamat, 51,100);
            $alamat3 = substr($alamat, 101,150);
        }

        if($urut < 10){
          $no_urut = '00'.$urut.'/3307/'.$tahun;
        }else if($urut < 100){
          $no_urut = '0'.$urut.'/3307/'.$tahun;
        }else{
          $no_urut = $urut.'/3307/'.$tahun;
        }

        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Nomor  : '.$no_urut,'',0,'C');
        $fpdf -> ln();
        $fpdf -> ln();
        $fpdf -> ln(0.5);

        $fpdf -> Setx(2.0);        
        $fpdf -> SetFont('Arial','B',12);
        $fpdf -> Cell(20,0.5,'Diberikan kepada : ','',0,'L');
        $fpdf -> ln();
        $fpdf -> ln(0.5);

        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Setx(2.0);
        $fpdf -> Cell(6,0.7,'1. Nama Perusahaan / Merk ','',0,'');
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(4,0.7,': "'.$namaPerusahaan.'"','',0,'');
        $fpdf -> ln();
        $fpdf -> ln(0.5);

        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Setx(2.0);
        $fpdf -> Cell(6,0.7,'2. Nama Pemilik ','',0,'');
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(4,0.7,': '.$namaPemilik,'',0,'');
        $fpdf -> ln();
        $fpdf -> ln(0.5);

        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Setx(2.0);
        $fpdf -> Cell(6,0.7,'3. Nama Penanggung Jawab ','',0,'');
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(4,0.7,': '.$namaPenanggung,'',0,'');
        $fpdf -> ln();
        $fpdf -> ln(0.5);

        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Setx(2.0);
        $fpdf -> Cell(6,0.7,'4. Alamat ','',0,'');
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(4,0.7,': '.$alamat1,'',0,'');
        $fpdf -> ln();
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Setx(2.0);
        $fpdf -> Cell(6,0.7,'','',0,'');
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(4,0.7,'  '.$alamat2,'',0,'');
        $fpdf -> ln();
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Setx(2.0);
        $fpdf -> Cell(6,0.7,'','',0,'');
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(4,0.7,'  '.$alamat3,'',0,'');
        $fpdf -> ln();

        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Setx(3.2);
        $fpdf -> Cell(6,0.7,'Yang  telah  mengikuti  Penyuluhan  Keamanan  Pangan ( PKP ) dalam  rangka Pemberian ','',0,'');        
        $fpdf -> ln();

        $fpdf -> SetFont('Arial','BI',12);
        $fpdf -> Setx(2.0);
        $fpdf -> Cell(6,0.7,'Sertifikat   Produksi   Pangan   Industri   Rumah   Tangga  ( SPP-IRT )','',0,'');
        $fpdf -> Setx(16.0);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(6,0.7,'berdasarkan Peraturan ','',0,'');        
        $fpdf -> ln();

        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Setx(2.0);
        $fpdf -> Cell(6,0.7,'Badan Pengawas  Obat dan Makanan Republik Indonesia tentang Pedoman Pemberian Sertifikat','',0,'');
        $fpdf -> ln();

        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Setx(2.0);
        $fpdf -> Cell(6,0.7,'Produksi Pangan Industri Rumah Tangga  Nomor : 22 Tahun 2018 tanggal 16 Agustus 2018 yang','',0,'');
        $fpdf -> ln();

        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Setx(2.0);
        $fpdf -> Cell(6,0.7,'diselenggarakan di :','',0,'');
        $fpdf -> ln();
        $fpdf -> ln(0.5);

        $fpdf -> SetFont('Arial','B',14);
        $fpdf -> Setx(2.0);
        $fpdf -> Cell(8,0.7,'- Kabupaten','',0,'');
        $fpdf -> Cell(6,0.7,': Wonosobo','',0,'');
        $fpdf -> ln();
        $fpdf -> ln(0.3);

        $fpdf -> Setx(2.0);
        $fpdf -> Cell(8,0.7,'- Provinsi','',0,'');
        $fpdf -> Cell(6,0.7,': Jawa Tengah','',0,'');
        $fpdf -> ln();
        $fpdf -> ln(0.3);

        $fpdf -> Setx(2.0);
        $fpdf -> Cell(8,0.7,'- pada tanggal ','',0,'');
        $fpdf -> Cell(6,0.7,': '.$tanggal,'',0,'');
        $fpdf -> ln();
        $fpdf -> ln();
        $fpdf -> ln();

        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Setx(11.0);
        $fpdf -> Cell(4,0.7,'Diterbitkan di ','',0,'');
        $fpdf -> Cell(4,0.7,':   Wonosobo','',0,'');
        $fpdf -> ln();

        $fpdf -> Setx(11.0);
        $fpdf -> Cell(4,0.7,'Pada tanggal ','',0,'');
        $fpdf -> Cell(4,0.7,':   '.$this->reversdate(date('Y-m-d')),'',0,'');
        $fpdf -> ln(0.3);

        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'','B',0,'');
        $fpdf -> ln(0.01);
        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'','B',0,'');
        $fpdf -> ln();
        $fpdf -> ln();


        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'KEPALA DINAS KESEHATAN','',0,'C');
        $fpdf -> ln();
        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'KABUPATEN WONOSOBO','',0,'C');
        $fpdf -> ln();
        $fpdf -> ln();
        $fpdf -> ln();
        $fpdf -> ln();
        
        $fpdf -> Setx(11.0);
        $fpdf -> SetFont('Arial','U',12);
        $fpdf -> Cell(9.2,0.7,'JUNAEDI ,SKM, M.Kes','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(11.0);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(9.2,0.7,'Pembina Utama Muda','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'NIP. 19640601198803 1 012','',0,'C');
        $fpdf -> ln();


                   
        
        $fpdf->Output();
    }

    public function hari_ini($hari){

        switch($hari){
            case '0':
                $hari_ini = "Minggu";
            break;

            case '1':           
                $hari_ini = "Senin";
            break;

            case '2':
                $hari_ini = "Selasa";
            break;

            case '3':
                $hari_ini = "Rabu";
            break;

            case '4':
                $hari_ini = "Kamis";
            break;

            case '5':
                $hari_ini = "Jumat";
            break;

            case '6':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";     
            break;
        }

        return $hari_ini;

    }

    public function reversdate($tanggal){
        $a=explode('-',$tanggal);
        $d=$a[2];
        $m=$a[1];
        $y=$a[0];
        $a[0]=$m+0;
        $a[1]=$d;
        $a[2]=$y;

        $nmbulan='';

        switch ( $a[0] ) {
            case 1 : $nmbulan   = 'Januari';    break;
            case 2 : $nmbulan   = 'Februari';   break;
            case 3 : $nmbulan   = 'Maret';      break;
            case 4 : $nmbulan   = 'April';      break;
            case 5 : $nmbulan   = 'Mei';        break;
            case 6 : $nmbulan   = 'Juni';       break;
            case 7 : $nmbulan   = 'Juli';       break;
            case 8 : $nmbulan   = 'Agustus';    break;
            case 9 : $nmbulan   = 'September';  break;
            case 10: $nmbulan   = 'Oktober';    break;
            case 11: $nmbulan   = 'November';   break;
            case 12: $nmbulan   = 'Desember';   break;
        }

        $res=$a[1] . ' ' . $nmbulan . ' ' . $a['2'];
        return $res;
    }
}