<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		// $data['dataMenu'] 	= $this->Menu_model->select_all();
		// $data['dataParent'] 	= $this->Menu_model->select_view_menu_parent();

		$data['page'] 		= "menu";
		$data['judul'] 		= "Data Menu";
		$data['deskripsi'] 	= "Manage Data Menu";

		
		$this->template->views('home', $data);
	}
	
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */