<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_pendaftaran_akun extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Akun_model');
		$this->load->library('session');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Jakarta'); 
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		// $data['dataGrup'] 	= $this->Grup_model->select_all();

		$data['page'] 		= "menunggu verivikasi";
		$data['judul'] 		= "Menunggu Verifikasi";
		$data['deskripsi'] 	= "Manage Data Menunggu Verifikasi";		
		$this->template->views('list_pendaftaran_akun/home', $data);
	}

	public function tampilPeserta() {
		$data['dataUser'] = $this->Akun_model->select_sudah_verifikasi();		
		$this->load->view('list_pendaftaran_akun/list_data', $data);
	}

	
}
