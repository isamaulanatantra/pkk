<?php
Class Cetakrekomendasi extends AUTH_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->library('ciqrcode');   
        $this->load->library('session');
        // $this->load->helper('url');     

        $this->load->model('Ijin_model');        
    }
    
    function index(){
        $id_pirt=!empty($_GET['id_pirt'])?$_GET['id_pirt']:0;

        $fpdf = new FPDF('P','cm','legal');  
        $fpdf->SetMargins(13.175, 3.175, 3.175, 3.175);
        $fpdf->SetAutoPageBreak(false);      
        $fpdf->AddPage();        

        $fpdf -> Image(base_url().'assets/img/logowsb.jpg',1.3,1.5,2.0,2.2);
        // $fpdf -> Image(base_url().'assets/img/kemenkes.jpg',17.2,0.2,2.2,3.0);
        
        $fpdf -> Sety(1.5);
        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','B',12);
        $fpdf -> Cell(20,0.5,'PEMERINTAH KABUPATEN WONOSOBO','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','B',14);
        $fpdf -> Cell(20,0.5,'DINAS KESEHATAN','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Alamat : Jl. T.Jogonegoro 2-4 Telp./Faks. : (0286) 321033 / 321319','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Email : dinkes@wonosobokab.go.id','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Wonosobo-56311','',0,'C');        
        $fpdf -> ln();
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.0);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.01);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.02);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.03);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.04);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.05);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.06);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.07);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> Sety(4.1);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');

        $fpdf -> Sety(4.17);
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',20);
        $fpdf -> Cell(20.2,0.5,'','B',0,'C');
        $fpdf -> ln();

        $fpdf -> ln();
        $fpdf -> Setx(11.0);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5,0.5,'Wonosobo, '.$this->reversdate(date('Y-m-d')),'',0,'L');
        $fpdf -> ln();
        $fpdf -> ln();
        $fpdf -> Setx(11.0);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5,0.5,'Kepada Yth.','',0,'L');
        $fpdf -> ln();
        $fpdf -> Setx(11.0);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5,0.5,'Kepala Dinas Penanaman Modal,','',0,'L');
        $fpdf -> ln();        
        $fpdf -> Setx(11.0);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5,0.5,'Pelayanan Terpadu Satu Pintu','',0,'L');
        $fpdf -> ln();
        $fpdf -> Setx(11.0);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5,0.5,'Kabupaten Wonosobo','',0,'L');
        $fpdf -> ln();
        $fpdf -> Setx(11.0);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5,0.5,'Di -','',0,'L');
        $fpdf -> ln();        
        $fpdf -> Setx(11.0);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5,0.5,'Wonosobo','',0,'C');
        $fpdf -> ln();

        $fpdf -> ln();
        $fpdf -> ln();
        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','U',12);
        $fpdf -> Cell(20,0.5,'REKOMENDASI','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(7.5);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Nomor : ','',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Tentang Sertifikasi Industri Rumah Tangga Pangan','',0,'C');
        $fpdf -> ln();
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Yang bertanda tangan dibawah ini :','',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(3.5,0.5,'Nama','',0,'L');
        $fpdf -> Cell(5,0.5,': JUNAEDI ,SKM, M.Kes','',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(3.5,0.5,'NIP','',0,'L');
        $fpdf -> Cell(5,0.5,': 19640601198803 1 012','',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(3.5,0.5,'Pangkat/Gol.','',0,'L');
        $fpdf -> Cell(5,0.5,': Pembina Utama Muda / IV c','',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(3.5,0.5,'Jabatan','',0,'L');
        $fpdf -> Cell(5,0.5,': Kepala Dinas Kesehatan Kabupaten Wonosobo','',0,'L');
        $fpdf -> ln();
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Setelah dilakukan pemeriksaan terhadap Sararana Produksi Industri Rumah Tangga Pangan dan ','',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'telah  memenuhi  persyaratan,   maka  dengan  ini kami  memberikan  Rekomendasi untuk  dapat','',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'diterbitkan  Perpanjangan Sertifikat Produksi Pangan Industri Rumah Tangga (SPP-IRT),  dengan','',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'data sebagai berikut :','',0,'L');
        $fpdf -> ln();
        $fpdf -> ln();

        $dataPirt = $this->Ijin_model->select_by_id($id_pirt);

        foreach ($dataPirt as $data) {
            $nama_perusahaan = $data->msSppIrtPerusahaan;
            $nama_pemilik = $data->msSppIrtPemilik;
            $nama_penanggung_jawab = $data->msSppIrtPenanggungJawab;
            $alamat = $data->msSppIrtAlamat;
            $nib = $data->msSppIrtNib;

            $alamat1 = substr($alamat, 0,50);
            $alamat2 = substr($alamat, 51,100);
        }

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5.5,0.5,'Nama Perusahaan / Merk','',0,'L');
        $fpdf -> Cell(5,0.5,': '.$nama_perusahaan,'',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5.5,0.5,'Nama Pemilik','',0,'L');
        $fpdf -> Cell(5,0.5,': '.$nama_pemilik,'',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5.5,0.5,'Nama Penanggung Jawab','',0,'L');
        $fpdf -> Cell(5,0.5,': '.$nama_penanggung_jawab,'',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5.5,0.5,'Alamat','',0,'L');
        $fpdf -> Cell(5,0.5,': '.$alamat1,'',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5.5,0.5,'','',0,'L');
        $fpdf -> Cell(5,0.5,'  '.$alamat2,'',0,'L');
        $fpdf -> ln();

        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(5.5,0.5,'NIB','',0,'L');
        $fpdf -> Cell(5,0.5,': '.$nib,'',0,'L');
        $fpdf -> ln();

        $fpdf -> ln();
        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(1,0.7,'No','LRT',0,'C');
        $fpdf -> Cell(5,0.7,'Nama Produk','LRT',0,'C');
        $fpdf -> Cell(3.5,0.7,'Kemasan','LRT',0,'C');
        $fpdf -> Cell(6,0.7,'No. PIRT','LRT',0,'C');
        $fpdf -> Cell(3,0.7,'Masa Berlaku','LRT',0,'C');
        $fpdf -> ln();
        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(1,0.7,'','LRB',0,'C');
        $fpdf -> Cell(5,0.7,'','LRB',0,'C');
        $fpdf -> Cell(3.5,0.7,'Primer','LRB',0,'C');
        $fpdf -> Cell(6,0.7,'','LRB',0,'C');
        $fpdf -> Cell(3,0.7,'SPP-IRT','LRB',0,'C');
        $fpdf -> ln();

        $dataProduk = $this->Ijin_model->select_produk($id_pirt);
        $no = 1;
        foreach ($dataProduk as $data) {
            $kemasan = $this->Ijin_model->getKemasan($data->msProdukKodeKemasan);
            $urut = $this->Ijin_model->getNoPirt($data->msProdukIdIrt);

            $thnBuat = $data->msProdukThnBuat;
            $masaBerlaku = $data->msProdukMasaBerlaku;    
            $yy = $thnBuat+$masaBerlaku;
            $kadaluarsa = substr($yy, 2,2);

            if($urut < 10){
              $no_pirt = '000'.$urut;
            }else if($urut < 100){
              $no_pirt = '00'.$urut;
            }else if($urut < 1000){
              $no_pirt = '0'.$urut;
            }else{
              $no_pirt = $urut;
            }

            $no_pirt = $data->msProdukKodeKemasan.'.'.$data->msProdukKodeJenis.'.33.07.0'.$data->msProdukNoUrutProduk.'.'.$no_pirt.'.'.$kadaluarsa;

            $nama_produk = $data->msProdukNama;
            $kemasan_primer = $data->msKemasanPrimer;
            $masa_berlaku = $data->msProdukMasaBerlaku;

            $fpdf -> Setx(1.8);        
            $fpdf -> SetFont('Arial','',12);
            $fpdf -> Cell(1,1.0,$no,'LRTB',0,'C');
            $fpdf -> Cell(5,1.0,$nama_produk,'LRTB',0,'L');
            $fpdf -> Cell(3.5,1.0,$kemasan_primer,'LRTB',0,'C');
            $fpdf -> Cell(6,1.0,'  '.$no_pirt,'LRTB',0,'L');
            $fpdf -> Cell(3,1.0,$masa_berlaku.' Tahun','LRTB',0,'C');
            $fpdf -> ln();

            $no++;
        }


        $fpdf -> ln(0.5);
        $fpdf -> Setx(1.8);        
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Demikian untuk dapat digunakan sebagaimana mestinya.','',0,'L');
        $fpdf -> ln();
        $fpdf -> ln();  


        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'KEPALA DINAS KESEHATAN','',0,'C');
        $fpdf -> ln();
        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'KABUPATEN WONOSOBO','',0,'C');
        $fpdf -> ln();
        $fpdf -> ln();
        $fpdf -> ln();
        $fpdf -> ln();
        
        $fpdf -> Setx(11.0);
        $fpdf -> SetFont('Arial','U',12);
        $fpdf -> Cell(9.2,0.7,'JUNAEDI ,SKM, M.Kes','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(11.0);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(9.2,0.7,'Pembina Utama Muda','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(11.0);
        $fpdf -> Cell(9.2,0.7,'NIP. 19640601198803 1 012','',0,'C');
        $fpdf -> ln();


                   
        
        $fpdf->Output();
    }

    public function hari_ini($hari){

        switch($hari){
            case '0':
                $hari_ini = "Minggu";
            break;

            case '1':           
                $hari_ini = "Senin";
            break;

            case '2':
                $hari_ini = "Selasa";
            break;

            case '3':
                $hari_ini = "Rabu";
            break;

            case '4':
                $hari_ini = "Kamis";
            break;

            case '5':
                $hari_ini = "Jumat";
            break;

            case '6':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";     
            break;
        }

        return $hari_ini;

    }

    public function reversdate($tanggal){
        $a=explode('-',$tanggal);
        $d=$a[2];
        $m=$a[1];
        $y=$a[0];
        $a[0]=$m+0;
        $a[1]=$d;
        $a[2]=$y;

        $nmbulan='';

        switch ( $a[0] ) {
            case 1 : $nmbulan   = 'Januari';    break;
            case 2 : $nmbulan   = 'Februari';   break;
            case 3 : $nmbulan   = 'Maret';      break;
            case 4 : $nmbulan   = 'April';      break;
            case 5 : $nmbulan   = 'Mei';        break;
            case 6 : $nmbulan   = 'Juni';       break;
            case 7 : $nmbulan   = 'Juli';       break;
            case 8 : $nmbulan   = 'Agustus';    break;
            case 9 : $nmbulan   = 'September';  break;
            case 10: $nmbulan   = 'Oktober';    break;
            case 11: $nmbulan   = 'November';   break;
            case 12: $nmbulan   = 'Desember';   break;
        }

        $res=$a[1] . ' ' . $nmbulan . ' ' . $a['2'];
        return $res;
    }
}