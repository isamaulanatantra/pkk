<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi_pendaftaran_penyuluhan extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Penyuluhan_model');
		$this->load->library('session');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Jakarta'); 
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$dateNow = date('Y-m-d');
		$data['dataJadwal'] = $this->Penyuluhan_model->select_all_jadwal($dateNow);

		$data['page'] 		= "verivikasi penyuluhan";
		$data['judul'] 		= "Penyuluhan Verifikasi";
		$data['deskripsi'] 	= "Manage Data Verifikasi Penyuluhan";		
		$this->template->views('verifikasi_penyuluhan/home', $data);
	}
	public function tampilPeserta() {
		$id_puskesmas = $this->userdata->msAdminIdPuskesmas;
		$data['dataPenyuluhan'] = $this->Penyuluhan_model->select_belum_verifikasi($id_puskesmas);
		$dateNow = date('Y-m-d');
		$data['dataJadwal'] = $this->Penyuluhan_model->select_all_jadwal($dateNow);		
		$this->load->view('verifikasi_penyuluhan/list_data', $data);
	}

	public function verifikasi() {
		$this->form_validation->set_rules('id_peserta', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nik', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('id_jadwal_peserta', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_perusahaan', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_pemilik', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('penanggung_jawab', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Keterangan ', 'trim|required');	
		$this->form_validation->set_rules('no_hp', 'Keterangan ', 'trim|required');

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$id_peserta = $this->input->post('id_peserta');
			$nik = $this->input->post('nik');
			$id_jadwal_peserta = $this->input->post('id_jadwal_peserta');
			$nama_perusahaan = $this->input->post('nama_perusahaan');
			$nama_pemilik = $this->input->post('nama_pemilik');
			$penanggung_jawab = $this->input->post('penanggung_jawab');
			$alamat = $this->input->post('alamat');
			$no_hp = $this->input->post('no_hp');
			
			$user = $this->userdata->msAdminId;
			$total = $this->Penyuluhan_model->getTotalPeserta($id_jadwal_peserta);

			$w = $this->db->query("SELECT msJadwalPesertaMaks from msjadwalpenyuluhan 
                          where msJadwalPenyuluhanId = '".$id_jadwal_peserta."'  AND status !=99");
			foreach($w->result() as $h){
				$maks = $h->msJadwalPesertaMaks;
			}

			if ($total >= $maks) {
				echo 4;	
			}else{
				$data_update = array(
		    		'msPesertaIdJadwal' => $id_jadwal_peserta,
		    		'msPesertaNik' => $nik,
		    		'msPesertaNamaPerusahaan' => $nama_perusahaan,
		    		'msPesertaPemilik' => $nama_pemilik,
		    		'msPesertaPenanggungJawab' => $penanggung_jawab,
		    		'msPesertaAlamat' => $alamat,
		    		'msPesertaNoHp' => $no_hp,
					'updated_by' => $user,
					'updated_time' => date('Y-m-d H:i:s'),
					'status' => 2
				);
				

				$where = array(
		    		'msPesertaId' => $id_peserta
				);

				$table_name  = 'mspeserta';
				$result = $this->Penyuluhan_model->update($data_update, $where, $table_name);
				if ($result > 0) {
					echo 1;
				} else {
					echo 2;
				}
			}	
		}

	}

	public function tidakVerifikasi() {
		$this->form_validation->set_rules('id_peserta', 'Keterangan ', 'trim|required');
		
		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$id_peserta = $this->input->post('id_peserta');
			

			
				$data_update = array(
		    		'updated_by' => $user,
					'updated_time' => date('Y-m-d H:i:s'),
					'status' => 10
				);
				

				$where = array(
		    		'msPesertaId' => $id_peserta
				);

				$table_name  = 'mspeserta';
				$result = $this->Penyuluhan_model->update($data_update, $where, $table_name);
				if ($result > 0) {
					echo 1;
				} else {
					echo 2;
				}

				
		}

	}
}