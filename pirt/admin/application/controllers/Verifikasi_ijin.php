<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verifikasi_ijin extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Ijin_model');
		$this->load->library('session');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Jakarta'); 
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;		

		$data['dataKemasan'] 	= $this->Ijin_model->select_all_kemasan();
		$data['dataPangan'] 	= $this->Ijin_model->select_all_pangan();

		$data['page'] 		= "verifikasi ijin pirt";
		$data['judul'] 		= "Verifikasi Ijin PIRT";
		$data['deskripsi'] 	= "Manage Data Verifikasi Ijin PIRT";		
		$this->template->views('ijin/home', $data);
	}

	public function tampilIjin() {
		$user = $this->userdata->msAdminId;
		$id_puskesmas = $this->userdata->msAdminIdPuskesmas;
		$data['dataSppirt'] = $this->Ijin_model->select_all_sppirt($id_puskesmas);		
		$this->load->view('ijin/list_data', $data);
	}

	public function tampilProduk() {
		$id_pirt = $_GET['id_pirt'];
		$data['dataProduk'] = $this->Ijin_model->select_produk($id_pirt);
		$this->load->view('ijin/list_data_produk', $data);
	}

	public function lengkap() {
		$this->form_validation->set_rules('id_peserta', 'Keterangan ', 'trim|required');		

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$id_peserta = $this->input->post('id_peserta');
			$user = $this->userdata->msAdminId;		
			
			$data_update = array(
				'msSppIrtTglLengkap' => date('Y-m-d'),
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				'status' => 3
			);
			

			$where = array(
	    		'msSppIrtId' => $id_peserta
			);

			$table_name  = 'mssppirt';
			$result = $this->Ijin_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}				
		}
	}

	public function selesaiKunjungan() {
		$this->form_validation->set_rules('id_peserta', 'Keterangan ', 'trim|required');		

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$id_peserta = $this->input->post('id_peserta');
			$user = $this->userdata->msAdminId;		
			
			$data_update = array(
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				'status' => 5
			);
			

			$where = array(
	    		'msSppIrtId' => $id_peserta
			);

			$table_name  = 'mssppirt';
			$result = $this->Ijin_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}				
		}
	}

	public function verifikasi() {
		$this->form_validation->set_rules('id_produk', 'Keterangan ', 'trim|required');	
		$this->form_validation->set_rules('id_pirt2', 'Keterangan ', 'trim|required');		

		if ($this->form_validation->run() == TRUE){
			echo 0;		
		}else{
			$id_produk = $this->input->post('id_produk');
			$id_peserta = $this->input->post('id_peserta');
			$user = $this->userdata->msAdminId;		
			
			$data_update = array(	    		
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				'status' => 2
			);
			

			$where = array(
	    		'msProdukId' => $id_produk
			);

			$table_name  = 'msproduk';
			$result = $this->Ijin_model->update($data_update, $where, $table_name);

			$where2 = array(
	    		'msSppIrtId' => $id_peserta
			);

			$table_name2  = 'mssppirt';
			$result2 = $this->Ijin_model->update($data_update, $where2, $table_name2);

			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}				
		}
	}

	public function tidakVerifikasi() {
		$this->form_validation->set_rules('id_produk', 'Keterangan ', 'trim|required');		
		$this->form_validation->set_rules('alasan', 'Keterangan ', 'trim|required');

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$id_produk = $this->input->post('id_produk');
			$alasan = $this->input->post('alasan');
			$user = $this->userdata->msAdminId;		
			
			$data_update = array(	
				'msProdukAlasan' => $alasan,    		
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				'status' => 10
			);
			

			$where = array(
	    		'msProdukId' => $id_produk
			);

			$table_name  = 'msproduk';
			$result = $this->Ijin_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}				
		}
	}
}