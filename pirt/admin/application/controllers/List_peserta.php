<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class List_peserta extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Penyuluhan_model');
		$this->load->library('session');
		$this->load->helper('url');
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		// $data['dataGrup'] 	= $this->Grup_model->select_all();

		$data['page'] 		= "jadwal pelatihan";
		$data['judul'] 		= "Data Jadwal Pelatihan";
		$data['deskripsi'] 	= "Manage Data Jadwal Pelatihan";
		
		$this->template->views('peserta/home', $data);		
	}

	public function tampilPeserta() {
		$data['dataPeserta'] = $this->Penyuluhan_model->select_all_peserta();
		$this->load->view('peserta/list_data', $data);
	}

	public function hadirPeserta() {
				
		$id_peserta = $this->input->post('id_peserta');

		$user = $this->userdata->msAdminId;
		
				
			$data_update = array(
	    		'updated_by' => $user,
				'deleted_time' => date('Y-m-d H:i:s'),					
				'kehadiran' => 1
			);		
			
			$where = array(
	    		'msPesertaId' => $id_peserta
			);

			$table_name  = 'mspeserta';
			$result = $this->Penyuluhan_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		
	}
	
}
