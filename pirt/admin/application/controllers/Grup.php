<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grup extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Grup_model');
		$this->load->library('session');
		$this->load->helper('url');
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		// $data['dataGrup'] 	= $this->Grup_model->select_all();

		$data['page'] 		= "grup";
		$data['judul'] 		= "Data Grup";
		$data['deskripsi'] 	= "Manage Data Grup";
		
		$this->template->views('grup/home', $data);		
	}

	public function tampil() {
		$data['dataGrup'] = $this->Grup_model->select_all();
		$this->load->view('grup/list_data', $data);
	}

	public function insert() {
		$this->form_validation->set_rules('nama_grup', 'Nama ', 'trim|required');		

		$nama_grup = $this->input->post('nama_grup');
		
		$user = $this->userdata->msAdminId;

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$data_input = array(
	    		'msGrupMenuNama' => $nama_grup,
				'created_by' => $user,
				'created_time' => date('Y-m-d H:i:s'),
				'status' => 1
			);

			$table_name  = 'msgrupmenu';
			$result = $this->Grup_model->insert($data_input, $table_name);
			if ($result > 0) {
				echo 2;
			} else {
				echo 1;
			}
		}	
	}

	public function update() {
		$this->form_validation->set_rules('id', 'Nama ', 'trim|required');
				
		$id = $this->input->post('id');
		$nama_grup = $this->input->post('nama_grup');
		
		$user = $this->userdata->msAdminId;

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{

			$data_update = array(
	    		'msGrupMenuNama' => $nama_grup,
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				'status' => 1
			);
			

			$where = array(
	    		'msGrupMenuId' => $id
			);

			$table_name  = 'msgrupmenu';
			$result = $this->Grup_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		}	
	}

	public function hapus() {
		$this->form_validation->set_rules('id', 'Nama ', 'trim|required');
				
		$id = $this->input->post('id');

		$user = $this->userdata->msAdminId;
		
		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{			
			$data_update = array(
	    		'deleted_by' => $user,
				'deleted_time' => date('Y-m-d H:i:s'),					
				'status' => 99
			);		
			
			$where = array(
	    		'msGrupMenuId' => $id
			);

			$table_name  = 'msgrupmenu';
			$result = $this->Grup_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		}	
	}
}
