<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun_model extends CI_Model {
	public function select_belum_verifikasi($id_puskesmas) {
		if ($id_puskesmas == 99) {
			$this->db->select('*');
			$this->db->from('msuser');
			$this->db->where('status',1);
			$data = $this->db->get();
			
		}else{
			$this->db->select('*');
			$this->db->from('msuser');
			$this->db->where('status',1);
			$this->db->where('msUserIdPuskesmas',$id_puskesmas);
			// $this->db->order_by('created_by DESC');
			$data = $this->db->get();
		}
		

		return $data->result();
	}

	public function select_sudah_verifikasi() {
		$this->db->select('*');
		$this->db->from('msuser');
		// $this->db->where('status',2);
		$this->db->order_by('created_by ASC');
		$data = $this->db->get();

		return $data->result();
	}	

	function update($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}

	
}
