<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ijin_model extends CI_Model {		
	
	public function select_all_sppirt($id_puskesmas) {

		if ($id_puskesmas == 99) {
			$this->db->select('*');
			$this->db->from('mssppirt');
			$this->db->where('status != 99');
			$this->db->where('status !=', 5);
			$data = $this->db->get();
		}else{
			$this->db->select('*');
			$this->db->from('mssppirt');
			$this->db->where('status != 99');
			$this->db->where('status !=', 5);
			$this->db->where('msSppIrtIdPuskesmas', $id_puskesmas);
			$data = $this->db->get();
		}

		return $data->result();
	}

	public function select_sppirt_sudah_lengkap() {
		$this->db->select('*');
		$this->db->from('mssppirt');
		$this->db->where('status != 99');
		$this->db->where('status ', 5);
		$data = $this->db->get();

		return $data->result();
	}

	public function select_by_id($id_pirt) {
		$this->db->select('*');
		$this->db->from('mssppirt');
		$this->db->where('msSppIrtId',$id_pirt);
		$data = $this->db->get();

		return $data->result();
	}

	function update($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}

	//produk
	public function select_produk($id_pirt) {
		$this->db->select('*');
		$this->db->from('msproduk');
		$this->db->where('status != 99');
		$this->db->where('msProdukIdIrt',$id_pirt);
		$data = $this->db->get();

		return $data->result();
	}

	function getKemasan($id_kemasan){
		$where = array(
		      'msKemasanKode' => $id_kemasan
		      );
	 	$this->db->select('msKemasanNama');
	    $this->db->where($where);		    
	    $query = $this->db->get('mskemasan');

	    foreach ($query->result() as $hsl) {
	    	$value = $hsl->msKemasanNama;
	    }

	    if ($query->num_rows() > 0) {
	    	$cetak = $value;
	    }else{
	    	$cetak = '';
	    }

		return $cetak;
	}

	function getNoPirt($id_pirt){
		$where = array(
		      'msSppIrtId' => $id_pirt
		      );
	 	$this->db->select('msSppIrtNoUrut');
	    $this->db->where($where);		    
	    $query = $this->db->get('mssppirt');

	    foreach ($query->result() as $hsl) {
	    	$value = $hsl->msSppIrtNoUrut;
	    }

	    if ($query->num_rows() > 0) {
	    	$cetak = $value;
	    }else{
	    	$cetak = '';
	    }

		return $cetak;
	}

	public function select_all_kemasan() {
		$this->db->select('*');
		$this->db->from('mskemasan');
		$this->db->where('status != 99');
		$data = $this->db->get();

		return $data->result();
	}

	public function select_all_pangan() {
		$this->db->select('*');
		$this->db->from('msjenispangan');
		$this->db->where('status != 99');
		$data = $this->db->get();

		return $data->result();
	}

}