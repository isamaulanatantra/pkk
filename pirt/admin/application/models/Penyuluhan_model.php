<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penyuluhan_model extends CI_Model {
	public function select_belum_verifikasi($id_puskesmas) {
		if ($id_puskesmas == 99) {
			$this->db->select('*');
			$this->db->from('mspeserta');
			$this->db->where('status',1);
			$this->db->order_by('created_by ASC');
			$data = $this->db->get();
		}else{
			$this->db->select('*');
			$this->db->from('mspeserta');
			$this->db->where('status',1);
			$this->db->where('msPesertaIdPuskesmas',$id_puskesmas);
			$this->db->order_by('created_by ASC');
			$data = $this->db->get();
		}		

		return $data->result();
	}

	public function select_sudah_verifikasi() {
		$this->db->select('*');
		$this->db->from('mspeserta');
		// $this->db->where('status',2);
		$this->db->order_by('created_by ASC');
		$data = $this->db->get();

		return $data->result();
	}

	public function select_all_peserta() {
		$this->db->select('*');
		$this->db->from('mspeserta');
		$this->db->where('status',2);
		$this->db->order_by('created_by DESC');
		$data = $this->db->get();

		return $data->result();
	}	

	public function select_all_jadwal($date) {
		$this->db->select('*');
		$this->db->from('msjadwalpenyuluhan');
		$this->db->where('msJadwalPenyuluhanTgl >= ',$date);
		$data = $this->db->get();

		return $data->result();
	}

	public function select_jadwal_by_id($id_jadwal) {
		$this->db->select('*');
		$this->db->from('msjadwalpenyuluhan');
		$this->db->where('msJadwalPenyuluhanId ',$id_jadwal);
		$data = $this->db->get();

		return $data->result();
	}

	public function select_peserta($id_jadwal) {
		$this->db->select('*');
		$this->db->from('mspeserta');
		$this->db->where('status != 99');
		$this->db->where('status = 2');
		$this->db->where('msPesertaIdJadwal =',$id_jadwal);
		$this->db->order_by('msPesertaId DESC');
		$data = $this->db->get();
		return $data->result();
	}

	function update($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}

	public function getTotalPeserta($id_jadwal) {
		$this->db->select('msPesertaId');
		$this->db->where('msPesertaIdJadwal',$id_jadwal);
		$this->db->where('status !=99');
		$this->db->from('mspeserta');
		
		$data = $this->db->get();
		$total = $data->num_rows();
		return $total;
	}

	
}
