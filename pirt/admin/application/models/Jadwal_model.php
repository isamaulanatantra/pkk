<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jadwal_model extends CI_Model {
	public function select_all() {
		$this->db->select('*');
		$this->db->from('msjadwalpenyuluhan');
		$this->db->where('status != 99');
		$this->db->order_by('msJadwalPenyuluhanTgl ASC');
		$data = $this->db->get();

		return $data->result();
	}	

	public function select_jadwal_by_id($id_jadwal) {
		$this->db->select('*');
		$this->db->from('msjadwalpenyuluhan');
		$this->db->where('msJadwalPenyuluhanId ',$id_jadwal);
		$data = $this->db->get();

		return $data->result();
	}		

	public function insert($data_input, $table_name) {
		$this->db->insert( $table_name, $data_input );	
		return $this->db->affected_rows();
	}	

	function update($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}

	public function no_urut($tahun) {
		$this->db->select('*');
		$this->db->from('mspeserta');
		$this->db->where('msPesertaTahun =',$tahun);
		$data = $this->db->get();
		$total = $data->num_rows();
		return $total;
	}

	public function jumlah_peserta($id_jadwal) {
		$this->db->select('*');
		$this->db->from('mspeserta');
		$this->db->where('status != 99');
		$this->db->where('status = 2');
		$this->db->where('msPesertaIdJadwal =',$id_jadwal);
		$data = $this->db->get();
		return $data->num_rows();
	}

	//mulai peserta
	public function select_all_peserta() {
		$this->db->select('*');
		$this->db->from('mspeserta');
		$this->db->where('status != 99');
		$this->db->where('status = 2');
		$this->db->order_by('msPesertaId DESC');
		$data = $this->db->get();
		return $data->result();
	}

	public function select_all_peserta_menunggu() {
		$this->db->select('*');
		$this->db->from('mspeserta');
		$this->db->where('status = 1');
		$this->db->order_by('msPesertaId ASC');
		$data = $this->db->get();
		return $data->result();
	}	

	public function select_peserta($id_jadwal) {
		$this->db->select('*');
		$this->db->from('mspeserta');
		$this->db->where('status != 99');
		$this->db->where('status = 2');
		$this->db->where('msPesertaIdJadwal =',$id_jadwal);
		$this->db->order_by('msPesertaId DESC');
		$data = $this->db->get();
		return $data->result();
	}	

	public function select_all_jadwal($dateNow) {
		$this->db->select('*');
		$this->db->from('msjadwalpenyuluhan');
		$this->db->where('status != 99');
		$this->db->where('msJadwalPenyuluhanTgl >=',$dateNow);
		$this->db->order_by('msJadwalPenyuluhanTgl ASC');
		$data = $this->db->get();
		return $data->result();
	}

	public function getTotalPeserta($id_jadwal) {
		$this->db->select('msPesertaId');
		$this->db->where('msPesertaIdJadwal',$id_jadwal);
		$this->db->where('status !=99');
		$this->db->from('mspeserta');
		
		$data = $this->db->get();
		$total = $data->num_rows();
		return $total;
	}

	public function getPesertaMaksimal($id_jadwal) {
		$this->db->select('msJadwalPesertaMaks');
		$this->db->where('msJadwalPenyuluhanId',$id_jadwal);
		$this->db->where('status !=99');
		$this->db->from('msjadwalpenyuluhan');
		
		$data = $this->db->get();
		return $data->result();
	}
}
