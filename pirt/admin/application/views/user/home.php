<style>
  .box-form {
    background-color: white;
    margin-bottom: 15px;
    margin-left: 5px;
    margin-right: 5px;
  }
</style>

<div class="msg" style="display:visible;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
  <div  class="col-md-8">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          <span><label>LISTING DATA</label> 
            <!-- <label id="judul2"> </label></span> -->
        </div>        
      </div>

      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;">NO</th>
                <th style="display: none;">Id Pegawai</th>
                <th style="text-align: center;">Nama</th>                        
                <th style="text-align: center;">Username</th>
                <th style="display: none;">Id Grup</th>
                <th style="text-align: center;">Grup User</th>
                <th style="display: none;">Id Puskesmas</th>
                <th style="text-align: center;">Wilayah Kerja</th>
                <!-- <th style="text-align: center;">Aksi</th> -->
              </tr>
            </thead>
            <tbody id="tblUtamaUser">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>
  </div>

  <div  class="col-md-4">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          <span><label>FORM INPUT</label></label></span>
        </div>        
      </div>
      <div class="box-form">
        <!-- <form id="form-tambah-user" method="POST"> -->
            <div class="form-group" style="display: none;">
              <label for="id_user">Id Pegawai</label>
              <input class="form-control" id="id_user" name="id_user" value="" placeholder="Id Pegawai" type="text" required="">
            </div> 

            <div class="form-group">
              <label for="nama_user">Nama Pegawai</label>
              <input class="form-control" id="nama_user" name="nama_user" value="" placeholder="Nama Pegawai" type="text" required="">
            </div> 
            <div class="form-group">
              <label for="username">Username</label>
              <input class="form-control" id="username" name="username" value="" placeholder="Username" type="text" required="">
            </div>
            <div class="form-group">
              <label for="sandi" >Sandi &nbsp;&nbsp;&nbsp;</label>
              <label for="sandi" id="lblsandi" id="lblsandi" style="display: none;"> *)kosongi jika sandi tidak dirubah</label>
              <input class="form-control" id="sandi" name="sandi" value="" placeholder="Sandi" type="password" required="">
            </div> 
            <div class="form-group">
              <label for="grup">Grup User</label>
              <select id="grup_user" name="grup_user" class="form-control select2" aria-describedby="sizing-addon2">        
                <?php
                foreach ($dataGrup as $data) {
                  ?>
                  <option value="<?php echo $data->msGrupMenuId; ?>">
                    <?php echo $data->msGrupMenuNama; ?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </div>

            <div class="form-group">
              <label for="id_puskesmas">Wilayah Kerja Puskesmas</label>
              <select id="id_puskesmas" name="id_puskesmas" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%;">        
                <?php
                foreach ($dataPuskesmas as $data) {
                  ?>
                  <option value="<?php echo $data->msPuskesmasId; ?>">
                    <?php echo $data->msPuskesmasNama; ?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </div>

            <center>
              <div class="form-group" id="formBtnSimpan" name="formBtnSimpan">
                <button type="submit" class="btn btn-success" id="btnBaru" name="btnBaru"><i class="fa fa-plus"></i>&nbsp;&nbsp;BARU</button>
                <button type="submit" class="btn btn-primary" id="btnSimpan" name="btnSimpan"><i class="fa fa-save"></i>&nbsp;&nbsp;SIMPAN</button>          
              </div>

              <div class="form-group" style="display: none;" id="formBtnUpdate" name="formBtnUpdate">
                <button type="submit" class="btn btn-warning" id="btnBatal" name="btnBaru"><i class="fa fa-minus-square-o"></i>&nbsp;&nbsp;BATAL</button>
                <button type="submit" class="btn btn-primary" id="btnUpdate" name="btnSimpan"><i class="fa fa-save"></i>&nbsp;&nbsp;UPDATE</button>          
                <button type="submit" class="btn btn-danger" id="btnHapus" name="btnBaru"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;HAPUS</button>
              </div>
            </center>
                                  
            <br>  
        <!-- </form>       -->
      </div>  
    </div>
  </div>
</div>

<script type="text/javascript">
  tampilUser();
  baru();

  $(function () {
      $(".select2").select2();

      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
      });
  });

  function tampilUser() {
    $.get('<?php echo base_url('User/tampilUser'); ?>', function(data) {
      MyTable.fnDestroy();
      $('#tblUtamaUser').html(data);
      refresh();
    });
  }
  
  function baru() {
      $('#nama_user').val("");
      $('#username').val("");
      $('#sandi').val("");

      $('#nama_user').val("");
      $('#username').val("");
      $('#sandi').val("");              
      $('#formBtnSimpan').show();

      $('#formBtnUpdate').hide();
      $('#lblsandi').hide();

      $('#nama_user').val("");
      $('#username').val("");
      $('#sandi').val("");              
      
  }
</script>

<script>
    $('#btnBaru').on("click", function(){
      baru();
    });
</script>

<script>
    $('#btnSimpan').on("click", function(){
      var nama_user = $('#nama_user').val();
      var username = $('#username').val();
      var sandi = $('#sandi').val();
      var grup_user = $('#grup_user').val();
      var id_puskesmas = $('#id_puskesmas').val();
      
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            nama_user:nama_user,
            username:username,
            sandi:sandi,
            grup_user:grup_user,
            id_puskesmas:id_puskesmas
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>User/insert',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              tampilUser();
              $('#nama_user').val("");
              $('#username').val("");
              $('#sandi').val("");              
            }
          }
      });
    });
</script>

<script>
  $('#tblUtamaUser').on('click','tr',function(){
            var id = $(this).find('td:eq(1)').text();
            var nama=$(this).find('td:eq(2)').text();
            var username=$(this).find('td:eq(3)').text();
            var grup=$(this).find('td:eq(4)').text();
            var id_puskesmas=$(this).find('td:eq(6)').text();

            $('#formBtnSimpan').hide();
            $('#formBtnUpdate').show();
            $('#lblsandi').show();
                        
            //$('#ModalUpdate').modal('show');
            $('[name="id_user"]').val(id);
            $('[name="nama_user"]').val(nama);
            $('[name="username"]').val(username);
            // $('#grup_user').val(grup);   
            $('#grup_user').val(grup).trigger('change');
            $('#id_puskesmas').val(id_puskesmas).trigger('change');       

      });

</script>

<script>
  $('#btnBatal').on("click", function(){
    $('#formBtnSimpan').show();
    $('#formBtnUpdate').hide();
    $('#lblsandi').hide();

    $('#nama_user').val("");
    $('#username').val("");
    $('#sandi').val("");              
    $('#id_user').val("");
  });
</script>

<script>
    $('#btnUpdate').on("click", function(){
      var id = $('#id_user').val();
      var nama_user = $('#nama_user').val();
      var username = $('#username').val();
      var sandi = $('#sandi').val();
      var grup_user = $('#grup_user').val();
      var id_puskesmas = $('#id_puskesmas').val();
      
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id:id,
            nama_user:nama_user,
            username:username,
            sandi:sandi,
            grup_user:grup_user,
            id_puskesmas:id_puskesmas
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>User/update',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              tampilUser();
              baru()
            }
          }
      });
    });
</script>

<script>
    $('#btnHapus').on("click", function(){
      var id = $('#id_user').val();
      var nama_user = $('#nama_user').val();
      var username = $('#username').val();
      var sandi = $('#sandi').val();
      var grup_user = $('#grup_user').val();

      alertify.confirm("Apakah anda yakin hapus data ini?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id:id
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>User/hapus',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              tampilUser();
              baru();
            }
          }
      });
        } else {
          alertify.error("Batal hapus data");
        }
      });
      return false;
      
      
    });
</script>
