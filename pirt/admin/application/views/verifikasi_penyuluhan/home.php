<style>
  .box-form {
    background-color: white;
    margin-bottom: 15px;
    margin-left: 5px;
    margin-right: 5px;
  }
</style>

<div class="msg" style="display:visible;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="peserta" id="peserta" name="peserta" style="display: visible;">
<div class="row">
  <div  class="col-md-8">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          <span><label>LISTING DATA</label> 
            <!-- <label id="judul2"> </label></span> -->
        </div>        
      </div>

      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data2" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;">NO</th>
                <th style="display: none;">Id</th>
                <th style="text-align: center;">NIK</th>
                <th style="text-align: center;">No Hp</th>
                <th style="text-align: center;">Nama Perusahaan</th>
                <th style="text-align: center;">Nama Pemilik</th>
                <th style="text-align: center;">Penanggung Jawab</th>
                <th style="text-align: center;">Alamat</th>
              </tr>
            </thead>
            <tbody id="tblPeserta">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>
  </div>

  <div  class="col-md-4">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          
          <span><label>FORM VERIFIKASI PESERTA</label></label></span>
        </div>        
      </div>
      <div class="box-form">
        <!-- <form id="form-tambah-user" method="POST"> -->
            <div class="form-group" style="display: none;">
              <label for="id_peserta">Id Peserta</label>
              <input class="form-control" id="id_peserta" name="id_peserta" value="" placeholder="Id Peserta" type="text" required="">
            </div>            

            <div class="form-group">
              <label for="nik">NIK</label>
              <input class="form-control" id="nik" name="nik" placeholder="No KTP" type="text" required="" >
            </div> 

            <div class="form-group">
              <label for="no_hp">No HP</label>
              <input class="form-control" id="no_hp" name="no_hp" placeholder="No HP" type="text" required="" >
            </div> 

            <div class="form-group">
              <label for="Nama">Nama Perusahaan</label>
              <input class="form-control" id="nama_perusahaan" name="nama_perusahaan" placeholder="Nama Perusahaan" type="text" required="" >
            </div> 

            <div class="form-group">
              <label for="">Nama Pemilik</label>
              <input class="form-control" id="nama_pemilik" name="nama_pemilik" placeholder="Nama Pemilik" type="text" required="" >
            </div> 

            <div class="form-group">
              <label for="">Penanggung Jawab</label>
              <input class="form-control" id="penanggung_jawab" name="penanggung_jawab" placeholder="Penanggung Jawab" type="text" required="" >
            </div> 

            <div class="form-group">
              <label for="alamat">Alamat</label>
              <textarea class="form-control" rows="2" id="alamat" name="alamat" value="" placeholder="Alamat" type="text" required="" ></textarea>
            </div>          
                        

            <center>             
              <div class="form-group" id="formBtnVerivikasi" name="formBtnVerivikasi">
                <button type="submit" class="btn btn-success" id="btnVerifikasi" name="btnVerifikasi"><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Verifikasi</button>          
                <button type="submit" class="btn btn-danger" id="btnTidakVerifikasi" name="btnTidakVerifikasi"><i class="glyphicon glyphicon-remove"></i>&nbsp;&nbsp;Tidak Verivikasi</button>
                <button type="submit" class="btn btn-warning" id="btnBatalVerivikasi" name="btnBatalVerivikasi"><i class="fa fa-minus-square-o"></i>&nbsp;&nbsp;BATAL</button>
              </div>            
            </center>

            <div class="form-group" style="display: none;" id="formSimpanVerivikasi" name="formSimpanVerivikasi">
              <label for="id_jadwal_peserta">Jadwal Pelatihan</label>
              <select id="id_jadwal_peserta" name="id_jadwal_peserta" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%;">   
                <?php
                foreach ($dataJadwal as $data) {
                  ?>
                  <option value="<?php echo $data->msJadwalPenyuluhanId; ?>">
                    [ <?php echo $data->msJadwalPenyuluhanTgl.' ] '.$data->msJadwalPenyuluhanTempat; ?>
                  </option>
                  <?php
                }
                ?>
              </select>
              <br>
              <br>
              <center>
              <button type="submit" class="btn btn-primary" id="btnSimpanVerifikasi" name="btnUpdateVerifikasi"><i class="fa fa-save"></i>&nbsp;&nbsp;SIMPAN</button>   
              </center>
            </div>

            
        <!-- </form>       -->
      </div>  
    </div>
  </div>
</div>
</div>

<script>
  $(function () {
      $(".select2").select2();

      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
      });
  });
</script>

<script>
  function tampilPeserta() {    
    $.get('<?php echo base_url();?>Verifikasi_pendaftaran_penyuluhan/tampilPeserta', function(data) {
      MyTable2.fnDestroy();
      $('#tblPeserta').html(data);
      refresh();
    });
  }
</script>

<script>
  function baruPeserta(){
    $("#id_peserta").val("");
    $("#nik").val("");
    $("#no_hp").val("");
    $("#nama_perusahaan").val("");
    $("#nama_pemilik").val("");
    $("#penanggung_jawab").val("");
    $("#alamat").val("");   
    
    $('#formBtnVerivikasi').hide();
    
    tampilPeserta();    
  }
</script>

<script>
  baruPeserta();
</script>

<script>
  $('#tblPeserta').on('click','tr',function(){
        var id_peserta = $(this).find('td:eq(1)').text();
        var nik=$(this).find('td:eq(2)').text(); 
        var no_hp=$(this).find('td:eq(3)').text();
        var nama_perusahaan=$(this).find('td:eq(4)').text();
        var nama_pemilik=$(this).find('td:eq(5)').text();
        var penanggung_jawab=$(this).find('td:eq(6)').text();
        var alamat=$(this).find('td:eq(7)').text();       
                    
        $("#id_peserta").val(id_peserta);
        $("#nik").val(nik);
        $("#no_hp").val(no_hp);
        $("#nama_perusahaan").val(nama_perusahaan);
        $("#nama_pemilik").val(nama_pemilik);
        $("#penanggung_jawab").val(penanggung_jawab);
        $("#alamat").val(alamat);   

        $('#formBtnVerivikasi').show();
        
  });
</script>

<script>
  $('#btnBatalVerivikasi').on('click',function(){
    baruPeserta();
  });
</script>

<script>
    $('#btnVerifikasi').on("click", function(){
      $("#formSimpanVerivikasi").show();
    });
</script>

<script>
    $('#btnSimpanVerifikasi').on("click", function(){
      var id_peserta = $("#id_peserta").val();
      var nik = $("#nik").val();
      var id_jadwal_peserta = $("#id_jadwal_peserta").val();
      var nama_perusahaan = $("#nama_perusahaan").val();
      var nama_pemilik = $("#nama_pemilik").val();
      var penanggung_jawab = $("#penanggung_jawab").val();
      var alamat = $("#alamat").val();
      var no_hp = $("#no_hp").val();
      
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_peserta:id_peserta,
            nik:nik,
            id_jadwal_peserta:id_jadwal_peserta,
            nama_perusahaan:nama_perusahaan,
            nama_pemilik:nama_pemilik,
            penanggung_jawab:penanggung_jawab,
            alamat:alamat,
            no_hp:no_hp
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Verifikasi_pendaftaran_penyuluhan/verifikasi',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal diupdate");

            }else if( text == 4 ){
              alertify.error("Peserta sudah penuh");

            }else{
              alertify.success("Data berhasil diupdate");
              tampilPeserta();
              baruPeserta();  
              $('#formSimpanVerivikasi').hide();
              $('#formBatalVerivikasi').hide();
            }
          }
      });
    });
</script>

<script>
    $('#btnTidakVerifikasi').on("click", function(){
      var id_peserta = $("#id_peserta").val();
      alertify.confirm("Apakah anda yakin hapus data ini?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_peserta:id_peserta
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Verifikasi_pendaftaran_penyuluhan/tidakVerifikasi',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal dihapus");

            }else{
              alertify.success("Tidak verifikasi berhasil");
              tampilPeserta();
              baruPeserta();  
              $('#formSimpanVerivikasi').hide();
              $('#formBatalVerivikasi').hide();
            }
          }
      });
        } else {
          alertify.error("Dibatalkan");
        }
      });
      return false;
    });
</script>