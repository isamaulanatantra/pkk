<style>
  .box-form {
    background-color: white;
    margin-bottom: 15px;
    margin-left: 5px;
    margin-right: 5px;
  }
</style>

<div class="msg" style="display:visible;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="peserta" id="peserta" name="peserta" style="display: visible;">
<div class="row">
  <div  class="col-md-12">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          <span><label>LISTING DATA</label> 
            <!-- <label id="judul2"> </label></span> -->
        </div>        
      </div>

      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data2" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;">NO</th>
                <th style="display: none;">Id</th>
                <th style="text-align: center;">NIK</th>
                <th style="text-align: center;">No Hp</th>
                <th style="text-align: center;">Nama</th>
                <th style="display: none;">Foto KTP</th>
                <th style="text-align: center;">Status</th>
                <th style="text-align: center;">Kirirm Notifikasi</th>
              </tr>
            </thead>
            <tbody id="tblPeserta">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>
  </div>

  <div  class="col-md-4" style="display: none;">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          
          <span><label>FORM VERIFIKASI PESERTA</label></label></span>
        </div>        
      </div>
      <div class="box-form">
        <!-- <form id="form-tambah-user" method="POST"> -->
            <div class="form-group" style="display: none;">
              <label for="id_peserta">Id Peserta</label>
              <input class="form-control" id="id_peserta" name="id_peserta" value="" placeholder="Id Peserta" type="text" required="">
            </div>            

            <div class="form-group">
              <label for="nik">NIK</label>
              <input class="form-control" id="nik" name="nik" value="" placeholder="No KTP" type="text" required="" >
            </div> 

            <div class="form-group">
              <label for="no_hp">No HP</label>
              <input class="form-control" id="no_hp" name="no_hp" value="" placeholder="No HP" type="text" required="" >
            </div> 



            <div class="form-group">
              <label for="Nama">Nama Pemilik</label>
              <input class="form-control" id="nama" name="nama" value="" placeholder="Nama" type="text" required="" >
            </div>             

            <div class="form-group">              
              <label for="Nama">Foto KTP</label>
              <br>
              <center>
              <img id="myImg" src="" width="350" height="200" style="display: none;">
              </center>
            </div>
            

            <center>             

              <div class="form-group" id="formBtnVerivikasi" name="formBtnVerivikasi">
                <button type="submit" class="btn btn-success" id="btnVerifikasi" name="btnVerifikasi"><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Verifikasi</button>          
                <button type="submit" class="btn btn-danger" id="btnTidakVerifikasi" name="btnTidakVerifikasi"><i class="glyphicon glyphicon-remove"></i>&nbsp;&nbsp;Tidak Verivikasi</button>
                <button type="submit" class="btn btn-warning" id="btnBatalVerivikasi" name="btnBatalVerivikasi"><i class="fa fa-minus-square-o"></i>&nbsp;&nbsp;BATAL</button>
              </div>
              <br>

              
            </center>

            
        <!-- </form>       -->
      </div>  
    </div>
  </div>
</div>
</div>

<script>
  $(function () {
      $(".select2").select2();

      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
      });
  });
</script>

<script>
  function tampilPeserta() {    
    $.get('<?php echo base_url();?>List_pendaftaran_akun/tampilPeserta', function(data) {
      MyTable2.fnDestroy();
      $('#tblPeserta').html(data);
      refresh();
    });
  }
</script>

<script>
  function baruPeserta(){

    $("#id_peserta").val("");
    $("#nik").val("");
    $("#no_hp").val("");
    $("#nama").val("");   
    document.getElementById("myImg").src = "";
    $("#myImg").hide(); 
    
    $('#formBtnVerivikasi').hide();
    
    tampilPeserta();    
  }
</script>

<script>
  baruPeserta();
</script>

<script>
  $('#tblPeserta').on('click','tr',function(){
        var id_peserta = $(this).find('td:eq(1)').text();
        var nik=$(this).find('td:eq(2)').text(); 
        var no_hp=$(this).find('td:eq(3)').text();
        var nama=$(this).find('td:eq(4)').text();
        var foto=$(this).find('td:eq(5)').text();
        
                    
        $("#id_peserta").val(id_peserta);
        $("#nik").val(nik);
        $("#no_hp").val(no_hp);
        $("#nama").val(nama); 
        $("#myImg").show();
        document.getElementById("myImg").src = "<?php echo base_url(); ?>gambar/"+foto;

        $('#formBtnVerivikasi').show();
        
  });
</script>

<script>
  $('#btnBatalVerivikasi').on('click',function(){
    baruPeserta();
  });
</script>

<script>
    $('#btnVerifikasi').on("click", function(){
      var id_peserta = $("#id_peserta").val();      
      
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_peserta:id_peserta
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Verifikasi_akun/verifikasi',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal diupdate");

            }else if( text == 2 ){
              alertify.success("Data berhasil diupdate");
              baruPeserta();                
            }
          }
      });
    });
</script>

<script>
  $('#tblPeserta').on('click','td:eq(7)',function(){
    
      alertify.error("API SMS belum siap");

  });
</script>