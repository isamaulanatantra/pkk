
<?php
  $no = 1;
  foreach ($dataUser as $data) {    

    $getstatus = $data->status;

    if ($getstatus == 1) {
      $status = 'Belum Verifikasi';
      $color = 'btn btn-warning';
      $sms = '';
    }else if ($getstatus == 2) {
      $status = 'Terverifikasi';
      $color = 'btn btn-success';
      $sms = '<a href="" class="btn btn-success">Kirim SMS</a>';
    }else if ($getstatus == 10) {
      $status = 'Tidak terverifikasi';
      $color = 'btn btn-danger';
      $sms = '<a href="" class="btn btn-danger">Kirim SMS</a>';
    }
    
    ?>
    <tr>
      <td style="text-align: center;"><?php echo $no; ?></td>
      <td  style="display: none;"><?php echo $data->msUserId; ?></td>
      <td ><?php echo $data->msUserNik; ?></td>
      <td ><?php echo $data->msUserNoHp; ?></td>
      <td ><?php echo $data->msUserNama; ?></td>
      <td  style="display: none;"><?php echo $data->msUserFile; ?></td>
      <td style="text-align: center;"><p class="<?php echo $color; ?>"><?php echo $status; ?></p></td>
      <!-- <td style="text-align: center;"><?php echo $sms; ?></td>       -->
      <td><center><button class="btn btn-success">Kirim SMS</button></center></td>
    </tr>
    <?php
    $no++;
  }
?>