<style>
  .box-form {
    background-color: white;
    margin-bottom: 15px;
    margin-left: 5px;
    margin-right: 5px;
  }
</style>

<div class="msg" style="display:visible;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="peserta" id="peserta" name="peserta" style="display: visible;">
<div class="row">
  <div  class="col-md-12">
    <div class="box">
      <div class="box-header">
        <div class="box-name">         
          
          <i class="fa fa-user"></i>
          <span><label>LISTING DATA PESERTA</label> 
            <!-- <label id="judul2"> </label></span> -->
        </div>        
      </div>

      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;">NO</th>
                <th style="display: none;">Id</th>
                <th style="text-align: center;">No Sertifikat</th>
                <th style="text-align: center;">No KTP</th>
                <th style="text-align: center;">Nama Perusahaan</th>
                <th style="text-align: center;">Nama Pemilik</th>
                <th style="text-align: center;">Nama Penggung Jawab</th>
                <th style="text-align: center;">Alamat</th>
                <th style="text-align: center;">No Telp.</th>
                <th style="text-align: center;">Konfirmasi Kehadiran</th>
                <th style="text-align: center;">Cetak</th>
              </tr>
            </thead>
            <tbody id="tblPeserta">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>
  </div>

  
</div>
</div>

<script>
  function tampil(){    
    $.get('<?php echo base_url();?>List_peserta/tampilPeserta', function(data) {
      MyTable.fnDestroy();
      $('#tblPeserta').html(data);
      refresh();
    });
  }
</script>

<script>
  tampil();
</script>

<script>
  $('#tblPeserta').on('click','#hadir',function(){
    
      var id = $( this ).text();   
      var str_id_peserta = id.split("-");

      var str_id_peserta = str_id_peserta[0];
      var id_peserta = str_id_peserta.trim();

      alertify.confirm("Konfirmasi kehadiran?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_peserta:id_peserta
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>List_peserta/hadirPeserta',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal di update");

            }else{
              alertify.success("Data berhasil di update");
              tampil();
            }
          }
      });
        } else {
          alertify.error("Konfirmasi di batalkan");
        }
      });
      return false;

  });
</script>


