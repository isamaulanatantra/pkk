
<?php
  $no = 1;
  foreach ($dataPeserta as $data) {
    $urut = $data->msPesertaNoUrut;
    if($urut < 10){
      $no_urut = '00'.$urut.'/3307/'.$data->msPesertaTahun;
    }else if($urut < 100){
      $no_urut = '0'.$urut.'/3307/'.$data->msPesertaTahun;
    }else{
      $no_urut = $urut.'/3307/'.$data->msPesertaTahun;
    }

    $kehadiran = $data->kehadiran;
    if ($kehadiran == 0) {
      $konfirmasi = 'display: visible;';
      $cetak = 'display: none';
    }if ($kehadiran == 1) {
      $konfirmasi = 'display: none;';
      $cetak = 'display: visible';
    }
    
    ?>
    <tr>
      <td width="5%"><?php echo $no; ?></td>
      <td  style="display: none;"><?php echo $data->msPesertaId; ?></td>
      <td ><?php echo $no_urut; ?></td>
      <td ><?php echo $data->msPesertaNik; ?></td>
      <td ><?php echo $data->msPesertaNamaPerusahaan; ?></td>
      <td ><?php echo $data->msPesertaPemilik; ?></td>
      <td ><?php echo $data->msPesertaPenanggungJawab; ?></td>
      <td ><?php echo $data->msPesertaAlamat; ?></td>
      <td ><?php echo $data->msPesertaNoHp; ?></td>
      <td >
        <center>
          <button class="btn btn-danger" id="hadir" style="<?php echo $konfirmasi; ?>"><p style="display: none;"><?php echo $data->msPesertaId; ?></p>-Hadir</button></td>
        </center>
      <td>
        <center> 
        <a id="cetak" style='<?php echo $cetak; ?>' href="<?php echo base_url("cetaksertifikat?id_peserta=$data->msPesertaId"); ?>"  target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Sertifikat</a>
        </a>
      </center>
      </td>
    </tr>
    <?php
    $no++;
  }
?>