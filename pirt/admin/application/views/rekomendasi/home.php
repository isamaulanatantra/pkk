<style>
  .box-form {
    background-color: white;
    margin-bottom: 15px;
    margin-left: 5px;
    margin-right: 5px;
  }

  .modal-dialog {
    width: 80%;
    height: 80%;
    margin: 10;
    padding: 10;
  }

  .modal-content {
    height: auto;
    min-height: 100%;
    border-radius: 0;
  }

</style>

<div class="msg" style="display:visible;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>


<div class="pendaftaran" id="pendaftaran" name="pendaftaran">
<div class="row">
  <div  class="col-md-12">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="glyphicon glyphicon-calendar"></i>
          <span><label>LISTING DATA PENDAFTARAN</label>            
        </div>        
      </div>

      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;">ID</th>
                <th style="display: none;">Id</th>
                <th style="text-align: center;">NIK</th>
                <th style="text-align: center;">Nama Perusahaan</th>
                <th style="text-align: center;">Pemilik</th>
                <th style="text-align: center;">Penanggung Jawab</th>
                <th style="text-align: center;">Alamat</th>
                <th style="text-align: center;">No Hp</th>
                <th style="text-align: center;">NIB</th>
                <!-- <th style="text-align: center;">Wilayah Kerja</th> -->
                <th style="text-align: center;">Cetak Rekomendasi</th>
              </tr>
            </thead>
            <tbody id="tblUtama">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>
  </div>

  
</div>
</div>

<script type="text/javascript">
  tampil();
  
  function tampil() {
    $.get('<?php echo base_url('Cetak_rekomendasi/tampilPendaftaran'); ?>', function(data) {
      MyTable.fnDestroy();
      $('#tblUtama').html(data);
      refresh();
    });
  }  
</script>
