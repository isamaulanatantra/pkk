<style>
  .box-form {
    background-color: white;
    margin-bottom: 15px;
    margin-left: 5px;
    margin-right: 5px;
  }
</style>

<div class="msg" style="display:visible;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="waktu" id="waktu" name="waktu">
<div class="row">
  <div  class="col-md-8">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="glyphicon glyphicon-calendar"></i>
          <span><label>LISTING JADWAL PENYULUHAN</label>            
        </div>        
      </div>

      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;">Id</th>
                <th style="display: none;">Id</th>
                <th style="text-align: center;">Tanggal Pelatihan</th>
                <th style="text-align: center;">Waktu</th>
                <th style="text-align: center;">Tempat Pelatihan</th>
                <th style="text-align: center;">Keterangan</th>
                <th style="display: none;">Peserta Maks</th>
                <th style="text-align: center;">Jml Peserta</th>
                <th style="text-align: center;">Aksi</th>
              </tr>
            </thead>
            <tbody id="tblUtama">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>
  </div>

  <div  class="col-md-4">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="glyphicon glyphicon-calendar"></i>
          <span><label>FORM INPUT JADWAL</label></label></span>
        </div>        
      </div>
      <div class="box-form">
        <!-- <form id="form-tambah-user" method="POST"> -->
            <div class="form-group" style="display: none;">
              <label for="id_jadwal">Id Jadwal</label>
              <input class="form-control" id="id_jadwal" name="id_jadwal" value="" placeholder="Id Grup" type="text" required="">
            </div> 

            <div class="form-group">
              <label for="tgl_jadwal">Tanggal Pelatihan</label>
              <input type="text" class="form-control tgl_jadwal" name="tgl_jadwal" id="tgl_jadwal" placeholder="" aria-describedby="sizing-addon2" value="">
            </div> 

            <div class="form-group">
              <label for="waktu_jadwal">Waktu</label>
              <input class="form-control" id="waktu_jadwal" name="waktu_jadwal" value="" placeholder="Waktu" type="text" required="">
            </div>

            <div class="form-group">
              <label for="tempat_jadwal">Tempat</label>
              <input class="form-control" id="tempat_jadwal" name="tempat_jadwal" value="" placeholder="Tempat" type="text" required="">
            </div>

            <div class="form-group">
              <label for="jml_peserta">Jumlah Maksimal Peserta</label>
              <input class="form-control" id="jml_peserta" name="jml_peserta" value="-" placeholder="Keterangan" type="number" required="" >
            </div> 

            <div class="form-group">
              <label for="ket_jadwal">Keterangan</label>
              <input class="form-control" id="ket_jadwal" name="ket_jadwal" value="-" placeholder="Keterangan" type="text" required="" >
            </div> 
           
            <center>
              <div class="form-group" id="formBtnSimpan" name="formBtnSimpan">
                
                <button type="submit" class="btn btn-primary" id="btnSimpan" name="btnSimpan"><i class="fa fa-save"></i>&nbsp;&nbsp;SIMPAN</button>  
                <button type="submit" class="btn btn-success" id="btnBaru" name="btnBaru"><i class="fa fa-plus"></i>&nbsp;&nbsp;BARU</button>        
              </div>

              <div class="form-group" style="display: none;" id="formBtnUpdate" name="formBtnUpdate">
                <button type="submit" class="btn btn-warning" id="btnBatal" name="btnBaru"><i class="fa fa-minus-square-o"></i>&nbsp;&nbsp;BATAL</button>
                <button type="submit" class="btn btn-primary" id="btnUpdate" name="btnSimpan"><i class="fa fa-save"></i>&nbsp;&nbsp;UPDATE</button>          
                <button type="submit" class="btn btn-danger" id="btnHapus" name="btnBaru"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;HAPUS</button>
              </div>
            </center>
                                  
            <br>  
        <!-- </form>       -->
      </div>  
    </div>
  </div>
</div>
</div>

<script type="text/javascript">
  tampil();
  baru();

  $(document).ready(function () {
      $('.tgl_jadwal').datepicker({
          format: "yyyy-mm-dd",
          autoclose:true,
      });
  });

  $(function () {
      $(".select2").select2();

      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
      });
  });

  function tampil() {
    $.get('<?php echo base_url('Jadwal_penyuluhan/tampil'); ?>', function(data) {
      MyTable.fnDestroy();
      $('#tblUtama').html(data);
      refresh();
    });
  }
  
  function baru() {
      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
      var yyyy = today.getFullYear();

      today = yyyy + '-' + mm + '-' + dd;
      // document.write(today);

      $('#id_jadwal').val("");
      $('#tgl_jadwal').val(today);
      $('#waktu_jadwal').val("");
      $('#tempat_jadwal').val("");
      $('#ket_jadwal').val("-");
      $('#jml_peserta').val("");
      
      $('#formBtnSimpan').show();

      $('#formBtnUpdate').hide();
      $('#lblsandi').hide();           
      
  }
</script>

<script>
    $('#btnBaru').on("click", function(){
      baru();
    });
</script>

<script>
    $('#btnSimpan').on("click", function(){
      var tgl_jadwal = $('#tgl_jadwal').val();
      var waktu_jadwal = $('#waktu_jadwal').val();
      var tempat_jadwal = $('#tempat_jadwal').val();
      var ket_jadwal = $('#ket_jadwal').val();
      var jml_peserta = $('#jml_peserta').val();
            
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            waktu_jadwal:waktu_jadwal,
            tgl_jadwal:tgl_jadwal,
            tempat_jadwal:tempat_jadwal,
            ket_jadwal:ket_jadwal,
            jml_peserta:jml_peserta
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Jadwal_penyuluhan/insert',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              tampil();
              baru();             
            }
          }
      });
    });
</script>

<script>
  $('#tblUtama').on('click','tr',function(){
            var id = $(this).find('td:eq(1)').text();
            var tgl=$(this).find('td:eq(2)').text();
            var waktu=$(this).find('td:eq(3)').text(); 
            var tempat_jadwal = $(this).find('td:eq(4)').text();       
            var ket=$(this).find('td:eq(5)').text();
            var maks=$(this).find('td:eq(6)').text();

            $('#formBtnSimpan').hide();
            $('#formBtnUpdate').show();
                        
            $('#id_jadwal').val(id);
            $('#tgl_jadwal').val(tgl);
            $('#waktu_jadwal').val(waktu);
            $('#tempat_jadwal').val(tempat_jadwal);
            $('#ket_jadwal').val(ket);
            $('#jml_peserta').val(maks);
      });

</script>

<script>
  $('#btnBatal').on("click", function(){
    baru();
  });
</script>

<script>
    $('#btnUpdate').on("click", function(){
      var id_jadwal = $('#id_jadwal').val();
      var tgl_jadwal = $('#tgl_jadwal').val();
      var waktu_jadwal = $('#waktu_jadwal').val();
      var tempat_jadwal = $('#tempat_jadwal').val();
      var ket_jadwal = $('#ket_jadwal').val();
      var jml_peserta = $('#jml_peserta').val();
      
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_jadwal:id_jadwal,
            tgl_jadwal:tgl_jadwal,
            waktu_jadwal:waktu_jadwal,
            tempat_jadwal:tempat_jadwal,
            ket_jadwal:ket_jadwal,
            jml_peserta:jml_peserta
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Jadwal_penyuluhan/update',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal diupdate");

            }else{
              alertify.success("Data berhasil diupdate");
              tampil();
              baru()
            }
          }
      });
    });
</script>

<script>
    $('#btnHapus').on("click", function(){
      var id_jadwal = $('#id_jadwal').val();
      
      alertify.confirm("Apakah anda yakin hapus data ini?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_jadwal:id_jadwal
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Jadwal_penyuluhan/hapus',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal dihapus");

            }else{
              alertify.success("Data berhasil dihapus");
              tampil();
              baru();
            }
          }
      });
        } else {
          alertify.error("Batal hapus data");
        }
      });
      return false;
      
      
    });
</script>

<div class="peserta" id="peserta" name="peserta" style="display: none;">
<div class="row">
  <div  class="col-md-8">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <div class="col-md-10">
            <div class="form-group" style="display: none;">
              <input class="form-control" id="" name="" value="" placeholder="Tempat" type="text" required="">
            </div>
          </div>
          <div class="col-md-2">
            <label for="tgl_jadwal">&nbsp;</label>
            <button class="btn btn-warning" id="selesai" name="selesai" ><i class="glyphicon glyphicon-remove"></i>&nbsp;SELESAI
          </div>

          <i class="fa fa-user"></i>
          <span><label>LISTING DATA</label> 
            <!-- <label id="judul2"> </label></span> -->
        </div>        
      </div>

      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data2" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;">NO</th>
                <th style="display: none;">Id</th>
                <th style="text-align: center;">No KTP</th>
                <th style="text-align: center;">Nama Perusahaan</th>
                <th style="text-align: center;">Nama Pemilik</th>
                <th style="text-align: center;">Nama Penggung Jawab</th>
                <th style="text-align: center;">Alamat</th>
                <th style="text-align: center;">No Telp.</th>
                <th style="text-align: center;">Aksi</th>
              </tr>
            </thead>
            <tbody id="tblPeserta">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>
  </div>

  <div  class="col-md-4">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          
          <span><label>FORM INPUT PESERTA</label></label></span>
        </div>        
      </div>
      <div class="box-form">
        <!-- <form id="form-tambah-user" method="POST"> -->
            <div class="form-group" style="display: none;">
              <label for="id_peserta">Id Peserta</label>
              <input class="form-control" id="id_peserta" name="id_peserta" value="" placeholder="Id Peserta" type="text" required="">
            </div> 

            <div class="form-group" style="display: none;">
              <label for="id_jadwal_peserta">Id Jadwal Peserta</label>
              <input class="form-control" id="id_jadwal_peserta" name="id_jadwal_peserta" value="" placeholder="Id Jadwal Peserta" type="text" required="">
            </div>

            <div class="form-group">
              <label for="nik">No KTP</label>
              <input class="form-control" id="nik" name="nik" value="" placeholder="No KTP" type="text" required="" >
            </div> 

            <div class="form-group">
              <label for="nama_perusahaan">Nama Perusahaan / Merk</label>
              <input class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="" placeholder="Nama Perusahaan / Merk" type="text" required="" >
            </div> 

            <div class="form-group">
              <label for="nama_pemilik">Nama Pemilik</label>
              <input class="form-control" id="nama_pemilik" name="nama_pemilik" value="" placeholder="Nama Pemilik" type="text" required="" >
            </div> 

            <div class="form-group">
              <label for="nama_penanggung_jawab">Nama Penanggung Jawab</label>
              <input class="form-control" id="nama_penanggung_jawab" name="nama_penanggung_jawab" value="" placeholder="Nama Penanggung Jawab" type="text" required="" >
            </div> 

            <div class="form-group">
              <label for="alamat_penanggung_jawab">Alamat Penanggung Jawab</label>
              <textarea class="form-control" rows="5" id="alamat_penanggung_jawab" name="alamat_penanggung_jawab" value="" placeholder="Alamat Penanggung Jawab" type="text" required="" ></textarea>
            </div> 

            <div class="form-group">
              <label for="no_telp">No Telp.</label>
              <input class="form-control" id="no_telp" name="no_telp" value="" placeholder="No Telp." type="text" required="" >
            </div> 

            <center>
              <div class="form-group" id="formBtnSimpanPeserta" name="formBtnSimpanPeserta">
                
                <button type="submit" class="btn btn-primary" id="btnSimpanPeserta" name="btnSimpanPeserta"><i class="fa fa-save"></i>&nbsp;&nbsp;SIMPAN</button>  
                <button type="submit" class="btn btn-success" id="btnBaruPeserta" name="btnBaruPeserta"><i class="fa fa-plus"></i>&nbsp;&nbsp;BARU</button>        
              </div>

              <div class="form-group" style="display: none;" id="formBtnUpdatePeserta" name="formBtnUpdatePeserta">
                <button type="submit" class="btn btn-warning" id="btnBatalPeserta" name="btnBatalPeserta"><i class="fa fa-minus-square-o"></i>&nbsp;&nbsp;BATAL</button>
                <button type="submit" class="btn btn-primary" id="btnUpdatePeserta" name="btnUpdatePeserta"><i class="fa fa-save"></i>&nbsp;&nbsp;UPDATE</button>          
                <button type="submit" class="btn btn-danger" id="btnHapusPeserta" name="btnHapusPeserta"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;HAPUS</button>
              </div>
            </center>
                                  
            <br>  
        <!-- </form>       -->
      </div>  
    </div>
  </div>
</div>
</div>



<script>
  $('#selesai').on('click',function(){
    $('#waktu').show(); 
    $('#peserta').hide();     
    $('#id_jadwal_peserta').val("");
    baru();
  });
</script>

<script>
  function baruPeserta(){

    $("#id_peserta").val("");
    $("#nik").val("");
    $("#nama_perusahaan").val("");
    $("#nama_pemilik").val("");
    $("#nama_penanggung_jawab").val("");
    $("#alamat_penanggung_jawab").val("");
    $("#no_telp").val("");   
    

    $('#formBtnSimpanPeserta').show();
    $('#formBtnUpdatePeserta').hide();
    // id_peserta
    // id_jadwal_peserta
    // nama_perusahaan
    // nama_pemilik
    // nama_penanggung_jawab
  }
</script>

<script>
  function tampilPeserta(id_jadwal) {    
    $.get('<?php echo base_url();?>Jadwal_penyuluhan/tampilPeserta?id_jadwal='+id_jadwal, function(data) {
      MyTable2.fnDestroy();
      $('#tblPeserta').html(data);
      refresh();
    });
  }
</script>

<script>
  $('#tblUtama').on('click','td:eq(0)',function(){
    $('#waktu').hide(); 
    $('#peserta').show(); 
    
    var id_jadwal = $( this ).text();   
    $('#id_jadwal_peserta').val(id_jadwal);
    tampilPeserta(id_jadwal);  
    baruPeserta();   
  });
</script>

<script>
    $('#btnBaruPeserta').on("click", function(){
      baruPeserta();
    });
</script>

<script>
    $('#btnSimpanPeserta').on("click", function(){
      var id_peserta = $("#id_peserta").val();
      var nik = $("#nik").val();
      var id_jadwal_peserta = $("#id_jadwal_peserta").val();
      var nama_perusahaan = $("#nama_perusahaan").val();
      var nama_pemilik = $("#nama_pemilik").val();
      var nama_penanggung_jawab = $("#nama_penanggung_jawab").val();
      var alamat_penanggung_jawab = $("#alamat_penanggung_jawab").val();
      var no_telp = $("#no_telp").val();
            
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            nik:nik,
            id_jadwal_peserta:id_jadwal_peserta,
            nama_perusahaan:nama_perusahaan,
            nama_pemilik:nama_pemilik,
            nama_penanggung_jawab:nama_penanggung_jawab,
            alamat_penanggung_jawab:alamat_penanggung_jawab,
            no_telp:no_telp
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Jadwal_penyuluhan/insertPeserta',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              tampilPeserta(id_jadwal_peserta);
              baruPeserta();             
            }
          }
      });
    });
</script>

<script>
  $('#tblPeserta').on('click','tr',function(){
            var id_peserta = $(this).find('td:eq(1)').text();
            var nik=$(this).find('td:eq(2)').text(); 
            var nama_perusahaan=$(this).find('td:eq(3)').text();
            var nama_pemilik=$(this).find('td:eq(4)').text();
            var nama_penanggung_jawab=$(this).find('td:eq(5)').text();
            var alamat_penanggung_jawab=$(this).find('td:eq(6)').text();
            var no_telp=$(this).find('td:eq(7)').text();

            $('#formBtnSimpanPeserta').hide();
            $('#formBtnUpdatePeserta').show();            
                        
            $('#id_peserta').val(id_peserta);
            $('#nik').val(nik);
            $('#nama_perusahaan').val(nama_perusahaan);
            $('#nama_pemilik').val(nama_pemilik);
            $('#nama_penanggung_jawab').val(nama_penanggung_jawab);
            $('#alamat_penanggung_jawab').val(alamat_penanggung_jawab);
            $('#no_telp').val(no_telp);
      });

</script>

<script>
  $('#btnBatalPeserta').on("click", function(){
    baruPeserta();
  });
</script>

<script>
    $('#btnUpdatePeserta').on("click", function(){
      var id_peserta = $("#id_peserta").val();
      var nik = $("#nik").val();
      var id_jadwal_peserta = $("#id_jadwal_peserta").val();
      var nama_perusahaan = $("#nama_perusahaan").val();
      var nama_pemilik = $("#nama_pemilik").val();
      var nama_penanggung_jawab = $("#nama_penanggung_jawab").val();
      var alamat_penanggung_jawab = $("#alamat_penanggung_jawab").val();
      var no_telp = $("#no_telp").val();
      
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_peserta:id_peserta,
            nik:nik,
            id_jadwal_peserta:id_jadwal_peserta,
            nama_perusahaan:nama_perusahaan,
            nama_pemilik:nama_pemilik,
            nama_penanggung_jawab:nama_penanggung_jawab,
            alamat_penanggung_jawab:alamat_penanggung_jawab,
            no_telp:no_telp
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Jadwal_penyuluhan/updatePeserta',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal diupdate");

            }else{
              alertify.success("Data berhasil diupdate");
              tampilPeserta(id_jadwal_peserta);
              baruPeserta();  
            }
          }
      });
    });
</script>

<script>
    $('#btnHapusPeserta').on("click", function(){
      var id_peserta = $('#id_peserta').val();
      var id_jadwal_peserta = $("#id_jadwal_peserta").val();
      
      alertify.confirm("Apakah anda yakin hapus data ini?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_peserta:id_peserta
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Jadwal_penyuluhan/hapusPeserta',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal dihapus");

            }else{
              alertify.success("Data berhasil dihapus");
              tampilPeserta(id_jadwal_peserta);
              baruPeserta();
            }
          }
      });
        } else {
          alertify.error("Batal hapus data");
        }
      });
      return false;
      
      
    });
</script>