<?php
  $no = 1;  
  foreach ($dataMenu as $data) {
    $parent = $this->Rolemenu_model->getParentById($data->msMenuParent);  
    $aktif = $this->Rolemenu_model->getAktif($data->msMenuId, $dataId);
    ?>    
    <tr>
      <td width="5%"><?php echo $no; ?></td>
      <td width="30%"><?php echo $data->msMenuNama; ?></td>      
      <td width="30%"><?php echo $parent; ?></td>      
      <td class="text-center" style="min-width:20%px;">
          <?php 
            
            if ($aktif == 1) {
                ?>
                  <label class="switch">
                    <input type="checkbox" checked>
                    <span class="slider round"></span>
                  </label>
                <?php 
            }elseif ($aktif == 0) {
              ?>
                  <label class="switch">
                    <input type="checkbox" >
                    <span class="slider round"></span>
                  </label>
              <?php 
            }
          ?>
      </td>
    </tr>
    <?php
    $no++;
  }
?>
