<style>
  .box-form {
    background-color: white;
    margin-bottom: 15px;
    margin-left: 5px;
    margin-right: 5px;
  }
</style>

<div class="msg" style="display:visible;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="sppirt" id="sppirt">
  <div class="box-form">
    <div class="box-content no-padding table-responsive">
      <table id="list-data" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th style="text-align: center;">ID</th>
            <th style="display: none;">Id</th>
            <th style="text-align: center;">NIK</th>
            <th style="text-align: center;">Nama Perusahaan</th>
            <th style="text-align: center;">Pemilik</th>
            <th style="text-align: center;">Penanggung Jawab</th>
            <th style="text-align: center;">Alamat</th>
            <th style="text-align: center;">No Hp</th>
            <th style="text-align: center;">NIB</th>
            <th style="text-align: center;">Produk</th>
            <th style="text-align: center;">Konfirmasi Kelangkapan</th>
            <th style="text-align: center;">Status</th>
            <th style="text-align: center;">Kunjungan</th>
          </tr>
        </thead>
        <tbody id="tblUtama">            
        </tbody>
      </table>
    </div>
    <br>
  </div>
</div>


<script>
  $(function () {
      $(".select2").select2();

      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
      });
  });
</script>

<script>
  function tampil() {
    $.get('<?php echo base_url('Verifikasi_ijin/tampilIjin'); ?>', function(data) {
      MyTable.fnDestroy();
      $('#tblUtama').html(data);
      refresh();
    });
  } 
</script>

<script>
	tampil();
</script>

<div class="produk" id="produk" name="produk" style="display: none;">
<div class="row">
  <div  class="col-md-8">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <div class="col-md-10">
            <div class="form-group" style="display: none;">
              <input class="form-control" id="id_pirt" name="id_pirt" value="" placeholder="" type="text" required="">
            </div>
          </div>
          <div class="col-md-2">
            <label for="tgl_jadwal">&nbsp;</label>
            <button class="btn btn-warning" id="selesai" name="selesai" ><i class="glyphicon glyphicon-remove"></i>&nbsp;SELESAI
          </div>

          <i class="fa fa-user"></i>
          <span><label>LISTING PRODUK</label> 
            <!-- <label id="judul2"> </label></span> -->
        </div>        
      </div>

      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data3" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;">No</th>
                <th style="display: none;">Id</th>
                <th style="display: none;">Id IRT</th>
                <th style="text-align: center;">Nama Produk</th>
                <th style="display: none;">Kode Kemasan</th>
                <th style="display: none;">Jenis Kemasan</th>
                <th style="text-align: center;">Kemasan Primer</th>
                <th style="display: none;">Kode Jenis Pangan</th>
                <th style="text-align: center;">N0. PIRT</th>
                <th style="display: none;">Mas Berlaku</th>
                <th style="text-align: center;">Masa berlaku SPP-IRT</th>
                <th style="display: none;">Lampiran</th>
                <th style="display: none;">Lampiran</th>
                <th style="display: none;">Lampiran</th>
                <th style="text-align: center;">Status</th>                
              </tr>
            </thead>
            <tbody id="tblProduk">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>
  </div>

  <div  class="col-md-4">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          
          <span><label>FORM TAMBAH PRODUK</label></label></span>
        </div>        
      </div>
      <div class="box-form">
        <!-- <form id="form-tambah-user" method="POST"> -->
            <div class="form-group" style="display: visible;">
              <label for="id_produk">Id Produk</label>
              <input class="form-control" id="id_produk" name="id_produk" value="" placeholder="Id Produk" type="text" required="">
            </div> 

            <div class="form-group" style="display: visible;">
              <label for="id_pirt2">Id PIRT</label>
              <input class="form-control" id="id_pirt2" name="id_pirt2" value="" placeholder="Id PIRT" type="text" required="">
            </div>

            <div class="form-group">
              <label for="nama_produk">Nama Produk</label>
              <input class="form-control" id="nama_produk" name="nama_produk" value="" placeholder="Nama Produk" type="text" required="" >
            </div>       

            <div class="form-group">
              <label for="jenis_kemasan">Jenis Kemasan</label>
              <select id="jenis_kemasan" name="jenis_kemasan" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%;">        
                <?php
                foreach ($dataKemasan as $data) {
                  ?>
                  <option value="<?php echo $data->msKemasanKode; ?>">
                    <?php echo $data->msKemasanNama; ?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </div>      

            <div class="form-group">
              <label for="kemasan_primer">Kemasan Primer</label>
              <input class="form-control" id="kemasan_primer" name="kemasan_primer" value="" placeholder="Kemasan Primer" type="text" required="" >
            </div>       

            <div class="form-group">
              <label for="jenis_pangan">Jenis Pangan</label>
              <select id="jenis_pangan" name="jenis_pangan" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%;">        
                <?php
                foreach ($dataPangan as $data) {
                  ?>
                  <option value="<?php echo $data->msJenisPanganKode; ?>">
                    <?php echo $data->msJenisPanganNama; ?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </div>   

            <div class="form-group">
              <label for="masa_berlaku">Masa Berlaku</label>
              <select id="masa_berlaku" name="masa_berlaku" class="form-control" aria-describedby="sizing-addon2" style="width: 100%;">        
                <option value="5">5 Tahun</option>
                <option value="4">4 Tahun</option>
                <option value="3">3 Tahun</option>
                <option value="2">2 Tahun</option>
                <option value="1">1 Tahun</option>
              </select>
            </div>   

            <div class="form-group" style="display: none;">
              <label for="lampiran">Lampiran</label>
              <input class="form-control" id="lampiran" name="lampiran" value="" placeholder="Lampiran" type="text" >
              <input class="form-control" id="lampiranBagan" name="lampiranBagan" value="" placeholder="LampiranBagan" type="text" >
            </div>           

            <center>
            	<div class="form-group" id="formBtnLampiran" style="display: none;">
            		<button type="submit" class="btn btn-primary" id="btnLampiran" name="btnLampiran"><i class="fa fa-file"></i>&nbsp;&nbsp;Lihat Label</button>
                <button type="submit" class="btn btn-primary" id="btnLampiranBagan" name="btnLampiranBagan"><i class="fa fa-file"></i>&nbsp;&nbsp;Lihat Bagan</button>
            	</div>               

				<div class="form-group" id="formbtnVerifikasi" name="formbtnVerifikasi" style="display: none;">
					<button type="submit" class="btn btn-success" id="btnVerifikasi" name="btnVerifikasi"><i class="glyphicon glyphicon-ok"></i>&nbsp;&nbsp;Verifikasi</button>          
					<button type="submit" class="btn btn-danger" id="btnTidakVerifikasi" name="btnTidakVerifikasi"><i class="glyphicon glyphicon-remove"></i>&nbsp;&nbsp;Tidak Verivikasi</button>
					<button type="submit" class="btn btn-warning" id="btnBatalVerivikasi" name="btnBatalVerivikasi"><i class="fa fa-minus-square-o"></i>&nbsp;&nbsp;BATAL</button>
				</div>
            </center>

            <div class="form-group" id="formAlasan" style="display: none;">
				<label for="lampiran">Masukan alasan tidak di verifikasi</label>
				<input class="form-control" id="alasan" name="alasan" placeholder="Lampiran" type="text" >
				<br>
        		<button type="submit" class="btn btn-primary" id="btnSimpanALasan" name="btnSimpanALasan"><i class="fa fa-save"></i>&nbsp;&nbsp;Simpan</button>
        		<button type="submit" class="btn btn-warning" id="btnBatalAlasan" name="btnBatalAlasan"><i class="fa fa-minus-square-o"></i>&nbsp;&nbsp;BATAL</button>
        	</div>
                                  
            <br>  
        <!-- </form>       -->
      </div>  
    </div>
  </div>
</div>
</div>

<script>
  function tampilProduk(id_pirt) {    
    $.get('<?php echo base_url();?>Verifikasi_ijin/tampilProduk?id_pirt='+id_pirt, function(data) {
      MyTable3.fnDestroy();
      $('#tblProduk').html(data);
      refresh();
    });
  }
</script>

<script>
	function baru(){
		$('#id_produk').val('');
        $('#id_pirt2').val('');
        $('#nama_produk').val('');
        $('#jenis_kemasan').val('1').trigger('change');
        $('#kemasan_primer').val('');
        $('#jenis_pangan').val('01').trigger('change');
        $('#masa_berlaku').val('5').trigger('change');        
        $('#lampiran').val('');

        $('#formbtnVerifikasi').hide();
        $('#formBtnLampiran').hide();
        $('#formAlasan').hide();
        
	}
</script>

<script>
  $('#tblUtama').on('click','#lengkap',function(){    
      var id = $( this ).text();   
      var str_id_peserta = id.split("-");

      var str_id_peserta = str_id_peserta[0];
      var id_peserta = str_id_peserta.trim();    
      
      alertify.confirm("Apakah persyaratan sudah lengkap?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_peserta:id_peserta
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Verifikasi_ijin/lengkap',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Verifikasi gagal");

            }else{
              alertify.success("Verifikasi berhasil");              
              tampil();
            }
          }
      });
        } else {
          alertify.error("Batal Verifikasi");
        }
      });
      return false;

  });
</script>

<script>
  $('#tblUtama').on('click','#kunjungn',function(){    
      var id = $( this ).text();   
      var str_id_peserta = id.split("-");

      var str_id_peserta = str_id_peserta[0];
      var id_peserta = str_id_peserta.trim();    
      
      alertify.confirm("Apakah kunjungan sudah dilakukan?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_peserta:id_peserta
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Verifikasi_ijin/selesaiKunjungan',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Verifikasi gagal");

            }else{
              alertify.success("Verifikasi berhasil");              
              tampil();
            }
          }
      });
        } else {
          alertify.error("Batal Verifikasi");
        }
      });
      return false;

  });
</script>


<script>
  $('#tblUtama').on('click','#list',function(){    
      var id = $( this ).text();   
      var str_id_peserta = id.split("-");

      var str_id_peserta = str_id_peserta[0];
      var id_peserta = str_id_peserta.trim();      
      
      $('#sppirt').hide();
      $('#produk').show();     

      tampilProduk(id_peserta);
  });
</script>

<script>
	$('#selesai').on('click', function(){
		window.location.href = "<?php echo base_url('Verifikasi_ijin'); ?>";     
	});
</script>

<script>
  $('#tblProduk').on('click','tr',function(){
        var id_produk = $(this).find('td:eq(1)').text();
        var id_pirt=$(this).find('td:eq(2)').text(); 
        var nama=$(this).find('td:eq(3)').text();
        var kode_kemasan=$(this).find('td:eq(4)').text();
        var kemasan_primer=$(this).find('td:eq(5)').text();
        var kode_jenis=$(this).find('td:eq(6)').text();
        var masa_berlaku=$(this).find('td:eq(8)').text();
        var lampiran=$(this).find('td:eq(10)').text();
        var lampiranBagan=$(this).find('td:eq(11)').text();
        
                    
        $('#id_produk').val(id_produk);
        $('#id_pirt2').val(id_pirt);
        $('#nama_produk').val(nama);
        $('#jenis_kemasan').val(kode_kemasan).trigger('change');
        $('#kemasan_primer').val(kemasan_primer);
        $('#jenis_pangan').val(kode_jenis).trigger('change');
        $('#masa_berlaku').val(masa_berlaku).trigger('change');
        $('#lampiran').val(lampiran);
        $('#lampiranBagan').val(lampiranBagan);

        $('#formbtnVerifikasi').show();
        $('#formBtnLampiran').show();

        
  });
</script>

<script>
	$('#btnBatalVerivikasi').on('click',function(){
		baru();
	});
</script>

<script>
	$('#btnLampiran').on('click',function(){
		var lampiran = document.getElementById('lampiran').value;
		window.open('<?php echo base_url(); ?>gambar/'+lampiran,'_blank');
	});
</script>

<script>
  $('#btnLampiranBagan').on('click',function(){
    var lampiranBagan = document.getElementById('lampiranBagan').value;
    window.open('<?php echo base_url(); ?>gambar/'+lampiranBagan,'_blank');
  });
</script>

<script>
	$('#btnVerifikasi').on('click', function(){
		var id_produk = $("#id_produk").val(); 
		var id_peserta = $("#id_pirt2").val();      
      
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_produk:id_produk,
            id_peserta:id_peserta

          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Verifikasi_ijin/verifikasi',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal diupdate");

            }else if( text == 2 ){
              alertify.success("Data berhasil diupdate");
              baru();
              tampilProduk(id_peserta);                
            }
          }
      });
	});
</script>

<script>
	$('#btnTidakVerifikasi').on('click', function(){
		$('#formAlasan').show();
	});
</script>

<script>
	$('#btnBatalAlasan').on('click',function(){
		baru();
	});
</script>

<script>
	$('#btnSimpanALasan').on('click', function(){
		var id_produk = $("#id_produk").val(); 
		var id_peserta = $("#id_pirt2").val(); 
		var alasan = $("#alasan").val();      
      
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_produk:id_produk,
            alasan:alasan
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Verifikasi_ijin/tidakVerifikasi',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal diupdate");

            }else if( text == 2 ){
              alertify.success("Data berhasil diupdate");
              baru();
              tampilProduk(id_peserta);                
            }
          }
      });
	});
</script>