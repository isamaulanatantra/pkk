<script type="text/javascript">
	var MyTable = $('#list-data').dataTable({
		  "paging": true,
		  "lengthChange": true,
		  "searching": true,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});

	var MyTable2 = $('#list-data2').dataTable({
		  "paging": true,
		  "lengthChange": true,
		  "searching": true,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});

	var MyTable3 = $('#list-data3').dataTable({
		  "paging": true,
		  "lengthChange": true,
		  "searching": true,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});

	window.onload = function() {
		// tampilMenu();
		// tampilGrup();
		// tampilRoleMenu();

		// tampilUser();
		
		<?php
			if ($this->session->flashdata('msg') != '') {
				echo "effect_msg();";
			}
		?>
	}

	function refresh() {
		MyTable = $('#list-data').dataTable();
		MyTable2 = $('#list-data2').dataTable();
		MyTable3 = $('#list-data3').dataTable();
	}

	

	function effect_msg_form() {
		// $('.form-msg').hide();
		$('.form-msg').show(1000);
		// $('.form-msg').show(1000);
		// $('.form-msg').animate({
  //                       opacity: 1,
  //                       right: "50px",
  //                       bottom: "10px",
  //                       height: "toggle",
  //                   }, 2000, function() {}).css('position','fixed');
		setTimeout(function() { $('.form-msg').fadeOut(1000); }, 3000);
	}

	function effect_msg() {
		// $('.msg').hide();
		$('.msg').show(1000);
		$('.msg').animate({
                        opacity: 1,
                        right: "50px",
                        bottom: "10px",
                        height: "toggle",
                    }, 2000, function() {}).css('position','fixed'); 

		setTimeout(function() { $('.msg').fadeOut(1000); }, 3000);
	}		
	
	
</script>

<!-- form  grup menu -->
<script type="text/javascript">
	function tampilGrup() {
		$.get('<?php echo base_url('Grup/tampil'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#tblUtamaGrup').html(data);
			refresh();
		});
	}	

	$('#form-tambah-grup').submit(function(e) {
		var data = $(this).serialize();

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('grup/prosesTambah'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			tampilGrup();
			if (out.status == 'form') {
				$('.form-msg').html(out.msg);
				effect_msg_form();
			} else {
				document.getElementById("form-tambah-grup").reset();
				$('#tambah-grup-menu').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			}
		})
		
		e.preventDefault();
	});

	$(document).on("click", ".update-dataGrup", function() {
		var id = $(this).attr("data-id");
		
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('Grup/update'); ?>",
			data: "id=" +id
		})
		.done(function(data) {
			$('#tempat-modal').html(data);
			$('#update-grup').modal('show');
		})
	})

	$(document).on('submit', '#form-update-grup', function(e){
		var data = $(this).serialize();

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('Grup/prosesUpdate'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			tampilGrup();
			if (out.status == 'form') {
				$('.form-msg').html(out.msg);
				effect_msg_form();
			} else {
				document.getElementById("form-update-grup").reset();
				$('#update-grup').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			}
		})
		
		e.preventDefault();
	});

	var id_grup;
	$(document).on("click", ".konfirmasiHapus-grup", function() {
		id_grup = $(this).attr("data-id");
	})
	$(document).on("click", ".hapus-dataGrup", function() {
		var id = id_grup;
		
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('Grup/delete'); ?>",
			data: "id=" +id
		})
		.done(function(data) {
			$('#konfirmasiHapus').modal('hide');
			tampilGrup();
			$('.msg').html(data);
			effect_msg();
		})
	})
</script>
<!-- akhir form grup  menu -->

<!-- form menu -->
<script type="text/javascript">
	function tampilMenu() {
		$.get('<?php echo base_url('Menu/tampil'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#tblUtamaMenu').html(data);
			refresh();
		});
	}

	$('#form-tambah-menu').submit(function(e) {
		var data = $(this).serialize();

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('menu/prosesTambah'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			tampilMenu();
			if (out.status == 'form') {
				$('.form-msg').html(out.msg);
				effect_msg_form();
			} else {
				document.getElementById("form-tambah-menu").reset();
				$('#tambah-menu').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			}
		})
		
		e.preventDefault();
	});

	$(document).on("click", ".update-dataMenu", function() {
		var id = $(this).attr("data-id");
		
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('Menu/edit'); ?>",
			data: "id=" +id
		})
		.done(function(data) {
			$('#tempat-modal').html(data);
			$('#update-menu').modal('show');
		})
	})

	$(document).on('submit', '#form-update-menu', function(e){
		var data = $(this).serialize();

		$.ajax({
			method: 'POST',
			url: '<?php echo base_url('Menu/prosesUpdate'); ?>',
			data: data
		})
		.done(function(data) {
			var out = jQuery.parseJSON(data);

			tampilMenu();
			if (out.status == 'form') {
				$('.form-msg').html(out.msg);
				effect_msg_form();
			} else {
				document.getElementById("form-update-menu").reset();
				$('#update-menu').modal('hide');
				$('.msg').html(out.msg);
				effect_msg();
			}
		})
		
		e.preventDefault();
	});

	var id_menu;
	$(document).on("click", ".konfirmasiHapus-menu", function() {
		id_grup = $(this).attr("data-id");
	})
	$(document).on("click", ".hapus-dataMenu", function() {
		var id = id_grup;
		
		$.ajax({
			method: "POST",
			url: "<?php echo base_url('Menu/delete'); ?>",
			data: "id=" +id
		})
		.done(function(data) {
			$('#konfirmasiHapus').modal('hide');
			tampilMenu();
			$('.msg').html(data);
			effect_msg();
		})
	})

	// $('#tblUtamaMenu').on('click','tr',function(){
 //            var id = $(this).find('td:eq(1)').text();
 //            var parent = $(this).find('td:eq(2)').text();
            
 //            $('#nama_menu').val(id);
 //            document.getElementById("parent_menu").value = parent;
 //      });
	 
</script>
<!-- akhir form menu -->


<!-- form menu -->
<!-- <script type="text/javascript">
	function tampilRoleMenu() {
		$.get('<?php echo base_url('RoleMenu/tampil'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#tblUtamaRole').html(data);
			refresh();
		});
	}

	function tampilAktifMenu() {
		$.get('<?php echo base_url('RoleMenu/tampilaktif'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#tblAktifRole').html(data);
			refresh();
		});
	}

	$('#tblUtamaRole').on('click','tr',function(){
            var id = $(this).find('td:eq(0)').text();
            
            $.get('<?php echo base_url('RoleMenu/tampilaktif'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#tblAktifRole').html(data);
			refresh();
		});

            
      });

</script> -->
<!-- akhir form menu -->