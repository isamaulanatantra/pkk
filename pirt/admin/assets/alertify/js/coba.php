<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="alertify.core.css" />
	<link rel="stylesheet" href="alertify.default.css" id="toggleCSS" />
	<meta name="viewport" content="width=device-width">
</head>
<body>
	<a href="#" id="confirm">Confirm Dialog</a><br>
</body>
</html>
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="alertify.min.js"></script>
<script>
	function reset () {
			$("#toggleCSS").attr("href", "../themes/alertify.default.css");
			alertify.set({
				labels : {
					ok     : "OK",
					cancel : "Cancel"
				},
				delay : 5000,
				buttonReverse : false,
				buttonFocus   : "ok"
			});
		}

	$("#confirm").on( 'click', function () {
				alertify.error("You've clicked Cancel");
			// reset();
			// alertify.confirm("This is a confirm dialog", function (e) {
			// 	if (e) {
			// 		alertify.success("You've clicked OK");
			// 	} else {
			// 		alertify.error("You've clicked Cancel");
			// 	}
			// });
			// return false;
		});
</script>