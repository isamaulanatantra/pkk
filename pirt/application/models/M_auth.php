<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_auth extends CI_Model {
	public function login($user, $pass) {
		$this->db->select('*');
		$this->db->from('msadmin');
		$this->db->where('msAdminUsername', $user);
		$this->db->where('msAdminSandi', md5($pass));
		$this->db->where('status !=', 99);

		$data = $this->db->get();

		if ($data->num_rows() == 1) {
			return $data->row();
		} else {
			return false;
		}
	}

	public function login_user($nik, $no_hp) {
		$this->db->select('*');
		$this->db->from('msuser');
		$this->db->where('msUserNik', $nik);
		$this->db->where('msUserNoHp', $no_hp);
		$this->db->where('status', 2);

		$data = $this->db->get();

		if ($data->num_rows() == 1) {
			return $data->row();
		} else {
			return false;
		}
	}
}

/* End of file M_auth.php */
/* Location: ./application/models/M_auth.php */