<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grup_model extends CI_Model {
	public function select_all() {
		$this->db->select('*');
		$this->db->from('msgrupmenu');
		$this->db->where('status != 99');
		$data = $this->db->get();

		return $data->result();
	}		

	public function insert($data_input, $table_name) {
		$this->db->insert( $table_name, $data_input );	
		return $this->db->affected_rows();
	}	

	function update($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
}
