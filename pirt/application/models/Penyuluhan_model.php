<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penyuluhan_model extends CI_Model {
	
	public function select_by_id($user) {
		$this->db->select('*');
		$this->db->from('mspeserta');
		$this->db->where('msPesertaUserId',$user);
		$this->db->where('status != 99');
		$data = $this->db->get();

		return $data->result();
	}

	public function total($user) {
		$this->db->select('msPesertaNik');
		$this->db->from('mspeserta');
		$this->db->where('msPesertaUserId',$user);
		$this->db->where('status != 99');
		$data = $this->db->get();
		$total = $data->num_rows();
		return $total;
	}	

	function update($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}

	public function insert($data_input, $table_name) {
		$this->db->insert( $table_name, $data_input );	
		return $this->db->affected_rows();
	}
	
	public function no_urut($tahun) {
		$this->db->select('msPesertaNik');
		$this->db->from('mspeserta');
		$data = $this->db->get();
		$total = $data->num_rows();
		return $total;
	}

	public function cekDobel($nik) {
		$this->db->select('msPesertaNik');
		$this->db->where('msPesertaNik',$nik);
		$this->db->where('status !=99');
		$this->db->from('mspeserta');
		
		$data = $this->db->get();
		$total = $data->num_rows();
		return $total;
	}

	public function viewByIdPeserta($id_peserta){
	    $this->db->where('msPesertaId', $id_peserta);
	    $result = $this->db->get('mspeserta')->row();    
	    return $result; 
  	}


}
