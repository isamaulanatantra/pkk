<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {
	public function select_all() {
		$this->db->select('*');
		$this->db->from('msadmin');
		$this->db->join('msgrupmenu','msGrupMenuId = msAdminGrupMenuId');
		$this->db->where('msadmin.status !=',99);
		//->where('tbl_somthing.Something != ',0,FALSE);

		$data = $this->db->get();
		return $data->result();
	}	

	public function insert($data_input, $table_name) {
		$this->db->insert( $table_name, $data_input );	
		return $this->db->affected_rows();
	}	

	function update($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}
}
