<?php 
class User_model extends CI_Model {	

	public function cekDobel($nik) {
		$this->db->select('msUserNik');
		$this->db->where('msUserNik',$nik);
		$this->db->where('status !=99');
		$this->db->from('msuser');
		
		$data = $this->db->get();
		$total = $data->num_rows();
		return $total;
	}

	public function insert($data_input, $table_name) {
		$this->db->insert( $table_name, $data_input );	
		return $this->db->affected_rows();
	}	

	public function select_all_puskesmas() {
		$this->db->select('*');
		$this->db->from('mspuskesmas');
		$this->db->where('status != 99');
		$data = $this->db->get();

		return $data->result();
	}

}