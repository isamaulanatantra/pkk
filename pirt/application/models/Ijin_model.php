<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ijin_model extends CI_Model {	
	
	public function total($user) {
		$this->db->select('msPesertaNik');
		$this->db->from('mspeserta');
		$this->db->where('msPesertaUserId',$user);
		$this->db->where('kehadiran',1);
		$data = $this->db->get();
		$total = $data->num_rows();
		return $total;
	}	

	public function select_peserta_by_id($user) {
		$this->db->select('*');
		$this->db->from('mspeserta');
		$this->db->where('msPesertaUserId',$user);
		$this->db->where('status ',2);
		$this->db->where('kehadiran ',1);
		$data = $this->db->get();

		return $data->result();
	}

	public function select_sppirt_by_id_user($user) {
		$this->db->select('*');
		$this->db->from('mssppirt');
		$this->db->where('msSppIrtIdUser',$user);
		$this->db->where('status != 99');
		$data = $this->db->get();

		return $data->result();
	}



	public function no_urut() {
		$this->db->select('msSppIrtNik');
		$this->db->from('mssppirt');
		$data = $this->db->get();
		$total = $data->num_rows();
		return $total;
	}

	function update($data_update, $where, $table_name)
	{
		$this->db->where($where);
		$this->db->update($table_name, $data_update);
	}

	public function insert($data_input, $table_name) {
		$this->db->insert( $table_name, $data_input );	
		return $this->db->affected_rows();
	}

	public function viewByIdSppirt($id_sppirt){
	    $this->db->where('msSppIrtId', $id_sppirt);
	    $result = $this->db->get('mssppirt')->row();    
	    return $result; 
  	}


  	//produk
	public function select_produk($id_pirt) {
		$this->db->select('*');
		$this->db->from('msproduk');
		$this->db->where('status != 99');
		$this->db->where('msProdukIdIrt',$id_pirt);
		$data = $this->db->get();

		return $data->result();
	}

	public function select_all_kemasan() {
		$this->db->select('*');
		$this->db->from('mskemasan');
		$this->db->where('status != 99');
		$data = $this->db->get();

		return $data->result();
	}

	public function select_all_pangan() {
		$this->db->select('*');
		$this->db->from('msjenispangan');
		$this->db->where('status != 99');
		$data = $this->db->get();

		return $data->result();
	}

	public function getTotalProdukById($id_pirt) {
		$this->db->select('msProdukIdIrt');
		$this->db->where('msProdukIdIrt',$id_pirt);
		$this->db->where('status !=99');
		$this->db->from('msproduk');
		
		$data = $this->db->get();
		$total = $data->num_rows();
		return $total;
	}

	public function no_urut_produk($id_pirt) {
		$this->db->select('msProdukIdIrt');
		$this->db->where('msProdukIdIrt',$id_pirt);
		$this->db->from('msproduk');
		
		$data = $this->db->get();
		$total = $data->num_rows();
		return $total;
	}

	function getKemasan($id_kemasan){
		$where = array(
		      'msKemasanKode' => $id_kemasan
		      );
	 	$this->db->select('msKemasanNama');
	    $this->db->where($where);		    
	    $query = $this->db->get('mskemasan');

	    foreach ($query->result() as $hsl) {
	    	$value = $hsl->msKemasanNama;
	    }

	    if ($query->num_rows() > 0) {
	    	$cetak = $value;
	    }else{
	    	$cetak = '';
	    }

		return $cetak;
	}

	function getNoPirt($id_pirt){
		$where = array(
		      'msSppIrtId' => $id_pirt
		      );
	 	$this->db->select('msSppIrtNoUrut');
	    $this->db->where($where);		    
	    $query = $this->db->get('mssppirt');

	    foreach ($query->result() as $hsl) {
	    	$value = $hsl->msSppIrtNoUrut;
	    }

	    if ($query->num_rows() > 0) {
	    	$cetak = $value;
	    }else{
	    	$cetak = '';
	    }

		return $cetak;
	}

	public function viewProdukById($id_produk){
	    $this->db->where('msProdukId', $id_produk);
	    $result = $this->db->get('msproduk')->row();    
	    return $result; 
  	}
	
}
