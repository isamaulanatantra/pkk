<?php
Class Cetakundangan extends AUTH_Controller{
    
    function __construct() {
        parent::__construct();
        $this->load->library('pdf');
        $this->load->library('ciqrcode');   
        $this->load->library('session');
        // $this->load->helper('url');     
    }
    
    function index(){
        $id_peserta=!empty($_GET['id_peserta'])?$_GET['id_peserta']:0;

        $fpdf = new FPDF('P','cm','undangan');  
        $fpdf->SetMargins(13.175, 3.175, 3.175, 3.175);
        $fpdf->SetAutoPageBreak(false);      
        $fpdf->AddPage();        

        $fpdf -> Image(base_url().'assets/img/logowsb.jpg',1.3,0.5,2.2,2.5);
        // $fpdf -> Image(base_url().'assets/img/kemenkes.jpg',17.2,0.2,2.2,3.0);
        
        $fpdf -> Sety(0.5);
        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','B',12);
        $fpdf -> Cell(20,0.5,'PEMERINTAH KABUPATEN WONOSOBO','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','B',14);
        $fpdf -> Cell(20,0.5,'DINAS KESEHATAN','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Alamat : Jl. T.Jogonegoro 2-4 Telp./Faks. : (0286) 321033 / 321319','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Email : dinkes@wonosobokab.go.id','',0,'C');
        $fpdf -> ln();

        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'Wonosobo-56311','',0,'C');        
        $fpdf -> ln();
        $fpdf -> Setx(0.5);
        $fpdf -> SetFont('Arial','',12);
        $fpdf -> Cell(20,0.5,'','B',0,'C');
        $fpdf -> ln();

        $fpdf -> ln();
        $fpdf -> Setx(0.5);        
        $fpdf -> SetFont('Arial','BU',12);
        $fpdf -> Cell(20,0.5,'UNDANGAN','',0,'C');
        $fpdf -> ln();
        $fpdf -> ln();

        $w = $this->db->query("SELECT msPesertaPenanggungJawab,msPesertaAlamat, msJadwalPenyuluhanTgl,  
                                        msJadwalPenyuluhanWaktu, msJadwalPenyuluhanTempat, mspeserta.updated_by 
                                from mspeserta 
                                LEFT JOIN msjadwalpenyuluhan ON msJadwalPenyuluhanId = msPesertaIdJadwal
                                where mspeserta.status != 99 
                                and msPesertaId ='".$id_peserta."'
                            ");
            
        foreach ($w->result() as $row){
            $nama =strtoupper($row->msPesertaPenanggungJawab); 
            $alamat=strtoupper($row->msPesertaAlamat);
            $tgl=strtoupper($row->msJadwalPenyuluhanTgl);
            $waktu=strtoupper($row->msJadwalPenyuluhanWaktu);
            $tempat=strtoupper($row->msJadwalPenyuluhanTempat);
            $updated_by=strtoupper($row->updated_by);   

            $unixTimestamp = strtotime($tgl);
            $hari = $this->hari_ini(date("w", $unixTimestamp));
            $tanggal = $this->reversdate($tgl);
        }

        $fpdf -> SetFont('Arial','',12);

        $fpdf -> Setx(1.5);
        $fpdf -> Cell(5,0.7,'Nama Peserta','',0,'');
        $fpdf -> Cell(4,0.7,': '.$nama,'',0,'');
        $fpdf -> ln();

        $fpdf -> Setx(1.5);
        $fpdf -> Cell(5,0.7,'Alamat','',0,'');
        $fpdf -> Cell(4,0.7,': '.$alamat,'',0,'');
        $fpdf -> ln();
        
        $fpdf -> Setx(1.5);
        $fpdf -> Cell(5,0.7,'Hari, Tanggal PKP','',0,'');
        $fpdf -> Cell(4,0.7,': '.$hari.', '.$tanggal,'',0,'');
        $fpdf -> ln();      

        $fpdf -> Setx(1.5);
        $fpdf -> Cell(5,0.7,'Waktu','',0,'');
        $fpdf -> Cell(4,0.7,': '.$waktu.' WIB','',0,'');
        $fpdf -> ln();

        $fpdf -> Setx(1.5);
        $fpdf -> Cell(5,0.7,'Acara','',0,'');        
        $fpdf -> Cell(4,0.7,': Penyuluhan Keamanan Pangan Bagi Industri Rumah Tangga ','',0,'');
        $fpdf -> ln();        

        $fpdf -> Setx(1.5);
        $fpdf -> Cell(5,0.7,'Tempat','',0,'');
        $fpdf -> Cell(4,0.7,': '.$tempat,'',0,'');
        $fpdf -> ln();

        $fpdf -> ln();
        $fpdf -> Setx(12.5);
        $fpdf -> Cell(5,0.7,'Wonosobo, '.$this->reversdate(date('Y-m-d')),'',0,'C');
        $fpdf -> ln();
        $fpdf -> Setx(12.5);
        $fpdf -> Cell(5,0.7,'Petugas,','',0,'C');
        $fpdf -> ln();
        $fpdf -> ln();

        // $user = $this->userdata->msAdminId;
        $sqluser = $this->db->query("SELECT msAdminNama 
                                from msadmin 
                                where status != 99 
                                and msAdminId ='".$updated_by."'
                            ");

        foreach ($sqluser->result() as $row){
            $nama_user = $row->msAdminNama;
        }

        $fpdf -> ln();
        $fpdf -> Setx(12.5);
        $fpdf -> Cell(5,0.7,$nama_user,'',0,'C');
              

        QRcode::png($id_peserta,"test.png");
        $fpdf->Image("test.png", 2.0,9.5,4.0,4.0);

        $fpdf->Output();
    }

    public function hari_ini($hari){

        switch($hari){
            case '0':
                $hari_ini = "Minggu";
            break;

            case '1':           
                $hari_ini = "Senin";
            break;

            case '2':
                $hari_ini = "Selasa";
            break;

            case '3':
                $hari_ini = "Rabu";
            break;

            case '4':
                $hari_ini = "Kamis";
            break;

            case '5':
                $hari_ini = "Jumat";
            break;

            case '6':
                $hari_ini = "Sabtu";
            break;
            
            default:
                $hari_ini = "Tidak di ketahui";     
            break;
        }

        return $hari_ini;

    }

    public function reversdate($tanggal){
        $a=explode('-',$tanggal);
        $d=$a[2];
        $m=$a[1];
        $y=$a[0];
        $a[0]=$m+0;
        $a[1]=$d;
        $a[2]=$y;

        $nmbulan='';

        switch ( $a[0] ) {
            case 1 : $nmbulan   = 'Januari';    break;
            case 2 : $nmbulan   = 'Februari';   break;
            case 3 : $nmbulan   = 'Maret';      break;
            case 4 : $nmbulan   = 'April';      break;
            case 5 : $nmbulan   = 'Mei';        break;
            case 6 : $nmbulan   = 'Juni';       break;
            case 7 : $nmbulan   = 'Juli';       break;
            case 8 : $nmbulan   = 'Agustus';    break;
            case 9 : $nmbulan   = 'September';  break;
            case 10: $nmbulan   = 'Oktober';    break;
            case 11: $nmbulan   = 'November';   break;
            case 12: $nmbulan   = 'Desember';   break;
        }

        $res=$a[1] . ' ' . $nmbulan . ' ' . $a['2'];
        return $res;
    }
}