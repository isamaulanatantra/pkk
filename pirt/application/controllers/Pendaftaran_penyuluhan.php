<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran_penyuluhan extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Penyuluhan_model');
		$this->load->library('session');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Jakarta'); 
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$user = $this->userdata->msUserId;
		$data['dataStatus'] 	= $this->Penyuluhan_model->select_by_id($user);
		$data['dataTotal'] 	= $this->Penyuluhan_model->total($user);		

		$data['page'] 		= "pendaftaran penyuluhan keamanan pangan";
		$data['judul'] 		= "Pendaftaran Penyuluhan Keamanan Pangan";
		$data['deskripsi'] 	= "Manage Data Pendaftaran Penyuluhan";		
		$this->template->views('penyuluhan/home', $data);
	}

	public function tampilPeserta() {
		$user = $this->userdata->msUserId;
		$data['dataPeserta'] = $this->Penyuluhan_model->select_by_id($user);		
		$this->load->view('penyuluhan/list_data', $data);
	}

	public function insertPeserta() {	
		$this->form_validation->set_rules('id_user', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nik', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('no_hp', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_perusahaan', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_pemilik', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_penanggung_jawab', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Keterangan ', 'trim|required');

		$id_user = $this->input->post('id_user');
		$nik = $this->input->post('nik');
		$no_hp = $this->input->post('no_hp');
		$nama_perusahaan = $this->input->post('nama_perusahaan');
		$nama_pemilik = $this->input->post('nama_pemilik');
		$nama_penanggung_jawab = $this->input->post('nama_penanggung_jawab');
		$alamat = $this->input->post('alamat');
		

		$now = date('Y-m-d');
		$a = explode('-', $now);
		$yy = $a[0];


		$geturut = $this->Penyuluhan_model->no_urut($yy);	
		$no_urut = $geturut+1;
		
		$user = $this->userdata->msUserId;
		$ktp = $this->userdata->msUserFile;
		$id_puskesmas = $this->userdata->msUserIdPuskesmas;

		$cek = $this->Penyuluhan_model->cekDobel($nik);

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			if ($cek > 0) {
				echo 3;
			}else{
		        $data_input = array(
		        	'msPesertaUserId' => $id_user,
		    		'msPesertaIdJadwal' => 0,
		    		'msPesertaNik' => $nik,
		    		'msPesertaNamaPerusahaan' => $nama_perusahaan,
		    		'msPesertaPemilik' => $nama_pemilik,
		    		'msPesertaPenanggungJawab' => $nama_penanggung_jawab,
		    		'msPesertaAlamat' => $alamat,
		    		'msPesertaNoHp' => $no_hp,
		    		'msPesertaTahun' => $yy,
		    		'msPesertaNoUrut' => $no_urut,
		    		'msPesertaIdPuskesmas' => $id_puskesmas,
		    		'msPesertaKtp' => $ktp,
		    		'created_by' => $user,
					'created_time' => date('Y-m-d H:i:s'),
					'status' => 1
				);

				$table_name  = 'mspeserta';
				$result = $this->Penyuluhan_model->insert($data_input, $table_name);
				if ($result > 0) {

					$query = $this->db->query("SELECT msTokenToken FROM mstoken  WHERE msTokenIdPuskesmas ='99'"); 						

					foreach($query->result() as $data){
					    $device_token = $data->msTokenToken;            
					    $fields          =  ["title" => "Hey Admin", "body" => "Ada pendaftaran penyuluhan keamanan pangan baru "];
					    $response        =  PushNotifications::sendPushNotification($device_token, $fields);
					    print_r($response); 

					}
				
					echo 2;
				} else {
					echo 1;
				}			        			
			}
			
		}	
	}

	public function load_peserta_by_id(){
	    
	    $id_peserta = $this->input->post('id_peserta');
	    
	    $data = $this->Penyuluhan_model->viewByIdPeserta($id_peserta);
	    
	    if( ! empty($data)){ 
	      
	      $callback = array(
	        'status' => 'success', 
	        'id' => $data->msPesertaId,
	        'nik' => $data->msPesertaNik,
	        'no_hp' => $data->msPesertaNoHp, 
	        'nama_perusahaan' => $data->msPesertaNamaPerusahaan, 
	        'nama_pemilik' => $data->msPesertaPemilik, 
	        'nama_penanggung_jawab' => $data->msPesertaPenanggungJawab, 
	        'alamat' => $data->msPesertaAlamat, 
	        
	        
	      );
	    }else{
	      $callback = array('status' => 'failed'); 
	    }
	    echo json_encode($callback); 
	}

	public function updatePeserta() {
		$this->form_validation->set_rules('id_peserta_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nik_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('no_hp_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_perusahaan_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_pemilik_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_penanggung_jawab_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('alamat_edit', 'Keterangan ', 'trim|required');

		$id_peserta_edit = $this->input->post('id_peserta_edit');
		$nik_edit = $this->input->post('nik_edit');
		$no_hp_edit = $this->input->post('no_hp_edit');
		$nama_perusahaan_edit = $this->input->post('nama_perusahaan_edit');
		$nama_pemilik_edit = $this->input->post('nama_pemilik_edit');
		$nama_penanggung_jawab_edit = $this->input->post('nama_penanggung_jawab_edit');
		$alamat_edit = $this->input->post('alamat_edit');
		
		$user = $this->userdata->msUserId;

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{

			$data_update = array(	    		
	        	'msPesertaNik' => $nik_edit,
	    		'msPesertaNamaPerusahaan' => $nama_perusahaan_edit,
	    		'msPesertaPemilik' => $nama_pemilik_edit,
	    		'msPesertaPenanggungJawab' => $nama_penanggung_jawab_edit,
	    		'msPesertaAlamat' => $alamat_edit,
	    		'msPesertaNoHp' => $no_hp_edit,
	    		'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				'status' => 1
			);
			

			$where = array(
	    		'msPesertaId' => $id_peserta_edit
			);

			$table_name  = 'mspeserta';
			$result = $this->Penyuluhan_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		}	
	}

	public function hapusPeserta() {
				
		$id_peserta = $this->input->post('id_peserta');

		$user = $this->userdata->msUserId;
		
				
			$data_update = array(
	    		'deleted_by' => $user,
				'deleted_time' => date('Y-m-d H:i:s'),					
				'status' => 99
			);		
			
			$where = array(
	    		'msPesertaId' => $id_peserta
			);

			$table_name  = 'mspeserta';
			$result = $this->Penyuluhan_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		
	}

	
}

class PushNotifications { 
private static $URL  = "https://fcm.googleapis.com/fcm/send";  //API URL of FCM 
private static $API_ACCESS_KEY = 'AAAA5OJemc8:APA91bGCPvOmqb-Tm-VOmbqd-j9XQhKPV1EQoGqOUJrK_sGyAGe8EJ250e-6CS1zL5PQmQsndpSTL-cSPNW8JlMTeOAfa8XmymfZmAuYGccSNZJDI2puWSU_ko_FJ7AqzCpVDTDQekuZ'; // YOUR_FIREBASE_API_KEY
 
public function __construct() {
 
}
 
public static function sendPushNotification($token = "", $fields = array())
 {
            $registrationIds = array();
             
            array_push($registrationIds, $token);
 
            $msg     = array('body' => $fields['body'], 'title'  => $fields['title']);
 
            $fields  = array('registration_ids' => $registrationIds, 'notification' => $msg);
 
            $headers = array('Authorization: key=' . self::$API_ACCESS_KEY, 'Content-Type: application/json');
 
            #Send Reponse To FireBase Server    
            $ch = curl_init(); 
            curl_setopt($ch,CURLOPT_URL, self::$URL);
            curl_setopt($ch,CURLOPT_POST, true);
            curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
 }
  }
  ?>