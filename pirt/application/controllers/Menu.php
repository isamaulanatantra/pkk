<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Menu_model');
		$this->load->library('session');
		$this->load->helper('url');
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$data['dataParent'] 	= $this->Menu_model->select_parent();

		$data['page'] 		= "menu";
		$data['judul'] 		= "Data Menu";
		$data['deskripsi'] 	= "Manage Data Menu";
		
		$this->template->views('menu/home', $data);		
	}

	public function tampil() {
		$data['dataMenu'] = $this->Menu_model->select_all();
		$this->load->view('menu/list_data', $data);
	}

	public function insert() {
		$this->form_validation->set_rules('nama_menu', 'Nama ', 'trim|required');
		$this->form_validation->set_rules('order_menu', 'Order', 'trim|required');		

		$nama_menu = $this->input->post('nama_menu');
      	$order_menu = $this->input->post('order_menu');
      	$parent_menu = $this->input->post('parent_menu');
      	$icon_menu = $this->input->post('icon_menu');
      	$link_menu = $this->input->post('link_menu');

		$user = $this->userdata->msAdminId;

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$data_input = array(
	    		'msMenuOrder' => $order_menu,				
				'msMenuParent' => $parent_menu,
				'msMenuNama' => $nama_menu,
				'msMenuLink' => $link_menu,
				'msMenuController' => $link_menu,
				'msMenuIcon' => $icon_menu,
				'created_by' => $user,
				'created_time' => date('Y-m-d H:i:s'),
				'status' => 1
			);

			$table_name  = 'msmenu';
			$result = $this->Menu_model->insert($data_input, $table_name);
			if ($result > 0) {
				echo 2;
			} else {
				echo 1;
			}
		}	
	}

	public function update() {
		$this->form_validation->set_rules('id', 'Id ', 'trim|required');
		$this->form_validation->set_rules('nama_menu', 'Nama ', 'trim|required');
		$this->form_validation->set_rules('order_menu', 'Order', 'trim|required');		

		$nama_menu = $this->input->post('nama_menu');
      	$order_menu = $this->input->post('order_menu');
      	$parent_menu = $this->input->post('parent_menu');
      	$icon_menu = $this->input->post('icon_menu');
      	$link_menu = $this->input->post('link_menu');
		
		$id = $this->input->post('id');		

		$user = $this->userdata->msAdminId;

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			
			$data_update = array(
	    		'msMenuOrder' => $order_menu,				
				'msMenuParent' => $parent_menu,
				'msMenuNama' => $nama_menu,
				'msMenuLink' => $link_menu,
				'msMenuController' => $link_menu,
				'msMenuIcon' => $icon_menu,
				'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				'status' => 1
			);
			

			$where = array(
	    		'msMenuId' => $id
			);

			$table_name  = 'msmenu';
			$result = $this->Menu_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		}	
	}

	public function hapus() {
		$this->form_validation->set_rules('id', 'Id ', 'trim|required');
		
		$id = $this->input->post('id');
		$user = $this->userdata->msAdminId;
		
		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{			
			$data_update = array(
	    		'deleted_by' => $user,
				'deleted_time' => date('Y-m-d H:i:s'),					
				'status' => 99
			);		
			
			$where = array(
	    		'msMenuId' => $id
			);

			$table_name  = 'msmenu';
			$result = $this->Menu_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		}	
	}
}
