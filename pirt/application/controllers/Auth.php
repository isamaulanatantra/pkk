<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_auth');
		$this->load->model('User_model');
		date_default_timezone_set('Asia/Jakarta'); 
	}
	
	public function index() {
		$session = $this->session->userdata('status');
		$data['dataPuskesmas'] 	= $this->User_model->select_all_puskesmas();

		if ($session == '') {
			$this->load->view('login', $data);
		} else {
			redirect('Home', $data);
		}
	}

	public function login() {
		$this->form_validation->set_rules('username', 'Username', 'required|min_length[4]|max_length[15]');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == TRUE) {
			$username = trim($_POST['username']);
			$password = trim($_POST['password']);

			$data = $this->M_auth->login($username, $password);

			if ($data == false) {
				$this->session->set_flashdata('error_msg', 'Username / Password Anda Salah.');
				redirect('Auth');
			} else {
				$session = [
					'userdata' => $data,
					'status' => "Loged in"
				];
				$this->session->set_userdata($session);
				redirect('Home');
			}
		} else {
			$this->session->set_flashdata('error_msg', validation_errors());
			redirect('Auth');
		}
	}

	public function login_user() {
		$this->form_validation->set_rules('nik_login', 'NIK', 'required|min_length[16]|max_length[16]');
		$this->form_validation->set_rules('no_hp_login', 'No HP', 'required');

		if ($this->form_validation->run() == TRUE) {
			$nik_login = trim($_POST['nik_login']);
			$no_hp_login = trim($_POST['no_hp_login']);

			$data = $this->M_auth->login_user($nik_login, $no_hp_login);

			if ($data == false) {
				$this->session->set_flashdata('error_msg', 'Nik / No HP Anda Salah.');
				redirect('Auth');
			} else {
				$session = [
					'userdata' => $data,
					'status' => "Loged in"
				];
				$this->session->set_userdata($session);
				redirect('Home');
			}
		} else {
			$this->session->set_flashdata('error_msg', validation_errors());
			redirect('Auth');
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('Auth');
	}


	public function insertUser() {		
		$this->form_validation->set_rules('nik_daftar', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('no_hp_daftar', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('id_puskesmas', 'Keterangan ', 'trim|required');
		
		$nik_daftar = $this->input->post('nik_daftar');
		$no_hp_daftar = $this->input->post('no_hp_daftar');
		$nama = $this->input->post('nama');
		$id_puskesmas = $this->input->post('id_puskesmas');

		$query_puskesmas = $this->db->query("SELECT msPuskesmasNama FROM mspuskesmas  WHERE msPuskesmasId ='$id_puskesmas'");
		foreach ($query_puskesmas->result() as $data) {
			$nama_puskesmas = $data->msPuskesmasNama;
		}
		
		
		$user = 0;

		$cek = $this->User_model->cekDobel($nik_daftar);

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			if (strlen($nik_daftar) != 16 ) {
				echo 4;
			}else{
				if ($cek > 0) {
					echo 3;
				}else{
					$config['upload_path']="./admin/gambar";
			        $config['allowed_types']='gif|jpg|png';
			        $config['encrypt_name'] = TRUE;
			        
			        $this->load->library('upload',$config);
			        if($this->upload->do_upload("file")){
				        $data = array('upload_data' => $this->upload->data());
				        
				        $image= $data['upload_data']['file_name']; 
				        // $image = 'asdasd';

				        $data_input = array(
				    		'msUserNik' => $nik_daftar,			    		
				    		'msUserNoHp' => $no_hp_daftar,
				    		'msUserNama' => $nama,				    		
				    		'msUserFile' => $image,
				    		'msUserIdPuskesmas' => $id_puskesmas,
							'created_by' => $user,
							'created_time' => date('Y-m-d H:i:s'),
							'status' => 1
						);

						$table_name  = 'msuser';
						$result = $this->User_model->insert($data_input, $table_name);
						if ($result > 0) {
							
							// $w = $this->db->query("SELECT * from perijinan_penggunaan_aula ";
							$query = $this->db->query("SELECT msTokenToken FROM mstoken  WHERE msTokenIdPuskesmas ='$id_puskesmas' OR msTokenIdPuskesmas ='99'"); 
							// $query->execute();

							foreach($query->result() as $data){
							    $device_token = $data->msTokenToken;            
							    $fields          =  ["title" => $nama_puskesmas, "body" => "Pendaftaran user baru "];
							    $response        =  PushNotifications::sendPushNotification($device_token, $fields);
							    // print_r($response); 

							}
							echo 2;
						} else {
							echo 1;
						}			        			        
			        }				
				}
			}
		}	
	}
}

class PushNotifications { 
private static $URL  = "https://fcm.googleapis.com/fcm/send";  //API URL of FCM 
private static $API_ACCESS_KEY = 'AAAA5OJemc8:APA91bGCPvOmqb-Tm-VOmbqd-j9XQhKPV1EQoGqOUJrK_sGyAGe8EJ250e-6CS1zL5PQmQsndpSTL-cSPNW8JlMTeOAfa8XmymfZmAuYGccSNZJDI2puWSU_ko_FJ7AqzCpVDTDQekuZ'; // YOUR_FIREBASE_API_KEY
 
public function __construct() {
 
}
 
public static function sendPushNotification($token = "", $fields = array())
 {
            $registrationIds = array();
             
            array_push($registrationIds, $token);
 
            $msg     = array('body' => $fields['body'], 'title'  => $fields['title']);
 
            $fields  = array('registration_ids' => $registrationIds, 'notification' => $msg);
 
            $headers = array('Authorization: key=' . self::$API_ACCESS_KEY, 'Content-Type: application/json');
 
            #Send Reponse To FireBase Server    
            $ch = curl_init(); 
            curl_setopt($ch,CURLOPT_URL, self::$URL);
            curl_setopt($ch,CURLOPT_POST, true);
            curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch,CURLOPT_POSTFIELDS, json_encode($fields));
            $result = curl_exec($ch);
            curl_close($ch);
            return $result;
 }
  }
  ?>