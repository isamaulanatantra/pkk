<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran_ijin_pirt extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Ijin_model');
		$this->load->library('session');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Jakarta'); 
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$user = $this->userdata->msUserId;
		$data['dataPenyuluhan'] 	= $this->Ijin_model->total($user);
		$data['dataPeserta'] 	= $this->Ijin_model->select_peserta_by_id($user);		

		$data['dataKemasan'] 	= $this->Ijin_model->select_all_kemasan();
		$data['dataPangan'] 	= $this->Ijin_model->select_all_pangan();

		$data['page'] 		= "pendaftaran ijin pirt";
		$data['judul'] 		= "Pendaftaran Ijin PIRT";
		$data['deskripsi'] 	= "Manage Data Pendaftaran Ijin PIRT";		
		$this->template->views('ijin/home', $data);
	}

	public function tampilIjin() {
		$user = $this->userdata->msUserId;
		$data['dataSppirt'] = $this->Ijin_model->select_sppirt_by_id_user($user);		
		$this->load->view('ijin/list_data', $data);
	}
}