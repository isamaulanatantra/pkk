<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('User_model');
		$this->load->model('Grup_model');
		$this->load->library('session');
		$this->load->helper('url');
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$data['dataGrup'] 	= $this->Grup_model->select_all();	

		$data['page'] 		= "user";
		$data['judul'] 		= "Data User";
		$data['deskripsi'] 	= "Manage Data User";
		
		$this->template->views('user/home', $data);		
	}

	public function tampilUser() {
		$data['dataUser'] = $this->User_model->select_all();
		$this->load->view('user/list_data', $data);
	}

	public function insert() {
		$this->form_validation->set_rules('nama_user', 'Nama ', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('sandi', 'Password', 'trim|required');

		$nama_user = $this->input->post('nama_user');
		$grup_user = $this->input->post('grup_user');
		$username = $this->input->post('username');
		$sandi = $this->input->post('sandi');

		$user = $this->userdata->msAdminId;

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$data_input = array(
	    		'msAdminNama' => $nama_user,				
				'msAdminGrupMenuId' => $grup_user,
				'msAdminUsername' => $username,
				'msAdminSandi' => MD5($sandi),			
				'created_by' => $user,
				'created_time' => date('Y-m-d H:i:s'),
				'status' => 1
			);

			$table_name  = 'msadmin';
			$result = $this->User_model->insert($data_input, $table_name);
			if ($result > 0) {
				echo 2;
			} else {
				echo 1;
			}
		}	
	}

	public function update() {
		$this->form_validation->set_rules('id', 'Nama ', 'trim|required');
		$this->form_validation->set_rules('nama_user', 'Nama ', 'trim|required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		
		$id = $this->input->post('id');
		$nama_user = $this->input->post('nama_user');
		$grup_user = $this->input->post('grup_user');
		$username = $this->input->post('username');
		$sandi = $this->input->post('sandi');

		$user = $this->userdata->msAdminId;

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{

			if ($sandi =="") {
				$data_update = array(
		    		'msAdminNama' => $nama_user,				
					'msAdminGrupMenuId' => $grup_user,
					'msAdminUsername' => $username,
					'updated_by' => $user,
					'updated_time' => date('Y-m-d H:i:s'),
					'status' => 1
				);
			}else{
				$data_update = array(
		    		'msAdminNama' => $nama_user,				
					'msAdminGrupMenuId' => $grup_user,
					'msAdminUsername' => $username,
					'msAdminSandi' => MD5($sandi),			
					'updated_by' => $user,
					'updated_time' => date('Y-m-d H:i:s'),
					'status' => 1
				);
			}
			

			$where = array(
	    		'msAdminId' => $id
			);

			$table_name  = 'msadmin';
			$result = $this->User_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		}	
	}

	public function hapus() {
		$this->form_validation->set_rules('id', 'Nama ', 'trim|required');
		
		$id = $this->input->post('id');
		$user = $this->userdata->msAdminId;
		
		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{			
			$data_update = array(
	    		'deleted_by' => $user,
				'deleted_time' => date('Y-m-d H:i:s'),					
				'status' => 99
			);		
			
			$where = array(
	    		'msAdminId' => $id
			);

			$table_name  = 'msadmin';
			$result = $this->User_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		}	
	}
}
