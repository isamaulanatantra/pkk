<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendaftaran_ijin extends AUTH_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Ijin_model');
		$this->load->library('session');
		$this->load->helper('url');
		date_default_timezone_set('Asia/Jakarta'); 
		
	}

	public function index() {
		$data['userdata'] 	= $this->userdata;
		$user = $this->userdata->msUserId;
		$data['dataPenyuluhan'] 	= $this->Ijin_model->total($user);
		$data['dataPeserta'] 	= $this->Ijin_model->select_peserta_by_id($user);		

		$data['dataKemasan'] 	= $this->Ijin_model->select_all_kemasan();
		$data['dataPangan'] 	= $this->Ijin_model->select_all_pangan();

		$data['page'] 		= "pendaftaran ijin pirt";
		$data['judul'] 		= "Pendaftaran Ijin PIRT";
		$data['deskripsi'] 	= "Manage Data Pendaftaran Ijin PIRT";		
		$this->template->views('ijin/home', $data);
	}

	public function tampilIjin() {
		$user = $this->userdata->msUserId;
		$data['dataSppirt'] = $this->Ijin_model->select_sppirt_by_id_user($user);		
		$this->load->view('ijin/list_data', $data);
	}

	public function insert() {
		$this->form_validation->set_rules('id_user', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nik', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_perusahaan', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_pemilik', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_penanggung_jawab', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('alamat', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('no_hp', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nib', 'Keterangan ', 'trim|required');
		
		$id_user = $this->input->post('id_user');
		$nik = $this->input->post('nik');
		$nama_perusahaan = $this->input->post('nama_perusahaan');
		$nama_pemilik = $this->input->post('nama_pemilik');
		$nama_penanggung_jawab = $this->input->post('nama_penanggung_jawab');
		$alamat = $this->input->post('alamat');
		$no_hp = $this->input->post('no_hp');
		$nib = $this->input->post('nib');
		
		$geturut = $this->Ijin_model->no_urut();	
		$no_urut = $geturut+1;

		$id_puskesmas = $this->userdata->msUserIdPuskesmas;
			
		
		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			$config['upload_path']="./admin/gambar";
	        $config['allowed_types']='gif|jpg|png';
	        $config['encrypt_name'] = TRUE;
	        
	        $this->load->library('upload',$config);
	        if($this->upload->do_upload("file")){
		        $data = array('upload_data' => $this->upload->data());
		        
		        $image= $data['upload_data']['file_name'];

		        $data_input = array(
		    		'msSppIrtIdUser' => $id_user,
		    		'msSppIrtNik' => $nik,
		    		'msSppIrtPerusahaan'=> $nama_perusahaan,
		    		'msSppIrtPemilik' => $nama_pemilik,
		    		'msSppIrtPenanggungJawab' => $nama_penanggung_jawab,
		    		'msSppIrtAlamat'=> $alamat,
		    		'msSppIrtNoHp' => $no_hp,
		    		'msSppIrtNib' => $nib,
		    		'msSppIrtNoUrut'=>$no_urut,
		    		'msSppIrtPasFoto' => $image,
		    		'msSppIrtIdPuskesmas' => $id_puskesmas,		    		
					'created_by' => 1,
					'created_time' => date('Y-m-d H:i:s'),
					'status' => 1
				);

				$table_name  = 'mssppirt';
				$result = $this->Ijin_model->insert($data_input, $table_name);
				if ($result > 0) {
					echo 2;
				} else {
					echo 1;
				}
		    }
			
		}	
	}

	public function load_sppirt_by_id(){
	    
	    $id_sppirt = $this->input->post('id_sppirt');
	    
	    $data = $this->Ijin_model->viewByIdSppirt($id_sppirt);
	    
	    if( ! empty($data)){ 
	      
	      $callback = array(
	        'status' => 'success', 
	        'id' => $data->msSppIrtId,
	        'id_user' => $data->msSppIrtIdUser,
	        'nik' => $data->msSppIrtNik,	         
	        'nama_perusahaan' => $data->msSppIrtPerusahaan, 
	        'nama_pemilik' => $data->msSppIrtPemilik, 
	        'nama_penanggung_jawab' => $data->msSppIrtPenanggungJawab, 
	        'alamat' => $data->msSppIrtAlamat, 
	        'no_hp' => $data->msSppIrtNoHp,
	        'nib' => $data->msSppIrtNib,
	        
	        
	      );
	    }else{
	      $callback = array('status' => 'failed'); 
	    }
	    echo json_encode($callback); 
	}

	public function updatePeserta() {
		$this->form_validation->set_rules('id_pendaftaran_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nik_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('no_hp_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_perusahaan_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_pemilik_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_penanggung_jawab_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('alamat_edit', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nib_edit', 'Keterangan ', 'trim|required');

		$id_pendaftaran_edit = $this->input->post('id_pendaftaran_edit');
		$nik_edit = $this->input->post('nik_edit');
		$no_hp_edit = $this->input->post('no_hp_edit');
		$nama_perusahaan_edit = $this->input->post('nama_perusahaan_edit');
		$nama_pemilik_edit = $this->input->post('nama_pemilik_edit');
		$nama_penanggung_jawab_edit = $this->input->post('nama_penanggung_jawab_edit');
		$alamat_edit = $this->input->post('alamat_edit');
		$nib_edit = $this->input->post('nib_edit');
		
		$user = $this->userdata->msUserId;

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{

			$data_update = array(	    		
	        	'msSppIrtNik' => $nik_edit,
	    		'msSppIrtPerusahaan' => $nama_perusahaan_edit,
	    		'msSppIrtPemilik' => $nama_pemilik_edit,
	    		'msSppIrtPenanggungJawab' => $nama_penanggung_jawab_edit,
	    		'msSppIrtAlamat' => $alamat_edit,
	    		'msSppIrtNoHp' => $no_hp_edit,
	    		'msSppIrtNib' => $nib_edit,
	    		'updated_by' => $user,
				'updated_time' => date('Y-m-d H:i:s'),
				'status' => 1
			);
			

			$where = array(
	    		'msSppIrtId' => $id_pendaftaran_edit
			);

			$table_name  = 'mssppirt';
			$result = $this->Ijin_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		}	
	}

	public function hapusIjin() {
				
		$id_pendaftaran_edit = $this->input->post('id_pendaftaran_edit');

		$user = $this->userdata->msUserId;
		
				
		$data_update = array(
    		'deleted_by' => $user,
			'deleted_time' => date('Y-m-d H:i:s'),					
			'status' => 99
		);		
		
		$where = array(
    		'msSppIrtId' => $id_pendaftaran_edit
		);

		$table_name  = 'mssppirt';
		$result = $this->Ijin_model->update($data_update, $where, $table_name);
		if ($result > 0) {
			echo 1;
		} else {
			echo 2;
		}		
	}

	//produk
	public function tampilProduk() {
		$id_pirt = $_GET['id_pirt'];
		$data['dataProduk'] = $this->Ijin_model->select_produk($id_pirt);
		$this->load->view('ijin/list_data_produk', $data);
	}

	public function insertProduk() {		
		$this->form_validation->set_rules('id_pirt2', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('nama_produk', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('jenis_kemasan', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('kemasan_primer', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('jenis_pangan', 'Keterangan ', 'trim|required');
		$this->form_validation->set_rules('masa_berlaku', 'Keterangan ', 'trim|required');
		
		// $id_produk = $this->input->post('id_produk');
		$id_pirt = $this->input->post('id_pirt2');
		$nama_produk = $this->input->post('nama_produk');
		$jenis_kemasan = $this->input->post('jenis_kemasan');
		$kemasan_primer = $this->input->post('kemasan_primer');
		$jenis_pangan = $this->input->post('jenis_pangan');
		$masa_berlaku = $this->input->post('masa_berlaku');
		

		$geturut = $this->Ijin_model->no_urut_produk($id_pirt);	
		$no_urut = $geturut+1;

		$now = date('Y-m-d');
		$a = explode('-', $now);
		$yy = $a[0];
				
		$user = $this->userdata->msUserId;

		$total = $this->Ijin_model->getTotalProdukById($id_pirt);

		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{
			if ($total < 5) {

				$config['upload_path']="./admin/gambar";
		        $config['allowed_types']='pdf';
		        $config['encrypt_name'] = TRUE;
		        
		        $this->load->library('upload',$config);
		        if($this->upload->do_upload("file1")){
			        $data = array('upload_data1' => $this->upload->data());			        
			        $image= $data['upload_data1']['file_name']; 
			        
			        $this->upload->do_upload("file2");
			        $data2 = array('upload_data2' => $this->upload->data());			        
			        $image2= $data2['upload_data2']['file_name']; 

			        $data_input = array(
			    		'msProdukIdIrt' => $id_pirt,
			    		'msProdukNama'=> $nama_produk,
			    		'msProdukKodeKemasan' => $jenis_kemasan,
			    		'msKemasanPrimer' => $kemasan_primer,
			    		'msProdukKodeJenis'=> $jenis_pangan,
			    		'msProdukNoUrutProduk' => $no_urut,
			    		'msProdukThnBuat'=> $yy,
			    		'msProdukMasaBerlaku'=>$masa_berlaku,
			    		'msProdukLampiranLabel'=>$image,
			    		'msProdukLampiranBagan'=>$image2,
						'created_by' => $user,
						'created_time' => date('Y-m-d H:i:s'),
						'status' => 1
					);

					$table_name  = 'msproduk';
					$result = $this->Ijin_model->insert($data_input, $table_name);
					if ($result > 0) {
						echo 2;
					} else {
						echo 1;
					}			        			        
		        }





			}else{
				echo 3;
			}
		}

	}

	public function hapusProduk() {
		$this->form_validation->set_rules('id_produk', 'Id ', 'trim|required');
				
		$id_produk = $this->input->post('id_produk');

		$user = $this->userdata->msUserId;
		
		if ($this->form_validation->run() == FALSE){
			echo 0;		
		}else{			
			$data_update = array(
	    		'deleted_by' => $user,
				'deleted_time' => date('Y-m-d H:i:s'),					
				'status' => 99
			);		
			
			$where = array(
	    		'msProdukId' => $id_produk
			);

			$table_name  = 'msproduk';
			$result = $this->Ijin_model->update($data_update, $where, $table_name);
			if ($result > 0) {
				echo 1;
			} else {
				echo 2;
			}
		}	
	}

	public function load_produk_by_id(){
	    
	    $id_produk = $this->input->post('id_produk');
	    
	    $data = $this->Ijin_model->viewProdukById($id_produk);
	    
	    if( ! empty($data)){ 
	      
	      $callback = array(
	        'status' => 'success', 
	        'id' => $data->msProdukId,
	        'id_irt' => $data->msProdukIdIrt,
	        'nama_produk' => $data->msProdukNama, 
	        'kode_kemasan' => $data->msProdukKodeKemasan, 
	        'kemasan_primer' => $data->msKemasanPrimer, 
	        'kode_jenis' => $data->msProdukKodeJenis, 
	        'masa_berlaku' => $data->msProdukMasaBerlaku,
	        'lampiran' => $data->msProdukLampiran, 
	        
	        
	      );
	    }else{
	      $callback = array('status' => 'failed'); 
	    }
	    echo json_encode($callback); 
	}

}