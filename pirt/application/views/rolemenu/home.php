<?php $this -> load -> view('rolemenu/js');  ?>

<div class="msg" style="display:none;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="box">
  
  <!-- /.box-header -->
  <div class="box-body" style="background: #d6d6c2 " >
    <div  class="col-md-5">
      <div class="box">
        <div class="box-header">
          <div class="box-name">
            <i class="fa fa-user"></i>
            <span><label>Setup</label> <label id="judul2"> </label></span>
          </div>

          <div class="box-content no-padding table-responsive">
              <table id="list-data" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Grup Menu</th>                    
                    </tr>
                  </thead>
                  <tbody id="tblUtamaRole">
                    
                  </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>

    <div  class="col-md-7">
      <div class="box">
        <div class="box-header">
          <div class="box-name">
            <i class="fa fa-user"></i>
            <span><label>Edit Data</label> <label id="judul2"> </label></span>
          </div>
         
          <form role="form" id="form_isian" method="post" action="<?php echo base_url(); ?>RoleMenu/save">
              <div class="box-content no-padding table-responsive">              
                  <table class="table table-bordered table-hover">
                    <thead>
                      <th>No</th>
                      <th style="display: none;">Grup</th>
                      <th style="display: none;">ID</th>
                      <th>Menu</th>
                      <th>Parent</th>
                      <th>Aktif</th>
                    </thead>
                    <tbody id="tblAktifRole">
                    </tbody>
                    <div id="pesan"></div>
                  </table>
                  <ul class="pagination pagination-sm no-margin pull-right" id="pagination">
                  </ul>

                  <div class="overlay" id="spinners_data" style="display:none;">
                    <i class="fa fa-refresh fa-spin"></i>
                  </div>

                  <div class="row">
                    <div class="col-md-4 col-md-offset-1">
                      <button type="submit" class="btn btn-primary" id="simpan_role">SIMPAN DATA</button>                  
                    </div>
                  </div>

              </div>
          </form>
        </div>
      </div>
    </div>


  </div>
</div>

<script>
  tampil();  
  
  $(function () {
      $(".select2").select2();

      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
      });
  });

  function tampil() {
    $.get('<?php echo base_url('Rolemenu/tampil'); ?>', function(data) {
      MyTable.fnDestroy();
      $('#tblUtamaRole').html(data);
      refresh();
    });
  }  

  // $('#tblUtamaRole').on('click','tr',function(){
  //   var id = $(this).find('td:eq(0)').text();    

  //   $.get('<?php echo base_url(); ?>Rolemenu/tampilaktif?id='+id, function(data) {
  //     MyTable.fnDestroy();
  //     $('#tblAktifRole').html(data);
  //     refresh();
  //   });

  // });

</script>

<script>  
    $('#tblUtamaRole').on('click','tr',function(){
        var id = $(this).find('td:eq(0)').text();
        var halaman = 1;
        var limit = 10;
        load_form_role(halaman, limit, id);
       
    });
</script>


<script>
  function load_form_role(halaman, limit, id) {
    $('#tblAktifRole').html('');
    $('#spinners_data').show();
    var halaman = 1;
      var limit = 100;
    $.ajax({
      type: 'POST',
      async: true,
      data: {
        halaman: halaman,
        limit: limit,
        id: id
      },
      dataType: 'html',
      url: '<?php echo base_url(); ?>RoleMenu/load_form_role/',
      success: function(html) {
        $('#tblAktifRole').html(html);
        $('#spinners_data').hide();
      }
    });
  }
</script>
