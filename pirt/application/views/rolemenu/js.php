<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>

<script type="text/javascript">
	var MyTable = $('#list-data').dataTable({
		  "paging": true,
		  "lengthChange": true,
		  "searching": true,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});

	var MyTable2 = $('#list-data2').dataTable({
		  "paging": true,
		  "lengthChange": true,
		  "searching": true,
		  "ordering": true,
		  "info": true,
		  "autoWidth": false
		});

	window.onload = function() {		
		tampilRoleMenu();
		
		<?php
			if ($this->session->flashdata('msg') != '') {
				echo "effect_msg();";
			}
		?>
	}

	function refresh() {
		MyTable = $('#list-data').dataTable();
	}

	function effect_msg_form() {
		// $('.form-msg').hide();
		$('.form-msg').show(1000);
		// $('.form-msg').show(1000);
		// $('.form-msg').animate({
  //                       opacity: 1,
  //                       right: "50px",
  //                       bottom: "10px",
  //                       height: "toggle",
  //                   }, 2000, function() {}).css('position','fixed');
		setTimeout(function() { $('.form-msg').fadeOut(1000); }, 3000);
	}

	function effect_msg() {
		// $('.msg').hide();
		$('.msg').show(1000);
		$('.msg').animate({
                        opacity: 1,
                        right: "50px",
                        bottom: "10px",
                        height: "toggle",
                    }, 2000, function() {}).css('position','fixed'); 

		setTimeout(function() { $('.msg').fadeOut(1000); }, 3000);
	}		
	
	
</script>

<script type="text/javascript">
	function tampilRoleMenu() {
		$.get('<?php echo base_url('RoleMenu/tampil'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#tblUtamaRole').html(data);
			refresh();
		});
	}

	function tampilAktifMenu() {
		$.get('<?php echo base_url('RoleMenu/tampilaktif'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#tblAktifRole').html(data);
			refresh();
		});
	}

	$('#tblUtamaRole').on('click','tr',function(){
            var id = $(this).find('td:eq(0)').text();
            
            $.get('<?php echo base_url('RoleMenu/tampilaktif'); ?>', function(data) {
			MyTable.fnDestroy();
			$('#tblAktifRole').html(data);
			refresh();
		});

            
      });

</script>
