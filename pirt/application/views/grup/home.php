<style>
  .box-form {
    background-color: white;
    margin-bottom: 15px;
    margin-left: 5px;
    margin-right: 5px;
  }
</style>

<div class="msg" style="display:visible;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
  <div  class="col-md-8">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          <span><label>LISTING DATA</label> 
            <!-- <label id="judul2"> </label></span> -->
        </div>        
      </div>

      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;">NO</th>
                <th style="display: none;">Id</th>
                <th style="text-align: center;">Nama Grup</th>                                       
              </tr>
            </thead>
            <tbody id="tblUtama">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>
  </div>

  <div  class="col-md-4">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          <span><label>FORM INPUT</label></label></span>
        </div>        
      </div>
      <div class="box-form">
        <!-- <form id="form-tambah-user" method="POST"> -->
            <div class="form-group" style="display: none;">
              <label for="id_grup">Id Grup</label>
              <input class="form-control" id="id_grup" name="id_grup" value="" placeholder="Id Grup" type="text" required="">
            </div> 

            <div class="form-group">
              <label for="nama_grup">Nama Grup</label>
              <input class="form-control" id="nama_grup" name="nama_grup" value="" placeholder="Nama Grup" type="text" required="">
            </div> 
            
            <center>
              <div class="form-group" id="formBtnSimpan" name="formBtnSimpan">
                
                <button type="submit" class="btn btn-primary" id="btnSimpan" name="btnSimpan"><i class="fa fa-save"></i>&nbsp;&nbsp;SIMPAN</button>  
                <button type="submit" class="btn btn-success" id="btnBaru" name="btnBaru"><i class="fa fa-plus"></i>&nbsp;&nbsp;BARU</button>        
              </div>

              <div class="form-group" style="display: none;" id="formBtnUpdate" name="formBtnUpdate">
                <button type="submit" class="btn btn-warning" id="btnBatal" name="btnBaru"><i class="fa fa-minus-square-o"></i>&nbsp;&nbsp;BATAL</button>
                <button type="submit" class="btn btn-primary" id="btnUpdate" name="btnSimpan"><i class="fa fa-save"></i>&nbsp;&nbsp;UPDATE</button>          
                <button type="submit" class="btn btn-danger" id="btnHapus" name="btnBaru"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;HAPUS</button>
              </div>
            </center>
                                  
            <br>  
        <!-- </form>       -->
      </div>  
    </div>
  </div>
</div>

<script type="text/javascript">
  tampil();
  baru();

  $(function () {
      $(".select2").select2();

      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
      });
  });

  function tampil() {
    $.get('<?php echo base_url('Grup/tampil'); ?>', function(data) {
      MyTable.fnDestroy();
      $('#tblUtama').html(data);
      refresh();
    });
  }
  
  function baru() {
      $('#id_grup').val("");
      $('#nama_grup').val("");
      
      $('#formBtnSimpan').show();

      $('#formBtnUpdate').hide();
      $('#lblsandi').hide();           
      
  }
</script>

<script>
    $('#btnBaru').on("click", function(){
      baru();
    });
</script>

<script>
    $('#btnSimpan').on("click", function(){
      var nama_grup = $('#nama_grup').val();
            
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            nama_grup:nama_grup
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Grup/insert',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              tampil();
              baru();             
            }
          }
      });
    });
</script>

<script>
  $('#tblUtama').on('click','tr',function(){
            var id = $(this).find('td:eq(1)').text();
            var nama=$(this).find('td:eq(2)').text();           

            $('#formBtnSimpan').hide();
            $('#formBtnUpdate').show();
                        
            $('#id_grup').val(id);
            $('#nama_grup').val(nama);
      });

</script>

<script>
  $('#btnBatal').on("click", function(){
    baru();
  });
</script>

<script>
    $('#btnUpdate').on("click", function(){
      var id = $('#id_grup').val();
      var nama_grup = $('#nama_grup').val();
      
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id:id,
            nama_grup:nama_grup
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Grup/update',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              tampil();
              baru()
            }
          }
      });
    });
</script>

<script>
    $('#btnHapus').on("click", function(){
      var id = $('#id_grup').val();
      
      alertify.confirm("Apakah anda yakin hapus data ini?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id:id
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Grup/hapus',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              tampil();
              baru();
            }
          }
      });
        } else {
          alertify.error("Batal hapus data");
        }
      });
      return false;
      
      
    });
</script>
