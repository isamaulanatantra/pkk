<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Ijin | PIRT</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/eksternal/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <nav class="navbar navbar-default">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
       <!-- <a class="navbar-brand" href="#">Ijin PIRT</a> -->
      </div>
      
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        
        
        <ul class="nav navbar-nav navbar-right">
          <li><button type="button" class="btn btn-primary navbar-btn" id="btnLoginAdmin">Login Admin</button></li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>  

  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="">Ijin<b>PIRT</b></a>
      </div>

      <!-- /.login-logo -->
      <div class="form_login" id="form_login">
      <div class="login-box-body">
        <p class="login-box-msg">
          Log in to start your session
        </p>

        <form action="<?php echo base_url('Auth/login_user'); ?>" method="post">
          <div class="form-group has-feedback">
            <input type="number" class="form-control" placeholder="No NIK" name="nik_login">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="number" class="form-control" placeholder="Nomor Hp" name="no_hp_login">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            
            <div class="col-xs-12">
              <button type="submit" class="btn btn-primary btn-block btn-flat">Log In</button>
            </div>                     
          </div>
        </form>

        <div class="social-auth-links text-center">
         <p class="text-center">Belum punya akun?</p>  
         <button id="btnPendaftaran" name="btnPendaftaran" class="btn btn-danger btn-block btn-flat">Daftar</button>
        </div>
        <!-- /.social-auth-links -->

        <!-- <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a> -->

      </div>
      </div>
      <!-- /.login-box-body -->
      <?php
        echo show_err_msg($this->session->flashdata('error_msg'));
      ?>

      <div class="form_daftar" id="form_daftar" style="display: none;">
      <div class="login-box-body">                
        <form class="form-horizontal" id="submit">
            <div class="form-group has-feedback">
            <label for="">No NIK</label>
            <input type="number" class="form-control" placeholder="No NIK" name="nik_daftar" id="nik_daftar">
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <label for="">Nomor Hp</label>
            <input type="number" class="form-control" placeholder="Nomor Hp" name="no_hp_daftar" id="no_hp_daftar">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>

          <div class="form-group has-feedback">
            <label for="">Nama Lengkap</label>
            <input type="text" class="form-control" placeholder="Nama" name="nama" id="nama">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>

          <div class="form-group">
            <label for="id_puskesmas">Wilayah Kerja Puskesmas</label>
            <select id="id_puskesmas" name="id_puskesmas" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%;">        
              <?php
              foreach ($dataPuskesmas as $data) {
                ?>
                <option value="<?php echo $data->msPuskesmasId; ?>">
                  <?php echo $data->msPuskesmasNama; ?>
                </option>
                <?php
              }
              ?>
            </select>
          </div>

          <div class="form-group">
            <label for="file">Upload Foto KTP</label>
            <input type="file" name="file" id="file" required accept=".png,.jpg,.jpeg">
          </div>            

          <div class="row">
            
            <div class="col-xs-12">
              <button type="submit" id="btnDaftar" name="btnDaftar"  class="btn btn-primary btn-block btn-flat">Daftar</button>
              <button type="submit" id="btnBatal" name="btnBatal"  class="btn btn-danger btn-block btn-flat">Batal</button>
            </div>                     
          </div>
          </form> 

        
         
      </div>
      </div>
    </div>
    

    <!-- /.login-box -->

    <!-- jQuery 2.2.3 -->
    <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
    


  </body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>assets/alertify/js/alertify.min.js"></script>
<!-- alertify -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/alertify/css/alertify.core.css" />
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/alertify/css/alertify.default.css" id="toggleCSS" />
<script src="<?php echo base_url(); ?>assets/plugins/select2/select2.full.min.js"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/plugins/select2/select2.min.css">

<script>
  $(function () {
      $(".select2").select2();     
  });
</script>

<script>
  $('#btnLoginAdmin').on('click', function(){
    window.location.href = "<?php echo base_url('admin'); ?>";
  });
</script>

<script>
  function baru(){
    $("#nik_daftar").val("");
    $("#no_hp_daftar").val("");
    $("#nama").val("");    
    $('#form_daftar').hide();
    $('#form_login').show();
  }
</script>

<script>
  $('#btnPendaftaran').on('click',function(){
    $('#form_daftar').show();
    $('#form_login').hide();
  });
</script>

<script>
  $('#btnBatal').on('click',function(){
    baru();
  });
</script>

<script>
  $(document).ready(function(){
    $('#submit').submit(function(e){
        e.preventDefault(); 
             $.ajax({
               url: '<?php echo base_url(); ?>Auth/insertUser',   
                 // url:'<?php echo base_url();?>index.php/upload/do_upload',
                 type:"post",
                 data:new FormData(this),
                 processData:false,
                 contentType:false,
                 cache:false,
                 async:false,
                  success: function(text){
                      if( text == 0 ){
                        alertify.error("Data belum lengkap");
                      }else if( text == 1 ){
                        alertify.error("Data gagal disimpan");

                      }else if( text == 3 ){
                        alertify.error("NIK sudah terdaftar");

                      }else if( text == 4 ){
                        alertify.error("NIK harus 16 digits");

                      }else if( text == 2 ){
                        alert("Pendaftaran berhasil, anda akan menerima sms konfirmasi jika di setujui");
                        baru();
                      }
               }
             });
        });
    
  });
</script>
