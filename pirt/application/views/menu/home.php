<style>
  .box-form {
    background-color: white;
    margin-bottom: 15px;
    margin-left: 5px;
    margin-right: 5px;
  }
</style>

<div class="msg" style="display:visible;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<div class="row">
  <div  class="col-md-8">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          <span><label>LISTING DATA</label> 
            <!-- <label id="judul2"> </label></span> -->
        </div>        
      </div>

      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;"  width="5%">NO</th>
                <th style="text-align: center; display: none;">Id</th>
                <th style="text-align: center; display: none;">Order</th>
                <th style="text-align: center; display: none;">Parent</th>
                <th style="text-align: center;">Nama Menu</th>
                <th style="text-align: center; display: none;">Link </th>
                <th style="text-align: center; display: none;">Icon </th>
                <th style="text-align: center;">Parent Menu</th>
                <!-- <th style="display: none;">Id Grup</th>
                <th style="text-align: center;">Grup User</th> -->                
              </tr>
            </thead>
            <tbody id="tblUtama">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>
  </div>

  <div  class="col-md-4">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          <span><label>FORM INPUT</label></label></span>
        </div>        
      </div>
      <div class="box-form">
        <!-- <form id="form-tambah-user" method="POST"> -->
            <div class="form-group" style="display: none;">
              <label for="id_menu">Id Menu</label>
              <input class="form-control" id="id_menu" name="id_menu" value="" placeholder="Id Menu" type="text" required="">
            </div> 

            <div class="form-group">
              <label for="nama_menu">Nama Menu</label>
              <input class="form-control" id="nama_menu" name="nama_menu" value="" placeholder="Nama Menu" type="text" required="">
            </div> 
            <div class="form-group">
              <label for="order_menu">Order Menu</label>
              <input class="form-control" id="order_menu" name="order_menu" value="" placeholder="Order Menu" type="text" required="">
            </div>            
            <div class="form-group">
              <label for="parent_menu">Parent Menu</label>
              <select id="parent_menu" name="parent_menu" class="form-control select2" aria-describedby="sizing-addon2">
                <option value="0">--</option>
                <?php
                foreach ($dataParent as $data) {
                  ?>
                  <option value="<?php echo $data->msMenuId; ?>">
                    <?php echo $data->msMenuNama; ?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </div>
            
            <div class="form-group">
              <label for="link_menu">Link Menu</label>
              <input class="form-control" id="link_menu" name="link_menu" value="" placeholder="Link Menu" type="text" required="">
            </div> 

            <div class="form-group">
              <label for="icon_menu">Icon Menu</label>
              <input class="form-control" id="icon_menu" name="icon_menu" value="" placeholder="Icon Menu" type="text" required="">
            </div> 
            
            <center>
              <div class="form-group" id="formBtnSimpan" name="formBtnSimpan">
                <button type="submit" class="btn btn-success" id="btnBaru" name="btnBaru"><i class="fa fa-plus"></i>&nbsp;&nbsp;BARU</button>
                <button type="submit" class="btn btn-primary" id="btnSimpan" name="btnSimpan"><i class="fa fa-save"></i>&nbsp;&nbsp;SIMPAN</button>          
              </div>

              <div class="form-group" style="display: none;" id="formBtnUpdate" name="formBtnUpdate">
                <button type="submit" class="btn btn-warning" id="btnBatal" name="btnBaru"><i class="fa fa-minus-square-o"></i>&nbsp;&nbsp;BATAL</button>
                <button type="submit" class="btn btn-primary" id="btnUpdate" name="btnSimpan"><i class="fa fa-save"></i>&nbsp;&nbsp;UPDATE</button>          
                <button type="submit" class="btn btn-danger" id="btnHapus" name="btnBaru"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;HAPUS</button>
              </div>
            </center>
                                  
            <br>  
        <!-- </form>       -->
      </div>  
    </div>
  </div>
</div>

<script type="text/javascript">
  tampil();
  baru();
  

  $(function () {
      $(".select2").select2();

      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
      });
  });

  function tampil() {
    $.get('<?php echo base_url('Menu/tampil'); ?>', function(data) {
      MyTable.fnDestroy();
      $('#tblUtama').html(data);
      refresh();
    });
  }

  function baru() {     
      $('#id_menu').val("");
      $('#nama_menu').val("");
      $('#order_menu').val("");
      $('#parent_menu').val("");
      $('#icon_menu').val("");
      $('#link_menu').val("");
      $('#parent_menu').val(0).trigger('change');

      $('#formBtnSimpan').show();
      $('#formBtnUpdate').hide();
  }
</script>

<script>
    $('#btnBaru').on("click", function(){
      baru();
    });
</script>

<script>
    $('#btnSimpan').on("click", function(){
      var nama_menu = $('#nama_menu').val();
      var order_menu = $('#order_menu').val();
      var parent_menu = $('#parent_menu').val();
      var icon_menu = $('#icon_menu').val();
      var link_menu = $('#link_menu').val();      
      
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            nama_menu:nama_menu,
            order_menu:order_menu,
            parent_menu:parent_menu,
            icon_menu:icon_menu,
            link_menu:link_menu
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Menu/insert',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              tampil();
              baru();             
            }
          }
      });
    });
</script>

<script>
  $('#tblUtama').on('click','tr',function(){
            var id = $(this).find('td:eq(1)').text();
            var order=$(this).find('td:eq(2)').text();
            var parent=$(this).find('td:eq(3)').text();
            var nama=$(this).find('td:eq(4)').text();  
            var link=$(this).find('td:eq(5)').text();
            var icon=$(this).find('td:eq(6)').text();

            $('#formBtnSimpan').hide();
            $('#formBtnUpdate').show();
            $('#lblsandi').show();
                        
            $('#id_menu').val(id);
            $('#nama_menu').val(nama);
            $('#order_menu').val(order);
            $('#parent_menu').val(parent).trigger('change');

            $('#icon_menu').val(icon);
            $('#link_menu').val(link);          

      });

</script>

<script>
  $('#btnBatal').on("click", function(){
    $('#formBtnSimpan').show();
    $('#formBtnUpdate').hide();
    $('#lblsandi').hide();

    baru()
  });
</script>

<script>
    $('#btnUpdate').on("click", function(){
      var id = $('#id_menu').val();
      var nama_menu = $('#nama_menu').val();
      var order_menu = $('#order_menu').val();
      var parent_menu = $('#parent_menu').val();
      var icon_menu = $('#icon_menu').val();
      var link_menu = $('#link_menu').val(); 
      
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id:id,
            nama_menu:nama_menu,
            order_menu:order_menu,
            parent_menu:parent_menu,
            icon_menu:icon_menu,
            link_menu:link_menu
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Menu/update',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              tampil();
              baru()
            }
          }
      });
    });
</script>

<script>
    $('#btnHapus').on("click", function(){
      var id = $('#id_menu').val();      

      alertify.confirm("Apakah anda yakin hapus data ini?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id:id
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Menu/hapus',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              tampil();
              baru();
            }
          }
      });
        } else {
          alertify.error("Batal hapus data");
        }
      });
      return false;
      
      
    });
</script>
