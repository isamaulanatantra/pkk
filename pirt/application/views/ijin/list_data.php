
<?php
  $no = 1;
  foreach ($dataSppirt as $data) {   

    // $status = $data->status;
    // if ($status == 1) {
    //   $ket = 'Menunggu verifikasi';
    //   $color = '#f4f108';
    // }elseif ($status == 2) {
    //   $ket = 'Terverifikasi';
    //   $color = '#08f45b';
    // }elseif ($status == 3) {
    //   $ket = 'Proses verifikasi';
    //   $color = '#f4f108';
    // }elseif ($status == 10) {
    //   $alasan = $data->msProdukAlasan;
    //   $ket = 'Tidak diverifikasi, '.$alasan;
    //   $color = '#ff0000';
    // }

    $tgl = $data->msSppIrtTglLengkap;
    $tambah = mktime(0,0,0,date('m')+1);
    $tgl_maks = date($tgl,$tambah_tanggal);

    $status = $data->status;
    if ($status == 1) {
      $ket = 'Menunggu verifikasi';
      $color = '#f4f108';
    }elseif ($status == 2) {
      $ket = 'Proses verifikasi';
      $color = '#FF8C00';
    }elseif ($status == 3) {
      $ket = 'Terverifikasi, jadwal kunjungan maksimal '.reverseDate($tgl_maks);
      $color = '#08f45b';
    }elseif ($status == 5) {
      $ket = 'Semua proses terverifikasi, silahkan cetak surat rekomendasi di dinas kesehatan ';
      $color = '#00FFFF';
    }elseif ($status == 10) {
      $alasan = $data->msProdukAlasan;
      $ket = 'Tidak diverifikasi, '.$alasan;
      $color = '#ff0000';
    }
    
  ?>
  <tr>
      <td style="text-align: center;"><?php echo $no; ?></td>
      <td  style="display: none;"><?php echo $data->msSppIrtId; ?></td>
      <td ><?php echo $data->msSppIrtNik; ?></td>
      <td ><?php echo $data->msSppIrtPerusahaan; ?></td>
      <td ><?php echo $data->msSppIrtPemilik; ?></td>
      <td ><?php echo $data->msSppIrtPenanggungJawab; ?></td>      
      <td ><?php echo $data->msSppIrtAlamat; ?></td>
      <td ><?php echo $data->msSppIrtNoHp; ?></td>
      <td ><?php echo $data->msSppIrtNib; ?></td>
      <td >
        <center>
          <button class="btn btn-primary" id="edit"><p style="display: none;"><?php echo $data->msSppIrtId; ?></p>-Edit</button>          
        </center>
      </td>  
      <td >
        <center>
          <button class="btn btn-danger" id="hapus"><p style="display: none;"><?php echo $data->msSppIrtId; ?></p>-Hapus</button>
        </center>
      </td> 

      <td >
        <center>
          <button class="btn btn-success" id="list"><p style="display: none;"><?php echo $data->msSppIrtId; ?></p>-Tambah Produk</button>
        </center>
      </td> 
      <td><p><mark style="background: <?php echo $color; ?>;"><?php echo $ket; ?></mark></p></td>
    </tr>
  <?php     
  $no++;
  }

  function reverseDate( $tgl ) {

        if ( substr($tgl, 2, 1) == '/' ) {
            $a=explode('/',$tgl);
        } else {
            $a=explode('-',$tgl);
            $d=$a[2];
            $m=$a[1];
            $y=$a[0];
            $a[0]=$m+0;
            $a[1]=$d;
            $a[2]=$y;
        }

        $nmbulan='';

        switch ( $a[0] ) {
            case 1 : $nmbulan   = 'Januari';    break;
            case 2 : $nmbulan   = 'Februari';   break;
            case 3 : $nmbulan   = 'Maret';      break;
            case 4 : $nmbulan   = 'April';      break;
            case 5 : $nmbulan   = 'Mei';        break;
            case 6 : $nmbulan   = 'Juni';       break;
            case 7 : $nmbulan   = 'Juli';       break;
            case 8 : $nmbulan   = 'Agustus';    break;
            case 9 : $nmbulan   = 'September';  break;
            case 10: $nmbulan   = 'Oktober';    break;
            case 11: $nmbulan   = 'November';   break;
            case 12: $nmbulan   = 'Desember';   break;
        }

        $res= $a[1] . ' ' . $nmbulan . ' ' . $a['2'];
        return $res;

    }
?>