
<?php
  $no = 1;
  foreach ($dataProduk as $data) {
    $kemasan = $this->Ijin_model->getKemasan($data->msProdukKodeKemasan);
    $urut = $this->Ijin_model->getNoPirt($data->msProdukIdIrt);

    $thnBuat = $data->msProdukThnBuat;
    $masaBerlaku = $data->msProdukMasaBerlaku;    
    $yy = $thnBuat+$masaBerlaku;
    $kadaluarsa = substr($yy, 2,2);
    
    if($urut < 10){
      $no_pirt = '000'.$urut;
    }else if($urut < 100){
      $no_pirt = '00'.$urut;
    }else if($urut < 1000){
      $no_pirt = '0'.$urut;
    }else{
      $no_pirt = $urut;
    }

    $no_pirt = $data->msProdukKodeKemasan.'.'.$data->msProdukKodeJenis.'.33.07.0'.$data->msProdukNoUrutProduk.'.'.$no_pirt.'.'.$kadaluarsa;

    $status = $data->status;
    if ($status == 1) {
      $ket = 'Menunggu verifikasi';
      $color = '#f4f108';
    }elseif ($status == 2) {
      $ket = 'Terverifikasi';
      $color = '#08f45b';
    }elseif ($status == 10) {
      $alasan = $data->msProdukAlasan;
      $ket = 'Tidak diverifikasi, '.$alasan.' silhkan hapus dan tambah produk lagi';
      $color = '#ff0000';
    }
    
    ?>
    <tr>
      <td style="text-align: center;"><?php echo $no; ?></td>      
      <td  style="display: none;"><?php echo $data->msProdukId; ?></td>
      <td ><?php echo $data->msProdukNama; ?></td>
      <td  style="display: none;"><?php echo $data->msProdukKodeKemasan; ?></td>      
      <th style="display: none;"><?php echo $kemasan; ?></td>
      <td style="text-align: center;"><?php echo $data->msKemasanPrimer; ?></td>
      <td  style="display: none;"><?php echo $data->msProdukKodeJenis; ?></td>      
      <td style="display: none;"><?php echo $no_pirt; ?></td>
      <td  style="display: none;"><?php echo $data->msProdukMasaBerlaku; ?></td>
      <td style="display: none;"><?php echo $data->msProdukMasaBerlaku; ?> Tahun</td>
      <td style="display: visible;">
          <a href="<?php echo base_url(); ?>/admin/gambar/<?php echo $data->msProdukLampiranLabel; ?>"  target="_blank" class="btn btn-primary">
            Lihat Lampiran  
          </a>
      </td>
       <td style="display: visible;">
          <a href="<?php echo base_url(); ?>/admin/gambar/<?php echo $data->msProdukLampiranBagan; ?>"  target="_blank" class="btn btn-primary">
            Lihat Lampiran  
          </a>
      </td>

       <td style="display: none;">
          <button class="btn btn-warning" id="edit_produk"><p style="display: none;"><?php echo $data->msProdukId; ?></p>-Edit</button>
      </td>
      <td style="text-align: center;">
          <button class="btn btn-danger" id="hapus_produk"><p style="display: none;"><?php echo $data->msProdukId; ?></p>-Hapus</button>
      </td>
      <td><p><mark style="background: <?php echo $color; ?>;"><?php echo $ket; ?></mark></p></td>
      
    </tr>
    <?php
    $no++;
  }
?>