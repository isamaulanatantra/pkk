<style>
  .box-form {
    background-color: white;
    margin-bottom: 15px;
    margin-left: 5px;
    margin-right: 5px;
  }
</style>

<div class="msg" style="display:visible;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<?php 
  $id_user = $userdata->msUserId;
  foreach ($dataPeserta as $data) {    
    $nik = $data->msPesertaNik;
    $no_hp = $data->msPesertaNoHp;
    $nama_perusahaan = $data->msPesertaNamaPerusahaan;
    $nama_pemilik = $data->msPesertaPemilik;
    $nama_penanggung_jawab = $data->msPesertaPenanggungJawab;
    $alamat = $data->msPesertaAlamat;
  }
?>

<?php 
  if ($dataPenyuluhan > 0 ) {
    ?> 
    <div class="row">
      <div class="btnTambah" id='btnTambah' name='btnTambah' style="display: visible;">
        <div  class="col-md-12">
          <div class="box">
            <div class="box-header">
              <div class="box-name">
                <div class="col-md-8 col-md-offset-4 ">
                  <button class="btn btn-success btn-lg" id="daftar" name="daftar" ><i class="glyphicon glyphicon-plus"></i>&nbsp;TAMBAH PENDAFTARAN IJIN PIRT</button>
                </div> 
              </div>      
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="table" id="table">
      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;">No</th>
                <th style="display: none;">Id</th>
                <th style="text-align: center;">NIK</th>
                <th style="text-align: center;">Nama Perusahaan</th>
                <th style="text-align: center;">Pemilik</th>
                <th style="text-align: center;">Penanggung Jawab</th>
                <th style="text-align: center;">Alamat</th>
                <th style="text-align: center;">No Hp</th>
                <th style="text-align: center;">NIB</th>
                <th style="text-align: center;">Edit</th>
                <th style="text-align: center;">Hapus</th>
                <th style="text-align: center;">Produk</th>
                <th style="text-align: center;">Status</th>
              </tr>
            </thead>
            <tbody id="tblUtama">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>

    <div id='form_isian' name='form_isian' style="display: none;">
      <div class="row">      
        <div  class="col-md-8 col-md-offset-2">
          <div class="box">
            <div class="box-header">
              <div class="box-name">            
                  <center>
                    <h1>FORM INPUT</h1>                     
                  </center>
                  <div class="box-form">
                    <form class="form-horizontal" id="submit_daftar">
                      <div  class="col-md-10 col-md-offset-1">
                        <div class="col-md-5">
                           <!--  <div class="form-group" style="display: none;">
                            <label for="id_pendaftaran">Id Pendaftaran</label>
                            <input class="form-control" id="id_pendaftaran" name="id_pendaftaran" value="" placeholder="Id Pendaftaran" type="text" required="">
                          </div> -->

                          <div class="form-group" style="visibility:hidden">
                            <!-- <label for="id_user">Id User</label> -->
                            <input class="form-control" id="id_user" name="id_user" placeholder="Id User" type="text" required="">
                          </div>             

                          <div class="form-group">
                            <label for="nik">NIK</label>
                            <input class="form-control" id="nik" name="nik" placeholder="NIK" type="text">
                          </div>

                          <div class="form-group">
                            <label for="nama_perusahaan">Nama Perusahaan</label>
                            <input class="form-control" id="nama_perusahaan" name="nama_perusahaan"  placeholder="Nama Perusahaan" type="text">
                          </div>

                          <div class="form-group">
                            <label for="nama_pemilik">Nama Pemilik</label>
                            <input class="form-control" id="nama_pemilik" name="nama_pemilik" placeholder="Nama Pemilik" type="text" >
                          </div> 

                          <div class="form-group">
                            <label for="nama_penanggung_jawab">Nama Penanggung Jawab</label>
                            <input class="form-control" id="nama_penanggung_jawab" name="nama_penanggung_jawab" placeholder="Nama Penanggung Jawab" type="text" >
                          </div>

                           
                        </div>

                        <div class="col-md-6 col-md-offset-1">

                          <br><br><br>

                          <div class="form-group">
                            <label for="alamat">Alamat</label>
                            <textarea class="form-control" rows="3" id="alamat" name="alamat" placeholder="Alamat" type="text"></textarea>
                            <!-- <input class="form-control" id="alamat" name="alamat" placeholder="No HP" type="text"> -->
                          </div>

                          <div class="form-group">
                            <label for="no_hp">No HP</label>
                            <input class="form-control" id="no_hp" name="no_hp" placeholder="No HP" type="text">
                          </div> 

                          <div class="form-group">
                            <label for="nib">No NIB</label>
                            <input class="form-control" id="nib" name="nib" value="" placeholder="NIB" type="text" required="" >
                          </div>    

                          <div class="form-group">
                            <label for="file">Pas Foto 4x6</label>
                            <input type="file" name="file" id="file" required accept=".png,.jpg,.jpeg">
                          </div>

                          
                        </div>

                        <div class="form-group" style="visibility: hidden;">
                            <label for="file">Pas Foto 4x6</label>                            
                        </div>

                        <center>
                          <div class="form-group" id="formBtnSimpan" name="formBtnSimpan" style="display: none;"> 
                            <button type="submit" class="btn btn-primary" id="btnSimpan" name="btnSimpan"><i class="fa fa-save"></i>&nbsp;&nbsp;SIMPAN</button>  
                            <button type="submit" class="btn btn-danger" id="btnBatalSimpan" name="btnBatalSimpan"><i class="fa fa-close"></i>&nbsp;&nbsp;BATAL</button>        
                          </div>  
                        </center>
                  
                  

                    
                  </div>
                  </form>

                  </div>        
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id='form_edit' name='form_edit' style="display: none;">
      <div class="row">      
        <div  class="col-md-4 col-md-offset-4">
          <div class="box">
            <div class="box-header">
              <div class="box-name">            
                  <center>
                    <label>FORM INPUT</label>                     
                  </center>
                  <div class="box-form">
                  
                    <div class="form-group" style="display: none;">
                      <label for="id_pendaftaran_edit">Id Pendaftaran</label>
                      <input class="form-control" id="id_pendaftaran_edit" name="id_pendaftaran_edit" value="" placeholder="Id Pendaftaran" type="text" required="">
                    </div>

                    <div class="form-group" style="display: none;">
                      <label for="id_user_edit">Id User</label>
                      <input class="form-control" id="id_user_edit" name="id_user_edit" placeholder="Id User" type="text" required="">
                    </div>             

                    <span style="display:none;" id="spin_cari" class="input-group-addon"><i class="fa fa-refresh fa-spin"></i></span>

                    <div class="form-group">
                      <label for="nik_edit">NIK</label>
                      <input class="form-control" id="nik_edit" name="nik_edit" placeholder="NIK" type="text">
                    </div>

                    <div class="form-group">
                      <label for="nama_perusahaan_edit">Nama Perusahaan</label>
                      <input class="form-control" id="nama_perusahaan_edit" name="nama_perusahaan_edit"  placeholder="Nama Perusahaan" type="text">
                    </div>

                    <div class="form-group">
                      <label for="nama_pemilik_edit">Nama Pemilik</label>
                      <input class="form-control" id="nama_pemilik_edit" name="nama_pemilik_edit" placeholder="Nama Pemilik" type="text" >
                    </div> 

                    <div class="form-group">
                      <label for="nama_penanggung_jawab_edit">Nama Penanggung Jawab</label>
                      <input class="form-control" id="nama_penanggung_jawab_edit" name="nama_penanggung_jawab_edit" placeholder="Nama Penanggung Jawab" type="text" >
                    </div>

                    <div class="form-group">
                      <label for="alamat_edit">Alamat</label>
                      <textarea class="form-control" rows="3" id="alamat_edit" name="alamat_edit" placeholder="Alamat" type="text"></textarea>
                      <!-- <input class="form-control" id="alamat" name="alamat" placeholder="No HP" type="text"> -->
                    </div> 

                    <div class="form-group">
                      <label for="no_hp_edit">No HP</label>
                      <input class="form-control" id="no_hp_edit" name="no_hp_edit" placeholder="No HP" type="text">
                    </div> 

                    <div class="form-group">
                      <label for="nib_edit">No NIB</label>
                      <input class="form-control" id="nib_edit" name="nib_edit" value="" placeholder="NIB" type="text" required="" >
                    </div>    

                    <center>
                    <div class="form-group" id="formBtnEdit" name="formBtnEdit" style="display: none;"> 
                      <button type="submit" class="btn btn-primary" id="btnUpdate" name="btnUpdate"><i class="fa fa-save"></i>&nbsp;&nbsp;UPDATE</button>  
                      <button type="submit" class="btn btn-danger" id="btnBatalUpdate" name="btnBatalUpdate"><i class="fa fa-close"></i>&nbsp;&nbsp;BATAL</button>        
                    </div>  
                    </center>

                  </div>        
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <?php 
  }else{
    ?>
      <div class="row">
      <div class="btnTambah" id='btnTambah' name='btnTambah' style="display: visible;">
        <div  class="col-md-12">
          <div class="box">
            <div class="box-header">
              <div class="box-name">
                <div class="col-md-10 col-md-offset-1 ">
                  <button class="btn btn-danger btn-lg" id="" name="" >ANDA BELUM MENGIKUTI PENYULUHAN KEMANAN PANGAN ATAU BELUM MENGAMBIL SERTIFIKAT<br>SILAHKAN DAFTAR TERLEBIH DAHULU ATAU AMBIL SERTIFIKAT PENYULUHAN PANGAN DI DINAS KESEHATAN</button>
                </div> 
              </div>        
            </div>
          </div>
        </div>
      </div>
    </div>      
    <?php 
  }
?> 

<script>
  $(function () {
      $(".select2").select2();

      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
      });
  });
</script>

<script>
  function baru(){
    document.getElementById("id_user").value = "<?php echo $id_user ?>";
    document.getElementById("nik").value = "<?php echo $nik ?>";
    document.getElementById("nama_perusahaan").value = "<?php echo $nama_perusahaan; ?>";
    document.getElementById("nama_pemilik").value = "<?php echo $nama_pemilik; ?>";
    document.getElementById("nama_penanggung_jawab").value = "<?php echo $nama_penanggung_jawab; ?>";
    document.getElementById("alamat").value = "<?php echo $alamat; ?>";
    document.getElementById("no_hp").value = "<?php echo $no_hp; ?>";
    $('#nib').val('');
  }
</script>

<script>
  function tampil() {
    $.get('<?php echo base_url('Pendaftaran_ijin/tampilIjin'); ?>', function(data) {
      MyTable.fnDestroy();
      $('#tblUtama').html(data);
      refresh();
    });
  } 
</script>

<script>
  tampil();
</script>

<script>
  $('#daftar').on('click',function(){
    $('#form_isian').show();
    $('#btnTambah').hide();
    $('#table').hide();
    baru();
    $('#formBtnSimpan').show();
  });
</script>

<script>
  $('#btnBatalSimpan, #btnBatalUpdate').on('click',function(){
    $('#form_isian').hide();
    $('#btnTambah').show();
    $('#table').show();
    baru();
    tampil();
    $('#formBtnSimpan').hide();
    $('#formBtnEdit').hide();
    $('#form_edit').hide();
  });  
</script>

<script>
  $(document).ready(function(){
    $('#submit_daftar').submit(function(e){
        e.preventDefault(); 
             $.ajax({
               url: '<?php echo base_url(); ?>Pendaftaran_ijin/insert',   
                 // url:'<?php echo base_url();?>index.php/upload/do_upload',
                 type:"post",
                 data:new FormData(this),
                 processData:false,
                 contentType:false,
                 cache:false,
                 async:false,
                  success: function(text){
                      if( text == 0 ){
                        alertify.error("Data belum lengkap");
                      }else if( text == 1 ){
                        alertify.error("Data gagal disimpan");

                      }else{
                        alertify.success("Data berhasil disimpan");
                        $('#form_isian').hide();
                        $('#btnTambah').show();
                        $('#table').show();
                        baru();
                        tampil();
                        $('#formBtnSimpan').hide();
                      }
               }
             });
        });
    
  });
</script>


<!-- <script>
    $('#btnSimpan').on("click", function(){
      var id_user = $('#id_user').val();
      var nik = $('#nik').val();
      var nama_perusahaan = $('#nama_perusahaan').val();
      var nama_pemilik = $('#nama_pemilik').val();
      var nama_penanggung_jawab = $('#nama_penanggung_jawab').val();
      var alamat = $('#alamat').val();
      var no_hp = $('#no_hp').val();
      var nib = $('#nib').val();
            
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_user:id_user,
            nik:nik,
            nama_perusahaan:nama_perusahaan,
            nama_pemilik:nama_pemilik,
            nama_penanggung_jawab:nama_penanggung_jawab,
            alamat:alamat,
            no_hp:no_hp,
            nib:nib
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Pendaftaran_ijin/insert',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else{
              alertify.success("Data berhasil disimpan");
              $('#form_isian').hide();
              $('#btnTambah').show();
              $('#table').show();
              baru();
              tampil();
              $('#formBtnSimpan').hide();
            }
          }
      });
    });
</script> -->

<script>
  $('#tblUtama').on('click','#edit',function(){
    
      var id = $( this ).text();   
      var str_id_peserta = id.split("-");

      var str_id_peserta = str_id_peserta[0];
      var id_peserta = str_id_peserta.trim();
      
      $('#id_pendaftaran_edit').val(id_peserta);
      load_sppirt_by_id();

      $('#form_edit').show();
      $('#btnTambah').hide();
      $('#table').hide();
      $('#formBtnEdit').show();     

  });
</script>

<script>
  function load_sppirt_by_id(){
    $("#spin_cari").show();      

    var id_sppirt = $('#id_pendaftaran_edit').val();
  
    $.ajax({
        type: "POST", 
        url: "<?php echo base_url(); ?>Pendaftaran_ijin/load_sppirt_by_id", 
        data: {id_sppirt : id_sppirt}, 
        dataType: "json",
        beforeSend: function(e) {
            if(e && e.overrideMimeType) {
                e.overrideMimeType("application/json;charset=UTF-8");
            }
    },
    success: function(response){ 
        $("#spin_cari").hide();
        if(response.status == "success"){         

          $('#id_user_edit').val(response.id_user);
          $('#nik_edit').val(response.nik);
          $('#no_hp_edit').val(response.no_hp);
          $('#nama_perusahaan_edit').val(response.nama_perusahaan);
          $('#nama_pemilik_edit').val(response.nama_pemilik);
          $('#nama_penanggung_jawab_edit').val(response.nama_penanggung_jawab);
          $('#alamat_edit').val(response.alamat);
          $('#nib_edit').val(response.nib);


        }else{ 
          $("#spin_cari").hide();
          alert("Data Tidak Ditemukan");
          // location.href ="<?php echo base_url(); ?>mesin_antrian";
        }
    },
    error: function (xhr, ajaxOptions, thrownError) { 
      alert(xhr.responseText);
    }
    });
}
</script>


<script>
  $('#btnUpdate').on('click', function(){
      var id_pendaftaran_edit = $("#id_pendaftaran_edit").val();
      var nik_edit = $('#nik_edit').val();
      var nama_perusahaan_edit = $('#nama_perusahaan_edit').val();
      var nama_pemilik_edit = $('#nama_pemilik_edit').val();
      var nama_penanggung_jawab_edit = $('#nama_penanggung_jawab_edit').val();
      var alamat_edit = $('#alamat_edit').val();
      var no_hp_edit = $('#no_hp_edit').val();
      var nib_edit = $('#nib_edit').val();
            
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_pendaftaran_edit:id_pendaftaran_edit,
            nik_edit:nik_edit,
            no_hp_edit:no_hp_edit,
            nama_perusahaan_edit:nama_perusahaan_edit,
            nama_pemilik_edit:nama_pemilik_edit,
            nama_penanggung_jawab_edit:nama_penanggung_jawab_edit,
            alamat_edit:alamat_edit,
            nib_edit:nib_edit
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Pendaftaran_ijin/updatePeserta',
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else if( text == 3 ){
              alertify.error("NIK sudah terdaftar");

            }else{
              alertify.success("Data berhasil disimpan");
              $('#form_isian').hide();
              $('#btnTambah').show();
              $('#table').show();
              baru();
              tampil();
              $('#formBtnSimpan').hide();
              $('#formBtnEdit').hide();
              $('#form_edit').hide();
            }
          }
      });
  });
</script>

<script>
  $('#tblUtama').on('click','#hapus',function(){
    
      var id = $( this ).text();   
      var str_id_peserta = id.split("-");

      var str_id_peserta = str_id_peserta[0];
      var id_pendaftaran_edit = str_id_peserta.trim();

      alertify.confirm("Apakah anda yakin hapus data ini?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_pendaftaran_edit:id_pendaftaran_edit
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Pendaftaran_ijin/hapusIjin',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal dihapus");

            }else{
              alertify.success("Data berhasil dihapus");
              $('#form_isian').hide();
              $('#btnTambah').show();
              $('#table').show();
              baru();
              tampil();
              $('#formBtnSimpan').hide();
              $('#formBtnEdit').hide();
              $('#form_edit').hide();
            }
          }
      });
        } else {
          alertify.error("Batal hapus data");
        }
      });
      return false;

  });
</script>

<div class="produk" id="produk" name="produk" style="display: none;">
<div class="row">

  <div  class="col-md-4">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <i class="fa fa-user"></i>
          
          <span><label>FORM TAMBAH PRODUK</label></label></span>
        </div>        
      </div>
      <div class="box-form">
        <form class="form-horizontal" id="submit">
          <div class="row">
          <div class="col-md-10 col-md-offset-1">
            <div class="form-group" style="display: none;">
              <label for="id_produk">Id Produk</label>
              <input class="form-control" id="id_produk" name="id_produk" value="" placeholder="Id Produk" type="text">
            </div> 

            <div class="form-group" style="display: none;">
              <label for="id_pirt2">Id PIRT</label>
              <input class="form-control" id="id_pirt2" name="id_pirt2" value="" placeholder="Id PIRT" type="text" required="">
            </div>

            <div class="form-group">
              <label for="nama_produk">Nama Produk</label>
              <input class="form-control" id="nama_produk" name="nama_produk" value="" placeholder="Nama Produk" type="text" required="" >
            </div>       

            <div class="form-group">
              <label for="jenis_kemasan">Jenis Kemasan</label>
              <select id="jenis_kemasan" name="jenis_kemasan" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%;">        
                <?php
                foreach ($dataKemasan as $data) {
                  ?>
                  <option value="<?php echo $data->msKemasanKode; ?>">
                    <?php echo $data->msKemasanNama; ?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </div>      

            <div class="form-group">
              <label for="kemasan_primer">Kemasan Primer</label>
              <input class="form-control" id="kemasan_primer" name="kemasan_primer" value="" placeholder="Kemasan Primer" type="text" required="" >
            </div>       

            <div class="form-group">
              <label for="jenis_pangan">Jenis Pangan</label>
              <select id="jenis_pangan" name="jenis_pangan" class="form-control select2" aria-describedby="sizing-addon2" style="width: 100%;">        
                <?php
                foreach ($dataPangan as $data) {
                  ?>
                  <option value="<?php echo $data->msJenisPanganKode; ?>">
                    <?php echo $data->msJenisPanganNama; ?>
                  </option>
                  <?php
                }
                ?>
              </select>
            </div>   

            <div class="form-group" style="display: none;"> 
              <label for="masa_berlaku">Masa Berlaku</label>
              <select id="masa_berlaku" name="masa_berlaku" class="form-control" aria-describedby="sizing-addon2" style="width: 100%;">        
                <option value="5">5 Tahun</option>
                <option value="4">4 Tahun</option>
                <option value="3">3 Tahun</option>
                <option value="2">2 Tahun</option>
                <option value="1">1 Tahun</option>
              </select>
            </div>                  

            <div class="form-group">
              <label for="file">Lable<br>(File Pdf/Word)</label>
              <input type="file" name="file1" id="file1" accept=".pdf,.doc,.docx" required>
            </div>

            <div class="form-group">
              <label for="file">Alur Bagan Produksi<br>(File Pdf/Word)</label>
              <input type="file" name="file2" id="file2" accept=".pdf,.doc,.docx" required>
            </div>

            <center>
              <div class="form-group" id="formBtnSimpanProduk" name="formBtnSimpanProduk">
                
                <button type="submit" class="btn btn-primary" id="btnSimpanProduk" name="btnSimpanProduk"><i class="fa fa-save"></i>&nbsp;&nbsp;SIMPAN</button>  
                <button type="" class="btn btn-success" id="btnBaruProduk" name="btnBaruProduk"><i class="fa fa-plus"></i>&nbsp;&nbsp;BARU</button>        
              </div>

              <div class="form-group" style="display: none;" id="formBtnUpdateProduk" name="formBtnUpdateProduk">
                <button type="submit" class="btn btn-warning" id="btnBatalProduk" name="btnBatalProduk"><i class="fa fa-minus-square-o"></i>&nbsp;&nbsp;BATAL</button>
                <button type="submit" class="btn btn-primary" id="btnUpdateProduk" name="btnUpdateProduk"><i class="fa fa-save"></i>&nbsp;&nbsp;UPDATE</button>          
                <button type="submit" class="btn btn-danger" id="btnHapusProduk" name="btnHapusProduk"><i class="fa fa-trash-o"></i>&nbsp;&nbsp;HAPUS</button>
              </div>
            </center>
                                  
            <br>  
          </div>
        </div>
        </form>      

      </div>  
    </div>
  </div>


  <div  class="col-md-8">
    <div class="box">
      <div class="box-header">
        <div class="box-name">
          <div class="col-md-10">
            <div class="form-group" style="display: none;">
              <input class="form-control" id="id_pirt" name="id_pirt" value="" placeholder="" type="text" required="">
            </div>
          </div>
          <div class="col-md-2">
            <label for="tgl_jadwal">&nbsp;</label>
            <button class="btn btn-warning" id="selesai" name="selesai" ><i class="glyphicon glyphicon-remove"></i>&nbsp;SELESAI
          </div>

          <i class="fa fa-user"></i>
          <span><label>LISTING PRODUK</label> 
            <!-- <label id="judul2"> </label></span> -->
        </div>        
      </div>

      <div class="box-form">
        <div class="box-content no-padding table-responsive">
          <table id="list-data3" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th style="text-align: center;">No</th>
                <th style="display: none;">Id</th>
                <th style="text-align: center;">Nama Produk</th>
                <th style="display: none;">Kode Kemasan</th>
                <th style="display: none;">Jenis Kemasan</th>
                <th style="text-align: center;">Kemasan Primer</th>
                <th style="display: none;">Kode Jenis Pangan</th>
                <th style="display: none;">N0. PIRT</th>
                <th style="display: none;">Mas Berlaku</th>
                <th style="display: none;">Masa berlaku SPP-IRT</th>
                <th style="text-align: center;">Label</th>
                <th style="text-align: center;">Bagan Alur</th>
                <th style="display: none;">Edit</th>                
                <th style="text-align: center;">Hapus</th>
                <th style="text-align: center;">Status</th>                
              </tr>
            </thead>
            <tbody id="tblProduk">            
            </tbody>
          </table>
        </div>
        <br>
      </div>
    </div>
  </div>

  
</div>
</div>

<script>
  function tampilProduk(id_pirt) {    
    $.get('<?php echo base_url();?>Pendaftaran_ijin/tampilProduk?id_pirt='+id_pirt, function(data) {
      MyTable3.fnDestroy();
      $('#tblProduk').html(data);
      refresh();
    });
  }
</script>

<script>
  function baruProduk() {
      $('#id_produk').val("");
      // $('#id_pirt').val("");
      // $('#id_pirt2').val("");
      $('#nama_produk').val("");
      $('#jenis_kemasan').val("1").trigger('change');
      $('#kemasan_primer').val("");
      $('#jenis_pangan').val("01").trigger('change');
      $('#masa_berlaku').val("5").trigger('change');
      $('#file1').val("");
      $('#file2').val("");

      $('#formBtnSimpanProduk').show();
      $('#formBtnUpdateProduk').hide();      
      
  }
</script>

<script>
  $('#tblUtama').on('click','#list',function(){
    
      var id = $( this ).text();   
      var str_id_peserta = id.split("-");

      var str_id_peserta = str_id_peserta[0];
      var id_peserta = str_id_peserta.trim();

      $('#id_pirt').val(id_peserta);
      $('#id_pirt2').val(id_peserta);
      
      
      $('#btnTambah').hide();
      $('#table').hide();
      $('#produk').show();     

      baru();
      tampilProduk(id_peserta);

  });
</script>

<script>
  $('#selesai').on('click',function(){
      $('#btnTambah').show();
      $('#table').show();
      $('#produk').hide();
  });
</script>

<script>
    $('#btnBaruProduk').on("click", function(){
      baruProduk();
    });
</script>

<script>
  $(document).ready(function(){
    $('#submit').submit(function(e){
        e.preventDefault(); 
             $.ajax({
               url: '<?php echo base_url(); ?>Pendaftaran_ijin/insertProduk',   
                 // url:'<?php echo base_url();?>index.php/upload/do_upload',
                 type:"post",
                 data:new FormData(this),
                 processData:false,
                 contentType:false,
                 cache:false,
                 async:false,
                  success: function(text){
                      if( text == 0 ){
                        alertify.error("Format file lampiran tidak sesuasi");
                      }else if( text == 1 ){
                        alertify.error("Data gagal disimpan");

                      }else if( text == 3 ){
                        alertify.error("Maksimal pendaftaran 5 produk");

                      }else if( text == 4 ){
                        alertify.error("NIK harus 16 digits");

                      }else if( text == 2 ){
                        alertify.success("Berhasil tambah produk");
                        baruProduk();
                        $('#file1').val("");
                        $('#file2').val("");
                      }
               }
             });
        });
    
  });
</script>

<script>
  function baruProduk(){
    $('#id_produk').val("");
    $('#nama_produk').val("");
    $('#jenis_kemasan').val("1").trigger('change');
    $('#kemasan_primer').val("");
    $('#jenis_pangan').val("01").trigger('change');
    $('#masa_berlaku').val("5").trigger('change');
    $('#file').val("");

    var id_produk = $('#id_pirt2').val();

    tampilProduk(id_produk);
                        
  }
</script>

<script>
  $('#tblProduk').on('click','#hapus_produk',function(){
    
      var id = $( this ).text();   
      var str_id_peserta = id.split("-");

      var str_id_peserta = str_id_peserta[0];
      var id_produk = str_id_peserta.trim();

      alertify.confirm("Apakah anda yakin hapus data ini?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_produk:id_produk
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Pendaftaran_ijin/hapusProduk',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal dihapus");

            }else{
              alertify.success("Data berhasil dihapus");
              baruProduk();
            }
          }
      });
        } else {
          alertify.error("Batal hapus data");
        }
      });
      return false;

  });
</script>