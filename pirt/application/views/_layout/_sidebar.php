<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">

    <!-- Sidebar user panel (optional) -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="<?php echo base_url(); ?>assets/img/<?php echo $userdata->msUserFoto; ?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $userdata->msUserNama; ?></p>
        <!-- Status -->
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>    

    <!-- Sidebar Menu -->
    <ul class="sidebar-menu">
      <li class="header">LIST MENU</li>
      <!-- Optionally, you can add icons to the links -->

      <li <?php if ($page == 'home') {echo 'class="active"';} ?>>
        <a href="<?php echo base_url('Home'); ?>">
          <i class="fa fa-home"></i>
          <span>Home</span>
        </a>
      </li>

      
          
            <?php 
                $sqlparent = "SELECT * FROM rolmenu LEFT JOIN msmenu ON msMenuId = rolMenuMsMenu WHERE msMenuParent='0' AND rolMenuMsGrupMenu = '".$userdata->msUserGrupMenuId."' AND rolMenuStatus = '1' AND status != '99' ORDER BY msMenuOrder ASC";
                $dataparent = $this->db->query($sqlparent);
                foreach ($dataparent->result() as $tampilparent) {
                    ?>
                    <li class="treeview">
                      <a href="<?php echo $tampilparent->msMenuLink; ?>">
                        <i class="fa <?php echo $tampilparent->msMenuIcon ?>"></i>
                        <span><?php echo $tampilparent->msMenuNama ?></span>
                        <!-- <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span> -->

                      </a>
                        <!-- <ul class="treeview-menu">
                          <?php 
                              $parent = $tampilparent->msMenuId;
                              $sqlchild = " SELECT * FROM rolmenu LEFT JOIN msmenu ON msMenuId = rolMenuMsMenu WHERE msMenuParent='".$parent."' AND rolMenuMsGrupMenu = '".$userdata->msUserGrupMenuId."' AND rolMenuStatus = '1' ORDER BY msMenuOrder ASC";

                              $datachild = $this->db->query($sqlchild);
                              foreach ($datachild->result() as $tampilchild) {
                                ?>
                                  <li <?php if ($page == $tampilchild->msMenuLink) {echo 'class="active"';} ?>>
                                    <a href="<?php echo base_url($tampilchild->msMenuController); ?>">
                                      <i class="fa <?php echo $tampilchild->msMenuIcon ?>"></i>
                                      <span><?php echo$tampilchild->msMenuNama; ?></span>
                                    </a>
                                  </li>          
                                <?php
                              }
                           ?>
                        </ul> -->
                        </li>
                    <?php 
                }
             ?>
        

      <!-- <?php 

        $sql = " SELECT * FROM msmenu WHERE msMenuParent='0'";

        $data = $this->db->query($sql);
        foreach ($data->result() as $tampil) {
          ?>
            <li <?php if ($page == $tampil->msMenuLink) {echo 'class="active"';} ?>>
              <a href="<?php echo base_url($tampil->msMenuController); ?>">
                <i class="fa fa-user"></i>
                <span><?php echo$tampil->msMenuNama; ?></span>
              </a>
            </li>          
          <?php
        }

     ?> -->

      
    <!-- /.sidebar-menu -->
  </section>
  <!-- /.sidebar -->
</aside>