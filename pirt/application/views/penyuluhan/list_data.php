
<?php
  $no = 1;
  foreach ($dataPeserta as $data) {   
    $status = $data->status;
      if ($status == '1') {
        $ket = 'Menunggu verifikasi';
        $color = 'btn btn-warning';
        $style = 'display: none;';        
      }else if ($status == '2') {
        $ket = 'Terverifikasi, silahkan <br> cetak undangan <br> dan kartu peserta';
        $color = 'btn btn-primary';        
        $style = 'display: visible;';

        $undangan = '<a href="<?php echo base_url("cetakundangan?id_peserta=$data->msPesertaId"); ?>"  target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Undangan</a>';

        $kartu = '<a href="<?php echo base_url("cetakkartu?id_peserta=$data->msPesertaId"); ?>"  target="_blank" class="btn btn-warning"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Kartu</a>';

      }else if ($status == '10') {
        // $alasan = $data->msPesertaAlasan;
        $ket = 'Tidak disetujui, <br> silahkan hapus dan <br> daftar ulang';
        $color = 'btn btn-danger';        
        $style = 'display: none;';
        $undangan ='';
        $kartu = '';
      }else{
        $ket = 'Error';
        $color = 'btn btn-warning';
        $style = 'display: none;';
        $undangan ='';
        $kartu = '';
    }
  ?>
  <tr>
      <td style="text-align: center;"><?php echo $no; ?></td>
      <td  style="display: none;"><?php echo $data->msPesertaId; ?></td>
      <td ><?php echo $data->msPesertaNik; ?></td>
      <td ><?php echo $data->msPesertaNoHp; ?></td>
      <td ><?php echo $data->msPesertaNamaPerusahaan; ?></td>
      <td ><?php echo $data->msPesertaPemilik; ?></td>
      <td ><?php echo $data->msPesertaPenanggungJawab; ?></td>
      <td ><?php echo $data->msPesertaAlamat; ?></td>
      <td>
        <center>
          <p class="<?php echo $color; ?>"><?php echo $ket; ?></p>
        </center>
      </td>
      <td>
        <button class="btn btn-success" style="<?php echo $style; ?>"><p style="display: none;"><?php echo $data->msPesertaId; ?></p>-Edit</button>
      </td>
      <td>
        <button class="btn btn-danger"><p style="display: none;"><?php echo $data->msPesertaId; ?></p>-Hapus</button>
      </td>
      <td>
        <a style='<?php echo $style; ?>' href="<?php echo base_url("cetakundangan?id_peserta=$data->msPesertaId"); ?>"  target="_blank" class="btn btn-success"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Undangan</a>

        <a style='<?php echo $style; ?>' href="<?php echo base_url("cetakkartu?id_peserta=$data->msPesertaId"); ?>"  target="_blank" class="btn btn-warning"><span class="glyphicon glyphicon-print"></span>&nbsp;&nbsp;Kartu</a>
      </td>      
    </tr>
  <?php     
  $no++;
  }
?>