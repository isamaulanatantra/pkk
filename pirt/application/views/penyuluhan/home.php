<style>
  .box-form {
    background-color: white;
    margin-bottom: 15px;
    margin-left: 5px;
    margin-right: 5px;
  }
</style>

<div class="msg" style="display:visible;">
  <?php echo @$this->session->flashdata('msg'); ?>
</div>

<?php 
  $id = $this->userdata->msUserId;
  $nik = $this->userdata->msUserNik;
  $no_hp = $this->userdata->msUserNoHp;
?>

<?php 
  if ($dataTotal < 1 ) {
    ?> 
    <div class="row">
      <div class="btnTambah" id='btnTambah' name='btnTambah' style="display: visible;">
        <div  class="col-md-12">
          <div class="box">
            <div class="box-header">
              <div class="box-name">
                <div class="col-md-8 col-md-offset-4 ">
                  <button class="btn btn-success btn-lg" id="daftar" name="daftar" ><i class="glyphicon glyphicon-plus"></i>&nbsp;PENDAFTARAN PENYULUHAN KEAMANAN PANGAN</button>
                </div> 
              </div>        
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id='form_isian' name='form_isian' style="display: none;">
      <div class="row">      
        <div  class="col-md-4 col-md-offset-4">
          <div class="box">
            <div class="box-header">
              <div class="box-name">            
                  <center>
                    <label>FORM INPUT</label>                     
                  </center>
                  <div class="box-form">                   

                      <div class="col-md-12">
                        <div class="form-group" style="display: none;">
                          <label for="id_peserta">Id Peserta</label>
                          <input class="form-control" id="id_peserta" name="id_peserta" value="" placeholder="Id Peserta" type="text" required="">
                        </div>            

                        <div class="form-group" style="display: none;">
                          <label for="id_user">Id User</label>
                          <input class="form-control" id="id_user" name="id_user" placeholder="Id Peserta" type="text" required="" value="<?php echo $id; ?>">
                        </div>            

                        <div class="form-group">
                          <label for="nik">NIK</label>
                          <input class="form-control" id="nik" name="nik" placeholder="No NIK" type="text" value="<?php echo $nik; ?>" >
                        </div> 

                        <div class="form-group">
                          <label for="no_hp">No HP</label>
                          <input class="form-control" id="no_hp" name="no_hp" placeholder="No HP" type="text" value="<?php echo $no_hp; ?>" >
                        </div> 

                        <div class="form-group">
                          <label for="nama_perusahaan">Nama Perusahaan</label>
                          <input class="form-control" id="nama_perusahaan" name="nama_perusahaan" value="" placeholder="Nama Perusahaan" type="text" required="" >
                        </div> 

                        <div class="form-group">
                          <label for="nama_pemilik">Nama Pemilik</label>
                          <input class="form-control" id="nama_pemilik" name="nama_pemilik" value="" placeholder="Nama Pemilik" type="text" required="" >
                        </div> 

                        <div class="form-group">
                          <label for="nama_penanggung_jawab">Nama Penanggung Jawab</label>
                          <input class="form-control" id="nama_penanggung_jawab" name="nama_penanggung_jawab" value="" placeholder="Nama Penanggung Jawab" type="text" required="" >
                        </div> 


                        <div class="form-group">
                          <label for="alamat">Alamat</label>
                          <textarea class="form-control" rows="5" id="alamat" name="alamat" value="" placeholder="Alamat" type="text" required="" ></textarea>
                        </div> 

                      <center>
                        <button class="btn btn-success" id="simpan" name="simpan" ><i class="fa fa-save"></i>&nbsp;SIMPAN</button>
                        <button class="btn btn-danger" id="batal" name="batal" ><i class="fa fa-close"></i>&nbsp;BATAL</button>
                      </center>

                      </div>
                  </div>        
              </div>        
            </div>
          </div>
        </div>
      </div>
    </div>


    <?php 
  }else{
    ?>
      <div class="peserta" id="peserta" name="peserta" style="display: visible;">
        <div class="row">
          <div  class="col-md-12">
            <div class="box">
              <div class="box-header">
                <div class="box-name">                  
                  <label>LISTING DATA</label> 
                </div>        
              </div>          

              <div class="box-form">
                <div class="box-content no-padding table-responsive">
                  <table id="list-data2" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style="text-align: center;">NO</th>
                        <th style="display: none;">Id</th>
                        <th style="text-align: center;">NIK</th>
                        <th style="text-align: center;">No Hp</th>
                        <th style="text-align: center;">Nama Perusahaan</th>
                        <th style="text-align: center;">Nama Pemilik</th>
                        <th style="text-align: center;">Penanggung Jawab</th>
                        <th style="text-align: center;">Alamat</th>
                        <th style="text-align: center;">Status</th>
                        <th style="text-align: center;" colspan="2" >Aksi</th>
                        <th style="text-align: center;" >Cetak</th>
                      </tr>
                    </thead>
                    <tbody id="tblUtama">            
                    </tbody>
                  </table>
                </div>
                <br>
              </div>
            </div>
          </div>          
        </div>
      </div>

      <div id='form_edit' name='form_edit' style="display: none;">
      <div class="row">      
        <div  class="col-md-4 col-md-offset-4">
          <div class="box">
            <div class="box-header">
              <div class="box-name">            
                  <center>
                    <label>FORM EDIT</label>                     
                  </center>
                  <div class="box-form">                   

                      <div class="col-md-12">
                        <div class="form-group" style="display: none;">
                          <label for="id_peserta_edit">Id Peserta</label>
                          <input class="form-control" id="id_peserta_edit" name="id_peserta_edit" value="" placeholder="Id Peserta" type="text" required="">
                        </div>            

                        <div class="form-group" style="display: none;">
                          <label for="id_user_edit">Id User</label>
                          <input class="form-control" id="id_user_edit" name="id_user_edit" placeholder="Id Peserta" type="text">
                        </div>            

                        <span style="display:none;" id="spin_cari" class="input-group-addon"><i class="fa fa-refresh fa-spin"></i></span>

                        <div class="form-group">
                          <label for="nik_edit">NIK</label>
                          <input class="form-control" id="nik_edit" name="nik_edit" placeholder="No NIK" type="text" >
                        </div> 

                        <div class="form-group">
                          <label for="no_hp_edit">No HP</label>
                          <input class="form-control" id="no_hp_edit" name="no_hp_edit" placeholder="No HP" type="text" >
                        </div> 

                        <div class="form-group">
                          <label for="nama_perusahaan_edit">Nama Perusahaan</label>
                          <input class="form-control" id="nama_perusahaan_edit" name="nama_perusahaan_edit" value="" placeholder="Nama Perusahaan" type="text" required="" >
                        </div> 

                        <div class="form-group">
                          <label for="nama_pemilik_edit">Nama Pemilik</label>
                          <input class="form-control" id="nama_pemilik_edit" name="nama_pemilik_edit" value="" placeholder="Nama Pemilik" type="text" required="" >
                        </div> 

                        <div class="form-group">
                          <label for="nama_penanggung_jawab_edit">Nama Penanggung Jawab</label>
                          <input class="form-control" id="nama_penanggung_jawab_edit" name="nama_penanggung_jawab_edit" value="" placeholder="Nama Penanggung Jawab" type="text" required="" >
                        </div> 


                        <div class="form-group">
                          <label for="alamat_edit">Alamat</label>
                          <textarea class="form-control" rows="5" id="alamat_edit" name="alamat_edit" value="" placeholder="Alamat" type="text" required="" ></textarea>
                        </div> 

                      <center>
                        <button class="btn btn-primary" id="update" name="update" ><i class="fa fa-save"></i>&nbsp;UPDATE</button>
                        <button class="btn btn-danger" id="batal_update" name="batal_update" ><i class="fa fa-close"></i>&nbsp;BATAL</button>
                      </center>

                      </div>
                  </div>        
              </div>        
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php 
  }
?> 

<script>
  $(function () {
      $(".select2").select2();

      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_flat-blue',
        radioClass: 'iradio_flat-blue'
      });
  });
</script>

<script>
  function tampil() {
    $.get('<?php echo base_url('Pendaftaran_penyuluhan/tampilPeserta'); ?>', function(data) {
      MyTable.fnDestroy();
      $('#tblUtama').html(data);
      refresh();
    });
  }
</script>

<script>
  function baru(){
    $('#id_peserta').val('');
    // $('#nik').val('');
    // $('#no_hp').val('');
    $('#nama_perusahaan').val('');
    $('#nama_pemilik').val('');
    $('#nama_penanggung_jawab').val('');
    $('#alamat').val('');

    $('#form_isian').hide();
    $('#btnTambah').show();

  };
</script>

<script>
  baru();
  tampil();
</script>

<script>
  $('#daftar').on('click', function(){
    $('#form_isian').show();
    $('#btnTambah').hide();
    $('#update').hide();
  });
</script>

<script>
  $('#batal').on('click', function(){
    baru();
  });
</script>

<script>
  $('#simpan').on('click', function(){
      var id_peserta = $("#id_peserta").val();
      var id_user = $("#id_user").val();
      var nik = $("#nik").val();
      var no_hp = $("#no_hp").val();
      var nama_perusahaan = $("#nama_perusahaan").val();
      var nama_pemilik = $("#nama_pemilik").val();
      var nama_penanggung_jawab = $("#nama_penanggung_jawab").val();
      var alamat = $("#alamat").val();
            
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_user:id_user,
            nik:nik,
            no_hp:no_hp,
            nama_perusahaan:nama_perusahaan,
            nama_pemilik:nama_pemilik,
            nama_penanggung_jawab:nama_penanggung_jawab,
            alamat:alamat
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Pendaftaran_penyuluhan/insertPeserta',
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else if( text == 3 ){
              alertify.error("NIK sudah terdaftar");

            }else{
              alertify.success("Data berhasil disimpan");
              baru();
              window.location.href = "<?php echo base_url('Pendaftaran_penyuluhan'); ?>";
            }
          }
      });
  });
</script>


<script>
  $('#tblUtama').on('click','td:eq(9)',function(){
    
      var id = $( this ).text();   
      var str_id_peserta = id.split("-");

      var str_id_peserta = str_id_peserta[0];
      var id_peserta = str_id_peserta.trim();
      
      $('#id_peserta_edit').val(id_peserta);
      load_peserta_by_id();

      $('#form_edit').show();
      $('#peserta').hide();

      

  });
</script>

<script>
  $('#batal_update').on('click', function(){
      $('#form_edit').hide();
      $('#peserta').show();
  });
</script>


<script>
  function load_peserta_by_id(){
    $("#spin_cari").show();      

    var id_peserta = $('#id_peserta_edit').val();
  
    $.ajax({
        type: "POST", 
        url: "<?php echo base_url(); ?>Pendaftaran_penyuluhan/load_peserta_by_id", 
        data: {id_peserta : id_peserta}, 
        dataType: "json",
        beforeSend: function(e) {
            if(e && e.overrideMimeType) {
                e.overrideMimeType("application/json;charset=UTF-8");
            }
    },
    success: function(response){ 
        $("#spin_cari").hide();
        if(response.status == "success"){         

          $('#id_peserta_edit').val(response.id);
          $('#nik_edit').val(response.nik);
          $('#no_hp_edit').val(response.no_hp);
          $('#nama_perusahaan_edit').val(response.nama_perusahaan);
          $('#nama_pemilik_edit').val(response.nama_pemilik);
          $('#nama_penanggung_jawab_edit').val(response.nama_penanggung_jawab);
          $('#alamat_edit').val(response.alamat);


        }else{ 
          $("#spin_cari").hide();
          alert("Data Tidak Ditemukan");
          // location.href ="<?php echo base_url(); ?>mesin_antrian";
        }
    },
    error: function (xhr, ajaxOptions, thrownError) { 
      alert(xhr.responseText);
    }
    });
}
</script>


<script>
  $('#update').on('click', function(){
      var id_peserta_edit = $("#id_peserta_edit").val();
      var nik_edit = $("#nik_edit").val();
      var no_hp_edit = $("#no_hp_edit").val();
      var nama_perusahaan_edit = $("#nama_perusahaan_edit").val();
      var nama_pemilik_edit = $("#nama_pemilik_edit").val();
      var nama_penanggung_jawab_edit = $("#nama_penanggung_jawab_edit").val();
      var alamat_edit = $("#alamat_edit").val();
            
      $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_peserta_edit:id_peserta_edit,
            nik_edit:nik_edit,
            no_hp_edit:no_hp_edit,
            nama_perusahaan_edit:nama_perusahaan_edit,
            nama_pemilik_edit:nama_pemilik_edit,
            nama_penanggung_jawab_edit:nama_penanggung_jawab_edit,
            alamat_edit:alamat_edit
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Pendaftaran_penyuluhan/updatePeserta',
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal disimpan");

            }else if( text == 3 ){
              alertify.error("NIK sudah terdaftar");

            }else{
              alertify.success("Data berhasil disimpan");
              baru();
              window.location.href = "<?php echo base_url('Pendaftaran_penyuluhan'); ?>";
            }
          }
      });
  });
</script>

<script>
  $('#tblUtama').on('click','td:eq(10)',function(){
    
      var id = $( this ).text();   
      var str_id_peserta = id.split("-");

      var str_id_peserta = str_id_peserta[0];
      var id_peserta = str_id_peserta.trim();

      alertify.confirm("Apakah anda yakin hapus data ini?", function (e) {
        if (e) {
          $.ajax({
        type: "POST",
        async: true, 
        data: {
            id_peserta:id_peserta
          }, 
        dataType: "text",
        url: '<?php echo base_url(); ?>Pendaftaran_penyuluhan/hapusPeserta',   
        success: function(text) {
            if( text == 0 ){
              alertify.error("Data belum lengkap");
            }else if( text == 1 ){
              alertify.error("Data gagal dihapus");

            }else{
              alertify.success("Data berhasil dihapus");
              baru();
              window.location.href = "<?php echo base_url('Pendaftaran_penyuluhan'); ?>";
            }
          }
      });
        } else {
          alertify.error("Batal hapus data");
        }
      });
      return false;

  });
</script>