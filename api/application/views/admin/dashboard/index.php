<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

<script src="<?php echo base_url($plugins_dir . '/simple_chart/Chart.js'); ?>"></script>

<?php
    $table = '';

    /* Mengambil query report*/
    echo json_encode($report);
    foreach($report as $result){
        $hari[] = $result->Day; //ambil hari
        $value[] = (float) $result->Total; //ambil nilai
        $query3 = $this->db->query("SELECT api_name FROM api_log GROUP BY api_name");
        $jumlah_api = $query3->num_rows();
        $random10 = rand(0,255);
        $random20 = rand(0,255);
        $random30 = rand(0,255);
        for ($i=1; $i <= $jumlah_api ; $i++) { 
            ${'value'.$i}[] = (float) $result->{'Api'.$i}; //ambil nilai
            ${'random1'.$i} = rand(0,255);
            ${'random2'.$i} = rand(0,255);
            ${'random3'.$i} = rand(0,255);
        }

        $ket = "";
        $i = 1;
        foreach($query3->result() as $data3){
            $ket .= '<tr><td style="width:20px;background-color:rgba('.${'random1'.$i}.','.${'random2'.$i}.','.${'random3'.$i}.',0.5)!important;color:rgba('.${'random1'.$i}.','.${'random2'.$i}.','.${'random3'.$i}.',0.5)!important;">&block;&block;</td><td> &nbsp;Nama Webservice:&nbsp;'.$data3->api_name.'</td></tr>';
            $i++;
        }

        $table .= "<tr><td>".$result->Day."</td>";
         
        $i = 1;        
        $query1 = $this->db->query("SELECT id, company FROM api_webservice_client");
        foreach($query1->result() as $data){
            $query2 = $this->db->query("SELECT COUNT(*) AS 'Total' FROM api_log WHERE client = $data->id AND date(datetime_request) = '$result->Day'");
            foreach($query2->result() as $data2){
                if($data2->Total != 0){
                    if($i == 1)
                    {
                        $table .= "<td>".$data->company."</td><td>".$data2->Total."</td></tr>";
                    }else{
                        $table .= "<td></td><td>".$data->company."</td><td>".$data2->Total."</td></tr>";
                    }
                }
            }
            $i++;
        }
    }
    /* end mengambil query*/
     
?>
<style>
.datepicker{z-index:9999 !important}
#myProgress {
  position: relative;
  width: 100%;
  height: 5px;
  background-color: #ddd;
}

#myBar {
  position: absolute;
  width: 1%;
  height: 100%;
  background-color: #8B008B;
}
</style>
<form role="form" id="form_pertanyaan" method="post" action="" enctype="multipart/form-data">
  <div class="modal fade" id="FormReviewer" tabindex="-1" role="dialog" aria-labelledby="myReviewer" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myReviewer">Silahkan tentukan peride yang ingin anda tampilkan</h4>
        </div>
        <div class="modal-body">
            <div class="box-body">
              <div class="form-group">
                <label for="dari_tanggal">Dari Tanggal</label>
                <input type="hidden" name="periodik" id="periodik" value="periodik">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input class="form-control pull-right" placeholder="Dari Tanggal" name="dari_tanggal" value="<?php echo $dari_tanggal; ?>" id="dari_tanggal" type="text">
                </div>                
              </div>
              <div class="form-group">
                <label for="sampai_tanggal">Sampai Tanggal</label>
                <input type="hidden" name="periodik" id="periodik" value="periodik">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div> 
                    <input class="form-control pull-right" placeholder="Sampai Tanggal" name="sampai_tanggal" value="<?php echo $sampai_tanggal; ?>" id="sampai_tanggal" type="text">
                </div>    
              </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" id="tampilkan"><i id="overlay" style="display:none;" class="fa fa-refresh fa-spin"></i> Tampilkan</button>
        </div>
      </div>
    </div>
  </div>
</form>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <!--<?php echo $dashboard_alert_file_install; ?>-->
                    <div class="row">
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-maroon"><i class="fa fa-legal"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Licence</span>
                                    <span class="info-box-number">Free</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Webservice Client</span>
                                    <span class="info-box-number"><?php echo $api_webservice_client; ?></span>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix visible-sm-block"></div>

                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-user"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Users</span>
                                    <span class="info-box-number"><?php echo $count_users; ?></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="info-box">
                                <span class="info-box-icon bg-aqua"><i class="fa fa-shield"></i></span>
                                <div class="info-box-content">
                                    <span class="info-box-text">Security groups</span>
                                    <span class="info-box-number"><?php echo $count_groups; ?></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                        </div>

                        <div class="col-md-12">
                            <div class="box">
                                <div class="box-header with-border">
                                    <div class="box-tools pull-right">
                                        <a href="#" id="pop_reviewer" data-toggle="modal" data-target="#FormReviewer" class="btn btn-default" ><i class="fa fa-fw fa-calendar"></i> Pilih Periode Lain</a>
                                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                                    </div>
                                    <h3 class="box-title"><b>Dashboard</b> - <?php echo $periode; ?></h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p class="text-left text-uppercase"><strong>Grafik Jumlah Permintaan Webservice</strong></p>
                                            <div>
                                                <canvas id="canvas" height="100"></canvas>
                                                <br>
                                                <table width="100%">
                                                <tr><td style="width:20px;background-color:rgba(<?php echo $random10; ?>,<?php echo $random20; ?>,<?php echo $random30; ?>,0.5) !important;color:rgba(<?php echo $random10; ?>,<?php echo $random20; ?>,<?php echo $random30; ?>,0.5) !important;">&block;&block;</td><td>&nbsp;Total Permintaan Webservice</td></tr>                                                
                                                <?php echo $ket;?>
                                                </table>
                                            </div>

                                            <script>
                                            //var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

                                            var barChartData = {
                                                labels : <?php echo json_encode($hari); ?>,
                                                datasets : [
                                                    <?php 
                                                    if ($jumlah_api != 0)
                                                    {
                                                        for ($j=1;$j<=$jumlah_api;$j++)
                                                        {
                                                    ?>
                                                    {
                                                        fillColor : "rgba(<?php echo ${'random1'.$j}; ?>,<?php echo ${'random2'.$j}; ?>,<?php echo ${'random3'.$j}; ?>,0.5)",
                                                        strokeColor : "rgba(<?php echo ${'random1'.$j}; ?>,<?php echo ${'random2'.$j}; ?>,<?php echo ${'random3'.$j}; ?>,0.8)",
                                                        highlightFill : "rgba(<?php echo ${'random1'.$j}; ?>,<?php echo ${'random2'.$j}; ?>,<?php echo ${'random3'.$j}; ?>,0.75)",
                                                        highlightStroke : "rgba(<?php echo ${'random1'.$j}; ?>,<?php echo ${'random2'.$j}; ?>,<?php echo ${'random3'.$j}; ?>,1)",
                                                        data : <?php echo json_encode(${'value'.$j}); ?>
                                                    },
                                                    <?php 
                                                        }
                                                    }
                                                    ?>

                                                    {
                                                        fillColor : "rgba(<?php echo $random10; ?>,<?php echo $random20; ?>,<?php echo $random30; ?>,0.5)",
                                                        strokeColor : "rgba(<?php echo $random10; ?>,<?php echo $random20; ?>,<?php echo $random30; ?>,0.8)",
                                                        highlightFill: "rgba<?php echo $random10; ?>,<?php echo $random20; ?>,<?php echo $random30; ?>,0.75)",
                                                        highlightStroke: "rgba(<?php echo $random10; ?>,<?php echo $random20; ?>,<?php echo $random30; ?>,1)",
                                                        data : <?php echo json_encode($value); ?>
                                                    },

                                                ]

                                            }
                                            window.onload = function(){
                                                var ctx = document.getElementById("canvas").getContext("2d");
                                                window.myBar = new Chart(ctx).Bar(barChartData, {
                                                    responsive : true
                                                });
                                            }

                                            </script>
                                        </div>
                                        <div class="col-md-6">
                                            <p class="text-left text-uppercase"><strong>Detail Jumlah Permintaan Webservice</strong></p>
                                            <table class="table table-striped table-hover table-condensed">
                                                <thead>
                                                    <tr>
                                                        <th>Tanggal</th> 
                                                        <th>Nama Instansi</th>
                                                        <th>Jumlah</th>                                                        
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php echo $table; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

<script>
function move() {
  var elem = document.getElementById("myBar");   
  var width = 1;
  var id = setInterval(frame, 50);
  function frame() {
    if (width >= 50) {
      clearInterval(id);
    } else {
      width++; 
      elem.style.width = ''+width + '%'; 
    }
  }
}
</script>

<script type="text/javascript">
  $(function () {
    $('#dari_tanggal').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
  // When the document is ready
  $(document).ready(function() {
    $('#sampai_tanggal').datepicker({
      format: 'yyyy-mm-dd',
    }).on('changeDate', function(e) {
      $(this).datepicker('hide');
    });
  });
  $(document).ready(function() {
  });
</script>

<script type="text/javascript">
$(document).ready(function() {
  move();
    $('#tampilkan').on('click', function() {
    $('#overlay').show();
    });
});
</script>