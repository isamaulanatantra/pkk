<?php
defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <div class="content-wrapper">
                <section class="content-header">
                    <?php echo $pagetitle; ?>
                    <?php echo $breadcrumb; ?>
                </section>

                <section class="content">
                    <div class="row">
                        <div class="col-md-12">
                             <div class="box">
                                <div class="box-header with-border">
                                    <h3 class="box-title"><?php echo anchor('admin/webservice_client/create', '<i class="fa fa-plus"></i> '. lang('webservice_client_create_user'), array('class' => 'btn btn-block btn-primary btn-flat')); ?></h3>
                                </div>
                                <div class="box-body">
                                    <table class="table table-striped table-hover">
                                        <thead>
                                            <tr>
                                                <th><?php echo lang('webservice_client_company');?></th>
                                                <th><?php echo lang('webservice_client_consumerID');?></th>
                                                <th><?php echo lang('webservice_client_kdAplikasi');?></th>
                                                <!--<th><?php echo lang('webservice_client_status');?></th>-->
                                                <th><?php echo lang('webservice_client_action');?></th>
                                            </tr>
                                        </thead>
                                        <tbody>

<?php 
    $webservice = json_decode($webservice_client); 
    foreach ($webservice as $webservice_client):?>
                                            <tr>
                                                <td><?php echo htmlspecialchars($webservice_client->company, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($webservice_client->consumerID, ENT_QUOTES, 'UTF-8'); ?></td>
                                                <td><?php echo htmlspecialchars($webservice_client->kdAplikasi, ENT_QUOTES, 'UTF-8'); ?></td>
<?php
/*
foreach ($user->groups as $group)
{

    // Disabled temporary !!!
    // echo anchor('admin/groups/edit/'.$group->id, '<span class="label" style="background:'.$group->bgcolor.';">'.htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8').'</span>');
    echo anchor('admin/groups/edit/'.$group->id, '<span class="label label-default">'.htmlspecialchars($group->name, ENT_QUOTES, 'UTF-8').'</span>');
}
*/
?>
                                                </td>
                                                <!--<td><?php echo ($webservice_client->active) ? anchor('admin/webservice_client/deactivate/'.$webservice_client->id, '<span class="label label-success">'.lang('webservice_client_active').'</span>') : anchor('admin/webservice_client/activate/'. $webservice_client->id, '<span class="label label-default">'.lang('webservice_client_inactive').'</span>'); ?></td>-->
                                                <td>
                                                    <?php echo anchor('admin/webservice_client/edit/'.$webservice_client->id, lang('actions_edit')); ?>&nbsp;
                                                    <!--<?php echo anchor('admin/webservice_client/profile/'.$webservice_client->id, lang('actions_see')); ?>-->
                                                </td>
                                            </tr>
<?php endforeach;?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                         </div>
                    </div>
                </section>
            </div>
