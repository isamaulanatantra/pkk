<?php
/**
 * @package	CodeIgniter
 * @author	domProjects Dev Team
 * @copyright   Copyright (c) 2015, domProjects, Inc. (http://domProjects.com/)
 * @license http://opensource.org/licenses/MIT	MIT License
 * @link    http://domProjects.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['webservice_client_action']              = 'Action';
$lang['webservice_client_active']              = 'Active';
$lang['webservice_client_company']             = 'Company';
$lang['webservice_client_create_user']         = 'Create Webservice CLient';
$lang['webservice_client_created_on']          = 'Created on';
$lang['webservice_client_deactivate_question'] = 'Are you sure you want to deactivate the Webservice CLient %s';
$lang['webservice_client_edit_user']           = 'Edit Webservice CLient';
$lang['webservice_client_consumerID']          = 'consumer ID /User name';
$lang['webservice_client_consumerSecret']         = 'Password';
$lang['webservice_client_consumerSecret_confirm'] = 'Password confirm';
$lang['webservice_client_kdAplikasi']              = 'kdAplikasi';
$lang['webservice_client_inactive']            = 'Inactive';
$lang['webservice_client_company']          = 'Nama Instansi';
$lang['webservice_client_member_of_groups']    = 'Member of groups';
$lang['webservice_client_status']              = 'Status';
