<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->load->helper('number');
        $this->load->model('admin/dashboard_model');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            $dari_tanggal = trim($this->input->post('dari_tanggal'));
            $sampai_tanggal = trim($this->input->post('sampai_tanggal'));
            $periodik = trim($this->input->post('periodik'));
            $hari = date('Y-m-d');
            $data['hari'] = date('Y-m-d');
            /* Title Page */
            $this->page_title->push(lang('menu_dashboard'));
            $this->data['pagetitle'] = $this->page_title->show();

            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Data */
            $this->data['api_webservice_client']= $this->dashboard_model->get_count_record('api_webservice_client');
            $this->data['count_users']       = $this->dashboard_model->get_count_record('api_users');
            $this->data['count_groups']      = $this->dashboard_model->get_count_record('api_groups');
            $this->data['disk_totalspace']   = $this->dashboard_model->disk_totalspace(DIRECTORY_SEPARATOR);
            $this->data['disk_freespace']    = $this->dashboard_model->disk_freespace(DIRECTORY_SEPARATOR);
            $this->data['disk_usespace']     = $this->data['disk_totalspace'] - $this->data['disk_freespace'];
            $this->data['disk_usepercent']   = $this->dashboard_model->disk_usepercent(DIRECTORY_SEPARATOR, FALSE);
            $this->data['memory_usage']      = $this->dashboard_model->memory_usage();
            $this->data['memory_peak_usage'] = $this->dashboard_model->memory_peak_usage(TRUE);
            $this->data['memory_usepercent'] = $this->dashboard_model->memory_usepercent(TRUE, FALSE);
            if($periodik == 'periodik'){
                $this->data['periode'] = 'Dari Tanggal '.$dari_tanggal.' - Sampai Tanggal '.$sampai_tanggal.' ';
                $this->data['dari_tanggal'] = ''.$dari_tanggal.'';
                $this->data['sampai_tanggal'] = ''.$sampai_tanggal.'';
                $this->data['report'] = $this->report($dari_tanggal,$sampai_tanggal);
                }
            else{
                $this->data['periode'] = "* 7 Hari Terakhir";
                $this->data['dari_tanggal'] = ''.date('Y-m-d', strtotime('-7 days')).'';
                $this->data['sampai_tanggal'] = ''.date('Y-m-d').'';
                $this->data['report'] = $this->report(date('Y-m-d', strtotime('-7 days')),date('Y-m-d'));
                }


            /* TEST */
            $this->data['url_exist']    = is_url_exist('https://www.wonosobokab.go.id');


            /* Load Template */
            $this->template->admin_render('admin/dashboard/index', $this->data);
        }
	}

    function report($dari_tanggal,$sampai_tanggal){
        $column = '';
        $query0 = $this->db->query("SELECT api_name FROM api_log GROUP BY api_name");
        $i=1;
        foreach($query0->result() as $data0){
            $column .= ", COUNT(IF( api_name = '$data0->api_name', api_name, NULL)) AS Api".$i."";
            $i++;
        }

        $query = $this->db->query("SELECT date(datetime_request) AS 'Day', COUNT(*) AS 'Total'$column FROM api_log WHERE date(datetime_request) >= '$dari_tanggal' AND date(datetime_request) <= '$sampai_tanggal' GROUP BY date(datetime_request) ORDER BY date(datetime_request)");
         
        if($query->num_rows() > 0){
            foreach($query->result() as $data){
                $hasil[] = $data;
            }
            return $hasil;
        }
    }
}
