<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Webservice_client extends Admin_Controller {

    public function __construct()
    {
        parent::__construct();

        /* Load :: Common */
        $this->lang->load('admin/webservice_client');
        $this->load->model('admin/webservice_client_model');

        /* Title Page :: Common */
        $this->page_title->push(lang('menu_webservice_client'));
        $this->data['pagetitle'] = $this->page_title->show();

        /* Breadcrumbs :: Common */
        $this->breadcrumbs->unshift(1, lang('menu_webservice_client'), 'admin/webservice_client');
    }


	public function index()
	{
        if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
        {
            redirect('auth/login', 'refresh');
        }
        else
        {
            /* Breadcrumbs */
            $this->data['breadcrumb'] = $this->breadcrumbs->show();

            /* Get all webservice_client */
            /*$this->data['webservice_client'] = $this->webservice_client_model->get_data('api_webservice_client');
            foreach ($this->data['webservice_client'] as $k => $user)
            {
                $this->data['webservice_client'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
            }*/
            $query = $this->db->get('api_webservice_client');

            if ($query->num_rows() > 0)
            {
                $this->data['webservice_client'] = json_encode($query->result_array());
            }
            else
            {
                return FALSE;
            }

            /* Load Template */
            $this->template->admin_render('admin/webservice_client/index', $this->data);
        }
	}


	public function create()
	{
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_webservice_client_create'), 'admin/webservice_client/create');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('consumerID', 'lang:webservice_client_consumerID', 'required');
		$this->form_validation->set_rules('kdAplikasi', 'lang:webservice_client_kdAplikasi', 'required');
		$this->form_validation->set_rules('company', 'lang:webservice_client_company', 'required');
		$this->form_validation->set_rules('consumerSecret', 'lang:webservice_client_consumerSecret', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[consumerSecret_confirm]');
		$this->form_validation->set_rules('consumerSecret_confirm', 'lang:webservice_client_consumerSecret_confirm', 'required');

		if ($this->form_validation->run() == TRUE)
		{    
      $data_insert = array(
        'api_webservice_client.id' => '',
        'api_webservice_client.consumerID' => $this->input->post('consumerID'),
        'api_webservice_client.consumerSecret' => $this->input->post('consumerSecret'),
        'api_webservice_client.kdAplikasi' => $this->input->post('kdAplikasi'),
        'api_webservice_client.created_on' => date('Y-m-d H:i:s'),
        'api_webservice_client.company' => $this->input->post('company'),
        'api_webservice_client.active' => 1
      );
    }

    if ($this->form_validation->run() == TRUE && $this->db->insert('api_webservice_client', $data_insert))
		{
			redirect('admin/webservice_client', 'refresh');
		}
		else
		{
            $this->data['message'] = (validation_errors());

			$this->data['consumerID'] = array(
				'name'  => 'consumerID',
				'id'    => 'consumerID',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('consumerID'),
			);
      $this->data['consumerSecret'] = array(
        'name'  => 'consumerSecret',
        'id'    => 'consumerSecret',
        'type'  => 'text',
                'class' => 'form-control',
        'value' => $this->form_validation->set_value('consumerSecret'),
      );
      $this->data['consumerSecret_confirm'] = array(
        'name'  => 'consumerSecret_confirm',
        'id'    => 'consumerSecret_confirm',
        'type'  => 'text',
                'class' => 'form-control',
        'value' => $this->form_validation->set_value('consumerSecret_confirm'),
      );
			$this->data['kdAplikasi'] = array(
				'name'  => 'kdAplikasi',
				'id'    => 'kdAplikasi',
				'type'  => 'kdAplikasi',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('kdAplikasi'),
			);
			$this->data['company'] = array(
				'name'  => 'company',
				'id'    => 'company',
				'type'  => 'text',
                'class' => 'form-control',
				'value' => $this->form_validation->set_value('company'),
			);

            /* Load Template */
            $this->template->admin_render('admin/webservice_client/create', $this->data);
        }
	}


	public function delete()
	{
        /* Load Template */
		$this->template->admin_render('admin/webservice_client/delete', $this->data);
	}

	public function edit($id)
	{
        $id = (int) $id;

		if ( ! $this->ion_auth->logged_in() OR ( ! $this->ion_auth->is_admin()))
		{
			redirect('auth', 'refresh');
		}

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_webservice_client_edit'), 'admin/webservice_client/edit');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();


    //Select Data    
    $webservice_client          = $this->ion_auth->api_webservice_client($id)->row();

		/* Validate form input */
    $this->form_validation->set_rules('consumerID', 'lang:webservice_client_consumerID', 'required');
    $this->form_validation->set_rules('kdAplikasi', 'lang:webservice_client_kdAplikasi', 'required');
    $this->form_validation->set_rules('company', 'lang:webservice_client_company', 'required');
    $this->form_validation->set_rules('consumerSecret', 'lang:webservice_client_consumerSecret', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']');

    if ($this->form_validation->run() == TRUE)
    {    

    //Update Data
    $where = array(
       'api_webservice_client.id' => $id
    );
      
    $data_update = array(
        'api_webservice_client.consumerID' => $this->input->post('consumerID'),
        'api_webservice_client.consumerSecret' => $this->input->post('consumerSecret'),
        'api_webservice_client.kdAplikasi' => $this->input->post('kdAplikasi'),
        'api_webservice_client.company' => $this->input->post('company'),
    );
    
    $this->db->where($where);

    }

    if ($this->form_validation->run() == TRUE && $this->db->update('api_webservice_client', $data_update))
    {
      redirect('admin/webservice_client', 'refresh');
    }
    else
    {

      $this->data['message'] = (validation_errors());
      $this->data['api_webservice_client'] = $webservice_client;

      $this->data['consumerID'] = array(
        'name'  => 'consumerID',
        'id'    => 'consumerID',
        'type'  => 'text',
                'class' => 'form-control',
        'value' => $this->form_validation->set_value('consumerID', $this->data['api_webservice_client']->consumerID),
      );
      $this->data['consumerSecret'] = array(
        'name'  => 'consumerSecret',
        'id'    => 'consumerSecret',
        'type'  => 'text',
                'class' => 'form-control',
        'value' => $this->form_validation->set_value('consumerSecret', $webservice_client->consumerSecret),
      );
      $this->data['kdAplikasi'] = array(
        'name'  => 'kdAplikasi',
        'id'    => 'kdAplikasi',
        'type'  => 'kdAplikasi',
                'class' => 'form-control',
        'value' => $this->form_validation->set_value('kdAplikasi', $webservice_client->kdAplikasi),
      );
      $this->data['company'] = array(
        'name'  => 'company',
        'id'    => 'company',
        'type'  => 'text',
                'class' => 'form-control',
        'value' => $this->form_validation->set_value('company', $webservice_client->company),
      );     

        /* Load Template */
		    $this->template->admin_render('admin/webservice_client/edit', $this->data);
        }
	}


	function activate($id, $code = FALSE)
	{
        $id = (int) $id;

		if ($code !== FALSE)
		{
            $activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
            $this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect('admin/webservice_client', 'refresh');
		}
		else
		{
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect('auth/forgot_password', 'refresh');
		}
	}


	public function deactivate($id = NULL)
	{
		if ( ! $this->ion_auth->logged_in() OR ! $this->ion_auth->is_admin())
		{
            return show_error('You must be an administrator to view this page.');
		}

        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_webservice_client_deactivate'), 'admin/webservice_client/deactivate');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

		/* Validate form input */
		$this->form_validation->set_rules('confirm', 'lang:deactivate_validation_confirm_label', 'required');
		$this->form_validation->set_rules('id', 'lang:deactivate_validation_user_id_label', 'required|alpha_numeric');

		$id = (int) $id;

		if ($this->form_validation->run() === FALSE)
		{
			$user = $this->ion_auth->user($id)->row();

            $this->data['csrf']       = $this->_get_csrf_nonce();
            $this->data['id']         = (int) $user->id;
            $this->data['firstname']  = ! empty($user->first_name) ? htmlspecialchars($user->first_name, ENT_QUOTES, 'UTF-8') : NULL;
            $this->data['lastname']   = ! empty($user->last_name) ? ' '.htmlspecialchars($user->last_name, ENT_QUOTES, 'UTF-8') : NULL;

            /* Load Template */
            $this->template->admin_render('admin/webservice_client/deactivate', $this->data);
		}
		else
		{
            if ($this->input->post('confirm') == 'yes')
			{
                if ($this->_valid_csrf_nonce() === FALSE OR $id != $this->input->post('id'))
				{
                    show_error($this->lang->line('error_csrf'));
				}

                if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			redirect('admin/webservice_client', 'refresh');
		}
	}


	public function profile($id)
	{
        /* Breadcrumbs */
        $this->breadcrumbs->unshift(2, lang('menu_webservice_client_profile'), 'admin/groups/profile');
        $this->data['breadcrumb'] = $this->breadcrumbs->show();

        /* Data */
        $id = (int) $id;

        $this->data['user_info'] = $this->ion_auth->user($id)->result();
        foreach ($this->data['user_info'] as $k => $user)
        {
            $this->data['user_info'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
        }

        /* Load Template */
		$this->template->admin_render('admin/webservice_client/profile', $this->data);
	}


	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key   = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return array($key => $value);
	}


	public function _valid_csrf_nonce()
	{
		if ($this->input->post($this->session->flashdata('csrfkey')) !== FALSE && $this->input->post($this->session->flashdata('csrfkey')) == $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}
