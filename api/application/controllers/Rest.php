<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rest extends MY_Controller {

	function __construct()
	{
		parent::__construct();
	}


	function index()
	{
        //Get Url Request
        $keterangan = $this->uri->segment(2);
        $method = $_SERVER['REQUEST_METHOD']; 
        //Cek
        if($this->uri->segment(2) == ''){
            json_encode(array('status' => 400,'message' => 'Bad request.'));
        } else {           
            if($method == 'GET'){
                $response = $this->KuotaModel->check_auth_client();
                if($response['status'] == 200){
                    if($keterangan=="golPegawai"){
                        $time = date("Y-m-d H:i:s");
                        mysql_query("INSERT INTO api_log VALUES ('', '$id_client', 'Jumlah Golongan Pegawai', '$time', '1')")
                            or die (json_encode(array('status' => 403,'message' => 'Internal Query Error')));
                        $querys = "Select gol, jml from golPegawai";
                        $query = mysql_query($querys)
                            or die (json_encode(array('status' => 403,'message' => 'Internal Query Error')));
                        $data = array();
                        while($rows = mysql_fetch_assoc($query)){
                            $data[] = $rows;
                        }
                        return json_encode(array('status' => 200, 'detail' =>'Data Ditemukan','data' =>$data));
                    }else{
                        return json_encode(array('status' => 404, 'detail' =>'Segment yang anda ketik salah', 'data' =>$resp));
                    }                  
                } 
            } else if($method == 'POST'){
                
            } else if($method == 'PUT'){
                
            } else if($method == 'DELETE'){
                
            } else {             
                json_encode(array('status' => 400,'message' => 'Bad request.'));
            }          
        }
	}

    public function check_auth_client(){        
        //Set Time Server
        $maxExpiredReq = 30000; //5 menit      
        date_default_timezone_set('UTC');
        $tStampServer = strval(time()-strtotime('1970-01-01 00:00:00'));       
        //Request Header
        $Xconsid = $this->input->get_request_header('X-id', TRUE);
        $XTimestamp  = $this->input->get_request_header('X-Timestamp', TRUE);
        $XSignature  = $this->input->get_request_header('X-Signature', TRUE);
        //Cek Id Server
        $query_user = mysql_query("SELECT `api_webservice_client`.`id`,`api_webservice_client`.`consumerID`,`api_webservice_client`.`consumerSecret` FROM `api_webservice_client` WHERE `api_webservice_client`.`consumerID` = '$Xconsid' AND `api_webservice_client`.`active` = 1")
            or die (json_encode(array('status' => 403,'message' => 'Internal Query Error')));
        while($rows = @mysql_fetch_array($query_user)){
            $id_client = $rows['id'];
            $consumerID = $rows['consumerID'];
            $consumerSecret = $rows['consumerSecret'];
        }

        $intervalRequest = $tStampServer-$XTimestamp;
        if($Xconsid == $consumerID){
            if($intervalRequest < $maxExpiredReq){
                //Check Auth
                $signature = hash_hmac('sha256', $consumerID."&".$XTimestamp, $consumerSecret, true);
                $encodedSignature = base64_encode($signature);          
                if($encodedSignature == $XSignature){
                    return array('status' => 200,'message' => 'Authorized.');
                }else{
                    return json_encode(array('status' => 401,'message' => 'Unauthorized'));
                }            
            }else{
                return json_encode(array('status' => 401,'message' => 'Unauthorized'));
            }
        }else{
            return json_encode(array('status' => 401,'message' => 'Unauthorized.'));
        }
    }

    function request()
	{
        $keterangan = $this->uri->segment(3);
        echo json_encode(array('keterangan' => 'sddww'.$keterangan));
    }

}
